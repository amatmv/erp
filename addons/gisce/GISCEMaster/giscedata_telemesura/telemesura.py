from __future__ import absolute_import
from osv import fields, osv
from osv.osv import TransactionExecute
from mongodb_backend import osv_mongodb
from pytz import timezone
from tools.translate import _
import logging
from .utils import *
from enerdata.contracts.tariff import get_tariff_by_code
from enerdata.profiles.profile import Profile
from oorq.decorators import job
import csv
import cStringIO as StringIO
from osv.expression import OOQuery
from sql.operators import Concat
from sql import Literal
from bisect import bisect
import time
from dateutil.relativedelta import relativedelta
from enerdata.profiles import Dragger
from datetime import datetime, time as dtime
from giscedata_telemesures_base.utils import prepare_hour_change_profiles

KIND_REAL = '1'
KIND_REAL_PROFILED = '2'
KIND_ADJUSTED = '3'
KIND_AUTOREAD_PROFILED = '4'
KIND_HISTORY_PROFILED = '5'
KIND_USE_FACTOR_PROFILED = '6'

ORIGIN_KIND_PROFILED = {
    '10-00': KIND_REAL_PROFILED,
    '11-00': KIND_REAL_PROFILED,
    '20-00': KIND_REAL_PROFILED,
    '21-00': KIND_REAL_PROFILED,
    '30-00': KIND_REAL_PROFILED,
    '31-00': KIND_REAL_PROFILED,
    '40-01': KIND_HISTORY_PROFILED,
    '40-02': KIND_USE_FACTOR_PROFILED,
    '50-00': KIND_AUTOREAD_PROFILED,
    '60-00': KIND_REAL_PROFILED
}

TIMEZONE = timezone('Europe/Madrid')

logger = logging.getLogger('openerp' + __name__)


class TmBilling(osv_mongodb.osv_mongodb):

    _name = 'tm.billing'
    _order = 'date_end desc'

    _type_billing = [
        ('day', 'Daily'),
        ('month', 'Monthly'),
    ]

    _type_value = [
        ('i', 'Incremental'),
        ('a', 'Absolute'),
    ]

    def get_billing(self, cursor, uid, meter, date,
                    type, num_periods, contract, context=None):
        '''get billing values for date and meter.
        Go back in time until valid billing found or
        no more billing data'''
        #For one period fare, we have to return 0 period of billing
        #The rest of the fares, takes exactly the same period number
        if num_periods == 1:
            periods = [0]
        else:
            periods = range(1, num_periods + 1)
        search_params = [('date_end', '=', date),
                         ('name', '=', meter),
                         ('type', '=', type),
                         ('value', '=', 'a'),
                         ('valid', '=', True),
                         ('period', 'in', periods),
                         ('contract', '=', contract)]
        billing_ids = self.search(cursor, uid, search_params,
                                  context=context)
        res = self.read(cursor, uid, billing_ids,
                        [], context=context)
        if len(res) != num_periods:
            if type == 'month':
                type = 'day'
                res = self.get_billing(cursor, uid, meter,
                                       date, type, num_periods, contract,
                                       context=context)
            if type == 'day':
                date = self.get_previous_date(cursor, uid, meter, date, type,
                                              contract, context=context)
                if not date:
                    return []
                res = self.get_billing(cursor, uid, meter,
                                       date, type, num_periods, contract,
                                       context=context)
        return res

    def get_previous_date(self, cursor, uid, meter, date, type, contract,
                          context=None):
        search_params = [('date_end', '<', date),
                         ('name', '=', meter),
                         ('type', '=', type),
                         ('value', '=', 'a'),
                         ('valid', '=', True),
                         ('contract', '=', contract)]
        previous_id = self.search(cursor, uid, search_params,
                                  limit=1, order='date_end desc')
        if previous_id:
            previous = self.read(cursor, uid, previous_id,
                                 ['date_end'])[0]
            return previous.get('date_end', False)
        return False

    def get_last_billing_date(self, cursor, uid, meter, date_limit, contract,
                              context=None):
        '''return date of last billing
        @date_limit: date. Limit date to search for billing info
        returns: dict with day and month values'''
        if not context:
            context = {}
        res = {'day': False, 'month': False}
        day_search = [('name', '=', meter),
                      ('value', '=', 'a'),
                      ('valid', '=', True),
                      ('type', '=', 'day'),
                      ('date_end', '<=', date_limit),
                      ('contract', '=', contract)]
        billing_ids = self.search(cursor, uid, day_search,
                                  limit=1)
        if billing_ids:
            res['day'] = self.read(cursor, uid,
                                   billing_ids,
                                   ['date_end'])[0]['date_end']
        month_search = [('name', '=', meter),
                        ('value', '=', 'a'),
                        ('valid', '=', True),
                        ('type', '=', 'month'),
                        ('date_end', '<=', date_limit),
                        ('contract', '=', contract)]
        billing_ids = self.search(cursor, uid, month_search,
                                  limit=1)
        if billing_ids:
            res['month'] = self.read(cursor, uid,
                                     billing_ids,
                                     ['date_end'])[0]['date_end']
        return res

    def validate(self, cursor, uid, ids, action='validate', context=None):
        '''mark the read as valid or not
        @action: string with values validate or unvalidate'''

        for id in ids:
            if action == 'validate':
                now = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
                self.write(cursor, uid, [id],
                           {'valid': True,
                            'valid_date': now})
            elif action == 'unvalidate':
                self.write(cursor, uid, [id],
                           {'valid': False,
                            'valid_date': None})

    @staticmethod
    def fill_absolute_billings(total, billing, flow):
        billing['value'] = 'a'
        if flow == 'Import':
            billing['ai'] = total['ActiveEnergyAbs']
            billing['r1'] = total['ReactiveInductiveEnergyAbs']
            billing['r4'] = total['ReactiveCapacitiveEnergyAbs']
        elif flow == 'Export':
            billing['ae'] = total['ActiveEnergyAbs']
            billing['r2'] = total['ReactiveInductiveEnergyAbs']
            billing['r3'] = total['ReactiveCapacitiveEnergyAbs']
        return billing

    @staticmethod
    def fill_incremental_billings(total, billing, flow):
        billing['value'] = 'i'
        if flow == 'Import':
            billing['ai'] = total['ActiveEnergyInc']
            billing['r1'] = total['ReactiveInductiveEnergyInc']
            billing['r4'] = total['ReactiveCapacitiveEnergyInc']
        elif flow == 'Export':
            billing['ae'] = total['ActiveEnergyInc']
            billing['r2'] = total['ReactiveInductiveEnergyInc']
            billing['r3'] = total['ReactiveCapacitiveEnergyInc']
        return billing

    @staticmethod
    def build_billings(res, vals_mongo):
        res_billing = []
        for contract in res:
            for total in contract['Totals']:
                billing = vals_mongo.copy()
                billing['date_begin'] = total['PeriodStart']
                billing['date_end'] = total['PeriodEnd']
                billing['contract'] = contract['Contract']
                billing['period'] = total['Tariff']
                billing['excess'] = total['Excess']
                billing['quality_excess'] = total['QualityExcess']
                if contract['Flow'] == 'Import':
                    billing['max'] = total['MaximumDemand']
                    billing['date_max'] = total['MaximumDemandTimeStamp']
                    billing['quality_ai'] = total['QualityActiveEnergy']
                    billing['quality_max'] = total['QualityMaximumDemand']
                    billing['quality_r1'] = total[
                        'QualityReactiveInductiveEnergy']
                    billing['quality_r4'] = total[
                        'QualityReactiveCapacitiveEnergy']
                elif contract['Flow'] == 'Export':
                    billing['quality_ae'] = total['QualityActiveEnergy']
                    billing['quality_r2'] = total[
                        'QualityReactiveInductiveEnergy']
                    billing['quality_r3'] = total[
                        'QualityReactiveCapacitiveEnergy']
                billing_abs = billing.copy()
                billing_inc = billing.copy()
                res_billing.append(TmBilling.fill_absolute_billings(total,
                                                billing_abs, contract['Flow']))
                res_billing.append(TmBilling.fill_incremental_billings(total,
                                                billing_inc, contract['Flow']))
        return res_billing

    @staticmethod
    def create_remaining_periods(vals_mongo, base_billing):
        res_billings = []
        for period in range(4, 7):  # [4, 5, 6]
            billing = vals_mongo.copy()
            billing['contract'] = base_billing['contract']
            billing['type'] = base_billing['type']
            billing['date_begin'] = base_billing['date_begin']
            billing['date_end'] = base_billing['date_end']
            billing['period'] = period

            billing_abs = billing.copy()
            billing_inc = billing.copy()
            billing_abs['value'] = 'a'
            billing_inc['value'] = 'i'

            res_billings.append(billing_abs)
            res_billings.append(billing_inc)
        return res_billings

    def request_tm_billings(self, cursor, uid, registrador_id, date_from, date_to):
        registrador_obj = self.pool.get('giscedata.registrador')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        conf_obj = self.pool.get('res.config')
        logger = logging.getLogger('openerp.{0}.request_tm_billings'.format(
            __name__))
        registrador = registrador_obj.read(cursor, uid, registrador_id, ['name', 'tm_last_load_billing'])
        meter_ids = meter_obj.search(
            cursor, uid, [('registrador_id', '=', registrador_id)]
        )
        reew = False
        params = registrador_obj.get_reemote_connection_params(cursor, uid, registrador_id, date_from, date_to, 'b')
        if params:
            reew = registrador_obj.get_reemote_connection(cursor, uid, registrador_id, params)
        if not reew:
            logger.info(
                "Wrong connectivity configuration on {} registrator".format(
                    registrador['name']))
            return _('Wrong connectivity configuration on {} registrator'.format(
                registrador['name']))
        logger.info(
            "Requesting billings for {} registrator".format(registrador['name'])
        )

        try:
            response = reew.execute_request()
        except Exception as e:
            return _('There was an error connecting to the device. '
                     'ERROR: {}'.format(e.message))

        checked_res = check_response_header(response)
        retries_max = int(conf_obj.get(cursor, uid, 'tm_reemote_retries_max', 5))
        content = False

        if checked_res['type'] == 'API' and not checked_res['error']:
            content = get_content_from_api_response(checked_res, reew,
                                                    retries_max)
        elif checked_res['message'] and checked_res['type'] == 'FILE' and not \
                checked_res['error']:
            content = checked_res['message']
        elif checked_res['error']:
            return _('No correct billings were received from the registrator. {}'
                     ''.format(checked_res['error']))
        if not content or not content.get('Results', False):
            return _('No correct billings were received from the registrator.')
        if content.get('Results', False):
            results = content['Results']
            if results[0].get('SerialNumber') != registrador['name']:
                result = _("The billings obtained for this registrator do not "
                           "have the same serial number. \nOn the "
                           "TmBilling: {} \nOn our database: {}").format(
                           results[0].get('SerialNumber'), registrador['name'])
                self.create_case_tm_billings(cursor, uid, registrador_id, result)
                return result
            else:
                try:
                    last_date = self.create_requested_billings(
                        cursor, uid, results, registrador_id, date_to
                    )
                    if last_date and registrador['tm_last_load_billing'] < last_date:
                        registrador_obj.write(cursor, uid, registrador_id, {
                            'tm_last_load_billing': last_date})
                        if meter_ids:
                            meter_obj.write(cursor, uid, meter_ids[0],
                            {'tm_last_load_billing': last_date}
                        )
                    info = "{} billings for {} registrator have been created from {} to {}".format(
                        len(results), registrador['name'], date_from, last_date
                    )
                    logger.info(info)
                    return info
                except Exception as e:
                    return _('There was an error importing the billings. '
                             'ERROR: {}'.format(e.message))
        else:
            return _('ERROR: No information received from the meter')

    def create_requested_billings(self, cursor, uid, res, reg_id, date_to):
        tm_billing_obj = self.pool.get('tm.billing')

        vals_mongo = {
            'name': res[0].get('SerialNumber'),
            'contract': 0,
            'type': 'month',
            'value': '',
            'date_begin': '',
            'date_end': '',
            'period': 0,
            'max': 0,
            'date_max': '',
            'ai': 0,
            'ae': 0,
            'r1': 0,
            'r2': 0,
            'r3': 0,
            'r4': 0,
            'excess': 0,
            'quality_excess': 0,
            'quality_ai': 0,
            'quality_ae': 0,
            'quality_max': 0,
            'quality_r1': 0,
            'quality_r2': 0,
            'quality_r3': 0,
            'quality_r4': 0,
            'valid': False,
            'valid_date': '',
            'meter_id': reg_id,
        }
        billings = TmBilling.build_billings(res, vals_mongo)
        logger.info("Creating monthly billings for {} meter".format(
            res[0].get('SerialNumber')))
        last_timestamp = False
        for billing in billings:
            tm_billing_obj.create(cursor, uid, billing)
            if last_timestamp < str(billing['date_end']):
                last_timestamp = str(billing['date_end'])
        if len(billings) == 8:
            rem_billings = TmBilling.create_remaining_periods(
                vals_mongo, billings[0])
            for billing in rem_billings:
                tm_billing_obj.create(cursor, uid, billing)
        return last_timestamp

    def create_case_tm_billings(self, cursor, uid, meter_id, case_message,
                                context=None):
        """
        Create case with invalid measures
        """
        case_obj = self.pool.get('crm.case')
        registrador_obj = self.pool.get('giscedata.registrador')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': 'giscedata.polissa'})

        #Do not open another case if already one in open or pending state
        #This way we let automatic checks to be closed manually when done
        #and do not create many cases with the same incidences
        if meter_obj.check_case_open(cursor, uid, meter_id, section='TM',
                                     category='TMBI', context=context):
            return True
        meter = meter_obj.browse(cursor, uid, meter_id)
        polissa_id = False
        # meter_ids = meter_obj.search([('registrador_id', '=', registrador_id)])
        # Only sets case in contract if there is only one meter related to registrator
        # if meter_ids and len(meter_ids) == 1:
        #     meter = meter_obj.read(meter_ids[0], ['polissa'])
        #     polissa_id = meter['polissa']

        polissa_id = meter.polissa.id
        case_vals = {
            'description': case_message,
            'ref2': 'giscedata.lectures.comptador,%i' % meter.id,
            'name': (_('TM Billing import for %s') % meter.name),
            'polissa_id': polissa_id,
        }

        case_id = case_obj.create_case_generic(cursor, uid,
                                               [polissa_id],
                                               context=ctx,
                                               section='TM',
                                               category='TMBI',
                                               extra_vals=case_vals)
        case_obj.case_open(cursor, uid, case_id)
        return True

    _columns = {
        'name': fields.char('Meter', size=50, required=True),
        'contract': fields.integer('Contract'),
        'type': fields.selection(_type_billing, 'Type', size=20,),
        'value': fields.selection(_type_value, 'Value', size=3,),
        'date_begin': fields.datetime('From'),
        'date_end': fields.datetime('To'),
        'period': fields.integer('Period'),
        'max': fields.integer('Max (W)'),
        'date_max': fields.datetime('Max register date'),
        'ai': fields.integer('In'),
        'ae': fields.integer('Out'),
        'r1': fields.integer('R1'),
        'r2': fields.integer('R2'),
        'r3': fields.integer('R3'),
        'r4': fields.integer('R4'),
        'excess': fields.integer('Excess'),
        'quality_excess': fields.integer('Quality Excess'),
        'quality_ai': fields.integer('Quality Active Energy Import'),
        'quality_ae': fields.integer('Quality Active Energy Export'),
        'quality_max': fields.integer('Quality Maximum Demand'),
        'quality_r1': fields.integer('Quality Reactive Inductive Energy '
                                     'Import'),
        'quality_r2': fields.integer('Quality Reactive Inductive Energy '
                                     'Export'),
        'quality_r3': fields.integer('Quality Reactive Capacitive Energy '
                                     'Export'),
        'quality_r4': fields.integer('Quality Reactive Capacitive Energy '
                                     'Import'),
        'valid': fields.boolean('Valid'),
        'valid_date': fields.datetime('Valid date'),
        'meter_id': fields.integer('MeterID'),
    }

    _defaults = {
        'max': lambda *a: 0,
        'date_max': lambda *a: False,
        'valid': lambda *a: False,
        'valid_date': lambda *a: False,
        'meter_id': lambda *a: 0,
    }

TmBilling()


class TmProfile(osv_mongodb.osv_mongodb):

    _name = 'tm.profile'
    _order = 'timestamp desc'

    _vals_mongo = {
        'name': '',
        'timestamp': False,
        'season': '',
        'magn': 1000,
        'ai': 0,
        'ae': 0,
        'r1': 0,
        'r2': 0,
        'r3': 0,
        'r4': 0,
        'quality_ai': 0,
        'quality_ae': 0,
        'quality_r1': 0,
        'quality_r2': 0,
        'quality_r3': 0,
        'quality_r4': 0,
        'valid': False,
        'valid_date': False,
        'ai_fact': 0,
        'ae_fact': 0,
        'r1_fact': 0,
        'r2_fact': 0,
        'r3_fact': 0,
        'r4_fact': 0,
        'firm_fact': False,
        'cch_bruta': False,
        'cch_fact': False,
        'kind_fact': ''
    }

    _type_profile = [
        ('p', 'Hourly'),
        ('p4', 'Quarter-hour'),
    ]

    def validate(self, cursor, uid, ids, action='validate', context=None):
        '''mark the read as valid or not
        @action: string with values validate or unvalidate'''

        for id in ids:
            if action == 'validate':
                now = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
                self.write(cursor, uid, [id],
                           {'valid': True,
                            'valid_date': now})
            elif action == 'unvalidate':
                self.write(cursor, uid, [id],
                           {'valid': False,
                            'valid_date': None})

    _kind_selection = [
        (KIND_REAL, '1 - Real'),
        (KIND_REAL_PROFILED, '2 - Real profiled'),
        (KIND_ADJUSTED, '3 - Adjusted'),
        (KIND_AUTOREAD_PROFILED, '4 - Autoread profiled'),
        (KIND_HISTORY_PROFILED, '5 - History profiled'),
        (KIND_USE_FACTOR_PROFILED, '6 - Use factor profiled')
    ]

    def get_tariff_by_meter(self, cursor, uid, meter_name, init_date,
                            context=None):
        """ Get tariff_name by meter and date.
        Get policy on date by meter, and check state this policy on date
        """
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')

        # Get policy by date
        meter_id = meter_obj.get_meter_on_date(
            cursor, uid, meter_name, init_date
        )
        if not meter_id:
            return False
        polissa_id = meter_obj.read(
            cursor, uid, meter_id, ['polissa']
        )['polissa']

        # Get policy tariff by date
        try:
            tariff_name = polissa_obj.read(
                cursor, uid, polissa_id[0], ['tarifa'],
                context={'date': init_date}
            )['tarifa'][1]
        except IndexError:
            return False

        return tariff_name

    @staticmethod
    def get_number_of_hours(date_from, date_to):
        """
        Returns number of hours from two dates
        :param date_from: Start of curve: Y-m-d 01:00:00
        :param date_to: End of curve Y-m-d 00:00:00
        :return: n_hours int
        """

        start = TIMEZONE.localize(datetime.strptime(date_from,
                                                    '%Y-%m-%d %H:00:00'))
        end = TIMEZONE.localize(datetime.strptime(date_to,
                                                  '%Y-%m-%d %H:00:00'))
        profile = Profile(start, end, [])

        return profile.n_hours

    def fix_61A_curve(self, profiles):
        """
        Fix and validate 61A curves if it's complete
        """
        # First check num of hours
        init_date = min([x['timestamp'] for x in profiles])
        end_date = max([x['timestamp'] for x in profiles])
        number_of_hours = self.get_number_of_hours(init_date, end_date)

        if number_of_hours != len(profiles):
            raise osv.except_osv(_('Error'), _('Curva incompleta'))

        # Validate and mark cch_fact
        for profile in profiles:
            now = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
            profile['cch_fact'] = True
            profile['valid'] = True
            profile['valid_date'] = now
            for key in ('ai', 'ae', 'r1', 'r2', 'r3', 'r4'):
                if key in profile:
                    profile[key + '_fact'] = profile[key]
        return profiles

    @staticmethod
    def build_profiles(res, vals_mongo):
        res_profiles = []
        for record in res['Records']:
            profile = vals_mongo.copy()
            profile['timestamp'] = record['TimeInfo']
            profile['season'] = record['Season']
            for channel in record['Channels']:
                if channel['Magnitude'] == 'AI':
                    profile['ai'] = channel['Value']
                    profile['quality_ai'] = channel['Quality']
                elif channel['Magnitude'] == 'AE':
                    profile['ae'] = channel['Value']
                    profile['quality_ae'] = channel['Quality']
                elif channel['Magnitude'] == 'R1':
                    profile['r1'] = channel['Value']
                    profile['quality_r1'] = channel['Quality']
                elif channel['Magnitude'] == 'R2':
                    profile['r2'] = channel['Value']
                    profile['quality_r2'] = channel['Quality']
                elif channel['Magnitude'] == 'R3':
                    profile['r3'] = channel['Value']
                    profile['quality_r3'] = channel['Quality']
                elif channel['Magnitude'] == 'R4':
                    profile['r4'] = channel['Value']
                    profile['quality_r4'] = channel['Quality']
                else:
                    raise ValueError(
                        'Unexpected field found on profiles object: '
                        '{}'.format(channel['Magnitude']))
            res_profiles.append(profile)
        return res_profiles

    def request_tm_profiles(self, cursor, uid, registrador_id, date_from, date_to,
                            option):
        registrador_obj = self.pool.get('giscedata.registrador')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        conf_obj = self.pool.get('res.config')
        logger = logging.getLogger('openerp.{0}.request_tm_profiles'.format(
            __name__))
        registrador = registrador_obj.read(cursor, uid, registrador_id,
                                           ['name', 'tm_last_load_profile', 'tm_last_load_quarter_profile'])
        meter_ids = meter_obj.search(cursor, uid, [('registrador_id','=',registrador_id)])
        reew = False
        params = registrador_obj.get_reemote_connection_params(cursor, uid, registrador_id, date_from, date_to, option)
        if params:
            reew = registrador_obj.get_reemote_connection(cursor, uid, registrador_id, params)
        if not reew:
            logger.info(
                "Wrong connectivity configuration on {} registrator".format(
                    registrador['name']))
            return _('Wrong connectivity configuration on {} registrator'.format(
                registrador['name']))
        logger.info("Requesting {} profiles for {} registrator from {} to {}".format(
            option, registrador['name'], date_from, date_to)
        )
        try:
            response = reew.execute_request()
        except Exception as e:
            return _('There was an error connecting to the device. '
                     'ERROR: {}'.format(e.message))

        checked_res = check_response_header(response)
        retries_max = int(conf_obj.get(cursor, uid, 'tm_reemote_retries_max', 5))
        content = False

        if checked_res['type'] == 'API' and not checked_res['error']:
            content = get_content_from_api_response(checked_res, reew,
                                                    retries_max)
        elif checked_res['message'] and checked_res['type'] == 'FILE' and not \
                checked_res['error']:
            content = checked_res['message']
        elif checked_res['error']:
            return _('No correct profiles were received from the registrator. {}'
                     '').format(checked_res.get('error_message'))
        if not content or not content.get('SerialNumber', False):
            return _('No correct profiles were received from the registrator.')
        if content.get('SerialNumber') != registrador['name']:
            result = _("The profiles obtained for this registrator do not "
                       "have the same serial number. \nOn the "
                       "TmProfile: {} \nOn our database: {}").format(
                       content.get('SerialNumber'), registrador['name'])
            self.create_case_tm_profiles(cursor, uid, registrador_id, result)
            return result
        else:
            try:
                last_timestamp = self.create_requested_profiles(
                    cursor, uid, content, date_to, option)
                if option == 'p4':
                    date_field = 'tm_last_load_quarter_profile'
                else:
                    date_field = 'tm_last_load_profile'
                if last_timestamp and registrador[date_field] < last_timestamp:
                    registrador_obj.write(cursor, uid, registrador_id, {
                        date_field: last_timestamp}
                    )
                    if meter_ids:
                        meter_obj.write(
                            cursor, uid, meter_ids, {date_field: last_timestamp}
                        )
                info = _("{} profiles for {} registrator have been created between {} and {}").format(
                    len(content.get('Records', [])), registrador['name'], date_from ,last_timestamp)
                logger.info(info)
                return info
            except Exception as e:
                return _('There was an error importing the profiles. '
                         'ERROR: {}'.format(e.message))

    def create_requested_profiles(self, cursor, uid, res, date_to, option):
        tm_profile_obj = self.pool.get('tm.profile')
        logger = logging.getLogger('openerp.{0}.request_tm_profiles'.format(
            __name__))

        vals_mongo = {
            'name': res.get('SerialNumber'),
            'timestamp': False,
            'season': '',
            'magn': 1000,
            'ai': 0,
            'ae': 0,
            'r1': 0,
            'r2': 0,
            'r3': 0,
            'r4': 0,
            'quality_ai': 0,
            'quality_ae': 0,
            'quality_r1': 0,
            'quality_r2': 0,
            'quality_r3': 0,
            'quality_r4': 0,
            'valid': False,
            'valid_date': False,
            'ai_fact': 0,
            'ae_fact': 0,
            'r1_fact': 0,
            'r2_fact': 0,
            'r3_fact': 0,
            'r4_fact': 0,
            'firm_fact': False,
            'cch_bruta': True,
            'cch_fact': False,
            'type': option
        }
        profiles = TmProfile.build_profiles(res, vals_mongo)
        logger.info("Creating profiles for {} meter".format(
            res.get('SerialNumber')))
        last_timestamp = False
        for profile in profiles:
            tm_profile_obj.create(cursor, uid, profile)
            if last_timestamp < str(profile['timestamp']):
                last_timestamp = str(profile['timestamp'])
        return last_timestamp

    @staticmethod
    def build_profiles_from_tpl(reader, vals_mongo):
        res_profiles = []
        for row in reader:
            profile = vals_mongo.copy()
            date = datetime.strptime(row[0], '%Y%m%d%H%M')
            profile['timestamp'] = datetime.strftime(date, '%Y-%m-%d %H:%M:%S')
            profile['ai'] = row[1]
            profile['quality_ai'] = row[2]
            profile['ae'] = row[3]
            profile['quality_ae'] = row[4]
            profile['r1'] = row[5]
            profile['quality_r1'] = row[6]
            profile['r2'] = row[7]
            profile['quality_r2'] = row[8]
            profile['r3'] = row[9]
            profile['quality_r3'] = row[10]
            profile['r4'] = row[11]
            profile['quality_r4'] = row[12]
            res_profiles.append(profile)
        res_profiles = prepare_hour_change_profiles(res_profiles)
        return res_profiles

    @job(queue='low', timeout=1800)
    def async_load_tpl_curve(self, cursor, uid, file_content, unit, context=None):
        if context.get('file_name', False):
            reader = csv.reader(StringIO.StringIO(file_content), delimiter='\t')
            self.load_tpl_curve(cursor, uid, reader, unit, context=context)

    def load_tpl_curve(self, cursor, uid, reader, unit, context=None):
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tm_profile_obj = self.pool.get('tm.profile')

        logger = logging.getLogger('openerp.{0}.load_tpl_curve'.format(
            __name__))
        profiles = []
        result = {
            'res': profiles,
            'error': False,
            'message': _('Unhandled error')
        }
        row = '-'
        meter_name = '-'
        try:
            vals_mongo = TmProfile._vals_mongo.copy()
            vals_mongo['magn'] = unit
            vals_mongo['cch_bruta'] = True
            # Manage first line
            row = reader.next()
            context.update({'active_test': False})
            meter_id = meter_obj.search(cursor, uid, [('name', '=', row[0])],
                                        context=context)[0]
            meter_name = row[0]

            logger.info("Importing profiles to telemeasure for meter {}".format(
                meter_name))
            if meter_id:
                vals_mongo['name'] = meter_name
            else:
                result.update({
                    'error': True,
                    'message': _('Could not find the meter {} on the '
                                 'database').format(row[0])
                })
            # Manage second line
            row = reader.next()
            date_ini = datetime.strptime(row[1], '%Y%m%d%H%M')
            date_end = datetime.strptime(row[2], '%Y%m%d%H%M')
            date_ini_str = datetime.strftime(date_ini, '%Y-%m-%d %H:%M:%S')
            date_end_str = datetime.strftime(date_end, '%Y-%m-%d %H:%M:%S')
            date_read = datetime.strftime(date_end, '%Y-%m-%d')
            logger.info("Importing profiles from {} to {} ".format(date_ini,
                                                                   date_end))
            profiles = TmProfile.build_profiles_from_tpl(reader, vals_mongo)
            tariff = self.get_tariff_by_meter(
                cursor, uid, meter_name, date_read
            )
            if tariff == '6.1A':
                self.fix_61A_curve(profiles)
            if profiles:
                logger.info(
                    "Delete old existing profiles from {} to {} ".format(
                        date_ini_str, date_end_str))
                old_profiles = tm_profile_obj.search(cursor, uid, [
                                ('name', '=', meter_name),
                                ('timestamp', '>=', date_ini_str),
                                ('timestamp', '<=', date_end_str),
                                ('type', '=', 'p'),
                            ])
                if old_profiles:
                    tm_profile_obj.unlink(cursor, uid, old_profiles)
            for profile in profiles:
                tm_profile_obj.create(cursor, uid, profile)
            loaded_date = meter_obj.read(cursor, uid, meter_id,
                                         ['tm_last_load_profile'])
            if loaded_date['tm_last_load_profile'] < date_end_str:
                meter_obj.write(cursor, uid, meter_id, {
                    'tm_last_load_profile': date_end_str})
            result.update({
                'message': _('Just created {} profiles for meter: {}').format(
                    len(profiles), meter_name)})
            logger.info(result['message'])
        except Exception, e:
            result.update({
                'error': True,
                'message': e.message
            })
            logger.error(_("ERROR Importing curves to telemeasure for meter {}."
                           "\nRow: {}\nException: {}").format(meter_name, row,
                                                              e))
        if context.get('file_name', False):
            tm_register_obj = self.pool.get('tm.reader.register')
            register = {'name': context['file_name'],
                        'file_type': 'tpl_profile',
                        'state': 'read',
                        'zip_name': context.get('zip_name', '')}
            if result['error']:
                register['state'] = 'error'
            tm_register_obj.create(cursor, uid, register)
        return result

    @job(queue='low', timeout=1800)
    def async_load_p1d_curve(self, cursor, uid, file_content, unit,
                             context=None):
        if context.get('file_name', False):
            reader = csv.reader(StringIO.StringIO(file_content), delimiter=';')
            self.load_p1d_curve(cursor, uid, reader, unit, context=context)

    def build_profiles_from_p1d(self, cursor, uid, reader, vals_mongo,
                                context=None):
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        res_profiles = []
        meters_dates = []
        meters_serials = []
        cups = ''
        tariff = ''

        for row in reader:
            if cups != row[0]:
                cups = row[0]
                data_ini = datetime.strptime(row[2], '%Y/%m/%d %H:%M').strftime(
                    '%Y-%m-%d'
                )
                q = OOQuery(meter_obj, cursor, uid)
                sql = q.select([
                    'name',
                    Concat('data_alta', Literal(' 01:00')),
                    'polissa.tarifa.name'
                ], only_active=False, order_by=('data_alta.asc',)).where([
                    ('polissa.cups.name', '=', cups),
                    '|',
                    ('data_alta', '>=', data_ini),
                    '|',
                    ('data_baixa', '>=', data_ini),
                    ('data_baixa', '=', None)
                ])
                cursor.execute(*sql)
                if not cursor.rowcount:
                    raise Exception('Meters not found')
                meters_dates = []
                meters_serials = []
                for meter in cursor.fetchall():
                    meters_serials.append(meter[0])
                    meters_dates.append(meter[1])
                    tariff = meter[2]

            date_ = datetime.strptime(row[2], '%Y/%m/%d %H:%M')

            serial = meters_serials[
                bisect(meters_dates, date_.strftime('%Y-%m-%d %H:%M')) - 1
            ]
            vals_mongo['name'] = serial
            profile = vals_mongo.copy()

            date_ = datetime.strptime(row[2], '%Y/%m/%d %H:%M')

            profile['timestamp'] = datetime.strftime(date_, '%Y-%m-%d %H:%M:%S')
            profile['season'] = num_to_season(row[3])
            profile['ai'] = row[4]
            profile['quality_ai'] = row[5]
            profile['ae'] = row[6]
            profile['quality_ae'] = row[7]
            profile['r1'] = row[8]
            profile['quality_r1'] = row[9]
            profile['r2'] = row[10]
            profile['quality_r2'] = row[11]
            profile['r3'] = row[12]
            profile['quality_r3'] = row[13]
            profile['r4'] = row[14]
            profile['quality_r4'] = row[15]
            profile['kind_fact'] = row[20]
            res_profiles.append(profile)

        if tariff == '6.1A':
            self.fix_61A_curve(res_profiles)
        return res_profiles

    def load_p1d_curve(self, cursor, uid, reader, unit, context=None):

        tm_profile_obj = self.pool.get('tm.profile')

        logger = logging.getLogger('openerp.{0}.load_p1d_curve'.format(
            __name__))
        profiles = []
        result = {
            'res': profiles,
            'error': False,
            'message': _('Unhandled error')
        }
        row = '-'
        meter_name = '-'
        try:
            vals_mongo = TmProfile._vals_mongo.copy()
            vals_mongo['magn'] = unit
            vals_mongo['cch_bruta'] = True
            context.update({'active_test': False})

            logger.info("Reading P1D profiles file...")

            profiles = self.build_profiles_from_p1d(cursor, uid, reader,
                                                    vals_mongo, context=context)
            logger.info("Creating P1D profiles")
            for profile in profiles:
                tm_profile_obj.create(cursor, uid, profile)
            result.update({
                'message': _('Just created {} profiles').format(
                    len(profiles))})
            logger.info(result['message'])
        except Exception, e:
            result.update({
                'error': True,
                'message': e.message
            })
            logger.error(_("ERROR Importing curves to telemeasure for meter {}."
                           "\nRow: {}\nException: {}").format(meter_name, row,
                                                              e))
        if context.get('file_name', False):
            tm_register_obj = self.pool.get('tm.reader.register')
            register = {'name': context['file_name'],
                        'file_type': 'tpl_profile',
                        'state': 'read'}
            if result['error']:
                register['state'] = 'error'
            tm_register_obj.create(cursor, uid, register)
        return result

    def create_case_tm_profiles(self, cursor, uid, meter_id, case_message,
                                context=None):
        '''Create case with invalid measures'''
        case_obj = self.pool.get('crm.case')
        registrador_obj = self.pool.get('giscedata.registrador')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': 'giscedata.polissa'})

        #Do not open another case if already one in open or pending state
        #This way we let automatic checks to be closed manually when done
        #and do not create many cases with the same incidences
        if meter_obj.check_case_open(cursor, uid, meter_id, section='TM',
                                     category='TMPR', context=context):
            return True
        meter = meter_obj.browse(cursor, uid, meter_id)
        polissa_id = False
        #meter_ids = meter_obj.search([('registrador_id', '=', registrador_id)])
        # Only sets case in contract if there is only one meter related to registrator
        #if meter_ids and len(meter_ids) == 1:
        #    meter = meter_obj.read(meter_ids[0], ['polissa'])
        #    polissa_id = meter['polissa']
        polissa_id = meter.polissa.id

        case_vals = {
            'description': case_message,
            'ref2': 'giscedata.lectures.comptador,%i' % meter.id,
            'name': (_('TM Profile import for %s') % meter.name),
            'polissa_id': meter_id,
        }
        case_id = case_obj.create_case_generic(cursor, uid,
                                               [polissa_id],
                                               context=ctx,
                                               section='TM',
                                               category='TMPR',
                                               extra_vals=case_vals)
        case_obj.case_open(cursor, uid, case_id)
        return True

    def fix(self, cursor, uid, meter, start, end, tariff, balance, origin,
            inv_number):
        """Fix a complete profile using enerdata lib.

        :param cursor: Database cursor
        :param uid: User id
        :param meter: Meter name
        :param start: Start date (included)
        :param end:  End date (included)
        :param tariff: Tariff code
        :param balance: Balance in kW
        :param origin: The origin code in form of `CODE-SUBCODE`. Eg:: `20-00`
        :return: True
        """
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        balance = balance.copy()

        logger = logging.getLogger('openerp')
        meter_contract = 1
        meter_id = meter_obj.search(cursor, uid, [('name', '=', meter)],
                                    context={'active_test': False})
        if meter_id:
            meter_contract = meter_obj.read(cursor, uid, meter_id,
                                            ['tm_contract_import'])[0]
        # Delete all cch_fact True and cch_bruta False because they were
        # gaps and if you want to recalculate it maybe is a refacturation
        del_profiles_ids = self.search(cursor, uid, [
            ('name', '=', meter),
            ('timestamp', '>=', start),
            ('timestamp', '<=', end),
            ('cch_fact', '=', True),
            ('cch_bruta', '=', False),
            ('type', '=', 'p'),
        ])
        if del_profiles_ids:
            logger.info(
                'Already found {0} gaps filled. Deleting them...'.format(
                    len(del_profiles_ids)
                )
            )
            self.unlink(cursor, uid, del_profiles_ids)

        # First get all the profiles hours from start to end of meter
        profiles_ids = self.search(cursor, uid, [
            ('name', '=', meter),
            ('timestamp', '>=', start),
            ('timestamp', '<=', end),
            ('type', '=', 'p'),
        ])

        ph_measures = []
        for profile in self.read(cursor, uid, profiles_ids):
            ph_measures.append(convert_to_profilehour(profile))

        start = TIMEZONE.localize(datetime.strptime(start, '%Y-%m-%d %H:00:00'))
        end = TIMEZONE.localize(datetime.strptime(end, '%Y-%m-%d %H:00:00'))
        t_obj = self.pool.get('giscedata.polissa.tarifa')
        t_id = t_obj.search(cursor, uid, [('name', '=', tariff)])
        if not t_id:
            raise Exception('{0} is not a valid tariff'.format(tariff))
        t_id = t_id[0]
        tariff = t_obj.read(cursor, uid, t_id, ['name', 'cof'])
        tariff_name = tariff['name']
        if '3.' in tariff['name']:
            try:
                number_of_periods = get_tariff_by_code(tariff_name)().get_number_of_periods()
                fact_obj = self.pool.get('giscedata.facturacio.factura')
                lect_energy_obj = self.pool.get('giscedata.facturacio.lectures.energia')
                fact_id = fact_obj.search(cursor, uid, [('number', '=', inv_number)])[0]
                fact = fact_obj.read(cursor, uid, fact_id, ['lectures_energia_ids'])
                lect_data = lect_energy_obj.read(cursor, uid, fact['lectures_energia_ids'], ['tipus', 'name', 'magnitud'])
                if len(list(set([x['name'] for x in lect_data if
                                 x['tipus'] == 'activa' and x['magnitud'] == 'AE']))) < number_of_periods:
                    tariff_name = tariff_name.replace(' LB', '')
                    tariff_name += ' C2'
            except Exception:
                tariff_name = tariff['name']
        t = get_tariff_by_code(tariff_name)()

        # Change cof_a, cof_b, cof_c.. to A, B, C
        # TODO: dependency between giscedata_perfils and giscedata_telegestio
        t.cof = tariff['cof'].split('_')[1].upper()
        ph_measures_pass = ph_measures
        if origin.startswith('40'):
            ph_measures_pass = []
        profile = Profile(start, end, ph_measures_pass)
        fixed_profile = profile.fixit(t, balance, 1)
        logger.info('Profile from meter: {0} ({1} - {2}) have {3} gaps'.format(
            meter, start, end, len(profile.gaps)
        ))
        invalid = dict((m.date, m) for m in ph_measures
                       if not m.valid or origin.startswith('40'))
        adjusted_periods = fixed_profile.adjusted_periods
        for measure in fixed_profile.measures:
            vals = convert_to_mongodb(measure)
            vals['invoice_num'] = inv_number
            if isinstance(measure, ProfileHour):
                # Is a gap or a measure that is not valid
                logger.info('Meter {0} filling gap {1} with {2}'.format(
                    meter, measure.date, measure.measure
                ))
                if origin in ORIGIN_KIND_PROFILED:
                    kind = ORIGIN_KIND_PROFILED[origin]
                else:
                    logger.warning(
                        'Origin: {0} not in ORIGIN_KIND_PROFILED: {1}. '
                        'Setting KIND_REAL ({2}) as default'.format(
                            origin, ORIGIN_KIND_PROFILED.keys(), KIND_REAL
                        )
                    )
                    kind = KIND_REAL
                vals['kind_fact'] = kind
                vals['name'] = meter
                original = invalid.pop(measure.date, None)
                if original is None:
                    vals['cch_bruta'] = False
                    self.create(cursor, uid, vals)
                else:
                    # Is not a real gap, it was a invalid measure
                    vals['ae_fact'] = 0
                    vals['r1_fact'] = 0
                    vals['r2_fact'] = 0
                    vals['r3_fact'] = 0
                    vals['r4_fact'] = 0
                    self.write(cursor, uid, [original.meta['id']], vals)
            else:
                period = t.get_period_by_date(measure.date).code
                # Is adjusted if period is marked as adjusted
                if period in adjusted_periods:
                    logger.info('Meter {0} adjusting {1} with {2}'.format(
                        meter, measure.date, measure.measure
                    ))
                    vals['kind_fact'] = KIND_ADJUSTED
                else:
                    vals['kind_fact'] = KIND_REAL
                vals['valid_date'] = measure.meta['valid_date']
                self.write(cursor, uid, [measure.meta['id']], vals)

    def load_ziverq_file_curve(self, cursor, uid, reader, unit, context=None):
        if context is None:
            context = {}

        tm_profile_obj = self.pool.get('tm.profile')

        logger = logging.getLogger('openerp.{0}.load_ziverq_curve'.format(
            __name__)
        )
        profiles = []
        result = {
            'res': profiles,
            'error': False,
            'message': _('Unhandled error')
        }
        row = '-'
        meter_name = '-'
        try:
            vals_mongo = TmProfile._vals_mongo.copy()
            vals_mongo['magn'] = unit
            vals_mongo['cch_bruta'] = True
            context.update({'active_test': False})

            logger.info("Reading ZiverQ profiles file...")

            profiles = self.build_profiles_from_file(
                cursor, uid, reader, vals_mongo, context=context)

            # Correct ambigous hours on october's last sunday
            profiles = TmProfile.correct_ambiguity_hours(profiles)

            logger.info("Creating ZiverQ profiles")
            for profile in profiles:
                tm_profile_obj.create(cursor, uid, profile)
            result.update({
                'message': _('Just created {} profiles').format(
                    len(profiles))})
            logger.info(result['message'])
        except Exception as e:
            result.update({
                'error': True,
                'message': e.message
            })
            logger.error(_("ERROR Importing curves to telemeasure for meter {}."
                           "\nRow: {}\nException: {}").format(meter_name, row,
                                                              e))
        if context.get('file_name', False):
            tm_register_obj = self.pool.get('tm.reader.register')
            register = {'name': context['file_name'],
                        'file_type': 'tpl_profile',
                        'state': 'read'}
            if result['error']:
                register['state'] = 'error'
            tm_register_obj.create(cursor, uid, register)
        return result

    @staticmethod
    def correct_ambiguity_hours(res_profiles):
        # correct ambiguity (hour 02:00 on october's last sunday)
        # calculate october's last sunday (time change)
        _year = int(res_profiles[0]['timestamp'].split('-')[0])
        target_hour = '{}-{}-{} 02:'.format(_year, 10, str(last_sunday(_year, 10)))
        # Fix duplicated hours (02:**)
        conflictive_hours_indexs = []
        for idx, p in enumerate(res_profiles, start=0):
            if target_hour in p['timestamp']:
                conflictive_hours_indexs.append(idx)
        # get conflictive winter hours in dataframe
        winter_hours = conflictive_hours_indexs[len(conflictive_hours_indexs) // 2:]
        for i in range(0, winter_hours[0]):
            res_profiles[i]['season'] = 'S'
        for i in range(winter_hours[0], len(res_profiles)):
            res_profiles[i]['season'] = 'W'
        return res_profiles

    def build_profiles_from_file(self, cursor, uid, reader, vals_mongo,
                                 context=None):
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        res_profiles = []
        meters_dates = []
        meters_serials = []
        porta_enllac = ''
        tariff = ''

        # 0: penlace, 1: punto_medida, 2: equipo, 3: NaN, 4: dia, 5: hora
        for row in reader:
            if len(row) <= 1:
                # '\x1a' char
                continue
            punt_mesura = row[1]
            the_date = row[4] + row[5]
            if porta_enllac != row[0]:
                porta_enllac = row[0]
                data_ini = datetime.strptime(the_date, '%Y%m%d%H%M').strftime(
                    '%Y-%m-%d'
                )
                q = OOQuery(meter_obj, cursor, uid)
                sql = q.select([
                    'name',
                    'polissa.tarifa.name',
                    Concat('data_alta', Literal(' 01:00'))
                ], only_active=False, order_by=('data_alta.asc',)).where([
                    ('tm_link_address', '=', porta_enllac),
                    ('tm_measuring_point', '=', punt_mesura),
                    '|',
                    ('data_alta', '>=', data_ini),
                    '|',
                    ('data_baixa', '>=', data_ini),
                    ('data_baixa', '=', None)
                ])
                cursor.execute(*sql)
                if not cursor.rowcount:
                    raise Exception('Meters not found')
                meters_dates = []
                meters_serials = []
                for meter in cursor.fetchall():
                    meters_serials.append(meter[0])
                    tariff = meter[1]
                    meters_dates.append(meter[2])

            _date = datetime.strptime(the_date, '%Y%m%d%H%M')
            serial = meters_serials[
                bisect(meters_dates, _date.strftime('%Y-%m-%d %H:%M')) - 1
                ]
            vals_mongo['name'] = serial
            profile = vals_mongo.copy()

            profile['timestamp'] = datetime.strftime(_date, '%Y-%m-%d %H:%M:%S')
            profile['season'] = get_season(TIMEZONE.localize(_date))
            profile['ai'] = int(row[6])
            profile['quality_ai'] = int(row[7])
            profile['ae'] = int(row[8])
            profile['quality_ae'] = int(row[9])
            profile['r1'] = int(row[10])
            profile['quality_r1'] = int(row[11])
            profile['r2'] = int(row[12])
            profile['quality_r2'] = int(row[13])
            profile['r3'] = int(row[14])
            profile['quality_r3'] = int(row[15])
            profile['r4'] = int(row[16])
            profile['quality_r4'] = int(row[17])
            profile['kind_fact'] = KIND_REAL
            if tariff == '6.1A':
                profile['cch_fact'] = True
                profile['valid'] = True
            for key in ('ai', 'ae', 'r1', 'r2', 'r3', 'r4'):
                profile[key + '_fact'] = profile[key]
            res_profiles.append(profile)

        # Check number of month hours
        if tariff == '6.1A':
            self.fix_61A_curve(res_profiles)

        return res_profiles

    _columns = {
        'name': fields.char('Meter', size=50, required=True),
        'timestamp': fields.datetime('Timestamp'),
        'season': fields.selection([('W', 'Winter'),
                                    ('S', 'Summer')], 'Season', size=1),
        'magn': fields.selection([(1, 'Watts'),
                                  (1000, 'kWatts')], 'Magnitude'),
        'ai': fields.float('In'),
        'ae': fields.float('Out'),
        'r1': fields.float('R1'),
        'r2': fields.float('R2'),
        'r3': fields.float('R3'),
        'r4': fields.float('R4'),
        'quality_ai': fields.integer('Quality Active Energy Import'),
        'quality_ae': fields.integer('Quality Active Energy Export'),
        'quality_r1': fields.integer('Quality Reactive Inductive Energy '
                                     'Import'),
        'quality_r2': fields.integer('Quality Reactive Inductive Energy '
                                     'Export'),
        'quality_r3': fields.integer('Quality Reactive Capacitive Energy '
                                     'Export'),
        'quality_r4': fields.integer('Quality Reactive Capacitive Energy '
                                     'Import'),
        'valid': fields.boolean('Valid'),
        'valid_date': fields.datetime('Valid date'),
        'ai_fact': fields.float('In fact'),
        'ae_fact': fields.float('Out fact'),
        'r1_fact': fields.float('R1 fact'),
        'r2_fact': fields.float('R2 fact'),
        'r3_fact': fields.float('R3 fact'),
        'r4_fact': fields.float('R4 fact'),
        'firm_fact': fields.boolean('CCH Firm'),
        'cch_bruta': fields.boolean('CCH raw'),
        'cch_fact': fields.boolean('CCH invoicing'),
        'kind_fact': fields.selection(_kind_selection, 'CCH obtaining method'),
        'invoice_num': fields.char('Invoice Number', size=32, readonly=True,
                                   help="Unique number of the invoice."),
        'type': fields.selection(_type_profile, 'Type', size=20, ),
    }

    _defaults = {
        'valid': lambda *a: False,
        'valid_date': lambda *a: False,
        'cch_bruta': lambda *a: True,
        'cch_fact': lambda *a: False,
        'type': lambda *a: 'p',
    }

TmProfile()

_file_types = [('tpl_profile', 'File .curva'),
               ('p1d_profile', 'File P1D'),
               ]


class TmReaderRegister(osv.osv):
    """
        implements a register for each read file
    """
    _name = 'tm.reader.register'
    _order = 'create_date desc'

    _columns = {
        'name': fields.char('File name', size=100, readonly=True),
        'zip_name': fields.char('Zip name', size=100, readonly=True),
        'create_date': fields.datetime('Date read', readonly=True),
        'state': fields.selection([('read', 'Read'),
                                   ('error', 'Error'),
                                   ('partial_read', 'Partially read')],
                                  'State', readonly=True),
        'file_type': fields.selection(_file_types, 'File type', readonly=True),
        'notes': fields.text('Notes'),
    }

    _defaults = {
        'state': lambda *a: 'read',
    }

TmReaderRegister()


class TmRequest(osv.osv):
    """
    Defines request registers for telemeasure meters. Shows if the requests have
    succeeded or not, when were they sent and what data was requested.
    Launches requests to all telemeasure meters requesting the necessary data.
    """
    _name = 'tm.request'
    _order = 'request_date desc'

    def request_new_telemeasure_data(self, cursor, uid, from_date=None,
                                     to_date=None):
        register_obj = self.pool.get('giscedata.registrador')

        logger = logging.getLogger(
            'openerp.{0}.request_new_telemeasure_data'.format(__name__)
        )

        _registradors = register_obj.search(cursor, uid, [
            ('technology', '=', 'telemeasure')])
        reg_datas = register_obj.read(cursor, uid, _registradors, [
            'name',
            'tm_import_quarter_profiles',
            'tm_import_hour_profiles',
            'tm_import_mbilling',
            'tm_last_load_billing',
            'tm_last_load_profile',
            'tm_last_load_quarter_profile'
        ])
        dt = datetime.now()
        logger.info(
            _('Getting telemeasure data from {} registrators').format(
                len(reg_datas)
            )
        )

        for reg_data in reg_datas:
            register_id_name = reg_data['name']
            if not register_id_name:
                continue
            register_id = reg_data['id']
            # Read profiles
            if reg_data['tm_import_hour_profiles']:
                profiles_error = False
                if from_date:
                    df = datetime.strptime(from_date, '%Y-%m-%d %H:%M:%S')
                elif reg_data['tm_last_load_profile']:
                    df = datetime.strptime(
                        reg_data['tm_last_load_profile'], '%Y-%m-%d %H:%M:%S')
                else:
                    df = datetime.combine(dt, dtime.min).replace(day=1, hour=1)
                if to_date:
                    dt = datetime.strptime(to_date, '%Y-%m-%d %H:%M:%S')
                pr_df = df
                while (dt - pr_df).days >= 1:
                    aux_dt = pr_df + relativedelta(days=4)
                    str_dt = aux_dt.strftime('%Y-%m-%dT%H:%M:%S')
                    str_df = pr_df.strftime('%Y-%m-%dT%H:%M:%S')

                    tm_profile = TransactionExecute(cursor.dbname, uid,
                                                    'tm.profile')
                    tm_profile.request_tm_profiles(
                        register_id, str_df, str_dt, 'p'
                    )
                    new_df = register_obj.read(cursor, uid, register_id, [
                        'tm_last_load_profile'])['tm_last_load_profile']
                    if new_df:
                        new_df = datetime.strptime(new_df, '%Y-%m-%d %H:%M:%S')
                    else:
                        new_df = pr_df
                    if new_df > pr_df:
                        pr_df = new_df
                    else:
                        profiles_error = True
                        break
                if profiles_error:
                    state = 'error'
                else:
                    state = 'success'
                tm_req = TransactionExecute(cursor.dbname, uid, 'tm.request')
                tm_req.create({
                    'register_id': register_id,
                    'request_date': dt,
                    'origin': 'auto',
                    'date_from': pr_df,
                    'date_to': dt,
                    'state': state,
                    'type': 'hourly'
                })
            # Read quarter profiles
            if reg_data['tm_import_quarter_profiles']:
                profiles_error = False
                if from_date:
                    df = datetime.strptime(from_date, '%Y-%m-%d %H:%M:%S')
                elif reg_data['tm_last_load_quarter_profile']:
                    df = datetime.strptime(
                        reg_data['tm_last_load_quarter_profile'], '%Y-%m-%d %H:%M:%S')
                else:
                    df = datetime.combine(dt, dtime.min).replace(day=1, hour=1)
                if to_date:
                    dt = datetime.strptime(to_date, '%Y-%m-%d %H:%M:%S')
                pr_df = df
                while (dt - pr_df).days >= 1:
                    aux_dt = pr_df + relativedelta(days=4)
                    str_dt = aux_dt.strftime('%Y-%m-%dT%H:%M:%S')
                    str_df = pr_df.strftime('%Y-%m-%dT%H:%M:%S')

                    tmprofile = TransactionExecute(cursor.dbname, uid,
                                                   'tm.profile')
                    tmprofile.request_tm_profiles(
                        register_id, str_df, str_dt, 'p4'
                    )
                    new_df = register_obj.read(cursor, uid, register_id, [
                        'tm_last_load_quarter_profile'])['tm_last_load_quarter_profile']
                    if new_df:
                        new_df = datetime.strptime(new_df, '%Y-%m-%d %H:%M:%S')
                    else:
                        new_df = pr_df
                    if new_df > pr_df:
                        pr_df = new_df
                    else:
                        profiles_error = True
                        break
                if profiles_error:
                    state = 'error'
                else:
                    state = 'success'
                tm_req = TransactionExecute(cursor.dbname, uid, 'tm.request')
                tm_req.create({
                    'register_id': register_id,
                    'request_date': dt,
                    'origin': 'auto',
                    'date_from': pr_df,
                    'date_to': dt,
                    'state': state,
                    'type': 'quarter'
                })
            # Read billings
            if reg_data['tm_import_mbilling']:
                billings_error = False
                if not from_date and reg_data['tm_last_load_billing']:
                    df = datetime.strptime(
                        reg_data['tm_last_load_billing'], '%Y-%m-%d')
                elif not from_date:
                    df = datetime.combine(dt, dtime.min).replace(day=1) - \
                         relativedelta(days=1)
                if df.month < dt.month:
                    str_dt = dt.strftime('%Y-%m-%dT%H:%M:%S')
                    str_df = df.strftime('%Y-%m-%dT%H:%M:%S')

                    tmbilling = TransactionExecute(cursor.dbname, uid,
                                                   'tm.billing')
                    tmbilling.request_tm_billings(
                        register_id, str_df, str_dt
                    )
                    new_df = register_obj.read(cursor, uid, register_id, [
                        'tm_last_load_billing'])['tm_last_load_billing']
                    if new_df:
                        new_df = datetime.strptime(new_df, '%Y-%m-%d')
                    else:
                        new_df = df
                    if not new_df > df:
                        billings_error = True
                if billings_error:
                    state = 'error'
                else:
                    state = 'success'
                tm_req = TransactionExecute(cursor.dbname, uid, 'tm.request')
                tm_req.create({
                    'register_id': register_id,
                    'request_date': dt,
                    'origin': 'auto',
                    'date_from': df,
                    'date_to': dt,
                    'state': state,
                    'type': 'm_billing'
                })
            # Sync datetime
            if reg_data['tm_synchronize_date']:
                conf_obj = self.pool.get('res.config')
                delay = int(conf_obj.get(cursor, uid, 'tm_auto_sincro_seconds', 60))
                tm_reg = TransactionExecute(cursor.dbname, uid, 'giscedata.registrador')
                tm_reg.request_tm_sync_date(register_id, 'ts', delay)

    _columns = {
        'create_uid': fields.many2one('res.users', 'Creator', required=True,
                                      readonly=True),
        'create_date': fields.datetime('Create date', required=True,
                                       readonly=True),
        'register_id': fields.many2one('giscedata.registrador', 'Register id',
                                       required=True, readonly=True),
        'request_date': fields.datetime('Date of request', required=True,
                                        readonly=True),
        'origin': fields.selection([('wizard', 'TM wizard'),
                                    ('auto', 'Automatic task')],
                                   'Request origin', readonly=True),
        'date_from': fields.datetime('Data date from', readonly=True),
        'date_to': fields.datetime('Data date to', readonly=True),
        'state': fields.selection([('error', 'Request failed'),
                                   ('success', 'Request succeeded')],
                                  'State', required=True, readonly=True),
        'type': fields.selection([('hourly', 'Hourly profile'),
                                  ('quarter', 'Quarter hour profile'),
                                  ('m_billing', 'Monthly billing')],
                                 'Requested type', required=True, readonly=True)
    }


TmRequest()
