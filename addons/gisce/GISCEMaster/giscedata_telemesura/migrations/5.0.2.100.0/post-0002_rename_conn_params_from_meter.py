# -*- coding: utf-8 -*-
"""
Modificar nombre de columnas tras completarse la migración de parámetros de conexión tm
"""
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('migration')
    logger.info('Deleting columns from meter')
    alter_query = "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_ip_address TO old_tm_ip_address;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_port TO old_tm_port;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_phone_number TO old_tm_phone_number;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_link_address TO old_tm_link_address;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_measuring_point TO old_tm_measuring_point;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_password TO old_tm_password;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_contract_1 TO old_tm_contract_1;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_contract_2 TO old_tm_contract_2;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_contract_3 TO old_tm_contract_3;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_import_hour_profiles TO old_tm_hour_profiles;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_import_quarter_profiles TO old_tm_quarter_profiles;" \
                  "ALTER TABLE giscedata_lectures_comptador " \
                  "RENAME COLUMN tm_import_mbilling TO old_tm_mbilling;"
    cursor.execute(alter_query)

migrate = up
