# -*- coding: utf-8 -*-
import logging
from oopgrade import DataMigration

def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info("Migrating TM validation cron's")
    xml_content = '''<?xml version="1.0" ?>
    <openerp>
        <data noupdate="1">
            <record model="ir.cron" id="ir_cron_tm_validate_billing_action">
                <field name="model">tm.lectures.validate</field>
                <field name="function">validate_billing</field>
            </record>
            <record model="ir.cron" id="ir_cron_tm_validate_profile_action">
                <field name="model">tm.profiles.validate</field>
                <field name="function">validate_profile</field>
            </record>
        </data>
    </openerp>
    '''
    cursor.execute("SELECT id from ir_cron WHERE model = 'tm.profiles.validate' and function = 'validate_profile'")
    res = cursor.fetchall()
    if res:
        dm = DataMigration(
            xml_content, cursor, 'giscedata_telemesura',
            search_params={'ir_cron': ['model', 'function']}
        )
        dm.migrate()
    logger.info("TM validation cron's migrated succesfully")


def down(cursor, installed_version):
    pass

migrate = up