# -*- coding: utf-8 -*-
"""
Migrar parámetros de conexión tm de giscedata.lectures.comptador a giscedata.registrador
"""
import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    if not installed_version:
        return

    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    registrator_obj = pool.get('giscedata.registrador')
    meter_obj = pool.get('giscedata.lectures.comptador')
    mox_obj = pool.get('giscedata.moxa')
    # USE SQL
    meters = []
    cursor.execute("SELECT tm_link_address, tm_measuring_point, tm_password, "
                   "tm_contract_1, tm_contract_2, tm_contract_3, "
                   "tm_import_hour_profiles, tm_import_quarter_profiles, tm_import_mbilling, "
                   "tm_ip_address, tm_port, tm_phone_number, registrador_id, name, id, "
                   "tm_last_load_billing, tm_last_load_profile, tm_last_load_quarter_profile "
                   "FROM giscedata_lectures_comptador "
                   "WHERE technology_type = 'telemeasure'")
    rows = cursor.fetchall()

    for row in rows:
        meter = {
            'tm_link_address': row[0],
            'tm_measuring_point': row[1],
            'tm_password': row[2],
            'tm_contract_1': row[3],
            'tm_contract_2': row[4],
            'tm_contract_3': row[5],
            'tm_import_hour_profiles': row[6],
            'tm_import_quarter_profiles': row[7],
            'tm_import_mbilling': row[8],
            'tm_ip_address': row[9],
            'tm_port': row[10],
            'tm_phone_number': row[11],
            'registrador_id': row[12],
            'name': row[13],
            'id': row[14],
            'tm_last_load_billing': row[15],
            'tm_last_load_profile': row[16],
            'tm_last_load_quarter_profile': row[17]
        }
        meters.append(meter)

    moxa_count = 1
    for meter in meters:
        logger.info('Meter {}'.format(meter['name']))
        if not meter['registrador_id']:
            # Meter doesn't have a registrator assigned
            data = {
                'type': 'billing',
                'technology': 'telemeasure',
                'tm_link_address': meter['tm_link_address'],
                'tm_measuring_point': meter['tm_measuring_point'],
                'tm_password': meter['tm_password'],
                'tm_contract_1': meter['tm_contract_1'],
                'tm_contract_2': meter['tm_contract_2'],
                'tm_contract_3': meter['tm_contract_3'],
                'tm_import_hour_profiles': meter['tm_import_hour_profiles'],
                'tm_import_quarter_profiles': meter['tm_import_quarter_profiles'],
                'tm_import_mbilling': meter['tm_import_mbilling'],
                'tm_last_load_billing': meter['tm_last_load_billing'],
                'tm_last_load_profile': meter['tm_last_load_profile'],
                'tm_last_load_quarter_profile': meter['tm_last_load_quarter_profile'],
            }
            if meter['tm_ip_address'] and meter['tm_port'] and meter['tm_phone_number']:
                moxa = mox_obj.search(cursor, uid, [('ip_address', '=', meter['tm_ip_address']), ('port', '=',  meter['tm_port'])])
                if moxa:
                    # Moxa found, use existing id
                    id_moxa = moxa[0]
                else:
                    # Create moxa and get id
                    data_moxa = {
                        'name': 'Generic MOXA ' + str(moxa_count),
                        'ip_address': meter['tm_ip_address'],
                        'port': meter['tm_port']
                    }
                    id_moxa = mox_obj.create(cursor, uid, data_moxa)
                    moxa_count += 1
                # Add connection params to data
                data.update({
                    'tm_connection': 'moxa',
                    'tm_moxa': id_moxa,
                    'tm_phone_number': meter['tm_phone_number']
                })
            elif meter['tm_ip_address'] and meter['tm_port'] and not meter['tm_phone_number']:
                # Add connection params to data
                data.update({
                    'tm_connection': 'ip',
                    'tm_ip_address': meter['tm_ip_address'],
                    'tm_port': meter['tm_port']
                })
            elif meter['tm_phone_number'] and not meter['tm_ip_address'] and not meter['tm_port']:
                # Add connection params to data
                data.update({
                    'tm_connection': 'local',
                    'tm_phone_number': meter['tm_phone_mumber']
                })

            # Create registrator
            reg_id = registrator_obj.register_tm(cursor, uid, meter['name'])
            registrator_obj.write(cursor, uid, reg_id, data)
            # Add fkey to meter
            meter_obj.write(cursor, uid, meter['id'], {'registrador_id': reg_id})
        else:
            # Meter already has registrator
            reg_id = meter['registrador_id']
            data = {
                'type': 'billing',
                'technology': 'telemeasure',
                'tm_link_address': meter['tm_link_address'],
                'tm_measuring_point': meter['tm_measuring_point'],
                'tm_password': meter['tm_password'],
                'tm_contract_1': meter['tm_contract_1'],
                'tm_contract_2': meter['tm_contract_2'],
                'tm_contract_3': meter['tm_contract_3'],
                'tm_import_hour_profiles': meter['tm_import_hour_profiles'],
                'tm_import_quarter_profiles': meter['tm_import_quarter_profiles'],
                'tm_import_mbilling': meter['tm_import_mbilling'],
                'tm_last_load_billing': meter['tm_last_load_billing'],
                'tm_last_load_profile': meter['tm_last_load_profile'],
                'tm_last_load_quarter_profile': meter['tm_last_load_quarter_profile'],
            }
            if meter['tm_ip_address'] and meter['tm_port'] and meter['tm_phone_number']:
                moxa = mox_obj.search(cursor, uid,
                                      [('ip_address', '=', meter['tm_ip_address']), ('port', '=', meter['tm_port'])])
                if moxa:
                    # Moxa found, use existing id
                    id_moxa = moxa[0]
                else:
                    # Create moxa and get id
                    data_moxa = {
                        'name': 'Generic MOXA ' + str(moxa_count),
                        'ip_address': meter['tm_ip_address'],
                        'port': meter['tm_port']
                    }
                    id_moxa = mox_obj.create(cursor, uid, data_moxa)
                    moxa_count += 1
                # Add connection params to data
                data.update({
                    'tm_connection': 'moxa',
                    'tm_moxa': id_moxa,
                    'tm_phone_number': meter['tm_phone_number']
                })
            elif meter['tm_ip_address'] and meter['tm_port'] and not meter['tm_phone_number']:
                # Add connection params to data
                data.update({
                    'tm_connection': 'ip',
                    'tm_ip_address': meter['tm_ip_address'],
                    'tm_port': meter['tm_port']
                })
            elif meter['tm_phone_number'] and not meter['tm_ip_address'] and not meter['tm_port']:
                # Add connection params to data
                data.update({
                    'tm_connection': 'local',
                    'tm_phone_number': meter['tm_phone_mumber']
                })

            registrator_obj.write(cursor, uid, reg_id, data)

        logger.info('TM connection params migrated succesfully {}'.format(meter['name']))

def down(cursor, installed_version):
    pass

migrate = up
