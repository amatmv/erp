# coding=utf-8
from mongodb_backend.mongodb2 import mdbpool
import logging


def up(cursor, installed_version):
    if not installed_version:
        return
    logger = logging.getLogger('openerp.migration')
    tm_profile_mdb = mdbpool.get_collection('tm_profile')

    logger.info("Creating new field type for existing TM profiles")

    tm_profile_mdb.update({"type": {"$ne": "p4"}}, {"$set": {"type": "p"}}, multi=True)

    logger.info("Updated TM profiles with new fields type")


def down(cursor, installed_version):
    pass

migrate = up