from destral import testing
from destral.transaction import Transaction
import unittest
from addons import get_module_resource
import json
from expects import *


class BillingsTests(testing.OOTestCase):

    def setUp(self):
        one_contract_path = get_module_resource(
            'giscedata_telemesura', 'tests', 'data', 'example_one_contract.json'
        )
        self.one_contract = open(one_contract_path, 'r')
        two_contract_path = get_module_resource(
            'giscedata_telemesura', 'tests', 'data', 'example_two_contracts.json'
        )
        self.two_contract = open(two_contract_path, 'r')

    def test_built_of_mongo_dictionaries_with_one_contract_import(self):
        tm_billing_obj = self.openerp.pool.get('tm.billing')
        result = json.loads(self.one_contract.read())
        contracts = result['Results']
        mongo_dict = {
                'name': contracts[0].get('SerialNumber'),
                'contract': contracts[0].get('Contract'),
                'type': 'month',
                'value': '',
                'date_begin': '',
                'date_end': '',
                'period': 0,
                'max': 0,
                'date_max': '',
                'ai': 0,
                'ae': 0,
                'r1': 0,
                'r2': 0,
                'r3': 0,
                'r4': 0,
                'excess': 0,
                'quality_excess': 0,
                'quality_ai': 0,
                'quality_ae': 0,
                'quality_max': 0,
                'quality_r1': 0,
                'quality_r2': 0,
                'quality_r3': 0,
                'quality_r4': 0,
                'valid': False,
                'valid_date': '',
                'meter_id': 0,
            }
        expected_res = [{'quality_ai': 2, 'ae': 0, 'ai': 3635845, 'date_end': u'2017-11-01 00:00:00', 'period': 0, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 128, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-10-20 09:15:00', 'max': 410, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 287838, 'r1': 1732319, 'r2': 0, 'r3': 0, 'value': 'a', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2}, {'quality_ai': 2, 'ae': 0, 'ai': 27458, 'date_end': u'2017-11-01 00:00:00', 'period': 0, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 128, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-10-20 09:15:00', 'max': 410, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 13650, 'r1': 11428, 'r2': 0, 'r3': 0, 'value': 'i', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2}, {'quality_ai': 0, 'ae': 0, 'ai': 296359, 'date_end': u'2017-11-01 00:00:00', 'period': 1, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-11-01 00:00:00', 'max': 0, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 21757, 'r1': 75535, 'r2': 0, 'r3': 0, 'value': 'a', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0}, {'quality_ai': 0, 'ae': 0, 'ai': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 1, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-11-01 00:00:00', 'max': 0, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 0, 'r1': 0, 'r2': 0, 'r3': 0, 'value': 'i', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0}, {'quality_ai': 0, 'ae': 0, 'ai': 389701, 'date_end': u'2017-11-01 00:00:00', 'period': 2, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-11-01 00:00:00', 'max': 0, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 28465, 'r1': 139847, 'r2': 0, 'r3': 0, 'value': 'a', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0}, {'quality_ai': 0, 'ae': 0, 'ai': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 2, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-11-01 00:00:00', 'max': 0, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 0, 'r1': 0, 'r2': 0, 'r3': 0, 'value': 'i', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0}, {'quality_ai': 0, 'ae': 0, 'ai': 203379, 'date_end': u'2017-11-01 00:00:00', 'period': 3, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-11-01 00:00:00', 'max': 0, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 12241, 'r1': 78313, 'r2': 0, 'r3': 0, 'value': 'a', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0}, {'quality_ai': 0, 'ae': 0, 'ai': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 3, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-11-01 00:00:00', 'max': 0, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 0, 'r1': 0, 'r2': 0, 'r3': 0, 'value': 'i', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0}, {'quality_ai': 0, 'ae': 0, 'ai': 360390, 'date_end': u'2017-11-01 00:00:00', 'period': 4, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-11-01 00:00:00', 'max': 0, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 20348, 'r1': 144216, 'r2': 0, 'r3': 0, 'value': 'a', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0}, {'quality_ai': 0, 'ae': 0, 'ai': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 4, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-11-01 00:00:00', 'max': 0, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 0, 'r1': 0, 'r2': 0, 'r3': 0, 'value': 'i', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0}, {'quality_ai': 2, 'ae': 0, 'ai': 513365, 'date_end': u'2017-11-01 00:00:00', 'period': 5, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-10-20 09:15:00', 'max': 410, 'meter_id': 0, 'excess': 1265, 'name': u'83600023', 'r4': 40554, 'r1': 188702, 'r2': 0, 'r3': 0, 'value': 'a', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2}, {'quality_ai': 2, 'ae': 0, 'ai': 15298, 'date_end': u'2017-11-01 00:00:00', 'period': 5, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-10-20 09:15:00', 'max': 410, 'meter_id': 0, 'excess': 1265, 'name': u'83600023', 'r4': 5479, 'r1': 4490, 'r2': 0, 'r3': 0, 'value': 'i', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2}, {'quality_ai': 2, 'ae': 0, 'ai': 1872647, 'date_end': u'2017-11-01 00:00:00', 'period': 6, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-10-20 06:45:00', 'max': 408, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 164471, 'r1': 1105705, 'r2': 0, 'r3': 0, 'value': 'a', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2}, {'quality_ai': 2, 'ae': 0, 'ai': 12159, 'date_end': u'2017-11-01 00:00:00', 'period': 6, 'quality_ae': 0, 'contract': 1, 'valid_date': '', 'quality_excess': 0, 'date_begin': u'2017-10-01 00:00:00', 'valid': False, 'type': 'month', 'date_max': u'2017-10-20 06:45:00', 'max': 408, 'meter_id': 0, 'excess': 0, 'name': u'83600023', 'r4': 8170, 'r1': 6938, 'r2': 0, 'r3': 0, 'value': 'i', 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2}]
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            result = tm_billing_obj.build_billings(contracts, mongo_dict)
            expect(result).to(equal(expected_res))

    def test_built_of_mongo_dictionaries_with_two_contracts_import_and_export(
            self):
        tm_billing_obj = self.openerp.pool.get('tm.billing')
        result = json.loads(self.two_contract.read())
        contracts = result['Results']
        mongo_dict = {
            'name': contracts[0].get('SerialNumber'),
            'contract': contracts[0].get('Contract'),
            'type': 'month',
            'value': '',
            'date_begin': '',
            'date_end': '',
            'period': 0,
            'max': 0,
            'date_max': '',
            'ai': 0,
            'ae': 0,
            'r1': 0,
            'r2': 0,
            'r3': 0,
            'r4': 0,
            'excess': 0,
            'quality_excess': 0,
            'quality_ai': 0,
            'quality_ae': 0,
            'quality_max': 0,
            'quality_r1': 0,
            'quality_r2': 0,
            'quality_r3': 0,
            'quality_r4': 0,
            'valid': False,
            'valid_date': '',
            'meter_id': 0,
        }
        expected_res = [{'quality_ai': 2, 'ae': 0, 'ai': 3635845, 'max': 410, 'date_end': u'2017-11-01 00:00:00', 'period': 0, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 128, 'r1': 1732319, 'r4': 287838, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2, 'type': 'month', 'valid': False, 'date_max': u'2017-10-20 09:15:00'}, {'quality_ai': 2, 'ae': 0, 'ai': 27458, 'max': 410, 'date_end': u'2017-11-01 00:00:00', 'period': 0, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 128, 'r1': 11428, 'r4': 13650, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2, 'type': 'month', 'valid': False, 'date_max': u'2017-10-20 09:15:00'}, {'quality_ai': 0, 'ae': 0, 'ai': 296359, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 1, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 0, 'r1': 75535, 'r4': 21757, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': u'2017-11-01 00:00:00'}, {'quality_ai': 0, 'ae': 0, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 1, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 0, 'r1': 0, 'r4': 0, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': u'2017-11-01 00:00:00'}, {'quality_ai': 0, 'ae': 0, 'ai': 389701, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 2, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 0, 'r1': 139847, 'r4': 28465, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': u'2017-11-01 00:00:00'}, {'quality_ai': 0, 'ae': 0, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 2, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 0, 'r1': 0, 'r4': 0, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': u'2017-11-01 00:00:00'}, {'quality_ai': 0, 'ae': 0, 'ai': 203379, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 3, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 0, 'r1': 78313, 'r4': 12241, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': u'2017-11-01 00:00:00'}, {'quality_ai': 0, 'ae': 0, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 3, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 0, 'r1': 0, 'r4': 0, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': u'2017-11-01 00:00:00'}, {'quality_ai': 0, 'ae': 0, 'ai': 360390, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 4, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 0, 'r1': 144216, 'r4': 20348, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': u'2017-11-01 00:00:00'}, {'quality_ai': 0, 'ae': 0, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 4, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 0, 'r1': 0, 'r4': 0, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': u'2017-11-01 00:00:00'}, {'quality_ai': 2, 'ae': 0, 'ai': 513365, 'max': 410, 'date_end': u'2017-11-01 00:00:00', 'period': 5, 'quality_ae': 0, 'meter_id': 0, 'excess': 1265, 'valid_date': '', 'quality_excess': 0, 'r1': 188702, 'r4': 40554, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2, 'type': 'month', 'valid': False, 'date_max': u'2017-10-20 09:15:00'}, {'quality_ai': 2, 'ae': 0, 'ai': 15298, 'max': 410, 'date_end': u'2017-11-01 00:00:00', 'period': 5, 'quality_ae': 0, 'meter_id': 0, 'excess': 1265, 'valid_date': '', 'quality_excess': 0, 'r1': 4490, 'r4': 5479, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2, 'type': 'month', 'valid': False, 'date_max': u'2017-10-20 09:15:00'}, {'quality_ai': 2, 'ae': 0, 'ai': 1872647, 'max': 408, 'date_end': u'2017-11-01 00:00:00', 'period': 6, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 0, 'r1': 1105705, 'r4': 164471, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2, 'type': 'month', 'valid': False, 'date_max': u'2017-10-20 06:45:00'}, {'quality_ai': 2, 'ae': 0, 'ai': 12159, 'max': 408, 'date_end': u'2017-11-01 00:00:00', 'period': 6, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 0, 'r1': 6938, 'r4': 8170, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 0, 'r3': 0, 'contract': 1, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 2, 'quality_r4': 2, 'type': 'month', 'valid': False, 'date_max': u'2017-10-20 06:45:00'}, {'quality_ai': 0, 'ae': 300000, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 0, 'quality_ae': 2, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 128, 'r1': 0, 'r4': 0, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 120000, 'r3': 60000, 'contract': 2, 'quality_max': 0, 'quality_r2': 2, 'quality_r3': 2, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': ''}, {'quality_ai': 0, 'ae': 30000, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 0, 'quality_ae': 2, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 128, 'r1': 0, 'r4': 0, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 30000, 'r3': 15000, 'contract': 2, 'quality_max': 0, 'quality_r2': 2, 'quality_r3': 2, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': ''}, {'quality_ai': 0, 'ae': 100000, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 1, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 128, 'r1': 0, 'r4': 0, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 40000, 'r3': 20000, 'contract': 2, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': ''}, {'quality_ai': 0, 'ae': 10000, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 1, 'quality_ae': 0, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 128, 'r1': 0, 'r4': 0, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 10000, 'r3': 5000, 'contract': 2, 'quality_max': 0, 'quality_r2': 0, 'quality_r3': 0, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': ''}, {'quality_ai': 0, 'ae': 100000, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 2, 'quality_ae': 2, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 128, 'r1': 0, 'r4': 0, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 40000, 'r3': 20000, 'contract': 2, 'quality_max': 0, 'quality_r2': 2, 'quality_r3': 2, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': ''}, {'quality_ai': 0, 'ae': 10000, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 2, 'quality_ae': 2, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 128, 'r1': 0, 'r4': 0, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 10000, 'r3': 5000, 'contract': 2, 'quality_max': 0, 'quality_r2': 2, 'quality_r3': 2, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': ''}, {'quality_ai': 0, 'ae': 100000, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 3, 'quality_ae': 2, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 128, 'r1': 0, 'r4': 0, 'value': 'a', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 40000, 'r3': 20000, 'contract': 2, 'quality_max': 0, 'quality_r2': 2, 'quality_r3': 2, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': ''}, {'quality_ai': 0, 'ae': 10000, 'ai': 0, 'max': 0, 'date_end': u'2017-11-01 00:00:00', 'period': 3, 'quality_ae': 2, 'meter_id': 0, 'excess': 0, 'valid_date': '', 'quality_excess': 128, 'r1': 0, 'r4': 0, 'value': 'i', 'date_begin': u'2017-10-01 00:00:00', 'name': u'83600023', 'r2': 10000, 'r3': 5000, 'contract': 2, 'quality_max': 0, 'quality_r2': 2, 'quality_r3': 2, 'quality_r1': 0, 'quality_r4': 0, 'type': 'month', 'valid': False, 'date_max': ''}]
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            result = tm_billing_obj.build_billings(contracts, mongo_dict)
            expect(result).to(equal(expected_res))

