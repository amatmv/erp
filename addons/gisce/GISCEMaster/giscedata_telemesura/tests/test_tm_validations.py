from destral import testing
from destral.transaction import Transaction
from destral.patch import PatchNewCursors
import unittest
from addons import get_module_resource
import json
from expects import *
from mongodb_backend.mongodb2 import mdbpool
from giscedata_telemesura.lectures import update_invalid
from giscedata_polissa.tests.utils import activar_polissa
import os
import random
from datetime import datetime
from dateutil.relativedelta import relativedelta


class BillingsValidationsTests(testing.OOTestCase):

    def setUp(self):
        self.mongo_dict = {
            'name': '',
            'contract': '',
            'type': 'month',
            'value': '',
            'date_begin': '',
            'date_end': '',
            'period': 0,
            'max': 0,
            'date_max': '',
            'ai': 0,
            'ae': 0,
            'r1': 0,
            'r2': 0,
            'r3': 0,
            'r4': 0,
            'excess': 0,
            'quality_excess': 0,
            'quality_ai': 0,
            'quality_ae': 0,
            'quality_max': 0,
            'quality_r1': 0,
            'quality_r2': 0,
            'quality_r3': 0,
            'quality_r4': 0,
            'valid': True,
            'valid_date': '',
            'meter_id': 0,
        }
        billings_dups_path = get_module_resource(
            'giscedata_telemesura', 'tests', 'data',
            'create_duplicated_billings.json'
        )
        self.billings_dups = open(billings_dups_path, 'r')
        billings_negative_path = get_module_resource(
            'giscedata_telemesura', 'tests', 'data',
            'create_negative_reads.json'
        )
        self.billings_negative = open(billings_negative_path, 'r')
        billings_previous_path = get_module_resource(
            'giscedata_telemesura', 'tests', 'data',
            'create_previous_reads.json'
        )
        self.billings_previous = open(billings_previous_path, 'r')
        one_day_before_path = get_module_resource(
            'giscedata_telemesura', 'tests', 'data', 'example_day_before_entry_date.json'
        )
        self.one_day_before = open(one_day_before_path, 'r')

        self.oorq = os.environ.get('OORQ_ASYNC', 'True')
        os.environ['OORQ_ASYNC'] = 'False'

    def tearDown(self):
        os.environ['OORQ_ASYNC'] = self.oorq

    def fill_mongo_dict(self, billings):
        result = self.mongo_dict.copy()
        result['name'] = billings[0].get('SerialNumber')
        result['contract'] = billings[0].get('Contract')
        result['date_begin'] = billings[0].get('DateFrom')
        result['date_end'] = billings[0].get('DateTo')
        return result

    def test_all_invalid_duplicates_cleaning(self):
        tm_billing_obj = self.openerp.pool.get('tm.billing')
        tm_validate_obj = self.openerp.pool.get('tm.lectures.validate')
        result = json.loads(self.billings_dups.read())
        billings = result['Results']
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(billings)
            tm_billing_obj.unlink(cursor, uid, tm_billing_obj.search(cursor, uid, []))
            result = tm_billing_obj.build_billings(billings, dict_test)
            result.extend(result)
            ids = []
            for billing in result:
                created_id = tm_billing_obj.create(cursor, uid, billing)
                ids.append(created_id)
            dups = tm_validate_obj.get_duplicates(cursor, uid, '42553686')
            before_clean_billings = tm_billing_obj.search(cursor, uid, [
                ('name', '=', '42553686'), ('date_begin', '=', '2017-01-01 00:00:00')])
            tm_validate_obj.clean_duplicates(cursor, uid, '42553686')
            after_clean_billings = tm_billing_obj.search(cursor, uid, [
                ('name', '=', '42553686'), ('date_begin', '=', '2017-01-01 00:00:00')])
            expect(len(ids)).to(equal(len(before_clean_billings)))
            expect(len(dups)).to(equal(len(after_clean_billings)))
            dups_ids = [x['id'] for x in dups]
            remaining = [x for x in ids if x not in dups_ids]
            tm_billing_obj.unlink(cursor, uid, remaining)

    def test_valid_invalid_duplicates_cleaning(self):
        tm_billing_obj = self.openerp.pool.get('tm.billing')
        tm_validate_obj = self.openerp.pool.get('tm.lectures.validate')
        result = json.loads(self.billings_dups.read())
        billings = result['Results']
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(billings)
            result = tm_billing_obj.build_billings(billings, dict_test)
            dict_test['valid'] = False
            result_no_valid = tm_billing_obj.build_billings(billings, dict_test)
            ids = []
            result.extend(result_no_valid)
            for billing in result:
                created_id = tm_billing_obj.create(cursor, uid, billing)
                ids.append(created_id)
            dups = tm_validate_obj.get_duplicates(cursor, uid, '42553686')
            before_clean_billings = tm_billing_obj.search(cursor, uid, [
                ('name', '=', '42553686'), ('date_begin', '=', '2017-01-01 00:00:00')])
            tm_validate_obj.clean_duplicates(cursor, uid, '42553686')
            after_clean_billings = tm_billing_obj.search(cursor, uid, [
                ('name', '=', '42553686'), ('date_begin', '=', '2017-01-01 00:00:00')])
            dups_ids = [x['id'] for x in dups]
            remaining = [x for x in ids if x not in dups_ids]
            tm_billing_obj.unlink(cursor, uid, remaining)

            expect(len(ids)).to(equal(len(before_clean_billings)))
            expect(len(dups)).to(equal(len(after_clean_billings)))

    def test_check_negative_read(self):
        tm_billing_obj = self.openerp.pool.get('tm.billing')
        tm_validate_obj = self.openerp.pool.get('tm.lectures.validate')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        result = json.loads(self.billings_negative.read())
        billings = result['Results']
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(billings)
            dict_test['valid'] = False
            result = tm_billing_obj.build_billings(billings, dict_test)
            ids = []
            for billing in result:
                created_id = tm_billing_obj.create(cursor, uid, billing)
                ids.append(created_id)
            contract_obj.send_signal(cursor, uid, [4], ['validar', 'contracte'])
            res = tm_validate_obj.check_negative_read(cursor, uid, '42553686')
            expected_result = u'Negative read Date 01/01/2017, period 1, AI: -500, , R1: -50, , R4: -100, \n'
            tm_billing_obj.unlink(cursor, uid, ids)
            expect(res['badids'].values()[0]).to(equal(expected_result))

    def test_check_same_time_many_measures(self):
        tm_billing_obj = self.openerp.pool.get('tm.billing')
        tm_validate_obj = self.openerp.pool.get('tm.lectures.validate')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        result = json.loads(self.billings_dups.read())
        billings = result['Results']
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(billings)
            result = tm_billing_obj.build_billings(billings, dict_test)
            billings[0]['Totals'][0]['ActiveEnergyAbs'] = 111
            result_diff_ai = tm_billing_obj.build_billings(billings, dict_test)
            ids = []
            result.extend(result_diff_ai)
            for billing in result:
                created_id = tm_billing_obj.create(cursor, uid, billing)
                ids.append(created_id)
            contract_obj.send_signal(cursor, uid, [4], ['validar', 'contracte'])
            tm_validate_obj.clean_duplicates(cursor, uid, '42553686')
            res = tm_validate_obj.check_same_time_many_measures(
                cursor, uid, '42553686')['badids'].values()
            tm_billing_obj.unlink(cursor, uid, ids)

            result = res[0] if res[0] else res[1]
            expect(result).to(
                contain('2 different reads found on 01/01/2017 for period 0. AI:'))
            expect(result).to(contain('3635845'))
            expect(result).to(contain('111'))

    def test_impossible_read(self):
        tm_billing_obj = self.openerp.pool.get('tm.billing')
        tm_validate_obj = self.openerp.pool.get('tm.lectures.validate')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        result = json.loads(self.billings_dups.read())
        result_prev = json.loads(self.billings_previous.read())
        billings = result['Results']
        billings_prev = result_prev['Results']
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(billings)
            result = tm_billing_obj.build_billings(billings, dict_test)
            dict_test = self.fill_mongo_dict(billings_prev)
            result_prev = tm_billing_obj.build_billings(billings_prev,
                                                        dict_test)
            ids = []
            ids_prev = []
            for billing in result:
                created_id = tm_billing_obj.create(cursor, uid, billing)
                ids.append(created_id)
            for billing in result_prev:
                created_id = tm_billing_obj.create(cursor, uid, billing)
                ids_prev.append(created_id)
            contract = contract_obj.browse(cursor, uid, 4)
            tm_billings = tm_billing_obj.read(cursor, uid, ids, [])
            result_reads = []
            for tm_billing in tm_billings:
                res = tm_validate_obj.impossible_read(cursor, uid, tm_billing,
                                                      contract)
                result_reads.append(res)
            tm_billing_obj.unlink(cursor, uid, ids)
            tm_billing_obj.unlink(cursor, uid, ids_prev)
            errors = [x for x in result_reads if x]
            expect(len(errors)).to(equal(6))

    def test_validate_billings_before_data_alta(self):
        tm_billing_obj = self.openerp.pool.get('tm.billing')
        tm_validate_obj = self.openerp.pool.get('tm.lectures.validate')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        result = json.loads(self.one_day_before.read())
        billings = result['Results']
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with PatchNewCursors():
                dict_test = self.fill_mongo_dict(billings)
                dict_test['valid'] = False
                result = tm_billing_obj.build_billings(billings, dict_test)
                polissa_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0008'
                )[1]

                ids = []
                for billing in result:
                    created_id = tm_billing_obj.create(cursor, uid, billing)
                    ids.append(created_id)
                ctx = {'validate': True,
                       'lang': 'en_US'}
                contract_obj.write(cursor, uid, polissa_id, {'data_alta': '2016-01-01'})
                activar_polissa(self.openerp.pool, cursor, uid, polissa_id)
                tm_validate_obj.validate_individual(cursor, uid, [], '81178117',
                                                    context=ctx)
                valid_billings = tm_billing_obj.search(cursor, uid, [
                    ('name', '=', '81178117'),
                    ('valid', '=', 'true'),
                ])
                expect(len(valid_billings)).to(equal(8))
                tm_billing_obj.unlink(cursor, uid, ids)


class ProfilesValidationsTests(testing.OOTestCase):

    def setUp(self):
        self.mongo_dict = {
            'name': '',
            'timestamp': False,
            'season': '',
            'magn': 1000,
            'ai': 0,
            'ae': 0,
            'r1': 0,
            'r2': 0,
            'r3': 0,
            'r4': 0,
            'quality_ai': 0,
            'quality_ae': 0,
            'quality_r1': 0,
            'quality_r2': 0,
            'quality_r3': 0,
            'quality_r4': 0,
            'valid': False,
            'valid_date': False,
            'ai_fact': 0,
            'ae_fact': 0,
            'r1_fact': 0,
            'r2_fact': 0,
            'r3_fact': 0,
            'r4_fact': 0,
            'firm_fact': False,
            'cch_bruta': True,
            'cch_fact': False,
        }
        one_day_profiles_path = get_module_resource(
            'giscedata_telemesura', 'tests', 'data',
            'create_one_day_profiles.json'
        )
        self.one_day_profiles = open(one_day_profiles_path, 'r')
        low_pot_profiles_path = get_module_resource(
            'giscedata_telemesura', 'tests', 'data',
            'low_pot_one_day_profiles.json'
        )
        self.low_pot_profiles = open(low_pot_profiles_path, 'r')

    def fill_mongo_dict(self, data):
        result = self.mongo_dict.copy()
        result['name'] = data.get('SerialNumber')
        return result

    def create_profiles(self, cursor, uid, profiles):
        tm_profile_obj = self.openerp.pool.get('tm.profile')
        ids = []
        for profile in profiles:
            created_id = tm_profile_obj.create(cursor, uid, profile)
            ids.append(created_id)
        return ids

    def test_all_invalid_duplicates_cleaning(self):
        tm_profile_obj = self.openerp.pool.get('tm.profile')
        tm_validate_obj = self.openerp.pool.get('tm.profiles.validate')
        result = json.loads(self.one_day_profiles.read())
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(result)
            tm_profile_obj.unlink(cursor, uid,
                                  tm_profile_obj.search(cursor, uid, []))
            result = tm_profile_obj.build_profiles(result, dict_test)
            result.extend(result)
            ids = self.create_profiles(cursor, uid, result)
            dups = tm_validate_obj.get_duplicates(cursor, uid, '42553686')
            before_clean_profiles = tm_profile_obj.search(cursor, uid, [
                                    ('name', '=', '42553686'),
                                    ('timestamp', '>=', '2018-01-11 01:00:00'),
                                    ('timestamp', '<=', '2018-01-12 00:00:00')])
            tm_validate_obj.clean_duplicates(cursor, uid, '42553686')
            after_clean_profiles = tm_profile_obj.search(cursor, uid, [
                                    ('name', '=', '42553686'),
                                    ('timestamp', '>=', '2018-01-11 01:00:00'),
                                    ('timestamp', '<=', '2018-01-12 00:00:00')])
            expect(len(ids)).to(equal(len(before_clean_profiles)))
            expect(len(dups)).to(equal(len(after_clean_profiles)))
            dups_ids = [x['id'] for x in dups]
            remaining = [x for x in ids if x not in dups_ids]
            tm_profile_obj.unlink(cursor, uid, remaining)

    def test_valid_invalid_duplicates_cleaning(self):
        tm_profile_obj = self.openerp.pool.get('tm.profile')
        tm_validate_obj = self.openerp.pool.get('tm.profiles.validate')
        result_json = json.loads(self.one_day_profiles.read())
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(result_json)
            result = tm_profile_obj.build_profiles(result_json, dict_test)
            dict_test['valid'] = True
            result_no_valid = tm_profile_obj.build_profiles(result_json, dict_test)
            result.extend(result_no_valid)
            ids = self.create_profiles(cursor, uid, result)
            dups = tm_validate_obj.get_duplicates(cursor, uid, '42553686')
            before_clean_profiles = tm_profile_obj.search(cursor, uid, [
                                    ('name', '=', '42553686'),
                                    ('timestamp', '>=', '2018-01-11 01:00:00'),
                                    ('timestamp', '<=', '2018-01-12 00:00:00')])
            tm_validate_obj.clean_duplicates(cursor, uid, '42553686')
            after_clean_profiles = tm_profile_obj.search(cursor, uid, [
                                    ('name', '=', '42553686'),
                                    ('timestamp', '>=', '2018-01-11 01:00:00'),
                                    ('timestamp', '<=', '2018-01-12 00:00:00')])
            dups_ids = [x['id'] for x in dups]
            remaining = [x for x in ids if x not in dups_ids]
            tm_profile_obj.unlink(cursor, uid, remaining)

            expect(len(ids)).to(equal(len(before_clean_profiles)))
            expect(len(dups)).to(equal(len(after_clean_profiles)))

    def test_check_negative_read(self):
        tm_profile_obj = self.openerp.pool.get('tm.profile')
        tm_validate_obj = self.openerp.pool.get('tm.profiles.validate')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        result_json = json.loads(self.one_day_profiles.read())
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(result_json)
            dict_test['valid'] = False
            result_json['Records'][5]['Channels'][0]['Value'] = -86
            result_json['Records'][6]['Channels'][1]['Value'] = -1
            result_json['Records'][10]['Channels'][2]['Value'] = -49
            result_json['Records'][13]['Channels'][3]['Value'] = -1
            result_json['Records'][15]['Channels'][4]['Value'] = -1
            result_json['Records'][15]['Channels'][4]['Value'] = -1
            result_json['Records'][20]['Channels'][5]['Value'] = -34
            result = tm_profile_obj.build_profiles(result_json, dict_test)
            ids = self.create_profiles(cursor, uid, result)

            contract_obj.send_signal(cursor, uid, [4], ['validar', 'contracte'])
            res = tm_validate_obj.check_negative_read(cursor, uid, '42553686')
            expected_result = ['Meter has negative measure at 11/01/2018 06 ai: -86.0, ae: 0.0, r1: 12.0, r2: 0.0, r3: 0.0, r4: 17.0', 'Meter has negative measure at 11/01/2018 07 ai: 277.0, ae: -1.0, r1: 71.0, r2: 0.0, r3: 0.0, r4: 0.0', 'Meter has negative measure at 11/01/2018 11 ai: 49.0, ae: 0.0, r1: -49.0, r2: 0.0, r3: 0.0, r4: 13.0', 'Meter has negative measure at 11/01/2018 14 ai: 53.0, ae: 0.0, r1: 1.0, r2: -1.0, r3: 0.0, r4: 8.0', 'Meter has negative measure at 11/01/2018 16 ai: 52.0, ae: 0.0, r1: 0.0, r2: 0.0, r3: -1.0, r4: 7.0', 'Meter has negative measure at 11/01/2018 21 ai: 19.0, ae: 0.0, r1: 0.0, r2: 0.0, r3: 2.0, r4: -34.0']
            tm_profile_obj.unlink(cursor, uid, ids)

            check = all(value in res['badids'].values() for value in
                    expected_result)
            expect(check).to(be_true)

    def test_check_same_time_many_measures(self):
        tm_profile_obj = self.openerp.pool.get('tm.profile')
        tm_validate_obj = self.openerp.pool.get('tm.profiles.validate')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        result_json = json.loads(self.one_day_profiles.read())
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(result_json)
            result = tm_profile_obj.build_profiles(result_json, dict_test)
            result_json['Records'][0]['Channels'][0]['Value'] = 111
            result_diff_ai = tm_profile_obj.build_profiles(result_json,
                                                           dict_test)
            ids = []
            result.extend(result_diff_ai)
            for profile in result:
                created_id = tm_profile_obj.create(cursor, uid, profile)
                ids.append(created_id)
            contract_obj.send_signal(cursor, uid, [4], ['validar', 'contracte'])
            tm_validate_obj.clean_duplicates(cursor, uid, '42553686')
            res = tm_validate_obj.check_same_time_many_measures(
                cursor, uid, '42553686')['badids'].values()
            tm_profile_obj.unlink(cursor, uid, ids)

            result = res[0] if res[0] else res[1]
            expect(result).to(contain('2 different reads found on 11/01/2018 01. AI:'))
            expect(result).to(contain('111.0'))
            expect(result).to(contain('4.0'))

    def test_check_impossible_measure(self):
        tm_profile_obj = self.openerp.pool.get('tm.profile')
        tm_validate_obj = self.openerp.pool.get('tm.profiles.validate')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        result_json = json.loads(self.one_day_profiles.read())
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(result_json)
            result_json['Records'][0]['Channels'][0]['Value'] = 1000000000
            result = tm_profile_obj.build_profiles(result_json, dict_test)
            ids = self.create_profiles(cursor, uid, result)

            contract_obj.send_signal(cursor, uid, [4], ['validar', 'contracte'])
            res = tm_validate_obj.check_impossible_measure(cursor, uid,
                                                           '42553686')
            tm_profile_obj.unlink(cursor, uid, ids)

            expected_result = 'Impossible measure at 11/01/2018 01 ai: 1000000000.0, ae: 0.0,r1: 0.0, r2: 0.0, r3: 3.0, r4: 43.0'
            expect(res['badids'].values()[0]).to(equal(expected_result))

    def test_check_high_reactive(self):
        tm_profile_obj = self.openerp.pool.get('tm.profile')
        tm_profile_mdb = mdbpool.get_collection('tm_profile')
        tm_validate_obj = self.openerp.pool.get('tm.profiles.validate')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        result_json = json.loads(self.one_day_profiles.read())
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(result_json)
            result_json['Records'][0]['Channels'][2]['Value'] = 5
            result = tm_profile_obj.build_profiles(result_json, dict_test)
            ids = self.create_profiles(cursor, uid, result)

            contract_obj.send_signal(cursor, uid, [4], ['validar', 'contracte'])

            to_be_validated = tm_profile_mdb.find({
                'name': '42553686',
                'valid': False
            })
            invalids = {}
            for measure in to_be_validated:
                res = tm_validate_obj.single_check_high_reactive(cursor, uid,
                                                                 measure, '')
                if res:
                    invalids = update_invalid(invalids, res)

            tm_profile_obj.unlink(cursor, uid, ids)

            expected_result = ['High reactive at 11/01/2018 01 ai: 4000.0, r1: 5000.0']
            expect(invalids.values()).to(equal(expected_result))

    def test_check_control_bits(self):
        tm_profile_obj = self.openerp.pool.get('tm.profile')
        tm_validate_obj = self.openerp.pool.get('tm.profiles.validate')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        result_json = json.loads(self.one_day_profiles.read())
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(result_json)
            result_json['Records'][0]['Channels'][0]['Quality'] = 128
            result_json['Records'][2]['Channels'][2]['Quality'] = 2
            result = tm_profile_obj.build_profiles(result_json, dict_test)
            ids = self.create_profiles(cursor, uid, result)

            contract_obj.send_signal(cursor, uid, [4], ['validar', 'contracte'])

            res = tm_validate_obj.check_control_bits(cursor, uid, '42553686')
            tm_profile_obj.unlink(cursor, uid, ids)

            expected_result = ['Invalid measure 11/01/2018 01 CB: 128 (10000000)\nBit 7. Invalid measure', 'Possible invalid measure 11/01/2018 03 CB: 0 (00000010)\nBit 1. Incomplete period due to power failure']
            check = all(value in res['badids'].values() for value in
                        expected_result)
            expect(check).to(be_true)

    def test_check_more_than_pot(self):
        tm_profile_obj = self.openerp.pool.get('tm.profile')
        tm_profile_mdb = mdbpool.get_collection('tm_profile')
        tm_validate_obj = self.openerp.pool.get('tm.profiles.validate')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        result_json = json.loads(self.low_pot_profiles.read())
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dict_test = self.fill_mongo_dict(result_json)
            result_json['Records'][10]['Channels'][0]['Value'] = 7
            result = tm_profile_obj.build_profiles(result_json, dict_test)
            ids = self.create_profiles(cursor, uid, result)

            contract_obj.send_signal(cursor, uid, [4], ['validar', 'contracte'])
            contract = contract_obj.browse(cursor, uid, 4)
            to_be_validated = tm_profile_mdb.find({
                'name': '42553686',
                'valid': False
            })
            invalids = {}
            for measure in to_be_validated:
                res = tm_validate_obj.single_check_more_than_pot(cursor, uid,
                                                                 measure,
                                                                 contract)
                if res:
                    invalids = update_invalid(invalids, res)

            tm_profile_obj.unlink(cursor, uid, ids)

            expected_result = ['Impossible consumption at 11/01/2018 11 ai: 7000.0, Power: 3.0']
            expect(invalids.values()).to(equal(expected_result))

    def test_n_curve_hours(self):
        tm_profile_obj = self.openerp.pool.get('tm.profile')
        di = '2018-03-01 01:00:00'
        df = '2018-04-01 00:00:00'
        expected_hours_march_18 = 743
        expected_hours_october_18 = 745
        expected_hours_normal_month_30d = 720

        n_hours = tm_profile_obj.get_number_of_hours(di, df)
        self.assertEqual(n_hours,expected_hours_march_18)

        di = '2018-04-01 01:00:00'
        df = '2018-05-01 00:00:00'
        n_hours = tm_profile_obj.get_number_of_hours(di, df)
        self.assertEqual(n_hours, expected_hours_normal_month_30d)

        di = '2018-10-01 01:00:00'
        df = '2018-11-01 00:00:00'
        n_hours = tm_profile_obj.get_number_of_hours(di, df)
        self.assertEqual(n_hours, expected_hours_october_18)

    def test_fix_61A(self):
        tm_profile_obj = self.openerp.pool.get('tm.profile')

        di = '2018-04-01 01:00:00'
        df = '2018-05-01 00:00:00'
        n_hours = tm_profile_obj.get_number_of_hours(di, df)
        profiles = []
        s_n = '11111111'
        ts = '2018-04-01 01:00:00'
        for hour in range(n_hours):
            random_measure = random.random()
            ts = (datetime.strptime(ts, '%Y-%m-%d %H:%M:%S') +
            relativedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S')

            _vals_mongo = self.mongo_dict.copy()
            _vals_mongo.update({
                'name': s_n,
                'ai': random_measure,
                'ae': random_measure,
                'r1': random_measure,
                'r4': random_measure,
                'timestamp': ts,
            })
            profiles.append(_vals_mongo)

        tm_profile_obj.fix_61A_curve(profiles)
        for profile in profiles:
            self.assertTrue(profile['valid'])
            self.assertTrue(profile['cch_fact'])
            self.assertNotEqual(profile['ai_fact'], 0)
