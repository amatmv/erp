# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from tools import cache
from datetime import datetime
from pyreemote.telemeasure import ReemoteTCPIPWrapper, ReemoteMOXAWrapper, ReemoteModemWrapper
from giscedata_telemesures_base.giscedata_telemesures_base import TECHNOLOGY_TYPE_METER_SEL
from .utils import get_content_from_api_response, check_response_header

if 'telemeasure' not in dict(TECHNOLOGY_TYPE_METER_SEL).keys():
    TECHNOLOGY_TYPE_METER_SEL += [('telemeasure', 'Telemeasure')]

if 'electronic' not in dict(TECHNOLOGY_TYPE_METER_SEL).keys():
    TECHNOLOGY_TYPE_METER_SEL += [('electronic', 'Electronic')]

SYNC_TYPE_SEL = [
    ('no_sync', "Don't sync"),
    ('auto_sync', 'Automatic sync'),
    ('force_sync', 'Force sync')
]


def get_contracts_list(meter):
    contracts = []
    if meter.get('tm_contract_1', False):
        contracts.append(1)
    if meter.get('tm_contract_2', False):
        contracts.append(2)
    if meter.get('tm_contract_3', False):
        contracts.append(3)
    return contracts


def build_modem_params(meter, contracts, date_from, date_to, option):
    params = build_generic_parameters(meter, contracts, date_from, date_to, option)
    params['phone'] = meter['tm_phone_number']
    return params


def build_moxa_parameters(meter, contracts, date_from, date_to, option):
    params = build_tcpip_parameters(meter, contracts, date_from, date_to, option)
    params['phone'] = meter['tm_phone_number']
    params['ipaddr'] = meter['moxa_ip_address']
    params['port'] = meter['moxa_port']
    return params


def build_tcpip_parameters(meter, contracts, date_from, date_to, option):
    params = build_generic_parameters(meter, contracts, date_from, date_to, option)
    params['ipaddr'] = meter['tm_ip_address']
    params['port'] = meter['tm_port']
    return params


def build_generic_parameters(meter, contracts, date_from, date_to, option):
    params = {
        'link': meter['tm_link_address'],
        'mpoint': meter['tm_measuring_point'],
        'passwrd': meter['tm_password'],
        'datefrom': date_from,
        'dateto': date_to,
        'option': option,
        'request': '1',
        'contract': contracts,
    }
    return params


class GiscedataRegistradorTM(osv.osv):

    _name = 'giscedata.registrador'
    _inherit = 'giscedata.registrador'

    def register_tm(self, cursor, uid, tm_name, context=None):
        technology = 'telemeasure'
        rtype = 'unknown'
        exists = self.search(cursor, uid, [('name', '=', tm_name)])
        reg_data = {'name': tm_name, 'technology': technology}
        if not exists:
            reg_data.update({'type': rtype})
            return self.create(cursor, uid, reg_data)
        else:
            self.write(cursor, uid, exists[0], reg_data)
            return exists[0]

    def request_tm_sync_date(self, cursor, uid, reg_id, option, delay):
        reew = False
        conf_obj = self.pool.get('res.config')
        current_date = datetime.today().strftime('%Y-%m-%dT%H:%M:%S')
        params = self.get_reemote_connection_params(cursor, uid, reg_id, current_date, current_date, option)
        if params:
            params['delay'] = delay
            reew = self.get_reemote_connection(cursor, uid, reg_id, params)
        if not reew:
            registrador = self.read(cursor, uid, reg_id, ['name'])
            return _('Wrong connectivity configuration on {} registrator'.format(registrador['name']))
        try:
            response = reew.execute_request()
        except Exception as e:
            return _('There was an error connecting to the device. ERROR: {}'.format(e.message))
        checked_res = check_response_header(response)
        retries_max = int(conf_obj.get(cursor, uid, 'tm_reemote_retries_max', 5))
        content = False
        if checked_res['type'] == 'API' and not checked_res['error']:
            content = get_content_from_api_response(checked_res, reew, retries_max)
        elif checked_res['message'] and checked_res['type'] == 'FILE' and not checked_res['error']:
            content = checked_res['message']
        elif checked_res['error']:
            return _('No correct datetime were received from the registrator. {}'.format(checked_res['error']))
        if not content:
            return _('No correct datetime were received from the registrator.')
        else:
            datetime_meter = content.get('datetime_meter')
            current_datetime = content.get('current_datetime')
            diff_datetime = content.get('diff')
            updated = 'yes' if content.get('updated') else 'no'
            self.write(cursor, uid, reg_id, {'tm_last_sync_diff_date': diff_datetime})
            self.write(cursor, uid, reg_id, {'tm_last_sync_date': current_date})
            return _('Datetime from meter: {}\n'
                     'Current datetime: {}\n'
                     'Difference: {} seconds\n'
                     'Updated meter datetime?: {}'.format(datetime_meter, current_datetime, diff_datetime, updated))

    def get_reemote_connection_params(self, cursor, uid, reg_id, date_from, date_to, option, context=None):
        if not context:
            context = {}
        params = False
        registrador = self.read(cursor, uid, reg_id, ['tm_connection', 'tm_ip_address', 'tm_port', 'tm_moxa',
                                                      'tm_phone_number', 'tm_link_address',
                                                      'tm_measuring_point', 'tm_password',
                                                      'tm_contract_1', 'tm_contract_2', 'tm_contract_3'])
        if registrador:
            contracts = get_contracts_list(registrador)
            if registrador['tm_connection'] == 'moxa':
                moxa_obj = self.pool.get('giscedata.moxa')
                moxa = moxa_obj.read(cursor, uid, registrador['tm_moxa'][0])
                registrador['moxa_ip_address'] = moxa['ip_address']
                registrador['moxa_port'] = moxa['port']
                params = build_moxa_parameters(registrador, contracts, date_from, date_to, option)
            elif registrador['tm_connection'] == 'ip':
                params = build_tcpip_parameters(registrador, contracts, date_from, date_to, option)
            elif registrador['tm_connection'] == 'local':
                params = build_modem_params(registrador, contracts, date_from, date_to, option)
        return params

    def get_reemote_connection(self, cursor, uid, reg_id, params, context=None):
        if not context:
            context = {}
        reew = False
        registrador = self.read(cursor, uid, reg_id, ['tm_connection'])
        if registrador:
            if registrador['tm_connection'] == 'moxa':
                reew = ReemoteMOXAWrapper(**params)
            elif registrador['tm_connection'] == 'ip':
                reew = ReemoteTCPIPWrapper(**params)
            elif registrador['tm_connection'] == 'local':
                reew = ReemoteModemWrapper(**params)
        return reew

    _columns = {
        'tm_connection': fields.selection([('moxa', 'Moxa'), ('ip', 'IP'), ('local', 'Local')], 'Connection Type'),
        'tm_moxa': fields.many2one('giscedata.moxa', 'Moxa'),
        'tm_ip_address': fields.char('IP address', size=20, help=_('Field for '
                                                                   'the IP address of the meter. Filling this'
                                                                   ' field and the Phone number will '
                                                                   'establish a MOXA or IP connection '
                                                                   'depending on selected Connection Type')),
        'tm_port': fields.integer('Port'),
        'tm_phone_number': fields.char('Phone number', size=20, help=_('Field '
                                                                       'for the phone number of the modem. '
                                                                       'Filling this field and the IP address '
                                                                       'will may establish a MOXA or Local connection '
                                                                       'depending on selected Connection Type')),
        'tm_link_address': fields.integer('Link Address'),
        'tm_measuring_point': fields.integer('Measuring Point Address'),
        'tm_password': fields.integer('Password'),
        'tm_contract_1': fields.boolean('Contract 1'),
        'tm_contract_2': fields.boolean('Contract 2'),
        'tm_contract_3': fields.boolean('Contract 3'),
        'tm_import_hour_profiles': fields.boolean('Hourly profiles'),
        'tm_import_quarter_profiles': fields.boolean('Quarter profiles'),
        'tm_import_mbilling': fields.boolean('Monthly billings'),
        'tm_synchronize_date': fields.boolean('Synchronize date'),

        'tm_last_load_billing': fields.date('Last loaded billing'),
        'tm_last_load_profile': fields.datetime('Last loaded profile'),
        'tm_last_load_quarter_profile': fields.datetime('Last loaded quarter '
                                                        'profile'),
        'tm_last_sync_date': fields.datetime('Last sync date'),
        'tm_last_sync_diff_date': fields.float('Last sync difference (seconds)'),
    }

    _defaults = {
        'tm_contract_1': lambda *a: False,
        'tm_contract_2': lambda *a: False,
        'tm_contract_3': lambda *a: False,
        'tm_import_hour_profiles': lambda *a: True,
        'tm_import_quarter_profiles': lambda *a: False,
        'tm_import_mbilling': lambda *a: True,
    }

GiscedataRegistradorTM()
