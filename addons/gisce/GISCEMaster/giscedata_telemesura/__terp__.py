# -*- coding: utf-8 -*-
{
    "name": "Obtention of telemeasures from meters",
    "description": """
    Obtention of readings from the meters
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "mongodb_backend",
        "infraestructura",
        "giscedata_telemesures_base",
        "giscedata_moxa",
        "giscedata_lectures_tecnologia",
        "giscedata_polissa_crm",
    ],
    "init_xml": [],
    "demo_xml":[
        "telemesura_telegestio_demo_billing.xml"
    ],
    "update_xml":[
        "telemesura_view.xml",
        "wizard/wizard_validate_telemeasures_view.xml",
        "wizard/wizard_show_read_tm_view.xml",
        "wizard/wizard_export_tm_readings_view.xml",
        "wizard/wizard_load_tm_profiles_view.xml",
        "wizard/wizard_load_tm_billings_view.xml",
        "wizard/wizard_load_tpl_profiles_view.xml",
        "wizard/wizard_sync_date_view.xml",
        "wizard/wizard_validate_billing_view.xml",
        "security/ir.model.access.csv",
        "lectures_view.xml",
        "telemesura_data.xml",
        "giscedata_telemesures_base_view.xml",
    ],
    "active": False,
    "installable": True
}
