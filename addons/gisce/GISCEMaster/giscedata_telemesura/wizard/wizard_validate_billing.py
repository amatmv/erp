# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardValidateLecturesTM(osv.osv_memory):

    _name = 'wizard.validate.tm.billing'

    def action_validate(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        tmmodel = context.get('tmmodel', 'tm.billing')

        wizard = self.browse(cursor, uid, ids[0])

        model_obj = self.pool.get(tmmodel)

        model_ids = context.get('active_ids', [])
        model_obj.validate(cursor, uid, model_ids,
                           action=wizard.action,
                           context=context)
        return {}

    _columns = {
        'action': fields.selection([('validate', 'Mark as valid'),
                                    ('unvalidate', 'Mark as invalid')],
                                   'Action', required=True),
    }

WizardValidateLecturesTM()
