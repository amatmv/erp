# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardValidateTelemeasures(osv.osv_memory):

    _name = 'wizard.validate.telemeasures'

    def action_validate(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context=context)

        if wizard.type == 'profile':
            tm_validate_obj = self.pool.get('tm.profiles.validate')
            meter_names = []
            if wizard.meter_id:
                meter_name = wizard.meter_id.name
                meter_names = [meter_name]
            tm_validate_obj.validate_profile(cursor, uid, [], meter_names,
                                             context=context)

        elif wizard.type == 'billing':
            tm_validate_obj = self.pool.get('tm.lectures.validate')
            meter_names = []
            if wizard.meter_id:
                meter_name = wizard.meter_id.name
                meter_names = [meter_name]
                context.update({'tg_check_unreaded': False})
            tm_validate_obj.validate_billing(cursor, uid, [], meter_names,
                                             context=context)

        return {}

    def _default_meter_id(self, cursor, uid, context=None):
        if context is None:
            context = {}

        if context.get('from_meter', False):
            return context.get('active_id', False)
        else:
            return False

    _columns = {
        'type': fields.selection([('profile', 'TM Profiles'),
                                  ('billing', 'TM Billing')], 'Type',
                                 required=True),
        'meter_id': fields.many2one('giscedata.lectures.comptador', 'Meter',
                                    help="Blank for all meters"),
    }

    _defaults = {
        'meter_id': _default_meter_id,
    }

WizardValidateTelemeasures()
