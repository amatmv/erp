# -*- coding: utf-8 -*-
from osv import osv, fields
from dateutil.relativedelta import relativedelta
from tools.translate import _
from giscedata_telemesura.utils import *


class TmBillingsLoader(osv.osv_memory):

    _name = 'wizard.tm.billings.load'

    def action_tm_billings_load(self, cursor, uid, ids, context=None):

        tm_billing_obj = self.pool.get('tm.billing')

        wizard = self.browse(cursor, uid, ids[0], context=context)
        registrator_id = wizard.registrator_id['id']

        wizard.write(
            {'info': _('Working on the request'),
             'state': 'request'}, context=context)
        dt_date_from = datetime.strptime(wizard.tm_date_from, '%Y-%m-%d')
        datetime_from = (dt_date_from + relativedelta(hours=1)).strftime(
            '%Y-%m-%dT%H:%M:%S')
        datetime_to = (dt_date_from + relativedelta(months=1)).strftime(
            '%Y-%m-%dT%H:%M:%S')

        res = tm_billing_obj.request_tm_billings(cursor, uid, registrator_id,
                                                 datetime_from, datetime_to)

        wizard.write(
            {'info': _(res),
             'state': 'end'}, context=context)

    _columns = {
        'registrator_id': fields.many2one('giscedata.registrador',
                                    'Registrator', required=True),
        'tm_date_from': fields.date('Date from', required=True),
        'tm_date_to': fields.date('Date to'),
        'info': fields.text('Information', readonly=True),
        'state': fields.selection(WIZARD_STATES, 'Working state')
    }

    _defaults = {
        'registrator_id': lambda cursor, uid, ids, context: context.get(
            'active_ids')[0],
        'info': lambda *a: _('Wizard to load telemeasure billings.'),
        'tm_date_from': lambda self, cr, uid, context: self.pool.get(
            'giscedata.registrador').read(cr, uid, context.get(
            'active_ids')[0], ['tm_last_load_billing'], context)[
            'tm_last_load_billing'],
        'state': lambda *a: 'standby',
    }

TmBillingsLoader()
