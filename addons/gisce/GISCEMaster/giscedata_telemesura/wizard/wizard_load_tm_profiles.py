# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from tools.translate import _
from dateutil.relativedelta import relativedelta
from giscedata_telemesura.utils import WIZARD_STATES


class TmProfilesLoader(osv.osv_memory):

    _name = 'wizard.tm.profiles.load'

    def action_tm_profiles_load(self, cursor, uid, ids, context=None):

        tm_profile_obj = self.pool.get('tm.profile')
        res = ''
        wizard = self.browse(cursor, uid, ids[0], context=context)

        wizard.write(
            {'info': _('Working on the request'),
             'state': 'request'}, context=context)

        date_from = datetime.strptime(wizard.tm_date_from, '%Y-%m-%d %H:%M:%S'
                                      ).strftime('%Y-%m-%dT%H:%M:%S')
        date_to = datetime.strptime(wizard.tm_date_to, '%Y-%m-%d %H:%M:%S'
                                    ).strftime('%Y-%m-%dT%H:%M:%S')

        if wizard.hour_profiles:
            res = tm_profile_obj.request_tm_profiles(cursor, uid,
                                                     wizard.registrator_id['id'],
                                                     date_from, date_to, 'p')
        if wizard.quarter_profiles:
            res_ = tm_profile_obj.request_tm_profiles(cursor, uid,
                                                      wizard.registrator_id['id'],
                                                      date_from, date_to, 'p4')
            res = '\n'.join([res, res_])

        wizard.write(
            {'info': _(res),
             'state': 'end'}, context=context)

    _columns = {
        'registrator_id': fields.many2one('giscedata.registrador',
                                    'Registrator', required=True),
        'tm_date_from': fields.datetime('Date from', required=True),
        'tm_date_to': fields.datetime('Date to', required=True),
        'info': fields.text('Information', readonly=True),
        'state': fields.selection(WIZARD_STATES, 'Working state'),
        'hour_profiles': fields.boolean('Read hourly profiles'),
        'quarter_profiles': fields.boolean('Read quarter hour profiles'),
    }

    _defaults = {
        'registrator_id': lambda cursor, uid, ids, context: context.get(
            'active_ids')[0],
        'info': lambda *a: _('Wizard to load telemeasure profiles.'),
        'tm_date_from': lambda self, cr, uid, context: self.pool.get(
            'giscedata.registrador').read(cr, uid, context.get(
            'active_ids')[0], ['tm_last_load_profile'], context)[
            'tm_last_load_profile'],
        'state': lambda *a: 'standby',
        'hour_profiles': lambda *a: True,
        'quarter_profiles': lambda *a: False,
    }

TmProfilesLoader()
