# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
import csv
from giscedata_telemesura.utils import get_file_content
from oorq.decorators import create_jobs_group
import cStringIO as StringIO


class Tpl2TmProfilesLoader(osv.osv_memory):

    _name = 'wizard.tpl.profiles.load'

    def action_tpl_to_tm_profiles_load(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0], context=context)

        info = _('Something went wrong')
        if wizard.file_type == 'tpl':
            info = self.import_tpl_type_file(cursor, uid, wizard,
                                             context=context)
        elif wizard.file_type == 'p1d':
            info = self.import_p1d_type_file(cursor, uid, wizard,
                                             context=context)
        elif wizard.file_type == 'ziverq':
            info = self.import_file_type_file(cursor, uid, wizard)

        wizard.write(
            {'info': info}, context=context)

    def import_tpl_type_file(self, cursor, uid,  wizard, context=None):
        tm_profile_obj = self.pool.get('tm.profile')
        files_zipped, file_content = get_file_content(wizard)

        if file_content['file_type'] == 'raw':
            reader = csv.reader(StringIO.StringIO(file_content['file']),
                                delimiter='\t')
            result = tm_profile_obj.load_tpl_curve(cursor, uid, reader,
                                                   wizard.unit, context=context)
            info = result['message']
            tm_register_obj = self.pool.get('tm.reader.register')
            register = {'name': file_content['name'],
                        'file_type': 'tpl_profile',
                        'state': 'read'}
            if result['error']:
                register['state'] = 'error'
            tm_register_obj.create(cursor, uid, register)

        elif file_content['file_type'] == 'zip':
            jobs_ids = []
            for filename in files_zipped:
                context.update({'file_name': filename})
                context.update({'zip_name': file_content['name']})
                j = tm_profile_obj.async_load_tpl_curve(cursor, uid,
                                                        file_content[filename],
                                                        wizard.unit,
                                                        context=context)
                jobs_ids.append(j.id)
            if len(jobs_ids) > 1:
                create_jobs_group(cursor.dbname, uid, _('Load TPL profiles'),
                                  'tm.profile', jobs_ids
                                  )
            info = _('The profiles are being imported on a background process')
        else:
            info = _('This file is not supported. The supported types of '
                     'file are:\n - A raw .curva file\n - A compressed '
                     'zip file of raw .curva files')
        return info

    def import_p1d_type_file(self, cursor, uid, wizard, context=None):
        tm_profile_obj = self.pool.get('tm.profile')
        files_zipped, file_content = get_file_content(wizard)

        if file_content['file_type'] == 'raw':
            reader = csv.reader(StringIO.StringIO(file_content['file']),
                                delimiter=';')
            result = tm_profile_obj.load_p1d_curve(cursor, uid, reader,
                                                   wizard.unit, context=context)
            info = result['message']
            tm_register_obj = self.pool.get('tm.reader.register')
            register = {'name': file_content['name'],
                        'file_type': 'p1d_profile',
                        'state': 'read'}
            if result['error']:
                register['state'] = 'error'
            tm_register_obj.create(cursor, uid, register)
        elif file_content['file_type'] == 'zip':
            jobs_ids = []
            for filename in files_zipped:
                context.update({'file_name': filename})
                j = tm_profile_obj.async_load_p1d_curve(cursor, uid,
                                                        file_content[filename],
                                                        wizard.unit,
                                                        context=context)
                jobs_ids.append(j.id)
            if len(jobs_ids) > 1:
                create_jobs_group(cursor.dbname, uid, _('Load TPL profiles'),
                                  'tm.profile', jobs_ids
                                  )
            info = _('The profiles are being imported on a background process')
        else:
            info = _('This file is not supported. The supported types of '
                     'file are:\n - A raw .curva file\n - A compressed '
                     'zip file of raw .curva files')
        return info

    def import_file_type_file(self, cursor, uid, wizard, context=None):
        tm_profile_obj = self.pool.get('tm.profile')
        tm_register_obj = self.pool.get('tm.reader.register')
        files_zipped, file_content = get_file_content(wizard)

        if file_content['file_type'] == 'raw':
            reader = csv.reader(StringIO.StringIO(file_content['file']),
                                delimiter='\t')
            result = tm_profile_obj.load_ziverq_file_curve(cursor, uid, reader,
                                                   wizard.unit, context=context)
            info = result['message']
            register = {'name': file_content['name'],
                        'file_type': 'p1d_profile',
                        'state': 'read'}
            if result['error']:
                register['state'] = 'error'
            tm_register_obj.create(cursor, uid, register)
        elif file_content['file_type'] == 'zip':
            # files_zipped keys of file_content dict
            # raw_file_name in zip
            info = ''
            for f_key_name in files_zipped:
                file_read = file_content[f_key_name]  # Get content raw
                # content in zip is already decode, decode 64 dont needed
                reader = csv.reader(
                    StringIO.StringIO(file_read), delimiter='\t'
                )
                result = tm_profile_obj.load_ziverq_file_curve(cursor, uid,
                                                               reader,
                                                               wizard.unit,
                                                               context=context
                                                               )
                info_f = result['message']
                info = '{}\n{}'.format(info, info_f)
                register = {'name': file_content['name'],
                            'file_type': 'p1d_profile',
                            'state': 'read'
                            }

                if result['error']:
                    register['state'] = 'error'

                tm_register_obj.create(cursor, uid, register)
        else:
            info = _('This file is not supported. The supported types of '
                     'file are:\n - A raw .curva file\n - A compressed '
                     'zip file of raw .curva files')
        return info

    _filetype_selection = [
        ('p1d', 'P1D'),
        ('tpl', 'TPL'),
        ('ziverq', 'ZiverQ')
    ]

    _columns = {
        'unit': fields.selection([(1000, 'KWatts'),
                                 (1, 'Watts')],
                                 "Unit",
                                 required=True),
        'file': fields.binary('File', required=True),
        'info': fields.text('Information', readonly=True),
        'f_name': fields.char('File name', size=256),
        'file_type': fields.selection(_filetype_selection,
                                      'File type',
                                      required=True),
    }

    _defaults = {
        'info': lambda *a: _('Wizard to load tpl curves to telemeasure '
                             'profiles.'),
    }

Tpl2TmProfilesLoader()
