# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
import time
from datetime import datetime


class WizardShowReadTM(osv.osv_memory):

    _name = 'wizard.show.read.tm'

    def action_show_read(self, cursor, uid, ids, context=None):
        cfg_obj = self.pool.get('res.config')
        if not context:
            context = {}
        tmmodel = context.get('tmmodel', 'tm.billing')

        wizard = self.browse(cursor, uid, ids[0], context=context)
        registrador = wizard.registrador_id

        registrador_name = registrador.name
        domain = [('name', '=', registrador_name)]

        if tmmodel == 'tm.billing':
            view_name = 'TM Billings'
        elif tmmodel == 'tm.profile':
            view_name = 'TM Profiles'
        else:
            view_name = 'TM Reads'

        vals_view = {
                'name': view_name,
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': tmmodel,
                'limit': 80,
                'type': 'ir.actions.act_window',
                'domain': domain,
                }

        return vals_view

    def default_registrador(self, cursor, uid, context=None):
        if context is None:
            context = {}
        model = context.get('src_model', 'giscedata.lectures.comptador')
        model_id = context.get('active_id')

        meter_obj = self.pool.get('giscedata.lectures.comptador')

        if model == 'giscedata.lectures.comptador':
            meter_data = meter_obj.read(cursor, uid, model_id, ['registrador_id'])
            reg_id = meter_data['registrador_id'][0]
        else:
            reg_id = model_id
        return reg_id

    _columns = {
        'registrador_id': fields.many2one('giscedata.registrador',
                                    'Registrator', required=True),
    }

    _defaults = {
        'registrador_id': default_registrador
    }

WizardShowReadTM()
