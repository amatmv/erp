# -*- coding: utf-8 -*-
from osv import osv, fields
from dateutil.relativedelta import relativedelta
from tools.translate import _
from giscedata_telemesura.utils import WIZARD_STATES
from giscedata_telemesura.giscedata_telemesures_base import SYNC_TYPE_SEL


class TmBillingsLoader(osv.osv_memory):

    _name = 'wizard.tm.sync.date'

    def action_sync_date(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context=context)
        wizard.write(
            {'info': _('Working on the request'),
             'state': 'request'}, context=context)
        conf_obj = self.pool.get('res.config')
        delay = int(conf_obj.get(cursor, uid, 'tm_auto_sincro_seconds', 60))
        option = 't'
        if wizard.sync_type_sel == 'no_sync':
            option = 't'
        elif wizard.sync_type_sel == 'auto_sync':
            option = 'ts'
        elif wizard.sync_type_sel == 'force_sync':
            option = 'ts'
            delay = 0
        registrator_id = wizard.registrator_id['id']
        tm_registrator_obj = self.pool.get('giscedata.registrador')
        res = tm_registrator_obj.request_tm_sync_date(cursor, uid, registrator_id, option, delay)
        wizard.write(
            {'info': _(res),
             'state': 'end'}, context=context)

    _columns = {
        'registrator_id': fields.many2one('giscedata.registrador', 'Registrator', required=True),
        'sync_type_sel': fields.selection(SYNC_TYPE_SEL, 'Sync type',
                                          help="Select wether the meter time must be syncronized or not. "
                                               "Force: Always sync the meter time, "
                                               "No: Don't sync, "
                                               "Auomatic: Sync only if the delay is significative"),
        'info': fields.text('Information', readonly=True),
        'state': fields.selection(WIZARD_STATES, 'Working state'),
    }

    _defaults = {
        'registrator_id': lambda cursor, uid, ids, context: context.get('active_ids')[0],
        'info': lambda *a: _('Wizard to sync registrator date.'),
        'sync_type_sel': 'no_sync',
        'state': lambda *a: 'standby',
    }

TmBillingsLoader()
