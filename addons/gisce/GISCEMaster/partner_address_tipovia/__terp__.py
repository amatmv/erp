# -*- coding: utf-8 -*-
{
    "name": "Tipo de Via para Partner Address",
    "description": """
    Modifica el formulari d'una partner address per tal que s'assembi a un CUPS.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_address_tipovia_view.xml"
    ],
    "active": False,
    "installable": True
}
