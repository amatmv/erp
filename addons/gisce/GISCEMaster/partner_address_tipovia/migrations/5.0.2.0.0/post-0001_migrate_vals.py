# -*- coding: utf-8 -*-

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import GisceUpgradeMigratoor
import netsvc

def migrate(cursor, installed_version):
    cursor.execute("UPDATE res_partner_address SET "
        "tv=tipovia, "
        "nv=carrer, "
        "pnp=numero, "
        "es=escala, "
        "pt=pis, "
        "pu=porta"
    )
