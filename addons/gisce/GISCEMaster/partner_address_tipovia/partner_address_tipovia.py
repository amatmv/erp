# -*- coding: utf-8 -*-
from osv import osv, fields

class ResPartnerAddress(osv.osv):
    """Direccions dels partners semblants als CUPS.
    """
    _name = 'res.partner.address'
    _inherit = 'res.partner.address'

    # El que farem és sobreescriure el mètode write per tal de que quan es guardi
    # escriurem al camp 'street' la composició dels diferents camps.

    # TODO: Fer on_change d'alguns camps per autocompletar.

    def _trg_street(self, cursor, uid, ids, context=None):
        """Trigger que es cridarà per tal de tornar a calcular l'street.
        """
        return ids
    
    def _street(self, cursor, uid, ids, field_name, arg, context=None):
        """Mètode write, per sobreescriure els camps originals.
        """
        res = {}
        for address in self.browse(cursor, uid, ids, context):
            comp = []
            if address.tv and address.tv.abr:
                comp.append(address.tv.abr)
                comp.append('. ')
            if address.nv:
                comp.append(address.nv)
            if address.pnp:
                comp.append(', ')
                comp.append(address.pnp)
            if address.es:
                comp.append(' ')
                comp.append(address.es)
            if address.pt:
                comp.append(' ')
                comp.append(address.pt)
            if address.pu:
                comp.append(' ')
                comp.append(address.pu)
            if address.apartat_correus:
                comp.append(' ')
                comp.append(address.apartat_correus)
            if address.aclarador:
                comp.append(' - ')
                comp.append(address.aclarador)
            street = ''.join(map(unicode, comp))
            res[address.id] = street
        return res

    def onchange_municipi_id(self, cursor, uid, ids, muni_id, context=None):
        ''' busquem provincia i pais i els assignem'''
        res = {'value': {}}
        if not muni_id:
            return res
        muni_obj = self.pool.get('res.municipi')
        prov_obj = self.pool.get('res.country.state')
        prov_id = muni_obj.read(cursor, uid, muni_id, ['state'])['state'][0]
        country = prov_obj.read(cursor, uid, prov_id, ['country_id'])
        country_id = country['country_id'][0]
        res['value']['state_id'] = prov_id
        res['value']['country_id'] = country_id
        return res

    _columns = {
        'ref_catastral': fields.char('Ref Catastral (c)', size=20),
        'tv': fields.many2one('res.tipovia', 'Tipus Via'),
        'nv': fields.char('Carrer', size=256),
        'pnp': fields.char('Número', size=10),
        'bq': fields.char('Bloc', size=4),
        'es': fields.char('Escala', size=10),
        'pt': fields.char('Planta',size=10),
        'pu': fields.char('Porta', size=10),
        'cpo': fields.char('Poligon', size=10),
        'cpa': fields.char('Parcel·la', size=10),
        'aclarador': fields.char('Aclarador', size=256),
        'street': fields.function(_street, method=True, string="Street",
                                type="char", size=128, readonly=True,
                                store={'res.partner.address': (
                                       _trg_street, ['tv', 'nv', 'pnp',
                                                     'es', 'pt', 'pu',
                                                     'cpo', 'cpa', 'aclarador',
                                                     'apartat_correus'], 10)}),
        'id_municipi': fields.many2one('res.municipi', 'Municipi'),
        'id_poblacio': fields.many2one('res.poblacio', 'Població'),
        'apartat_correus': fields.char("Apartat de Correus", size=5)
    }

ResPartnerAddress()
