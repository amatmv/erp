# -*- coding: iso-8859-1 -*-
from osv import osv, fields

class giscedata_qualitat_incidence(osv.osv):
	_name = "giscedata.qualitat.incidence"
	_inherit = "giscedata.qualitat.incidence"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}
	_defaults = {
		#'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}
giscedata_qualitat_incidence()

class giscedata_qualitat_install_power_data(osv.osv):
	_name = "giscedata.qualitat.install_power.data"
	_inherit = "giscedata.qualitat.install_power.data"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}
	_defaults = {
		#'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}
giscedata_qualitat_install_power_data()

