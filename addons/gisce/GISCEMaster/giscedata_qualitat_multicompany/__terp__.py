# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Qualitat Multicompany",
    "description": """Revisions 


 $Id: __terp__.py 324 2007-08-17 14:17:57Z eduard $""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi company",
    "depends":[
        "giscedata_qualitat",
        "partner_multicompany",
        "giscedata_cts_multicompany",
        "giscedata_transformadors_multicompany"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_qualitat_multicompany_view.xml",
        "giscedata_qualitat_multicompany_wizard.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
