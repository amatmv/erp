# -*- coding: iso-8859-1 -*-
import wizard
import pooler
from datetime import *
import tools

_avis_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Es genererà la potència instal·lada del mes actual." />
</form>"""

_avis_fields = {}

def _empresa_selection(self, cr, uid, context={}):
  res = pooler.get_pool(cr.dbname).get('res.company').read(cr, uid, tools.get_user_companies(cr, uid), ['id', 'name'])
  return [(r['id'], r['name']) for r in res]


_cosfi_form = """<?xml version="1.0"?>
<form string="Avís">
  <field name="empresa" required="1" />
  <newline />
  <field name="cosfi" required="1" />
</form>"""

_cosfi_fields = {
  'cosfi': {'type': 'float', 'string': 'Cosfi', 'size': 60},
  'empresa': {'string': 'Empresa', 'type':'selection', 'selection': _empresa_selection },
}

def _calc(self, cr, uid, data, context={}):
  pool = pooler.get_pool(cr.dbname)
  id = pool.get('giscedata.qualitat.install_power.data').create(cr, uid, {'name': (datetime.now()).strftime('%Y-%m-01 00:00:00'), 'cosfi': data['form']['cosfi']})
  cr.execute("select "+str(id)+" as install_power_data_id, 'T' as type,t.name,ct.name as code_ct,ct.id_municipi as codeine,t.potencia_nominal as power, ct.zona_id as zone_ct_id, ct.x, ct.y from giscedata_transformador_trafo t, giscedata_cts ct, giscedata_transformador_estat e where t.id_estat = e.id and e.codi =1 and t.ct = ct.id and ct.company_id = "+str(data['form']['empresa'])+" and t.company_id = "+str(data['form']['empresa'])+" and t.tiepi = True")
  for trafo in cr.dictfetchall():
    pool.get('giscedata.qualitat.install_power').create(cr, uid, trafo)
  cr.execute("select 'C' as type,e.name, p.pot as power from res_partner p, giscedata_cups_ps cups, giscedata_cups_blockname block, giscedata_cups_escomesa e where p.company_id = "+str(data['form']['empresa'])+" and p.cups = cups.id and cups.id_escomesa = e.id and e.blockname = block.id and block.code = 1")
  return {}


class wizard_qualitat_potencia_installada(wizard.interface):

  states = {
    'init': {
    	'actions': [],
      'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('cosfi', 'Continuar', 'gtk-go-forward')]}
    },
    'cosfi': {
    	'actions': [],
      'result': {'type': 'form', 'arch': _cosfi_form,'fields': _cosfi_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('calc', 'Continuar', 'gtk-go-forward')]}
    },
    'calc': {
    	'actions': [_calc],
    	'result': { 'type' : 'state', 'state' : 'pre_end' },
    },
    'pre_end': {
    	'actions': [],
    	'result': { 'type' : 'state', 'state' : 'end' },
    },
  }

wizard_qualitat_potencia_installada('giscedata.qualitat.potencia.installada.multicompany')

