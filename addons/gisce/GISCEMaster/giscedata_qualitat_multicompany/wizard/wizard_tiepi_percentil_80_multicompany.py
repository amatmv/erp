# -*- coding: iso-8859-1 -*-
import wizard
import pooler
import math
from datetime import *
import tools

def _any_selection(self, cr, uid, context={}):
  cr.execute("select to_char(begin_date, 'YYYY') from giscedata_qualitat_span order by begin_date limit 1")
  any_inici = cr.fetchone()
  if len(any_inici):
    res = [(a, a) for a in range(int(any_inici[0]), datetime.now().year+1)]
  return res

def _empresa_selection(self, cr, uid, context={}):
  res = pooler.get_pool(cr.dbname).get('res.company').read(cr, uid, tools.get_user_companies(cr, uid), ['id', 'name'])
  return [(r['id'], r['name']) for r in res]

_any_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Esculli l'any" colspan="2"/>
  <newline />
  <field name="empresa" required="1" />
  <newline />
  <field name="any" required="1"/>
</form>"""

_any_fields = {
  'any': {'string': 'Any', 'type':'selection', 'selection': _any_selection },
  'empresa': {'string': 'Empresa', 'type':'selection', 'selection': _empresa_selection },
}


def _calc(self, cr, uid, data, context={}):

  # Borrem les entrades del mes que volem escriure
  cr.execute("delete from giscedata_qualitat_report_tiepi_percentil80 where to_char(data, 'YYYY') = '"+str(data['form']['any'])+"'")
  cr.commit()

  # Consulta per numerador (mes info a tiepi_mensual.sql)
  cr.execute("select data, municipi, codeine, provincia, zona, zona_id, cause, sum(power*((temps::float/3600))) as pot_affect from (select s.id as span_id, to_char(s.begin_date, '01-01-YYYY') as data, to_char(s.end_date - s.begin_date, 'SSSS')::int as temps,m.name as municipi, prov.name as provincia,inst.codeine,z.name as zona, z.id as zona_id,c.code as cause,sum(case when inst.type = 'C' then inst.power/(select cosfi from giscedata_qualitat_install_power_data where to_char(data, 'MM-YYYY') = to_char(s.begin_date, 'MM-YYYY')) when inst.type = 'T' then inst.power end) as power from giscedata_qualitat_affected_installation inst, giscedata_qualitat_span s, giscedata_qualitat_incidence i, giscedata_cts_zona z, giscedata_qualitat_cause c, res_municipi m, res_country_state prov where c.id = i.cause_id and m.id = inst.codeine and m.state = prov.id and z.id = inst.zone_ct_id and s.incidence_id = i.id and inst.span_id = s.id and i.id in (select distinct s.incidence_id from giscedata_qualitat_incidence i, giscedata_qualitat_span s where s.incidence_id = i.id and i.company_id = "+str(data['form']['empresa'])+" and to_char(begin_date, 'YYYY') = '"+str(data['form']['any'])+"' and i.affected_means = 1 and to_char(s.end_date - s.begin_date, 'SSSS')::int > 180) group by s.id, s.begin_date, temps, m.name, z.name, z.id, inst.type, c.code, inst.codeine, prov.name order by m.name) as foo group by data, municipi, codeine, provincia, zona, zona_id, cause order by municipi")

  report_ids = []
  poblacions = cr.dictfetchall()
  q_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.report.tiepi.percentil80')
  for poble in poblacions:
    vals = {}
    # Anem a buscar la potencia instal·lada per municipi, zona i mes
    cr.execute("select sum(case when pi.type = 'C' then pi.power/d.cosfi when pi.type = 'T' then pi.power end) as power from giscedata_qualitat_install_power pi, giscedata_qualitat_install_power_data d where pi.install_power_data_id = d.id and d.company_id = "+str(data['form']['empresa'])+" and to_char(d.name, 'YYYY') = '"+str(data['form']['any'])+"' and  pi.codeine = "+str(poble['codeine'])+" and pi.zone_ct_id = "+str(poble['zona_id'])+" group by d.name order by d.name DESC")
    pi = float("%.4f" % cr.fetchone()[0])
    tiepi = float("%.4f" % (poble['pot_affect']/pi)) 
    vals['provincia'] = poble['provincia']
    vals['municipi'] = poble['municipi']
    vals['zona'] = poble['zona']
    vals['data'] = poble['data']
    vals['tiepi'] = tiepi
    vals['percentil80'] = False

    if poble['cause'] == 1:
      id = q_obj.search(cr, uid, [('data','=',vals['data']), ('municipi','=',vals['municipi']), ('zona', '=', vals['zona'])])
      if len(id):
        t = q_obj.read(cr, uid, id, ['tiepi'])
        q_obj.write(cr, uid, id, {'tiepi': (t[0]['tiepi']+tiepi)})
      else:
        report_ids.append(int(q_obj.create(cr, uid, vals)))
    elif poble['cause'] == 2:
      id = q_obj.search(cr, uid, [('data','=',vals['data']), ('municipi','=',vals['municipi']), ('zona', '=', vals['zona'])])
      if len(id):
        t = q_obj.read(cr, uid, id, ['tiepi'])
        q_obj.write(cr, uid, id, {'tiepi': (t[0]['tiepi']+tiepi)})
      else:
        report_ids.append(int(q_obj.create(cr, uid, vals)))

        
  data['report'] = {'ids': report_ids}
  return {}

def _percentil(self, cr, uid, data, context={}):
  q_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.report.tiepi.percentil80')
  # Mirem les zones que hi ha per provincia
  cr.execute("select distinct zona,provincia from giscedata_qualitat_report_tiepi_percentil80 where to_char(data, 'YYYY') = '"+str(data['form']['any'])+"'")
  # Busquem el percentil 80 de cada zona
  zones = cr.dictfetchall()
  for zona in zones:
    # Agafem per cada zona_provincia tots els tiepis i busquem quin és el p80
    cr.execute("select id,municipi,tiepi from giscedata_qualitat_report_tiepi_percentil80 where zona = '%s' and provincia = '%s' order by tiepi asc" % (zona['zona'], zona['provincia']))
    pobles = cr.dictfetchall()
    pos = int(math.ceil(float(80/float(100/len(pobles))))) - 1
    q_obj.write(cr, uid, [pobles[pos]['id']], {'percentil80': True})
  return {}


def _print_report(self, cr, uid, data, context={}):
  return data['report']



class wizard_qualitat_tiepi_percentil80(wizard.interface):

  states = {
    'init': {
    	'actions': [],
      'result': {'type': 'form', 'arch': _any_form,'fields': _any_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('calc', 'Continuar', 'gtk-go-forward')]}
    },
    'calc': {
    	'actions': [_calc],
    	'result': { 'type' : 'state', 'state' : 'percentil' },
    },
    'percentil': {
    	'actions': [_percentil],
    	'result': { 'type' : 'state', 'state' : 'print_report' },
    },
    'print_report': {
    	'actions': [_print_report],
      'result': {'type': 'print', 'report': 'giscedata.qualitat.report5', \
        'get_id_from_action':True, 'state':'end'}
    },
  }

wizard_qualitat_tiepi_percentil80('giscedata.qualitat.tiepi.percentil80.multicompany')

