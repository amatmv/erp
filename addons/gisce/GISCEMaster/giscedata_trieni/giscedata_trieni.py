# -*- coding: utf-8 -*-
"""Trienis.
"""

# pylint: disable=E1101,W0223

from osv import fields, osv

class GiscedataTrieni(osv.osv):

    _name = "giscedata.trieni"
    _description = "Trienis"

    _columns = {
      'name': fields.char('Trimestre', size=10),
      'anyy': fields.integer('Any'),
      'trieni': fields.integer('Trieni'),
      'data_inici': fields.date('Inici'),
      'data_final': fields.date('Final'),
    }

    _defaults = {
    }

    _order = "name , id"

GiscedataTrieni()