# -*- coding: utf-8 -*-
{
    "name": "GISCE Trieni",
    "description": """
    This module provide :
    Els trienis per revisions i altres
    """,
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_trieni_demo.xml"
    ],
    "update_xml":[
        "ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
