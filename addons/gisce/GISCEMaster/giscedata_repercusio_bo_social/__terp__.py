# -*- coding: utf-8 -*-
{
    "name": "Repercutir bono social en las facturas",
    "description": """
    Repercutir bono social en las facturas
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Facturació",
    "depends":[
        "base",
        "giscedata_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_data.xml",
    ],
    "active": False,
    "installable": True
}
