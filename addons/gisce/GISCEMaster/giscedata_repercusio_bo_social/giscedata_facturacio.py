# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import datetime, date, timedelta
import calendar


def _get_quantity_by_month(start_date, end_date):
        if isinstance(start_date, basestring):
            start_date = datetime.strptime(start_date, '%Y-%m-%d')
        if isinstance(end_date, basestring):
            end_date = datetime.strptime(end_date, '%Y-%m-%d')
        value = 0.0
        while end_date >= start_date:
            units = 1.0 / calendar.monthrange(end_date.year, end_date.month)[1]
            value += units
            end_date -= timedelta(days=1)
        return round(value, 2)


class GiscedataFacturacioFacturador(osv.osv):
    """Sobreescrivim el mètode per facturar per incloure el donatiu"""

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        """Sobreescrivim el mètode per incloure-hi la línia de factura
           rel·lacionada amb el donatiu si s'escau"""
        if not context:
            context = {}

        factures_creades = super(
            GiscedataFacturacioFacturador, self
        ).fact_via_lectures(cursor, uid, polissa_id, lot_id, context)

        cfg = self.pool.get('res.config')

        active_bo_social = int(cfg.get(
            cursor, uid, 'invoice_active_bo_social', '0'
        ))
        start_date_bo_social = cfg.get(
            cursor, uid, 'invoice_start_date_bo_social', '2016-01-01'
        )
        fes_bo_social = active_bo_social or False
        if not fes_bo_social:
            return factures_creades

        ctx = context.copy()
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        product_obj = self.pool.get('product.product')
        uom_obj = self.pool.get('product.uom')
        imd_obj = self.pool.get('ir.model.data')

        # identificar el producte Bo Social
        pbosocial_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_repercusio_bo_social', 'bosocial_BS01'
        )[1]

        # Obtenim unitats de mesura per calcular el preu mes tard
        uom_dies_rbs = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_repercusio_bo_social', 'product_uom_day'
        )[1]
        uom_dies_t_rbs = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_repercusio_bo_social', 'product_uom_day_traspas'
        )[1]
        uom_mes = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_repercusio_bo_social', 'product_uom_mes'
        )[1]

        for fact in fact_obj.browse(cursor, uid, factures_creades, context):
            # Only invoices with start date after 'start_date_bo_social'
            # Si la factura és del diari d'energia s'afegeix la línia de
            # factura rel·lacionada amb el producte Bo Social
            if fact.journal_id.code in ('ENERGIA', 'ENERGIA.R') and fact.data_inici >= start_date_bo_social:
                ctx.update({'lang': fact.partner_id.lang})
                p_br = product_obj.read(cursor, uid, pbosocial_id, ['uom_id', 'description'], ctx)

                data_final = datetime.strptime(fact.data_final, '%Y-%m-%d')
                data_inici = datetime.strptime(fact.data_inici, '%Y-%m-%d')

                uom_final = p_br['uom_id'][0]
                if uom_final == uom_mes:
                    final_quantitiy = _get_quantity_by_month(data_inici, data_final)
                elif uom_final in [uom_dies_rbs, uom_dies_t_rbs]:
                    final_quantitiy = (data_final - data_inici).days + 1
                else:
                    raise Exception(
                        "El producte {0} (id:{1}) ha d'estar configurat amb "
                        "unitats per dia (id:{2}) o per mes (id:{3}).".format(
                            p_br['description'], p_br['id'], uom_dies_rbs,
                            uom_mes
                        )
                    )

                vals = {
                    'data_desde': fact.data_inici,
                    'data_fins': fact.data_final,
                    'uos_id': uom_final,
                    'quantity': final_quantitiy,
                    'multi': 1,
                    'product_id': pbosocial_id,
                    'tipus': 'altres',
                    'name': p_br['description']
                }
                self.crear_linia(cursor, uid, fact.id, vals, ctx)
                fact_obj.button_reset_taxes(cursor, uid, [fact.id])

        facts_to_remove_iese = self.get_invoices_to_remove_iese(
            cursor, uid, factures_creades, context=context
        )

        if facts_to_remove_iese:
            self.remove_iese_tax(
                cursor, uid, facts_to_remove_iese, context=context
            )

        return factures_creades


GiscedataFacturacioFacturador()
