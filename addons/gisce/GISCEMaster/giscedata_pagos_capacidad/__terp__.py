# -*- coding: utf-8 -*-
{
    "name": "Pagos por capacidad ITC 3353/2010",
    "description": """Aquest mòdul defineix el pricelist type""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "product"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_pagos_capacidad_data.xml"
    ],
    "active": False,
    "installable": True
}
