<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="cables"/>
  </xsl:template>

  <xsl:template match="cables">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>

      <stylesheet>
        <paraStyle name="parastyle" fontName="Helvetica" fontSize="10" spaceBefore="0" spaceAfter="0"/>
      </stylesheet>

      <story>
        <h1 t="1">Llistat de Cables</h1>
        <xsl:apply-templates select="cable" mode="story"/>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="cable" mode="story">
    <para style="parastyle" t="1">Nom: <xsl:value-of select="name"/></para>
  </xsl:template>
</xsl:stylesheet>
