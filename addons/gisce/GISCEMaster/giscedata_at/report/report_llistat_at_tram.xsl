<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="trams"/>
  </xsl:template>

  <xsl:template match="trams">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>

      <stylesheet>
        <paraStyle name="parastyle" fontName="Helvetica" fontsize="10" spaceBefore="0" spaceAfter="0"/>
        
        <paraStyle name="titol"
        	fontName="Helvetica-Bold"
        	fontSize="14"
        	leading="28"
        />
        
        
        <blockTableStyle id="trams">
        	<blockBackground colorName="grey" start="0,0" stop="10,0" />
        	<blockFont name="Helvetica" size="10" />
        	<blockFont name="Helvetica-Bold" size="10" start="0,0" stop="10,0"/>
        	<blockAlignment value="RIGHT" start="3,1" stop="3,-1" />
        	<blockAlignment value="RIGHT" start="4,1" stop="4,-1" />
        </blockTableStyle>
      </stylesheet>

      <story>
      <blockTable style="trams" colWidths="1cm,2.5cm,1.2cm,2cm,2cm,2.5cm,1.7cm,1.7cm,1cm,1.3cm,1.5cm" repeatRows="1">
    <tr>
      <td t="1">Codi</td>
      <td t="1">Ordre</td>
      <td t="1">Baixa</td>
      <td t="1">Long. CAD</td>
      <td t="1">Long. Op.</td>
      <td t="1">Conductor</td>
      <td t="1">A�llam.</td>
      <td t="1">Secci�</td>
      <td t="1">Mat.</td>
      <td t="1">N� Cir.</td>
      <td t="1">Pl�nol</td>
    </tr>
    <xsl:apply-templates select="tram" mode="story" />
    </blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="tram" mode="story">
    <tr>
      <td><xsl:value-of select="name" /></td>
      <td><xsl:value-of select="ordre" /></td>
      <td><xsl:value-of select="baixa" /></td>
      <td><xsl:value-of select="longitud_cad" /></td>
      <td><xsl:value-of select="longitud_op" /></td>
      <td><xsl:value-of select="conductor" /></td>
      <td><xsl:value-of select="aillament" /></td>
      <td><xsl:value-of select="seccio" /></td>
      <td><xsl:value-of select="material" /></td>
      <td><xsl:value-of select="circuits" /></td>
      <td><xsl:value-of select="planol" /></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
