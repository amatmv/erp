<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="linies"/>
  </xsl:template>

  <xsl:template match="linies">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>

      <stylesheet>
        <paraStyle name="parastyle" fontName="Helvetica" fontsize="10" spaceBefore="0" spaceAfter="0"/>
        
        <paraStyle name="titol"
        	fontName="Helvetica-Bold"
        	fontSize="14"
        	leading="28"
        />
        
        
        <blockTableStyle id="trams">
        	<blockBackground colorName="grey" start="0,0" stop="-1,0" />
        	<blockFont name="Helvetica" size="10" />
        	<blockFont name="Helvetica-Bold" size="10" start="0,0" stop="-1,0"/>
        </blockTableStyle>
      </stylesheet>

      <story>
        <xsl:apply-templates select="linia" mode="story"/>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="linia" mode="story">
    <para style="titol">Expedients L�nia <xsl:value-of select="name" /></para>
    <xsl:apply-templates select="trams" mode="story" />
  </xsl:template>

  <xsl:template match="trams" mode="story">
    <blockTable style="trams" colWidths="2.7cm,2.7cm,2.7cm,2.7cm,2.7cm,2.7cm,2.7cm" repeatRows="1">
    <tr>
      <td t="1">Codi</td>
      <td t="1">Ordre</td>
      <td t="1">Exp. Intern</td>
      <td t="1">Exp. Ind�stria</td>
      <td t="1">Data Inici</td>
      <td t="1">Data CFO</td>
      <td t="1">Data Autorit.</td>
    </tr>
    <xsl:apply-templates select="tram" mode="story">
    	<xsl:sort select="ordre" />
    </xsl:apply-templates>
    </blockTable>
  </xsl:template>
  
  <xsl:template match="tram" mode="story">
  	<xsl:if test="count(expedients/expedient)&gt;0">
  		<xsl:apply-templates select="expedients/expedient" mode="story" />
  	</xsl:if>
  	<xsl:if test="count(expedients/expedient)=0">
  		<tr>
      		<td><xsl:value-of select="name" /></td>
      		<td><xsl:value-of select="ordre" /></td>
      		<td></td>
      		<td></td>
      		<td></td>
      		<td></td>
     		<td></td>
    	</tr>
  	</xsl:if>
  </xsl:template>

  <xsl:template match="expedients/expedient" mode="story">
    <tr>
      <td><xsl:value-of select="../../name" /></td>
      <td><xsl:value-of select="../../ordre" /></td>
      <td><xsl:value-of select="intern" /></td>
      <td><xsl:value-of select="industria" /></td>
      <td><xsl:if test="data_inici!=''"><xsl:value-of select="concat(substring(data_inici, 9, 2),'/',substring(data_inici, 6, 2),'/',substring(data_inici, 1, 4))" /></xsl:if></td>
      <td><xsl:if test="data_cfo!=''"><xsl:value-of select="concat(substring(data_cfo, 9, 2),'/',substring(data_cfo, 6, 2),'/',substring(data_cfo, 1, 4))" /></xsl:if></td>
      <td><xsl:if test="data_industria!=''"><xsl:value-of select="concat(substring(data_industria, 9, 2),'/',substring(data_industria, 6, 2),'/',substring(data_industria, 1, 4))" /></xsl:if></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
