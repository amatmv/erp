<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="linies"/>
  </xsl:template>

  <xsl:template match="linies">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>

      <stylesheet>
        <paraStyle name="titol" 
        	fontName="Helvetica-Bold"
        	fontSize="14"
        	leading="28"
        />
        
        <paraStyle name="para"
        	fontName="Helvetica"
        	fontSize="10"
        />
        
        <blockTableStyle id="linies">
        	<blockBackground colorName="grey" start="0,0" stop="4,0" />
        	<blockFont name="Helvetica" size="10" />
        	<blockFont name="Helvetica-Bold" size="10" start="0,0" stop="4,0"/>
        	<blockAlignment value="RIGHT" start="2,1" stop="2,-1" />
        	<blockAlignment value="RIGHT" start="3,1" stop="3,-1" />
        	<blockAlignment value="RIGHT" start="4,1" stop="4,-1" />
        	<!--<blockAlignment value="RIGHT" start="5,1" stop="5,-1" />-->
        	<lineStyle kind="GRID" colorName="silver" />
        </blockTableStyle>
        
      </stylesheet>

      <story>
        <para style="titol" t="1">Llistat de L�nies</para>
        <blockTable style="linies" colWidths="1.2cm,5cm,2.2cm,2.2cm,6.2cm" repeatRows="1">
        	<tr t="1">
        		<td>L�nia</td>
        		<td>Descripci�</td>
        		<td>A�ria (m)</td>
        		<td>Subt. (m)</td>
        		<td>N� Suports</td>
        	</tr>
	        <xsl:apply-templates select="linia" mode="story"/>
	    </blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="linia" mode="story">
  	<tr>
		<td><para style="para"><xsl:value-of select="name" /></para></td>
        <xsl:choose>
            <xsl:when test="string-length(descripcio) &gt; 35">
              <td><para style="para" fontSize="8"><xsl:value-of select="concat(substring(descripcio, 1, 33), '...')" /></para></td>
            </xsl:when>
            <xsl:otherwise>
              <td><para style="para" fontSize="8"><xsl:value-of select="descripcio" /></para></td>
            </xsl:otherwise>
        </xsl:choose>

		<td><xsl:value-of select="floor(long_aeria_cad)" /></td>
		<td><xsl:value-of select="floor(long_subt_cad)" /></td>
        <td><para t="1">
            <t>Formig�:</t><xsl:value-of select="concat(' ', n_suports_formigo, ' ')"/>
            <t>Fusta:</t><xsl:value-of select="concat(' ', n_suports_fusta, ' ')"/>
            <t>Met�l�lics:</t><xsl:value-of select="concat(' ', n_suports_metallics)"/>
        </para></td>
	</tr>
  </xsl:template>
</xsl:stylesheet>
