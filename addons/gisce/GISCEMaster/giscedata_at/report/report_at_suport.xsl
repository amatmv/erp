<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="suports"/>
  </xsl:template>

  <xsl:template match="suports">
    <document>
      <template pageSize="(297mm,19cm)" topMargin="0.5cm" bottomMargin="1cm" rightMargin="1cm">
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="277mm" height="17cm"/>
        </pageTemplate>
      </template>

      <stylesheet>
        <paraStyle name="titol" 
        	fontName="Helvetica-Bold"
        	fontSize="14"
        	leading="28"
        />
        
        <paraStyle name="para"
        	fontName="Helvetica"
        	fontSize="10"
        />
        
        <blockTableStyle id="taula">
        	<blockBackground colorName="grey" start="0,0" stop="-1,0" />
        	<blockFont name="Helvetica" size="10" />
        	<blockFont name="Helvetica-Bold" size="10" start="0,0" stop="-1,0"/>
        	<lineStyle kind="GRID" colorName="silver" />
        </blockTableStyle>
        
      </stylesheet>

      <story>
        <para style="titol" t="1">Llistat de Suports</para>
        <blockTable style="taula" colWidths="2.5cm,8.1cm,1.7cm,2.7cm,1.7cm,1.2cm,2.7cm,2cm,1.7cm,2.7cm" repeatRows="1">
        	<tr>
        		<td t="1">Codi</td>
        		<td t="1">Observacions</td>
        		<td t="1">A�lladors</td>
        		<td t="1">Poste</td>
        		<td t="1">Esfor�</td>
        		<td t="1">Altura</td>
        		<td t="1">Material</td>
        		<td t="1">Model</td>
        		<td t="1">Armat</td>
        		<td t="1">Posici�</td>
        	</tr>
	        <xsl:apply-templates select="suport" mode="story">
	        	<xsl:sort select="name" />
	        </xsl:apply-templates>
	    </blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="suport" mode="story">
    <tr>
		<td><para style="para"><xsl:value-of select="name" /></para></td>
		<td><para style="para"><xsl:value-of select="observacions" /></para></td>
		<td>
		<xsl:for-each select="ailladors/aillador">
			<para style="para"><xsl:value-of select="codi" /></para>
		</xsl:for-each>
		</td>
		<td><para style="para"><xsl:value-of select="poste/codi" /></para></td>
		<td><para style="para"><xsl:value-of select="poste/esforc" /></para></td>
		<td><para style="para"><xsl:value-of select="poste/altura" /></para></td>
		<td><para style="para"><xsl:value-of select="poste/material" /></para></td>
		<td><para style="para"><xsl:value-of select="poste/model" /></para></td>
		<td><para style="para"><xsl:value-of select="armat/descripcio" /></para></td>
		<td><para style="para"><xsl:value-of select="armat/posicio" /></para></td>
	</tr>
  </xsl:template>
  
</xsl:stylesheet>
