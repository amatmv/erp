# -*- coding: utf-8 -*-

from datetime import date, datetime
from dateutil import relativedelta
from osv import osv, fields
from giscedata_administracio_publica_cnmc_distri.wizard.wizard_recalcul_cinis_tis import MODELS_SELECTION
import time
from giscedata_administracio_publica_cnmc_distri.giscedata_tipus_installacio import TIPUS_REGULATORI


MODELS_SELECTION.append(
    ('giscedata.at.tram', 'Línies AT'),
)


class GiscedataAtInstallacio(osv.osv):

    _name = "giscedata.at.installacio"
    _description = "Instal·lació Línia"

    _columns = {
      'name': fields.char('Tipus d\'instal·lació', size=60),
    }

    _defaults = {
    }

    _order = "name, id"


GiscedataAtInstallacio()


class GiscedataAtLinia(osv.osv):

    def _longitud_aeria_cad(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for linia in self.browse(cr, uid, ids):
            res[linia.id] = 0
            for tram in linia.trams:
                if tram.tipus == 1 and not tram.baixa and tram.active:
                    res[linia.id] += tram.longitud_cad
        return res

    def _longitud_aeria_op(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for linia in self.browse(cr, uid, ids):
            res[linia.id] = 0.0
            for tram in linia.trams:
                if tram.tipus == 1 and tram.baixa and tram.active:
                    if tram.longitud_op > 0:
                        res[linia.id] += tram.longitud_op
                    else:
                        res[linia.id] += tram.longitud_cad
        return res

    def _longitud_sub_cad(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for linia in self.browse(cr, uid, ids):
            res[linia.id] = 0
            for tram in linia.trams:
                if tram.tipus == 2 and  not tram.baixa and tram.active:
                    res[linia.id] += tram.longitud_cad
        return res

    def _longitud_sub_op(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for linia in self.browse(cr, uid, ids):
            res[linia.id] = 0.0
            for tram in linia.trams:
                if tram.tipus == 2 and tram.baixa and tram.active:
                    if tram.longitud_op > 0:
                        res[linia.id] += tram.longitud_op
                    else:
                        res[linia.id] += tram.longitud_cad
        return res

    def _tipus_installacio(self, cr, uid, context=None):
        if context is None:
            context = {}
        obj = self.pool.get('giscedata.at.installacio')
        ids = obj.search(cr, uid, [])
        res = obj.read(cr, uid, ids, ['name', 'id'], context)
        res = [(r['id'], r['name']) for r in res]
        return res

    def _seccio_string(self, cr, uid, ids, field_name, ar, context):
        res = {}
        sql = """
        SELECT distinct c.name
        FROM giscedata_at_cables c, giscedata_at_tram t, giscedata_at_linia l
        WHERE t.cable = c.id AND t.linia = l.id AND l.id = %s
        """
        for id in ids:
            id = int(id)
            cr.execute(sql, (id,))
            res[id] = ','.join([a[0] for a in cr.fetchall()])
        return res

    def _expedients_string(self, cr, uid, ids, field_name, ar, context):
        res = {}
        sql = """
        SELECT DISTINCT e.industria
        FROM giscedata_expedients_expedient e, giscedata_at_tram_expedient te,
            giscedata_at_tram t, giscedata_at_linia l
        WHERE e.id = te.expedient_id AND te.tram_id = t.id AND
            t.active = True AND t.linia = l.id AND
            e.industria IS NOT NULL AND l.id = %s
        """
        for id in ids:
            id = int(id)
            cr.execute(sql, (id,))
            res[id] = ','.join([a[0] for a in cr.fetchall()])
        return res

    def _expedients(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        sql = """
        SELECT DISTINCT e.id
        FROM giscedata_expedients_expedient e,
            giscedata_at_tram_expedient te, giscedata_at_tram t,
            giscedata_at_linia l
        WHERE e.id = te.expedient_id AND te.tram_id = t.id AND
            t.active=True AND t.linia = l.id AND
            e.industria IS NOT NULL AND l.id = %s
        """
        for id in ids:
            id = int(id)
            cr.execute(sql, (id,))
            res[id] = [a[0] for a in cr.fetchall()]
        return res

    def _suports_inactius(self, cr, uid, ids, field_name, ar, context=None):
        res = {}
        sql = """
        SELECT id
        FROM giscedata_at_suport
        WHERE active = false AND linia = %s
        """
        for id in ids:
            id = int(id)
            cr.execute(sql, (id,))
            res[id] = [a[0] for a in cr.fetchall()]
        return res

    def _data_industria(self, cr, uid, ids, field_name, args, context):
        res = {}
        sql = """
        SELECT DISTINCT e.industria_data AS data_industria
        FROM giscedata_expedients_expedient e, giscedata_at_linia l,
            giscedata_at_tram t, giscedata_at_tram_expedient r
        WHERE l.id = t.linia AND t.id = r.tram_id AND t.active = True AND
            r.expedient_id = e.id AND l.id = %s
        ORDER BY e.industria_data DESC LIMIT 1
        """
        for id in ids:
            id = int(id)
            cr.execute(sql, (id,))
            exp = cr.dictfetchone()
            if exp:
                res[id] = exp['data_industria']
            else:
                res[id] = False
        return res

    def _get_poste_ids(self, cursor, uid, ids, context=None):
        res = []

        obj_suport = self.pool.get('giscedata.at.suport')

        for pid in ids:
            id_suports = obj_suport.search(cursor, uid,
                                           [('poste.id', '=', pid)])
            linia = obj_suport.read(cursor, uid, id_suports, ['linia'])
            res += [x['linia'][0] for x in linia]

        return set(res)

    def _get_sup_ids(self, cursor, uid, ids, context=None):
        obj_suport = self.pool.get('giscedata.at.suport')

        linia = obj_suport.read(cursor, uid, ids, ['linia'])
        return set([x['linia'][0] for x in linia if x['linia']])

    def _ff_calc_n_suports(self, cursor, uid, ids, field_name, args, context):
        """
        Field function to calculate the number of supports
        :param cursor: Database cursor
        :param uid: User ID
        :type uid: int
        :param ids: at lines ids
        :type ids: list
        :param field_name: Field name to calculate
        :type field_name: str
        :param args:
        :param context: OpenErp context
        :type context: dict
        :return: support type by id
        :rtype: dict
        """
        res = {}

        for linia in self.browse(cursor, uid, ids):
            # sumatori de postes dependent del material
            sum_pfo = 0
            sum_pfu = 0
            sum_pm = 0
            sum_altres = 0

            for sup in linia.suports:
                lmaterial = sup.poste.material
                if lmaterial == 'pfo':
                    sum_pfo += 1
                elif lmaterial == 'pfu':
                    sum_pfu += 1
                elif lmaterial == 'pm':
                    sum_pm += 1
                else:
                    sum_altres += 1

            res[linia.id] = {
                'n_suports_formigo': sum_pfo,
                'n_suports_fusta': sum_pfu,
                'n_suports_metallics': sum_pm,
            }
        return res

    _name = "giscedata.at.linia"
    _description = "Línies Alta Tensió"

    _columns = {
        'name': fields.char('Línia', size=10, required=True),
        'descripcio': fields.char('Descripció', size=60),
        'poblacio': fields.many2one('res.poblacio', 'Població'),
        'municipi': fields.many2one('res.municipi', 'Municipi'),
        'comarca': fields.many2one('res.comarca', 'Comarca'),
        'provincia': fields.many2one('res.country.state', 'Província'),
        'n_suports_formigo': fields.function(
            _ff_calc_n_suports, method=True, store={
                'giscedata.at.poste': (_get_poste_ids, ['material'], 10),
                'giscedata.at.suport': (_get_sup_ids, ['poste', 'linia'], 10)
            },
            multi='calc_suports',
            type='integer',
            string='Suports Formigó'),
        'n_suports_fusta': fields.function(
            _ff_calc_n_suports, method=True,
            store={
                'giscedata.at.poste': (_get_poste_ids, ['material'], 10),
                'giscedata.at.suport': (_get_sup_ids, ['poste', 'linia'], 10)
            },
            multi='calc_suports', type='integer', string='Suports Fusta'),
        'n_suports_metallics': fields.function(
            _ff_calc_n_suports, method=True, store={
                'giscedata.at.poste': (_get_poste_ids, ['material'], 10),
                'giscedata.at.suport': (_get_sup_ids, ['poste', 'linia'], 10)
            },
            multi='calc_suports', type='integer', string='Suports Metàl·lics'),
        'tensio': fields.integer('Tensió normalitzada'),
        "tensio_id": fields.many2one(
            "giscedata.tensions.tensio",
            string="Tensió normalitzada",
            widget="selection",
            select=1,
            domain=[("tipus", "=", "AT")]
        ),
        'observacions': fields.text('Observacions'),
        'propietari': fields.boolean('Propietari'),
        'baixa': fields.boolean('LAT de Baixa'),
        'obres': fields.boolean('LAT en Obres'),
        'tipus_installacio': fields.many2one(
            'giscedata.at.installacio',
            'Tipus d\'instal·lació',
            selection=_tipus_installacio),
        'origen': fields.char('Origen', size=60),
        'final': fields.char('Final', size=60),
        'trams': fields.one2many('giscedata.at.tram', 'linia', 'Trams'),
        'longitud_aeria_cad': fields.function(
            _longitud_aeria_cad, method=True, string='Longitud Aèria Autocad'),
        'longitud_aeria_op': fields.function(
            _longitud_aeria_op,
            method=True,
            string='Longitud Aèria Operador'
        ),
        'longitud_sub_cad': fields.function(
            _longitud_sub_cad,
            method=True,
            string='Longitud Subt. Autocad'
        ),
        'longitud_sub_op': fields.function(
            _longitud_sub_op,
            method=True,
            string='Longitud Subt. Operador'
        ),
        'suports': fields.one2many(
            'giscedata.at.suport', 'linia', 'Suports Actius'
        ),
        'suports_inactius': fields.function(
            _suports_inactius,
            method=True,
            type='one2many',
            relation='giscedata.at.suport',
            string='Suports Inactius'
        ),
        'propietat_de': fields.many2one('res.partner', 'Propietari'),
        'active': fields.boolean('Activa'),
        'seccio_string': fields.function(
            _seccio_string,
            method=True,
            type='char',
            size=255,
            string='Secció conductors'),
        'expedients_string': fields.function(
            _expedients_string,
            method=True,
            type='char',
            size=255,
            string='Expedients'
        ),
        'expedients': fields.function(
            _expedients,
            method=True,
            type='one2many',
            relation='giscedata.expedients.expedient',
            string='Expedients'
        ),
        'data_industria': fields.function(
            _data_industria,
            type='date',
            method=True
        ),

    }

    _defaults = {
      'active': lambda *a: 1,
      'baixa': lambda *a: 0,
      'propietari': lambda *a: 1,
      'obres': lambda *a: 0,
    }

    _order = "name, id"

    _sql_constraints = [
        (
            'name_uniq',
            'unique (name)',
            'Ja existeix una linia AT amb aquest codi.'
         )
    ]

GiscedataAtLinia()


class GiscedataAtTipuslinia(osv.osv):

    _name = "giscedata.at.tipuslinia"
    _description = "Tipus de la línia"

    _columns = {
      'name': fields.char('Tipus', size=60, required=True),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataAtTipuslinia()


class GiscedataAtMaterial(osv.osv):

    _name = "giscedata.at.material"
    _description = "Material"

    _columns = {
      'name': fields.char('Material', size=60),
    }

    _defaults = {
    }

    _order = "name, id"

GiscedataAtMaterial()


class GiscedataAtTipuscable(osv.osv):

    _name = "giscedata.at.tipuscable"
    _description = "Tipus de Cable"

    _columns = {
      'codi': fields.char('Codi', size=1),
      'name': fields.char('Descripció', size=60),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataAtTipuscable()


class GiscedataAtConductors(osv.osv):

    _name = "giscedata.at.conductors"
    _description = "Conductors del cable"

    _columns = {
      'name': fields.char('Nom', size=60),
      'n_conductors': fields.integer('Número de conductors'),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataAtConductors()


class GiscedataAtFamiliacables(osv.osv):

    _name = "giscedata.at.familiacables"
    _description = "Família dels cables"

    _columns = {
      'name': fields.char('Nom', size=60),
    }

GiscedataAtFamiliacables()


class GiscedataAtCables(osv.osv):

    _name = "giscedata.at.cables"
    _description = "Cables"

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for cable in self.browse(cr, uid, ids):
            name = cable.name or ''
            if cable.material and cable.material.name:
                name = '%s "%s"' % (name.strip(),  cable.material.name.strip())
            res.append((cable.id, name))
        return res

    def _n_trams(self, cr, uid, ids, field_name, ar, context={}):

        res = {}
        sql = """
        SELECT c.id,count(t.id)
        FROM giscedata_at_cables c
            LEFT JOIN giscedata_at_tram t ON
                (t.cable = c.id AND t.active = True)
        WHERE c.id IN (%s)
        GROUP BY c.id"""
        cr.execute(sql % (','.join(map(str, map(int, ids)))))
        for cable in cr.fetchall():
            res[cable[0]] = cable[1]
        return res

    _columns = {
      'name': fields.char('Codi', size=60),
      'material': fields.many2one('giscedata.at.material', 'Material'),
      'seccio': fields.float('Secció'),
      'resistencia': fields.float('Resistència (Ohms/Km)', digits=(16, 8)),
      'reactancia': fields.float('Reactancia XL (Ohms/Km)', digits=(16, 8)),
      'observacions': fields.text('Observacions'),
      'd_interior': fields.float('Diàmetre interior'),
      'd_aillament': fields.float('Diàmetre sobre aïllament'),
      'd_exterior': fields.float('Diàmetre exteriror'),
      'pes': fields.float('Pes (Kg/Km)'),
      'capacitat': fields.float('Capacitat (microF/Km)'),
      'r_minim': fields.float('Radi mínim de curvatura (mm)'),
      'carrega_trencament': fields.float('Càrrega de trancament (Nw)'),
      'aillament': fields.char('Aïllament', size=10),
      'tipus': fields.many2one('giscedata.at.tipuscable', 'Tipus'),
      'conductors': fields.many2one('giscedata.at.conductors', 'Conductors'),
      'intensitat_admisible': fields.float('Intensitat Admisible'),
      'familia': fields.many2one('giscedata.at.familiacables', 'Família'),
      'n_trams': fields.function(_n_trams, method=True, type='integer',
                                 string='Nº de Trams'),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataAtCables()


class GiscedataAtTram(osv.osv):

    def create(self, cr, uid, vals, context={}):
        if 'name' not in vals:
            vals['name'] = self._get_nextnumber(cr, uid, context)
        vals['mslink'] = vals['name']

        return super(osv.osv, self).create(cr, uid, vals, context)

    def _get_nextnumber(self, cursor, uid, context=None):
        """Retorna el següent número de Tram AT"""

        return int(
            self.pool.get('ir.sequence').get(cursor, uid, 'giscedata.at.tram')
        )

    _name = "giscedata.at.tram"
    _description = "Trams Alta Tensió"

    def on_change_today_date(self, cr, uid, ids, baixa, data, context=None):
        """
        Make data_baixa as today when baixa is 1 and there wasn't any date

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Ids of the trams
        :type ids: list[int]
        :param baixa: State of the baixa field
        :param data: Field data_baixa
        :param context: OpenERP context
        :type context: dict
        :return: Values to update
        :rtype: dict
        """

        result = {}

        if baixa == 1 and not data:
            now = datetime.now().strftime("%Y-%m-%d")
            result = {'value': {
                'data_baixa': now, }
            }
        return result

    def _material(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for tram in self.browse(cr, uid, ids):
            if tram.cable and tram.cable.material:
                res[tram.id] = tram.cable.material.name
            else:
                res[tram.id] = ''
        return res

    def name_get(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not len(ids):
            return []
        res = []
        for tram in self.read(cursor, uid, ids, ['name', 'origen', 'final']):
            name = tram['name']
            if tram['origen'] and tram['final']:
                params = [tram['name'], tram['origen'], tram['final']]
                name = '{0} {1} - {2}'.format(*params)
            res.append((tram['id'], str(name)))
        return res

    def _aillament(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for tram in self.browse(cr, uid, ids):
            if tram.cable:
                res[tram.id] = tram.cable.aillament
            else:
                res[tram.id] = ''
        return res

    def _seccio(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for tram in self.browse(cr, uid, ids):
            if tram.cable:
                res[tram.id] = str(tram.cable.seccio)
            else:
                res[tram.id] = ''
        return res

    def read_distinct(self, cr, uid, values=[]):
        if not len(values):
            values.append(-1)

        sql = """
        SELECT t.id, t.name, l.name as linia,
            CASE WHEN t.ordre IS NULL THEN ''
            ELSE t.ordre
            END AS ordre,
            CASE WHEN t.origen IS NULL THEN ''
            ELSE t.origen
            END AS origen,
            CASE WHEN t.final IS NULL THEN ''
            ELSE t.final END AS final,
            CASE WHEN c.name IS NULL THEN ''
            ELSE c.name
            END AS cable,
            CASE WHEN t.longitud_cad  IS NULL THEN 0
            ELSE t.longitud_cad
            END AS longitud_cad,
            CASE WHEN t.longitud_op IS NULL THEN 0
            ELSE t.longitud_op END AS longitud_op,
            CASE WHEN t.baixa IS NULL THEN false
            ELSE t.baixa END AS baixa
        FROM giscedata_at_tram t,giscedata_at_linia l, giscedata_at_cables c
        WHERE t.cable = c.id AND t.linia = l.id AND
            t.baixa = false AND t.name NOT IN ({})
        """
        cr.execute(sql.format(','.join(map(str, map(int, values)))))
        return cr.dictfetchall()

    def read_distinct2(self, cr, uid, values):
        in_array = []
        sql = "SELECT name FROM giscedata_at_tram WHERE name = %s"
        for value in values:
            cr.execute(sql, (value,))
            if not cr.fetchone():
                in_array.append(value)
        return in_array

    def _expedients(self, cr, uid, ids, field_name, ar, context=None):
        if not context:
            context = {}
        res = {}
        sql = """
        SELECT coalesce(e.industria, 'S/N')
        FROM giscedata_expedients_expedient e, giscedata_at_tram_expedient te
        WHERE te.expedient_id = e.id AND te.tram_id = %s
        """
        for id in ids:
            id = int(id)
            cr.execute(sql, (id,))
            res[id] = ','.join([a[0] for a in cr.fetchall()])
        return res

    def _data_industria(self, cr, uid, ids, field_name, ar, context=None):
        if not context:
            context = {}
        res = {}

        if not ids:
            return res
            
        ids = ",".join([str(t) for t in ids])
        cr.execute("""SELECT t.id,max(e.industria_data) as industria_data
        FROM giscedata_at_tram t
        LEFT JOIN giscedata_at_tram_expedient te ON (t.id=te.tram_id)
        LEFT JOIN giscedata_expedients_expedient e ON (te.expedient_id=e.id)
        WHERE
           t.id in (%s)
        GROUP BY t.id""" % ids)
        sql = cr.dictfetchall()
        for exp in sql:
            res[exp["id"]] = exp["industria_data"] or False
        return res

    def _propietari(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Camp propietari de la lína del tram:
        Si la linia es propietari els trams d'aquesta també ho son

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Trams ids
        :type ids: list of int
        :param field_name: Name of the field to calcualate
        :type field_name: str
        :param arg:
        :param context: OpenERP context
        :type context: dict
        :return: Calcualted values
        :rtype: dict
        """

        if not isinstance(ids, (list, tuple)):
            ids = [ids]
            
        if not ids:
            return {}
        res = {}.fromkeys(ids, False)
        sql = """
        SELECT t.id, l.propietari FROM giscedata_at_tram t
        LEFT JOIN giscedata_at_linia l ON (l.id=t.linia)
        WHERE t.id IN %s
        """
        cursor.execute(sql, (tuple(ids), ))
        linies = cursor.dictfetchall()
        for lin in linies:
            res[lin['id']] = lin['propietari'] or False
        return res

    def _propietari_srch(self, cursor, uid, obj, name, args, context=None):
        """
        Search function for the propietari field

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param obj: Object ot search
        :param name: Search field name
        :type name: str
        :param args: Search arguemtns
        :type args: list of (str,str,str)
        :param context: Open ERP context
        :return: List of trams
        :rtype: list of int
        """

        sql = """
        SELECT tram.id FROM giscedata_at_tram AS tram
        LEFT JOIN giscedata_at_linia AS linia ON (tram.linia= linia.id)
        WHERE linia.propietari=%(propietari)s;
        """

        if args[0][2] ==0:
            params = {"propietari": False}
        else:
            params = {"propietari": True}
        cursor.execute(sql, params)
        data = cursor.fetchall()
        return [('id', 'in', [x[0] for x in data])]

    def _data_pm(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        for item in self.browse(cursor, uid, ids, context):
            if item.expedients_ids and not item.bloquejar_pm:
                res[item.id] = max(e.industria_data
                                   for e in item.expedients_ids)
            else:
                res[item.id] = item.data_pm
        return res

    def _get_trams(self, cursor, uid, ids, context=None):
        sql = """
        SELECT e__t.tram_id
        FROM giscedata_at_tram_expedient AS e__t
            LEFT JOIN giscedata_at_tram AS t ON (e__t.tram_id=t.id)
        WHERE e__t.expedient_id IN %s AND NOT t.bloquejar_pm"""
        cursor.execute(sql, (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _get_trams_nobloc(self, cursor, uid, ids, context=None):
        sql = """
        SELECT id
        FROM giscedata_at_tram
        WHERE NOT bloquejar_pm AND id IN %s
        """
        cursor.execute(sql, (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _overwrite_apm(self, cursor, uid, ids, field_name, field_value, args,
                       context=None):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        sql = """
        UPDATE giscedata_at_tram
        SET data_pm=%s
        WHERE id IN %s AND bloquejar_pm
        """
        cursor.execute(sql, (field_value or None, tuple(ids),))

    def _get_trams_nobloc_cini(self, cursor, uid, ids, context=None):
        if self._name == 'giscedata.at.tram':
            cursor.execute("""
                SELECT id
                FROM giscedata_at_tram
                WHERE NOT bloquejar_cini AND id IN %s
                """, (tuple(ids),))
        elif self._name == 'giscedata.at.cables':
            cursor.execute("""
                SELECT tram.id
                FROM giscedata_at_tram AS tram
                LEFT JOIN giscedata_at_cables AS cab ON
                (cab.id = tram.cable)
                WHERE cab.id in %s
                AND not tram.bloquejar_cini
                """, (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    def _cini(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        from cini.models import Linea
        for at in self.browse(cursor, uid, ids):
            linea = Linea()
            try:
                if at.tensio_max_disseny_id:
                    linea.tension = at.tensio_max_disseny_id.tensio
                else:
                    linea.tension = int(at.linia.tensio)
            except (TypeError, ValueError):
                linea.tension = None
            linea.num_conductores = at.conductors
            linea.num_circuitos = at.circuits
            if at.cable:
                linea.seccion = at.cable.seccio
            if at.tipus == 1:
                # Aèria
                linea.despliegue = 'AP'
            elif at.tipus == 2:
                # Subterrània
                linea.despliegue = 'S'
            res[at.id] = str(linea.cini)
        return res

    def _cini_inv(self, cursor, uid, ids, name, value, args, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        cursor.execute("update giscedata_at_tram set cini=%s "
                       "where id in %s and bloquejar_cini",
                       (value or None, tuple(ids),))

    def _default_linia(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('linia_id', False)

    def _get_trams_nobloc_ti(self, cursor, uid, ids, context=None):
        search_params = [
            ('bloquejar_cnmc_tipus_install', '!=', 1),
            ('id', 'in', ids)
        ]
        return self.search(cursor, uid, search_params, context=context)

    def _tipus_instalacio_cnmc_id(self, cursor, uid, ids, field_name, arg,
                                  context=None):
        """
        Calculem de forma automàtica el tipus d'instal·lació
        """
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from tipoinstalacion.models import Linea
        for tram in self.browse(cursor, uid, ids, context):
            if tram.bloquejar_cnmc_tipus_install:
                res[tram.id] = tram.tipus_instalacio_cnmc_id.id
                continue
            ti = Linea()

            try:
                if tram.tensio_max_disseny_id:
                    ti.tension = float(tram.tensio_max_disseny_id.tensio)/1000
                else:
                    ti.tension = float(tram.linia.tensio)/1000
            except (TypeError, ValueError):
                ti.tension = None
            ti.num_conductores = tram.conductors
            ti.num_circuitos = tram.circuits
            if tram.cable:
                ti.seccion = tram.cable.seccio
            if tram.tipus == 1:
                # Aèria
                ti.despliegue = 'AP'
            elif tram.tipus == 2:
                # Subterrània
                ti.despliegue = 'S'
            else:
                continue
            if ti.tipoinstalacion:
                ti_obj = self.pool.get('giscedata.tipus.installacio')
                ti_ids = ti_obj.search(cursor, uid, [
                    ('name', '=', ti.tipoinstalacion)
                ])
                res[tram.id] = ti_ids[0]

        return res

    def _tipus_instalacio_cnmc_id_inv(self, cursor, uid, ids, name, value, args, context=None):
        if not context:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        cursor.execute(
            "update giscedata_at_tram set tipus_instalacio_cnmc_id=%s "
            "where id in %s and bloquejar_cnmc_tipus_install",
            (value or None, tuple(ids),)
        )
        return True

    def _ff_cfo(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        today = datetime.today().strftime('%Y-%m-%d')
        data_inici = context.get('data_inici_cfo', today)
        date_format = "%Y-%m-%d"
        data_inici_obj = datetime.strptime(data_inici, date_format)
        cfg_obj = self.pool.get('res.config')
        cfo_durada = int(cfg_obj.get(cursor, uid, 'giscedata_at_durada_cfo', 6))
        data_aux = data_inici_obj - relativedelta.relativedelta(
            years=cfo_durada)
        for ct in self.read(cursor, uid, ids, ['data_pm']):
            if not ct['data_pm']:
                continue
            data_APM = datetime.strptime(ct['data_pm'], date_format)
            res[ct['id']] = data_aux <= data_APM
        return res

    def _cfo_search(self, cursor, uid, obj, name, args, context=None):
        if not args:
            return []
        today = datetime.today().strftime('%Y-%m-%d')
        data_inici = context.get('data_inici_cfo', today)
        date_format = "%Y-%m-%d"
        data_inici_obj = datetime.strptime(data_inici, date_format)
        cfg_obj = self.pool.get('res.config')
        cfo_durada = int(cfg_obj.get(cursor, uid, 'giscedata_at_durada_cfo', 6))
        data_aux = data_inici_obj - relativedelta.relativedelta(
            years=cfo_durada)
        data_aux = data_aux.strftime('%Y-%m-%d %H:%M:%S')
        if args[0][2]:
            search_params = [
                ('data_pm', '>=', data_aux)
            ]
        else:
            search_params = [
                ('data_pm', '<', data_aux)
            ]
        return search_params

    _columns = {
        'name': fields.integer('Codi'),
        'linia': fields.many2one('giscedata.at.linia', 'Línia Alta Tensió'),
        'origen': fields.char('Origen', size=60),
        'final': fields.char('Final', size=60),
        'baixa': fields.boolean('Tram de Baixa'),
        'data_baixa': fields.date('Data de baixa'),
        'observacions': fields.text('Observacions'),
        'tipus': fields.selection([(1, 'Aèria'), (2, 'Subterrània')], 'Tipus'),
        'longitud_cad': fields.float('Longitud Autocad'),
        'longitud_op': fields.float('Longitud Operador'),
        'cable': fields.many2one('giscedata.at.cables', 'Conductor'),
        'ordre': fields.char('Ordre', size=25),
        'circuits': fields.selection(
            [(1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')],
            'Circuits'
        ),
        'conductors': fields.selection(
            [(1, 'Simplex'), (2, 'Dúplex'), (3, 'Tríplex')],
            'Conductors'
        ),
        'expedients_ids': fields.many2many(
            'giscedata.expedients.expedient', 'giscedata_at_tram_expedient',
            'tram_id', 'expedient_id', 'Expedients'
        ),
        'material': fields.function(
            _material, method=True, type='char', string='Material'
        ),
        'aillament': fields.function(
            _aillament, method=True, type='char', string='Aïllament'
        ),
        'seccio': fields.function(
            _seccio, method=True, type='char', string='Secció'
        ),
        'planol': fields.integer('Plànol'),
        'map': fields.boolean('Map'),
        'mslink': fields.integer('Mslink'),
        'mapid': fields.integer('Mapid'),
        'rev_coberta': fields.char('Revisió Coberta', size=20),
        'rva': fields.char('RVA', size=20),
        'aany': fields.char('Any', size=20),
        'marca': fields.char('Marca', size=20),
        'active': fields.boolean('Actiu'),
        'expedients': fields.function(
            _expedients, method=True, type='char', string='Expedients'
        ),
        'cini': fields.function(
            _cini, type='char', size=8, method=True, store={
                'giscedata.at.tram': (
                    _get_trams_nobloc_cini, [
                        'conductors', 'circuits', 'tipus', 'cable', 'linia',
                        'bloquejar_cini', 'tensio_max_disseny_id'
                    ], 10
                ),
                'giscedata.at.cables': (
                    _get_trams_nobloc_cini, [
                        'seccio'
                    ], 10
                )
            }, fnct_inv=_cini_inv, string='CINI'
        ),
        'bloquejar_cini': fields.boolean('Bloquejar CINI'),
        'data_pm': fields.function(
            _data_pm, type='date', method=True, store={
                'giscedata.expedients.expedient': (
                    _get_trams, ['industria_data'], 10
                ),
                'giscedata.at.tram': (
                    _get_trams_nobloc, ['expedients_ids', 'bloquejar_pm'], 10
                )
            },
            fnct_inv=_overwrite_apm,
            string='Data APM', help=u"Data de l'acta de posada en marxa de la"
                                    u" instal·lació"
        ),
        'cfo': fields.function(_ff_cfo, type='boolean', method=True,
                               string='CFO', fnct_search=_cfo_search),
        'perc_financament': fields.float('% pagat per la companyia'),
        'coeficient': fields.float('Coeficient', digits=(5, 4)),
        'data_industria': fields.function(
            _data_industria, type='date', method=True),
        'grup_empalmes': fields.one2many('giscedata.at.empalmes.grup',
                                         'tram_id', "Grups empalmes",
                                         context={'active_test': False}),
        'tensio_max_disseny': fields.integer('Tensió máxima de disseny'),
        "tensio_max_disseny_id": fields.many2one(
            "giscedata.tensions.tensio",
            string="Tensió máxima de disseny",
            widget="selection",
            select=1,
            domain=[("tipus", "=", "AT")]
        ),
        'propietari': fields.function(
            _propietari, method='True', type='boolean', string='propietari',
            fnct_search=_propietari_srch,
            help=u'marcat si la distribuïdora és la '
                 u'propietaria de la línia del tram'),
        'bloquejar_pm': fields.boolean('Bloquejar APM',
                                       help=u"Si està activat, agafa la data "
                                            u"entrada, si no, la de ,"
                                            u"l'expedient"),
        'cnmc_tipo_instalacion': fields.char(
            u'CNMC Tipus Instal·lació', size=10, readonly=False),
        'bloquejar_cnmc_tipus_install': fields.boolean(
            u'Bloquejar TI', readonly=False,
            help=u"Si està activat, permet modificar manualment el "
                 u"tipus d'instal·lació de la CNMC"
        ),
        '4771_entregada': fields.json('Dades entregades 4771'),
        'tipus_instalacio_cnmc_id': fields.function(
            _tipus_instalacio_cnmc_id,
            method=True,
            string='Tipologia CNMC',
            relation='giscedata.tipus.installacio',
            type='many2one',
            fnct_inv=_tipus_instalacio_cnmc_id_inv,
            store={
                'giscedata.at.tram': (
                    _get_trams_nobloc_ti, [
                        'conductors', 'circuits', 'tipus', 'cable', 'linia',
                        'tensio_max_disseny_id','bloquejar_cnmc_tipus_install'
                    ], 10
                )
            }
        ),
        '4131_entregada_2016': fields.json('Dades entregades 4131 2016'),
        '4666_entregada_2017': fields.json('Dades entregades 4666 2017'),
        '4666_entregada_2018': fields.json('Dades entregades 4666 2018'),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Froçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
        '4666_entregada_2019': fields.json('Dades entregades 4666 2019')
    }

    _defaults = {
        'active': lambda *a: 1,
        'circuits': lambda *a: 1,
        'conductors': lambda *a: 1,
        'baixa': lambda *a: 0,
        'longitud_op': lambda *a: 0,
        'longitud_cad': lambda *a: 0,
        'perc_financament': lambda *a: 100.00,
        'coeficient': lambda *a: 1.0,
        'bloquejar_pm': lambda *a: 0,
        'linia': _default_linia,
        'bloquejar_cini': lambda *a: 0,
        "criteri_regulatori": lambda *a: "criteri",
        'name': _get_nextnumber
    }

    _sql_constraints = [
     ('perc_financament_0_100', 'CHECK(perc_financament between 0 and 100 )',
         "El % de finançament a d'estar entre 0 i 100"),
     ('coeficient_ge', 'CHECK(coeficient > 0 )',
         "El coeficient de longitud ha de ser més gran que 0")]

    _order = "ordre, name, id"

GiscedataAtTram()

# Herència d'expedients


class GiscedataExpedientsExpedient(osv.osv):

    _name = 'giscedata.expedients.expedient'
    _inherit = 'giscedata.expedients.expedient'

    _columns = {
      'trams': fields.many2many(
          'giscedata.at.tram', 'giscedata_at_tram_expedient', 'expedient_id',
          'tram_id', 'Trams'
      ),
    }

GiscedataExpedientsExpedient()


class GiscedataAtPoste(osv.osv):

    _name = "giscedata.at.poste"
    _description = "Poste del suport"

    _columns = {
      'name': fields.char('Codi', size=60),
      'altura': fields.float('Altura'),
      'esforc': fields.integer('Esforç'),
      'material': fields.selection(
          [('pfo', 'Formigó'), ('pfu', 'Fusta'), ('pm', 'Metàl·lic')],
          'Material'
      ),
      'model': fields.char('Model', size=60),
      'fabricant': fields.char('Fabricant', size=60),
      'suports': fields.one2many('giscedata.at.suport', 'poste', 'Suports'),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataAtPoste()


class GiscedataAtArmat(osv.osv):

    _name = "giscedata.at.armat"
    _description = "Armat del suport"

    _columns = {
      'name': fields.char('Codi', size=60),
      'descripcio': fields.char('Descripció', size=60),
      'designacio': fields.char('Designació', size=60),
      'cota_a': fields.float('Cota A'),
      'cota_b': fields.float('Cota B'),
      'cota_c': fields.float('Cota C'),
      'posicio': fields.char('Posició del cap', size=60),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataAtArmat()


class GiscedataAtPlat(osv.osv):

    _name = "giscedata.at.plat"
    _description = "Plats dels Aïlladors"

    _columns = {
      'name': fields.char('Codi', size=60),
      'descripcio': fields.char('Descripció', size=60),
      'material': fields.char('Material', size=60),
      'foto': fields.binary('Foto'),
      'observacions': fields.text('Observacions'),
    }

    _defaults = {

    }

    _orders = "name, id"

GiscedataAtPlat()


class GiscedataAtAillador(osv.osv):

    _name = "giscedata.at.aillador"
    _description = "Aïllador d'un armat"

    _columns = {
      'name': fields.char('Codi', size=60),
      'descripcio': fields.char('Descripció', size=60),
      'plat': fields.many2one('giscedata.at.plat', 'Plat'),
      'n_plats': fields.integer('Número de plats'),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataAtAillador()


class GiscedataAtSuport(osv.osv):

    def _coficient(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for suport in self.browse(cr, uid, ids):
            res[suport.id] = 0.0
        return res

    def _prof_eq(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for suport in self.browse(cr, uid, ids):
            res[suport.id] = 0.0
        return res

    def _resistivitat(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for suport in self.browse(cr, uid, ids):
            res[suport.id] = 0.0
        return res

    def _linia_descripcio(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for suport in self.browse(cr, uid, ids):
            res[suport.id] = suport.linia.descripcio
        return res

    def on_change_baixa(self, cr, uid, ids, baixa, context=None):
        """
        On change of the field baixa fills the value of data_baixa

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Afected ids
        :type ids: list(int)
        :param baixa:
        :type baixa: bool
        :param context: OpenERP context
        :type context: dict
        :return: Value of the data_baixa
        :rtype: dict
        """

        if baixa:
            return {"value": {"data_baixa": time.strftime('%Y-%m-%d')}}
        else:
            return {}

    _name = "giscedata.at.suport"
    _description = "Suports d'Alta Tensió"

    _columns = {
        'name': fields.char('Codi', size=60),
        'autovalvules': fields.boolean('Autovàlvules'),
        'seccionador': fields.boolean('Seccionador'),
        'observacions': fields.text('Observacions'),
        'poste': fields.many2one('giscedata.at.poste', 'Poste'),
        'armat': fields.many2one('giscedata.at.armat', 'Armat'),
        'dist_piques_1': fields.float('Distància entre piques 1'),
        'dist_piques_2': fields.float('Distància entre piques 2'),
        'dist_piques_3': fields.float('Distància entre piques 3'),
        'resistivitat_a': fields.float('Resistivitat A'),
        'resistivitat_b': fields.float('Resistivitat B'),
        'resistivitat_c': fields.float('Resistivitat C'),
        'prof_eq1': fields.function(_prof_eq, method=True),
        'prof_eq2': fields.function(_prof_eq, method=True),
        'prof_eq3': fields.function(_prof_eq, method=True),
        'resistivitat_data': fields.datetime('Data de la resistivitat'),
        'resistencia_1': fields.float('Resistencia 1'),
        'resistencia_2': fields.float('Resistencia 2'),
        'resistencia_3': fields.float('Resistencia 3'),
        'cof_1': fields.function(_coficient, method=True),
        'cof_2': fields.function(_coficient, method=True),
        'cof_3': fields.function(_coficient, method=True),
        'resistivitat_1': fields.function(_resistivitat, method=True),
        'resistivitat_2': fields.function(_resistivitat, method=True),
        'resistivitat_3': fields.function(_resistivitat, method=True),
        'estat_terreny': fields.char('Estat del terreny', size=60),
        'malla_equipotencial': fields.boolean('Malla equipotencial'),
        'placa_perill': fields.boolean('Placa de perill'),
        'numeracio': fields.boolean('Numeració Suports'),
        'antiescala': fields.boolean('Antiescala'),
        'tpc_contacte_adm': fields.float('Contacte'),
        'tpc_pas_adm': fields.float('Pas'),
        'ailladors': fields.one2many(
            'giscedata.at.suport.aillador', 'suport', 'Aïlladors'
        ),
        'mesures': fields.one2many(
          'giscedata.at.historicmesures', 'suport', 'Mesures'
        ),
        'linia': fields.many2one('giscedata.at.linia', 'Línia'),
        'creua_linia': fields.boolean('Creua Línia AT, BT, TF...'),
        'frequentat': fields.selection([
            ('no_frequentat', 'No freqüentat'),
            ('no_calcat', 'Freqüentat sense calçat'),
            ('calcat', 'Freqüentat amb calçat')
        ], 'Freqüentat'),
        'centre_transformador': fields.boolean('Centre transformador'),
        'creua_carretera': fields.boolean('Creua Carretera'),
        'foto_1': fields.binary('Foto 1'),
        'foto_2': fields.binary('Foto 2'),
        'foto_3': fields.binary('Foto 3'),
        'linia_descripcio': fields.function(
            _linia_descripcio,
            type='char',
            method=True,
            string="Nom de la línia"
        ),
        'active': fields.boolean('Actiu'),
        "anti_picot": fields.selection(
            [
                ("ferro", "Malla ferro"),
                ("plastic", "Malla plàstic"),
                ("moixo", "Moixó")
            ],
            "Malla anti-picot"
        ),
        "bola_senyalitzacio": fields.boolean("Bola senyalitzacio"),
        "anti_colisio": fields.boolean("Anella anticol·lisió"),
        "salva_ocells": fields.boolean("Salva ocells"),
        "seguretat_reforcada": fields.boolean("Seguretat reforçada"),
        "pas_per_zones": fields.boolean("Pas per zones"),
        "avifauna": fields.selection(
            [
                ("no", "No"),
                ("si", "Sí"),
                ("parcial", "Parcialment"),
            ],
            "Avifauna"
         ),
        'baixa': fields.boolean('Baixa'),
        'data_apm': fields.date('Data APM'),
        'data_baixa': fields.date('Data baixa'),
        'tram_at_ids': fields.many2many(
            'giscedata.at.tram', 'suports_trams_at_rel', 'suport_id',
            'tram_at_id', 'Trams AT'
        ),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Froçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
        'tirant': fields.boolean('Tirant'),
        'tornapuntes': fields.boolean('Tornapuntes')
    }

    _defaults = {
        'tirant': lambda *a: False,
        'tornapuntes': lambda *a: False,
        'active': lambda *a: 1,
        'frequentat': lambda *a: 'no_frequentat',
        'baixa': lambda *a: 0,
        'data_apm': lambda *a: time.strftime('%Y-%m-%d'),
        "criteri_regulatori": lambda *a: "criteri"
    }

    _order = "name, id"


GiscedataAtSuport()


class GiscedataAtSuportAillador(osv.osv):

    def onchange_aillador(self, cr, uid, ids, qty):
        if qty < 1:
            return {'value': {'name': 1}}
        else:
            return {'value': {'name': qty}}

    _name = "giscedata.at.suport.aillador"
    _description = "Aïlladors a cada suport"

    _columns = {
      'suport': fields.many2one(
          'giscedata.at.suport', 'Suport', ondelete='restrict'
      ),
      'aillador': fields.many2one('giscedata.at.aillador', 'Aïllador'),
      'name': fields.integer('Quantitat'),
    }

    _defaults = {
    }

    _order = "id"

GiscedataAtSuportAillador()


class GiscedataAtHistoricmesures(osv.osv):

    _name = "giscedata.at.historicmesures"
    _description = "Històric de les mesures"

    _columns = {
      'name': fields.datetime('Data'),
      'suport': fields.many2one(
          'giscedata.at.suport', 'Suport', ondelete='restrict'
      ),
      'trimestre': fields.char('Trimestre de revisió', size=60),
      'ferramenta': fields.float('Ferramenta'),
      'autovalvules': fields.float('Autovàlvules'),
      'intensitat_prova': fields.float('Intensitat de prova'),
      'tpc_pas_exterior': fields.float('Tensió pas exteriror'),
      'tpc_contacte': fields.float('Tensió de contacte'),
      'codi_suport_at': fields.char('Codi Suport-Linia', size=60),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataAtHistoricmesures()


class GiscedataAtTramsdgn(osv.osv):

    _name = "giscedata.at.tramsdgn"
    _description = "Vista pels mapes"
    _auto = False

    _columns = {
      'id': fields.integer('Id', readonly=True),
      'name': fields.char('Name', size=60, readonly=True),
      'baixa': fields.boolean('Baixa', readonly=True),
      'ordre': fields.char('Ordre', size=25, readonly=True),
      'linia': fields.char('Línia', size=25, readonly=True),
      'origen': fields.char('Origen', size=60, readonly=True),
      'final': fields.char('Final', size=60, readonly=True),
      'longitud_cad': fields.float('Longitud CAD', readonly=True),
      'planol': fields.integer('Plànol', readonly=True),
      'cable_name': fields.char('Cable', size=25, readonly=True),
      'cable_material': fields.char('Material Cable', size=25, readonly=True),
    }

    def init(self, cr):
        sql = """
        CREATE OR REPLACE VIEW giscedata_at_tramsdgn AS (
            SELECT t.id, t.name, t.baixa, t.ordre, l.name AS linia, t.origen,
                t.final, t.longitud_cad, t.planol, c.name AS cable_name,
                m.name AS cable_material
            FROM giscedata_at_tram t, giscedata_at_linia l,
                giscedata_at_cables c, giscedata_at_material m
            WHERE t.linia = l.id AND t.cable = c.id and c.material = m.id
            )
        """
        cr.execute(sql)

GiscedataAtTramsdgn()


class GiscedataAtEmpalmesGrup(osv.osv):

    _name = "giscedata.at.empalmes.grup"
    _description = "Empalmes Alta Tensió"

    def _get_nextnumber(self, cursor, uid, context=None):
        """
        Returns the next node numbers from  sequence giscedata.at.empalmes.grup
        :param cursor: Database Cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP context
        :type context: dic[str,Any]
        :return: Next giscedata.at.empalmes.grup name value
        :rtype: int
        """

        return self.pool.get('ir.sequence').get(
            cursor, uid, 'giscedata.at.empalmes.grup'
        )

    def _linia(self, cr, uid, ids, field_name, ar, context=None):
        if context is None:
            context = {}
        res = {}
        g_empalmes = self.browse(cr, uid, ids)
        for grup in g_empalmes:
            if grup.tram_id and grup.tram_id.linia:
                lat_id = grup.tram_id.linia.id
                lat_desc = grup.tram_id.linia.descripcio
                municipi_id = grup.tram_id.linia.municipi.id
                poblacio_id = grup.tram_id.linia.poblacio.id
                codi = '%s-%s-%s' % (grup.tram_id.linia.name or '',
                                     grup.tram_id.name or '',
                                     grup.name or '')
                origen = grup.tram_id.origen
                final = grup.tram_id.final
            else:
                lat_id = False
                lat_desc = False
                municipi_id = False
                poblacio_id = False
                codi = False
                origen = False
                final = False

            res[grup.id] = {
                'lat_id': lat_id,
                'lat_desc': lat_desc,
                'municipi_id': municipi_id,
                'poblacio_id': poblacio_id,
                'codi': codi,
                'origen': origen,
                'final': final
            }

        return res

    def _historic(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        g_empalmes = self.browse(cr, uid, ids)
        emp_obj = self.pool.get('giscedata.at.empalmes.empalme')
        avui = datetime.strftime(date.today(), '%Y-%m-%d') + ' 24:00:00'
        for grup in g_empalmes:
            search_params = [
                ('grup_id', '=', grup.id),
                ('data_baixa', '<=', avui)
            ]
            res[grup.id] = emp_obj.search(
                cr, uid, search_params, context={'active_test': False})
        return res

    def _coord(self, cr, uid, ids, field_name, ar, context=None):
        if context is None:
            context = {}
        res = dict([(i, dict([('coord_x', False), ('coord_y', False)]))
                   for i in ids])
        gis = self.pool.get('giscegis.blocs.empalmes.grup')
        grup_gis_ids = gis.search(cr, uid, [('grup.id', 'in', ids)])
        grup_gis = gis.browse(cr, uid, grup_gis_ids)
        for grup in grup_gis:
            if grup.vertex:
                res[grup.grup.id] = {'coord_x': grup.vertex.x,
                                     'coord_y': grup.vertex.y}
        return res

    def _municipi_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        search_params = [
            ('tram_id.linia.municipi.name', 'ilike', args[0][2])
        ]
        res = self.search(cr, uid, search_params)
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    def _lat_name_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        search_params = [
            ('tram_id.linia.name', 'ilike', args[0][2])
        ]
        res = self.search(cr, uid, search_params)
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    def _lat_desc_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        search_params = [
            ('tram_id.linia.descripcio', 'ilike', args[0][2])
        ]
        res = self.search(cr, uid, search_params)
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    _columns = {
        'name': fields.char('Nom', size=16, readonly=True),
        'codi': fields.function(
            _linia, method=True, type='char', string='Codi',
            multi='linia', size=60
        ),
        'active': fields.boolean('Actiu'),
        'data_alta': fields.datetime('Data d\'alta'),
        'data_baixa': fields.datetime('Data de baixa'),
        'observacions': fields.text('Observacions'),
        'tram_id': fields.many2one('giscedata.at.tram', 'Tram'),
        'origen': fields.function(_linia, method=True, type='char',
                                  string='Origen', multi='linia', size=60),
        'final': fields.function(
            _linia, method=True, type='char', string='Final',
            multi='linia', size=60
        ),
        'lat_id': fields.function(
            _linia, method=True, type='many2one',
            relation='giscedata.at.linia', string='Línia',
            multi='linia', fnct_search=_lat_name_search
        ),
        'lat_desc': fields.function(
            _linia, method=True, type='char', size=60, string='Nom línia',
            multi='linia', fnct_search=_lat_desc_search
        ),
        'municipi_id': fields.function(
            _linia, method=True, type='many2one', relation='res.municipi',
            string='Municipi', multi='linia', fnct_search=_municipi_search
        ),
        'poblacio_id': fields.function(
            _linia, method=True, type='many2one', relation='res.poblacio',
            string='Poblacio', multi='linia'
        ),
        'carrer': fields.text('Carrer'),
        'coord_x': fields.function(
            _coord, method=True, type='float', digits=(7, 6),
            string='X', multi='coord'
        ),
        'coord_y': fields.function(
            _coord, method=True, type='float', digits=(7, 6),
            string='Y', multi='coord'
        ),
        'empalmes': fields.one2many(
            'giscedata.at.empalmes.empalme', 'grup_id', 'Empalmes'
        ),
        'historic': fields.function(
            _historic, method=True, type='one2many',
            relation='giscedata.at.empalmes.empalme',
            string="Històric d'empalmes", context={'active_test': False}
        ),
        'num_obra': fields.char("Núm. obra", size=16),
        'num_encarrec': fields.char("Núm. encàrrec", size=16)
    }

    _defaults = {
        'active': lambda *a: 1,
        'name': _get_nextnumber
    }

GiscedataAtEmpalmesGrup()


class GiscedataAtEmpalmesEmpalme(osv.osv):

    _name = "giscedata.at.empalmes.empalme"

    def _product(self, cr, uid, ids, field_name, ar, context=None):
        if context is None:
            context = {}
        res = {}
        empalmes = self.browse(cr, uid, ids, context=context)
        for emp in empalmes:
            if emp.stock_id and emp.stock_id.product_id:
                prod_id = emp.stock_id.product_id.id
                prod_name = emp.stock_id.product_id.name
                res[emp.id] = (prod_id, prod_name)
            else:
                res[emp.id] = False
        return res

    def _partner(self, cr, uid, ids, field_name, ar, context=None):
        if context is None:
            context = {}
        res = {}
        empalmes = self.browse(cr, uid, ids)
        for emp in empalmes:
            if emp.inst_id:
                res[emp.id] = emp.inst_id.vat
            else:
                res[emp.id] = False
        return res

    def _company(self, cr, uid, ids, field_name, ar, context=None):
        if context is None:
            context = {}
        res = {}
        empalmes = self.browse(cr, uid, ids)
        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cr, uid, uid)
        for emp in empalmes:
            try:
                res[emp.id] = user.company_id.partner_id.id
            except:
                res[emp.id] = False
        return res

    def _cable(self, cr, uid, ids, field_name, ar, context=None):
        if context is None:
            context = {}
        res = {}
        empalmes = self.browse(cr, uid, ids)
        for emp in empalmes:
            aill1 = emp.cable1_id and emp.cable1_id.aillament or False
            aill2 = emp.cable2_id and emp.cable2_id.aillament or False
            res[emp.id] = {'aillament1': aill1,
                           'aillament2': aill2}
        return res

    def onchange_grup(self, cr, uid, ids, grup_id):
        res = {'value': {}}
        if grup_id:
            grup_obj = self.pool.get('giscedata.at.empalmes.grup')
            grup = grup_obj.browse(cr, uid, grup_id)
            if grup.tram_id and grup.tram_id.cable:
                res = {'value': {
                                  'cable1_id': grup.tram_id.cable.id,
                                  'cable2_id': grup.tram_id.cable.id,
                                  'aillament1': grup.tram_id.cable.aillament,
                                  'aillament2': grup.tram_id.cable.aillament
                                }}
        return res

    def onchange_cable_1(self, cr, uid, ids, cable_id):
        res = {'value': {}}
        if cable_id:
            cable_obj = self.pool.get('giscedata.at.cables')
            cable = cable_obj.browse(cr, uid, cable_id)
            res = {'value': {
                                'aillament1': cable.aillament
                            }}
        return res

    def onchange_cable_2(self, cr, uid, ids, cable_id):
        res = {'value': {}}
        if cable_id:
            cable_obj = self.pool.get('giscedata.at.cables')
            cable = cable_obj.browse(cr, uid, cable_id)
            res = {'value': {
                                'aillament2': cable.aillament
                            }}
        return res

    def _default_grup(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('grup_id', False)

    def _default_data_alta(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('data_alta', False)

    def _default_data_baixa(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('data_baixa', False)

    def _default_cable(self, cursor, uid, context=None):
        if context is None:
            context = {}
        val = False
        grup_id = context.get('grup_id', False)
        if grup_id:
            grup_obj = self.pool.get('giscedata.at.empalmes.grup')
            tram = grup_obj.browse(cursor, uid, grup_id).tram_id
            if tram and tram.cable:
                val = tram.cable.id
        return val

    def _default_aillament(self, cursor, uid, context=None):
        if context is None:
            context = {}
        val = False
        grup_id = context.get('grup_id', False)
        if grup_id:
            grup_obj = self.pool.get('giscedata.at.empalmes.grup')
            tram = grup_obj.browse(cursor, uid, grup_id).tram_id
            if tram and tram.cable:
                val = tram.cable.aillament
        return val

    def _default_company(self, cursor, uid, context=None):
        if context is None:
            context = {}
        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid)
        if user.company_id and user.company_id.partner_id:
            val = user.company_id.partner_id.id
        return val

    def _cif_inst_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        search_params = [
            ('inst_id.vat', 'ilike', args[0][2])
        ]
        res = self.search(cr, uid, search_params)
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    def _prod_name_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        search_params = [
            ('stock_id.product_id.name', 'ilike', args[0][2])
        ]
        res = self.search(cr, uid, search_params)
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    _columns = {
        'grup_id': fields.many2one(
            'giscedata.at.empalmes.grup', 'Grup d\'empalmes'
        ),
        'active': fields.boolean('Actiu'),
        'linia': fields.selection(
            [
                ('R', 'L1 (R)'),
                ('S', 'L2 (S)'),
                ('T', 'L3 (T)')
            ],
            'Línia'),
        'distancia': fields.integer(
            'Distància [m]',
            help="Distància de l'origen del grup d'empalmes a l'empalme "
                 "d'aquesta fase"
        ),
        'cable1_id': fields.many2one(
            'giscedata.at.cables', "Cable d'entrada",
            help="Cable del tram associat a la primera punta de l'empalme"
        ),
        'cable2_id': fields.many2one(
            'giscedata.at.cables', "Cable de sortida",
            help="Cable del tram associat a la segona punta de l'empalme"
        ),
        'aillament1': fields.function(
            _cable, method=True, type='char', size=10,
            string="Aïllament entrada", multi="aillament"
        ),
        'aillament2': fields.function(_cable, method=True, type='char',
                                      size=10, string="Aïllament sortida",
                                      multi="aillament"),
        'stock_id': fields.many2one('stock.production.lot', "Número de sèrie"),
        'product_id': fields.function(
            _product, method=True, type='many2one', relation="product.product",
            string='Producte', fnct_search=_prod_name_search
        ),
        'inst_id': fields.many2one('res.partner', 'Empresa instal·ladora'),
        'cif_inst': fields.function(
            _partner, method=True, type='char', size=32, string='CIF',
            help="CIF de l'empresa instal·ladora", fnct_search=_cif_inst_search
        ),
        'operaris_ids': fields.many2many(
            'res.partner', 'giscedata_at_empalmes_empalme_partner',
            'partner_id', 'empalme_id', 'Operaris externs'
        ),
        'operari_id': fields.many2one('res.partner.address',
                                      'Operari companyia'),
        'data_alta': fields.datetime('Data de instal·lació'),
        'data_baixa': fields.datetime(
            'Data de baixa',
            help="Data en que es va eliminiar l'empalme"
        ),
        'const_id': fields.many2one(
            'res.partner', 'Empresa constructura',
            help="Empresa que obre i tanca la rasa"
        ),
        'observacions': fields.text('Observacions'),
        'company_id': fields.function(
            _company, method=True, type='many2one',
            relation="res.partner", string="Company"
        ),
    }

    _defaults = {
        'active': lambda *a: 1,
        'grup_id': _default_grup,
        'data_alta': _default_data_alta,
        'data_baixa': _default_data_baixa,
        'cable1_id': _default_cable,
        'cable2_id': _default_cable,
        'aillament1': _default_aillament,
        'aillament2': _default_aillament,
        'company_id': _default_company,
    }

GiscedataAtEmpalmesEmpalme()
