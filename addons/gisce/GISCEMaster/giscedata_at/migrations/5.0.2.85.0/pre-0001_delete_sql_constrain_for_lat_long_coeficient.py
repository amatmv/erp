# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Deleting sql constraint coeficient_ge_1 to be able to update it.')
    cursor.execute(
        """
        ALTER TABLE giscedata_at_tram RENAME COLUMN  tensio_max_disseny_id to tensio_max_disseny_id_old;
        ALTER TABLE giscedata_at_tram ADD COLUMN  tensio_max_disseny_id INTEGER;
        UPDATE giscedata_at_tram SET tensio_max_disseny_id = tensio_max_disseny_id_old::INT;
        """
    )


def down(cursor, installed_version):
    pass

migrate = up
