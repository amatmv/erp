# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info('Setting UP the Trams AT sequence for the field NAME.')

    sql = """
        UPDATE ir_sequence AS seq 
        SET number_next = data.num
        FROM (
          SELECT coalesce(max(tat.name::int),0)+1 AS num,
                 seq.id AS id
          FROM giscedata_at_tram tat, ir_sequence seq
          WHERE seq.name='Numeració Trams AT'
          AND seq.code='giscedata.at.tram'
          GROUP BY seq.id
        ) AS data
        WHERE seq.id=data.id
    """
    cursor.execute(sql)

    logger.info('Tram AT NAME sequence set up with the current value')


def down(cursor, installed_version):
    pass


migrate = up