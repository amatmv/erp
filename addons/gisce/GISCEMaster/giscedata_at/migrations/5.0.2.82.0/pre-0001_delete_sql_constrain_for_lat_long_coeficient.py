# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Deleting sql constraint coeficient_ge_1 to be able to update it.')
    cursor.execute(
        """
        ALTER TABLE giscedata_at_tram 
        DROP CONSTRAINT giscedata_at_tram_coeficient_ge_1
        """
    )

def down(cursor, installed_version):
    pass

migrate = up
