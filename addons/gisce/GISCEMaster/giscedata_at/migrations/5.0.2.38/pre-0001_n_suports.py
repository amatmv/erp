# -*- coding: utf-8 -*-
"""Renombrar les columnes de n_suports a _old
"""

import netsvc

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         "Migrant els n_suports_formigo,"
                         "n_suports_fusta, n_suports_metallics")

    #Eliminar les columnes
    cursor.execute("""alter table
                   giscedata_at_linia drop column n_suports_formigo""")
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                            u"n_suports_formigo realitzat correctament")

    cursor.execute("""alter table
                   giscedata_at_linia drop column n_suports_fusta""")
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                             u"n_suports_fusta realitzat correctament")

    cursor.execute("""alter table
                   giscedata_at_linia drop column n_suports_metallics""")
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                             u"n_suports_metallics realitzat correctament")
