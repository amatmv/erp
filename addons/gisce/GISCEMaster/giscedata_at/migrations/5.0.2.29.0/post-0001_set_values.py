# -*- coding: utf-8 -*-
import netsvc
import pooler
from fuzzywuzzy import process
import unicodedata

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                            "Migrant poblacions dels lats...")
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    mun_obj = pool.get('res.municipi')
    pob_obj = pool.get('res.poblacio')
    lat_obj = pool.get('giscedata.at.linia')
    cursor.execute("""select count(column_name) as existeix
                        FROM information_schema.columns
                        WHERE table_name='giscedata_at_linia'
                         and column_name='poblacio_char'""")
    existeix_columna = cursor.dictfetchall()[0]['existeix']
    if existeix_columna:
        logger.notifyChannel('migration', netsvc.LOG_INFO,
                            u"Ja està feta la migració dels lats...")
    else:
        munis = {}
        mun_ids = mun_obj.search(cursor, uid, [])
        ine_mun_desc = ('00000', '99999')
        id_mun_desc = None
        for mun in mun_obj.read(cursor, uid, mun_ids, ['name', 'ine']):
            munis[mun['id']] = mun['name']
            if mun['ine'] in ine_mun_desc:
                id_mun_desc = mun['id']
        if not id_mun_desc:
            # Si no existeix el municipi desconnegut es crea
            cursor.execute("""insert into res_municipi (ine, name)
                              values ('00000', 'DESCON.')""")
            cursor.execute("""select id from res_municipi where ine = '00000'""")
            id_mun_desc = cursor.dictfetchall()[0]['id']
            munis[id_mun_desc] = 'DESCON.'
        existing_pobs = {}
        pob_ids = pob_obj.search(cursor, uid, [])
        for pob in pob_obj.read(cursor, uid, pob_ids, ['name']):
            existing_pobs[strip_accents(pob['name'])] = pob['id']

        cursor.execute("""SELECT c.id, c.municipi, c.poblacio_char
                          FROM giscedata_at_linia c""")
        for lat in cursor.dictfetchall():
            if not lat['poblacio_char']:
                if not lat['municipi']:
                    continue
                # Localitzar una població que s'anomeni com el municipi
                # Si no hi és es crea
                pobs_like = pob_obj.search(cursor, uid,
                                [('municipi_id', '=', lat['municipi'])])
                pobs = dict([(p['name'], p['id']) for p in
                                  pob_obj.read(cursor, uid, pobs_like, ['name'])])
                res = process.extractOne(munis[lat['municipi']], pobs.keys())
                if res and res[1] >= 90:
                    id_poblacio = pobs[res[0]]
                else:
                    pob_name = munis[lat['municipi']]
                    id_poblacio = pob_obj.create(cursor, uid,
                                    {'name': pob_name,
                                     'municipi_id': lat['municipi']})
                    existing_pobs[strip_accents(pob_name)] = id_poblacio
            else:
                res = process.extractOne(strip_accents(lat['poblacio_char']),
                                                   existing_pobs.keys()) or ('', 0)
                if res and res[1] >= 95:
                    id_poblacio = existing_pobs[res[0]]
                else:
                    pob_name = lat['poblacio_char']
                    if lat['municipi']:
                        id_poblacio = pob_obj.create(cursor, uid,
                                            {'name': pob_name,
                                             'municipi_id': lat['municipi']})
                    else:
                        id_poblacio = pob_obj.create(cursor, uid,
                                            {'name': pob_name,
                                             'municipi_id': munis[id_mun_desc]})
                    existing_pobs[strip_accents(pob_name)] = id_poblacio
            lat_obj.write(cursor, uid, lat['id'], {'poblacio': id_poblacio})

def strip_accents(s):
    try:
        return ''.join((c for c in unicodedata.normalize('NFD', s)
                                   if unicodedata.category(c) != 'Mn'))
    except Exception, e:
        return s