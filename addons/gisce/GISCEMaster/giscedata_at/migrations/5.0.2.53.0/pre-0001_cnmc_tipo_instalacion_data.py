# -*- coding: utf-8 -*-
import netsvc
import pooler


def migrate(cursor, installed_version):
    logger = netsvc.Logger()

    uid = 1
    pool = pooler.get_pool(cursor.dbname)

    cnmc_obj = pool.get('giscedata_cnmc.tipo_instalacion')

    if not cnmc_obj:
        logger.notifyChannel('migrations', netsvc.LOG_INFO,
                             u"El model giscedata_cnmc.tipo_instalacion no "
                             u"existeix. No cal migrar")
        return

    sql1 = "DELETE FROM giscedata_cnmc_tipo_instalacion"

    sql2 = (u"DELETE FROM ir_model_data WHERE "
            u"model='giscedata_cnmc.tipo_instalacion'")

    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         u"Eliminar dades existents de "
                         u"giscedata_cnmc.tipo_instalacion")

    cursor.execute(sql1)

    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         u"Taula 'giscedata_cnmc_tipo_instalacion' esborrada")

    cursor.execute(sql2)

    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         u"Eliminar dades existents de ir_model_data model "
                         u"giscedata_cnmc.tipo_instalacion")