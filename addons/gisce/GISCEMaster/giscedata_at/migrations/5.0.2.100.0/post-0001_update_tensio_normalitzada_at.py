# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info('Setting UP the Lineas AT normalized tension.')

    sql = """
        UPDATE giscedata_at_linia lat
          SET tensio_id = tt.id
        FROM giscedata_tensions_tensio tt 
        WHERE lat.tensio = tt.tensio;
    """
    cursor.execute(sql)
    logger.info('Linea AT normalized tension fixed')

    sql = """
        SELECT lat.tensio, lat.descripcio, lat."name"
        FROM giscedata_at_linia lat 
        WHERE lat.tensio NOT IN (
            SELECT tensio 
            FROM giscedata_tensions_tensio 
            WHERE tipus = 'AT'
        )
    """
    cursor.execute(sql)
    for linia in cursor.fetchall():
        logger.info("La linia amb nom {} {}, te una tensió de {} i no s'ha pogut normalitzar.").format(linia[2], linia[1], linia[0])


def down(cursor, installed_version):
    pass


migrate = up
