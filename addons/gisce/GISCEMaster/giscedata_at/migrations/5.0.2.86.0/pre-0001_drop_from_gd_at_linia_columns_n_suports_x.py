# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info("Droping from table giscedata_at_linia columns " +
                "n_suports_metallics, n_suports_fusta and n_suports_formigo " +
                "to force recalculate the value of this fields.")
    cursor.execute(
        """
        ALTER TABLE giscedata_at_linia 
          DROP COLUMN n_suports_metallics, 
          DROP COLUMN n_suports_fusta, 
          DROP COLUMN n_suports_formigo;
        """
    )


def down(cursor, installed_version):
    pass


migrate = up
