# coding=utf-8
from oopgrade import oopgrade


def up(cursor, installed_version):
    if not installed_version:
        return
    oopgrade.rename_columns(cursor, {
        'giscedata_at_tram': [('cini', 'cini_pre_auto_at')]
    })


def down(cursor):
    oopgrade.rename_columns(cursor, {
        'giscedata_at_tram': [('cini_pre_auto_at', 'cini')]
    })


migrate = up
