# -*- coding: utf-8 -*-
"""Renombrar les columnes de n_suports a _old
"""

import netsvc

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         "Omplint la tensió máxima disseny dels trams AT")

    #Omplim tensio_max_disseny amb la tenisó de la línia
    cursor.execute("""update giscedata_at_tram 
                      set tensio_max_disseny=l.tensio from giscedata_at_linia l
                      where linia=l.id""")
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                            u"tensio_max_disseny realitzat correctament")

