import logging
import pooler


def migrate(cursor, installed_version):
    """
    Migration script to update the tension field

    :param cursor: Database cursor
    :param installed_version: Installed version
    :type installed_version: str
    :return:
    """

    logger = logging.getLogger('openerp.migration')

    pool = pooler.get_pool(cursor.dbname)
    tensions_obj = pool.get("giscedata.tensions.tensio")
    trams_obj = pool.get("giscedata.at.tram")

    search_params = [("tipus", "=", "AT")]
    ids_v_norm = tensions_obj.search(
        cursor, 1, search_params, 0, 0, False, {'active_test': False})

    tensions_normalitzades = tensions_obj.read(
        cursor, 1, ids_v_norm, ["tensio"])

    map_id_tensio = dict.fromkeys(ids_v_norm, False)
    for t in tensions_normalitzades:
        map_id_tensio[t["id"]] = t["tensio"]

    logger.info("tensions normalitzades")
    logger.info("----------------------")

    for t in sorted(tensions_normalitzades):
        logger.info(t["tensio"])
    logger.info("\n")
    tensions_trams = set()
    ids_trams = trams_obj.search(cursor, 1, [])
    trams = trams_obj.read(cursor, 1, ids_trams, ["tensio_max_disseny"])
    for tram in trams:
        tensions_trams.add(tram["tensio_max_disseny"])

    map_tensions = dict.fromkeys(list(tensions_trams), False)
    for t in map_tensions.keys():
        if t and tensions_normalitzades:
            millor = False
            millor_dist = abs(t - tensions_normalitzades[0]["tensio"])
            for tn in tensions_normalitzades:
                if abs(t - tn["tensio"]) <= millor_dist:
                    millor = tn["id"]
                    millor_dist = abs(t - tn["tensio"])
                map_tensions[t] = millor

    for elem in sorted(map_tensions.items()):
        if elem[1]:
            logger.info("{}->{}".format(elem[0], map_id_tensio[elem[1]]))
        else:
            logger.info("{}->{}".format(elem[0], elem[1]))
    for tram_id in ids_trams:
        logger.info("id:{} tensio:{} nova_tensio_id:{}".format(tram_id,tram["tensio_max_disseny"], map_tensions[tram["tensio_max_disseny"]]))
        tram = trams_obj.read(cursor, 1, tram_id, ["tensio_max_disseny"])
        if tram["tensio_max_disseny"]:
            write_data = {
                "tensio_max_disseny_id": map_tensions[tram["tensio_max_disseny"]]
            }
            trams_obj.write(cursor, 1, tram_id, write_data)

up = migrate
