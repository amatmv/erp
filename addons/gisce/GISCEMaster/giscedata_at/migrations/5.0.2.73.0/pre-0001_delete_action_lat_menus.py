# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Deleting menu_at_manteniment_suport action')
    cursor.execute(
        """
        DELETE FROM ir_values
        WHERE
            key = 'action' AND
            key2 = 'tree_but_open' AND
            res_id IN (
                SELECT res_id FROM ir_model_data
                WHERE name = 'menu_at_manteniment_suport'
        )"""
    )

    logger.info('Deleting menu_at_manteniment_empalmes action')
    cursor.execute(
        """
        DELETE FROM ir_values
        WHERE
            key = 'action' AND
            key2 = 'tree_but_open' AND
            res_id IN (
                SELECT res_id FROM ir_model_data
                WHERE name = 'menu_at_manteniment_empalmes'
        )"""
    )

    logger.info('Deleting menu_at_manteniment_cables action')
    cursor.execute(
        """
        DELETE FROM ir_values
        WHERE
            key = 'action' AND
            key2 = 'tree_but_open' AND
            res_id IN (
                SELECT res_id FROM ir_model_data
                WHERE name = 'menu_at_manteniment_cables'
        )"""
    )

    logger.info('Deleting menu_at_manteniment_trams action')
    cursor.execute(
        """
        DELETE FROM ir_values
        WHERE
            key = 'action' AND
            key2 = 'tree_but_open' AND
            res_id IN (
                SELECT res_id FROM ir_model_data
                WHERE name = 'menu_at_manteniment_trams'
        )"""
    )


def down(cursor, installed_version):
    pass

migrate = up
