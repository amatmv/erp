# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Alta Tensió",
    "description": """Alta Tensió


 $Id: __terp__.py 1247 2008-01-21 08:53:07Z raul $""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Alta Tensió",
    "depends":[
        "base_extended",
        "base_extended_distri",
        "giscedata_expedients",
        "stock",
        "giscedata_administracio_publica_cnmc_distri",
        "giscedata_tensions"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_at_demo.xml",
        "giscedata_at_sequence_demo.xml"
    ],
    "update_xml":[
        "giscedata_at_view_base.xml",
        "giscedata_at_wizard.xml",
        "giscedata_cnmc_tipoinstalacion_data.xml",
        "giscedata_at_view.xml",
        "giscedata_at_report.xml",
        "ir.model.access.csv",
        "giscedata_at_sequence.xml",
        "security/giscedata_at_security.xml",
        "security/ir.model.access.csv",
        "giscedata_tipus_installacio_at_data.xml"
    ],
    "active": False,
    "installable": True
}
