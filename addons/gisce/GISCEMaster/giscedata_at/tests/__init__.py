# coding=utf-8
from __future__ import unicode_literals
from dateutil.relativedelta import relativedelta
from datetime import datetime

from destral import testing
from destral.transaction import Transaction

from expects import *


class TestATs(testing.OOTestCase):
    def test_field_cfo_returns_false_if_not_data_pm(self):
        """
        Fem que si la no hi ha data APM no es pot calcular el CFO i
        per tant el camp cfo ha de ser true
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            ats_obj = self.openerp.pool.get('giscedata.at.tram')

            at_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_at', 'at_tram_0001'
            )[1]
            at = ats_obj.browse(cursor, uid, at_id)
            expect(at.cfo).to(be_false)

    def test_field_cfo_returns_true_under_six_years(self):
        """
        Fem que si la data actual és inferior a 6 anys la línia  té
        CFO i per tant el camp cfo ha de ser true
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            ats_obj = self.openerp.pool.get('giscedata.at.tram')

            at_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_at', 'at_tram_0001'
            )[1]

            dt = (datetime.now() - relativedelta(years=5)).strftime('%Y-%m-%d')

            ats_obj.write(cursor, uid, [at_id], {'bloquejar_pm': 1})
            ats_obj.write(cursor, uid, [at_id], {'data_pm': dt})

            at = ats_obj.browse(cursor, uid, at_id)
            expect(at.cfo).to(be_true)

    def test_field_cfo_returns_false_over_six_years(self):
        """
        Fem que si la data actual és superior a 6 anys la línia NO té
        CFO i per tant el camp cfo ha de ser false
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            ats_obj = self.openerp.pool.get('giscedata.at.tram')

            at_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_at', 'at_tram_0001'
            )[1]

            dt = (datetime.now() - relativedelta(years=7)).strftime('%Y-%m-%d')

            ats_obj.write(cursor, uid, [at_id], {'bloquejar_pm': 1})
            ats_obj.write(cursor, uid, [at_id], {'data_pm': dt})

            at = ats_obj.browse(cursor, uid, at_id)
            expect(at.cfo).to(be_false)

    def test_field_cfo_is_calculated_depending_on_date_context(self):
        """
        Fem que es calculi segons la data en context per valors més grans de
        6 anys i menors, es donen els dos casos, amb i sense CFO i per tant el
        camp cfo ha de ser true i false
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            ats_obj = self.openerp.pool.get('giscedata.at.tram')

            at_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_at', 'at_tram_0001'
            )[1]

            dt = (datetime.now() - relativedelta(years=8))
            dt_context = dt + relativedelta(years=5)

            ats_obj.write(cursor, uid, [at_id], {'bloquejar_pm': 1})
            ats_obj.write(cursor, uid, [at_id], {
                'data_pm': dt.strftime('%Y-%m-%d')
            })

            at = ats_obj.browse(cursor, uid, at_id, context={
                'data_inici_cfo': dt_context.strftime('%Y-%m-%d')
            })
            expect(at.cfo).to(be_true)

    def test_ff_calc_n_suports(self):
        """
        Test de ff_calc_n_suports
        :return: None
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            sup_obj = self.openerp.pool.get('giscedata.at.suport')
            linia_obj = self.openerp.pool.get('giscedata.at.linia')
            poste_obj = self.openerp.pool.get('giscedata.at.poste')

            pfo_id = poste_obj.create(cursor, uid, {"material": "pfo"})
            pfu_id = poste_obj.create(cursor, uid, {"material": "pfu"})
            pm_id = poste_obj.create(cursor, uid, {"material": "pm"})

            linia_id = linia_obj.create(cursor, uid, {"name": "test"})

            sup_obj.create(cursor, uid, {"linia": linia_id, "poste": pfo_id})

            sup_obj.create(cursor, uid, {"linia": linia_id, "poste": pfu_id})
            sup_obj.create(cursor, uid, {"linia": linia_id, "poste": pfu_id})

            sup_obj.create(cursor, uid, {"linia": linia_id, "poste": pm_id})
            sup_obj.create(cursor, uid, {"linia": linia_id, "poste": pm_id})
            sup_obj.create(cursor, uid, {"linia": linia_id, "poste": pm_id})

            data_linia = linia_obj.read(cursor, uid, linia_id, ["n_suports_formigo","n_suports_fusta","n_suports_metallics"])

            self.assertEqual(data_linia["n_suports_formigo"], 1)
            self.assertEqual(data_linia["n_suports_fusta"], 2)
            self.assertEqual(data_linia["n_suports_metallics"], 3)
