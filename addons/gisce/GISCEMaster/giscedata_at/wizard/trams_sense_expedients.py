# -*- coding: utf-8 -*-
import wizard

def _buscar(self, cr, uid, data, context={}):
  
  cr.execute("select tram_id from giscedata_at_tram_expedient")
  ids = map(lambda x: x[0], cr.fetchall())

  action = {
    'domain': "[('linia.name', '!=', '1'), ('id','not in', ["+','.join(map(str,map(int, ids)))+"])]",
    'name': 'Trams sense expedient',
    'view_type': 'form',
    'view_mode': 'tree,form',
    'res_model': 'giscedata.at.tram',
    'view_id': False,
    'limit': len(ids),
    'type': 'ir.actions.act_window'
  }
  return action


class wizard_trams_sense_expedients(wizard.interface):
  
  states = {
    'init': {
      'actions': [],
      'result': {'type': 'state', 'state': 'buscar'}
    },
    'buscar': {
      'actions': [],
      'result': {'type': 'action', 'action': _buscar, 'state': 'end'}
    },
  }

wizard_trams_sense_expedients('trams.sense.expedients')