# -*- coding: utf-8 -*-
import wizard
import pooler

def _init(self, cr, uid, data, context={}):
  cr.execute("select id from giscedata_at_suport where linia = %s", (int(data['id']),))
  result = [a[0] for a in cr.fetchall()]
  return {'suports': result}

_init_form = """<?xml version="1.0"?>
<form string="Escollir un capítol" col="4">
  <label string="Deixa en aquesta llista els suports que s'hagin de canviar de Línia" colspan="4" />
  <group string="Suports a moure" colspan="4">
    <field name="suports" required="1" widget="many2many" nolabel="1" width="700" height="300" />
  </group>
  <field name="linia" required="1" />
</form>"""

_init_fields = {
  'suports': {'string': "Suports", 'type': 'one2many', 'relation':'giscedata.at.suport'},
  'linia': {'string': 'Línia', 'type': 'many2one', 'relation': 'giscedata.at.linia'},
}


def _moure(self, cr, uid, data, context={}):
  pool = pooler.get_pool(cr.dbname)
  tram_obj = pool.get('giscedata.at.suport')
  tram_obj.write(cr, uid, data['form']['suports'][0][2], {'linia': data['form']['linia']})
  return {}

class wizard_giscedata_at_suport_moure(wizard.interface):

  states = {
    'init': {
      'actions': [_init],
      'result': {
        'type': 'form', 
        'arch': _init_form,
        'fields': _init_fields, 
        'state':[
          ('end', 'Cancel·lar', 'gtk-cancel'),
          ('moure', 'Moure', 'gtk-ok')
        ]
      }
    },
    'moure': {
      'actions': [_moure],
      'result': {
        'type': 'state',
        'state': 'end',
      }
    },
    'end': {
      'actions': [],
      'result': {
        'type':'state',
        'state': 'end'
      }
    },
  }

wizard_giscedata_at_suport_moure('giscedata.at.suport.moure_suport')

