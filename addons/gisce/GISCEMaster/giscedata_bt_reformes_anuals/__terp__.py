# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Reformes anuals BT",
    "description": "Modul de reformes anuals de BT",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Baixa Tensió",
    "depends":[
        "base",
        "giscedata_bt",
        "giscedata_cts",
        "giscedata_obres_bt",
        "giscedata_cne"
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "wizard/reformes_anuals_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
