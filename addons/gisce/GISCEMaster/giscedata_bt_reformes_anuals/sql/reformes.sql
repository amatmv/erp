  SELECT
    bt.name,
    obra.descripcio AS descripcio,
    split_part(ct.name,'-',2)||lpad(bt.num_linia::text,2,'0') AS codi_linia,
    municipi.name AS municipi,
    split_part(ct.name,'-',2) AS ct,
    CASE
      WHEN bt.voltatge = '230' THEN 'B1'
      WHEN bt.voltatge = '400' THEN 'B2'
    END
      AS tensio,
    CASE
      WHEN tipus_linia.name = 'Aèria' THEN longitud_cad
      ELSE 0
    END
      AS nova_longitud_aeria,
    0 AS longitud_aeria_retirada,
    CASE
      WHEN tipus_linia.name = 'Subterrània' THEN longitud_cad
      ELSE 0
    END
     AS nova_longitud_subterrania,
    0 AS longitud_subterrania_retirada,
    cable.seccio AS seccions,
    '' AS suports,
    obra.tipus AS tipus_obra,
    neutres.rpt_neutre AS pat_neutre,
    CASE
      WHEN bt.cedida THEN 'SI'
      ELSE 'NO'
    END AS cedida
  FROM giscedata_bt_element bt
    LEFT JOIN giscedata_bt_cables AS cable ON (bt.cable = cable.id)
    LEFT JOIN giscedata_bt_tipuscable AS tipus_cable ON (cable.tipus = tipus_cable.id)
    LEFT JOIN giscedata_bt_tipuslinia AS tipus_linia ON (bt.tipus_linia = tipus_linia.id)
    LEFT JOIN giscedata_cts AS ct ON (bt.ct=ct.id)
    LEFT JOIN res_municipi AS municipi ON (bt.municipi = municipi.id)
    LEFT JOIN giscedata_obres_obra_bt_element_rel AS rel_obres ON (bt.id=rel_obres.element_id)
    LEFT JOIN giscedata_obres_obra AS obra ON (obra.id = rel_obres.obra_id)
    LEFT JOIN (
      SELECT id_ct,
        max(name) OVER (PARTITION BY "id_ct" ORDER BY name desc),
        rank() OVER (PARTITION BY "id_ct" order by name desc) AS rank,
        rpt_neutre
        FROM giscedata_cts_mesures) AS neutres ON (bt.ct=neutres.id_ct AND neutres.rank=1)
  WHERE
    bt.data_alta BETWEEN '01/01/%(year)s' AND '31/12/%(year)s' AND
    tipus_cable.codi !='E'
UNION
  SELECT
    bt.name,
    obra.descripcio AS descripcio,
    split_part(ct.name,'-',2)||lpad(bt.num_linia::text,2,'0') AS codi_linia,
    municipi.name AS municipi,
    split_part(ct.name,'-',2) AS ct,
    CASE
      WHEN bt.voltatge = '230' THEN 'B1'
      WHEN bt.voltatge = '400' THEN 'B2'
    END
    AS tensio,
    0 AS nova_longitud_aeria,
    CASE
      WHEN tipus_linia.name = 'Aèria' THEN longitud_cad
      ELSE 0
    END AS longitud_aeria_retirada,
    0 AS nova_longitud_subterrania,
    CASE
      WHEN tipus_linia.name = 'Subterrània' THEN longitud_cad
      ELSE 0
    END AS longitud_subterrania_retirada,
    cable.seccio AS seccions,
    '' AS suports,
    obra.tipus AS tipus_obra,
    neutres.rpt_neutre AS pat_neutre,
    CASE
      WHEN bt.cedida THEN 'SI'
      ELSE 'NO'
    END AS cedida
  FROM giscedata_bt_element bt
    LEFT JOIN giscedata_bt_cables AS cable ON (bt.cable = cable.id)
    LEFT JOIN giscedata_bt_tipuscable AS tipus_cable ON (cable.tipus = tipus_cable.id)
    LEFT JOIN giscedata_bt_tipuslinia AS tipus_linia ON (bt.tipus_linia = tipus_linia.id)
    LEFT JOIN giscedata_cts AS ct ON (bt.ct=ct.id)
    LEFT JOIN res_municipi AS municipi ON (bt.municipi = municipi.id)
    LEFT JOIN giscedata_obres_obra_bt_element_rel AS rel_obres ON (bt.id=rel_obres.element_id)
    LEFT JOIN giscedata_obres_obra AS obra ON (obra.id = rel_obres.obra_id)
    LEFT JOIN (
      SELECT id_ct,
        max(name) OVER (PARTITION BY "id_ct" ORDER BY name desc),
        rank() OVER (PARTITION BY "id_ct" order by name desc) AS rank,
        rpt_neutre
        FROM giscedata_cts_mesures
      ) AS neutres ON (bt.ct=neutres.id_ct AND neutres.rank=1)
  WHERE
    bt.data_baixa BETWEEN '01/01/%(year)s' AND '31/12/%(year)s' AND
    tipus_cable.codi !='E';