# -*- coding: utf-8 -*-
from osv import osv, fields
import tools
from tools.translate import _
import base64
import os
import StringIO
import csv
import time


class WizardGiscedataBtReformesAnuals(osv.osv_memory):
    """
    Class of the wizzard of reformes anuals
    """

    _name = 'wizard.giscedata.bt.reformes.anuals'

    sql_path = "%s/giscedata_bt_reformes_anuals/sql" % tools.config['addons_path']

    def action_exec_reformes_bt(self, cursor, uid, ids, context=None):
        """
        Function that handles the generation of the report of reformes bt from
        the wizard

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Id of  the wizard
        :type ids: list of id
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype: None
        """

        if not context:
            context = {}

        sql = self.get_query(cursor, uid, ids, context)
        params = {
            "year": self.read(cursor, uid, ids[0], ["year"])[0]["year"]
        }

        cursor.execute(sql, params)
        data_sql = cursor.fetchall()

        output = StringIO.StringIO()

        headers = [
                "Linia BT", "Descripcio", "Codi linia", "Municipi", "CT",
                "tensio", "Nova longitud aeria", "Longitud aeria retirada",
                "Nova longitud subterrania", "Longitud subterrania retirada",
                "Seccio", "Suports", "Tipus obra", "PAT Neutre","Cedida"]

        writer = csv.writer(
            output,
            delimiter=';', quoting=csv.QUOTE_NONE, escapechar="\\",


        )
        writer.writerow(headers)

        for row in data_sql:
            writer.writerow(row)

        file_data = base64.b64encode(output.getvalue())
        wiz_data = {
            "state": "done",
            "name": "reformes_bt.csv",
            "file": file_data
        }

        self.write(cursor, uid, ids, wiz_data)

    def get_query(self, cursor, uid, ids, context=None):
        """
        Retorna la consulta a executar

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: id of the wizard
        :type ids: list of int
        :param context: OpenERP context
        :type context: dict
        :return: String with the query
        :rtype: str
        """

        if not context:
            context = {}

        fname_path = os.path.join(self.sql_path, "reformes.sql")
        query = open(fname_path).read()
        return query

    _columns = {
        'name': fields.char('Filename', size=128),
        'file': fields.binary('Fitxer'),
        'state': fields.char('Estat', size=16),
        'year': fields.integer('Any')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'year': lambda *a: int(time.strftime("%Y"))
    }
WizardGiscedataBtReformesAnuals()
