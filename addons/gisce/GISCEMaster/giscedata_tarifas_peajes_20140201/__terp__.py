# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes Febrero 2014",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 28 - 01/02/2014.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures",
        "product",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_peajes_20140201_data.xml"
    ],
    "active": False,
    "installable": True
}
