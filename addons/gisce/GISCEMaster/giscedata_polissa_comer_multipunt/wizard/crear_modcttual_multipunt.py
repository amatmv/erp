# -*- coding: utf-8 -*-

import time
from tools.translate import _
from osv import osv, fields

class WizardModcttualMultipunt(osv.osv_memory):

    _name = 'wizard.modcontractual.multipunt'

    _duracio_selection = [
        ('actual', 'Final del contracte actual'),
        ('nou', 'Nou periode complet de contracte')
    ]

    _accio_selection = [
        ('modificar', 'Modificar el contracte existent'),
        ('nou', 'Crear una modificació contractual nova')
    ]

    def _default_observacions(self, cursor, uid, context=None):
        """Observacions per defecte
        """
        obs = _('Renovació multipunt per canvi de versió de preus')
        return obs

    def _default_data_inici(self, cursor, uid, context=None):
        """Obtenim la data d'inici.
        """
        data_inici = time.strftime('%Y-%m-%d')
        return data_inici

    def _default_multipunt_id(self, cursor, uid, context=None):
        """Obtenim el multipunt que estem modificant
        """
        return context.get('active_id', False)

    def _default_state(self, cursor, uid, context=None):
        """Comprovem amb quin estat s'ha d'inicialitzar l'assistent.
        Si qualsevol de les polisses del multipunt tenen modificacions
        contractuals pendents, fallarà.
        """
        multipunt_id = context.get('multipunt_id', False)
        modcont_obj = self.pool.get('giscedata.polissa.modcontractual')
        polissa_obj = self.pool.get('giscedata.polissa')
        pol_params = [('multipunt_id', '=', multipunt_id)]
        polissa_ids = polissa_obj.search(cursor, uid, pol_params)
        search_params = [
            ('polissa_id.id', 'in', polissa_ids),
            ('state', '=', 'pendent'),
        ]
        count = modcont_obj.search(cursor, uid, search_params)
        if count:
            return 'fail'
        else:
            return 'ok'

    def action_crear_modificaciones(self, cursor, uid, ids, context=None):
        """Creem la modificació contractual a tots els contractes del multipunt.
        """
        if context is None:
            context = {}

        polisses_error = []
        polisses_ok = []
        mods_creades = []

        wizard = self.browse(cursor, uid, ids[0], context)
        multipunt_id = context.get('multipunt_id', False)
        polissa_obj = self.pool.get('giscedata.polissa')
        wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')
        pol_params = [('multipunt_id', '=', multipunt_id)]
        polissa_ids = polissa_obj.search(cursor, uid, pol_params)

        duracio = wizard.duracio
        accio = wizard.accio
        data_inici = wizard.data_inici
        data_final = wizard.data_final
        data_firma = wizard.data_firma_contracte
        obs = wizard.observacions

        # crear la modificació contractual en cada polissa del multipunt
        for polissa_id in polissa_ids:

            polissa = polissa_obj.browse(cursor, uid, polissa_id)

            polissa.send_signal('modcontractual')

            ctx = context.copy()
            ctx.update({'active_id': polissa_id})

            if obs:
                ctx.update({'observacions': obs})

            params = {'duracio': duracio,
                      'accio': accio,
                      }
            if data_firma:
                params.update({'data_firma_contracte': data_firma})

            wz_id = wz_crear_mc_obj.create(cursor, uid, params, ctx)
            wiz = wz_crear_mc_obj.browse(cursor, uid, wz_id, ctx)
            res = wz_crear_mc_obj.onchange_duracio(cursor, uid,
                                                   [wz_id], data_inici,
                                                   wiz.duracio, ctx)

            # si falla, guardar-la per al resum
            if res.get('warning', False):
                polisses_error.append(polissa.name)
                polissa.send_signal('undo_modcontractual')
                continue

            # si no falla, crear el contracte i guardar-la per al resum
            wiz.write({'data_final': data_final or '',
                       'data_inici': data_inici,
                       })
            wiz.action_crear_contracte(ctx)
            polisses_ok.append(polissa.name)
            if accio == 'nou':
                polissa = polissa_obj.browse(cursor, uid, polissa_id)
                mods_creades.append(str(polissa.modcontractuals_ids[0].id))

        resum = _('Modificaciones contractuales creadas en los '
                  'contratos: {0}\nErrores en los contratos: {1}'.
                  format(','.join(polisses_ok), ','.join(polisses_error)))
        self.write(cursor, uid, ids, {'resum': resum,
                                      'state': 'end',
                                      'mods_creades': ','.join(mods_creades),
                                      })

        return True

    def action_anar_a_modificacions(self, cursor, uid, ids, context=None):
        # buscar les modificacions contractuals que ha creat l'assistent

        mods = self.read(cursor, uid, ids, ['mods_creades'])
        mods_ids = mods[0]['mods_creades'].split(',')
        domain = [('id', 'in', mods_ids)]
        return {
                'name': _(u'Modificacions contractuals'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.polissa.modcontractual',
                'type': 'ir.actions.act_window',
                'domain': domain,
                }

    def action_cancel(self, cursor, uid, ids, context=None):
        """Cancel·lem.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    def onchange_duracio(self, cursor, uid, ids, data_inici, duracio,
                         context=None):
        """Funció quan canviem la duració.
        """
        modcont_obj = self.pool.get('giscedata.polissa.modcontractual')
        res = {'value': {}, 'warning': {}, 'domain': {}}
        if not duracio or not data_inici:
            return res
        # Canviem els valors
        if duracio == 'nou':
            data_final = modcont_obj.get_data_final(cursor, uid,
                                                    data_inici, context)
            res['value']['data_final'] = data_final
        else:
            res['value']['data_final'] = False
        if res.get('value').get('data_final', False):
            if res['value']['data_final'] <= data_inici:
                res['warning'].update({'title': _('Error'),
                                       'message': _('La data final no pot ser '
                                                    'inferior a la inicial.')})
                res['value']['data_final'] = False

        return res

    _columns = {
        'data_inici': fields.date("Data d'activació"),
        'data_final': fields.date('Data final'),
        'data_firma_contracte': fields.datetime('Data firma %H:%M:%S'),
        'observacions': fields.text("Observacions", required=True),
        'duracio': fields.selection(_duracio_selection, 'Duració'),
        'accio': fields.selection(_accio_selection, 'Acció', required=True),
        'state': fields.selection([('ok', 'OK'),
                                   ('fail', 'Fail'),
                                   ('end', 'End')],
                                  'State'),
        'multipunt_id': fields.many2one('giscedata.polissa.multipunt',
                                        'Multipunto', required=True),
        'error_mods_pendents': fields.text(_('Error: Hay pólizas que tienen'
                                             'modificaciones pendientes')),
        'resum': fields.text(u'Resumen', size=300, readonly=True),
        'mods_creades': fields.text(_('Modificaciones creadas')),
    }

    _defaults = {
        'multipunt_id': _default_multipunt_id,
        'observacions': _default_observacions,
        'data_inici': _default_data_inici,
        'state': _default_state,
        'accio': lambda *a: 'nou',
    }

WizardModcttualMultipunt()
