# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _

class WizardAssignarPolissaAMultipunt(osv.osv_memory):

    _name = 'wizard.assignar.polissa.a.multipunt'

    def set_to_multipoint(self, cursor, uid, id_wiz, context=None):
        if context is None:
            context = {}

        if isinstance(id_wiz, (tuple, list)):
            id_wiz = id_wiz[0]

        polissa_obj = self.pool.get('giscedata.polissa')

        wizard = self.browse(cursor, uid, id_wiz)

        if wizard.multipunt:
            multipunt_name = wizard.multipunt.name
            multipunt_id = wizard.multipunt.id
        else:
            multipunt_name = ''
            multipunt_id = False

        added_polisses = []
        for polissa_id in context.get('active_ids', []):
            polissa_obj.write(
                cursor, uid, polissa_id, {'multipunt_id': multipunt_id}
            )

            polissa_name = polissa_obj.read(
                cursor, uid, polissa_id, ['name']
            )['name']
            added_polisses.append(polissa_name)

        if multipunt_id:
            result = _('Les polisses ') + ', '.join(added_polisses)
            result += _(' han estat afegides al multipunt ') + multipunt_name
        else:
            result = _('Les polisses ') + ', '.join(added_polisses)
            result += _(' ja no formen part de cap multipunt')
        wizard.write({'result': result, 'state': 'end'})

        return True

    _columns = {
        'state': fields.selection([('start', 'Inici'), ('end', 'Fi')], 'Estat'),
        'multipunt': fields.many2one(
            'giscedata.polissa.multipunt', 'Assignar al multipunt'
        ),
        'result': fields.text('Resultat', readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'start',
    }

WizardAssignarPolissaAMultipunt()
