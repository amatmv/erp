# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _

class WizardObrirModificacionsMultipunt(osv.osv_memory):

    _name = 'wizard.anar.a.modificacions'

    def _default_multipunt_id(self, cursor, uid, context=None):
        """Obtenim el multipunt que estem modificant
        """
        return context.get('active_id', False)

    def anar_a_modcons_multipunt(self, cursor, uid, ids, context=None):
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        multipunt_obj = self.pool.get('giscedata.polissa.multipunt')
        pol_obj = self.pool.get('giscedata.polissa')

        multipunt_id = context.get('multipunt_id', False)
        nom = multipunt_obj.read(cursor, uid, [multipunt_id], ['name'])[0]['name']
        params = [('multipunt_id', '=', multipunt_id)]
        polisses_ids = pol_obj.search(cursor, uid, params)

        modconparams = [('polissa_id', 'in', polisses_ids)]
        modcon_ids = modcon_obj.search(cursor, uid, modconparams)

        vista_obj = self.pool.get('ir.ui.view')
        name = 'giscedata.polissa.modcontractual.tree.llistat'
        vista_id = vista_obj.search(cursor, uid, [('name', '=', name)])
        domain = [('id', 'in', modcon_ids)]
        return {
                'name': _(u'Modificacions contractuals de {0}'.format(nom)),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': vista_id,
                'res_model': 'giscedata.polissa.modcontractual',
                'type': 'ir.actions.act_window',
                'domain': domain,
                }

    _columns = {
        'multipunt_id': fields.many2one('giscedata.polissa.multipunt',
                                        'Multipunt', required=True),
    }

    _defaults = {
        'multipunt_id': _default_multipunt_id,
    }

WizardObrirModificacionsMultipunt()
