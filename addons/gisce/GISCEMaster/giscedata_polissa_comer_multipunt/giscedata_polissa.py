# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy_data(self, cursor, uid, id, default=None, context=None):
        res, x = super(GiscedataPolissa, self).copy_data(
            cursor, uid, id, default=default, context=context)
        res.update({'multipunt_id': False})
        return res, x

    _columns = {
        'multipunt_id': fields.many2one(
            'giscedata.polissa.multipunt', 'Multipunt', select=1
        ),
    }

GiscedataPolissa()
