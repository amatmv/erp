# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscedataPolissaMultipunt(osv.osv):
    _name = 'giscedata.polissa.multipunt'

    _columns = {
        'name': fields.char('Nom multipunt', size=256, required=True),
        'partner_id': fields.many2one('res.partner', 'Titular', required=True),
        'partner_address_id': fields.many2one(
            'res.partner.address', u'Direcció titular', required=True
        ),
        'contact_address_id': fields.many2one(
            'res.partner.address', u'Direcció persona contacte', required=True
        ),
        'bank_id': fields.many2one(
            'res.partner.bank', 'Compte bancari', required=True
        ),
        'polisses': fields.one2many(
            'giscedata.polissa', 'multipunt_id', 'Contractes',
            context={'active_test': False}
        ),
        'active': fields.boolean('Actiu', select=1),
    }

    _defaults = {
        'active': lambda *a: True
    }

    def update_mandate(self, cursor, uid, polissa_multi_id, context=None):

        if not context:
            context = {}

        if isinstance(polissa_multi_id, (list, tuple)):
            polissa_multi_id = polissa_multi_id[0]

        polissa_multi = self.browse(cursor, uid, polissa_multi_id)
        debtor_address = (polissa_multi.partner_address_id.
                          name_get(context=context)[0][1]).upper()
        debtor_contact = (polissa_multi.partner_address_id.name
                          and '%s, ' % polissa_multi.partner_address_id.name
                          or '').upper()
        if debtor_contact:
            debtor_address = debtor_address.replace(debtor_contact, '')
        debtor_state = (polissa_multi.partner_address_id.state_id
                        and polissa_multi.partner_address_id.state_id.name
                        or '').upper()
        debtor_country = (polissa_multi.partner_address_id.country_id
                          and polissa_multi.partner_address_id.country_id.name
                          or '').upper()
        notes = u"Contracte Multipunt: %s\n" % polissa_multi.name

        vals = {
            'debtor_name': polissa_multi.partner_id.name,
            'debtor_vat': polissa_multi.partner_id.vat,
            'debtor_address': debtor_address,
            'debtor_state': debtor_state,
            'debtor_country': debtor_country,
            'debtor_iban': polissa_multi.bank_id.iban,
            'reference': '%s,%s' % (
                'giscedata.polissa.multipunt', polissa_multi_id
            ),
            'notes': notes,
        }

        return vals


GiscedataPolissaMultipunt()
