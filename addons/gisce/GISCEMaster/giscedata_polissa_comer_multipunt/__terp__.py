# -*- coding: utf-8 -*-
{
    "name": "Contractes multipunt",
    "description": """Crea el model de contracte multipunt i afegeix als contractes la opció d'assignar un mulitpunt""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_polissa_comer",
        "giscedata_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_multipunt_data.xml",
        "giscedata_polissa_multipunt_view.xml",
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv",
        "wizard/crear_modcttual_multipunt_view.xml",
        "wizard/anar_a_modcttual_multipunt_view.xml",
        "wizard/assignar_polissa_a_multipunt_view.xml"
    ],
    "active": False,
    "installable": True
}
