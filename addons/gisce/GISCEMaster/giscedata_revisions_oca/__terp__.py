# -*- coding: utf-8 -*-
{
    "name": "Normativa OCA",
    "description": """
    This module provide :
        * Defectes segons la normativa OCA.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "c2c_webkit_report",
        "base",
        "giscedata_revisions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_revisions_oca_report.xml",
        "giscedata_revisions_oca_data.xml"
    ],
    "active": False,
    "installable": True
}
