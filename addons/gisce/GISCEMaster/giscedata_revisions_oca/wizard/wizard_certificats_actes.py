# -*- coding: utf-8 -*-

import pooler
import netsvc


def _certificats(self, cr, uid, data, context=None):
    ids = []
    flag = True
    es_acta = False
    obj_revisio = pooler.get_pool(cr.dbname).get('giscedata.'
                                                 'revisions.at.revisio')

    for revisio in obj_revisio.browse(cr, uid, data['revisions']):
        defectes_obj = pooler.get_pool(cr.dbname).get('giscedata.'
                                                      'revisions.at.defectes')
        # Si no tÃ© defectes passa directament als certificables
        if len(revisio.defectes_ids) == 0:
            ids.append(revisio.id)
        # Si te defectes perÃ² tots son interns tambÃ© ha de passar
        # a ser certificable
        # Afegim que si el defecte es lleu també s'ha de considerar certificable
        else:
            for defecte in revisio.defectes_ids:
                valoracio = defectes_obj.read(cr, uid, defecte.id,
                                              ['valoracio'])
                if valoracio['valoracio']:
                    if es_acta is True:
                        break
                    elif valoracio['valoracio'][1] == 'Grave' \
                            or valoracio['valoracio'][1] == 'Muy grave':
                        es_acta = True
                    else:
                        if valoracio['valoracio'][1] == 'Leve':
                            ids.append(revisio.id)

            for d in revisio.defectes_ids:
                flag &= d.intern
            if flag:
                ids.append(revisio.id)

    if len(ids) and es_acta is not True:
        data['certificats'] = ids
    else:
        data['certificats'] = []
    return {}


def _actes(self, cr, uid, data, context=None):
    ids = []
    tots_leve = True

    defectes_obj = pooler.get_pool(cr.dbname) \
        .get('giscedata.revisions.at.defectes')

    obj_revisio = pooler.get_pool(cr.dbname) \
        .get('giscedata.revisions.at.revisio')

    for revisio in obj_revisio.browse(cr, uid, data['revisions']):
        # En que trobem un defecte que no sigui intern i no estigui
        # reparat ja tindrem una acta

        # Si trobem un defecte que no es lleu o intern, sera una acta
        for defecte in revisio.defectes_ids:
            # Comprovo si tots els defectes son lleus, si ho son no mostrar acta.
            valoracio = defectes_obj.read(cr, uid, defecte.id, ['valoracio'])
            if valoracio['valoracio']:
                if valoracio['valoracio'][1] != 'Leve':
                    tots_leve = False

                if not defecte.intern and not defecte.reparat:
                    ids.append(revisio.id)

    if ids and tots_leve is False:
        data['actes'] = ids
    else:
        data['actes'] = []
    return {}


# Monkeypatching!

logger = netsvc.Logger()

if 'wizard.giscedata.revisions.at.certificats_actes' in netsvc.SERVICES:
    wizard_old = netsvc.SERVICES[
        'wizard.giscedata.revisions.at.certificats_actes']
    logger.notifyChannel(
        'monkeypatching', netsvc.LOG_INFO,
        'Patching _certificats (%s) -> (%s)'
        % (wizard_old.states['certificats']['actions'][0], _certificats)
    )
    wizard_old.states['certificats']['actions'][0] = _certificats
    logger.notifyChannel(
        'monkeypatching', netsvc.LOG_INFO,
        'Patching _actes (%s) -> (%s)'
        % (wizard_old.states['actes']['actions'][0], _actes)
    )
    wizard_old.states['actes']['actions'][0] = _certificats = _actes
