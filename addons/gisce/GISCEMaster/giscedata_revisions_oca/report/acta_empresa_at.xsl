<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list"/>
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template pageSize="(19cm,297mm)" leftMargin="3cm" rightMargin="3cm" showBoundary="0">
        <pageTemplate id="main">
          <frame id="body" x1="1cm" y1="5mm" width="175mm" height="287mm" />

          <pageGraphics>
          	<setFont name="Helvetica" size="8" />
          	<drawRightString x="18.5cm" y="0.5cm">Página <pageNumber /></drawRightString>
            <translate dx="-168mm" dy="170mm" />
            <rotate degrees="-90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="0mm" y="170mm">doc. 13.3-65_02 </drawString>
            <rotate degrees="90" />
            <setFont name="Helvetica" size="6" />
          	<!--<drawRightString x="34.8cm" y="10cm">núm.:OC-I/255</drawRightString>-->
          </pageGraphics>
        </pageTemplate>
      </template>

      <stylesheet>
        <paraStyle name="text"
					fontName="Helvetica"
					fontSize="8.5"
                    alignment="justify"
				    leading="10"
                />
        <paraStyle name="cert"
					fontName="Helvetica"
					fontSize="8"
                    alignment="justify"
				    leading="10"
                />
        <paraStyle name="subtitol"
					fontName="Helvetica-Bold"
					fontSize="8"
                    alignment="justify"
				/>
        <paraStyle name="obser"
					fontName="Helvetica"
					fontSize="8"
                    alignment="justify"
				/>
        <paraStyle name="footnote"
					fontName="Helvetica"
					fontSize="7"
                    alignment="justify"
				/>
        <paraStyle name="symbol"
					fontName="Helvetica"
					fontSize="8"
                    alignment="center"
				/>
        <paraStyle name="tdefectes"
					fontName="Helvetica-Bold"
					fontSize="10"
                    alignment="center"
				/>
        <paraStyle name="petit"
					fontName="Helvetica"
					fontSize="6.5"
                    alignment="RIGHT"
				/>

        <blockTableStyle id="taula_logos">
          <blockAlignment value="LEFT" start="0,0" stop="0,0" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_titol">
          <blockAlignment value="CENTER" />
          <blockTopPadding length="0" />
          <blockLeftPadding length="0" />
          <blockRightPadding length="0" />
          <blockBottomPadding length="0" />
          <blockBackground colorName="silver" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockFont name="Helvetica-Bold" size="9" />
          <blockLeading length="4" start="0,0" stop="0,0" />
          <blockLeading length="4" start="0,2" stop="0,2" />
        </blockTableStyle>

        <blockTableStyle id="taula_contingut">
          <blockBottomPadding length="1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="1,0" />
          <blockFont name="Helvetica" size="8.5"/>
          <blockLeading length="7" />

          <blockLeading length="0.000001" start="0,0" stop="1,0" />

          <blockLeading length="20" start="0,16" stop="1,16" />
          <lineStyle kind="LINEBELOW" colorName="black" start="0,17" stop="1,17" />
          <blockFont name="Helvetica-Bold" start="0,17" stop="1,17"/>
          <blockLeading length="10" start="0,17" stop="1,17" />

          <blockLeading length="15" start="0,19" stop="1,19" />
          <blockFont name="Helvetica-Bold" start="0,20" stop="1,20"/>
        </blockTableStyle>

        <blockTableStyle id="taula_recer">
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="1,0" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="1,0" size="8"/>
          <blockLeading length="10" start="0,0" stop="1,0" />

          <blockLeading length="0.000001" start="0,1" stop="1,1" />

          <blockFont name="Helvetica-Bold" start="0,2" stop="1,2" size="8"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,2" stop="1,2" />
          <blockLeading length="10" start="0,2" stop="1,2"/>
        </blockTableStyle>

        <blockTableStyle id="taula_certi">
          <blockFont name="Helvetica" size="8.5" />
        </blockTableStyle>

        <blockTableStyle id="taula_certi_column">
          <blockFont name="Helvetica" size="8.5" />
          <blockRightPadding length="12mm" />
        </blockTableStyle>

        <blockTableStyle id="taula_trafos">
          <blockFont name="Helvetica" size="8.5" />
          <blockBottomPadding length="1"/>
        </blockTableStyle>

        <blockTableStyle id="taula_trafos_pot">
          <blockFont name="Helvetica" size="8.5" />
          <blockFont name="Helvetica-Bold" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_firma">
          <blockFont name="Helvetica" size="8.5" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="cap_taula_defectes">
          <blockFont name="Helvetica-Bold" size="10" />
          <lineStyle kind="LINEBELOW" colorName="black" />
          <blockAlignment value="CENTER" />
        </blockTableStyle>

        <blockTableStyle id="taula_defectes">
          <blockAlignment value="CENTER" />
          <blockFont name="Helvetica" size="7.5" />
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="6,0"/>
          <blockFont name="Helvetica-Bold" start="0,0" stop="6,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_tecnic">
          <blockFont name="Helvetica" size="10" />
          <blockFont name="Helvetica-Bold" start="0,1" stop="3,1" />
        </blockTableStyle>

      </stylesheet>

      <story>
        <xsl:apply-templates select="revisio" mode="story" />
      </story>
    </document>
  </xsl:template>

  <xsl:template match="revisio" mode="story">
    <blockTable colWidths="100mm, 75mm" style="taula_logos">
      <tr>
        <td><image file="addons/giscedata_revisions_oca/report/gisce_oca.tif"
        width="45mm" height="12.5mm" /></td>
        <td>
           <image file="addons/giscedata_revisions_oca/report/enac_oca.tif"
        width="28mm" height="16.88mm" /><para style="petit">núm.:OC-I/255</para>
        </td>
      </tr>
    </blockTable>
    <spacer length="10" />
    <blockTable colWidths="175mm" style="taula_titol">
      <tr><td></td></tr>
      <tr>
        <td>ACTA DE RECONOCIMIENTO PERIÓDICO</td>
      </tr>
      <tr>
        <td>INSTALACIÓN ELÉCTRICA</td>
      </tr>
      <tr><td></td></tr>
    </blockTable>
    <spacer length="6" />
    <para alignment="center" fontName="Helvetica-Bold">Líneas de Alta Tensión</para>
    <blockTable colWidths="70mm,105mm" style="taula_contingut">
      <tr><td></td><td></td></tr>
      <tr>
        <td>Código de documento</td>
        <td>C-AT-<xsl:value-of select="concat(linia/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr>
        <td>Código de revisión</td>
        <td><xsl:value-of select="concat(linia/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr>
        <td>Expediente autorización administrativa</td>
        <td><xsl:value-of select="linia/expedients" /></td>
      </tr>
      <tr>
        <td>Fecha de revisión y hora</td>
        <td><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4), ' ', substring(data, 12, 5), ' hores')"/></td>
      </tr>
      <tr>
        <td>Unidad de reconocimiento</td>
        <td>LAT-<xsl:value-of select="linia/codi" /></td>
      </tr>
      <tr>
        <td>Tipo de instalación</td>
        <td>Línea aerea de alta tensión</td>
      </tr>
      <tr>
        <td>Empresa titular</td>
        <td><xsl:value-of select="linia/propietari/nom" /></td>
      </tr>
      <tr>
        <td>Dirección para notificaciones</td>
        <td><xsl:value-of select="concat(linia/propietari/adreca/carrer, ' ', linia/propietari/adreca/cp, ' - ', linia/propietari/adreca/ciutat)" /></td>
      </tr>
      <tr>
        <td>Organismo de inspección</td>
        <td><xsl:value-of select="tecnic/organisme" /></td>
      </tr>
      <tr>
        <td>Dirección Organismo de inspección</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr>
        <td>Nombre y apellidos del inspector</td>
        <td><xsl:value-of select="tecnic/nom" /></td>
      </tr>
      <tr>
        <td>Titulación</td>
        <td><xsl:value-of select="tecnic/titolacio" /></td>
      </tr>
      <tr>
        <td>Número de colegiado</td>
        <td><xsl:value-of select="tecnic/ncol" /></td>
      </tr>
      <tr>
        <td>Dirección para notificaciones</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr>
        <td>Teléfono</td>
        <td><xsl:value-of select="tecnic/telefon" /></td>
      </tr>
      <tr>
        <td>Correo electrónico</td>
        <td><xsl:value-of select="tecnic/email" /></td>
      </tr>
      <tr>
        <td>Datos de la instalación</td>
        <td></td>
      </tr>
      <tr>
        <td>Instalación</td>
        <td>Línea AT <xsl:value-of select="linia/codi" /></td>
      </tr>
      <tr>
        <td>Emplazamiento</td>
        <td><xsl:value-of select="linia/descripcio" /></td>
      </tr>
      <tr>
        <td>Características técnicas principales</td>
        <td></td>
      </tr>
      <tr>
        <td>Origen de línea</td>
        <td><xsl:value-of select="linia/origen" /></td>
      </tr>
      <tr>
        <td>Final de línea</td>
        <td><xsl:value-of select="linia/final" /></td>
      </tr>
      <tr>
        <td>Tensión de servicio</td>
        <td><xsl:value-of select="concat(linia/tensio, ' V')" /></td>
      </tr>
      <tr>
        <td>Sección conductores</td>
        <td><xsl:value-of select="linia/seccio_conductors" /></td>
      </tr>
      <tr>
        <td>Longitud Aérea</td>
        <td>
          <xsl:choose>
            <xsl:when test="linia/aeri_op&gt;0">
              <xsl:value-of select="concat(format-number(linia/aeri_op, '###,###.##'), ' m')" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="concat(format-number(linia/aeri_cad, '###,###.##'), ' m')" />
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <tr>
        <td>Longitud Subterránea</td>
        <td>
          <xsl:choose>
            <xsl:when test="linia/subt_op&gt;0.0">
              <xsl:value-of select="concat(format-number(linia/subt_op, '###,###.##'), ' m')" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="concat(format-number(linia/subt_cad, '###,###.##'), ' m')" />
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <tr>
        <td>Apoyos</td>
        <td>Metálicos: <xsl:value-of select="linia/n_sup_metal"/>; Hormigón: <xsl:value-of select="linia/n_sup_for" />; Madera: <xsl:value-of select="linia/n_sup_fus" /></td>
      </tr>
    </blockTable>

    <spacer length="10" />

    <blockTable colWidths="120mm, 55mm" style="taula_recer">
      <tr>
        <td><para style="subtitol"><super>(1)</super> Registro de las ultimas operaciones de mantenimiento:</para></td>
        <td></td>
      </tr>
      <tr>
        <td><spacer length="20" /></td>
        <td></td>
      </tr>
      <tr>
        <td>Acta</td>
        <td></td>
      </tr>
    </blockTable>
    <spacer length="5" />
    <para style="text">De acuerdo con los Reglamentos que le son de aplicación, el inspector que suscribe este certificado, ha realizado las comprobaciones y controles que establece la legislación vigente para el tipo de inspección indicado, según:</para>
    <blockTable colWidths="55mm, 100mm" rowHeights="7mm" style="taula_certi">
      <tr>
        <td>
            <blockTable colWidths="5mm, 55mm" style="taula_certi_column">
                <tr>
                    <td><image style="align:left" file="addons/giscedata_revisions/report/squarecross.tif" width="2.5mm" height="2.5mm"/></td>
                    <td><para style="symbol"> - RD 3151/1968 de 28/11/68</para></td>
                </tr>
            </blockTable>
        </td>
        <td>
        </td>
      </tr>
    </blockTable>
    <blockTable colWidths="55mm, 100mm" rowHeights="7mm" style="taula_certi">
      <tr>
        <td>
            <blockTable colWidths="5mm, 50mm" style="taula_certi_column">
                <tr>
                    <td><image style="align:left" file="addons/giscedata_revisions/report/square.tif" width="2.5mm" height="2.5mm"/></td>
                    <td><para style="symbol"> - RD 223/08 de 15/02/08</para></td>
                </tr>
            </blockTable>
        </td>
        <td>
        </td>
      </tr>
    </blockTable>
    <blockTable colWidths="55mm, 100mm" rowHeights="7mm" style="taula_certi">
      <tr>
        <td>
            <blockTable colWidths="5mm, 60mm" style="taula_certi_column">
                <tr>
                    <td><image style="align:left" file="addons/giscedata_revisions/report/squarecross.tif" width="2.5mm" height="2.5mm"/></td>
                    <td><para style="symbol"> - Procedimiento GISCE: 13.3.49</para></td>
                </tr>
            </blockTable>
        </td>
        <td>
            <blockTable colWidths="5mm, 60mm" style="taula_certi_column">
                <tr>
                    <td><image style="align:left" file="addons/giscedata_revisions/report/square.tif" width="2.5mm" height="2.5mm"/></td>
                    <td><para style="symbol"> - Procedimiento GISCE: 13.3.51</para></td>
                </tr>
            </blockTable>
        </td>
      </tr>
    </blockTable>

    <spacer length="12" />

    <para style="cert"><b>HACE CONSTAR:</b></para>
    <para style="cert">
     Que la instalación inspeccionada es <b>Incorrecta</b> y obtiene la calificación de <b>CODICIONADA</b> referente a seguridad del funcionamiento y al cumplimineto de las disposiciones reglamentarias.
    </para>

    <spacer length="9" />
    <para style="cert">
      Este documento queda a disposición del organismo competente
      El presente informe tiene una validez de 3 años a partir de la fecha de revisión.
    </para>
    <para style="obser">
      Observaciones y explicación de acciones no realizadas:
    </para>
    <spacer length="5" />
    <blockTable style="taula_firma" colWidths="65mm,100mm">
      <tr>
        <td>Firma</td><td>Girona, a <xsl:choose><xsl:when test="data_ca2 != ''"><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))" /></xsl:when><xsl:otherwise><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))" /></xsl:otherwise></xsl:choose></td>
      </tr>
    </blockTable>
    <spacer length="55" />
    <para style="footnote">
      <super>(1)</super> aplicable solamente a instalaciones que no sean propiedad de empresas de transporte y distribución electrica
    </para>
    <nextPage />
    <xsl:apply-templates select="defectes" mode="story" />
  </xsl:template>

  <xsl:template match="defectes" mode="story">
    <blockTable colWidths="100mm, 75mm" style="taula_logos">
     <tr>
        <td><image file="addons/giscedata_revisions_oca/report/gisce_oca.tif"
        width="45mm" height="12.5mm" /></td>
        <td><image file="addons/giscedata_revisions_oca/report/enac_oca.tif"
        width="28mm" height="16.88mm" /></td>
      </tr>
    </blockTable>
    <spacer length="10" />
    <blockTable colWidths="175mm" style="taula_titol">
      <tr><td></td></tr>
      <tr>
        <td>ACTA DE RECONOCIMIENTO PERIÓDICO</td>
      </tr>
      <tr>
        <td>INSTALACIÓN ELÉCTRICA</td>
      </tr>
      <tr><td></td></tr>
    </blockTable>
    <spacer length="6" />
    <!--<para alignment="center" fontName="Helvetica-Bold">Reconocimiento de Centros Transformadores <xsl:value-of select="ct/tipus/general" /></para>-->
    <blockTable style="cap_taula_defectes" colWidths="175mm">
        <tr><td>Líneas de Alta Tensión</td></tr>
    </blockTable>
    <spacer length="10" />
    <para alignment="center" fontName="Helvetica-Bold">DEFECTOS AL LAS LÍNEAS DE ALTA TENSIÓN - LINEA AT <xsl:value-of select="../linia/codi" /></para>
    <blockTable style="taula_defectes" colWidths="2cm,1cm,3.5cm,1cm,5cm,2cm,3cm" repeatRows="0">
      <tr>
        <td>Código</td>
        <td>Código soporte</td>
        <td>Situación línea</td>
        <td>Tipo</td>
        <td>Descripción</td>
        <td>CORRECCIÓN</td>
        <td>Límite Reparación</td>
      </tr>
        <xsl:apply-templates select="defecte" mode="story" />
    </blockTable>

    <blockTable style="taula_tecnic" colWidths="5cm, 5cm, 3cm, 4.5cm">
      <tr>
        <td>Técnico:</td>
        <td></td>
        <td></td>
        <td>Fecha de reconocimiento:</td>
      </tr>
      <tr>
        <td><xsl:value-of select="../tecnic/nom" /></td>
        <td></td>
        <td></td>
        <td><xsl:value-of select="concat(substring(../data, 9, 2), '/', substring(../data, 6, 2), '/', substring(../data, 1, 4))" /></td>
      </tr>
    </blockTable>
    <nextFrame />
  </xsl:template>

  <xsl:template match="defecte" mode="story">
    <xsl:if test="intern!=1">
    <tr>
      <td><xsl:value-of select="codi" /></td>
      <td><xsl:value-of select="suport" /></td>
      <xsl:choose>
        <xsl:when test="string-length(../../linia/descripcio) &gt; 15">
          <td><xsl:value-of select="concat(substring(../../linia/descripcio, 1, 15), '...')" /></td>
        </xsl:when>
        <xsl:otherwise>
          <tr>
      <td><xsl:value-of select="../../linia/descripcio" /></td>
          </tr>
        </xsl:otherwise>
      </xsl:choose>
      <td><xsl:value-of select="tipus" /></td>
      <xsl:choose>
        <xsl:when test="string-length(descripcio) &gt; 5">
          <td><xsl:value-of select="concat(substring(descripcio, 1, 30), '...')" /></td>
        </xsl:when>
        <xsl:otherwise>
          <tr>
            <td><xsl:value-of select="descripcio" /></td>
          </tr>
        </xsl:otherwise>
      </xsl:choose>
      <td><xsl:value-of select="valoracio" /></td>
      <td>
        <xsl:choose>
          <xsl:when test="temps_repa_dimensio = 'month'">
              <para style="symbol">
                  <xsl:value-of select="concat(temps_repa_unitat,' meses')" />
              </para>
          </xsl:when>
          <xsl:when test="temps_repa_dimensio = 'year'">
              <para style="symbol">
                  <xsl:value-of select="concat(temps_repa_unitat,' años')" />
              </para>
          </xsl:when>
          <xsl:otherwise>
              <para style="symbol">
                  <xsl:value-of select="concat(temps_repa_unitat,' dias')" />
              </para>
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td>
      <xsl:choose>
          <xsl:when test="string-length(observacions) &gt; 1">
              <xsl:value-of select="observacions" /><xsl:text>&#xa;</xsl:text>
          </xsl:when>
      </xsl:choose>
      </td>
      <td></td>
      <td></td>
      <td></td>
    </tr>

    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
