## -*- coding: utf-8 -*-
<%
    from datetime import datetime,date
    from babel.dates import format_date, format_datetime, format_time

    def get_tipus_ct(codiTipus):
        cts_subtipus_obj = objects[0].pool.get("giscedata.cts.subtipus")
        ids = cts_subtipus_obj.search(cursor,uid,[("codi",'=',codiTipus)])
        return cts_subtipus_obj.browse(cursor,uid,ids[0]).tipus_id.name

    def te_defectes_interns(revisio):
        return revisio.num_ind_reparats.split('/')[-1] == '0'

    def get_qualificacio_segons_defectes(revisio):
        res = _("favorable")
        for defecte in revisio.defectes_ids:
            if not defecte.intern and not defecte.reparat:
                valoracio = str(defecte.valoracio.name)
                if "Grave" in valoracio or "Muy grave" in valoracio:
                    res = _("condicionada con defectos graves")
                    break
                if "Leve" in valoracio:
                    res = _("favorable con defectos leves")
        return res

    def te_defectes_greus_i_estan_reparats(revisio):
        te_defectes_greus = False
        for defecte in revisio.defectes_ids:
            valoracio = str(defecte.valoracio.name)
            if not defecte.intern and ("Grave" in valoracio or "Muy grave" in valoracio):
                te_defectes_greus = True
                if not defecte.reparat:
                    return False
        return te_defectes_greus

    def te_defectes_lleus_sense_reparar(revisio):
        for defecte in revisio.defectes_ids:
            valoracio = str(defecte.valoracio.name)
            if not defecte.intern and "Leve" in valoracio:
                if not defecte.reparat:
                    return True
        return False

%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/reconeixement_certificats.css"/>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions_oca/report/reconeixement_certificats.css"/>
</head>
<body>
    <%
        revisions = sorted(objects, key=lambda elem: elem.name.name)
    %>
    %for rev in revisions:
        <div class="top_margin"></div>
        <%
            te_defectes = not te_defectes_interns(rev)
            if te_defectes:
                tots_reparats = rev.ind_reparats
            else:
                tots_reparats = True

            mostrar_acta = te_defectes and not tots_reparats
            segona_revisio = (te_defectes and tots_reparats) or (te_defectes_lleus_sense_reparar(rev) and te_defectes_greus_i_estan_reparats(rev))
        %>
        <div class="lateral_container">
            <div class="lateral">${_("Format doc.")} 13.3-70_02</div>
        </div>
        <div class="images_header padded_text">
            <div class="leftplacement">
                <img src="${addons_path}/giscedata_revisions/report/gisce_logo.png">
            </div>
            <div class="rightplacement">
                <img src="${addons_path}/giscedata_revisions_oca/report/enac_oc.png">
            </div>
            <div style="clear:both"></div>
        </div>

        <br>
        <table id="title">
            <colgroup>
                <col/>
                <col width="15%"/>
            </colgroup>
            <tr>
                <th>${_("ACTA DE VERIFICACIÓN")}</th>
                <td>☐ ${_(u"INICIAL")}</td>
            </tr>
            <tr>
                <th>${_("INSTALACIÓN ELÉCTRICA")}</th>
                <td>☒ ${_(u"PERIÒDICA")}</td>
            </tr>
        </table>

        <div class="bold_header1">${_("Verificación de Centros Transformadores")}</div>
        <hr>

        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Código de documento")}</div>
                <% prefix_document_code = 'V-P-CT-' if mostrar_acta else 'V2-P-CT-' %>
                <div class="right_col">${prefix_document_code}${rev.name.name} ${rev.trimestre.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Código de revisión")}</div>
                <div class="right_col">${rev.name.name} ${rev.trimestre.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Expediente autorización administrativa")}</div>
                <div class="right_col">${rev.name.expedient_actual if rev.name.expedient_actual else ""}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Fecha de revisión")}</div>
                <%
                    data_obj_rev = datetime.strptime(rev.data, '%Y-%m-%d %H:%M:%S')
                    data_rev = data_obj_rev.strftime('%d/%m/%Y')
                %>
                <div class="right_col">${data_rev}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Hora inicio / hora final de la inspección")}</div>
                <%
                    if rev.data:
                        data_obj = datetime.strptime(rev.data, '%Y-%m-%d %H:%M:%S')
                        hora_inici = data_obj.strftime('%H:%M')
                    else:
                        hora_inici = ""

                    if rev.data_fi:
                        data_obj = datetime.strptime(rev.data_fi, '%Y-%m-%d %H:%M:%S')
                        hora_final = data_obj.strftime('%H:%M')
                    else:
                        hora_final = False
                %>
                <div class="right_col">${hora_inici} ${" / " + hora_final if hora_final else ""} horas</div>
            </div><br>
            %if segona_revisio:
                <div class="new_row padded_text">
                    <div class="left_col">${_("Fecha comprobación reparación defectos")}</div>
                    <%
                        data_obj_compr = datetime.strptime(rev.data_comprovacio, '%Y-%m-%d %H:%M:%S')
                        data_compr = data_obj_compr.strftime('%d/%m/%Y')
                    %>
                    <div class="right_col">${data_compr}</div>
                </div><br>
                <div class="new_row padded_text">
                    <div class="left_col">${_("Tipo de comprobación")}</div>
                    <div class="right_col">${_("2a visita")}</div>
                </div><br>
            %endif
            <div class="new_row padded_text">
                <div class="left_col">${_("Unidad de reconocimiento")}</div>
                <div class="right_col">${rev.name.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Empresa titular")}</div>
                <div class="right_col">${company.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Dirección para notificaciones")}</div>
                <% company_address = company.partner_id %>
                <div class="right_col">
                    ${company_address.address[0].street} - ${company_address.address[0].zip} - ${company_address.city}
                </div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Organismo de inspección autorizado")}</div>
                <div class="right_col">${rev.tecnic.organisme}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Dirección Organismo de inspección")}</div>
                <div class="right_col">${rev.tecnic.domicili}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Nombre y apellidos del inspector")}</div>
                <div class="right_col">${rev.tecnic.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("DNI")}</div>
                <div class="right_col">${rev.tecnic.dni}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Titulación")}</div>
                <div class="right_col">${rev.tecnic.titolacio}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Número de colegiado")}</div>
                <div class="right_col">${rev.tecnic.n_collegiat}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Dirección para notifiacions")}</div>
                <div class="right_col">${rev.tecnic.domicili}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Teléfono")}</div>
                <div class="right_col">${rev.tecnic.telefon}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Correo electrónico")}</div>
                <div class="right_col">${rev.tecnic.email}</div>
            </div><br>
        </div>

        <br class="new_line">

        <div class="bold_header2 padded_text" >${_("Datos de la instalación")}</div><hr>
        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Instalación")}<br></div>
                <div class="right_col">${_("Centro Transformador")} ${rev.name.name}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Denominación")}<br></div>
                <div class="right_col">${rev.name.descripcio}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Población")}<br></div>
                <div class="right_col">${rev.name.id_poblacio.name}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Municipio")}<br></div>
                <div class="right_col">${rev.name.id_municipi.name}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tipo de Centro Transformador")}<br></div>
                <div class="right_col">${get_tipus_ct(rev.name.id_subtipus.codi)}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tensión de primario")}<br></div>
                <div class="right_col">${rev.name.tensio_p} V</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tensión de secundario")}<br></div>
                %if rev.name.tensio_s:
                    <div class="right_col">${rev.name.tensio_s} V</div>
                %else:
                    <div class="right_col">-</div>
                %endif
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Salidas de baja tensión")}<br></div>
                <%
                    if len(rev.name.sortidesbt) == 0:
                        num_sortides_bt = 0
                    else:
                        num_sortides_bt = len([x for x in rev.name.sortidesbt.seccio if x != False])
                %>
                <div class="right_col">${num_sortides_bt}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Fecha de puesta en marcha")}<br></div>
                <%
                    data_pm = '-'
                    if rev.name.data_pm:
                        data_obj = datetime.strptime(rev.name.data_pm, '%Y-%m-%d')
                        data_pm = data_obj.strftime('%d/%m/%Y')
                %>
                <div class="right_col">${data_pm}</div>
            </div>
            <table class="padded_text" style="font-size: inherit; width:100%;">
                <tr>
                    <td rowspan="2" style="vertical-align:center; width:40%;">${_("Transformadores")}</td>
                    <td>
                        <table style="font-size: inherit; width:100%;">
                            <% transformadors =  rev.name.transformadors or [] %>
                            %for transformador in transformadors:
                                %if transformador.id_estat.codi == 1:
                                    <tr>
                                        <td style="width: 20%;">${_("Núm.")}</td>
                                        <td style="width: 20%; text-align:right">${transformador.name}</td>
                                        <td style="text-align:right">${_("Potencia")}</td>
                                        <td style="text-align:right">${int(transformador.potencia_nominal)} ${_("kVA")}</td>
                                    </tr>
                                %endif
                            %endfor
                            <tr>
                                <td colspan="2">${_("POTENCIA TOTAL")}</td>
                                <td colspan="2" style="text-align:right">${int(rev.name.potencia)} ${_("kVA")}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <div class="bold_header2 padded_text" >
            <sup>(1)</sup>
            ${_("Registro de las últimas operaciones de mantenimiento:")}
        </div><hr>

        <div class="padded_text">${_("Resultados comprobaciones:")}</div>
        <br>

        %if rev.name.mesures:
        <%
            mesura = sorted(rev.name.mesures, key=lambda mesura: mesura.name)[-1]
        %>
        <div class="padded_text">
            <table class="dades_mesura">
                <tr>
                    <th>${_("Tomas de Tierra")}</th>
                    <th>${_("Tensiones de Seguridad")}</th>
                    <th style="width:25%">${_("Protecciones Sobreintensidades")}</th>
                    <th>${_("Aislamiento del transformador")}</th>
                </tr>
                <tr>
                    <td>
                        ${_("Protección (Ω):")}
                        <span class="rightplacement">${mesura.rpt_proteccio or ''}</span>
                    </td>
                    <td>
                        ${_("Tensión de Paso (V):")}
                        <span class="rightplacement">${mesura.tpc_pas_exterior}</span>
                    </td>
                    <td>
                        ${_("Relé Interruptor Auto.:")}
                        <span class="rightplacement">${_("Correcto") if mesura.ps_interruptor else _("N/C")}</span>
                    </td>
                    <td>
                        ${_("Primario-Secundario (MΩ):")}
                        <span class="rightplacement">${mesura.ailla_trafo_primari_secundari or _("N/C")}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        ${_("Neutro (Ω):")}
                        <span class="rightplacement">${mesura.rpt_neutre or _("N/A")}</span>
                    </td>
                    <td>
                        ${_("Tensión de contacto (V):")}
                        <span class="rightplacement">${mesura.tpc_contacte}</span>
                    </td>
                    <td>
                        ${_("Ruptofusible A.P.R.:")}
                        <span class="rightplacement">${_("Correcto") if mesura.ps_ruptofusible else _("N/C")}</span>
                    </td>
                    <td>
                        ${_("Primario-Tierra (MΩ):")}
                        <span class="rightplacement">${mesura.ailla_trafo_primari_terra or _("N/C")}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        ${_("Autoválvulas (Ω):")}
                        <span class="rightplacement">${mesura.rpt_autovalvules or _("N/A")}</span>
                    </td>
                    <td></td>
                    <td>
                        ${_("Temperatura:")}
                        <span class="rightplacement">${_("Correcto") if mesura.ps_temperatura else _("N/C")}</span>
                    </td>
                    <td>
                        ${_("Secundario-Tierra (MΩ):")}
                        <span class="rightplacement">${mesura.ailla_trafo_secundari_terra or _("N/C")}</span>
                    </td>
                </tr>
            </table>
        </div>
        %else:
            ${_("No existen medidas.")}
        %endif
        <br class="new_line">

        <div class="bold_header2 padded_text" >${_("Observaciones y detalles de acciones no realizadas:")}</div><hr>
        %if rev.observacions:
            <div class="padded_text justified">
                ${rev.observacions}
            </div>
        %endif

        <br class="new_line">

        <div class="wrap">
            <div class="bold_header2 padded_text">${_("Certificación")}</div><hr>
            <p class="padded_text justified">
                ${_("De acuerdo con los Reglamentos que le son de aplicación, el inspector que suscribe este certificado, ha realizado las comprobaciones y controles que establece la legislación vigente para el tipo de inspección indicado, según:")}
            </p>

            <%
                reglaments = [reg.name for reg in rev.reglaments]
                regl_index = 0
            %>
            <table class="reglaments">
                <tr>
                    %for regl in reglaments:
                        %if regl_index == 3:
                            </tr><tr>
                        <%
                            regl_index = 0
                        %>
                        %endif
                        <td>
                            <div class="checkbox">X</div>
                            ${regl}
                        </td>
                        <%
                            regl_index += 1
                        %>
                    %endfor
                </tr>
            </table>
            <br>

            <div class="bold_header2 padded_text">${_("CERTIFICA:")}</div>
            <p class="padded_text justified">
                ${_("Que la instalación inspeccionada obtiene la calificación de")} ${get_qualificacio_segons_defectes(rev)}${_(", referente a seguridad del funcionamiento y al cumplimiento de las disposiciones reglamentarias.")}
            </p>

            <br><br><br><br>

            <div>
                <span class="leftplacement padded_text">${_("Firma")}</span>
                <%
                    if segona_revisio:
                        data_obj = data_obj_compr
                    else:
                        data_obj = data_obj_rev

                    d = date(data_obj.year, data_obj.month, data_obj.day)
                    data_document = format_date(d, "d 'de' MMMM 'de' yyyy", locale='es_ES')
                %>
                <span class="rightplacement padded_text document_date">${_("Girona")}, ${data_document}</span>
            </div>
            <br class="new_line">
            <div class="padded_text"><br>
                ${_("Este documento queda a disposición del organismo competente")}
            </div>
            <div class="padded_text">
                ${_("El presente informe tiene una validez de 3 años a partir de la fecha de revisión.")}
            </div>
        </div>
        <div class="padded_text notes_peu_pagina">
            <sup>(1)</sup> Aplicable solamente a instalaciones que no sean
            propiedad de empresas de transporte distribución eléctrica.
        </div>

        <p style="page-break-after:always;"></p>
        %if mostrar_acta:
            <div class="top_margin"></div>
            <div class="lateral_container">
                <div class="lateral">${_("Format doc.")} 13.3-70_02</div>
            </div>
            <div class="images_header padded_text">
                <div class="leftplacement">
                    <img src="${addons_path}/giscedata_revisions/report/gisce_logo.png"  >
                </div>
                <div class="rightplacement">
                    <img src="${addons_path}/giscedata_revisions_oca/report/enac_oc.png">
                </div>
                <div style="clear:both"></div>
            </div>

            <br>
            <div class="grey_header">
                <div class="bold_header1">
                    ${_("ACTA")} ${_("DE VERIFICACIÓN PERIÓDICA")}<br>
                    ${_("INSTALACIÓN ELÉCTRICA")}
                </div>
            </div>

            <div class="bold_header1">${_("Verificación de Centros Transformadores")}</div>

            <div class="border_gray">
                <div class="titol_defectes">
                    ${_("DEFECTOS DEL CENTRO TRANSFORMADOR")} ${rev.name.name}
                </div>
            </div>
            <div class="border_gray without_border_top">
                <table class="llista_defectes">
                    <tr class="bold_text">
                        <td>${_("Código")}</td>
                        <td>${_("Tipo")}</td>
                        <td>${_("Descripción")}</td>
                        <td>${_("Clasificación")}</td>
                        <td>${_("Límite reparación")}</td>
                    </tr>
                    %for defecte in rev.defectes_ids:
                        %if defecte.estat == 'B' and not defecte.intern and not defecte.reparat:
                            <tr>
                                <td>${defecte.id}</td>
                                <td>${defecte.codi}</td>
                                <%
                                    if len(defecte.descripcio) > 100:
                                        defecte_descripcio = defecte.descripcio[:100] + "..."
                                    else:
                                        defecte_descripcio = defecte.descripcio
                                %>
                                <td style="text-align:left">${defecte_descripcio}</td>
                                <td>${defecte.valoracio.name}</td>
                                <%
                                    data_obj = datetime.strptime(defecte.data_limit_reparacio, '%Y-%m-%d %H:%M:%S')
                                    data_limit_reparacio = data_obj.strftime('%d/%m/%Y')
                                %>
                                <td>${data_limit_reparacio}</td>
                            </tr>
                            <tr>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td style="text-align:left">${defecte.observacions}</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                            </tr>
                        %endif
                    %endfor
                </table>
            </div>
            <%
                if segona_revisio:
                    data_verificacio = data_compr
                else:
                    data_verificacio = data_rev
            %>
            <div class="border_gray without_border_top">
                <table class="taula_tecnic">
                    <tr>
                        <td>${_("Técnico:")}</td>
                        <td>${_("Titulación:")}</td>
                        <td>${_("Colegiado:")}</td>
                        <td>${_("Fecha de inspección")}:</td>
                    </tr>
                    <tr class="bold_text">
                        <td>${rev.tecnic.name}</td>
                        <td>${rev.tecnic.titolacio}</td>
                        <td>${rev.tecnic.n_collegiat}</td>
                        <td>${data_verificacio}</td>
                    </tr>
                </table>
            </div>
        %endif

        %if rev != revisions[-1]:
            <p style="page-break-after:always;"></p>
        %endif
    %endfor
</body>
</html>
