from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
        })


webkit_report.WebKitParser(
    'report.giscedata.revisions.oca.verificacions.at',
    'giscedata.revisions.at.revisio',
    'giscedata_revisions_oca/report/guia_verificacions_at.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.reconeixement.certificats.oca',
    'giscedata.revisions.ct.revisio',
    'giscedata_revisions_oca/report/reconeixement_certificats.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.reconeixement.certificats_at.oca',
    'giscedata.revisions.at.revisio',
    'giscedata_revisions_oca/report/reconeixement_certificats_at.mako',
    parser=report_webkit_html
)
