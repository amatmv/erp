<%
    from datetime import datetime,date
    from babel.dates import format_date, format_datetime, format_time

    def te_defectes_interns(revisio):
        return revisio.num_ind_reparats.split('/')[-1] == '0'

    def get_qualificacio_segons_defectes(revisio):
        for defecte in revisio.defectes_ids:
            if not defecte.intern and not defecte.reparat:
                valoracio = str(defecte.valoracio.name)
                if "Grave" in valoracio or "Muy grave" in valoracio:
                    return _("condicionada con defectos graves")
                if "Leve" in valoracio:
                    return _("favorable con defectos leves")
        return _("favorable")

    def te_defectes_greus_i_estan_reparats(revisio):
        te_defectes_greus = False
        for defecte in revisio.defectes_ids:
            valoracio = str(defecte.valoracio.name)
            if not defecte.intern and ("Grave" in valoracio or "Muy grave" in valoracio):
                te_defectes_greus = True
                if not defecte.reparat:
                    return False
        return te_defectes_greus

    def te_defectes_lleus_sense_reparar(revisio):
        for defecte in revisio.defectes_ids:
            valoracio = str(defecte.valoracio.name)
            if not defecte.intern and "Leve" in valoracio:
                if not defecte.reparat:
                    return True
        return False

%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/reconeixement_certificats.css"/>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions_oca/report/reconeixement_certificats.css"/>
</head>
<body>
    <%
        revisions = sorted(objects, key=lambda elem: elem.name.name)
    %>
    %for rev in revisions:
        <div class="top_margin"></div>
        <%
            te_defectes = not te_defectes_interns(rev)
            if te_defectes:
                tots_reparats = rev.ind_reparats
            else:
                tots_reparats = True

            mostrar_acta = te_defectes and not tots_reparats
            segona_revisio = (te_defectes and tots_reparats) or (te_defectes_lleus_sense_reparar(rev) and te_defectes_greus_i_estan_reparats(rev))
        %>
        <div class="lateral_container">
            <div class="lateral">${_("Format doc.")} 13.3-71_01</div>
        </div>
        <div class="images_header padded_text">
            <div class="leftplacement">
                <img src="${addons_path}/giscedata_revisions/report/gisce_logo.png">
            </div>
            <div class="rightplacement">
                <img src="${addons_path}/giscedata_revisions_oca/report/enac_oc.png">
            </div>
            <div style="clear:both"></div>
        </div>

        <br>
        <div class="grey_header">
            <div class="bold_header1">
                ${_("ACTA")} ${_("DE VERIFICACIÓN PERIÓDICA")}<br>
                ${_("INSTALACIÓN ELÉCTRICA")}
            </div>
        </div>

        <div class="bold_header1">${_("Verificación de Líneas de Alta Tensión")}</div>
        <hr>

        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Código de documento")}</div>
                <div class="right_col">C-AT-${rev.name.name} ${rev.trimestre.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Código de revisión")}</div>
                <div class="right_col">${rev.name.name} ${rev.trimestre.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Expediente autorización administrativa")}</div>
                <div class="right_col">${rev.name.expedients_string if rev.name.expedients_string else ""}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Fecha de revisión")}</div>
                <%
                    data_obj_rev = datetime.strptime(rev.data, '%Y-%m-%d %H:%M:%S')
                    data_rev = data_obj_rev.strftime('%d/%m/%Y')
                %>
                <div class="right_col">${data_rev}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Hora inicio / hora final de la inspección")}</div>
                <%
                    if rev.data:
                        data_obj = datetime.strptime(rev.data, '%Y-%m-%d %H:%M:%S')
                        hora_inici = data_obj.strftime('%H:%M')
                    else:
                        hora_inici = ""

                    if rev.data_fi:
                        data_obj = datetime.strptime(rev.data_fi, '%Y-%m-%d %H:%M:%S')
                        hora_final = data_obj.strftime('%H:%M')
                    else:
                        hora_final = False
                %>
                <div class="right_col">${hora_inici} ${" / " + hora_final if hora_final else ""} horas</div>
            </div><br>
            %if segona_revisio:
                <div class="new_row padded_text">
                    <div class="left_col">${_("Fecha comprobación reparación defectos")}</div>
                    <%
                        data_obj_compr = datetime.strptime(rev.data_comprovacio, '%Y-%m-%d %H:%M:%S')
                        data_compr = data_obj_compr.strftime('%d/%m/%Y')
                    %>
                    <div class="right_col">${data_compr}</div>
                </div><br>
                <div class="new_row padded_text">
                    <div class="left_col">${_("Tipo de comprobación")}</div>
                    <div class="right_col">${_("2a visita")}</div>
                </div><br>
            %endif
            <div class="new_row padded_text">
                <div class="left_col">${_("Unidad de reconocimiento")}</div>
                <div class="right_col">LAT-${rev.name.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tipo de instalación")}</div>
                <div class="right_col">${_("Línea aérea de alta tensión")}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Empresa titular")}</div>
                <div class="right_col">${company.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Dirección para notificaciones")}</div>
                <% company_address = company.partner_id %>
                <div class="right_col">
                    ${company_address.address[0].street} - ${company_address.address[0].zip} - ${company_address.city}
                </div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Organismo de inspección autorizado")}</div>
                <div class="right_col">${rev.tecnic.organisme}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Dirección Organismo de inspección")}</div>
                <div class="right_col">${rev.tecnic.domicili}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Nombre y apellidos del inspector")}</div>
                <div class="right_col">${rev.tecnic.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("DNI")}</div>
                <div class="right_col">${rev.tecnic.dni}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Titulación")}</div>
                <div class="right_col">${rev.tecnic.titolacio}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Número de colegiado")}</div>
                <div class="right_col">${rev.tecnic.n_collegiat}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Dirección para notifiaciones")}</div>
                <div class="right_col">${rev.tecnic.domicili}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Teléfono")}</div>
                <div class="right_col">${rev.tecnic.telefon}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Correo electrónico")}</div>
                <div class="right_col">${rev.tecnic.email}</div>
            </div><br>
        </div>

        <br class="new_line">

        <div class="bold_header2 padded_text" >${_("Datos de la instalación")}</div><hr>
        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Instalación")}<br></div>
                <div class="right_col">${_("Línea AT")} ${rev.name.name}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Emplazamiento")}<br></div>
                <div class="right_col">${rev.name.descripcio}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Población")}<br></div>
                <div class="right_col">${rev.name.poblacio.name}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Municipio")}<br></div>
                <div class="right_col">${rev.name.municipi.name}</div>
            </div>
        </div>

        <br class="new_line">
        <br class="new_line">
        <div class="bold_header2 padded_text" >${_("Características técnicas principales")}</div><hr>
        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Origen de la línea")}<br></div>
                <div class="right_col">${rev.name.origen}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Final de la línea")}<br></div>
                <div class="right_col">${rev.name.final}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tensión de servicio")}<br></div>
                <div class="right_col">${rev.name.tensio} V</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Sección conductores")}<br></div>
                <div class="right_col">${rev.name.seccio_string}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Longitud Aérea")}<br></div>
                <div class="right_col">
                    ${0 if rev.name.longitud_aeria_cad == 0 else rev.name.longitud_aeria_cad} m
                </div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Longitud Subterránea")}<br></div>
                <div class="right_col">
                    ${0 if rev.name.longitud_sub_cad == 0 else rev.name.longitud_sub_cad} m
                </div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Soportes")}<br></div>
                <div class="right_col">
                    ${_("Metálicos:")}
                    ${rev.name.n_suports_metallics if rev.name.n_suports_metallics else 0};
                    ${_("Hormigón:")}
                    ${rev.name.n_suports_formigo if rev.name.n_suports_formigo else 0};
                    ${_("Madera:")}
                    ${rev.name.n_suports_fusta if rev.name.n_suports_fusta else 0}
                </div>
            </div>
        </div>

        <br class="new_line">
        <br class="new_line">

        <div class="bold_header2 padded_text"><sup>(1)</sup> ${_("Registro de las últimas operaciones de mantenimiento:")}</div><hr>
        <br>

        <div class="bold_header2 padded_text" >${_("Observaciones y detalles de acciones no realizadas:")}</div><hr>
        %if rev.observacions:
            <div class="padded_text justified">
                ${rev.observacions}
            </div>
        %endif

        <br class="new_line">

        <div class="wrap">
            <div class="bold_header2 padded_text">${_("Certificación")}</div><hr>
            <p class="padded_text justified">
                ${_("De acuerdo con los Reglamentos que le son de aplicación, el inspector que suscribe este certificado, ha realizado las comprobaciones y controles que establece la legislación vigente para el tipo de inspección indicado, según:")}
            </p>

            <%
                reglaments = [reg.name for reg in rev.reglaments]
                regl_index = 0
            %>
            <table class="reglaments">
                <tr>
                    %for regl in reglaments:
                        %if regl_index == 3:
                            </tr><tr>
                        <%
                            regl_index = 0
                        %>
                        %endif
                        <td>
                            <div class="checkbox">X</div>
                            ${regl}
                        </td>
                        <%
                            regl_index += 1
                        %>
                    %endfor
                </tr>
            </table>
            <br>

            <div class="bold_header2 padded_text">${_("CERTIFICA:")}</div>
            <p class="padded_text justified">
                ${_("Que la instalación inspeccionada obtiene la calificación de")} ${get_qualificacio_segons_defectes(rev)}${_(", referente a seguridad del funcionamiento y al cumplimiento de las disposiciones reglamentarias.")}
            </p>

            <br><br><br><br>

            <div>
                <span class="leftplacement padded_text">${_("Firma")}</span>
                <%
                    if segona_revisio:
                        data_obj = data_obj_compr
                    else:
                        data_obj = data_obj_rev

                    d = date(data_obj.year, data_obj.month, data_obj.day)
                    data_document = format_date(d, "d 'de' MMMM 'de' yyyy", locale='es_ES')
                %>
                <span class="rightplacement padded_text document_date">${_("Girona")}, ${data_document}</span>
            </div>
            <br class="new_line">
            <div class="padded_text"><br>
                ${_("Este documento queda a disposición del organismo competente")}
            </div>
            <div class="padded_text">
                ${_("El presente informe tiene una validez de 3 años a partir de la fecha de revisión.")}
            </div>
        </div>
        <div class="padded_text notes_peu_pagina">
            <sup>(1)</sup> aplicable solamente a instalaciones que no sean
            propiedad de empresas de transporte distribución eléctrica.
        </div>

        <p style="page-break-after:always;"></p>
        %if mostrar_acta:
            <div class="top_margin"></div>
            <div class="lateral_container">
                <div class="lateral">${_("Format doc.")} 13.3-71_01</div>
            </div>
            <div class="images_header padded_text">
                <div class="leftplacement">
                    <img src="${addons_path}/giscedata_revisions/report/gisce_logo.png">
                </div>
                <div class="rightplacement">
                    <img src="${addons_path}/giscedata_revisions_oca/report/enac_oc.png">
                </div>
                <div style="clear:both"></div>
            </div>

            <br>
            <div class="grey_header">
                <div class="bold_header1">
                    ${_("ACTA")} ${_("DE VERIFICACIÓN PERIÓDICA")}<br>
                    ${_("INSTALACIÓN ELÉCTRICA")}
                </div>
            </div>

            <div class="bold_header1">${_("Verificación de Líneas de Alta Tensión")}</div>

            <div class="border_gray">
                <div class="titol_defectes">
                    ${_("DEFECTOS A LAS LÍNEAS DE ALTA TENSIÓN - LÍNEA AT")} ${rev.name.name}
                </div>
            </div>
            <div class="border_gray without_border_top">
                <table class="llista_defectes">
                    <tr class="bold_text">
                        <td style="width:2%">${_("Código")}</td>
                        <td style="width:2%">${_("Soporte")}</td>
                        <td style="width:5%">${_("Situación línea")}</td>
                        <td style="width:2%">${_("Tipo")}</td>
                        <td style="width:20%">${_("Descripción")}</td>
                        <td style="width:3%">${_("Clasificación")}</td>
                        <td style="width:3%">${_("Límite reparación")}</td>
                    </tr>
                    %for defecte in rev.defectes_ids:
                        %if not defecte.intern and not defecte.reparat:
                            <tr>
                                <td>${defecte.id}</td>
                                <td>${defecte.suport.name}</td>
                                <td>${rev.name.descripcio}</td>
                                <td>${defecte.defecte_id.name}</td>
                                <%
                                    if len(defecte.defecte_id.descripcio) > 55:
                                        defecte_descripcio = defecte.defecte_id.descripcio[:55] + "..."
                                    else:
                                        defecte_descripcio = defecte.defecte_id.descripcio
                                %>
                                <td style="text-align:left">${defecte_descripcio}</td>
                                <td style="text-align:left">${defecte.valoracio.name}</td>
                                <%
                                    data_obj = datetime.strptime(defecte.data_limit_reparacio, '%Y-%m-%d %H:%M:%S')
                                    data_limit_reparacio = data_obj.strftime('%d/%m/%Y')
                                %>
                                <td>${data_limit_reparacio}</td>
                            </tr>
                            <tr>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td style="text-align:left">${defecte.observacions}</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                            </tr>
                        %endif
                    %endfor
                </table>
            </div>
            <%
                if segona_revisio:
                    data_verificacio = data_compr
                else:
                    data_verificacio = data_rev
            %>
            <div class="border_gray without_border_top">
                <table class="taula_tecnic">
                    <tr>
                        <td>${_("Técnico:")}</td>
                        <td>${_("Titulación:")}</td>
                        <td>${_("Colegiado:")}</td>
                        <td>${_("Fecha de verificación:")}</td>
                    </tr>
                    <tr class="bold_text">
                        <td>${rev.tecnic.name}</td>
                        <td>${rev.tecnic.titolacio}</td>
                        <td>${rev.tecnic.n_collegiat}</td>
                        <td>${data_verificacio}</td>
                    </tr>
                </table>
            </div>
        %endif

        %if rev != revisions[-1]:
            <p style="page-break-after:always;"></p>
        %endif
    %endfor
</body>
</html>
