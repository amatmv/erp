<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list"/>
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template pageSize="(19cm,297mm)" leftMargin="3cm" rightMargin="3cm" showBoundary="0">
        <pageTemplate id="main">
          <frame id="body" x1="1cm" y1="5mm" width="175mm" height="287mm" />

          <pageGraphics>
          	<setFont name="Helvetica" size="8" />
          	<drawRightString x="18.5cm" y="0.5cm">Página <pageNumber /></drawRightString>
            <translate dx="-168mm" dy="170mm" />
            <rotate degrees="-90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="0mm" y="170mm">doc. 13.3-61_03 </drawString>
            <rotate degrees="90" />
            <setFont name="Helvetica" size="6" />
          	<drawRightString x="34.8cm" y="10cm">núm.:OC-I/255</drawRightString>
          </pageGraphics>
        </pageTemplate>
      </template>
      
      <stylesheet>
        <paraStyle name="text"
					fontName="Helvetica"
					fontSize="8.5"
                    alignment="justify"
				    leading="10"
                />
        <paraStyle name="cert"
					fontName="Helvetica"
					fontSize="8"
                    alignment="justify"
				    leading="10"
                />
        <paraStyle name="subtitol"
					fontName="Helvetica-Bold"
					fontSize="8"
                    alignment="justify"
				/>
        <paraStyle name="obser"
					fontName="Helvetica"
					fontSize="8"
                    alignment="justify"
				/>
        <paraStyle name="footnote"
					fontName="Helvetica"
					fontSize="7"
                    alignment="justify"
				/>
        <paraStyle name="symbol"
					fontName="Helvetica"
					fontSize="8"
                    alignment="center"
				/>
        <paraStyle name="petit"
					fontName="Helvetica"
					fontSize="6.5"
                    alignment="RIGHT"
				/>

        <blockTableStyle id="taula_logos">
          <blockAlignment value="LEFT" start="0,0" stop="0,0" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_titol">
          <blockAlignment value="CENTER" />
          <blockTopPadding length="0" />
          <blockLeftPadding length="0" />
          <blockRightPadding length="0" />
          <blockBottomPadding length="0" />
          <blockBackground colorName="silver" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockFont name="Helvetica-Bold" size="9" />
          <blockLeading length="4" start="0,0" stop="0,0" />
          <blockLeading length="4" start="0,2" stop="0,2" />
        </blockTableStyle>

        <blockTableStyle id="taula_contingut">
          <blockBottomPadding length="1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="1,0" />
          <blockFont name="Helvetica" size="8.5"/>
          <blockLeading length="7" />

          <blockLeading length="0.000001" start="0,0" stop="1,0" />

          <blockLeading length="20" start="0,16" stop="1,16" />
          <lineStyle kind="LINEBELOW" colorName="black" start="0,17" stop="1,17" />
          <blockFont name="Helvetica-Bold" start="0,17" stop="1,17"/>
          <blockLeading length="10" start="0,17" stop="1,17" />
          <blockValing value="TOP" start="0,27" stop="1,27" />
        </blockTableStyle>

        <blockTableStyle id="taula_recer">
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="1,0" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="1,0" size="8"/>
          <blockLeading length="10" start="0,0" stop="1,0" />

          <blockLeading length="0.000001" start="0,1" stop="1,1" />

          <blockFont name="Helvetica-Bold" start="0,2" stop="1,2" size="8"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,2" stop="1,2" />
          <blockLeading length="10" start="0,2" stop="1,2"/>
        </blockTableStyle>

        <blockTableStyle id="taula_certi">
          <blockFont name="Helvetica" size="8.5" />
        </blockTableStyle>

        <blockTableStyle id="taula_certi_column">
          <blockFont name="Helvetica" size="8.5" />
          <blockRightPadding length="12mm" />
        </blockTableStyle>

        <blockTableStyle id="taula_trafos">
          <blockFont name="Helvetica" size="8.5" />
          <blockBottomPadding length="1"/>
        </blockTableStyle>
        
        <blockTableStyle id="taula_trafos_pot">
          <blockFont name="Helvetica" size="8.5" />
          <blockFont name="Helvetica-Bold" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_firma">
          <blockFont name="Helvetica" size="8.5" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
        <xsl:apply-templates select="revisio" mode="story" />
      </story>
    </document>
  </xsl:template>

  <xsl:template match="revisio" mode="story">
    <blockTable colWidths="90mm, 75mm" style="taula_logos">
      <tr>
        <td><image file="addons/giscedata_revisions_oca/report/gisce_oca.tif"
        width="45mm" height="12.5mm" /></td>
        <td><image file="addons/giscedata_revisions_oca/report/enac_oca.tif"
        width="28mm" height="16.88mm" /></td>
      </tr>
    </blockTable>
    <spacer length="9" />
    <blockTable colWidths="175mm" style="taula_titol">
      <tr><td></td></tr>
      <tr>
        <td>CERTIFICADO DE RECONOCIMIENTO PERIÓDICO</td>
      </tr>
      <tr>
        <td>INSTALACIÓN ELÉCTRICA</td>
      </tr>
      <tr><td></td></tr>
    </blockTable>
    <spacer length="6" />
    <para alignment="center" fontName="Helvetica-Bold">Reconocimiento de Centros Transformadores <xsl:value-of select="tipus/general" /></para>
    <blockTable colWidths="70mm,105mm" style="taula_contingut">
      <tr><td></td><td></td></tr>
      <tr>
        <td>Código de documento</td>
        <td>C-CT-<xsl:value-of select="concat(ct/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr>
        <td>Código de revisión</td>
        <td><xsl:value-of select="concat(ct/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr>
        <td>Expediente autorización administrativa</td>
        <td><xsl:value-of select="ct/expedient" /></td>
      </tr>
      <tr>
        <td>Fecha de revisión y hora</td>
        <td><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4), ' ', substring(data, 12, 5), ' hores')"/></td>
      </tr>
      <tr>
        <td>Fecha de comprobación reparación de defectos</td>
        <td><xsl:if test="data_comprovacio!=''"><xsl:value-of select="concat(substring(data_comprovacio, 9, 2), '/', substring(data_comprovacio, 6, 2), '/', substring(data_comprovacio, 1, 4))" /></xsl:if></td>
      </tr>
      <tr>
        <td>Unidad de reconocimiento</td>
        <td><xsl:value-of select="ct/codi" /></td>
      </tr>
      <tr>
        <td>Empresa titular</td>
        <td><xsl:value-of select="ct/propietari/nom" /></td>
      </tr>
      <tr>
        <td>Dirección para notificaciones</td>
        <td><xsl:value-of select="concat(ct/propietari/partner/adreca/carrer, ' - ', ct/propietari/partner/adreca/cp, ' - ', ct/propietari/partner/adreca/ciutat)" /></td>
      </tr>
      <tr>
        <td>Organismo de inspección autorizado</td>
        <td><xsl:value-of select="tecnic/organisme" /></td>
      </tr>
      <tr>
        <td>Dirección Organismo de inspección</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr>
        <td>Nombre y apellidos del inspector</td>
        <td><xsl:value-of select="tecnic/nom" /></td>
      </tr>
      <tr>
        <td>Titulación</td>
        <td><xsl:value-of select="tecnic/titolacio" /></td>
      </tr>
      <tr>
        <td>Número de colegiado</td>
        <td><xsl:value-of select="tecnic/ncol" /></td>
      </tr>
      <tr>
        <td>Dirección para notificaciones</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr>
        <td>Teléfono</td>
        <td><xsl:value-of select="tecnic/telefon" /></td>
      </tr>
      <tr>
        <td>Correo electrónico</td>
        <td><xsl:value-of select="tecnic/email" /></td>
      </tr>
      <tr>
        <td>Datos de la instalación</td>
        <td></td>
      </tr>
      <tr>
        <td>Instalación</td>
        <td>Centro Transformador <xsl:value-of select="ct/codi" /></td>
      </tr>
      <tr>
        <td>Emplazamiento</td>
        <td><xsl:value-of select="ct/descripcio" /></td>
      </tr>
      <tr>
        <td>Población</td>
        <td><xsl:value-of select="ct/poblacio" /></td>
      </tr>
      <tr>
        <td>Municipio</td>
        <td><xsl:value-of select="ct/municipi" /></td>
      </tr>
      <tr>
        <td>Tipo de Centro Transformador</td>
        <td><xsl:value-of select="ct/tipus/general" /></td>
      </tr>
      <tr>
        <td>Tensión de primario</td>
        <!-- buscar de totes les connexions quina és la que està connectada
             i mostrar la tensió -->
        <td><xsl:value-of select="ct/tensio_p" /></td>
      </tr>
      <tr>
        <td>Tensión de secundario</td>
        <td><xsl:value-of select="ct/tensio_s" /></td>
      </tr>
      <tr>
        <td>Salidas de baja tensión</td>
        <td><xsl:value-of select="ct/sortides_baixa" /></td>
      </tr>
      <tr>
        <td>Fecha de puesta en marcha</td>
        <!-- Depen de l'expedient -->
        <td><xsl:if test="ct/data_marxa!=''"><xsl:value-of select="concat(substring(ct/data_marxa, 9, 2), '/', substring(ct/data_marxa, 6, 2), '/', substring(ct/data_marxa, 1, 4))"/></xsl:if></td>
      </tr>
      <tr>
        <td>Transformadores</td>
        <td>
          <xsl:apply-templates select="ct/trafos" mode="story" />
        </td>
      </tr>
    </blockTable>
    <spacer length="10" />
    <blockTable colWidths="120mm, 55mm" style="taula_recer">
      <tr>
        <td><para style="subtitol"><super>(1)</super> Registro de las ultimas operaciones de mantenimiento:</para></td>
        <td></td>
      </tr>
      <tr>
        <td><spacer length="20" /></td>
        <td></td>
      </tr>
      <tr>
        <td>Certificación</td>
        <td></td>
      </tr>
    </blockTable>
    <spacer length="5" />
    <para style="text">De acuerdo con los Reglamentos que le son de aplicación, el inspector que suscribe este certificado, ha realizado las comprobaciones y controles que establece la legislación vigente para el tipo de inspección indicado, según:</para>
    <blockTable colWidths="55mm, 55mm" rowHeights="7mm" style="taula_certi">
      <tr>
        <td>
            <blockTable colWidths="5mm, 50mm" style="taula_certi_column">
                <tr>
                    <td><image style="align:left" file="addons/giscedata_revisions/report/squarecross.tif" width="2.5mm" height="2.5mm"/></td>
                    <td><para style="symbol"> - RD 3275/1982 de 12/11/82</para></td>
                </tr>
            </blockTable>
        </td>
        <td>
             <blockTable colWidths="5mm, 52mm" style="taula_certi_column">
                <tr>
                    <td><image style="align:left" file="addons/giscedata_revisions/report/square.tif" width="2.5mm" height="2.5mm"/></td>
                    <td><para style="symbol"> - RD 337/2014 de 09/05/2014</para></td>
                </tr>
            </blockTable>
        </td>
      </tr>
    </blockTable>
    <blockTable colWidths="55mm, 55mm" rowHeights="7mm" style="taula_certi">
      <tr>
        <xsl:if test="ct/tipus/codi=1">
            <td>
                <blockTable colWidths="5mm, 55mm" style="taula_certi_column">
                    <tr>
                        <td><image style="align:left" file="addons/giscedata_revisions/report/square.tif" width="2.5mm" height="2.5mm"/></td>
                        <td><para style="symbol"> - Procedimiento GISCE: 13.3.45</para></td>
                    </tr>
                </blockTable>
            </td>
            <td>
                <blockTable colWidths="5mm, 55mm" style="taula_certi_column">
                    <tr>
                        <td><image style="align:left" file="addons/giscedata_revisions/report/squarecross.tif" width="2.5mm" height="2.5mm"/></td>
                        <td><para style="symbol"> - Procedimiento GISCE: 13.3.47</para></td>
                    </tr>
                </blockTable>
            </td>
        </xsl:if>
        <xsl:if test="ct/tipus/codi=2">
            <td>
                <blockTable colWidths="5mm, 55mm" style="taula_certi_column">
                    <tr>
                        <td><image style="align:left" file="addons/giscedata_revisions/report/squarecross.tif" width="2.5mm" height="2.5mm"/></td>
                        <td><para style="symbol"> - Procedimiento GISCE: 13.3.45</para></td>
                    </tr>
                </blockTable>
            </td>
            <td>
                <blockTable colWidths="5mm, 55mm" style="taula_certi_column">
                    <tr>
                        <td><image style="align:left" file="addons/giscedata_revisions/report/square.tif" width="2.5mm" height="2.5mm"/></td>
                        <td><para style="symbol"> - Procedimiento GISCE: 13.3.47</para></td>
                    </tr>
                </blockTable>
            </td>
        </xsl:if>
      </tr>
    </blockTable>

    <spacer length="12" />
   <para style="cert"><b>CERTIFICA:</b></para>
    <para style="cert">
     Que la instalación inspeccionada es <b>Correcta</b> y obtiene la calificación de <b>FAVORABLE</b> referente a seguridad del funcionamiento y al cumplimineto de las disposiciones reglamentarias.
    </para>

    <spacer length="9" />
    <para style="cert">
      Este documento queda a disposición del organismo competente
      El presente informe tiene una validez de 3 años a partir de la fecha de revisión.
    </para>
    <para style="obser">
      Observaciones y explicación de acciones no realizadas:
    </para>
    <spacer length="5" />
    <blockTable style="taula_firma" colWidths="65mm,100mm">
      <tr>
        <td>Firma</td><td>Girona, a <xsl:choose><xsl:when test="data_ca2 != ''"><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))" /></xsl:when><xsl:otherwise><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))" /></xsl:otherwise></xsl:choose></td>
      </tr>
    </blockTable>
    <spacer length="55" />
    <para style="footnote">
      <super>(1)</super> aplicable solamente a instalaciones que no sean propiedad de empresas de transporte y distribución electrica
    </para>
    <nextFrame />
  </xsl:template>

  <xsl:template match="trafos" mode="story">
    <xsl:if test="count(trafo[estat/codi=1][reductor=0])&gt;0">
    <blockTable colWidths="2cm,2cm,3cm,4.5cm" style="taula_trafos"> 
      <xsl:for-each select="trafo[estat/codi=1][reductor=0]">
      <tr>
        <td>Núm.</td>
        <td><xsl:value-of select="numero" /></td>
        <td>Potencia</td>
        <td><xsl:value-of select="concat(potencia, ' kVA')" /></td>
      </tr>
      </xsl:for-each>
    </blockTable>
    <blockTable colWidths="7cm, 4.5cm" style="taula_trafos_pot">
      <tr>
        <td>POTENCIA TOTAL</td>
        <td><xsl:value-of select="concat(sum(trafo[estat/codi=1][reductor=0]/potencia), ' kVA')" /></td>
      </tr>
    </blockTable>
    </xsl:if>
    <xsl:if test="count(trafo[estat/codi=1][reductor=1])&gt;0">
    <blockTable colWidths="7cm, 4.5cm" style="taula_trafos_pot">
      <tr>
        <td>Reductores</td>
        <td></td>
      </tr>
    </blockTable>
    <blockTable colWidths="2cm,2cm,3cm,4.5cm" style="taula_trafos"> 
      <xsl:for-each select="trafo[estat/codi=1][reductor=1]">
      <tr>
        <td>Núm.</td>
        <td><xsl:value-of select="numero" /></td>
        <td>Potencia</td>
        <td><xsl:value-of select="concat(potencia, ' kVA')" /></td>
      </tr>
      </xsl:for-each>
    </blockTable>
    <blockTable colWidths="7cm, 4.5cm" style="taula_trafos_pot">
      <tr>
        <td>POTENCIA TOTAL</td>
        <td><xsl:value-of select="concat(sum(trafo[estat/codi=1][reductor=1]/potencia), ' kVA')" /></td>
      </tr>
    </blockTable>
    </xsl:if>
  </xsl:template>


  <xsl:template match="trafo" mode="story">
    <xsl:choose>
      <xsl:when test="count(estat[codi=1])&gt;0">
        <xsl:for-each select="trafo/estat[codi=1]">
          <tr>
            <td>Núm.</td>
            <td><xsl:value-of select="numero" /></td>
            <td>Potencia</td>
            <td><xsl:value-of select="concat(potencia, ' kVA')" /></td>
          </tr>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

</xsl:stylesheet>
