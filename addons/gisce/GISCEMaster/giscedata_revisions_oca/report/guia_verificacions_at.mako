<%
    defectes_obj = objects[0].pool.get("giscedata.revisions.at.tipusdefectes")
    llista_tipus_defectes=[]
    for i in defectes_obj.search(cursor,uid,[('normativa.vigent', '=', True)]):
        llista_tipus_defectes.append(defectes_obj.read(cursor,uid,i,["name","descripcio"]))


    def get_num_defecte(nom):
      return int(nom.split('.')[0])


    llista_tipus_defectes.sort(key=lambda defecte: (get_num_defecte(defecte['name']), defecte['name'].split('.')[1]))

    classes_defectes_castella=["CONDUCTORES","MEDIDA DE TIERRA", "AISLADORES","APOYOS", "VIENTOS", "CIMIENTOS", "CRUZAMIENTOS","PASO POR ZONAS","MANIOBRA Y PROTECCIÓN","CONVERSIONES", "PARALELISMOS","12","13","14","OTROS","16"]

    def get_defectes_suport(suport,revisio):
      defectes_trobats_obj = objects[0].pool.get("giscedata.revisions.at.defectes")
      llista_defectes_trobats=[]
      for i in defectes_trobats_obj.search(cursor,uid,[('suport.name', '=', suport.name),('revisio_id', '=', revisio.id)]):
          llista_defectes_trobats.append(defectes_trobats_obj.read(cursor,uid,i,["observacions","suport","valoracio","defecte_id"]))
      return sorted(llista_defectes_trobats)

    def get_mesura_suport(suport,revisio):
      mesures_obj = objects[0].pool.get("giscedata.at.historicmesures")
      llista_mesures=[]
      for i in mesures_obj.search(cursor,uid,[('suport.id', '=', suport.id),('trimestre', '=', revisio.trimestre.name)]):
          llista_mesures.append(mesures_obj.read(cursor,uid,i,["autovalvules","ferramenta"]))
      if len(llista_mesures) == 0 :
          return False
      return llista_mesures[0]
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
      <style type="text/css">
        ${css}
      </style>
      <link rel="stylesheet" href="${addons_path}/giscedata_revisions_oca/report/estils_guia_verificacions_at.css"/>
    </head>
    <body>
    % for revisioAT in objects:
        % for suportAT in revisioAT.name.suports:
            <div class="capcalera">
                <div class="logo">
                    <img src="${addons_path}/giscedata_revisions_oca/report/gisce_logo.png"/>
                </div>
                <div class="dades_full">
                    ${_("Codi document: 13.3-48_03")}<br>
                    ${_("Guia general verificaciones iniciales y periódicas-LAT")}<br>
                    ${_("aerea<br>")}
                    ${_("Revisión 3")}<br>
                </div>
                <div style="clear: both"></div>
                <table id="table1">
                    <tr>
                        <th colspan="3">13.3-48</th>
                    </tr>
                    <tr>
                        <td width="69%">
                            <p style="font-size: 8px"><b>${_("CODIGO LÍNEA / DESCRIPCION:")}</b></p>
                            <p style="font-size: 10px">${"{0} / {1}".format(revisioAT.name.name,revisioAT.name.descripcio)}</p>
                        </td>
                        <td  width="15%">
                            <p><b>${_("NÚMERO DE APOYO:")}</b></p>
                            <p style="font-size: 10px">${suportAT.name}</p>
                        </td>
                        <td  width="15%">
                            <table id="table2">
                                <tr>
                                    <td colspan="2"><p>${_("APOYO FRECUENTADO")}:</p></td>
                                </tr>
                                <tr>
                                    <td>
                                        ${_("CON Calzado")}<br>
                                        <div class="quadre"></div>
                                    </td>
                                    <td>
                                        ${_("SIN Calzado")}<br>
                                        <div class="quadre"></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="clear: both"></div>
            <table class="taula_info">
                <tr>
                    <th colspan="6"  style="font-size: 10px"> ${_("GUIA GENERAL VERIFICACIONES INICIALES Y REVISIONES PERIÓDICAS - LAT AEREA")}</th>
                </tr>
                <tr class="capçalera_taula">
                    <td rowspan="2" style="vertical-align: bottom;width: 8%;"><b>${_("Código")}</b></td>
                    <td style="width: 70%;font-size: 7px"><b>${_("Defecto")}</b></td>
                    <td style="width: 5%;"><b>${_("correcto")}</b></td>
                    <td colspan="3" style="width: 15%;"><b>${_("Classificación defecto")}</b></td>
                </tr>
                <tr class="capçalera_taula">
                    <td style="width: 70%;"></td>
                    <td style="width: 5%;"><b>&#x2714;</b></td>
                    <td style="width: 5%;"><b>L</b></td>
                    <td style="width: 5%;"><b>G</b></td>
                    <td style="width: 5%;"><b>MG</b></td>
                </tr>
                <%
                  llista_defectes_trobats = get_defectes_suport(suportAT,revisioAT)
                  defecte_actual=-1
                %>
                % for defecte in llista_tipus_defectes:
                      % if get_num_defecte(defecte['name']) != defecte_actual:
                        <tr>
                            <td class="subtitol_num">
                                ${get_num_defecte(defecte['name'])}
                            </td>
                            <td  class="subtitol">
                                ${classes_defectes_castella[get_num_defecte(defecte['name'])-1]}
                            </td>
                            <td class="subtitol_check">
                                <% trobat=False %>
                                % for elem in llista_defectes_trobats:
                                  % if get_num_defecte(defecte['name']) == get_num_defecte(elem['defecte_id'][1]):
                                      <% trobat=True %>
                                  % endif
                                % endfor
                                % if not trobat :
                                    &#x2714;
                                % else :
                                    <b>X</b>
                                % endif
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <% defecte_actual=get_num_defecte(defecte['name']) %>
                      % endif
                      <tr>
                        <td class="num" style="width: 8%;">${defecte['name']}</td>
                        <td style="width: 70%;">${defecte['descripcio']}</td>
                        <td class="num" style="width: 5%;">
                            <% valoracio = '0' %>
                            % for elem in llista_defectes_trobats:
                              % if defecte['name'] == elem['defecte_id'][1]:
                                  <% valoracio = elem['valoracio'][0] %>
                              % endif
                            % endfor
                        </td>
                        <td class="num" style="width: 5%;">
                            % if valoracio == 1:
                                <b>X</b>
                            % endif;
                        </td>
                        <td class="num" style="width: 5%;">
                            % if valoracio == 2:
                                <b>X</b>
                            % endif;
                        </td>
                        <td class="num" style="width: 5%;">
                            % if valoracio == 3:
                                <b>X</b>
                            % endif;
                        </td>
                    </tr>
                % endfor
                <tr>
                    <th colspan="6"  style="font-size: 5px"> ${_("Descripcion apoyo / Foto / Observaciones")}</th>
                </tr>
                % if len(llista_defectes_trobats) == 0 :
                    <tr><td colspan="6" style="padding-left: 50px;border: none;height:20px"></td></tr>
                % else:
                    % for defecte_trobat in llista_defectes_trobats:
                        <tr>
                            <td style="padding-left: 50px;border: none">${defecte_trobat['defecte_id'][1]} </td>
                            <td colspan="5" style="border: none">${defecte_trobat['observacions']}</td>
                        </tr>
                    % endfor
                % endif
            </table>
            <div id="text_final">
              <p>${_('En el caso de ser apoyo frecuentado, se marcará con un simbolo &#x2714; la casilla gris de "APOYO FRECUENTADO:" "CON Calzado" o "SIN Calzado" según corresponda.')}</p>
              <p>${_("Se marcará con un simbolo &#x2714; la casilla gris, una vez comprovados los defectos de cada grupo.")}</p>
              <p>${_("En caso de la existencia de defecto se marcará con  ")}<b>X</b>${_(" la casilla de classificación del defecto y las casillas gris de su grupo.")}</p>
            </div>
            %if suportAT != revisioAT.name.suports[-1]:
                <p style="page-break-after:always;clear: both"></p>
            %endif

            <div class="capcalera">
                <div class="logo">
                    <img src="${addons_path}/giscedata_revisions/report/gisce_logo.png"/>
                </div>
                <div class="dades_full">
                    ${_("Codi document: 13.3-48_03")}<br>
                    ${_("Guia general verificaciones iniciales y periódicas-LAT")}<br>
                    ${_("aerea<br>")}
                    ${_("Revisión 3")}<br>
                </div>
                <div style="clear: both"></div>
                <table id="table1">
                    <tr>
                        <th colspan="3">13.3-48</th>
                    </tr>
                    <tr>
                        <td width="69%">
                            <p style="font-size: 8px"><b>${_("CODIGO LÍNEA / DESCRIPCION:")}</b></p>
                            <p style="font-size: 10px">${"{0} / {1}".format(revisioAT.name.name,revisioAT.name.descripcio)}</p>
                        </td>
                        <td  width="15%">
                            <p><b>${_("NÚMERO DE APOYO:")}</b></p>
                            <p style="font-size: 10px">${suportAT.name}</p>
                        </td>
                        <td  width="15%">
                            <table id="table2">
                                <tr>
                                    <td colspan="2"><p>${_("APOYO FRECUENTADO")}:</p></td>
                                </tr>
                                <tr>
                                    <td>
                                        ${_("CON Calzado")}<br>
                                        <div class="quadre"></div>
                                    </td>
                                    <td>
                                        ${_("SIN Calzado")}<br>
                                        <div class="quadre"></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="clear: both"></div>
            <div id="titol_pg2"> ${_("GUIA GENERAL VERIFICACIONES INICIALES Y REVISIONES PERIÓDICAS - LAT AEREA")}</div>
            <div id="subtitol_pg2"> ${_("TOMA DE DATOS PARA LA VERIFICACIÓN DE LOS SISTEMAS DE PUESTA A TIERRA EN APOYOS NO FRECUENTADOS")}</div>
            <table class="taula_pg2" style="width: 85%">
                <tr>
                    <td  style="width: 10%">${_("Elementos (*)")}</td>
                    <td style="width: 15%">${_("Resistencia máxima de puesta tierra Rmax(&#937;) (s/proyecto)")}</td>
                    <td>${_("Valor medio de p.a.t.")}<p>${_("Rf de protección (&#937;)")}</p></td>
                    <td>${_("Valor medio de p.a.t.")}<p>${_("Rf de autoválvulas (&#937;)")}</p></td>
                    <td style="width: 35%">${_("Valoracion")}</td>
                </tr>
                <tr>
                    <td>
                      <br><br>
                    </td>
                    <td>20</td>
                    % if get_mesura_suport(suportAT,revisioAT):
                        <td>
                            ${get_mesura_suport(suportAT,revisioAT)['ferramenta'] or "0"}
                        </td>
                        <td>
                            ${get_mesura_suport(suportAT,revisioAT)['autovalvules'] or ""}
                        </td>
                        <td>
                            % if get_mesura_suport(suportAT,revisioAT)['ferramenta'] < 20:
                                  ${_("Correcto")}
                            % else :
                                  ${_("Incorrecto")}
                            % endif
                        </td>
                    % else :
                          <td></td>
                          <td></td>
                          <td></td>
                    % endif
                </tr>
            </table>
            <p style="font-size: 6px; text-align:center"> ${_("(*)ELEMENTOS: A= Autoválvulas, SB= Seccionador vacío, SC= Seccionador carga, I= Interruptor, C= Conversion Subt.")}</p>
            <div id="subtitol_pg2"> ${_("TOMA DE DATOS PARA LA VERIFICACIÓN DE LOS SISTEMAS DE PUESTA A TIERRA EN APOYOS FRECUENTADOS")}</div>
            <table class="taula_pg2" style="width: 100%">
                <tr>
                    <td>${_("Elementos (*)")}</td>
                    <td>${_("Valor de Resistencia de p.a.t.")}<br><br><br><p><b>${_("Rf (&#937;)")}</b></p></td>
                    <td>${_("Intensidad máxima de defecto a tierra")}<br><br><br><p><b>${_("I")}<small>mdt (A)</small></b></p>${_("Dato a facilitar por la compañia eléctrica")}<br></td>
                    <td>${_("Tiempo calculado de actuacion de la protección")}<br><br><br><p><b>${_("t ")}<small>(s)</small></b></p>${_("Dato a facilitar por la compañia eléctrica")}<br></td>
                    <td>${_("Valor máximo adoptado de la tensión de paso aplicada")}<br><br><br><p><b>${_("V")}<small>pa (v)</small></b></p>${_("Obtener este dato a partir de la tabla adjunta")}<br></td>
                    <td>${_("Valor máximo adoptado de la tensión de contacto aplicada")}<br><br><br><p><b>${_("V")}<small>ca (v)</small></b></p>${_("Obtener este dato a partir de la tabla adjunta")}<br></td>
                    <td>${_("Intensidad inyectada")}<br><br><br><p><b>${_("I")}<small>m (A)</small></b></p></td>
                </tr>
                <tr>
                    <td><br><br></td>
                    <td><br><br></td>
                    <td><br><br></td>
                    <td><br><br></td>
                    <td style="background-color: #62BFFF"><br><br></td>
                    <td style="background-color: #5B97CD"><br><br></td>
                    <td><br><br></td>
                </tr>
            </table>
            <p style="font-size: 9px; text-align:center;margin-top:20px;"> ${_("Tabla orientativa de los valores de paso y contacto máximos admisibles en función del tiempo máximo de duración de la falta.")}</p>
            <table class="taula_pg2" style="width: 100%">
                <tr>
                    <td>${_("Tiempo ")}<b>t(s)</b></td>
                    <td><= 0.1</td>
                    <td>0.2</td>
                    <td>0.5</td>
                    <td>0.7</td>
                    <td>0.9</td>
                    <td>1</td>
                    <td>2</td>
                    <td>${_("entre ")}3 i 5</td>
                    <td>${_("más de ")}5</td>
                </tr>
                <tr style="background-color: #62BFFF">
                  <td><b>Vpa(V)</b></td>
                  <td>7200</td>
                  <td>3600</td>
                  <td>1440</td>
                  <td>1020</td>
                  <td>800</td>
                  <td>785</td>
                  <td>690</td>
                  <td>640</td>
                  <td>500</td>
                </tr>
                <tr style="background-color: #5B97CD">
                  <td><b>Vca(V)</b></td>
                  <td>720</td>
                  <td>360</td>
                  <td>144</td>
                  <td>102</td>
                  <td>80</td>
                  <td>78.5</td>
                  <td>69</td>
                  <td>64</td>
                  <td>50</td>
                </tr>
            </table>
            <table style="margin:auto;margin-top:20px;">
              <tr>
                <td class="formula">
                    ${_("Calculo de la tensión resultante:")}<br>
                    <img src="${addons_path}/giscedata_revisions_oca/report/formula_tension_de_paso.png"/>
                </td>
                <td class="formula">
                      ${_("Calculo del valor real de la tension de paso o contacto:")}
                      <table>
                        <tr>
                          <td><img src="${addons_path}/giscedata_revisions_oca/report/formula_tension_resultante.png"/></td>
                          <td style="font-size:7px;">${_("(Resultado en V)")}</td>
                        </tr>
                      </table>
                </td>
              </tr>
            </table>
            <table class="taula2_pg2">
                <tr>
                    <td rowspan="2" style="width: 9%">${_("Posición")}</td>
                    <td colspan="3">${_("Valores tensiones medidas")}</td>
                    <td>${_("Tensión Resultante")}</td>
                    <td>${_("Paso o Contacto")}</td>
                    <td>${_("Valor Real de la tensión de paso o contacto")}</td>
                    <td style="background-color: #62BFFF;">${_("Si")} <p>V<small>p</small> <= V<small>pa</small></p>${_("Cumple")}</td>
                    <td style="background-color: #5B97CD;">${_("Si")} <p>V<small>o</small> <= V<small>ca</small></p>${_("Cumple")}</td>
                    <td>${_("Valoración")}</td>
                </tr>
                <tr>
                    <td style="width: 9%">Vo (mV)</td>
                    <td style="width: 9%">V+ (mV)</td>
                    <td style="width: 9%">V- (mV)</td>
                    <td style="width: 9%">V (mV)</td>
                    <td style="width: 9%">P/C</td>
                    <td style="width: 9%">V<small>p</small> - V<small>c</small>  (V)</td>
                    <td style="background-color: #62BFFF; width: 8%"></td>
                    <td style="background-color: #5B97CD"></td>
                    <td style="width: 10%"></td>
                </tr>
                <tr>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                </tr>
                <tr>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                </tr>
                <tr>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                </tr>
                <tr>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                    <td class="casella_buida"></td>
                </tr>
            </table>
            <p style="font-size: 9px; text-align:left;margin-top:20px;"> ${_("CROQUIS U OBSERVACIONES:")}</p>
            <div class="quadre_observacions">
            </div>
            %if suportAT != revisioAT.name.suports[-1]:
                <p style="page-break-after:always;clear: both"></p>
            %endif
        % endfor
        %if revisioAT != objects[-1]:
            <p style="page-break-after:always;clear: both"></p>
        %endif
    % endfor
    </body>
</html>
