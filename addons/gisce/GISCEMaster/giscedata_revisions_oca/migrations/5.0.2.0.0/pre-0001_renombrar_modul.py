# -*- coding: utf-8 -*-

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log

def migrate(cursor, installed_version):
    """Renombrar el mòdul giscedata_revisions_cts_oc_esp
       a giscedata_revisions_oca
    """ 
    log("Renombrar el mòdul giscedata_revisions_cts_oc_es "
        "a giscedata_revisions_oca")
    cursor.execute("""update ir_model_data set 
    module = 'giscedata_revisions_oca' where 
    module = 'giscedata_revisions_cts_oc_esp'""")
    cursor.execute("""delete from ir_module_module where 
    name = 'giscedata_revisions_cts_oc_esp'""")

