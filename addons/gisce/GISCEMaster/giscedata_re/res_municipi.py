# -*- coding: utf-8 -*-
from osv import osv, fields


class ResMunicipi(osv.osv):
    """Municipis + Zona climatica."""
    _name = 'res.municipi'
    _inherit = 'res.municipi'

    _columns = {
        'climatic_zone': fields.char('Zona climàtica', size=2, readonly=True),
    }


ResMunicipi()
