# -*- coding: utf-8 -*-
{
    "name": "Mòdul d'instal·lacions de Règim Especial",
    "description": """
Ens permet portar el control de totes les instal·lacions de regim especial que tinguem.
* Importar curves horàries en format regim especial (Activa entrant, Activa sortint, R1, R2, R3, R4).
* Perfilacio automatica al importar curves a regim especial desde tg/tm.
* Generacio de fitxers de regim especial (MAGRE, INMERE, MEDIDAS).""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_cups",
        "product",
        "stock",
        "crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_re_wizard.xml",
        "wizard/importar_curva_view.xml",
        "wizard/importar_curva_re_from_profile.xml",
        "wizard/importar_curva_re_from_file.xml",
        "wizard/wizard_cups_without_curve_view.xml",
        "giscedata_re_data.xml",
        "giscedata_re_view.xml",
        "res_municipi_data.xml",
        "res_municipi_view.xml",
        "security/giscedata_re_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
