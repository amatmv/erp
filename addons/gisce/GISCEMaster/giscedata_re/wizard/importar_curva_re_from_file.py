# -*- coding: utf-8 -*-
from osv import osv, fields
import base64
import StringIO
import zipfile
import pandas as pd

ZIV_HEADER = [
    'puerta_enlace', 'punto_medida', 'equipo', 'Nan', 'dia', 'hora', 'ai', 'quality_ai', 'ae',
    'quality_ae', 'r1', 'quality_r1', 'r2', 'quality_r2', 'r3', 'quality_r3',
    'r4', 'quality_r4', 'zc', 'Nan', 'Nan', 'Nan', 'Nan', 'Nan', 'Nan', 'Nan', 'Nan'
]


class ImportarCurvaREFromFile(osv.osv_memory):
    _name = 'giscedata.re.importar.curva.from.file'

    def importar_curva_re_from_file(self, cursor, uid, ids, context=None):
        # read File
        # pass to create_curve
        # get report
        re_obj = self.pool.get('giscedata.re')
        wizard = self.browse(cursor, uid, ids[0])

        _data = base64.decodestring(wizard.file)
        file_handler = StringIO.StringIO(_data)
        try:
            zip_file = zipfile.ZipFile(file_handler, "r")
            for name in zip_file.namelist():
                df = pd.read_csv(
                    zip_file.open(name), sep='\t', names=ZIV_HEADER,
                    dtype={'dia': str, 'hora': str, 'punto_de_medida': int}
                )
                # Drop last row (unaccepted char) and convert to dict
                df = df.drop(df.index[len(df) - 1])
                content = df.T.to_dict().values()
                try:
                    re_obj.import_re_curve_from_file(cursor, uid, content)
                except:
                    pass

        except zipfile.BadZipfile as e:
            content = base64.b64decode(wizard.file)
            re_obj.import_re_curve_from_file(cursor, uid, content)

        # re_ids = context['active_ids']
        # self_vs = self.read(cursor, uid, ids, [], context=context)
        # for self_v in self_vs:
        #     di = self_v['init_date']
        #     df = self_v['end_date']
        #     re_obj.create_curve(cursor, uid, re_ids, di, df, context=context)

        return True

    _columns = {
        'info': fields.text("info"),
        'file': fields.binary('Fitxer', required=True),
        'filename': fields.char('Nom', size=1024),
    }


ImportarCurvaREFromFile()
