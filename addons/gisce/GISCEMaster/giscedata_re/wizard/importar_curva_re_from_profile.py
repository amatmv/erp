# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _

class ImportarCurvaREFromTGProfile(osv.osv_memory):

    _name = 'giscedata.re.importar.curva.from.tg.profile'
    _columns = {
        'init_date': fields.date(
            "From", help="DI: inclosa, DF: NO inclosa (ex: 01/01 a 01/02)"
        ),
        'end_date': fields.date(
            "To", help="DI: inclosa, DF: NO inclosa (ex: 01/01 a 01/02)"
        ),
        'background': fields.boolean("Background"),
        'info': fields.text('Info', readonly=True),
        'state': fields.selection([('init', 'Init'), ('end', 'Done')]),
        'profile': fields.boolean("Profile if the curve doesn't exist"),
    }

    def importar_curva_re_from_profile(self, cursor, uid, ids, context=None):
        re_o = self.pool.get('giscedata.re')
        re_ids = context['active_ids']
        wiz_obj = self.browse(cursor, uid, ids[0], context=context)
        self_vs = self.read(cursor, uid, ids, [], context=context)

        info = _("Error")
        for self_v in self_vs:
            di = self_v['init_date'] + ' 01:00:00'
            df = self_v['end_date'] + ' 00:00:00'
            if self_v['background']:
                re_o.create_curve_async(
                    cursor, uid, re_ids, di, df, context=context
                )
                info = _("Procés generant-se en segon pla")
            else:
                re_o.create_curve(cursor, uid, re_ids, di, df, context=context)
                info = _("Procés Finalitzat")
        self.write(
            cursor, uid, ids,
            {'info': info, 'state': 'end'}
        )

        return True
      
    _defaults = {
        'profile': lambda *a: False,
        'background': lambda *a: False,
        'state': lambda *a: 'init'
    }    

ImportarCurvaREFromTGProfile()
