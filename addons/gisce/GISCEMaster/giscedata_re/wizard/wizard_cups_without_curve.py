# -*- coding: utf-8 -*-
from datetime import datetime
from osv import osv, fields
from tools.translate import _


class WizardCupsWithoutCurve(osv.osv_memory):

    _name = 'wizard.cups.without.curve'

    def action_list_cups(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)

        cursor.execute("""SELECT instalacio_re.id as ID,
                                 cups.name  as CUPS,
                                 instalacio_re.name as INSTALACION,
                                 instalacio_re.tipo as TIPO
                                 FROM giscedata_re instalacio_re
                                 INNER JOIN giscedata_cups_ps cups
                                 ON instalacio_re.cups=cups.id
                                 WHERE instalacio_re.name not in
                                 (
                                     SELECT inst.name
                                     FROM giscedata_re_curva curva
                                     INNER JOIN giscedata_re inst ON curva.inst_re=inst.id
                                     AND (inici >= %s AND final <= %s )
                                 )
                                 AND instalacio_re.active = 'True'""",
                       (wizard.date_from, wizard.date_to))

        results = cursor.fetchall()
        domain = []
        for res in results:
            domain.append(res[0])

        vals_view = {
            'name': _("CUPS sin curva"),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.re',
            'limit': 80,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % domain
        }

        return vals_view

    _columns = {
        'date_from': fields.date(_('Desde'), required=True,
                                 help=_('Mes completo, primer día incluido. Ejemplo: De 01/08/2019 a 01/09/2019')),
        'date_to': fields.date(_('Hasta'), required=True,
                               help=_('Mes completo, último día excluido. Ejemplo: De 01/08/2019 a 01/09/2019')),
    }

    _defaults = {
        'date_from': lambda *a: datetime.now().strftime('%Y-%m-%d'),
        'date_to': lambda *a: datetime.now().strftime('%Y-%m-%d'),
    }


WizardCupsWithoutCurve()
