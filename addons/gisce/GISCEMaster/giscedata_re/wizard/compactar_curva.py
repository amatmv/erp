# -*- coding: utf-8 -*-

import wizard
import pooler
import base64
import csv
import StringIO
import time
import bz2
import osv
import calendar

_selection_mes = []
_selection_opts = []

def _init(self, cr, uid, data, context={}):
    cr.execute("""select distinct to_char(inici, 'MM/YYYY') as month, to_char(inici, 'YYYYMM') as ts from giscedata_re_curva where inst_re = %s and compact = False order by ts desc;""", (data['id'],))
    _selection_mes = [(a[0], a[0]) for a in cr.fetchall()]
    self.states['triar_vals']['result']['fields']['mes']['selection'] = _selection_mes
    data['ids_lectures_ok'] = []
    data['index'] = 0
    # Reiniciem els states dels checks
    self.states['check_values']['result']['type'] = 'form'
    self.states['check_values']['result']['state'] = [('end', 'Cancelar'), ('resolve', 'Siguiente')]
    self.states['check_hour']['result']['type'] = 'form'
    self.states['check_hour']['result']['state'] = [('end', 'Cancelar'), ('resolve_hour', 'Siguiente')]
    return {}

def _triar_vals(self, cr, uid, data, context={}):
    return {}


_triar_vals_form = """<?xml version="1.0"?>
<form string="Compactación de Curva">
  <field name="mes" required="1"/>
</form>"""

_triar_vals_fields = {
  'mes': {'string': 'Mes a compactar', 'type': 'selection', 'selection': _selection_mes},
}


def _check_values(self, cr, uid, data, context={}):

    lectura_obj = pooler.get_pool(cr.dbname).get('giscedata.re.curva.lectura')
    month,year = data['form']['mes'].split('/')

    # Si no existeix la clau 'diffs' és que encara no s'han buscat les diferencies.
    # Les busquem
    if not data.has_key('diffs'):
        cr.execute("""select
      count(name),
      name,
      magnitud,
      estacio
    from (
      select distinct
        l.name,
        l.magnitud,
        l.estacio,
        l.arrastre,
        l.lectura_real,
        l.perdues,
        l.lectura,
        l.tipo,
        l.upr
      from
        giscedata_re_curva_lectura l,
        giscedata_re_curva c
      where
        l.curva = c.id
        and c.inst_re = %s
        and l.year = %s
        and l.month = %s
      ) as foo
    group by
      name,
      magnitud,
      estacio
    having
      count(name) > 1""", (data['id'], year, month))

        data['diffs'] = cr.dictfetchall()

    while data['index'] < len(data['diffs']):
        diff = data['diffs'][data['index']]
        # Tenim més d'un valor per aquesta hora
        search_params = [
          ('name', '=', diff['name']),
          ('estacio', '=', diff['estacio']),
          ('curva.inst_re', '=', data['id']),
          ('magnitud', '=', diff['magnitud'])
        ]
        id_lecturas = lectura_obj.search(cr, uid, search_params)
        lectures_text = []
        _selection_opts = []
        _lectures = []
        i = 1

        for l in lectura_obj.browse(cr, uid, id_lecturas):
            lectures_text.append("[%i] T:%s M:%s A:%s LR:%s P:%s L:%s UPR:%s C:\"%s\" (#%s)" % (i, l.name, l.magnitud, l.arrastre, l.lectura_real, l.perdues, l.lectura, l.upr, l.curva.name, l.id))
            _selection_opts.append((l.id, str(i)))
            _lectures.append(l.id)
            i += 1

        self.states['check_values']['result']['fields']['selection_opts']['selection'] = _selection_opts
        self.states['check_values']['result']['fields']['curva_browser']['domain'] = [('inst_re.id', '=', data['id'])]
        hora = diff['name']
        return {
          'timestamp': '%s/%s/%s %s:00:00' % (hora[6:8],hora[4:6], hora[0:4], hora[8:10]),
          'magnitud': diff['magnitud'],
          'text_opts': '\n'.join(lectures_text),
          'lectures': ','.join(map(str, _lectures)),
          'curva_browser': False,
        }

    # Hem acabat tot el bucle, anem a finalitzar, modifiquem el tipus de result d'aquest
    # estat per tal que no mostri form, sino que vagi a un altre estat
    self.states['check_values']['result']['type'] = 'state'
    # Això ho canviarem per comprovar les hores
    self.states['check_values']['result']['state'] = 'check_hour'
    return {}


_check_values_form = """<?xml version="1.0"?>
<form string="Compactación de Curva" col="4">
  <label colspan="4" string="Conflictos en valores de lecturas" />
      <field name="timestamp" />
      <field name="magnitud" />
      <separator string="Valores disponibles" colspan="4" />
      <field name="text_opts" colspan="4" nolabel="1" width="700"/>
  <notebook tabpos="down" colspan="4">
    <page string="Diff">
      <field name="selection_opts" width="50"/>
    </page>
    <page string="Navegador de curvas">
      <field name="curva_browser" colspan="4"/>
    </page>
  </notebook>
  <field name="lectures" invisible="1" />
</form>"""

_check_values_fields = {
  'timestamp': {'string': 'Día', 'type': 'char', 'size': 20, 'readonly': True},
  'magnitud': {'string': 'Maginitud', 'type': 'char', 'size': 2, 'readonly': True},
  'text_opts': {'string': 'Valores disponibles', 'type': 'text', 'readonly': True},
  'selection_opts': {'string': 'Opción válida', 'type': 'selection', 'selection': _selection_opts, 'required': True},
  'lectures': {'string': 'IDS Lecturas', 'type': 'text', 'invisible': True},
  'curva_browser': {'string': 'Curva a explorar', 'type': 'many2one', 'relation': 'giscedata.re.curva'}
}

def _resolve(self, cr, uid, data, context={}):
    lectures_obj = pooler.get_pool(cr.dbname).get('giscedata.re.curva.lectura')
    form = data['form']
    # Transformem l'string d'ids de lectures a un array
    lectures = map(int, form['lectures'].split(','))
    # Eliminem de les lectures que hi havia conflicte la lectura correcte
    lectures.remove(form['selection_opts'])
    # Posem la opció vàlida a ids_lectures_ok
    data['ids_lectures_ok'].append(form['selection_opts'])
    # Eliminem les lectures no vàlides
    lectures_obj.unlink(cr, uid, lectures)
    data['index'] += 1
    return {
      'timestamp': False,
      'magnitud': False,
      'text_opts': False,
      'selection_opts': False,
      'lectures': False,
      'curva_browser': False,
    }

def _check_hour(self, cr, uid, data, context={}):
    # Per comprovar els dies generarem una llista amb NAME_ESTACIO_MAGNITUD després farem una consulta i
    # eliminarem les claus que anem troban, si dona una excepció que no troba la clau és que hi ha una
    # hora no vàlida a la curva,
    # Després d'eliminar totes les claus que hi hagi a la consula si el diccionari no està buit aquelles
    # són les hores que falten lectura

    lectures_obj = pooler.get_pool(cr.dbname).get('giscedata.re.curva.lectura')
    re_obj = pooler.get_pool(cr.dbname).get('giscedata.re')
    uprs_obj = pooler.get_pool(cr.dbname).get('giscedata.re.uprs')

    #TODO Unificar la manera de crear aquest array de dies, ja que es fa servir en la importació també
    # S'hauria de fer una classe que et retornés el diccionari

    if not data.has_key('dies'):

        # Tornem a posar l'index a 0
        data['index'] = 0

        month,year = map(int, data['form']['mes'].split('/'))
        # Busquem el dia de canvi d'hora pel mes/any que passem
        dia_canvi_hora = {}
        dia_canvi_hora['estiu'] = 0
        dia_canvi_hora['hivern'] = 0
        estiu = 1
        hivern = 0
        i = -1
        while not dia_canvi_hora['estiu']:
            dia_canvi_hora['estiu'] = calendar.monthcalendar(year, 3)[i][-1]
            i -= 1
        i = -1
        while not dia_canvi_hora['hivern']:
            dia_canvi_hora['hivern'] = calendar.monthcalendar(year, 10)[i][-1]
            i -= 1
        # Busquem el número de dies del mes
        n_dies = calendar.monthrange(year, month)[1]
        data['dies'] = {}
        magnituds = ['AE', 'AS', 'R1', 'R2', 'R3', 'R4']
        for mag in magnituds:
            for d in xrange(1, n_dies+1):
                if month in (1,2,11,12):
                    # Son mesos d'hivern normals (24h)
                    for h in xrange(1, 25):
                        key = '%s%s%s%s_%s_%s' % (year, str(month).zfill(2), str(d).zfill(2), str(h).zfill(2), hivern, mag)
                        data['dies'][key] = False
                if month in (4,5,6,7,8,9):
                    # Son mesos d'estiu normals (24h)
                    for h in xrange(1, 25):
                        key = '%s%s%s%s_%s_%s' % (year, str(month).zfill(2), str(d).zfill(2), str(h).zfill(2), estiu, mag)
                        data['dies'][key] = False
                if month == 10:
                    if d < dia_canvi_hora['hivern']:
                        # dies normals abans del canvi d'hora d'hivern (24h)
                        for h in xrange(1, 25):
                            key = '%s%s%s%s_%s_%s' % (year, str(month).zfill(2), str(d).zfill(2), str(h).zfill(2), estiu, mag)
                            data['dies'][key] = False
                    if d == dia_canvi_hora['hivern']:
                        # dia d'estiu/hivern (25h)
                        for h in xrange(1, 3):
                            key = '%s%s%s%s_%s_%s' % (year, str(month).zfill(2), str(d).zfill(2), str(h).zfill(2), estiu, mag)
                            data['dies'][key] = False
                        for h in xrange(2, 25):
                            key = '%s%s%s%s_%s_%s' % (year, str(month).zfill(2), str(d).zfill(2), str(h).zfill(2), hivern, mag)
                            data['dies'][key] = False
                    if d > dia_canvi_hora['hivern']:
                        # dies normals després del canvi d'hora d'hivern (24h)
                        for h in xrange(1, 25):
                            key = '%s%s%s%s_%s_%s' % (year, str(month).zfill(2), str(d).zfill(2), str(h).zfill(2), hivern, mag)
                            data['dies'][key] = False
                if month == 3:
                    if d < dia_canvi_hora['estiu']:
                        # dies normals abans del canvi d'hora d'estiu (24h)
                        for h in xrange(1, 25):
                            key = '%s%s%s%s_%s_%s' % (year, str(month).zfill(2), str(d).zfill(2), str(h).zfill(2), hivern, mag)
                            data['dies'][key] = False
                    if d == dia_canvi_hora['estiu']:
                        # dia hivern/estiu (25h)
                        for h in xrange(1, 2):
                            key = '%s%s%s%s_%s_%s' % (year, str(month).zfill(2), str(d).zfill(2), str(h).zfill(2), hivern, mag)
                            data['dies'][key] = False
                        for h in xrange(3, 25):
                            key = '%s%s%s%s_%s_%s' % (year, str(month).zfill(2), str(d).zfill(2), str(h).zfill(2), estiu, mag)
                            data['dies'][key] = False
                    if d > dia_canvi_hora['estiu']:
                        # dies normals després del canvi d'hora d'estiu (24h)
                        for h in xrange(1, 25):
                            key = '%s%s%s%s_%s_%s' % (year, str(month).zfill(2), str(d).zfill(2), str(h).zfill(2), estiu, mag)
                            data['dies'][key] = False

        # Ja tenim el diccionari creat, ara creem les claus de la bbdd
        month,year = data['form']['mes'].split('/')
        cr.execute("""select distinct
        l.name  || '_' || l.estacio || '_' || l.magnitud as key
      from
        giscedata_re_curva_lectura l,
        giscedata_re_curva c
      where
        l.curva = c.id
        and c.inst_re = %s
        and l.year = %s
        and l.month = %s
      order by
        key asc""", (data['id'], year, month))
        data['bd_keys'] = [a[0] for a in cr.fetchall()]

        # Comprovar quines lectures estan manquen i quines sobren
        for key in data['bd_keys']:
            # incrementem l'index
            try:
                del data['dies'][key]
            except:
                # En l'array de dies no hi és, això vol dir que és una lectura no vàlida
                # Per tant l'eliminem
                name,estacio,magnitud = key.split('_')
                search_params = [
                  ('name', '=', name),
                  ('estacio', '=', estacio),
                  ('magnitud', '=', magnitud),
                  ('curva.inst_re', '=', data['id'])
                ]
                del_ids = lectures_obj.search(cr, uid, search_params)
                lectures_obj.unlink(cr, uid, del_ids)

        # Un cop eliminades les claus, sabem quines són les lectures que falten,
        # seran les claus que no s'hagin eliminat del diccionari data['dies']

        data['no_lectures'] = sorted(data['dies'].keys())

    month,year = data['form']['mes'].split('/')

    while data['index'] < len(data['no_lectures']):
        key = data['no_lectures'][data['index']]
        name,estacio,magnitud = key.split('_')

        # Posem paràmetres predefinits per aquesta hora (UPR, Provincia, Distribuidora, CUPS)

        re = re_obj.browse(cr, uid, data['id'])

        # Posem la curva a qualsevol del periode, després amb la compactació ja la posarà bé
        search_params = [
          ('month', '=', month),
          ('year', '=', year),
          ('curva.inst_re', '=', data['id'])
        ]
        lect_ids = lectures_obj.search(cr, uid, search_params, limit=1)

        curva_id = lectures_obj.browse(cr, uid, lect_ids[0]).curva.id

        any = name[0:4]
        mes = name[4:6]
        dia = name[6:8]
        hora = name[8:10]

        search_params = [
          ('inst_re', '=', data['id']),
          ('data_alta', '<=', '%s-%s-%s %s:00:00' % (any,mes,dia,hora)),
          ('data_baixa', '>', '%s-%s-%s %s:00:00' % (any,mes,dia,hora))
        ]
        uprs_id = uprs_obj.search(cr, uid, search_params)
        uprs = uprs_obj.browse(cr, uid, uprs_id[0])
        perdues = 0
        if magnitud in ('AE', 'AS'):
            if re.p_ae:
                perdues = re.perdidas
            if re.p_as:
                perdues = re.perdidas

        return {
          'name': name,
          'year': any,
          'month': mes,
          'day': dia,
          'hour': hora,
          'estacio': int(estacio),
          'magnitud': magnitud,
          'curva': curva_id,
          'distribuidora': re.cups.name[2:6],
          'cups': re.cups.name,
          'provincia': re.provincia,
          'lectura_real': 0,
          'perdues': perdues,
          'tipo': re.tipo,
          'upr': uprs.name
        }

    # Hem acabat tot el bucle, anem a finalitzar, modifiquem el tipus de result d'aquest
    # estat per tal que no mostri form, sino que vagi a un altre estat
    self.states['check_hour']['result']['type'] = 'state'
    self.states['check_hour']['result']['state'] = 'make_compact'
    return {}

_check_hour_form = """<?xml version="1.0"?>
<form string="Compactación de Curva" col="4">
  <label colspan="4" string="Horas no existentes" />
  <field name="name" invisible="1" />
  <field name="curva" invisible="1" />
  <group colspan="4" col="12" string="Hora">
    <field name="day" />
    <field name="month" />
    <field name="year" />
    <field name="hour" />
    <field name="estacio" />
    <field name="magnitud" />
  </group>
  <group colspan="4" string="Instalación">
    <field name="cups" />
    <field name="distribuidora" />
    <field name="tipo" />
    <field name="upr" />
    <field name="provincia" />
  </group>
  <notebook tabpos="down" colspan="4">
    <page string="Valores">
      <field name="lectura_real" />
      <field name="perdues" />
    </page>
    <page string="Navegador de curvas">
      <field name="curva_browser" colspan="4"/>
    </page>
  </notebook>
</form>"""

_check_hour_fields = {
  'name': {'string': 'Name', 'type': 'char', 'size': 10, 'readonly': True},
  'curva': {'string': 'Curva', 'type': 'integer', 'readonly': True},
  'year': {'string': 'Año', 'type': 'char', 'size': 4, 'readonly': True},
  'month': {'string': 'Mes', 'type': 'char', 'size': 2, 'readonly': True},
  'day': {'string': 'Día', 'type': 'char', 'size': 2, 'readonly': True},
  'hour': {'string': 'Hora', 'type': 'char', 'size': 2, 'readonly': True},
  'estacio': {'string': 'Estación', 'type': 'integer', 'readonly': True},
  'magnitud': {'string': 'Maginitud', 'type': 'char', 'size': 2, 'readonly': True},
  'cups': {'string': 'CUPS', 'type': 'char', 'size': 22, 'readonly': True},
  'distribuidora': {'string': 'Distribuidora', 'type': 'char', 'size': 4, 'readonly': True},
  'tipo': {'string': 'Tipo', 'type': 'selection', 'selection': [('1', 'Tipo 1'), ('2', 'Tipo 2'), ('3', 'Tipo 3'), ('4', 'Tipo 4'), ('5', 'Tipo 5')], 'readonly': True},
  'upr': {'string': 'UPR', 'type': 'char', 'size': 8, 'readonly': True},
  'provincia': {'string': 'Provincia', 'type': 'char', 'size': 2, 'readonly': True},
  'lectura_real': {'string': 'Lectura (sín pérdidas y en kWh)', 'type': 'integer', 'required': True},
  'perdues': {'string': 'Pérdidas', 'type': 'float', 'required': True},
  'curva_browser': {'string': 'Curva a explorar', 'type': 'many2one', 'relation': 'giscedata.re.curva'}
}

def _resolve_hour(self, cr, uid, data, context={}):
    lectures_obj = pooler.get_pool(cr.dbname).get('giscedata.re.curva.lectura')
    form = data['form']
    data['index'] += 1

    lectura = form['lectura_real']
    if form['perdues']:
        lectura = form['lectura_real'] - (form['lectura_real']*form['perdues'])

    vals = {
      'name': form['name'],
      'year': form['year'],
      'month': form['month'],
      'day': form['day'],
      'hour': form['hour'],
      'estacio': form['estacio'],
      'magnitud': form['magnitud'],
      'cups': form['cups'],
      'distribuidora': form['distribuidora'],
      'tipo': form['tipo'],
      'upr': form['upr'],
      'curva': form['curva'],
      'provincia': form['provincia'],
      'lectura_real': form['lectura_real'],
      'perdues': form['perdues'],
      'lectura': lectura
    }
    lectures_obj.create(cr, uid, vals)

    return {}


def _make_compact(self, cr, uid, data, context={}):
    month,year = data['form']['mes'].split('/')
    curva_obj = pooler.get_pool(cr.dbname).get('giscedata.re.curva')
    lectures_obj = pooler.get_pool(cr.dbname).get('giscedata.re.curva.lectura')

    # Busquem un inici i un final
    cr.execute("select distinct l.name from giscedata_re_curva_lectura l, giscedata_re_curva c where l.curva = c.id and c.inst_re = %s and year = %s and month = %s order by name asc", (data['id'], year, month))
    data['hores'] = [a[0] for a in cr.fetchall()]
    inici = data['hores'][0]
    final = data['hores'][-1]
    data['inici'] = '%s-%s-%s 01:00:00' % (inici[0:4], inici[4:6], inici[6:8])
    cr.execute("select %s::timestamp", ('%s-%s-%s %s:00:00' % (final[0:4], final[4:6], final[6:8], final[8:10]),))
    data['final'] = cr.fetchone()[0]

    # Creem la curva compactada
    vals = {
      'name': 'Curva compactada el %s' % time.strftime('%d/%m/%Y %H:%M:%S'),
      'compact': 1,
      'inst_re': data['id'],
      'inici': data['inici'],
      'final': data['final']
    }
    id_curva = curva_obj.create(cr, uid, vals)

    # Crearem totes les hores que ja hem corretgit de nou
    cr.execute("""select
    count(name),
    name,
    cups,
    distribuidora,
    tipo,
    magnitud,
    year,
    month,
    day,
    hour,
    estacio,
    lectura_real,
    perdues,
    lectura,
    arrastre,
    upr,
    provincia
  from (
    select distinct
      l.name,
      l.cups,
      l.distribuidora,
      l.tipo,
      l.magnitud,
      l.year,
      l.month,
      l.day,
      l.hour,
      l.estacio,
      l.lectura_real,
      l.perdues,
      l.lectura,
      l.arrastre,
      l.upr,
      l.provincia
    from
      giscedata_re_curva_lectura l,
      giscedata_re_curva c
    where
      l.curva = c.id
      and c.inst_re = %s
      and l.year = %s
      and l.month = %s
    ) as foo
  group by
    name,
    cups,
    distribuidora,
    tipo,
    magnitud,
    year,
    month,
    day,
    hour,
    estacio,
    lectura_real,
    perdues,
    lectura,
    arrastre,
    upr,
    provincia
  having
    count(name) = 1
  order by
    name asc""", (data['id'], year, month))

    hores = cr.dictfetchall()

    for hora in hores:
        del hora['count']
        hora['curva'] = id_curva
        lectures_obj.create(cr, uid, hora)

    # Busquem els ids de les curves no compactades
    cr.execute("SELECT distinct l.curva from giscedata_re_curva_lectura l, giscedata_re_curva c where l.curva = c.id and c.compact = False and l.month = %s and l.year = %s and c.inst_re = %s", (month, year, data['id']))
    del_ids = [a[0] for a in cr.fetchall()]

    # Eliminem les curves
    curva_obj.unlink(cr, uid, del_ids)
    return {}

_make_compact_form = """<?xml version="1.0"?>
<form string="Compactación de Curva" col="4">
  <label string="Curva compactada correctamente" colspan="4"/>
</form>"""

_make_compact_fields = {}

class wizard_compactar_curva(wizard.interface):

    states = {
      'init': {
        'actions': [_init],
        'result': {'type': 'state', 'state': 'triar_vals'},
      },
      'triar_vals': {
        'actions': [_triar_vals],
        'result': {'type': 'form', 'arch': _triar_vals_form, 'fields': _triar_vals_fields, 'state': [('check_values', 'Siguiente')]}
      },
      'check_values': {
        'actions': [_check_values],
        'result': {'type': 'form', 'arch': _check_values_form, 'fields': _check_values_fields, 'state': [('end', 'Cancelar'), ('resolve', 'Siguiente')]}
      },
      'resolve': {
        'actions': [_resolve],
        'result': {'type': 'state', 'state': 'check_values'},
      },
      'check_hour': {
        'actions': [_check_hour],
        'result': {'type': 'form', 'arch': _check_hour_form, 'fields': _check_hour_fields, 'state': [('end', 'Cancelar'), ('resolve_hour', 'Siguiente')]}
      },
      'resolve_hour': {
        'actions': [_resolve_hour],
        'result': {'type': 'state', 'state': 'check_hour'},
      },
      'make_compact': {
        'actions': [_make_compact],
        'result': {'type': 'form', 'arch': _make_compact_form, 'fields': _make_compact_fields, 'state': [('end', 'Finalizar')]}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'},
      }
    }

wizard_compactar_curva('giscedata.re.compact')
