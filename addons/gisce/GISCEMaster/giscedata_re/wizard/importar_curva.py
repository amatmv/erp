# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
import base64
import csv
import StringIO
import time
import bz2
import calendar
from lxml import etree


class ImportarCurvaRE(osv.osv_memory):

    _name = 'giscedata.re.importar.curva'

    def check_format(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])
        if wizard.prime_format:
            wizard.write({'errors': u"Ningún error detectado, se puede "
                                    u"proceder con la importación",
                          'state': 'check'})
            return True

        if wizard.compressed:
            file_content = bz2.decompress(base64.b64decode(wizard.file))
        else:
            file_content = base64.b64decode(wizard.file)

        csv_file = StringIO.StringIO(file_content)
        reader = csv.reader(csv_file, delimiter=',')
        line = 0
        errors = []
        for row in reader:
            line += 1
            # Comprovem que hi hagi 7 columnes
            if not len(row) == 7:
                errors.append('Línea %i: No tiene todos los valores' % line)

            # Comprovem que el format de la data sigui el correcte
            try:
                time.strptime(row[0], '%d/%m/%Y %H:00')
            except:
                errors.append(u"Línea %i: El formato del timestamp del "
                              u"fichero no es correcto. Actual es %s y "
                              u"debe ser: DD/MM/AAAA HH:00" % (line, row[0]))

            # Comprovem que els valors de les lectures siguin enters
            for i in range(1, 7):
                try:
                    int(row[i])
                except:
                    errors.append(u"Línea %i, Columna %i: "
                                  u"El formato no es correcto, "
                                  u"debe ser un entero." % (line, i))
        if len(errors):
            wizard.write({'errors': '\n'.join(errors),
                          'state': 'retry'})
        else:
            wizard.write({'errors': u"Ningún error detectado, se puede "
                                    u"proceder con la importación",
                          'state': 'check'})

        return True

    def retry(self, cursor, uid, ids, context=None):

        self.write(cursor, uid, [ids[0]], {'state': 'init',
                                           'file': False,
                                           'errors': ''})

    def translate(self, xml):

        salida = ''
        tree = etree.fromstring(xml)
        for element in tree.iter():
            if element.tag == 'S02':
                time = '%s/%s/%s %s:00' % (element.get('Fh')[6:8],
                                         element.get('Fh')[4:6],
                                         element.get('Fh')[:4],
                                         element.get('Fh')[8:10],)
                linea = '%s,%s,%s,%s,%s,%s,%s\n' % (time,
                                           element.get('AI'),
                                           element.get('AE'),
                                           element.get('R1'),
                                           element.get('R2'),
                                           element.get('R3'),
                                           element.get('R4'),)
                salida += linea

        return salida

    def import_curva(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])

        # Creem l'objecte de la curva
        re_curva_obj = self.pool.get('giscedata.re.curva')
        re_curva_lectura_obj = self.pool.get('giscedata.re.curva.lectura')
        re_uprs_obj = self.pool.get('giscedata.re.uprs')

        inst_re = self.pool.get('giscedata.re').browse(cursor, uid,
                                                        wizard.inst_re.id)

        n_lines = 0

        arrastres = {
          'AE': 0,
          'AS': 0,
          'R1': 0,
          'R2': 0,
          'R3': 0,
          'R4': 0
        }

        if wizard.compressed:
            file_content = bz2.decompress(base64.b64decode(wizard.file))
        else:
            file_content = base64.b64decode(wizard.file)
        if wizard.prime_format:
            file_content = self.translate(file_content)

        csv_file = StringIO.StringIO(file_content)

        reader = csv.reader(csv_file, delimiter=',')

        dates = [r[0] for r in reader]
        d = dates[0]
        inici = '%s-%s-%s %s:%s:00' % (d[6:10], d[3:5], d[0:2],
                                       d[11:13], d[14:16])
        d = dates[-1]
        final = '%s-%s-%s %s:%s:00' % (d[6:10], d[3:5], d[0:2],
                                       d[11:13], d[14:16])

        vals = {
         'name': 'Curva importada el %s' %
                    (time.strftime('%d/%m/%Y %H:%M:%S')),
         'inst_re': inst_re.id,
         'inici': inici,
         'final': final,
         'compact': 0
        }

        # Generem array estiu (1)/hivern (0)
        estiu = 1
        hivern = 0

        # Agafem tots els mesos diferents MM/YYYY
        months = []
        dies = {}
        for d in dates:
            s = '%s/%s' % (d[3:5], d[6:10])
            if s not in months:
                months.append(s)

        for m in months:
            month, year = map(int, m.split('/'))
            # Busquem el dia de canvi d'hora pel mes/any que passem
            dia_canvi_hora = {}
            dia_canvi_hora['estiu'] = 0
            dia_canvi_hora['hivern'] = 0
            i = -1
            while not dia_canvi_hora['estiu']:
                dia_canvi_hora['estiu'] = calendar.monthcalendar(year,
                                                                 3)[i][-1]
                i -= 1
            i = -1
            while not dia_canvi_hora['hivern']:
                dia_canvi_hora['hivern'] = calendar.monthcalendar(year,
                                                                  10)[i][-1]
                i -= 1
            # Busquem el número de dies del mes
            n_dies = calendar.monthrange(year, month)[1]
            for d in xrange(1, n_dies + 1):
                if month in (1, 2, 11, 12):
                    # Son mesos d'hivern normals (24h)
                    for h in xrange(1, 25):
                        key = '%s%s%s%s' % (year, str(month).zfill(2),
                                            str(d).zfill(2), str(h).zfill(2))
                        dies[key] = hivern
                if month in (4, 5, 6, 7, 8, 9):
                    # Son mesos d'estiu normals (24h)
                    for h in xrange(1, 25):
                        key = '%s%s%s%s' % (year, str(month).zfill(2),
                                            str(d).zfill(2), str(h).zfill(2))
                        dies[key] = estiu
                if month == 10:
                    if d < dia_canvi_hora['hivern']:
                        # dies normals abans del canvi d'hora d'hivern (24h)
                        for h in xrange(1, 25):
                            key = '%s%s%s%s' % (year, str(month).zfill(2),
                                                str(d).zfill(2),
                                                str(h).zfill(2))
                            dies[key] = estiu
                    if d == dia_canvi_hora['hivern']:
                        # dia d'estiu/hivern (25h)
                        for h in xrange(1, 3):
                            key = '%s%s%s%s' % (year, str(month).zfill(2),
                                                str(d).zfill(2),
                                                str(h).zfill(2))
                            dies[key] = estiu
                        for h in xrange(3, 25):
                            key = '%s%s%s%s' % (year, str(month).zfill(2),
                                                str(d).zfill(2),
                                                str(h).zfill(2))
                            dies[key] = hivern
                    if d > dia_canvi_hora['hivern']:
                        # dies normals després del canvi d'hora d'hivern (24h)
                        for h in xrange(1, 25):
                            key = '%s%s%s%s' % (year, str(month).zfill(2),
                                                str(d).zfill(2),
                                                str(h).zfill(2))
                            dies[key] = hivern
                if month == 3:
                    if d < dia_canvi_hora['estiu']:
                        # dies normals abans del canvi d'hora d'estiu (24h)
                        for h in xrange(1, 25):
                            key = '%s%s%s%s' % (year, str(month).zfill(2),
                                                str(d).zfill(2),
                                                str(h).zfill(2))
                            dies[key] = hivern
                    if d == dia_canvi_hora['estiu']:
                        # dia hivern/estiu (25h)
                        for h in xrange(1, 2):
                            key = '%s%s%s%s' % (year, str(month).zfill(2),
                                                str(d).zfill(2),
                                                str(h).zfill(2))
                            dies[key] = hivern
                        for h in xrange(3, 25):
                            key = '%s%s%s%s' % (year, str(month).zfill(2),
                                                str(d).zfill(2),
                                                str(h).zfill(2))
                            dies[key] = estiu
                    if d > dia_canvi_hora['estiu']:
                        # dies normals després del canvi d'hora d'estiu (24h)
                        for h in xrange(1, 25):
                            key = '%s%s%s%s' % (year, str(month).zfill(2),
                                                str(d).zfill(2),
                                                str(h).zfill(2))
                            dies[key] = estiu

        curva_id = re_curva_obj.create(cursor, uid, vals)

        csv_file = StringIO.StringIO(file_content)
        reader = csv.reader(csv_file, delimiter=',')

        for row in reader:
            dia, mes, any = row[0].split('/')
            any, hora = any.split(' ')
            hora = hora[0:2]
            if hora == '00':
                hora = '24'
                dia = dia_ant
                if mes != mes_ant:
                    any = any_ant
                    mes = mes_ant
                    dia = dia_ant

            # Buscar l'UPR segons dia i hora
            search_params = [
              ('inst_re', '=', inst_re.id),
              ('data_alta', '<=', '%s-%s-%s %s:00:00' % (any, mes, dia, hora)),
              ('data_baixa', '>', '%s-%s-%s %s:00:00' % (any, mes, dia, hora))
            ]
            uprs_id = re_uprs_obj.search(cursor, uid, search_params)

            if not len(uprs_id):
                #EXCEPTION
                raise osv.except_osv('Error',
                                     _(u'No existe ninguna unidad '
                                       u'de programación '
                                       u'para la hora %s-%s-%s %s:00:00'
                                       % (any, mes, dia, hora)))

            re_uprs = re_uprs_obj.browse(cursor, uid, uprs_id[0])

            timestamp = '%s%s%s%s' % (any, mes, dia, hora)

            vals = {
                          'name': timestamp,
                          'cups': inst_re.cups.name,
                          'distribuidora': inst_re.cups.name[2:6],
                          'tipo': inst_re.tipo,
                          'year': any,
                          'month': mes,
                          'day': dia,
                          'hour': int(hora),
                          'upr': re_uprs.name,
                          'provincia': inst_re.provincia,
                          'curva': curva_id
                        }

            lectures = {
                          'AE': row[1],
                          'AS': row[2],
                          'R1': row[3],
                          'R2': row[4],
                          'R3': row[5],
                          'R4': row[6]
                        }

            perdues = {
              'AE': wizard.p_ae,
              'AS': wizard.p_as,
              'R1': 0,
              'R2': 0,
              'R3': 0,
              'R4': 0,
            }

            for mag in lectures.keys():
                vals['magnitud'] = mag
                lectura = float(lectures[mag]) / wizard.div
                vals['lectura_real'] = lectura
                vals['perdues'] = wizard.perd * perdues[mag]
                lectura -= lectura * vals['perdues']
                lectura += arrastres[mag]
                aprox = int(abs(round(lectura)))
                arrastres[mag] = lectura - aprox
                if abs(arrastres[mag]) > 1:
                    aprox += int(arrastres[mag])
                    arrastres[mag] -= int(arrastres[mag])
                vals['arrastre'] = arrastres[mag]
                vals['lectura'] = aprox
                vals['estacio'] = dies[timestamp]

                f = re_curva_lectura_obj.create(cursor, uid, vals)

            n_lines += 1
            # Com que al mes 10 es repeteix la hora 0200,
            # el flag es troba en mode estiu (1)
            # i quan hi tornem a passar estarà en mode hivern (0) restant-li 1.
            # A les altres hores no afecta perquè no es repeteixen mai
            dies[timestamp] -= 1

            mes_ant = mes
            dia_ant = dia
            any_ant = any

        wizard.write({'result': '\n'.join(["%s: %s" % (v[0], v[1])
                                        for v in sorted(arrastres.items())]),
                      'n_lines': n_lines,
                      'state': 'end'})

    _columns = {
        'inst_re': fields.many2one('giscedata.re', 'Instalación RE',
                                   required=True),
        'perd': fields.float('Pérdidas', digits=(2, 2), required=True),
        'div': fields.selection([(1, 'kWh'),
                                 (1000, 'Wh')],
                                "Unidad",
                                required=True),
        'file': fields.binary('Fichero', required=True),
        'compressed': fields.boolean('BZip2'),
        'p_ae': fields.boolean('Activa Entrante'),
        'p_as': fields.boolean('Activa Saliente'),
        'errors': fields.text('Errores'),
        'result': fields.text('Arrastres'),
        'n_lines': fields.integer('# Líneas procesadas'),
        'prime_format': fields.boolean('PRIME',
                                help=u"Marque esta opción si está importando "
                                     u"una curva de un contador PRIME"),
        'state': fields.selection([('init', 'Init'),
                                   ('check', 'Check'),
                                   ('retry', 'Retry'),
                                   ('end', 'End'), ],
                                  'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

ImportarCurvaRE()
