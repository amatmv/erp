from addons import get_module_resource
from destral import testing
import pandas as pd


class TestGiscedataRE(testing.OOTestCaseWithCursor):

    def test_check_curves_with_time_change(self):
        """
        Check if season column is correctly added to dataframe
        """
        re_obj = self.openerp.pool.get('giscedata.re')

        ziv_header = [
            'puerta_enlace', 'punto_medida', 'equipo', 'Nan1', 'dia', 'hora', 'ai', 'quality_ai', 'ae',
            'quality_ae', 'r1', 'quality_r1', 'r2', 'quality_r2', 'r3', 'quality_r3',
            'r4', 'quality_r4', 'zc', 'Nan2', 'Nan3', 'Nan4', 'Nan5', 'Nan6', 'Nan7', 'Nan8', 'Nan9'
        ]

        # read curve file
        curve_file = get_module_resource(
            'giscedata_re', 'tests', 'curve.txt'
        )
        self.curve_file = open(curve_file, 'r')
        df = pd.read_csv(self.curve_file, sep='\t', names=ziv_header,
                         dtype={'dia': str, 'hora': str, 'punto_de_medida': int}
                         )

        # date to timestamp
        df = df.fillna(0)
        df['timestamp_day'] = df['dia'].map(
            lambda x: '{}-{}-{}'.format(x[:4], x[4:6], x[6:])
        )
        df['timestamp_hour'] = df['hora'].map(
            lambda x: ' {}:{}:00'.format(x[:2], x[2:])
        )
        df['timestamp'] = df['timestamp_day'] + df['timestamp_hour']
        df['magn'] = 1000
        df = df[['puerta_enlace', 'punto_medida', 'timestamp', 'ai', 'ae',
                 'r1', 'r2', 'r3', 'r4', 'magn']]

        # add the column 'season'
        df = re_obj.add_season_column(df)

        expected = ['S', 'S', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W',
                    'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W']

        returned = list(df.season)

        self.assertEqual(expected, returned)
