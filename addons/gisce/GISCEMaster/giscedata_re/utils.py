import calendar


def last_sunday(year, month):
    """
    Get last sunday of month to check hour change
    :param year: int year
    :param month: int month
    :return: int day
    """
    for day in reversed(range(1, calendar.monthrange(year, month)[1] + 1)):
        if calendar.weekday(year, month, day) == calendar.SUNDAY:
            return day
