# -*- coding: utf-8 -*-
import time
from base_extended.base_extended import MultiprocessBackground, NoDependency
from datetime import datetime
from dateutil.relativedelta import relativedelta
from osv import osv, fields
from tools.translate import _
from operator import itemgetter
import math
import decimal
import pandas as pd
import logging
from enerdata.datetime.timezone import TIMEZONE
from enerdata.profiles.profile import Profile, REProfile
from osv.expression import OOQuery
from oorq.decorators import job
from utils import last_sunday


TIPOS = [('1', 'Tipo 1'),
         ('2', 'Tipo 2'),
         ('3', 'Tipo 3'),
         ('4', 'Tipo 4'),
         ('5', 'Tipo 5')]

PROPIETAT_COMPTADOR_SELECTION = [('empresa', 'Empresa'),
                                  ('client', 'Client')]

PROFILE_TRANSLATION = {
    'AE': 'ai',
    'AS': 'ae',
    'R1': 'r1',
    'R2': 'r2',
    'R3': 'r3',
    'R4': 'r4',
}

ZIV_HEADER = [
    'puerta_enlace', 'punto_medida', 'equipo', 'Nan', 'dia', 'hora', 'ai',
    'quality_ai', 'ae', 'quality_ae', 'r1', 'quality_r1', 'r2', 'quality_r2',
    'r3', 'quality_r3', 'r4', 'quality_r4', 'zc'
]

DATETIME_FORMAT = '%d/%m/%Y %H:%M:%S'

profile_origin_vals = {
    'ai': 0.0,
    'ae': 0.0,
    'r1': 0.0,
    'r2': 0.0,
    'r3': 0.0,
    'r4': 0.0,
}


class GiscedataREProteccio(osv.osv):
    """Dispositius de protecció.
    """
    _name = 'giscedata.re.proteccio'
    _columns = {
      'name': fields.char('Nom Dispositiu Protector', size=64, required=True),
      'descripcio': fields.text('Descripció'),
    }
    _order = "name asc"

GiscedataREProteccio()

class GiscedataREConnexio(osv.osv):
    """Elements de connexió.
    """
    _name = 'giscedata.re.connexio'
    _columns = {
      'name': fields.char('Nom Element Connexió', size=64, required=True),
      'descripcio': fields.text('Descripció'),
    }
    _order = "name asc"

GiscedataREConnexio()

class GiscedataREInversor(osv.osv):
    """Inversor.
    """
    _name = 'giscedata.re.inversor'
    _columns = {
      'name': fields.char('Nom Element Connexió', size=64, required=True),
      'descripcio': fields.text('Descripció'),
    }
    _order = "name asc"

GiscedataREInversor()

class GiscedataRE(osv.osv):
    """Classe per les instal·lacions de Règim Especial.
    """
    _name = 'giscedata.re'

    _columns = {
        'name': fields.char('Nom', size=64, required=True),
        'descripcio': fields.char('Descripció', size=255),
        'cups': fields.many2one('giscedata.cups.ps', 'CUPS', required=True),
        'titular': fields.many2one('res.partner', 'Titular', required=True),
        'provincia': fields.char('Provincia/Subsistema', size=2, required=True,
                                 help=(u"Se pueden obtener los códigos de "
                                       u"provincias des de la página de REE "
                                       u"(http://simel.simel.ree.es/"
                                       u"index_simel_info.htm)")),
        'tipo': fields.selection(TIPOS, "Tipus", required=True),
        'uprs': fields.one2many('giscedata.re.uprs', 'inst_re',
                                'Histórico UPRs'),
        'unidad_lec': fields.selection([(1, 'kWh'), (1000, 'Wh')],
                                       'Unitat de lectura', required=True,
                                       help=(u"Establece la unidad de lectura "
                                             u"por defecto, en el momento de "
                                             u"importar una curva se puede "
                                             u"cambiar")),
        'perdidas': fields.float('Valor de pèrdues', required=True,
                                 help=(u"Valor por defecto de las pérdidas, se "
                                       u"puede cambiar al importar la curva")),
        'p_ae': fields.boolean('Activa Entrant', required=True,
                               help=(u"Aplicar pérdidas en Activa Entrante?, se "
                                     u"puede cambiar al importar la curva")),
        'p_as': fields.boolean('Activa Sortint', required=True,
                               help=(u"Aplicar pérdidas en Activa Saliente?, se "
                                     u"puede cambiar al importar la curva")),
        'active': fields.boolean('Activa', required=True),
        'curvas': fields.one2many('giscedata.re.curva', 'inst_re', 'Curvas'),
        'comptadors': fields.one2many('giscedata.re.comptador', 'inst_re',
                                      'Comptadors'),
        'punt_connexio_client': fields.char('Punt de Connexió Client',
                                            size=255),
        'potencia_pic': fields.float('Potència Pic (kW)'),
        'potencia_nominal': fields.float('Potència Nominal (kW)'),
        'potencia_nominal_max': fields.float('Potència Nominal Màxima (kW)'),
        'n_plaques': fields.integer('Nº de plaques'),
        'ondulador': fields.many2one('product.product', 'Ondulador'),
        'n_onduladors': fields.integer('Nº Onduladors'),
        'potencia_cc': fields.float('Potència de Curtcircuit'),
        'proteccions': fields.many2many('giscedata.re.proteccio',
                                        'giscedata_re_proteccio_rel', 'inst_re',
                                        'proteccio', 'Elements de protecció'),
        'connexions': fields.many2many('giscedata.re.connexio',
                                       'giscedata_re_connexio_rel', 'inst_re',
                                       'connexio', 'Connexions'),
        'inversor': fields.many2one('giscedata.re.inversor', 'Inversor'),
    }

    _defaults = {
      'unidad_lec': lambda *a: 1,
      'perdidas': lambda *a: 0,
      'p_ae': lambda *a: 0,
      'p_as': lambda *a: 0,
      'active': lambda *a: 1,
    }

    @staticmethod
    def get_estacio(season):
        """
        returns 1 if summer, 0 if winter, -1 if unknown.
        """
        if season == 'S':
            return 1
        elif season == 'W':
            return 0
        else:
            return -1

    @staticmethod
    def get_number_of_hours(date_from, date_to):
        """
        Returns number of hours from two dates
        :param date_from: Start of curve: Y-m-d 01:00:00
        :param date_to: End of curve Y-m-d 00:00:00
        :return: n_hours int
        """

        start = TIMEZONE.localize(datetime.strptime(date_from,
                                                    '%Y-%m-%d %H:00:00'))
        end = TIMEZONE.localize(datetime.strptime(date_to,
                                                  '%Y-%m-%d %H:00:00'))
        profile = Profile(start, end, [])

        return profile.n_hours

    @staticmethod
    def get_estacio_ts(timestamp):
        """
        returns S if summer, W if winter, -1 if unknown.
        source: https://stackoverflow.com/questions/13464009/calculate-tm-isdst-from-date-and-timezone
        """
        time1 = time.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
        mktime1 = time.mktime(time1)
        localtime1 = time.localtime(mktime1)
        season = localtime1.tm_isdst
        if season == 1:
            return 'S'
        elif season == 0:
            return 'W'
        else:
            return -1

    def add_season_column(self, dataframe):
        """
        returns the dataframe with a new column named 'season', indicating the time season with 'W' or 'S' values
        """
        # calculate october's last sunday (time change)
        _year = int(dataframe.timestamp[0].split('-')[0])
        target_hour = '{}-{}-{} 02:'.format(_year, 10, str(last_sunday(_year, 10)))
        # Fix duplicated hours (02:**)
        target_datetimes = dataframe[dataframe.timestamp.str.contains(target_hour)]
        # get conflictive winter hours in dataframe
        conflictive_hours_indexs = list(target_datetimes.index)
        summer_hours = conflictive_hours_indexs[:len(conflictive_hours_indexs) // 2]
        winter_hours = conflictive_hours_indexs[len(conflictive_hours_indexs) // 2:]
        dataframe['season'] = dataframe.timestamp.apply(lambda x: self.get_estacio_ts(x))
        # correct conflictive hours
        dataframe.loc[:summer_hours[-1], 'season'] = 'S'
        dataframe.loc[winter_hours[0]:, 'season'] = 'W'
        # reorder columns in dataframe
        order = ['puerta_enlace', 'punto_medida', 'timestamp', 'season', 'ai', 'ae', 'r1', 'r2', 'r3', 'r4', 'magn']
        return dataframe[order]

    @staticmethod
    def get_previous_day(day, month, year):
        the_date = datetime.strptime(year + month + day, '%Y%m%d')
        the_date = the_date - relativedelta(days=1)

        day = str(the_date.day).zfill(2)
        month = str(the_date.month).zfill(2)
        year = str(the_date.year)

        return day, month, year

    def check_exported_energy(self, cursor, uid, cups_name, profile_ids, origin):
        """
        Check if there are energy values in hours with zero RE coefficient
        :param cursor: db cursor
        :param uid: user id
        :param cups_name: a string with the cup's name
        :param profile_ids: a list of profile ids
        :param origin: 'tm' or 'tg'
        :return: True if the RE curve is valid, Else otherwise
        """
        crm_section_obj = self.pool.get('crm.case.section')
        crm_obj = self.pool.get('crm.case')
        profile_obj = self.pool.get('{}.profile'.format(origin))

        re_measures = []
        for profile in profile_obj.read(cursor, uid, profile_ids):
            re_measures.append(profile_obj.convert_to_profilehour_re(profile))
        valid, invalid_measures = REProfile.validate_exported_energy(re_measures)
        # invalid profile hours found
        if not valid:
            # create CRM to inform
            section_id = crm_section_obj.search(
                cursor, uid, [('code', '=', 'RESP')]
            )[0]
            crm_case_vals = {
                'name': _('Perfils RE invàlids al CUPS {}').format(cups_name),
                'section_id': section_id,
                'description': _("La curva RE no s'ha importat al haver trobat els següents perfils invàlids: {}"
                                 ).format('\n'.join(invalid_measures)),
            }
            crm_obj.create(cursor, uid, crm_case_vals)
            return False
        return True

    def get_climatic_zone(self, cursor, uid, cups):
        cups_obj = self.pool.get('giscedata.cups.ps')
        cups_data = cups_obj.q(cursor, uid).read(
            ['id_municipi.climatic_zone']
        ).where([('name', '=', cups)])
        cz = cups_data[0]['id_municipi.climatic_zone']

        return int(cz)

    def get_re_curva_lectura(self, cursor, uid, re_id, profile_v, re_curva_id,
                             measures, arrastres, origin=None, context=None):
        """

        :param cursor:
        :param uid:
        :param re_id:
        :param profile_v: profile LIKE
        {
            'ai': int, 'ae': int,
            'r1': int, 'r2': int, 'r3': int, 'r4': int, 'timestamp': str
        }
        :param re_curva_id:
        :param measures:
        :param arrastres:
        :param type: agree_tipe LIKE '3', '4', '5'
        :param context:
        :return:
        """
        if isinstance(re_id, list):
            raise osv.except_osv(
                "Error",
                "get_registers_from_curve functions expects one RE identifier"
            )

        re_obj = self.pool.get('giscedata.re')
        upr_obj = self.pool.get('giscedata.re.uprs')
        agcl_obj = self.pool.get('giscedata.perfils.agcl')

        year, month, day = profile_v['timestamp'][:10].split('-')
        hour = profile_v['timestamp'][11:13]
        name = year + month + day + hour
        estacio = self.get_estacio(profile_v['season'])
        # Translate 00 hour
        if hour == '00':
            hour = '24'
            day, month, year = self.get_previous_day(day, month, year)

        re_f = ['provincia', 'cups', 'uprs', 'tipo']
        re_v = re_obj.read(cursor, uid, re_id, re_f)
        upr_n = upr_obj.read(cursor, uid, re_v['uprs'][0], ['name'])['name']

        # Todo get default distribuidora from another form
        distribuidora = agcl_obj.default_distribuidora(cursor, uid, uid)

        # lectura_real: (lectura * magnitud) / 1000 (W -> kW)
        # perdues: perdues del contracte per cada magnitud (generalment 0)
        # lectura: int(lectura) (truncada)
        # arrastre: arrastre_anterior += part_decimal_lectura_real

        _tmp_vals = {
            'name': name,
            'year': year, 'month': month, 'day': day, 'hour': int(hour),
            'estacio': estacio,
            'cups': re_v['cups'][1],
            'provincia': re_v['provincia'],
            'tipo': re_v['tipo'],
            'upr': upr_n,
            'distribuidora': distribuidora,
            'curva': re_curva_id
        }
        perdues_config = 0
        perdues = dict.fromkeys(measures, 0)

        re_curva_lectura_vs = []
        for measure in measures:
            profile_measure = PROFILE_TRANSLATION[measure]
            lectura = (profile_v[profile_measure] * profile_v['magn']) / 1000

            re_curva_lectura_v = _tmp_vals.copy()
            re_curva_lectura_v.update({
                'lectura_real': lectura,
                'magnitud': measure,
                'perdues': perdues[measure] * perdues_config
            })

            if origin == 'tg':
                lectura -= lectura * re_curva_lectura_v['perdues']
                # get (arrastre, lectura) modf function (decimal_part, int part)
                arrastres[measure], lectura = math.modf(
                    lectura + arrastres[measure]
                )
                lectura = int(lectura)

                arrastre = round(arrastres[measure], 3)
                if arrastre >= 1.0:
                    # truncate to 3 decimals
                    arrastre = float(
                        decimal.Decimal(arrastres[measure]).quantize(
                            decimal.Decimal('.001'), rounding=decimal.ROUND_DOWN
                        )
                    )
            else:
                arrastre = 0.0

            re_curva_lectura_v.update({
                'arrastre': arrastre,
                'lectura': lectura
            })
            re_curva_lectura_vs.append(re_curva_lectura_v)

        return re_curva_lectura_vs

    def get_billing(self, cursor, uid, date_begin, date_end, meter_name, model):
        billing_obj = self.pool.get(model)
        date_begin = (
                datetime.strptime(date_begin, '%Y-%m-%d %H:%M:%S') -
                relativedelta(hours=1)
        ).strftime('%Y-%m-%d %H:%M:%S')
        # Get last billing on month
        search_params = [
            ('name', '=', meter_name), ('type', '=', 'month'),
            ('period', '=', 0), ('value', '=', 'i'),
            ('date_end', '<=', date_end),
            ('date_begin', '>=', date_begin)
        ]
        if model == 'tm.billing':
            search_params += [('contract', '=', 3)]
        billing_id = billing_obj.search(cursor, uid, search_params, order='date_end DESC', limit=1)
        if not billing_id:
            return False
        else:
            billing = billing_obj.read(cursor, uid, billing_id[0], ['ae'])['ae']

        return billing

    @job(queue='profiling', timeout=3600 * 2)
    def create_curve_async(self, cursor, uid, ids, init_date_str, end_date_str,
                           context=None):
        return self.create_curve(cursor, uid, ids, init_date_str, end_date_str,
                                 context=context)

    def create_curve(self, cursor, uid, ids, init_date_str, end_date_str,
                     context=None):
        """
        re_id: Identificador de la instalació de règim especial ('giscedata.re')
        di: data inicial de la corba
        df: data final de la corba
        """
        policy_o = self.pool.get('giscedata.polissa')
        meter_o = self.pool.get('giscedata.lectures.comptador')
        profile_tg_o = self.pool.get('tg.profile')
        profile_tm_o = self.pool.get('tm.profile')
        re_curva_o = self.pool.get('giscedata.re.curva')
        re_curva_lectura_o = self.pool.get('giscedata.re.curva.lectura')

        self_f = ['id', 'cups.name', 'tipo']
        self_dmn = [('id', 'in', ids)]
        q = self.q(cursor, uid).select(self_f).where(self_dmn)
        cursor.execute(*q)

        logger = logging.getLogger('openerp.{}'.format(__name__))

        for re_v in cursor.dictfetchall():
            cups_n = re_v['cups.name']
            climatic_zone = self.get_climatic_zone(cursor, uid, cups_n)
            logger.info(
                u"Important corbes a instalació RE amb CUPS: {}".format(cups_n)
            )
            # TODO: get meter from modcon
            policy_id = policy_o.search(
                cursor, uid, [('cups.name', '=', cups_n)]
            )
            meter_id = meter_o.search(
                cursor, uid, [('polissa', '=', policy_id)]
            )
            meter_f = meter_o.read(cursor, uid, meter_id[0], [
                'tg', 'technology_type'])
            profiled = False
            if meter_f['tg'] and meter_f['technology_type'] not in (
                    'electronic', 'telemeasure'):
                origin = 'tg'
                meter_name = meter_o.build_name_tg(cursor, uid, meter_id)
                profile_ids = profile_tg_o.search(cursor, uid, [
                    ('name', '=', meter_name),
                    ('timestamp', '>=', init_date_str),
                    ('timestamp', '<=', end_date_str)
                ])

                if not profile_ids:
                    logger.info(
                        u"El comptador {} no té cap perfil de telegestió entre "
                        u"{} - {}. Es perfilarà".format(
                            meter_name, init_date_str, end_date_str)
                    )
                else:
                    # check exported profiles with energy when zero coeffients
                    valid = self.check_exported_energy(cursor, uid, cups_n, profile_ids, origin)
                    if not valid:
                        continue
                # If not a complete and valid curve, profile
                n_hours = self.get_number_of_hours(init_date_str, end_date_str)
                if n_hours != len(profile_ids):
                    # need profile
                    billing = self.get_billing(
                        cursor, uid, init_date_str, end_date_str,
                        meter_name, 'tg.billing')
                    if not billing and billing != 0:
                        logger.info(
                            u"No s'ha trobat tancament pel comptador {}! No es "
                            u"pot perfilar".format(meter_name)
                        )
                        continue
                    billing = {'P0': billing}
                    climatic_zone = {'RE': climatic_zone}
                    profile_tg_o.fix_re(
                        cursor, uid, meter_name, init_date_str, end_date_str,
                        climatic_zone, billing, '10-00'
                    )
                    profile_ids = profile_tg_o.search(cursor, uid, [
                        ('name', '=', meter_name),
                        ('timestamp', '>=', init_date_str),
                        ('timestamp', '<=', end_date_str),
                        ('valid', '=', True),
                        ('cch_fact', '=', True)
                    ])
                    profile_hours = profile_tg_o.search_count(cursor, uid, [
                        ('name', '=', meter_name),
                        ('timestamp', '>=', init_date_str),
                        ('timestamp', '<=', end_date_str),
                        ('valid', '=', True),
                        ('cch_fact', '=', True),
                        ('cch_bruta', '=', False),
                    ])
                    profiled = True
                generic_profile_obj = profile_tg_o
            elif meter_f['technology_type'] in ('electronic', 'telemeasure'):
                origin = 'tm'
                meter_name = meter_o.read(
                    cursor, uid, meter_id, ['name'])[0]['name']
                profile_ids = profile_tm_o.search(cursor, uid, [
                    ('name', '=', meter_name),
                    ('timestamp', '>=', init_date_str),
                    ('timestamp', '<=', end_date_str),
                    ('type', '=', 'p'),
                ])

                if not profile_ids:
                    logger.info(
                        u"El comptador {} no té cap perfil de telemesura entre "
                        u"{} - {}. Es perfilarà".format(
                            meter_name, init_date_str, end_date_str)
                    )
                else:
                    # check exported profiles with energy when zero coeffients
                    valid = self.check_exported_energy(cursor, uid, cups_n, profile_ids, origin)
                    if not valid:
                        continue

                # If not a complete and valid curve, profile
                n_hours = self.get_number_of_hours(init_date_str, end_date_str)
                if n_hours != len(profile_ids):
                    # need profile
                    billing = self.get_billing(
                        cursor, uid, init_date_str, end_date_str,
                        meter_name, 'tm.billing')
                    if not billing and billing != 0:
                        logger.info(
                            u"No s'ha trobat tancament pel comptador {}! No es "
                            u"pot perfilar".format(meter_name)
                        )
                        continue
                    billing = {'P0': billing}
                    climatic_zone = {'RE': climatic_zone}
                    profile_tm_o.fix_re(
                        cursor, uid, meter_name, init_date_str, end_date_str,
                        climatic_zone, billing, '10-00'
                    )
                    profile_ids = profile_tm_o.search(cursor, uid, [
                        ('name', '=', meter_name),
                        ('timestamp', '>=', init_date_str),
                        ('timestamp', '<=', end_date_str),
                        ('valid', '=', True),
                        ('cch_fact', '=', True),
                        ('type', '=', 'p'),
                    ])
                    profile_hours = profile_tm_o.search_count(cursor, uid, [
                        ('name', '=', meter_name),
                        ('timestamp', '>=', init_date_str),
                        ('timestamp', '<=', end_date_str),
                        ('valid', '=', True),
                        ('cch_fact', '=', True),
                        ('cch_bruta', '=', False),
                        ('type', '=', 'p'),
                    ])
                    profiled = True
                generic_profile_obj = profile_tm_o
            else:
                logger.info(
                    u"Comptador Electromecànic: {}. Cal actualitzar la "
                    u"tecnología".format(meter_id, init_date_str, end_date_str)
                )
                continue
            if not profiled:
                observations = 'Curva completa'
            else:
                observations = 'Curva perfilada. {} horas perfiladas.'.format(profile_hours)
            create_date = datetime.now().strftime(DATETIME_FORMAT)

            vals = {
                'name': 'Curva creada el: {}'.format(create_date),
                'inici': init_date_str,
                'final': end_date_str,
                'inst_re': re_v['id'],
                'compact': True,
                'observacions': observations
            }
            re_curva_id = re_curva_o.create(cursor, uid, vals)

            measures = ['AE', 'AS', 'R1', 'R2', 'R3', 'R4']
            arrastres = dict.fromkeys(measures, 0)
            to_check = []
            profile_vs = generic_profile_obj.read(cursor, uid, profile_ids, [])
            for profile_v in sorted(profile_vs, key=itemgetter('timestamp')):
                if profiled:
                    ae_fact = profile_v.pop('ae_fact', 0.0)
                    profile_v['ae'] = ae_fact
                profile = profile_origin_vals.copy()
                profile.update(profile_v)
                logger.info(u"Timestamp: {}".format(profile['timestamp']))
                re_curva_lectura_vs = self.get_re_curva_lectura(
                    cursor, uid, re_v['id'], profile, re_curva_id, measures,
                    arrastres, origin=origin, context=context
                )

                for re_curva_lectura_v in re_curva_lectura_vs:
                    to_check.append(re_curva_lectura_v)
                    re_curva_lectura_o.create(cursor, uid, re_curva_lectura_v)

            logger.info(u"Corba de la instalacio amb CUPS {} creada correctament".format(cups_n))
            # Validate hour
            end_date = datetime.strptime(end_date_str, '%Y-%m-%d %H:%M:%S')
            init_date = datetime.strptime(init_date_str, '%Y-%m-%d %H:%M:%S')

            diff = end_date - init_date
            total = diff.seconds / 3600 * 6

            if total != len(to_check):
                logger.info(
                    u"La corba amb CUPS {} no s'ha validat correctament".format(
                        cups_n
                    )
                )

        return True

    def get_instalacio_by_meter_info(self, cursor, uid, porta_enllac,
                                     punt_mesura):
        tm_lectures = self.pool.get('giscedata.lectures.comptador')
        re_obj = self.pool.get('giscedata.re')
        pol_obj = self.pool.get('giscedata.polissa')
        tm_ids = tm_lectures.search(cursor, uid,
            [
                ('tm_link_address', '=', porta_enllac),
                ('tm_measuring_point', '=', punt_mesura)
            ]
        )
        meter_data = tm_lectures.read(cursor, uid,
            tm_ids, ['polissa']
        )[0]
        cups_name = pol_obj.browse(
            cursor, uid, meter_data['polissa'][0]).cups.name
        re_ids = re_obj.search(cursor, uid, [('cups', '=', str(cups_name))])

        return re_ids, meter_data['id']

    def import_re_curve_from_file(self, cursor, uid, content):
        """
        Construeix perfils de telegestió provinents d'un fitxer importat
        Perfiles como
        :param file_path: ruta del fitxer a importar
        :return:
        """
        df = pd.DataFrame(data=content)
        df = df.fillna(0)
        df['timestamp_day'] = df['dia'].map(
            lambda x: '{}-{}-{}'.format(x[:4], x[4:6], x[6:])
        )
        df['timestamp_hour'] = df['hora'].map(
            lambda x: ' {}:{}:00'.format(x[:2], x[2:])
        )
        df['timestamp'] = df['timestamp_day'] + df['timestamp_hour']
        df['magn'] = 1000
        df = df[['puerta_enlace', 'punto_medida', 'timestamp', 'ai', 'ae',
                 'r1', 'r2', 'r3', 'r4', 'magn']]

        logger = logging.getLogger('openerp.{}'.format(__name__))
        try:
            inst_re_id, meter_id = self.get_instalacio_by_meter_info(
                cursor, uid, int(df['puerta_enlace'][0]), int(df['punto_medida'][0])
            )
        except Exception as err:
            logger.info(
                u"No existeix aquest punt de régim especial "
                u"{}".format(list(df['puerta_enlace'])[0])
            )
            return False

        re_curva_obj = self.pool.get('giscedata.re.curva')
        re_curva_lectura_obj = self.pool.get('giscedata.re.curva.lectura')
        re_obj = self.pool.get('giscedata.re')
        create_date = datetime.now().strftime(DATETIME_FORMAT)
        date_start = min(df['timestamp'])[:10]
        date_end = max(df['timestamp'])[:10]
        vals = {
            'name': 'Curva creada el: {}'.format(create_date),
            'inici': date_start,
            'final': date_end,
            'inst_re': inst_re_id[0],
            'compact': True
        }
        re_curva_id = re_curva_obj.create(cursor, uid, vals)

        measures = ['AE', 'AS', 'R1', 'R2', 'R3', 'R4']
        arrastres = dict.fromkeys(measures, 0)

        df = self.add_season_column(df)

        data_set = df.T.to_dict().values()
        to_check = []
        for profile in sorted(data_set, key=itemgetter('timestamp')):
            curva_lectura_data = re_obj.get_re_curva_lectura(
                cursor, uid, inst_re_id[0], profile, re_curva_id,
                measures, arrastres
            )
            for curva_lectura in curva_lectura_data:
                to_check.append(curva_lectura)
                re_curva_lectura_obj.create(cursor, uid, curva_lectura)

        hour_change = 1
        nhours = ((31 * 24) + hour_change) * len(measures)
        if nhours != len(to_check):
            logger.info(
                u"La curva no té el número d'hores correcte {}".format(
                    list(df['puerta_enlace'])[0]
                )
            )

GiscedataRE()


class GiscedataREComptador(osv.osv):
    """Comptadors de Règim especial.
    """
    _name = 'giscedata.re.comptador'
    _columns = {
        'name': fields.char('Nº de sèrie', size=32, required=True),
        'inst_re': fields.many2one('giscedata.re', 'Instal·lació RE',
                                   required=True),
        'active': fields.boolean('Actiu'),
        'data_alta': fields.date('Data Alta', select="1"),
        'data_baixa': fields.date('Data Baixa', select="1"),
        'propietat': fields.selection(PROPIETAT_COMPTADOR_SELECTION,
                                      "Propietat de"),
        'serial': fields.many2one("stock.production.lot",
                                  "Nº de sèrie (magatzem)", select="1"),
        'product_id': fields.related('serial', 'product_id', type='many2one',
                                     relation='product.product', store=True,
                                     readonly=True, string="Producte"),
        'tensio': fields.integer('Tensió'),
        'conf_telelectura': fields.char('Conf. Telelectura', size=255),
    }

    _defaults = {
      'active': lambda *a: 1,
      'data_alta': lambda *a: time.strftime('%Y-%m-%d'),
      'propietat': lambda *a: 'empresa',
      'conf_telelectura': lambda *a: 'com1:9600:8:None:1:1:1:2:2000:1I',
    }

    _order = 'data_alta desc'

    _sql_constraints = [
        ('data_baixa_alta', 'CHECK (data_baixa >= data_alta)',
         _("La data de baixa del comptador no pot ser menor a la data d'alta")),
    ]

GiscedataREComptador()

class GiscedataREUPRS(osv.osv):

    _name = 'giscedata.re.uprs'

    _columns = {
      'name': fields.char('Unidad Programación', size=8, required=True),
      'representante': fields.many2one('res.partner', 'Representate',
                                       required=True),
      'inst_re': fields.many2one('giscedata.re', 'Instalación RE',
                                 required=True),
      'data_alta': fields.datetime('Fecha Alta', required=True),
      'data_baixa': fields.datetime('Fecha Baja', required=True),
    }

    _defaults = {
      'data_alta': lambda *a: time.strftime('%Y-%m-%d 00:00:00'),
      'data_baixa': lambda *a: '3000-01-01 00:00:00',
    }

    _order = "data_alta desc"

    # TODO: Comprovar que no hi hagi 'overlaps' de dates en unitats de
    # programacio per instal·lació

GiscedataREUPRS()

class GiscedataRECurva(osv.osv):

    _name = 'giscedata.re.curva'

    _columns = {
      'name': fields.char('Descripción', size=64, required=True),
      'inici': fields.datetime('Inicio', required=True, readonly=True),
      'final': fields.datetime('Final', required=True, readonly=True),
      'inst_re': fields.many2one('giscedata.re', 'Instalación RE',
                                 required=True),
      'compact': fields.boolean('Compactada', required=True, readonly=True),
      'observacions': fields.text('Observaciones'),
      'hores': fields.one2many('giscedata.re.curva.lectura', 'curva', 'Hores'),
    }

    _defaults = {
      'compact': lambda *a: 0,
    }

    _order = "final desc"

    def create(self, cursor, uid, vals, context=None):
        search_params = [
            ('inici', '=', vals['inici']),
            ('final', '=', vals['final']),
            ('inst_re', '=', vals['inst_re'])
        ]
        # check if exists an RE profile with the same installation and date range
        existing_profiles = self.search(cursor, uid, search_params, context=context)
        if existing_profiles:
            # delete existent RE profiles to avoid duplicity
            self.unlink(cursor, uid, existing_profiles)
        return super(GiscedataRECurva, self).create(cursor, uid, vals, context)


GiscedataRECurva()


class GiscedataRECurvaLectura(osv.osv):

    _name = 'giscedata.re.curva.lectura'

    _columns = {
      'curva': fields.many2one('giscedata.re.curva', 'Curva', required=True,
                               ondelete='cascade'),
      'name': fields.char('Timestamp', size=10, required=True),
      'cups': fields.char('CUPS', size=25, required=True),
      'distribuidora': fields.char('Distribuidora', size=4),
      'tipo': fields.selection(TIPOS, 'Tipo de Punto'),
      'magnitud': fields.char('Maginitud', size=2),
      'year': fields.char('Any', size=4, required=True),
      'month': fields.char('Mes', size=2, required=True),
      'day': fields.char('Dia', size=2, required=True),
      'hour': fields.integer('Hora', required=True),
      'estacio': fields.integer('Estació', selection=[(0, 'Hivern'),
                                                      (1, 'Estiu')],
                                required=True),
      'lectura_real': fields.integer('Lectura Real'),
      'perdues': fields.float('Perdues'),
      'lectura': fields.integer('Lectura', required=True),
      'arrastre': fields.float('Arrastre', digits=(3,3)),
      'upr': fields.char('Unidad Programación', size=8, required=True),
      'provincia': fields.char('Provincia', size=2, required=True)
    }

    _order = "name asc, magnitud asc"

GiscedataRECurvaLectura()
