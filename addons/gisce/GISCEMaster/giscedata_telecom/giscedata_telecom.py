# -*- coding: utf-8 -*-
"""
giscedata_telecom.py

Fitxer amb classes comunes per telecomunicacions.

"""

from osv import fields, osv
from tools.translate import _

_tipus_installacio = [('PMP', _('Punt-MultiPunt')),
                      ('SUB', _('Subscriptora')),
                      ('PaP', _('Punt-a-Punt')),
                      ('CPE', _('CPE')),
                      ('BASE', _('Base')),
                      ('AGRS', _('Agregació simple')),
                      ('AGRR', _('Agregació Redundada'))]

_configuracio_node_installacio = [('C', _('Coordinador')),
                                  ('B', _('Bàsic')),
                                  ('A', _('Avançat')),
                                  ('T', _('Troncal'))]

_conversor_medi_fo = [('rs232', 'RS-232'),
                      ('rs485', 'RS-485'),
                      ('rs422', 'RS-422'),
                      ('eth', 'ETH'),
                      ('eia530', 'EIA-530'), ]

_tecnologia_medi_plc = [('prime', 'PRIME'),
                        ('bplc', 'BPLC'),
                        ('ssplc', 'SSPLC'),
                        ('nplc', 'NPLC'),
                        ('other', _('Altres')), ]

_tensions_norm = {'ac': [('230', '230'),
                         ('110', '110'),
                         ('66', '66')],
                  'dc': [('9', '9'),
                         ('12', '12'),
                         ('24', '24'),
                         ('48', '48'),
                         ('60', '60'),
                         ('110', '110'),
                         ('125', '125'),
                         ('220', '220'),
                         ('230', '230')]}


def inherits_base_model_view_get(self, cr, orig_view, base_view,
                                 base_model='giscedata.telecom.equip.base',
                                 module_orig='giscedata_telecom',
                                 module='giscedata_telecom'):
    """HACK ecarreras:
       * Per poder heretar la vista de model giscedata_telecom_equip
         incloses les possibles herencies que la modifiquen fem aquest
         joc de mans:
         1) Creen una vista buida (base_view) a un xml
         2) Omplim la vista amb la vista (orig_view) del model base
            (base_model)
         3) Heretarem d'aquesta vista normalment l'xml

        Exemple:
            orig_view = 'view_telecom_equip_form'
            base_view = 'view_telecom_equips_sisaux_form_base'
            base_model = 'giscedata.telecom.equip.base'
            module_orig = 'giscedata_telecom'
            module = 'giscedata_telecom'
         """
    user_id = 1

    ir_model_data = self.pool.get('ir.model.data')
    base_obj = self.pool.get(base_model)
    ir_ui_view = self.pool.get('ir.ui.view')

    orig_view_id = ir_model_data.search(cr, user_id,
                                        [('name', '=', orig_view),
                                         ('module', '=', module_orig)])

    if not orig_view_id:
        return

    orig_view_res_id = ir_model_data.read(cr, user_id, orig_view_id[0],
                                          ['res_id'])
    orig_view_fields = base_obj.fields_view_get(cr, user_id,
                                                orig_view_res_id['res_id'],
                                                context={'lang': 'en_US'})

    orig_view_arch = orig_view_fields.get('arch', None)

    base_view_id = ir_model_data.search(cr, user_id,
                                        [('name', '=', base_view),
                                         ('module', '=', module)])

    # Quan instal.lem això no s'acomplirà
    if base_view_id:
        base_view_res_id = ir_model_data.read(cr, user_id,
                                              base_view_id[0], ['res_id'])

        ir_ui_view.write(cr, user_id, base_view_res_id['res_id'],
                         {'arch': orig_view_arch})


class GiscedataTelecomInstallacio(osv.osv):
    """Classe per representar instalacions de telecomunicacions.

       Tenen un codi, i poden estar relacionades amb altres instalacions com
       CT's, Subestacions, etc.. que li donaran referècnia geogràfica

       Dins una instalacio trobarem els equips de telecomunicacions.
    """
    _name = 'giscedata.telecom.installacio'
    _description = 'Instalacio de telecomunicacions'

    def _situacio_get(self, cr, uid, context=None):
        s_obj = self.pool.get('giscedata.telecom.installacio.situacio.model')
        model_obj = self.pool.get('ir.model')

        s_models = s_obj.read(cr, uid, s_obj.search(cr, uid, []),
                              ['model_name', 'description'])

        opt = [m for m in s_models if self.pool.get(m['model_name'])]

        return [(m['model_name'], m['description']) for m in opt]

    def _situacio_search(self, cr, uid, obj, name, args, context=None):
        """ Per cercar segons el nom de la situació de la instal·lació """
        if not context:
            context = {}
        if not len(args):
            return []

        ids = []
        ids_model = []
        search_param = [('name', a[1], a[2]) for a in args]
        for model in self._situacio_get(cr, uid, context=context):
            ids_model = self.pool.get(model[0]).search(cr, uid, search_param)
            sit_str = ["%s,%d" % (model[0], i) for i in ids_model]
            ids += self.search(cr, uid, [('situacio', 'in', sit_str)])

        return [('id', 'in', ids)]

    def _situacio_name(self, cr, uid, ids, name, arg, context=None):
        """ Nom de la situació de la instal·lació """
        res = dict([(i, False) for i in ids])

        installacions = self.browse(cr, uid, ids)
        for i in installacions:
            if i.situacio:
                (model, id) = i.situacio.split(',')
                if model and int(id):
                    res[i.id] = self.pool.get(model).name_get(cr, uid, int(id)
                                                              )[0][1]

        return res

    _columns = {
        'name': fields.char('Nom', size=64, required=True,
                            help='Nom descriptiu de la instal·lació',),
        'codi': fields.char('Codi', size=16, required=True,
                            help='Codi semàntic de la instal·lació',),
        'tipus': fields.selection(_tipus_installacio, 'Tipus', required=True,
                                  help='Tipus instal·lació segons topologia',),
        'node': fields.selection(_configuracio_node_installacio, 'Node',
                                 required=True,
                                 help="Configuració de l'enllaç",),
        'situacio': fields.reference('Equipament on està ubicada',
                                     selection=_situacio_get, size=128),
        'nom_situacio': fields.function(_situacio_name, method=True,
                                        string="Situació", type='char',
                                        fnct_search=_situacio_search),
        'sistemes_auxiliars_id': fields.one2many(
            'giscedata.telecom.equip.sistemaauxiliar', 'installacio_id',
            string='Sistemes Auxiliars', required=True),
        'routers_id': fields.one2many('giscedata.telecom.equip.router',
                                      'installacio_id', string='Routers',
                                      required=True),
        'ip_gestio': fields.char('IP', size=15),
        'mask_gestio': fields.char('màscara', size=15),
        'dns_gestio': fields.char('Nom DNS', size=64),
        'mac_gestio': fields.char('MAC (xx:xx:xx:xx:xx)', size=17),
        'wireless_id': fields.one2many('giscedata.telecom.equip.wireless',
                                       'installacio_id', string='Wireless',
                                       required=True),
        'operadors_id': fields.one2many('giscedata.telecom.equip.operador',
                                        'installacio_id', string='Operadors',
                                        required=True),
        'fos_id': fields.one2many('giscedata.telecom.equip.fo',
                                  'installacio_id', string='Fibra Òptica',
                                  required=True,),
        'plc_id': fields.one2many('giscedata.telecom.equip.plc',
                                  'installacio_id', string='PLC',
                                  required=True,),
    }

GiscedataTelecomInstallacio()


class GiscedataTelecomInstallacioSituacioModel(osv.osv):
    """ Models susceptibles de ser instal·lacions de telecomunicacions """

    _name = 'giscedata.telecom.installacio.situacio.model'
    _descripicio = '''Actius que poden contenir instal·lacions de
                      telecomunicacions '''

    _columns = {
        'model_name': fields.char('Module Name', size=64, required=True,
                                  readonly=True),
        'description': fields.char('Descripcio', size=64, required=True,
                                   readonly=True),
    }

GiscedataTelecomInstallacioSituacioModel()


class GiscedataTelecomEquipBase(osv.osv):
    """ Model genèric d'equip de telecomunicacions

        Inclou tots els camps comuns d'un equip de telecomunicacions
        La vinculació amb el producte i el número de sèrie en seria un exemple
    """
    _name = 'giscedata.telecom.equip.base'
    _description = "Model base per tots els equips"

    def _get_product(self, cr, uid, ids, field_name, arg, context=None):
        """ Producte segons numero de sèrie"""
        res = dict([(i, False) for i in ids])

        equips = self.browse(cr, uid, ids)
        for equip in equips:
            if equip.serial_number:
                res[equip.id] = equip.serial_number.product_id.id

        return res

    def _get_n_ports(self, cr, uid, ids, field_name, arg, context=None):
        """ Calcula el número de ports"""
        res = dict([(i, False) for i in ids])

        equips = self.browse(cr, uid, ids)
        for equip in equips:
            res[equip.id] = len(equip.ports)

        return res

    def _search_product(self, cr, uid, obj, field_name, args, context=None):
        """ Cerca el producte a partir del nom"""
        if not context:
            context = {}
        if not len(args):
            return []

        prod_obj = self.pool.get('product.product')
        sn_obj = self.pool.get('stock.production.lot')

        search = [('name', a[1], a[2]) for a in args]

        prod_ids = prod_obj.search(cr, uid, search)
        sn_ids = sn_obj.search(cr, uid, [('product_id', 'in', prod_ids)])
        return [('serial_number', 'in', sn_ids)]

    def _get_installacio(self, cr, uid, context=None):
        res = context.get('installacio_id', False)
        return res

    _columns = {
        'name': fields.char('Descripció', size=64, required=True),
        'serial_number': fields.many2one('stock.production.lot',
                                         'Número de sèrie', required=True),
        'product_id': fields.function(_get_product, string='Model',
                                      fnct_search=_search_product,
                                      type='many2one', obj='product.product',
                                      readonly=True, method=True),
        'installacio_id': fields.many2one('giscedata.telecom.installacio',
                                          'Instal·lació', required=True),
        'data_installacio': fields.date('Data instal.lació', required=True),
        'active': fields.boolean('Actiu'),
        'n_ports': fields.function(_get_n_ports, 'Num. Ports', type='integer',
                                   readonly=True, method=True),
        'ports': fields.one2many('giscedata.telecom.port', 'equip_id',
                                 string='Ports'),
    }

    _defaults = {
        'active': lambda *a: True,
        'installacio_id': _get_installacio,
    }

GiscedataTelecomEquipBase()


class GiscedataTelecomPortServei(osv.osv):
    """ Servei assignat a un port """
    _name = 'giscedata.telecom.port.servei'
    _description = "Tipus de servei de dades que es transmet pel port"

    _columns = {
        'name': fields.char('Name', size=32, required=True, readonly=True),
        'codi': fields.integer('Codi', required=True),
        'critic': fields.boolean('Criticitat', required=True),
        'sensible': fields.boolean('Sensibilitat', required=True),
        'velocitat': fields.char('Velocitat', size=16, required=True),
        'trafic': fields.selection([('C', _('Constant')),
                                    ('A', _('Aleatori')),
                                    ('P', _('Periódic'))], 'Tipus Tràfic',
                                   required=True),
        'latencia': fields.float('Latencia Màx. [s]', required=True),
    }

GiscedataTelecomPortServei()


class GiscedataTelecomPortConnector(osv.osv):
    """Classe per representar els connectors disponibles a un port d'un equip.
    """
    _name = 'giscedata.telecom.port.connector'
    _description = 'Connectors disponibles pels ports'
    _columns = {
        'codi': fields.char('Codi', size=16, required=True),
        'name': fields.char('Nom', size=255, required=True),
    }

GiscedataTelecomPortConnector()


class GiscedataTelecomPort(osv.osv):
    """ Ports de telecomunicacions per associar als equips """
    _name = 'giscedata.telecom.port'
    _description = "Model port de comunicacions d'un equip"

    def _get_name(self, cr, uid, context=None):
        """ Nom del port """
        equip_id = context.get('equip_id', False)
        model = context.get('model', False)
        if not equip_id or not model:
            return 'Port'
        equip = self.pool.get(model).browse(cr, uid, equip_id)
        return _("Port %02d") % (len(equip.ports) + 1)

    def _get_next_port(self, cr, uid, context=None):
        """ Assigna el ordre del port mirant l'últim"""
        equip_id = context.get('equip_id', False)
        model = context.get('model', False)
        if not equip_id or not model:
            return 1
        equip = self.pool.get(model).browse(cr, uid, equip_id)
        return len(equip.ports) + 1

    def _get_equip_id(self, cr, uid, context=None):
        """ Selecciona l'equip automàticament"""
        equip_id = context.get('equip_id', False)
        model = context.get('model', False)
        if not equip_id or not model:
            return False

        res = self.pool.get(model).browse(cr, uid, equip_id).equip_id.id

        return res

    def _get_default_connector(self, cr, uid, context=None):
        """ Selecciona el connector RJ45 com a per defecte"""
        ir_md_obj = self.pool.get('ir.model.data')
        model = 'giscedata.telecom.port.connector'
        v_id = ir_md_obj.search(cr, uid, [('model', '=', model),
                                          ('name', '=', 'connector_rj45')])
        if v_id:
            return ir_md_obj.read(cr, uid, v_id, ['res_id'])[0]['res_id']
        else:
            return False

    def _get_velocitat_str(self, cr, uid, ids, field_name, arg, context=None):
        """ Velocitat Bonica"""
        res = dict([(i, '') for i in ids])

        ports = self.read(cr, uid, ids, ['velocitat'])
        for port in ports:
            if port['velocitat'] < 1000:
                factor = 1
                unit = "Kbps"
            elif port['velocitat'] < 1000000:
                factor = 1000
                unit = "Mbps"
            elif port['velocitat'] < 1000000000:
                factor = 1000000
                unit = "Gbps"

            res[port['id']] = "%g %s" % (round(port['velocitat'] / factor, 3),
                                         unit)

        return res

    _columns = {
        'name': fields.char('Nom', size=16),
        'equip_id': fields.many2one('giscedata.telecom.equip.base', 'Equip',
                                    size=128, required=True),
        'ordre': fields.integer('Ordre', required=True, readonly=True),
        'tipus': fields.selection([('E', _('Elèctric')), ('O', _('Òptic')),
                                   ('R', _('Ràdio'))], 'Tipus', required=True),
        'connector': fields.many2one('giscedata.telecom.port.connector',
                                     'Connector', required=True),
        'velocitat': fields.float('Velocitat (kbps)', required=True),
        'velocitat_str': fields.function(_get_velocitat_str, type='char',
                                         string='Velocitat', method=True,),
        'servei': fields.many2one('giscedata.telecom.port.servei', 'Servei'),
        'ip': fields.char('IP', size=15),
        'mask': fields.char('màscara', size=15),
        'dns': fields.char('Nom DNS', size=64),
        'mac': fields.char('MAC (xx:xx:xx:xx:xx)', size=17),
        'en_us': fields.boolean('En Ús')
    }

    _defaults = {
        'name': _get_name,
        'ordre': _get_next_port,
        'equip_id': _get_equip_id,
        'tipus': lambda *a: 'E',
        'connector': _get_default_connector,
    }

GiscedataTelecomPort()


class GiscedataTelecomEquipSistemaAuxiliar(osv.osv):
    """Equip sistema auxiliar (Bateries) """
    _name = "giscedata.telecom.equip.sistemaauxiliar"
    _description = "Sistema Auxiliar de telecomunicacions (Bateries)"
    _inherits = {"giscedata.telecom.equip.base": "equip_id"}

    def init(self, cursor):
        """ Es crida cada cop que inicialitzem el mòdul"""

        # Hereta la vista de Equip Base
        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_form',
                                     'view_telecom_equips_sisaux_form_base')

        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_tree',
                                     'view_telecom_equips_sisaux_tree_base')

    _columns = {
        'equip_id': fields.many2one('giscedata.telecom.equip.base', 'Equip',
                                    required=True),
        'data_install_bateries': fields.date("Data instal·lació Bateries"),
        'tensio_alimentacio': fields.selection(_tensions_norm['ac'],
                                               "Tensió d'alimentació"),
        'tensio_sortida': fields.selection(_tensions_norm['dc'],
                                           "Tensió de sortida"),
    }

GiscedataTelecomEquipSistemaAuxiliar()


class GiscedataTelecomEquipRouter(osv.osv):
    """Equip Router"""
    _name = "giscedata.telecom.equip.router"
    _description = "Router/Switch"
    _inherits = {"giscedata.telecom.equip.base": "equip_id"}

    def init(self, cursor):
        """ Es crida cada cop que inicialitzem el mòdul"""

        # Hereta la vista de Equip Base
        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_form',
                                     'view_telecom_equips_router_form_base')

        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_tree',
                                     'view_telecom_equips_router_tree_base')

    _columns = {
        'equip_id': fields.many2one('giscedata.telecom.equip.base', 'Equip',
                                    required=True),
        'tensio_aux1': fields.selection(_tensions_norm['ac'],
                                        "Tensió Auxiliar (AC)"),
        'tensio_aux2': fields.selection(_tensions_norm['dc'],
                                        "Tensió Auxiliar (DC)"),
        'id_epsa': fields.char("Identificació EPSA", size=64),
    }

GiscedataTelecomEquipRouter()


class GiscedataTelecomEquipWireless(osv.osv):
    """Equip Wireless"""
    _name = "giscedata.telecom.equip.wireless"
    _description = "Wireless"
    _inherits = {"giscedata.telecom.equip.base": "equip_id"}

    def init(self, cursor):
        """ Es crida cada cop que inicialitzem el mòdul"""

        # Hereta la vista de Equip Base
        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_form',
                                     'view_telecom_equips_wireless_form_base')

        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_tree',
                                     'view_telecom_equips_wireless_tree_base')

    _columns = {
        'equip_id': fields.many2one('giscedata.telecom.equip.base', 'Equip',
                                    required=True),
        'tensio_ac': fields.selection(_tensions_norm['ac'],
                                      "Tensió Auxiliar (AC)"),
        'tensio_dc': fields.selection(_tensions_norm['dc'],
                                      "Tensió Auxiliar (DC)"),
        'tecnologia': fields.selection([('wifi', 'WiFi'), ('wimax', 'WiMAX'),
                                        ('zbee', 'Zigbee')],
                                       string="Tecnologia", size=16),
        'enllas': fields.char("Enllaç", size=64),
    }

GiscedataTelecomEquipWireless()


class GiscedataTelecomEquipOperador(osv.osv):
    """Equip Operador"""
    _name = "giscedata.telecom.equip.operador"
    _description = "Operador"
    _inherits = {"giscedata.telecom.equip.base": "equip_id"}

    def init(self, cursor):
        """ Es crida cada cop que inicialitzem el mòdul"""

        # Hereta la vista de Equip Base
        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_form',
                                     'view_telecom_equips_operador_form_base')

        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_tree',
                                     'view_telecom_equips_operador_tree_base')

    _columns = {
        'equip_id': fields.many2one('giscedata.telecom.equip.base', 'Equip',
                                    required=True),
        'tecnologia': fields.selection([('3g', '3G'), ('4g', '4G'),
                                        ('umts', 'UMTS'), ('gprs', 'GPRS'),
                                        ('wimax', 'WiMAX'), ('xdsl', 'xDSL'),
                                        ('cable', _('Cable')),
                                        ('others', _('Altres'))],
                                       string="Tecnologia"),
        'sim_pin': fields.char("PIN", size=4),
        'sim_puk': fields.char("PUK", size=8),
        'imei': fields.char("IMEI", size=15),
    }

GiscedataTelecomEquipOperador()


class GiscedataTelecomEquipFibraOptica(osv.osv):
    """Equip Fibra Òptica"""
    _name = "giscedata.telecom.equip.fo"
    _description = "Fibra Òptica"
    _inherits = {"giscedata.telecom.equip.base": "equip_id"}

    def init(self, cursor):
        """ Es crida cada cop que inicialitzem el mòdul"""

        # Hereta la vista de Equip Base
        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_form',
                                     'view_telecom_equips_fo_form_base')

        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_tree',
                                     'view_telecom_equips_fo_tree_base')

    def _get_patch_lliures(self, cr, uid, ids, field_name, arg, context=None):
        """ Calcula els patch lliures en funció dels ocupats """
        res = dict([(i, '') for i in ids])

        fos = self.read(cr, uid, ids,
                        ['patch_num_circuits', 'patch_cir_ocupats'])
        for fo in fos:
            res[fo['id']] = fo['patch_num_circuits'] - fo['patch_cir_ocupats']

        return res

    _columns = {
        'equip_id': fields.many2one('giscedata.telecom.equip.base', 'Equip',
                                    required=True),
        'enllas': fields.char('Enllaç', size=64),
        'num_fibres': fields.integer('Num. Fibres'),
        'num_parells': fields.integer('Num. Parells'),
        'id_circuit': fields.char('Identificador Circuït', size=64),
        'tecnologia': fields.selection([('mono', 'Monomode'),
                                        ('multi', 'Multimode')],
                                       string='Tecnologia'),
        'patch_num_circuits': fields.integer('Num. Circuïts'),
        'patch_cir_ocupats': fields.integer('Ocupats'),
        'patch_cir_lliures': fields.function(_get_patch_lliures,
                                             string='Lliures', type='integer',
                                             method=True),
        'tensio_ac': fields.selection(_tensions_norm['ac'], 'Tensio AC'),
        'tensio_dc': fields.selection(_tensions_norm['dc'], 'Tensio DC'),
        'conversor_medi': fields.selection(_conversor_medi_fo,
                                           string="Conversor de medi")
    }

GiscedataTelecomEquipFibraOptica()


class GiscedataTelecomEquipPLC(osv.osv):
    """Equip PLC"""
    _name = "giscedata.telecom.equip.plc"
    _description = "PLC"
    _inherits = {"giscedata.telecom.equip.base": "equip_id"}

    def init(self, cursor):
        """ Es crida cada cop que inicialitzem el mòdul"""

        # Hereta la vista de Equip Base
        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_form',
                                     'view_telecom_equips_plc_form_base')

        inherits_base_model_view_get(self, cursor,
                                     'view_telecom_equip_tree',
                                     'view_telecom_equips_plc_tree_base')

    _columns = {
        'equip_id': fields.many2one('giscedata.telecom.equip.base', 'Equip',
                                    required=True),
        'tensio_ac': fields.selection(_tensions_norm['ac'], 'Tensio [AC]'),
        'tensio_dc': fields.selection(_tensions_norm['dc'], 'Tensio [DC]'),
        'tecnologia': fields.selection(_tecnologia_medi_plc,
                                       string='Tecnologia'),
        'frequencia': fields.char('Freqüencia', size=64),
        'enllas': fields.char('Enllaç', size=64),
        'acobladors': fields.one2many('giscedata.telecom.equip.plc.acoblador',
                                      'plc_id', string='Acobladors PLC',
                                      required=True),
    }

GiscedataTelecomEquipPLC()


class GiscedataTelecomEquipPLCAcoblador(osv.osv):
    """Equip Acoblador PLC"""
    _name = "giscedata.telecom.equip.plc.acoblador"
    _description = "Acoblador PLC"

    def _get_product(self, cr, uid, ids, field_name, arg, context=None):
        """ Producte segons numero de sèrie"""
        res = dict([(i, False) for i in ids])

        equips = self.browse(cr, uid, ids)
        for equip in equips:
            if equip.serial_number:
                res[equip.id] = equip.serial_number.product_id.id

        return res

    def _get_installacio(self, cr, uid, ids, field_name, arg, context=None):
        """ Agafa la instal·lació a partir del PLC """
        res = dict([(i, False) for i in ids])

        acobladors = self.browse(cr, uid, ids)
        for acb in acobladors:
            if acb.plc_id:
                res[acb.id] = acb.plc_id.installacio_id.id

        return res

    def _search_installacio(self, cr, uid, obj, field_name, args,
                            context=None):
        """ Cerca la instal·lació a partir del nom"""
        if not context:
            context = {}
        if not len(args):
            return []

        inst_obj = self.pool.get('giscedata.telecom.installacio')
        plc_obj = self.pool.get('giscedata.telecom.equip.plc')

        search = [('name', a[1], a[2]) for a in args]

        inst_ids = inst_obj.search(cr, uid, search)
        plc_ids = plc_obj.search(cr, uid, [('installacio_id', 'in', inst_ids)])
        return [('plc_id', 'in', plc_ids)]

    def _search_product(self, cr, uid, obj, field_name, args, context=None):
        """ Cerca el producte a partir del nom"""
        if not context:
            context = {}
        if not len(args):
            return []

        prod_obj = self.pool.get('product.product')
        sn_obj = self.pool.get('stock.production.lot')

        search = [('name', a[1], a[2]) for a in args]

        prod_ids = prod_obj.search(cr, uid, search)
        sn_ids = sn_obj.search(cr, uid, [('product_id', 'in', prod_ids)])
        return [('serial_number', 'in', sn_ids)]

    _columns = {
        'name': fields.char('Descripció', size=64, required=True),
        'serial_number': fields.many2one('stock.production.lot',
                                         'Número de sèrie', required=True),
        'product_id': fields.function(_get_product, string='Model',
                                      fnct_search=_search_product,
                                      type='many2one', obj='product.product',
                                      readonly=True, method=True),
        'plc_id': fields.many2one('giscedata.telecom.equip.plc', 'Equip PLC',
                                  required=True),
        'installacio_id': fields.function(_get_installacio, method=True,
                                          fnct_search=_search_installacio,
                                          obj='giscedata.telecom.installacio',
                                          string="Instal·lació",
                                          type='many2one', readonly=True),
        'data_installacio': fields.date('Data instal.lació', required=True),
        'active': fields.boolean('Actiu'),
    }

GiscedataTelecomEquipPLCAcoblador()
