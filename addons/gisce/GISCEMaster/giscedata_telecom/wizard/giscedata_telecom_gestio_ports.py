# -*- coding: utf-8 -*-
"""
giscedata_telecom_gestio_ports.py

Wizard per crear tots els ports d'un equip.

"""
import wizard
import pooler

_init_form = """<?xml version="1.0"?>
<form string="Gestió de ports">
    <field name="equip_id" colspan="4"/>
    <label string="velocitat per defecte en tots els ports nous [kbps]"
           colspan="4"/>
    <field name="velocitat" colspan="4"/>
    <field name="connector" colspan="4"/>
    <label string="Esculli el número de ports de l'equip" colspan="4"/>
    <field name="n_ports"/>
</form>
"""

_init_fields = {
    'equip_id': {'type': 'many2one', 'string': 'Equip', 'readonly': True,
                 'relation': 'giscedata.telecom.equip.base'},
    'connector': {'type': 'many2one', 'string': 'Connector',
                  'relation': 'giscedata.telecom.port.connector'},
    'velocitat': {'type': 'float', 'string': 'Velocitat', 'required': True},
    'n_ports': {'type': 'integer', 'string': 'Num. ports', 'required': True},
}

_creat_form = """<?xml version="1.0"?>
<form string="Gestió de ports">
    <field name="new_ports" string = "Núm. Ports creats" readonly="True"/>
</form>
"""

_creat_fields = {
    'new_ports': {'type': 'integer', 'string': 'Num. ports creates',
                  'required': True},
}


def _init(self, cr, uid, data, context=None):
    model = data.get('model', False)
    obj_id = data.get('id', False)
    if not model or not obj_id:
        return {}

    equip_obj = pooler.get_pool(cr.dbname).get(model)
    equip = equip_obj.browse(cr, uid, data['id'])

    n_ports = len(equip.ports)
    velocitat = (equip.ports and max([port.velocitat for port in equip.ports])
                 or 100000)
    equip_id = equip.equip_id.id

    return {'n_ports': n_ports, 'equip_id': equip_id, 'velocitat': 1000000}


def _crea_ports(self, cr, uid, data, context=None):
    model = data.get('model', False)
    form = data.get('form', {})
    equip_id = form.get('equip_id', False)
    velocitat = form.get('velocitat', 1)
    connector = form.get('connector', False)
    ports_afegits = []
    # equip id ja és del model equip.base
    if equip_id:
        ctx = {'model': 'giscedata.telecom.equip.base', 'equip_id': equip_id}
        pool = pooler.get_pool(cr.dbname)
        equip_obj = pool.get('giscedata.telecom.equip.base')
        port_obj = pool.get('giscedata.telecom.port')
        equip = equip_obj.browse(cr, uid, equip_id)
        n_ports_actuals = len(equip.ports)
        n_ports_final = form.get('n_ports', n_ports_actuals)

        if n_ports_actuals < n_ports_final:
            for i in xrange(n_ports_actuals, n_ports_final):
                params = {'velocitat': velocitat, 'equip_id': equip_id}
                if connector:
                    params['connector'] = connector

                port_id = port_obj.create(cr, uid, params, context=ctx)
                if port_id:
                    ports_afegits.append(port_id)

    return {'new_ports': len(ports_afegits)}


class WizardTelecomGestioPorts(wizard.interface):
    _name = "wizard.telecom.gestio.ports"
    _description = "Wizard per crear tots els ports d'un equip"

    states = {
        'init': {
            'actions': [_init],
            'result': {
                'type': 'form',
                'arch': _init_form,
                'fields': _init_fields,
                'state': [('end', 'Cancelar', 'gtk-cancel'),
                          ('crea_ports', 'continuar', 'gtk-go-forward')]
            }
        },
        'crea_ports': {
            'actions': [_crea_ports],
            'result': {
                'type': 'form',
                'arch': _creat_form,
                'fields': _creat_fields,
                'state': [('end', 'OK', 'gtk-cancel')]
            }
        },
        'end': {
            'actions': [],
            'result': {'type': 'state', 'state': 'end'},
        }
    }

WizardTelecomGestioPorts('wizard.telecom.gestio.ports')
