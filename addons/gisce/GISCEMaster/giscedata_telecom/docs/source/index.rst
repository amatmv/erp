.. Lectures i comptadors switching documentation master file, created by
   sphinx-quickstart on Wed Apr  3 14:21:33 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentació del mòdul gestió d'equips de telecomunicacions
***********************************************************

Equips de telecomunicacions
===========================

El mòdul de gestió d'equips de telecomunicacions permet inventariar els equips
de telecomunicacion de l'empresa i la seva rel·lació amb les diferents
instal·lacions.

Permetrar fer un seguiment de fabricant, producte i número de sèrie de cada
equip. Per això s'utilitzaran les eines pròpies de **Openerp** per a la compra
i recepció de productes utilitzant albarans i lots de producció.

Podem inventariar Sistemes auxiliars, Routers i switchos, sistemes WIFI,
operadors de telecomunicacions, sistemes de fibra óptica i equips i elements
PLC.

Cadascún d'aquests equips poden tenir ports de comunicacions associats a un
servei de comunicacions.

Qualsevol equip es pot associar amb una **instal·lació** de tipus centre
transformador o subestació. D'aquesta forma podem relacionar la xarxa elèctrica
amb la xarxa de telecomunicacions

Tota la gestió dels equips de telecomunicacions es fa des del menú específic de
Telecomunicacions (:ref:`menutelecom`).

.. _menutelecom:

.. figure:: /_static/MenuTelecomunicacions.png

   Menu de telecomunicacions


Productes i números de sèrie
============================

Pel funcionament del mòdul, cal gestionar els productes, les categories de
productes i les números de sèrie correctament.

El funcionament és idèntic al que s'explica al document *Compra i magatzem de
comptadors* amb algunes particularitats.

Categories de producte
----------------------

Per facilitar la gestió dels productes dins el mòdul de telecomunicacions, s'han
creat categories de productes (veure :ref:`categoriesproducte`) directament relacionades
amb els diferents equips i elements que es gestionen en el mòdul de
telecomunicacions. Així, p.e. s'ha creat la categoria de producte ``Routers`` on
s'inclouran tots els equips routers. Les fitxes del mòdul de telecomunicacions
només permetran seleccionar els productes de la categoria corresponent per
facilitar la tasca de l'usuari. Podem accedir als productes pe categoria
mitjançant el menú ``Productes > Productes per categoria``

.. _categoriesproducte:

.. figure:: /_static/ArbreCategoriesProducte.png

   Categories de productes de telecomunicacions

Producte
--------

Els productes es poden crear des del llistat de categories de producte de forma
que ja s'associaran a la categoria corresponent al obrir la fitxa. També es pot
canviar manualment tal i com es veu en la captura :ref:`fitxaproductecategoria`

.. _fitxaproductecategoria:

.. figure:: /_static/CategoriaFitxaProducte.png

   Categoria a la fitxa de producte

Instal·lacions
==============

Qualsevol equip o elements es pot associar a una instal·lació de la empresa.
Aquesta instal·lació pot ser tan un **Centre Transformador** com una
**Subestació**

.. _fitxainstallacio:

.. figure:: /_static/Installacions.png

   Fitxa d'instal·lació de telecomunicacions

Com es pot veure a :ref:`fitxainstallacio` , les dades que es poden introduïr en una
instal·lació són:

 * *Nom*: Nom descriptiu de la instal·lació de telecomunicacions
 * *Codi*: Codi de referència de la instal·lació de telecomunicacions
 * *Tipus*: Tipus de instal·lació
 * *Node*: Funcionalitat de la instal·lació de telecomunicacions com a node
   dins la xarxa
 * *Equipament on està ubicada*: Equipament de la xarxa de distribució
   elèctrica on està ubicada la instal·lació de telecomunicacions. Pot ser un
   **Centre Transformador** o una **Subestació**

Des de la fitxa de la instal·lació es poden llistar tots els tipus d'elements i
equips que formen la instal·lació, permentent obrir la fitxa corresponent.

Equips de telecomunicacions
===========================
S'han organitzat tots els equips i elements de sistemes de telecomunicacions en
diferents models de forma que es puguin gestionar les dades específiques de cada
tipus de d'element. No obstant, algunes característiques són comunes a totes
els equips. Concretament:

* 

Sistemes Auxiliars
==================
Els sistemes auxiliars inclouen els sistemes d'alimentació i bateries