# -*- coding: utf-8 -*-
{
    "name": "GISCE Telecom",
    "description": """Mòdul per gestionar actius de telecomunicacions""",
    "version": "0-dev",
    "author": "GISCE TI",
    "category": "",
    "depends":[
        "base",
        "base_extended",
        "giscedata_cts",
        "product",
        "stock"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_telecom_data.xml",
        "giscedata_telecom_view_base.xml",
        "wizard/giscedata_telecom_gestio_ports.xml",
        "giscedata_telecom_view.xml",
        "security/giscedata_telecom_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
