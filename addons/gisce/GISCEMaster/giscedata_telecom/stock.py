# -*- coding: utf-8 -*-
"""
mòdul telecom: stock.py

Afegim la cetgoria a stock_production_lot per poder crear nums de sèrie filtrant
el producte segons categoria

"""

from osv import fields, osv


class stock_production_lot(osv.osv):
    """ Afegim product_id a stock_production_lot"""
    _name = "stock.production.lot"
    _inherit = "stock.production.lot" 

    def _get_category_default(self, cr, uid, ids, context=None):
        """ Agafem la categoria del context o telecom""" 
        if not context:
            context = {}
       
        res = dict([(i, False) for i in ids])

        return res

    _columns = {
        'category_id': fields.many2one('product.category', 'Category',
                                      required=False),
    }
    _defaults = {
        'category_id': _get_category_default,
    }

stock_production_lot()
