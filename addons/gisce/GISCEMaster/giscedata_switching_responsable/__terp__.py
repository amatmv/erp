# -*- coding: utf-8 -*-
{
    "name": "Switching responsible",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Assignar el responsable del cas segons el responsable de la pòlissa
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_switching"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
