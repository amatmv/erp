# -*- coding: utf-8 -*-
from osv import osv


class GiscedataSwitching(osv.osv):
    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def update_from_xml(self, cursor, uid, sw_id, filename, partner_id,
                        cups_id, xml, context=None):
        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')

        sw_id = super(GiscedataSwitching, self).update_from_xml(
            cursor, uid, sw_id, filename, partner_id, cups_id, xml, context=context
        )
        sw = self.browse(cursor, uid, sw_id)
        if len(sw.stage_ids) == 1:
            # S'acaba de crear el cas
            if 'user_id' in polissa_obj.fields_get():
                user = sw.cups_polissa_id.user_id
                if user:
                    sw.write({'user_id': user.id})
        return sw_id

GiscedataSwitching()