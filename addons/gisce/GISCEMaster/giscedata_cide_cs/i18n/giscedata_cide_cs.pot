# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_cide_cs
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2015-01-30 11:56:53+0000\n"
"PO-Revision-Date: 2015-01-30 11:56:53+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_cide_cs
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_cide_cs
#: code:addons/giscedata_cide_cs/wizard/exportar_curva_cs_cide_wizard.py:113
#, python-format
msgid "Exportades %d línies de %d comptadors desde %s (inclosa) fins a %s (no inclosa)"
msgstr ""

#. module: giscedata_cide_cs
#: code:addons/giscedata_cide_cs/wizard/exportar_curva_cs_cide_wizard.py:64
#, python-format
msgid "Has de seleccionar la data de inici i la data fi"
msgstr ""

#. module: giscedata_cide_cs
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_cide_cs
#: model:ir.actions.act_window,name:giscedata_cide_cs.action_exportar_tg_curves_cs_cide
msgid "Exportar Curves TG CS CIDE"
msgstr ""

#. module: giscedata_cide_cs
#: view:giscedata.curva.cs.cide.wizard:0
msgid "Tancar"
msgstr ""

#. module: giscedata_cide_cs
#: field:giscedata.curva.cs.cide.wizard,name:0
msgid "Nom"
msgstr ""

#. module: giscedata_cide_cs
#: model:ir.module.module,shortdesc:giscedata_cide_cs.module_meta_information
msgid "GISCE CIDE CS"
msgstr ""

#. module: giscedata_cide_cs
#: code:addons/giscedata_cide_cs/wizard/exportar_curva_cs_cide_wizard.py:42
#, python-format
msgid "\n"
" S'exportaran totes les curves dels comptadors de Telegestió: %s comptadors"
msgstr ""

#. module: giscedata_cide_cs
#: code:addons/giscedata_cide_cs/wizard/exportar_curva_cs_cide_wizard.py:76
#, python-format
msgid "No s'ha trobat el comptador"
msgstr ""

#. module: giscedata_cide_cs
#: code:addons/giscedata_cide_cs/giscedata_lectures.py:38
#, python-format
msgid "%s: %04d/%04d"
msgstr ""

#. module: giscedata_cide_cs
#: field:giscedata.curva.cs.cide.wizard,file:0
msgid "Fitxer"
msgstr ""

#. module: giscedata_cide_cs
#: field:giscedata.curva.cs.cide.wizard,info:0
msgid "Info"
msgstr ""

#. module: giscedata_cide_cs
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_cide_cs
#: field:giscedata.curva.cs.cide.wizard,data_inici:0
msgid "Data inici"
msgstr ""

#. module: giscedata_cide_cs
#: code:addons/giscedata_cide_cs/wizard/exportar_curva_cs_cide_wizard.py:37
#, python-format
msgid "Exportació de curves per CS de CIDE des de perfils de Telegestió."
msgstr ""

#. module: giscedata_cide_cs
#: code:addons/giscedata_cide_cs/wizard/exportar_curva_cs_cide_wizard.py:119
#, python-format
msgid "Exportades %d línies del comptador %s desde %s (inclosa) fins a %s (no inclosa)"
msgstr ""

#. module: giscedata_cide_cs
#: code:addons/giscedata_cide_cs/wizard/exportar_curva_cs_cide_wizard.py:75
#, python-format
msgid "Comptador no trobat"
msgstr ""

#. module: giscedata_cide_cs
#: model:ir.ui.menu,name:giscedata_cide_cs.menu_exportar_tg_curves_cs_cide
msgid "Exportar Curves telegestió CS CIDE"
msgstr ""

#. module: giscedata_cide_cs
#: model:ir.module.module,description:giscedata_cide_cs.module_meta_information
msgid " Ficheros de intercambio con Concentrador Secundario    de CIDE:                                               * Exportación de curvas de contadores de telegestión  "
msgstr ""

#. module: giscedata_cide_cs
#: code:addons/giscedata_cide_cs/wizard/exportar_curva_cs_cide_wizard.py:49
#, python-format
msgid "\n"
" S'exportaran %d comptadors: '%s' a '%s'"
msgstr ""

#. module: giscedata_cide_cs
#: field:giscedata.curva.cs.cide.wizard,state:0
msgid "State"
msgstr ""

#. module: giscedata_cide_cs
#: code:addons/giscedata_cide_cs/wizard/exportar_curva_cs_cide_wizard.py:63
#, python-format
msgid "Dates invàlides"
msgstr ""

#. module: giscedata_cide_cs
#: view:giscedata.curva.cs.cide.wizard:0
#: model:ir.actions.act_window,name:giscedata_cide_cs.action_exportar_curva_cs_cide
msgid "Exportar Curva CS CIDE"
msgstr ""

#. module: giscedata_cide_cs
#: field:giscedata.curva.cs.cide.wizard,data_fi:0
msgid "Data Fi"
msgstr ""

#. module: giscedata_cide_cs
#: model:ir.model,name:giscedata_cide_cs.model_giscedata_curva_cs_cide_wizard
msgid "giscedata.curva.cs.cide.wizard"
msgstr ""

