# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import csv
import StringIO
import base64
import zipfile

class GiscedataCurvaCsCIDEWizard(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscedata.curva.cs.cide.wizard'

    def get_all_tg_meters(self, cursor, uid):
        '''Returns all active TG meters '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_ids = meter_obj.search(cursor, uid, [('tg', '!=', False)])

        return meter_ids

    def _default_data_inici(self, cursor, uid, context=None):
        if not context:
            context = {}

        mes_anterior = datetime.today() + relativedelta(months=-1)
        return mes_anterior.strftime("%Y-%m-01")

    def _default_info(self, cursor, uid, context=None):
        if not context:
            context = {}

        meter_obj = self.pool.get('giscedata.lectures.comptador')

        txt = _(u'Exportació de curves per CS de CIDE des de perfils de '
                u'Telegestió.')
        meter_ids = context.get('active_ids', [])
        if context.get('get_all_meters', False):
            meter_ids = self.get_all_tg_meters(cursor, uid)
            txt += _(u"\n S'exportaran totes les curves dels comptadors de "
                     u"Telegestió: %s comptadors") % len(meter_ids)
        elif len(meter_ids) > 1:
            # Tenim més d'un comptador
            tgname1 = meter_obj.build_name_tg(cursor, uid, meter_ids[0])
            tgname2 = meter_obj.build_name_tg(cursor, uid, meter_ids[-1])
            txt += (_(u"\n S'exportaran %d comptadors: "
                      u"'%s' a '%s'") % (len(meter_ids), tgname1, tgname2))

        return txt

    def action_exportar_curva_cs_cide(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_ids = context.get('active_ids', False)
        all_meters = context.get('get_all_meters', False)
        multi = all_meters or (len(meter_ids) > 1)


        wiz = self.browse(cursor, uid, ids[0])

        if not (wiz.data_inici and wiz.data_fi):
            raise osv.except_osv(_(u'Dates invàlides'),
                                 _(u'Has de seleccionar la data de inici i la '
                                   u'data fi'))

        data_inici = datetime.strptime(wiz.data_inici, '%Y-%m-%d')
        data_fi = datetime.strptime(wiz.data_fi, '%Y-%m-%d')
        if all_meters:
            meter_ids = self.get_all_tg_meters(cursor, uid)
            tgname = 'all'
        else:
            if not meter_ids[0]:
                raise osv.except_osv(_(u'Comptador no trobat'),
                                     _(u"No s'ha trobat el comptador"))
            tgname = meter_obj.build_name_tg(cursor, uid, meter_ids[0])
            if multi:
                tgname += '-%s' % meter_obj.build_name_tg(cursor, uid,
                                                          meter_ids[-1])

        curves = meter_obj.get_curva_tg_cide(cursor, uid, meter_ids,
                                             data_inici.strftime("%Y-%m-%d"),
                                             data_fi.strftime("%Y-%m-%d"),
                                             context=context)

        output = StringIO.StringIO()
        zip_io = StringIO.StringIO()
        extension = 'csv'
        filename_tmpl = "curva_cs_cide_%s.%s"
        writer = csv.writer(output, delimiter='\t')

        for curva in curves.values():
            for line in curva:
                writer.writerow(line)

        if multi:
            extension = 'zip'
            zip_fp = zipfile.ZipFile(zip_io, 'w',
                                     compression=zipfile.ZIP_DEFLATED)
            zip_filename = filename_tmpl % (tgname, 'csv')
            zip_fp.writestr(zip_filename, output.getvalue())
            output.close()
            zip_fp.close()
            mfile = base64.b64encode(zip_io.getvalue())
        else:
            mfile = base64.b64encode(output.getvalue())
            output.close()

        filename = filename_tmpl % (tgname, extension)

        if multi:
            info_tmpl = _(u"Exportades %d línies de %d comptadors desde "
                          u"%s (inclosa) fins a %s (no inclosa)")
            num_linies = sum([len(c) for c in curves.values()])
            info = info_tmpl % (num_linies, len(meter_ids), data_inici,
                                data_fi)
        else:
            info_tmpl = _(u"Exportades %d línies del comptador %s desde "
                          u"%s (inclosa) fins a %s (no inclosa)")
            info = info_tmpl % (len(curves.values()[0]), tgname, data_inici,
                                data_fi)

        wiz.write({'file': mfile, 'name': filename, 'state': 'done',
                   'info': info}, context=context)

    _columns = {
        'state': fields.char('State', size=16),
        'data_inici': fields.date('Data inici'),
        'data_fi': fields.date('Data Fi'),
        'name': fields.char('Nom', size=256),
        'file': fields.binary('Fitxer'),
        'info': fields.text('Info')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        #'data_inici': _default_data_inici,
    }

GiscedataCurvaCsCIDEWizard()