# -*- coding: utf-8 -*-
from osv import osv, fields
import netsvc
from datetime import datetime
from tools.translate import _

from addons.giscedata_lectures_telegestio.lectures import fill_measure

class GiscedataLecturesComptador(osv.osv):
    """Comptador de distribuidrora.
    """

    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def get_curva_tg_cide(self, cursor, uid, ids, data_inici, data_fi,
                          context=None):
        """ Retorna la curva del comptador(s) des de telegestió en format
            CIDE CS"""

        logger = netsvc.Logger()

        if not context:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        res = dict([(i, []) for i in ids])

        tp_obj = self.pool.get('tg.profile')
        polissa_obj = self.pool.get('giscedata.polissa')

        conta_fields = ['name', 'data_alta', 'data_baixa', 'active']
        counter = 0
        for conta_vals in self.read(cursor, uid, ids, conta_fields):
            counter += 1
            meter_id = conta_vals['id']
            tg_name = self.build_name_tg(cursor, uid, meter_id)
            logger.notifyChannel('exporta_curves_tg', netsvc.LOG_DEBUG,
                                 _(u'%s: %04d/%04d') % (tg_name, len(ids),
                                                        counter))
            start = max(data_inici, conta_vals['data_alta'])
            search_vals = [('name', '=', tg_name),
                           ('timestamp', '>=', start),
                           ('timestamp', '<', data_fi),
                           ('cch_bruta', '=', True)
                           ]

            if conta_vals['data_baixa'] and not conta_vals['active']:
                search_vals.append(('timestamp', '<=',
                                    conta_vals['data_baixa']))

            #searching for cups
            if meter_id:
                polissa_id = self.read(cursor, uid, meter_id,
                                       ['polissa'])['polissa'][0]
                cups_vals = polissa_obj.read(cursor, uid, polissa_id,
                                             ['cups'])
                cups_name = cups_vals['cups'][1]
            else:
                cups_name = 'n/a'

            tp_ids = tp_obj.search(cursor, uid, search_vals, 0, 0,
                                   'timestamp asc')

            for tp_id in tp_ids:
                tp_vals = tp_obj.read(cursor, uid, tp_id, [])
                season = tp_vals['season'] == 'W' and 'I' or 'V'
                timestamp = datetime.strptime(tp_vals['timestamp'],
                                              '%Y-%m-%d %H:%M:%S')
                profile_datetime = timestamp.strftime('%d/%m/%Y %H:%M')

                line = [cups_name[:20],
                        '1',
                        profile_datetime,
                        season,
                        int(tp_vals['ai']),
                        int(tp_vals['bc'], 16),
                        int(tp_vals['ae']),
                        int(tp_vals['bc'], 16),
                        int(tp_vals['r1']),
                        int(tp_vals['bc'], 16),
                        int(tp_vals['r2']),
                        int(tp_vals['bc'], 16),
                        int(tp_vals['r3']),
                        int(tp_vals['bc'], 16),
                        int(tp_vals['r3']),
                        int(tp_vals['bc'], 16),
                        ]

                res[meter_id].append(line)

        return res

    def get_curve_type_dict(self, cursor, uid, context):
        res = super(GiscedataLecturesComptador, self).get_curve_type_dict(
            cursor, uid, context
        )
        res.update({
            'cch_cide_chf': ('fact', 'CIDE_CHF', _(u'CCH_FACT_CIDE (CHF)'))
        })
        return res

    def cch_format_CIDE_CHF(self, cursor, uid, tp_val, cups_name,
                            context=None):
        """"Returs a TgProfile record list as a CIDE CHF format list"""

        if not context:
            context = {}

        season = tp_val['season'] == 'W' and 'I' or 'V'
        timestamp = datetime.strptime(tp_val['timestamp'],
                                      '%Y-%m-%d %H:%M:%S')
        magnitude = tp_val['magn']

        profile_datetime = timestamp.strftime('%d/%m/%Y %H:%M')
        fact_number = context.get('fact_number', 'F0000000000')
        kind = int(tp_val.get('kind_fact', 6))
        # distinct kind_facts in whole curve
        kind_facts = context.get('kind_facts', [])

        sureness = (kind == 1 and 1 or 0)
        if kind == 3 and len(kind_facts) == 1 and kind_facts[0] == 3:
            sureness = 1

        line = [cups_name[:20],
                profile_datetime,
                season,
                #int(tp_val['ai_fact'] * magnitude),
                int(fill_measure('ai', tp_val) * magnitude),
                int(fill_measure('ae', tp_val) * magnitude),
                int(fill_measure('r1', tp_val) * magnitude),
                int(fill_measure('r2', tp_val) * magnitude),
                int(fill_measure('r3', tp_val) * magnitude),
                int(fill_measure('r4', tp_val) * magnitude),
                '{0:02d}'.format(kind),
                sureness,
                fact_number,
                '',
                '',
                ]

        return line

GiscedataLecturesComptador()
