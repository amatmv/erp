# -*- coding: utf-8 -*-
{
    "name": "GISCE CIDE CS",
    "description": """ Ficheros de intercambio con Concentrador Secundario    de CIDE:                                               * Exportación de curvas de contadores de telegestión  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_lectures_telegestio",
        "giscedata_facturacio_distri_tg"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/exportar_curva_cs_cide_wizard.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
