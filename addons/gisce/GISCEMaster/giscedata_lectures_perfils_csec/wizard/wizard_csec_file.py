# -*- coding: utf-8 -*-
import subprocess
import tempfile
import base64
import os

from osv import osv, fields
import tools

class WizardCSECFile(osv.osv_memory):
    """Wizard per exportar el fitxer de Sítel.
    """
    _name = 'wizard.csec.file'

    def onchange_periode(self, cursor, uid, ids, context=None):
        return {'value': {'state': 'init', 'file': False}}

    def action_exec_script(self, cursor, uid, ids, context=None):
        """Llença l'script per l'informe.

            Options:
      --version             show program's version number and exit
      -h, --help            show this help message and exit
      -o FOUT, --output=FOUT
                            Fitxer de sortida
      -e PERIODE, --periode=PERIODE
                            El periode de liquidació/comptable (id) pel qual es
                            farà el resum
      Server options:
        -s SERVER, --server=SERVER
                            Adreça servidor ERP
        -p PORT, --port=PORT
                            Port servidor ERP
        -u USER, --user=USER
                            Usuari servidor ERP
        -w PASSWORD, --password=PASSWORD
                            Contrasenya usuari ERP
        -d DATABASE, --database=DATABASE
                            Nom de la base de dades
        """
        tmp = tempfile.mkstemp()[1]
        wizard = self.browse(cursor, uid, ids[0], context)
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        port = tools.config['port'] or 8069
        csec_perf_obj = self.pool.get('giscedata.perfils.csec')
        csec_perf_obj.generate(
            cursor, uid, periode=wizard.periode, file=tmp, context=context
        )
        report = base64.b64encode(open(tmp).read())
        os.unlink(tmp)
        year, month = wizard.periode.split('/')
        filename = 'perf_csec_%s%s.csv' % (year, month)
        wizard.write({'name': filename, 'file': report, 'state': 'done'})

    _columns = {
        'name': fields.char('File name', size=64),
        'periode': fields.char('Periode (MM/YYYY)', size=7, required=True),
        'file': fields.binary('Fitxer'),
        'state': fields.char('State', size=16)
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardCSECFile()
