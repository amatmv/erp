#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Generador de CSV resums de facturació i lectures
================================================

Genera fitxers CSV amb informació de facturació o lectures segons
s'especifiqui.

Rus as: ./resum_distribuidora.py

"""
import csv
import re
import sys
import calendar
from osv import osv

__version__ = '0.1'

PAT = re.compile('^.*\((?P<nom>.*)\)$')

RECTIFYING_RECTIFICATIVE_INVOICE_TYPES = ['R', 'RA']

class GiscedataPerfilsCSEC(osv.osv):
    _name = 'giscedata.perfils.csec'

    def calculate(self, cursor, uid, factures_ids, context=None):
        """Calculate csec profiles.
        """
        lines = []
        refs = {}
        sys.stderr.write('^Generating CSEC Profiles...\n')
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        energy_reads_obj = self.pool.get('giscedata.facturacio.lectures.energia')
        reads_metter_obj = self.pool.get('giscedata.lectures.comptador')
        partner_obj = self.pool.get('res.partner')

        for factura in invoice_obj.read(cursor, uid, factures_ids,
                          ['cups_id', 'tarifa_acces_id', 'data_inici', 'data_final',
                           'polissa_id', 'potencia', 'lectures_energia_ids',
                           'tipo_rectificadora', 'partner_id', 'type']):

            #Llegir modcontract amb polissa_id, dates i Active test = false
            comer_id = modcon_obj.search(cursor, uid,
                [('polissa_id', '=', factura['polissa_id'][0]),
                 ('data_inici', '<=', factura['data_final']),
                 ('data_final', '>=', factura['data_inici'])],
                0, 0, False, {'active_test': False}
            )

            comer = modcon_obj.read(cursor, uid,
                comer_id, ['comercialitzadora'])

            partner_id = comer[0]['comercialitzadora'][0]
            try:
                if partner_id not in refs:
                    refs[partner_id] = partner_obj.read(cursor, uid, partner_id, ['ref'])['ref']
            except Exception:
                refs[partner_id] = '000'
            row = []
            row.append(factura['cups_id'][1])  # CUPS
            row.append(factura['tarifa_acces_id'][1])  # TARIFA
            row.append(factura['data_inici'])  # DATA INICI
            row.append(factura['data_final'])  # DATA FINAL
            lectures = {
                'activa': {},
                'reactiva': {},
            }
            for lectura in energy_reads_obj.read(cursor, uid,
                                factura['lectures_energia_ids'],
                                ['comptador_id', 'lect_actual', 'lect_anterior',
                                 'consum', 'name', 'tipus']):
                consum = lectura['lect_actual'] - lectura['lect_anterior']
                if consum < 0:
                    giro = reads_metter_obj.read(cursor, uid,
                        lectura['comptador_id'][0], ['giro']
                    )['giro']
                    consum += giro
                _match = re.match(PAT, lectura['name'])
                if _match:
                    _name = _match.group('nom')
                else:
                    _name = lectura['name'] # per tarifes 2.X
                if 'invoice' in factura['type']:
                    sign = 1
                elif 'refund' in factura['type']:
                    sign = -1
                lectures[lectura['tipus']][_name] = consum * sign
            for tipus in ('activa', 'reactiva'):
                if tipus in lectures:
                    for periode in ('P1', 'P2', 'P3', 'P4', 'P5', 'P6'):
                        row.append(lectures[tipus].get(periode, 0))
                else:
                    row.extend([0]*6)

            row.append(refs[partner_id])  # COMERCIALITZADORA
            row.append(factura['tarifa_acces_id'][1])  # TARIFA
            row.append('%.3f' % round(factura['potencia'], 3))  # POTENCIA
            row.append(factura['polissa_id'][1])  # N. CLIENT
            lines.append(row)

        return lines

    def generate(self, cursor, uid, periode, file, context=None):
        """Generate profiles read csec.
        """
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        fout = open(file, 'wb')
        month, year = map(int, periode.split('/'))
        data_final_0 = '%04i-%02i-01' % (year, month)
        data_final_1 = '%04i-%02i-%02i' % (year, month,
                                     calendar.monthrange(year, month)[1])

        cancelling_invoice_ids = invoice_obj.search(cursor, uid,
            [('tipo_rectificadora', 'in', ('R', 'A', 'B', 'RA', 'BRA'))]
        )
        cancelling_invoices = invoice_obj.read(cursor, uid,
            cancelling_invoice_ids,
            ['ref']
        )
        cancelled_invoice_ids = [ x['ref'][0] for x in cancelling_invoices ]

        search_params = [
            ('data_final', '>=', data_final_0),
            ('data_final', '<=', data_final_1),
            (
                'tipo_rectificadora', 'in',
                RECTIFYING_RECTIFICATIVE_INVOICE_TYPES + ['N']
            ),
            ('id', 'not in', cancelled_invoice_ids)
        ]
        invoices_ids = invoice_obj.search(cursor, uid, search_params)
        linia_perfil = self.calculate(cursor, uid, invoices_ids, context)

        fitxer = csv.writer(fout, delimiter=';')
        for row in linia_perfil:
            fitxer.writerow(row)
        fout.close()
        sys.stderr.write('Resum fet!\n')
        sys.stderr.flush()

GiscedataPerfilsCSEC()