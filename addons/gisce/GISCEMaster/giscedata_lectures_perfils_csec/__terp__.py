# -*- coding: utf-8 -*-
{
    "name": "Fitxer perfils CSEC",
    "description": """
    This module provide :
      * Wizard per obtenir el fitxer de perfils pel CSEC.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_csec_file_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
