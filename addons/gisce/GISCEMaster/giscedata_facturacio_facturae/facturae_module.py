# -*- encoding: utf-8 -*-

from osv import osv
from facturae import facturae


class AccountInvoiceFacturae(osv.osv):

    _name = 'account.invoice.facturae'
    _inherit = 'account.invoice.facturae'

    def facturae_documents(self, cursor, uid, invoice, attachment=None,
                           context=None):
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        documents = super(AccountInvoiceFacturae, self).facturae_documents(
            cursor, uid, invoice, attachment, context=context)

        fact_ids = fact_obj.search(
            cursor, uid, [('invoice_id', '=', invoice.id)]
        )

        if fact_ids:
            fact_id = fact_ids[0]

            attachments_facturacio_data = self.get_facturae_attachments_data(
                cursor, uid, fact_id, 'giscedata.facturacio.factura',
                context=context
            )
            new_attachments = self.facturae_attachments(
                cursor, uid, attachments_facturacio_data, context=context
            )

            if new_attachments:
                if documents is None:
                    documents = facturae.RelatedDocuments()
                    documents.feed({'attachment': new_attachments})
                else:
                    documents.attachment = (
                        documents.attachment + new_attachments
                    )

        return documents

    def facturae_issue_data(self, cursor, uid, invoice, context=None):
        issuedata = super(AccountInvoiceFacturae, self).facturae_issue_data(
            cursor, uid, invoice, context=context)

        conf_obj = self.pool.get('res.config')
        add_invoicing_period = bool(int(conf_obj.get(
            cursor, uid, 'facturae_invoicing_period', False
        )))
        if add_invoicing_period:  # Esquema 3.1.2.4.
            fact_obj = self.pool.get('giscedata.facturacio.factura')
            fact_ids = fact_obj.search(
                cursor, uid, [('invoice_id', '=', invoice.id)])
            if fact_ids:
                factura = fact_obj.read(
                    cursor, uid, fact_ids[0], ['data_inici', 'data_final'])
                invoicing_period = facturae.InvoicingPeriod()
                invoicing_period.feed({
                    'startdate': factura['data_inici'],
                    'enddate': factura['data_final'],
                })

                issuedata.feed({
                    'invoicingperiod': invoicing_period,
                })

        return issuedata


AccountInvoiceFacturae()
