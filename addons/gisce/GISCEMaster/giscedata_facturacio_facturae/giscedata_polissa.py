# -*- encoding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'facturae_filereference': fields.char(
            'Facturae file reference',
            help='File Reference for indicate into XML',
            size=20,
            readonly=True,
            states={
                'modcontractual': [('readonly', False)],
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)]
            }
        )
    }

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'facturae_filereference': fields.char(
            'Facturae file reference',
            help='File Reference for indicate into XML',
            size=20
        )
    }

GiscedataPolissaModcontractual()