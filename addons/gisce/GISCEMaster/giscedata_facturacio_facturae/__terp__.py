# -*- coding: utf-8 -*-
{
    "name": "Facturació Facturae ",
    "description": """Allow exporting invoices to facturae format""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_facturacio",
        "facturae_module",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/wizard_export_facturae_view.xml",
        "giscedata_facturacio_view.xml",
        "giscedata_facturacio_data.xml",
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
