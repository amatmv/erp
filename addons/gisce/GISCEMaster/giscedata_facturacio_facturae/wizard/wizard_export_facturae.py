# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
import base64
import os
import tempfile
from tools.translate import _
from face.face import FACe
from tools import config


def zipdir(basedir):
    from contextlib import closing
    from zipfile import ZipFile, ZIP_DEFLATED

    assert os.path.isdir(basedir)

    compressed_file = None
    with tempfile.SpooledTemporaryFile() as f:
        with closing(ZipFile(f, "w", ZIP_DEFLATED)) as z:
            for root, dirs, files in os.walk(basedir):
                for fn in files:
                    absfn = os.path.join(root, fn)
                    zfn = absfn[len(basedir)+len(os.sep):]
                    z.write(absfn, zfn)
        f.seek(0)
        compressed_file = f.read()
    return compressed_file


class WizardExportFacturae(osv.osv_memory):

    _name = 'wizard.export.facturae'

    _default_face_environment = 'prod'

    def _get_default_wizard_info(self, cursor, uid, context=None):
        face_server = {'prod': _('Real'), 'staging': _('Test')}

        s = _(
            'This wizard allows to export facturae file and store as '
            'attachment of the invoice or send the invoice to FACe server.'
            '\n\nCurrent FACe server: "{}"'
        ).format(
            face_server[
                config.get('face_environment', self._default_face_environment)
            ]
        )

        return s

    def get_report_name(self, cursor, uid, model, context=None):

        if model == 'giscedata.facturacio.factura':
            report_name = 'giscedata.facturacio.factura'
        else:
            report_name = 'account.invoice'

        return report_name

    def generate_facturae(self, cursor, uid, model, model_id,
                          attach_original=False, context=None):
        if context is None:
            context = {}

        info = None
        attachment = None
        model_obj = self.pool.get(model)
        object = model_obj.browse(cursor, uid, model_id)

        if model == 'giscedata.facturacio.factura':
            invoice_id = object.invoice_id.id
            info = 'Contrato: %s - CUPS: %s - Dir: %s' % (
                object.polissa_id.name,
                object.cups_id.name,
                object.cups_id.direccio)
            # Check if we have the field 'payment_mode_id' (only in comer)
            if 'payment_mode_id' in model_obj.fields_get(cursor, uid):
                context.update({'payment_mode_id': object.payment_mode_id.id})
        else:
            invoice_id = object.id
            if object.origin:
                info = 'Referencia: %s' % object.origin

        if attach_original:
            report_name = self.get_report_name(cursor, uid, model,
                                               context=context)
            attachment = [{'report': report_name,
                           'model': model,
                           'model_id': model_id}]

        facturae_obj = self.pool.get('account.invoice.facturae')
        str_xml, filename, xml = facturae_obj.facturae_generate_xml(
            cursor, uid, invoice_id,
            info=info, attachment=attachment, context=context
        )

        return str_xml, filename, xml

    def export_facturae(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        attach_obj = self.pool.get('ir.attachment')

        wizard = self.browse(cursor, uid, ids[0], context=context)

        model = context.get('model', None)
        if model is None:
            raise osv.except_osv('Error',
                                 _('No associated model found'))

        model_ids = context.get('active_ids', [])
        if not model_ids:
            return {}

        compress_file = False
        if wizard.compress_file:
            compress_file = True
            compress_folder = tempfile.mkdtemp()

        for model_id in model_ids:
            str_xml, filename, xml = self.generate_facturae(
                cursor, uid, model, model_id,
                attach_original=wizard.attach_original, context=context
            )

            # Store result xml as attachment
            vals = {
                'name': 'Facturae',
                'datas': base64.b64encode(str_xml),
                'datas_fname': filename,
                'res_model': model,
                'res_id': model_id,
            }
            attach_obj.create(cursor, uid, vals)
            self.oncreated_facturae(cursor, uid, model, model_id,
                    context=context)

            if compress_file:
                with open(os.path.join(compress_folder, filename), 'w') as f:
                    f.write(str_xml)

        wizard_info = _('Facturae file stored as attachment of the invoice')
        wizard.write({'state': 'end_export', 'info': wizard_info})
        if compress_file:
            wizard.write({
                'compressed_filename': 'efact.zip',
                'compressed_file': base64.b64encode(zipdir(compress_folder))
                })

        return True

    def send_facturae(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context=context)

        model = context.get('model', None)
        if model is None:
            raise osv.except_osv('Error',
                                 _('No associated model found'))

        model_ids = context.get('active_ids', [])
        if not model_ids:
            return {}

        wizard_info = ''
        model_obj = self.pool.get(model)
        for model_id in model_ids:
            invoice_number = model_obj.read(
                cursor, uid, model_id, ['number'])['number']
            str_xml, filename, xml = self.generate_facturae(
                cursor, uid, model, model_id,
                attach_original=wizard.attach_original, context=context
            )

            temp_file_folder = tempfile.mkdtemp()
            invoice_filename = os.path.join(temp_file_folder, filename)
            with open(invoice_filename, 'w') as f:
                f.write(str_xml)

            face = FACe(
                environment=config.get(
                    'face_environment', self._default_face_environment),
                certificate=config.get('face_certificate', None),
                email=config.get('face_notification_email', None)
            )
            attachments = []
            if wizard.attach_original:
                invoice_attachments = (
                    xml.invoices.invoice[0].additionaldata.relateddocuments.attachment
                )
                for i, attach in enumerate(invoice_attachments):
                    attachments.append({
                        'anexo': {
                            'anexo': attach.attachmentdata.value,
                            'mime': 'application/{}'.format(
                                attach.attachmentformat.value
                            ),
                            'nombre': 'adjunto {} - {}'.format(
                                invoice_number, (i + 1)
                            )
                        }
                    })

            result = face.invoices.send(
                invoice=str(invoice_filename), attachments=attachments
            )
            result_code = result.data['resultado']['codigo']
            # Si el estado es Correcto
            if result_code is 0:  # Is exactly 0, not False neither empty string
                self.oncreated_facturae(
                    cursor, uid, model, model_id, context=context
                )
            result_str = _(
                'Invoice "{invoice_number}": '
                '\n  - Code: "{code}"'
                '\n  - Tracking code: "{tracking_code}"'
                '\n  - Description: "{description}"'
            ).format(
                invoice_number=invoice_number,
                code=result_code,
                tracking_code=result.data['resultado']['codigoSeguimiento'],
                description=result.data['resultado']['descripcion']
            )
            if result.errors:
                result_str += _('\n  - Error: "{}"').format(result.errors)
            wizard_info += '\n' + result_str

        wizard.write({'state': 'end_send', 'info': wizard_info})

        return True

    def oncreated_facturae(self, cursor, uid, model, model_id, context=None):
        model_obj = self.pool.get(model)
        model_obj.write(cursor, uid, model_id, {
            "facturae_exported": True,
            "facturae_exported_date": datetime.now().strftime('%Y-%m-%d')
        })
        return True


    _state_selection = [('init', 'Init'),
                        ('end_export', 'End export'),
                        ('end_send', 'End send')]

    _columns = {
        'attach_original': fields.boolean('Attach original',
                                          help="Attach invoice pdf"),
        'compress_file': fields.boolean('Create compressed file',
                                          help="Create compressed file with invoices"),
        'compressed_filename': fields.char('Compressed filename',
                                          help="Compressed filename  with invoices", size=256),
        'compressed_file': fields.binary('Compressed file',
                                          help="Compressed file with invoices"),
        'file': fields.binary('Compressed file',
                                          help="Compressed file with invoices"),
        'state': fields.selection(_state_selection, 'State', required=True),
        'info': fields.text('Information', readonly=True)
    }

    _defaults = {
        'attach_original': lambda *a: True,
        'state': lambda *a: 'init',
        'info': _get_default_wizard_info
    }

WizardExportFacturae()
