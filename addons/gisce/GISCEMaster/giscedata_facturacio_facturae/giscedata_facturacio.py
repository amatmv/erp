# -*- encoding: utf-8 -*-
from osv import osv, fields


class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def onchange_polissa(self, cursor, uid, ids, polissa_id, type_,
                         context=None):
        """S'actualitzen tots els camps possibles
        segons la pòlissa seleccionada"""

        if not context:
            context = {}

        res = super(GiscedataFacturacioFactura, self).onchange_polissa(
            cursor, uid, ids, polissa_id, type_, context=context)

        if polissa_id and type_ in ('out_invoice', 'out_refund'):
            pol_obj = self.pool.get('giscedata.polissa')
            polissa_data = pol_obj.read(
                cursor, uid, polissa_id,
                ['facturae_filereference']
            )
            if polissa_data['facturae_filereference']:
                res['value'].update({
                    'facturae_filereference': polissa_data['facturae_filereference']
                    }
                )
        return res

GiscedataFacturacioFactura()
