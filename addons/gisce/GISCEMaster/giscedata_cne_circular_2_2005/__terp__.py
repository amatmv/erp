# -*- coding: utf-8 -*-
{
    "name": "CNE Circular 2/2005",
    "description": """
        Informe per a la circular 
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "base",
        "base_extended",
        "giscedata_administracio_publica_cne",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/wizard_circular_2_2005_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
