SELECT
  aggregation.ANIO_ENVIO,
  aggregation.TRIMESTRE_ENVIO,
  aggregation.COD_COM,
  aggregation.COD_DIS,
  aggregation.COD_TPM,
  aggregation.TIPO_SUMINISTRO,
  aggregation.MODO_FACTURACION,
  (
    SELECT
      COUNT(DISTINCT md.polissa_id)
    FROM
      giscedata_polissa_modcontractual md
      LEFT JOIN res_partner comer ON md.comercialitzadora = comer.id
      LEFT JOIN res_partner distri ON md.distribuidora = distri.id
      JOIN giscedata_cups_ps cups ON md.cups = cups.id
      JOIN res_municipi mun ON cups.id_municipi = mun.id
    WHERE %(dfinal_ener)s BETWEEN md.data_inici AND md.data_final
      AND md.agree_tipus = aggregation.COD_TPM
      AND comer.ref2 = aggregation.COD_COM
      AND (
        distri.ref2 IS NULL
        OR distri.ref2 = aggregation.COD_DIS
      )
  ) AS NUM_SUM,
  CASE
    WHEN aggregation.ENERGIA = 0 THEN 0
    ELSE ROUND(aggregation.PRE_MED_TAR / ENERGIA, 3)
  END AS PRE_MED_TAR,
  CASE
    WHEN aggregation.ENERGIA = 0 THEN 0
    ELSE ROUND(aggregation.PRE_MED_SUM / ENERGIA, 3)
  END AS PRE_MED_SUM
FROM (
  SELECT
    TO_CHAR(DATE(%(dfinal_ener)s),'YYYY') AS ANIO_ENVIO,
    %(periode_t)s AS TRIMESTRE_ENVIO,
    comer.ref2 AS COD_COM,
    COALESCE(distri.ref2, distri.name) AS COD_DIS,
    polissa.agree_tipus AS COD_TPM,
    'E' AS TIPO_SUMINISTRO,
    1 AS MODO_FACTURACION,
    COALESCE(SUM(flin.atrprice_subtotal * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END * 100), 0) AS PRE_MED_TAR,
    COALESCE(SUM((line.price_subtotal - flin.atrprice_subtotal) * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END * 100), 0) AS PRE_MED_SUM,
    ROUND(SUM(COALESCE(
      CASE WHEN flin.tipus = 'energia' THEN (CASE WHEN inv.type = 'out_refund' THEN -line.quantity  ELSE line.quantity END)
      ELSE 0 END, 0
    )), 0)::int AS ENERGIA
  FROM
    giscedata_facturacio_factura fact
    JOIN giscedata_facturacio_factura_linia flin ON fact.id = flin.factura_id
    JOIN account_invoice inv ON inv.id = fact.invoice_id
    JOIN account_invoice_line line ON flin.invoice_line_id = line.id
    JOIN giscedata_polissa polissa ON polissa.id = fact.polissa_id
    JOIN giscedata_polissa_tarifa t ON t.id = polissa.tarifa
    LEFT JOIN res_partner comer ON polissa.comercialitzadora = comer.id
    LEFT JOIN res_partner distri ON polissa.distribuidora = distri.id
    JOIN giscedata_cups_ps cups ON polissa.cups = cups.id
    JOIN res_municipi mun ON cups.id_municipi = mun.id
    LEFT JOIN res_country_state cs ON mun.state = cs.id
  WHERE
    inv.date_invoice BETWEEN %(ini_per_energia)s AND %(fin_per_energia)s
    AND flin.tipus IN ('energia', 'potencia', 'reactiva', 'exces_potencia')
    AND inv.type IN ('out_invoice', 'out_refund')
    AND inv.state NOT IN ('draft', 'cancel', 'proforma')
  GROUP BY
    ANIO_ENVIO, TRIMESTRE_ENVIO, COD_DIS, COD_COM, COD_TPM
) AS aggregation
WHERE
  aggregation.ENERGIA > 0
