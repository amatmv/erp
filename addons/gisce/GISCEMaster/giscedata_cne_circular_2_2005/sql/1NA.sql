SELECT
  aggregation.ANIO_ENVIO,
  aggregation.TRIMESTRE_ENVIO,
  aggregation.COD_COM,
  aggregation.COD_DIS,
  aggregation.COD_TPM,
  aggregation.MODO_FACTURACION,
  (
    SELECT
      COUNT(DISTINCT md.polissa_id)
    FROM
      giscedata_polissa_modcontractual md
      INNER JOIN (
        SELECT
          CASE
            WHEN t.name = '2.0A' THEN '01'
            WHEN t.name = '2.0DHA' THEN '02'
            WHEN t.name = '3.0A' THEN '03'
            WHEN t.name = '3.0A LB' THEN '03'
            WHEN t.name = '3.1A' THEN '04'
            WHEN t.name = '3.1A LB' THEN '04'
            WHEN t.name = '6.1' THEN '05'
            WHEN t.name = '6.1A' THEN '05'
            WHEN t.name = '6.1B' THEN '17'
            WHEN t.name = '6.2' THEN '06'
            WHEN t.name = '6.3' THEN '07'
            WHEN t.name = '6.4' THEN '08'
            WHEN t.name = '6.5' THEN '09'
            WHEN t.name = '2.1A' THEN '10'
            WHEN t.name = '2.1DHA' THEN '11'
            WHEN t.name = '2.0DHS' THEN '12'
            WHEN t.name = '2.1DHS' THEN '13'
          END AS codi_tarifa,
          t.id AS tarifa_id
        FROM giscedata_polissa_tarifa t
      ) tarifa ON md.tarifa = tarifa.tarifa_id
      LEFT JOIN res_partner comer ON md.comercialitzadora = comer.id
      LEFT JOIN res_partner distri ON md.distribuidora = distri.id
      JOIN giscedata_cups_ps cups ON md.cups = cups.id
      JOIN res_municipi mun ON cups.id_municipi = mun.id
      LEFT JOIN res_country_state cs ON mun.state = cs.id
    WHERE %(dfinal_ener)s BETWEEN md.data_inici AND md.data_final
      AND md.agree_tipus = aggregation.COD_TPM
      AND (
        distri.ref2 IS NULL OR distri.ref2 = aggregation.COD_DIS
      )
      AND comer.ref2 = aggregation.COD_COM
      AND RPAD(cs.code::text, 5, '0') = aggregation.COD_PRV
      AND tarifa.codi_tarifa = aggregation.COD_TIPO_TAR_ACCESO
  ) AS num_sum,
  aggregation.COD_PRV,
  aggregation.COD_TIPO_TAR_ACCESO,
  CASE
    WHEN aggregation.ENERGIA = 0 THEN 0
    ELSE round(aggregation.PRE_MED_TAR / ENERGIA, 3)
  END,
  CASE
    WHEN aggregation.ENERGIA = 0 THEN 0
    ELSE round(aggregation.PRE_MED_SUM / ENERGIA, 3)
  END,
  aggregation.ENERGIA
FROM (
  SELECT
    TO_CHAR(DATE(%(dfinal_ener)s),'YYYY') AS ANIO_ENVIO,
    %(periode_t)s AS TRIMESTRE_ENVIO,
    comer.ref2 AS COD_COM,
    COALESCE(distri.ref2, distri.name) AS COD_DIS,
    polissa.agree_tipus AS COD_TPM,
    1 AS MODO_FACTURACION,
    RPAD(cs.code::text, 5, '0') AS COD_PRV,
    (
      SELECT CASE
        WHEN t.name = '2.0A' THEN '01'
        WHEN t.name = '2.0DHA' THEN '02'
        WHEN t.name = '3.0A' THEN '03'
        WHEN t.name = '3.0A LB' THEN '03'
        WHEN t.name = '3.1A' THEN '04'
        WHEN t.name = '3.1A LB' THEN '04'
        WHEN t.name = '6.1' THEN '05'
        WHEN t.name = '6.1A' THEN '05'
        WHEN t.name = '6.1B' THEN '17'
        WHEN t.name = '6.2' THEN '06'
        WHEN t.name = '6.3' THEN '07'
        WHEN t.name = '6.4' THEN '08'
        WHEN t.name = '6.5' THEN '09'
        WHEN t.name = '2.1A' THEN '10'
        WHEN t.name = '2.1DHA' THEN '11'
        WHEN t.name = '2.0DHS' THEN '12'
        WHEN t.name = '2.1DHS' THEN '13'
      END
    ) AS COD_TIPO_TAR_ACCESO,
    COALESCE(sum(flin.atrprice_subtotal * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END * 100), 0) AS PRE_MED_TAR,
    COALESCE(sum((line.price_subtotal - flin.atrprice_subtotal)* CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END * 100), 0) AS PRE_MED_SUM,
    round(sum(COALESCE(
      CASE WHEN flin.tipus = 'energia' THEN (CASE WHEN inv.type = 'out_refund' THEN -line.quantity  ELSE line.quantity END)
      ELSE 0 END, 0
    )), 0)::int AS ENERGIA
  FROM
    giscedata_facturacio_factura fact
    JOIN giscedata_facturacio_factura_linia flin ON fact.id = flin.factura_id
    JOIN account_invoice inv ON inv.id = fact.invoice_id
    JOIN account_invoice_line line ON flin.invoice_line_id = line.id
    JOIN giscedata_polissa polissa ON polissa.id = fact.polissa_id
    JOIN giscedata_polissa_tarifa t ON t.id = polissa.tarifa
    LEFT JOIN res_partner comer ON polissa.comercialitzadora = comer.id
    LEFT JOIN res_partner distri ON polissa.distribuidora = distri.id
    JOIN giscedata_cups_ps cups ON polissa.cups = cups.id
    JOIN res_municipi mun ON cups.id_municipi = mun.id
    LEFT JOIN res_country_state cs ON mun.state = cs.id
  WHERE
    inv.date_invoice BETWEEN %(ini_per_energia)s AND %(fin_per_energia)s
    AND flin.tipus IN ('energia', 'potencia', 'reactiva', 'exces_potencia')
    AND inv.type IN ('out_invoice', 'out_refund')
    AND inv.state NOT IN ('draft', 'cancel', 'proforma')
  GROUP BY
    ANIO_ENVIO, TRIMESTRE_ENVIO, COD_COM, COD_DIS, COD_TPM, MODO_FACTURACION, COD_PRV, COD_TIPO_TAR_ACCESO
  ORDER BY
    COD_PRV
) AS aggregation
WHERE
  aggregation.ENERGIA > 0
