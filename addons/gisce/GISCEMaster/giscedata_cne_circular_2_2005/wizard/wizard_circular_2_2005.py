# coding=utf-8
from osv import osv, fields
from datetime import datetime
import base64
import netsvc
from addons import get_module_resource



class GiscedataCNECircular22005(osv.osv_memory):
    """
    Assistent per generar els inventaris de la CNE: Circular 2/2005
    """
    _name = 'giscedata.cne.circular.2.2005'

    def _generar_anys(self, cursor, uid, context=None):
        return [
            (year, str(year)) for year in
            range(datetime.now().year - 10, datetime.now().year + 1)
        ]

    _columns = {
        'type': fields.selection(
            [('1NA', '1NA'), ('3', '3')], 'Tipo', required=True
        ),
        'periode_t': fields.selection(
            [('T1', 'T1'), ('T2', 'T2'), ('T3', 'T3'), ('T4', 'T4')],
            'Trimestre', required=True
        ),
        'any_t': fields.selection(
            _generar_anys, 'Año', required=True
        ),
        'ini_per_energia': fields.date('Período de energía del'),
        'fin_per_energia': fields.date('al'),
        'dinici_ener': fields.date('Período del'),
        'dfinal_ener': fields.date('al'),
        'file_name': fields.char('Nombre de fichero', size=30),
        'file': fields.binary('Fichero salida'),
        # State controllers
        'state': fields.selection([('init', 'Init'),
                                   ('fready', 'File ready')]),
        'file_state': fields.selection([
            ('nogen', 'nogen'), ('ready', 'ready'), ('gen', 'gen')
        ]),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'file': lambda *a: False,
        'file_state': lambda *a: 'nogen',
        'periode_t': lambda *a: False,
        'any_t': lambda *a: False,
    }

    def onchange_update_wizard(self, cursor, uid, ids, type, periode_t, any_t, context=None):
        """
        Function that updates the wizard view to show only the necesary fields
        in function of wich fields are selected

        :param      cursor:         Database cursor, unused
        :type       cursor:         cursor
        :param      uid:            User id, unused
        :type       uid:            int
        :param      ids:            Affected ids, unused
        :type       ids:            int
        :param      type:           Iventory to generate
        :type       type:           str
        :param      periode_t:      Trimestre to generate the inventory
        :type       periode_t:      str
        :param      any_t:          Year of the trimestre to generate the inven.
        :type       any_t:          str
        :param      context:    OpenERP context, unused
        :type       context:    dict
        :return:    Dictionary with the fields to update of the view.
        :rtype:     dict[str, str or bool]
        """

        if any_t is False or periode_t is False:
            return {
                'value': {
                    'dinici_ener': False,
                    'dfinal_ener': False,
                    'ini_per_energia': False,
                    'fin_per_energia': False,
                    'file_state': 'ready'
                }
            }
        else:
            # Definim el periode temporal i el periode d'energia
            periode = {
                'T1': ('{}-01-01'.format(any_t), '{}-03-31'.format(any_t)),
                'T2': ('{}-04-01'.format(any_t), '{}-06-30'.format(any_t)),
                'T3': ('{}-07-01'.format(any_t), '{}-09-30'.format(any_t)),
                'T4': ('{}-10-01'.format(any_t), '{}-12-31'.format(any_t))
            }
            periode_ener = {
                'T1': ('{}-04-01'.format(any_t - 1), '{}-03-31'.format(any_t)),
                'T2': ('{}-07-01'.format(any_t - 1), '{}-06-30'.format(any_t)),
                'T3': ('{}-10-01'.format(any_t - 1), '{}-09-30'.format(any_t)),
                'T4': ('{}-01-01'.format(any_t), '{}-12-31'.format(any_t))
            }
            return {
                'value': {
                    'dinici_ener': periode[periode_t][0],
                    'dfinal_ener': periode[periode_t][1],
                    'ini_per_energia': periode_ener[periode_t][0],
                    'fin_per_energia': periode_ener[periode_t][1],
                    'file_state': 'ready'
                }
            }

    def export_file(self, cursor, uid, ids, context=None):
        """
        Function that executes the inventory query and create the file with the
        result of the query. It also updates the view to show the file manager.

        :param      cursor:     Database cursor
        :type       cursor:     cursor
        :param      uid:        User id
        :type       uid:        int
        :param      ids:        Affected ids
        :type       uid:        int
        :param      context:    OpenERP context
        :type       context:    dict
        """

        sql_name = {
            '1NA': '1NA.sql',
            '3': '3.sql'
        }

        logger = netsvc.Logger()
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Starting CNE export")

        # Carreguem parametres de l'informe
        wizard = self.read(
            cursor, uid, ids[0], []
        )[0]
        sql_file = sql_name[wizard['type']]

        sql = open(get_module_resource(
            'giscedata_cne_circular_2_2005', 'sql', sql_file)
        ).read()

        # Executem informe amb els parametres seleccionats en el wizard
        params = {
            'dinici_ener': wizard['dinici_ener'],
            'dfinal_ener': wizard['dfinal_ener'],
            'ini_per_energia': wizard['ini_per_energia'],
            'fin_per_energia': wizard['fin_per_energia'],
            'periode_t': wizard['periode_t']
        }
        cursor.execute(sql, params)

        # Generem l'informe amb les dades retornades per la query
        informe = []
        for line in cursor.fetchall():
            line = list(line)
            informe.append(';'.join([str(a) or '' for a in line]))
        informe = '\n'.join(informe)
        informe += '\n'
        mfile = base64.b64encode(informe.encode('utf-8'))

        # Generem el nom de l'informe amb el que es guardara el fitxer
        cursor.execute(*(
            self.pool.get('res.users').q(cursor, uid).select(
                ['company_id.partner_id.ref2']
            ).where([('id', '=', 1)])
        ))
        r2 = cursor.fetchone()[0]
        year = wizard['any_t']
        period = wizard['periode_t'][1]

        filename = 'CIR2_2005_{}_{}_{}{}.txt'.format(
            wizard['type'], r2, year, period
        )
        self.write(cursor, uid, ids, {
            'state': 'fready', 'file': mfile, 'file_name': filename
        }, context)
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Ending CNE export")

    def reengage(self, cursor, uid, ids, context=None):
        """
        Function that updates the wizard view to generate another inventory
        without close the wizard.

        :param      cursor:     Database cursor
        :type       cursor:     cursor
        :param      uid:        User id
        :type       uid:        int
        :param      ids:        Affected ids
        :type       uid:        int
        :param      context:    OpenERP context
        :type       context:    dict
        """
        self.write(cursor, uid, ids, {'state': 'init'}, context)


GiscedataCNECircular22005()
