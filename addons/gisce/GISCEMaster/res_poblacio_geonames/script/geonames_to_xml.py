# -*- coding: utf-8 -*-
import csv


import click
from slugify import slugify
from progressbar import ProgressBar, ETA, Percentage, Bar

def get_xml_line(pob_name, ine):
    record = '''
    <record model="res.poblacio" id="{pob_slug}">
        <field name="name">{pob_name}</field>
        <field ref="base_extended.ine_{ine}" model='res.municipi' name='municipi_id'/>
    </record>'''
    pob_slug = 'pob_{ine}_{slug}'.format(ine=ine,slug=slugify(pob_name))
    return record.format(ine=ine, pob_name=pob_name,pob_slug=pob_slug[:64])
     


@click.command()
@click.option('--file', help='CSV file path')
@click.option('--outfile', help='Out XML file')
def process_pob(file,outfile):
    f = open(file, 'rb')
    of = open(outfile, 'wb')
    of.write('''<?xml version="1.0" encoding="UTF-8"?>
    <openerp>
        <data>''')
    reader = csv.reader(f, delimiter='\t')
    lines = [line for line in reader]
    widgets = [Percentage(), ' ', Bar(), ' ', ETA()]
    pbar = ProgressBar(widgets=widgets, maxval=len(lines)).start()
    done = 0
    pob = {}
    for row in lines:
        if row[7].startswith('PPL') and row[12].strip():
            key = '{}_{}'.format(row[12], slugify(row[1]))
            if key not in pob:
                record = get_xml_line(row[1], row[12])
                of.write(record)
                pob.update({key:1})
            else:
                pob[key] += 1
        done += 1
        pbar.update(done)
    of.write('''
    </data>
    </openerp>''')
    f.close()
    of.close()
    pbar.finish()


if __name__ == '__main__':
    process_pob()
