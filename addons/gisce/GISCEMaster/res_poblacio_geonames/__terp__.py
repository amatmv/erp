# -*- coding: utf-8 -*-
{
    "name": "Res poblacio geonames",
    "description": """
    Creació de poblacions basats en fitxer obtingut de
    http://www.geonames.org/""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "res_poblacio_data.xml"
    ],
    "active": False,
    "installable": True
}
