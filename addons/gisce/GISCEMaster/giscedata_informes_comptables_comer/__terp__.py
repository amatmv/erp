# -*- coding: utf-8 -*-
{
    "name": "Informes comptables comercialitzadora",
    "description": """
Diferents informes comptables per comercialitzadora
    * Llistat IESE per factures
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Account",
    "depends":[
        "giscedata_facturacio_comer",
        "jasper_reports",
        "giscedata_informes_comptables"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "informes_comptabilitat_comer_report.xml"
    ],
    "active": False,
    "installable": True
}
