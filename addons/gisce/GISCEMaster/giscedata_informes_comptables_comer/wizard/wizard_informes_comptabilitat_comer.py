# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import time


class WizardInformesComptabilitatComer(osv.osv_memory):

    _name = "wizard.informes.comptabilitat"
    _inherit = "wizard.informes.comptabilitat"

    _get_informe = [('report_facturacio_llibre_registre_iva_fechas',
                     'Llibre registre'),
                    ('report_facturacio_llibre_registre_general_fechas',
                     'Llibre registre general'),
                    ('report_facturacio_llistat_iese_fechas',
                     'Listado IESE'),
                    ('libro_registro_iva',
                     'Llibre registra IVA (multiserie)')
                   ]

    _columns = {
        'informe': fields.selection(_get_informe, 'Informe', required=True),
                
    }

WizardInformesComptabilitatComer()
