# -*- coding: utf-8 -*-

import jasper_reports

def informes(cursor, uid, ids, data, context):

        
    return {
        'parameters': {'data_inici':data['form']['data_inici'],
                       'data_final':data['form']['data_final'],
                       'serie': data['form']['serie'],
                      },
    }

jasper_reports.report_jasper(
   'report.report_facturacio_llistat_iese_fechas',
   'wizard.informes.comptabilitat',
   parser=informes
)
