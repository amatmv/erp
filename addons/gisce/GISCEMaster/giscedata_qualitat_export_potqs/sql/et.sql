SELECT
  substring(ct.name,4,4) AS C_NUMERO_ET,
  ct.descripcio AS C_DESCRIPCION,
  tr.potencia_nominal AS C_POTENCIA_NOMINAL,
  tr.ordre_dins_ct AS C_ORDRE_DINS_CT,
  tr.id_estat AS C_ID_ESTAT,
  mun.ine || mun.dc AS C_MUN_INE,
  mun.name AS C_MUN_NAME,
  CTcount.numtr as C_TRAFOS_A_ET,
  tr.name as C_NUMERO_TRAFO,
  ct.id_installacio as C_TIPO_ET
FROM
  giscedata_transformador_trafo tr
  LEFT JOIN giscedata_cts ct ON ct.id = tr.ct
  LEFT JOIN giscedata_transformador_connexio trcon ON trcon.trafo_id = tr.id
  LEFT JOIN res_municipi mun ON mun.id = ct.id_municipi
  LEFT JOIN (SELECT
              ct.name as CTname, count(*) as numTR
            FROM
              giscedata_transformador_trafo tr
            LEFT JOIN giscedata_cts ct ON ct.id = tr.ct
            LEFT JOIN giscedata_transformador_connexio trcon ON trcon.trafo_id = tr.id
            WHERE
              trcon.conectada = 't' and (tr.id_estat = 1 or tr.id_estat = 46) and ct.ct_baixa = 'f' and tr.tiepi = true
              group by ct.name) CTcount ON ct.name = CTcount.CTname
WHERE
  trcon.conectada = 't' and (tr.id_estat = 1 or tr.id_estat = 46) and tr.tiepi = true
  and (ct.id_installacio <> 7 or (ct.id_installacio = 7 and tr.ordre_dins_ct is not null)) /* Que no sea CM (trafo del cliente) */
UNION
SELECT
  substring(ct.name,4,4) AS C_NUMERO_ET,
  ct.descripcio AS C_DESCRIPCION,
  '0' AS C_POTENCIA_NOMINAL,
  99 AS C_ORDRE_DINS_CT,
  46 AS C_ID_ESTAT,
  mun.ine || mun.dc AS C_MUN_INE,
  mun.name AS C_MUN_NAME,
  0 as C_TRAFOS_A_ET,
  '0' as C_NUMERO_TRAFO,
  ct.id_installacio as C_TIPO_ET
FROM
  giscedata_cts ct
  LEFT JOIN res_municipi mun ON mun.id = ct.id_municipi
WHERE ct.id_installacio = 7 AND ct.ct_baixa = 'f' /* Solo los que son CM */
ORDER BY
  C_NUMERO_ET ASC,
  C_ORDRE_DINS_CT ASC
