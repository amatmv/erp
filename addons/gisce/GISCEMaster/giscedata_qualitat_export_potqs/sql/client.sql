SELECT
  cups.name AS C_CUPS,
  res_partner.name AS C_NAME,
  cups_ps.direccio AS C_ADDRESS,
  cups_ps.zona AS C_ZONA,
  cups_ps.ordre AS C_ORDRE,
  p.name AS C_POLISSA,
  e.name AS C_ESCOMESA,
  etr.tensio AS C_TENSIO,
  p.potencia AS C_POT_CONTRATADA,
  '' AS C_NUM_FASES,
  numTR AS C_TRAFOS_A_ET,
  substring(ct.name,4,4) AS C_NUMERO_ET,
  tr.ordre_dins_ct AS C_ORDRE_TR,
  f.sortida_bt AS C_SORTIDA,
  ct.tensio_p AS C_CT_TENSIO

FROM
  giscedata_cups_escomesa e
  LEFT JOIN giscegis_escomeses_traceability etr ON e.id = etr.escomesa
  LEFT JOIN giscedata_cts ct ON etr.ct = ct.id
  LEFT JOIN giscedata_transformador_trafo tr ON etr.trafo = tr.id
  LEFT JOIN giscegis_blocs_fusiblesbt f ON etr.fusible = f.id
  LEFT JOIN giscedata_cups_ps cups ON e.id = cups.id_escomesa
  INNER JOIN giscedata_polissa p ON cups.id = p.cups AND p.active = true
  LEFT JOIN res_partner ON p.titular = res_partner.id
  LEFT JOIN giscedata_cups_ps cups_ps ON p.cups = cups_ps.id
  LEFT JOIN (SELECT ct.name as CTname, count(*) as numTR FROM giscedata_transformador_trafo tr LEFT JOIN giscedata_cts ct ON ct.id = tr.ct WHERE ct.ct_baixa = 'f' group by ct.name) CTcount ON ct.name = CTcount.CTname

ORDER BY
  e.name
