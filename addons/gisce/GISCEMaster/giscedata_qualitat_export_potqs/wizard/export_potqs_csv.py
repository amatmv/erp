# -*- coding: utf-8 -*-
import wizard
import pooler
import base64
import csv
import StringIO
import time
import re


def _init(self, cr, uid, data, context={}):
    data['n'] = len(data['ids'])
    data['i'] = 0
    return {}

def _continuar(self, cr, uid, data, context={}):
    if data['i'] >= data['n']:
        return 'end'
    else:
        return 'csv'

def _seguent(self, cr, uid, data, context={}):
    data['i'] += 1
    return {}

def _csv(self, cr, uid, data, context={}):
    #crearem el fitxer CSV
    # SQL substitutiu:
    install_data_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.install_power.data')
    install_data = install_data_obj.browse(cr, uid, data['ids'][data['i']])
    cr.execute("select lpad(regexp_replace(ct, '[A-Za-z_-]+', ''), 4, '0') as ct, descripcio, sum(power), municipi from (select p.code_ct as ct, (select descripcio from giscedata_cts where name = p.code_ct limit 1) as descripcio, case when p.type = 'C' then p.power/pd.cosfi else p.power end as power, p.type, m.name as municipi from giscedata_qualitat_install_power p, giscedata_qualitat_install_power_data pd, res_municipi m where p.install_power_data_id = pd.id and p.codeine = m.id and install_power_data_id = %s) as foo group by ct,descripcio,municipi order by ct", (data['ids'][data['i']],))
    output = StringIO.StringIO()
    writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE)
    for line in cr.fetchall():
        writer.writerow(line)
    file = base64.b64encode(output.getvalue())
    output.close()
    return {'name': 'potqs_%s.csv' % install_data.name[0:10], 'file': file}

_csv_form = """<?xml version="1.0"?>
<form string="Fitxer PotQS">
  <field name="name" colspan="4" width="500"/>
  <field name="file" colspan="4"/>
</form>
"""

_csv_fields = {
  'name': {'string': 'Nom', 'type': 'char', 'size': 60},
  'file': {'string': 'Fitxer', 'type': 'binary'},
}


class wizard_qualitat_potqs_csv(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
          'result': { 'type' : 'state', 'state' : 'continuar' },
      },
      'csv': {
          'actions': [_csv],
          'result': {'type': 'form', 'arch': _csv_form, 'fields': _csv_fields, 'state': [('seguent', 'Següent', 'gtk-go-forward'), ('end', 'Tancar', 'gtk-close')]}
      },
      'seguent': {
        'actions': [_seguent],
        'result': {'type': 'state', 'state': 'continuar'}
      },
      'continuar': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _continuar}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'}
      },
    }

wizard_qualitat_potqs_csv('giscedata.qualitat.export.potqs')
