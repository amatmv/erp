# -*- coding: utf-8 -*-
import netsvc
from osv import osv
from tools import config
from tools.sql import drop_view_if_exists

class ViewGiscedataQualitatExportPotQSEt(osv.osv):
    """Vista pel CALSER.
    """
    _name = 'view.giscedata.qualitat.export.potqs.et'
    _auto = False

    def init(self, cursor):
        logger = netsvc.Logger()
        view_name = self._name.replace('.', '_')
        view_query = open('%s/giscedata_qualitat_export_potqs/sql/et.sql'
                          % config['addons_path'], 'r').read()
        drop_view_if_exists(cursor, view_name)
        logger.notifyChannel('addons', netsvc.LOG_INFO,
                             "creating view %s" % view_name)
        cursor.execute("create or replace view %s as (%s)"
                       % (view_name, view_query))

ViewGiscedataQualitatExportPotQSEt()

class ViewGiscedataQualitatExportPotQSClient(osv.osv):
    """Vista pel CALSER.
    """
    _name = 'view.giscedata.qualitat.export.potqs.client'
    _auto = False

    def init(self, cursor):
        logger = netsvc.Logger()
        view_name = self._name.replace('.', '_')
        view_query = open('%s/giscedata_qualitat_export_potqs/sql/client.sql'
                          % config['addons_path'], 'r').read()
        drop_view_if_exists(cursor, view_name)
        logger.notifyChannel('addons', netsvc.LOG_INFO,
                             "creating view %s" % view_name)
        cursor.execute("create or replace view %s as (%s)"
                       % (view_name, view_query))

ViewGiscedataQualitatExportPotQSClient()
