# -*- coding: utf-8 -*-
{
    "name": "GISCE Qualitat Export Calser",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "giscedata_qualitat",
        "giscedata_transformadors",
        "giscedata_cups_distri",
        "giscegis_escomeses_traceability"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_qualitat_export_calser_wizard.xml"
    ],
    "active": False,
    "installable": True
}
