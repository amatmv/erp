# -*- coding: utf-8 -*-
import wizard
import pooler
import time
import netsvc


def _init(self, cr, uid, data, context={}):
    data['partners'] = []
    data['text'] = ''
    rfur_obj = pooler.get_pool(cr.dbname).get('giscedata.liquidacio.rfur')
    ids = rfur_obj.search(cr, uid, [('state', '=', 'para_liquidar'), ('inici', '<', 'now()'), ('final', '>=', 'now()')])
    data['rfur_ids'] = ids
    for rfur in rfur_obj.browse(cr, uid, ids, context):
        if len(rfur.company_id.partner_id.address):
            if rfur.company_id.partner_id.address[0].email:
                data['text'] += '%s: %s <%s>\n' % (rfur.company_id.name, rfur.company_id.partner_id.address[0].name, rfur.company_id.partner_id.address[0].email)
                data['partners'].append(rfur.company_id.partner_id.id)

    return {}

def _check(self, cr, uid, data, context={}):
    if len(data['partners']):
        return 'message'
    else:
        return 'no_partners'


def _message(self, cr, uid, data, context={}):
    val_obj = pooler.get_pool(cr.dbname).get('ir.values')
    subject = val_obj.get(cr, uid, 'meta', 'subject_para_liquidar', ['giscedata.liquidacio.rfur'])
    if len(subject):
        subject = subject[0][2]
    body = val_obj.get(cr, uid, 'meta', 'text_para_liquidar', ['giscedata.liquidacio.rfur'])
    if len(body):
        body = body[0][2]
    return {'mails_text': data['text'], 'subject': subject, 'text': body}



_message_form = """<?xml version="1.0"?>
<form string="Correo de aviso">
  <label string="Va a generar un correo de aviso para las siguientes personas:" colspan="4"/>
  <field name="mails_text" colspan="4" nolabel="1" readonly="1" />
  <group colspan="4" string="Correo Electrónico">
    <field name="subject" colspan="4" />
    <field name="text" colspan="4" width="400" height="300"/>
  </group>
</form>"""

_message_fields = {
  'mails_text': {'string': 'Mails', 'type': 'text'},
  'subject': {'string': 'Asunto', 'type': 'char', 'size': 255},
  'text': {'string': 'Mensaje', 'type': 'text'}
}

def _send(self, cr, uid, data, context={}):
    val_obj = pooler.get_pool(cr.dbname).get('ir.values')
    val_obj.set(cr, uid, 'meta', 'subject_para_liquidar', 'subject_para_liquidar', ['giscedata.liquidacio.rfur'], data['form']['subject'])
    val_obj.set(cr, uid, 'meta', 'text_para_liquidar', 'text_para_liquidar', ['giscedata.liquidacio.rfur'], data['form']['text'])
    partner_obj = pooler.get_pool(cr.dbname).get('res.partner')
    user = pooler.get_pool(cr.dbname).get('res.users').browse(cr, uid, uid)
    if user.address_id and user.address_id.email:
        mail_from = '%s <%s>' % (user.address_id.name, user.address_id.email)
    else:
        mail_from = 'noreply@gisce.net'
    if len(data['partners']):
        partner_obj.email_send(cr, uid, data['partners'], mail_from, data['form']['subject'], data['form']['text'])

    return {}


_end_form = """<?xml version="1.0"?>
<form string="Correo de aviso">
  <label string="Mensajes enviados correctamente." />
</form>"""

_end_fields = {}

_no_partners_form = """<?xml version="1.0"?>
<form string="Correo de aviso">
  <label string="No hay empresas para enviar mensajes o no tienen correo electrónico en su ficha de contacto." />
</form>"""

_no_partners_fields = {}


_marcar_enviats_form = """<?xml version="1.0"?>
<form string="Marcar como enviados">
  <label string="Quieres marcar como enviadas las liquidaciones?" />
  <newline />
  <group colspan="2">
    <field name="mail" />
  </group>
</form>"""

_marcar_enviats_fields = {
  'mail': {'string': 'Enviar correo electrónico', 'type': 'boolean'}
}

def _check_mail(self, cr, uid, data, context={}):
    if data['form']['mail']:
        return 'check'
    else:
        return 'marcar_enviats_workflow'


def _marcar_enviats_workflow(self, cr, uid, data, context={}):
    wf_service = netsvc.LocalService("workflow")
    for id in data['rfur_ids']:
        wf_service.trg_validate(uid, 'giscedata.liquidacio.rfur', id, 'enviar', cr)
    return {}


class wizard_liquidacio_marcar_enviats_rfur(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
        'result': {'type': 'state', 'state': 'marcar_enviats'},
      },
      'check': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _check},
      },
      'message': {
        'actions': [_message],
        'result': {'type': 'form', 'arch': _message_form,'fields': _message_fields, 'state':[('marcar_enviats_workflow', 'Cancelar', 'gtk-cancel'), ('send', 'Continuar', 'gtk-go-forward')]}
      },
      'no_partners': {
        'actions': [],
        'result': {'type': 'form', 'arch': _no_partners_form, 'fields': _no_partners_fields, 'state': [('marcar_enviats_workflow', 'Cerrar', 'gtk-close')]}
      },
      'marcar_enviats': {
        'actions': [],
        'result': {'type': 'form', 'arch': _marcar_enviats_form, 'fields': _marcar_enviats_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('check_mail', 'Continuar', 'gkt-ok')]}
      },
      'check_mail': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _check_mail},
      },
      'marcar_enviats_workflow': {
        'actions': [_marcar_enviats_workflow],
        'result': {'type': 'state', 'state': 'end'},
      },

      'send': {
        'actions': [_send],
        'result': {'type': 'state', 'state': 'pre_end'}
      },
      'pre_end': {
        'actions': [],
        'result': {'type': 'form', 'arch': _end_form, 'fields': _end_fields, 'state': [('marcar_enviats_workflow', 'De acuerdo', 'gtk-ok')]}
      },

    }

wizard_liquidacio_marcar_enviats_rfur('giscedata.liquidacio.dashboard.rfur.marcar_enviats')
