# -*- coding: utf-8 -*-
import wizard
import pooler
import time
import os
import zipfile
import base64
import StringIO


def _init(self, cr, uid, data, context={}):
    return {}

_init_form = """<?xml version="1.0"?>
<form string="XML" col="4">
  <field name="year" />
</form>"""

_init_fields = {
  'year': {'string': 'Año (AAAA)', 'type': 'char', 'size': 4, 'required': True},
}

def _gen(self, cr, uid, data, context={}):
    fpd_obj = pooler.get_pool(cr.dbname).get('giscedata.liquidacio.fpd')
    xmls = {}
    for c in pooler.get_pool(cr.dbname).get('res.company').search(cr, uid, []):
        xml = fpd_obj.xml_anual(cr, uid, c, data['form']['year'])
        xmls.update(xml)


    filename = 'FPDA_%s_%s12.zip' % (cr.dbname, data['form']['year'])

    zf = StringIO.StringIO()
    zfile = zipfile.ZipFile(zf, 'w', compression=zipfile.ZIP_DEFLATED)

    for file in xmls.keys():
        zfile.writestr(file, xmls[file])
    zfile.close()

    f64 = base64.b64encode(zf.getvalue())

    return {'file': f64, 'name': filename}


_gen_form = """<?xml version="1.0"?>
<form string="Generar ficheros">
  <field name="file" />
  <field name="name" invisible="1" />
</form>"""


_gen_fields = {
  'file': {'string': 'Fichero Zip', 'type': 'binary', 'readonly': True},
  'name': {'type': 'char', 'size': 50}
}

class wizard_liquidacio_generar_xml_fpda(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
        'result': {'type': 'form', 'arch': _init_form,'fields': _init_fields, 'state':[('gen', 'Generar')]}

      },
      'gen': {
        'actions': [_gen],
        'result': {'type': 'form', 'arch': _gen_form,'fields': _gen_fields, 'state':[('end', 'Cerrar', 'gtk-close')]}
      },
    }

wizard_liquidacio_generar_xml_fpda('giscedata.liquidacio.dashboard.fpda.generar_xml')
