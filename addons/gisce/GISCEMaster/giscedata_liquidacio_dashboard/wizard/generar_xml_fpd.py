# -*- coding: utf-8 -*-
import wizard
import pooler
import time
import os
import zipfile
import base64


def _init(self, cr, uid, data, context={}):
    fpd_obj = pooler.get_pool(cr.dbname).get('giscedata.liquidacio.fpd')
    ids = fpd_obj.search(cr, uid, [('state', '=', 'para_liquidar'), ('inici', '<', 'now()'), ('final', '>=', 'now()')])
    xmls = fpd_obj.xml(cr, uid, ids, context)
    if len(xmls):
        return 'gen'
    else:
        return 'no_files'

def _gen(self, cr, uid, data, context={}):
    fpd_obj = pooler.get_pool(cr.dbname).get('giscedata.liquidacio.fpd')
    ids = fpd_obj.search(cr, uid, [('state', '=', 'para_liquidar'), ('inici', '<', 'now()'), ('final', '>=', 'now()')])
    xmls = fpd_obj.xml(cr, uid, ids, context)

    filename = 'FPD_%s%s.zip' % (fpd_obj.browse(cr, uid, ids[0]).year, fpd_obj.browse(cr, uid, ids[0]).month)

    zfile = zipfile.ZipFile('/tmp/%s' % filename, 'w')
    for file in xmls.keys():
        zfile.writestr(file, xmls[file])
    zfile.close()

    f = open('/tmp/%s' % filename, 'r')
    f64 = base64.b64encode(f.read())
    f.close()
    os.unlink('/tmp/%s' % filename)



    return {'file': f64, 'name': filename}


_gen_form = """<?xml version="1.0"?>
<form string="Generar ficheros">
  <field name="file" />
  <field name="name" invisible="1" />
</form>"""


_gen_fields = {
  'file': {'string': 'Fichero Zip', 'type': 'binary', 'readonly': True},
  'name': {'type': 'char', 'size': 50}
}

_no_files_form = """<?xml version="1.0"?>
<form string="Generar ficheros">
  <label string="No hay ficheros que generar." />
</form>"""

_no_files_fields = {}

class wizard_liquidacio_generar_xml_fpd(wizard.interface):

    states = {
      'init': {
          'actions': [],
        'result': {'type': 'choice', 'next_state': _init}
      },
      'gen': {
        'actions': [_gen],
        'result': {'type': 'form', 'arch': _gen_form,'fields': _gen_fields, 'state':[('end', 'Cerrar', 'gtk-close')]}
      },
      'no_files': {
        'actions': [],
        'result': {'type': 'form', 'arch': _no_files_form, 'fields': _no_files_fields, 'state': [('end', 'Cerrar', 'gtk-close')]}
      }
    }

wizard_liquidacio_generar_xml_fpd('giscedata.liquidacio.dashboard.fpd.generar_xml')
