# -*- coding: utf-8 -*-
import wizard
import pooler
import time


def _init(self, cr, uid, data, context={}):
    data['partners'] = []
    data['text'] = ''
    fpd_obj = pooler.get_pool(cr.dbname).get('giscedata.liquidacio.fpd')
    ids = fpd_obj.search(cr, uid, [('state', '=', 'borrador'), ('inici', '<', 'now()'), ('final', '>=', 'now()')])
    for fpd in fpd_obj.browse(cr, uid, ids, context):
        if len(fpd.company_id.partner_id.address):
            if fpd.company_id.partner_id.address[0].email:
                data['text'] += '%s: %s <%s>\n' % (fpd.company_id.name, fpd.company_id.partner_id.address[0].name, fpd.company_id.partner_id.address[0].email)
                data['partners'].append(fpd.company_id.partner_id.id)

    return {}

def _check(self, cr, uid, data, context={}):
    if len(data['partners']):
        return 'message'
    else:
        return 'no_partners'


def _message(self, cr, uid, data, context={}):
    val_obj = pooler.get_pool(cr.dbname).get('ir.values')
    subject = val_obj.get(cr, uid, 'meta', 'subject', ['giscedata.liquidacio.fpd'])
    if len(subject):
        subject = subject[0][2]
    body = val_obj.get(cr, uid, 'meta', 'text', ['giscedata.liquidacio.fpd'])
    if len(body):
        body = body[0][2]
    return {'mails_text': data['text'], 'subject': subject, 'text': body}



_message_form = """<?xml version="1.0"?>
<form string="Correo de aviso">
  <label string="Va a generar un correo de aviso para las siguientes personas:" colspan="4"/>
  <field name="mails_text" colspan="4" nolabel="1" readonly="1" />
  <group colspan="4" string="Correo Electrónico">
    <field name="subject" colspan="4" />
    <field name="text" colspan="4" width="400" height="300"/>
  </group>
</form>"""

_message_fields = {
  'mails_text': {'string': 'Mails', 'type': 'text'},
  'subject': {'string': 'Asunto', 'type': 'char', 'size': 255},
  'text': {'string': 'Mensaje', 'type': 'text'}
}

def _send(self, cr, uid, data, context={}):
    val_obj = pooler.get_pool(cr.dbname).get('ir.values')
    val_obj.set(cr, uid, 'meta', 'subject', 'subject', ['giscedata.liquidacio.fpd'], data['form']['subject'])
    val_obj.set(cr, uid, 'meta', 'text', 'text', ['giscedata.liquidacio.fpd'], data['form']['text'])
    partner_obj = pooler.get_pool(cr.dbname).get('res.partner')
    user = pooler.get_pool(cr.dbname).get('res.users').browse(cr, uid, uid)
    if user.address_id and user.address_id.email:
        mail_from = '%s <%s>' % (user.address_id.name, user.address_id.email)
    else:
        mail_from = 'noreply@gisce.net'
    if len(data['partners']):
        partner_obj.email_send(cr, uid, data['partners'], mail_from, data['form']['subject'], data['form']['text'])

    return {}


_end_form = """<?xml version="1.0"?>
<form string="Correo de aviso">
  <label string="Mensajes enviados correctamente." />
</form>"""

_end_fields = {}

_no_partners_form = """<?xml version="1.0"?>
<form string="Correo de aviso">
  <label string="No hay empresas para enviar mensajes o no tienen correo electrónico en su ficha de contacto." />
</form>"""

_no_partners_fields = {}

class wizard_liquidacio_generar_avis_fpd(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
        'result': {'type': 'state', 'state': 'check'},
      },
      'check': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _check},
      },
      'message': {
        'actions': [_message],
        'result': {'type': 'form', 'arch': _message_form,'fields': _message_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('send', 'Continuar', 'gtk-go-forward')]}
      },
      'send': {
        'actions': [_send],
        'result': {'type': 'state', 'state': 'pre_end'}
      },
      'pre_end': {
        'actions': [],
        'result': {'type': 'form', 'arch': _end_form, 'fields': _end_fields, 'state': [('end', 'De acuerdo', 'gtk-ok')]}
      },
      'no_partners': {
        'actions': [],
        'result': {'type': 'form', 'arch': _no_partners_form, 'fields': _no_partners_fields, 'state': [('end', 'Cerrar', 'gtk-close')]}
      },
    }

wizard_liquidacio_generar_avis_fpd('giscedata.liquidacio.dashboard.fpd.generar_avis')
