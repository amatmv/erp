# -*- coding: utf-8 -*-
{
    "name": "Dashboard para las liquidaciones",
    "description": """Dashboard para las liquidaciones""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Facturació",
    "depends":[
        "base",
        "board",
        "giscedata_liquidacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_liquidacio_dashboard_wizard.xml",
        "giscedata_liquidacio_dashboard_view.xml"
    ],
    "active": False,
    "installable": True
}
