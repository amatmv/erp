# -*- coding: utf-8 -*-
{
    "name": "Tipus de vies segons el CNMC",
    "description": """
    Aquest mòdul carrega els tipus de vies i codis de carrer segons la CNMC
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tipovia_cnmc_data.xml"
    ],
    "active": False,
    "installable": True
}