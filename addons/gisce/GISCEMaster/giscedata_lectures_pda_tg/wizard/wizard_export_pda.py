# -*- coding: utf-8 -*-

from osv import osv, fields


class TgGiscedataLecturesPdaExport(osv.osv_memory):

    _name = 'giscedata.lectures.pda.export'
    _inherit = 'giscedata.lectures.pda.export'

    def _get_comptador_actiu(self, cursor, uid, wizard,
                             polissa_id, dia, context=None):
        '''export comptadors tg cnc connected depending on
        export_tg flag'''

        comptador = super(TgGiscedataLecturesPdaExport,
                          self)._get_comptador_actiu(cursor, uid,
                                                     wizard,
                                                     polissa_id,
                                                     dia, context=context)
        if not comptador:
            return comptador
        #A meter without connection has to be exported
        if not comptador.tg_cnc_conn:
            return comptador
        #It has connection, check if we have to export it
        if wizard.exportar_tg:
            return comptador
        else:
            return False

    _columns = {
        'exportar_tg': fields.boolean('Exportar contador de telegestión'),
    }

    _defaults = {
        'exportar_tg': lambda *a: False
    }

TgGiscedataLecturesPdaExport()
