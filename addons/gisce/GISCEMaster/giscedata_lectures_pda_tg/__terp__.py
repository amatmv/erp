# -*- coding: utf-8 -*-
{
    "name": "Exportació de Lectures per PDA (telegestió)",
    "description": """
  * Es pot triar si exportar els comptadors de telegestió ja connectats a un concentrador
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_lectures_telegestio",
        "giscedata_lectures_pda"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_export_pda_view.xml"
    ],
    "active": False,
    "installable": True
}
