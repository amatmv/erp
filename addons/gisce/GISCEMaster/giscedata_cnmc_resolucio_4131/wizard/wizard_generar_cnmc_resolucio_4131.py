# -*- coding: utf-8 -*-
import subprocess
import tempfile
import base64
import os
from tools import config
from StringIO import StringIO
import csv

from osv import osv, fields
import tools
from tools.translate import _
from datetime import date
from zipfile import ZipFile, BadZipfile

from libcnmc.models import F1Res4771, F2Res4771, F3Res4771, F4Res4771
from libcnmc.models import F5Res4771, F6Res4771, F7Res4771, F8Res4771

from libcnmc.models import F1Res4131, F2Res4131, F3Res4131, F4Res4131
from libcnmc.models import F5Res4131, F6Res4131, F7Res4131, F8Res4131

import netsvc


OPCIONS = [
    ('lat', 'LAT CSV (1)'), ('lbt', 'BT CSV (2)'),
    ('sub', 'Subestacions CSV (3)'), ('pos', 'Posicions CSV (4)'),
    ('maq', 'Maquines CSV (5)'), ('con', 'Condensadors CSV(5)'),
    ('desp', 'Despatx CSV (6)'), ('fia', 'Fiabilitat CSV (7)'),
    ('cts', 'CTS CSV (8)')
]


class WizardGenerarCnmcResolucio4131(osv.osv_memory):
    """Wizard per generar els XML de CNMC de inventari"""
    _name = 'wizard.generar.cnmc.resolucio.4131'
    _max_hours = 10
    logger = netsvc.Logger()

    def add_status(self, cursor, uid, ids, status):
        wizard = self.browse(cursor, uid, ids[0], None)
        if wizard.status:
            status = wizard.status + '\n' + status
        wizard.write({'status': status})

    def _load_4771(self, cursor, uid, ids, csv_data, res_file, context=None):
        """
        Private method to load the 4771 report in the database

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param csv_data: csv data of the file
        :param res_file: Report rile name ex f1
        :param context: OpenERP context
        :return: True if its loaded properly
        """
        if res_file == 'f1':
            model = 'giscedata.at.tram'
            cnmc_obj = F1Res4771
        elif res_file == 'f2':
            model = 'giscedata.bt.element'
            cnmc_obj = F2Res4771
        elif res_file == 'f3':
            model = 'giscedata.cts.subestacions'
            cnmc_obj = F3Res4771
        elif res_file == 'f4':
            model = 'giscedata.cts.subestacions.posicio'
            cnmc_obj = F4Res4771
        elif res_file == 'f5':
            model = 'giscedata.transformador.trafo'
            cnmc_obj = F5Res4771
        elif res_file == 'f6':
            model = 'giscedata.despatx'
            cnmc_obj = F6Res4771
        elif res_file == 'f7':
            model = 'giscedata.celles.cella'
            cnmc_obj = F7Res4771
        elif res_file == 'f8':
            model = 'giscedata.cts'
            cnmc_obj = F8Res4771
        model = self.pool.get(model)
        csv_file = StringIO(csv_data)
        reader = csv.reader(csv_file, delimiter=';', quotechar='|')
        for line in reader:
            if line:
                if cnmc_obj in [F1Res4771, F2Res4771] and len(line) == 16:
                    line.append("")
                cnmc = cnmc_obj(*line)
                name = cnmc.ref
                identifier = model.search(
                    cursor, uid, [('name', '=', name)], 0, 0, False,
                    {'active_test': False})
                model.write(cursor, uid, identifier,
                            {'4771_entregada': cnmc.dump()},
                                context=context)
        return True

    def _load_4131(self, cursor, uid, ids, csv_data, res_file, context=None):
        """
        Private method to load the 4131 report in the database

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Affected ids
        :param csv_data: csv data of the file
        :param res_file: Report rile name ex f1
        :param context: OpenERP context
        :return: True if its loaded properly
        """
        if res_file == 'f1':
            model = 'giscedata.at.tram'
            cnmc_obj = F1Res4131
        elif res_file == 'f2':
            model = 'giscedata.bt.element'
            cnmc_obj = F2Res4131
        elif res_file == 'f3':
            model = 'giscedata.cts.subestacions'
            cnmc_obj = F3Res4131
        elif res_file == 'f4':
            model = 'giscedata.cts.subestacions.posicio'
            cnmc_obj = F4Res4131
        elif res_file == 'f5':
            model = 'giscedata.transformador.trafo'
            cnmc_obj = F5Res4131
        elif res_file == 'f6':
            model = 'giscedata.despatx'
            cnmc_obj = F6Res4131
        elif res_file == 'f7':
            model = 'giscedata.celles.cella'
            cnmc_obj = F7Res4131
        elif res_file == 'f8':
            model = 'giscedata.cts'
            cnmc_obj = F8Res4131
        model = self.pool.get(model)
        csv_file = StringIO(csv_data)
        reader = csv.reader(csv_file, delimiter=';', quotechar='|')
        for line in reader:
            if line:
                if len(line) == 19 and (res_file in ['f1', 'f2']):
                    del line[-2]
                cnmc = cnmc_obj(*line)
                name = cnmc.ref
                try:
                    identifier = model.search(
                        cursor, uid, [('name', '=', name)], 0, 0, False,
                        {'active_test': False})
                except ValueError:
                    continue
                try:
                    model.write(
                        cursor,
                        uid, identifier,
                        {'4131_entregada_2016': cnmc.dump()},
                        context=context)
                except UnicodeDecodeError:
                    self.add_status(cursor, uid, ids,
                                    _("Error de codificacio, els fitxers han d'esta en UTF-8"))
                    return False
        return True

    def load_4771(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        data_zip = base64.decodestring(wizard.file_4771)
        s = StringIO(data_zip)
        input_zip = ZipFile(s)
        try:
            filenames = sorted(input_zip.namelist())
        except BadZipfile:
            self.logger.notifyChannel(
                'load_4771', netsvc.LOG_WARNING,
                _('Fitxer corrupte'))
            self.add_status(cursor, uid, ids, _('Fitxer corrupte'))
        if filenames:
            for filename in filenames:
                csv_data = input_zip.read(filename)
                if filename.split('_')[-1] == '1.txt':
                    self._load_4771(
                        cursor, uid, ids, csv_data, 'f1', context=context)
                elif filename.split('_')[-1] == '2.txt':
                    self._load_4771(
                        cursor, uid, ids, csv_data, 'f2', context=context)
                elif filename.split('_')[-1] == '3.txt':
                    self._load_4771(
                        cursor, uid, ids, csv_data, 'f3', context=context)
                elif filename.split('_')[-1] == '4.txt':
                    self._load_4771(
                        cursor, uid, ids, csv_data, 'f4', context=context)
                elif filename.split('_')[-1] == '5.txt':
                    self._load_4771(
                        cursor, uid, ids, csv_data, 'f5', context=context)
                elif filename.split('_')[-1] == '6.txt':
                    self._load_4771(
                        cursor, uid, ids, csv_data, 'f6', context=context)
                elif filename.split('_')[-1] == '7.txt':
                    self._load_4771(
                        cursor, uid, ids, csv_data, 'f7', context=context)
                elif filename.split('_')[-1] == '8.txt':
                    self._load_4771(
                        cursor, uid, ids, csv_data, 'f8', context=context)
        else:
            self.logger.notifyChannel(
                'load_4771', netsvc.LOG_WARNING,
                _('No hi ha fitxers en el ZIP'))
            self.add_status(cursor, uid, ids, _('No hi ha fitxers en el ZIP'))
            pass

        return True

    def load_4131(self, cursor, uid, ids, context=None):
        """
        Loads the 4131 file into the model

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP context
        :return: Boolean
        """
        wizard = self.browse(cursor, uid, ids[0], context)
        data_zip = base64.decodestring(wizard.file_4131)
        s = StringIO(data_zip)
        input_zip = ZipFile(s)
        try:
            filenames = sorted(input_zip.namelist())
        except BadZipfile:
            self.logger.notifyChannel(
                'load_4131', netsvc.LOG_WARNING,
                _('Fitxer corrupte'))
            self.add_status(cursor, uid, ids, _('Fitxer corrupte'))
            return False
        result = False
        if filenames:
            for filename in filenames:
                csv_data = input_zip.read(filename)
                if filename.split('_')[-1] == '1.txt':
                    result = self._load_4131(
                        cursor, uid, ids, csv_data, 'f1', context=context)
                    continue
                elif filename.split('_')[-1] == '2.txt':
                    self._load_4131(
                        cursor, uid, ids, csv_data, 'f2', context=context)
                elif filename.split('_')[-1] == '3.txt':
                    self._load_4131(
                        cursor, uid, ids, csv_data, 'f3', context=context)
                elif filename.split('_')[-1] == '4.txt':
                    self._load_4131(
                        cursor, uid, ids, csv_data, 'f4', context=context)
                elif filename.split('_')[-1] == '5.txt':
                    self._load_4131(
                        cursor, uid, ids, csv_data, 'f5', context=context)
                elif filename.split('_')[-1] == '6.txt':
                    self._load_4131(
                        cursor, uid, ids, csv_data, 'f6', context=context)
                elif filename.split('_')[-1] == '7.txt':
                    self._load_4131(
                        cursor, uid, ids, csv_data, 'f7', context=context)
                elif filename.split('_')[-1] == '8.txt':
                    self._load_4131(
                        cursor, uid, ids, csv_data, 'f8', context=context)
        else:
            self.logger.notifyChannel(
                'load_4131', netsvc.LOG_WARNING,
                _('No hi ha fitxers en el ZIP'))
            self.add_status(cursor, uid, ids, _('No hi ha fitxers en el ZIP'))
            pass

        if not result:
            self.add_status(cursor, uid, ids,
                            _("No s'han carregat correctament"))
        else:
            self.add_status(cursor, uid, ids,
                            _("S'han carregat correctament"))
        return True


    def action_exec_script(self, cursor, uid, ids, context=None):
        """
        Llença l'script per l'informe de l'inventari.
        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP cursor
        :return: None
        """
        script_name = {'lat': ['res_4131_lat', 'LAT CSV'],
                       'lbt': ['res_4131_lbt', 'BT CSV'],
                       'sub': ['res_4131_sub', 'Subestacions CSV'],
                       'pos': ['res_4131_pos', 'Posicions CSV'],
                       'maq': ['res_4131_maq', 'Maquina CSV'],
                       'desp': ['res_4131_des', 'Despatx CSV'],
                       'fia': ['res_4131_fia', 'Fiabilitat CSV'],
                       'cts': ['res_4131_cts', 'CTS CSV'],
                       'con': ['res_4131_con', 'Condensadors']}

        wizard = self.browse(cursor, uid, ids[0], context)

        # Agafar l'script que seleccionem al tipus
        exe = script_name.get(wizard.tipus)[0]

        # Crido la llibreria de CNMC
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        port = tools.config['port'] or 8069
        filename = tempfile.mkstemp()[1]
        args = ['cnmc', exe, '-d', cursor.dbname, '-p', port, '-u', user.login,
                '-w', user.password, '--no-interactive', '-o', filename,
                '-c', wizard.r1, '-y', wizard.anyo]

        # Afegir la crida al log
        logger = netsvc.Logger()
        logger.notifyChannel(
            'server', netsvc.LOG_INFO,
            'libcnmc executed: {}'.format(' '.join(map(str, args)))
        )

        # Carrega el fitxer CSV
        env = os.environ.copy()
        sentry_dsn = config.get('sentry_dsn')
        if sentry_dsn:
            env['SENTRY_DSN'] = sentry_dsn
        retcode = subprocess.call(map(str, args), env=env)
        if retcode:
            raise osv.except_osv(
                'Error',
                _('El procés de generar el fitxer ha fallat. Codi: %s')
                % retcode
            )

        tmpxmlfile = open(filename, 'r+')
        report = base64.b64encode(tmpxmlfile.read())
        filenom = ''
        if wizard.tipus == 'lat':
            filenom = 'INVENTARIO_R1-%s_1.txt' % wizard.r1
        elif wizard.tipus == 'lbt':
            filenom = 'INVENTARIO_R1-%s_2.txt' % wizard.r1
        elif wizard.tipus == 'sub':
            filenom = 'INVENTARIO_R1-%s_3.txt' % wizard.r1
        elif wizard.tipus == 'pos':
            filenom = 'INVENTARIO_R1-%s_4.txt' % wizard.r1
        elif wizard.tipus == 'maq':
            filenom = 'INVENTARIO_R1-%s_5.txt' % wizard.r1
        elif wizard.tipus == 'desp':
            filenom = 'INVENTARIO_R1-%s_6.txt' % wizard.r1
        elif wizard.tipus == 'fia':
            filenom = 'INVENTARIO_R1-%s_7.txt' % wizard.r1
        elif wizard.tipus == 'cts':
            filenom = 'INVENTARIO_R1-%s_8.txt' % wizard.r1
        elif wizard.tipus == 'con':
            filenom = 'INVENTARIO_R1-%s_9.txt' % wizard.r1

        wizard.write({'name': filenom, 'file': report, 'state': 'done'})
        os.unlink(filename)

    def action_another(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)

        wizard.write({'state': 'init'})

    def _default_r1(self, cursor, uid, context=None):
        """
            Gets R1 code from company if defined

        :param cursor: Database cursor
        :param uid: User id
        :param context: OpenERP Context
        :return: R1 code as string or empty string
        """
        if not context:
            context = {}

        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid, context=context)

        r1 = user.company_id.partner_id.ref2

        if r1 and r1.startswith('R1-'):
            r1 = r1[3:]

        return r1 or ''

    _columns = {
        'state': fields.selection([('init', 'Init'),
                                   ('done', 'Done'), ],
                                  'State'),
        'name': fields.char('File name', size=64),
        'file': fields.binary('Fitxer generat'),
        'r1': fields.char('Codi R1', size=12,
                          help=u'Agafa automàticament el camp ref2 de la '
                               u'Empresa configurada a la Companyia'),
        'tipus': fields.selection(OPCIONS, 'Tipus', required=True),
        'anyo': fields.integer('Any', size=4),
        'file_4771': fields.binary('Fitxer 4771'),
        'file_4131': fields.binary('Fitxer 4131'),
        'csv_lat': fields.binary('Fitxer LAT'),
        'csv_lbt': fields.binary('Fitxer BT'),
        'csv_sub': fields.binary('Fitxer Subestacions'),
        'csv_pos': fields.binary('Fitxer Posicions'),
        'csv_maq': fields.binary('Fitxer Màquines'),
        'csv_desp': fields.binary('Fitxer Despatx'),
        'csv_fia': fields.binary('Fitxer Fiabilitat'),
        'csv_trans': fields.binary('Fitxer CTS'),
        'status': fields.text(_('Resultat'))
    }

    _defaults = {
        'state': lambda *a: 'init',
        'tipus': lambda *a: 'lat',
        'anyo': lambda *a: date.today().year - 1,
        'r1': _default_r1,
    }


WizardGenerarCnmcResolucio4131()
