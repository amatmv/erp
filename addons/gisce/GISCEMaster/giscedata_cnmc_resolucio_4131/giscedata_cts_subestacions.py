# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCtsSubestacionsPosicio(osv.osv):
    _name = 'giscedata.cts.subestacions.posicio'
    _inherit = 'giscedata.cts.subestacions.posicio'

    def get_codi_instalacio_xmlrpc(self, cursor, uid, ids, context=None):
        res = self.get_codi_instalacio(cursor, uid, ids, context)

        return [(str(k), v) for k, v in res.items()]

    def get_codi_instalacio(self, cursor, uid, ids, context=None):
        res = {}

        pos_fields = ['tensio', 'tipus_posicio']

        for pos in self.read(cursor, uid, ids, pos_fields):
            tensio_name = pos['tensio'][1]
            tipus_posicio = pos['tipus_posicio']
            codi = 0
            if 1000 <= int(tensio_name) < 36000:
                if tipus_posicio == 'B':
                    codi = 102
                elif tipus_posicio == 'C':
                    codi = 106
            if 36000 <= int(tensio_name) < 110000:
                if tipus_posicio == 'B':
                    codi = 95
                elif tipus_posicio == 'C':
                    codi = 99
            if 110000 <= int(tensio_name) < 132000:
                if tipus_posicio == 'B':
                    codi = 88
                elif tipus_posicio == 'C':
                    codi = 92

            res[pos['id']] = 'TI-%s' % ('%s' % codi).zfill(3)

        return res

GiscedataCtsSubestacionsPosicio()
