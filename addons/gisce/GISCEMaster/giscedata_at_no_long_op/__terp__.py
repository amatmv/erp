# -*- coding: utf-8 -*-
{
    "name": "Desactivar la longitud Operador als Trams At",
    "description": """Desactiva el camp longitud op""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_et_name_view.xml"
    ],
    "active": False,
    "installable": True
}
