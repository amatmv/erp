# -*- coding: utf-8 -*-

from osv import osv, fields
from types import *
from datetime import *
from psycopg2.extensions import AsIs

import pooler


class giscedata_cts(osv.osv):
    _name = "giscedata.cts"
    _inherit = "giscedata.cts"
    _columns = {
                  'revisions_bt': fields.one2many('giscedata.revisions.bt.revisio', 'name', 'Revisions BT'),
          }


giscedata_cts()


class GiscedataRevisionsBtReglaments(osv.osv):
    _name = 'giscedata.revisions.bt.reglaments'

    _columns = {
        'name': fields.char('Nom del reglament', size=50),
        'data_ini': fields.date('Data inici', required=True),
        'data_fi': fields.date('Data final'),
        'active': fields.boolean('Actiu')
    }


GiscedataRevisionsBtReglaments()


class giscedata_revisions_bt_revisio(osv.osv):

    def assignar(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            if not revisio.data or not revisio.tecnic.id:
                raise osv.except_osv('Error !', 'Has d\'assignar un tècnic i una data.')
                return False
            else:
                self.write(cr, uid, [revisio.id], {'state': 'assignat'})
                return True

    def desassignar(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            self.write(cr, uid, [revisio.id], {'state': 'sense assignar'})
        return True

    def tancar(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            self.write(cr, uid, [revisio.id], {'state': 'tancada'})
        return True

    def reobrir(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            self.write(cr, uid, [revisio.id], {'state': 'reoberta'})
        return True

    def _data_comprovacio(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for id in ids:
            cr.execute("SELECT data_comprovacio FROM giscedata_revisions_bt_defectes \
              WHERE revisio_id = %s and data_comprovacio is not null ORDER BY data_comprovacio DESC \
              LIMIT 1", (id,))
            data = cr.fetchone()
            if data and len(data):
                res[id] = data[0]
            else:
                res[id] = ''
        return res

    def _nom_ct(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            res[defecte.id] = defecte.name.descripcio
        return res

    def _nom_ct_search(self, cr, uid, obj, name, args, context=None):
        if not len(args):
            return []
        else:
            s_ids = self.pool.get('giscedata.cts').search(cr, uid,\
              [('descripcio', args[0][1], args[0][2])])
            if not len(s_ids):
                return [('id','=','0')]
            else:
                ids = []
                for ct in self.pool.get('giscedata.cts').browse(cr, uid, s_ids):
                    for r in ct.revisions_bt:
                        ids.append(r.id)
                return [('id', 'in', ids)]

    def _tots_reparats(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for revisio in self.browse(cr, uid, ids):
            tots_reparats = []
            ind_reparats = []
            if len(revisio.defectes_ids):
                for defecte in revisio.defectes_ids:
                    if defecte.reparat:
                        tots_reparats.append(True)
                        if not defecte.intern:
                            ind_reparats.append(True)
                    else:
                        tots_reparats.append(False)
                        if not defecte.intern:
                            ind_reparats.append(False)

                res[revisio.id] = {'tots_reparats': not False in tots_reparats,
                                   'num_tots_reparats': '%s/%s' % \
                                         (tots_reparats.count(True),
                                          len(tots_reparats)),
                                   'ind_reparats': not False in ind_reparats,
                                   'num_ind_reparats':  '%s/%s' %\
                                         (ind_reparats.count(True),
                                          len(ind_reparats)),
                                  }
            else:
                res[revisio.id] = {'tots_reparats': True,
                                   'num_tots_reparats': '0/0',
                                   'ind_reparats': True,
                                   'num_ind_reparats': '0/0',
                                  }
        return res

    def _tots_reparats_search(self, cr, uid, obj, name, args, context=None):
        #TODO: Substituir per una query SQL potent
        if not len(args):
            return []
        else:
            res = []
            for revisio in self.browse(cr, uid, self.search(cr, uid, [('state','=','tancada')])):
                if revisio.tots_reparats == bool(args[0][2]):
                    res.append(revisio.id)
            if not len(res):
                return [('id','=','0')]
            return [('id','in',res)]

    def _ind_reparats_search(self, cr, uid, obj, name, args, context=None):
        #TODO: Substituir per una query SQL potent
        if not len(args):
            return []
        else:
            res = []
            for revisio in self.browse(cr, uid, self.search(cr, uid, [('state','=','tancada')])):
                if revisio.ind_reparats == bool(args[0][2]):
                    res.append(revisio.id)
            if not len(res):
                return [('id','=','0')]
            return [('id','in',res)]

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for revisio in self.browse(cr, uid, ids, context):
            name = '%s - %s' % (revisio.name.name, revisio.trimestre.name)
            res.append((revisio.id, name))
        return res

    def name_search(self, cursor, uid, name='', args=None, operator='ilike',
                    context=None, limit=80):
        """
        Overwritte the name_search function to find revisions by the name
        composed on the name_get, in this case "ct_name - rev_trimestre"
        :param cursor: DB Cursor
        :type cursor: DB Cursos
        :param uid: User ID
        :type uid: int
        :param name: Search filter keyword
        :type name: str
        :param args: Not Used
        :param operator: Not Used
        :param context: OpenERP context
        :type context: dic[str,Any]
        :param limit: Search limit
        :type limit: int
        :return: list of tuples made of revisions ids and revisions names
        :rtype list of (int,str)
        """
        if not context:
            context = {}
        query_params = {
            'filter': '%'+name+'%',
            'limit': limit
        }
        query = """
        SELECT rev.id AS rev_id 
        FROM  giscedata_revisions_bt_revisio rev
        LEFT JOIN giscedata_trieni AS tri ON rev.trimestre = tri.id
        LEFT JOIN giscedata_cts AS cts ON rev.name = cts.id
        WHERE (cts.name || ' - ' || tri.name) ILIKE %(filter)s
        LIMIT %(limit)s
        """
        cursor.execute(query, query_params)

        ids = [x[0] for x in cursor.fetchall()]

        return self.name_get(cursor, uid, ids)

    def create(self, cursor, uid, vals, context=None):
        if not context:
            context = {}
        id_ct = vals.get('name', False)
        if id_ct:
            cts_obj = self.pool.get('giscedata.cts')
            data_marxa_lat = cts_obj.read(
                cursor, uid, id_ct, ['data_pm'])['data_pm']
            if data_marxa_lat:
                regl_obj = self.pool.get('giscedata.revisions.bt.reglaments')
                regl_ids = regl_obj.search(
                    cursor, uid, [
                        ('data_ini', '<=', data_marxa_lat),
                        '|',
                        ('data_fi', '=', False),
                        ('data_fi', '>=', data_marxa_lat)
                    ], context={'active_test': False}
                )
                vals['reglaments'] = [(6, False, regl_ids)]

        res_id = super(giscedata_revisions_bt_revisio,
                       self).create(cursor, uid, vals, context)
        return res_id

    _name = "giscedata.revisions.bt.revisio"
    _description = "Revisió BT"

    _columns = {
        'name': fields.many2one(
            'giscedata.cts', 'CT', required=True, readonly=False,
            states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'nom_ct': fields.function(
            _nom_ct, fnct_search=_nom_ct_search, method=True,
            type='char', string="Nom del CT"
        ),
        'trimestre': fields.many2one(
            'giscedata.trieni', 'Trimestre', required=True, readonly=False,
            states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'tecnic': fields.many2one(
            'giscedata.revisions.tecnic', 'Tècnic', readonly=False,
            states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'operari': fields.many2one(
            'res.partner.address', 'Operari', readonly=False,
            states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'data': fields.datetime(
            'Data Inici', readonly=False,
            states={
                'tancada': [('readonly', True)]
            }
        ),
        'data_fi': fields.datetime(
            'Data Final', readonly=False,
            states={
                'tancada': [('readonly', True)]
            }
        ),
        'reglaments': fields.many2many(
            'giscedata.revisions.bt.reglaments',
            'revisio_reglaments_bt_rel', 'revisio_id', 'reglament_id',
            'Reglaments'),
        'observacions': fields.text('Observacions'),
        'equips_utilitzats': fields.many2many(
            'giscedata.revisions.equips', 'giscedata_revisions_bt_equips',
            'revisio_id', 'equip_id', 'Equips'
        ),
        'data_ca': fields.datetime('Data Certificat Acta'),
        'data_ca2': fields.datetime('Data Certificat Reparació'),
        'mesures': fields.boolean('Mesures'),
        'talar': fields.char('Trimestre de Tala', size=60),
        'trieni': fields.integer('Trieni'),
        'state': fields.char('Estat', size=60, readonly=True),
        'defectes_ids': fields.one2many(
            'giscedata.revisions.bt.defectes', 'revisio_id', 'Defectes',
            states={
                'sense assignar': [('readonly', True)],
                'tancada': [('readonly', True)]
            }
        ),
        'data_comprovacio': fields.function(
            _data_comprovacio, method=True,
            string="Data comprovació reparació de defectes", type="text"
        ),
        'tots_reparats': fields.function(
            _tots_reparats, method=True, type='boolean',
            string='Defectes reparats', fnct_search=_tots_reparats_search,
            multi='defecte'
        ),
        'num_tots_reparats': fields.function(
            _tots_reparats, method=True, type='char',
            string='Reparats/Totals', multi='defecte'
        ),
        'ind_reparats': fields.function(
            _tots_reparats, method=True, type='boolean',
            string='Defectes Indústria reparats',
            fnct_search=_ind_reparats_search, multi='defecte'
        ),
        'num_ind_reparats': fields.function(
            _tots_reparats, method=True, type='char',
            string='Reparats/Totals Indústria', multi='defecte'
        ),
    }

    _defaults = {
      'state': lambda *a: 'sense assignar',
    }

    _order = "name, id"

    _sql_constraints = [('ct_trim', 'unique (name, trimestre)', 'No es pot revisar una ct més d\'una vegada al mateix trimestre.')]


giscedata_revisions_bt_revisio()


class giscedata_revisions_bt_tipusvaloracio(osv.osv):

    _name = "giscedata.revisions.bt.tipusvaloracio"
    _description = "Tipus de valoració"

    _columns = {
      'name': fields.char('Valoració', size=60),
      'active': fields.boolean('Active'),
      't_qty': fields.integer('Temps'),
      't_type': fields.selection([('day', 'Dia'),('month', 'Mes'), ('year', 'Any')], 'Unitat')
    }

    _defaults = {
      'active': lambda *a: True,
      't_type': lambda *a: 'day',
    }


giscedata_revisions_bt_tipusvaloracio()


class giscedata_revisions_bt_normativa(osv.osv):

    _name = "giscedata.revisions.bt.normativa"
    _description = "Normativa"

    _columns = {
      'name': fields.char('Nom de la normativa', size=255),
      'inici': fields.date('Inici'),
      'final': fields.date('Final'),
      'vigent': fields.boolean('Vigent'),
    }

    _defaults = {

    }

    _order = "name, id"


giscedata_revisions_bt_normativa()


class giscedata_revisions_bt_tipusdefectes(osv.osv):

    _name = "giscedata.revisions.bt.tipusdefectes"
    _description = "Tipus de defecte i a quin tipus de lat s'aplica"

    _columns = {
      'name': fields.char('Codi', size=25),
      'descripcio': fields.text('Descripció'),
      'normativa': fields.many2one('giscedata.revisions.bt.normativa', 'Normativa'),
      'intern': fields.boolean('Intern'),
    }

    _defaults = {

    }

    _order = "name, name"


giscedata_revisions_bt_tipusdefectes()


class giscedata_revisions_bt_defectes(osv.osv):
    def name_search(self, cr, user, name='', args=None, operator='ilike',
                    context=None, limit=80):
        """
        Search the id of the tipusdefecte bt by it's name
        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param name: Value to search
        :type name: str
        :param args: Unused
        :param operator: Unused
        :param context: Unused
        :param limit: Unused
        :return: Return the ids of the matching elements
        :rtype: list of (int, bool)
        """

        def_ids = self.search(
            cr, user, [('defecte_id.name', 'ilike', name)]
        )
        return self.name_get(cr, user, def_ids, context)

    def onchange_defecte_desc(self, cr, uid, ids, defecte_id):
        res = {}
        if defecte_id:
            td = self.pool.get('giscedata.revisions.bt.tipusdefectes').browse(cr, uid, defecte_id)
            return {'value':{'defecte_desc': td.descripcio, 'intern': td.intern}}
        else:
            return {'value':{'defecte_desc': ''}}


    def _defecte_desc(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            res[defecte.id] = defecte.defecte_id.descripcio
        return res

    def _data_limit_reparacio(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.valoracio:
                query = """
                    SELECT r.data + INTERVAL %s AS data_limit_reparacio
                    FROM giscedata_revisions_bt_revisio r,
                      giscedata_revisions_bt_defectes d
                    WHERE r.id = d.revisio_id
                    AND d.id = %s
                """
                cr.execute(query, (
                    str(defecte.valoracio.t_qty) + ' ' +
                    defecte.valoracio.t_type,
                    int(defecte.id)
                ))
                res[defecte.id] = cr.fetchone()[0]
            else:
                res[defecte.id] = ''
        return res

    def _intern(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.defecte_id:
                res[defecte.id] = defecte.defecte_id.intern
            else:
                res[defecte.id] = 0
        return res

    def _search_by_data_lim(self, cr, uid, obj, name, args, context=None):
        """
        Function used to search by the function field 'data_limit_reparacio'
        :param cr: Database cursor
        :param uid: Not used
        :param obj: Not used
        :param name: Not used
        :param args: Search filter parameters
        :type args: [(str, str, any)]
        :param context: Not used
        :return: Ids search filter
        :rtype [(str, str, [int])]
        """

        search_date = args[0][2]
        search_operator = args[0][1]
        query = """
            SELECT d.id AS id_defecte
            FROM giscedata_revisions_bt_revisio r, 
                 giscedata_revisions_bt_defectes d,
                 giscedata_revisions_bt_tipusvaloracio v
            WHERE r.id = d.revisio_id
            AND d.valoracio = v.id
            AND (r.data + (v.t_qty::text || ' ' || v.t_type)::interval) %s %s
        """
        cr.execute(query, (AsIs(search_operator), search_date))
        ids_res = cr.fetchall()
        return [('id', 'in', ids_res)]

    _name = "giscedata.revisions.bt.defectes"
    _description = "Defectes BT"

    _columns = {
      'id': fields.integer('Id', readonly=True),
      'name': fields.char('Codi', size=60),
      'linia_bt': fields.many2one('giscedata.bt.element', 'Línia BT'),
      'defecte_id': fields.many2one(
          'giscedata.revisions.bt.tipusdefectes',
          'Codi tipus defecte'
      ),
      'defecte_desc': fields.function(
          _defecte_desc,
          method=True,
          string="Descripció tipus defecte",
          type="text"
      ),
      'data_limit_reparacio': fields.function(
          _data_limit_reparacio,
          method=True,
          string="Data límit de reparació",
          type="datetime",
          fnct_search=_search_by_data_lim
      ),
      'data_reparacio': fields.datetime('Data de reparació'),
      'valoracio': fields.many2one(
          'giscedata.revisions.bt.tipusvaloracio',
          'Valoració',
          required=True
      ),
      'observacions': fields.text('Observacions'),
      'reparat': fields.boolean('Reparat'),
      'observacions_reparacio': fields.text('Observacions de Reparació'),
      'data_comprovacio': fields.datetime('Data de comprovació'),
      'revisio_id': fields.many2one(
          'giscedata.revisions.bt.revisio',
          'Revisió'
      ),
      'intern': fields.function(
          _intern,
          method=True,
          string="Intern",
          type="boolean"
      ),
    }

    _defaults = {

    }

    _order = "name, id"


giscedata_revisions_bt_defectes()


class giscedata_cts(osv.osv):

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def _seccions_bt_string(self, cr, uid, ids, field_name, args, context={}):
        res = {}
        for id in ids:
            cr.execute(
                "SELECT distinct c.name "
                "FROM giscedata_bt_cables c, giscedata_bt_element e "
                "WHERE c.seccio > 0 AND e.cable = c.id AND e.ct = %s",
                (int(id),)
            )
            res[id] = ", ".join([a[0] for a in cr.fetchall()])
        return res

    _columns = {
      'seccions_bt_string': fields.function(
          _seccions_bt_string,
          method=True,
          type='text',
          string='Seccions'
      ),
    }


giscedata_cts()

