# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(
            cursor, uid, name, context=context
        )
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'origin_access': name,
        })

webkit_report.WebKitParser(
    'report.giscedata.revisions.bt.portada.bt',
    'giscedata.trieni',
    'giscedata_revisions_bt/report/portada_bt.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParserNumberPages(
    'report.giscedata.revisions.bt.lliurament.certificats.bt',
    'giscedata.trieni',
    'giscedata_revisions_bt/report/lliurament_certificats_bt.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.reco_bt',
    'giscedata.revisions.bt.revisio',
    'giscedata_revisions_bt/report/reco_bt_report.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.bt.inspeccio.bt',
    'giscedata.revisions.bt.revisio',
    'giscedata_revisions_bt/report/inspeccio_bt.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.reconeixements.trieni.bt',
    'giscedata.trieni',
    'giscedata_revisions_bt/report/reconeixements_bt.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.reconeixements.revisions.bt',
    'giscedata.revisions.bt.revisio',
    'giscedata_revisions_bt/report/reconeixements_bt.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParserNumberPages(
    'report.giscedata.revisions.bt.reconeixement.certificats.bt',
    'giscedata.revisions.bt.revisio',
    'giscedata_revisions_bt/report/reconeixement_certificats_bt.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.full.reparacio_bt',
    'giscedata.revisions.bt.revisio',
    'giscedata_revisions/report/full_reparacio_bt.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.full.reparacio_bt_intern',
    'giscedata.revisions.bt.revisio',
    'giscedata_revisions/report/full_reparacio_bt.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.full.reparacio_bt_industria',
    'giscedata.revisions.bt.revisio',
    'giscedata_revisions/report/full_reparacio_bt.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.full.reparacio_bt_defectes',
    'giscedata.revisions.bt.defectes',
    'giscedata_revisions/report/full_reparacio_bt_defectes.mako',
    parser=report_webkit_html
)
