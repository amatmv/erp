## coding=utf-8
<%
    from datetime import datetime,date
    from dateutil.relativedelta import relativedelta

    def obtenir_trimestres(objectes):
        if "trieni" not in origin_access:
            return filtrar_per_trimestres(objectes)
        return objectes

    def filtrar_per_trimestres(objectes):
        dic_trimestres = {}
        for rev in objectes:
            if not rev.trimestre.name in dic_trimestres.keys():
                dic_trimestres.update({rev.trimestre.name : [rev]})
            else:
                dic_trimestres[rev.trimestre.name].append(rev)
        return dic_trimestres

    def definir_tipus_ct(rev_bt):
        if rev_bt.name.id_installacio_name == "CT":
            if rev_bt.name.id_subtipus.tipus_id.code == 1:
                return "PT"
            elif rev_bt.name.id_subtipus.tipus_id.code == 2:
                return "ET"
            else:
                return "Desconegut"
        else:
            return rev_bt.name.id_installacio_name
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
    ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_revisions_bt/report/stylesheet_reconeixements_bt.css"/>
</head>
<body>
    <%
        llista_trimestres = obtenir_trimestres(objects)
        if "trieni" not in origin_access:
            diccionari_trimestres = llista_trimestres
            llista_trimestres = sorted(llista_trimestres.keys())
    %>
    %for trieni in llista_trimestres:
        <div id="logo">
              <img id="logo_img" src="${addons_path}/giscedata_revisions/report/gisce_logo.png">
        </div>
        <div id="separador"></div>
        <div id="capcalera1">
            <h1>${_("RECONEIXEMENTS LBT")}</h1>
            <h2>${company.name}</h2>
        </div>
        <br><br><br><br>
        %if "trieni" not in origin_access:
            <h2 style="margin-top: -15px">${_("Trimestre")} ${trieni}</h2>
        %else:
            <h2 style="margin-top: -15px">${_("Trimestre")} ${trieni.name}</h2>
        %endif

        <div id="separador2"></div>
        <div class="llista_info">
            <p>${_("LBT dels Centres Transformadors:")}</p>
            <table class="table-intercoloured-rows">
                <colgroup>
                    <col class="col-codi"/>
                    <col class="col-desc"/>
                    <col class="col-type"/>
                    <col class="col-data"/>
                    <col class="sub-col"/>
                    <col class="sub-col"/>
                    <col class="sub-col"/>
                    <col class="sub-col"/>
                    <col class="sub-col"/>
                    <col class="sub-col"/>
                    <col class="col-obse joder"/>
                </colgroup>
                <thead>
                    <tr>
                        <th rowspan="2" class="center no-bold">${_("Codi")}</th>
                        <th rowspan="2" class="center no-bold">${_("Descripció")}</th>
                        <th rowspan="2" class="no-bold">${_("Tipus")}</th>
                        <th rowspan="2" class="no-bold">${_("Data rev.")}</th>
                        <th colspan="3" class="center no-bold">${_("Real (m)")}</th>
                        <th colspan="3" class="center no-bold">${_("Factura (m)")}</th>
                        <th rowspan="2" class="center no-bold">${_("Observacions")}</th>
                    </tr>
                    <tr>
                        <th class="center no-bold">AP</th>
                        <th class="center no-bold">AF</th>
                        <th class="center no-bold">S</th>
                        <th class="center no-bold">AP</th>
                        <th class="center no-bold">AF</th>
                        <th class="center no-bold">S</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        if "trieni" not in origin_access:
                            lbt = diccionari_trimestres.get(trieni)
                        else:
                            lbt = trieni.lbt
                        lbt = sorted(lbt, key=lambda elem: elem.name.name)
                        revisions = {}
                        lenght_summary = {
                            'AP': {'planned': 0, 'reviewed': 0},
                            'AF': {'planned': 0, 'reviewed': 0},
                            'S': {'planned': 0, 'reviewed': 0},
                        }
                        distancia_total = 0
                        sum_feat = 0
                        sum_feaf = 0
                        sum_feas = 0
                        EMPTY_SYMBOL = "-"
                        G_ZERO = ">0"
                        LBT_TYPES = ['AP', 'AF', 'S']
                        lenght_summary_map = {
                            'AP': 'LBT aèria tensada sobre postes',
                            'AF': 'LBT aèria recolzada sobre façana',
                            'S': 'LBT subterrània'
                        }
                    %>
                    %for rev_bt in lbt:
                        <%
                            tipus_CT = definir_tipus_ct(rev_bt)
                            cfo = None
                            revisio = revisions.get(tipus_CT, False)
                            reviewed = bool(rev_bt.data)
                            if not revisio:
                                revisions[tipus_CT] = {'planned': 1, 'reviewed': int(reviewed)}
                            else:
                                revisio['planned'] += 1
                                if reviewed:
                                    revisio['reviewed'] += 1

                            data_rev = ""
                            if rev_bt.data:
                                data_revisio = datetime.strptime(rev_bt.data, '%Y-%m-%d %H:%M:%S')
                                data_rev = data_revisio.strftime('%d/%m/%Y')

                            ct = rev_bt.name

                            real = {
                                "AP": ct.get_longitud_bt_aeria_tensat_sobre_postes()[ct.id],
                                "AF": ct.get_longitud_bt_aeria_sobre_facana()[ct.id],
                                "S": ct.longitud_bt_subt
                            }
                            factura = dict.fromkeys(["AP", "AF", "S"], 0)

                            suml = ct.longitud_bt
                            per_sota = 0 < suml <= 1000

                            if per_sota:
                                longitud_bt = 1
                                factura["AP"] = 1000 * real["AP"] / suml
                                factura["AF"] = 1000 * real["AF"] / suml
                                factura["S"] = 1000 * real["S"] / suml
                            else:
                                factura["AP"] = real["AP"]
                                factura["AF"] = real["AF"]
                                factura["S"] = real["S"]
                                longitud_bt = ct.longitud_bt_km

                            distancia_total += longitud_bt

                            sum_feat += factura["AP"]
                            sum_feaf += factura["AF"]
                            sum_feas += factura["S"]

                            for lbt_type in LBT_TYPES:
                                row = lenght_summary[lbt_type]
                                val = factura[lbt_type]
                                row['planned'] += val
                                if reviewed and lbt_type != 'S':
                                    row['reviewed'] += val

                            for key, value in real.items():
                                if value == 0:
                                    real[key] = EMPTY_SYMBOL
                                elif value < 1:
                                    real[key] = G_ZERO
                                else:
                                    real[key] = int(round(value))

                            for key, value in factura.items():
                                if value == 0:
                                    factura[key] = EMPTY_SYMBOL
                                elif value < 1:
                                    factura[key] = G_ZERO
                                else:
                                    factura[key] = int(round(value))
                        %>
                        <tr>
                            <td>${ct.name}</td>
                            <td>${ct.descripcio}</td>
                            <td>${tipus_CT}</td>
                            <td>${data_rev}</td>
                            <td class="right">${real["AP"]}</td>
                            <td class="right">${real["AF"]}</td>
                            <td class="right">${real["S"]}</td>
                            <td class="right">${factura["AP"]}</td>
                            <td class="right">${factura["AF"]}</td>
                            <td class="right">${factura["S"]}</td>
                            <td class="right">${longitud_bt} km</td>
                        </tr>
                    %endfor
                    <tr>
                        <td class="right bold" colspan="7"><div class="h40px va-center">${_("Totals:")}</div></td>
                        <td class="right bold">${int(round(sum_feat))}</td>
                        <td class="right bold">${int(round(sum_feaf))}</td>
                        <td class="right bold">${int(round(sum_feas))}</td>
                        <td class="right bold">${distancia_total} km</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="div-1st-summary" class="llista_info summary">
            <p>${_("Resum instal·lacions revisades:")}</p>
            <table class="table-intercoloured-rows">
                <thead>
                    <tr>
                        <th class="center no-bold">${_("Instal·lació")}</th>
                        <th class="center no-bold">${_("Previstes")}</th>
                        <th class="center no-bold">${_("Revisades")}</th>
                    </tr>
                </thead>
                <tbody>
                    <% total_previstes = 0 %>
                    %for k, v in revisions.items():
                        <%
                            total_previstes += v['planned']
                        %>
                        <tr>
                            <td class="center">${k}</td>
                            <td class="center">${v['planned']}</td>
                            <td class="center">${v['reviewed']}</td>
                        </tr>
                    %endfor
                    <tr class="h40px">
                        <td class="bold center">${_("Total")}</td>
                        <td class="bold center">${total_previstes}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="div-2nd-summary" class="llista_info summary">
            <table class="table-intercoloured-rows">
                <colgroup>
                    <col id="col-inst">
                    <col id="col-prev">
                    <col id="col-revi">
                </colgroup>
                <thead>
                    <tr>
                        <th class="no-bold center">${_("Instal·lació")}</th>
                        <th class="no-bold center">${_("Previstes")} (km)</th>
                        <th class="no-bold center">${_("Revisades")} (km)</th>
                    </tr>
                </thead>
                <tbody>
                    %for row in LBT_TYPES:
                        <tr>
                            <td>${_(lenght_summary_map[row])}</td>
                            %for col in ['planned', 'reviewed']:
                                <td class="right">${int(round(lenght_summary[row][col]))}</td>
                            %endfor
                        </tr>
                    %endfor
                </tbody>
            </table>
        </div>
        %if trieni != llista_trimestres[-1]:
            <p style="page-break-after:always;"></p>
        %endif
    %endfor
</body>
</html>
