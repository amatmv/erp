## -*- coding: utf-8 -*-
<%
    from datetime import datetime
    flag_ind= False
    flag_int= False
    companyia = company.name

    if 'reparacio_at_industria' in origin_access:
        flag_ind= True

    if 'reparacio_at_intern' in origin_access:
        flag_int= True
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/full_reparacio.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/full_reparacio_ct.css"/>
    </head>
    <body>
        <%
            _list_defectes_ct = []
            _list_defectes_new = []
            contador_reports = 0
            ct_actual= objects[0].name.name
            flag_salt= False

            for revisio in objects:
                if not revisio.tots_reparats:
                    for defecte in revisio.defectes_ids:
                        if not defecte.reparat:
                            if revisio.data:
                                data_temp = datetime.strptime(revisio.data, '%Y-%m-%d %H:%M:%S')
                                data = data_temp.strftime('%d/%m/%Y')
                            else:
                                data= ''

                            if flag_int:
                                if defecte.intern:
                                    _list_defectes_ct.append((revisio, defecte, data))
                            elif flag_ind:
                                if not defecte.intern:
                                    _list_defectes_ct.append((revisio, defecte, data))
                            else:
                                _list_defectes_ct.append((revisio, defecte, data))

            if _list_defectes_ct:
                _list_defectes_new= sorted(_list_defectes_ct, key= lambda tup: (tup[0].name.name,tup[1].id))
                ct_actual= _list_defectes_new[0][0].name.name
        %>
        <h5 class="center">${_("VERIFICACIÓ PERIÒDICA DE LES INSTAL·LACIONS DE PRODUCCIÓ, TRANSFORMACIÓ, TRANSPORT I DISTRIBUCIÓ D'ENERGIA ELÈCTRICA")}</h5>

        <table id="table-header" class="center">
            <colgroup>
                <col class="col-head-1"/>
                <col class="col-head-2"/>
                <col class="col-head-3"/>
                <col class="col-head-4"/>
            </colgroup>
            <tbody>
                <tr>
                    <td>${_("LLISTAT DE DEFECTES EN LÍNIES BT")}</td>
                    <td class="bold">${_("FULLS DE REPARACIÓ")}</td>
                    <td>${companyia}</td>
                    <td><img style="width: 200px; margin-left: 15px;" src="${addons_path}/giscedata_revisions/report/gisce_thumb.png"/></td>
                </tr>
                <tr>
                    <td class="center f11px" colspan="4">${_("S'han d'omplir els apartats 'Data de reparació', 'Observacions' i 'Operari/s'")}</td>
                </tr>
            </tbody>
        </table>
        <table id="table-data">
            <colgroup>
                <col class="col-codi"/>
                <col class="col-revi"/>
                <col class="col-data"/>
                <col class="col-lini"/>
                <col class="col-ctdc"/>
                <col class="col-desc"/>
                <col class="col-repa"/>
                <col class="col-oper"/>
            </colgroup>
            <thead class="bluegisce">
                <tr class="bluegisce">
                    <th>${_("Codi")}</th>
                    <th>${_("Data")}</th>
                    <th>${_("Revisió")}</th>
                    <th>${_("Línia BT")}</th>
                    <th>${}</th>
                    <th>${_("Descripció")}</th>
                    <th>${_("Data de reparació")}</th>
                    <th>${_("Operari/s")}</th>
                </tr>
            </thead>
            <tbody>
                %for rev, elem, date in _list_defectes_new:
                    <tr>
                        <td colspan="8">
                            <table>
                                <colgroup>
                                    <col class="col-codi"/>
                                    <col class="col-revi"/>
                                    <col class="col-data"/>
                                    <col class="col-lini"/>
                                    <col class="col-ctdc"/>
                                    <col class="col-desc"/>
                                    <col class="col-repa"/>
                                    <col class="col-oper"/>
                                </colgroup>
                                <tbody>
                                    <tr class="h30px">
                                        <%
                                            descr = elem.defecte_id.name if elem.defecte_id.name else ""
                                            if descr:
                                                descr += " {}".format(elem.defecte_desc)
                                        %>
                                        <td class="center bold" rowspan="3">${elem.id}</td>
                                        <td class="center">${rev.trimestre.name}</td>
                                        <td class="center">${date}</td>
                                        <td class="center bold">${rev.name.name}</td>
                                        <td class="bold">${rev.name.descripcio}</td>
                                        <td>${descr}</td>
                                        <td class="bordered-cell"><div class="border" ></div></td>
                                        <td class="bordered-cell" rowspan="3"><div class="border" ></div></td>
                                    </tr>
                                    <tr>
                                        <td class="bordered-cell" colspan="6">
                                            <div class="border gray mh25px" >
                                                <div class="inner-div">
                                                    <h6>${_("OBSERVACIONS VERIFICACIÓ")}</h6>
                                                    %if elem.observacions:
                                                        ${elem.observacions}
                                                    %endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bordered-cell" colspan="6">
                                            <div class="border" >
                                                <div class="inner-div">
                                                    <h6>${_("OBSERVACIONS REPARACIÓ")}</h6>
                                                    <p class="align-right">${rev.name.id_municipi.name}</p>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        <td>
                    </tr>
                %endfor
            </tbody>
        </table>
    </body>
</html>
