<%
    from datetime import datetime
    page_break = 1
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_revisions_bt/report/full_reparacio_bt_defectes.css"/>
    </head>
    <body>
        <div class="doc_title">
            <strong>${_("RECONEIXEMENTS PERIÒDICS DE LES INSTAL·LACIONS DE PRODUCCIÓ,")}</strong><br>
            <strong>${_("TRANSFORMACIÓ, TRANSPORT I DISTRIBUCIÓ D'ENERGIA ELÈCTRICA")}</strong>
        </div>
        <div>
            <div class="haederbox_left">
                <div id="headerbox_left_text">
                    ${_("LLISTAT DE DEFECTES NO")}<br>
                    ${_("REPARATS EN LÍNIES DE BT")}
                </div>
            </div>
            <div class="haederbox_center">
                <p><strong>${_("FULL DE REPARACIÓ")}</strong></p>
            </div>
            <div class="haederbox_right">
                <img id="logo" src="data:image/jpeg;base64,${company.logo}"/>
                <div>${company.name}</div>
            </div>
            <div id="nota">
                ${_("S'han d'omplir els apartats 'Data de reparació', 'Observacions' i 'Operari/s'.")}
            </div>
        </div>
        <table>
            <thead>
                <tr>
                    <th style="width: 8%">${_("Codi")}</th>
                    <th style="width: 8%">${_("Data")}</th>
                    <th style="width: 16%">${_("Instal·lació")}</th>
                    <th style="width: 40%">${_("Descripció")}</th>
                    <th style="width: 15%">${_("Data de reparació")}</th>
                    <th style="width: auto">${_("Operari/s")}</th>
                </tr>
            </thead>
            <tbody>
                %for defecte in objects:
                    %if defecte.revisio_id.data:
                        <%
                            date_temp = datetime.strptime(defecte.revisio_id.data, '%Y-%m-%d %H:%M:%S')
                            date = date_temp.strftime('%d/%m/%Y')
                        %>
                    %else:
                        <%
                            date= _("No disponible")
                        %>
                    %endif
                    %if not defecte.reparat:
                        %if page_break == 0:
            </tbody>
        </table>
        <p style="page-break-after:always"></p>
        <table>
            <thead>
                <tr>
                    <th style="width: 8%">${_("Codi")}</th>
                    <th style="width: 8%">${_("Data")}</th>
                    <th style="width: 16%">${_("Instal·lació")}</th>
                    <th style="width: 40%">${_("Descripció")}</th>
                    <th style="width: 15%">${_("Data de reparació")}</th>
                    <th style="width: auto">${_("Operari/s")}</th>
                </tr>
            </thead>
            <tbody>
                        %endif
                        %if defecte.revisio_id.name:
                            <%
                                revisio = defecte.revisio_id
                                ct = revisio.name
                                municipi_element = ct.id_municipi.name
                                descripcio = "{} {}".format(
                                    ct.name,
                                    ct.descripcio
                                )
                                page_break += 1
                            %>
                        %else:
                            <%
                                descripcio = _("No Disponible")
                                municipi_element = _("<br>")
                                page_break += 1
                            %>
                        %endif
                        <tr class="data_row">
                            <td>
                                <strong>${defecte.id}</strong>
                            </td>
                            <td >
                                ${date}
                            </td>
                            <td>
                                <strong><span class="lletra_petita">${descripcio}</span></strong>
                            </td>
                            <td>
                                <span class="lletra_petita">${defecte.defecte_id.name} ${defecte.defecte_id.descripcio}</span>
                            </td>
                            <td class="bordered_cell">
                            </td>
                            <td class="bordered_cell" rowspan="5">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td colspan="4" rowspan="2" class="bordered_cell">
                                <strong>${_("OBSERVACIONS RECONEIXEMENT")}</strong><br>
                                <div class="observacions_reconeixament">${defecte.observacions or "<br><br>"}</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td colspan="4" rowspan="2" class="bordered_cell">
                                <strong>${_("OBSERVACIONS REPARACIÓ")}</strong><br><br>
                                <div class="municipi"><strong>${municipi_element}</strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    %endif
                    %if page_break >= 4:
                        <%
                            page_break = 0
                        %>
                    %endif
                %endfor
            </tbody>
        </table>
    </body>
</html>
