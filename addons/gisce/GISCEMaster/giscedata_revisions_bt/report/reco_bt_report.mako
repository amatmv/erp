## -*- coding: utf-8 -*-
<%
    def get_num_defecte(nom):
      return int(nom.split('.')[0])

    def conteError(ll_defectes, codi_defecte, linia):
        for i in ll_defectes:
            if i['linia'] == linia and i['codi_defec'] == codi_defecte:
                return True
        return False

    def conteErrorCom(ll_defectes, codi_defecte, linia):
        for i in ll_defectes:
            if i['linia'] == linia and get_num_defecte(i['codi_defec']) == int(codi_defecte):
                return True
        return False

    checkPerPag = 80
    defecPerPag = 40

    tipus_defectes_obj = objects[0].pool.get("giscedata.revisions.bt.tipusdefectes")
    defectes_obj = objects[0].pool.get("giscedata.revisions.bt.defectes")
    linia_bt_obj = objects[0].pool.get("giscedata.bt.element")

    llista_tipus_defectes_aux = []
    for i in tipus_defectes_obj.search(cursor, uid, [('normativa.vigent', '=', True)]):
        llista_tipus_defectes_aux.append(tipus_defectes_obj.read(cursor, uid, i, ['name', 'descripcio']))

    llista_tipus_defectes_aux.sort(key=lambda defecte: (get_num_defecte(defecte['name']), defecte['name'].split('.')[1]))
    llista_tipus_defectes = []

    classes_defectes_castella = ["CONDUCTORS", "AÏLLADORS", "SUPORTS", "DISTÀNCIES", "CREUAMENTS", "PARAL·LELISMES", "TERRES", "ALTRES"]

    defecte_actual = -1
    for defec in llista_tipus_defectes_aux:
        if get_num_defecte(defec['name']) != defecte_actual:
            defecte_actual = get_num_defecte(defec['name'])
            llista_tipus_defectes.append({'name': str(get_num_defecte(defec['name'])), 'descripcio': classes_defectes_castella[get_num_defecte(defec['name'])-1], 'titol': True})
        defec['titol'] = False
        llista_tipus_defectes.append(defec)

    numeroCheck = len(llista_tipus_defectes)
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
            </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_revisions_bt/report/estils_reco_bt.css"/>
    </head>

    <body>
        %for rev in objects:
            <%
                numLinies = len(rev.name.sortidesbt)

                llista_defectes = []
                for id_defec in rev.defectes_ids:
                    defec= defectes_obj.read(cursor, uid, id_defec.id, ['linia_bt', 'defecte_id', 'valoracio', 'observacions'])

                    if defec['linia_bt']:
                        element = linia_bt_obj.read(cursor, uid, defec['linia_bt'][0], ['num_linia', 'name'])
                        linia = ''
                        if element['num_linia']:
                            linia = 'L' + element['num_linia']
                        tram = element['name']
                    else:
                        linia = '-'
                        tram = '-'

                    llista_defectes.append({
                        'linia': linia,
                        'tram': tram,
                        'codi_defec': defec['defecte_id'][1],
                        'id': id_defec.id,
                        'valoracio': defec['valoracio'][0],
                        'observ': defec['observacions']
                    })

                numeroDefec = len(llista_defectes)
            %>

            <script>
                numPag=0;
            </script>

            <% act=0 %>
            %while True:
                <div class="header">
                    <div style="width: 100%;">
                        <img src="${addons_path}/giscedata_revisions_bt/report/gisce_logo.png">

                        <div>
                            13.3-18_05<br>
                            ${_('Guia de Verificació - LBT')}<br>
                            ${_('Revisió')} 5<br>
                            <script>
                                numPag++;
                                document.write('Full ' + numPag + ' de ' + totalPag);
                            </script>
                        </div>
                    </div>

                    <hr>

                    <table>
                        <tr>
                            <th>${_("CODI DE LA INSPECCIÓ")}</th>
                            <td>LBT-${rev.name.name}/${rev.trimestre.name}</td>
                        </tr>
                    </table>
                </div>


                <div class="insp">
                    <table>
                        <tr>
                            <th width="20%">${_('DATA INSPECCIÓ')}</th>
                            <td width="29%">${'{0[2]}/{0[1]}/{0[0]}'.format(rev.data.split(' ')[0].split('-'))}</td>
                            <th width="35%">${_('TOTAL NÚMERO DE LÍNIES REVISADES')}</th>
                            <td width="15%">${numLinies}</td>
                        </tr>
                    </table>
                </div>

                <table class="checks">
                    <thead>
                        <tr>
                            <th width="5%" rowspan="2">${_('Codi Defecte')}</th>
                            <th width="60%" rowspan="2">${_('Defecte')}</th>
                            <th width="35%" colspan="${numLinies}" style="font-size:5px">${_('Identificació número de línia')}</th>
                        </tr>
                        <tr>
                            %for i in range(1,numLinies+1):
                                <th style="width: ${35/numLinies}%">${i}</th>
                            %endfor
                        </tr>
                    </thead>
                    <tbody>
                        %for defec in range(act,min(act + checkPerPag, numeroCheck)):
                            % if llista_tipus_defectes[defec]['titol']:
                                <tr>
                                    <th>${llista_tipus_defectes[defec]['name']}</th>
                                    <th>${llista_tipus_defectes[defec]['descripcio']}</th>
                                    %for i in range(1,numLinies+1):
                                        %if conteErrorCom(llista_defectes, llista_tipus_defectes[defec]['name'], "L{}".format(i)):
                                            <th style="color:red">&#x2717;</th>
                                        %else:
                                            <th>&#x2714;</th>
                                        %endif
                                    %endfor
                                </tr>
                            %else:
                                <tr>
                                    <td>${llista_tipus_defectes[defec]['name']}</td>
                                    <td>${llista_tipus_defectes[defec]['descripcio']}</td>
                                    %for i in range(1, numLinies+1):
                                        %if conteError(llista_defectes, llista_tipus_defectes[defec]['name'], "L{}".format(i)):
                                            <td style="color:red">&#x2717;</td>
                                        %else:
                                            <td></td>
                                        %endif
                                    %endfor
                                </tr>
                            %endif
                        %endfor
                        <%
                            act += checkPerPag
                        %>
                    </tbody>
                </table>

                <%
                    if act >= numeroCheck:
                        break
                    endif
                %>
            %endwhile

            %if numeroDefec>0:
                <% act=0 %>
                %while True:
                    <div class="header">
                        <div style="width: 100%;">
                            <img src="${addons_path}/giscedata_revisions_bt/report/gisce.png">

                            <div>
                                13.3-18_05<br>
                                ${_('Guia de Verificació - LBT')}<br>
                                ${_('Revisió')} 5<br>
                                <script>
                                    numPag++;
                                    document.write('Full ' + numPag + ' de ' + totalPag);
                                </script>
                            </div>
                        </div>

                        <hr>

                        <table>
                            <tr>
                                <th>${_('CODI DE LA INSPECCIÓ')}</th>
                                <td>LBT-${rev.name.name}/${rev.trimestre.name}</td>
                            </tr>
                        </table>
                    </div>

                    <div>
                        <table class="defec">
                            <thead>
                            <tr>
                                <th rowspan="2">${_('NÚM LÍNIA')}</th>
                                <th rowspan="2">${_("TRAM IDENTIFICACIÓ DE L'OBJECTE")}</th>
                                <th rowspan="2">${_('CODI DE DEFECTE')}</th>
                                <th rowspan="2">${_('CODI DEFECTE DEL PLÀNOL')}</th>
                                <th colspan="3" style="font-size: 5px;">${_('CLASSIFICACIÓ DEL DEFECTE')}</th>
                                <th rowspan="2" width="100%">${_('OBSERVACIONS DEL DEFECTE')}</th>
                            </tr>
                            <tr>
                                <th class="rotated"><div>${_('LLEU')}</div></th>
                                <th class="rotated"><div>${_('GREU')}</div></th>
                                <th class="rotated"><div>${_('MOLT GREU')}</div></th>
                            </tr>
                            </thead>
                            <tbody>
                                %for i in range(act, min(act + defecPerPag, numeroDefec)):
                                    <tr>
                                        <td>${llista_defectes[i]['linia']}</td>
                                        <td>${llista_defectes[i]['tram']}</td>
                                        <td>${llista_defectes[i]['codi_defec']}</td>
                                        <td>${llista_defectes[i]['id']}</td>
                                        %for class_def in range(1, 4):
                                            %if class_def == llista_defectes[i]['valoracio']:
                                                <td>X</td>
                                            %else:
                                                <td></td>
                                            %endif
                                        %endfor
                                        <td>${llista_defectes[i]['observ']}</td>
                                    </tr>
                                %endfor
                                <%
                                    act += defecPerPag
                                %>
                            </tbody>
                        </table>
                    </div>
                    <%
                        if act >= numeroDefec:
                            break
                        endif
                    %>
                %endwhile
            %endif

            %if (numeroDefec % defecPerPag) >= 38 or (numeroDefec % defecPerPag) == 0:
                <div class="header">
                    <div style="width: 100%;">
                        <img src="${addons_path}/giscedata_revisions_bt/report/gisce.png">

                        <div>
                            13.3-18_05<br>
                            ${_('Guia de Verificació - LBT')}<br>
                            ${_('Revisió')} 5<br>
                            <script>
                                numPag++;
                                document.write('Full ' + numPag + ' de ' + totalPag);
                            </script>
                        </div>
                    </div>

                    <hr>

                    <table>
                        <tr>
                            <th>${_('CODI DE LA INSPECCIÓ')}</th>
                            <td>LBT-${rev.name.name}/${rev.trimestre.name}</td>
                        </tr>
                    </table>
                </div>
            %endif

            <div class="peu">
                <div class="terraNeu">
                    <table>
                        <tr><th colspan="3">${_('MESURES DE TERRA NEUTRE')}</th></tr>
                        <tr>
                            <th>${_('IDENTIFICACIÓ')}</th>
                            <th>${_('TERRA NEUTRE Rn')}</th>
                            <th width="70%">${_('OBSERVACIONS DE LA MESURA DE TERRA')}</th>
                        </tr>
                        <tr>
                            <th>A</th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>B</th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>C</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </table>
                </div>

                <div class="observ">
                    <table>
                        <tr><th>${_('Observacions de la inspecció')}</th></tr>
                        %if rev.observacions:
                            <tr><td>${rev.observacions}</td></tr>
                        %else:
                            <tr><td style="text-align: center">...........................................................................................................................................................................</td></tr>
                        %endif
                    </table>
                </div>

                <div class="firma">
                    <table>
                        <tr><th>${_('Inspector')}</th><td>${rev.tecnic.name}</td></tr>
                        <tr><th>${_('signatura data:')}</th><td>${'{0[2]}/{0[1]}/{0[0]}'.format(rev.data.split(' ')[0].split('-'))}</td></tr>
                    </table>
                </div>
            </div>
        %endfor
    </body>

    <script>
        totalPag = numPag;
        var aux = document.body.innerHTML;
        document.body.innerHTML = "";
        document.write(aux.split("</body>")[0]+"</html>");
    </script>

</html>
