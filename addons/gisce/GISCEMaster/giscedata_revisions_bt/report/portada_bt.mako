<%
    from datetime import datetime
    pool = objects[0].pool

    def get_revisions_bt_ordenades(trieni):
        revisions_bt_obj = objects[0].pool.get("giscedata.revisions.bt.revisio")
        llista_revisions_bt=[]
        for i in revisions_bt_obj.search(cursor,uid,[("trimestre",'=',trieni),("state","=","tancada")]):
            llista_revisions_bt.append(revisions_bt_obj.read(cursor,uid,i,["name"]))
        if llista_revisions_bt:
            return sorted(llista_revisions_bt, key=lambda elem : elem['name'][1])
        else:
            return []
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions_bt/report/estils_portada.css"/>
</head>
<body>
  <div id=content>
    %for trieni in objects:
      <div id=capcalera>
        ${_("RECONEIXEMENTS PERIÒDICS DE LES INSTAL·LACIONS DE PRODUCCIÓ, TRANSFORMACIÓ, TRANSPORT I DISTRIBUCIÓ D'ENERGIA ELÈCTRICA")}
        <h1>Trimestre ${trieni.name}</h1>
      </div>
      <div class=llista_info>
        <h2>${_("RECONEIXEMENT DE LES LÍNIES DE BAIXA TENSIÓ")}</h2>
        <table>
            <thead>
                <tr>
                    <td>
                        <br><br>
                    </td>
                </tr>
            </thead>
          <%
            bts = get_revisions_bt_ordenades(trieni.id)
          %>
            %for rev_bt in bts:
                <%
                    index = trieni.lbt.name.name.index(rev_bt['name'][1])
                    elem = trieni.lbt[index]
                %>
                <tr>
                    <td>${elem.name.name}</td>
                    <td>${elem.name.descripcio}</td>
                </tr>
            %endfor
        </table>
      </div>

      %if trieni != objects[-1]:
        <p style="page-break-after:always"></p>
      %endif

    %endfor
  </div>
  <div id="logos">
    <div id="logo_gisce">
      <img class="logo_img" src="${addons_path}/giscedata_revisions/report/gisce_logo.png">
    </div>
    <div id="logo_company">
      %if company.logo:
        <img class="logo_img" src="data:image/jpeg;base64,${company.logo}"/>
      %endif
    </div>
    <div style="clear: both;"></div>
  </div>
</body>
</html>
