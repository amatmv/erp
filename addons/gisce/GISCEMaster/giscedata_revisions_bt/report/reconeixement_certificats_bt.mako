## -*- encoding: utf-8 -*-
<%
    from datetime import datetime,date
    from babel.dates import format_date, format_datetime, format_time

    def te_defectes_interns(rev):
        return rev.num_ind_reparats[2] == "0"

    tecnic_text = _(
        u"{}, enginyer competent que ha realitzat la inspecció de la "
        u"instal·lacció elèctrica de referència, d'acord amb la ITC-BT-06 del "
        u"Reglament Electrotècnic de baixa tensió RD 842/2002 de 2 d'agost, o "
        u"les MIEBT 002, 003 i 004 del Reglament Electrotècnic de baixa tensió "
        u"RD 2413/1973 de 20 de setembre per les línies legalitzades abans del "
        u"18 de setembre de 2003."
    )

    fa_constar_te_defectes = _(
        u"Que la instal·lació verificada obté la qualificació de <b>favorable</b> "
        u"amb <b>defectes lleus</b> referent a seguretat del funcionament i al "
        u"compliment de les disposicions reglamentaries."
    )

    fa_constar_no_te_defectes = _(
        u"Que la instal·lació verificada obté la qualificació de <b>favorable</b> "
        u"referent a seguretat del funcionament i al compliment de les "
        u"disposicions reglamentaries."
    )
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/reconeixement_certificats.css"/>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions_bt/report/reconeixement_certificats_bt.css"/>
</head>
<body>
    <%
        revisions = sorted(objects, key=lambda elem: elem.name.name)
    %>
    %for rev in revisions:
        <div class="top_margin"></div>
        <%
            te_defectes = not te_defectes_interns(rev)
            if te_defectes:
                tots_reparats = rev.ind_reparats
            else:
                tots_reparats = True
        %>
        <div class="lateral_container">
            <div class="lateral">${_("Format doc.")}
                13.3-40_06
            </div>
        </div>
        <div class="images_header padded_text">
            <div class="leftplacement">
                <img src="${addons_path}/giscedata_revisions/report/gisce-eti_logo.png">
            </div>
            <div class="rightplacement">
                <img src="${addons_path}/giscedata_revisions/report/enac.png">
            </div>
            <div style="clear:both"></div>
        </div>

        <br>
        <table id="title">
            <colgroup>
                <col/>
                <col width="15%"/>
            </colgroup>
            <tr>
                <th>${_("ACTA DE VERIFICACIÓ")}</th>
                <td>☐ ${_(u"INICIAL")}</td>
            </tr>
            <tr>
                <th>${_("INSTAL·LACIÓ ELÈCTRICA DE SERVEI PÚBLIC")}</th>
                <td>☒ ${_(u"PERIÒDICA")}</td>
            </tr>
        </table>

        <div class="bold_header1">${_("Verificació de Línies Aèries de Baixa Tensió")}</div>
        <hr>

        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Codi de document")}</div>
                <div class="right_col">
                    %if tots_reparats:
                        V2-BT-${rev.name.name} ${rev.trimestre.name}
                    %else:
                        V-BT-${rev.name.name} ${rev.trimestre.name}
                    %endif
                </div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Codi de revisió")}</div>
                <div class="right_col">BT-${rev.name.name} ${rev.trimestre.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Data de revisió")}</div>
                <%
                    data_obj_rev = datetime.strptime(rev.data, '%Y-%m-%d %H:%M:%S')
                    data_rev = data_obj_rev.strftime('%d/%m/%Y')
                %>
                <div class="right_col">${data_rev}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Hora inspeccio")}</div>
                <div class="right_col">${data_obj_rev.hour}:${data_obj_rev.minute}</div>
            </div><br>

            %if te_defectes and tots_reparats:
                <div class="new_row padded_text">
                    <div class="left_col">${_("Data comprovació reparació de defectes")}</div>
                    <%
                        data_obj_compr = datetime.strptime(rev.data_comprovacio, '%Y-%m-%d %H:%M:%S')
                        data_compr = data_obj_compr.strftime('%d/%m/%Y')
                    %>
                    <div class="right_col">${data_compr}</div>
                </div><br>
                <div class="new_row padded_text">
                    <div class="left_col">${_("Tipus de reconeixement")}</div>
                    <div class="right_col">${_("2a visita")}</div>
                </div><br>
            %endif
            <div class="new_row padded_text">
                <div class="left_col">${_("Unitat de reconeixement")}</div>
                <div class="right_col">${_("Línies aèries BT corresponents al CT")} ${rev.name.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Empresa titular")}</div>
                <div class="right_col">${company.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Adreça per a notificacions")}</div>
                <% company_address = company.partner_id %>
                <div class="right_col">
                    ${company_address.address[0].street} ${company_address.address[0].zip} - ${company_address.city}
                </div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Entitat d'inspecció")}</div>
                <div class="right_col">${rev.tecnic.organisme}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Adreça Entitat d'inspecció")}</div>
                <div class="right_col">${rev.tecnic.domicili}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Nom cognoms del titulat")}</div>
                <div class="right_col">${rev.tecnic.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Titulació")}</div>
                <div class="right_col">${rev.tecnic.titolacio}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Número de col·legiat")}</div>
                <div class="right_col">${rev.tecnic.n_collegiat}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Adreça per a notifiacions")}</div>
                <div class="right_col">${rev.tecnic.domicili}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Telèfon")}</div>
                <div class="right_col">${rev.tecnic.telefon}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Correu electrònic")}</div>
                <div class="right_col">${rev.tecnic.email}</div>
            </div><br>
        </div>

        <br class="new_line">
        <br class="new_line">

        <div class="bold_header2 padded_text" >${_("Dades de la instal·lació")}</div><hr>
        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Instal·lació")}<br></div>
                <div class="right_col">${_("Línies aèries BT del Centre Transformador")} ${rev.name.name}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Emplaçament")}<br></div>
                <div class="right_col">${rev.name.descripcio}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Municipi")}<br></div>
                <div class="right_col">${rev.name.id_municipi.name}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tensió")} (V)<br></div>
                <div class="right_col">${rev.name.tensio_s}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Sortides de baixa tensió")}<br></div>
                <%
                    if len(rev.name.sortidesbt) == 0:
                        num_sortides_bt = 0
                    else:
                        num_sortides_bt = len([x for x in rev.name.sortidesbt.seccio if x != False])
                %>
                <div class="right_col">${num_sortides_bt}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Longitud de línies aèries")}<br></div>
                <div class="right_col">${0 if rev.name.longitud_bt_aeri == 0 else rev.name.longitud_bt_aeri} m</div>
            </div>
        </div>

        <br class="new_line">
        <br class="new_line">

        <div class="wrap">
            %if tots_reparats:
                <div class="bold_header2 padded_text">${_("Certificació")}</div><hr>
            %else:
                <div class="bold_header2 padded_text">${_("Acta")}</div><hr>
            %endif
            <p class="padded_text justified">
                ${tecnic_text.format(rev.tecnic.name)}
            </p>
            <br><br>
            %if te_defectes:
                <div class="bold_header2 padded_text">${_("FA CONSTAR:")}</div>
                <p class="padded_text">
                    ${_(fa_constar_te_defectes)}
                </p>
            %else:
                <div class="bold_header2 padded_text">${_("FA CONSTAR:")}</div>
                <p class="padded_text">
                    ${_(fa_constar_no_te_defectes)}
                </p>
            %endif
            <br><br>
            <div class="padded_text">
                ${_("Observacions i/o explicació d'accions no realitzades:")}
            </div>
            %if rev.observacions:
                <br>
                <div class="padded_text">
                    ${rev.observacions}
                </div>
            %endif

            <br><br><br><br><br><br>
            <div class="leftplacement padded_text">${_("Signatura")}</div>
            <%
                if te_defectes and tots_reparats:
                    data_obj = data_obj_compr
                else:
                    data_obj = data_obj_rev

                d = date(data_obj.year, data_obj.month, data_obj.day)
                data_document = format_date(d, "d MMMM 'de' yyyy", locale='ca_ES')
            %>
            <div class="rightplacement padded_text">Girona, ${data_document}</div>
            <br class="new_line">
            <div class="padded_text"><br>
                ${_("El present informe té una validesa de 3 anys a partir de la data de revisió")}
            </div>
        </div>

        %if te_defectes and not tots_reparats:
            <p style="page-break-after:always;"><br></p>
            <div class="top_margin"></div>
            <div class="lateral_container">
                <div class="lateral">${_("Format doc.")} 13.3-40_06</div>
            </div>
            <div class="images_header padded_text">
                <div class="leftplacement">
                    <img src="${addons_path}/giscedata_revisions/report/gisce-eti_logo.png"  >
                </div>
                <div class="rightplacement">
                    <img src="${addons_path}/giscedata_revisions/report/enac.png"  >
                </div>
                <div style="clear:both"></div>
            </div>

            <br>
            <div class="grey_header">
                <div class="bold_header1">
                    ${_("ACTA DE VERIFICACIÓ")}<br>
                    ${_("INSTAL·LACIÓ ELÈCTRICA DE SERVEI PÚBLIC")}
                </div>
            </div>

            <div class="bold_header1">${_("Verificació de Línies de Baixa Tensió")}</div>

            <div class="border_gray">
                <div class="titol_defectes">
                    ${_("DEFECTES A LES LÍNIES BT DEL CENTRE TRANSFORMADOR")} ${rev.name.name}
                </div>
            </div>
            <div class="border_gray without_border_top">
                <table class="llista_defectes">
                    <tr class="bold_text">
                        <td style="width:2%">${_("Codi")}</td>
                        <td style="width:2%">${_("Línia")}</td>
                        <td style="width:2%">${_("Tipus")}</td>
                        <td style="width:20%">${_("Descripció")}</td>
                        <td style="width:3%">${_("Classificació")}</td>
                        <td style="width:3%">${_("Limit reparació")}</td>
                    </tr>
                    %for defecte in rev.defectes_ids:
                        %if not defecte.intern:
                            <tr>
                                <td>${defecte.id}</td>
                                <td>${defecte.linia_bt.name}</td>
                                <td>${defecte.defecte_id.name}</td>
                                <%
                                    if len(defecte.defecte_id.descripcio) > 50:
                                        defecte_descripcio = defecte.defecte_id.descripcio[:50] + "..."
                                    else:
                                        defecte_descripcio = defecte.defecte_id.descripcio
                                %>
                                <td style="text-align:left">${defecte_descripcio}</td>
                                <td style="text-align:center">${defecte.valoracio.name}</td>
                                <%
                                    data_obj = datetime.strptime(defecte.data_limit_reparacio, '%Y-%m-%d %H:%M:%S')
                                    data_limit_reparacio = data_obj.strftime('%d/%m/%Y')
                                %>
                                <td>${data_limit_reparacio}</td>
                            </tr>
                            <tr>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td style="text-align:left">${defecte.observacions}</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                            </tr>
                        %endif
                    %endfor
                </table>
            </div>
            <div class="border_gray without_border_top">
                <table class="taula_tecnic">
                    <tr>
                        <td>${_("Tècnic:")}</td>
                        <td>${_("Data de verificació:")}</td>
                    </tr>
                    <tr class="bold_text">
                        <td>${rev.tecnic.name}</td>
                        <td>${data_rev}</td>
                    </tr>
                </table>
            </div>
        %else:
            <p style="page-break-after:always;"><br></p>
        %endif

        %if rev != revisions[-1]:
            <p style="page-break-after:always;"><br></p>
        %endif
    %endfor
</body>
</html>
