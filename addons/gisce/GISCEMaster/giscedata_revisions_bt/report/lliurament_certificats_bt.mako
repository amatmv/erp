<%
    from datetime import datetime,date

    def getUltimaData(trieni):
        if not trieni:
            return ''
        data = str(trieni.data_final)
        dates = data.split('/')
        if len(dates) != 3:
            return ''
        else:
            m = dates[0]
            d = dates[1]
            y = dates[2]
        d = '{0}-{1}-{2}'.format(d, m, y)
        return d

    def get_revisions_bt_ordenades(trieni):
        revisions_bt_obj = objects[0].pool.get("giscedata.revisions.bt.revisio")
        llista_revisions_bt=[]
        for i in revisions_bt_obj.search(cursor,uid,[("trimestre",'=',trieni),("state","=","tancada")]):
            llista_revisions_bt.append(revisions_bt_obj.read(cursor,uid,i,["name"]))
        return sorted(llista_revisions_bt, key=lambda elem : elem['name'][1])
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions_bt/report/lliurament_certificats.css"/>
</head>
<body>
  %for trieni in objects:
    <div id="separador">13.3-06</div>
    <div id=capcalera1>
        <h1>${_("FULL DE LLIURAMENT DE CERTIFICATS")}</h1>
        <div id="text_petit_esquerra">${_("Lliurament de certificats a:")}</div>
        <div id="text_petit_dreta">${_("Segell de registre:")}</div>
        <h2>${company.name}</h2>
        <div id="quadre_registre">
            <table>
                <tr>
                    <th colspan="2">${_("REGISTRE DE SORTIDA")}</th>
                </tr>
                <tr>
                    <td width="60%">DATA<p style="font-size: 15px;margin:0;padding:0">&emsp;&emsp;/&emsp;&emsp;/</p></td>
                    <td  width="40%">NÚM.</td>
                </tr>
            </table>
        </div>
    </div>
    <div id="list-title">
        <h2 style="margin-top: -15px">Trimestre ${trieni.name}</h2>
        <div id="separador2"></div>
    </div>
    <div class=llista_info>
      <p>${_("Línies de Baixa Tensió:")}</p>
      <table>
          <%
            bts = get_revisions_bt_ordenades(trieni.id)
          %>
            %for rev_bt in bts:
                <%
                    index = trieni.lbt.name.name.index(rev_bt['name'][1])
                    elem = trieni.lbt[index]
                %>
                <tr>
                    <td>${elem.name.name}</td>
                    <td>${elem.name.descripcio}</td>
                </tr>
            %endfor
      </table>
    </div>
    <div id="data_final">
        <div id="text_data_final">${_("Realitzat per (inspector):")}</div>
            <br><br><br><br>Data: ${date.today().strftime('%d/%m/%Y')}
    </div>
    %if trieni != objects[-1]:
      <p style="page-break-after:always;clear: both"></p>
    %endif
  %endfor
</body>
</html>
