<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list" />
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template pageSize="(297mm,19cm)" topMargin="0.5cm" bottomMargin="0.5cm" rightMargin="1cm">
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="277mm" height="175mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
					fontName="Helvetica"
					fontSize="10"
          leading="20"
          alignment="center"
				/>

        <paraStyle name="empresa"
					fontName="Helvetica"
					fontSize="8"
          leading="16"
          alignment="right"
				/>

        <blockTableStyle id="taula1">
          <blockFont name="Helvetica" size="8" />
          <blockValign value="MIDDLE" />
          <blockFont name="Helvetica-Bold" size="12" start="1,0" stop="1,0" />
          <lineStyle kind="BOX" colorName="black" start="1,0" stop="1,0" />
          <blockAlignment value="CENTER" start="1,0" stop="2,0" />
          <blockTopPadding leading="0" />
        </blockTableStyle>

        <blockTableStyle id="taula2">
          <blockFont name="Helvetica" size="8" />
          <blockAlignment value="CENTER" />
          <blockLeading length="10" />
          <lineStyle kind="LINEBELOW" colorName="silver" />
        </blockTableStyle>

        <blockTableStyle id="taula3">
          <blockFont name="Helvetica" size="8" />
          <blockLeading length="10" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,0" stop="9,0" />
        </blockTableStyle>

        <blockTableStyle id="taula4">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="0,0" />
          <blockFont name="Helvetica-Bold" start="4,0" stop="4,0" />
          <blockFont name="Helvetica-Bold" start="3,0" stop="3,0" />
          <lineStyle kind="BOX" colorName="silver" start="7,0" stop="7,0" />
          <lineStyle kind="LINEBEFORE" colorName="silver" start="9,0" stop="9,0" />
          <lineStyle kind="LINEABOVE" colorName="silver" start="9,0" stop="9,0" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="9,0" stop="9,0" />
        </blockTableStyle>
        
        <blockTableStyle id="taula5">
          <blockFont name="Helvetica" size="8" />

          <blockLeading length="0.1" start="0,0" stop="3,0" />
          <blockTopPadding length="0" start="0,0" stop="3,0" />

          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,0" stop="3,0" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,0" stop="3,0" />

          <lineStyle kind="LINEBEFORE" colorName="silver" start="1,1" stop="1,2" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="1,2" stop="1,2" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="1,1" stop="1,2" />
          <lineStyle kind="LINEABOVE" colorName="silver" start="1,1" stop="1,1" />
          
          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,1" stop="3,1" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,1" stop="3,1" />
          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,2" stop="3,2" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,2" stop="3,2" />
          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,3" stop="3,3" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,3" stop="3,3" />
          
          <lineStyle kind="LINEBEFORE" colorName="silver" start="1,4" stop="1,5" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="1,5" stop="1,5" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="1,4" stop="1,5" />
          <lineStyle kind="LINEABOVE" colorName="silver" start="1,4" stop="1,4" />
          <blockFont name="Helvetica-Bold" start="1,5" stop="1,5" />
          <blockAlignment value="RIGHT" start="1,5" stop="1,5" />
          
          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,4" stop="3,4" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,4" stop="3,4" />
          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,5" stop="3,5" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,5" stop="3,5" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="3,5" stop="3,5" />

          <blockLeading length="0.1" start="0,3" stop="3,3" />
          <blockTopPadding length="0" start="0,3" stop="3,3" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
      <xsl:apply-templates select="revisio" mode="story">
        <xsl:sort select="defectes/defecte/name" data-type="number" order="ascending"/>
      </xsl:apply-templates>
      </story>
    </document>

    </xsl:template>

    <xsl:template match="revisio" mode="story">
      <xsl:if test="count(defectes/defecte) &gt; 0 and estat != 'sense assignar'">
      <para style="titol" t="1">RECONEIXEMENTS PERI�DICS DE LES INSTAL�LACIONS DE PRODUCCI�, TRANSFORMACI�, TRANSPORT I DISTRIBUCI� D'ENERGIA EL�CTRICA</para>
      <blockTable style="taula1" colWidths="8.92cm,5.92cm,8.92cm,3.92cm">
        <tr>
          <td t="1">LLISTAT DE DEFECTES EN CENTRES DE TRANSFORMACI�</td>
          <td t="1">FULL DE REPARACI�</td>
          <td><xsl:value-of select="../corporate-header/corporation/name" /></td>
          <td><image file="addons/giscedata_revisions/report/gisce.tif"
        width="30mm" height="8mm" /></td>
        </tr>
      </blockTable>

      <blockTable style="taula2" colWidths="27.7cm">
        <tr>
          <td t="1">S'han d'omplir els apartats 'Data de reparaci�', 'Observacions' i 'Operari/s'</td>
        </tr>
      </blockTable>
      <blockTable style="taula3" colWidths="1.2cm,1.2cm,2cm,1.5cm,4cm,1cm,11.8cm,2.4cm,1mm,2.5cm">
        <tr>
          <td t="1">Codi</td>
          <td t="1">Revisi�</td>
          <td t="1">Data</td>
          <td t="1">CT</td>
          <td></td>
          <td t="1">Descripci�</td>
          <td></td>
          <td t="1">Data reparaci�</td>
          <td></td>
          <td t="1">Operari/s</td>
        </tr>
      </blockTable>
        <xsl:apply-templates select="defectes/defecte" mode="story" />
      <setNextTemplate name="main" />
      </xsl:if>

  </xsl:template>

  <xsl:template match="defectes/defecte" mode="story">
  	<xsl:if test="reparat!=1">
      <spacer length="1mm" />
      <condPageBreak height="3.5cm"/>
      <blockTable style="taula4" colWidths="1.2cm,1.2cm,2cm,1.5cm,4cm,1cm,11.8cm,2.4cm,1mm,2.5cm">
        <tr>
          <td><xsl:value-of select="name"/></td>
          <td><xsl:value-of select="../../trimestre"/></td>
          <td><xsl:value-of select="concat(substring(../../data, 9, 2), '/', substring(../../data, 6, 2), '/', substring(../../data, 1, 4))"/></td>
          <td><xsl:value-of select="../../ct/codi"/></td>
          <td><xsl:value-of select="../../ct/descripcio"/></td>
          <td><xsl:value-of select="codi"/></td>
          <td><xsl:value-of select="descripcio" /></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </blockTable>
      <blockTable style="taula5" colWidths="1.6cm,23.5cm,1mm,2.5cm">
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td t="1">OBSERVACIONS RECONEIXEMENT:</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><xsl:value-of select="observacions" /></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>    
        <tr>
          <td></td>
          <td t="1">OBSERVACIONS REPARACI�:</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><xsl:value-of select="../../ct/municipi" /></td>
          <td></td>
          <td></td>
        </tr>
      </blockTable>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
