# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Revisions BT",
    "description": """Revisions""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Revisions",
    "depends":[
        "c2c_webkit_report",
        "giscedata_revisions",
        "giscedata_cts",
        "giscedata_bt",
        "giscedata_trieni"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_revisions_bt_demo.xml",
    ],
    "update_xml":[
        "giscedata_revisions_bt_data.xml",
        "giscedata_revisions_bt_view.xml",
        "giscedata_revisions_bt_wizard.xml",
        "giscedata_revisions_bt_report.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
