from osv import fields, osv


class GiscedataTrieni(osv.osv):
    _name = "giscedata.trieni"
    _inherit = "giscedata.trieni"

    _columns = {
        'lbt': fields.one2many('giscedata.revisions.bt.revisio', 'trimestre',
                               'Revisions LBT'),
}


GiscedataTrieni()