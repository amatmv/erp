# -*- coding: iso-8859-1 -*-
import wizard
import pooler
from datetime import *


_avis_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Amb aquest assistent canviaràs la normativa vigent." />
</form>"""

_avis_fields = {}

_escollir_normativa_form = """<?xml version="1.0"?>
<form string="Informacio d'entrada">
  <field name="normativa" />
</form>"""

_escollir_normativa_fields = {
	'normativa': {'string': 'Normativa', 'required': True, 'type': 'many2one', 'relation': 'giscedata.revisions.bt.normativa'},
}


def _guardar(self, cr, uid, data, context={}):
  for normativa in pooler.get_pool(cr.dbname).get('giscedata.revisions.bt.normativa').search(cr, uid, [('vigent', '=', True)]):
    pooler.get_pool(cr.dbname).get('giscedata.revisions.bt.normativa').write(cr, uid, [normativa], {'vigent': False})
  pooler.get_pool(cr.dbname).get('giscedata.revisions.bt.normativa').write(cr, uid, [data['form']['normativa']], {'vigent': True})
  return {}

class wizard_vigencia_normativa_bt(wizard.interface):

  states = {
    'init': {
    	'actions': [],
      'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('escollir_normativa', 'Continuar', 'gtk-go-forward')]}
    },
    'escollir_normativa': {
    	'actions': [],
    	'result': {'type': 'form', 'arch': _escollir_normativa_form,'fields': _escollir_normativa_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('guardar', 'Activar', 'gtk-ok')]}
    },
    'guardar': {
    	'actions': [_guardar],
    	'result': { 'type' : 'state', 'state' : 'end' },
    },
  }

wizard_vigencia_normativa_bt('giscedata.revisions.bt.normativa.vigencia')
