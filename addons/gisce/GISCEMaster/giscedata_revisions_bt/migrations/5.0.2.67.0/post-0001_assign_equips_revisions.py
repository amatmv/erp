# -*- coding: utf-8 -*-
import netsvc
from oopgrade import oopgrade


def up(cursor, installed_version):
    if not installed_version:
        return
    logger = netsvc.Logger()
    # Cal assignar a totes les revisions BT l'equip utilitzat que era hardcoded
    # BT: {
    #   E-01 MESURADOR DE POSADA A TERRA, E-02 MESURADOR DE DISTÀNCIES LASER,
    #   E-15 PLOMADA, E-20 CÀMERA TERMOGRÀFICA,
    #   E-24 PINÇA AMPERIMÈTRICA DIGITAL MASTECH MS2015A
    # }
    equips_bt = ['E-01', 'E-02', 'E-15', 'E-20', 'E-24']

    query = "SELECT id FROM giscedata_revisions_equips WHERE codi IN %s"
    cursor.execute(query, (tuple(equips_bt),))
    equips_list = [x[0] for x in cursor.fetchall()]

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO, 'Updating equips for all revisions BT'
    )
    cursor.execute(
        "SELECT id FROM giscedata_revisions_bt_revisio WHERE state='tancada'"
    )
    revisions = cursor.fetchall()
    insert_list = []
    for revisio in revisions:   # For each revisio BT
        id_revisio = revisio[0]
        for id_equip in equips_list:      # For each equip used in the reports
            insert = "INSERT INTO giscedata_revisions_bt_equips " \
                "(equip_id, revisio_id) VALUES (%s, %s)"
            params = (id_equip, id_revisio)
            insert_list.append((insert, params))
    for insert, params in insert_list:
        cursor.execute(insert, params)


def down(cursor):
    return

migrate = up
