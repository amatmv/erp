# -*- coding: utf-8 -*-
{
    "name": "GISCE Ordres de Treball d'Incidències i Descàrrecs",
    "description": """GISCE Ordres de Treball d'Incidències i Descàrrecs:
    - Nova relació amb llista de descàrrecs.
    - Nova relació amb llista d'incidències de qualitat de servei.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_ot",
        "giscedata_descarrecs",
        "giscedata_qualitat",
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "giscedata_ot_view.xml",
    ],
    "active": False,
    "installable": True
}
