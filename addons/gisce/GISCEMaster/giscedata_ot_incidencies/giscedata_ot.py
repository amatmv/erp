# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataOt(osv.osv):

    _name = "giscedata.ot"
    _inherit = "giscedata.ot"

    _columns = {
        'descarrec_ids': fields.many2many(
            'giscedata.descarrecs.descarrec',
            'giscedata_ot_descarrec_rel',
            'ot_id', 'descarrec_id',
            string='Descargos relacionados'
        ),
        'qualitat_ids': fields.many2many(
            'giscedata.qualitat.incidence',
            'giscedata_ot_qualitat_rel',
            'ot_id', 'incidence_id',
            string='Incidencias de calidad relacionadas'
        ),
    }


GiscedataOt()
