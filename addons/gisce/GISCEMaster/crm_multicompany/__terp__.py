# -*- coding: utf-8 -*-
{
    "name": "CRM multicompany",
    "description": """Multi-company support for CRM""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "crm"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "crm_multicompany_view.xml"
    ],
    "active": False,
    "installable": True
}
