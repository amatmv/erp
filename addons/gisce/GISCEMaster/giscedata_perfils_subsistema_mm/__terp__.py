# -*- coding: utf-8 -*-
{
    "name": "Subsitema per Mallorca-Menorca",
    "description": """
    This module provide :
      * Codi de subsistema per Mallorca-Menorca
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_perfils",
        "l10n_ES_toponyms"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_perfils_data.xml"
    ],
    "active": False,
    "installable": True
}
