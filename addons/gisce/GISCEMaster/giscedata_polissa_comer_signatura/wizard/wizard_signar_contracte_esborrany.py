# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from giscedata_polissa.giscedata_polissa import CONTRACT_STATES


class WizardSignarContracteEsborrany(osv.osv_memory):

    _name = 'wizard.signar.contracte.esborrany'

    def _default_email(self, cursor, uid, context=None):
        addr_obj = self.pool.get('res.partner.address')
        pol_obj = self.pool.get('giscedata.polissa')
        pol_id = context['active_id']
        payment = pol_obj.read(
            cursor, uid, pol_id, ['direccio_pagament']
        )['direccio_pagament']
        if payment:
            payment_id = payment[0]
            email = addr_obj.read(
                cursor, uid, payment_id, ['email'])['email']
            if email:
                return email

        raise osv.except_osv(
            _("Es necessita un correu electrònic"),
            _(u"Per signar el contracte digitalment cal que el contracte "
              u"tingui una direcció de correu electrònic a l'adreça fiscal")
        )

    def _default_estat(self, cursor, uid, context=None):
        pol_obj = self.pool.get('giscedata.polissa')
        pol_id = context['active_id']
        state = pol_obj.read(cursor, uid, pol_id, ['state'])['state']
        if state != 'esborrany':
            raise osv.except_osv(
                _("Aquest contracte no està en Esborrany"),
                _(
                    u"Per signar el contracte digitalment cal que el contracte "
                    u"estigui en esborrany"
                )
            )
        return state

    def request(self, cursor, uid, ids, context=None):
        raise Exception("S'ha d'instal·lar un módul de proveïdor de signatures digitals")

    _columns = {
        'email': fields.char('Correu electrònic', size=100, required=True,
                             help=u'Aquest és el correu de la direcció fiscal'),
        'estat_contracte': fields.selection(
            CONTRACT_STATES, 'Estat', required=True, readonly=True),
        'delivery_type': fields.selection([
            ('email', 'Correu electrònic')
        ], "Forma d'enviament", required=True),
    }

    _defaults = {
        'email': _default_email,
        'estat_contracte': _default_estat,
        'delivery_type': lambda *a: 'email',
    }


WizardSignarContracteEsborrany()
