# -*- coding: utf-8 -*-
{
    "name": "Signatura digital de polisses dels clients (Comercialitzadora)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa_comer",
        "giscedata_signatura_documents",
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        "security/ir.model.access.csv",
        "giscedata_polissa_comer_data.xml",
        "wizard/wizard_signar_contracte_esborrany_view.xml",
    ],
    "active": False,
    "installable": True
}
