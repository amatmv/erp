# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv
import logging
from tools.translate import _


class GiscedataPolissa(osv.osv):
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'

    def start_signature_process(self, cursor, uid, contract_id, context=None):
        raise Exception("S'ha d'instal·lar un módul de proveïdor de signatures digitals")

    def alta_contracte(self, cursor, uid, contract_id, context=None):
        if not context:
            context = {}

        logger = logging.getLogger(__name__)

        config_vals = {
            'activacio_cicle': 'A'
        }

        # Activo usuari a l'oficina virtual si no existeix
        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')
        sw_obj = self.pool.get('giscedata.switching')
        contracte_data = self.read(
            cursor, uid, contract_id, ['titular', 'state'])
        partner_id = contracte_data['titular'][0]

        partner_data = partner_obj.read(
            cursor, uid, partner_id, ['vat', 'ov_users_ids', 'address'])
        address = partner_data['address'][0]
        vat = partner_data['vat'].replace('ES', '')
        addr = partner_address_obj.read(
            cursor, uid, address, ['email', 'mobile']
        )
        email = addr['email']
        mobile = addr['mobile']

        if not partner_data.get('ov_users_ids', False):
            partner_obj.enable_ov_user(cursor, uid, partner_id, vat, email, mobile)

        # Si no hi ha sw_obj -> No hi ha switching instalat
        if not sw_obj or contracte_data['state'] != 'esborrany':
            return
        # Creo cas ATR C1
        search_params = [('cups_polissa_id', '=', contract_id)]
        polissa = sw_obj.search(cursor, uid, search_params)

        if not polissa:
            cas_atr = self.crear_cas_atr(
                cursor, uid, contract_id, 'C1',
                config_vals=config_vals, context=context
            )
            sw_id = cas_atr[2]

            if sw_id:
                # Busco ID del pas
                step = sw_obj.read(cursor, uid, sw_id, ['step_id'])['step_id'][0]
                pas = sw_obj.get_pas_id(cursor, uid, sw_id=sw_id, step_id=step)
            else:
                raise osv.except_osv(_('Error!'), cas_atr[1])

    def process_signature_callback(self, cursor, uid, contract_id, context=None):
        if not context:
            context = {}
        process_data = context.get('process_data', False)
        if process_data:
            method = process_data.get('callback_method', False)
            if method == 'alta_contracte':
                self.alta_contracte(cursor, uid, contract_id, context=context)

    def process_attachment_callback(self, cursor, uid, pol_id, doc_filename,
                                    signature_id, signature_doc_id, model, context=None):
        if not context:
            context = {}
        process_data = context.get('process_data', False)
        if process_data:
            method = process_data.get('callback_method', False)
            if method == '':
                pass
            else:
                pro_obj = self.pool.get('giscedata.signatura.process')
                pro_obj.default_attach_document(
                    cursor, uid, pol_id, signature_id,
                    signature_doc_id, doc_filename, model, context=context
                )


GiscedataPolissa()
