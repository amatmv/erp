# -*- coding: utf-8 -*-
{
    "name": "GISCE LAT multicompany",
    "description": """Multi-company support for LAT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscedata_at"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_at_multicompany_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
