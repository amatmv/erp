# -*- encoding: utf-8 -*-
import base64

from datetime import datetime
from dateutil.relativedelta import relativedelta
from addons import get_module_resource
from osv import osv, fields
from lxml import etree
from tools.translate import _

import logging

CCAA_CODES_TRANSLATION = [
    # (res.comunitat_autonoma codi, CNMC codi)
    ('01', '1'),  # Andalucia
    ('02', '2'),  # Aragón
    ('03', '3'),  # Asturias, Principado de
    ('04', '4'),  # Baleares, Islas
    ('05', '5'),  # Canarias
    ('06', '6'),  # Cantabria
    ('07', '8'),  # Castilla y León
    ('08', '7'),  # Castilla - La Mancha
    ('09', '9'),  # Cataluña
    ('10', '19'),  # Comunidad Valenciana
    ('11', '11'),  # Extremadura
    ('12', '12'),  # Galicia
    ('13', '14'),  # Madrid, Comunidad de
    ('14', '16'),  # Murcia, Región de
    ('15', '17'),  # Navarra, Comunidad Foral de
    ('16', '18'),  # País Vasco
    ('17', '13'),  # Rioja, La
    ('18', '10'),  # Ceuta
    ('19', '15'),  # Melilla
]

ANY_CODES = [
    ('2017', '2017'),
    ('2018', '2018'),
    ('2019', '2019')
]

MES_CODES = [  # [(str(x).zfill(2), str(x).zfill(2)) for x in range(1, 13)]
    ('01', 'Gener'),
    ('02', 'Febrer'),
    ('03', 'Març'),
    ('04', 'Abril'),
    ('05', 'Maig'),
    ('06', 'Juny'),
    ('07', 'Juliol'),
    ('08', 'Agost'),
    ('09', 'Setembre'),
    ('10', 'Octubre'),
    ('11', 'Novembre'),
    ('12', 'Desembre')
]

COLUMNA_MENSUAL = {
    'STCM_CODIGO_DISTRIBUIDORA': (int, 3),
    'STCM_ANIO_ABONO': (int, 4),
    'STCM_MES_ABONO': (int, 2),
    'STCM_ANIO_CONSUMO': (int, 4),
    'STCM_CODIGO_CCAA': (int, 2),
    'STCM_CODIGO_TARIFA': (int, 3),
    'STCM_NUM_CLIENTES': (int, 8),
    'STCM_TOTAL_ENERGIA': (int, 14),
    'STCM_TOTAL_IMPORTE': (float, (16, 2))
}

COLUMNA_ANUAL = {
    'STCA_CODIGO_DISTRIBUIDORA': (int, 3),
    'STCA_ANIO_CONSUMO': (int, 4),
    'STCA_CODIGO_CCAA': (int, 2),
    'STCA_CODIGO_TARIFA': (int, 3),
    'STCA_TIPO_CLIENTE': (int, 2),
    'STCA_NUM_CLIENTES': (int, 8),
    'STCA_TOTAL_ENERGIA': (int, 14),
    'STCA_TOTAL_IMPORTE': (float, (16, 2))
}


class WizardCrearXMLSuplementTerritorialTEC271(osv.osv_memory):

    _name = 'wizard.crear.xml.suplement.territorial.tec271'

    def fix_xml_value(self, fieldname, fieldvalue, anual=False):
        cols = COLUMNA_ANUAL if anual else COLUMNA_MENSUAL
        tipus, llarg = cols[fieldname]
        if not isinstance(fieldvalue, tipus):
            fieldvalue = tipus(fieldvalue)
        if isinstance(llarg, tuple):
            fieldvalue = round(fieldvalue, llarg[1])
        elif len(str(fieldvalue)) > llarg:
            fieldvalue = fieldvalue[-llarg:]
        return str(fieldvalue)

    def fix_dades_setu(self, cursor, uid, ids, context=None):
        sql_path = get_module_resource(
            'giscedata_liquidacions_tec271_2019', 'sql',
            'fix_dades_tec.sql'
        )
        with open(sql_path, 'r') as sql_updates:
            sql_queries = sql_updates.read().strip().split(';')
        for query in sql_queries:
            if not query:
                continue
            cursor.execute(query)

    def _get_tipus_xml(self, cursor, uid, context=None):
        if context is None:
            context = {}

        tipus_xml = context.get('tipus_xml', False)

        return tipus_xml

    def _get_any_abonament(self, cursor, uid, context=None):
        if str(datetime.now().year) in [k for k, v in ANY_CODES]:
            return str(datetime.now().year)

    def _get_mes_abonament(self, cursor, uid, context=None):
        if str(datetime.now().month).zfill(2) in [k for k, v in MES_CODES]:
            return str(datetime.now().month).zfill(2)

    def get_codi_distri(self, cursor, uid, ids, context=None):
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        codi_r1 = user.company_id.codi_r1
        if not codi_r1:
            raise osv.except_osv(
                'Error', _('L\'empresa {} no té "codi_r1" assignat.').format(
                    user.company_id.name
                )
            )
        return codi_r1.zfill(4)

    def generar_xml_mensual(self, cursor, uid, wizard_id, context=None):
        if context is None:
            context = {}

        # Get Liquidacio files

        wiz = self.browse(cursor, uid, wizard_id)

        codi_distri = self.get_codi_distri(cursor, uid, wizard_id)
        any_abonament = wiz.any_abonament
        any_abon_num = int(any_abonament)
        mes_abonament = wiz.mes_abonament
        mes_abon_num = int(mes_abonament)
        any_consum = str(2013)

        imd_obj = self.pool.get('ir.model.data')
        supl_terri_product_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_contractacio_distri', 'energia_ordre_tec_271_2019'
        )[1]
        sql_file = 'dades_xml_suplement_tec271_2019_mensual.sql'
        mesos_a_sumar = 1
        if mes_abon_num == 7 and any_abon_num == 2018:
            mesos_a_sumar = 4
            sql_file = 'dades_xml_suplement_tec271_2019_mensual_i_pendent.sql'
        sql_path = get_module_resource(
            'giscedata_liquidacions_tec271_2019', 'sql',
            sql_file
        )
        filename = 'STM_{any}{mes}.{codi_distri}.xml'.format(
            any=any_abonament, mes=mes_abonament, codi_distri=codi_distri
        )

        with open(sql_path, 'r') as f:
            sql = f.read()

        data_inici = datetime(any_abon_num, mes_abon_num, 1)
        data_inici_str = data_inici.strftime('%Y-%m-%d')
        data_fi_str = (
            data_inici + relativedelta(months=mesos_a_sumar)
        ).strftime('%Y-%m-%d')
        cursor.execute(
            sql, {
                'data_inici': data_inici_str,
                'data_fi': data_fi_str,
                'product_id': supl_terri_product_id
            }
        )

        results = cursor.fetchall()

        root = etree.Element("ST_CONSUMO_MENSUAL")

        ccaa_dict = dict(CCAA_CODES_TRANSLATION)
        for result in results:
            codi_ccaa, tarifa, num_clients, total_energia, total_import = result

            consum = etree.SubElement(root, u'CONSUMO')
            etree.SubElement(
                consum, u'STCM_CODIGO_DISTRIBUIDORA').text = codi_distri
            etree.SubElement(
                consum, u'STCM_ANIO_ABONO').text = any_abonament
            etree.SubElement(
                consum, u'STCM_MES_ABONO').text = mes_abonament
            etree.SubElement(
                consum, u'STCM_ANIO_CONSUMO').text = any_consum
            etree.SubElement(
                consum, u'STCM_CODIGO_CCAA').text = ccaa_dict[codi_ccaa]
            etree.SubElement(
                consum, u'STCM_CODIGO_TARIFA').text = tarifa
            etree.SubElement(
                consum, u'STCM_NUM_CLIENTES').text = num_clients
            etree.SubElement(
                consum, u'STCM_TOTAL_ENERGIA').text = total_energia
            etree.SubElement(
                consum, u'STCM_TOTAL_IMPORTE').text = total_import

        xml_generat = base64.encodestring(etree.tostring(root))

        vals = {
            'name': filename,
            'xml': xml_generat,
            'state': 'end'
        }

        wiz.write(vals)

    def generar_xml_anual(self, cursor, uid, wizard_id, context=None):
        if context is None:
            context = {}

        logger = logging.getLogger('liquidacion_setu_anual')
        wiz = self.browse(cursor, uid, wizard_id)
        wiz.fix_dades_setu()
        codi_distri = self.get_codi_distri(cursor, uid, wizard_id)
        any_abonament = wiz.any_abonament
        mes_abonament = wiz.mes_abonament
        any_consum = str(2013)

        imd_obj = self.pool.get('ir.model.data')
        supl_terri_product_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_contractacio_distri', 'energia_ordre_tec_271_2019'
        )[1]

        filename = 'STA_{any}{mes}.{codi_distri}.xml'.format(
            any=any_abonament, mes=mes_abonament, codi_distri=codi_distri
        )
        logger.info('Generant fitxer {}'.format(filename))

        sql_path = get_module_resource(
            'giscedata_liquidacions_tec271_2019', 'sql',
            'dades_xml_suplement_tec271_2019_unic_anual.sql'
        )

        with open(sql_path, 'r') as f:
            sql = f.read()

        cursor.execute(sql, {'product_id': supl_terri_product_id})

        dades_sql_anuals = cursor.fetchall()
        ccaa_dict = dict(CCAA_CODES_TRANSLATION)

        # Generem el XML
        root = etree.Element("ST_CONSUMO_ANUAL")

        for result in dades_sql_anuals:
            codi_ccaa, tarifa, tipus, num_clients, total_energia, total_import = result
            consum = etree.SubElement(root, u'CONSUMO')
            etree.SubElement(
                consum, u'STCA_CODIGO_DISTRIBUIDORA'
            ).text = self.fix_xml_value(
                fieldname=u'STCA_CODIGO_DISTRIBUIDORA',
                fieldvalue=codi_distri,
                anual=True
            )
            etree.SubElement(
                consum, u'STCA_ANIO_CONSUMO'
            ).text = self.fix_xml_value(
                fieldname=u'STCA_ANIO_CONSUMO',
                fieldvalue=any_consum,
                anual=True
            )
            etree.SubElement(
                consum, u'STCA_CODIGO_CCAA'
            ).text = self.fix_xml_value(
                fieldname=u'STCA_CODIGO_CCAA',
                fieldvalue=ccaa_dict[codi_ccaa],
                anual=True
            )
            etree.SubElement(
                consum, u'STCA_CODIGO_TARIFA'
            ).text = self.fix_xml_value(
                fieldname=u'STCA_CODIGO_TARIFA',
                fieldvalue=tarifa,
                anual=True
            )
            etree.SubElement(
                consum, u'STCA_TIPO_CLIENTE'
            ).text = self.fix_xml_value(
                fieldname=u'STCA_TIPO_CLIENTE',
                fieldvalue=tipus,
                anual=True
            )
            etree.SubElement(
                consum, u'STCA_NUM_CLIENTES'
            ).text = self.fix_xml_value(
                fieldname=u'STCA_NUM_CLIENTES',
                fieldvalue=num_clients,
                anual=True
            )
            etree.SubElement(
                consum, u'STCA_TOTAL_ENERGIA'
            ).text = self.fix_xml_value(
                fieldname=u'STCA_TOTAL_ENERGIA',
                fieldvalue=total_energia,
                anual=True
            )
            etree.SubElement(
                consum, u'STCA_TOTAL_IMPORTE'
            ).text = self.fix_xml_value(
                fieldname=u'STCA_TOTAL_IMPORTE',
                fieldvalue=total_import,
                anual=True
            )

        xml_generat = base64.encodestring(etree.tostring(root))

        vals = {
            'name': filename,
            'xml': xml_generat,
            'state': 'end'
        }
        wiz.write(vals)

    def action_generate_xml(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids[0])

        tipus_xml = wiz.tipus_xml

        if tipus_xml == 'mensual':
            self.generar_xml_mensual(cursor, uid, wiz.id, context)

        elif tipus_xml == 'anual':
            self.generar_xml_anual(cursor, uid, wiz.id, context)

    _columns = {
        'name': fields.char('Nom del fitxer', size=19),
        'any_abonament': fields.selection(
            selection=ANY_CODES, string='Any abonament'),
        'mes_abonament': fields.selection(
            selection=MES_CODES, string='Mes abonament'),
        'tipus_xml': fields.char('Tipus XML', size=16),
        'info': fields.text('Informació', readonly=True),
        'xml': fields.binary('XML generat', filters='*.xml'),
        'state': fields.selection(
            [('init', 'Initial'), ('end', 'End')], 'Estat'
        )
    }

    _defaults = {
        'state': lambda *a: 'init',
        'tipus_xml': _get_tipus_xml,
        'any_abonament': _get_any_abonament,
        'mes_abonament': _get_mes_abonament,
    }


WizardCrearXMLSuplementTerritorialTEC271()
