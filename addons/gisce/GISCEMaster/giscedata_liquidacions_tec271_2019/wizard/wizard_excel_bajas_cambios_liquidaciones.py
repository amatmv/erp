# -*- encoding: utf-8 -*-

from osv import osv, fields
import pandas as pd
from StringIO import StringIO
from base64 import b64encode


class WizardExcelBajasCambiosLiquidaciones(osv.osv_memory):

    _name = 'wizard.excel.bajas.cambios.liquidaciones'

    def generate_excel_liquidaciones_bajas_cambios(self, cursor, uid, ids, context=None):
        select_liquidacions_baja_cambio = (
            u"SELECT DISTINCT"
            u"  cups,"
            u"  CASE WHEN tipus = '1' THEN 'Baja' ELSE 'Cambio de titularidad' END AS tipo_liquidacion "
            u"FROM giscedata_liquidacio_suplement_territorial_tec271_data "
            u"WHERE tipus IN ('1', '2')"
        )

        cursor.execute(select_liquidacions_baja_cambio)

        res = cursor.dictfetchall()

        df_liquidacions = pd.DataFrame(data=res)

        df_liquidacions = df_liquidacions.sort_values(by=[u'tipo_liquidacion'], ascending=True)

        file_xls = StringIO()

        writer_complet = pd.ExcelWriter(file_xls)

        df_liquidacions.to_excel(
            writer_complet, index=None, header=True, encoding='utf-8-sig'
        )

        writer_complet.save()

        liquidacions_xls_str_file = b64encode(file_xls.getvalue())

        file_xls.close()

        write_dict = {
            'file': liquidacions_xls_str_file,
            'state': 'end',
            'info': 'Exportado correctamente'
        }

        self.write(cursor, uid, ids, write_dict, context=context)

    _columns = {
        'state': fields.selection([('init', 'Inici'), ('end', 'Fi')], 'Estat'),
        'file': fields.binary('File'),
        'file_name': fields.char('Nombre del fichero', size=256),
        'info': fields.text('Información'),
    }

    _defaults = {
        'file_name': lambda *a: 'liquidaciones_bajas_cambios.xlsx',
        'state': lambda *a: 'init'
    }


WizardExcelBajasCambiosLiquidaciones()