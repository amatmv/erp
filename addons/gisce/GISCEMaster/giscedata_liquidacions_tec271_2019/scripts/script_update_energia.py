# -*- coding: utf-8 -*-
import json

import click
from erppeek import Client
from tqdm import tqdm


'''Script Comer que marca les factures de gestion cobrador com a impagades
quan la data de venciment és igual al dia anterior a l'execució de l'script'''

@click.command()
@click.option('-u', '--user_comer', default='admin', help='Usuari servidor ERP COMER')
@click.option('-w', '--password_comer', default=None, help='Contrasenya usuari ERP COMER')
@click.option('-s', '--server_comer', default='localhost', help=u'IP servidor ERP COMER')
@click.option('-p', '--port_comer', default=8069, help='Port servidor ERP COMER', type=click.INT)
@click.option('-d', '--database_comer', default='db_name', help='Nom de la base de dades de COMER')
def revisar_energia_extra_lines(**kwargs):

    server = '{0}:{1}'.format(kwargs['server_comer'],
                                           kwargs['port_comer'])
    client = Client(server=server,
                      db=kwargs['database_comer'],
                      user=kwargs['user_comer'],
                      password=kwargs['password_comer'])
    
    prod_obj = client.ProductProduct
    prod_id = prod_obj.search([('default_code', '=', 'TEC271')])
    if not prod_id:
        print 'Producte Sup TEC no trobat'
        return False
    
    prod_id = prod_id[0]
    ex_obj = client.model('giscedata.facturacio.extra')
    extras_ids = ex_obj.search([('product_id', '=', prod_id)])
    for extra in tqdm(ex_obj.read(extras_ids, ['polissa_id','notes'])):
        try:
            #if extra['polissa_id'][0] == 876:
            energy = get_energy_notes(extra['notes'])
            update_liquidacio(client, extra['polissa_id'][0], energy)
        except KeyboardInterrupt as e:
            return False
        except Exception as e:
            print ('Failed', extra)
    
    return True


def get_energy_notes(full_note):
    # Expect full_note to be any string but only will parse and return
    # something if contains JSON brackets

    start = '<JSON>'
    end = '</JSON>'
    # This finds substring between JSON brackets
    json_parsed_from_note_str = (
        full_note[full_note.find(start) + len(start):full_note.rfind(end)]
    )

    if not json_parsed_from_note_str:
        return False

    # Convert string json format to dict
    json_parsed_from_note = json.loads(
        json_parsed_from_note_str
    )
    energia = json_parsed_from_note[u'energia_comentari']
    if energia.strip():
        energies = [''.join(x) for x in list(zip(*[iter(energia)]*14))]
        return sum([float(x)/100.0 for x in energies if x.replace(' ', '')])
    else:
        return 0.0


def update_liquidacio(client, polissa_id, energy):
    pol_obj = client.GiscedataPolissa
    liq_obj = client.model('giscedata.liquidacio.suplement.territorial.tec271.data')
    polissa = pol_obj.browse(polissa_id)
    liq_ids = liq_obj.search([(
        'cups', '=like', '{}%'.format(polissa.cups.name[:20])
    )])
    
    for liq in liq_obj.read(liq_ids, ['proporcio', 'tarifa_id']):
        if liq['proporcio'] != 1.0:
            print liq
        liq_id = liq['id']
        prop_energy = liq['proporcio'] * energy
        liq_obj.write([liq_id], {
            'energia_total': prop_energy
        })

if __name__ == '__main__':
    revisar_energia_extra_lines()
