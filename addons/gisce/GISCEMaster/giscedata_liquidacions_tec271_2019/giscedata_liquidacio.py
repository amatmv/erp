# -*- coding: utf-8 -*-
from __future__ import division

from osv import osv, fields
from datetime import datetime


DATA_COBRAMENT_LINIES = datetime(year=2017, month=8, day=31)


class GiscedataLiquidacioSuplementTerritorialTEC271Data(osv.osv):
    _name = 'giscedata.liquidacio.suplement.territorial.tec271.data'

    def update_tipus_client(self, cursor, uid, context=False):
        pol_obj = self.pool.get('giscedata.polissa')
        normal = {'tipus': '0'}
        baixa = {'tipus': '1'}
        canvi = {'tipus': '2'}
        ids = self.search(cursor, uid, [])
        for setu in self.read(
                cursor, uid, ids, ['cups_id', 'polissa_id', 'tipus']):
            setu_id = setu['id']
            if not setu['polissa_id']:
                vals = baixa
            else:
                polissa_id = setu['polissa_id'][0]
                pol = pol_obj.read(cursor, uid, polissa_id, ['data_baixa'])
                if pol['data_baixa'] and pol['data_baixa'] < '01-08-2018':
                    vals = baixa
                else:
                    modcons = pol_obj.get_modcontractual_intervals(
                        cursor, uid,
                        polissa_id, '2017-07-01', '2018-06-30',
                        context={'ffields': ['titular']})
                    if len(modcons) > 1:
                        vals = canvi
                    else:
                        vals = normal

            self.write(cursor, uid, setu_id, vals)

    def get_data_cobrament_linies(self, cursor, uid, context=None):
        return datetime(year=2017, month=8, day=31)

    def _tgr_proporcio_liquidacio(self, cursor, uid, ids, context=None):
        res = {}
        for cups in self.read(cursor, uid, ids, ['cups_id', 'cups']):
            cups_id = cups['cups_id']
            if cups_id:
                cups_id = cups_id[0]
                for setu_id in self.search(cursor, uid, [('cups_id', '=', cups_id)]):
                    res[setu_id] = True
            else:
                if cups['cups']:
                    for setu_id in self.search(cursor, uid, [('cups', '=', cups['cups'])]):
                        res[setu_id] = True
        return res

    def get_proporcio_liquidacio_cups(self, cursor, uid, cups_id, cups_name, context=None):

        res = {}
        proporcio_total = 0
        total_import = 0
        if cups_id:
            linies_ids = self.search(cursor, uid, [('cups_id', '=', cups_id)])
        else:
            linies_ids = self.search(cursor, uid, [('cups', '=', cups_name)])
        if not linies_ids:
            return res

        fields_liquidacio = self.read(cursor, uid, linies_ids, ['import_total'])
        for linia_suplement in fields_liquidacio:
            total_import += linia_suplement['import_total']

        for dades_linia in fields_liquidacio:
            linia_id = dades_linia['id']
            try:
                if dades_linia == fields_liquidacio[-1]:
                    res[linia_id] = round(1 - proporcio_total, 2)
                    continue
                propi = dades_linia['import_total']
                proporcio_actual = round(propi / total_import, 2)
                proporcio_total += proporcio_actual
                res[linia_id] = proporcio_actual
            except Exception as e:
                res[linia_id] = 0.0
        return res

    def get_proporcio_liquidacio(
            self, cursor, uid, ids, field_name, arg, context=None):
        """
        Returns the proportion of the import relative to other lines with
            the same CUPS
        :param cursor:  OpenERP Cursor
        :param uid:     OpenERP User ID
        :param ids: OpenERP Object IDs
        :param field_name: OpenERP Object field to calc
        :param arg: OpenERP Object field arguments to calc
        :param context: OpenERP Context
        :return: self.import_total/cups_id.import_total
        """
        if context is None:
            context = {}
        res = dict.fromkeys(ids, 0.0)
        for linia_id in ids:
            fields_liquidacio = self.read(
                cursor, uid, linia_id, ['cups', 'cups_id', 'import_total'])
            cups_liquidacio = fields_liquidacio['cups_id'][0] \
                if fields_liquidacio['cups_id'] \
                else fields_liquidacio['cups_id']

            res.update(
                self.get_proporcio_liquidacio_cups(
                    cursor, uid, cups_id=cups_liquidacio,
                    cups_name=fields_liquidacio['cups'],
                    context=context
                )
            )

        return res

    _STORE_PROPORCIO = {
        'giscedata.liquidacio.suplement.territorial.tec271.data': (
            _tgr_proporcio_liquidacio, ['cups_id'], 10
        )
    }

    _columns = {
        'cups_id': fields.many2one('giscedata.cups.ps', 'CUPS'),
        'cups': fields.text('CUPS'),
        'ccaa_id': fields.many2one(
            'res.comunitat_autonoma', 'Comunitat Autonoma'),
        'polissa_id': fields.many2one('giscedata.polissa', 'Polissa Cobrament'),
        'tarifa_id': fields.many2one(
            'giscedata.polissa.tarifa', 'Tarifa'
        ),
        'energia_total': fields.float('Energia 2013', digits=(16, 7)),
        'import_total': fields.float('Import Suplement'),
        'proporcio': fields.function(
            get_proporcio_liquidacio, string='Proporció',
            type='float', method=True, store=_STORE_PROPORCIO
        ),
        'tipus': fields.selection(
            [
                ('0', 'Normal'),
                ('1', 'Baixa'),
                ('2', 'Canvi Titularitat'),
            ], 'Tipus de Client', readonly=True
        ),
    }

    _defaults = {
        'energia_total': lambda *a: 0.0,
        'import_total': lambda *a: 0.0,
        'tipus': lambda *a: '0'
    }


GiscedataLiquidacioSuplementTerritorialTEC271Data()
