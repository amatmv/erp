# -*- coding: utf-8 -*-
{
  "name": "Liquidació de suplements territorials TEC/271/2019",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Permet imprimir les liquidacions dels suplements territorials TEC/271/2019
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": ['base', 'giscedata_facturacio', 'giscedata_contractacio_distri', 'giscedata_liquidacio'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['wizard/wizard_crear_xml_suplement_tec271_2019_view.xml', 'giscedata_liquidacio_view.xml', 'wizard/wizard_excel_bajas_cambios_liquidaciones_view.xml', 'security/ir.model.access.csv'],
  "active": False,
  "installable": True
}
