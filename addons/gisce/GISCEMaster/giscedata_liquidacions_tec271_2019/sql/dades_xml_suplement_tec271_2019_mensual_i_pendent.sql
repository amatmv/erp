SELECT
  ca.codi AS "ccaa",
  liq_tarf.codi AS tarifa,
  import.num_clients::text AS num_clients,
  energia.energia::text AS energia,
  import.import::text AS import
FROM giscedata_liquidacio_suplement_territorial_tec271_data AS tec271
INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
INNER JOIN giscedata_liquidacio_tarifes liq_tarf ON tarifa.tarifa_liquidacio = liq_tarf.id
INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
INNER JOIN (
  SELECT
    ca.codi AS ccaa,
    liq_tarf.codi AS tarifa,
    ROUND(
        SUM(
            tec271.energia_total /
            (CASE
                WHEN
                (
                    tec271.import_total > 2.0
                    and
                    tec271.import_total <= 12.12
                ) THEN tec271.import_total::INTEGER
                WHEN tec271.import_total > 12.12 THEN 12
                ELSE 1
            END)
            * tec271.proporcio
            * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END
        ), 0
    )
    AS energia
  FROM giscedata_facturacio_factura AS f
  INNER JOIN account_invoice AS inv ON (inv.id = f.invoice_id)
  INNER JOIN giscedata_liquidacio_suplement_territorial_tec271_data AS tec271 ON f.cups_id = tec271.cups_id
  INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
  INNER JOIN giscedata_liquidacio_tarifes liq_tarf ON tarifa.tarifa_liquidacio = liq_tarf.id
  INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
  WHERE tec271.proporcio != 0
  AND tec271.cups_id IN (
    SELECT DISTINCT f.cups_id
    FROM account_invoice_line AS inv_line
    INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
    INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
    INNER JOIN giscedata_liquidacio_fpd as period_liq on (f.periode_liquidacio = period_liq.id and %(data_inici)s BETWEEN period_liq.inici and period_liq.final)
    WHERE inv.state IN ('open', 'paid')
    AND inv_line.product_id = %(product_id)s
  )
  GROUP BY ca.codi, liq_tarf.codi
) AS energia ON (
  energia.ccaa = ca.codi
  AND energia.tarifa = liq_tarf.codi
)
INNER JOIN (
  SELECT
    foo.ccaa AS "ccaa",
    foo.tarifa AS "tarifa",
    COUNT(DISTINCT foo.client) AS num_clients,
    ROUND(SUM(foo.import), 2) AS import
   FROM (
    SELECT
      ca.codi AS "ccaa",
      liq_tarf.codi AS "tarifa",
      inv.partner_id AS client,
      SUM((inv_line.price_subtotal * tec271.proporcio) * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END) AS "import"
    FROM account_invoice_line AS inv_line
    INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
    INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
    INNER JOIN giscedata_liquidacio_suplement_territorial_tec271_data AS tec271 ON f.cups_id = tec271.cups_id
    INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
    INNER JOIN giscedata_liquidacio_tarifes liq_tarf ON tarifa.tarifa_liquidacio = liq_tarf.id
    INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
    INNER JOIN giscedata_liquidacio_fpd as period_liq on (f.periode_liquidacio = period_liq.id and %(data_inici)s BETWEEN period_liq.inici and period_liq.final)
    WHERE inv.state IN ('open', 'paid')
    AND inv_line.product_id = %(product_id)s
    GROUP BY ca.codi, liq_tarf.codi, inv.partner_id
    UNION (
      SELECT
        ca.codi AS "ccaa",
        liq_tarf.codi AS "tarifa",
        pol.id AS client,
        SUM(extra.total_amount_pending * tec271.proporcio) AS "import"
      FROM giscedata_facturacio_extra AS extra
      INNER JOIN giscedata_polissa AS pol ON (pol.id = extra.polissa_id)
      INNER JOIN giscedata_liquidacio_suplement_territorial_tec271_data AS tec271 ON pol.cups = tec271.cups_id
      INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
      INNER JOIN giscedata_liquidacio_tarifes liq_tarf ON tarifa.tarifa_liquidacio = liq_tarf.id
      INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
      WHERE extra.product_id = %(product_id)s
      AND extra.total_amount_pending != 0
      -- AND tec271.proporcio != 0
      AND pol.state = 'activa'
      GROUP BY ca.codi, liq_tarf.codi, pol.id
    )
  ) AS foo
  GROUP BY foo.ccaa, foo.tarifa
) AS import ON (
  import.ccaa = ca.codi
  AND import.tarifa = liq_tarf.codi
)
WHERE tec271.proporcio != 0
AND tec271.cups_id IN (
  SELECT DISTINCT f.cups_id
  FROM account_invoice_line AS inv_line
  INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
  INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
  INNER JOIN giscedata_liquidacio_fpd as period_liq on (f.periode_liquidacio = period_liq.id and %(data_inici)s BETWEEN period_liq.inici and period_liq.final)
  WHERE inv.state IN ('open', 'paid')
  AND inv_line.product_id = %(product_id)s
)
GROUP BY ca.codi, liq_tarf.codi, import.num_clients, energia.energia, import.import
ORDER BY tarifa
