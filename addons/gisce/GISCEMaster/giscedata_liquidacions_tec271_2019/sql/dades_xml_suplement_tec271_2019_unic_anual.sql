SELECT
  ca.codi AS "ccaa",
  CASE
    WHEN liq_tarf.codi = (
      SELECT codi FROM giscedata_liquidacio_tarifes WHERE NAME = '6.1A'
    )
    THEN (SELECT codi FROM giscedata_liquidacio_tarifes WHERE NAME = '6.1.A')
    ELSE liq_tarf.codi
  END AS "tarifa",
  tec271.tipus AS "tipus_client",
  --%(product_id)s
  COUNT(DISTINCT tec271.cups) AS num_clients,
  ROUND(SUM(tec271.energia_total), 0) AS energia,
  ROUND(SUM(tec271.import_total::decimal), 2) AS import
FROM giscedata_liquidacio_suplement_territorial_tec271_data AS tec271
INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
INNER JOIN giscedata_liquidacio_tarifes liq_tarf ON tarifa.tarifa_liquidacio = liq_tarf.id
INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
WHERE tec271.proporcio != 0
GROUP BY ca.codi, liq_tarf.codi, tec271.tipus
ORDER BY tarifa, tec271.tipus
