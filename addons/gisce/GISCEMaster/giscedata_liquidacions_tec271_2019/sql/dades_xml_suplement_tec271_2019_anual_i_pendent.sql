SELECT
  ca.codi AS "ccaa",
  tarifa.codi_ocsum AS tarifa,
  tec271.tipus AS "tipus_client",
  import.num_clients,
  energia.energia AS energia,
  import.import AS import
FROM giscedata_liquidacio_suplement_territorial_tec271_data AS tec271
INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
INNER JOIN (
  SELECT
    ca.codi AS "ccaa",
    tarifa.codi_ocsum AS "tarifa",
    tec271.tipus AS "tipus_client",
    ROUND(SUM(tec271.energia_total), 0) AS "energia"
  FROM giscedata_liquidacio_suplement_territorial_tec271_data AS tec271
  INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
  INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
  WHERE tec271.proporcio != 0
  AND tec271.cups_id IN (
    SELECT DISTINCT f.cups_id
    FROM account_invoice_line AS inv_line
    INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
    INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
    WHERE inv.date_invoice >= '2017-08-01'
    AND inv.state IN ('open', 'paid')
    AND inv_line.product_id = %(product_id)s
  )
  GROUP BY ca.codi, tarifa.codi_ocsum, tec271.tipus
) AS energia ON (
      energia.ccaa = ca.codi
      AND energia.tarifa = tarifa.codi_ocsum
      AND energia.tipus_client = tec271.tipus
      )
INNER JOIN (
  SELECT
    foo.ccaa AS "ccaa",
    foo.tarifa AS "tarifa",
    foo.tipus_client AS "tipus_client",
    COUNT(DISTINCT foo.client) AS num_clients,
    ROUND(SUM(foo.import), 2) AS import
   FROM (
    SELECT
      ca.codi AS "ccaa",
      tarifa.codi_ocsum AS "tarifa",
      tec271.tipus AS "tipus_client",
      inv.partner_id AS client,
      SUM((inv_line.price_subtotal * tec271.proporcio) * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END) AS "import"
    FROM account_invoice_line AS inv_line
    INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
    INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
    INNER JOIN giscedata_liquidacio_suplement_territorial_tec271_data AS tec271 ON f.cups_id = tec271.cups_id
    INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
    INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
    WHERE inv.date_invoice >= '2017-08-01'
    AND inv.state IN ('open', 'paid')
    AND inv_line.product_id = %(product_id)s
    GROUP BY ca.codi, tarifa.codi_ocsum, tec271.tipus, inv.partner_id
    UNION (
      SELECT
        ca.codi AS "ccaa",
        tarifa.codi_ocsum AS "tarifa",
        tec271.tipus AS "tipus_client",
        pol.id AS client,
        SUM(extra.total_amount_pending * tec271.proporcio) AS "import"
      FROM giscedata_facturacio_extra AS extra
      INNER JOIN giscedata_polissa AS pol ON (pol.id = extra.polissa_id)
      INNER JOIN giscedata_liquidacio_suplement_territorial_tec271_data AS tec271 ON pol.cups = tec271.cups_id
      INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
      INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
      WHERE extra.product_id = %(product_id)s
      AND extra.total_amount_pending != 0
      -- AND tec271.proporcio != 0
      AND pol.state = 'activa'
      GROUP BY ca.codi, tarifa.codi_ocsum, tec271.tipus, pol.id
    )
  ) AS foo
  GROUP BY foo.ccaa, foo.tarifa, foo.tipus_client
) AS import ON (
      import.ccaa = ca.codi
      AND import.tarifa = tarifa.codi_ocsum
      AND import.tipus_client = tec271.tipus
      )
WHERE tec271.proporcio != 0
AND tec271.cups_id IN (
  SELECT DISTINCT f.cups_id
  FROM account_invoice_line AS inv_line
  INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
  INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
  WHERE inv.date_invoice >= '2017-08-01'
  AND inv.state IN ('open', 'paid')
  AND inv_line.product_id = %(product_id)s
)
GROUP BY ca.codi, tarifa.codi_ocsum, tec271.tipus, import.num_clients, energia.energia, import.import
ORDER BY tarifa, tec271.tipus
