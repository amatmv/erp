SELECT
  -- %(data_fi)s
  ca.codi AS "ccaa",
    CASE
    WHEN liq_tarf.codi = (
      SELECT codi FROM giscedata_liquidacio_tarifes WHERE NAME = '6.1A'
    )
    THEN (SELECT codi FROM giscedata_liquidacio_tarifes WHERE NAME = '6.1.A')
    ELSE liq_tarf.codi
  END AS "tarifa",
  import.num_clients::text AS num_clients,
  import.energia::text AS energia,
  import.import::text AS import
FROM giscedata_liquidacio_suplement_territorial_tec271_data AS tec271
INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
INNER JOIN giscedata_liquidacio_tarifes liq_tarf ON tarifa.tarifa_liquidacio = liq_tarf.id
INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
INNER JOIN (
  SELECT
    ca.codi AS "ccaa",
    liq_tarf.codi AS "tarifa",
    COUNT(DISTINCT f.polissa_id) AS num_clients,
    ROUND(SUM((inv_line.price_subtotal * tec271.proporcio) * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END), 2) AS "import",
    ROUND(
        SUM(
            tec_total.energia_total /
            (CASE
                WHEN
                (
                    tec_total.import_total > 2.0
                    and
                    tec_total.import_total <= 12.12
                ) THEN tec_total.import_total::INTEGER
                WHEN tec_total.import_total > 12.12 THEN 12
                ELSE 1
            END)
            * tec271.proporcio
            * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END
        ), 0
    )
    AS energia
  FROM account_invoice_line AS inv_line
  INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
  INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
  INNER JOIN giscedata_liquidacio_suplement_territorial_tec271_data AS tec271 ON f.cups_id = tec271.cups_id
  INNER JOIN (
    SELECT cups_id, SUM(energia_total) AS energia_total, SUM(import_total) AS import_total
    FROM giscedata_liquidacio_suplement_territorial_tec271_data
    GROUP BY cups_id
  ) AS tec_total ON f.cups_id = tec_total.cups_id
  INNER JOIN giscedata_polissa_tarifa AS tarifa ON tarifa.id = tec271.tarifa_id
  INNER JOIN giscedata_liquidacio_tarifes liq_tarf ON tarifa.tarifa_liquidacio = liq_tarf.id
  INNER JOIN res_comunitat_autonoma AS ca ON ca.id = tec271.ccaa_id
  INNER JOIN giscedata_liquidacio_fpd as period_liq on (f.periode_liquidacio = period_liq.id and year = to_char(date_part('year', timestamp %(data_inici)s), 'fm0000') and month=to_char(date_part('month', timestamp %(data_inici)s), 'fm00'))
  WHERE inv.state IN ('open', 'paid')
  AND inv_line.product_id = %(product_id)s
  GROUP BY ca.codi, liq_tarf.codi
) AS import ON (
  import.ccaa = ca.codi
  AND import.tarifa = liq_tarf.codi
)
WHERE tec271.proporcio != 0
AND tec271.cups_id IN (
  SELECT DISTINCT f.cups_id
  FROM account_invoice_line AS inv_line
  INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
  INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
  INNER JOIN giscedata_liquidacio_fpd as period_liq on (f.periode_liquidacio = period_liq.id and year = to_char(date_part('year', timestamp %(data_inici)s), 'fm0000') and month=to_char(date_part('month', timestamp %(data_inici)s), 'fm00'))
  WHERE inv.state IN ('open', 'paid')
  AND inv_line.product_id = %(product_id)s
)
GROUP BY ca.codi, liq_tarf.codi, import.num_clients, import.energia, import.import
ORDER BY tarifa
