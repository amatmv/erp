from destral import testing
from destral.transaction import Transaction
import base64
from lxml import objectify
from datetime import datetime
import unittest


class TestWizardXMLSuplementTerritorialTEC271(testing.OOTestCase):
    @unittest.skip('To be redone')
    def test_create_xml_mensual(self):
        wiz_obj = self.openerp.pool.get(
            'wizard.crear.xml.suplement.territorial.tec271'
        )
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        tec_obj = self.openerp.pool.get(
            'giscedata.liquidacio.suplement.territorial.tec271.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        linia_fact_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura.linia')
        product_obj = self.openerp.pool.get('product.product')
        account_obj = self.openerp.pool.get('account.account')
        imd_obj = self.openerp.pool.get('ir.model.data')
        user_obj = self.openerp.pool.get('res.users')
        company_obj = self.openerp.pool.get('res.company')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            user = user_obj.browse(cursor, uid, uid)
            company_id = user.company_id.id
            company_obj.write(cursor, uid, [company_id], {'codi_r1': '9999'})

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            product_id = product_obj.search(
                cursor, uid, [('default_code', '=', 'TEC271')])[0]
            account_id = account_obj.search(cursor, uid, [])[0]
            product = product_obj.browse(cursor, uid, product_id)
            values_linia = {
                'factura_id': fact_id,
                'tipus': 'altres',
                'quantity': 1,
                'price_unit_multi': 100,
                'account_id': account_id,
                'product_id': product_id,
                'name': product.name,
                'data_desde': '2017-01-01',
                'data_fins': '2017-12-31'
            }
            linia_id = linia_fact_obj.create(cursor, uid, values_linia)

            polissa_fact = fact_obj.read(
                cursor, uid, fact_id, ['polissa_id'])['polissa_id'][0]
            cups_id = polissa_obj.read(
                cursor, uid, polissa_fact, ['cups'])['cups'][0]
            tarifa_id = tarifa_obj.search(
                cursor, uid, [('codi_ocsum', '=', '001')]
            )[0]  # 2.0A

            tec_obj.create(cursor, uid, {
                'cups_id': cups_id,
                'polissa_id': polissa_fact,
                'tarifa_id': tarifa_id,
                'import_total': 1,
                'tipus': '0',
            })

            any_abonament = '2017'
            any_abon_num = int(any_abonament)
            mes_abonament = '03'
            mes_abon_num = int(mes_abonament)
            date_invoice = datetime(
                any_abon_num, mes_abon_num, 15
            ).strftime('%Y-%m-%d')
            values_factura = {'date_invoice': date_invoice}
            fact_obj.write(cursor, uid, fact_id, values_factura)

            context = {'active_ids': [1], 'tipus_xml': 'mensual'}
            wiz_vals = {
                'any_abonament': any_abonament,
                'mes_abonament': mes_abonament
            }
            wiz_id = wiz_obj.create(cursor, uid, wiz_vals, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            wiz.action_generate_xml(context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            xml_string = base64.decodestring(wiz.xml)

            xml_obj = objectify.fromstring(xml_string)

            self.assertEqual(xml_obj.CONSUMO.STCM_CODIGO_DISTRIBUIDORA, 9999)
            self.assertEqual(xml_obj.CONSUMO.STCM_ANIO_ABONO, any_abon_num)
            self.assertEqual(xml_obj.CONSUMO.STCM_MES_ABONO, mes_abon_num)
            self.assertEqual(xml_obj.CONSUMO.STCM_ANIO_CONSUMO, 2013)
            self.assertEqual(xml_obj.CONSUMO.STCM_CODIGO_CCAA, 18)
            self.assertEqual(xml_obj.CONSUMO.STCM_CODIGO_TARIFA, '001')
            self.assertEqual(xml_obj.CONSUMO.STCM_NUM_CLIENTES, 1)
            self.assertEqual(xml_obj.CONSUMO.STCM_TOTAL_ENERGIA, 10)
            self.assertEqual(xml_obj.CONSUMO.STCM_TOTAL_IMPORTE, 100.00)
