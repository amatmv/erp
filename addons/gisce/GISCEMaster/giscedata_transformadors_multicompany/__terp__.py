# -*- coding: utf-8 -*-
{
    "name": "GISCE Transformadors multicompany",
    "description": """Multi-company support for Transformadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscedata_transformadors"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_transformadors_multicompany_view.xml"
    ],
    "active": False,
    "installable": True
}
