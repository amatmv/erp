# -*- coding: iso-8859-1 -*-
from osv import osv, fields

class giscedata_transformador_trafo(osv.osv):
	_name = "giscedata.transformador.trafo"
	_inherit = "giscedata.transformador.trafo"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}
	_defaults = {
		#'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}
giscedata_transformador_trafo()

class giscedata_transformador_magatzem(osv.osv):
	_name = "giscedata.transformador.magatzem"
	_inherit = "giscedata.transformador.magatzem"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}
	_defaults = {
		#'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}
giscedata_transformador_magatzem()
