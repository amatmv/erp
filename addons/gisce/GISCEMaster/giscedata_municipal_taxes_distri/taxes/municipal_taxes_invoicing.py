# -*- coding: utf-8 -*-
import StringIO
import csv
import xlwt
import xml.etree.ElementTree as ET

from addons import get_module_resource
from tools.translate import _
from giscedata_facturacio_distri.sql.sql_column_titles import QUERY_RESUM_DISTRI_TITLES

NO_MUNICIPIS = _(
    "No hi ha factures per l\'interval i els municipis seleccionats"
)

INVOICES_TITLES = [
    _('Distribuidora'),     # distri.ref
    _('Nom Factura'),       # invoice.reference
    _(u'Any'),              # QUARTER + 'T.' + YEAR (from invoice.date_invoice)
    _('CUPS'),              # cups.name
    _('Client'),            # polissa.name
    _('Exempte impost municipal'),  # exempte impost municipal
    _('Nom'),               # titular.name
    _('Direccio'),          # cups.direccio
    _('Tarifa'),            # tarifa.name
    _('Data Factura'),      # invoice.date_invoice
    _('Data Inici'),        # invoice.date_inici
    _('Data Fi'),           # invoice.date_final
    _('Total brut'),        # SUM(invoice_line.price_subtotal)
    _('IVA'),               # SUM(invoice_line.price_subtotal*tax.amount)
    _('Total'),             # Sum of two previous
]

SUMMARY_TITLES_1 = [
    _('Any'),
    _('Trimestre'),
    _('Pagaments a distribuidora'),
    _('Factures a clients'),
]

SUMMARY_TITLES_2 = [
    '', '', '', '',
    _('Ingresos bruts'),
    _('Tasa'),
    _('Total'),
]


def get_invoice_line(invoice):
    preus = invoice[1].split(',')
    brut = float(preus[0])
    iva = round(float(preus[1]), 2)
    res = [
        invoice[12],     # distri.ref
        str(invoice[0]),      # invoice.reference
        str(int(invoice[3])) + 'T.' + str(int(invoice[2])),  # any
        invoice[5],      # cups.name
        invoice[4],      # polissa.name
        invoice[14],     # exempt municipal tax
        invoice[7],      # titular.name
        invoice[6],      # cups.direccio
        invoice[8],      # tarifa.name
        invoice[9],      # invoice.date_invoice
        invoice[10],     # factura.data_inici
        invoice[11],     # factura.data_final
        brut,            # SUM(invoice_line.price_subtotal)
        iva,             # SUM(invoice_line.price_subtotal*tax.amount)
        brut + iva,      # Sum of two previous
    ]

    return res


def parse_invoices(invoices):
    for invoice in invoices:
        yield get_invoice_line(invoice)


def get_quarter_line(quarter):
    res = [
        quarter['year'],
        quarter['quarter'],
        round(quarter['provider_amount'], 2),
        round(quarter['client_amount'], 2)
    ]

    return res


def get_total_line(total_client, total_provider, tax):
    diff = total_client - total_provider

    res = [
        'Total',
        '',
        round(total_provider, 2),
        round(total_client, 2),
        round(diff, 2),
        tax,
        round(diff * (tax / 100.0), 2)
    ]

    return res


def write_table_csv(report, table_titles, table_contents):
    report.writerow(table_titles)
    for table_item in table_contents:
        report.writerow(table_item)


def write_invoices_table_csv(report, table_title, invoices):
    # First we write the town name
    report.writerow([table_title])
    # Then we write the table itself
    write_table_csv(report, INVOICES_TITLES, parse_invoices(invoices))

    report.writerow([])


def write_row_xls(sheet, row_line, row, style=None):
    if not style:
        style = xlwt.Style.default_style

    c = 0
    for cell in row:
        sheet.write(row_line, c, cell, style=style)
        c += 1

    return row_line + 1


def write_table_xls(sheet, row_line, table_titles, table_contents,
                    table_titles_style=None):
    row_line = write_row_xls(sheet, row_line, table_titles, table_titles_style)
    for table_item in table_contents:
        row_line = write_row_xls(sheet, row_line, table_item)

    return row_line


def write_invoices_table_xls(sheet, row_line, name, invoices,
                             title_style, table_title_style):
    sheet.write(row_line, 0, name, style=title_style)
    sheet.row(row_line).height = 320
    row_line += 2

    row_line = write_table_xls(
        sheet, row_line, INVOICES_TITLES, parse_invoices(invoices),
        table_title_style
    )

    return row_line


class MunicipalTaxesInvoicingReport:
    def __init__(self, cursor, uid, start_date, end_date, tax, file_type,
                 inv_list, polissa_categ_imu_ex_id, whereiam, invoiced_states):
        self.cursor = cursor
        self.uid = uid
        self.start_date = start_date
        self.end_date = end_date
        self.tax = tax
        self.file_type = file_type
        self.inv_list = inv_list
        self.invoicing_by_name = {}
        self.polissa_categ_imu_ex_id = polissa_categ_imu_ex_id
        self.whereiam = whereiam
        self.invoiced_states = invoiced_states

    def build_report_taxes(self, ids):
        records = self.get_totals_by_city(ids)

        for record in records:
            name = record[0]
            self.invoicing_by_name.setdefault(name, {
                'total_provider_amount': 0,
                'total_client_amount': 0,
                'quarters': [],
                'ine': '0',
            })

            done_search = 'client_invoices_ids' in self.invoicing_by_name[name]
            if self.inv_list and not done_search:
                # We only do the search once for town because it's quite long
                prov_inv = self.get_provider_invoices_taxes(name)
                client_inv = self.get_client_invoices_taxes(name)
                self.invoicing_by_name[name].update({
                    'provider_invoices': prov_inv,
                    'client_invoices': client_inv,
                    'provider_invoices_ids': [str(inv[13]) for inv in prov_inv],
                    'client_invoices_ids': [str(inv[13]) for inv in client_inv],
                    # The ids of the invoices are in the position 13 of the
                    # returned list, so we don't need another query
                })
            elif not done_search:
                # If we won't need the full invoices we don't do the long query,
                # but this shorter one
                prov_inv = self.get_provider_invoices_ids(name)
                client_inv = self.get_client_invoices_ids(name)
                self.invoicing_by_name[name].update({
                    'provider_invoices_ids': [str(inv[0]) for inv in prov_inv],
                    'client_invoices_ids': [str(inv[0]) for inv in client_inv],
                })
            self.invoicing_by_name[name]['total_provider_amount'] += record[3]
            self.invoicing_by_name[name]['total_client_amount'] += record[4]
            self.invoicing_by_name[name]['quarters'].append({
                'year': record[1],
                'quarter': record[2],
                'provider_amount': record[3],
                'client_amount': record[4]
            })
            self.invoicing_by_name[name]['ine'] = record[5]

        if self.file_type == "csv":
            return self.build_report_taxes_csv()
        elif self.file_type == "xml":
            return self.build_report_taxes_xml_xaloc()
        else:
            return self.build_report_taxes_xls()

    def build_report_taxes_csv(self):
        output_report = StringIO.StringIO()
        writer_report = csv.writer(output_report, quoting=csv.QUOTE_NONNUMERIC)

        if not self.invoicing_by_name:
            writer_report.writerow([NO_MUNICIPIS])
        else:
            for name, v in sorted(self.invoicing_by_name.items()):
                writer_report.writerow([name])
                writer_report.writerow(SUMMARY_TITLES_1)
                for quarter in v['quarters']:
                    writer_report.writerow(get_quarter_line(quarter))

                writer_report.writerow([])
                writer_report.writerow(SUMMARY_TITLES_2)
                writer_report.writerow(
                    get_total_line(
                        v['total_client_amount'], v['total_provider_amount'],
                        self.tax
                    )
                )

                if self.inv_list:
                    writer_report.writerow([])
                    write_invoices_table_csv(
                        writer_report, _('Factures proveidors'),
                        v['provider_invoices']
                    )
                    write_invoices_table_csv(
                        writer_report, _('Factures clients'),
                        v['client_invoices']
                    )

                writer_report.writerow([])
                writer_report.writerow([])

        return output_report.getvalue()

    def build_report_taxes_xls(self):
        output_report = StringIO.StringIO()
        writer_report = xlwt.Workbook(encoding='utf-8')

        title_style = xlwt.XFStyle()
        title_font = xlwt.Font()
        title_font.bold = True
        title_font.underline = True
        title_font.height = 260
        title_style.font = title_font

        table_title_style = xlwt.XFStyle()
        table_title_font = xlwt.Font()
        table_title_font.bold = True
        table_title_style.font = table_title_font

        if not self.invoicing_by_name:
            writer_sheet = writer_report.add_sheet(_('Sense Factures'))
            writer_sheet.write(0, 0, NO_MUNICIPIS)
        else:
            resum_sheet = writer_report.add_sheet(_('Resum'))
            resum_sheet.col(2).width = 6500
            resum_sheet.col(3).width = 4500
            resum_sheet.col(4).width = 4500
            fila_resum = 0

            if self.inv_list:
                provider_sheet = writer_report.add_sheet(
                    _('Factures proveidors')
                )
                provider_sheet.col(0).width = 3100
                provider_sheet.col(1).width = 4100
                provider_sheet.col(3).width = 6300
                provider_sheet.col(5).width = 12000
                provider_sheet.col(6).width = 17000
                fila_provider = 0

                client_sheet = writer_report.add_sheet(_('Factures clients'))
                client_sheet.col(0).width = 3100
                client_sheet.col(1).width = 4100
                client_sheet.col(3).width = 6300
                client_sheet.col(5).width = 12000
                client_sheet.col(6).width = 17000
                fila_client = 0

            for name, v in sorted(self.invoicing_by_name.items()):
                # Data in the general sheet
                resum_sheet.write(fila_resum, 0, name, style=title_style)
                resum_sheet.row(fila_resum).height = 320
                fila_resum += 1
                fila_resum = write_row_xls(
                    resum_sheet, fila_resum, SUMMARY_TITLES_1,
                    style=table_title_style
                )

                for quarter in v['quarters']:
                    fila_resum = write_row_xls(
                        resum_sheet, fila_resum, get_quarter_line(quarter)
                    )

                fila_resum += 1  # Empty line
                fila_resum = write_row_xls(
                    resum_sheet, fila_resum, SUMMARY_TITLES_2,
                    style=table_title_style
                )

                fila_resum = write_row_xls(
                    resum_sheet, fila_resum, get_total_line(
                        v['total_client_amount'], v['total_provider_amount'],
                        self.tax
                    )
                )
                fila_resum += 2

                if self.inv_list:
                    # Data in the provider sheet
                    fila_provider = write_invoices_table_xls(
                        provider_sheet, fila_provider, name,
                        v['provider_invoices'],
                        title_style, table_title_style
                    )
                    fila_provider += 2

                    # Data in the client sheet
                    fila_client = write_invoices_table_xls(
                        client_sheet, fila_client, name, v['client_invoices'],
                        title_style, table_title_style
                    )
                    fila_client += 2

        writer_report.save(output_report)

        return output_report.getvalue()

    def build_report_taxes_xml_xaloc(self):
        """Format XML Diputació de Girona (XALOC)
	"""
        empresa = ET.Element('EMPRESA')
        empresa.set('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        datos = ET.SubElement(empresa,'DATOS')
        datos_nombre = ET.SubElement(datos, 'NOMBRE')
        company_data = self.get_company_data()
        datos_nombre.text = str(company_data[0][0])
        datos_nif = ET.SubElement(datos, 'NIF')
        datos_nif.text = str(company_data[0][1])[2:]
        municipios = ET.SubElement(empresa, "MUNICIPIOS")
        for name, v in sorted(self.invoicing_by_name.items()):
            municipio = ET.SubElement(municipios,"MUNICIPIO")
            municipio_ine = ET.SubElement(municipio,"INEMUNICIPIO")
            ejercicio = ET.SubElement(municipio,"EJERCICIO")
            periodo = ET.SubElement(municipio,"PERIODO")
            fecha_alta = ET.SubElement(municipio,"FECHAALTA")
            fecha_baja = ET.SubElement(municipio,"FECHABAJA")
            tiposumin = ET.SubElement(municipio,"TIPOSUMIN")
            descsum = ET.SubElement(municipio,"DESCSUM")
            descperi = ET.SubElement(municipio,"DESCPERI")
            facturacion = ET.SubElement(municipio,"FACTURACION")
            derechos_acceso = ET.SubElement(municipio,"DERECHOSACCESO")
            compensacion = ET.SubElement(municipio,"COMPENSACION")
            base_imponible = ET.SubElement(municipio,"BASEIMPONIBLE")
            cuota_basica = ET.SubElement(municipio,"CUOTABASICA")
            total_ingresar = ET.SubElement(municipio,"TOTALINGRESAR")
            municipio_ine.text = str(v['ine'])
            ejercicio.text = str(int(v['quarters'][0]['year']))
            periodo.text = str(int(v['quarters'][0]['quarter']))
            tiposumin.text = "2" #1=Gas, 2=Electrecity
            descsum.text = "Energia Electrica"
            descperi.text = str(int(v['quarters'][0]['quarter'])) + " Trimestre"
            importes = get_total_line(v['quarters'][0]['client_amount'], v['quarters'][0]['provider_amount'],self.tax)
            facturacion.text = str(importes[3]) #Importes facturas cliente final
            derechos_acceso.text = str(importes[2]) #Importes facutras distribuidora
            compensacion.text = "0" #Siempre 0
            base_imponible.text = str(importes[4]) #Resta facturas client-distribuidora
            cuota_basica.text = str(importes[6]) #Total a pagar
            total_ingresar.text = str(importes[6]) #Total a pagar

        return ET.tostring(empresa, encoding='utf8', method='xml')

    def build_report_providers(self):
        output_report = StringIO.StringIO()
        writer_report = csv.writer(output_report, quoting=csv.QUOTE_NONNUMERIC)

        for municipi in self.invoicing_by_name:
            provider_invoices_ids = \
                self.invoicing_by_name[municipi]['provider_invoices_ids']
            if provider_invoices_ids:
                provider_invoices = self.get_client_invoices_detail(
                    provider_invoices_ids
                )
                writer_report.writerow([municipi])
                write_table_csv(
                    writer_report, QUERY_RESUM_DISTRI_TITLES, provider_invoices
                )
                writer_report.writerow([])

        return output_report.getvalue()

    def build_report_clients(self):
        output_report = StringIO.StringIO()
        writer_report = csv.writer(output_report, quoting=csv.QUOTE_NONNUMERIC)

        for municipi in self.invoicing_by_name:
            client_invoices_ids = \
                self.invoicing_by_name[municipi]['client_invoices_ids']
            if client_invoices_ids:
                client_invoices = self.get_client_invoices_detail(
                    client_invoices_ids
                )

                writer_report.writerow([municipi])
                write_table_csv(
                    writer_report, QUERY_RESUM_DISTRI_TITLES, client_invoices
                )
                writer_report.writerow([])

        return output_report.getvalue()

    def get_totals_by_city(self, ids):
        sql = self.cursor.mogrify(
            '''
            SELECT
              municipi.name AS name,
              EXTRACT(YEAR FROM invoice.date_invoice) AS invoice_year,
              EXTRACT(QUARTER FROM invoice.date_invoice) AS invoice_quarter,
              COALESCE(SUM(invoice_line.price_subtotal::float*(
              CASE
                WHEN factura_line.tipus IN ('subtotal_xml', 'subtotal_xml_ene', 'subtotal_xml_pow', 'subtotal_xml_rea')
                  AND invoice_line.product_id IS NULL
                  AND invoice.type='in_invoice'  THEN 1
                WHEN factura_line.tipus IN ('subtotal_xml', 'subtotal_xml_ene', 'subtotal_xml_pow', 'subtotal_xml_rea')
                  AND invoice.type='in_refund'   THEN -1
                ELSE 0
              END
              )),0.0) AS provider_amount,
              COALESCE(SUM(invoice_line.price_subtotal::float*(
              CASE
                WHEN factura_line.tipus IN ('energia','reactiva','potencia', 'exces_potencia')
                  AND invoice.type='out_invoice' THEN 1
                WHEN factura_line.tipus IN ('energia','reactiva','potencia', 'exces_potencia')
                  AND invoice.type='out_refund'  THEN -1
                ELSE 0
              END
              )),0.0) AS client_amount,
              municipi.ine AS ine
              FROM giscedata_facturacio_factura_linia AS factura_line
              LEFT JOIN account_invoice_line AS invoice_line
                ON invoice_line.id = factura_line.invoice_line_id
              LEFT JOIN giscedata_facturacio_factura AS factura
                ON factura.id = factura_line.factura_id
              LEFT JOIN account_invoice AS invoice
                ON invoice.id = factura.invoice_id
              LEFT JOIN giscedata_polissa AS polissa
                ON polissa.id = factura.polissa_id
              LEFT JOIN giscedata_cups_ps AS cups
                ON cups.id = polissa.cups
              LEFT JOIN res_municipi AS municipi
                ON municipi.id = cups.id_municipi
              LEFT JOIN account_journal AS journal
                ON journal.id = invoice.journal_id
              WHERE municipi.id IN %(municipi_ids)s
                AND (invoice.date_invoice >= %(start_date)s
                  AND invoice.date_invoice < %(end_date)s)
                AND ((invoice.type LIKE %(out_invoice_type)s
                  AND invoice.state IN %(invoiced_states)s)
                  OR invoice.type LIKE %(in_invoice_type)s)
                AND polissa.id NOT IN (
                  SELECT polissa.id
                  FROM giscedata_polissa AS polissa
                  LEFT JOIN giscedata_polissa_category_rel AS polissa_categ_rel
                    ON polissa_categ_rel.polissa_id = polissa.id
                  LEFT JOIN giscedata_polissa_category AS pol_categ
                    ON pol_categ.id = polissa_categ_rel.category_id
                  WHERE pol_categ.id = %(pol_categ_exempt_id)s
                )
                AND journal.code LIKE '%%ENERGIA%%'
            GROUP BY municipi.name, invoice_year, invoice_quarter, ine
            ORDER BY municipi.name, invoice_year, invoice_quarter, ine
            ''', {
                'municipi_ids': tuple(ids),
                'start_date': self.start_date,
                'end_date': self.end_date,
                'out_invoice_type': 'out_%',
                'in_invoice_type': 'in_%',
                'invoiced_states': tuple(self.invoiced_states),
                'pol_categ_exempt_id': self.polissa_categ_imu_ex_id
            })

        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def get_provider_invoices_taxes(self, name):
        sql = '''
            SELECT
              COALESCE(invoice.reference, %(reference)s),
              COALESCE(
                (
                  SELECT SUM(invoice_line.price_subtotal::float*(
                    CASE
                      WHEN factura_line.tipus IN ('subtotal_xml', 'subtotal_xml_ene', 'subtotal_xml_pow', 'subtotal_xml_rea')
                        AND invoice_line.product_id IS NULL
                        AND invoice.type='in_invoice' THEN 1
                      WHEN factura_line.tipus IN ('subtotal_xml', 'subtotal_xml_ene', 'subtotal_xml_pow', 'subtotal_xml_rea')
                        AND invoice.type='in_refund' THEN -1
                      ELSE 0
                    END
                    )) || ',' ||
                    SUM(invoice_line.price_subtotal::float*tax.amount*(
                      CASE
                        WHEN factura_line.tipus IN ('subtotal_xml', 'subtotal_xml_ene', 'subtotal_xml_pow', 'subtotal_xml_rea')
                          AND invoice_line.product_id IS NULL
                          AND invoice.type='in_invoice' THEN 1
                        WHEN factura_line.tipus IN ('subtotal_xml', 'subtotal_xml_ene', 'subtotal_xml_pow', 'subtotal_xml_rea')
                          AND invoice.type='in_refund' THEN -1
                        ELSE 0
                      END
                    ))
                    FROM account_invoice_line AS invoice_line
                    LEFT JOIN giscedata_facturacio_factura_linia AS factura_line
                      ON invoice_line.id = factura_line.invoice_line_id
                    LEFT JOIN account_invoice_line_tax AS line_tax
                      ON line_tax.invoice_line_id = invoice_line.id
                    JOIN account_tax AS tax
                      ON line_tax.tax_id = tax.id
                        AND tax.name LIKE %(tax_name)s
                        AND tax.type = 'percent'
                    WHERE invoice_line.invoice_id = invoice.id
                ),'0,0'
              ) AS provider_amount,
              EXTRACT(YEAR FROM invoice.date_invoice) AS invoice_year,
              EXTRACT(QUARTER FROM invoice.date_invoice) AS invoice_quarter,
              polissa.name, cups.name, cups.direccio, titular.name, tarifa.name,
              invoice.date_invoice, factura.data_inici, factura.data_final,
              distri.ref, factura.id,
              CASE
                WHEN polissa.id IN (
                  SELECT polissa.id
                    FROM giscedata_polissa AS polissa
                    LEFT JOIN giscedata_polissa_category_rel AS polissa_categ_rel
                      ON polissa_categ_rel.polissa_id = polissa.id
                    LEFT JOIN giscedata_polissa_category AS pol_categ
                      ON pol_categ.id = polissa_categ_rel.category_id
                    WHERE pol_categ.id = %(pol_categ_exempt_id)s
                ) THEN 'Si'
                ELSE 'No'
              END AS municipal_tax_exempt
              FROM giscedata_facturacio_factura AS factura
              LEFT JOIN account_invoice AS invoice
                ON invoice.id = factura.invoice_id
              LEFT JOIN giscedata_polissa AS polissa
                ON polissa.id = factura.polissa_id
              LEFT JOIN giscedata_cups_ps AS cups
                ON cups.id = polissa.cups
              LEFT JOIN res_municipi AS municipi
                ON municipi.id = cups.id_municipi
              LEFT JOIN res_partner AS titular
                ON polissa.titular = titular.id
              LEFT JOIN giscedata_polissa_tarifa AS tarifa
                ON polissa.tarifa = tarifa.id
              LEFT JOIN res_partner AS distri
                ON distri.id = cups.distribuidora_id
              LEFT JOIN account_journal AS journal
                ON journal.id = invoice.journal_id
              WHERE municipi.name = %(municipi_name)s
                AND (invoice.date_invoice >= %(start_date)s
                  AND invoice.date_invoice < %(end_date)s)
                AND invoice.type LIKE %(invoice_type)s
                AND journal.code LIKE 'CENERGIA%%'
              ORDER BY distri.ref, titular.name, provider_amount,
              invoice.date_invoice, invoice.reference
            '''

        self.cursor.execute(
            sql, {
                'reference': _('Altres'),
                'tax_name': '%IVA%',
                'pol_categ_exempt_id': self.polissa_categ_imu_ex_id,
                'municipi_name': name,
                'start_date': self.start_date,
                'end_date': self.end_date,
                'invoice_type': 'in_%',
            }
        )

        return self.cursor.fetchall()

    def get_client_invoices_taxes(self, name):
        sql = '''
            SELECT
              COALESCE(invoice.number, %(reference)s),
              COALESCE(
                (
                  SELECT SUM(invoice_line.price_subtotal::float*(
                    CASE
                      WHEN factura_line.tipus IN ('energia','reactiva','potencia', 'exces_potencia')
                        AND invoice.type='out_invoice' THEN 1
                      WHEN factura_line.tipus IN ('energia','reactiva','potencia', 'exces_potencia')
                        AND invoice.type='out_refund'  THEN -1
                      ELSE 0
                    END
                    )) || ',' ||
                    SUM(invoice_line.price_subtotal::float*tax.amount*(
                      CASE
                        WHEN factura_line.tipus IN ('energia','reactiva','potencia', 'exces_potencia')
                          AND invoice.type='out_invoice' THEN 1
                        WHEN factura_line.tipus IN ('energia','reactiva','potencia', 'exces_potencia')
                          AND invoice.type='out_refund'  THEN -1
                        ELSE 0
                      END
                    ))
                    FROM account_invoice_line AS invoice_line
                    LEFT JOIN giscedata_facturacio_factura_linia AS factura_line
                      ON invoice_line.id = factura_line.invoice_line_id
                    LEFT JOIN account_invoice_line_tax AS line_tax
                      ON line_tax.invoice_line_id = invoice_line.id
                    JOIN account_tax AS tax
                      ON line_tax.tax_id = tax.id
                        AND tax.name LIKE %(tax_name)s
                        AND tax.type = 'percent'
                    WHERE invoice_line.invoice_id = invoice.id
                ),'0.0,0.0'
              ) AS client_amount,
              EXTRACT(YEAR FROM invoice.date_invoice) AS invoice_year,
              EXTRACT(QUARTER FROM invoice.date_invoice) AS invoice_quarter,
              polissa.name, cups.name, cups.direccio, titular.name, tarifa.name,
              invoice.date_invoice, factura.data_inici, factura.data_final,
              distri.ref, factura.id,
              CASE
                WHEN polissa.id IN (
                  SELECT polissa.id
                    FROM giscedata_polissa AS polissa
                    LEFT JOIN giscedata_polissa_category_rel AS polissa_categ_rel
                      ON polissa_categ_rel.polissa_id = polissa.id
                    LEFT JOIN giscedata_polissa_category AS pol_categ
                      ON pol_categ.id = polissa_categ_rel.category_id
                    WHERE pol_categ.id = %(pol_categ_exempt_id)s
                ) THEN 'Si'
                ELSE 'No'
              END AS municipal_tax_exempt
              FROM giscedata_facturacio_factura AS factura
              LEFT JOIN account_invoice AS invoice
                ON invoice.id = factura.invoice_id
              LEFT JOIN giscedata_polissa AS polissa
                ON polissa.id = factura.polissa_id
              LEFT JOIN giscedata_cups_ps AS cups
                ON cups.id = polissa.cups
              LEFT JOIN res_municipi AS municipi
                ON municipi.id = cups.id_municipi
              LEFT JOIN res_partner AS titular
                ON polissa.titular = titular.id
              LEFT JOIN giscedata_polissa_tarifa AS tarifa
                ON polissa.tarifa = tarifa.id
              LEFT JOIN res_partner AS distri
                ON distri.id = cups.distribuidora_id
              LEFT JOIN account_journal AS journal
                ON journal.id = invoice.journal_id
              WHERE municipi.name = %(municipi_name)s
                AND (invoice.date_invoice >= %(start_date)s
                  AND invoice.date_invoice < %(end_date)s)
                AND (invoice.type LIKE %(invoice_type)s
                  AND invoice.state IN %(invoiced_states)s)
                AND journal.code LIKE 'ENERGIA%%'
              ORDER BY distri.ref, titular.name, client_amount,
              invoice.date_invoice, invoice.reference
            '''

        self.cursor.execute(
            sql, {
                'reference': _('Altres'),
                'tax_name': '%IVA%',
                'pol_categ_exempt_id': self.polissa_categ_imu_ex_id,
                'municipi_name': name,
                'start_date': self.start_date,
                'end_date': self.end_date,
                'invoice_type': 'out_%',
                'invoiced_states': tuple(self.invoiced_states),
            }
        )

        return self.cursor.fetchall()

    def get_provider_invoices_ids(self, name):
        sql = '''
            SELECT
              factura.id
              FROM giscedata_facturacio_factura AS factura
              LEFT JOIN account_invoice AS invoice
                ON invoice.id = factura.invoice_id
              LEFT JOIN giscedata_polissa AS polissa
                ON polissa.id = factura.polissa_id
              LEFT JOIN giscedata_cups_ps AS cups
                ON cups.id = polissa.cups
              LEFT JOIN res_municipi AS municipi
                ON municipi.id = cups.id_municipi
              LEFT JOIN account_journal AS journal
                ON journal.id = invoice.journal_id
              WHERE municipi.name = %s
                AND (invoice.date_invoice >= %s
                  AND invoice.date_invoice < %s)
                AND invoice.type LIKE %s
                AND journal.code LIKE 'CENERGIA%%'
            '''

        self.cursor.execute(sql, (name, self.start_date, self.end_date, 'in_%'))

        return self.cursor.fetchall()

    def get_client_invoices_ids(self, name):
        sql = '''
            SELECT
              factura.id
              FROM giscedata_facturacio_factura AS factura
              LEFT JOIN account_invoice AS invoice
                ON invoice.id = factura.invoice_id
              LEFT JOIN giscedata_polissa AS polissa
                ON polissa.id = factura.polissa_id
              LEFT JOIN giscedata_cups_ps AS cups
                ON cups.id = polissa.cups
              LEFT JOIN res_municipi AS municipi
                ON municipi.id = cups.id_municipi
              LEFT JOIN account_journal AS journal
                ON journal.id = invoice.journal_id
              WHERE municipi.name = %s
                AND (invoice.date_invoice >= %s
                  AND invoice.date_invoice < %s)
                AND (invoice.type LIKE %s
                  AND invoice.state IN %s)
                AND journal.code LIKE 'ENERGIA%%'
            '''

        self.cursor.execute(
            sql, (name, self.start_date, self.end_date, 'out_%',
                  tuple(self.invoiced_states))
        )

        return self.cursor.fetchall()

    def get_provider_invoices_detail(self, ids_providers):
        pass

    def get_client_invoices_detail(self, ids_clients):
        sql_resum = {
            'comer': 'query_resum_comer.sql',
            'distri': 'query_resum_xml.sql'
        }

        sql_path = get_module_resource(
            'giscedata_facturacio_{}'.format(self.whereiam),
            'sql', sql_resum[self.whereiam]
        )

        with open(sql_path, 'r') as f:
            sql = f.read()

        self.cursor.execute(sql % ', '.join(ids_clients))

        return self.cursor.fetchall()

    def get_company_data(self):
        sql = '''
            SELECT rp.name, rp.vat
              FROM res_users AS ru, res_company AS rc, res_partner AS rp
              WHERE rc.id = ru.company_id
                AND rp.id = rc.partner_id
                AND ru.id = %s
            ''' % self.uid

        self.cursor.execute(sql)

        return self.cursor.fetchall()
