# -*- coding: utf-8 -*-

from osv import osv, fields


class ResMunicipi(osv.osv):
    _name = 'res.municipi'
    _inherit = 'res.municipi'

    _columns = {
        'imu_diputacio': fields.boolean(
            string='IMU diputació',
            help="L'impost municipal d'aquest municipi es gestiona a través de "
                 "la diputació provincial"
        )
    }

ResMunicipi()
