# -*- coding: utf-8 -*-
{
    "name": "Municipal taxes",
    "description": """ Municipal taxes
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_extended",
        "giscedata_cups",
        "giscedata_polissa",
        "account",
        "giscedata_polissa_category",
        "giscedata_facturacio",
        "giscedata_administracio_publica"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "res_municipi_view.xml",
        "giscedata_municipal_taxes_data.xml",
        "wizard/wizard_municipal_taxes_view.xml",
        "wizard/wizard_municipal_taxes_invoicing_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
