# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes Enero 2016",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 302 - 18/12/2015.
IET/2735/2015
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures",
        "product",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_peajes_20160101_data.xml"
    ],
    "active": False,
    "installable": True
}
