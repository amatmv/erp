# -*- coding: utf-8 -*-
{
    "name": "GISCE Liquidacio multicompany",
    "description": """Multi-company support for Liquidacio""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscedata_liquidacio"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_liquidacio_multicompany_view.xml"
    ],
    "active": False,
    "installable": True
}
