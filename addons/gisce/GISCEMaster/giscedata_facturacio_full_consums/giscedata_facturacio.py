# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from giscedata_facturacio.defs import SIGN as signes_rectificadores


class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def _ff_potencia(self, cr, uid, ids, field_name, arg, context={}):
        res = {}
        linia_obj = self.pool.get('giscedata.facturacio.lectures.potencia')
        for factura_id in ids:
            lids = linia_obj.search(cr, uid, [('factura_id', '=', factura_id)])
            pot = 0.0
            if lids:
                pot = max([a['pot_contract'] for a in linia_obj.read(cr, uid, lids, ['pot_contract'])])  
            res[factura_id] = pot
        return res

    def _ff_p_ene(self, cr, uid, ids, field_name, arg, context={}):
        res = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        for factura_id in ids:
            fact = fact_obj.read(cr,uid,factura_id,['tipo_rectificadora'])
            lids = linia_obj.search(cr, uid, [('factura_id', '=', factura_id), ('tipus', '=', 'energia'),
                                     ('name', '=', arg['periode'])])
            ene = 0.0
            # te en compte el signe per les abonament/rectificadora
            signe = signes_rectificadores[fact['tipo_rectificadora']]
            if lids:
                linies_data = linia_obj.read(
                    cr, uid, lids, ['quantity', 'isdiscount'])
                for linia in linies_data:
                    if not linia.get('isdiscount', False):
                        ene += linia['quantity'] * signe
            res[factura_id] = ene
        return res
    
    _columns = {
        'p1_ene': fields.function(_ff_p_ene, method=True, type='float', string='Periode 1', arg={'periode': 'P1'}),
        'p2_ene': fields.function(_ff_p_ene, method=True, type='float', string='Periode 2', arg={'periode': 'P2'}),
        'p3_ene': fields.function(_ff_p_ene, method=True, type='float', string='Periode 3', arg={'periode': 'P3'}),
        'p4_ene': fields.function(_ff_p_ene, method=True, type='float', string='Periode 4', arg={'periode': 'P4'}),
        'p5_ene': fields.function(_ff_p_ene, method=True, type='float', string='Periode 5', arg={'periode': 'P5'}),
        'p6_ene': fields.function(_ff_p_ene, method=True, type='float', string='Periode 6', arg={'periode': 'P6'}),
        'potencia_max': fields.function(_ff_potencia, method=True, type='float', string='Potencia Màxima'),
        'create_uid': fields.many2one('res.users', 'create_uid')
    }

GiscedataFacturacioFactura()
