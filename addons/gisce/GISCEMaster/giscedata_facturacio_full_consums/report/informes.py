# -*- coding: utf-8 -*-

import jasper_reports

def full_consums(cr, uid, ids, data, context):
    return {
        'ids': data['form']['parameters']['ids'],
        'parameters': data['form']['parameters'],
    }

jasper_reports.report_jasper(
   'report.giscedata.facturacio.full.consums',
   'giscedata.facturacio.factura',
   parser=full_consums
)
