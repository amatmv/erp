# -*- coding: utf-8 -*-
{
    "name": "Informes de relació de consums",
    "description": """
    This module provide :
      * Informe de relació de consums.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_facturacio",
        "giscedata_lectures",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "informe_full_consums_view.xml",
        "informe_full_consums_report.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
