# -*- coding: utf-8 -*-

import wizard
import pooler
from tools.translate import _
from osv import osv

def _escollir_polissa(self, cr, uid, data, context=None):
  return {}


_escollir_polissa_form = """<?xml version="1.0"?>
<form string="Generar relació de consums" col="2">
  <field name="polissa_id" />
</form>"""

_escollir_polissa_fields = {
    'polissa_id': {'string': _(u'Número de pólissa'), 'type': 'many2one', 'relation': 'giscedata.polissa', 'required': True},
}

def _escollir_dates(self, cr, uid, data, context=None):
  return {}

_escollir_dates_form = """<?xml version="1.0"?>
<form string="Generar relació de consums" col="2">
  <field name="data_inicial" />
  <field name="data_final" />
</form>"""

_escollir_dates_fields = {
    'data_inicial': {'string': _(u'Data inicial'), 'type': 'date', 'required': True},
    'data_final': {'string': _(u'Data final'), 'type': 'date', 'required': True},
}

def _generar_informe(self, cr, uid, data, context=None):
  form = data['form']
  polissa_id = form['polissa_id']
  data_inicial = form['data_inicial']
  data_final = form['data_final']
  
  factura_obj = pooler.get_pool(cr.dbname).get('giscedata.facturacio.factura')
  # Busquem totes les factures de la pòlissa associades a aquest periode
  factura_ids = factura_obj.search(
      cr, uid, [
          ('polissa_id', '=', polissa_id),
          ('data_inici', '>=', data_inicial),
          ('data_final', '<=', data_final),
          ('type', 'in', ['out_invoice', 'out_refund'])
      ], order="data_inici asc"
  )
  if not factura_ids:
     raise osv.except_osv(_(u'Error'), _(u'No hi ha factures entre les dates seleccionades'))
  data.update({'factura_ids': factura_ids})
  return {}

def _print(self, cr, uid, data, context=None):
  return {'parameters': {'ids': data['factura_ids']},
          'ids':  data['factura_ids']}

  
class informe_full_consums(wizard.interface):

  states = {
      'init': {
          'actions': [],
          'result': {'type': 'state', 'state': 'escollir_polissa'}
      },
      'escollir_polissa': {
          'actions': [_escollir_polissa],
          'result': {'type': 'form', 'arch': _escollir_polissa_form, 'fields': _escollir_polissa_fields, 'state': [('escollir_dates', _(u'Següent')), ('end', _(u'Cancel·lar'))]}
      },
      'escollir_dates': {
          'actions': [_escollir_dates],
          'result': {'type': 'form', 'arch': _escollir_dates_form, 'fields': _escollir_dates_fields, 'state': [('generar_informe', _(u'Següent')), ('end', _(u'Cancel·lar'))]}
      },
      'generar_informe': {
          'actions': [_generar_informe],
          'result': {'type': 'state', 'state': 'print'}
      },
      'print': {
          'actions': [_print],
          'result': {
                'type': 'print', 
#                'xml':  'giscedata_facturacio_full_consums/report/full_consums.jrxml', 
                'report': 'giscedata.facturacio.full.consums', 
                'name': _(u'Generar relació de consums'),
                'get_id_from_action':True, 
                'state':'end'}
      },
      'end': {
          'actions': [],
          'result': {'type': 'state', 'state': 'end'}
      }
  }
  
informe_full_consums('giscedata.facturacio.full.consums')

