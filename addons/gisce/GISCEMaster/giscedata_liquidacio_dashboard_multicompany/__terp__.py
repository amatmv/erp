# -*- coding: utf-8 -*-
{
    "name": "GISCE Liquidacio Dashboard multicompany",
    "description": """Multi-company support for Liquidacio""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscedata_liquidacio_dashboard"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_liquidacio_dashboard_multicompany_view.xml"
    ],
    "active": False,
    "installable": False
}
