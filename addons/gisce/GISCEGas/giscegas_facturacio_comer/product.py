# -*- coding: utf-8 -*-
from osv import osv, fields
import time
from tools.translate import _
from tools import cache


class GiscegasFacturacioComerProduct(osv.osv):

    _name = 'product.product'
    _inherit = 'product.product'

    @cache()
    def get_categories(self, cursor, uid, product_id, context=None):
        '''Retrieve categories of the product recursively'''

        if isinstance(product_id, (list, tuple)):
            product_id = product_id[0]
        product = self.browse(cursor, uid, product_id)
        category = product.categ_id or False
        category_ids = []
        while category:
            category_ids.append(category.id)
            category = category.parent_id or False
        
        return category_ids

GiscegasFacturacioComerProduct()
