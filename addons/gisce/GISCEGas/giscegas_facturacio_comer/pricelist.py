# -*- coding: utf-8 -*-
from osv import osv, fields
import time
from tools.translate import _
from tools import cache


class GiscegasProductPricelistGroup(osv.osv):

    _name = 'giscegas.product.pricelist.group'

    _columns = {
        'pricelist_id': fields.many2one(
            'product.pricelist', 'Lista de precios', required=True
        ),
        'periode_id': fields.many2one(
            'giscegas.polissa.tarifa.periodes', 'Período donde agrupar',
            required=True
        ),
        'grouped_ids': fields.many2many(
            'giscegas.polissa.tarifa.periodes', 'giscegas_periode_pricelist_group_rel',
            'periode_id', 'group_id', 'Períodos a agrupar', required=True
        ),
    }

    def get_periodes(self, cursor, uid, group_id, context=None):

        if isinstance(group_id, (list, tuple)):
            group_id = group_id[0]
        group = self.browse(cursor, uid, group_id)
        return {'to': [group.periode_id.name, group.periode_id.name_get()[0][1]],
                'from': [x.name_get()[0][1] for x in 
                         group.grouped_ids]}

    def get_products(self, cursor, uid, group_id, context=None):

        if isinstance(group_id, (list, tuple)):
            group_id = group_id[0]
        group = self.browse(cursor, uid, group_id)
        periode = group.periode_id
        
        res = []
        #Add product_id mapping
        res.append({
            'to': periode.product_id.id,
            'from': [x.product_id.id 
                    for x in group.grouped_ids]})
        #If te, also add reactiva mapping
        if periode.tipus == 'te':
            if periode.product_reactiva_id:
                res.append({
                'to': periode.product_reactiva_id.id,
                'from': [x.product_reactiva_id.id 
                        for x in group.grouped_ids]})
        #If tp, also add exces potencia mapping
        elif periode.tipus == 'tp':
            if periode.product_exces_pot_id:
                res.append({
                'to': periode.product_exces_pot_id.id,
                'from': [x.product_exces_pot_id.id 
                        for x in group.grouped_ids]})
        return res

    def _cnt_check_tarifa(self, cursor, uid, ids, context=None):

        for group in self.browse(cursor, uid, ids, context=context):
            #Check if all periodes belong to the same tarifa
            tarifa_id = group.periode_id.tarifa.id
            tarifa_ids = [x.tarifa.id for x in group.grouped_ids
                         if x.tarifa.id != tarifa_id]
            if tarifa_ids:
                return False
        return True

    def _cnt_check_periode(self, cursor, uid, ids, context=None):

        for group in self.browse(cursor, uid, ids, context=context):
            #Check if a periode is in both parts of the group
            #To group and for grouping
            periode_id = group.periode_id.id
            periode_ids = [x.id for x in group.grouped_ids
                           if x.id == periode_id]
            if periode_ids:
                return False
        return True

    _constraints = [
        (
            _cnt_check_tarifa,
            _(u"No se pueden agrupar períodos de distintas tarifas"),
            ['periode_id', 'grouped_ids']
        ),
        (
            _cnt_check_periode,
            _(u"El perído donde agrupar no puede estar dentro de los períodos "
              u"a agrupar"),
            ['periode_id', 'grouped_ids']
        ),
    ]

GiscegasProductPricelistGroup()


class ProductPricelist(osv.osv):
    """Llistes de preus.
    """
    _name = 'product.pricelist'
    _inherit = 'product.pricelist'
    
    def price_get_xmlrpc(self, cr, uid, ids, prod_id, qty, partner=None,
                         context=None):
        """Retorna el preu a través d'XML-RPC.
        
        Resol el bug: https://bugs.launchpad.net/openobject-server/+bug/689575
        """
        for lid in ids:
            return self.price_get(
                cr, uid, [lid], prod_id, qty, partner, context
            )[lid]
    
    def qty_get(self, cursor, uid, ids, *args, **kwargs):
        """Retorna les quantitats que s'han de facturar per trams segons
        el producte.
        """
        res = []
        version_obj = self.pool.get('product.pricelist.version')
        item_obj = self.pool.get('product.pricelist.item')
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]
        if args:
            product_id, quantity, context = args
        else:
            context = kwargs.get('context', {})
            product_id = kwargs['product_id']
            quantity = kwargs.get('quantity', 0)
        pricelist = self.browse(cursor, uid, ids, context)
        date = context.get('date', time.strftime('%Y-%m-%d'))
        search_params = [
            ('pricelist_id.id', '=', pricelist.id),
            '&',
            '|',
            ('date_start', '=', False),
            ('date_start', '<=', date),
            '|',
            ('date_end', '=', False),
            ('date_end', '>=', date)
        ]
        version = version_obj.search(cursor, uid, search_params, limit=1)
        if not version:
            raise osv.except_osv(_('Warning !'),
                                 _('No active version for the selected '
                                   'pricelist !\nPlease create or activate '
                                   'one.'))
        else:
            version = version[0]
        # Busquem si en aquesta versio hi ha llista per trams per aquest
        # producte
        search_params = [
            ('price_version_id.id', '=', version),
            ('product_id.id', '=', product_id),
            ('gas_tram', '=', 1)
        ]
        item_ids = item_obj.search(cursor, uid, search_params)
        if not item_ids:
            return [quantity]
        for item in item_obj.browse(cursor, uid, item_ids):
            if item.base == -1:
                res += self.qty_get(cursor, uid, item.base_pricelist_id.id,
                                    product_id=product_id, quantity=quantity,
                                    context=context,)
            else:
                res.append(item.gas_tram_quantity)
        res = sorted(res, reverse=True)
        rest = 0
        qty = []
        while rest < quantity:
            tram = res.pop()
            if quantity < tram:
                tram = quantity
            if tram >= rest:
                tram -= rest
            else:
                tram = quantity - rest
            rest += tram
            qty.append((tram, rest))
            if not len(res):
                res.append(quantity - rest)
        return qty

    @cache(timeout=300)
    def get_groups(self, cursor, uid, pricelist_id,
                   get='product', context=None):
        '''returns dict with groups of products'''

        group_obj = self.pool.get('giscegas.product.pricelist.group')
        if isinstance(pricelist_id, (list, tuple)):
            pricelist_id = pricelist_id[0]
        pricelist = self.browse(cursor, uid, pricelist_id)

        res = []
        for group in pricelist.gas_group_ids:
            if get == 'product':
                res.extend(group.get_products())
            elif get == 'periode':
                res.append(group.get_periodes())
        return res

    @cache(timeout=300)
    def get_product_group(self, cursor, uid, pricelist_id,
                          product_id, context=None):
        if isinstance(pricelist_id, (list, tuple)):
            pricelist_id = pricelist_id[0]
        product_groups = self.get_groups(cursor, uid, pricelist_id)
        search_product_id = [x['to'] for x in product_groups
                             if product_id in x['from']]
        if search_product_id:
            return search_product_id[0]
        return False

    @cache(timeout=300)
    def get_periode_group(self, cursor, uid, pricelist_id,
                          periode, context=None):
        if isinstance(pricelist_id, (list, tuple)):
            pricelist_id = pricelist_id[0]
        periode_groups = self.get_groups(cursor, uid, pricelist_id,
                                         get='periode')
        search_periode = [x['to'] for x in periode_groups
                          if periode in x['from']]
        if search_periode:
            return search_periode[0]
        return False

    def _get_pricelist_from_group(self, cursor, uid, ids, context=None):
        '''return pricelist associated with group'''
        group_obj = self.pool.get('giscegas.product.pricelist.group')

        pricelists = group_obj.read(cursor, uid, ids, ['pricelist_id'])
        return [x['pricelist_id'][0] for x in pricelists]

    def _ff_agrupacio(self, cursor, uid, ids, field_name, arg, context=None):

        res = {}
        for pricelist in self.browse(cursor, uid, ids, context=context):
            res[pricelist.id] = pricelist.gas_group_ids and True or False
        return res  

    _columns = {
        'gas_group_ids': fields.one2many(
            'giscegas.product.pricelist.group', 'pricelist_id', 'Agrupaciones'
        ),
        'gas_agrupacio': fields.function(
            _ff_agrupacio, method=True, type='boolean', string='Agrupaciones',
            store={
                'giscegas.product.pricelist.group': (
                    _get_pricelist_from_group, ['product_id'], 20
                ),
                'product.pricelist': (
                    lambda self, cr, uid, ids, c=None: ids, ['gas_group_ids'], 20
                ),
            }
        ),
        'gas_visible_discount': fields.boolean('Descuento visible'),
        'tarifes_atr_gas_compatibles': fields.many2many(
            'giscegas.polissa.tarifa', 'giscegas_polissa_tarifa_pricelist',
            'pricelist_id', 'tarifa_id', 'Listas de precios compatibles'
        )
    }

    _defaults = {
        'gas_visible_discount': lambda *a: False,
    }

ProductPricelist()


class ProductPricelistItem(osv.osv):
    """Llistes de preus per trams.
    """
    _name = 'product.pricelist.item'
    _inherit = 'product.pricelist.item'

    _columns = {
        'gas_tram': fields.boolean(
            'Tram', help=u"Indica si hay que facturar por tramo. Esto va a hacer "
                         u"que se facture justo la cantidad del tramo"
        ),
        'gas_tram_quantity': fields.float('Tram Quantity'),
        'gas_min_quantity': fields.float(
            'Min. Quantity', required=True,
            help="Este regla sólamente se aplica si el partner compra/vende más"
                 " que esta cantidad."
        )
    }

    _defaults = {
        'gas_tram': lambda *args: 0,
    }

ProductPricelistItem()
