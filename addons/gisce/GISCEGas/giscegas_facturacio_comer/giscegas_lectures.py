# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config
from tools.translate import _


class GiscegasLecturesComptador(osv.osv):
    """Afegim camps per facturar el lloguer dels comptadors.
    """
    _name = 'giscegas.lectures.comptador'
    _inherit = 'giscegas.lectures.comptador'

    def _default_uom_id(self, cursor, uid, context=None):
        """Obtenir la unitat per defecte de mesura pels lloguers.
        """
        imd_obj = self.pool.get('ir.model.data')
        try:
            imd_id = imd_obj._get_id(
                cursor, uid, 'giscegas_lectures', 'uom_alq_elec'
            )
            res = imd_obj.read(cursor, uid, imd_id, ['res_id'])
            return res['res_id']
        except:
            return False

    _columns = {
        'descripcio_lloguer': fields.char('Descripción alquiler', size=256),
        'preu_lloguer': fields.float('Precio alquiler',
            digits=(16, int(config['price_accuracy']))
        ),
        'uom_id': fields.many2one('product.uom', 'Unidad', ondelete='restrict',
            domain=[('category_id.name', '=', 'ALQ GAS')]
        )
    }

    _defaults = {
        'descripcio_lloguer': lambda *a: _('ALQ Equipo Medida'),
        'preu_lloguer': lambda *a: 0.0,
        'uom_id': _default_uom_id
    }

GiscegasLecturesComptador()
