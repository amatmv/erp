# -*- coding: utf-8 -*-
"""Modificacions del model giscegas_facturacio per la comercialitzadora.
"""
# pylint: disable-msg=E1101,W0223
import re
import time

import StringIO
import base64

from osv import osv, fields
from tools.translate import _
from tools import config, float_round
from datetime import datetime, timedelta
import netsvc
from tools.misc import cache, timeit

from base_extended.excel import Sheet


class GiscegasFacturacioFacturador(osv.osv):
    """Metaclasse per generar les factures (no té taula SQL).
    """
    _name = 'giscegas.facturacio.facturador'
    _inherit = 'giscegas.facturacio.facturador'

    #TODO Gas
    def _get_atrprice_subtotal(self, cursor, uid, vals,
                               linia, context=None):
        '''returns atr price depending on product line'''

        model_obj = self.pool.get('ir.model.data')
        pricelist_obj = self.pool.get('product.pricelist')
        tarifa_elec = model_obj._get_obj(cursor, uid, 'giscegas_facturacio',
                                         'pricelist_tarifas_gas')
        date = (linia.data_desde or
                linia.factura_id.date_invoice or
                time.strftime('%Y-%m-%d'))
        #Do not compute atr price in altres
        if linia.tipus == 'altres':
            return 0
        #lloguer lines already have atr price
        elif linia.tipus == 'lloguer':
            return linia.price_subtotal
        else:
            if isinstance(vals['product_id'], (list, tuple)):
                product_id = vals['product_id'][0]
            else:
                product_id = vals['product_id']
            #Cas energia negativa (FENOSA)
            # Busquem el preu ATR com si fos positiu
            quantity = abs(vals['quantity'])

        if not product_id:
            return 0
        atrprice = pricelist_obj.price_get(cursor, uid,
                         [tarifa_elec.id],
                         product_id, quantity,
                         context={'date': date,
                                  'uom': linia.uos_id.id})[tarifa_elec.id]
        if atrprice:
            return atrprice * vals['quantity']

    def crear_linia(self, cursor, uid, factura_id, vals, context=None):

        factura_obj = self.pool.get('giscegas.facturacio.factura')
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        product_obj = self.pool.get('product.product')

        factura = factura_obj.browse(cursor, uid, factura_id)
        orig_vals = vals.copy()
        #Check if we have to group product
        if (vals['product_id'] and
            factura.llista_preu and
            factura.llista_preu.gas_agrupacio):
            new_product_id = (factura.llista_preu.
                              get_product_group(vals['product_id']))
            if new_product_id:
                vals['product_id'] = new_product_id
                vals['name'] = product_obj.read(cursor, uid,
                                                new_product_id,
                                                ['name'])['name']
        linia_id = super(GiscegasFacturacioFacturador,
                     self).crear_linia(cursor, uid, factura_id,
                                       vals, context=context)
        #Compute atrprice_subtotal for the new line
        #We use original product and original quantity
        #before grouping
        linia = linia_obj.browse(cursor, uid, linia_id)
        atrprice = self._get_atrprice_subtotal(cursor, uid,
                                               orig_vals,
                                               linia,
                                               context=context)
        linia.write({'atrprice_subtotal': atrprice + linia.atrprice_subtotal})
        return linia_id

    def crear_lectures_energia(self, cursor, uid, factura_id, vals,
                               context=None):
        """Agrupacio de les lectures d'energia segons els periodes
        """
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        factura = factura_obj.browse(cursor, uid, factura_id)
        ids = []
        new_vals = vals.copy()
        if factura.llista_preu and factura.llista_preu.gas_agrupacio:
            for periode, lectura in vals.iteritems():
                if (not lectura.get('anterior', {})
                    or not lectura.get('actual', {})):
                    continue
                per_name = lectura['actual']['periode'][1]
                newper = (factura.llista_preu.
                          get_periode_group(lectura['actual']['periode'][1]))
                if not newper:
                    continue
                #We have to group this period
                actual_new = new_vals[newper[0]]['actual']
                actual_old = new_vals[periode]['actual']
                actual_new.update({'lectura': (actual_new.get('lectura', 0)
                                          + actual_old.get('lectura', 0)),
                                   'consum': (actual_new.get('consum', 0)
                                           + actual_old.get('consum', 0)),
                                   'data_actual': max(actual_new['name'],
                                                      actual_old['name']),
                                   'data_anterior': min(actual_new['name'],
                                                    actual_old['name'])})
                ant_new = new_vals[newper[0]]['anterior']
                ant_old = new_vals[periode]['anterior']
                ant_new.update({'lectura': (ant_new.get('lectura', 0) +
                                            ant_old.get('lectura', 0)),
                                'consum': (ant_new.get('consum', 0) +
                                           ant_old.get('consum', 0)),
                                'data_actual': max(ant_new['name'],
                                                   ant_old['name']),
                                'data_anterior': min(ant_new['name'],
                                                     ant_old['name'])})
                del new_vals[periode]

        return super(GiscegasFacturacioFacturador,
                     self).crear_lectures_energia(cursor, uid, factura_id,
                                                  new_vals, context=context)

    def crear_linies_lloguer(self, cursor, uid, factura_id, vals,
                             context=None):
        """Creem les línies de lloguer per la factura segons els termes.
        """
        if not context:
            context = {}
        product_obj = self.pool.get('product.product')
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        compt_obj = self.pool.get('giscegas.lectures.comptador')
        # El preu ens el passaran com a product_id
        comptador_id = vals.get('product_id', False)
        if comptador_id:
            comptador = compt_obj.browse(cursor, uid, comptador_id)
            vals['name'] = comptador.descripcio_lloguer
            vals['uos_id'] = comptador.uom_id.id
            price = comptador.preu_lloguer
            # Fem forçar el preu del lloguer a X
            vals['force_price'] = price
        product_id = product_obj.search(cursor, uid, [('default_code', '=', 'ALQG1')])[0]
        vals['product_id'] = product_id
        ctx = context.copy()
        ctx.update({'lloguer_comer': True})
        linia_id = self.crear_linia(cursor, uid, factura_id, vals, ctx)
        # Eliminem el producte
        linia_obj.write(cursor, uid, [linia_id], {'product_id': False})
        return linia_id

    def fact_lloguer(self, facturador, comptador, data_inici_periode_f,
                     data_final_periode_f, context=None):
        """Facturem el lloguer.
        """
        lloguer_name = comptador.descripcio_lloguer
        facturador.conf['lloguers'] = [lloguer_name]
        data_alta_c = max(comptador.data_alta, data_inici_periode_f)
        data_baixa_c = min(comptador.data_baixa or '3000-01-01',
                           data_final_periode_f)
        base = 'mes'
        factor = comptador.uom_id.factor
        if not factor % 365 or not factor % 366:
            base = 'dia'
        facturador.factura_lloguer(data_alta_c, data_baixa_c, base=base)
        return {comptador.descripcio_lloguer: comptador.id}

GiscegasFacturacioFacturador()


class GiscegasFacturacioContracteLot(osv.osv):
    """Contracte Lot per comercialitzadora.
    """
    _name = 'giscegas.facturacio.contracte_lot'
    _inherit = 'giscegas.facturacio.contracte_lot'

    def __init__(self, pool, cursor):
        """Init to add new states."""
        super(GiscegasFacturacioContracteLot, self).__init__(pool, cursor)
        for sel in [('proveidor', 'Proveidor')]:
            if sel not in self._columns['tipus_facturacio'].selection:
                self._columns['tipus_facturacio'].selection.append(sel)
            if sel not in self._columns['state'].selection:
                self._columns['state'].selection.append(sel)

    def cnd_proveidor(self, cursor, uid, ids, context=None):
        """Condició si té factures de proveïdor.
        """
        cfg_obj = self.pool.get('res.config')
        permit_provider = int(cfg_obj.get(cursor, uid, 'permit_provider', '0'))
        if not permit_provider:
            return False
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        for clot in self.browse(cursor, uid, ids):
            # Busquem si té factures de proveïdor per aquesta pòlissa en
            # aquest lot.
            search_params = [
                ('polissa_id.id', '=', clot.polissa_id.id),
                ('data_final', '>', clot.lot_id.data_inici),
                ('data_final', '<=', clot.lot_id.data_final),
                ('type', '=', 'in_invoice'),
                ('state', '=', 'open')
            ]
            if not factura_obj.search_count(cursor, uid, search_params,
                                            context={'active_test': False}):
                return False
        return True

    def wkf_lectures(self, cursor, uid, ids, context=None):
        """Si es compleix la condicio per facturar per proveidor
        aquest estat cridara a wkf_proveidor encara que es pugui
        facturar per lectures. Si tenim factura de proveidor, a per ella"""

        for clot_id in ids:
            if self.cnd_proveidor(cursor, uid, [clot_id], context):
                self.wkf_proveidor(cursor, uid, [clot_id], context)
            else:
                super(GiscegasFacturacioContracteLot,
                      self).wkf_lectures(cursor, uid, [clot_id], context)

        return True

    def wkf_proveidor(self, cursor, uid, ids, context=None):
        """Estat proveïdor.
        """
        self.write(cursor, uid, ids, {'state': 'proveidor',
                                      'tipus_facturacio': 'proveidor',
                                      'status': False})
        #Transicio a facturar
        self.wkf_facturar(cursor, uid, ids, context)
        return True

    def cnd_facturat(self, cursor, uid, ids, context=None):
        """Condició per saber si ja té la factura feta (Exportada).
        """
        factura_obj = self.pool.get('giscegas.facturacio.factura')

        for clot in self.browse(cursor, uid, ids):
            # Busquem si ja té una factura creada
            search_params = [
                ('polissa_id.id', '=', clot.polissa_id.id),
                ('lot_facturacio.id', '=', clot.lot_id.id),
                ('type', '=', 'out_invoice')
            ]
            context = {'active_test': False}
            if not factura_obj.search_count(cursor, uid, search_params,
                                            context=context):
                return False

        return True

    def validate_invoice_mandate(self, cursor, uid, id, context=None):

        factura_obj = self.pool.get('giscegas.facturacio.factura')
        config_obj = self.pool.get('res.config')
        fact_check_mandato = int(config_obj.get(cursor, uid,
                                                'fact_check_mandato', '0'))
        if not fact_check_mandato:
            return True
        clot = self.browse(cursor, uid, id)
        search_params = [
            ('polissa_id.id', '=', clot.polissa_id.id),
            ('lot_facturacio.id', '=', clot.lot_id.id),
            ('type', '=', 'out_invoice')
        ]
        context = {'active_test': False}
        factura_ids = factura_obj.search(cursor, uid, search_params,
                                        context=context)
        mandate_missing = False
        for factura in factura_obj.browse(cursor, uid, factura_ids):
            if factura.partner_bank and not factura.mandate_id:
                mandate_missing = True
        if mandate_missing:
            msg = _(u"* Hay facturas domiciliadas sin mandato asociado")
            clot.write({'status': msg})
            return False
        return True

    def wkf_obert(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        for clot_id in ids:
            if (not context.get('from_lot', False) and
                self.cnd_facturat(cursor, uid, [clot_id], context=context)):
                if self.validate_invoice_mandate(cursor, uid, clot_id):
                    self.wkf_facturat(cursor, uid, [clot_id], context=context)
            else:
                super(GiscegasFacturacioContracteLot,
                      self).wkf_obert(cursor, uid, [clot_id], context=context)

        return True

GiscegasFacturacioContracteLot()


class GiscegasFacturacioFactura(osv.osv):
    """Classe per la factura de comercialitzadora."""
    _name = 'giscegas.facturacio.factura'
    _inherit = 'giscegas.facturacio.factura'

    enviat_carpeta_selection = [
        ('inbox', 'Entrada'),
        ('drafts', 'Borradores'),
        ('outbox', 'Salida'),
        ('trash', 'Papelera'),
        ('followup', 'Seguir'),
        ('sent', 'Enviados'),
    ]

    def _trg_totals(self, cursor, uid, ids, context=None):
        """Funció per especificar els IDs a recalcular
        """
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        inv_ids = [a['invoice_id'][0] for a in self.read(cursor, uid, ids,
                                                         ['invoice_id'])]
        return fact_obj.search(cursor, uid, [('invoice_id', 'in', inv_ids)])

    def _trg_dies(self, cursor, uid, ids, context=None):
        """Funció to specify ids to recalcultae `dies` field
        """
        return ids

    #TODO Gas
    def _ff_total_atr(self, cursor, uid, ids, field, arg, context=None):
        """Funció que retorna el total d'€ facturats en concepte de peatges.
        """
        query = '''
            SELECT
              fl.factura_id,
              sum(coalesce(fl.atrprice_subtotal, 0))
            FROM giscegas_facturacio_factura_linia fl
            INNER JOIN account_invoice_line il
              ON il.id = fl.invoice_line_id
            INNER JOIN account_invoice i
            ON il.invoice_id = i.id
            WHERE fl.tipus = 'variable'
            AND fl.factura_id in %s
            AND i.date_invoice >= %s
            GROUP BY fl.factura_id
        '''
        cursor.execute(query, (tuple(ids), '2013-01-01'))
        return dict([(x[0], round(x[1], 2)) for x in cursor.fetchall()])

    def _ff_total_consum(self, cursor, uid, ids, field, arg, context=None):
        """
        :param ids: ids de les factures sobre les que es vol calcular el consum
        :return: retorna la quantitat total de consum de les factures "ids"
        """
        res = {}
        for factura in self.browse(cursor, uid, ids):
            res[factura.id] = 0
            for linia in factura.linies_consum:
                if linia.isdiscount:
                    continue
                res[factura.id] += linia.quantity
            res[factura.id] = int(res[factura.id])
        return res

    @cache(timeout=10)
    def _fnc_per_enviar(self, cursor, uid, ids, field_name, args,
                        context=None):
        """Retorna si una factura està marcada per enviar o no.
        """
        res = dict([(x, 'postal') for x in ids])
        cursor.execute("""
            select
              f.id,
              m.enviament
            from
              giscegas_facturacio_factura f
              left join giscegas_polissa_modcontractual m
                on (m.polissa_id = f.polissa_id)
            where
              f.id in %s
              and f.data_final between m.data_inici and m.data_final
        """, (tuple(ids), ))
        res.update(dict([(x[0], x[1]) for x in cursor.fetchall()]))
        return res

    def search(self, cursor, uid, args, offset=0, limit=None, order=None,
               context=None, count=False):

        #Always search with like when per_enviar field
        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, match = arg
            if field == 'per_enviar':
                operator = 'like'
            new_args.append((field, operator, match))

        return super(GiscegasFacturacioFactura,
                     self).search(cursor, uid, new_args, offset=offset,
                                  limit=limit, order=order,
                                  context=context, count=count)

    def copy(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default['per_enviar'] = ''
        return super(GiscegasFacturacioFactura,
                     self).copy(cursor, uid, id, default, context)

    _STORE_TOTALS = {'account.invoice.line': (_trg_totals, ['quantity'], 20)}

    _columns = {
        'payment_mode_id': fields.many2one('payment.mode', 'Grupo de pago',
                                           required=True, readonly=False),
        'total_atr': fields.function(_ff_total_atr, type='float',
                                     method=True,
                                     string='Total Peajes',
                                     store={
                                         'account.invoice.line':
                                         (_trg_totals, ['quantity'], 0)}),
        'enviat': fields.boolean('Enviada por E-mail', readonly=True),
        'enviat_data': fields.datetime('Fecha de envío', readonly=True),
        'enviat_carpeta': fields.selection(enviat_carpeta_selection,
                                           'Carpeta', readonly=True),
        'per_enviar': fields.function(
            _fnc_per_enviar,
            method=True,
            type='selection',
            selection=[('email', 'E-mail'), ('postal', 'Correu postal')],
            string='Tipo de envío',
            store=True,
            select=2
        ),
    }

    def onchange_polissa(self, cursor, uid, ids, polissa_id, type_,
                         context=None):
        """Sobreescrivim el mètode per afegir els camps que hem afegit.

        Fem que retorni payment_mode_id
        """
        res = super(GiscegasFacturacioFactura,
                    self).onchange_polissa(cursor, uid, ids, polissa_id, type_,
                                           context)
        if polissa_id:
            polissa = self.pool.get('giscegas.polissa').browse(cursor, uid,
                                                                polissa_id,
                                                                context)
            res['value'].update({
                'payment_mode_id': polissa.payment_mode_id.id
            })
        return res


    _defaults = {
        'enviat': lambda *a: 0,
        'enviat_data': lambda *a: False
    }

    def _cnt_llista_preu_compatible(self, cursor, uid, ids):
        """Comprovem que la llista de preu és compatible amb la tarifa.
        """
        for factura in self.browse(cursor, uid, ids):
            if not factura.llista_preu or not factura.tarifa_acces_id:
                continue
            if (factura.llista_preu not in
                factura.tarifa_acces_id.llistes_preus_comptatibles):
                return False
        return True

    # Poweremails hooks
    def poweremail_create_callback(self, cursor, uid, ids, vals,
                                   context=None):
        """Hook que cridarà el poweremail quan es creei un email
        a partir d'una factura.
        """
        folder = vals.get('folder', False)
        self.write(cursor, uid, ids, {'enviat': 1, 'enviat_carpeta': folder})
        return True

    def poweremail_unlink_callback(self, cursor, uid, ids, context=None):
        """Hook que cridarà el poweremail quan s'elimini un email.
        """
        self.write(cursor, uid, ids, {'enviat': 0})
        return True

    def poweremail_write_callback(self, cursor, uid, ids, vals,
                                  context=None):
        """Hook que cridarà el poweremail quan es modifiqui un email.
        """
        vals_w = {}
        key_map = (
            ('enviat_data', 'date_mail'),
            ('enviat_carpeta', 'folder')
        )
        for f_key, key in key_map:
            if key in vals:
                vals_w[f_key] = vals[key]
        if vals_w:
            self.write(cursor, uid, ids, vals_w)
        return True

    #TODO Gas
    def generar_resum_factures_xls(self, cursor, uid, ids, header, context=None):
        line_obj = self.pool.get('giscegas.facturacio.factura.linia')
        polissa_obj = self.pool.get('giscegas.polissa')

        output = StringIO.StringIO()
        sheet = Sheet(_('Resum factures'))
        sheet.add_row([_(h).encode('utf8') for h in header])
        for fact_id in ids:
            read_fields = ['date_invoice', 'polissa_id', 'facturacio',
                           'tarifa_acces_id', 'llista_preu',
                           'linies_potencia', 'linies_consum', 'name',
                           ]

            fact = self.read(cursor, uid, fact_id, read_fields)
            ctx = context.copy()
            ctx.update({'date': fact['date_invoice']})

            polissa = polissa_obj.browse(cursor, uid, fact['polissa_id'][0],
                                         context=ctx)

            from gestionatr.defs_gas import (
                TAULA_PERIODICIDAD_LECTURA_FACTURACION as periodicidad_lecturas
            )
            facturacio = filter(
                lambda elem: elem[0] == fact['facturacio'], periodicidad_lecturas
            )[0][1]

            row = [
                   fact['name'],
                   polissa.cups.dp,
                   facturacio,
                   polissa.distribuidora.name,
                   fact['tarifa_acces_id'][1],
                   fact['llista_preu'][1].replace(' (EUR)', '')
            ]

            #            P1 P2 P3 P4 P5 P6
            potencies = [0, 0, 0, 0, 0, 0]
            for line in line_obj.read(cursor, uid, fact['linies_potencia']):
                if (len(line['product_id'][1]) >= 2 and
                    line['product_id'][1][-2] == 'P'):
                    pos = int(line['product_id'][1][-1]) - 1
                    if not line['isdiscount'] and not potencies[pos]:
                        # name Px where x in 1..6
                        potencies[pos] += line['quantity']

            #           P1 P1 P2 P2 P3 P3 P4 P4 P5 P5 P6 P6
            #           €  kw €  kw €  kw €  kw €  kw €  kw
            energies = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            for line in line_obj.read(cursor, uid, fact['linies_consum']):
                if (len(line['product_id'][1]) >= 2 and
                    line['product_id'][1][-2] == 'P'):
                    # €
                    #     |---           Px          ---|          |-- € are even
                    pos = (int(line['product_id'][1][-1]) - 1) * 2
                    energies[pos] += line['price_subtotal']
                    # kW
                    if not line['isdiscount']:  #                      |-- € are even
                        pos = (int(line['product_id'][1][-1]) - 1) * 2 + 1
                        energies[pos] += line['quantity']

            row.extend(potencies)
            row.extend(energies)
            sheet.add_row(row)
        sheet.save(output)
        return {'csv': base64.b64encode(output.getvalue())}

    _constraints = [(_cnt_llista_preu_compatible,
                     _(u"La lista de precios no es compatible con la tarifa "
                       u"de acceso."),
                     ['tarifa', 'llista_preu']), ]


GiscegasFacturacioFactura()


class GiscegasFacturacioFacturaLinia(osv.osv):

    _name = 'giscegas.facturacio.factura.linia'
    _inherit = 'giscegas.facturacio.factura.linia'

    #TODO Gas
    def product_id_change(self, cursor, uid, ids, pricelist, date_invoice,
                          product, uom, polissa_id=None, qty=0, name='',
                          type_='out_invoice', partner_id=False,
                          fposition_id=False, price_unit=False,
                          address_invoice_id=False, context=None):
        '''make the discount visible'''
        pricelist_obj = self.pool.get('product.pricelist')
        pricetype_obj = self.pool.get('product.price.type')
        product_obj = self.pool.get('product.product')

        if not context:
            context = {}

        res = super(GiscegasFacturacioFacturaLinia,
                    self).product_id_change(cursor, uid, ids, pricelist,
                          date_invoice, product, uom, polissa_id=polissa_id,
                          qty=qty, name=name, type_=type_,
                          partner_id=partner_id,
                          fposition_id=fposition_id, price_unit=price_unit,
                          address_invoice_id=address_invoice_id,
                          context=context)

        list = pricelist_obj.browse(cursor, uid, pricelist)
        if (type_ in ['out_invoice'] and
            product and list.gas_visible_discount):
            # Get pricelist version
            date = date_invoice or time.strftime('%Y-%m-%d')
            cursor.execute(
                '''SELECT id
                   FROM product_pricelist_version
                   WHERE pricelist_id = %s AND active=True
                   AND (date_start IS NULL OR date_start <= %s)
                   AND (date_end IS NULL OR date_end >= %s)
                   ORDER BY id LIMIT 1''', (pricelist, date, date))
            version_id = cursor.fetchone()[0]
            warning_msg = res.get('warning', False)
            if warning_msg:
                raise osv.except_osv(
                    warning_msg.get('title', _(u'Error')),
                    warning_msg.get('message')
                )
            price = res['value']['price_unit']
            # Get parent price
            prod = product_obj.browse(cursor, uid, product)
            category_ids = prod.get_categories()
            # Search for then item that give us the price for the product
            cursor.execute(
                '''SELECT i.*, pl.currency_id
                   FROM product_pricelist_item i
                   INNER JOIN product_pricelist_version v
                     ON i.price_version_id = v.id
                   INNER JOIN product_pricelist pl
                     ON v.pricelist_id = pl.id
                   WHERE
                     (coalesce(product_tmpl_id, 0) = 0 OR product_tmpl_id = %s)
                     AND (coalesce(product_id, 0) = 0 OR product_id = %s)
                     AND (coalesce(categ_id, 0) = 0 OR categ_id IN %s)
                     AND price_version_id = %s
                     AND (gas_min_quantity IS NULL OR gas_min_quantity <= %s)
                   ORDER BY sequence LIMIT 1''',
                   (prod.product_tmpl_id.id, prod.id, tuple(category_ids),
                    version_id, qty or 1.0))
            item_vals = cursor.dictfetchone()
            pricetype_id = item_vals['base']
            #if price is based on a product field
            if pricetype_id > 0:
                # Only allow visible discount for energy products
                # based on other pricelist. We cannot read directly from
                # product because of uom's and historic prices
                if prod.categ_id.name == 'variable':
                    parent_price = price
                else:
                    field_name = pricetype_obj.read(cursor, uid, pricetype_id,
                                                ['field'])['field']
                    parent_price = product_obj.read(cursor, uid, product,
                                                    [field_name])[field_name]
            #if price is based on another pricelist
            elif pricetype_id == -1:
                ctx = context.copy()
                ctx['uom'] = uom
                ctx['date'] = date
                parent_pricelist_id = item_vals['base_pricelist_id']
                parent_price = pricelist_obj.price_get(cursor, uid,
                               [parent_pricelist_id], product,
                               qty or 1.0, partner_id,
                               context=ctx)[parent_pricelist_id]
            elif pricetype_id == -3:
                parent_price = item_vals['base_price']
                product_uom = prod.uos_id or prod.uom_id
                parent_price = self.pool.get('product.uom')._compute_price(
                        cursor, uid, product_uom.id, parent_price, uom
                )
            elif pricetype_id == -4:
                parent_price = context['pricelist_base_price']
            else:
                return res
            #See if we have any changes between parent_price and price
            #If so, compute discount based on item fields
            if parent_price > price:
                discount = (parent_price - price) / parent_price * 100
                res['value']['price_unit'] = parent_price
                res['value']['discount'] = discount

        return res

    def create(self, cursor, uid, values, context=None):
        '''if discount present, create new line with discount quantity'''

        fact_obj = self.pool.get('giscegas.facturacio.factura')
        inv_obj = self.pool.get('account.invoice')
        lang_obj = self.pool.get('res.lang')
        config_obj = self.pool.get('res.config')
        show_discount_separate = int(
            config_obj.get(cursor, uid, 'giscegas_show_discount_separate', '1')
        )

        if not context:
            context = {}

        is_in_invoice = None

        if context.get('type_invoice', None):
            is_in_invoice = context['type_invoice'].startswith('in')
        else:
            inv_id = values.get('invoice_id', None)

            if not inv_id:
                if values.get('factura_id', None):
                    inv_id = fact_obj.read(
                        cursor, uid, values['factura_id'], ['invoice_id']
                    )['invoice_id'][0]

            if inv_id:
                invoice_type = inv_obj.read(
                    cursor, uid, inv_id, ['type']
                )['type']

                is_in_invoice = invoice_type.startswith('in')

        if values.get('discount', 0) and show_discount_separate and not is_in_invoice:
            #Search for lang
            lang_code = context.get('lang', 'ca_ES')
            lang_id = lang_obj.search(cursor, uid, [('code', '=', lang_code)])
            ctx = context.copy()
            discount_vals = values.copy()
            discount = discount_vals['discount']
            price = discount_vals['price_unit']
            discount_price = ((-1 * price) +
                              price *
                               (1 - (values['discount'] or 0.0) / 100.0))
            #update discount_vals
            discount_vals['price_unit'] = float_round(discount_price,
                                                int(config['price_accuracy']))
            discount_vals['discount'] = 0
            discount_vals['atrprice_subtotal'] = 0
            discount_vals['isdiscount'] = True
            if lang_id:
                format_discount = lang_obj.format(cursor, uid, lang_id,
                                              '%.2f', discount)
                format_price = lang_obj.format(cursor, uid, lang_id,
                                              '%.6f', price)
                discount_vals['note'] = _(u"-%s %% sobre %s" %
                                      (format_discount, format_price))
            else:
                discount_vals['note'] = _(u"-{:.2f} % sobre {:6f}").format(
                                            discount, price)
            #Remove discount from values
            values['discount'] = 0
            self.create(cursor, uid, discount_vals, context=context)
        #lloguer products are removed when the invoice
        #is created directly from comer. If not removed from
        #values when creating, lloguer lines will not group
        if (values['tipus'] == 'lloguer'
            and context.get('lloguer_comer', False)):
            values['product_id'] = 0
        return super(GiscegasFacturacioFacturaLinia,
                     self).create(cursor, uid, values, context=context)

    def _get_line_from_iline(self, cursor, uid, ids, context=None):

        invoice_line_obj = self.pool.get('account.invoice.line')
        factura_linia_obj = self.pool.get('giscegas.facturacio.factura.linia')

        search_params = [('invoice_line_id', 'in', ids)]
        return factura_linia_obj.search(cursor, uid, search_params)

    _columns = {
        'atrprice_subtotal': fields.float("Subtotal Peatges",
                                          digits=(16, 2), readonly=True),
        'isdiscount': fields.boolean("Línia de descompte")
    }

    _defaults = {
        'atrprice_subtotal': lambda *a: 0,
        'isdiscount': lambda *a: False,
    }

GiscegasFacturacioFacturaLinia()


class GiscegasFacturacioFacturaLectures(osv.osv):
    """Classe per les lectures d'energia de les factures.
    """

    _name = 'giscegas.facturacio.lectures'
    _inherit = 'giscegas.facturacio.lectures'

    def create(self, cursor, uid, vals, context=None):
        if 'comptador_id' in vals:
            comptador_obj = self.pool.get('giscegas.lectures.comptador')
            vals['comptador'] = comptador_obj.browse(cursor, uid,
                                                     vals['comptador_id']).name
        res_id = super(GiscegasFacturacioFacturaLectures,
                       self).create(cursor, uid, vals, context)
        return res_id

    _columns = {
         'comptador': fields.char('Comptador', size=64, required=True,
                                  readonly=True),
         'comptador_id': fields.many2one('giscegas.lectures.comptador',
                                         'Comptador', required=True,
                                         readonly=True),
    }

GiscegasFacturacioFacturaLectures()
