# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from tools import config


class TestGiscegasWizardFacturesPerEmail(testing.OOTestCase):

    def test_query_path_by_default(self):
        wiz_obj = self.openerp.pool.get('giscegas.wizard.factures.per.email')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            origin_query_file = ("%s/giscegas_facturacio_comer/sql"
                                 "/query_mostrar_sense_email.sql") % config['addons_path']
            query_file = wiz_obj.get_query_path(
                cursor, uid, 'query_mostrar_sense_email')
            assert query_file == origin_query_file
            origin_query_file = ("%s/giscegas_facturacio_comer/sql"
                                 "/query_fact_per_enviar.sql") % config['addons_path']
            query_file = wiz_obj.get_query_path(
                cursor, uid, 'query_fact_per_enviar')
            assert query_file == origin_query_file
            origin_query_file = ("%s/giscegas_facturacio_comer/sql"
                                 "/query_fact_per_lang.sql") % config['addons_path']
            query_file = wiz_obj.get_query_path(
                cursor, uid, 'query_fact_per_lang')
            assert query_file == origin_query_file

    def test_query_path_enable_contact(self):
        wiz_obj = self.openerp.pool.get('giscegas.wizard.factures.per.email')
        conf_obj = self.openerp.pool.get('res.config')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Enable contact query
            config_id = conf_obj.search(cursor, uid,
                [('name', '=', 'giscegas_fact_use_adr_contact_email')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})

            origin_query_file = ("%s/giscegas_facturacio_comer/sql"
                                 "/query_mostrar_sense_email_contact.sql") % config['addons_path']
            query_file = wiz_obj.get_query_path(
                cursor, uid, 'query_mostrar_sense_email'
            )
            assert query_file == origin_query_file
            origin_query_file = ("%s/giscegas_facturacio_comer/sql"
                                 "/query_fact_per_enviar_contact.sql") % config['addons_path']
            query_file = wiz_obj.get_query_path(
                cursor, uid, 'query_fact_per_enviar')
            assert query_file == origin_query_file
            origin_query_file = ("%s/giscegas_facturacio_comer/sql"
                                 "/query_fact_per_lang_contact.sql") % config['addons_path']
            query_file = wiz_obj.get_query_path(
                cursor, uid, 'query_fact_per_lang')
            assert query_file == origin_query_file