SELECT
  gpt.name AS tarifa,
  gff.id AS factura_id,
  ai.number,
  serie.code AS codi_serie,
  serie.name AS nom_serie,
  gpt.id AS tarifa_id,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p1_activa.consumo,0) * -1
      ELSE coalesce(p1_activa.consumo,0)
  END AS p1_consumo,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p2_activa.consumo,0) * -1
      ELSE coalesce(p2_activa.consumo,0)
  END AS p2_consumo,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p3_activa.consumo,0) * -1
      ELSE coalesce(p3_activa.consumo,0)
  END AS p3_consumo,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p4_activa.consumo,0) * -1
      ELSE coalesce(p4_activa.consumo,0)
  END AS p4_consumo,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p5_activa.consumo,0) * -1
      ELSE coalesce(p5_activa.consumo,0)
  END AS p5_consumo,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p6_activa.consumo,0) * -1
      ELSE coalesce(p6_activa.consumo,0)
  END AS p6_consumo,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p1_reactiva.consumo,0) * -1
      ELSE coalesce(p1_reactiva.consumo,0)
  END AS p1_consumor,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p2_reactiva.consumo,0) * -1
      ELSE coalesce(p2_reactiva.consumo,0)
  END AS p2_consumor,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p3_reactiva.consumo,0) * -1
      ELSE coalesce(p3_reactiva.consumo,0)
  END AS p3_consumor,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p4_reactiva.consumo,0) * -1
      ELSE coalesce(p4_reactiva.consumo,0)
  END AS p4_consumor,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p5_reactiva.consumo,0) * -1
      ELSE coalesce(p5_reactiva.consumo,0)
  END AS p5_consumor,
  CASE
      WHEN ai.type = 'out_refund' THEN coalesce(p6_reactiva.consumo,0) * -1
      ELSE coalesce(p6_reactiva.consumo,0)
  END AS p6_consumor,
  CASE
      WHEN ai.type = 'out_refund' THEN lectures_energia.cantidad * -1
      ELSE lectures_energia.cantidad
  END AS le_kw,
  CASE
      WHEN ai.type = 'out_refund' THEN lectures_energia.suma * -1
      ELSE lectures_energia.suma
  END AS le_euros,
  CASE
      WHEN ai.type = 'out_refund' THEN lectures_potencia.cantidad * -1
      ELSE lectures_potencia.cantidad
  END AS lp_kw,
  CASE
      WHEN ai.type = 'out_refund' THEN lectures_potencia.suma * -1
      ELSE lectures_potencia.suma
  END AS lp_euros,
  CASE
      WHEN ai.type = 'out_refund' THEN lectures_reactiva.cantidad * -1
      ELSE lectures_reactiva.cantidad
  END AS lr_kw,
  CASE
      WHEN ai.type = 'out_refund' THEN lectures_reactiva.suma * -1
      ELSE lectures_reactiva.suma
  END AS lr_euros,
  CASE
      WHEN ai.type = 'out_refund' THEN otros.suma * -1
      ELSE otros.suma
  END AS otros,
  CASE
      WHEN ai.type = 'out_refund' THEN impuesto_electricidad.amount * -1
      ELSE impuesto_electricidad.amount
  END AS ie_amount,
  CASE
      WHEN ai.type = 'out_refund' THEN lloguers.suma * -1
      ELSE lloguers.suma
  END AS lloguers_euros,
  CASE
      WHEN ai.type = 'out_refund' THEN excesos.suma * -1
      ELSE excesos.suma
  END AS excesos_euros,
  CASE
      WHEN ai.type = 'out_refund' then ai.amount_untaxed * -1
      ELSE ai.amount_untaxed
  END as amount_untaxed,
  CASE
      WHEN ai.type = 'out_refund' then ai.amount_tax * -1
      ELSE ai.amount_tax
  END as amount_tax,
  CASE
      WHEN ai.type = 'out_refund' then ai.amount_total * -1
      ELSE ai.amount_total
  END as amount_total
FROM giscegas_facturacio_factura gff
  INNER JOIN account_invoice ai ON gff.invoice_id = ai.id
  INNER JOIN product_pricelist gpt ON gpt.id = gff.llista_preu
  INNER JOIN account_journal journal ON journal.id = ai.journal_id
  INNER JOIN ir_sequence serie ON serie.id = journal.invoice_sequence_id
  LEFT JOIN
  (
    SELECT gffl.factura_id,gffl.tipus, max(ail.quantity) AS cantidad , sum(ail.price_subtotal) AS suma
    FROM giscegas_facturacio_factura_linia gffl
    LEFT JOIN account_invoice_line ail ON gffl.invoice_line_id = ail.id
    WHERE gffl.tipus = 'energia'
    GROUP BY gffl.factura_id, gffl.tipus
  ) lectures_energia ON gff.id = lectures_energia.factura_id
  LEFT JOIN
  (
    SELECT gffl.factura_id,gffl.tipus,avg(ail.quantity) AS cantidad , sum(ail.price_subtotal) AS suma
    FROM giscegas_facturacio_factura_linia gffl
    LEFT JOIN account_invoice_line ail ON gffl.invoice_line_id = ail.id
    WHERE gffl.tipus = 'potencia'
    GROUP BY gffl.factura_id,gffl.tipus
  ) lectures_potencia ON gff.id = lectures_potencia.factura_id
  LEFT JOIN
  (
    SELECT gffl.factura_id,gffl.tipus,max(ail.quantity) AS cantidad , sum(ail.price_subtotal) AS suma
    FROM giscegas_facturacio_factura_linia gffl
    LEFT JOIN account_invoice_line ail ON gffl.invoice_line_id = ail.id
    WHERE gffl.tipus = 'reactiva'
    GROUP BY gffl.factura_id,gffl.tipus
  ) lectures_reactiva ON gff.id = lectures_reactiva.factura_id
  LEFT JOIN
  (
    SELECT gffl.factura_id,gffl.tipus,max(ail.quantity) AS cantidad , sum(ail.price_subtotal) AS suma
    FROM giscegas_facturacio_factura_linia gffl
    LEFT JOIN account_invoice_line ail ON gffl.invoice_line_id = ail.id
    WHERE gffl.tipus = 'lloguer'
    GROUP BY gffl.factura_id,gffl.tipus
  ) lloguers ON gff.id = lloguers.factura_id
  LEFT JOIN
  (
    SELECT gffl.factura_id,gffl.tipus, sum(ail.price_subtotal) AS suma
    FROM giscegas_facturacio_factura_linia gffl
    LEFT JOIN account_invoice_line ail ON gffl.invoice_line_id = ail.id
    WHERE gffl.tipus = 'exces_potencia'
    GROUP BY gffl.factura_id,gffl.tipus
  ) excesos ON gff.id = excesos.factura_id
  LEFT JOIN
  (
    SELECT gffl.factura_id,gffl.tipus, sum(ail.price_subtotal) AS suma
    FROM giscegas_facturacio_factura_linia gffl
    LEFT JOIN account_invoice_line ail ON gffl.invoice_line_id = ail.id
    WHERE gffl.tipus = 'altres'
    GROUP BY gffl.factura_id,gffl.tipus
  ) otros ON gff.id = otros.factura_id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'energia' AND name LIKE '%%P1%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p1_activa ON p1_activa.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'energia' AND name LIKE '%%P2%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p2_activa ON p2_activa.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'energia' AND name LIKE '%%P3%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p3_activa ON p3_activa.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'energia' AND name LIKE '%%P4%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p4_activa ON p4_activa.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'energia' AND name LIKE '%%P5%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p5_activa ON p5_activa.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'energia' AND name LIKE '%%P6%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p6_activa ON p6_activa.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'reactiva' AND name LIKE '%%P1%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p1_reactiva ON p1_reactiva.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'reactiva' AND name LIKE '%%P2%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p2_reactiva ON p2_reactiva.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'reactiva' AND name LIKE '%%P3%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p3_reactiva ON p3_reactiva.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'reactiva' AND name LIKE '%%P4%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p4_reactiva ON p4_reactiva.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'reactiva' AND name LIKE '%%P5%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p5_reactiva ON p5_reactiva.factura_id = gff.id
  LEFT JOIN
  (
    SELECT sum(il.quantity) AS consumo, fl.factura_id FROM giscegas_facturacio_factura_linia fl
    INNER JOIN account_invoice_line il ON fl.invoice_line_id = il.id
    WHERE fl.tipus = 'reactiva' AND name LIKE '%%P6%%' AND fl.isdiscount = False
    GROUP BY fl.factura_id
  ) p6_reactiva ON p6_reactiva.factura_id = gff.id
  LEFT JOIN
  (
    SELECT invoice_id, amount
    FROM account_invoice_tax
    WHERE name LIKE '%%electricidad%%'
  ) impuesto_electricidad ON impuesto_electricidad.invoice_id = ai.id
  LEFT JOIN
  (
    SELECT invoice_id,sum(base_amount) AS base_amount,
    sum(amount) AS amount
    FROM account_invoice_tax
    WHERE name LIKE '%%IVA%%'
    GROUP BY invoice_id
  ) IVA ON IVA.invoice_id = ai.id
WHERE ai.date_invoice  between to_date(%s,'YYYY-MM-DD')
      AND to_date(%s,'YYYY-MM-DD')
      AND gff.facturacio IN %s AND gff.tipo_factura = '01'
      AND ai.state not IN %s
      AND serie.code IN %s
ORDER BY gpt.id