SELECT
  f.id
FROM giscegas_facturacio_factura f
INNER JOIN account_invoice i
  ON i.id = f.invoice_id
INNER JOIN res_partner_address address
  ON address.id = i.address_contact_id
WHERE
  lot_facturacio = %s
  AND per_enviar like '%%email%%'
  AND coalesce(address.email, '') = ''