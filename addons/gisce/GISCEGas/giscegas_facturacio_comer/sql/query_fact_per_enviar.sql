SELECT
  f.id
FROM giscegas_facturacio_factura f
INNER JOIN account_invoice i
  ON i.id = f.invoice_id
INNER JOIN res_partner_address address
  ON address.id = i.address_invoice_id
WHERE
  f.lot_facturacio = %s
  AND f.per_enviar like '%%email%%'
  AND f.enviat = false
  AND i.state IN ('open', 'paid')
  AND coalesce(address.email, '') != ''
