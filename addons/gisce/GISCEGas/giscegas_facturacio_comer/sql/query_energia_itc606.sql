SELECT f.polissa_id, sum(il.quantity)
FROM giscegas_facturacio_factura_linia fl
INNER JOIN account_invoice_line il
  ON il.id = fl.invoice_line_id
INNER JOIN giscegas_facturacio_factura f
  ON f.id = fl.factura_id
INNER JOIN account_invoice i
ON i.id = f.invoice_id
INNER JOIN account_journal journal
ON journal.id = i.journal_id
WHERE i.date_invoice between %s and %s
AND fl.tipus = 'energia' AND fl.isdiscount = False
AND journal.code like 'GAS%%'
AND i.state in ('open', 'paid')
AND f.tipo_rectificadora in ('N', 'R', 'RA')
AND i.type = 'out_invoice'
AND f.id not in
(SELECT ref
  FROM giscegas_facturacio_factura f
  INNER JOIN account_invoice i
  ON i.id = f.invoice_id
  INNER JOIN account_journal journal
  ON journal.id = i.journal_id
  WHERE f.tipo_rectificadora in ('A', 'B', 'BRA')
  AND journal.code like 'GAS%%'
  AND coalesce(ref, 0) <> 0)
GROUP BY f.polissa_id