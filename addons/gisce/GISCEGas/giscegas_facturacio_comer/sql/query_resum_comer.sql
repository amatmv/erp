SELECT
  distri.name AS "Distribuidora",
  distri.ref AS "Ref. Distribuidora",
  comer.name AS "Comercializadora",
  comer.ref AS "Cod. Comer.",
  pol.name AS "Contrato",
  REPLACE(tit.name,'"', '') AS "Nombre cliente",
  SUBSTRING(cups.name, 0, 21) AS "CUPS",
  REPLACE(cups.direccio,'"', '') AS "Dirección",
  state.code AS "Código Prov.",
  t.name AS "Tarifa Acceso",
  CASE WHEN ai.type LIKE 'out_%%' THEN ai.number
    WHEN ai.type LIKE 'in_%%' THEN ai.origin
  END AS "Factura",
  CASE WHEN f.tipo_rectificadora='A' THEN 'Anuladora'
    WHEN f.tipo_rectificadora='B' THEN 'Anuladora+Rectificadora'
    WHEN f.tipo_rectificadora='R' THEN 'Rectificadora'
    WHEN f.tipo_rectificadora='RA' THEN 'Rectificadora sin anuladora'
    WHEN f.tipo_rectificadora='BRA' THEN 'Anuladora (ficticia)'
    ELSE 'Normal'
  END AS "Tipo factura",
  ai.date_invoice AS "Fecha factura",
  f.data_inici AS "Fecha inicial",
  f.data_final AS "Fecha final",
  (f.data_final - f.data_inici) + 1 AS "Num. días",
  t.name AS "Tarifa",
  pp.name AS "Lista de precios",
  TO_CHAR(COALESCE(f.consumo_kwh, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D990') AS "Total término variable (kWh)",
  TO_CHAR(COALESCE(f.total_variable, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Importe término variable sin descuento",
  TO_CHAR(COALESCE(descuentos_variable.desc_ener, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Descuento término variable",
  TO_CHAR(COALESCE(f.total_termino_fijo, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Importe término fijo sin descuento",
  TO_CHAR(COALESCE(descuentos_fijo.descuento, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Descuento término fijo",
  TO_CHAR(COALESCE(it_hidrocarb.base, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990')  AS "Base IESH",
  TO_CHAR(COALESCE(it_hidrocarb.amount, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Total IESH",
  TO_CHAR(COALESCE(f.total_lloguers, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Total alquiler",
  TO_CHAR(COALESCE(altres.altres, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Otros conceptos",
  TO_CHAR(COALESCE(ai.amount_untaxed, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Total conceptos",
  TO_CHAR(COALESCE(it_21.base, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Base IVA",
  TO_CHAR(COALESCE(it_21.amount, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Total IVA",
  TO_CHAR(COALESCE(fianza.fianza, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Fianza",
  TO_CHAR(COALESCE(ai.amount_total, 0) * (CASE WHEN ai.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Total"
FROM
  giscegas_facturacio_factura f
  LEFT JOIN giscegas_cups_ps cups ON (f.cups_id=cups.id)
  LEFT JOIN giscegas_polissa pol ON (f.polissa_id=pol.id)
  LEFT JOIN account_invoice ai ON (ai.id=f.invoice_id)
  LEFT JOIN giscegas_polissa_tarifa t ON (t.id=f.tarifa_acces_id)
  LEFT JOIN product_pricelist pp ON (pp.id=f.llista_preu)
  LEFT JOIN (
    SELECT lf.factura_id AS factura_id,SUM(lai.price_subtotal) AS desc_ener
    FROM giscegas_facturacio_factura_linia lf
    LEFT JOIN account_invoice_line lai ON (lai.id=lf.invoice_line_id)
    WHERE
        lf.tipus ='variable'
        AND lf.isdiscount = True
    GROUP BY lf.factura_id
  ) AS descuentos_variable ON (descuentos_variable.factura_id=f.id)
  LEFT JOIN (
    SELECT lf.factura_id AS factura_id,SUM(lai.price_subtotal) AS descuento
    FROM giscegas_facturacio_factura_linia lf
    LEFT JOIN account_invoice_line lai ON (lai.id=lf.invoice_line_id)
    WHERE
        lf.tipus ='fijo'
        AND lf.isdiscount = True
    GROUP BY lf.factura_id
  ) AS descuentos_fijo ON (descuentos_fijo.factura_id=f.id)
  LEFT JOIN (
    SELECT lf.factura_id AS factura_id,SUM(lai.price_subtotal) AS altres
    FROM giscegas_facturacio_factura_linia lf
    LEFT JOIN account_invoice_line lai ON (lai.id=lf.invoice_line_id)
    WHERE lf.tipus='altres'
    GROUP BY lf.factura_id
  ) AS altres ON (altres.factura_id=f.id)
  LEFT JOIN (
    SELECT lai.invoice_id AS invoice_id, SUM(lai.price_subtotal) AS fianza
    FROM account_invoice_line lai
    JOIN product_product p ON (lai.product_id = p.id)
    WHERE p.default_code = 'FIANZA'
    GROUP BY lai.invoice_id
  ) AS fianza ON (fianza.invoice_id = ai.id)
  LEFT JOIN account_invoice_tax it_21 ON (it_21.invoice_id=ai.id AND (it_21.name LIKE 'IVA 21%%' OR it_21.name LIKE '21%%%% IVA%%'))
  LEFT JOIN account_invoice_tax it_hidrocarb ON (it_hidrocarb.invoice_id=ai.id AND it_hidrocarb.name LIKE '%%hidrocarburos%%')
  LEFT JOIN res_partner distri ON (distri.id=cups.distribuidora_id)
  LEFT JOIN res_partner comer ON (comer.id=ai.company_id)
  LEFT JOIN res_partner tit ON (tit.id = ai.partner_id)
  LEFT JOIN res_partner_address address ON (ai.address_invoice_id = address.id)
  LEFT JOIN res_country_state state ON (address.state_id = state.id)
  LEFT JOIN payment_type pt ON (ai.payment_type = pt.id)
WHERE 
  f.id IN %s
ORDER BY
  "Factura"
