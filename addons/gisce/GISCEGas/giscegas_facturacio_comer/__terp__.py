# -*- coding: utf-8 -*-
{
    "name": "Facturació (Comercialitzadora)",
    "description": """Facturació (Comercialitzadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscegas_facturacio",
        "giscegas_polissa_comer",
        "purchase",
        "giscegas_lectures_comer",
        "poweremail",
        "poweremail_references",
        "giscedata_administracio_publica",
        "giscemisc_participant"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscegas_polissa_demo.xml",
        "giscegas_facturacio_comer_demo.xml"
    ],
    "update_xml":[
        "giscegas_facturacio_view.xml",
        "giscegas_facturacio_data.xml",
        "giscegas_polissa_view.xml",
        "giscegas_lectures_view.xml",
        "wizard/giscegas_config_taxes_facturacio_comer_view.xml",
        "product_view.xml",
        "pricelist_view.xml",
        "giscegas_facturacio_comer_wizard.xml",
        "wizard/giscegas_wizard_factures_per_email_view.xml",
        "wizard/giscegas_wizard_payer_change.xml",
        "wizard/giscegas_wizard_invoice_open_and_send.xml",
        "wizard/giscegas_pricelist_from_file_view.xml",
        "wizard/giscegas_wizard_bank_account_change.xml",
        "wizard/wizard_list_cups_notification_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
