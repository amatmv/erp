# -*- coding: utf-8 -*-
"""Modififiquem coses del package giscegas_polissa.
"""
from osv import osv, fields
from tools.translate import _

TIPUS_ENVIAMENT = [("postal", "Correo postal"),
                   ("email", "E-mail"),
                   ("postal+email", "Correo postal y e-mail")]

class GiscegasPolissa(osv.osv):
    """Afegim una constraint per tal de comprovar si la tarifa és vàlida.
    """
    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def onchange_tipo_pago(self, cursor, uid, ids, tipo_pago, pagador, context=None):
        """ This function  automatize the completation of some fields
            if payment type is Recibo_CBS (Recibo domiciliado)
              if only exist one bank account automatic add it in field, 
              the same for payment group (Sepa 19)
        """
        res = super(GiscegasPolissa, self).onchange_tipo_pago(
            cursor, uid, ids, tipo_pago, pagador, context
        )
        if tipo_pago:
            tpg = self.pool.get('payment.type').browse(cursor, uid, tipo_pago)
            if tpg.code == 'RECIBO_CSB':
                pm_obj = self.pool.get('payment.mode')
                pm_ids = pm_obj.search(cursor, uid, [
                    ('type', '=', tpg.id),
                    ('tipo', '=', 'sepa19')
                ])
                if len(pm_ids) == 1:
                    res['value'].update({'payment_mode_id': pm_ids[0]})

        return res

    def onchange_modo_pago(self, cursor, uid, ids, modo_pago, pagador, context=None):
        if not modo_pago:
            return {}
        pm_obj = self.pool.get('payment.mode')
        pminfo = pm_obj.read(cursor, uid, modo_pago, ['type'], context=context)
        if not pminfo['type']:
            return {}
        tipo_pago = pminfo['type']
        if tipo_pago and isinstance(tipo_pago, tuple):
            tipo_pago = tipo_pago[0]
        res = self.onchange_tipo_pago(
            cursor, uid, ids, tipo_pago, pagador, context
        )
        res['value']['tipo_pago'] = tipo_pago
        return res

    def _default_enviament(self, cursor, uid, context=None):
        """Obté el valor per defecte de l'enviament segons configuració
        """
        cfg_obj = self.pool.get('res.config')
        cfg_id = cfg_obj.search(cursor, uid,
                                [('name', '=', 'giscegas_polissa_enviament_default')])
        if not cfg_id:
            valor = 'postal'
        else:
            valor = cfg_obj.read(cursor, uid, cfg_id[0], ['value'])['value']
        return valor

    def _ff_emails(self, cursor, uid, ids, field_name, arg,
                          context=None):
        if not context:
            context = {}
        res = {}
        for polissa in self.browse(cursor, uid, ids):
            res[polissa.id] = {
                'pagador_email': polissa.direccio_pagament.email,
                'notificacio_email': polissa.direccio_notificacio.email
            }
        return res

    _columns = {
        'payment_mode_id': fields.many2one(
            'payment.mode', 'Grupo de pago', readonly=True,
            states={'modcontractual': [('readonly', False),
                                       ('required', True)],
                    'esborrany': [('readonly', False)],
                    'validar': [('readonly', False),
                                ('required', True)]}
        ),
        'enviament': fields.selection(
            TIPUS_ENVIAMENT,
            "Enviar factura vía",
            readonly=True,
            states={'modcontractual': [('readonly', False), ('required', True)],
                    'esborrany': [('readonly', False)],
                    'validar': [('readonly', False)]}
        ),
        'pagador_email': fields.function(_ff_emails, method=True,
                                         type='char', readonly=True,
                                         string=u'Email dir. fiscal', size=240,
                                         multi='emails'),
        'notificacio_email': fields.function(_ff_emails, method=True,
                                             type='char', readonly=True,
                                             string=u'Email dir. notif.',
                                             size=240, multi='emails'),
        'refacturacio_pendent': fields.boolean('Refacturación Pendiente')
    }

    _defaults = {
      'enviament': _default_enviament,
      'refacturacio_pendent': lambda *a: 0,
    }

    def _cnt_llista_preu_compatible(self, cursor, uid, ids):
        """Comprovem que la llista de preu és compatible amb la tarifa.
        """
        for polissa in self.browse(cursor, uid, ids):
            if not polissa.llista_preu or not polissa.tarifa:
                continue
            if polissa.llista_preu not in polissa.tarifa.llistes_preus_comptatibles:
                return False
        return True
    _constraints = [
        (
            _cnt_llista_preu_compatible,
            _(u"La lista de precio no es compatible con la tarifa de acceso."),
            ['tarifa']
        )
    ]

    def update_mandate(self, cursor, uid, polissa_id, context=None):
        """Afegim el codi de creditor segons el grup de pagament.
        """
        res = super(GiscegasPolissa,
                    self).update_mandate(cursor, uid, polissa_id, context)
        if not context:
            context = {}

        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]

        polissa = self.browse(cursor, uid, polissa_id)
        if polissa.payment_mode_id:
            res['creditor_code'] = polissa.payment_mode_id.sepa_creditor_code
        return res

GiscegasPolissa()

class GiscegasPolissaModcontractual(osv.osv):
    
    _name = 'giscegas.polissa.modcontractual'
    _inherit = 'giscegas.polissa.modcontractual'

    _columns = {
        'payment_mode_id': fields.many2one('payment.mode', 'Grupo de pago'),
        'enviament': fields.selection(TIPUS_ENVIAMENT, "Enviar factura vía"),
        'pagador_email': fields.char('Email dir. fiscal', size=240, readonly=True),
        'notificacio_email': fields.char(
            'Email dir. notif.', size=240, readonly=True
        ),
    }

GiscegasPolissaModcontractual()


class GiscegasPolissaTarifa(osv.osv):
    """Modifiquem la classe per ficar amb quines llistes de preus és compatible.
    """
    _name = 'giscegas.polissa.tarifa'
    _inherit = 'giscegas.polissa.tarifa'

    _columns = {
        'llistes_preus_comptatibles': fields.many2many(
            'product.pricelist', 'giscegas_polissa_tarifa_pricelist',
            'tarifa_id', 'pricelist_id', 'Listas de precios compatibles'
        )
    }

GiscegasPolissaTarifa()
