# -*- encoding: utf-8 -*-
from osv import osv


class GiscegasFacturacioFacturador(osv.osv):

    _name = 'giscegas.facturacio.facturador'
    _inherit = 'giscegas.facturacio.facturador'

    def fact_canons(self, facturador, canon, data_inici_periode_f, data_final_periode_f, context=None):
        """
        Invoices Energy
        :param facturador: Tariff class
        """
        canon_name = canon.name

        canon_start_date = canon.data_alta
        canon_end_date = canon.data_baixa

        facturador.conf['canon'] = [canon_name]

        facturador.factura_canon(
            data_inici_periode_f, data_final_periode_f,
            canon.preu_dia,
            canon_start_date, canon_end_date
        )

        return {canon_name: canon.canon_product_id.id}

    def crear_linies_canon(self, cursor, uid, factura_id, vals, context=None):
        """Creem les línies de canon per la factura segons els termes."""
        # vals['force_price'] = price -> ITS DONE IN LIBFACT

        # We do that to avoid problems with existing sql queries
        # Lo que se deberia hacer es crear un tipo:
        # <record model="giscegas.facturacio.factura.linia.tipus"
        #                                                id="linia_tipus_canon">
        #     <field name="name">Canon</field>
        #     <field name="codi">canon</field>
        # </record>
        # Pero romperia las consultas sql

        vals.update({'tipus': 'lloguer'})

        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')

        line_id = super(GiscegasFacturacioFacturador, self).crear_linies_canon(
            cursor, uid, factura_id, vals, context=context
        )

        # For the moment we want
        # if line_id:
        #     linia_obj.write(cursor, uid, [line_id], {'product_id': False})

        return line_id

    def crear_linies_canon_account_invoice(
            self, cursor, uid, factura_id, vals, context=None):
        """Creem les línies de canon per la factura (invoice) segons els termes.
        """
        return super(GiscegasFacturacioFacturador, self).crear_linies_canon_account_invoice(
            cursor, uid, factura_id, vals, context=context
        )


GiscegasFacturacioFacturador()
