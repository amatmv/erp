# -*- coding: utf-8 -*-

from osv import osv, fields
from oorq.decorators import job
from tools.translate import _
from tools.misc import cache
from tools import config
from addons import get_module_resource
import tools

class GiscegasWizardFacturesPerEmail(osv.osv_memory):
    _name = 'giscegas.wizard.factures.per.email'

    def get_query_path(self, cursor, uid, query_file):
        conf_obj = self.pool.get('res.config')
        use_adr_contact = int(
            conf_obj.get(cursor, uid, 'giscegas_fact_use_adr_contact_email', '0')
        )
        query_suffix = ''
        if use_adr_contact:
            query_suffix = '_contact'
        query_file  = "{}{}.sql".format(query_file, query_suffix)
        return get_module_resource(
            'giscegas_facturacio_comer', 'sql', query_file)

    def action_mostrar_sense_email(self, cursor, uid, ids, context=None):
        query_file = self.get_query_path(
            cursor, uid, 'query_mostrar_sense_email')
        query = open(query_file).read()
        lot_id = context.get('active_id', False)
        cursor.execute(query, (lot_id, ))
        
        fact_ids = [x[0] for x in cursor.fetchall()]

        return {
            'name': 'Factures',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.facturacio.factura',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', fact_ids)],
        }

    def action_enviar_lot_per_mail(self, cursor, uid, ids, context=None):
        """Enviar les factures la pòlissa de les quals està configurada
           amb l'enviament per correu electrònic
        """
        if not context:
            context = {}

        query_file = self.get_query_path(cursor, uid, 'query_fact_per_enviar')
        query = open(query_file).read()
        lot_id = context.get('active_id', False)
        cursor.execute(query, (lot_id, ))
        
        fact_ids = [x[0] for x in cursor.fetchall()]

        return self.send_invoices_by_email(cursor, uid, ids, fact_ids,
                                           context=context)

    def send_invoices_by_email(self, cursor, uid, ids, fact_ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])
        ir_mod_dat = self.pool.get('ir.model.data')

        if not fact_ids:
            msg = _("No s'ha trobat cap factura per enviar per email\n"
                    "Si n'hi ha d'haver alguna s'ha de revisar que el "
                    "contracte estigui marcat per enviar per email i que "
                    "tingui assignada una direcció d'email.")
            wizard.write({'state': 'err', 'info': msg})
            return True

        tmpl = ir_mod_dat._get_obj(cursor, uid,
                                   'giscegas_facturacio_comer',
                                   'env_fact_via_email')

        ctx = context.copy()

        ctx.update({
            'state': 'single',
            'priority': '0',
            'from': tmpl.enforce_from_account.id,
            'template_id': tmpl.id,
            'src_model': 'giscegas.facturacio.factura',
            'type': 'out_invoice'
        })

        for fact_id in fact_ids:
            ctx.update({
                'src_rec_ids': [fact_id],
                'active_ids': [fact_id],
            })
            self.action_enviar_lot_per_mail_background(cursor, uid, ids, ctx)

        return {'type': 'ir.actions.act_window_close'}

    @job(queue=config.get('poweremail_render_queue', 'poweremail'))
    def action_enviar_lot_per_mail_background(self, cursor, uid, ids, context=None):
        email_wizard_obj = self.pool.get('poweremail.send.wizard')

        wiz_vals = {
                    'state': context['state'],
                    'priority': context['priority'],
                    'from': context['from'],
        }
        pe_wiz = email_wizard_obj.create(cursor, uid, wiz_vals,
                                         context=context)
        return email_wizard_obj.send_mail(cursor, uid, [pe_wiz], context=context)

    def _fact_per_lang(self, cursor, uid, context=None):
        if not context:
            context = {}
        lot_id = context.get('active_id', False)
        query_file = self.get_query_path(cursor, uid, 'query_fact_per_lang')
        query = open(query_file).read()
        cursor.execute(query, (lot_id, ))

        return [x for x in cursor.fetchall()]

    def _default_info(self, cursor, uid, context=None):
        """ Fem un resum per poder escollir quins idiomes tenim pendents per
        " enviar.
        """
        msg = ''
        if msg:
            msg += '\n\n'
            
        msg_send  = _('enviats')
        msg_no_mail = _('sense correu')
        msg_no_send = _('no enviats')
        msg_no_lang = _('Sense idioma')
        msg += _("Estat de les factures per enviar:\n")
        for vals in self._fact_per_lang(cursor, uid, context):
            num, send, lang, email_ok = vals
            lang_msg = lang == '*' and msg_no_lang or lang
            status_msg = send and msg_send or msg_no_send
            status_msg = email_ok and status_msg or msg_no_mail 
            msg += _(" * %s (%s): %s \n") % (lang_msg, status_msg, num)
            
        msg += _(u"\nSi pitja continuar s'enviaran totes les factures "
                 u"no enviades amb correu electrònic")
        return msg

    _columns = {
        'state': fields.char('Estat', size=16, required=True),
        'info': fields.text('info', readonly=True),
    }

    _defaults = {
        'info': _default_info,
        'state': lambda *a: 'init',
    }

GiscegasWizardFacturesPerEmail()
