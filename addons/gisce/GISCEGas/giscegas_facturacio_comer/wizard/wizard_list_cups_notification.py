# -*- coding: utf-8 -*-
import base64

from osv import osv, fields
from tools.translate import _


class WizardListCupsNotificacio(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'wizard.list.cups.notificacio'

    def action_generate_file(self, cursor, uid, ids, context=None):
        partner_obj = self.pool.get('res.partner')
        participant_obj = self.pool.get('giscemisc.participant')
        wiz_vals = self.read(cursor, uid, ids[0], ['distribuidora', 'comunitat'])[0]
        participant_id = wiz_vals['distribuidora']
        distri_id = participant_obj.read(cursor, uid, participant_id, ['partner_id'])['partner_id'][0]
        comunitat_id = wiz_vals['comunitat']
        cups_ids = self.get_cups_from_distri(cursor, uid, distri_id, comunitat_id, context=context)

        file_data = _("CUPS;DNI;Nombre;Apellidos;Provincia;Municipio;Calle;Portal;Piso;Letra/Mano;CP;Telefono;Movil;Correo Electrónico\n")
        if not cups_ids:
            file_data += _(u"No se han encontrado registros")
        else:
            field_count = 0
            for cups in cups_ids:
                for field in cups:
                    if field is None:
                        field = ''
                    if field_count == 2:
                        nom = ''
                        cognoms = ''
                        if cups[1] and not partner_obj.is_enterprise_vat(cups[1]):
                            name_dict = partner_obj.separa_cognoms(cursor, uid, field, context=context)
                            if name_dict['cognoms']:
                                cognoms = "{} {}".format(name_dict['cognoms'][0], name_dict['cognoms'][1])
                            nom = name_dict['nom']
                        field = "{};{}".format(nom, cognoms)
                    file_data += "{};".format(field)
                    field_count += 1
                file_data += '\n'
                field_count = 0

        csv_file = base64.b64encode(file_data)
        csv_name = 'Listado direcciones notificación CUPS.csv'

        res = {'export_file': csv_file,
               'file_name': csv_name,
               'state': 'done'}

        self.write(cursor, uid, ids, res)
        return True

    def get_cups_from_distri(self, cursor, uid, distri_id, comunitat_id, context=None):

        query = """ SELECT cups.name as cups_name, tit.vat as dni, tit.name as name, prov.name as provincia, 
                    mun.name as municipi, add.nv as calle, add.pnp as portal, add.pt as piso, add.pu as letra, 
                    add.zip as postal, add.phone as fixe, add.mobile as mobile, add.email as correu 
        FROM giscegas_cups_ps cups
        LEFT JOIN giscegas_polissa pol ON pol.cups = cups.id
        LEFT JOIN res_partner_address add ON add.id = pol.direccio_pagament
        LEFT JOIN res_partner tit ON add.partner_id = tit.id
        LEFT JOIN res_municipi mun ON add.id_municipi = mun.id
        LEFT JOIN res_country_state prov ON mun.state = prov.id 
        LEFT JOIN res_comunitat_autonoma aut ON prov.comunitat_autonoma = aut.id
        WHERE cups.distribuidora_id = {} AND aut.id = {} 
        AND pol.state in ('activa', 'impagament', 'tall')
        """

        cursor.execute(query.format(distri_id, comunitat_id))
        cups_records = cursor.fetchall()
        return cups_records

    _columns = {
        'distribuidora': fields.many2one('giscemisc.participant', 'Distribuidora', required=True,
                                         domain="[('participant_type', '=', 'distri'),"
                                                "('sector_gas','=', True)]"),
        'comunitat': fields.many2one('res.comunitat_autonoma', 'C. autónoma', required=True),
        'export_file': fields.binary('fitxer'),
        'file_name': fields.char('Nom del fitxer', size=64),
        'info': fields.text('Info', size=4000),
        'state': fields.char('State', size=16),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardListCupsNotificacio()