# -*- coding: utf-8 -*-
from osv import fields, osv


class GiscegasConfigTaxesFacturacioComer(osv.osv_memory):
    _name = 'giscegas.config.taxes.facturacio.comer'

    def _get_productes_termes_gas(self, cursor, uid, context=None):
        """ Get the products of the products """

        if 'terme' not in context or context['terme'] not in ('fix', 'variable'):
            return []

        md_obj = self.pool.get('ir.model.data')
        product_obj = self.pool.get('product.product')

        terme = 'v' if context['terme'] == 'variable' else 'f'
        categs = [
            'categ_{}_t31_gas', 'categ_{}_t32_gas',
            'categ_{}_t33_gas', 'categ_{}_t34_gas',
        ]
        categs = list(map(lambda c: c.format(terme), categs))

        categ_ids = [
            md_obj.get_object_reference(cursor, uid, 'giscegas_polissa', categ)[1]
            for categ in categs
        ]
        return product_obj.search(cursor, uid, [
            ('categ_id', 'child_of', categ_ids)
        ])

    def action_set(self, cursor, uid, ids, context=None):

        if context is None:
            context = {}

        if isinstance(ids, (tuple, list)):
            ids = ids[0]

        wiz_values = self.read(
            cursor, uid, ids, ['iva_venda', 'iesh_venda', 'iva_compra'], context
        )[0]

        product_obj = self.pool.get('product.product')
        md_obj = self.pool.get('ir.model.data')

        ctx = context.copy()
        ctx.update({'terme': 'variable'})
        productes_variable = self._get_productes_termes_gas(
            cursor, uid, context=ctx
        )

        ctx['terme'] = 'fix'
        productes_fixe = self._get_productes_termes_gas(cursor, uid, context=ctx)

        taxes_fixe = {'taxes_id': [(6, 0, [wiz_values['iva_venda']])]}
        taxes_variable = {
            'taxes_id': [
                (6, 0, [wiz_values['iesh_venda'], wiz_values['iva_venda']])
            ]
        }

        product_obj.write(cursor, uid, productes_variable, taxes_variable)
        product_obj.write(cursor, uid, productes_fixe, taxes_fixe)

        # Ara falta tots els lloguers de comptadors
        vals = {
            'taxes_id': [(6, 0, [wiz_values['iva_venda']])],
            'supplier_taxes_id': [(6, 0, [wiz_values['iva_compra']])]
        }

        # busquem l'id de (categ_alq_conta) child_of
        md_id = md_obj.search(cursor, uid, [
            ('module', '=', 'giscegas_lectures'),
            ('name', '=', 'categ_alq_conta_gas')
        ])[0]
        categ_id = md_obj.read(cursor, uid, md_id, ['res_id'])['res_id']

        pids = product_obj.search(cursor, uid, [
            ('categ_id', 'child_of', [categ_id])
        ])
        # Escrivim a tots aquests productes 
        product_obj.write(cursor, uid, pids, vals)
        return {
            'view_type': 'form',
            "view_mode": 'form',
            'res_model': 'ir.actions.configuration.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    _columns = {
        'iva_venda': fields.many2one(
            'account.tax', 'IVA (venta)', domain=[('type_tax_use', '=', 'sale')]
        ),
        'iesh_venda': fields.many2one(
            'account.tax', 'IESH (Impuesto Especial Sobre Hidrocarburos)',
            domain=[('type_tax_use', '=', 'sale')]
        ),
        'iva_compra': fields.many2one(
            'account.tax', 'IVA (compra)',
            domain=[('type_tax_use', '=', 'purchase')]
        )
    }


GiscegasConfigTaxesFacturacioComer()
