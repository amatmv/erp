# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from tools import config
from giscegas_facturacio.wizard.wizard_informes_facturacio import (
    MULTI_SEQUENCE_GAS, INFORMES_GAS_DESC, INFORMES_EXPORTABLES
)

INFORMES_GAS_DESC.update(
    {
        'export_factures_desglossat':
        _(u"Resum de les factures en CSV."),
    }
)

INFORMES_EXPORTABLES.update({
    'export_factures_desglossat': "{}/giscegas_facturacio_comer/sql/query_resum_comer.sql".format(config['addons_path'])
})

MULTI_SEQUENCE_GAS.update({})


class GiscegasWizardInformesFacturacioComer(osv.osv_memory):

    _name = "giscegas.wizard.informes.facturacio"

    _inherit = "giscegas.wizard.informes.facturacio"

    def _get_informe(self, cursor, uid, context=None):
        """ Com a funció perquè si no no tradueix"""
        _opcions = super(GiscegasWizardInformesFacturacioComer, self)._get_informe(
            cursor, uid, context=context)

        _opcions += [
            ('export_factures_desglossat', _('Resum Factures (CSV)')),
         ]

        return _opcions

    _columns = {
        'informe': fields.selection(_get_informe, 'Informe', required=True),
    }

    def exportar(self, cursor, uid, ids, context=None):
        """ Retorna el CSV sol·licitat """

        if not context:
            context = {}
        is_exported = super(GiscegasWizardInformesFacturacioComer, self).exportar(
            cursor, uid, ids, context=context
        )

        if not is_exported:
            wizard = self.browse(cursor, uid, ids[0])
            vals = {}
            if wizard.informe == 'export_factures_desglossat':
                res, info = self.exportar_resum_factures(
                    cursor, uid, ids, context
                )
            else:
                return is_exported

            vals = {
                'file': res,
                'estat': 'done',
                'info': info,
            }
            wizard.write(vals)
            is_exported = True

        return is_exported


GiscegasWizardInformesFacturacioComer()
