# -*- coding: utf-8 -*-

import csv
import base64
import StringIO
from datetime import datetime
from datetime import timedelta

from osv import osv
from osv import fields
from tools.translate import _

class GiscegasWizardPricelistFromFile(osv.osv_memory):
    '''Pujar un fitxer csv per crear una llista de preus.
    Cada línia ha d'acabar en '\n' i els camps han d'estar separats per
    punt i coma.

    Cada línia ha de tenir el format següent:

    tarifes compatibles separades per coma;
    nom de la llista de preus; preu P1 energia;...;[Preu P1 potencia; ...]

    tarifa_compatible1[,tarifa_compatible2,tarifa_compatible3...];
    nom de la llista de preus;
    preu P1[;preu P2; preu P3]...,

    Exemple:

    Línia amb preus de potència:
    2.0A;1601 20A 0550 ME;0,126162;42.043426

    Una sense preus de potència:
    3.1A,3.1A LB;1608 31A 1500 CM;0.123248;0.104112;0.705466

    La mateixa però amb preus de potència
    3.1A,3.1A LB;1608 31A 1500 CM;0.123248;0.104112;0.705466;50;45;35

    '''

    _name = 'giscegas.wizard.pricelist.from.file'

    def get_base_pricelist(self, cr, uid, ids, context=None):
        imd_obj = self.pool.get('ir.model.data')
        peatge_pl = imd_obj.get_object_reference(cr, uid,
            'giscegas_facturacio',
            'pricelist_tarifas_gas'
        )[1]
        return peatge_pl

    def create_item(self, cr, uid, vals):
        prod_list_item_obj = self.pool.get('product.pricelist.item')
        prod_list_item_obj.create(cr, uid, vals)
        return True

    def action_upload_pricelist(self, cr, uid, ids, context=None):

        prod_list_obj = self.pool.get('product.pricelist')
        prod_list_v_obj = self.pool.get('product.pricelist.version')
        tarifa_obj = self.pool.get('giscegas.polissa.tarifa')
        period_obj = self.pool.get('giscegas.polissa.tarifa.periodes')

        wizard = self.browse(cr, uid, ids[0])
        fitxer = StringIO.StringIO(base64.b64decode(wizard.pricelist_file))
        reader = csv.reader(fitxer, delimiter=';')

        for row in reader:

            if not row:
                self.write(cr, uid, ids,
                           {'state': 'end',
                            })
                return True

            resum = self.read(cr, uid, ids[0])[0]['resum']

            # mirem que tinguem les tarifes d'accés indicades
            tarifes_acces_compatibles = row[0]
            tarifes_compatibles = []
            for tarifa_name in tarifes_acces_compatibles.split(','):
                param = [('name', '=', tarifa_name)]
                tarifa_id = tarifa_obj.search(cr, uid, param)
                if len(tarifa_id) == 1:
                    tarifa = tarifa_obj.browse(cr, uid, tarifa_id[0])
                    tarifes_compatibles.append(tarifa)
                else:
                    self.write(cr, uid, ids,
                               {'error': _(u'Tariff not found: {0}\n'
                                           u'No pricelist was created.').format(
                                   tarifa_name
                               ),
                                'state': 'error',
                                })
                    return False

            # mirem si la llista de preus existeix
            list_name = row[1]
            search_params = [('type', '=', 'sale'),
                             ('name', '=', list_name)]
            list_id = prod_list_obj.search(cr, uid, search_params)

            if list_id:
                list_id = list_id[0]
                resum += _('\nTariff: {0}, pricelist: {1} exists').\
                    format(tarifa_name,
                           list_name
                           )

            # si no existeix la creem
            else:
                list_id = prod_list_obj.create(cr, uid, {
                    'name': list_name,
                    'type': 'sale',
                    'gas_visible_discount': True
                })
                resum += _('\nTariff: {0}, pricelist: {1} created').\
                    format(tarifa_name,
                           list_name
                           )

            # i la fem compatible amb les tarifes indicades
            for tar in tarifes_compatibles:
                if list_id not in [x.id for x in tar.llistes_preus_comptatibles]:
                    # 4 és l'id de la tarifa de preus d'electricitat
                    tar.write({'llistes_preus_comptatibles': [(4, list_id), ]})

            # sempre intentem crear una nova versió de preus
            version_date_start = wizard.data_inici
            version_date_end = wizard.data_final
            v_name = '{0} {1}'.format(list_name, version_date_start)
            search_params = [('pricelist_id', '=', list_id),
                             ('name', '=', v_name),
                             ('date_start', '=', version_date_start)]

            version_id = prod_list_v_obj.search(cr, uid, search_params)

            if version_id:
                # ja existeix una versió de preus que comença en la mateixa
                # data, no la creem
                resum += _('\nA pricelist version already exists for '
                           'pricelist {0} with start date {1}. '
                           'Pricelist version was not created.').\
                    format(list_name, version_date_start)
                self.write(cr, uid, ids, {'resum': resum})
                continue

            else:

                # hem de tancar la versió anterior, si n'hi ha,
                # amb data 1 dia abans de la data inici de la nova
                params = [('pricelist_id', '=', list_id),
                                 ('date_start', '<=', wizard.data_inici),
                                 ('date_end' ,'>=', wizard.data_inici)]
                params_o = [('pricelist_id', '=', list_id),
                            ('date_start', '<=', wizard.data_inici),
                            ('date_end', '=', False)]

                version_existent = prod_list_v_obj.search(cr, uid, params)
                if not version_existent:
                    version_existent = prod_list_v_obj.search(cr, uid, params_o)

                if version_existent:

                    name_existent = prod_list_v_obj.read(
                        cr, uid, version_existent, ['name']
                    )[0]['name']

                    inici = datetime.strptime(wizard.data_inici, '%Y-%m-%d')
                    dia_abans = inici - timedelta(days=1)
                    dia_abans_str = dia_abans.strftime('%Y-%m-%d')

                    prod_list_v_obj.write(cr, uid, version_existent,
                                          {'date_end': dia_abans_str})
                    resum += '\nCerrada la versión de precios {0} a fecha {1}'.\
                        format(
                        name_existent,
                        dia_abans_str
                    )

                # un cop tancada l'anterior (si calia), creem la nova
                version_id = prod_list_v_obj.create(cr, uid,
                    {'name': v_name,
                     'pricelist_id': list_id,
                     'date_start': version_date_start,
                     'date_end': version_date_end,
                     }
                )
                vals = {
                    'name': 'Otros',
                    'sequence': 999,
                    'price_version_id': version_id,
                    'base_pricelist_id': self.get_base_pricelist(cr, uid, ids)
                }

                # creem la regla de tarifa 'otros'
                self.create_item(cr, uid, vals)

            # busquem el num de periodes d'energia de la tarifa compatible
            params = [('tarifa','=',tarifes_compatibles[0].id),
                      ('agrupat_amb', '=', False),
                      ('tipus', '=', 'te'),
                      ]
            periodes = period_obj.search(cr, uid, params)

            # define -3 on base indicates that item is based on pricelist price
            vals = {'price_version_id': version_id,
                    'sequence': 5,
                    'base': -3
                    }

            # creem els preus d'energia
            pos = 2
            for i in range(len(periodes)):
                price = float(row[pos+i].replace(',', '.'))
                nom_periode = 'P{0}'.format(i+1)
                params = [('tarifa', '=', tarifes_compatibles[0].id),
                          ('product_id.name', '=', nom_periode),
                          ('tipus', '=', 'te')]
                periode_id = period_obj.search(cr, uid, params)
                periode = period_obj.browse(cr, uid, periode_id)[0]

                vals_per = vals.copy()
                name = '{}_GAS_{}'.format(periode.name, v_name)
                vals_per.update({
                    'name': name,
                    'product_id': periode.product_id.id,
                })
                vals_per.update({
                    # 'price_discount': discount,
                    'base_price': price,

                })
                self.create_item(cr, uid, vals_per)

            # mirem si tenim preus de potència a continuació i els creem
            pos = pos + len(periodes)
            if len(row) - 1 >= pos:

                # busquem el num de periodes de potencia de la tarifa compatible
                params = [('tarifa','=',tarifes_compatibles[0].id),
                          ('agrupat_amb', '=', False),
                          ('tipus', '=', 'tp')]
                periodes = period_obj.search(cr, uid, params)

                for i in range(len(periodes)):
                    price = float(row[pos+i].replace(',', '.'))
                    nom_periode = 'P{0}'.format(i+1)
                    params = [('tarifa', '=', tarifes_compatibles[0].id),
                              ('product_id.name', '=', nom_periode),
                              ('tipus', '=', 'tp')]
                    periode_id = period_obj.search(cr, uid, params)
                    periode = period_obj.browse(cr, uid, periode_id)[0]

                    vals_per = vals.copy()
                    name = '{}_POTENCIA_{}'.format(periode.name, v_name)
                    vals_per.update({
                        'name': name,
                        'product_id': periode.product_id.id,
                    })
                    vals_per.update({
                        # 'price_discount': discount,
                        'base_price': price,

                    })
                    self.create_item(cr, uid, vals_per)

            resum += _('\nPricelist version created {0}').format(v_name)

            self.write(cr, uid, ids,
                       {'resum': resum})

        return True


    _columns = {
        'pricelist_file':fields.binary('Fitxer de tarifa',
            required=True,
            filters=None,
            help="Crea una nova llista de preus o si ja existeix una amb el "
                 "mateix nom l'actualitza creant una versió de preus nova.\n\n"
                 "IMPORTANT Cada línia ha d'acabar en \\n (salt de línia o "
                 "intro), LA ULTIMA LÍNIA TAMBÉ.\n\n"
                 "Els camps han d'estar separats per punt i coma.\n"
                 "Cada línia ha de tenir el format següent:\n"
                 "tarifes compatibles separades per coma; nom de la "
                 "llista de preus; preu P1 energia;...;[Preu P1 potencia; ...]\n"
                 "Exemple línia amb preus de potència:\n"
                 "2.0A;1601 20A 0550 ME;0,126162;42.043426\n"
                 "Exemple línia sense preus de potència:\n"
                 "3.1A,3.1A LB;1608 31A 1500 CM;0.123248;0.104112;0.705466"
                                       ),
        'data_inici': fields.date('Data inici'),
        'data_final': fields.date('Data final'),
        'state': fields.selection([('init', 'init'),
                                   ('end', 'end'),
                                   ('error', 'error'),],
                                  'Estat', readonly=True),
        'error': fields.text('Errors'),
        'resum': fields.text('Resum'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'resum': lambda *a: '\n',
    }

GiscegasWizardPricelistFromFile()
