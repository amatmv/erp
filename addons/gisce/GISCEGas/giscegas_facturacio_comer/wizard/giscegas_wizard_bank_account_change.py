# -*- coding: utf-8 -*-
from __future__ import absolute_import
from tools.translate import _
from osv import osv, fields
from l10n_ES_remesas.mandato import MANDATE_SCHEME_SELECTION

from datetime import datetime, date


class GiscegasWizardBankAccountChange(osv.osv_memory):

    def _get_default_pagador(self, cursor, uid, context=None):
        polissa = self.pool.get('giscegas.polissa').browse(
            cursor, uid, context['active_id'], context=context
        )
        return polissa.pagador.id

    def _get_default_pagador_address(self, cursor, uid, context=None):
        polissa = self.pool.get('giscegas.polissa').browse(
            cursor, uid, context['active_id'], context=context
        )
        return polissa.direccio_pagament.id

    def _get_default_payment_mode(self, cursor, uid, context=None):
        polissa = self.pool.get('giscegas.polissa').browse(
            cursor, uid, context['active_id'], context=context
        )
        payment_mode = polissa.payment_mode_id
        return payment_mode.id if payment_mode.require_bank_account else False

    def action_bank_account_change(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids, context=context)[0]
        account_iban = wiz.account_iban.replace(" ", "")

        mandate_new_id = self.pool.get('giscegas.polissa').change_bank_account(
            cursor, uid, [context.get('active_id', False)], account_iban,
            wiz.change_date, wiz.pagador.id, wiz.mandate_scheme,
            wiz.account_owner, wiz.owner_address, wiz.payment_mode,
            context=context
        )
        context.update({
            'print': ['client', 'bank', 'company'],
            'address': wiz.owner_address.id
        })
        if wiz.print_mandate:
            return self.print_mandate(
                cursor, uid, [mandate_new_id], context=context
            )
        else:
            return {}

    def print_mandate(self, cursor, uid, ids, context=None):
        mandate_obj = self.pool.get('payment.mandate')
        return mandate_obj.print_mandate(
            cursor, uid, ids, context=context
        )

    _name = 'giscegas.wizard.bank.account.change'

    _columns = {
        'account_iban': fields.char(
            'Cuenta bancaria', size=126, required=True
        ),
        'account_owner': fields.many2one(
            'res.partner', 'Propietario', required=True
        ),
        'owner_address': fields.many2one(
            'res.partner.address', 'Dirección', required=True
        ),
        'change_date': fields.date('Fecha de cambio', required=True),
        'pagador': fields.many2one('res.partner', 'Razón fiscal', readonly=True),
        'payment_mode': fields.many2one(
            'payment.mode', 'Grupo de pago', required=True
        ),
        'print_mandate': fields.boolean('Imprimir mandato'),
        'mandate_scheme': fields.selection(
            MANDATE_SCHEME_SELECTION, 'Esquema de mandato', required=True,
        )
    }

    _defaults = {
        'pagador': _get_default_pagador,
        'mandate_scheme': lambda *a: 'core',
        'account_owner': _get_default_pagador,
        'owner_address': _get_default_pagador_address,
        'change_date': lambda *a: date.today().strftime('%Y-%m-%d'),
        'payment_mode': _get_default_payment_mode,
        'print_mandate': True
    }


GiscegasWizardBankAccountChange()
