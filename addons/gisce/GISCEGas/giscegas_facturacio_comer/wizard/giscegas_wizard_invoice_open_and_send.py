# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _

import netsvc
import pooler

from giscegas_facturacio.giscegas_facturacio \
    import REFUND_RECTIFICATIVE_INVOICE_TYPES


class GiscegasWizardInvoiceOpenAndSend(osv.osv_memory):

    _name = 'giscegas.wizard.invoice.open.and.send'

    def get_invoices_data(self, cursor, uid, ids, context=None):
        """ Returns invoices digest
            {'invoices': Invoices OK
             'not_draft': Invoices not in draft state
             'count': {('N','A','B', 'R'): num of this "tipo_rectificadora"}
            }
        """
        inv_obj = self.pool.get('giscegas.facturacio.factura')

        inv_fields = ['tipo_rectificadora', 'state', 'polissa_id', 'type']
        inv_data = inv_obj.read(
            cursor, uid, ids, inv_fields
        )

        res = {
            'invoices': [],
            'not_draft': [],
            'count': {},
            'contract_names': [],
            'not_out': [],
        }

        for inv in inv_data:
            tipo_rect = inv['tipo_rectificadora']
            if inv['state'] != 'draft':
                res['not_draft'].append(inv['id'])
                continue
            elif inv['type'] not in ['out_invoice', 'out_refund']:
                res['not_out'].append(inv['id'])
                continue
            else:
                res['invoices'].append(inv['id'])
            contract_name = inv['polissa_id'][1]
            if contract_name not in res['contract_names']:
                res['contract_names'].append(contract_name)
            res['count'].setdefault(tipo_rect, 0)
            res['count'][tipo_rect] += 1

        return res

    def _default_info(self, cursor, uid, context=None):
        if context is None:
            context = {}

        ids = context.get('active_ids', [])

        inv_obj = self.pool.get('giscegas.facturacio.factura')
        inv_fields = inv_obj.fields_get(cursor, uid)
        selection = dict(inv_fields['tipo_rectificadora']['selection'])

        invoice_info = self.get_invoices_data(cursor, uid, ids, context)

        type_tmpl = _(u" * {0}: {1}\n")
        type_str = ""
        for type_inv, count in invoice_info['count'].items():
            type_str += type_tmpl.format(selection[type_inv], count)

        nodraft_str = ""
        if invoice_info['not_draft']:
            nodraft_str = _(
                u"Hi ha {0} factures que no estan en esborrany i per tant ni "
                u"s'obriran ni s'enviaran\n"
            ).format(len(invoice_info['not_draft']))

        noout_str = ""
        if invoice_info['not_out']:
            noout_str = _(
                u"Hi ha {0} factures de DISTRIBUIDORA que ni s'obriran ni "
                u"s'enviaran\n"
            ).format(len(invoice_info['not_out']))

        tmpl = _(
            u"Enviar Factures per email:\n"
            u"S'obriran {0}/{1} factures de les quals:\n"
            u"\n{2}\n"
            u"{3}\n"
            u"{4}\n"
            u"Els contractes afectats són:\n{5}"
        )

        contractes = ', '.join(
            invoice_info['contract_names'][:5] or [_(u"Cap")]
        )
        if len(invoice_info['contract_names']) > 5:
            contractes += '...'
        info = tmpl.format(
            len(invoice_info['invoices']),
            len(ids),
            type_str,
            nodraft_str,
            noout_str,
            contractes,
        )

        return info

    def _default_num_contracts(self, cursor, uid, context=None):
        if context is None:
            context = {}

        ids = context.get('active_ids', [])

        invoice_info = self.get_invoices_data(cursor, uid, ids, context)

        return len(invoice_info['contract_names'])

    def _default_contract_info(self, cursor, uid, context=None):
        if context is None:
            context = {}

        ids = context.get('active_ids', [])

        invoice_info = self.get_invoices_data(cursor, uid, ids, context)

        if len(invoice_info['contract_names']) == 1:
            return "Contracte {0}".format(invoice_info['contract_names'][0])
        else:
            return u"({0} contractes)".format(
                len(invoice_info['contract_names'])
            )

    def envia_mail_a_client(self, cursor, uid, obj_id, model, template=None,
                            context=None):

        mdata_obj = self.pool.get('ir.model.data')
        pwetmpl_obj = self.pool.get('poweremail.templates')
        pwswz_obj = self.pool.get('poweremail.send.wizard')

        try:
            if template:
                #Busquem la plantilla de mail d'activació
                search_vals = ([('model', '=', 'poweremail.templates'),
                                ('name', '=', template)])

                template_id = mdata_obj.read(cursor, uid,
                                             mdata_obj.search(cursor, uid,
                                                              search_vals)[0],
                                             ['res_id'])['res_id']
                template = pwetmpl_obj.browse(cursor, uid, template_id)
                if template.enforce_from_account:
                    mail_from = template.enforce_from_account.id

                ctx = {'active_ids': [obj_id], 'active_id': obj_id,
                       'template_id': template_id, 'src_rec_ids': [obj_id],
                       'src_model': model, 'from': mail_from,
                       'state': 'single', 'priority': '0'}
                params = {'state': 'single', 'priority': '0',
                          'from': ctx['from']}

                pwswz_id = pwswz_obj.create(cursor, uid, params, ctx)
                pwswz_obj.send_mail(cursor, uid, [pwswz_id], ctx)

        except osv.except_osv as e:
                info = u'%s' % unicode(e.value)
                return (_(u'ERROR'), info)

        except Exception as e:
                raise Exception(e)

    def envia_factura_a_client(self, cursor, uid, inv_id, template=None,
                               context=None):
        return self.envia_mail_a_client(
            cursor, uid, inv_id, 'giscegas.facturacio.factura',
            template=template, context=context
        )

    def get_poweremail_template(self, cursor, uid, invoice_type):
        """
        Returns poweremail template depending on tipo_rectificadora
        It may be modified by retailers to match its own needs
        """
        normal_template_name = 'env_fact_via_email'
        refund_template_name = 'env_fact_via_email_abonadora'

        types_dict = {
            'N': normal_template_name,
            'A': refund_template_name,
            'B': refund_template_name,
            'R': normal_template_name,
        }

        return types_dict.get(invoice_type, None)

    def action_obrir_i_enviar(self, cursor, uid, ids, context=None):
        logger = netsvc.Logger()
        if context is None:
            context = {}

        wiz_id = ids[0]

        ids = context.get('active_ids', [])
        inv_obj = self.pool.get('giscegas.facturacio.factura')
        wiz_fields = [
            'send_refund', 'send_rectified', 'send_digest', 'num_contracts'
        ]
        wiz_data = self.read(
            cursor, uid, wiz_id, wiz_fields, context=context
        )[0]

        invoice_info = self.get_invoices_data(cursor, uid, ids, context)
        errors = ""

        db = pooler.get_db_only(cursor.dbname)
        not_open_ids = []

        inv_fields = ['tipo_rectificadora', 'number', 'polissa_id', 'type']
        inv_data = inv_obj.read(
            cursor, uid, invoice_info['invoices'], inv_fields
        )
        logger.notifyChannel(
            'invoice_comer', netsvc.LOG_INFO,
            'Opening and sending_invoices {0}'.format(invoice_info['invoices'])
        )

        # Send digest when only 1 contract selected
        if (wiz_data['send_digest'] and wiz_data['num_contracts'] == 1
                and inv_data):
            # Search for contract (from first invoice)
            contract_id = inv_data[0]['polissa_id'][0]

            template_name = 'env_fact_via_email_refund_digest'
            self.envia_mail_a_client(
                cursor, uid, contract_id, 'giscegas.polissa',
                template=template_name, context=context
            )

        inv_number = False
        for inv in inv_data:
            try:
                opened = False
                tmp_cr = db.cursor()
                inv_id = inv['id']
                inv_type = inv['tipo_rectificadora']
                inv_number = False
                inv_obj.invoice_open(tmp_cr, uid, [inv_id], context)
                tmp_cr.commit()
                opened = True
                tmp_cr = db.cursor()
                inv_number = inv_obj.read(
                    tmp_cr, uid, inv_id, ['number']
                )['number']
                if ((wiz_data['send_refund'] and inv_type in REFUND_RECTIFICATIVE_INVOICE_TYPES)
                    or (wiz_data['send_refund'] and inv_type in ['R', 'N', 'RA']
                        )):
                    template = self.get_poweremail_template(
                        tmp_cr, uid, inv_type
                    )
                    self.envia_factura_a_client(
                        tmp_cr, uid, inv_id, template=template, context=context
                    )
                tmp_cr.commit()
            except Exception as e:
                not_open_ids.append(inv_id)
                opened_txt = opened and "Si" or "No"
                error_txt = '{0}({1}) Oberta {2}: {3} \n'.format(
                    inv_number, inv_id, opened_txt, e
                )
                logger.notifyChannel(
                    "invoice_open&sending", netsvc.LOG_ERROR, error_txt
                )
                errors += error_txt
                tmp_cr.rollback()
            finally:
                if tmp_cr:
                    tmp_cr.close()

        res = {'state': 'done', 'info': errors or 'OK'}
        self.write(cursor, uid, [wiz_id], res, context=context)

    _columns = {
        'state': fields.char('Estat', size=16, required=True),
        'info': fields.text('info', readonly=True),
        'send_refund': fields.boolean('Enviar abonadores'),
        'send_rectified': fields.boolean('Enviar rectificadores'),
        'send_digest': fields.boolean('Enviar mail resum',
                                      help=u"S'enviarà un resum de TOTES "
                                           u"les factures en esborrany del"
                                           u" contracte seleccionat"),
        'num_contracts': fields.integer('Contractes afectats'),
        'contract_info': fields.char('Info Contracte', size="32", readonly=True)
    }

    _defaults = {
        'info': _default_info,
        'state': lambda *a: 'init',
        'send_refund': lambda *a: True,
        'send_rectified': lambda *a: True,
        'send_digest': lambda *a: False,
        'num_contracts': _default_num_contracts,
        'contract_info': _default_contract_info,
    }

GiscegasWizardInvoiceOpenAndSend()