# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction

from datetime import date


class TestUniqueSequenceContract(testing.OOTestCase):

    def setUp(self):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        polissa_gas_obj = self.openerp.pool.get('giscegas.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        # We want to delete all contracts not to collide one each other
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.demo_contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            contracts = polissa_obj.search(
                cursor, uid, [('id', '!=', self.demo_contract_id)]
            )
            polissa_obj.unlink(cursor, uid, contracts)

            self.gas_demo_contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
            contracts_gas = polissa_gas_obj.search(
                cursor, uid, [('id', '!=', self.gas_demo_contract_id)]
            )
            polissa_gas_obj.unlink(cursor, uid, contracts_gas)

    def test_contract_creates_with_unique_sequence(self):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        polissa_gas_obj = self.openerp.pool.get('giscegas.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            polissa_gas_obj.write(cursor, uid, self.gas_demo_contract_id, {
                'name': ''
            })
            polissa_obj.write(cursor, uid, self.demo_contract_id, {
                'name': ''
            })

            polissa_obj.send_signal(
                cursor, uid, [self.demo_contract_id], ['validar', 'contracte']
            )
            fields = ['state', 'name']
            demo_contract = polissa_obj.read(
                cursor, uid, self.demo_contract_id, fields
            )
            self.assertEqual(demo_contract['state'], 'activa')
            self.assertEqual(demo_contract['name'], '00001')

            polissa_gas_obj.send_signal(
                cursor, uid, [self.gas_demo_contract_id],
                ['validar', 'contracte']
            )

            gas_demo_contract = polissa_gas_obj.read(
                cursor, uid, self.gas_demo_contract_id, fields
            )
            self.assertEqual(gas_demo_contract['state'], 'activa')
            self.assertEqual(gas_demo_contract['name'], '00002')

            vals, _ = polissa_obj.copy_data(cursor, uid, self.demo_contract_id)

            vals.update({
                'cups': imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_cups', 'cups_02'
                )[1],
                'data_alta': date.today(),
                'data_firma_contracte': date.today()
            })
            new_contract_id = polissa_obj.create(cursor, uid, vals)

            polissa_obj.send_signal(
                cursor, uid, [new_contract_id], ['validar', 'contracte']
            )
            new_contract = polissa_obj.read(
                cursor, uid, new_contract_id, fields
            )
            self.assertEqual(new_contract['state'], 'activa')
            self.assertEqual(new_contract['name'], '00003')
