# -*- coding: utf-8 -*-

from osv import osv


class GiscegasPolissa(osv.osv):

    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def get_contract_sequence(self, cursor, uid):
        return self.pool.get('ir.sequence').get(cursor, uid, 'giscedata.polissa')


GiscegasPolissa()
