# -*- coding: utf-8 -*-
{
    "name": "Unificador de seqüències dels contractes",
    "description": """
        Mòdul que unifica les seqüències dels contractes de Gas i Electricitat
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends":[
        "giscedata_polissa",
        "giscegas_polissa",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
