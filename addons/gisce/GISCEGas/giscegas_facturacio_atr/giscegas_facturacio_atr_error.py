# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from tools import cache
from datetime import datetime, timedelta
import json

ERROR_LEVEL = [
    ('critical', _(u'Crític')),
    ('warning', _('Warnig')),
    ('information',_(u'Informatiu'))
]

class GiscegasFacturacioAtrErrorTemplate(osv.osv):

    _name = 'giscegas.facturacio.atr.error.template'

    _rec_name = 'code'

    @cache(30)
    def get_error(self, cursor, uid, phase, code, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx['active_test'] = False

        error_data = None
        error_id = self.search(
            cursor, uid, [('code', '=', code), ('phase', '=', phase)],
            limit=1, context=ctx
        )
        if error_id:
            error_data = self.read(
                cursor, uid, error_id[0], ['description', 'level'],
                context=ctx
            )
        return error_data

    def _ff_desactivable(self, cursor, uid, ids, field_name, ar, context=None):
        if context is None:
            context = {}

        res = {}
        for error_vals in self.read(cursor, uid, ids, ['method', 'level']):
            # The validation will only be desactivable if it's not critical and
            # it is linked to a method
            res[error_vals['id']] = bool(
                error_vals['level'] != 'critical' and error_vals['method']
            )
        return res

    def _desactivable_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        if args[0][2]:
            domain = [
                ('level', '!=', 'critical'), ('method', '!=', False),
                ('method', '!=', '')
            ]
        else:
            domain = [
                '|',
                '|',
                ('level', '=', 'critical'),
                ('method', '=', False),
                ('method', '=', '')
            ]
        return domain

    def _ff_parameters(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}

        res = {}
        for import_vals in self.read(cursor, uid, ids, ['parameters']):
            res[import_vals['id']] = json.dumps(
                import_vals['parameters'], indent=4
            )
        return res

    def _fi_parameters(self, cursor, uid, ids, name, value, arg, context=None):
        if not context:
            context = {}

        try:
            parameters = json.loads(value)
            self.write(cursor, uid, ids, {'parameters': parameters})
        except ValueError as e:
            pass

    def check_correct_json(self, cursor, uid, ids, parameters_text):
        try:
            parameters = json.loads(parameters_text)
        except ValueError as e:
            return {
                'warning': {
                    'title': _(u'Atenció'),
                    'message': _('Els parametres entrats no tenen un format '
                                 'correcte de JSON')
                }
            }
        if not isinstance(parameters, dict):
            return {
                'warning': {
                    'title': _(u'Atenció'),
                    'message': _('Els parametres han de ser un diccionari')
                }
            }
        return {}

    _columns = {
        'code': fields.char(
            'Codi', size=4, required=True, readonly=True
        ),
        'phase': fields.char(
            'Fase', size=10, required=True, readonly=True
        ),
        'level': fields.selection(
            ERROR_LEVEL, string='Nivell', required=True, readonly=True
        ),
        'description': fields.char(
            u'Descripció error', size=1024, required=True, readonly=True,
            translate=True
        ),
        'observation': fields.text(u'Observació', readonly=True),
        'method': fields.char(
            u'Mètode a executar per comprovar aquest warning', size=100,
            readonly=True
        ),
        'active': fields.boolean('Actiu', readonly=False),
        'desactivable': fields.function(
            _ff_desactivable, type='boolean', method=True,
            string="Es pot desactivar", fnct_search=_desactivable_search,
        ),
        'parameters': fields.json('Parametres', readonly=False),
        'parameters_text': fields.function(
            _ff_parameters, type='text', method=True, string='Parametres',
            fnct_inv=_fi_parameters, readonly=False
        ),
    }

    _defaults = {
        'active': lambda *a: True,
        'method': lambda *a: '',
        'description': lambda *a: '',
        'level': lambda *a: 'warning',
    }

GiscegasFacturacioAtrErrorTemplate()


class GiscegasFacturacioAtrError(osv.osv):

    def _get_level(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        templ_obj = self.pool.get(
            'giscegas.facturacio.atr.error.template'
        )
        res = {}
        for vals in self.read(cursor, uid, ids, ['error_template_id']):
            res[vals['id']] = templ_obj.read(
                cursor, uid, vals['error_template_id'][0], ['level'], context
            )['level']

        return res

    _name = 'giscegas.facturacio.atr.error'

    _columns = {
        'name': fields.char(
            'Codi error', size=14, required=True, readonly=True, select=True
        ),
        'message': fields.char(
            "Missatge d'error", size=2048, required=True, readonly=True
        ),
        'error_template_id': fields.many2one(
            'giscegas.facturacio.atr.error.template',
            string='Error plantilla', select=1, readonly=True
        ),
        'line_id': fields.many2one(
            'giscegas.facturacio.importacio.linia',
            string=u"Línia d'importació", select=1, readonly=True
        ),
        'active': fields.boolean('Actiu', readonly=True, select=1),
        'level': fields.related(
            'error_template_id', 'level', string=u"Nivell", type='selection',
            selection=ERROR_LEVEL, readonly=True
        ),
    }

    def get_error_text(self, cursor, uid, error_id, context=None):
        """
        Gets Error text from line errors
        :param line_id:
        :return: text with error as
        [code]: message
        """
        vals = self.read(cursor, uid, error_id, ['name', 'message', 'level'])
        txt = '[{0}] {1}'.format(vals['name'], vals['message'])

        if vals['level'] == 'critical':
            txt = u'* {0}'.format(txt)

        return txt

    def create_error_from_code(self, cursor, uid, line_id,
                               phase, code, values, context=None):
        if context is None:
            context = {}

        error_template = self.pool.get(
            'giscegas.facturacio.atr.error.template')

        ctx = context.copy()
        conf_obj = self.pool.get('res.config')
        force_lang = int(conf_obj.get(cursor, 1, 'b70_errors_force_lang', '1'))
        if force_lang:
            new_lang = self.get_lang_of_b70(cursor, uid, line_id, ctx)
            if new_lang:
                ctx['lang'] = new_lang

        error_temp = error_template.get_error(
            cursor, uid, phase, code, context=ctx)
        if error_temp:
            vals = {
                'name': '{0}{1}'.format(phase, code),
                'message': error_temp['description'].format(**values),
                'error_template_id': error_temp['id'],
                'line_id': line_id
            }
            error_id = self.create(cursor, uid, vals)

            return {
                'error_id': error_id,
                'level': error_temp['level'],
                'code': code
            }
        return False

    def get_lang_of_b70(self, cursor, uid, line_id, context=None):
        line_obj = self.pool.get("giscegas.facturacio.importacio.linia")
        part = line_obj.read(cursor, uid, line_id, ['distribuidora_id'])
        if not part['distribuidora_id']:
            imp_con_obj = self.pool.get('giscegas.facturacio.atr.config')
            code = line_obj.read(cursor, uid, line_id, ['ree_source_code'])
            if not code['ree_source_code']:
                return False
            try:
                partner = imp_con_obj.get_emissor(cursor, uid, code['ree_source_code'])
            except:
                pass
            finally:
                if not partner:
                    return False
        else:
            part = part['distribuidora_id']

        pid = self.pool.get("giscemisc.participant").get_partner_id(cursor,
                                                                    uid, part)
        res_partner_obj = self.pool.get("res.partner")
        info = res_partner_obj.read(
            cursor, uid, pid, {'lang'}, context
        )
        return info['lang']

    _defaults = {
        'active': lambda *a: True,
    }

GiscegasFacturacioAtrError()


class GiscegasFacturacioAtrValidator(osv.osv):
    _name = 'giscegas.facturacio.atr.validator'
    _desc = 'Validador de B70'

    def get_checks(self, cursor, uid, phase=None, context=None):
        if context is None:
            context = {}

        template_obj = self. pool.get(
            'giscegas.facturacio.atr.error.template'
        )

        search_parameters = [
            ('active', '=', True), ('method', '!=', False), ('method', '!=', '')
        ]
        if phase:
            search_parameters.append(('phase', '=', phase))

        template_ids = template_obj.search(
            cursor, uid, search_parameters, order='code'
        )

        return template_ids

    def run_check(self, cursor, uid, line_id, b70, phase, error_code,
                  context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        template_obj = self.pool.get(
            'giscegas.facturacio.atr.error.template'
        )

        line = fact_obj.browse(cursor, uid, line_id)

        search_parameters = [
            ('active', '=', True),
            ('method', '!=', False),
            ('phase', '=', phase),
            ('code', '=', error_code)
        ]

        check_ids = template_obj.search(cursor, uid, search_parameters)

        return self.execute_checks(
            cursor, uid, line, b70, check_ids, phase, context
        )

    def validate_b70(self, cursor, uid, line_id, b70, index, phase, context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscegas.facturacio.importacio.linia')

        line = fact_obj.browse(cursor, uid, line_id)

        check_ids = self.get_checks(
            cursor, uid, phase=phase, context=context
        )
        error_ids, has_critical = self.execute_checks(
            cursor, uid, line, b70, index, check_ids, phase, context
        )

        return error_ids, has_critical

    def execute_checks(self, cursor, uid, line, b70, index, template_ids, phase,
                       context=None):
        if context is None:
            context = {}

        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        template_obj = self.pool.get(
            'giscegas.facturacio.atr.error.template'
        )

        has_critical = False
        error_ids = []

        template_values = template_obj.read(
            cursor, uid, template_ids, ['code', 'method', 'level', 'parameters']
        )
        for template_vals in template_values:
            check_method = getattr(self, template_vals['method'], None)
            if check_method:
                try:
                    vals_list = check_method(
                        cursor, uid, line, b70, index, template_vals['parameters']
                    )
                    if vals_list:
                        for vals in vals_list:
                            error_ids.append(
                                error_obj.create_error_from_code(
                                    cursor, uid, line.id, phase,
                                    template_vals['code'], vals, context=context
                                )
                            )

                        if template_vals['level'] == 'critical':
                            has_critical = True
                except Exception as e:
                    error_ids.append(
                        error_obj.create_error_from_code(
                            cursor, uid, line.id, phase, '999',
                            {
                                'text': '[{0}] {1}'.format(
                                    template_vals['code'], e
                                )
                            }
                        )
                    )
                    has_critical = True
            else:
                # If we didn't find the method to be executed we add the error
                # that indicates that, which is 998

                # We say what are the check number and method that didn't work
                vals = {
                    'check_number': template_vals['code'],
                    'method': template_vals['method']
                }
                error_ids.append(
                    error_obj.create_error_from_code(
                        cursor, uid, line.id, phase, '998', vals,
                        context=context
                    )
                )

        return error_ids, has_critical

    def check_divergence(self, cursor, uid, line, b70, index, parameters=None):
        lect_help_obj = self.pool.get('giscegas.lectures.atr.helper')
        tariff_obj = self.pool.get('giscegas.polissa.tarifa')
        res_config = self.pool.get('res.config')

        overwrite_atr = bool(int(
            res_config.get(
                cursor, uid, 'lect_atr_gas_overwrite_atr_measure_when_found', '0'
            )
        ))
    
        errors = []
        atr_inv = b70.facturas[index]
        if atr_inv.is_only_conceptes() or b70.codproceso == '33':
            return errors

        data_inici, data_final = atr_inv.get_periode_factura()
        data_inici = datetime.strptime(data_inici, '%Y-%m-%d')
        data_inici += timedelta(days=1)
        data_inici = data_inici.strftime('%Y-%m-%d')

        polissa_id = lect_help_obj.find_polissa_with_cups_id(
            cursor, uid, line.cups_id.id, data_inici, data_final
        )
        if atr_inv.clasefact in ['N', 'G', 'A']:
            tariff_id = tariff_obj.get_tarifa_from_ocsum(
                cursor, uid, atr_inv.tipopeaje
            )

            for compt_xml in atr_inv.get_comptadors():
                errors += self.calculate_divergences_meter(
                    cursor, uid, polissa_id, tariff_id, compt_xml,
                    overwrite_atr
                )
    
        return errors

    def calculate_divergences_meter(self, cursor, uid, polissa_id, tariff_id, compt_xml, overwrite_atr):
        compt_obj = self.pool.get('giscegas.lectures.comptador')

        errors = []
        compt_id = compt_obj.search_with_contract(
            cursor, uid, compt_xml.numseriemedidor, polissa_id
        )
        if not compt_id:
            return errors
        compt_id = compt_id[0]
        # Check lect.
        errors.extend(
            self.process_lect_check(
                cursor, uid, compt_xml.get_lectures_info(), tariff_id, compt_id, compt_xml,
                overwrite_atr
            )
        )
        return errors

    def process_lect_check(self, cursor, uid, lect_datas, tariff_id, compt_id,
                           compt_xml, overwrite_atr):
        errors = []
        for lect_data in lect_datas:
            readings_to_check = [
                {'fecha': lect_data['fecha_desde'], 'lectura': lect_data['lectura_desde_m3'], 'periode': "P"+lect_data['periode'][-1]},
                {'fecha': lect_data['fecha_actual'], 'lectura': lect_data['lectura_actual_m3'], 'periode': "P"+lect_data['periode'][-1]}
            ]
            for reading in readings_to_check:
                error = self.calculate_divergences_reading(
                    cursor, uid, tariff_id, compt_id, reading['periode'], compt_xml.numseriemedidor,
                    reading, overwrite_atr
                )
                if error:
                    errors.append(error)
        return errors

    def calculate_divergences_reading(self, cursor, uid, tariff_id, compt_id,
                                      periode, numero_serie, reading_data,
                                      overwrite_atr):
        lect_helper = self.pool.get('giscegas.lectures.atr.helper')
        origen_comer_obj = self.pool.get('giscegas.lectures.origen_comer')

        reading = lect_helper.get_reading_model_id(
            cursor, uid, compt_id, reading_data['fecha'], periode, tariff_id
        )
        if reading:
            lect_obj = self.pool.get(reading['model'])

            read_fields = ['lectura', 'origen_comer_id']

            lect_vals = lect_obj.read(
                cursor, uid, reading['id'], read_fields
            )

            lectura_bd = lect_vals['lectura']
            lectura_xml = reading_data['lectura']
            if lectura_bd != lectura_xml:
                origen_comer_code = origen_comer_obj.read(
                    cursor, uid, lect_vals['origen_comer_id'][0], ['codi']
                )['codi']
                bd_has_at_ori = origen_comer_code == 'AT'
                if not overwrite_atr or not bd_has_at_ori:
                    reading_date = reading_data['fecha']
                    return {
                        'meter_name': numero_serie,
                        'period': periode,
                        'reading_date': reading_date,
                        'reading_value': lectura_bd,
                        'b70_value': lectura_xml,
                    }
        return None

    def check_consum_correcte(self, cursor, uid, line, b70, index, parameters=None):
        errors = []
        atr_inv = b70.facturas[index]
        if atr_inv.is_only_conceptes() or b70.codproceso == '33':
            return errors

        for compt_xml in atr_inv.get_comptadors():
            for lectinfo in compt_xml.get_lectures_info():
                if lectinfo['consum_m3'] != (lectinfo['lectura_actual_m3'] - lectinfo['lectura_desde_m3'] + lectinfo['ajust']):
                    errors += [{
                        'meter_name': compt_xml.numseriemedidor,
                        'period': lectinfo['periode'],
                        'reading_value_act': lectinfo['lectura_actual_m3'],
                        'reading_value_ant': lectinfo['lectura_desde_m3'],
                        'reading_ajust': lectinfo['ajust'],
                        'consum_b70': lectinfo['consum_m3'],
                    }]
        return errors

GiscegasFacturacioAtrValidator()


class GiscegasFacturacioImportacioLinia(osv.osv):
    """Agrupació d'importacions"""

    _name = 'giscegas.facturacio.importacio.linia'
    _inherit = 'giscegas.facturacio.importacio.linia'

    _columns = {
        'error_ids': fields.one2many(
            'giscegas.facturacio.atr.error', 'line_id',
            string='Errors importacio'
        )
    }

    def unlink_errors(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        for line_error in self.read(cursor, uid, ids, ['error_ids']):
            error_obj.unlink(cursor, uid, line_error['error_ids'])

    def clean_linia_and_commit(self, cursor, uid, lid, context=None):
        """Eliminar lectures, factures, línies extra i comitar"""
        if context is None:
            context = {}
        self.unlink_errors(cursor, uid, lid, context=context)
        super(GiscegasFacturacioImportacioLinia, self).clean_linia_and_commit(
            cursor, uid, lid, context=context)

    def unlink(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        self.unlink_errors(
            cursor, uid, ids, context=context
        )
        return super(GiscegasFacturacioImportacioLinia, self).unlink(
            cursor, uid, ids, context=context)


GiscegasFacturacioImportacioLinia()
