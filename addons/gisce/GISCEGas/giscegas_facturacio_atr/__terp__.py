# -*- coding: utf-8 -*-
{
    "name": "Facturació atr_gas",
    "description": """Facturació atr_gas.
        Per importar factures cal configurar correctament la tarifa
        de compra de les distribuidores a TARIFAS ELECTRICIDAD""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscegas_facturacio_comer",
        "giscegas_lectures_atr",
        # "giscegas_remeses",
        "attachments_tab",
        "giscegas_atr",
        "giscegas_facturacio_refund_signed",
    ],
    "init_xml": [],
    "demo_xml":[
        "giscegas_facturacio_atr_demo.xml"
    ],
    "update_xml":[
        "wizard/wizard_canvi_fase.xml",
        "wizard/giscegas_facturacio_atr_wizard.xml",
        "giscegas_facturacio_atr_view.xml",
        "giscegas_facturacio_view.xml",
        "giscegas_facturacio_atr_error_view.xml",
        "giscegas_facturacio_data.xml",
        "giscegas_facturacio_atr_data.xml",
        "giscegas_facturacio_atr_error_data.xml",
        "giscegas_cups_ps_view.xml",
        "wizard/wizard_importacio_B70_wizard.xml",
        "wizard/wizard_reimportacio_B70.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
