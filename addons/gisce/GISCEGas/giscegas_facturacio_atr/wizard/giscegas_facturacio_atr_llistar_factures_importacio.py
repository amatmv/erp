# -*- coding: utf-8 -*-

from osv import osv


class GiscegasFacturacioAtrLlistarFactures(osv.osv_memory):

    _name = 'giscegas.facturacio.atr.llistar.factures.importacio'

    def action_get_factures(self, cursor, uid, ids, context=None):
        active_id = context['active_id']
        linia_obj = self.pool.get('giscegas.facturacio.importacio.linia')

        lids = linia_obj.search(cursor, uid, [('importacio_id', '=', active_id)])
        inv_ids = []
        for linia_imp in linia_obj.browse(cursor, uid, lids):
            if not linia_imp.liniafactura_id and linia_imp.linia_account_invoice_id:
                factures = [lf.factura_id.id for lf in linia_imp.linia_account_invoice_id]
                inv_ids.extend(factures)
            else:
                factures = [lf.factura_id.invoice_id.id for lf in linia_imp.liniafactura_id]
                inv_ids.extend(factures)

        return {
            'name': 'Listado de facturas',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': "account.invoice",
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % str(tuple(inv_ids)),
        }

GiscegasFacturacioAtrLlistarFactures()
