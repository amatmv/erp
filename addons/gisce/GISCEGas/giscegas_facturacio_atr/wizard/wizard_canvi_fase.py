# -*- coding: utf-8 -*-
from __future__ import absolute_import
import pooler
from tools.translate import _
from tools.misc import ustr
from tools import config
from osv import osv, fields, orm
from giscegas_facturacio_atr.giscegas_facturacio_atr import _IMPORT_PHASES


class WizardCanviFase(osv.osv_memory):

    _name = 'giscegas.wizard.canvi.fase'

    _columns = {
        'info': fields.text('Informació'),
        'import_phase': fields.selection(
            _IMPORT_PHASES, string="Fase de càrrega", required=True
        ),
        'state': fields.char('Estat', size=16),
    }

    def action_canvia_fase(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        line_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        line_ids = context.get('active_ids', [])
        if wizard.import_phase == 50:
            line_obj.set_phase_manual(cursor, uid, line_ids, context=context)
        else:
            line_obj.set_phase(cursor, uid, line_ids, wizard.import_phase, context=context)
        wizard.write({'state': 'done'})
        return True

    def _get_init_info(self, cursor, uid, context=None):
        if context is None:
            context = {}
        n = len(context.get('active_ids', []))
        return _(u"S'han seleccionat {} casos B70").format(n)

    _defaults = {
        'info': _get_init_info,
        'import_phase': lambda *a: 50,
        'state': lambda *a: 'init'
    }

WizardCanviFase()
