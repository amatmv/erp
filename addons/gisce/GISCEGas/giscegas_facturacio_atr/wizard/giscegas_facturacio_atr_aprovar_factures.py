# -*- coding: utf-8 -*-

from tools import config
from tools.translate import _

import pooler
from osv import osv, fields
import netsvc


class GiscegasFacturacioAtrAprovarFactures(osv.osv_memory):
    """Wizard per mostrar les factures des del form de linia"""
    _name = 'giscegas.facturacio.atr.aprovar.factura'

    def action_aprovar_factures(self, cursor, uid, ids, context=None):
        """Aprovem totes les factures que haguem filtrat (ids).
        """
        if context is None:
            context = {}
        db = pooler.get_db_only(cursor.dbname)
        wizard = self.browse(cursor, uid, ids[0])
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        linia_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        lf_obj = self.pool.get('giscegas.facturacio.importacio.linia.factura')
        imp_obj = self.pool.get('giscegas.facturacio.importacio')
        wf_service = netsvc.LocalService("workflow")
        invoice_obj = self.pool.get("account.invoice")
        invoices = []

        active_ids = context.get('active_ids', [])
        if context.get('model') == 'giscegas.facturacio.importacio':
            factures = []
            for linia in imp_obj.browse(cursor, uid, active_ids[0], context).linia_ids:
                for lf in linia.liniafactura_id:
                    if lf.factura_id.tipo_factura == "11":
                        inv_id = invoice_obj.search(cursor, uid, [
                                ('origin', '=', linia.invoice_number_text),
                                ('partner_id', '=', linia.distribuidora_id.partner_id.id),
                                ('type', 'in', ['in_invoice', 'in_refund'])
                        ])
                        if len(inv_id) == 1 and inv_id[0] not in [inv.id for inv in invoices]:
                            invoices.append(invoice_obj.browse(cursor, uid, inv_id[0]))
                    factures.append(lf.factura_id)
                for lf in linia.linia_account_invoice_id:
                    invoices.append(lf.factura_id)
        else:
            factures = factura_obj.browse(cursor, uid, active_ids)
        factures = list(set(factures))
        invoices = list(set(invoices))
        aprovades = []
        no_aprovades = []
        for factura in factures+invoices:
            if factura in invoices:
                is_acc_invoice = True
                fact_obj = invoice_obj
            else:
                is_acc_invoice = False
                fact_obj = factura_obj

            if not is_acc_invoice and factura.tipo_rectificadora == 'BRA':
                continue
            # comprovar que la factura no és oberta
            if factura.state == 'open':
                no_aprovades.append(factura.id)
                continue
            if not factura.origin:
                no_aprovades.append(factura.id)
                continue
            try:
                cr = db.cursor()
                if factura.type in ('in_invoice', 'in_refund'):
                    check_total = factura.check_total
                    amount_total = factura.amount_total
                    if (wizard.open_1ct_diff_invoices and
                            check_total != amount_total and
                            abs(check_total - amount_total) <= 0.015):
                        # Accepts 1 ct diff invoices
                        fact_obj.write(
                            cr, uid, [factura.id],
                            {'check_total': factura.amount_total}
                        )
                    elif (wizard.open_negative_invoices and
                            check_total != amount_total and
                            abs(check_total) == abs(amount_total)):
                        # Accepts negative invoices
                        fact_obj.write(
                            cr, uid, [factura.id],
                            {'check_total': factura.amount_total}
                        )
                if not is_acc_invoice:
                    fact_obj.invoice_open(cr, uid, [factura.id])
                else:
                    wf_service.trg_validate(
                        uid, 'account.invoice', factura.id, 'invoice_open', cursor
                    )
                cr.commit()
                aprovades.append(factura.id)
            except Exception as e:
                cr.rollback()
                no_aprovades.append(factura.id)
                continue
            finally:
                cr.close()

            # setejar la línia de factura com a vàlida
            l_ids = []
            if is_acc_invoice:
                other_ids = self.pool.get("giscegas.facturacio.factura").get_pseudofactures_for_invoice(cursor, uid, factura.id, context=context)
                for fact_id in other_ids:
                    lf_id = lf_obj.search(cursor, uid, [('factura_id', '=', fact_id)])
                    if lf_id:
                        l_id = lf_obj.read(cursor, uid, lf_id[0], ['linia_id'])['linia_id'][0]
                        l_ids.append(l_id)
            else:
                lf_id = lf_obj.search(cursor, uid, [('factura_id', '=', factura.id)])
                if lf_id:
                    l_id = lf_obj.read(cursor, uid, lf_id[0], ['linia_id'])['linia_id'][0]
                    l_ids.append(l_id)

            for l_id in l_ids:
                l_info = linia_obj.read(cursor, uid, l_id, ['info'])
                info = l_info['info'] + _(u' - Aprovat!')
                l_vals = {'state': 'valid', 'info': info}
                linia_obj.write(cursor, uid, l_id, l_vals)

        wizard.write({'num_factures_aprovades': len(aprovades),
                      'num_factures_no_aprovades': len(no_aprovades),
                      'fact_aprovades':
                                ', '.join(['%s' % x for x in aprovades]),
                      'fact_no_aprovades':
                                ', '.join(['%s' % x for x in no_aprovades]),
                      'state': 'end'})

    def action_get_factures(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0])
        fact = wizard.fact_no_aprovades
        return {
            'name': _('Factures no aprovades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.facturacio.factura',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % \
                            str(tuple([int(x) for x in fact.split(', ') if x])),
        }

    def action_cancel(self, cursor, uid, ids, context=None):
        """Cancel·lem.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    _columns = {
        'num_factures_aprovades': fields.integer('Factures aprovades',
                                                 readonly=True),
        'num_factures_no_aprovades': fields.integer('Factures no aprovades',
                                                 readonly=True),
        'fact_aprovades': fields.text('Factures aprovades'),
        'fact_no_aprovades': fields.text('Factures no aprovades'),
        'open_1ct_diff_invoices': fields.boolean(
            u"Aprova amb error d'1 cèntim",
            help=u"Aprova la factura encara que la diferència entre el total "
                 u"i el residual sigui d'un cèntim"
        ),
        'open_negative_invoices': fields.boolean(
            u"Aprova factura negativa",
            help=u"Aprova la factura encara que sigui negativa"
        ),
        'state': fields.char('State', size=4),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'open_1ct_diff_invoices': lambda *a: False,
        'open_negative_invoices': lambda *a: False,
    }


GiscegasFacturacioAtrAprovarFactures()
