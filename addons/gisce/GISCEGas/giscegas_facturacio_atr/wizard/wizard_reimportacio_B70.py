# -*- coding: utf-8 -*-

from osv import fields, osv, orm
from tools.translate import _
import logging


class WizardRemportacioB70(osv.osv_memory):
    """ Wizard per reimportar els fitxers que no s'han pogut importar. """
    _name = 'wizard.reimportacio.b70'

    _columns = {
        'info': fields.text('Info'),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')], 'State')
    }

    _defaults = {
        'state': lambda *a: 'init'
    }

    def action_reimport_b70_lines(self, cursor, uid, ids, context):
        """ Re-importa tots els fitxers donat un lot de facturació """
        if not context:
            context = {'active_ids': []}

        import_obj = self.pool.get('giscegas.facturacio.importacio')
        line_obj = self.pool.get('giscegas.facturacio.importacio.linia')

        wrong_imports_repo = {}
        for active_id in context['active_ids']:
            import_obj.process_import(
                cursor, uid, active_id, context={'import_errors': True}
            )
            wrong_imports_repo[active_id] = line_obj.search_count(
                cursor, uid, [
                    ('importacio_id', '=', active_id),
                    '|',
                    ('state', '=', False),
                    ('state', '=', 'erroni')
                ]
            )

        text = _(u"Fitxers que han quedat pendents de importar degut a "
                 u"errors:\n\n")

        text += '\n'.join(
            [
                '{0}: {1} fitxers'.format(
                    import_obj.read(cursor, uid, imp_id, ['name'])['name'],
                    wrong_imports_repo[imp_id]
                )
                for imp_id, num_linies in wrong_imports_repo.items()
            ]
        )

        self.write(cursor, uid, ids, {'info': text, 'state': 'end'})
        return True


WizardRemportacioB70()
