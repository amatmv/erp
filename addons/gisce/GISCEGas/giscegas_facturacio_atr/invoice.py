# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, orm, fields
import netsvc
import pooler


class AccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def get_in_invoice_by_origin(
            self, cursor, uid, origin, partner_id, year=None, context=None
    ):
        """
        Returns provider invoice id of partner with selected origin
        :param origin:
        :param partner_id:
        :param context:
        :param year:
        :type year: str
        :return: return invoice ids or empty list
        """
        search_dict = [('origin', '=', origin),
                       ('partner_id', '=', partner_id),
                       ('type', 'in', ['in_invoice', 'in_refund']),
                       ]

        if year:
            ini = '{0}-01-01'.format(year)
            end = '{0}-12-31'.format(year)
            search_dict.append(('origin_date_invoice', '>=', ini))
            search_dict.append(('origin_date_invoice', '<=', end))

        invoice_ids = self.search(cursor, uid, search_dict, context=context)
        return invoice_ids

    def set_state(self, cursor, uid, ids, state, context=None):
        if context is None:
            context = {}
        if state == 'open':
            invoice_vals = self.read(
                cursor, uid, ids, ['type', 'origin'], context
            )
            fact_obj = self.pool.get("giscegas.facturacio.factura")
            for inv_vals in invoice_vals:
                # Don't open proforma invoices from supplier. They will be in a
                # proforma group
                in_type = 'in' in inv_vals['type']
                fact_id = fact_obj.search(cursor, uid, [('invoice_id', '=', inv_vals['id']), ('tipo_factura', '=', '11')])
                state_pro = len(fact_id) != 0

                if in_type and state_pro:
                    self.clean_workflow(cursor, uid, inv_vals['id'], context=context)
                    raise osv.except_osv(
                        "Error Usuario",  "La factura con origen {0} no puede abrirse. "
                                          "Las pseudofacturas de proveedor de gas no deben abrirse, se passaran a proforma cuando se haya abierto la factura real.".format(inv_vals['origin'])
                    )
                elif in_type:
                    # The proforma group invoices can be only opened if all proformas are imported
                    self.check_open_factura_atr_agregada(cursor, uid, inv_vals['id'], context=context)

        res = super(AccountInvoice, self).set_state(cursor, uid, ids, state, context=context)
        for invoice in self.read(cursor, uid, ids, ['payment_order_id', 'state', 'origin', 'partner_id'], context=context):
            order_id = invoice.get('payment_order_id', False)
            invoice_state = invoice.get('state', False)
            if order_id and invoice_state in ['open']:
                invoice_id = invoice['id']
                self.afegeix_a_remesa(
                    cursor, uid, [invoice_id], order_id[0], context=context
                )
            self.set_proforma_related_pseudofacturas_from_invoice(cursor, uid, invoice['id'], context=context)
        return res

    def check_open_factura_atr_agregada(self, cursor, uid, invoice_id, context=None):
        if context is None:
            context = {}
        if not invoice_id:
            return
        info = self.read(cursor, uid, invoice_id, ['state', 'type'])
        if info['state'] != 'draft':
            return True
        if info['type'] not in ('in_invoice', 'in_refund'):
            return True

        fact_obj = self.pool.get("giscegas.facturacio.factura")
        other_ids = fact_obj.get_pseudofactures_for_invoice(cursor, uid, invoice_id, context=context)
        if not len(other_ids):
            return True
        total_proformas = 0.0
        proforma_origins = []
        for info in fact_obj.read(cursor, uid, other_ids, ['signed_amount_total', 'origin']):
            total_proformas += info['signed_amount_total']
            proforma_origins.append(info['origin'])
        total_fiscal_invoice = self.read(cursor, uid, invoice_id, ['amount_total'])['amount_total']
        # Un marge d'error de 1 centim per pseudofactura per arrodoniments
        marge_error = len(other_ids) / 100.0
        dins_marge_error = abs(round(total_fiscal_invoice, 2) - round(total_proformas, 2)) <= marge_error
        if round(total_fiscal_invoice, 2) != round(total_proformas, 2) and not dins_marge_error:
            origin = self.read(cursor, uid, invoice_id, ['origin'])['origin']
            self.clean_workflow(cursor, uid, invoice_id, context=context)
            raise osv.except_osv(
                "Atención",
                "No se puede abrir la factura con origen {0} porque el total "
                "de sus pseudofacturas ({1}€) no es igual al total de la "
                "factura ({2}€). Pseudofacturas comprovadas: {3}".format(
                    origin, total_proformas, total_fiscal_invoice, proforma_origins
                )
            )

    def clean_workflow(self, cursor, uid, ids, context=None):
        """
        Donada una account.invoice en esborrany, s'encarrega de deixar el
        workflow "net i corregit" perqué en un futur no hi hagi problemes en
        tornar-la a obrir.
        """
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for info in self.read(cursor, uid, ids, ['state']):
            if info['state'] != 'draft':
                continue
            db = pooler.get_db(cursor.dbname)
            tmp_cursor = db.cursor()
            tmp_cursor.execute("""
            select
                witem.id
            from
                wkf_instance ins
                JOIN wkf_workitem witem ON witem.inst_id=ins.id
                JOIN wkf_activity act ON act.id=witem.act_id
                JOIN account_invoice pol ON pol.id = ins.res_id
                JOIN wkf wk ON wk.id=act.wkf_id
            where
                ins.res_type = 'account.invoice'
                and act.id in (
                    SELECT act.id
                    FROM wkf_activity act
                    JOIN wkf wk ON wk.id=act.wkf_id
                    WHERE wk.osv='account.invoice'
                    AND act.name = 'open'
                )
                and pol.state = 'draft'
                and pol.id = %s
            """, (info['id'], ))
            res = tmp_cursor.fetchall()
            if len(res) != 1:
                return True
            tmp_cursor.execute("""
            UPDATE wkf_workitem
            SET
                act_id = (
                    SELECT act.id
                    FROM wkf_activity act
                    JOIN wkf wk ON wk.id=act.wkf_id
                    WHERE wk.osv='account.invoice'
                    AND act.name = 'draft'
                ),
                state = 'active'
            WHERE id in (
                select
                    witem.id
                from
                    wkf_instance ins
                    JOIN wkf_workitem witem ON witem.inst_id=ins.id
                    JOIN wkf_activity act ON act.id=witem.act_id
                    JOIN account_invoice pol ON pol.id = ins.res_id
                    JOIN wkf wk ON wk.id=act.wkf_id
                where
                    ins.res_type = 'account.invoice'
                    and act.id in (
                        SELECT act.id
                        FROM wkf_activity act
                        JOIN wkf wk ON wk.id=act.wkf_id
                        WHERE wk.osv='account.invoice'
                        AND act.name = 'open'
                    )
                    and pol.state = 'draft'
                    and pol.id = %s
            )
            """, (info['id'], ))
            tmp_cursor.commit()
        return True

    def set_proforma_related_pseudofacturas_from_invoice(self, cursor, uid, invoice_id, context=None):
        if context is None:
            context = {}
        if not invoice_id:
            return
        if self.read(cursor, uid, invoice_id, ['state'])['state'] != 'open':
            return
        fact_obj = self.pool.get("giscegas.facturacio.factura")
        other_ids = fact_obj.get_pseudofactures_for_invoice(cursor, uid, invoice_id, context=context)
        if not other_ids:
            return
        fact_obj.set_proforma_pseudofacturas_from_invoice(cursor, uid, invoice_id, other_ids, context=context)


AccountInvoice()
