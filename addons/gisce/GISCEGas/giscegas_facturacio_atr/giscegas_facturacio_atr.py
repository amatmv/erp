# -*- coding: utf-8 -*-
import base64
import hashlib
import zipfile
import logging
import re
from datetime import datetime

import pooler
from osv import osv, fields
from tools.translate import _
from oorq.decorators import job
import netsvc
from giscegas_facturacio_atr_utils import (
    get_remeses_from_zip, get_remesa_from_xml, get_cups_from_xml,
    get_header_from_xml, get_number_of_invoices_from_xml
)
from osv.expression import OOQuery
from sql.aggregate import Count
from gestionatr.defs_gas import TAULA_CLASE_FACTURA_O_TIPO_DE_FACTURA_RECTIFICADORA

from gestionatr.input.messages import B70, message_gas as message

_IMPORT_PHASES = [
    (10, _(u'1 - Càrrega XML')),
    (20, _(u'2 - Creació Factura')),
    (30, _(u'3 - Validació dades')),
    (40, _(u'4 - Càrrega de dades')),
    (50, _(u'5 - Gestionat manualment')),
]


def get_md5(data):
    cleaned_data = data.replace(" ", "")
    cleaned_data = cleaned_data.replace("\n", "")
    cleaned_data = cleaned_data.replace("\t", "")
    md5 = hashlib.md5(cleaned_data)
    return md5.hexdigest()


class GiscegasFacturacioImportacio(osv.osv):
    """Agrupació d'importacions"""

    _name = 'giscegas.facturacio.importacio'
    _description = "Agrupació d'importacions"

    def unlink(self, cursor, uid, ids, context=None):
        """S'esborren les importacions que no contenen factures
           obertes. En el cas de tenir factures obertes, s'esborren
           únicament les línies de d'importació sense factures obertes
           i aquestes factures"""
        lin = self.pool.get('giscegas.facturacio.importacio.linia')
        for iid in ids:
            ivals = self.read(cursor, uid, iid, ['linia_ids'])
            if lin.unlink(cursor, uid, ivals['linia_ids']):
                super(GiscegasFacturacioImportacio, self).unlink(cursor, uid,
                                                        iid, context=context)

    def _ff_check_status(self, cursor, uid, ids, field_name, arg,
                         context=None):
        if context is None:
            context = {}
        res = {}
        lin_pool = self.pool.get('giscegas.facturacio.importacio.linia')
        linfact_pool = self.pool.get(
            'giscegas.facturacio.importacio.linia.factura')
        linaccinv_pool = self.pool.get(
            'giscegas.facturacio.importacio.linia.account.invoice')
        fact_obj = self.pool.get(
            'giscegas.facturacio.factura'
        )
        inv_obj = self.pool.get(
            'account.invoice'
        )
        for i_id in ids:
            # Ids of the lines in the import
            linies = lin_pool.search(
                cursor, uid, [('importacio_id', '=', i_id)], context=context)
            # Total number of lines in the import
            n_linies = len(linies)
            # Total number of already processed lines
            n_linies_proc = lin_pool.search_count(
                cursor, uid, [
                    ('importacio_id', '=', i_id), ('state', '!=', False)
                ]
            )
            # Total number of correct lines
            n_linies_ok = lin_pool.search_count(
                cursor, uid, [
                    ('importacio_id', '=', i_id), ('state', '=', 'valid')
                ]
            )

            linfact_ids = linfact_pool.search(
                cursor, uid, [('linia_id', 'in', linies)]
            )
            linaccinv_ids = linaccinv_pool.search(
                cursor, uid, [('linia_id', 'in', linies)]
            )

            # Total number of created invoices
            fact_count = len(linfact_ids+linaccinv_ids)

            linfact_data = linfact_pool.read(
                cursor, uid, linfact_ids, ['factura_id']
            )
            linaccinv_data = linaccinv_pool.read(
                cursor, uid, linaccinv_ids, ['factura_id']
            )

            fact_ids = [x['factura_id'][0] for x in linfact_data]
            inv_ids = [x['factura_id'][0] for x in linaccinv_data]

            fact_data = fact_obj.read(
                cursor, uid, fact_ids, ['tipo_rectificadora']
            )
            inv_data = inv_obj.read(
                cursor, uid, inv_ids, ['type']
            )

            resum = {}
            for fact in fact_data:
                tipus_rect = fact['tipo_rectificadora']
                if not resum.get(tipus_rect, False):
                    resum[fact['tipo_rectificadora']] = 0
                resum[fact['tipo_rectificadora']] += 1
            for fact in inv_data:
                tipus_rect = fact['type']
                if not resum.get(tipus_rect, False):
                    resum[fact['type']] = 0
                resum[fact['type']] += 1

            # Create resum text
            digest_text = ''
            types_dict = dict(TAULA_CLASE_FACTURA_O_TIPO_DE_FACTURA_RECTIFICADORA)
            for key, value in resum.items():
                title = types_dict.get(key[0], key)
                digest_text += '{0}: {1}\n'.format(title, value)

            complet = (n_linies_ok == n_linies)

            if n_linies:
                progress = float(n_linies_proc)/n_linies * 100
                progress_importation = float(n_linies_ok)/n_linies * 100
            else:
                # If we don't have any line to import we have imported all
                progress = 100
                progress_importation = 100
            res[i_id] = {
                'importat_ok': complet,
                'num_xml': n_linies,
                'num_fact_creades': fact_count,
                'info': u"{0} XML importats, {1} factures creades".format(
                    n_linies_proc, fact_count
                ),
                'digest': digest_text,
                'progres': progress,
                'progres_importation': progress_importation,
            }
        return res

    def _ff_opened(self, cursor, uid, ids, field_name, arg, context=None):
        self_obj = self.pool.get('giscegas.facturacio.importacio')
        self_querier = OOQuery(self_obj, cursor, uid)
        # linies que estan ja han passat l'estat 10
        linia_20_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        linia_20_querier = OOQuery(linia_20_obj, cursor, uid)

        # linies que estan ja han passat l'estat 20, factures creades
        linia_30_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.factura'
        )
        linia_30_querier = OOQuery(linia_30_obj, cursor, uid)
        accinv_linia_30_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.account.invoice'
        )
        accinv_linia_30_querier = OOQuery(accinv_linia_30_obj, cursor, uid)

        res = {}

        for importacio_id in ids:
            # Linies importades → total XML de la carrega
            search_params = [
                ('importacio_id', '=', importacio_id)
            ]
            linia_20_query = linia_20_querier.select(
                ['id', 'state']
            ).where(
                search_params
            )
            cursor.execute(*linia_20_query)
            linies_20_vals = cursor.dictfetchall()

            # Valid lines
            linies_20_ids_valides = []
            # XML no processat
            n_unprocessed_lines = 0
            # XML no generat correctament
            n_invalid_lines = 0
            for linia in linies_20_vals:
                if linia['state']:
                    if linia['state'] == 'valid':
                        linies_20_ids_valides.append(linia['id'])
                    else:
                        n_invalid_lines += 1
                else:
                    n_unprocessed_lines += 1

            n_linies_30 = 0
            not_opened_n_linies_30 = 0
            acc_inv_n_linies_30 = 0
            not_opened_acc_inv_n_linies_30 = 0
            if linies_20_ids_valides:
                search_params = [
                    ('linia_id', 'in', linies_20_ids_valides),
                    ('factura_id.invoice_id.state', 'in', ['open', 'paid', 'proforma', 'proforma2'])
                ]
                linia_30_query = linia_30_querier.select(
                    [Count('id')],
                    order_by=None
                ).where(
                    search_params
                )
                cursor.execute(*linia_30_query)
                n_linies_30 = cursor.fetchall()[0][0]
                search_params = [
                    ('linia_id', 'in', linies_20_ids_valides),
                    ('factura_id.invoice_id.state', 'not in', ['open', 'paid', 'proforma', 'proforma2'])
                ]
                linia_30_query = linia_30_querier.select(
                    [Count('id')],
                    order_by=None
                ).where(
                    search_params
                )
                cursor.execute(*linia_30_query)
                not_opened_n_linies_30 = cursor.fetchall()[0][0]

                # Ara contem les account.invoice creades
                search_params = [
                    ('linia_id', 'in', linies_20_ids_valides),
                    ('factura_id.state', 'in', ['open', 'paid'])
                ]
                linia_30_query = accinv_linia_30_querier.select(
                    [Count('id')],
                    order_by=None
                ).where(
                    search_params
                )
                cursor.execute(*linia_30_query)

                # Total number of created giscegas.factures
                acc_inv_n_linies_30 = cursor.fetchall()[0][0]
                search_params = [
                    ('linia_id', 'in', linies_20_ids_valides),
                    ('factura_id.state', 'not in', ['open', 'paid'])
                ]
                linia_30_query = accinv_linia_30_querier.select(
                    [Count('id')],
                    order_by=None
                ).where(
                    search_params
                )
                cursor.execute(*linia_30_query)

                # Total number of created giscegas.factures
                not_opened_acc_inv_n_linies_30 = cursor.fetchall()[0][0]

            res[importacio_id] = 0.0
            total_invoices = n_linies_30 + n_unprocessed_lines + n_invalid_lines + acc_inv_n_linies_30 + not_opened_acc_inv_n_linies_30 + not_opened_n_linies_30
            if total_invoices > 0:
                res[importacio_id] = float(n_linies_30+acc_inv_n_linies_30) / total_invoices * 100
        return res

    def _get_importacio_ids(self, cursor, uid, ids, context=None):
        l_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        i_id = []
        for a in l_obj.read(cursor, uid, ids, ['importacio_id']):
            if a['importacio_id']:
                i_id.append(a['importacio_id'][0])
        return i_id

    _STORE_PROC_AGREE_FIELDS = {
        'giscegas.facturacio.importacio.linia': (_get_importacio_ids,
                              ['state'], 20),
    }

    def _ff_trobar_polissa(self, cursor, uid, ids, field_name, arg,
                            context=None):
        """Aquest camp funció no es mostrarà en cap formulari.
           No ha d'actualitzar cap id.
        """
        return dict([(i, False) for i in ids])

    def _trobar_polissa(self, cursor, uid, ids, field, arg, context=None):
        """Retorna els ids dels lots d'importació amb factures de
           a la pòlissa especificada
        """
        if not context:
            context = {}
        poli = self.pool.get('giscegas.polissa')
        _poli = arg[0][2]
        p_id = poli.search(cursor, uid, [('name', '=', _poli)],
                                            context={'active_test': False})
        if not p_id:
            return [('id', 'in', [])]
        journal_obj = self.pool.get('account.journal')
        journal_code = 'CGAS'
        jid = journal_obj.search(cursor, uid, [('code', '=', journal_code)],
                                 limit=1)
        if not jid:
            return [('id', 'in', [])]
        fact = self.pool.get('giscegas.facturacio.factura')
        search_params = [('journal_id', '=', jid[0]),
                         ('polissa_id', '=', p_id[0])]
        ids = fact.search(cursor, uid, search_params,
                                            context={'active_test': False})
        vals = fact.read(cursor, uid, ids, ['importacio_id'])
        i_id = [i['importacio_id'][0] for i in vals]
        return [('id', 'in', i_id)]

    def _ff_data_carrega(self, cursor, uid, ids, field_name, arg,
                         context=None):
        "Data de càrrega del fitxer"
        if not context:
            context = {}
        res = {}
        dates = self.perm_read(cursor, uid, ids)
        res = dict([(d['id'], d['create_date'].split('.')[0]) for d in dates])

        return res

    def create_payment_orders_from_file(self, cursor, uid, import_file,
                                        context=None):
        if context is None:
            context = {}
        remeses = []
        if isinstance(import_file, zipfile.ZipFile):
            remeses = get_remeses_from_zip(import_file)
        else:
            remeses.append(get_remesa_from_xml(import_file))
        fswh_obj = self.pool.get('giscegas.facturacio.atr.helper')
        fswconfig_obj = self.pool.get('giscegas.facturacio.atr.config')
        part_obj = self.pool.get('giscemisc.participant')
        # generate's payment orders (remeses)
        for remesa in remeses:
            if not remesa:
                continue
            # gets emissor reference
            try:
                po_emisor_id = fswconfig_obj.get_emissor(
                    cursor, uid, remesa['emisor'], context
                )
            except message.except_b70 as e:
                # The DSO with this code is not found
                continue

            if po_emisor_id:
                po_emisor = part_obj.read(
                    cursor, uid, po_emisor_id, ['codi_sifco']
                )['codi_sifco']
                if po_emisor:
                    fswh_obj.get_payment_order(
                        cursor, uid, po_emisor, remesa, context=context
                    )
        return True

    def process_import(self, cursor, uid, import_id, context=None):
        """ Process the lines of an import. If the error parameter is True,
        the method will only import those lines that are in state:'error'. """
        if context is None:
            context = {}
        line_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        search_params = [('importacio_id', '=', import_id)]

        if context.get('import_errors', False):
            search_params += [
                '|',
                ('state', '=', False),
                ('state', '=', 'erroni')
            ]

        lines_ids = line_obj.search(
            cursor, uid, search_params
        )

        # Llegir els cups_text de lines_ids
        lines_vals = sorted(
            line_obj.read(
                cursor, uid, lines_ids, ['cups_text', 'codi_sollicitud'],
                context=context
            ), key=lambda k: k['codi_sollicitud']
        )

        # Group line_ids by cups_text
        cups_dict = {}

        for linia in lines_vals:
            fac_data = cups_dict.setdefault(linia['cups_text'], {})
            fac_data.setdefault('line_ids', []).append(linia['id'])

        # Format {CUP_TEXT_1: {line_ids: [line1, line2]}, CUP_TEXT_2...}

        db = pooler.get_db_only(cursor.dbname)

        for cups_t in cups_dict:
            try:
                tmp_cr = db.cursor()
                line_obj.process_line(
                    tmp_cr, uid, cups_dict[cups_t]['line_ids'], context=context
                )
                tmp_cr.commit()
            except Exception as e:
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()
                tmp_cr.rollback()
            finally:
                tmp_cr.close()

    def importat_ok_search(self, cursor, uid, obj, name, args, context):
        cursor.execute("SELECT DISTINCT importacio_id from giscegas_facturacio_importacio_linia where state != 'valid'")
        incorrect_ids = cursor.fetchall()
        incorrect_ids = [x[0] for x in incorrect_ids]
        operator = 'not in' if args[0][2] == 1 else 'in'
        return [('id', operator, incorrect_ids)]

    _columns = {
        'info': fields.function(
            _ff_check_status, string='Informació', type='text', multi='proc',
            method=True
        ),
        'digest': fields.function(
            _ff_check_status, string='Resum', type='text', multi='proc',
            method=True
        ),
        'name': fields.char('Nom Fitxer', size=1024, required=True),
        'linia_ids': fields.one2many(
                            'giscegas.facturacio.importacio.linia',
                            'importacio_id', 'Linies', ondelete='cascade'),
        'progres': fields.function(
            _ff_check_status, string='Progrés', readonly=True, method=True,
            type='float', multi='proc'
        ),
        'progres_importation': fields.function(
            _ff_check_status, string='Finalitzats', readonly=True, method=True,
            type='float', multi='proc'
        ),
        'progres_obertes': fields.function(
            _ff_opened, string='Oberts', method=True, type='float'
        ),
        'importat_ok': fields.function(_ff_check_status, method=True,
                                       string="Sense errors", type='boolean',
                                       multi='proc', fnct_search=importat_ok_search),
        'num_xml': fields.function(_ff_check_status, method=True,
                                       string="Total XMLs", type='integer',
                                       multi='proc'),
        'num_fact_creades': fields.function(_ff_check_status, method=True,
                                       string="Total factures creades",
                                       type='integer',
                                       multi='proc'),
        'partner_id': fields.many2one('res.partner', 'Empresa emisora',
                                      required=False),
        'hash': fields.char('Hash', size=32, requiered=True, readonly=True),
        'ff_polissa': fields.function(_ff_trobar_polissa, type='char',
                                      size='30', string='Contracte',
                                      fnct_search=_trobar_polissa,
                                      method=True),
        'data_carrega': fields.function(_ff_data_carrega, type='datetime',
                                        string='Data Càrrega', method=True)
    }

    _defaults = {
        'progres': lambda *a: 0,
    }

    _order = "id desc"

GiscegasFacturacioImportacio()


class LineProcessException(Exception):
    pass


class GiscegasFacturacioImportacioLinia(osv.osv):
    """Agrupació d'importacions"""

    _name = 'giscegas.facturacio.importacio.linia'
    _description = "Llistat de fitxers importats"

    def create_from_xml(self, cursor, uid, import_id, name, xml_data,
                        context=None):
        if context is None:
            context = {}

        line_ids = []
        for i in range(get_number_of_invoices_from_xml(xml_data)):
            vals = {
                'importacio_id': import_id,
                'name': "{0}_i{1}".format(name, i),
                'index_factura': i
            }

            header_dict = self.get_header_dict(xml_data, i)
            vals.update(header_dict)

            line_id = self.create(cursor, uid, vals)
            self.adjuntar_xml(cursor, uid, line_id, xml_data, name, context=context)
            line_ids.append(line_id)

        return line_ids

    def adjuntar_xml(self, cursor, uid, line_id, xml_data, fname, context=None):
        """Desa l'xml en l'erp"""
        attach_obj = self.pool.get('ir.attachment')
        if not context:
            context = {}

        desc = _(u"Fitxer adjunt: {0}".format(fname))
        md5_hash = get_md5(xml_data)
        vals = {
            'name': fname,
            'datas': base64.b64encode(xml_data),
            'datas_fname': fname,
            'res_model': 'giscegas.facturacio.importacio.linia',
            'res_id': line_id,
            'description': desc,
        }
        attch_id = attach_obj.create(cursor, uid, vals, context=context)
        vals = {'md5_hash': md5_hash, 'attachment_id': attch_id}
        self.write(cursor, uid, [line_id], vals)
        return attch_id

    def unlink(self, cursor, uid, ids, context=None):
        """S'esborren les línies d'importació que no contenen:
           + Factures de proveïdor obertes
           + Extres vinculats a factures de client
           Retorna:
           + True si esborra totes les línies
           + False en cas contrari
        """
        if not context:
            context = {}
        imp = self.pool.get('giscegas.facturacio.importacio')
        lf = self.pool.get('giscegas.facturacio.importacio.linia.factura')
        la = self.pool.get('giscegas.facturacio.importacio.linia.account.invoice')
        fact = self.pool.get('giscegas.facturacio.factura')
        inv = self.pool.get('account.invoice')
        sum_fact = 0
        u_imp = []
        u_linia = []
        u_linfac = []
        u_fact = []
        u_inv = []
        for lid in ids:
            lvals = self.read(cursor, uid, lid, ['importacio_id',
                                                 'liniafactura_id',
                                                 'linia_account_invoice_id'])
            # comprovar si la factura associada a la línia és oberta
            obert = False
            if lvals and lvals['liniafactura_id']:
                lfvals = lf.read(cursor, uid, lvals['liniafactura_id'],
                                 ['factura_id'])
                for val in lfvals:
                    fid = val['factura_id'][0]
                    f_estat = fact.read(cursor, uid, fid, ['state'])['state']
                    if f_estat != 'open':
                        u_fact.append(fid)
                        u_linfac.append(val['id'])
                        continue
                    obert = True
                if obert:
                    continue
            obert = False
            if lvals and lvals['linia_account_invoice_id']:
                lfvals = la.read(cursor, uid, lvals['linia_account_invoice_id'], ['factura_id'])
                for val in lfvals:
                    fid = val['factura_id'][0]
                    f_estat = inv.read(cursor, uid, fid, ['state'])['state']
                    if f_estat != 'open':
                        u_inv.append(fid)
                        u_linfac.append(val['id'])
                        continue
                    obert = True
                if obert:
                    continue
            if (lvals['importacio_id']
                and lvals['importacio_id'][0] not in u_imp):
                u_imp.append(lvals['importacio_id'][0])
            u_linia.append(lid)
            self.unlink_lectures(cursor, uid, lid, context)
            self.unlink_facturacio_extra(cursor, uid, lid, context)
            self.unlink_adjunts(cursor,  uid, lid, context)
        fact.unlink(cursor, uid, u_fact, context=context)
        inv.unlink(cursor, uid, u_inv, context=context)
        super(GiscegasFacturacioImportacioLinia, self).unlink(cursor, uid,
                                                     u_linia, context=context)
        # actualitzar info de les importacions
        for iid in u_imp:
            ivals = imp.read(cursor, uid, iid,
                                        ['num_xml', 'num_fact_creades'])
            i_info = ("%d XML importats, %d factures creades" %
                                 (ivals['num_xml'], ivals['num_fact_creades']))
            imp.write(cursor, uid, iid, {'info': i_info})
        return u_linia == ids

    def unlink_adjunts(self, cursor, uid, lid, context=None):
        """Eliminar els adjunts vinculats a la línia
        """
        if not context:
            context = {}
        att = self.pool.get('ir.attachment')
        att_ids = att.search(cursor, uid,
                [('res_id','=',lid),
                 ('res_model','=','giscegas.facturacio.importacio.linia')])
        if att_ids:
            att.unlink(cursor, uid, att_ids)

    def unlink_lectures(self, cursor, uid, lid, context=None):
        """Eliminar les lectures de comptador associades a la línia a través
           de giscegas.facturacio.importacio.lectures_lectura/potencia
        """
        if not context:
            context = {}
        lin_lect = self.pool.get('giscegas.facturacio.importacio.linia.lecturafactura')
        lect_lect = self.pool.get('giscegas.lectures.lectura.pool')
        lin_lect_id = lin_lect.search(cursor, uid, [('linia_id', '=', lid)])
        if lin_lect_id:
            lect_id_obj = lin_lect.read(cursor, uid, lin_lect_id, ['lectura_id'])
            lect_ids = [x['lectura_id'][0] for x in lect_id_obj if x['lectura_id']]
            if lect_ids:
                lect_lect.unlink(cursor, uid, lect_ids, context)

    def unlink_factures(self, cursor, uid, lid, context=None):
        """Eliminar factures associades a la linia lid"""
        lin_fact = self.pool.get(
                    'giscegas.facturacio.importacio.linia.factura')
        fact = self.pool.get('giscegas.facturacio.factura')

        lin_fact_id = lin_fact.search(cursor, uid, [('linia_id', '=', lid)])
        fact_id = lin_fact.read(cursor, uid, lin_fact_id, ['factura_id'],
                                                                     context)
        ids = [x['factura_id'][0] for x in fact_id]
        if ids:
            fact.unlink(cursor, uid, ids, context)

    def unlink_extra(self, cursor, uid, lid, context=None):
        """Eliminiar linies Extra associades a la linia lid"""
        lin_extra = self.pool.get(
                    'giscegas.facturacio.importacio.linia.extra')
        extra = self.pool.get('giscegas.facturacio.extra')
        lin_extra_id = lin_extra.search(cursor, uid, [('linia_id', '=', lid)])
        extra_id = lin_extra.read(cursor, uid, lin_extra_id, ['extra_id'],
                                                                       context)
        ids = [x['extra_id'][0] for x in extra_id]
        if ids:
            extra.unlink(cursor, uid, ids, context)

    def unlink_facturacio_extra(self, cursor, uid, lid, context=None):
        """Eliminiar les línies de giscegas.facturacio.extra associades a
           la línia a través de giscegas.facturacio.importacio.linia.extra
        """
        if not context:
            context = {}
        lin_ext = self.pool.get(
                    'giscegas.facturacio.importacio.linia.extra')
        ext = self.pool.get('giscegas.facturacio.extra')
        lin_ext_id = lin_ext.search(cursor, uid, [('linia_id', '=', lid)])
        if lin_ext_id:
            lin_ext_obj = lin_ext.read(cursor, uid, lin_ext_id, ['extra_id'])
            ext_ids = [x['extra_id'][0] for x in lin_ext_obj]
            ext.unlink(cursor, uid, ext_ids, context)

    _states_selection = [
        ('erroni', 'Error en la importació'),
        ('valid', 'Importat correctament'),
        ('incident', 'Incidents en la importació'),
    ]

    def recreate_checks(self, cursor, uid, line_id, imported_invoices,
                        context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscegas.facturacio.factura')
        error_obj = self.pool.get('giscegas.facturacio.atr.error')

        for inv_id in imported_invoices:
            inv_vals = fact_obj.read(
                cursor, uid, inv_id, ['comment']
            )
            comments = inv_vals['comment'].split('\n')
            tipus = ''
            for comment in comments:
                if '*******' in comment:
                    tipus = comment.split()[1].lower()
                if 'Subt XML' in comment:
                    split_com = comment.split()

                    total_xml = float(split_com[2])
                    total_imp = float(split_com[7])
                    diff = float(split_com[10])
                    if abs(diff) > 0.01:
                        error_obj.create_error_from_code(
                            cursor, uid, line_id, '2', '006',
                            {
                                'tipus': tipus,
                                'total_xml': total_xml,
                                'total_imp': total_imp
                            },
                            context=context
                        )

    def check_invoice_exists(self, cursor, uid, line_id, factura, context=None):
        """
        Checks if exists an the same invoice (by origin) and partner (DSO)
        :param line_id: The id of the giscegas.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact: Atr's version of the invoice
        :param context: OpenERP context
        :return: True if not found, False (exception) if found
        """
        if context is None:
            context = {}

        use_account_invoice = context.get('use_account_invoice')
        if use_account_invoice:
            inv_obj = self.pool.get('account.invoice')
            liniafact_obj = self.pool.get(
                'giscegas.facturacio.importacio.linia.account.invoice'
            )

            line_vals = self.read(
                cursor, uid, line_id, ['distribuidora_id', 'linia_account_invoice_id'],
                context=context
            )
            line_vals_fact_ids = line_vals['linia_account_invoice_id']
        else:
            inv_obj = self.pool.get('giscegas.facturacio.factura')
            liniafact_obj = self.pool.get(
                'giscegas.facturacio.importacio.linia.factura'
            )

            line_vals = self.read(
                cursor, uid, line_id, ['distribuidora_id', 'liniafactura_id'],
                context=context
            )
            line_vals_fact_ids = line_vals['liniafactura_id']

        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        part_obj = self.pool.get('giscemisc.participant')

        pdistribuidora_id = line_vals['distribuidora_id'][0]
        distribuidora_id = part_obj.get_partner_id(cursor, uid, pdistribuidora_id)

        inv_number = factura.get_origin()
        fact_data = datetime.strptime(
            factura.fecfactura, '%Y-%m-%d'
        )
        inv_ids = inv_obj.get_in_invoice_by_origin(
            cursor, uid, inv_number, distribuidora_id, year=str(fact_data.year),
            context=context
        )
        if len(inv_ids):
            imported_invoices = []
            for liniafact_id in line_vals_fact_ids:
                imported_invoices.append(
                    liniafact_obj.read(
                        cursor, uid, liniafact_id, ['factura_id']
                    )['factura_id'][0]
                )

            # All the inv ids should have already been imported with this line
            # If any of the invoices hasn't been imported in this line, we need
            # to raise an error
            raise_error = False
            for inv_id in inv_ids:
                if inv_id not in imported_invoices:
                    raise_error = True

            # We recreate the checks that we had already created before but have
            # been deleted on reimport
            if not use_account_invoice:
                self.recreate_checks(
                    cursor, uid, line_id, imported_invoices, context=context
                )

            if raise_error:
                distri_vals = part_obj.read(
                    cursor, uid, pdistribuidora_id, ['codi_sifco']
                )

                vals = {
                    'distribuidora': distri_vals['codi_sifco'],
                    'num_factura': inv_number
                }
                error_obj.create_error_from_code(
                    cursor, uid, line_id, '2', '001', vals, context=context
                )
                raise LineProcessException('STOP Procces')

    def set_phase_manual(self, cursor, uid, line_ids, context=None):
        self.set_phase(cursor, uid, line_ids, 50, context=context)
        amb_fact = []
        sense_fact = []
        for linfo in self.read(cursor, uid, line_ids, ['liniafactura_id'], context):
            if len(linfo.get('liniafactura_id')) > 0:
                amb_fact.append(linfo['id'])
            else:
                sense_fact.append(linfo['id'])
        self.write(cursor, uid, amb_fact, {'state': 'valid'})
        self.write(cursor, uid, sense_fact, {'state': 'erroni'})

    def set_phase(self, cursor, uid, line_ids, phase, context=None):
        """
        Sets the import_phase of the line(s) to phase
        :param cursor: Database cursor
        :param uid: User's id
        :param context: OpenERP context
        :param line_id: Id(s) of the line(s) to update
        :param phase: Phase we want to set the line(s)
        """
        if context is None:
            context = {}

        if isinstance(line_ids, (int, long)):
            line_ids = [line_ids]

        error_obj = self.pool.get('giscegas.facturacio.atr.error')

        self.write(cursor, uid, line_ids, {'import_phase': phase}, context)

        phase_error = str(phase * 100)

        # Clean errors when reimport
        error_ids = error_obj.search(
            cursor, uid, [
                ('name', '>=', phase_error),
                ('line_id', 'in', line_ids)
            ]
        )
        error_obj.unlink(cursor, uid, error_ids, context)

    @job(queue='import_xml')
    def process_line(self, cursor, uid, line_id, context=None):
        if context is None:
            context = {}
        logger = logging.getLogger(
            'openerp.{}.process_file'.format(__name__)
        )
        if isinstance(line_id, (int, long)):
            line_id = [line_id]

        for line in line_id:
            self.process_line_sync(cursor, uid, line, context=context)
            logger.info('Processed line id {0}'.format(line))

    def validate_phase_1(self, cursor, uid, line_id, context=None):
        """
        Validates B70 xml. if OK, returns B70 atr_gas object to be used in next
        steps
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :return: B70 atr_gas object
        """
        if context is None:
            context = {}
        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        try:
            data = self.get_xml_from_adjunt(
                cursor, uid, line_id, context=context)
            b70_xml = B70(data, 'B70')
            b70_xml.parse_xml()
        except message.except_f1 as e:
            if "'facturasctd': No matching global declaration available for the validation root." in e.value:
                # A vegades envien el B70 sense l'atribut a la capçalera facturasctd....
                try:
                    b70_xml = B70(data, 'B70')
                    b70_xml.parse_xml(validate=False)
                    return b70_xml
                except message.except_f1 as e:
                    pass
                except Exception as e:
                    pass
            error_obj.create_error_from_code(
                cursor, uid, line_id, '1', '001', {'exception': e.value or str(e)},
                context=context
            )
            raise LineProcessException('STOP Procces')
        except Exception as e:
            error_obj.create_error_from_code(
                cursor, uid, line_id, '1', '001', {'exception': str(e)},
                context=context
            )
            raise LineProcessException('STOP Procces')
        return b70_xml

    def process_phase_1(self, cursor, uid, line_id, index, context=None):
        """
        Phase 1 of B70 processing. XML loading, payment order creation and
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :return: B70 atr_gas object
        """
        if context is None:
            context = {}

        error_obj = self.pool.get('giscegas.facturacio.atr.error')

        self.set_phase(cursor, uid, line_id, 10, context)

        # Clean errors when reimport
        line_vals = self.read(cursor, uid, line_id, ['error_ids'])
        for error_id in line_vals['error_ids']:
            error_obj.unlink(cursor, uid, error_id, context)

        b70_xml = self.validate_phase_1(cursor, uid, line_id, context=context)
        self.read_lectures_from_xml(cursor, uid, line_id, b70_xml, index, context=context)
        self.update_header_info(cursor, uid, line_id, index, context=context)
        self.check_processed(cursor, uid, line_id, index, context=context)
        self.check_cups_distri(cursor, uid, line_id, context=context)
        return b70_xml

    def read_lectures_from_xml(self, cursor, uid, line_id, b70_xml, index,
                               context=None):
        lecturas_b70_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.lectures'
        )
        if not b70_xml:
            return
        lect_remove_ids = lecturas_b70_obj.search(
            cursor, uid, [('linia_id', '=', line_id)], context=context
        )
        if lect_remove_ids:
            lecturas_b70_obj.unlink(
                cursor, uid, lect_remove_ids, context=context)
        factura_atr = b70_xml.facturas[index]
        if not factura_atr.is_only_conceptes():
            for medidor in factura_atr.listamedidores:
                vals = medidor.get_lectures_info()
                for l_vals in vals:
                    l_vals.update({'linia_id': line_id})
                    l_vals['lectura_desde'] = l_vals['lectura_desde_m3']
                    l_vals['lectura_actual'] = l_vals['lectura_actual_m3']
                    del l_vals['lectura_desde_m3']
                    del l_vals['lectura_actual_m3']
                    lecturas_b70_obj.create(
                        cursor, uid, l_vals, context=context
                    )

    def update_header_info(self, cursor, uid, line_id, index, context=None):
        """
        Updates header info from current attachment
        :param line_id:
        :return: result of update
        """
        imp_conf_obj = self.pool.get('giscegas.facturacio.atr.config')
        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        cups_obj = self.pool.get('giscegas.cups.ps')

        xml_data = self.get_xml_from_adjunt(
            cursor, uid, line_id, context=context
        )

        vals = self.get_header_dict(xml_data, index)

        # Emissor
        source_code = vals['ree_source_code']
        try:
            emissor_id = imp_conf_obj.get_emissor(cursor, uid, source_code)
            vals.update({'distribuidora_id': emissor_id})
        except message.except_b70 as e:
            phase = '1'
            error_code = '999'
            error_vals = {'text': e.value}
            if e.name == 'PartnerNotFound':
                error_code = '002'
                error_vals = {'ree_origin_code': source_code}
            elif e.name == 'MultiplePartnerForm':
                error_code = '003'
                error_vals = {'ree_origin_code': source_code}
            else:
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()
            error_obj.create_error_from_code(
                cursor, uid, line_id, phase, error_code, error_vals
            )
            raise LineProcessException('STOP Procces')

        if vals.get('tipus_b70') == '33':
            return self.write(cursor, uid, [line_id], vals, context=context)

        # CUPS
        ctx = context.copy()
        ctx.update({'active_test': False})
        cups = vals.get('cups_text', '').strip().upper()
        cups_ids = cups_obj.search(
            cursor, uid, [('name', '=like', cups + '%')], context=ctx
        )

        if not cups_ids:
            # Si no hem trobat cap CUPS intentem buscar sense la terminacio
            if len(cups) > 20:
                cups_ids = cups_obj.search(
                    cursor, uid, [('name', '=like', cups[:20] + '%')],
                    context=ctx
                )

        if len(cups_ids) == 1:
            # Si hem trobat un sol CUPS, es aquest
            vals.update({'cups_id': cups_ids[0]})
        elif len(cups_ids) > 1:
            # Si hem trobat més d'un CUPS, no sabem quin triar
            error_obj.create_error_from_code(
                cursor, uid, line_id, '1', '008', {'cups': cups}
            )
            raise LineProcessException('STOP Procces')
        else:
            # Si no hem trobat cap CUPS no podem continuar
            error_obj.create_error_from_code(
                cursor, uid, line_id, '1', '005', {'cups': cups}
            )
            raise LineProcessException('STOP Procces')

        return self.write(cursor, uid, [line_id], vals, context=context)

    def get_header_dict(self, xml_data, index):
        """
        Returns dict to update line b70 header fields
        :param xml_data:
        :return:dict with values to update
        `giscegas.facturacio.importacio.line` model:
        {
            'cups_text': CUPS,
            'ree_source_code': SOURCE,
            'ree_destination_code': DESTINATION,
            'codi_sollicitud': SOLLICITUD code,
            'index_factura': SOLLICITUD sequence,
            'b70_date': DATE,
        }
        """
        header_b70 = get_header_from_xml(xml_data, index)

        invoice_number_text = re.findall(
            '<numfactura>([^<]*)</numfactura>', xml_data
        )

        vals = {
            'cups_text': header_b70['cups'],
            'ree_source_code': header_b70['codigo_ree_empresa_emisora'],
            'ree_destination_code': header_b70['codigo_ree_empresa_destino'],
            'codi_sollicitud': header_b70['codigo_solicitud'],
            'b70_date': header_b70['fecha_solicitud'],
            'invoice_number_text': invoice_number_text[index],
            'fecha_factura': header_b70['fecha_factura'],
            'tipo_factura_b70': header_b70['tipo_factura_b70'],
            'type_factura': header_b70['type_factura'],
            'fecha_factura_desde': header_b70['fecha_factura_desde'],
            'fecha_factura_hasta': header_b70['fecha_factura_hasta'],
            'tipus_b70': header_b70['proces_b70']
        }

        return vals

    def check_processed(self, cursor, uid, line_id, index, context=None):
        if context is None:
            context = {}
        md5_hash = self.read(cursor, uid, line_id, ['md5_hash'])['md5_hash']
        search_params = [
            ('md5_hash', '=', md5_hash),
            ('id', '!=', line_id),
            ('index_factura', '=', index)
        ]
        lines_search = self.search(cursor, uid, search_params)
        if lines_search:
            error_obj = self.pool.get('giscegas.facturacio.atr.error')
            error_obj.create_error_from_code(
                cursor, uid, line_id, '1', '006',
                {'fitxer_ids': lines_search}
            )
            raise LineProcessException('STOP Process')
        return True

    def check_cups_distri(self, cursor, uid, line_id, context=None):
        """
        Checks if the given CUPS belongs to the given distri
        :param cursor:
        :param uid:
        :param line_id:
        :param context:
        :return: True when CUPS belongs to the given distri, raises
        LineProcessException if it doesn't
        """
        cups_obj = self.pool.get('giscegas.cups.ps')
        imp_conf_obj = self.pool.get('giscegas.facturacio.atr.config')
        partner_obj = self.pool.get('res.partner')
        error_obj = self.pool.get('giscegas.facturacio.atr.error')

        vals = self.read(cursor, uid, line_id, ['cups_id', 'ree_source_code'])

        if vals['cups_id']:
            cups_id = vals['cups_id'][0]
            # CUPS DSO Validation
            cups_data = cups_obj.read(
                cursor, uid, cups_id, ['name', 'distribuidora_id'],
                context=context
            )
            emissor_id = imp_conf_obj.get_emissor(
                cursor, uid, vals['ree_source_code']
            )
            part_obj = self.pool.get('giscemisc.participant')
            emissor_id = part_obj.get_partner_id(cursor, uid, emissor_id)
            if cups_data['distribuidora_id'][0] != emissor_id:
                emissor_data = partner_obj.read(
                    cursor, uid, emissor_id, ['name']
                )
                cups_dso = cups_data['distribuidora_id'][1]
                emissor_dso = emissor_data['name']

                error_obj.create_error_from_code(
                    cursor, uid, line_id, '1', '007',
                    {
                        'cups': cups_data['name'],
                        'distri_cups': cups_dso,
                        'distri_b70': emissor_dso
                    }
                )
                raise LineProcessException('STOP Procces')
        return True

    def process_phase_2(self, cursor, uid, line_id, b70_xml, index, context=None):
        """
        Phase 2 of B70 processing. DSO invoice creation
        :param context:
        :param uid:
        :param cursor:
        :param line_id:
        :param b70_xml: B70 atr object
        :return:
        """
        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        part_obj = self.pool.get('giscemisc.participant')
        imp_line_fact_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.factura'
        )
        imp_line_acc_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.account.invoice'
        )
        imp_extra_line_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.extra'
        )
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        extra_obj = self.pool.get('giscegas.facturacio.extra')
        inv_obj = self.pool.get("account.invoice")

        self.set_phase(cursor, uid, line_id, 20, context)

        try:
            all_invoices = [b70_xml.facturas[index]]
            if all_invoices[0].is_only_conceptes():
                atr_invoices = []
                other_invoices = all_invoices
            else:
                atr_invoices = all_invoices
                other_invoices = []

            line_vals = self.read(
                cursor, uid, line_id,
                ['distribuidora_id', 'liniafactura_id', 'liniaextra_id', 'linia_account_invoice_id'],
                context=context
            )

            distri_id = line_vals['distribuidora_id'][0]
            inf = part_obj.read(cursor, uid, distri_id, ['codi_sifco', 'partner_id'])
            emissor = inf['codi_sifco']
            partner_id = inf['partner_id'][0]

            # No tenim info remeses, nomes un sol ID....
            # remesa_data = b70_xml.registro.get_remesa()

            # First of all, we unlink all the invoices we have imported which are
            # still in draft
            imp_line_fact_ids = imp_line_fact_obj.search(
                cursor, uid, [
                    ('state', 'in', ('draft',)),
                    ('id', 'in', line_vals['liniafactura_id'])
                ]
            )
            factura_ids = [
                fact['factura_id'][0] for fact in imp_line_fact_obj.read(
                    cursor, uid, imp_line_fact_ids, ['factura_id']
                )
            ]
            fact_obj.unlink(cursor, uid, factura_ids)

            imp_line_acc_inv_ids = imp_line_acc_obj.search(
                cursor, uid, [
                    ('state', 'in', ('draft',)),
                    ('id', 'in', line_vals['linia_account_invoice_id'])
                ]
            )
            acc_inv_ids = [
                fact['factura_id'][0] for fact in imp_line_acc_obj.read(
                    cursor, uid, imp_line_acc_inv_ids, ['factura_id']
                )
            ]
            inv_obj.unlink(cursor, uid, acc_inv_ids)

            # now we unlink the proforma invoices
            imp_line_fact_ids = imp_line_fact_obj.search(
                cursor, uid, [
                    ('state', 'in', ('proforma2',)),
                    ('id', 'in', line_vals['liniafactura_id'])
                ]
            )
            if imp_line_fact_ids:
                factura_ids = [
                    fact['factura_id'][0] for fact in imp_line_fact_obj.read(
                        cursor, uid, imp_line_fact_ids, ['factura_id']
                    )
                ]
                base_ids = fact_obj._get_base_ids(cursor, uid, factura_ids)
                wf_service = netsvc.LocalService("workflow")
                for bid in base_ids:
                    wf_service.trg_validate(
                        uid, 'account.invoice', bid, 'invoice_cancel', cursor
                    )
                fact_obj.unlink(cursor, uid, factura_ids)

            # Finally we unlink the account invoices created by proformas
            orig = all_invoices[0].numfactura
            inv_ids = inv_obj.search(
                cursor, uid,
                [('origin', '=', orig), ('comment', 'like', "B70 {0}".format(orig))]
            )
            inv_obj.unlink(cursor, uid, inv_ids)

            non_deletable_facts = imp_line_fact_obj.search(
                cursor, uid, [
                    ('id', 'in', line_vals['liniafactura_id'])
                ]
            )
            if not non_deletable_facts:
                # If we don't delete the invioces we won't create the new invoice
                # lines so we shouldn't be deleting them
                deletable_imp_line_ids = imp_extra_line_obj.search(
                    cursor, uid, [
                        ('id', 'in', line_vals['liniaextra_id']),
                        ('amount_invoiced', '=', 0),
                        ('total_amount_invoiced', '=', 0),
                    ]
                )
                extra_line_ids = [
                    line['extra_id'][0] for line in imp_extra_line_obj.read(
                        cursor, uid, deletable_imp_line_ids, ['extra_id']
                    )
                ]
                extra_obj.unlink(cursor, uid, extra_line_ids)

                non_deletable_lines = imp_extra_line_obj.search(
                    cursor, uid, [('id', 'in', line_vals['liniaextra_id'])]
                )
                if non_deletable_lines:
                    error_obj.create_error_from_code(
                        cursor, uid, line_id, '2', '014',
                        {
                            'line_ids': ', '.join(map(str, non_deletable_lines)),
                        },
                        context=context
                    )
                    raise LineProcessException('STOP Process')

            invoice_numbers = []
            imported_invoices = []
            extra_lines = []

            fact_help_obj = self.pool.get('giscegas.facturacio.atr.helper')
            remesa_data = b70_xml.facturas[index].get_remesa()
            payment_order_id = fact_help_obj.get_payment_order(
                cursor, uid, emissor, remesa_data, context=context
            )
            data_remesa = remesa_data.get('fecha_valor_remesa')

            # Common part for ATR and others
            for invoice in all_invoices:
                origin_number = invoice.get_origin()
                if origin_number not in invoice_numbers:
                    invoice_numbers.append(origin_number)
                else:
                    error_obj.create_error_from_code(
                        cursor, uid, line_id, '2', '013',
                        {
                            'inv_number': origin_number,
                        },
                        context=context
                    )
                    raise LineProcessException('STOP Process')

                if b70_xml.codproceso == '33':
                    self.process_all_invoice_phase_2_b7033(
                        cursor, uid, line_id, invoice, context=context
                    )
                elif b70_xml.codproceso == '32':
                    other_ids = self.check_pseudofactura_repetida(cursor, uid, line_id, origin_number, invoice, b70_xml, context=context)
                    if len(other_ids):
                        self.set_phase(cursor, uid, line_id, 40)
                        self.update_pseudofactura_repetida(cursor, uid, line_id, other_ids, origin_number, invoice, b70_xml, context=context)
                        self.write(cursor, uid, line_id, {'state': 'valid'})
                        raise LineProcessException('END Process')
                else:
                    self.process_all_invoice_phase_2(
                        cursor, uid, line_id, invoice, context=context
                    )
            ctx = context.copy()
            if b70_xml.codproceso == '31':
                ctx['tipo_factura'] = '01'
            elif b70_xml.codproceso == '32':
                ctx['tipo_factura'] = '11'
            elif b70_xml.codproceso == '33':
                ctx['tipo_factura'] = '99'

            if b70_xml.codproceso == '33':
                self.process_atr_invoice_phase_2_b7033(
                    cursor, uid, line_id, invoice,
                    data_remesa, partner_id,
                    payment_order_id, context=ctx
                )
            else:
                # Only for ATR
                for invoice in atr_invoices:
                    imp_inv, extra_lines_it = self.process_atr_invoice_phase_2(
                        cursor, uid, line_id, invoice,
                        data_remesa, partner_id,
                        payment_order_id, context=ctx
                    )
                    if imp_inv:
                        imported_invoices.append(imp_inv)
                        extra_lines += extra_lines_it

                # Only for Others
                for invoice in other_invoices:
                    imp_inv, extra_lines_it = self.process_other_invoice_phase_2(
                        cursor, uid, line_id, invoice,
                        False, partner_id,
                        False, context=ctx
                    )
                    if imp_inv:
                        imported_invoices.append(imp_inv)
                        extra_lines += extra_lines_it

                for inv in imp_line_fact_obj.read(cursor, uid, imported_invoices, ['factura_id']):
                    if self.check_must_be_proforma(cursor, uid, line_id, b70_xml, index, inv['factura_id'][0], context=context):
                        self.process_set_proforma(cursor, uid, line_id, b70_xml, index, inv['factura_id'][0], context=context)

        except LineProcessException:
            raise  # If we get a LineProcessException we raise it directly
        except message.except_b70 as e:
            phase = '2'

            error_dic = {
                'UnidentifiablePeriode': {'code': '003', 'vals': e.values_dict},
                'JournalNotFound': {'code': '004', 'vals': {'diari': e.value}},
                'ContractNotFound': {'code': '005', 'vals': e.values_dict},
                'ReferenceInvoiceNotFound': {
                    'code': '007', 'vals': {'fact_ref': e.value}
                },
                'atrConfigNotFound': {
                    'code': '008', 'vals': e.values_dict
                },
            }

            error_params = error_dic.get(e.name, {})
            error_code = error_params.get('code', '999')
            error_vals = error_params.get('vals', {'text': e.value})

            error_obj.create_error_from_code(
                cursor, uid, line_id, phase, error_code, error_vals,
                context=context
            )
            raise LineProcessException('STOP Procces')
        except osv.except_osv as e:
            phase = '2'
            error_code = '999'
            error_vals = {'text': e.value}
            if u"No s'ha trobat la configuració" in e.value:
                error_code = '008'
                error_vals = {'emissor': emissor}
            elif u"No s'ha configurat el mode de pagament" in e.value:
                pass
            error_obj.create_error_from_code(
                cursor, uid, line_id, phase, error_code, error_vals,
                context=context
            )
            raise LineProcessException('STOP Procces')
        except Exception as e:
            error_obj.create_error_from_code(
                cursor, uid, line_id, '2', '999', {'text': e},
                context=context
            )
            raise

    def check_pseudofactura_repetida(self, cursor, uid, line_id, origin_number, invoice_xml, b70_xml, context=None):
        # Busquem si hi ha alguna factura en borrador creada amb el mateix
        # partner i  el mateix numero de pseudofactura (la part dreta del origen)
        # que sigui iguala a aquesta
        fact_obj = self.pool.get("giscegas.facturacio.factura")
        part_obj = self.pool.get('giscemisc.participant')
        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        pdistribuidora_id = part_obj.get_id_from_sifco(cursor, uid, b70_xml.get_codi_emisor)
        distribuidora_id = part_obj.get_partner_id(cursor, uid, pdistribuidora_id)
        origin = origin_number.split("-")[1].strip()
        data_inici, data_final = invoice_xml.get_periode_factura()
        search_params = [
            ('origin', '=like', "% - {0}".format(origin)),
            ('state', '=', 'draft'),
            ('partner_id', '=', distribuidora_id),
            ('type', '=like', 'in_%'),
            ('data_inici', '=', data_inici),
            ('data_final', '=', data_final),
        ]
        other_ids = fact_obj.search(cursor, uid, search_params, context=context)
        if len(other_ids) > 1:
            e = _(
                u"Més de una factura trobada amb numero de pseudofactura {0} "
                u" com a origen amb periode facturat desde {1} fins a {2} per la distribuidora {3}."
            ).format(origin, data_inici, data_final, b70_xml.get_codi_emisor)
            error_obj.create_error_from_code(
                cursor, uid, line_id, '2', '999', {'text': e},
                context=context
            )
            raise LineProcessException('STOP Procces')
        elif len(other_ids) == 1:
            return other_ids
        return []

    def update_pseudofactura_repetida(self, cursor, uid, line_id, other_ids, origin_number, invoice_xml, b70_xml, context=None):
        fact_obj = self.pool.get("giscegas.facturacio.factura")
        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        for finfo in fact_obj.read(cursor, uid, other_ids, ['origin']):
            new_origin = finfo['origin']
            if finfo['origin'] and len(finfo['origin'].split("-")) == 2:
                part_dreta, part_esquerra = finfo['origin'].split("-")
                if part_dreta.strip() == part_esquerra.strip():
                    fact_obj.write(cursor, uid, finfo['id'], {'origin': origin_number})
                    new_origin = origin_number
            error_obj.create_error_from_code(
                cursor, uid, line_id, '4', '003', {
                    'old_origin': finfo['origin'],
                    'factura_id': finfo['id'],
                    'new_origin': new_origin,
                }, context=context
            )
        return True

    def check_must_be_proforma(self, cursor, uid, line_id, b70_xml, index, inv_id, context=None):
        if context is None:
            context = {}

        inv_obj = self.pool.get("giscegas.facturacio.factura")
        # Només en els b7032 generem proformas
        if b70_xml.codproceso != '32':
            return False
        else:
            com = inv_obj.read(cursor, uid, inv_id, ['comment'])['comment'] or "\n"
            inv_obj.write(cursor, uid, inv_id, {'comment': com+"Pseudofactura GAS\n"})
        # Nomes fa falta generar proforma si el num factura esta repetit
        i = 0
        origen = b70_xml.facturas[index].get_origin()
        for fact in b70_xml.facturas:
            if fact.get_origin() == origen and index != i:
                return True
            i += 1
        return True

    def process_set_proforma(self, cursor, uid, line_id, b70_xml, index, inv_id, context=None):
        if context is None:
            context = {}

        mp_origin = b70_xml.facturas[index].numfactura
        if self.check_must_create_invoice_from_proformas(cursor, uid, mp_origin, b70_xml, context=context):
            self.create_invoice_from_proformas(cursor, uid, mp_origin, b70_xml, context=context)

    def check_must_create_invoice_from_proformas(self, cursor, uid, mp_origin, b70_xml, context=None):
        if context is None:
            context = {}

        conf_obj = self.pool.get("giscegas.facturacio.atr.config")
        conf_vals = conf_obj.get_config(cursor, uid, b70_xml.get_codi_emisor, context=context)

        # Hi ha distris que envien B7033 a més de B7032. El B7033 es la factura
        # real i ja no necessitem que els B7032 generin factures agrupades
        if not conf_vals.get('b7032_generates_invoice'):
            return False

        # Hi ha distris que envien B7032 amb pseudofactura = numfactura.
        # Son com un avanç a enviar un B7032 amb totes les factures alhora
        # i un numero de factura real.
        # En aquest cas no s'ha de generar factura.
        all_equals = True
        for fxml in b70_xml.facturas:
            if fxml.numpseudofactura != fxml.numfactura:
                all_equals = False
                break
        if not conf_vals.get("b7032_same_num_generates_invoice") and all_equals:
            return False

        fact_obj = self.pool.get("giscegas.facturacio.factura")
        part_obj = self.pool.get('giscemisc.participant')
        pdistribuidora_id = part_obj.get_id_from_sifco(cursor, uid, b70_xml.get_codi_emisor)
        distribuidora_id = part_obj.get_partner_id(cursor, uid, pdistribuidora_id)
        # If we have all the invoices from b70 created in proforma2 we can create the multipunt
        for factura in b70_xml.facturas:
            if factura.numfactura != mp_origin:
                continue

            inv_number = factura.get_origin()
            fact_data = datetime.strptime(
                factura.fecfactura, '%Y-%m-%d'
            )
            inv_ids = fact_obj.get_in_invoice_by_origin(
                cursor, uid, inv_number, distribuidora_id, year=str(fact_data.year),
                context=context
            )
            if not len(inv_ids):
                return False
        return True

    def create_invoice_from_proformas(self, cursor, uid, mp_origin, b70_xml, context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get("giscegas.facturacio.factura")
        partner_obj = self.pool.get("res.partner")
        part_obj = self.pool.get('giscemisc.participant')
        pdistribuidora_id = part_obj.get_id_from_sifco(cursor, uid, b70_xml.get_codi_emisor)
        distribuidora_id = part_obj.get_partner_id(cursor, uid, pdistribuidora_id)
        inv_ids = []
        for factura in b70_xml.facturas:
            if factura.numfactura != mp_origin:
                continue

            inv_number = factura.get_origin()
            fact_data = datetime.strptime(
                factura.fecfactura, '%Y-%m-%d'
            )
            inv_id = fact_obj.get_in_invoice_by_origin(
                cursor, uid, inv_number, distribuidora_id, year=str(fact_data.year),
                context=context
            )[0]
            inv_ids.append(inv_id)
        ginv_id = fact_obj.create_grouped_invoice(cursor, uid, inv_ids, mp_origin, context=context)
        return ginv_id

    def process_all_invoice_phase_2(self, cursor, uid, line_id, fact_xml,
                                    context=None):
        """
        Processes an individual invoice from B70 of either type
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :param fact_xml: The atr version of the invoice we are importing
        :return:
        """
        imp_lin_obj = self.pool.get('giscegas.facturacio.importacio.linia')

        imp_lin_obj.check_invoice_exists(
            cursor, uid, line_id, fact_xml, context=context
        )

    def process_all_invoice_phase_2_b7033(self, cursor, uid, line_id, fact_xml,
                                    context=None):
        """
        Processes an individual invoice from B70 of either type
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :param fact_xml: The atr version of the invoice we are importing
        :return:
        """
        imp_lin_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        ctx = context.copy()
        ctx['use_account_invoice'] = True
        imp_lin_obj.check_invoice_exists(
            cursor, uid, line_id, fact_xml, context=ctx
        )

    def process_atr_invoice_phase_2(self, cursor, uid, line_id, fact_xml,
                                    data_limit_pagament, distri_id,
                                    payment_order_id, context=None):
        """
        Processes an individual ATR invoice from B70
        :param payment_order_id:
        :param distri_id:
        :param data_limit_pagament:
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :param fact_xml: The atr version of invoice we are importing
        :return:
        """
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        fact_help_obj = self.pool.get('giscegas.facturacio.atr.helper')
        imp_line_fact_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.factura'
        )
        invoice_id = False
        try:
            invoice_id = fact_help_obj.import_invoice_atr(
                cursor, uid, line_id, fact_xml, data_limit_pagament, distri_id,
                payment_order_id, context=context
            )
        finally:
            if invoice_id:
                imp_line_id = imp_line_fact_obj.create(
                    cursor, uid, {
                        'linia_id': line_id,
                        'factura_id': invoice_id,
                    }, context
                )

                fact_help_obj.create_invoice_lines(
                    cursor, uid, line_id, invoice_id, fact_xml, context=context
                )

                imp_line_extra_ids = self.import_extra_lines(
                    cursor, uid, invoice_id, fact_xml, line_id, context=context
                )

                # En GAS de moment no tenim BRA
                # tipo_factura = fact_xml.clasefact
                # invoice_ref = fact_obj.read(
                #     cursor, uid, invoice_id, ['ref']
                # )['ref']
                # if tipo_factura == 'R' and invoice_ref:
                #     # If it's a RA we need to create the BRA
                #     bra_id, extra_ids = self.generate_bra_invoice(
                #         cursor, uid, fact_xml, invoice_ref[0], line_id,
                #         payment_order_id=payment_order_id, context=context
                #     )
                #     imp_line_extra_ids += extra_ids
                return imp_line_id, imp_line_extra_ids
        return None, None

    def process_atr_invoice_phase_2_b7033(self, cursor, uid, line_id, fact_xml,
                                    data_limit_pagament, distri_id,
                                    payment_order_id, context=None):
        """
        Processes an individual ATR invoice from B70
        :param payment_order_id:
        :param distri_id:
        :param data_limit_pagament:
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :param fact_xml: The atr version of invoice we are importing
        :return:
        """
        if context is None:
            context = {}
        ctx = context.copy()
        ctx['use_account_invoice'] = True
        fact_help_obj = self.pool.get('giscegas.facturacio.atr.helper')
        imp_line_fact_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.account.invoice'
        )
        invoice_id = False
        try:
            invoice_id = fact_help_obj.import_invoice_agregada(
                cursor, uid, line_id, fact_xml, data_limit_pagament, distri_id,
                payment_order_id, context=ctx
            )
        finally:
            if invoice_id:
                imp_line_id = imp_line_fact_obj.create(
                    cursor, uid, {
                        'linia_id': line_id,
                        'factura_id': invoice_id,
                    }, context
                )

                fact_help_obj.create_invoice_lines(
                    cursor, uid, line_id, invoice_id, fact_xml, context=ctx
                )
                return imp_line_id, []
        return None, None

    def process_other_invoice_phase_2(self, cursor, uid, line_id, fact_xml,
                                      data_limit_pagament, distri_id,
                                      payment_order_id, context=None):
        """
        Processes an individual Concept invoice from B70
        :param payment_order_id:
        :param distri_id:
        :param data_limit_pagament:
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :param fact_xml: The atr version of the invoice we are importing
        :return:
        """
        fact_help_obj = self.pool.get('giscegas.facturacio.atr.helper')
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        imp_line_fact_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.factura'
        )

        invoice_id = fact_help_obj.import_invoice_others(
            cursor, uid, line_id, fact_xml, data_limit_pagament, distri_id,
            payment_order_id, context=context
        )

        if invoice_id:
            imp_line_id = imp_line_fact_obj.create(
                cursor, uid, {
                    'linia_id': line_id,
                    'factura_id': invoice_id,
                }, context
            )

            fact_help_obj.create_invoice_lines(
                cursor, uid, line_id, invoice_id, fact_xml, context=context
            )

            imp_line_extra_ids = self.import_extra_lines(
                cursor, uid, invoice_id, fact_xml, line_id, context=context
            )

            # En GAS de moment no tenim BRA
            # tipo_factura = fact_xml.datos_factura.tipo_factura
            # invoice_ref = fact_obj.read(
            #     cursor, uid, invoice_id, ['ref']
            # )['ref']
            # if tipo_factura == 'R' and invoice_ref:
            #     # If it's a RA we need to create the BRA
            #     # If it's a RA we need to create the BRA
            #     bra_id, extra_ids = self.generate_bra_invoice(
            #         cursor, uid, fact_xml, invoice_ref[0], line_id,
            #         context=context
            #     )
            #     imp_line_extra_ids += extra_ids
            return imp_line_id, imp_line_extra_ids
        return None, None

    def import_extra_lines(self, cursor, uid, invoice_id, fact_xml, line_id,
                           context=None):
        if context is None:
            context = {}

        fact_help_obj = self.pool.get('giscegas.facturacio.atr.helper')
        imp_line_extra_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.extra'
        )

        line_name = self.read(cursor, uid, line_id, ['name'])['name']
        ctx = fact_help_obj.actualitzar_idioma_ctx(cursor, uid, invoice_id,
                                                   'out_invoice', context)
        extra_lines = self.get_extra_lines(
            cursor, uid, invoice_id, fact_xml, line_id, ctx
        )

        imp_line_extra_ids = []
        extra_ids = fact_help_obj.create_invoicing_extra(
            cursor, uid, invoice_id, line_name, extra_lines, context
        )
        for extra_id in extra_ids:
            imp_line_extra_ids.append(
                imp_line_extra_obj.create(
                    cursor, uid, {
                        'linia_id': line_id,
                        'extra_id': extra_id
                    }
                )
            )

        return imp_line_extra_ids

    def get_extra_lines(self, cursor, uid, invoice_id, fact_xml, line_id, context=None):
        if context is None:
            context = {}

        fact_lin_obj = self.pool.get('giscegas.facturacio.factura.linia')
        extra_lines = []
        productes_canon = self.pool.get("product.product").search(
            cursor, uid, [('default_code', 'in', ["CONG1302", "CONG1911", "CONG1912", "CONG1913"])]
        )
        other_lines_ids = fact_lin_obj.search(
            cursor, uid, [
                ('tipus', '=', 'altres'),
                ('factura_id', '=', invoice_id),
                ('product_id', 'not in', productes_canon)
            ]
        )
        for linia_fact_id in other_lines_ids:
            extra_lines.append({'line_id': linia_fact_id})

        return extra_lines

    def process_phase_3(self, cursor, uid, line_id, b70_xml, index, context=None):
        if context is None:
            context = {}

        validator_obj = self.pool.get(
            'giscegas.facturacio.atr.validator'
        )

        self.set_phase(cursor, uid, line_id, 30, context)

        error_ids, has_critical = validator_obj.validate_b70(
            cursor, uid, line_id, b70_xml, index, phase=3, context=context
        )

        if has_critical:
            raise LineProcessException('STOP Procces')

        return error_ids

    def process_phase_4(self, cursor, uid, line_id, b70_xml, index, context=None):
        if context is None:
            context = {}

        fact_help_obj = self.pool.get('giscegas.facturacio.atr.helper')
        error_obj = self.pool.get('giscegas.facturacio.atr.error')

        self.set_phase(cursor, uid, line_id, 40, context)
        if b70_xml.codproceso == '33':
            return []

        try:
            fact_help_obj.update_contract(cursor, uid, line_id, b70_xml, index, context)
            fact_help_obj.update_meters(cursor, uid, line_id, b70_xml, index, context)
            fact_help_obj.update_canons(
                cursor, uid, line_id, b70_xml, index, context=context
            )
            ene_reads = fact_help_obj.import_readings(
                cursor, uid, line_id, b70_xml, index, context
            )
        except Exception as e:
            error_obj.create_error_from_code(
                cursor, uid, line_id, '4', '999', {'text': e},
                context=context
            )
            raise LineProcessException('STOP Procces')

        return ene_reads

    def process_line_sync(self, cursor, uid, line_id, context=None):
        if context is None:
            context = {}

        if isinstance(line_id, (list, tuple)):
            line_id = line_id[0]

        state = 'valid'
        res = True
        inf = self.read(cursor, uid, line_id, ['import_phase', 'index_factura'])
        actual_phase = inf['import_phase']
        index = inf['index_factura']

        if not actual_phase:
            actual_phase = 10
        try:
            if actual_phase > 10:
                data = self.get_xml_from_adjunt(cursor, uid, line_id, context=context)
                b70_xml = B70(data, 'B70')
                b70_xml.parse_xml()
            else:
                b70_xml = self.process_phase_1(cursor, uid, line_id, index, context=context)
            if actual_phase <= 20:
                self.process_phase_2(cursor, uid, line_id, b70_xml, index, context=context)
            if actual_phase <= 30:
                self.process_phase_3(cursor, uid, line_id, b70_xml, index, context=context)
            if actual_phase <= 40:
                self.process_phase_4(cursor, uid, line_id, b70_xml, index, context=context)
        except LineProcessException as e:
            if e.message == "END Process":
                state = 'valid'
            else:
                state = 'erroni'
                res = False
        except Exception as e:
            state = 'erroni'
            res = False
            sentry = self.pool.get('sentry.setup')
            if sentry:
                sentry.client.captureException()
        finally:
            has_critical = self.update_info(
                cursor, uid, line_id, context=context
            )

            # This shouldn't be necessary but for some reason some B70 end with
            # valid state while having errors
            if has_critical:
                state = 'erroni'

            self.write(cursor, uid, [line_id], {'state': state})

        return res

    def get_xml_from_adjunt(self, cursor, uid, line_id, context=None):
        """
        Reads XML from current attachment
        :param line_id: Id of line
        :return: xml_data (as string) of attachment
        """
        att_obj = self.pool.get('ir.attachment')

        line_vals = self.read(
            cursor, uid, line_id, ['attachment_id'], context=context
        )

        attachment_id = line_vals['attachment_id'][0]

        if not attachment_id:
            return ''

        b64_data = att_obj.read(
            cursor, uid, attachment_id, ['datas'], context=context
        )['datas']

        if not b64_data:
            return ''
        return base64.b64decode(b64_data)

    def _ff_check_factures(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        for l_id in ids:
            lf = self.read(cursor, uid, l_id, ['liniafactura_id', 'linia_account_invoice_id'], context)
            existeix = False
            if len(lf.get('liniafactura_id')) > 0:
                existeix = True
            if len(lf.get('linia_account_invoice_id')) > 0:
                existeix = True
            res[l_id] = existeix
        return res

    def _ff_data_carrega(self, cursor, uid, ids, field_name, arg,
                         context=None):
        "Data de càrrega del fitxer"
        if not context:
            context = {}
        res = {}
        dates = self.perm_read(cursor, uid, ids)
        res = dict([(d['id'], d['create_date'].split('.')[0]) for d in dates])

        return res

    def _data_carrega_search(self, cursor, uid, ids, field, arg, context=None):
        """Cerca segons data creació"""
        if not context:
            context = {}
        search_vals = [('create_date', a[1], a[2]) for a in arg]
        trobats = self.search(cursor, uid, search_vals)
        return [('id', 'in', trobats)]

    def _ff_distribuidora(self, cursor, uid, ids, field_name, arg,
                          context=None):
        """Rertorna la distribuidora del fitxer"""
        if context is None:
            context = {}
        l_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        part_obj = self.pool.get('giscemisc.participant')
        res = dict([(l, False) for l in ids])
        linies_vals = l_obj.read(
            cursor, uid, ids, ['distribuidora_id'], context=context
        )
        for linia_vals in linies_vals:
            l_id = linia_vals['id']
            if linia_vals['distribuidora_id']:
                res[l_id] = part_obj.read(
                    cursor, uid, linia_vals['distribuidora_id'][0], ['codi_sifco']
                )['codi_sifco']
        return res

    def _distribuidora_search(self, cursor, uid, ids, field, arg, context=None):
        """Cerca per distribuidora"""
        if not context:
            return context
        search_vals = []
        for a in arg:
            search_vals.append(('distribuidora_id.codi_sifco', a[1], a[2]))
        i_ids = self.search(cursor, uid, search_vals)
        return [('id', 'in', i_ids)]

    def change_attachment_id(self, cursor, uid, line_id, attachment_id):
        self.write(cursor, uid, [line_id], {'attachment_id': attachment_id})
        xml_data = self.get_xml_from_adjunt(cursor, uid, line_id)
        md5_hash = get_md5(xml_data)
        vals = {'md5_hash': md5_hash}
        self.write(cursor, uid, [line_id], vals)
        return True

    def update_info(self, cursor, uid, line_id, context=None):
        """
        Updates the field info of line_id based on the errors it has
        :param cursor: Database cursor
        :param uid: User's id
        :param line_id: Id of the line we are importing
        :param context: OpenERP's context
        :return Returns true if the line has critical info
        """
        info = self.get_errors_text(
            cursor, uid, line_id, context=context
        )
        critical_info = self.get_errors_text(
            cursor, uid, line_id, only_critical=True, context=context
        )
        self.write(
            cursor, uid, line_id, {
                'info': info,
                'critical_info': critical_info,
            }, context=context
        )

        return bool(critical_info)

    def get_errors_text(self, cursor, uid, line_id, only_critical=False,
                        context=None):
        """
        Gets full errors text from line
        :param line_id:
        :return: text with error as
            [code]: message\n
            [code]: message\n
            [code]: ...\n
        """
        error_obj = self.pool.get('giscegas.facturacio.atr.error')

        critical_line_ids = error_obj.search(
            cursor, uid, [
                ('line_id', '=', line_id),
                ('level', '=', 'critical')
            ], context=context
        )

        texts = [
            error_obj.get_error_text(cursor, uid, l_id, context=context)
            for l_id in critical_line_ids
        ]

        if not only_critical:
            line_ids = error_obj.search(
                cursor, uid, [
                    ('line_id', '=', line_id),
                    ('level', '!=', 'critical')
                ], context=context
            )

            texts += [
                error_obj.get_error_text(cursor, uid, l_id, context=context)
                for l_id in line_ids
            ]

        text = '\n'.join(texts)
        return text

    _columns = {
        'name': fields.char('Fitxer', size=128),
        'state': fields.selection(_states_selection, 'Estat', size=32),
        'importacio_id': fields.many2one('giscegas.facturacio.importacio',
                                         'Importacio', required=True,
                                         select=True),
        'liniafactura_id': fields.one2many(
                              'giscegas.facturacio.importacio.linia.factura',
                              'linia_id', 'Factures', ondelete='cascade'),
        'linia_account_invoice_id': fields.one2many(
                              'giscegas.facturacio.importacio.linia.account.invoice',
                              'linia_id', 'Factures Agregades', ondelete='cascade'),
        'liniaextra_id': fields.one2many(
            'giscegas.facturacio.importacio.linia.extra',
            'linia_id', 'Línies extres'
        ),
        'critical_info': fields.text('Info Critica', readonly=True, size=4000),
        'info': fields.text('Info', readonly=True, size=4000),
        'conte_factures': fields.function(_ff_check_factures, method=True,
                                       string="Conté factures", type='boolean'),

        # B70 header fields
        'tipus_b70': fields.char("Tipo de B70", size=2, readonly=True, select=1),
        'codi_sollicitud': fields.char(
            'Codi sol·licitud', size=24, readonly=True
        ),
        'index_factura': fields.integer(
            'Indice factura importada', readonly=False,
            help=_(u"En los ficheros B7032 en que se informa de varias "
                   u"facturas, este campo indica que factura se está importando"
                   u" en esta linea de importación")
        ),
        'cups_text': fields.char('B70 CUPS', size=22, readonly=True),
        'invoice_number_text': fields.char(
            'Numero Factura Origen', size=30, readonly=True
        ),
        'ree_source_code': fields.char(
            'Origen segons B70', size=4, readonly=True
        ),
        'ree_destination_code': fields.char(
            'Destí segons B70', size=4, readonly=True
        ),
        'b70_date': fields.char(
            'Data creacio B70', size=128, readonly=True
        ),

        'data_carrega': fields.function(_ff_data_carrega, type='datetime',
                                        string="Data Carrega", method=True,
                                        fnct_search=_data_carrega_search),
        'distribuidora': fields.function(_ff_distribuidora, type='char',
                                         string="Codi Distribuïdora",
                                         fnct_search=_distribuidora_search,
                                         method=True, size=4),
        'cups_id': fields.many2one('giscegas.cups.ps', 'CUPS afectat',
                                   required=False, size=22),
        'import_phase': fields.selection(
            _IMPORT_PHASES, string="Fase de càrrega", required=True
        ),
        'distribuidora_id': fields.many2one(
            'giscemisc.participant', 'Distribuïdora', readonly=True, required=False,
            select=True
        ),
        'md5_hash': fields.char('Hash', size=32, readonly=True),
        'attachment_id': fields.many2one(
            'ir.attachment', 'Adjunt utilitzat', readonly=True, select=True,
            ondelete='cascade'
        ),
        'ignore_reference': fields.boolean(
            'Forçar creacio sense necessitar factura de referencia'
        ),
        'lectures_processades': fields.boolean(
            "Lectures Processades",
            help="Indica si les lectures del B70 s'han carregat ja al contracte",
            select=2
        ),
        'tipo_factura_b70': fields.selection(
            [('atr', 'ATR'), ('otros', 'OTROS')], string="Tipo factura B70",
            help=u'Indica si es tracte d\'una '
                 u'factura ATR o d\'altres conceptes', readonly=True
        ),
        'type_factura': fields.char(
            'Tipus de factura', size=4, help=u'Indica de quin tipus de factura '
                                             u'es tracta (N, R...)',
            readonly=True
        ),
        'fecha_factura': fields.date('Data Factura', readonly=True),
        'fecha_factura_desde': fields.date('Inici periode factura',
                                           readonly=True),
        'fecha_factura_hasta': fields.date('Fi periode factura', readonly=True),
        'importacio_lectures_ids': fields.one2many(
            'giscegas.facturacio.importacio.linia.lectures', 'linia_id',
            'Lectures Importades XML'
        )
    }

    _defaults = {
        'import_phase': lambda *a: _IMPORT_PHASES[0][0],
        'ignore_reference': lambda *a: False,
        'lectures_processades': lambda *a: False,
        'index_factura': lambda *a: 0
    }

    _order = 'create_date desc, write_date desc'

GiscegasFacturacioImportacioLinia()


class GiscegasFacturacioImportacioLiniaFactura(osv.osv):
    """Model relacional linia-factura"""

    _name = 'giscegas.facturacio.importacio.linia.factura'
    _description = 'relació entre linies d\'importació i factures'
    _inherits = {'giscegas.facturacio.factura': 'factura_id'}

    _columns = {
        'linia_id': fields.many2one('giscegas.facturacio.importacio.linia',
                                   'Linia', requiered=True, select=1),
        'factura_id': fields.many2one('giscegas.facturacio.factura',
                                 'Factura', requiered=True, select=1,ondelete="cascade"),
    }

GiscegasFacturacioImportacioLiniaFactura()


class GiscegasFacturacioImportacioLiniaAccountInvoice(osv.osv):
    """Model relacional linia-factura"""

    _name = 'giscegas.facturacio.importacio.linia.account.invoice'
    _description = 'relació entre linies d\'importació i factures'
    _inherits = {'account.invoice': 'factura_id'}

    _columns = {
        'linia_id': fields.many2one('giscegas.facturacio.importacio.linia',
                                   'Linia', requiered=True, select=1),
        'factura_id': fields.many2one('account.invoice',
                                 'Factura Agregada', requiered=True, select=1,ondelete="cascade"),
    }

GiscegasFacturacioImportacioLiniaAccountInvoice()

class GiscegasFacturacioAtrConfig(osv.osv):
    """Configuració pels fitxers B70 segons l'emissor.
    """
    _name = 'giscegas.facturacio.atr.config'
    _rec_name = 'emissor'

    def get_emissor(self, cursor, uid, code, context=None):
        """
        Gets origin partner from ree_code in atr_gas_file. It gets partner_id
        with ree_code. If partner found has a parent partner, it returns it as
        origin partner.
        :param ree_code:
        :return: origin DSO partner id
        """
        partner_obj = self.pool.get('res.partner')
        part_obj = self.pool.get('giscemisc.participant')
        pdistribuidora_id = part_obj.get_id_from_sifco(cursor, uid, code)

        if not pdistribuidora_id:
            raise message.except_b70(
                'PartnerNotFound',
                _("No s'ha trobat cap empresa amb codi {0}").format(code)
            )
        partner_id = part_obj.get_partner_id(cursor, uid, pdistribuidora_id)
        parent_id = partner_obj.read(
            cursor, uid, partner_id, ['parent_id']
        )['parent_id']

        if parent_id:
            # Nomes fem el get_id_from_partner per asegurarnos que te un
            # participant creat
            return part_obj.get_id_from_partner(cursor, uid, parent_id[0])
        else:
            return pdistribuidora_id

    def default_pressio(self, cursor, uid, context=None):
        """Retorna la unitat per defecte del lloguer.
        """
        uom_obj = self.pool.get('product.uom')
        uom_id = uom_obj.search(
            cursor, uid,
            [('category_id.name', '=', 'Presión GAS'), ('factor', '=', 1)],
            limit=1
        )
        if uom_id:
            return uom_id[0]
        return False

    _columns = {
        'emissor': fields.many2one('giscemisc.participant', 'Emissor', required=True),
        'uom_pressio_f': fields.many2one('product.uom',
            u'Unitat de presión orígen', required=True,
            domain=[('category_id.name', '=', 'Presión GAS')]),
        'uom_pressio_t': fields.many2one('product.uom',
            u'Unitat de presión destino', required=True,
            domain=[('category_id.name', '=', 'Presión GAS')]),
        # payment order info
        'create_payment_order': fields.boolean(
            u'Crea la Remesa de pagament',
            help=u"Crear una remesa de pagament amb el mateix nom que la "
                 u"que s'indica en el B70 i hi afegeix les factures generades"),
        'payment_mode_id': fields.many2one(
            'payment.mode', string=u'Mode de pagament',
            help=u"Mode de pagament per defecte de la remesa que es crearà"),
        'b7032_generates_invoice': fields.boolean('B7032 Generan factura fiscal'),
        'b7032_same_num_generates_invoice': fields.boolean('B7032 Sin numero de factura real generan factura'),
    }

    def get_config(self, cursor, uid, emissor, context=None):
        """Retorna un diccionari amb els valors de la config.
        """
        config_ids = self.search(cursor, uid, [('emissor.codi_sifco', '=', emissor)],
                                 context={'active_test': False})
        if not config_ids:
            # Si no troba la configuració genera una excepció
            raise osv.except_osv('Error',
                                 _(u"No s'ha trobat la configuració de "
                                 "atr_gas per l'emisor: %s") % emissor)
        else:
            config = self.read(cursor, uid, config_ids[0])
            del config['emissor']
            del config['id']
            # Convertim els many2one (id, Name)
            non_tuple_fields = ['create_payment_order', 'b7032_generates_invoice', 'b7032_same_num_generates_invoice']
            for key in config:
                if key not in non_tuple_fields and config[key]:
                    config[key] = config[key][0]
        return config

    _defaults = {
        'uom_pressio_f': default_pressio,
        'uom_pressio_t': default_pressio,
        'create_payment_order': lambda *a: False,
    }

    _sql_constraints = [('emissor_uniq', 'unique (emissor)',
                         _(u'Ja existeix una configuració entrada per '
                           u'aquest emissor'))]

GiscegasFacturacioAtrConfig()


class GiscegasFacturacioImportacioLiniaLecturaenergia(osv.osv):
    """Model relacional linia-lecturafactura per línies divergents"""

    _name = 'giscegas.facturacio.importacio.linia.lecturafactura'
    _description = 'relació entre linies d\'importació i lectures d\'energia'

    _columns = {
        'linia_id': fields.many2one('giscegas.facturacio.importacio.linia',
                                   'Linia', requiered=True, ondelete="cascade"),
        'lectura_id': fields.many2one('giscegas.lectures.lectura.pool',
                                 'Lectura', requiered=True, ondelete="cascade"),
        'factura_id': fields.many2one('giscegas.facturacio.factura',
                                 'Factura', requiered=True, ondelete="cascade"),
    }

GiscegasFacturacioImportacioLiniaLecturaenergia()


class GiscegasFacturacioImportacioLiniaExtra(osv.osv):
    """Model relacional linia-facturacioextra"""

    _name = 'giscegas.facturacio.importacio.linia.extra'
    _description = 'relació entre linies d\'importació'
    _inherits = {'giscegas.facturacio.extra': 'extra_id'}

    _columns = {
        'linia_id': fields.many2one('giscegas.facturacio.importacio.linia',
                                   'Linia', requiered=True, ondelete="cascade"),
        'extra_id': fields.many2one('giscegas.facturacio.extra',
                                    'Extra', requiered=True, ondelete="cascade"),

    }

GiscegasFacturacioImportacioLiniaExtra()


class GiscegasFacturacioImportacioLiniaLectures(osv.osv):
    _name = 'giscegas.facturacio.importacio.linia.lectures'
    _description = 'Relacio entre linies (B70) i lectures'

    _columns = {
        'linia_id': fields.many2one('giscegas.facturacio.importacio.linia',
                                    'Linia', requiered=True,
                                    ondelete="cascade"),
        'lectura_desde': fields.float('Lectura Inicial', readonly=True),
        'lectura_actual': fields.float('Lectura Actual', readonly=True),
        'fecha_desde': fields.date('Data Lectura Inicial', readonly=True),
        'fecha_actual': fields.date('Data Lectura Actual', readonly=True),
        'comptador': fields.char('Nº de sèrie', size=32, readonly=True),
        'origen_desde': fields.char('Origen Inicial', size=2, readonly=True),
        'origen_actual': fields.char('Origen Actual', size=2, readonly=True),
        'periode': fields.char('Període', size=16, readonly=True),
        'ajust': fields.float('Ajust Integrador', readonly=True),
        'motiu': fields.char('Motiu ajust', size=64, readonly=True),
        'tipo_lect_num': fields.char('Tipo de Lectura de Numerador', size=2),
        'factor_k': fields.float('Factor k', readonly=True, digits=(16, 6)),
        'pcs': fields.float('PCS', readonly=True, digits=(16, 6)),

    }
    _order = "comptador,periode"
GiscegasFacturacioImportacioLiniaLectures()