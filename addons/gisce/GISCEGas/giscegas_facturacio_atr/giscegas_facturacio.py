# -*- coding: utf-8 -*-
from __future__ import absolute_import
from tools import config
from tools.translate import _
from osv import osv, orm, fields
from datetime import datetime, timedelta
from gestionatr.input.messages import message_gas as message
from gestionatr.defs_gas import REFUND_RECTIFICATIVE_TYPES, REQUIRE_REFERENCE_TYPES, SIGN
from calendar import isleap
from gestionatr.input.messages.B70 import agrupar_lectures_per_data, obtenir_data_inici_i_final
from giscegas_facturacio.defs import TIPO_RECTIFICADORA_SELECTION, SIGN
import netsvc


class GiscegasFacturacioLot(osv.osv):
    """Funció d'ajuda per localitzar el lot"""

    _name = 'giscegas.facturacio.lot'
    _inherit = 'giscegas.facturacio.lot'

    def lot_per_data(self, cursor, uid, data, context=None):
        """Buscar el lot de facturació tal que contingui la data"""
        search_params = [('data_inici', '<=', data),
                         ('data_final', '>=', data)]
        lot = self.search(cursor, uid, search_params)
        if not lot:
            raise osv.except_osv('Error', _("No s'ha trobat el lot"))
        return lot[0]

GiscegasFacturacioLot()


class GiscegasFacturacioAtrHelper(osv.osv):
    """Funcions d'ajuda atr_gas B70"""

    _name = 'giscegas.facturacio.atr.helper'

    def get_payment_order(self, cursor, uid, emissor, b70_data, context=None):
        """Returns a payment order id of name id_remesa.
        if not found it is created"""
        b70config_obj = self.pool.get('giscegas.facturacio.atr.config')
        payorder_obj = self.pool.get('payment.order')

        b70_config = b70config_obj.get_config(cursor, uid, emissor)
        if not b70_config.get('create_payment_order', False):
            return None

        payment_mode = b70_config.get('payment_mode_id', False)
        if not payment_mode:
            raise osv.except_osv(
                'Error',
                _(u"No s'ha configurat el mode de pagament per crear la "
                  u"remesa a l'empresa {0}").format(emissor))

        payment_order_name = '{0}-{1}'.format(emissor, b70_data['id_remesa'])
        search_vals = [('reference', '=', payment_order_name),
                       ('type', '=', 'payable')]

        payorder_ids = payorder_obj.search(cursor, uid, search_vals,
                                           context=context)

        if payorder_ids:
            return payorder_ids[0]
        else:
            vals_pm = {
                'reference': payment_order_name,
                'date_prefered': 'fixed',
                'date_planned': b70_data.get('fecha_valor_remesa'),
                'user_id': uid,
                'state': 'draft',
                'mode': payment_mode,
                'type': 'payable',
                'create_account_moves': 'direct-payment',
            }
            payment_order_id = payorder_obj.create(
                cursor, uid, vals_pm, context
            )
        return payment_order_id

    def import_invoice_atr(self, cursor, uid, file_id, fact,
                           data_limit_pagament, partner_id, payment_order_id,
                           context=None):
        if context is None:
            context = {}

        return self.create_invoice_atr(
            cursor, uid, file_id, fact, data_limit_pagament, partner_id,
            payment_order_id, context=context
        )

    def import_invoice_others(self, cursor, uid, file_id, fact,
                              data_limit_pagament, partner_id, payment_order_id,
                              context=None):
        if context is None:
            context = {}
        return self.create_invoice_others(
            cursor, uid, file_id, fact, data_limit_pagament, partner_id,
            payment_order_id, context=context
        )

    def import_invoice_agregada(self, cursor, uid, file_id, fact,
                                data_limit_pagament, partner_id,
                                payment_order_id, context=None):
        if context is None:
            context = {}

        return self.create_invoice_agregada(
            cursor, uid, file_id, fact, data_limit_pagament, partner_id,
            payment_order_id, context=context
        )

    def create_invoice_agregada(self, cursor, uid, file_id, fact,
                                data_limit_pagament, partner_id,
                                payment_order_id, context=None):
        """
        Creates an invoice for atributes
        :param file_id: The id of the giscegas.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact: Switching's version of the xml invoice
        :param data_limit_pagament: Data limit de pagament especificada a l'xml
        :param partner_id: Id of the sender of the B70 file
        :param payment_order_id: Id of the payment order depending on the sender
        and the date
        :param context: OpenERP context
        :return: Returns the id of the created invoice or None if we didn't
        create it
        """
        if context is None:
            context = {}

        fact_obj = self.pool.get('account.invoice')

        vals = self.create_vals_invoice_agregada(
            cursor, uid, fact, file_id, data_limit_pagament, partner_id,
            'CGAS', payment_order_id, context=context
        )

        data_inici, data_final = fact.get_periode_factura()

        vals.update({
            'data_inici': data_inici,
            'data_final': data_final,
            'date_boe': len(fact.listaboe) and fact.listaboe[0].fecboe,
        })

        fact_data = datetime.strptime(
            fact.fecfactura, '%Y-%m-%d'
        )
        invoice_already_exists = fact_obj.get_in_invoice_by_origin(
            cursor, uid, fact.get_origin(), partner_id, year=fact_data.year,
            context=context
        )
        # If the invoice already exists we don't actually want to create it
        # (we have done everything up to now to create the necessary errors)
        if invoice_already_exists:
            return None

        fact_id = fact_obj.create(cursor, uid, vals, context=context)

        return fact_id

    def create_invoice_atr(self, cursor, uid, file_id, fact,
                           data_limit_pagament, partner_id, payment_order_id,
                           context=None):
        """
        Creates an invoice for atributes
        :param file_id: The id of the giscegas.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact: Switching's version of the xml invoice
        :param data_limit_pagament: Data limit de pagament especificada a l'xml
        :param partner_id: Id of the sender of the B70 file
        :param payment_order_id: Id of the payment order depending on the sender
        and the date
        :param context: OpenERP context
        :return: Returns the id of the created invoice or None if we didn't
        create it
        """
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscegas.facturacio.factura')

        vals = self.create_invoice_general(
            cursor, uid, fact, file_id, data_limit_pagament, partner_id,
            'CGAS', payment_order_id, context=context
        )

        data_inici, data_final = fact.get_periode_factura()

        vals.update({
            'data_inici': data_inici,
            'data_final': data_final,
            'date_boe': len(fact.listaboe) and fact.listaboe[0].fecboe,
        })

        fact_data = datetime.strptime(
            fact.fecfactura, '%Y-%m-%d'
        )
        invoice_already_exists = fact_obj.get_in_invoice_by_origin(
            cursor, uid, fact.get_origin(), partner_id, year=fact_data.year,
            context=context
        )
        # If the invoice already exists we don't actually want to create it
        # (we have done everything up to now to create the necessary errors)
        if invoice_already_exists:
            return None

        fact_id = fact_obj.create(cursor, uid, vals, context=context)

        return fact_id

    def create_invoice_others(self, cursor, uid, file_id, fact,
                           data_limit_pagament, partner_id, payment_order_id,
                           context=None):
        """
        Creates an invoice for atributes
        :param file_id: The id of the giscegas.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact: Switching's version of the xml invoice
        :param data_limit_pagament: Data limit de pagament especificada a l'xml
        :param partner_id: Id of the sender of the B70 file
        :param payment_order_id: Id of the payment order depending on the sender
        and the date
        :param context: OpenERP context
        :return: Returns the id of the created invoice or None if we didn't
        create it
        """
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscegas.facturacio.factura')

        vals = self.create_invoice_general(
            cursor, uid, fact, file_id, data_limit_pagament, partner_id,
            'CCONCEPTOS_GAS', payment_order_id, context=context
        )

        data_inici, data_final = fact.get_periode_factura()

        vals.update({
            'data_inici': data_inici,
            'data_final': data_final,
            'date_boe': len(fact.listaboe) and fact.listaboe[0].fecboe or fact.fecfactura,
        })

        fact_data = datetime.strptime(
            fact.fecfactura, '%Y-%m-%d'
        )
        invoice_already_exists = fact_obj.get_in_invoice_by_origin(
            cursor, uid, fact.get_origin(), partner_id, year=fact_data.year,
            context=context
        )
        # If the invoice already exists we don't actually want to create it
        # (we have done everything up to now to create the necessary errors)
        if invoice_already_exists:
            return None

        fact_id = fact_obj.create(cursor, uid, vals, context=context)

        return fact_id
    
    def create_invoice_general(self, cursor, uid, fact, file_id,
                               data_limit_pagament, partner_id, journal_code,
                               payment_order_id, context=None):
        """
        This function is used to create the values common in both a ATR and an
         Others invoice
        :param file_id: The id of the giscegas.facturacio.importacio.linia
        :param journal_code: Code of the account.journal we are using
        :param cursor: Database cursor
        :param uid: User's id
        :param fact: Switching's version of the xml invoice
        :param data_limit_pagament: Data limit de pagament especificada a l'xml
        :param partner_id: Id of the sender of the B70 file
        :param payment_order_id: Id of the payment order depending on the sender
        and the date
        :param context: OpenERP context
        :return: Returns the the values of the general invoice.
        """
        if context is None:
            context = {}
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        line_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        lect_help_obj = self.pool.get('giscegas.lectures.atr.helper')
        journal_obj = self.pool.get('account.journal')

        line_vals = line_obj.read(
            cursor, uid, file_id,
            ['cups_id', 'cups_text', 'importacio_id', 'ignore_reference']
        )
        cups_id = line_vals['cups_id'][0]

        data_inici, data_final = fact.get_periode_factura()
        if not data_inici:
            data_inici = fact.fecfactura
        if not data_final:
            data_final = fact.fecfactura

        try:
            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, cups_id, data_inici, data_final
            )
        except osv.except_osv as e:
            if not fact.is_only_conceptes():
                raise message.except_b70(
                    'ContractNotFound', e.message,
                    {
                        'cups': line_vals['cups_text'],
                        'start_date': data_inici,
                        'end_date': data_final
                    }
                )
            else:
                # We will try to read polissa 1 day after given data_inici
                # because for some reason some distris send this kind of
                # invoices 1 day before activation
                data_inici = datetime.strptime(data_inici,  "%Y-%m-%d") + timedelta(days=1)
                data_inici = data_inici.strftime("%Y-%m-%d")
                try:
                    polissa_id = lect_help_obj.find_polissa_with_cups_id(
                        cursor, uid, cups_id, data_inici, data_final
                    )
                except osv.except_osv as e:
                    raise message.except_b70(
                        'ContractNotFound', e.message,
                        {
                            'cups': line_vals['cups_text'],
                            'start_date': data_inici,
                            'end_date': data_final
                        }
                    )

        if fact.clasefact not in REFUND_RECTIFICATIVE_TYPES:
            invoice_type = 'in_invoice'
        else:
            invoice_type = 'in_refund'
        context['type_invoice'] = invoice_type

        if fact.clasefact != 'N':
            journal_code += '.%s' % fact.clasefact
        journal_id = journal_obj.search(
            cursor, uid, [('code', '=', journal_code)], limit=1
        )
        if not journal_id:
            raise message.except_b70('JournalNotFound', journal_code)

        vals = fact_obj.onchange_polissa(
            cursor, uid, [], polissa_id, invoice_type, context=context
        )['value']
        vals.update(fact.get_create_invoice_params())
        if context.get('tipo_factura'):
            vals.update({'tipo_factura': context.get('tipo_factura')})

        vals['saldo'] = fact.saldo_total_a_cobrar

        # We must use the tarif from the B70, not the tarif from the contract
        if fact.tipopeaje:
            tarifa_obj = self.pool.get('giscegas.polissa.tarifa')
            tarifa_atr = fact.tipopeaje
            tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
                cursor, uid, tarifa_atr, context=context
            )
            if tarifa_id:
                vals.update({'tarifa_acces_id': tarifa_id})

        vals.update({
            'polissa_id': polissa_id,
            'type': invoice_type,
            'data_due': data_limit_pagament,
            'importacio_id': line_vals['importacio_id'][0],
            'partner_id': partner_id,
            'journal_id': journal_id[0],
        })
        try:
            ref = self.get_factura_referencia(
                cursor, uid, fact, journal_code, partner_id, context
            )

            vals.update({
                'ref': ref,
            })
        except osv.except_osv:
            fact_ref = fact.numfactorigen
            if line_vals['ignore_reference']:
                # If ignore_reference is set to true we allow to continue the
                # import but we give a message saying we didn't find the
                # reference invoice
                err_vals = {'fact_ref': fact_ref}
                error_obj.create_error_from_code(
                    cursor, uid, file_id, '2', '012', err_vals, context=context
                )
            else:
                # If ignore_reference is not set to true we raise an error
                # (this is what we do by default)
                raise message.except_b70('ReferenceInvoiceNotFound', fact_ref)

        if payment_order_id:
            vals.update({'payment_order_id': payment_order_id})

        return vals

    def create_vals_invoice_agregada(self, cursor, uid, fact, file_id,
                                     data_limit_pagament, partner_id,
                                     journal_code, payment_order_id, context=None):
        if context is None:
            context = {}
        fact_obj = self.pool.get('account.invoice')
        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        line_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        journal_obj = self.pool.get('account.journal')

        line_vals = line_obj.read(
            cursor, uid, file_id,
            ['importacio_id', 'ignore_reference']
        )

        if fact.indfacturarect not in REFUND_RECTIFICATIVE_TYPES:
            invoice_type = 'in_invoice'
        else:
            invoice_type = 'in_refund'
        context['type_invoice'] = invoice_type

        if fact.indfacturarect and fact.indfacturarect != 'N':
            journal_code += '.%s' % fact.indfacturarect
        journal_id = journal_obj.search(
            cursor, uid, [('code', '=', journal_code)], limit=1
        )
        if not journal_id:
            raise message.except_b70('JournalNotFound', journal_code)

        vals = fact_obj.onchange_partner_id(cursor, uid, [], 'in_invoice',
                                            partner_id, fact.fecfactura, False,
                                            False)['value']
        vals.update(fact.get_create_invoice_params())

        vals['saldo'] = fact.saldo_total_a_cobrar

        # We must use the tarif from the B70, not the tarif from the contract
        if fact.tipopeaje:
            tarifa_obj = self.pool.get('giscegas.polissa.tarifa')
            tarifa_atr = fact.tipopeaje
            tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
                cursor, uid, tarifa_atr, context=context
            )
            if tarifa_id:
                vals.update({'tarifa_acces_id': tarifa_id})

        vals.update({
            'type': invoice_type,
            'data_due': data_limit_pagament,
            'importacio_id': line_vals['importacio_id'][0],
            'partner_id': partner_id,
            'journal_id': journal_id[0],
        })
        try:
            ref = self.get_factura_referencia(
                cursor, uid, fact, journal_code, partner_id, context
            )

            vals.update({
                'rectifying_id': ref,
            })
        except osv.except_osv:
            fact_ref = fact.numfactorigen
            if line_vals['ignore_reference']:
                # If ignore_reference is set to true we allow to continue the
                # import but we give a message saying we didn't find the
                # reference invoice
                err_vals = {'fact_ref': fact_ref}
                error_obj.create_error_from_code(
                    cursor, uid, file_id, '2', '012', err_vals, context=context
                )
            else:
                # If ignore_reference is not set to true we raise an error
                # (this is what we do by default)
                raise message.except_b70('ReferenceInvoiceNotFound', fact_ref)

        if payment_order_id:
            vals.update({'payment_order_id': payment_order_id})

        return vals

    def get_factura_referencia(self, cursor, uid, fact_xml, journal_code,
                               partner_id, context=None):
        """Retorna l'id de la factura que anul·la o rectifica aquesta
        factura.
        """
        if context is None:
            context = {}
        # Agafem el journal_code base, això vol dir la part esquerra del punt
        # ja que separem els d'anul·ladures/rectificadores amb JOURNAL_CODE.R
        journal_code = journal_code.split('.')[0]

        if fact_xml.clasefact not in REQUIRE_REFERENCE_TYPES:
            return False
        if not fact_xml.numfactorigen:
            raise osv.except_osv(
                _(u"Error"),
                _(u"No se informa de ningun numero de factura.")
            )
        if context.get("use_account_invoice"):
            f_obj = self.pool.get("account.invoice")
        else:
            f_obj = self.pool.get('giscegas.facturacio.factura')
        journal_obj = self.pool.get('account.journal')

        journal_ids = journal_obj.search(
            cursor, uid, [('code', 'ilike', journal_code)]
        )

        fid = f_obj.search(cursor, uid, [
            ('origin', '=', fact_xml.numfactorigen),
            ('journal_id', 'in', journal_ids),
            ('partner_id', '=', partner_id)
        ])
        if not (fid and fact_xml.numfactorigen):
            fid = f_obj.search(cursor, uid, [
                ('origin', '=like', "% - "+fact_xml.numfactorigen),
                ('journal_id', 'in', journal_ids)
            ])
            if not (fid and fact_xml.numfactorigen):
                raise osv.except_osv(
                    _(u"Error"),
                    _(u"No s'ha trobat la factura de referència amb número %s") % (
                        fact_xml.numfactorigen
                    )
                )
        elif fid and len(fid) > 1:
            raise osv.except_osv(
                _(u"Error"),
                _(u"Se ha encontrado mas de una factura de referencia con numero %s") % (
                    fact_xml.numfactorigen
                )
            )
        return fid[0]

    def actualitzar_idioma_ctx(self, cursor, uid, factura_id, type_invoice, context):
        """Actulitza la clau 'lang' del context amb l'idioma del partner
           associat a la factura"""
        _factura = self.pool.get('giscegas.facturacio.factura')
        fact = _factura.browse(cursor, uid, factura_id, context)
        if type_invoice == 'in_invoice':
            lang = fact.partner_id.lang
        else:
            lang = fact.polissa_id.pagador.lang
        ctx = context.copy()
        ctx.update({'lang': lang})
        return ctx
    
    def create_invoicing_extra(self, cursor, uid, fact_id, fact_name, ori_lines,
                               context=None):
        """Crear una nova entrada a giscegas.facturacio.extra especificant els
                   periodes entre els quals hi ha linies de refacturació"""
        extra_obj = self.pool.get('giscegas.facturacio.extra')

        extra_ids = []
        values = self.get_values_in_ext(
            cursor, uid, fact_id, fact_name, ori_lines, context
        )
        for vals in values:
            extra_ids.append(
                extra_obj.create(cursor, uid, vals, context=context)
            )
        return extra_ids
    
    def get_values_in_ext(self, cursor, uid, fact_id, fact_name, ori_lines,
                               context=None):
        if not context:
            context = {}
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        product_obj = self.pool.get('product.product')
        journal_obj = self.pool.get('account.journal')

        journal_id = journal_obj.search(
            cursor, uid, [('code', '=', 'GAS')], limit=1
        )
        if not journal_id:
            raise message.except_b70('JournalNotFound', 'GAS')

        fact_vals = fact_obj.read(
            cursor, uid, fact_id, ['tipo_rectificadora', 'polissa_id']
        )
        ctx = self.actualitzar_idioma_ctx(
            cursor, uid, fact_id, 'out_invoice', context
        )

        read_params = [
            'product_id',
            'name',
            'price_unit',
            'data_desde',
            'data_fins',
            'quantity',
            'uos_id',
            'invoice_line_tax_id',
        ]

        # El text següent ha de traduïr-se segons l'idioma del partner i no
        # de l'usuari de l'erp
        extra_ids = []
        values = []
        for ori_line in ori_lines:
            linia = linia_obj.read(
                cursor, uid, ori_line['line_id'], read_params, context=ctx
            )

            product_id = None

            if 'force_product' in ori_line:
                product_id = ori_line['force_product']
            elif linia.get('product_id', False):
                product_id = linia['product_id'][0]
            product_fields_read = [
                'description', 'description_sale', 'code', 'taxes_id'
            ]

            product_vals = product_obj.read(
                cursor, uid, product_id, product_fields_read, context=ctx
            )
            extra_name = (
                product_vals['description_sale'] or product_vals['description']
            )

            if ori_line.get('force_name', False):
                extra_name = ori_line['force_name']

            price_unit = linia['price_unit']
            sign_value = SIGN[fact_vals['tipo_rectificadora']]
            price_unit = price_unit * sign_value

            if ori_line.get('force_price', False):
                price_unit = ori_line['force_price']

            notes = self.crear_notes_extra(
                cursor, uid, fact_id, fact_name, context
            )

            vals = {
                'polissa_id': fact_vals['polissa_id'][0],
                'date_from': linia['data_desde'],
                'date_to': linia['data_fins'],
                'date_line_from': linia['data_desde'],
                'date_line_to': linia['data_fins'],
                'name': extra_name,
                'product_id': product_id,
                'quantity': linia['quantity'],
                'uos_id': linia['uos_id'][0],
                'price_unit': price_unit,
                'term': 1,
                'journal_ids': [(6, 0, journal_id)],
                'notes': notes,
            }

            # Agafem els impostos segons el producte
            vals['tax_ids'] = [(6, 0, product_vals['taxes_id'])]
            values.append(vals)

        return values
    
    def crear_notes_extra(self, cursor, uid, f_id, fname, context=None):
        """Generar la descripció per a les notes de la línia extra"""
        if not context:
            context = {}
        _fact = self.pool.get('giscegas.facturacio.factura')
        notes = _(u"Creada desde la factura de proveïdor amb id: %d\n"
                  u"Fitxer XML: %s") % (f_id, fname)
        return notes

    def create_invoice_lines(self, cursor, uid, file_id, fact_id, fact_xml,
                             context=None):
        """
        Creates the lines of an ATR invoice
        :param file_id: The id of the giscegas.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscegas.facturacio.factura)
        :param fact_xml: The atr's version of the xml's invoice of the B70
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the ids of the created invoice lines
        """
        if context is None:
            context = {}
        if context.get("use_account_invoice"):
            fact_obj = self.pool.get('account.invoice')
        else:
            fact_obj = self.pool.get('giscegas.facturacio.factura')

        error_obj = self.pool.get('giscegas.facturacio.atr.error')

        lines_by_type = fact_xml.get_linies_factura_by_type()
        line_ids = []
        for line_type, lines in lines_by_type.items():
            line_id = self.create_invoice_line_by_type(
                cursor, uid, line_type, file_id, fact_xml, fact_id, lines,
                context
            )
            if line_id:
                line_ids += line_id
        fact_obj.button_reset_taxes(cursor, uid, [fact_id], context)
        val_lectures = self.get_total_factura(cursor, uid, fact_id, context)
        rounded_invoice_total = round(fact_xml.importetotal, 2)
        rounded_readings_total = round(val_lectures, 2)
        if abs(rounded_invoice_total) != abs(rounded_readings_total):
            error_values = {
                'total_importacio': rounded_readings_total,
                'total_xml': rounded_invoice_total
            }
            error_obj.create_error_from_code(
                cursor, uid, file_id, '2', '002', error_values,
                context=context
            )

        return line_ids

    def get_total_factura(self, cursor, uid, fid, context=None):
        """Retorna el total de la factura"""
        if context.get("use_account_invoice"):
            fact = self.pool.get('account.invoice')
        else:
            fact = self.pool.get('giscegas.facturacio.factura')
        total = fact.read(cursor, uid, fid, ['amount_total'], context)
        return total['amount_total']

    def create_invoice_line_by_type(self, cursor, uid, tipus, file_id, fact_xml,
                                    fact_id, line_xml, context=None):
        """
        Creates a line of an invoice depending on the type of line it is
        :param tipus: Tipus de linia a crear
        :param line_xml: The atr's version of the line we are creating
        :param file_id: The id of the giscegas.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscegas.facturacio.factura)
        :param fact_xml: The atr's version of the xml's invoice of the B70
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}
        use_account_invoice = context.get("use_account_invoice")
        if tipus == 'lloguer':
            if use_account_invoice:
                return self.create_subtotal_line_account_invoice(
                    cursor, uid, 'ren', fact_xml, line_xml, fact_id,
                    context=context
                )
            else:
                return self.create_invoice_line_rent(
                    cursor, uid, fact_xml, fact_id, line_xml, context=context
                )
        elif tipus == 'tvariable':
            if use_account_invoice:
                return self.create_subtotal_line_account_invoice(
                    cursor, uid, 'tvr', fact_xml, line_xml, fact_id,
                    context=context
                )
            else:
                return self.create_invoice_line_tvariable(
                    cursor, uid, file_id, fact_xml, fact_id, line_xml,
                    context=context
                )
        elif tipus == 'tfixe':
            if use_account_invoice:
                return self.create_subtotal_line_account_invoice(
                    cursor, uid, 'tfx', fact_xml, line_xml, fact_id,
                    context=context
                )
            else:
                return self.create_invoice_line_tfixe(
                    cursor, uid, file_id, fact_xml, fact_id, line_xml,
                    context=context
                )
        elif tipus == 'altres':
            if use_account_invoice:
                return self.create_subtotal_line_account_invoice(
                    cursor, uid, 'alt', fact_xml, line_xml, fact_id,
                    context=context
                )
            else:
                return self.create_invoice_line_others(
                    cursor, uid, fact_xml, fact_id, line_xml, context=context
                )

    def create_invoice_line_rent(self, cursor, uid, fact_xml, fact_id, line_xml,
                                 context=None):
        """
        Creates a line of an invoice with type rent
        :param line_xml: The atr's version of the line we are creating
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscegas.facturacio.factura)
        :param fact_xml: The atr's version of the xml's invoice of the B70
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscegas.facturacio.facturador')
        imd_obj = self.pool.get('ir.model.data')

        date = fact_xml.get_periode_factura()[0]
        if isleap(int(date.split('-')[0])):
            uom_alq_dia = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio',
                'uom_aql_gas_dia_traspas'
            )[1]
        else:
            uom_alq_dia = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'uom_aql_gas_dia'
            )[1]

        data = line_xml.get('lines', None)
        if not data:
            return None

        line_ids = []
        for llog in data:
            vals = {
                'data_desde': llog.fecdesde,
                'data_fins': llog.fechasta,
                'force_price': llog.precunidad or llog.importe/llog.unidad,
                'uos_id': uom_alq_dia,
                'quantity': llog.unidad,
                'tipus': 'lloguer',
                'name': _('Lloguer equip de mesura')
            }

            if llog.precunidad != 0 and llog.unidad != 0:
                line_id = facturador_obj.crear_linies_lloguer(
                    cursor, uid, fact_id, vals, context
                )
                if llog.impuestoconcepto == 'N':
                    linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
                    linia_obj.write(cursor, uid, line_id, {'invoice_line_tax_id': [(6, False, [])]})

                line_ids.append(line_id)

        if line_ids:
            line_ids.append(
                self.create_subtotal_line(
                    cursor, uid, 'ren', fact_xml, line_xml, fact_id, line_ids
                )
            )

        return line_ids

    def create_invoice_line_tvariable(self, cursor, uid, file_id, fact_xml,
                                      fact_id, line_xml, context=None):
        """
        Creates a line of an invoice with type energy
        :param line_xml: The atr's version of the line we are creating
        :param file_id: The id of the giscegas.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscegas.facturacio.factura)
        :param fact_xml: The atr's version of the xml's invoice of the B70
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscegas.facturacio.facturador')
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        tarifa_obj = self.pool.get('giscegas.polissa.tarifa')
        uom_obj = self.pool.get('product.uom')
        imd_obj = self.pool.get('ir.model.data')

        uom_kwatt_hour = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'uom_eneg_gas'
        )[1]

        tipus = 'variable'
        data = line_xml.get('lines', None)
        if not data:
            return None

        line_ids = set()

        tarifa_atr = fact_xml.tipopeaje
        tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
            cursor, uid, tarifa_atr
        )
        productes = tarifa_obj.get_periodes_producte(
            cursor, uid, tarifa_id, 'te', context
        )

        ctx = context.copy()
        for concepte in data:
            vals = self._get_vals_for_termes_line(
                concepte, productes, fact_xml
            )
            vals.update({
                'tipus': tipus,
                'force_price': concepte.precunidad or concepte.importe/concepte.unidad,
                'quantity': concepte.unidad,
                'uos_id': uom_kwatt_hour
            })

            line_id = facturador_obj.crear_linia(
                cursor, uid, fact_id, vals, ctx
            )
            if concepte.impuestoconcepto == 'N':
                linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
                linia_obj.write(cursor, uid, line_id, {'invoice_line_tax_id': [(6, False, [])]})
            line_ids.add(line_id)

        line_ids = list(set(line_ids))
        self.write_lines_summary(
            cursor, uid, file_id, tipus, line_xml, fact_id, line_ids
        )

        line_ids.append(
            self.create_subtotal_line(
                cursor, uid, 'tvr', fact_xml, line_xml, fact_id, line_ids
            )
        )

        return line_ids

    def create_invoice_line_tfixe(self, cursor, uid, file_id, fact_xml, fact_id,
                                  line_xml, context=None):
        """
        Creates a line of an invoice with type power
        :param line_xml: The atr's version of the line we are creating
        :param file_id: The id of the giscegas.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscegas.facturacio.factura)
        :param fact_xml: The atr's version of the xml's invoice of the B70
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscegas.facturacio.facturador')
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        b70config_obj = self.pool.get('giscegas.facturacio.atr.config')
        tarifa_obj = self.pool.get('giscegas.polissa.tarifa')
        partner_obj = self.pool.get('res.partner')
        part_obj = self.pool.get('giscemisc.participant')
        uom_obj = self.pool.get('product.uom')
        imd_obj = self.pool.get('ir.model.data')

        factura_vals = factura_obj.read(
            cursor, uid, fact_id, ['partner_id', 'date_invoice']
        )
        emissor = part_obj.get_id_from_partner(cursor, uid, factura_vals['partner_id'][0])
        emissor = part_obj.read(cursor, uid, emissor, ['codi_sifco'])['codi_sifco']
        try:
            b70config = b70config_obj.get_config(cursor, uid, emissor)
        except osv.except_osv as e:
            raise message.except_b70(
                'SwitchingConfigNotFound',
                'Switching configuration not found for {0}'.format(emissor),
                {'emissor': emissor}
            )
        tipus = 'fijo'
        data = line_xml.get('lines', None)
        if not data:
            return None
        line_ids = set()

        date = fact_xml.get_periode_factura()[0]
        if isleap(int(date.split('-')[0])):
            uom_dia = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'product_uom_dia_traspas_gas'
            )[1]
        else:
            uom_dia = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'product_uom_dia_gas'
            )[1]

        tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
            cursor, uid, fact_xml.tipopeaje
        )
        productes = tarifa_obj.get_periodes_producte(
            cursor, uid, tarifa_id, 'tp', context
        )
        ctx = context.copy()
        for concepte in data:
            vals = self._get_vals_for_termes_line(
                concepte, productes, fact_xml
            )
            vals.update({
                'tipus': tipus,
                'force_price': concepte.precunidad or concepte.importe/concepte.unidad,
                'quantity': concepte.unidad,
                'uos_id': uom_dia
            })

            line_id = facturador_obj.crear_linia(
                cursor, uid, fact_id, vals, ctx
            )
            if concepte.impuestoconcepto == 'N':
                linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
                linia_obj.write(cursor, uid, line_id, {'invoice_line_tax_id': [(6, False, [])]})

            line_ids.add(line_id)

        line_ids = list(set(line_ids))
        self.write_lines_summary(
            cursor, uid, file_id, tipus, line_xml, fact_id, line_ids
        )

        line_ids.append(
            self.create_subtotal_line(
                cursor, uid, 'tfx', fact_xml, line_xml, fact_id, line_ids
            )
        )

        return line_ids

    def create_invoice_line_others(self, cursor, uid, fact_xml, fact_id,
                                   line_xml, context=None):
        """
        Creates a line of an invoice with type others
        :param line_xml: The atr's version of the line we are creating
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscegas.facturacio.factura)
        :param fact_xml: The atr's version of the xml's invoice of the B70
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscegas.facturacio.facturador')
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        imd_obj = self.pool.get('ir.model.data')

        factura_vals = factura_obj.read(
            cursor, uid, fact_id, ['partner_id', 'date_invoice']
        )
        tipus = 'altres'
        data = line_xml.get('lines', None)
        if not data:
            return None
        line_ids = []

        for concepte in data:
            xml_id = 'concepte_gas_%s' % concepte.codconcepto
            prod = imd_obj._get_obj(
                cursor, uid, 'giscegas_facturacio_comer', xml_id, context
            )
            vals = self.get_line_others_vals(cursor, uid, concepte, factura_vals, tipus, context=context)

            linia_id = facturador_obj.crear_linies_altres(
                cursor, uid, fact_id, vals, context
            )
            if concepte.impuestoconcepto == 'N':
                linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
                linia_obj.write(cursor, uid, linia_id, {'invoice_line_tax_id': [(6, False, [])]})

            self.aplicar_descompte(cursor, uid, linia_id)
            line_ids.append(linia_id)
            vals = {
                'data_desde': factura_vals['date_invoice'],
                'data_fins': factura_vals['date_invoice'],
                'name': 'Total %s' % prod.name,
                'force_price': concepte.importe,
                'uos_id': prod.uom_id.id,
                'quantity': 1,
                'tipus': 'subtotal_xml',
                'product_id': prod.id,
            }
            # Envien les anuladores amb tots els preus en negatiu. Es una
            # redundancia perque una anuladora ja significa que tornes els diners...
            if fact_xml.clasefact in REFUND_RECTIFICATIVE_TYPES and vals['force_price'] < 0:
                vals['force_price'] *= -1
            vals.update(
                self.get_account_and_tax(cursor, uid, linia_id, context)
            )
            line_ids.append(
                facturador_obj.crear_linia(
                    cursor, uid, fact_id, vals, context
                )
            )
        return line_ids

    def get_line_others_vals(self, cursor, uid, concepte, factura_vals, tipus, context=None):
        imd_obj = self.pool.get('ir.model.data')
        xml_id = 'concepte_gas_%s' % concepte.codconcepto
        prod = imd_obj._get_obj(
            cursor, uid, 'giscegas_facturacio_comer', xml_id, context
        )
        # Si total té valor, hem de facturar el concepte, per tant,
        # quantity = 1.0
        quantity = concepte.unidad or (concepte.importe and 1.0)
        vals = {
            'quantity': quantity,
            'product_id': prod.id,
            'name': prod.name,
            'tipus': tipus,
            'data_desde': factura_vals['date_invoice'],
            'data_fins': factura_vals['date_invoice']
        }
        if not concepte.precunidad:
            vals['force_price'] = concepte.importe
        elif concepte.precunidad and concepte.importe and not concepte.unidad:
            vals['force_price'] = concepte.precunidad
            vals['quantity'] = float(
                concepte.importe / concepte.precunidad
            )
        else:
            vals['force_price'] = concepte.precunidad

        # Quantity must be positive
        if vals['quantity'] < 0:
            vals['force_price'] = vals['force_price'] * -1
            vals['quantity'] = vals['quantity'] * -1

        return vals

    def write_lines_summary(self, cursor, uid, file_id, tipus, line_xml,
                            factura_id, line_ids, context=None):
        """
        Escriurem el resum de les línies per poder fer un seguiment.
        :param cursor:
        :param uid:
        :param file_id:
        :param line_xml:
        :param factura_id:
        :param line_ids:
        :param context:
        :return:
        """
        if context is None:
            context = {}

        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        factura_vals = fact_obj.read(cursor, uid, factura_id, ['comment'])
        comment = factura_vals['comment'] or ''
        if comment:
            comment += '\n'
        comment += u"******* %s ******\n" % tipus.title()
        subtotal = 0
        read_params = [
            'name', 'quantity', 'uos_id', 'price_unit',
            'price_subtotal'
        ]

        for line_vals in linia_obj.read(cursor, uid, line_ids, read_params):
            comment += u"  {0}: {1} {2} * {3} €  = {4} €\n".format(
                line_vals['name'], line_vals['quantity'],
                line_vals['uos_id'][1], line_vals['price_unit'],
                line_vals['price_subtotal']
            )
            subtotal += line_vals['price_subtotal']
        comment += '\n'
        comment += u"  Subt XML: %s € - Subt calc: %s € = %s €\n" % (
            line_xml['total'], subtotal, line_xml['total'] - subtotal
        )
        fact_obj.write(cursor, uid, factura_id, {'comment': comment})

        # If the invoice has an error of more than a cent between the sum of the
        # lines and the total it gives
        if abs(round(line_xml['total'] - subtotal, 2)) > 0.01:
            error_obj.create_error_from_code(
                cursor, uid, file_id, '2', '006',
                {
                    'tipus': tipus,
                    'total_xml': line_xml['total'],
                    'total_imp': subtotal
                },
                context=context
            )

        return factura_id

    @staticmethod
    def _get_vals_for_termes_line(concepte, productes, fact_xml):
        # En els conceptes no ens diuen el periode al que pertany.
        # Com que de moment només tractem tarifes sense DH suposem que es el P1
        name = 'P1'
        return {
            'data_desde': concepte.fecdesde,
            'data_fins': concepte.fechasta,
            'product_id': productes.get(name, False),
            'quantity': concepte.unidad,
            'name': name
        }

    def create_subtotal_line(self, cursor, uid, subtotal_type, fact_xml,
                             line_xml, fact_id, line_ids, context=None):
        """
        Creates a line of an invoice with type subtotal_type (taking into
        account the subtotal_type passed) and the value of the total the
        line_xml.  Also sets all the line_ids descounts to 100.
        :param line_ids: Ids of the lines we are creating the subtotal from
        :param subtotal_type: Type of subtotal value we are creating
        :param line_xml: The atr's version of the line we are creating
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscegas.facturacio.factura)
        :param fact_xml: The atr's version of the xml's invoice of the B70
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created subtotal invoice line
        """
        if context is None:
            context = {}

        tipus_dict = {
            'tvr': 'variable',
            'tfx': 'fixe',
            'ren': 'lloguer',
        }

        facturador_obj = self.pool.get('giscegas.facturacio.facturador')

        self.aplicar_descompte(cursor, uid, line_ids)
        # Creem la línia fictícia amb el total
        # Agafem els valors dels impostos i comptes comptables de l'última
        # línia creada
        vals = {
            'data_desde': fact_xml.get_periode_factura()[0],
            'data_fins': fact_xml.get_periode_factura()[1],
            'name': 'Total {0}'.format(tipus_dict.get(subtotal_type, '')),
            'force_price': line_xml['total'],
            'quantity': 1,
            'tipus': 'subtotal_xml_{0}'.format(subtotal_type),
            'product_id': False,
        }
        # Envien les anuladores amb tots els preus en negatiu. Es una
        # redundancia perque una anuladora ja significa que tornes els diners...
        if fact_xml.clasefact in REFUND_RECTIFICATIVE_TYPES and vals['force_price'] < 0:
            vals['force_price'] *= -1
        vals.update(
            self.get_account_and_tax(cursor, uid, line_ids[-1], context)
        )
        subtotal_id = facturador_obj.crear_linia(
            cursor, uid, fact_id, vals, context
        )

        return subtotal_id

    def create_subtotal_line_account_invoice(self, cursor, uid, subtotal_type, fact_xml, line_xml, fact_id, context=None):
        """
        Creates a line of an invoice with type subtotal_type (taking into
        account the subtotal_type passed) and the value of the total the
        line_xml.  Also sets all the line_ids descounts to 100.
        :param subtotal_type: Type of subtotal value we are creating
        :param line_xml: The atr's version of the line we are creating
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscegas.facturacio.factura)
        :param fact_xml: The atr's version of the xml's invoice of the B70
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created subtotal invoice line
        """
        if context is None:
            context = {}

        tipus_dict = {
            'tvr': 'variable',
            'tfx': 'fixe',
            'ren': 'lloguer',
            'alt': 'altres',
        }

        facturador_obj = self.pool.get('giscegas.facturacio.facturador')

        # Creem la línia fictícia amb el total
        # Agafem els valors dels impostos i comptes comptables de l'última
        # línia creada
        vals = {
            'data_desde': fact_xml.get_periode_factura()[0],
            'data_fins': fact_xml.get_periode_factura()[1],
            'name': 'Total {0}'.format(tipus_dict.get(subtotal_type, '')),
            'force_price': line_xml['total'],
            'quantity': 1,
            'tipus': 'subtotal_xml_{0}'.format(subtotal_type),
            'product_id': False,
        }
        # Envien les anuladores amb tots els preus en negatiu. Es una
        # redundancia perque una anuladora ja significa que tornes els diners...
        if fact_xml.indfacturarect in REFUND_RECTIFICATIVE_TYPES and vals['force_price'] < 0:
            vals['force_price'] *= -1

        vals.update(
            self.get_account_and_tax_from_type(cursor, uid, subtotal_type, fact_id, line_xml, context=context)
        )
        subtotal_id = facturador_obj.crear_linia_account_invoice(
            cursor, uid, fact_id, vals, context
        )

        return [subtotal_id]

    def get_account_and_tax_from_type(self, cursor, uid, subtotal_type, fact_id, line_xml, context=None):
        account_inv_obj = self.pool.get("account.invoice")
        factura = account_inv_obj.browse(cursor, uid, fact_id, context=context)
        partner_id = factura.partner_id.id
        product_id = False
        res = {}
        if factura.rectifying_id:
            fiscal_position_id = factura.rectifying_id.fiscal_position.id
        else:
            fiscal_position_id = factura.fiscal_position.id

        if subtotal_type == 'ren':
            product_id = self.get_default_product_ren(cursor, uid, line_xml, context=context)
        elif subtotal_type == 'tvr':
            product_id = self.get_default_product_tvr(cursor, uid, line_xml, context=context)
        elif subtotal_type == 'tfx':
            product_id = self.get_default_product_tfx(cursor, uid, line_xml, context=context)
        elif subtotal_type == 'alt':
            product_id = self.get_default_product_alt(cursor, uid, line_xml, context=context)

        if product_id:
            res = self.pool.get('account.invoice.line').product_id_change(
                cursor, uid, [], product_id, None, type='in_invoice',
                partner_id=partner_id, fposition_id=fiscal_position_id, context=context
            )
            res = {
                'account_id': res['value']['account_id'],
                'invoice_line_tax_id': [(6, 0, res['value'].get('invoice_line_tax_id', []))]
            }
        return res

    def get_default_product_ren(self, cursor, uid, line_xml, context=None):
        product_obj = self.pool.get('product.product')
        product_id = product_obj.search(cursor, uid, [('default_code', '=', 'ALQG1')])[0]
        return product_id

    def get_default_product_tvr(self, cursor, uid, line_xml, context=None):
        product_obj = self.pool.get('product.product')
        ir_model_obj = self.pool.get('ir.model.data')
        categ_id = ir_model_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', "categ_v_t31_gas"
        )[1]
        product_id = product_obj.search(cursor, uid, [('categ_id', '=', categ_id)])[0]
        return product_id

    def get_default_product_tfx(self, cursor, uid, line_xml, context=None):
        product_obj = self.pool.get('product.product')
        ir_model_obj = self.pool.get('ir.model.data')
        categ_id = ir_model_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'categ_f_t31_gas'
        )[1]
        product_id = product_obj.search(cursor, uid, [('name', '=', 'P1'), ('categ_id', '=', categ_id)])[0]
        return product_id

    def get_default_product_alt(self, cursor, uid, line_xml, context=None):
        imd_obj = self.pool.get('ir.model.data')
        data = line_xml.get('lines', None)
        xml_id = 'concepte_gas_%s' % data[0].codconcepto
        product_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio_comer', xml_id, context
        )[1]
        return product_id

    def aplicar_descompte(self, cursor, uid, lid):
        """Aplica un descompte del 100%.
        """
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        linia_obj.write(cursor, uid, lid, {'discount': 100})
        return True

    def get_account_and_tax(self, cursor, uid, lid, context=None):
        """Retorna la compta comptable i els impostos de la línia per
        insertar-los a un altre.
        """
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        vals = linia_obj.read(cursor, uid, lid, ['invoice_line_tax_id',
                                                 'account_id'], context)
        del vals['id']
        vals['invoice_line_tax_id'] = [(6, 0,
                                        vals.get('invoice_line_tax_id', []))]
        vals['account_id'] = vals['account_id'][0]
        return vals

    def update_contract(self, cursor, uid, line_id, b70_xml, index, context=None):
        if context is None:
            context = {}
        self.update_tarif_from_contract(cursor, uid, line_id, b70_xml, index, context=context)

    def update_tarif_from_contract(self, cursor, uid, line_id, b70_xml, index, context=None):
        if context is None:
            context = {}
        fact_xml = b70_xml.facturas[index]
        if not fact_xml.tipopeaje:
            return

        line_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        lect_help_obj = self.pool.get('giscegas.lectures.atr.helper')
        line_vals = line_obj.read(cursor, uid, line_id, ['cups_id'])
        cups_id = line_vals['cups_id'][0]
        start_date = datetime.strptime(
            fact_xml.get_periode_factura()[0], '%Y-%m-%d'
        )
        data_inici = start_date.strftime('%Y-%m-%d')
        start_date -= timedelta(days=1)

        end_date = datetime.strptime(
            fact_xml.get_periode_factura()[1], '%Y-%m-%d'
        )
        try:
            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, cups_id, data_inici,
                end_date.strftime("%Y-%m-%d")
            )
        except Exception as e:
            polissa_id = False
        if not polissa_id:
            return

        pol_obj = self.pool.get('giscegas.polissa')
        tarifa_obj = self.pool.get('giscegas.polissa.tarifa')
        tarifa_atr = fact_xml.tipopeaje
        tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
            cursor, uid, tarifa_atr, context=context
        )
        ctx = context.copy()
        ctx['date'] = data_inici
        polissa_info = pol_obj.read(cursor, uid, polissa_id, ['tarifa', 'caudal_diario', 'name'], context=ctx)
        pol_tarif_id = polissa_info['tarifa'][0]
        if pol_tarif_id == tarifa_id:
            return

        qdcontract = polissa_info['caudal_diario']
        for compt in fact_xml.listamedidores:
            nou_qd = compt.qdcontratado if compt.qdcontratado else 0
            qdcontract = max([qdcontract, nou_qd])

        if qdcontract is None or qdcontract is False:
            error_obj = self.pool.get('giscegas.facturacio.atr.error')
            error_obj.create_error_from_code(
                cursor, uid, line_id, '4', '002', {
                    'contract_name': polissa_info['name'],
                    'old_tarif': polissa_info['tarifa'][1],
                    'new_tarif': fact_xml.tipopeaje,
                    'caudal_diario': polissa_info['caudal_diario'],
                }, context=context
            )
            return False

        pol_obj.send_signal(cursor, uid, [polissa_id], ['modcontractual'])
        pol_obj.write(cursor, uid, polissa_id, {'tarifa': tarifa_id, 'caudal_diario': qdcontract})
        wz_crear_mc_obj = self.pool.get('giscegas.polissa.crear.contracte')
        ctx = {'active_id': polissa_id}
        params = {
            'duracio': 'actual',
            'accio': 'nou',
            'data_inici': fact_xml.get_periode_factura()[0]
        }

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_final': res['value']['data_final']
        })
        wiz_mod.action_crear_contracte(ctx)
        error_obj = self.pool.get('giscegas.facturacio.atr.error')
        error_obj.create_error_from_code(
            cursor, uid, line_id, '4', '001', {
                'contract_name': polissa_info['name'],
                'old_tarif': polissa_info['tarifa'][1],
                'new_tarif': fact_xml.tipopeaje,
                'start_date': data_inici,
            }, context=context
        )

    def update_meters(self, cursor, uid, line_id, b70_xml, index, context=None):
        """
        Here we will handle everything related with updating meters while
        importing B70 files. We will do things like:
            -> Create unexisting meters
            -> Deactivate old meters
            -> Handle dates on active and deactivated meters
        :param cursor: Database cursor
        :param uid: User id
        :param line_id: The line_id that we are currently working with
        :param b70_xml: B70 file that we are currently importing, from the
        atr library
        :param context: OpenERP's context
        :return: -
        """
        if context is None:
            context = {}

        lect_helper = self.pool.get('giscegas.lectures.atr.helper')
        line_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        compt_obj = self.pool.get('giscegas.lectures.comptador')
        imd_obj = self.pool.get('ir.model.data')

        uom_alq_dia = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'uom_aql_gas_dia'
        )[1]

        line_vals = line_obj.read(cursor, uid, line_id, ['cups_id'])
        cups_id = line_vals['cups_id'][0]

        fact_xml = b70_xml.facturas[index]
        if not fact_xml.is_only_conceptes():
            start_date = datetime.strptime(
                fact_xml.get_periode_factura()[0], '%Y-%m-%d'
            )

            data_inici = start_date.strftime('%Y-%m-%d')
            start_date -= timedelta(days=1)

            end_date = datetime.strptime(
                fact_xml.get_periode_factura()[1], '%Y-%m-%d'
            )

            num_dias_fact = (end_date - start_date).days

            polissa_id = lect_helper.find_polissa_with_cups_id(
                cursor, uid, cups_id, data_inici,
                end_date.strftime("%Y-%m-%d")
            )

            comptadors_xml = fact_xml.get_comptadors()
            lloguers, total = fact_xml.get_info_lloguer()

            calc_total = sum([
                llog.precunidad * llog.unidad
                for llog in lloguers
            ])

            num_compt = len(comptadors_xml)
            for i, compt_xml in enumerate(comptadors_xml):
                lectures = compt_xml.get_lectures_info()
                dict_lect = agrupar_lectures_per_data(lectures)
                start_date, end_date = obtenir_data_inici_i_final(dict_lect)

                # We add 1 day to start_date because initial readings are
                # always not included, so the meter must start 1 day after
                # initial reading
                start_date = datetime.strptime(start_date, "%Y-%m-%d")
                start_date += timedelta(days=1)
                start_date = start_date.strftime("%Y-%m-%d")

                compt_id = lect_helper.check_meter(
                    cursor, uid, compt_xml.numseriemedidor,
                    compt_xml.get_giro(), polissa_id, start_date,
                    end_date, num_compt, fact_xml, context
                )

                if compt_id:
                    if num_dias_fact == 0:
                        preu = 0
                    elif round(total, 2) != round(calc_total, 2):
                        preu = total / num_dias_fact
                    else:
                        # Add to context if rents must be processed as
                        # "simultanious" rents. They are simultanious rents
                        # when the number of total rent days is greater
                        # than num_dias_fact and for each individual
                        # rent the number of days is equal to num_dias_fact
                        new_context = context.copy()
                        new_context.update({
                            "simultanious_rents":
                                self.check_simultanious_rents(
                                    num_dias_fact, lloguers
                                )
                        })
                        preu = lect_helper.calculate_price_from_rent_prices(
                            cursor, uid, comptadors_xml, lloguers, i,
                            new_context
                        )

                    compt_vals = compt_obj.read(
                        cursor, uid, compt_id, ['preu_lloguer', 'lloguer']
                    )

                    price_changed = preu != compt_vals['preu_lloguer']

                    # If it's currently on rent and the price on the XML is
                    # different from the one we have
                    new_rent_price = price_changed and compt_vals['lloguer']
                    # Or if it's currently as not on rent but they send us
                    # a price
                    to_set_to_rent = preu > 0 and not compt_vals['lloguer']
                    if new_rent_price or to_set_to_rent:
                        compt_obj.write(
                            cursor, uid, compt_id, {
                                'preu_lloguer': preu,
                                'lloguer': True,
                                'uom_id': uom_alq_dia
                            }
                        )

    @staticmethod
    def check_simultanious_rents(num_dias_fact, lloguers_xml):
        equal_rent_days = False
        rent_days = len(lloguers_xml) and lloguers_xml[0].unidad
        for l in lloguers_xml:
            equal_rent_days = rent_days == l.unidad
            if not equal_rent_days:
                break
        return equal_rent_days and rent_days == num_dias_fact

    def import_readings(self, cursor, uid, line_id, b70_xml, index, context=None):
        """
        Here we will create the readings in the database and make the necessary
        links to the other rows
        :param cursor: Database cursor
        :param uid: User id
        :param line_id: The line_id that we are currently working with
        :param b70_xml: B70 file that we are currently importing, from the
        atr library
        :param context: OpenERP's context
        :return: The ids of the energy and power readings that we have created
        """
        if context is None:
            context = {}
        context['codi_emisor'] = b70_xml.get_codi_emisor
        energy_readings = []

        fact_xml = b70_xml.facturas[index]
        if not fact_xml.is_only_conceptes():
            it_ener_read = self.import_readings_from_fact(
                cursor, uid, line_id, fact_xml, context
            )
            energy_readings += it_ener_read

        return energy_readings

    def import_readings_from_fact(self, cursor, uid, line_id, fact_xml,
                                  context=None):
        if context is None:
            context = {}

        start_date, end_date = fact_xml.get_periode_factura()
        context.update(
            {
                'date': end_date,
                'from_b70': True,
            }
        )

        lect_helper = self.pool.get('giscegas.lectures.atr.helper')
        line_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        lin_fact_obj = self.pool.get(
            'giscegas.facturacio.importacio.linia.factura'
        )
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        cups_obj = self.pool.get('giscegas.cups.ps')
        polissa_obj = self.pool.get('giscegas.polissa')
        tarifa_obj = self.pool.get('giscegas.polissa.tarifa')
        compt_obj = self.pool.get('giscegas.lectures.comptador')
        cfg = self.pool.get('res.config')

        line_vals = line_obj.read(
            cursor, uid, line_id, ['cups_id', 'liniafactura_id'], context
        )

        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        data_inici = start_date.strftime('%Y-%m-%d')
        start_date -= timedelta(days=1)

        polissa_id = lect_helper.find_polissa_with_cups_id(
            cursor, uid, line_vals['cups_id'][0], data_inici, end_date
        )
        polissa_vals = polissa_obj.read(
            cursor, uid, polissa_id, ['tarifa'], context
        )
        tarifa_vals = tarifa_obj.read(
            cursor, uid, polissa_vals['tarifa'][0], ['codi_ocsum'], context
        )
        lin_fact_id = lin_fact_obj.search(
            cursor, uid, [
                ('id', 'in', line_vals['liniafactura_id']),
                ('origin', '=', fact_xml.get_origin())
            ]
        )[0]

        overwrite_atr = bool(int(
            cfg.get(
                cursor, uid, 'lect_atr_gas_overwrite_atr_measure_when_found', '0'
            )
        ))
        rectificadora = fact_xml.clasefact == 'R'
        tariff_name = polissa_vals['tarifa'][1]
        ocsum_tarifa = tarifa_vals['codi_ocsum']
        factura_id = lin_fact_obj.read(
            cursor, uid, lin_fact_id, ['factura_id']
        )['factura_id'][0]

        energy_read_ids = []
        energy_fact_read_ids = []

        # Unlink old lectures of invoice
        old_lect = fact_obj.read(cursor, uid, factura_id, ['lectures_ids'])
        ene_read_obj = self.pool.get('giscegas.facturacio.lectures')
        ene_read_obj.unlink(cursor, uid, old_lect['lectures_ids'])

        # Les anuladores eliminen les lectures
        ctx = context.copy()
        ctx['eliminar_lectures'] = fact_xml.clasefact == "A"

        for compt_xml in fact_xml.get_comptadors():
            compt_id = compt_obj.search_with_contract(
                cursor, uid, compt_xml.numseriemedidor, polissa_id
            )[0]

            for lect_xml in compt_xml.get_lectures_info():
                if not lect_xml.get('ometre'):
                    # We process active and reactive readings
                    l1, l2 = self.proces_lect_xml(
                        cursor, uid, lect_xml, compt_id, ocsum_tarifa,
                        factura_id, tariff_name, rectificadora, overwrite_atr,
                        context=ctx
                    )
                    energy_read_ids.extend(l1)
                    energy_fact_read_ids.extend(l2)

        # Write new lect.
        fact_obj.write(
            cursor, uid, factura_id, {
                'lectures_ids': [(6, 0, energy_fact_read_ids)],
            }
        )

        # Mark the import line with "lectures_processades"
        line_obj.write(
            cursor, uid, line_id, {"lectures_processades": True}, context
        )

        return energy_read_ids

    def proces_lect_xml(self, cursor, uid, lect_xml, compt_id, ocsum_tarifa,
                        factura_id, tariff_name, rectificadora, overwrite_atr,
                        context=None):
        if context is None:
            context = {}
        lect_helper = self.pool.get('giscegas.lectures.atr.helper')
        energy_read_ids = []
        energy_fact_read_ids = []
        lect_vals = {'inicial': {}, 'final': {}}

        # Si ens ho passen per context, eliminarem la lectura final
        eliminar_lectures = False
        if "eliminar_lectures" in context:
            eliminar_lectures = context['eliminar_lectures']
            context.pop("eliminar_lectures")
        for lect_point in ['inicial', 'final']:
            # We process both initial and final readings
            ctx = context.copy()
            if lect_point == 'final':
                ctx['eliminar_lectures'] = eliminar_lectures
            lect_vals[lect_point] = lect_helper.vals_lectura(
                cursor, uid, lect_xml, lect_point, compt_id,
                tarifa=ocsum_tarifa, context=ctx
            )
            lect_id = lect_helper.import_reading(
                cursor, uid, lect_vals[lect_point], rectificadora=rectificadora,
                overwrite_atr=overwrite_atr, context=ctx
            )
            if lect_id:
                energy_read_ids.append(lect_id)
        fact_lect_id = self.import_invoicing_reading(
            cursor, uid, factura_id, tariff_name, 'P1',
            lect_vals, context=context
        )
        energy_fact_read_ids.append(fact_lect_id)
        return energy_read_ids, energy_fact_read_ids

    def import_invoicing_reading(self, cursor, uid, factura_id,
                                        tariff_name, period, lect_vals,
                                        context=None):
        if context is None:
            context = {}

        inv_read_obj = self.pool.get('giscegas.facturacio.lectures')

        lect_final_vals = lect_vals['final']
        lect_inic_vals = lect_vals['inicial']
        vals = {
            'name': "%s (%s)" % (tariff_name, period),
            'lect_actual': lect_final_vals['lectura'],
            'consum': lect_final_vals['consum'],
            'lect_anterior': lect_inic_vals['lectura'],
            'data_actual': lect_final_vals['name'],
            'data_anterior': lect_inic_vals['name'],
            'factura_id': factura_id,
            'comptador_id': lect_final_vals['comptador'],
            'origen_id': lect_final_vals['origen_id'],
            'origen_anterior_id': lect_inic_vals['origen_id'],
            'pcs': lect_final_vals['pcs'],
            'factor_k': lect_final_vals['factor_k'],
            'pressio_subministrament': lect_final_vals['pressio_subministrament'],
            'tipo_lect_num_actual': lect_final_vals['tipo_lect_num'],
            'tipo_lect_num_anterior': lect_inic_vals['tipo_lect_num'],
        }
        ajust = lect_final_vals.get('ajust', False)
        motiu_ajust = lect_final_vals.get('motiu_ajust', False)
        if ajust and motiu_ajust:
            vals.update(
                {
                    'ajust': ajust,
                    'motiu_ajust': motiu_ajust,
                }
            )

        inv_read_id = inv_read_obj.create(cursor, uid, vals, context)

        return inv_read_id

GiscegasFacturacioAtrHelper()


class GiscegasFacturacioFactura(osv.osv):
    """Classe per importar factures en XML
    """
    _name = 'giscegas.facturacio.factura'
    _inherit = 'giscegas.facturacio.factura'

    def search(self, cursor, uid, args, offset=0, limit=None, order=None,
        context=None, count=False):
        for pos, arg in enumerate(args):
            if arg[0] == 'div_totals' and arg[1] in ('>=', '<=', '='):
                cursor.execute("select id from giscegas_facturacio_factura "
                               "where "
                               "@div_totals %s %%s" % arg[1], (arg[2],))
                ids = [a[0] for a in cursor.fetchall()] or [-1]
                args[pos] = ['id', 'in', ids]
        return super(GiscegasFacturacioFactura, self).search(cursor, uid,
            args, offset=offset, limit=limit, order=order, context=context,
            count=count)

    def invoice_open(self, cursor, uid, ids, context=None):
        """Adds invoice to payment order configured in payment_order_id"""

        # Check if configuration var is enabled
        # - If an invoice has b70 and not in phase 4-5 can be opened
        # - If the B70 is in phase 4 will check if B70 is valid to be opened
        cfg_obj = self.pool.get('res.config')
        b70_constraint = int(
            cfg_obj.get(cursor, uid, 'fact_open_b70_constraint', '0')
        )
        if b70_constraint:
            # Relation Table Factura(factura_id) - B70(linia_id)
            fact_b70_obj = self.pool.get('giscegas.facturacio.importacio.linia.factura')
            from osv.expression import OOQuery
            q = OOQuery(fact_b70_obj, cursor, uid)
            sql = q.select(['linia_id', 'linia_id.import_phase', 'linia_id.state']).where([
                ('factura_id', 'in', tuple(ids)),
                ('linia_id.import_phase', '!=', 50),
            ])
            cursor.execute(*sql)
            result = cursor.dictfetchall()
            result = [b70['linia_id'] for b70 in result
                      if b70['linia_id.import_phase'] != 40
                      or b70['linia_id.state'] != 'valid'
                      ]
            if result:
                raise osv.except_osv(_('Error'),
                                     _('No es pot obrir la factura ja que te un'
                                       ' B70 en fase 1-3 o un en fase 4 no valid'
                                       '\nB70 detectats: {0}'
                                       ).format(",".join(str(x) for x in result))
                                     )
        invoice_vals = self.read(
            cursor, uid, ids, ['type', 'tipo_factura'], context
        )

        proforma_inv = []
        no_proforma_inv = []
        for inv_vals in invoice_vals:
            # Don't open proforma invoices from supplier. They will be in a
            # proforma group
            in_type = 'in' in inv_vals['type']
            state_pro = inv_vals['tipo_factura'] == '11'

            if in_type and state_pro:
                proforma_inv.append(inv_vals['id'])
            else:
                no_proforma_inv.append(inv_vals['id'])

        ids = no_proforma_inv
        res = super(
            GiscegasFacturacioFactura, self
        ).invoice_open(cursor, uid, ids, context=context)

        invoice_fields = ['payment_order_id', 'state']
        for invoice in self.read(cursor, uid, ids, invoice_fields,
                                 context=context):
            order_id = invoice.get('payment_order_id', False)
            invoice_state = invoice.get('state', False)
            if order_id and invoice_state in ['open']:
                invoice_id = invoice['id']
                self.afegeix_a_remesa(
                    cursor, uid, [invoice_id], order_id[0], context=context
                )

        return res

    def get_in_invoice_by_origin(
            self, cursor, uid, origin, partner_id, year=None, context=None
    ):
        """
        Returns provider invoice id of partner with selected origin
        :param origin:
        :param partner_id:
        :param context:
        :param year:
        :type year: str
        :return: return invoice ids or empty list
        """
        search_dict = [('origin', '=', origin),
                       ('partner_id', '=', partner_id),
                       ('type', 'in', ['in_invoice', 'in_refund']),
                       ]

        if year:
            ini = '{0}-01-01'.format(year)
            end = '{0}-12-31'.format(year)
            search_dict.append(('origin_date_invoice', '>=', ini))
            search_dict.append(('origin_date_invoice', '<=', end))

        invoice_ids = self.search(cursor, uid, search_dict, context=context)
        return invoice_ids
    
    def create_grouped_invoice(self, cursor, uid, proforma_ids, mp_origin, context=None):
        if context is None:
            context = {}

        wf_service = netsvc.LocalService("workflow")

        inv_obj = self.pool.get("account.invoice")
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        linia_fact_obj = self.pool.get('giscegas.facturacio.factura.linia')
        line_inv_obj = self.pool.get('account.invoice.line')

        rectif_types = dict(TIPO_RECTIFICADORA_SELECTION)
        line_types = dict(linia_fact_obj._tipus_selection(cursor, uid, context))

        ignore_on_line_name = ['N']

        read_params = ['number', 'tipo_rectificadora', 'linia_ids',
                       'date_invoice', 'check_total']
        fact_reads = fact_obj.read(
            cursor, uid, proforma_ids, read_params, context
        )
        
        # We use info about one of the proformas
        invoice_id = self.create_base_grouped_invoice(
            cursor, uid, proforma_ids[0], mp_origin, context
        )

        comment = _(
            u'Això és la factura agrupadora de les factures [{0}] per el B70 {1}'
        ).format(
            ', '.join([str(fact['number']) for fact in fact_reads]), mp_origin
        )
        inv_obj.write(cursor, uid, invoice_id, {'comment': comment})
        check_total_group = 0.0
        for fact_vals in fact_reads:
            sign = SIGN[fact_vals['tipo_rectificadora']]
            check_total_group += sign * fact_vals['check_total']
            for linia_fact_id in fact_vals['linia_ids']:
                linia_fact_vals = linia_fact_obj.read(
                    cursor, uid, linia_fact_id, ['invoice_line_id', 'tipus'],
                    context
                )
                line_inv_id = linia_fact_vals['invoice_line_id'][0]

                line_data = line_inv_obj.copy_data(
                    cursor, uid, line_inv_id, context
                )[0]

                line_data.update({'invoice_id': invoice_id})

                old_name = line_data['name']

                line_name = '{0} - {1} - {2}'.format(
                    fact_vals['number'], old_name,
                    line_types[linia_fact_vals['tipus']]
                )
                rect_type = fact_vals['tipo_rectificadora']
                if rect_type not in ignore_on_line_name:
                    fact_rect_type = rectif_types[rect_type]
                    line_name += ' - {0}'.format(fact_rect_type)
                line_data['name'] = line_name
                line_data['quantity'] = line_data['quantity'] * sign
                line_inv_id = line_inv_obj.create(cursor, uid, line_data)

        inv_obj.write(cursor, uid, invoice_id, {'check_total': check_total_group})

        inv_obj.button_reset_taxes(cursor, uid, [invoice_id], context)
        date_invoice = inv_obj.read(
            cursor, uid, invoice_id, ['date_invoice']
        )['date_invoice']
        fact_obj.write(
            cursor, uid, proforma_ids, {'date_invoice': date_invoice}
        )

        base_ids = fact_obj._get_base_ids(cursor, uid, proforma_ids, context)
        for fact_id in base_ids:
            wf_service.trg_validate(
                uid, 'account.invoice', fact_id, 'invoice_group', cursor
            )

        return invoice_id  # Generar una account invoice o genera una factura amb un contracte "random"?
    
    def create_base_grouped_invoice(self, cursor, uid, invoice_id, origin, context=None):
        if context is None:
            context = {}

        inv_obj = self.pool.get('account.invoice')
        journal_obj = self.pool.get('account.journal')
        fact_obj = self.pool.get("giscegas.facturacio.factura")
        invoice_id = fact_obj._get_base_ids(cursor, uid, [invoice_id])[0]

        journal_code = "CGAS"
        journal_id = journal_obj.search(
            cursor, uid, [('code', '=', journal_code)], limit=1
        )[0]

        pro_vals = inv_obj.read(
            cursor, uid, invoice_id, [
                'name',
                'partner_id',
                'partner_bank',
                'payment_type',
                'origin_date_invoice'
            ]
        )

        inv_vals = inv_obj.onchange_partner_id(
            cursor, uid, [], 'in_invoice', pro_vals['partner_id'][0],
            partner_bank=pro_vals['partner_bank'] and pro_vals['partner_bank'][0]
        )['value']

        payment_type = None
        if pro_vals['payment_type']:
            payment_type = pro_vals['payment_type'][0]

        inv_vals.update(
            {
                'origin': origin,
                'comment': None,
                'number': False,
                'journal_id': journal_id,
                'partner_id': pro_vals['partner_id'][0],
                'reference_type': u'none',
                'state': 'draft',
                'partner_bank': pro_vals['partner_bank'] and pro_vals['partner_bank'][0],
                'type': 'in_invoice',
                'payment_type': payment_type,
                'date_invoice': datetime.now(),
                'origin_date_invoice': pro_vals['origin_date_invoice']
            }
        )
        return inv_obj.create(cursor, uid, inv_vals)

    def get_pseudofactures_for_invoice(self, cursor, uid, invoice_id, context=None):
        if context is None:
            context = {}
        inv_obj = self.pool.get("account.invoice")
        origin = inv_obj.read(cursor, uid, invoice_id, ['origin'])['origin']
        partner_id = inv_obj.read(cursor, uid, invoice_id, ['partner_id'])['partner_id'][0]
        search_params = [
            ('tipo_factura', '=', '11'),
            ('partner_id', '=', partner_id),
            ('origin', '=like', "{0} -%".format(origin)),
        ]
        return self.search(cursor, uid, search_params, context=context)

    def set_proforma_pseudofacturas_from_invoice(self, cursor, uid, invoice_id, pseudofactura_ids, context=None):
        if context is None:
            context = {}
        if not pseudofactura_ids:
            return
        inv_obj = self.pool.get("account.invoice")
        inv_data = inv_obj.read(cursor, uid, invoice_id, ['number', 'date_invoice', 'origin_date_invoice'])
        self.write(cursor, uid, pseudofactura_ids, {
            'number': inv_data['number'],
            'date_invoice': inv_data['date_invoice'],
            'origin_date_invoice': inv_data['origin_date_invoice'],
            'fiscal_invoice_id': invoice_id
        })
        wf_service = netsvc.LocalService("workflow")
        for info in self.read(cursor, uid, pseudofactura_ids, ['invoice_id']):
            wf_service.trg_validate(
                uid, 'account.invoice', info['invoice_id'][0], 'invoice_proforma2', cursor
            )

    _columns = {
        'importacio_id': fields.many2one('giscegas.facturacio.importacio',
                                         'Importacio'),
        'div_totals': fields.float('Divergència',
                                   digits=(16, int(config['price_accuracy']))),
        'fiscal_invoice_id': fields.many2one('account.invoice', "Factura ATR Agregada")
    }

GiscegasFacturacioFactura()
