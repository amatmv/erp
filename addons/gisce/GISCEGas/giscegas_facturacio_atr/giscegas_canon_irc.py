# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, orm, fields
import netsvc
from time import strftime
from datetime import datetime, timedelta
from gestionatr.defs_gas import TAULA_CONCEPTO_FACTURACION

# Codigos tabla CONCEPTOS FACTURACION
# 1302 - Canon Finca
# 1911 - Canon IRC 1
# 1912 - Canon IRC 2
# 1913 - Canon Plan 10
# 1928 - Canon IRC 3
# 1936 - Canon IRC 4

# CAMPOS A TRATAR DE UN B70
# <concepto>
#   <fecdesde>            - Fecha inicio periodo
#   <fechasta>            - Fecha final periodo
#   <unidad>              - Numero de dias?
#   <precunidad>          - Precio por dia
#   <importe>             -
#   <codconcepto>         - Codigo de la TABLA CONCEPTOS
#   <desconcepto>         - Descripcion del concepto
#   <porcentajeconcepto>  -
#   <impuestoconcepto>    - Aplicar impuesto si/no
#   <codtipoimpuesto>     -
#   <porcentajeimpcto>    -
# <concepto>


class CanonIRC(osv.osv):

    _name = 'giscegas.canon.irc'
    _inherit = 'giscegas.canon.irc'

    # This implemented functions are here because giscegas_facturacio
    # doesnt has canon products

    def get_canon_product_by_concept_code(self, cursor, uid, codconcept, context=None):
        # Only allow these codes
        # 1302 - Canon Finca
        # 1911 - Canon IRC 1
        # 1912 - Canon IRC 2
        # 1913 - Canon Plan 10
        # 1928 - Canon IRC 3
        # 1936 - Canon IRC 4
        imd_obj = self.pool.get('ir.model.data')
        product_canon_id = False

        if codconcept == '1302':
            product_canon_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_comer', 'concepte_gas_1302'
            )[1]
        elif codconcept == '1911':
            product_canon_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_comer', 'concepte_gas_1911'
            )[1]

        elif codconcept == '1912':
            product_canon_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_comer', 'concepte_gas_1912'
            )[1]

        elif codconcept == '1913':
            product_canon_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_comer', 'concepte_gas_1913'
            )[1]

        elif codconcept == '1928':
            product_canon_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_comer', 'concepte_gas_1928'
            )[1]

        elif codconcept == '1936':
            product_canon_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_comer', 'concepte_gas_1936'
            )[1]

        return product_canon_id

    def search_and_update_or_create_canon(self, cursor, uid, vals, context=None):
        code_canon = vals.get('codi_cnmc', False)
        polissa_id = vals.get('polissa_id', False)

        product_canon_id = self.get_canon_product_by_concept_code(
            cursor, uid, code_canon, context=context
        )

        if not product_canon_id:
            # Is not a canon product
            return False
        else:
            ctx = context.copy()
            ctx['active_test'] = False

            canon_id = self.search(
                cursor, uid,
                [
                    ('polissa_id', '=', polissa_id),
                    ('codi_cnmc', '=', code_canon)
                ],
                context=ctx
            )
            if canon_id:  # Exist a record
                canon_id = canon_id[0]
                if 'canon_product_id' in vals:
                    vals.pop('canon_product_id')
                if 'data_alta' in vals:
                    vals.pop('data_alta')
                if 'polissa_id' in vals:
                    vals.pop('polissa_id')
                vals['active'] = True

                self.write(cursor, uid, [canon_id], vals, context=ctx)
                return canon_id
            else:
                vals['canon_product_id'] = product_canon_id
                return self.create(cursor, uid, vals, context=context)

CanonIRC()


class GiscegasFacturacioAtrHelper(osv.osv):
    """Funcions d'ajuda atr_gas B70"""

    _name = 'giscegas.facturacio.atr.helper'
    _inherit = 'giscegas.facturacio.atr.helper'

    def update_canons(self, cursor, uid, line_id, b70_xml, index, context=None):
        """
        Here we will update current canons asosciated to contract from B70:
            -> Create canon if not exist with product asociated to code
            -> Deactivate outated canons
            -> Update canon price

        :param cursor: Database cursor
        :param uid: User id
        :param line_id: The line_id that we are currently working with
        :param b70_xml: B70 file that we are currently importing, from the
        atr library
        :param context: OpenERP's context
        :return: -
        """

        if context is None:
            context = {}

        lect_helper = self.pool.get('giscegas.lectures.atr.helper')
        line_obj = self.pool.get('giscegas.facturacio.importacio.linia')
        canon_obj = self.pool.get('giscegas.canon.irc')

        line_vals = line_obj.read(cursor, uid, line_id, ['cups_id'])
        cups_id = line_vals['cups_id'][0]

        fact_xml = b70_xml.facturas[index]

        if not fact_xml.is_only_conceptes():
            start_date = datetime.strptime(
                fact_xml.get_periode_factura()[0], '%Y-%m-%d'
            )

            data_inici = start_date.strftime('%Y-%m-%d')
            start_date -= timedelta(days=1)

            end_date = datetime.strptime(
                fact_xml.get_periode_factura()[1], '%Y-%m-%d'
            )

            num_dias_fact = (end_date - start_date).days

            polissa_id = lect_helper.find_polissa_with_cups_id(
                cursor, uid, cups_id, data_inici,
                end_date.strftime("%Y-%m-%d")
            )

            conceptes_xml = fact_xml.listaconceptos

            active_canons = []
            for i, compt_xml in enumerate(conceptes_xml):
                codigo_concepto = compt_xml.codconcepto
                desc_concepto = compt_xml.desconcepto
                fec_desde = compt_xml.fecdesde
                fec_hasta = compt_xml.fechasta
                importe = compt_xml.importe

                period_days = (
                    datetime.strptime(fec_hasta, '%Y-%m-%d') -
                    (datetime.strptime(fec_desde, '%Y-%m-%d') - timedelta(days=1))  # Add 1 day to get all days
                ).days

                precio_dia = float(importe) / float(period_days) # Todo mirar decimales

                # El precio por dia lo calcularemos con los dias entre
                # el periodo y el importe total, dado que los otros datos
                # pueden utilizar unidades distintas segun la empresa emisora

                canon_vals = {
                    'polissa_id': polissa_id,
                    'data_alta': fec_desde,
                    'codi_cnmc': codigo_concepto,
                    'active': True,
                    'preu_dia': precio_dia,
                    'name': desc_concepto
                }

                canon_id = canon_obj.search_and_update_or_create_canon(
                    cursor, uid, canon_vals, context=context
                )
                if canon_id:
                    active_canons.append(canon_id)

            all_canons_polissa_ids = canon_obj.search(
                cursor, uid,
                [
                    ('polissa_id', '=', polissa_id),
                    ('active', '=', True)
                ]
            )

            to_inactivate_canon_ids = list(
                set(all_canons_polissa_ids) - set(active_canons)
            )
            if to_inactivate_canon_ids:
                canon_obj.write(
                    cursor, uid, to_inactivate_canon_ids,
                    {
                        'data_baixa': start_date.strftime('%Y-%m-%d'),
                        'active': False
                    },
                    context=context
                )


GiscegasFacturacioAtrHelper()