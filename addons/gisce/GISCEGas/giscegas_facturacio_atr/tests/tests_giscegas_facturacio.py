# -*- coding: utf-8 -*-
import unittest
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from tools import cache


class TestImporting(testing.OOTestCase):
    def switch(self, txn, where):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')
        cups_obj = self.openerp.pool.get('giscegas.cups.ps')
        part_obj = self.openerp.pool.get('giscemisc.participant')
        partner_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'main_partner'
        )[1]
        part_partner_id = part_obj.get_id_from_partner(
            cursor, uid, partner_id
        )
        other_id = imd_obj.get_object_reference(
            cursor, uid, 'base',  'res_partner_agrolait'
        )[1]
        part_other_id = part_obj.get_id_from_partner(
            cursor, uid, other_id
        )
        another_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_c2c'
        )[1]
        part_another_id = part_obj.get_id_from_partner(
            cursor, uid, another_id
        )
        codes = {'distri': '1234', 'comer': '4321'}
        part_obj.write(cursor, uid, [part_partner_id], {
            'codi_sifco': codes.pop(where)
        })
        part_obj.write(cursor, uid, [part_other_id], {
            'codi_sifco': codes.values()[0]
        })
        part_obj.write(cursor, uid, [part_another_id], {
            'codi_sifco': '5555'
        })
        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_cups', 'giscegas_cups_01'
        )[1]
        distri_ids = {'distri': partner_id, 'comer': other_id}
        cups_obj.write(cursor, uid, [cups_id], {
            'distribuidora_id': distri_ids[where]
        })
        cache.clean_caches_for_db(cursor.dbname)

    def activar_polissa_CUPS(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscegas.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def crear_modcon(self, txn, potencia, ini, fi):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'tarifa': 2})
        wz_crear_mc_obj = pool.get('giscegas.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

