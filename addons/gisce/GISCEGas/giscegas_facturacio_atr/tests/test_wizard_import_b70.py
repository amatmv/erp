# -*- coding: utf-8 -*-
import unittest
import base64

from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.osv import except_osv
from addons import get_module_resource


class TestWizardImportB70(testing.OOTestCase):

    require_demo_data = True

    def test_import_xml_fail_phase_1_with_info(self):
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_utils_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
        wiz_obj = self.openerp.pool.get(
            'wizard.importacio.b70'
        )
        import_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio'
        )
        linia_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            vals = {
                'file': base64.b64encode(xml_file),
                'filename': 'FILE_gas.xml'
            }
            ctx = {}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            before_ids = import_obj.search(cursor, uid, [])
            wiz.action_importar_b70()
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            expect('done').to(equal(wiz.state))
            after_ids = import_obj.search(cursor, uid, [])
            new_ids = list(set(after_ids) - set(before_ids))
            # Only one file is created
            expect(len(new_ids)).to(equal(1))
            import_data = import_obj.read(cursor, uid, new_ids[0], [])
            expect(import_data['name']).to(equal('FILE_gas.xml'))
            expect(len(import_data['linia_ids'])).to(equal(1))
            linia_obj.process_line_sync(cursor, uid, import_data['linia_ids'][0])
            errors = error_obj.search(
                cursor, uid, [('line_id', '=', import_data['linia_ids'])]
            )
            expect(len(errors)).to(equal(1))
            error = error_obj.browse(cursor, uid, errors[0])
            expect(error.message).to(equal(
                "No s'ha trobat l'emissor amb aquest codi: 1234"
            ))
            expect(error.name).to(equal("1002"))

    def test_import_incorrect_xml_successfully(self):
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_fail_import_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
        wiz_obj = self.openerp.pool.get(
            'wizard.importacio.b70'
        )
        import_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio'
        )
        linia_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            vals= {
                'file': base64.b64encode(xml_file),
                'filename': 'FILE_gas.xml'
            }
            ctx = {}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            before_ids = import_obj.search(cursor, uid, [])
            wiz.action_importar_b70()
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            expect('done').to(equal(wiz.state))
            after_ids = import_obj.search(cursor, uid, [])
            new_ids = list(set(after_ids)-set(before_ids))
            # Only one file is created
            expect(len(new_ids)).to(equal(1))
            import_data = import_obj.read(cursor, uid, new_ids[0], [])
            expect(import_data['name']).to(equal('FILE_gas.xml'))
            expect(len(import_data['linia_ids'])).to(equal(1))
            linia_obj.process_line_sync(cursor, uid, import_data['linia_ids'][0])
            linia_data = linia_obj.read(
                cursor, uid, import_data['linia_ids'][0], []
            )
            expect(linia_data['state']).to(equal('erroni'))
            expect(linia_data['import_phase']).to(equal(10))
            expect(len(linia_data['error_ids'])).to(equal(1))

    def test_import_zip_successfully(self):
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_fail_import_gas.xml.zip'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
        wiz_obj = self.openerp.pool.get(
            'wizard.importacio.b70'
        )
        import_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            vals= {
                'file': base64.b64encode(xml_file),
                'filename': 'test_fail_import_gas.xml.zip'
            }
            ctx = {}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            before_ids = import_obj.search(cursor, uid, [])
            wiz.action_importar_b70()
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            expect('done').to(equal(wiz.state))
            after_ids = import_obj.search(cursor, uid, [])
            new_ids = list(set(after_ids)-set(before_ids))
            # Only one file is created
            expect(len(new_ids)).to(equal(1))
            import_data = import_obj.read(cursor, uid, new_ids[0], [])
            expect(import_data['name']).to(equal('test_fail_import_gas.xml.zip'))
            expect(len(import_data['linia_ids'])).to(equal(1))

    def test_import_multifolders_zip_successfully(self):
        '''
        Import ZIP File with the following structure:

        ZIP.-b70_invalid_emisor_gas.xml
            -import 1 (folder)
              - test_fail_import_gas.xml
              - import 2 (folder)
                - test_utils_gas.xml
        '''
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'b70_multi_folder_imports.zip'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
        wiz_obj = self.openerp.pool.get(
            'wizard.importacio.b70'
        )
        import_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            vals= {
                'file': base64.b64encode(xml_file),
                'filename': 'test_fail_import_gas.xml.zip'
            }
            ctx = {}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            before_ids = import_obj.search(cursor, uid, [])
            wiz.action_importar_b70()
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            expect('done').to(equal(wiz.state))
            after_ids = import_obj.search(cursor, uid, [])
            new_ids = list(set(after_ids)-set(before_ids))
            # Only one file is created
            expect(len(new_ids)).to(equal(1))
            import_data = import_obj.read(cursor, uid, new_ids[0], [])
            expect(import_data['name']).to(equal('test_fail_import_gas.xml.zip'))
            expect(len(import_data['linia_ids'])).to(equal(3))
