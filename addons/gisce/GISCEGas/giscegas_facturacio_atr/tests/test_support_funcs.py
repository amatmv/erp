# -*- coding: utf-8 -*-
from addons import get_module_resource
from giscegas_facturacio_atr.giscegas_facturacio_atr import \
    LineProcessException
from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.osv import except_osv
from gestionatr.input.messages import B70, message


class TestFactura(testing.OOTestCase):

    require_demo_data = True

    def setUp(self):
        module_obj = self.openerp.pool.get('ir.module.module')

        newest_name = None

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            newest_id = module_obj.search(
                cursor, uid, [
                    ('name', 'like', 'giscegas_tarifas_peajes_20180101'),
                    ('state', '!=', 'installed')
                ], limit=1, order='name DESC'
            )
            if newest_id:
                newest_name = module_obj.read(
                    cursor, uid, newest_id[0], ['name']
                )['name']

        if newest_name:
            self.openerp.install_module(newest_name)
        return super(TestFactura, self).setUp()

    def test_check_in_invoice_exists_dso(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            inv_obj = self.openerp.pool.get('giscegas.facturacio.factura')
            imd_obj = self.openerp.pool.get('ir.model.data')

            partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_asus'
            )[1]
            other_partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_c2c'
            )[1]

            # found
            self.assertTrue(
                inv_obj.get_in_invoice_by_origin(
                    cursor, uid, 'ASUS00001', partner_id
                )
            )
            # Not Found Bad origin
            self.assertFalse(
                inv_obj.get_in_invoice_by_origin(
                    cursor, uid, 'NONASUS00001', partner_id
                )
            )
            # Not Found Bad partner
            self.assertFalse(
                inv_obj.get_in_invoice_by_origin(
                    cursor, uid, 'ASUS00001', other_partner_id
                )
            )

    def test_process_all_invoice_phase_2(self):
        imp_lin_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_obj = self.openerp.pool.get("account.invoice")

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_utils_gas.xml'
        )
        with open(xml_path, 'r') as f:
            xml_file = f.read()
            xml_file = xml_file.replace('2018-02-28', '2016-02-28')
            other_xml_file = xml_file.replace('12345', 'ASUS00001')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            b70_xml = B70(xml_file, 'B70')
            b70_xml.parse_xml()

            line_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr',
                'line_01_b70_import_01'
            )[1]

            factura = b70_xml.get_facturas_atr()[0]

            # With a different origen and distri it should work
            imp_lin_obj.process_all_invoice_phase_2(
                cursor, uid, line_id, factura
            )

            other_b70_xml = B70(other_xml_file, 'B70')
            other_b70_xml.parse_xml()
            other_factura = other_b70_xml.get_facturas_atr()[0]

            # With the same origen and different distri it should still work
            imp_lin_obj.process_all_invoice_phase_2(
                cursor, uid, line_id, other_factura
            )

            new_dist_id = imd_obj.get_object_reference(
                cursor, uid, 'giscemisc_participant', 'participant_res_partner_asus'
            )[1]
            imp_lin_obj.write(
                cursor, uid, line_id, {'distribuidora_id': new_dist_id}
            )

            # Same origin and distri but different year still should work
            imp_lin_obj.process_all_invoice_phase_2(
                cursor, uid, line_id, other_factura
            )

            # But not for same origen and same distri and same year
            inv_ids = inv_obj.search(cursor, uid, [('origin', '=', 'ASUS00001')])
            inv_obj.write(cursor, uid, inv_ids, {'origin_date_invoice': other_factura.fecfactura})
            expect(lambda: imp_lin_obj.process_all_invoice_phase_2(
                cursor, uid, line_id, other_factura
            )).to(raise_error(LineProcessException))

    def test_create_subtotal_line(self):
        fact_help_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.helper'
        )
        linia_obj = self.openerp.pool.get(
            'giscegas.facturacio.factura.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_utils_gas.xml'
        )
        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            b70_xml = B70(xml_file, 'B70')
            b70_xml.parse_xml()

            sub_type = 'tvr'
            fact_xml = b70_xml.get_facturas_atr()[0]
            lines_by_type = fact_xml.get_linies_factura_by_type()
            line_xml = lines_by_type['tvariable']

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
            )[1]
            desc_line_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'linia_gas_factura_0001'
            )[1]

            subtotal_id = fact_help_obj.create_subtotal_line(
                cursor, uid, sub_type, fact_xml, line_xml, fact_id,
                [desc_line_id]
            )

            assert subtotal_id
            assert linia_obj.search(cursor, uid, [('id', '=', subtotal_id)])

            linia = linia_obj.browse(cursor, uid, subtotal_id)
            assert linia.price_subtotal == line_xml['total']
            assert linia.tipus == 'subtotal_xml_tvr'

            linia_descomptada = linia_obj.browse(cursor, uid, desc_line_id)
            assert linia_descomptada.atrprice_subtotal == 0

    def test_write_lines_summary_creates_warning_when_total_is_incorrect(self):
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        fact_help_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.helper'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        expected_comment = \
            u'******* Fijo ******\n' \
            u'  P1: 100.0 dia (GAS) * 0.114873 €  = 11.49 €\n' \
            u'\n' \
            u'  Subt XML: 5.33 € - Subt calc: 11.49 € = -6.16 €\n'
        expected_error = \
            u'Error subtotal línia factura en fijo. XML: 5.33 IMP: 11.49'

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_utils_gas.xml'
        )
        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            file_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_dso_0001'
            )[1]
            line_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'linia_dso_0001'
            )[1]
            b70_xml = B70(xml_file, 'B70')
            b70_xml.parse_xml()
            fact_xml = b70_xml.get_facturas_atr()[0]
            lines_by_type = fact_xml.get_linies_factura_by_type()
            line_xml = lines_by_type['tfixe']

            fact_help_obj.write_lines_summary(
                cursor, uid, file_id, 'fijo', line_xml, fact_id, [line_id]
            )

            fact_vals = fact_obj.read(cursor, uid, fact_id, ['comment'])
            expect(fact_vals['comment']).to(equal(expected_comment))

            search_params = [('line_id', '=', file_id)]
            error_ids = error_obj.search(cursor, uid, search_params)
            expect(len(error_ids)).to(equal(1))
            error = error_obj.browse(cursor, uid, error_ids[0])
            expect(error.name).to(equal('2006'))
            expect(error.message).to(equal(expected_error))

    def test_write_lines_sum_doesnt_create_warning_when_total_is_correct(self):
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        fact_help_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.helper'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_utils_gas.xml'
        )
        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            file_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_dso_0001'
            )[1]
            line_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'linia_dso_0002'
            )[1]
            b70_xml = B70(xml_file, 'B70')
            b70_xml.parse_xml()
            fact_xml = b70_xml.get_facturas_atr()[0]
            lines_by_type = fact_xml.get_linies_factura_by_type()
            line_xml = lines_by_type['tvariable']

            fact_help_obj.write_lines_summary(
                cursor, uid, file_id, 'variable', line_xml, fact_id, [line_id]
            )

            search_params = [('line_id', '=', file_id)]
            error_ids = error_obj.search(cursor, uid, search_params)
            assert len(error_ids) == 0

    def test_write_lines_sum_doent_give_warning_with_less_1_cent_error(self):
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        fact_help_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.helper'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        ori_imp = '<importe>59.35</importe>'
        new_imp = '<importe>59.34</importe>'

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_utils_gas.xml'
        )
        with open(xml_path, 'r') as f:
            xml_file = f.read()
            # We change the total energy to check with a 1 cent error
            xml_file = xml_file.replace(ori_imp, new_imp)

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            file_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_dso_0001'
            )[1]
            line_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'linia_dso_0002'
            )[1]
            b70_xml = B70(xml_file, 'B70')
            b70_xml.parse_xml()
            fact_xml = b70_xml.get_facturas_atr()[0]
            lines_by_type = fact_xml.get_linies_factura_by_type()
            line_xml = lines_by_type['tvariable']

            fact_help_obj.write_lines_summary(
                cursor, uid, file_id, 'variable', line_xml, fact_id, [line_id]
            )

            search_params = [('line_id', '=', file_id)]
            error_ids = error_obj.search(cursor, uid, search_params)
            assert len(error_ids) == 0

    def test_create_invoice_by_type_works_with_rent(self):
        inv_line_obj = self.openerp.pool.get(
            'giscegas.facturacio.factura.linia'
        )
        fact_help_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.helper'
        )
        fact_obj = self.openerp.pool.get(
            'giscegas.facturacio.factura'
        )
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_utils_gas.xml'
        )
        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            b70_xml = B70(xml_file, 'B70')
            b70_xml.parse_xml()

            file_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_dso_0001'
            )[1]
            fact_xml = b70_xml.get_facturas_atr()[0]
            lines_by_type = fact_xml.get_linies_factura_by_type()
            line_xml = lines_by_type['lloguer']

            # We assign a ref value to the sender so that we have an import
            # configuration
            emisor_id = fact_obj.read(
                cursor, uid, fact_id, ['partner_id']
            )['partner_id'][0]
            part_obj = self.openerp.pool.get('giscemisc.participant')
            pemisor_id = part_obj.get_id_from_sifco(cursor, uid, '0031')
            part_obj.write(cursor, uid, pemisor_id, {'partner_id': emisor_id})

            inv_line_id = fact_help_obj.create_invoice_line_by_type(
                cursor, uid, 'lloguer', file_id, fact_xml, fact_id, line_xml
            )

            rent_line = inv_line_obj.browse(cursor, uid, inv_line_id)[0]
            assert rent_line.uos_id.name == 'ALQ/dia (GAS)'
            assert rent_line.tipus == 'lloguer'
            expect(rent_line.price_unit).to(equal(round(0.018929, 6)))
            # This is 0 now because we use subtotals
            expect(rent_line.price_subtotal).to(equal(0))

            subtotal_rent_line = inv_line_obj.browse(
                cursor, uid, inv_line_id
            )[1]
            expect(subtotal_rent_line.price_subtotal).to(equal(0.53))

    def test_create_invoice_by_type_works_with_tfijo(self):
        inv_line_obj = self.openerp.pool.get(
            'giscegas.facturacio.factura.linia'
        )
        fact_help_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.helper'
        )
        fact_obj = self.openerp.pool.get(
            'giscegas.facturacio.factura'
        )
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_utils_gas.xml'
        )
        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            b70_xml = B70(xml_file, 'B70')
            b70_xml.parse_xml()

            file_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_dso_0001'
            )[1]
            fact_xml = b70_xml.get_facturas_atr()[0]
            lines_by_type = fact_xml.get_linies_factura_by_type()
            line_xml = lines_by_type['tfixe']

            inv_line_id = fact_help_obj.create_invoice_line_by_type(
                cursor, uid, 'tfixe', file_id, fact_xml, fact_id, line_xml
            )

            price_unit = 0.190356164383
            inv_line = inv_line_obj.browse(cursor, uid, inv_line_id)[0]
            expect(inv_line.uos_id.name).to(equal('dia (GAS)'))
            expect(inv_line.tipus).to(equal('fijo'))
            expect(inv_line.price_unit).to(equal(round(price_unit, 6)))
            expect(inv_line.price_subtotal).to(equal(0))
            inv_line = inv_line_obj.browse(cursor, uid, inv_line_id)[1]
            expect(inv_line.tipus).to(equal('subtotal_xml_tfx'))
            expect(inv_line.price_subtotal).to(equal(5.33))

    def test_create_invoice_by_type_works_with_tvariable(self):
        inv_line_obj = self.openerp.pool.get(
            'giscegas.facturacio.factura.linia'
        )
        fact_help_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.helper'
        )
        fact_obj = self.openerp.pool.get(
            'giscegas.facturacio.factura'
        )
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_utils_gas.xml'
        )
        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            b70_xml = B70(xml_file, 'B70')
            b70_xml.parse_xml()

            file_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_dso_0001'
            )[1]
            fact_xml = b70_xml.get_facturas_atr()[0]
            lines_by_type = fact_xml.get_linies_factura_by_type()
            line_xml = lines_by_type['tvariable']

            # We assign a ref value to the sender so that we have an import
            # configuration
            emisor_id = fact_obj.read(
                cursor, uid, fact_id, ['partner_id']
            )['partner_id'][0]
            part_obj = self.openerp.pool.get('giscemisc.participant')
            pemisor_id = part_obj.get_id_from_sifco(cursor, uid, '0031')
            part_obj.write(cursor, uid, pemisor_id, {'partner_id': emisor_id})

            inv_line_id = fact_help_obj.create_invoice_line_by_type(
                cursor, uid, 'tvariable', file_id, fact_xml, fact_id, line_xml
            )

            price_unit = 0.02241300
            inv_line = inv_line_obj.browse(cursor, uid, inv_line_id)[0]
            expect(inv_line.uos_id.name).to(equal('kWh (GAS)'))
            expect(inv_line.tipus).to(equal('variable'))
            expect(inv_line.price_unit).to(equal(round(price_unit, 6)))
            expect(inv_line.price_subtotal).to(equal(0))
            inv_line = inv_line_obj.browse(cursor, uid, inv_line_id)[1]
            expect(inv_line.tipus).to(equal('subtotal_xml_tvr'))
            expect(inv_line.price_subtotal).to(equal(59.35))
