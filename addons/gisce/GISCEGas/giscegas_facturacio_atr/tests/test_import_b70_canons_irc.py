# -*- coding: utf-8 -*-

from test_import_b70 import TestImportB70
from destral import testing
from destral.transaction import Transaction
from expects import *
from addons import get_module_resource
from datetime import datetime
from ast import literal_eval as eval


class TestImportB7Canons(testing.OOTestCase):
    activar_polissa_CUPS_method = TestImportB70.__dict__["activar_polissa_CUPS"]
    add_supported_iva_to_concepts_method = TestImportB70.__dict__["add_supported_iva_to_concepts"]
    clean_hash_lines_method = TestImportB70.__dict__["clean_hash_lines"]
    add_supported_iva_to_concepts_method = TestImportB70.__dict__["add_supported_iva_to_concepts"]
    crear_modcon_method = TestImportB70.__dict__["crear_modcon"]

    def setUp(self):
        module_obj = self.openerp.pool.get('ir.module.module')
        newest_name = None

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            newest_id = module_obj.search(
                cursor, uid, [
                    ('name', 'like', 'giscegas_tarifas_peajes_20180101'),
                    ('state', '!=', 'installed')
                ], limit=1, order='name DESC'
            )
            if newest_id:
                newest_name = module_obj.read(
                    cursor, uid, newest_id[0], ['name']
                )['name']

        if newest_name:
            self.openerp.install_module(newest_name)
        return super(TestImportB7Canons, self).setUp()

    def prepare_for_phase_3(self, txn, context=None):
        if not context:
            context = {}
        polissa_obj = self.openerp.pool.get(
            'giscegas.polissa'
        )
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        pricelist_obj = self.openerp.pool.get('product.pricelist')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')
        cups_obj = self.openerp.pool.get('giscegas.cups.ps')
        address_obj = self.openerp.pool.get('res.partner.address')
        part_obj = self.openerp.pool.get('giscemisc.participant')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            context.get("test_xml", 'B70_canon_concepts.xml')
        )

        with open(xml_path, 'r') as f:
            self.xml_file_canon = f.read()

        cursor = txn.cursor
        uid = txn.user

        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_cups',
            context.get('cups', 'giscegas_cups_01')
        )[1]

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa',
            context.get('polissa', 'polissa_gas_0001')
        )[1]

        distri_id = part_obj.search(
            cursor, uid, [('codi_sifco', '=', '0021')]
        )
        if not distri_id:
            distri_id = part_obj.search(
                cursor, uid, [('codi_sifco', '=', '1234')]
            )[0]
        else:
            distri_id = distri_id[0]

        pricelist_id = pricelist_obj.search(
            cursor, uid, [('name', '=', 'TARIFAS GAS')]
        )[0]

        part_obj.write(cursor, uid, distri_id, {'codi_sifco': '1234'})
        partner_id = part_obj.get_partner_id(cursor, uid, distri_id)
        address_obj.create(cursor, uid, {'partner_id': partner_id})
        participant_base = part_obj.get_id_from_partner(cursor, uid, 1)
        part_obj.write(cursor, uid, participant_base, {'codi_sifco': '0706'})
        cups_obj.write(
            cursor, uid, cups_id, {'distribuidora_id': partner_id}
        )
        polissa_obj.write(
            cursor, uid, polissa_id, {
                'data_alta': context.get('data_alta', '2018-01-01'),
                'cups': cups_id,
                'state': 'activa',
            }
        )
        partner_obj.write(
            cursor, uid, partner_id, {
                'property_product_pricelist_purchase': pricelist_id
            }
        )
        self.activar_polissa_CUPS_method(txn, context=context)
        self.add_supported_iva_to_concepts_method(txn)
        self.clean_hash_lines_method(txn)

    def test_full_import_b70_with_canons(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )

        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        fact_line_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')

        imd_obj = self.openerp.pool.get('ir.model.data')

        canon_obj = self.openerp.pool.get('giscegas.canon.irc')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file_canon
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]

            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['lectures_processades']).to(equal(True))
            expect(line_vals['import_phase']).to(equal(40))
            imp_lect_obj = self.openerp.pool.get(
                'giscegas.facturacio.importacio.linia.lectures'
            )
            lectures_b70_ids = imp_lect_obj.search(
                cursor, uid, [('linia_id', '=', line_id)]
            )
            self.assertTrue(lectures_b70_ids)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.liniafactura_id).to(have_len(1))
            expect(line.liniafactura_id[0].rectificative_type).to(equal('N'))
            fact = line.liniafactura_id[0].factura_id
            self.assertEqual(fact.check_total, 65.32)
            self.assertEqual(fact.amount_total, 270.05)  # total + concepts
            self.assertEqual(len(fact.lectures_ids), 1)
            self.assertEqual(fact.lectures_ids[0].lect_actual, 10063.00)
            self.assertEqual(fact.lectures_ids[0].pressio_subministrament, 0.05)
            self.assertEqual(fact.origin, "12345")
            self.assertEqual(fact.tarifa_acces_id.name, "3.1")
            self.assertEqual(fact.type, "in_invoice")
            pol = fact.polissa_id
            compt = False
            for compta in pol.comptadors:
                if compta.name == "704414207":
                    compt = compta
            self.assertTrue(compt)
            self.assertTrue(compt.active)
            lec = False
            for l in compt.pool_lectures:
                if l.name == "2018-02-28":
                    lec = l
            self.assertTrue(lec)
            self.assertEqual(lec.lectura, 10063.00)
            self.assertEqual(lec.factor_k, 0.979900)
            self.assertEqual(lec.pcs, 11.698000)

            canon_id = canon_obj.search(
                cursor, uid,
                [
                    ('polissa_id', '=', pol.id)
                ]
            )

            self.assertEqual(len(canon_id), 1)
            canon = canon_obj.browse(cursor, uid, canon_id[0], [])

            expected_product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_comer', 'concepte_gas_1928'
            )[1]

            self.assertEqual(canon.canon_product_id.id, expected_product_id)
            self.assertEqual(canon.data_alta, '2018-02-01')

            # < fecdesde > 2018 - 02 - 01 < / fecdesde >
            # < fechasta > 2018 - 02 - 28 < / fechasta >
            # < unidad > 28 < / unidad >
            # < precunidad > 6.043 < / precunidad >
            # < importe > 169.204 < / importe >

            expected_price_per_day = float(169.204) / float(28)
            self.assertEqual(canon.preu_dia, expected_price_per_day)

            # Invoicing Fase -> factura from polissa

            wiz_fact_manual_obj = self.openerp.pool.get(
                'giscegas.wizard.manual.invoice')

            wiz_fact_manual_id = wiz_fact_manual_obj.create(cursor, uid, vals={
                'polissa_id': pol.id,
                'date_start': datetime(
                    year=2018, month=2, day=1).strftime('%Y-%m-%d'),
                'date_end': datetime(
                    year=2018, month=2, day=25).strftime('%Y-%m-%d'),
                'date_invoice': datetime(
                    year=2018, month=2, day=25).strftime('%Y-%m-%d'),
                'journal_id': fact.journal_id.id
            })

            #   -   Get Facturacio's PriceList

            pricelist_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio',
                'pricelist_tarifas_gas'
            )[1]
            context = {'llista_preu': pricelist_id}

            #   -   Create regular invoice within ExtraLine's dates

            wiz_fact_manual_obj.action_manual_invoice(
                cursor, uid, [wiz_fact_manual_id], context)

            generated_fact = wiz_fact_manual_obj.read(
                cursor, uid, wiz_fact_manual_id, ['invoice_ids']
            )[0]['invoice_ids']

            generated_fact_id = eval(generated_fact)[0]

            gas_fact = fact_obj.browse(cursor, uid, generated_fact_id)

            fact_line_ids = fact_line_obj.search(
                cursor, uid, [('factura_id', '=', generated_fact_id),
                              ('name', 'ilike', 'CANON%')
                              ]
            )

            self.assertTrue(fact_line_ids)

            tax_obj = self.openerp.pool.get('account.tax')

            for f_lline in fact_line_obj.read(cursor, uid, fact_line_ids, []):
                tax = tax_obj.read(
                    cursor, uid, f_lline['invoice_line_tax_id'][0], ['amount']
                )['amount']
                self.assertEqual(tax, 0.21)

    def test_b7032_generates_canon(self):
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_32_canon_concepts.xml'
        )

        xml_path_second = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_32_canon_concepts_second_import_rm_upd.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with open(xml_path_second, 'r') as f:
            xml_file_second = f.read()


        wiz_obj = self.openerp.pool.get(
            'wizard.importacio.b70'
        )
        import_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio'
        )
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        inv_obj = self.openerp.pool.get(
            'account.invoice'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            conf_obj = self.openerp.pool.get("giscegas.facturacio.atr.config")
            conf_ids = conf_obj.search(cursor, uid, [])
            conf_obj.write(
                cursor, uid, conf_ids,
                {
                    'b7032_generates_invoice': True,
                    'b7032_same_num_generates_invoice': True
                }
            )
            # First we import the B7032.
            # It has 2 invoices so it should generate 2 import lines

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]

            canon_obj = self.openerp.pool.get('giscegas.canon.irc')

            # Importamos otro b70 con el canon actualizado
            canon_ids = canon_obj.search(cursor, uid, [])

            self.assertEqual(len(canon_ids), 2)

            cano_fields_origin = canon_obj.read(cursor, uid, canon_ids, [])

            canon_irc_3_origin = False
            canon_irc_origin = False
            for canon in cano_fields_origin:
                if canon['codi_cnmc'] == u'1928':
                    canon_irc_3_origin = canon
                elif canon['codi_cnmc'] == u'1911':
                    canon_irc_origin = canon

            self.assertTrue(canon_irc_origin)
            self.assertTrue(canon_irc_3_origin)

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file_second
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]

            canon_ids = canon_obj.search(cursor, uid, [])

            self.assertEqual(len(canon_ids), 1)

            cano_fields_post = canon_obj.read(cursor, uid, canon_ids, [])

            canon_irc_3_post = cano_fields_post[0]

            self.assertEqual(
                canon_irc_3_origin['codi_cnmc'],
                canon_irc_3_post['codi_cnmc']
            )
            self.assertEqual(
                canon_irc_3_origin['id'],
                canon_irc_3_post['id']
            )
            self.assertEqual(
                canon_irc_3_origin['data_alta'],
                canon_irc_3_post['data_alta']
            )
            self.assertNotEqual(
                canon_irc_3_origin['preu_dia'],
                canon_irc_3_post['preu_dia']
            )

            canon_ids = canon_obj.search(
                cursor, uid, [], context={'active_test': False}
            )

            self.assertEqual(len(canon_ids), 2)

            canon_irc_1_baixa_id = canon_obj.search(
                cursor, uid, [('codi_cnmc', '=', '1911')],
                context={'active_test': False}
            )
            self.assertEqual(len(canon_irc_1_baixa_id), 1)

            canon_irc_post = canon_obj.read(
                cursor, uid, canon_irc_1_baixa_id[0], []
            )
            self.assertTrue(canon_irc_post['data_baixa'])
