# -*- coding: utf-8 -*-
import base64

from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.osv import except_osv
from addons import get_module_resource


class TestReImportWizardB70(testing.OOTestCase):

    require_demo_data = True

    def test_wizard_reimport_b70(self):
        """ Test que comprova que el wizard "wizard.reimportacio.b70" reimporti
        correctament els fitxers """

        imd = self.openerp.pool.get('ir.model.data')
        wiz_obj = self.openerp.pool.get('wizard.reimportacio.b70')
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_ids = []

            import_ids.append(imd.get_object_reference(
                cursor, uid,
                'giscegas_facturacio_atr', 'b70_import_01'
            )[1])
            import_ids.append(imd.get_object_reference(
                cursor, uid,
                'giscegas_facturacio_atr', 'b70_import_02'
            )[1])

            vals = {
                'state': 'init'
            }
            context = {
                'active_ids': import_ids
            }
            wiz_id = wiz_obj.create(cursor, uid, vals, context)

            self.assertTrue(
                wiz_obj.action_reimport_b70_lines(cursor, uid, [wiz_id], context)
            )

        return False

    def test_wizard_reimport_b70_only_allow_xml(self):
        zipfile_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_fail_import_gas.xml.zip'
        )
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_fail_import_gas.xml'
        )
        with open(zipfile_path, 'r') as f:
            zipfile = f.read()

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        imd = self.openerp.pool.get('ir.model.data')
        wiz_obj = self.openerp.pool.get('giscegas.facturacio.atr.wizard')
        wiz = None
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            active_id = imd.get_object_reference(
                cursor, uid,
                'giscegas_facturacio_atr', 'line_01_b70_import_01'
            )[1]
            values = {
                'file':  base64.b64encode(zipfile),
                'filename': 'XML_FILE.zip'
            }
            ctx = {
                'fitxer_xml': True,
                'origen': 'nou',
                'active_id': active_id
            }
            wiz_id = wiz_obj.create(cursor, uid, values, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            expect(
                lambda: wiz.action_importar_b70(context=ctx)
            ).to(
                raise_error(except_osv,
                            'warning -- Error\n\n'
                            'El fitxer a reimportar ha de ser un fitxer XML')
            )
            values = {
                'file':  base64.b64encode(xml_file),
                'filename': 'XML_FILE_gas.xml'
            }
            wiz_id = wiz_obj.create(cursor, uid, values, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.action_importar_b70(context=ctx)
            expect(wiz.state).to(equal('done'))
            expect(wiz.info).to(equal('Fitxer processat.'))




