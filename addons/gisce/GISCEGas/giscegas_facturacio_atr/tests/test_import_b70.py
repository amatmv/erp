# -*- coding: utf-8 -*-
import unittest
import base64

from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.osv import except_osv
from addons import get_module_resource
from gestionatr.input.messages import B70
from datetime import datetime, timedelta
from destral.patch import PatchNewCursors


class TestImportB70(testing.OOTestCase):
    def setUp(self):
        module_obj = self.openerp.pool.get('ir.module.module')
        newest_name = None

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            newest_id = module_obj.search(
                cursor, uid, [
                    ('name', 'like', 'giscegas_tarifas_peajes_20180101'),
                    ('state', '!=', 'installed')
                ], limit=1, order='name DESC'
            )
            if newest_id:
                newest_name = module_obj.read(
                    cursor, uid, newest_id[0], ['name']
                )['name']

            newest_id = module_obj.search(
                cursor, uid, [
                    ('name', 'like', 'giscegas_tarifas_peajes_20190101'),
                    ('state', '!=', 'installed')
                ], limit=1, order='name DESC'
            )
            if newest_id:
                newest_name = module_obj.read(
                    cursor, uid, newest_id[0], ['name']
                )['name']

        if newest_name:
            self.openerp.install_module(newest_name)
        return super(TestImportB70, self).setUp()

    def clean_hash_lines(self, txn):
        cursor = txn.cursor
        uid = txn.user
        line_obj = self.openerp.pool.get('giscegas.facturacio.importacio.linia')
        lids = line_obj.search(cursor, uid, [])
        line_obj.write(cursor, uid, lids, {"md5_hash": "123456789", 'index_factura': 9})

    def test_ignore_reference_is_false_by_default(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            distri_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_partner'
            )[1]

            vals = {
                'name': 'Import Name',
                'importacio_id': import_id,
                'distribuidora_id': distri_id,
            }
            line_id = line_obj.create(cursor, uid, vals)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['ignore_reference']).to(equal(False))

    def test_b70_with_reference_invoice_dont_import_by_default(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        cups_obj = self.openerp.pool.get('giscegas.cups.ps')
        part_obj = self.openerp.pool.get('giscemisc.participant')
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_rectificadora_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_cups', 'giscegas_cups_01'
            )[1]
            distri_id = part_obj.search(
                cursor, uid, [('codi_sifco', '=', '0021')]
            )[0]
            part_obj.write(cursor, uid, distri_id, {'codi_sifco': '1234'})
            distri_id = part_obj.get_partner_id(cursor, uid, distri_id)
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2018-01-01',
                    'data_baixa': '2019-01-01'
                }
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id[0])
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(20))
            expect(line_vals['lectures_processades']).to(equal(False))
            expect(len(line_vals['error_ids'])).to(equal(1))

            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])
            expect(error.error_template_id.phase).to(equal('2'))
            expect(error.error_template_id.code).to(equal('007'))

    def test_b70_with_reference_invoice_dont_import_when_no_code(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        f_obj = self.openerp.pool.get('giscegas.facturacio.factura')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_rectificadora_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
            xml_file = xml_file.replace(
                "<numfactorigen>12345</numfactorigen>",
                ""
            )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # First we import a correct B70 to have an invoice in the db
            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))

            # Delete origin from new invoice
            factura = f_obj.browse(cursor, uid, line.liniafactura_id[0].factura_id.id)
            factura.write({'origin': False})
            factura = f_obj.browse(cursor, uid, line.liniafactura_id[0].factura_id.id)
            self.assertFalse(factura.origin)

            # Now we import a rectificative without ref code
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(20))
            expect(len(line_vals['error_ids'])).to(equal(1))

            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])
            expect(error.error_template_id.phase).to(equal('2'))
            expect(error.error_template_id.code).to(equal('007'))

    def test_b70_with_reference_invoice_imports_on_set_ignore_true(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        temp_err_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error.template'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_rectificadora_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            self.prepare_for_phase_3(txn)
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )
            line_obj.write(cursor, uid, line_id, {'ignore_reference': True})

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('valid'))

            error = error_obj.read(
                cursor, uid, line_vals['error_ids'], ['error_template_id']
            )
            template_ids = [
                err_vals['error_template_id'][0]
                for err_vals in error
            ]
            error_templates = [
                {'phase': temp_err_vals['phase'], 'code': temp_err_vals['code']}
                for temp_err_vals in temp_err_obj.read(
                    cursor, uid, template_ids, ['phase', 'code']
                )
            ]
            expect(error_templates).to(contain({'phase': '2', 'code': '012'}))

    def activar_polissa_CUPS(self, txn, context=None):
        if context is None:
            context = {}
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscegas.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', context.get('polissa', 'polissa_gas_0001')
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])
        polissa = polissa_obj.browse(cursor, uid, polissa_id).modcontractual_activa.write({'data_final': '2040-01-01'})

    def crear_modcon(self, txn, caudal_diario, ini, fi, tariff_id=None):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'caudal_diario': caudal_diario})

        if tariff_id:
            polissa_obj.write(cursor, uid, polissa_id, {'tarifa': tariff_id})

        wz_crear_mc_obj = pool.get('giscegas.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def add_supported_iva_to_concepts(self, txn, iva='21'):
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = '{}% IVA Soportado (operaciones corrientes)'.format(iva)
        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        no_iva_concepts = []
        product_ids = product_obj.search(
            cursor, uid, [
                '|',
                ('default_code', 'like', 'CON'),
                ('default_code', 'like', 'ALQ'),
                ('default_code', 'not in', no_iva_concepts)
            ]
        )
        product_obj.write(
            cursor, uid, product_ids, {
                'supplier_taxes_id': [(6, 0, iva_id)],
                'taxes_id': [(6, 0, iva_id)],
            }
        )

    def prepare_for_phase_3(self, txn, context=None):
        if not context:
            context = {}
        polissa_obj = self.openerp.pool.get(
            'giscegas.polissa'
        )
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        pricelist_obj = self.openerp.pool.get('product.pricelist')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')
        cups_obj = self.openerp.pool.get('giscegas.cups.ps')
        address_obj = self.openerp.pool.get('res.partner.address')
        part_obj = self.openerp.pool.get('giscemisc.participant')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            context.get("test_xml", 'test_utils_gas.xml')
        )

        with open(xml_path, 'r') as f:
            self.xml_file = f.read()

        conceptes_xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_conceptes_gas.xml'
        )

        with open(conceptes_xml_path, 'r') as f:
            self.conceptes_xml_file = f.read()

        cursor = txn.cursor
        uid = txn.user

        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_cups', context.get('cups', 'giscegas_cups_01')
        )[1]
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', context.get('polissa', 'polissa_gas_0001')
        )[1]
        import_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
        )[1]
        distri_id = part_obj.search(
            cursor, uid, [('codi_sifco', '=', '0021')]
        )
        if not distri_id:
            distri_id = part_obj.search(
                cursor, uid, [('codi_sifco', '=', '1234')]
            )[0]
        else:
            distri_id = distri_id[0]

        pricelist_id = pricelist_obj.search(
            cursor, uid, [('name', '=', 'TARIFAS GAS')]
        )[0]
        self.line_id = line_obj.create_from_xml(
            cursor, uid, import_id, 'Import Name', self.xml_file
        )
        line_obj.write(cursor, uid, self.line_id, {'cups_id': cups_id})

        part_obj.write(cursor, uid, distri_id, {'codi_sifco': '1234'})
        partner_id = part_obj.get_partner_id(cursor, uid, distri_id)
        address_obj.create(cursor, uid, {'partner_id': partner_id})
        participant_base = part_obj.get_id_from_partner(cursor, uid, 1)
        part_obj.write(cursor, uid, participant_base, {'codi_sifco': '0706'})
        cups_obj.write(
            cursor, uid, cups_id, {'distribuidora_id': partner_id}
        )
        polissa_obj.write(
            cursor, uid, polissa_id, {
                'data_alta': context.get('data_alta', '2018-01-01'),
                'cups': cups_id,
                'state': 'activa',
                'distribuidora': partner_id
            }
        )
        partner_obj.write(
            cursor, uid, partner_id, {
                'property_product_pricelist_purchase': pricelist_id
            }
        )
        self.activar_polissa_CUPS(txn, context=context)
        self.add_supported_iva_to_concepts(txn)
        self.clean_hash_lines(txn)

    def test_validation_3001(self):
        validator_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.validator'
        )
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_utils_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
            xml_file = xml_file.replace("<consumo>231.00</consumo>", "<consumo>200.00</consumo>")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.prepare_for_phase_3(txn)

            b70 = B70(xml_file)
            b70.parse_xml()

            error_ids, has_critical = validator_obj.validate_b70(
                cursor, uid, self.line_id[0], b70, 0, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '001', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_validation_3007(self):
        validator_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.validator'
        )
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        lectures_obj = self.openerp.pool.get('giscegas.lectures.lectura.pool')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            b70 = B70(self.xml_file)
            b70.parse_xml()

            line = line_obj.browse(cursor, uid, self.line_id)[0]

            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'p1_v_tarifa_31_gas'
            )[1]
            distri_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_partner'
            )[1]
            line.cups_id.polissa_polissa.comptadors[0].write({'name': '704414207'})
            compt_id = line.cups_id.polissa_polissa.comptadors[0].id
            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'origen_gas_1'
            )[1]

            lectures_obj.create(
                cursor, uid, {
                    'periode': periode_id,
                    'name': '2018-01-31',
                    'origen_comer_id': distri_id,
                    'comptador': compt_id,
                    'origen_id': origen_id,
                    'lectura': 11,
                    'pressio_subministrament': 1,
                    'factor_k': 1,
                    'pcs': 1,
                }
            )

            error_ids, has_critical = validator_obj.validate_b70(
                cursor, uid, self.line_id[0], b70, 0, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '007', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_full_import_works(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['lectures_processades']).to(equal(True))
            expect(line_vals['import_phase']).to(equal(40))
            imp_lect_obj = self.openerp.pool.get(
                'giscegas.facturacio.importacio.linia.lectures'
            )
            lectures_b70_ids = imp_lect_obj.search(
                cursor, uid, [('linia_id', '=', line_id)]
            )
            self.assertTrue(lectures_b70_ids)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.liniafactura_id).to(have_len(1))
            expect(line.liniafactura_id[0].rectificative_type).to(equal('N'))
            fact = line.liniafactura_id[0].factura_id
            self.assertEqual(fact.check_total, 65.32)
            self.assertEqual(fact.amount_total, 65.32)
            self.assertEqual(len(fact.lectures_ids), 1)
            self.assertEqual(fact.lectures_ids[0].lect_actual, 10063.00)
            self.assertEqual(fact.lectures_ids[0].pressio_subministrament, 0.05)
            self.assertEqual(fact.origin, "12345")
            self.assertEqual(fact.tarifa_acces_id.name, "3.1")
            self.assertEqual(fact.type, "in_invoice")
            pol = fact.polissa_id
            compt = False
            for compta in pol.comptadors:
                if compta.name == "704414207":
                    compt = compta
            self.assertTrue(compt)
            self.assertTrue(compt.active)
            lec = False
            for l in compt.pool_lectures:
                if l.name == "2018-02-28":
                    lec = l
            self.assertTrue(lec)
            self.assertEqual(lec.lectura, 10063.00)
            self.assertEqual(lec.factor_k, 0.979900)
            self.assertEqual(lec.pcs, 11.698000)

    def test_pressio_arriba_en_milibars(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {"test_xml": 'test_utils_gas_pressio_milibars.xml'}
            self.prepare_for_phase_3(txn, context=context)

            conf_o = self.openerp.pool.get("giscegas.facturacio.atr.config")
            conf_id = conf_o.search(cursor, uid, [('emissor.codi_sifco', '=', '1234')])
            conf_o.write(
                cursor, uid, conf_id,
                {'uom_pressio_f': imd_obj.get_object_reference(cursor, uid, 'giscegas_polissa', 'product_uom_pressio_milibar')[1]}
            )

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['lectures_processades']).to(equal(True))
            expect(line_vals['import_phase']).to(equal(40))
            imp_lect_obj = self.openerp.pool.get(
                'giscegas.facturacio.importacio.linia.lectures'
            )
            lectures_b70_ids = imp_lect_obj.search(
                cursor, uid, [('linia_id', '=', line_id)]
            )
            self.assertTrue(lectures_b70_ids)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.liniafactura_id).to(have_len(1))
            expect(line.liniafactura_id[0].rectificative_type).to(equal('N'))
            fact = line.liniafactura_id[0].factura_id
            self.assertEqual(fact.check_total, 65.32)
            self.assertEqual(fact.amount_total, 65.32)
            self.assertEqual(len(fact.lectures_ids), 1)
            self.assertEqual(fact.lectures_ids[0].lect_actual, 10063.00)
            self.assertEqual(fact.lectures_ids[0].pressio_subministrament, 0.5005)
            self.assertEqual(fact.origin, "12345")
            self.assertEqual(fact.tarifa_acces_id.name, "3.1")
            self.assertEqual(fact.type, "in_invoice")
            pol = fact.polissa_id
            compt = False
            for compta in pol.comptadors:
                if compta.name == "704414207":
                    compt = compta
            self.assertTrue(compt)
            self.assertTrue(compt.active)
            lec = False
            for l in compt.pool_lectures:
                if l.name == "2018-02-28":
                    lec = l
            self.assertTrue(lec)
            self.assertEqual(lec.lectura, 10063.00)
            self.assertEqual(lec.factor_k, 0.979900)
            self.assertEqual(lec.pcs, 11.698000)

    def test_b70_anuladora_A_ok(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_anuladora_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
            xml_file = xml_file.replace("<clasefact>B</clasefact>", "<clasefact>A</clasefact>")

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)
            # Import original invoice
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.state).to(equal('valid'))
            expect(line.lectures_processades).to(equal(True))
            expect(line.import_phase).to(equal(40))

            # Import anuladora invoice
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_02'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.liniafactura_id).to(have_len(1))
            expect(line.liniafactura_id[0].rectificative_type).to(equal('A'))
            fact = line.liniafactura_id[0].factura_id
            self.assertEqual(fact.check_total, 65.32)
            self.assertEqual(fact.amount_total, 65.32)
            # Comprovem que les lectures s'hagin eliminat
            lect_obj = self.openerp.pool.get("giscegas.lectures.lectura.pool")
            lids = lect_obj.search(cursor, uid, [('name', '=', '2018-02-28')])
            self.assertEqual(len(lids), 0)

    def test_b70_anuladora_negativa_A_ok(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B7031_anulador_import_negatiu.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
            xml_file = xml_file.replace("<clasefact>B</clasefact>", "<clasefact>A</clasefact>")

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)
            # Import original invoice
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.state).to(equal('valid'))
            expect(line.lectures_processades).to(equal(True))
            expect(line.import_phase).to(equal(40))

            # Import rectifyer invoice
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_02'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.liniafactura_id).to(have_len(1))
            expect(line.liniafactura_id[0].rectificative_type).to(equal('A'))
            fact = line.liniafactura_id[0].factura_id
            self.assertEqual(fact.check_total, 76.73)
            self.assertEqual(fact.amount_total, 76.73)
            # Comprovem que les lectures s'hagin eliminat
            lect_obj = self.openerp.pool.get("giscegas.lectures.lectura.pool")
            lids = lect_obj.search(cursor, uid, [('name', '=', '2019-01-04')])
            self.assertEqual(len(lids), 0)

    def test_b70_anuladora_B_ok(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_anuladora_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)
            # Import original invoice
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.state).to(equal('valid'))
            expect(line.lectures_processades).to(equal(True))
            expect(line.import_phase).to(equal(40))

            # Import rectifyer invoice
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_02'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.liniafactura_id).to(have_len(1))
            expect(line.liniafactura_id[0].rectificative_type).to(equal('B'))
            fact = line.liniafactura_id[0].factura_id
            self.assertEqual(fact.check_total, 65.32)
            self.assertEqual(fact.amount_total, 65.32)
            # Comprovem que les lectures no s'hagin eliminat
            lect_obj = self.openerp.pool.get("giscegas.lectures.lectura.pool")
            lids = lect_obj.search(cursor, uid, [('name', '=', '2018-02-28')])
            self.assertEqual(len(lids), 1)

    def test_b70_rectifyer_ok(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_rectificadora_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)
            # Import original invoice
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.state).to(equal('valid'))
            expect(line.lectures_processades).to(equal(True))
            expect(line.import_phase).to(equal(40))

            # Import rectifyer invoice
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_02'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.liniafactura_id).to(have_len(1))
            expect(line.liniafactura_id[0].rectificative_type).to(equal('R'))
            fact = line.liniafactura_id[0].factura_id
            self.assertEqual(fact.check_total, 49.13)
            self.assertEqual(fact.amount_total, 49.13)
            self.assertEqual(len(fact.lectures_ids), 1)
            self.assertEqual(fact.lectures_ids[0].lect_actual, 10000.00)
            self.assertEqual(fact.origin, "123456")
            self.assertEqual(fact.ref.origin, "12345")
            self.assertEqual(fact.tarifa_acces_id.name, "3.1")
            self.assertEqual(fact.type, "in_invoice")
            pol = fact.polissa_id
            compt = False
            for compta in pol.comptadors:
                if compta.name == "704414207":
                    compt = compta
            self.assertTrue(compt)
            self.assertTrue(compt.active)
            lec = False
            for l in compt.pool_lectures:
                if l.name == "2018-02-28":
                    lec = l
            self.assertTrue(lec)
            self.assertEqual(lec.lectura, 10000.00)
            self.assertEqual(lec.factor_k, 0.979900)
            self.assertEqual(lec.pcs, 11.698000)
            msg = u"Valor de lectura sobreescrit pel valor d'una rectificadora. Valor antic: 10063. Valor nou: 10000.0"
            self.assertEqual(lec.observacions, msg)

    def test_b70_rectifyer_needs_anuladora_B(self):  # TODO GAS
        return

    def test_b7032_generates_proforma_and_multipunt_and_aprove_invoices(self):
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_B7032.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        wiz_obj = self.openerp.pool.get(
            'wizard.importacio.b70'
        )
        import_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio'
        )
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        inv_obj = self.openerp.pool.get(
            'account.invoice'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            conf_obj = self.openerp.pool.get("giscegas.facturacio.atr.config")
            conf_ids = conf_obj.search(cursor, uid, [])
            conf_obj.write(cursor, uid, conf_ids, {'b7032_generates_invoice': True, 'b7032_same_num_generates_invoice': True})
            # First we import the B7032.
            # It has 2 invoices so it should generate 2 import lines
            vals = {
                'file': base64.b64encode(xml_file),
                'filename': 'FILE_B7032_gas.xml'
            }
            ctx = {}
            self.prepare_for_phase_3(txn)
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            before_ids = import_obj.search(cursor, uid, [])
            wiz.action_importar_b70()
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            expect('done').to(equal(wiz.state))
            after_ids = import_obj.search(cursor, uid, [])
            new_ids = list(set(after_ids) - set(before_ids))
            expect(len(new_ids)).to(equal(1))
            import_data = import_obj.read(cursor, uid, new_ids[0], [])
            lines = import_data['linia_ids']
            self.assertEquals(len(lines), 2)

            # Now we process and check the first line.
            # It should generate an invoice in proforma
            line_obj.process_line_sync(cursor, uid, lines[0])
            line_vals = line_obj.browse(cursor, uid, lines[0])
            expect(line_vals.state).to(equal('valid'))
            expect(line_vals.import_phase).to(equal(40))
            self.assertEquals(len(line_vals.liniafactura_id), 1)
            fact = line_vals.liniafactura_id[0].factura_id
            self.assertEquals(fact.state, 'draft')
            self.assertEquals(fact.amount_total, 65.32)
            self.assertEqual(len(fact.lectures_ids), 1)
            self.assertEqual(fact.lectures_ids[0].lect_actual, 10063.00)
            self.assertEqual(fact.origin, "12345 - AA")
            self.assertEqual(fact.tarifa_acces_id.name, "3.1")
            self.assertEqual(fact.type, "in_invoice")
            # We can search the multipunt by origin and we SHOULDN'T find it
            gid = inv_obj.search(cursor, uid, [('origin', '=', "12345")])
            self.assertEquals(len(gid), 0)
            # Finally we process and check the second line.
            # As it's the lastone of the B7032 it should generate the proforma and the multipunt invoice
            self.prepare_for_phase_3(txn, context={'cups': 'giscegas_cups_02', 'polissa': 'polissa_gas_0002'})
            line_obj.write(cursor, uid, lines[1], {'index_factura': 1})
            line_obj.process_line_sync(cursor, uid, lines[1])
            line_vals = line_obj.browse(cursor, uid, lines[1])
            expect(line_vals.state).to(equal('valid'))
            expect(line_vals.import_phase).to(equal(40))
            self.assertEquals(len(line_vals.liniafactura_id), 1)
            fact = line_vals.liniafactura_id[0].factura_id
            self.assertEquals(fact.state, 'draft')
            self.assertEquals(fact.amount_total, 49.13)
            self.assertEqual(len(fact.lectures_ids), 1)
            self.assertEqual(fact.lectures_ids[0].lect_actual, 10000.00)
            self.assertEqual(fact.origin, "12345 - BB")
            self.assertEqual(fact.tarifa_acces_id.name, "3.1")
            self.assertEqual(fact.type, "in_invoice")
            # We can search the multipunt by origin and we SHOULD find it
            gid = inv_obj.search(cursor, uid, [('origin', '=', "12345")])
            self.assertEquals(len(gid), 1)
            fact = inv_obj.browse(cursor, uid, gid[0])
            self.assertEquals(fact.amount_total, 114.45)
            self.assertEquals(fact.check_total, 114.45)
            self.assertEquals(fact.origin, "12345")
            self.assertEquals(fact.state, "draft")
            # We aprove the invoices. Aproving one of the proformas the grouped
            # should be opened
            wiz_aprovar = self.openerp.pool.get('giscegas.facturacio.atr.aprovar.factura')
            ctx['model'] = 'giscegas.facturacio.importacio'
            ctx['active_ids'] = [line_vals.importacio_id.id]
            ids = wiz_aprovar.create(cursor, uid, {}, context=ctx)
            with PatchNewCursors():
                wiz_aprovar.action_aprovar_factures(cursor, uid, [ids], context=ctx)
            fact = inv_obj.browse(cursor, uid, gid[0])
            self.assertEquals(fact.state, "open")
            factura = line_vals.liniafactura_id[0].factura_id.browse()[0]
            self.assertEquals(factura.state, "proforma2")

    def test_b7033_creates_account_invoice(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'test_B7033.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            partner_obj = self.openerp.pool.get("res.partner")
            partner_obj.write(cursor, uid, partner_obj.search(cursor, uid, []), {
                'payment_type_supplier': 2
            })
            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['lectures_processades']).to(equal(False))
            expect(line_vals['import_phase']).to(equal(40))
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.linia_account_invoice_id).to(have_len(1))
            expect(line.linia_account_invoice_id[0].rectificative_type).to(equal('N'))
            fact = line.linia_account_invoice_id[0]
            self.assertEqual(fact.check_total, 114.45)
            self.assertEqual(fact.amount_total, 114.45)
            self.assertEqual(fact.payment_type.id, fact.partner_id.payment_type_supplier.id)
            self.assertEqual(fact.partner_id.name, u'IBERDROLA DISTRIBUCIÓN ELÉCTRICA, S.A.U')
            self.assertEqual(fact.origin, "12345")
            self.assertEqual(fact.type, "in_invoice")

    def test_we_import_extra_lines_correctly(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_path = get_module_resource(
                'giscegas_facturacio_atr', 'tests', 'fixtures',
                'test_utils_amb_conceptes_gas.xml'
            )

            with open(xml_path, 'r') as f:
                xml_file = f.read()

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.browse(cursor, uid, line_id)[0]
            expect(line_vals.state).to(equal('valid'))
            expect(line_vals.import_phase).to(equal(40))
            expect(line_vals.liniafactura_id[0].factura_id.amount_total).to(equal(170.18))

            expect(len(line_vals.liniaextra_id)).to(equal(1))

            extra_line = line_vals.liniaextra_id[0]

            expect(len(extra_line.tax_ids)).to(equal(1))
            expect(
                {tax.name for tax in extra_line.tax_ids}
            ).to(
                equal(
                    {'21% IVA Soportado (operaciones corrientes)'}
                )
            )

    def test_tarif_is_updated(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            self.xml_file = self.xml_file.replace(
                "<tipopeaje>31</tipopeaje>", "<tipopeaje>32</tipopeaje>")
            self.xml_file = self.xml_file.replace(
                "<qdcontratado>5</qdcontratado>", "<qdcontratado>50</qdcontratado>")

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['lectures_processades']).to(equal(True))
            expect(line_vals['import_phase']).to(equal(40))
            imp_lect_obj = self.openerp.pool.get(
                'giscegas.facturacio.importacio.linia.lectures'
            )
            lectures_b70_ids = imp_lect_obj.search(
                cursor, uid, [('linia_id', '=', line_id)]
            )
            self.assertTrue(lectures_b70_ids)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.liniafactura_id).to(have_len(1))
            expect(line.liniafactura_id[0].rectificative_type).to(equal('N'))
            pol = line.liniafactura_id[0].factura_id.polissa_id
            self.assertEqual(pol.tarifa.name, "3.2")
            self.assertEqual(len(pol.modcontractuals_ids), 2)
            mod = pol.modcontractual_activa
            self.assertEqual(mod.data_inici, "2018-02-01")
            self.assertTrue(mod.active)

            error_ids = error_obj.read(cursor, uid, line_vals['error_ids'])
            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_template_id')
                error_code.pop('id')
                error_code.pop('line_id')
                error_code.pop('message')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'name': '4001', 'level': 'information', 'active': True})
            )

    def test_new_meter_is_created(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        compt_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            old_compt_id = compt_obj.search_by_name(cursor, uid, '1111111')[0]
            compt_obj.write(
                cursor, uid, old_compt_id, {
                    'active': False,
                    'data_baixa': '2018-02-01'
                }
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            old_compt_vals = compt_obj.read(cursor, uid, old_compt_id)
            expect(old_compt_vals['active']).to(equal(False))
            expect(old_compt_vals['data_alta']).to(equal('2018-01-01'))
            expect(old_compt_vals['data_baixa']).to(equal('2018-02-01'))

            new_compt_id = compt_obj.search_by_name(cursor, uid, '704414207')[0]
            new_compt_vals = compt_obj.read(cursor, uid, new_compt_id)
            expect(new_compt_vals['name']).to(equal('704414207'))
            expect(new_compt_vals['active']).to(equal(True))
            expect(new_compt_vals['data_alta']).to(equal('2018-02-01'))
            expect(new_compt_vals['data_baixa']).to(equal(False))
            expect(new_compt_vals['giro']).to(equal(100000))
            expect(new_compt_vals['preu_lloguer']).to(equal(0.018929))

    def test_active_meters_are_deactivated_on_change(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        compt_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            old_compt_id = compt_obj.search_by_name(cursor, uid, '1111111')[0]
            old_compt_vals = compt_obj.read(cursor, uid, old_compt_id)
            expect(old_compt_vals['active']).to(equal(False))
            expect(old_compt_vals['data_alta']).to(equal('2018-01-01'))
            expect(old_compt_vals['data_baixa']).to(equal('2018-01-31'))

            new_compt_id = compt_obj.search_by_name(cursor, uid, '704414207')[0]
            new_compt_vals = compt_obj.read(cursor, uid, new_compt_id)
            expect(new_compt_vals['name']).to(equal('704414207'))
            expect(new_compt_vals['active']).to(equal(True))
            expect(new_compt_vals['data_alta']).to(equal('2018-02-01'))
            expect(new_compt_vals['data_baixa']).to(equal(False))
            expect(new_compt_vals['giro']).to(equal(100000))
            expect(new_compt_vals['preu_lloguer']).to(equal(0.018929))


    def test_activation_date_is_upated_on_import(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        compt_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]

            compt_id = compt_obj.search_by_name(cursor, uid, '1111111')[0]
            compt_obj.write(
                cursor, uid, compt_id, {
                    'data_alta': '2019-01-01', 'name': '704414207'
                }
            )

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            compt_vals = compt_obj.read(cursor, uid, compt_id)
            expect(compt_vals['active']).to(equal(True))
            expect(compt_vals['data_alta']).to(equal('2018-02-01'))
            expect(compt_vals['data_baixa']).to(equal(False))
            expect(compt_vals['name']).to(equal('704414207'))

    def test_baixa_date_is_upated_on_import(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        compt_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')
        lect_obj = self.openerp.pool.get("giscegas.lectures.lectura")
        lect_pool_obj = self.openerp.pool.get("giscegas.lectures.lectura.pool")

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]

            compt_id = compt_obj.search_by_name(cursor, uid, '1111111')[0]
            compt_obj.write(
                cursor, uid, compt_id, {
                    'data_baixa': '2019-01-01',
                    'active': False, 'name': '704414207'
                }
            )

            # Delete lects after 2016-02-19
            params = [('comptador', '=', compt_id), ('name', '>=', '2017-01-01')]
            ctx = {'active_test': False}
            for obj in [lect_obj, lect_pool_obj]:
                obj_ids = obj.search(cursor, uid, params, context=ctx)
                obj.unlink(cursor, uid, obj_ids, ctx)

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            compt_vals = compt_obj.read(cursor, uid, compt_id)
            expect(compt_vals['active']).to(equal(False))
            expect(compt_vals['data_alta']).to(equal('2018-01-01'))
            expect(compt_vals['data_baixa']).to(equal('2018-02-28'))
            expect(compt_vals['name']).to(equal('704414207'))

    def test_meter_is_created_when_none_exists(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        compt_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]

            polissa_2 = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0002'
            )[1]

            compt_id = compt_obj.search_by_name(cursor, uid, '1111111')[0]
            compt_obj.write(cursor, uid, [compt_id], {'polissa': polissa_2})

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            new_compt_id = compt_obj.search_by_name(cursor, uid, '704414207')[0]
            compt_vals = compt_obj.read(cursor, uid, new_compt_id)
            expect(compt_vals['active']).to(equal(True))
            expect(compt_vals['name']).to(equal('704414207'))
            expect(compt_vals['active']).to(equal(True))
            expect(compt_vals['data_alta']).to(equal('2018-02-01'))
            expect(compt_vals['data_baixa']).to(equal(False))
            expect(compt_vals['giro']).to(equal(100000))
            expect(compt_vals['preu_lloguer']).to(equal(0.018929))

    def test_new_meters_can_be_imported_before_currently_active_ones(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        compt_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]

            post_compt_id = compt_obj.search_by_name(cursor, uid, '1111111')[0]
            compt_obj.write(
                cursor, uid, post_compt_id, {
                    'data_alta': '2019-01-01'
                }
            )

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            post_compt_vals = compt_obj.read(cursor, uid, post_compt_id)
            expect(post_compt_vals['active']).to(equal(True))
            expect(post_compt_vals['data_alta']).to(equal('2019-01-01'))
            expect(post_compt_vals['data_baixa']).to(equal(False))

            pre_compt_id = compt_obj.search_by_name(cursor, uid, '704414207')[0]
            pre_compt_vals = compt_obj.read(cursor, uid, pre_compt_id)
            expect(pre_compt_vals['name']).to(equal('704414207'))
            expect(pre_compt_vals['active']).to(equal(False))
            expect(pre_compt_vals['data_alta']).to(equal('2018-02-01'))
            expect(pre_compt_vals['data_baixa']).to(equal('2018-12-31'))
            
    def test_incorrect_new_b70_return_case_001(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        # We get the xml directly from the 'fixtures' directory to get the
        # new version
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures', 'test_utils_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
            xml_file = xml_file.replace("<razonsocial>Emisora, S.A</razonsocial>", "")

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(10))

            expect(len(line_vals['error_ids'])).to(equal(1))
            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])

            expect(error.error_template_id.phase).to(equal('1'))
            expect(error.error_template_id.code).to(equal('001'))

    def test_extra_line_negative_quantity(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn, context={
                "test_xml": "test_extra_line_negative_quantity_gas.xml"})

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['import_phase']).to(equal(40))
            # Check if extralines are correctlly created
            line = line_obj.browse(cursor, uid, line_id)[0]
            extralines = line.liniaextra_id
            self.assertEqual(extralines[0].quantity, 1)
            self.assertEqual(extralines[0].price_unit, -86.66)

    def test_b70_conceptes_correcte(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            "B70_conceptes_gas.xml"
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            with open(xml_path, 'r') as f:
                self.xml_file = f.read()

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))
            self.assertEqual(len(line.liniafactura_id), 1)
            self.assertEqual(line.liniafactura_id[0].factura_id.amount_total, 104.86)
            self.assertEqual(line.liniafactura_id[0].factura_id.check_total, 104.86)
            self.assertEquals(len(line.liniaextra_id), 1)
            extralines = line.liniaextra_id
            self.assertEqual(extralines[0].quantity, 1)
            self.assertEqual(extralines[0].price_unit, 86.66)

    def test_b70_conceptes_correcte_daia_abans_alta(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            "B70_conceptes_gas.xml"
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
            pol_alta = self.openerp.pool.get("giscegas.polissa").write(
                cursor, uid, polissa_id, {'data_alta': '2018-01-10'}
            )
            altamenys1 = '2018-01-09'
            with open(xml_path, 'r') as f:
                self.xml_file = f.read()
                self.xml_file = self.xml_file.replace("2018-01-17", altamenys1)
                self.xml_file = self.xml_file.replace("2018-02-28", altamenys1)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))
            self.assertEqual(len(line.liniafactura_id), 1)
            self.assertEqual(line.liniafactura_id[0].factura_id.amount_total, 104.86)
            self.assertEqual(line.liniafactura_id[0].factura_id.check_total, 104.86)
            self.assertEquals(len(line.liniaextra_id), 1)
            extralines = line.liniaextra_id
            self.assertEqual(extralines[0].quantity, 1)
            self.assertEqual(extralines[0].price_unit, 86.66)

    def test_validation_2001_fail(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        cups_obj = self.openerp.pool.get('giscegas.cups.ps')
        part_obj = self.openerp.pool.get('giscemisc.participant')
        imd_obj = self.openerp.pool.get('ir.model.data')
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_rectificadora_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            distri_id = part_obj.search(
                cursor, uid, [('codi_sifco', '=', '1234')]
            )[0]
            distri_id = part_obj.get_partner_id(cursor, uid, distri_id)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0003_polissa_0001_N'
            )[1]

            params = {
                'origin_date_invoice': '2018-07-01',
                'origin': '123456',
                'partner_id': distri_id
            }
            fact_obj.write(cursor, uid, [fact_id], params)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_cups', 'giscegas_cups_01'
            )[1]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2018-01-01',
                    'data_baixa': '2019-01-01'
                }
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(20))
            expect(line_vals['lectures_processades']).to(equal(False))
            expect(len(line_vals['error_ids'])).to(equal(1))

            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])
            expect(error.error_template_id.phase).to(equal('2'))
            expect(error.error_template_id.code).to(equal('001'))

    def test_validation_2001_success(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscegas.facturacio.atr.error'
        )
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        cups_obj = self.openerp.pool.get('giscegas.cups.ps')
        imd_obj = self.openerp.pool.get('ir.model.data')
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')

        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            'B70_rectificadora_gas.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.prepare_for_phase_3(txn)
            part_obj = self.openerp.pool.get('giscemisc.participant')
            distri_id = part_obj.search(
                cursor, uid, [('codi_sifco', '=', '1234')]
            )[0]
            distri_id = part_obj.get_partner_id(cursor, uid, distri_id)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0003_polissa_0001_N'
            )[1]

            params = {
                'origin_date_invoice': '2017-07-01',
                'origin': '123456',
                'partner_id': distri_id
            }
            fact_obj.write(cursor, uid, [fact_id], params)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr',
                'b70_import_01'
            )[1]

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_cups', 'giscegas_cups_01'
            )[1]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2018-01-01',
                    'data_baixa': '2019-01-01'
                }
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(20))
            expect(line_vals['lectures_processades']).to(equal(False))
            expect(len(line_vals['error_ids'])).to(equal(1))

            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])
            expect(error.error_template_id.phase).to(equal('2'))
            expect(error.error_template_id.code).to(equal('007'))

    def test_full_import_works_B70_2_preiodes_lectura(self):
        line_obj = self.openerp.pool.get(
            'giscegas.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        xml_path = get_module_resource(
            'giscegas_facturacio_atr', 'tests', 'fixtures',
            "B70_2_preiodes_lectura.xml"
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            with open(xml_path, 'r') as f:
                xml_file = f.read()

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_atr', 'b70_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)[0]
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['lectures_processades']).to(equal(True))
            expect(line_vals['import_phase']).to(equal(40))
            imp_lect_obj = self.openerp.pool.get(
                'giscegas.facturacio.importacio.linia.lectures'
            )
            lectures_b70_ids = imp_lect_obj.search(
                cursor, uid, [('linia_id', '=', line_id[0])]
            )
            self.assertTrue(lectures_b70_ids)
            line = line_obj.browse(cursor, uid, line_id)[0]
            expect(line.liniafactura_id).to(have_len(1))
            expect(line.liniafactura_id[0].rectificative_type).to(equal('N'))
            fact = line.liniafactura_id[0].factura_id
            self.assertEqual(fact.check_total, 48.53)
            self.assertEqual(fact.amount_total, 48.53)
            self.assertEqual(len(fact.lectures_ids), 2)
            self.assertEqual(fact.lectures_ids[0].lect_anterior, 21162.0)
            self.assertEqual(fact.lectures_ids[0].lect_actual, 21164.0)
            self.assertEqual(fact.lectures_ids[0].pressio_subministrament, 0.02)
            self.assertEqual(fact.lectures_ids[1].lect_anterior, 21164.0)
            self.assertEqual(fact.lectures_ids[1].lect_actual, 21310.0)
            self.assertEqual(fact.lectures_ids[1].pressio_subministrament, 0.02)
            self.assertEqual(fact.origin, "12345 - C131427210")
            self.assertEqual(fact.tarifa_acces_id.name, "3.2")
            self.assertEqual(fact.type, "in_invoice")
            pol = fact.polissa_id
            compt = False
            for compta in pol.comptadors:
                if compta.name == "797078597":
                    compt = compta
            self.assertTrue(compt)
            self.assertTrue(compt.active)
            lec1, lec2, lec3 = False, False, False
            for l in compt.pool_lectures:
                if l.name == "2019-01-01":
                    lec1 = l
                if l.name == "2019-01-02":
                    lec2 = l
                if l.name == "2019-03-01":
                    lec3 = l
            self.assertTrue(lec1)
            self.assertEqual(lec1.lectura, 21162.0)
            self.assertEqual(lec1.factor_k, 0.9123)
            self.assertEqual(lec1.pcs, 11.679)
            self.assertTrue(lec2)
            self.assertEqual(lec2.lectura, 21164.0)
            self.assertEqual(lec2.factor_k, 0.9123)
            self.assertEqual(lec2.pcs, 11.679)
            self.assertTrue(lec3)
            self.assertEqual(lec3.lectura, 21310.0)
            self.assertEqual(lec3.factor_k, 0.9123)
            self.assertEqual(lec3.pcs, 11.747)
