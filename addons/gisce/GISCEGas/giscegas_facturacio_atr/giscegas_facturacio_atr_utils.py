# -*- coding: utf-8 -*-
import re


def get_cups_from_xml(xml_data):
    cups = re.findall('<cups>([^<]*)</cups>', xml_data)
    if cups:
        return cups[0]
    else:
        return None


def get_header_from_xml(xml_data, index):
    res = {}
    cups = re.findall('<cups>([^<]*)</cups>', xml_data)
    res.setdefault('cups', False)
    if cups:
        res['cups'] = cups[index]

    proces_b70 = re.findall(
        '<codproceso>([^<]*)</codproceso>',
        xml_data
    )
    res.setdefault('proces_b70', False)
    if proces_b70:
        res['proces_b70'] = proces_b70[0]

    emisor_b70 = re.findall(
        '<empresaemisora>([^<]*)</empresaemisora>',
        xml_data
    )
    res.setdefault('codigo_ree_empresa_emisora', False)
    if emisor_b70:
        res['codigo_ree_empresa_emisora'] = emisor_b70[0]

    destino_b70 = re.findall(
        '<empresadestino>([^<]*)</empresadestino>',
        xml_data
    )
    res.setdefault('codigo_ree_empresa_destino', False)
    if destino_b70:
        res['codigo_ree_empresa_destino'] = destino_b70[0]

    sol_b70 = re.findall(
        '<numfactura>([^<]*)</numfactura>',
        xml_data
    )
    res.setdefault('codigo_solicitud', False)
    if sol_b70:
        res['codigo_solicitud'] = sol_b70[index]

    fecha_b70 = re.findall(
        '<fechacomunic>([^<]*)</fechacomunic>',
        xml_data
    )
    res.setdefault('fecha_solicitud', False)
    if fecha_b70:
        res['fecha_solicitud'] = fecha_b70[0]

    codigo_proceso = re.findall(
        '<codproceso>([^<]*)</codproceso>',
        xml_data
    )
    res.setdefault('codigo_proceso', False)
    if codigo_proceso:
        res['codigo_proceso'] = codigo_proceso[0]

    res.setdefault('fecha_factura_desde', False)
    res.setdefault('fecha_factura_hasta', False)
    res.setdefault('type_factura', 'N')
    res.setdefault('fecha_factura', False)

    res['tipo_factura_b70'] = 'atr'
    fecha_desde = re.findall(
        '<fecdesde>([^<]*)</fecdesde>', xml_data
    )
    if fecha_desde:
        res['fecha_factura_desde'] = min(fecha_desde)

    fecha_hasta = re.findall(
        '<fechasta>([^<]*)</fechasta>', xml_data
    )
    if fecha_hasta:
        res['fecha_factura_hasta'] = max(fecha_hasta)

    # Tipo factura N/R..
    invoice_type = re.findall(
        '<clasefact>([^<]*)</clasefact>', xml_data
    )
    if not invoice_type:
        invoice_type = re.findall(
            '<indfacturarect>([^<]*)</clasefact>', xml_data
        )
    if invoice_type:
        res['type_factura'] = invoice_type[index]

    invoice_date = re.findall(
        '<fecfactura>([^<]*)</fecfactura>', xml_data
    )
    if invoice_date:
        res['fecha_factura'] = invoice_date[index]

    return res


def get_filename(filename):
    if isinstance(filename, str):
        return unicode(filename, errors='ignore')
    return filename


def get_remesa_from_xml(xml_data, emisor_lot=''):
    return None


def get_remeses_from_zip(zfile, emisor_lot=''):
    """
    :param zfile: ZIP handle
    :param emisor_lot: from code
    :return: payment orders dict list
    """
    remeses = []
    return remeses


def get_number_of_invoices_from_xml(xml_data):
    factures = re.findall(
        '</factura>',
        xml_data
    )
    return len(factures)