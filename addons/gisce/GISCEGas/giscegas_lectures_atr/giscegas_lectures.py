# -*- coding: utf-8 -*-

import zipfile
import base64
import traceback
import StringIO
import sys
from datetime import datetime, timedelta
from os.path import basename

import pooler
from osv import osv, fields, orm
from tools.translate import _

from libfacturacioatr import tarifes


class GiscegasLecturesAtrHelper(osv.osv):
    """Funcions d'ajuda de atr_gas"""

    _name = 'giscegas.lectures.atr.helper'

    def check_emisor_polissa(self, cursor, uid, polissa_id, emisor):
        pol = self.pool.get('giscegas.polissa')
        p_data = pol.browse(cursor, uid, polissa_id)
        part_obj = self.pool.get('giscemisc.participant')
        pdistribuidora_id = part_obj.get_id_from_partner(cursor, uid, p_data.cups.distribuidora_id.id)
        ref_cups = part_obj.read(cursor, uid, pdistribuidora_id, ['codi_sifco'])['codi_sifco']
        if (ref_cups != emisor):
            msg = (_(u"L'empresa distribuidora del cups %s no es correspon "
                     u"amb l'emisor especificat %s") % (p_data.cups.name, emisor))
            raise osv.except_osv('Error', _(msg))

    def check_gir(self, gir, name):
        """Comprova que el gir hi quep en un enter"""
        if (gir > sys.maxint):
            msg = _(u"El gir del comptador a crear amb número de sèrie %s "
                    u"supera tamany de l'enter") % name
            raise osv.except_osv('Error', _(msg))
        return True

    def calculate_price_from_rent_prices(self, cursor, uid, comptadors_xml,
                                         lloguers_xml, num_compt, context=None):
        if context is None:
            context = {}

        num_llogers = len(lloguers_xml)
        num_comptadors = len(comptadors_xml)

        if num_compt < num_llogers:
            # By default, we get the price from the same position
            start_date = datetime.strptime(
                lloguers_xml[num_compt].fecdesde, '%Y-%m-%d'
            )
            start_date -= timedelta(days=1)

            end_date = datetime.strptime(
                lloguers_xml[num_compt].fechasta, '%Y-%m-%d'
            )
            num_dias_fact = (end_date - start_date).days
            preu = lloguers_xml[num_compt].importe / num_dias_fact

        elif lloguers_xml:
            # If we don't have enough prices, we get the last one
            start_date = datetime.strptime(
                lloguers_xml[-1].fecdesde, '%Y-%m-%d'
            )
            start_date -= timedelta(days=1)

            end_date = datetime.strptime(
                lloguers_xml[-1].fechasta, '%Y-%m-%d'
            )
            num_dias_fact = (end_date - start_date).days
            preu = lloguers_xml[-1].importe / num_dias_fact
        else:
            # If the list is empty, we return 0
            preu = 0

        if num_comptadors < num_llogers and num_compt == num_comptadors - 1:
            # If we have more rents than meters and it's the last one, we get
            # the weighted average of the prices by number of days
            preu_total = sum(
                [
                    l.precunidad * l.unidad
                    for l in lloguers_xml[num_compt:]
                ]
            )

            # Check in context if rents are simultanious. With simultanious
            # rents the numbers of days must be equal for all rents and they
            # are not summed (because rents are simultanious, occurs in the
            # same period of time)
            equal_rent_days = False
            rent_days = lloguers_xml[num_compt].unidad
            for l in lloguers_xml[num_compt:]:
                equal_rent_days = rent_days == l.unidad
                if not equal_rent_days:
                    break

            if context.get("simultanious_rents", False) and equal_rent_days:
                num_dies = lloguers_xml[num_compt].unidad
            else:
                num_dies = sum([l.unidad for l in lloguers_xml[num_compt:]])

            # This will be correct if the prices are broken down, because the
            # days will be the same in each case so we will get the sum of all
            # of them.
            # It will also work if we have two different prices for a single
            # meter because we will be calculating the weighted average. If we
            # invoice the same number of days the price will be the same.
            if num_dies == 0:
                preu = 0
            else:
                preu = preu_total / num_dies

        return preu

    def update_current_meter(self, cursor, uid, fact, compt_id, start_date,
                             end_date, context=None):
        if context is None:
            context = {}

        compt_obj = self.pool.get('giscegas.lectures.comptador')

        compt_vals = compt_obj.read(
            cursor, uid, compt_id,
            ['preu_lloguer', 'lloguer', 'data_alta', 'data_baixa', 'polissa']
        )
        data_baixa_pol = self.pool.get("giscegas.polissa").read(
            cursor, uid, compt_vals['polissa'][0], ['data_baixa']
        )['data_baixa']
        vals = {}

        if compt_vals['data_alta'] > start_date:
            vals.update({'data_alta': start_date})

        if compt_vals.get('data_baixa', end_date) > end_date and compt_vals.get('data_baixa', end_date) != data_baixa_pol:
            last_date = self.get_last_reading_from_meter(cursor, uid, compt_id)
            if not last_date or (last_date and last_date <= end_date):
                vals.update({'data_baixa': end_date})

        compt_obj.write(cursor, uid, compt_id, vals)

    def get_last_reading_from_meter(self, cursor, uid, compt_id, context=None):
        if context is None:
            context = {}

        lect_obj = self.pool.get("giscegas.lectures.lectura")
        lect_pool_obj = self.pool.get("giscegas.lectures.lectura.pool")
        params = [('comptador', '=', compt_id)]
        ctx = context.copy()
        ctx['active_test'] = False

        dates = []
        for obj in [lect_obj, lect_pool_obj]:
            obj_id = obj.search(cursor, uid, params, limit=1, order="name DESC", context=ctx)
            if not obj_id:
                continue
            obj_info = obj.read(cursor, uid, obj_id[0], ['name'])
            if obj_info['name']:
                dates.append(obj_info['name'])

        return len(dates) and max(dates) or False

    def create_new_meter(self, cursor, uid, polissa_vals, meter_name, turn,
                         start_date, end_date, context=None):
        if context is None:
            context = {}

        compt_obj = self.pool.get('giscegas.lectures.comptador')

        # Check is the start date from meter is 1 day before data alta of
        # contract. This happens with new contracts with it's first F1.
        cstart_date = datetime.strptime(start_date, "%Y-%m-%d")
        pol_obj = self.pool.get("giscegas.polissa")
        polissa_data_alta = pol_obj.read(
            cursor, uid, polissa_vals['id'], ['data_alta']
        )['data_alta']
        polissa_data_alta = datetime.strptime(polissa_data_alta, "%Y-%m-%d")
        if (polissa_data_alta - cstart_date).days == 1:
            start_date = cstart_date + timedelta(days=1)
            start_date = start_date.strftime("%Y-%m-%d")

        self.check_gir(turn, meter_name)
        vals = {
            'name': meter_name,
            'polissa': polissa_vals['id'],
            'giro': turn,
            'data_alta': start_date,
            'active': True,
        }

        # We also need to check if we already have one and handle that
        search_params = [('polissa.id', '=', polissa_vals['id'])]
        current_meter_id = compt_obj.search(cursor, uid, search_params)

        if current_meter_id:
            compt = compt_obj.browse(cursor, uid, current_meter_id[0])
            date_type = '%Y-%m-%d'
            d_alta_compt = datetime.strptime(compt.data_alta, date_type)
            d_lect_ini = datetime.strptime(start_date, date_type)
            d_lect_fi = datetime.strptime(end_date, date_type)
            if d_alta_compt < d_lect_ini:
                # Donar de baixa el comptador actiu
                # Es dona de baixa en la data anterior a l'alta del nou
                # comptador.
                data_baixa = datetime.strptime(start_date, "%Y-%m-%d")
                data_baixa -= timedelta(days=1)
                data_baixa = data_baixa.strftime("%Y-%m-%d")
                current_meter_vals = {
                    'active': False,
                    'data_baixa': data_baixa
                }
                compt_obj.write(
                    cursor, uid, current_meter_id, current_meter_vals
                )
            elif d_lect_ini <= d_alta_compt < d_lect_fi:
                # If we already have a meter during the period where we want
                # to import the new one this means that we would end up
                # having to meters active during the same time, which we
                # don't want
                ############################################################
                # IMPORTANT
                ############################################################
                # This should never happen since we have a critical
                # validation on phase 3 to prevent this. Despite that, this
                # code will be kept just in case
                msg = (_(
                    u"La pòlissa %s té assignat el comptador %s diferent "
                    u"al de l'xml (%s) amb data d'alta dins els periodes "
                    u"de lectura"
                ) % (polissa_vals['name'], compt.name, meter_name))
                raise osv.except_osv('Error', msg)
            elif d_alta_compt >= d_lect_fi:
                # If the new meter goes before the currently active one we
                # need to make it so that the new one has active at False
                # and data_baixa 1 day before the date in which the currently
                # active one has the data_alta
                data_baixa = datetime.strptime(compt.data_alta, "%Y-%m-%d")
                data_baixa -= timedelta(days=1)
                data_baixa = data_baixa.strftime("%Y-%m-%d")
                vals.update(
                    {'active': False, 'data_baixa': data_baixa}
                )

        # And we finally create the new meter
        return compt_obj.create(cursor, uid, vals, context=context)

    def check_meter(self, cursor, uid, meter_name, turn, polissa_id, start_date,
                    end_date, num_compt, fact=None, context=None):
        polissa_obj = self.pool.get('giscegas.polissa')
        compt_obj = self.pool.get('giscegas.lectures.comptador')

        polissa_vals = polissa_obj.read(
            cursor, uid, polissa_id, ['cups', 'name']
        )

        compt_id = compt_obj.search_with_contract(
            cursor, uid, meter_name, polissa_id
        )

        if compt_id:
            compt_id = compt_id[0]
            # If we find the meter
            self.update_current_meter(
                cursor, uid, fact, compt_id, start_date, end_date, context
            )
        else:
            # If we didn't find the meter
            # We will have to create a new one
            compt_id = self.create_new_meter(
                cursor, uid, polissa_vals, meter_name, turn, start_date,
                end_date, context
            )
        return compt_id

    def find_polissa(self, cursor, uid, cups, data_inici, data_final):
        """Buscar la pòlissa associada al cups
        """
        search_params = [('name', 'ilike',  '%s%%' % cups[:20])]
        cups_pool = self.pool.get('giscegas.cups.ps')
        cups_id = cups_pool.search(cursor, uid, search_params,
                                        context={'active_test': False})
        if not cups_id:
            msg = _(u"No s'ha trobat el CUPS '%s'") % cups[:20]
            raise osv.except_osv('Error', msg)

        return self.find_polissa_with_cups_id(
            cursor, uid, cups_id[0], data_inici, data_final
        )

    def find_polissa_with_cups_id(self, cursor, uid, cups_id, data_inici,
                                  data_final):
        search_params = [('cups.id', '=', cups_id),
                         ('data_alta', '<=', data_inici),
                         '|', ('data_baixa', '>=', data_final),
                         ('data_baixa', '=', False)]
        polisses_pool = self.pool.get('giscegas.polissa')
        cups_obj = self.pool.get('giscegas.cups.ps')
        polissa_id = polisses_pool.search(cursor, uid, search_params,
                                            context={'active_test': False})
        if not polissa_id:
            cups = cups_obj.read(cursor, uid, cups_id, ['name'])['name']
            msg = _(u"No s'ha trobat la pòlissa vinculada al cups %s en "
                    u"el periode de %s a %s") % (cups[:20], data_inici,
                                                                data_final)
            raise osv.except_osv('Error', msg)
        return polissa_id[0]

    def get_num_periodes(self, cursor, uid, ids, context=None):
        """Retorna el número de periodes reals (no agrupats de la tarifa).
        """
        periodes_obj = self.pool.get('giscegas.polissa.tarifa.periodes')
        for tid in ids:
            search_params = [('tarifa.id', '=', tid)]
            return periodes_obj.search_count(cursor, uid, search_params)

    def get_id_periode(self, cursor, uid, lect, tarifa):
        search_params = [('codi_ocsum', '=', tarifa)]
        tarifa_pool = self.pool.get('giscegas.polissa.tarifa')
        tarifa_id = tarifa_pool.search(cursor, uid, search_params)

        # GAS: De moment només treballem amb 1 periode
        search_params = [('tarifa.id', '=', tarifa_id[0]),
                         ('name', '=', 'P1')]
        periodes_pool = self.pool.get('giscegas.polissa.tarifa.periodes')
        periodes_id = periodes_pool.search(cursor, uid, search_params)
        if not periodes_id:
            raise osv.except_osv(
                'Error',
                _("No s'ha trobat el periode %s pel codi de "
                  "tarifa OCSUM %s") % ('P1', tarifa))
        return periodes_id[0]

    def set_uom_lectura(self, cursor, uid, valor, tipus='V', context=None):
        """Retorna el valor de la lectura segons la configuració
           de atr_gas de l'emisor.
        """
        if not context:
            context = {}

        uom_obj = self.pool.get('product.uom')
        imd_obj = self.pool.get('ir.model.data')

        uom_eneg_gas = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'uom_eneg_gas'
        )[1]
        product_uom_any_gas = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'product_uom_any_gas'
        )[1]

        # De moment son fixes. Si en algun moment es necessita el configurador
        # de atr_gas s'haura de utilitzar aqui
        b70_uoms = {
            'V': {  # Terme variable
                'from': uom_eneg_gas,
                'to': uom_eneg_gas,
            },
            'F': {  # Terme Fix
                'from': product_uom_any_gas,
                'to': product_uom_any_gas,
            },
        }

        ref = b70_uoms[tipus]
        return uom_obj._compute_qty(cursor, uid, ref['from'], valor, ref['to'])

    def vals_lectura(self, cursor, uid, lect, ordre, compt, tarifa=None, context=None):
        """Retorna els valors de la lectura"""
        if context is None:
            context = {}

        if ordre == 'inicial':
            mod = "desde"
            consum = 0
        else:
            mod = "actual"
            consum = lect['consum']

        codi = lect["origen_%s" % mod]
        name = lect["fecha_%s" % mod]
        lecm3_ = lect["lectura_%s_m3" % mod]

        periode = self.get_id_periode(
            cursor, uid, lect, tarifa
        )
        search_params = [('codi', '=', 'B70')]
        origen_pool = self.pool.get('giscegas.lectures.origen_comer')
        orig_comer = origen_pool.search(cursor, uid, search_params)
        vals = {
            'name': name,
            'periode': periode,
            'comptador': compt,
            'origen_comer_id': orig_comer[0],
            'lectura': lecm3_,
            'pcs': lect['pcs'],
            'factor_k': lect['factor_k'],
            'consum': consum,
            'pressio_subministrament': lect['pressio_subministrament'],
            'tipo_lect_num': lect.get('tipo_lect_num')
        }
        if lect['ajust'] and ordre != 'inicial':
            vals['ajust'] = lect['ajust']
            vals['motiu_ajust'] = lect['motiu']
        # else:
        #     vals['ajust'] = 0
        #     vals['motiu_ajust'] = False

        # Mirem en quines unitats ens arriba la pressio
        if context.get("codi_emisor"):
            conf_o = self.pool.get("giscegas.facturacio.atr.config")
            uom_o = self.pool.get('product.uom')
            conf_vals = conf_o.get_config(cursor, uid, context.get("codi_emisor"), context=context)
            vals['pressio_subministrament'] = uom_o._compute_qty(cursor, uid, conf_vals['uom_pressio_f'], vals['pressio_subministrament'], conf_vals['uom_pressio_t'])

        search_params = [('codi', '=', codi)]
        origen_pool = self.pool.get('giscegas.lectures.origen')

        orig = origen_pool.search(cursor, uid, search_params)
        vals['origen_id'] = orig[0]

        return vals

    def check_reading_exists(self, cursor, uid, lect_vals, context=None):
        if context is None:
            context = {}

        search_params = [
            ('name', '=', lect_vals['name']),
            ('comptador', '=', lect_vals['comptador']),
            ('periode', '=', lect_vals['periode'])
        ]

        model = 'giscegas.lectures.lectura.pool'

        lect_pool = self.pool.get(model)
        lect_id = lect_pool.search(
            cursor, uid, search_params, context={'active_test': False}
        )
        if lect_id:
            return True, lect_id[0]
        else:
            return False, None

    def import_reading(self, cursor, uid, lect_vals, rectificadora=False,
                       overwrite_atr=False, context=None):
        if context is None:
            context = {}

        lect_vals = lect_vals.copy()
        lect_obj = self.pool.get('giscegas.lectures.lectura.pool')
        origen_comer_obj = self.pool.get('giscegas.lectures.origen_comer')
        compt_obj = self.pool.get('giscegas.lectures.comptador')

        existeix, lect_id = self.check_reading_exists(
            cursor, uid, lect_vals, context
        )
        if not existeix:
            lect_id = lect_obj.create(
                cursor, uid, lect_vals, context
            )
        elif existeix:
            overwrite_existing = False
            overwrite_message = ''

            read_fields = ['origen_comer_id', 'observacions', 'lectura']
            lect_ini_vals = lect_obj.read(
                cursor, uid, lect_id, read_fields
            )

            if rectificadora:
                overwrite_existing = True
                overwrite_message = _(
                    'Valor de lectura sobreescrit pel valor d\'una '
                    'rectificadora. '
                    'Valor antic: {0}. Valor nou: {1}'
                ).format(lect_ini_vals['lectura'], lect_vals['lectura'])
            elif overwrite_atr:
                ini_origen_comer_id = lect_ini_vals['origen_comer_id'][0]
                ini_origen_comer_code = origen_comer_obj.read(
                    cursor, uid, ini_origen_comer_id, ['codi']
                )['codi']

                new_origen_comer_id = lect_vals['origen_comer_id']

                ini_has_at_origen = ini_origen_comer_code == 'AT'
                new_origen_is_diff = new_origen_comer_id != ini_origen_comer_id
                if ini_has_at_origen and new_origen_is_diff:
                    overwrite_existing = True
                    overwrite_message = _(
                        'Valor de lectura de gestió ATR sobreescrit '
                        'pel valor d\'un B70. '
                        'Valor antic: {0}. Valor nou: {1}'
                    ).format(lect_ini_vals['lectura'], lect_vals['lectura'])

            # If we have to overwrite the existing reading
            if overwrite_existing:
                obs = lect_ini_vals['observacions'] or ''
                if obs:
                    obs += '\n'
                lect_vals['observacions'] = obs + overwrite_message
                lect_obj.write(
                    cursor, uid, [lect_id], lect_vals, context
                )
            else:
                # We check if the current reading has the same value we would
                # import. If it is the same we don't do anything. If it's
                # different we give a warning
                lect_imp = lect_vals['lectura']
                lect_ori = lect_ini_vals['lectura']

                if round(lect_imp, 6) != round(lect_ori, 6):
                    compt_vals = compt_obj.read(
                        cursor, uid, lect_vals['comptador'], ['name']
                    )
                    msg = _(
                        u"Divergència en el valor de lectura existent."
                        u" Comptador: %s Data: %s. Període: %s."
                        u" valor: XML: %s BBDD:%s"
                    ) % (
                        compt_vals['name'], lect_vals['name'],
                        lect_vals['periode'], lect_imp,
                        lect_ori
                    )
                    raise osv.except_osv('Error', msg)
        if context.get("eliminar_lectures"):
            lect_obj.unlink(cursor, uid, [lect_id], context=context)
            lect_id = None
        return lect_id

    def get_reading_model_id(self, cursor, uid, compt_id, data_lectura, periode,
                            tarifa_id, context=None):
        if context is None:
            context = {}

        search_params = [
            ('periode.name', '=', periode),
            ('name', '=', data_lectura),
            ('comptador', '=', compt_id),
            ('periode.tarifa', '=', tarifa_id)
        ]

        # If we are searching for energy readings
        model = 'giscegas.lectures.lectura.pool'
        lect_obj = self.pool.get(model)

        lectura_ids = lect_obj.search(cursor, uid, search_params)

        if not lectura_ids:
            return None
        if len(lectura_ids) != 1:
            raise osv.except_osv(
                'Error',
                _(
                    u"Hi ha més d'una lectura per el comptador {0}, "
                    u"en data {1}, periode {2}"
                ).format(compt_id, data_lectura, periode)
            )
        res = {'model': model, 'id': lectura_ids[0]}
        return res

    def move_meters_of_contract(self, cursor, uid, old_contract, new_contract, limit_date, context=None):
        """
            Copy meters which have readings after limit_date from old contract to new contract.
            Readings after limit_date are moved to meters of new contract (they are deleted from old meter).
            If a meter in old_contract is left without any reading, it's deleted.
        """
        if not context:
            context = {}
        lpool_obj = self.pool.get('giscegas.lectures.lectura.pool')
        meter_obj = self.pool.get("giscegas.lectures.comptador")
        new_meters = []
        for meter in old_contract.comptadors:
            # If meter have lects (not in pool) after limit_date we can't move it
            after_limit_lect = [l for l in meter.lectures if l.name > limit_date]
            if len(after_limit_lect):
                inf = _(u"No es pot moure el comptador {0} de la pólissa {1} "
                        u"perqué hi ha lectures facturades (no son del pool) "
                        u"posteriors al {2}").format(meter.name, old_contract.name, limit_date)
                raise osv.except_osv(_(u'ERROR'), inf)
            after_limit_lect = [l for l in meter.lectures_pot if l.name > limit_date]
            if len(after_limit_lect):
                inf = _(u"No es pot moure el comptador {0} de la pólissa {1} "
                        u"perqué hi ha lectures de poténcia facturades (no son "
                        u"del pool) posteriors al {2}").format(meter.name, old_contract.name, limit_date)
                raise osv.except_osv(_(u'ERROR'), inf)

            # Check if we need to work with this meter
            pool_lects_to_move = [l for l in meter.pool_lectures if l.name >= limit_date]
            if not len(pool_lects_to_move):
                continue

            # Create new meter in new_contract using old_meter vals
            limit_date_after = (datetime.strptime(limit_date, "%Y-%m-%d") + timedelta(days=1)).strftime("%Y-%m-%d")
            new_meter_vals = meter_obj.copy_data(cursor, uid, meter.id, context=context)[0]
            new_meter_vals.update({
                'polissa': new_contract.id,
                'data_alta': limit_date_after,
                'active': True,
                'lectures': [],
                'pool_lectures': [],
            })
            new_meter_id = meter_obj.create(cursor, uid, new_meter_vals)
            new_meters.append(new_meter_id)

            for l in pool_lects_to_move:
                if l.name > limit_date:  # Move to new meter
                    l.write({'comptador': new_meter_id})
                elif l.name == limit_date:  # Init. lect. must be in both meters
                    new_lect_vals = lpool_obj.copy_data(cursor, uid, l.id, context=context)[0]
                    new_lect_vals.update({'comptador': new_meter_id})
                    # ####### !!!TEMPORAL!!!: add 1 day to lect ######
                    new_lect_vals.update({'name': limit_date_after})
                    ##################################################
                    lpool_obj.create(cursor, uid, new_lect_vals)

            # Deactivate old meter
            meter.write({
                'active': False,
                'data_baixa': limit_date
            })

        return new_meters

GiscegasLecturesAtrHelper()

