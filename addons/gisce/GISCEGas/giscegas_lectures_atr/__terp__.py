# -*- coding: utf-8 -*-
{
    "name": "Lectures atr_gas",
    "description": """Atr de lectures gas""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscegas_lectures",
        "giscegas_lectures_pool"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegas_lectures_atr_data.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
