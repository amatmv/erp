# -*- coding: utf-8 -*-
{
    "name": "Gestió Impagats de Gas Suspesa",
    "description": """Gestió Impagats Suspesa.
        Per aturar la suspensió i baixa de determinats contractes de gas marcats amb "Gestió de cobraments suspesa".""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends": [
        "giscegas_facturacio",
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml": [
        "giscegas_polissa_view.xml",
    ],
    "active": False,
    "installable": True
}
