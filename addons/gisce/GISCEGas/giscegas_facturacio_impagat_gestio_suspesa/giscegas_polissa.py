# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscegasPolissa(osv.osv):

    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    _columns = {
        'gestio_baixa_suspesa': fields.boolean(string="Gestión de Cobros Suspendida")
    }

    _defaults = {
        'gestio_baixa_suspesa': lambda *a: False
    }


GiscegasPolissa()
