# -*- coding: utf-8 -*-
{
    "name": "Pending invoices of GAS management",
    "description": """
Pending invoices management:
    * Pending state per invoice
    * Pending amount and worst pending state of polissa associated invoices
    * Configurable pending states
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "Facturació",
    "depends": [
        "giscegas_facturacio",
        "giscegas_atr",
        "impagat_base",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/wizard_unpay_view.xml",
        "wizard/wizard_antiquity_debt_view.xml",
        "giscegas_facturacio_view.xml",
        "giscegas_polissa_view.xml",
        "giscegas_facturacio_impagat_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
