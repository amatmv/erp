select
  i.number,
  amount_total * (CASE WHEN i.type like '%%refund' THEN -1 ELSE 1 END) as amount_total,
  CASE WHEN agr_sin.deuda = 0
   THEN 0
  ELSE amount_total * (CASE WHEN i.type like '%%refund' THEN -1 ELSE 1 END)
   END as deuda,
  %s::date - i.date_invoice as days,
  a.name as cuenta,
  case when lower(p.vat) similar to 'es(p|q|s)%%' then
    ' Org. oficial'
  else
    ''
  end as organismo_oficial,
  g.ref as agrupacion
  from account_invoice i
  inner join account_move g on (i.group_move_id = g.id)
  inner join (
    select agr_sin.number,sum(deuda) as deuda
    from (
    select
      i.number,
      sum(l.debit - l.credit) as deuda,
      j.name as diario,
      a.name as cuenta,
      case when lower(p.vat) similar to 'es(p|q|s)%%' then
        ' Org. oficial'
      else
        ''
      end as organismo_oficial
    from
      account_move_line l
      inner join account_account a on (l.account_id = a.id)
      inner join account_journal j on (l.journal_id = j.id)
      inner join res_partner p on (l.partner_id = p.id)
      inner join (
           select distinct g.ref as number
          from account_invoice i, account_move g
          where i.group_move_id = g.id
         and i.type in ('out_refund','out_invoice')

      ) i on (l.ref = i.number)
    where
      a.type = 'receivable'
      and (a.code = '431500' or %s)
      and l.date <= %s
    group by
      j.name,
      cuenta,
      p.vat,
      i.number) agr_sin
   group by agr_sin.number
  ) agr_sin on (agr_sin.number = g.ref)
  inner join account_account a on (i.account_id = a.id)
  inner join res_partner p on (i.partner_id = p.id)
  where i.type in ('out_refund','out_invoice')
     and agr_sin.deuda != 0
order by i.number