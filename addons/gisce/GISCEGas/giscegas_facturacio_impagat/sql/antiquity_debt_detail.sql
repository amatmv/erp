SELECT
  i.number as numero_factura,
  j.name as diario,
  i.date_invoice as fecha_factura,
  i.date_due as fecha_vencimiento,
  par.name as razon_fiscal,
  par.vat as nif_cif,
  c.name as cups,
  c.direccio as cups_direccion,
  pol.name as contrato
FROM  account_invoice i
inner join res_partner par on par.id = i.partner_id
inner join account_journal j on j.id = i.journal_id
inner join (
    VALUES {joins}
) as t (number) on t.number = i.number
left join giscedata_facturacio_factura  f on f.invoice_id = i.id
left join giscedata_cups_ps c on f.cups_id = c.id
left join giscedata_polissa pol on f.polissa_id = pol.id
