select
    i.number,
    i.amount_total,
    sum(l.debit - l.credit) as deuda,
    %s::date - i.date_invoice as days,
    a.name as cuenta,
    case when lower(p.vat) similar to 'es(p|q|s)%%' then
      ' Org. oficial'
    else
      ''
    end as organismo_oficial,
    '' as agrupacion
  from
    account_move_line l
    inner join account_account a on (l.account_id = a.id)
    inner join account_journal j on (l.journal_id = j.id)
    inner join res_partner p on (l.partner_id = p.id)
    inner join (
       select number, date_invoice, amount_total * (CASE WHEN i.type like '%%refund' THEN -1 ELSE 1 END) as amount_total
       from account_invoice i
       where i.group_move_id is null
        and i.type in ('out_refund','out_invoice')
    ) i on (l.ref = i.number)
  where
    a.type = 'receivable'
    and (a.code = '431500' or %s)
    and l.date <= %s
  group by
    days,
    cuenta,
    p.vat,
    i.number,
    i.amount_total,
    agrupacion
  having sum(l.debit - l.credit)!=0
  order by i.number