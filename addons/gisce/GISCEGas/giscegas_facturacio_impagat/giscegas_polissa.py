# -*- encoding: utf-8 -*-

from __future__ import absolute_import
from osv import osv, fields
from tools import cache
from giscegas_polissa.giscegas_polissa import CONTRACT_IGNORED_STATES
from addons.giscegas_facturacio.giscegas_polissa import _get_polissa_from_invoice


class GiscegasPolissa(osv.osv):

    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def copy(self, cursor, uid, id, default=None, context=None):
        """Change copy funcionality
        Do not copy pending_amount and pending_state"""

        default.update({'pending_amount': 0,
                        'pending_state': False,
                       })

        res_id = super(GiscegasPolissa, self).copy(cursor, uid, id, default,
                                                                context)
        return res_id

    @cache(timeout=300)
    def _get_pending_states(self, cursor, uid, polissa_id=None, context=None):
        '''Returns dictionary with pending default values'''

        if not context:
            context = {}

        state_obj = self.pool.get('account.invoice.pending.state')
        res = {}

        state_ids = state_obj.search(cursor, uid, [], context=context)
        for state in state_obj.browse(cursor, uid, state_ids):
            res.update({state.weight: state.name})
        return res

    @staticmethod
    def get_max_weight(weight, factura):
        return max(weight, factura.pending_state.weight)

    def _get_pending(self, cursor, uid, ids, name, arg, context=None):

        if context is None:
            context = {}

        ctx = context.copy()
        ctx['active_test'] = False

        res = {}
        factura_obj = self.pool.get('giscegas.facturacio.factura')

        for polissa in self.browse(cursor, uid, ids, context=context):
            states = self._get_pending_states(
                cursor, uid, polissa_id=polissa.id, context=ctx
            )
            # Default return values if no values
            res[polissa.id] = {
                'pending_state': states[0],
                'pending_amount': 0,
                'debt_amount': 0,
            }
            # If not activa, return default values
            if polissa.state in CONTRACT_IGNORED_STATES:
                continue

            search_params = [('polissa_id', '=', polissa.id),
                             ('state', '=', 'open'),
                             ('type', 'in', ('out_invoice', 'out_refund'))]

            factura_ids = factura_obj.search(cursor, uid, search_params,
                                             context=ctx)
            weight = 0
            for factura in factura_obj.browse(cursor, uid, factura_ids,
                                              context=context):
                if factura.type == 'out_invoice':
                    sign = 1
                else:
                    sign = -1
                res[polissa.id]['pending_amount'] += sign * factura.residual
                if factura.pending_state.weight > 0:
                    res[polissa.id]['debt_amount'] += sign * factura.residual
                weight = self.get_max_weight(weight, factura)
            res[polissa.id]['pending_state'] = states[weight]

        return res

    def _get_polissa_from_factura(self, cursor, uid, ids, context=None):
        """Returns ids of polissa in facturas passed as ids"""

        factura_obj = self.pool.get('giscegas.facturacio.factura')

        vals = factura_obj.read(cursor, uid, ids, ['polissa_id'])
        return [val['polissa_id'][0] for val in vals if val['polissa_id']]

    def change_state(self, cursor, uid, ids, context):
        values = self.read(cursor, uid, ids, ['invoice_id'])
        return _get_polissa_from_invoice(self,
            cursor, uid, [value['invoice_id'][0] for value in values])

    _columns = {
        'pending_state': fields.function(
            _get_pending, method=True, type='char', size=64,
            string='Pending State', store={
                'account.invoice': (
                    _get_polissa_from_invoice,
                    ['state', 'residual', 'pending_state'], 20
                ),
                'account.invoice.pending.history': (
                    change_state, ['change_date'], 10
                ),
            },
            multi='pending',
        ),
        'pending_amount': fields.function(
            _get_pending, method=True, type='float', digits=(16, 2),
            string='Pending amount', store={
                'account.invoice': (
                    _get_polissa_from_invoice,
                    ['state', 'residual', 'pending_state'], 20
                ),
                'account.invoice.pending.history': (
                    change_state, ['change_date'], 10
                ),
            },
            multi='pending'
        ),
        'debt_amount': fields.function(
            _get_pending, method=True, type='float', digits=(16, 2),
            string='Debt amount', store={
                'account.invoice': (
                    _get_polissa_from_invoice,
                    ['state', 'residual', 'pending_state'],
                    20
                ),
                'account.invoice.pending.history': (
                    change_state, ['change_date'], 10
                ),
            },
            multi='pending'
        ),
    }


GiscegasPolissa()
