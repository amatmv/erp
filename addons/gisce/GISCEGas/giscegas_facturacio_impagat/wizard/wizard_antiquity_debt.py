# coding=utf-8
from datetime import datetime
import tempfile
import logging
import base64

from osv import osv, fields
from addons import get_module_resource
from tools.translate import _

import pandas as pd


def change_days(days):
    if days <= 30:
        return '30'
    if days <= 60 :
        return '60'
    if days <= 90 :
        return '90'
    if days  <= 120 :
        return '120'
    if days  <= 150 :
        return '150'
    if days  <= 180 :
        return '180'
    if days  <= 210 :
        return '210'
    if days  <= 240 :
        return '240'
    if days  <= 270 :
        return '270'
    if days  <= 300 :
        return '300'
    if days  <= 330 :
        return '330'
    if days  <= 360:
        return '360'
    else:
        return '>360'

class WizardAntiquityDebt(osv.osv_memory):
    _name = 'giscegas.wizard.antiquity.debt'

    _columns = {
        'date': fields.date('Data'),
        'between_dates': fields.boolean('Entre dates'),
        'date_start': fields.date('Data incial'),
        'date_end': fields.date('Data final'),
        'unpaid_data': fields.selection([
            ('unpaid', 'Impagaments'),
            ('debt', 'Tot el deute')
        ], 'Dades'),
        'format': fields.selection([
            ('pdf', 'PDF'),
            ('xls', 'XLS')
        ], 'Format')
        , 'summarize_file': fields.binary('Fitxer resum file')
        , 'summarize_filename': fields.char('Nom fitxer resum', size=200)
        , 'detailed_file': fields.binary('Detall de factures')
        , 'detailed_filename': fields.char('Nom fitxer detall factures', size=200)

    }

    _defaults = {
        'date': lambda *a: datetime.now().strftime('%Y-%m-%d'),
        'between_dates': 0,
        'unpaid_data': 'debt',
        'format': 'pdf'
    }

    def get_between_dates_antiquity_debt(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        logger = logging.getLogger(
            'openerp.{0}.get_between_dates_antiquity_debt'.format(__name__)
        )
        wiz = self.browse(cursor, uid, ids[0], context=context)
        if wiz.unpaid_data == 'unpaid':
            all_debt = False
        else:
            all_debt = True

        if wiz.between_dates:
            sql_file = get_module_resource(
                'giscegas_facturacio_impagat', 'sql',
                'anitquity_debt_interval_not_grouped.sql'
            )
            params = (
                wiz.date,
                wiz.date_start, wiz.date_end,
                all_debt,
                wiz.date
            )
        else:
            sql_file = get_module_resource(
                'giscegas_facturacio_impagat', 'sql',
                'anitquity_debt_not_grouped.sql'
            )
            params = (
                wiz.date,
                all_debt,
                wiz.date
            )
        with open(sql_file, 'r') as f:
            sql = f.read()
        cursor.execute(sql, params)
        results = cursor.dictfetchall()
        df_fact = pd.DataFrame(results)
        df_fact.fillna({'organismo_oficial': 'No Oficial'})
        if wiz.between_dates:
            sql_file = get_module_resource(
                'giscegas_facturacio_impagat', 'sql',
                'anitquity_debt_interval_grouped.sql'
            )
            params = (
                wiz.date,
                wiz.date_start, wiz.date_end,
                all_debt,
                wiz.date,
                wiz.date_start, wiz.date_end,
            )
        else:
            sql_file = get_module_resource(
                'giscegas_facturacio_impagat', 'sql',
                'anitquity_debt_grouped.sql'
            )
            params = (
                wiz.date,
                all_debt,
                wiz.date,
            )
        with open(sql_file, 'r') as f:
            sql = f.read()
            cursor.execute(sql, params)
            results = cursor.dictfetchall()
            df_fact_agr_deuda = pd.DataFrame(results)
            df_fact_agr_deuda.fillna({'organismo_oficial': 'No Oficial'})

            facturas_deuda = df_fact.groupby(
                ['number', 'cuenta', 'organismo_oficial', 'days',
                 'amount_total'], as_index=False).agg({"deuda": "sum"})
            sum_total = pd.concat(
                [facturas_deuda, df_fact_agr_deuda])
            sum_total.amount_total = sum_total.amount_total.round(2)
            sum_total['interval_days'] = map(change_days, sum_total['days'])
            logger.info('Total debt lines: \n {}'.format(
                sum_total.count())
            )
            if not sum_total.empty:
                aggregation = ['interval_days', 'cuenta', 'organismo_oficial']
                summarize_file = tempfile.mkstemp(suffix=".xlsx")[1]
                export_data = sum_total.groupby(
                    aggregation, as_index=False).agg(
                    {"deuda": "sum"})
                # Use the StringIO object as the filehandle.
                writer = pd.ExcelWriter(summarize_file)
                logger.warning(
                    'generate summarize file to {}'.format(summarize_file)
                )
                export_data.to_excel(writer)
                writer.close()
                detailed_file = tempfile.mkstemp(suffix=".xlsx")[1]
                writer = pd.ExcelWriter(detailed_file)
                logger.warning(
                    'generate detailed file to {}'.format(detailed_file)
                )
                sum_total.to_excel(writer)
                writer.close()
                if wiz.between_dates:
                    sum_filename = _('antiguetat_deuda_{}_entre_{}_{}.xlsx').format(
                        wiz.date, wiz.date_start, wiz.date_end,
                    )
                    detailed_filename = _('detall_antiguetat_deuda_{}_entre_{}_{}.xlsx').format(
                        wiz.date, wiz.date_start, wiz.date_end,
                    )
                else:
                    sum_filename = _('antiguetat_deuda_{}.xlsx').format(wiz.date)
                    detailed_filename = _('detall_antiguetat_deuda_{}.xlsx').format(
                        wiz.date
                    )
                with open(summarize_file) as sum_f:
                    wiz.write({
                        'summarize_file': base64.b64encode(sum_f.read()),
                        'summarize_filename': sum_filename
                    })
                with open(detailed_file) as det_f:
                    wiz.write({
                        'detailed_file': base64.b64encode(det_f.read()),
                        'detailed_filename': detailed_filename
                    })
                return sum_total
            else:
                logger.warning('Not invoices to calculate')
                return None

    def get_antiquity_debt(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        result = self.get_between_dates_antiquity_debt(
            cursor, uid, ids, context=None
        )


    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context=context)
        result = self.get_between_dates_antiquity_debt(
            cursor, uid, ids, context=None
        )
        if not result is None:
            aggregation = ['interval_days', 'cuenta', 'organismo_oficial']
            export_data = result.groupby(
                aggregation, as_index=False).agg(
                {"deuda": "sum"})
            debt = {}
            for row in export_data.to_dict('records'):
                acc = '{}{}'.format(row['cuenta'], row['organismo_oficial'])
                debt.setdefault(acc, {})
                debt[acc][row['interval_days']] = row['deuda']
        else:
            debt = {}
        datas = {
            'form': wiz.read(), 'debt': debt, 'report_type': wiz.format,
            'between_dates': wiz.between_dates, 'start_date': wiz.date_start,
            'end_date': wiz.date_end
        }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'antiquity.debt',
            'datas': datas,
        }


WizardAntiquityDebt()
