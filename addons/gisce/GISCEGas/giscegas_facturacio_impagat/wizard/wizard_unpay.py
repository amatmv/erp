# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscegasWizardUnpay(osv.osv_memory):
    '''
    Wizard per marcar com a no pagada una factura fent servir la comptabilitat
    '''
    _name = 'giscegas.wizard.unpay'
    _inherit = 'giscegas.wizard.unpay'
    
    def unpay(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context)
        ctx = context.copy()
        ctx['unpay_move_pending_state'] = wiz.go_on_pending_state
        return super(GiscegasWizardUnpay, self).unpay(
            cursor, uid, ids, context=ctx
        )

    def _default_go_on_pending_state(self, cursor, uid, context=None):
        config_obj = self.pool.get('res.config')
        return int(config_obj.get(
            cursor, uid, 'unpay_move_pending_state', 0)
        )

    def _default_pay_account_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        if 'active_ids' in context and 'model' in context:
            obj = self.pool.get(context['model'])
            invoice_id = context['active_ids'][0]
            invoice = obj.browse(cursor, uid, invoice_id, context=context)
            return invoice.partner_id.property_account_debtor.id
        return False

    _columns = {
        'go_on_pending_state': fields.boolean(
            'Avanzar estado pendiente',
        )
    }

    _defaults = {
        'go_on_pending_state': _default_go_on_pending_state,
        'pay_account_id': _default_pay_account_id
    }


GiscegasWizardUnpay()
