# -*- encoding: utf-8 -*-
import logging
from datetime import timedelta, datetime
from ast import literal_eval

import pooler
from osv import osv
from tools.translate import _

from workalendar.europe import Spain


def must_cancel_A1_44(cursor, uid, factura_id, state_id, context=None):
    """Check if we can cancel a A1_44 checking if there are more pending invoices.

    :param cursor: Database cursor
    :param uid: User Id
    :param factura_id: Invoice id
    :param state_id: Next pending state id
    :param context: Context application
    :returns True if we can cancel the process
    """
    pool = pooler.get_pool(cursor.dbname)
    sw_obj = pool.get('giscegas.atr')
    return sw_obj.must_cancel_a1_44(
        cursor, uid, factura_id, state_id, context=context)


def find_A1_44(cursor, uid, factura_id, step=None, context=None):
    """Find the A1_44 process

    Only search for process which are in 'open' or 'draft' state and linked
    with the same contract.

    :param cursor: Database cursor
    :param uid: User id
    :param factura_id: Infoice id
    :param step: Step name (optional)
    :param context: Application context
    :returns ids of A1_44 process
    """
    pool = pooler.get_pool(cursor.dbname)
    sw_obj = pool.get('giscegas.atr')
    return sw_obj.find_a1_44(cursor, uid, factura_id, step=step, context=context)


def cancel_A1_44(cursor, uid, sw_id, context=None):
    """Cancel the A1_44 process.

    The steps are the following:
        - If the ATR process in in draft state it can be cancelled directly.
        - If ATR is ended is not possible to cancel.
        - If ATR is in step 01 and is not marked as sent it can be cancelled
          directly.
        - In other cases step 03 is generated.

    :param cursor: Database cursor.
    :param uid: User identifier.
    :param sw_id: ATR Case id.
    :param context: Application context.
    :returns True if has been cancelled or False if not.
    """
    pool = pooler.get_pool(cursor.dbname)
    sw_obj = pool.get('giscegas.atr')
    return sw_obj.cancel_a1_44(cursor, uid, sw_id, context=context)


class GiscegasFacturacioFacturaPending(osv.osv):

    _name = 'giscegas.facturacio.factura'
    _inherit = 'giscegas.facturacio.factura'

    def set_pending_pre_hook(self, cursor, uid, ids, pending_id, context=None):
        polissa_obj = self.pool.get('giscegas.polissa')
        cfg_obj = self.pool.get('res.config')
        atr_enabled = hasattr(polissa_obj, 'crear_cas_atr')

        a1_44_from_invoices = int(
            cfg_obj.get(cursor, uid, 'giscegas_atr_a1_44_from_pending_state', '0')
        )

        # if atr_enabled and a1_44_from_invoices:
        #     for f_id in ids:
        #         if must_cancel_A1_44(cursor, uid, f_id, pending_id):
        #             a1_44_ids = find_A1_44(cursor, uid, f_id)
        #             for a1_44 in a1_44_ids:
        #                 cancel_A1_44(cursor, uid, a1_44, context=context)
        return True

    def generate_unpayment_expenses(self, cursor, uid, fact_ids, context=None):
        # search for a giscegas.facturacio.factura linked with the
        # invoice, and generate extra lines
        pool = pooler.get_pool(cursor.dbname)
        fact_obj = pool.get('giscegas.facturacio.factura')
        extra_obj = pool.get('giscegas.facturacio.extra')
        imd_obj = pool.get('ir.model.data')
        prod_obj = pool.get('product.product')

        unpayment_fee_product_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio_impagat', 'giscegas_product_unpaid_management'
        )[1]
        uos_id = imd_obj.get_object_reference(cursor, uid,
                                              'product', 'product_uom_unit'
                                              )[1]
        journal_id = imd_obj.get_object_reference(cursor, uid,
                                                  'giscegas_facturacio',
                                                  'facturacio_journal_gas'
                                                  )[1]
        unpayment_fee_product = prod_obj.browse(
            cursor, uid, unpayment_fee_product_id
        )
        extra_created = {}
        unpayment_text = (
            unpayment_fee_product.description_sale or
            unpayment_fee_product.description or
            _(u'Despeses gestió impagament')
        )
        for fact in fact_obj.browse(cursor, uid, fact_ids, context=context):
            vals = {
                'name': _(u'{0} {1}').format(unpayment_text, fact.number),
                'polissa_id': fact.polissa_id.id,
                'product_id': unpayment_fee_product_id,
                'uos_id': uos_id,
                'date_from': fact.date_invoice,
                'date_to': fact.date_invoice,
                'price_unit': unpayment_fee_product.list_price,
                'quantity': 1,
                'term': 1,
                'account_id': unpayment_fee_product.property_account_income.id,
                'tax_ids': [
                    (6, 0, [t.id for t in unpayment_fee_product.taxes_id])
                ],
                'journal_ids': [(6, 0, [journal_id])]
            }
            id_extra = extra_obj.create(cursor, uid, vals)
            extra_created.update({id_extra: fact.number})
        return extra_created

    def check_if_generate_a1_44_by_id(self, cursor, uid, factura_id, context=None):
        if context is None:
            context = {}
        cfg_obj = self.pool.get('res.config')
        polissa_obj = self.pool.get('giscegas.polissa')

        a1_44_from_invoices = int(
            cfg_obj.get(cursor, uid, 'giscegas_atr_a1_44_from_pending_state', '0')
        )
        atr_enabled = hasattr(polissa_obj, 'crear_cas_atr')
        factura = self.browse(cursor, uid, factura_id, context=context)
        return self.check_if_generate_a1_44(
            cursor, uid, factura, atr_enabled, a1_44_from_invoices)

    def check_if_generate_a1_44(self, cursor, uid, factura, atr_enabled, a1_44_from_invoices):
        polissa = factura.polissa_id
        return (
            factura.pending_state.is_last and
            polissa.active and atr_enabled and a1_44_from_invoices
            and not polissa.nocutoff
            )

    def set_pending_post_hook(self, cursor, uid, ids, pending_id, context=None):
        logger = logging.getLogger(
            'openerp.{0}.set_pending_post_hook'.format(__name__)
        )
        # polissa_obj = self.pool.get('giscegas.polissa')
        # sw_obj = self.pool.get('giscegas.atr')
        # cfg_obj = self.pool.get('res.config')
        #
        # atr_enabled = ha  sattr(polissa_obj, 'crear_cas_atr')
        # a1_44_cat_canceled = int(
        #     cfg_obj.get(cursor, uid, 'giscegas_atr_a1_44_cat_canceled', '0')
        # )
        # a1_44_from_invoices = int(
        #     cfg_obj.get(cursor, uid, 'giscegas_atr_a1_44_from_pending_state', '0')
        # )
        # dies_no_permesos = literal_eval(
        #     cfg_obj.get(cursor, uid, 'giscegas_atr_a1_44_dies_no_permesos', '[4, 5, 6]')
        # )
        # management_expenses = int(
        #     cfg_obj.get(cursor, uid, 'giscegas_fact_unpaid_management_expenses', '0')
        # )
        # for factura in self.browse(cursor, uid, ids, context=context):
        #     polissa = factura.polissa_id
        #     if self.check_if_generate_a1_44(cursor, uid,
        #         factura, atr_enabled, a1_44_from_invoices):
        #         if find_A1_44(cursor, uid, factura.id, context=context):
        #             logger.info('A1_44 found for Invoice {0}'.format(
        #                     factura.number
        #             ))
        #             continue
        #         # Generar A1_44-03
        #         logger.info(
        #             'Invoice {0} from contract {1} is in the pending last '
        #             'state. Generating a A1_44-01 (Motivo 03) ATR.'.format(
        #                 factura.number, polissa.name
        #             )
        #         )
        #
        #         # Calcular data final
        #         ctx = context.copy()
        #         ctx.update({'sector': 'gas'})
        #         data_accio = factura.pending_state.process_id.get_cutoff_day(
        #             context=ctx
        #         )
        #
        #         config = dict(
        #             data_accio=data_accio.strftime('%Y-%m-%d'),
        #             motiu='03',
        #         )
        #         res = polissa.crear_cas_atr('44', config, context=context)
        #         sw_id = res[2]
        #         if not res[2]:
        #             raise osv.except_osv('Error', res[1])
        #         state = 'draft'
        #         # cancelar els talls a persones fisiques a CAT
        #         if a1_44_cat_canceled:
        #             if factura.cups_id.id_municipi.state.\
        #                     comunitat_autonoma.codi == '09' \
        #                     and factura.partner_id.cifnif == 'NI':
        #                 state = 'pending'
        #         sw_obj.write(cursor, uid, [sw_id], {
        #             'ref': 'giscegas.facturacio.factura,{0}'.format(factura.id),
        #             'state': state
        #         })
        #     if factura.pending_state.is_last:
        #         if management_expenses:
        #             logger.info(
        #                 'Unpaid management expenses for invoice {0}.'.format(
        #                     factura.number
        #                 )
        #             )
        #             self.generate_unpayment_expenses(
        #                 cursor, uid, [factura.id], context=context
        #             )
        return True

    def set_pending(self, cursor, uid, ids, pending_id, context=None):
        if context is None:
            context = {}

        invoice_ids = self._get_base_ids(cursor, uid, ids)
        res = self.pool.get('account.invoice').set_pending(cursor,
                                            uid, invoice_ids,
                                            pending_id, context=context)

        return res

    def go_on_pending(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        invoice_ids = self._get_base_ids(cursor, uid, ids)
        res = self.pool.get('account.invoice').go_on_pending(
            cursor, uid, invoice_ids, context=context
        )

        return res


GiscegasFacturacioFacturaPending()
