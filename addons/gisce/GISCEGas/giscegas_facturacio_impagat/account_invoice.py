# -*- encoding: utf-8 -*-
from ast import literal_eval

from osv import osv


class AccountInvoicePendingStateProcess(osv.osv):

    """ Classe que extén el procés de tall per a que per a que segons el sector,
    es pugui gestionar el dia de tall dels impagaments"""

    _name = 'account.invoice.pending.state.process'
    _inherit = 'account.invoice.pending.state.process'

    def get_non_allowed_cutoff_days(self, cursor, uid, context=None):
        if context is None:
            context = {}

        if context.get('sector') == 'gas':
            return literal_eval(self.pool.get('res.config').get(
                cursor, uid, 'giscegas_atr_a1_44_dies_no_permesos', '[4, 5, 6]'
            ))
        return super(AccountInvoicePendingStateProcess, self).get_non_allowed_cutoff_days(
            cursor, uid, context=context
        )


AccountInvoicePendingStateProcess()
