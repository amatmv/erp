# coding=utf-8
from StringIO import StringIO

from c2c_webkit_report import webkit_report
from report import report_sxw
from base_extended.excel import Sheet


class XLSDebtReport(object):

    DAYS = ('30', '60', '90', '120', '150', '180', '210', '240', '270', '300',
            '330', '360', '>360')

    def __init__(self, debt):
        self.sheet = Sheet('Debt')
        self.debt = debt

    def report(self):
        self.sheet.add_row(('',) + self.DAYS)
        for k, v in self.debt.items():
            aging = [k] + [v.get(d, 0) for d in self.DAYS]
            self.sheet.add_row(aging)
        totals = ['Totals'] + [
            sum(v.get(d, 0) for v in self.debt.values()) for d in self.DAYS
        ]
        self.sheet.add_row(totals)
        out = StringIO()
        self.sheet.save(out)
        res = out.getvalue()
        out.close()
        return res


class DummyHeader(object):
    margin_top = 0
    margin_bottom = 0
    margin_left = 0
    margin_right = 0
    orientation = 'Landscape'
    format = 'A4'
    html = '<html></html>'
    footer_html = '<html></html>'
    css = ''


class NoXMLWebkitReport(webkit_report.WebKitParser):

    def create(self, cursor, uid, ids, data, context=None):
        report_type = data.get('report_type', 'pdf')
        if report_type == 'xls':
            report = XLSDebtReport(data['debt'])
            return report.report(), 'xls'
        return super(NoXMLWebkitReport, self).create(
            cursor, uid, ids, data, context
        )

    def create_single_pdf(self, cursor, uid, ids, data, report_xml=False,
                          context=None):
        webkit_header = getattr(report_xml, 'webkit_header', None)
        if not webkit_header:
            report_xml.webkit_header = DummyHeader()
        return super(NoXMLWebkitReport, self).create_single_pdf(
            cursor, uid, ids, data, report_xml, context
        )


NoXMLWebkitReport(
    'report.antiquity.debt', 'wizard.antiquity.debt',
    'giscegas_facturacio_impagat/report/report_antiquity_debt.mako',
    parser=report_sxw.rml_parse
)
