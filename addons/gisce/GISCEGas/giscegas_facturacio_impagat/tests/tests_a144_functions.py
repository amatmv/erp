# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction

from giscegas_facturacio_impagat.giscegas_facturacio import find_A1_44


class TestB1Functions(testing.OOTestCase):

    def test_find_b1(self):
        sw_obj = self.openerp.pool.get('giscegas.atr')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Enable contact query
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
            )[1]
            found = sw_obj.find_a144(cursor, uid, factura_id)
            assert not found

    def test_wrapper_find_b1_equal(self):
        sw_obj = self.openerp.pool.get('giscegas.atr')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Enable contact query
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
            )[1]
            found = sw_obj.find_a144(cursor, uid, factura_id)
            assert not found
            found_b1 = find_A1_44(cursor, uid, factura_id)
            assert found == found_b1
