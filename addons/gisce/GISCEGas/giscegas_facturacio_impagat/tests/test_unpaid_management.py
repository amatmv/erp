# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from destral import testing
from destral.transaction import Transaction

from giscegas_facturacio.tests.utils import prepare_fiscal_year

from workalendar.europe import Spain

import netsvc


class TestUnpaidManagement(testing.OOTestCase):
    # def test_go_on_pending_with_extra_line(self):
    #     """Test pending state from invoice is going on.
    #     """
    #     pool = self.openerp.pool
    #     imd_obj = pool.get('ir.model.data')
    #     pstate_obj = pool.get('account.invoice.pending.state')
    #     extra_obj = pool.get('giscegas.facturacio.extra')
    #     fact_obj = pool.get('giscegas.facturacio.factura')
    #     config_obj = pool.get('res.config')
    #     with Transaction().start(self.database) as txn:
    #         cursor = txn.cursor
    #         uid = txn.user
    #         # Preprare fiscal year
    #
    #         prepare_fiscal_year(pool, cursor, uid)
    #
    #         model, ref = imd_obj.get_object_reference(
    #             cursor, uid, 'giscegas_facturacio', 'factura_gas_0007_polissa_0001_N'
    #         )
    #         correct_state_id = imd_obj.get_object_reference(
    #             cursor, uid,
    #             'account_invoice_pending', 'default_invoice_pending_state'
    #         )[1]
    #         default_process_id = imd_obj.get_object_reference(
    #             cursor, uid, 'account_invoice_pending',
    #             'default_pending_state_process'
    #         )[1]
    #         # configure unpaid management
    #         config_unpaid_id = imd_obj.get_object_reference(
    #             cursor, uid, 'giscegas_facturacio_impagat',
    #             'giscegas_config_unpaid_management_expenses'
    #         )[1]
    #         config_obj.write(cursor, uid, [config_unpaid_id], {'value': '1'})
    #
    #         invoice_id = fact_obj.copy(cursor, uid, ref)
    #         invoice = fact_obj.browse(cursor, uid, invoice_id)
    #         wf_service = netsvc.LocalService("workflow")
    #         wf_service.trg_validate(
    #             uid, 'account.invoice', invoice.invoice_id.id, 'invoice_open', cursor
    #         )
    #         invoice = fact_obj.browse(cursor, uid, invoice_id)
    #         self.assertEqual(invoice.state, 'open')
    #         invoice.set_pending(correct_state_id)
    #         # Refresh invoice object
    #         invoice = fact_obj.browse(cursor, uid, invoice_id)
    #         self.assertEqual(invoice.pending_state.id, correct_state_id)
    #
    #         id_last = pstate_obj.create(cursor, uid, {
    #             'name': 'Last state 10',
    #             'weight': 10,
    #             'process_id': default_process_id
    #         })
    #         invoice.set_pending(id_last)
    #         # Refresh invoice object
    #         invoice = fact_obj.browse(cursor, uid, invoice_id)
    #         self.assertEqual(invoice.pending_state.id, id_last)
    #         unpayment_fee_product_id = imd_obj.get_object_reference(
    #             cursor, uid, 'giscegas_facturacio_impagat',
    #             'giscegas_product_unpaid_management'
    #         )[1]
    #         extra_lines = extra_obj.search(
    #             cursor, uid, [('product_id', '=', unpayment_fee_product_id)]
    #         )
    #         self.assertIs(1, len(extra_lines))

    def test_go_on_pending_without_extra_line(self):
        """Test pending state from invoice is going on.
        """
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        pstate_obj = pool.get('account.invoice.pending.state')
        extra_obj = pool.get('giscegas.facturacio.extra')
        fact_obj = pool.get('giscegas.facturacio.factura')
        config_obj = pool.get('res.config')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            prepare_fiscal_year(pool, cursor, uid)
            model, ref = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0007_polissa_0001_N'
            )
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]
            # without configure unpaid management
            config_unpaid_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_impagat',
                'giscegas_config_unpaid_management_expenses'
            )[1]
            config_obj.write(cursor, uid, [config_unpaid_id], {'value': '0'})

            invoice_id = fact_obj.copy(cursor, uid, ref)
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', invoice.invoice_id.id, 'invoice_open',
                cursor
            )
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.state, 'open')
            invoice.set_pending(correct_state_id)
            # Refresh invoice object
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.pending_state.id, correct_state_id)

            id_last = pstate_obj.create(cursor, uid, {
                'name': 'Last state 10',
                'weight': 10,
                'process_id': default_process_id
            })
            invoice.set_pending(id_last)
            # Refresh invoice object
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.pending_state.id, id_last)
            unpayment_fee_product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio_impagat',
                'giscegas_product_unpaid_management')[1]
            extra_lines = extra_obj.search(
                cursor, uid, [('product_id', '=', unpayment_fee_product_id)]
            )
            self.assertIs(0, len(extra_lines))


# class TestUnpaidManagementa144Polissa(testing.OOTestCase):
#
#     def clear_cache_pending(self, txn):
#         pool = self.openerp.pool
#         polissa_obj = pool.get('giscegas.polissa')
#         account_pending_obj = pool.get('account.invoice.pending.state')
#         cursor = txn.cursor
#         account_pending_obj.get_next.clear_cache(cursor.dbname)
#         polissa_obj._get_pending_states.clear_cache(cursor.dbname)
#
#     def calc_action_day(self, start_day):
#         not_allowed_days = [4, 5, 6]
#         allowed_days = [0, 1, 2, 3]
#         allowed_days = list(set(allowed_days) - set(not_allowed_days))
#         calendar = Spain()
#         action_day = start_day
#         while action_day.weekday() not in allowed_days:
#             action_day = calendar.add_working_days(
#                 action_day, 1)
#         return action_day
#
#     def prepare_for_test(self, txn):
#         pool = self.openerp.pool
#         polissa_obj = pool.get('giscegas.polissa')
#         imd_obj = pool.get('ir.model.data')
#
#         cursor = txn.cursor
#         uid = txn.user
#
#         model, ref = imd_obj.get_object_reference(
#             cursor, uid, 'giscegas_facturacio', 'factura_gas_0007_polissa_0001_N'
#         )
#         # Create a new invoice (giscegas.facturacio.factura)
#         fact_obj = pool.get('giscegas.facturacio.factura')
#         self.factura_id = fact_obj.copy(cursor, uid, ref)
#         factura = fact_obj.browse(cursor, uid, self.factura_id)
#
#         prepare_fiscal_year(pool, cursor, uid)
#
#         partner_obj = pool.get('res.partner')
#         partner_obj.write(cursor, uid, [factura.partner_id.id], {
#             'ref': '4321'
#         })
#         partner_obj.write(cursor, uid, [factura.company_id.id], {
#             'ref': '1234'
#         })
#         cups_obj = pool.get('giscegas.cups.ps')
#         cups_obj.write(cursor, uid, [factura.cups_id.id], {
#             'distribuidora_id': factura.partner_id.id
#         })
#
#         # Activate the polissa
#         polissa_id = factura.polissa_id.id
#         polissa_obj.write(
#             cursor, uid, [polissa_id], {'comercialitzadora': 1})
#         polissa_obj.send_signal(cursor, uid, [polissa_id], [
#             'validar', 'contracte'
#         ])
#
#         # Allow to create a144 from
#         config_a1_44_from_pending_state_id = imd_obj.get_object_reference(
#             cursor, uid, 'giscegas_facturacio_impagat',
#             'giscegas_config_atr_a1_44_from_pending_state')[1]
#         config_obj = pool.get('res.config')
#         config_obj.write(
#             cursor, uid, [config_a1_44_from_pending_state_id], {'value': '1'})
#
#         # Create request link
#         rl_obj = pool.get('res.request.link')
#         rl_obj.create(cursor, uid, {
#             'priority': 30, 'object': 'giscegas.facturacio.factura',
#             'name': 'Factura'
#         })
#
#     def test_dont_generate_a144_if_not_cutoff(self):
#         """Test if it doesn't generate a144 if polissa is not cutoff."""
#         pool = self.openerp.pool
#         imd_obj = pool.get('ir.model.data')
#         fact_obj = pool.get('giscegas.facturacio.factura')
#         with Transaction().start(self.database) as txn:
#             cursor = txn.cursor
#             uid = txn.user
#             self.prepare_for_test(txn)
#             correct_state_id = imd_obj.get_object_reference(
#                 cursor, uid,
#                 'account_invoice_pending', 'default_invoice_pending_state'
#             )[1]
#             default_process_id = imd_obj.get_object_reference(
#                 cursor, uid, 'account_invoice_pending',
#                 'default_pending_state_process'
#             )[1]
#
#             # Create the invoice last pending state
#             pstate_obj = pool.get('account.invoice.pending.state')
#             last_pending_state_id = pstate_obj.create(cursor, uid, {
#                 'name': 'Last state 10',
#                 'weight': 10,
#                 'process_id': default_process_id
#             })
#             # Clear cache
#             self.clear_cache_pending(txn)
#             # Create a new invoice (giscegas.facturacio.factura)
#             factura_id = self.factura_id
#             factura = fact_obj.browse(cursor, uid, factura_id)
#
#             # Make the polissa no-cutoff
#             nocutoff_id = imd_obj.get_object_reference(
#                 cursor, uid, 'giscegas_polissa', 'd2_nocutoff'
#             )[1]
#             factura.polissa_id.write({'nocutoff': nocutoff_id})
#
#             # Open the invoice
#             wf_service = netsvc.LocalService("workflow")
#             wf_service.trg_validate(
#                 uid, 'account.invoice', factura.invoice_id.id,
#                 'invoice_open', cursor
#             )
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.state, 'open')
#
#             # Set the invoice pending state to correct
#             factura.set_pending(correct_state_id)
#             # Refresh invoice object
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.pending_state.id, correct_state_id)
#
#             # Set the invoice pending state to last
#             factura.set_pending(last_pending_state_id)
#             # Refresh invoice object
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.pending_state.id, last_pending_state_id)
#
#             # Check a144 was created for the invoice
#             sw_obj = pool.get('giscegas.atr')
#             a144_found = sw_obj.find_a1_44(cursor, uid, factura_id)
#             self.assertTrue(len(a144_found) == 0)
#
#     def test_go_on_pending_with_a144_generated(self):
#         """Test last pending state from invoice generates a a144 if the
#         polissa is active"""
#         pool = self.openerp.pool
#         imd_obj = pool.get('ir.model.data')
#         fact_obj = pool.get('giscegas.facturacio.factura')
#         with Transaction().start(self.database) as txn:
#             cursor = txn.cursor
#             uid = txn.user
#             self.prepare_for_test(txn)
#             correct_state_id = imd_obj.get_object_reference(
#                 cursor, uid,
#                 'account_invoice_pending', 'default_invoice_pending_state'
#             )[1]
#             default_process_id = imd_obj.get_object_reference(
#                 cursor, uid, 'account_invoice_pending',
#                 'default_pending_state_process'
#             )[1]
#
#             # Create the invoice last pending state
#             pstate_obj = pool.get('account.invoice.pending.state')
#             last_pending_state_id = pstate_obj.create(cursor, uid, {
#                 'name': 'Last state 10',
#                 'weight': 10,
#                 'process_id': default_process_id
#             })
#             # Clear cache
#             self.clear_cache_pending(txn)
#             # Create a new invoice (giscegas.facturacio.factura)
#             factura_id = self.factura_id
#             factura = fact_obj.browse(cursor, uid, factura_id)
#
#             # Open the invoice
#             wf_service = netsvc.LocalService("workflow")
#             wf_service.trg_validate(
#                 uid, 'account.invoice', factura.invoice_id.id,
#                 'invoice_open', cursor
#             )
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.state, 'open')
#
#             # Set the invoice pending state to correct
#             factura.set_pending(correct_state_id)
#             # Refresh invoice object
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.pending_state.id, correct_state_id)
#
#             # Set the invoice pending state to last
#             factura.set_pending(last_pending_state_id)
#             # Refresh invoice object
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.pending_state.id, last_pending_state_id)
#
#             # Check a144 was created for the invoice
#             sw_obj = pool.get('giscegas.atr')
#             a144_found = sw_obj.find_a1_44(cursor, uid, factura_id)
#             self.assertTrue(len(a144_found) > 0)
#
#     def test_go_on_pending_without_a144_generated(self):
#         """Test last pending state from invoice doesn't generate a a144 if the
#         polissa is not active"""
#         pool = self.openerp.pool
#         imd_obj = pool.get('ir.model.data')
#         fact_obj = pool.get('giscegas.facturacio.factura')
#         with Transaction().start(self.database) as txn:
#             cursor = txn.cursor
#             uid = txn.user
#             self.prepare_for_test(txn)
#             correct_state_id = imd_obj.get_object_reference(
#                 cursor, uid,
#                 'account_invoice_pending', 'default_invoice_pending_state'
#             )[1]
#             default_process_id = imd_obj.get_object_reference(
#                 cursor, uid, 'account_invoice_pending',
#                 'default_pending_state_process'
#             )[1]
#
#             # Create the invoice last pending state
#             pstate_obj = pool.get('account.invoice.pending.state')
#             last_pending_state_id = pstate_obj.create(cursor, uid, {
#                 'name': 'Last state 10',
#                 'weight': 10,
#                 'process_id': default_process_id
#             })
#             # Clear cache
#             self.clear_cache_pending(txn)
#             # Create a new invoice (giscegas.facturacio.factura)
#             factura_id = self.factura_id
#             factura = fact_obj.browse(cursor, uid, factura_id)
#
#             # Open the invoice
#             wf_service = netsvc.LocalService("workflow")
#             wf_service.trg_validate(
#                 uid, 'account.invoice', factura.invoice_id.id,
#                 'invoice_open', cursor
#             )
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.state, 'open')
#
#             # Set the invoice pending state to correct
#             factura.set_pending(correct_state_id)
#             # Refresh invoice object
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.pending_state.id, correct_state_id)
#
#             # Activate the polissa
#             polissa_id = factura.polissa_id.id
#             polissa_obj = pool.get('giscegas.polissa')
#             polissa_obj.write(
#                 cursor, uid, [polissa_id], {'renovacio_auto': False,
#                                             'data_baixa': '2018-04-01'})
#             polissa = polissa_obj.browse(cursor, uid, polissa_id)
#             for comptador in polissa.comptadors:
#                 if comptador.active:
#                     comptador.write({'active': False})
#             polissa_obj.send_signal(cursor, uid, [polissa_id], [
#                 'baixa', 'contracte'
#             ])
#
#             # Set the invoice pending state to last
#             factura.set_pending(last_pending_state_id)
#             # Refresh invoice object
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.pending_state.id, last_pending_state_id)
#
#             # Check a144 was created for the invoice
#             sw_obj = pool.get('giscegas.atr')
#             a144_found = sw_obj.find_a1_44(cursor, uid, factura_id)
#             self.assertTrue(len(a144_found) == 0)
#
#     def test_process_calculate_natural_days(self):
#         """Test last pending state from invoice generates a a144 if the
#         polissa is active"""
#         pool = self.openerp.pool
#         imd_obj = pool.get('ir.model.data')
#         with Transaction().start(self.database) as txn:
#             cursor = txn.cursor
#             uid = txn.user
#             self.prepare_for_test(txn)
#             correct_state_id = imd_obj.get_object_reference(
#                 cursor, uid,
#                 'account_invoice_pending', 'default_invoice_pending_state'
#             )[1]
#             default_process_id = imd_obj.get_object_reference(
#                 cursor, uid, 'account_invoice_pending',
#                 'default_pending_state_process'
#             )[1]
#             pprocess_obj = pool.get('account.invoice.pending.state.process')
#             pprocess_obj.write(cursor, uid, [default_process_id], {
#                 'cutoff_days': 14, 'days_type': 'natural'
#             })
#             action_day = datetime.now()
#             action_day = action_day + timedelta(days=14)
#             a1_44_action_day = self.calc_action_day(action_day)
#             process_action_day = pprocess_obj.get_cutoff_day(
#                 cursor, uid, default_process_id, not_allowed_days=[4, 5, 6])
#             self.assertGreaterEqual(process_action_day, action_day)
#             self.assertEqual(
#                 a1_44_action_day.strftime('%Y-%m-%d'),
#                 process_action_day.strftime('%Y-%m-%d')
#             )
#
#     def test_process_calculate_bussines_days(self):
#         """Test last pending state from invoice generates a a144 if the
#         polissa is active"""
#         pool = self.openerp.pool
#         imd_obj = pool.get('ir.model.data')
#         with Transaction().start(self.database) as txn:
#             cursor = txn.cursor
#             uid = txn.user
#             self.prepare_for_test(txn)
#             correct_state_id = imd_obj.get_object_reference(
#                 cursor, uid,
#                 'account_invoice_pending', 'default_invoice_pending_state'
#             )[1]
#             default_process_id = imd_obj.get_object_reference(
#                 cursor, uid, 'account_invoice_pending',
#                 'default_pending_state_process'
#             )[1]
#             pprocess_obj = pool.get('account.invoice.pending.state.process')
#             pprocess_obj.write(cursor, uid, [default_process_id], {
#                 'cutoff_days': 14, 'days_type': 'business'
#             })
#             action_day = datetime.now()
#             calendar = Spain()
#             action_day = calendar.add_working_days(action_day, 14)
#             a1_44_action_day = self.calc_action_day(action_day)
#             process_action_day = pprocess_obj.get_cutoff_day(
#                 cursor, uid, default_process_id, not_allowed_days=[4, 5, 6])
#             self.assertGreaterEqual(
#                 process_action_day, a1_44_action_day
#             )
#             self.assertEqual(
#                 a1_44_action_day.strftime('%Y-%m-%d'),
#                 process_action_day.strftime('%Y-%m-%d')
#             )
#
#     def test_go_on_pending_with_a144_generated_correct_day(self):
#         """Test last pending state from invoice generates a a144 if the
#         polissa is active"""
#         pool = self.openerp.pool
#         imd_obj = pool.get('ir.model.data')
#         fact_obj = pool.get('giscegas.facturacio.factura')
#         with Transaction().start(self.database) as txn:
#             cursor = txn.cursor
#             uid = txn.user
#             self.prepare_for_test(txn)
#             correct_state_id = imd_obj.get_object_reference(
#                 cursor, uid,
#                 'account_invoice_pending', 'default_invoice_pending_state'
#             )[1]
#             default_process_id = imd_obj.get_object_reference(
#                 cursor, uid, 'account_invoice_pending',
#                 'default_pending_state_process'
#             )[1]
#
#             # Create the invoice last pending state
#             pstate_obj = pool.get('account.invoice.pending.state')
#             last_pending_state_id = pstate_obj.create(cursor, uid, {
#                 'name': 'Last state 10',
#                 'weight': 10,
#                 'process_id': default_process_id
#             })
#             pprocess_obj = pool.get('account.invoice.pending.state.process')
#             pprocess_obj.write(cursor, uid, [default_process_id], {
#                 'cutoff_days': 14, 'days_type': 'business'
#             })
#             # Clear cache
#             self.clear_cache_pending(txn)
#             # Create a new invoice (giscegas.facturacio.factura)
#             factura_id = self.factura_id
#             factura = fact_obj.browse(cursor, uid, factura_id)
#
#             # Open the invoice
#             wf_service = netsvc.LocalService("workflow")
#             wf_service.trg_validate(
#                 uid, 'account.invoice', factura.invoice_id.id,
#                 'invoice_open', cursor
#             )
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.state, 'open')
#
#             # Set the invoice pending state to correct
#             factura.set_pending(correct_state_id)
#             # Refresh invoice object
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.pending_state.id, correct_state_id)
#
#             # Set the invoice pending state to last
#             factura.set_pending(last_pending_state_id)
#             # Refresh invoice object
#             factura = fact_obj.browse(cursor, uid, factura_id)
#             self.assertEqual(factura.pending_state.id, last_pending_state_id)
#
#             # Check a144 was created for the invoice
#             sw_obj = pool.get('giscegas.atr')
#             a1_44_01_obj = pool.get('giscegas.atr.44.01')
#             a144_found = sw_obj.find_a1_44(cursor, uid, factura_id)
#             self.assertTrue(len(a144_found) > 0)
#
#             a1_44_01 = a1_44_01_obj.search(cursor, uid, [('sw_id', '=', a144_found[0])])
#             a1_44_01_info = a1_44_01_obj.read(cursor, uid, a1_44_01[0], ['data_accio'])
#             action_day = datetime.now()
#             calendar = Spain()
#             action_day = calendar.add_working_days(action_day, 14)
#             a1_44_action_day = self.calc_action_day(action_day)
#             self.assertEqual(a1_44_01_info['data_accio'], a1_44_action_day.strftime('%Y-%m-%d'))
