# -*- encoding: utf-8 -*-

from osv import fields, osv


class GiscegasFacturacioFactura(osv.osv):

    _name = 'giscegas.facturacio.factura'
    _inherit = 'giscegas.facturacio.factura'

    _columns = {
        'devolucio_id': fields.many2one(
            'giscedata.facturacio.devolucio', 'Devolució'
        )
    }

    def copy_data(self, cr, uid, id, default=None, context=None):
        default.update({'devolucio_id': False})
        return super(GiscegasFacturacioFactura, self).copy_data(cr, uid, id, default, context)


GiscegasFacturacioFactura()
