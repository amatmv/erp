# -*- coding: utf-8 -*-
{
    "name": "Devolucions remeses de gas",
    "description": """
        Gestió de devolucions del banc
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "Facturació",
    "depends": [
        "devolucions_base",
        "giscegas_facturacio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscegas_facturacio_devolucions_data.xml",
        "giscegas_facturacio_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
