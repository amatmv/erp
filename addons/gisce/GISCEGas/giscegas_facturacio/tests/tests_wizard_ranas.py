from destral import testing
from destral.transaction import Transaction



class TestWizardReRectifiedInvoices(testing.OOTestCase):

    # A rectified invoice shouldn't be rectified again
    """
    This testing method try to re-rectify an invoice,
        if program allows re-rectification test fails
        otherwise test passed
    """
    def test_prevent_rerectified_invoices(self):
        from osv import osv

        # Install tarifas 2018 module
        self.openerp.install_module(
            'giscegas_tarifas_peajes_20180101'
        )

        # Get policy's table from DB
        pol_obj = self.openerp.pool.get('giscegas.polissa')

        # Get invoices table from DB.
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')

        # Link xml files with modules.
        imd_obj = self.openerp.pool.get('ir.model.data')

        # Get wizzard object.
        wiz_obj = self.openerp.pool.get('giscegas.wizard.ranas')

        # Context reference to dict, now is void.
        context = {}

        # All at next of this line don't affect to other tests.
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Activate policy
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]

            pol_obj.send_signal(cursor, uid, [polissa_id], [
                'validar', 'contracte'
            ])

            base_invoice_refs = [
                'factura_gas_0007_polissa_0001_N',
                'factura_gas_0008_polissa_0001_N',
                'factura_gas_0012_polissa_0002_N'
            ]

            # Store id's of original invoices
            list_fact_ids = map(
                lambda x: (imd_obj.get_object_reference(
                    cursor, uid, 'giscegas_facturacio', x)[1]
                ), base_invoice_refs
            )

            vals = {}

            context = {
                'active_ids': None,
                'active_id': None,
                'llista_preu': None,
                'model': 'giscegas.facturacio.factura'
            }

            # This internal function set context with invoice id and price list
            def set_context(fact_id):
                context['active_ids'] = [fact_id]
                context['active_id'] = fact_id
                context['llista_preu'] = factura_obj.read(cursor, uid, fact_id,
                                                          ['llista_preu']
                                                          )['llista_preu'][0]

            # This internal function modify context with corresponding fields
            # of invoice, execute create with new context and return
            # result of that
            def wiz_create():
                wiz_id = wiz_obj.create(cursor, uid, vals, context=context)
                return wiz_id

            for fac in list_fact_ids:
                set_context(fac)
                self.assertRaises(osv.except_osv, wiz_create)