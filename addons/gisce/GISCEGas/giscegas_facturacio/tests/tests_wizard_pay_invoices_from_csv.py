# -*- coding: utf-8 -*-
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from osv.orm import except_orm
import pandas as pd
from datetime import datetime
import time
import base64
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO


class TestGiscegasWizardPayInvoicesFromCsv(testing.OOTestCase):
    def setUp(self):
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        line_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        for fact_id in fact_obj.search(cursor, uid, []):
            fact_obj.write(cursor, uid, fact_id, {'state': 'open'})

        for linia_id in line_obj.search(cursor, uid, []):
            quant = line_obj.read(
                cursor, uid, linia_id, ['quantity']
            )['quantity']
            line_obj.write(cursor, uid, linia_id, {'quantity': quant})

    def tearDown(self):
        self.txn.stop()

    def test_wizard_process_correctly_csv_file(self):
        # This test generate a temporary CSV file with next format:
        # COLUMN_0: invoice_number, COLUMN_1: amount
        # with valid invoices to be payed and tries to pay them.
        # If the process ends correctly then test passed.

        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        wiz_obj = self.openerp.pool.get('giscegas.wizard.pay.invoices.from.csv')
        journal_obj = self.openerp.pool.get('account.journal')

        cursor = self.txn.cursor
        uid = self.txn.user

        gas_journal = journal_obj.search(cursor, uid, [
            ('code', '=', 'GAS')
        ])[0]

        # Search all invoices id's
        fact_ids = fact_obj.search(
            cursor, uid, [
                ('state', '=', 'open'), ('amount_total', '!=', False),
                ('name', '=like', '0%'), ('journal_id', '=', gas_journal)
            ]
        )

        # Search invoice_number and amount_total for all invoices ids
        invoices_data = fact_obj.read(
            cursor, uid, fact_ids, ['number', 'amount_total']
        )
        list_to_be_processed = []

        # Prepare array to be processed by pandas
        for invoice in invoices_data:
            list_to_be_processed.append(
                [invoice['number'], invoice['amount_total']]
            )

        # Generate csv file with padas library
        my_df = pd.DataFrame(list_to_be_processed)
        csv_file = StringIO.StringIO()
        my_df.to_csv(
            csv_file, index=False, header=False, sep=';'
        )

        # Sets wizard values
        ctx = {}
        values = {
            'partner_id': False,
            'journal_id': 1,
            'fact_mode': 'gen',
            'fact_date': datetime.strptime(
                time.strftime('%Y-%m-%d'), '%Y-%m-%d'
            ),
            'file_fact': base64.b64encode(csv_file.getvalue()),
        }

        # Create wizard
        wiz_id = wiz_obj.create(cursor, uid, values, context=ctx)
        wiz = wiz_obj.browse(cursor, uid, wiz_id, context=ctx)
        wiz.importar()

        assert True