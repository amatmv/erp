# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime
import json


class TestsManualInvoiceWizard(testing.OOTestCase):

    def test_manual_invoice(self):
        self.openerp.install_module(
            'giscegas_tarifas_peajes_20180101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscegas.polissa')
        lectura_obj = self.openerp.pool.get('giscegas.lectures.lectura')
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("giscegas.wizard.manual.invoice")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
            vals = {
                'data_alta': '2018-10-01',
                'data_baixa': False,
                'pressio': 3.5,
                'data_ultima_lectura': '2018-10-28',
                'facturacio': 1,
                'contract_type': '01',
                'lot_facturacio': False
            }
            contract_obj.write(cursor, uid, contract_id, vals)
            contract_obj.send_signal(cursor, uid, [contract_id], [
                'validar', 'contracte'
            ])

            contract = contract_obj.browse(cursor, uid, contract_id)

            # Delete all lectures from meter
            for comptador in contract.comptadors:
                for l in comptador.lectures:
                    l.unlink(context={})
                # Delete lloguer because it will fail in manual invoice because
                # it's implemented in comer/dist facturacio modules
                comptador.write({'lloguer': False})

            comptador = contract.comptadors[0]
            # Create lectures 1 day before activation
            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'p1_v_tarifa_31_gas'
            )[1]
            comptador_id = comptador.id

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'origen_gas_0'
            )[1]

            vals = {
                'name': '2018-10-28',
                'periode': periode_id,
                'lectura': 8000,
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
                'pressio_subministrament': 1,
                'factor_k': 1,
                'pcs': 1,
            }
            lectura_obj.create(cursor, uid, vals)

            vals = {
                'name': '2018-11-28',
                'periode': periode_id,
                'lectura': 9000,
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
                'pressio_subministrament': 1,
                'factor_k': 1,
                'pcs': 1,
            }
            lectura_obj.create(cursor, uid, vals)

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'GAS')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2018-11-01',
                'date_end': '2018-11-30',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2018-11-01')
            # Check lines
            self.assertEqual(len(invoice.lectures_ids), 1)
            self.assertEqual(len(invoice.linies_consum), 1)
            self.assertEqual(invoice.linies_consum[0].tipus, 'variable')
            self.assertEqual(invoice.linies_consum[0].invoice_line_id.quantity, 1000)

            # Check lectures
            self.assertEqual(len(invoice.lectures_ids), 1)
            self.assertEqual(invoice.lectures_ids[0].name, '3.1 (P1)')
            self.assertEqual(invoice.lectures_ids[0].lect_actual, 9000)
            self.assertEqual(invoice.lectures_ids[0].lect_anterior, 8000)
            self.assertEqual(invoice.lectures_ids[0].data_actual, '2018-11-28')
            self.assertEqual(invoice.lectures_ids[0].data_anterior, '2018-10-28')
