# coding=utf-8
from datetime import datetime
from destral import testing
from destral.transaction import Transaction


class TestsDeposits(testing.OOTestCase):

    def create_deposit(self, cursor, uid, polissa_name, deposit):
        move_line_obj = self.openerp.pool.get('account.move.line')
        imd_obj = self.openerp.pool.get('ir.model.data')
        period_obj = self.openerp.pool.get('account.period')
        cfg_obj = self.openerp.pool.get('res.config')
        product_obj = self.openerp.pool.get('product.product')
        product_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'extra_line_product'
        )[1]
        journal_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'facturacio_journal_gas'
        )[1]
        acc_id = product_obj.get_account(cursor, uid, product_id, 'income')
        date_move = datetime.now().strftime('%Y-%m-%d')
        period_id = period_obj.find(cursor, uid, date_move)[0]
        ml_id = move_line_obj.create(cursor, uid, {
            'product_id': product_id,
            'account_id': acc_id,
            'period_id': period_id,
            'date': date_move,
            'journal_id': journal_id,
            'name': 'Fianza recibida póliza %s' % polissa_name,
            'credit': deposit,
            'ref': polissa_name
        })
        cfg_obj.set(cursor, uid, 'deposit_product_id', product_id)
        return ml_id

    def test_polissa_deposit_sum_of_move_lines(self):
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            polissa_name = '0002'
            self.create_deposit(cursor, uid, polissa_name, 50)
            polissa_id = polissa_obj.search(cursor, uid, [
                ('name', '=', polissa_name)
            ])[0]
            polissa = polissa_obj.read(cursor, uid, polissa_id, ['deposit'])
            self.assertEqual(polissa['deposit'], -50)

    def test_refund_wizard_default_deposit(self):
        refund_obj = self.openerp.pool.get('giscegas.wizard.refund.deposit')
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            polissa_name = '0002'
            self.create_deposit(cursor, uid, polissa_name, 150)
            polissa_id = polissa_obj.search(cursor, uid, [
                ('name', '=', polissa_name)
            ])[0]
            ctx = {'active_id': polissa_id}
            defaults = refund_obj.default_get(
                cursor, uid, ['deposit'], context=ctx
            )
            self.assertEqual(defaults['deposit'], 150)

    def test_refund_move_lines(self):
        refund_obj = self.openerp.pool.get('giscegas.wizard.refund.deposit')
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        move_line_obj = self.openerp.pool.get('account.move.line')
        journal_obj = self.openerp.pool.get('account.journal')
        product_obj = self.openerp.pool.get('product.product')
        cfg_obj = self.openerp.pool.get('res.config')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            polissa_name = '0002'
            self.create_deposit(cursor, uid, polissa_name, 150)
            polissa_id = polissa_obj.search(cursor, uid, [
                ('name', '=', polissa_name)
            ])[0]
            ctx = {'active_id': polissa_id}
            journal_id = journal_obj.search(cursor, uid, [
                ('code', '=', 'CAJA')
            ])[0]
            wiz_id = refund_obj.create(cursor, uid, {
                'journal_id': journal_id
            }, context=ctx)
            wiz = refund_obj.browse(cursor, uid, wiz_id)
            wiz.refund()
            polissa = polissa_obj.read(cursor, uid, polissa_id, ['deposit'])
            self.assertEqual(polissa['deposit'], 0)
            product_id = int(cfg_obj.get(cursor, uid, 'giscegas_deposit_product_id'))
            acc_id = product_obj.get_account(cursor, uid, product_id, 'income')

            ml_ids = move_line_obj.search(cursor, uid, [
                ('ref', '=', polissa_name),
                ('account_id', '=', acc_id),
                ('debit', '=', 150)
            ])
            self.assertEqual(len(ml_ids), 1)
            ml_ids = move_line_obj.search(cursor, uid, [
                ('ref', '=', polissa_name),
                ('account_id', '=', wiz.partner_id.property_account_payable.id),
                ('credit', '=', 150)
            ])
            self.assertEqual(len(ml_ids), 1)
