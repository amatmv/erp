# -*- coding: utf-8 -*-


from destral import testing
from destral.transaction import Transaction


class TestWizardChangeDatesFact(testing.OOTestCase):

    def setUp(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def tearDown(self):
        self.txn.stop()

    def forcar_canvis(self, force, state):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        wz_canviar_dates = pool.get('giscegas.wizard.canviar.dates')
        fact_obj = pool.get('giscegas.facturacio.factura')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon = pol.modcontractuals_ids[0]
        ctx = {'active_id': modcon.id}

        wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)
        wiz.write({
            'data_final': '2018-08-01',
            'data_inici': '2018-01-01',
            'force': force
        })
        polissa_obj.write(cursor, uid, polissa_id, {
            'renovacio_auto': False,
            'data_baixa': '2018-08-01'
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id], ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)

        # Comprovació de resultats

        self.assertEqual(
            wiz.state,
            state
        )
        # comprovem que les factures apareixen al quadre de informació
        # Si no es força, informa de quines factures es veurien afectades.
        # Si força, mostra les factures afectades per tant sempre haurien
        # d'aparèixer al quadre d'informació.

        ids_factures = wz_canviar_dates.get_invoices(
            cursor, polissa_id, wiz.data_inici, wiz.data_final
        )

        numeros = fact_obj.read(cursor, uid, ids_factures, ['number'])
        for elem in numeros:
            self.assertIn(
                elem['number'],
                wiz.info
            )

    def test_sense_forcar_canvi_dona_error(self):
        """Aquest test comprova que l'assistent acaba en estat d'error
        al haver posat unes dates que afecten a unes determinades factures i
        NO s'ha forçat el canvi de dates."""
        self.forcar_canvis(False, 'error')

    def test_forcar_canvi_no_dona_error(self):
        """Aquest test comprova que l'assistent acaba correctament al forçar
        el canvi de dates."""
        self.forcar_canvis(True, 'end')

    def comprovar_factures_afectades(self, data_inici, data_fi):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        wz_canviar_dates = pool.get('giscegas.wizard.canviar.dates')
        fact_obj = pool.get('giscegas.facturacio.factura')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon = pol.modcontractuals_ids[0]
        ctx = {'active_id': modcon.id}

        # Borrem el lot de facturacio en el qual esta la polissa perque no hi
        # hagui problemes a l'hora de canviar la data d'alta (si intentem posar
        # una data d'alta posterior a la data del lot en el que esta donara
        # error, aixi ja no ho fa)
        polissa_obj.write(cursor, uid, polissa_id, {
            'lot_facturacio': None,
        })

        wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)

        # Comprovem les factures que estan dins del rang de dates original

        ids_factures = wz_canviar_dates.get_invoices(
            cursor, polissa_id, wiz.data_inici, wiz.data_final
        )
        wiz.write({
            'data_final': data_fi,
            'data_inici': data_inici,
            'force': True
        })
        polissa_obj.write(cursor, uid, polissa_id, {
            'renovacio_auto': False,
            'data_baixa': data_inici
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id], ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)

        # Comprovem les factures que estan dins del nou rang de dates

        ids_factures_noves = wz_canviar_dates.get_invoices(
            cursor, polissa_id, wiz.data_inici, wiz.data_final
        )

        union = list(set(ids_factures_noves) | set(ids_factures))

        numeros = fact_obj.read(cursor, uid, union, ['number'])
        for elem in numeros:
            self.assertIn(
                elem['number'],
                wiz.info
            )

    def test_comprovar_factures_fora_rang_data_final(self):
        """Aquest test comprova que l'assistent informa correctament de quines
        factures es veuen afectades per el canvi de dates. Comprova que les
        factures que abans estaven dins el rang de dates ara han quedat fora
        amb les noves dates. Concretament, comprova les factures posteriors a
        la data final que abans si que estaven al rang."""
        self.comprovar_factures_afectades('2018-01-01', '2018-04-30')

    def test_comprovar_factures_fora_rang_data_inici(self):
        """Aquest test comprova que l'assistent informa correctament de quines
        factures es veuen afectades per el canvi de dates. Comprova que les
        factures que abans estaven dins el rang de dates ara han quedat fora
        amb les noves dates. Concretament, comprova les factures anteriors a
        la data inicial que abans si que estaven al rang."""
        self.comprovar_factures_afectades('2018-03-01', '2018-06-30')
