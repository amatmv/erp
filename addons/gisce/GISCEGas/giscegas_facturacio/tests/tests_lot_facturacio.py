# coding=utf-8
from destral import testing
from destral.transaction import Transaction


class TestsLotFacturacio(testing.OOTestCase):

    def setUp(self):
        self.cnt_lot_obj = self.openerp.pool.get('giscegas.facturacio.contracte_lot')
        self.lot_obj = self.openerp.pool.get('giscegas.facturacio.lot')
        self.imd_obj = self.openerp.pool.get('ir.model.data')
        self.comptador_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        self.pol_obj = self.openerp.pool.get('giscegas.polissa')
        self.measure_obj = self.openerp.pool.get('giscegas.lectures.lectura')
        self.imd_obj = self.openerp.pool.get('ir.model.data')

    def deprecated_test_validation_V006_lot(self):

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            _, periode_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'p1_v_tarifa_33_gas'
            )

            _, origen_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'origen_gas_1'
            )

            _, cnt_lot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'cont_lot_gas_0002'
            )

            # We remove other lot_contracts associated with out invoicing lot
            lot_id = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['lot_id']
            )['lot_id'][0]
            cnt_lot_to_remove = self.lot_obj.read(
                cursor, uid, lot_id, ['contracte_lot_ids']
            )['contracte_lot_ids']

            cnt_lot_to_remove.remove(cnt_lot_id)
            self.cnt_lot_obj.unlink(cursor, uid, cnt_lot_to_remove)

            # Let's validate the contract
            _, demo_contract_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0002'
            )
            self.pol_obj.send_signal(cursor, uid, [demo_contract_id], [
                'validar', 'contracte'
            ])

            compt_id = self.comptador_obj.search(
                cursor, uid, [('polissa', '=', demo_contract_id)]
            )[0]

            # We will create two measures with a lower value on the second one,
            # so the validation V006 might pop

            lot_vals = self.lot_obj.read(
                cursor, uid, lot_id, ['data_inici', 'data_final']
            )

            # To prevent errors, we unlink all measures existing in the meter
            self.measure_obj.unlink(
                cursor, uid, self.measure_obj.search(cursor, uid, [
                    ('comptador', '=', compt_id)
                ])
            )

            fst_measure = self.measure_obj.create(cursor, uid, {
                'name': lot_vals['data_inici'],
                'periode': periode_id,
                'lectura': 1,
                'ajust': 0,
                'tipus': 'A',
                'comptador': compt_id,
                'observacions': '',
                'origen_id': origen_id,
                'pressio_subministrament': 1,
                'factor_k': 1,
                'pcs': 1,
            })

            last_measure = self.measure_obj.create(cursor, uid, {
                'name': lot_vals['data_final'],
                'periode': periode_id,
                'lectura': 0,
                'ajust': 0,
                'tipus': 'A',
                'comptador': compt_id,
                'observacions': '',
                'origen_id': origen_id,
                'pressio_subministrament': 1,
                'factor_k': 1,
                'pcs': 1,
            })

            # Next step, we want to validate the lot and the V006 should pop
            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertIn('V006', lot_status)

            # But if we add an adjust to the second measure, the V006 will be
            # fixed
            self.measure_obj.write(cursor, uid, last_measure, {'ajust': 3})

            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertNotIn('V006', lot_status)

    def test_validation_V020_lot(self):

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            _, periode_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'p1_v_tarifa_33_gas'
            )

            _, origen_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'origen_gas_1'
            )

            _, cnt_lot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'cont_lot_gas_0002'
            )

            # We remove other lot_contracts associated with out invoicing lot
            lot_id = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['lot_id']
            )['lot_id'][0]
            cnt_lot_to_remove = self.lot_obj.read(
                cursor, uid, lot_id, ['contracte_lot_ids']
            )['contracte_lot_ids']

            cnt_lot_to_remove.remove(cnt_lot_id)
            self.cnt_lot_obj.unlink(cursor, uid, cnt_lot_to_remove)

            # Let's validate the contract
            _, demo_contract_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0002'
            )

            self.pol_obj.send_signal(
                cursor, uid, [demo_contract_id],
                ['validar', 'contracte']
            )

            self.pol_obj.write(
                cursor, uid, demo_contract_id,
                {'data_baixa': '2019-01-31', 'renovacio_auto': False}
            )

            compt_ids = self.comptador_obj.search(
                cursor, uid, [('polissa', '=', demo_contract_id)]
            )

            self.comptador_obj.write(
                cursor, uid, compt_ids,
                {'data_baixa': '2019-01-31', 'active': False}
            )

            self.pol_obj.send_signal(
                cursor, uid, [demo_contract_id],
                ['baixa']
            )

            # Next step, we want to validate the lot and the V020 should pop
            pol_data = {'data_ultima_lectura': '2019-02-01'}
            self.pol_obj.write(cursor, uid, demo_contract_id, pol_data)

            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertIn('V000', lot_status)

            # Next step, we want to validate the lot and the V020 should pop
            pol_data = {'data_ultima_lectura': '2019-01-31'}
            self.pol_obj.write(cursor, uid, demo_contract_id, pol_data)

            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertIn('V000', lot_status)
            self.assertIn(
                u"[V000] Contracte de baixa el 2019-01-31 ja facturat "
                u"fins 2019-01-31",
                lot_status
            )

            # But if we add an adjust to the second measure, the V006 will be
            # fixed
            pol_data = {'data_ultima_lectura': '2019-01-01'}
            self.pol_obj.write(cursor, uid, demo_contract_id, pol_data)

            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertNotIn('V000', lot_status)


class TestsMovePolissesLot(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_next_lot_weekly_lots(self):
        lot_obj = self.openerp.pool.get('giscegas.facturacio.lot')
        cfg_obj = self.openerp.pool.get('res.config')

        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/01 - 8',
            'data_inici': '2017-01-01',
            'data_final': '2017-01-07',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/01 - 15',
            'data_inici': '2017-01-08',
            'data_final': '2017-01-14',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/01 - 22',
            'data_inici': '2017-01-15',
            'data_final': '2017-01-21',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/02 - 1',
            'data_inici': '2017-01-22',
            'data_final': '2017-01-31',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/02 - 8',
            'data_inici': '2017-02-01',
            'data_final': '2017-02-07',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/02 - 15',
            'data_inici': '2017-02-08',
            'data_final': '2017-02-14',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/02 - 22',
            'data_inici': '2017-02-15',
            'data_final': '2017-02-21',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/03 - 1',
            'data_inici': '2017-02-22',
            'data_final': '2017-02-28',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/03 - 8',
            'data_inici': '2017-03-01',
            'data_final': '2017-03-07',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/03 - 15',
            'data_inici': '2017-03-08',
            'data_final': '2017-03-14',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/03 - 22',
            'data_inici': '2017-03-15',
            'data_final': '2017-03-21',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/04 - 1',
            'data_inici': '2017-03-22',
            'data_final': '2017-03-31',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/04 - 8',
            'data_inici': '2017-04-01',
            'data_final': '2017-04-07',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/04 - 15',
            'data_inici': '2017-04-08',
            'data_final': '2017-04-14',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/04 - 22',
            'data_inici': '2017-04-15',
            'data_final': '2017-04-21',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/05 - 1',
            'data_inici': '2017-04-22',
            'data_final': '2017-04-30',
        })

        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-07', 1)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/02 - 8',
            'data_inici': '2017-02-01',
            'data_final': '2017-02-07',
        }, lot)
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-14', 1)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/02 - 15',
            'data_inici': '2017-02-08',
            'data_final': '2017-02-14',
        }, lot)
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-21', 1)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/02 - 22',
            'data_inici': '2017-02-15',
            'data_final': '2017-02-21',
        }, lot)
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-31', 1)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/03 - 1',
            'data_inici': '2017-02-22',
            'data_final': '2017-02-28',
        }, lot)

        # Test with bimestral
        cfg_obj.set(
            self.cursor, self.uid, 'giscegas_fact_bimestrals_sempre_lot_parell', '0'
        )
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-07', 2)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/03 - 8',
            'data_inici': '2017-03-01',
            'data_final': '2017-03-07',
        }, lot)
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-14', 2)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/03 - 15',
            'data_inici': '2017-03-08',
            'data_final': '2017-03-14',
        }, lot)
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-21', 2)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/03 - 22',
            'data_inici': '2017-03-15',
            'data_final': '2017-03-21',
        }, lot)
        cfg_obj.set(
            self.cursor, self.uid, 'giscegas_fact_bimestrals_sempre_lot_parell', '1'
        )
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-31', 2)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/03 - 1',
            'data_inici': '2017-02-22',
            'data_final': '2017-02-28',
        }, lot)
