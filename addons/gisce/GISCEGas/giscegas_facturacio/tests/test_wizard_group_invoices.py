# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestWizardAfegirFacturesRemesa(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_group_different_ibans(self):
        pool = self.openerp.pool
        factura_obj = pool.get('giscegas.facturacio.factura')
        period_obj = pool.get('account.period')
        wz_agrupar = pool.get('giscegas.wizard.group.invoices.payment')
        bank_obj = pool.get('res.partner.bank')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user

        bank_ids = bank_obj.search(cursor, uid, [], limit=2)
        bank_obj.write(cursor, uid, bank_ids[0], {'iban': "XXXXXXXXXXXXX"})
        bank_obj.write(cursor, uid, bank_ids[1], {'iban': "YYYYYYYYYYYYY"})

        factura_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0004_polissa_0001_N'
        )[1]
        factura_id2 = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0005_polissa_0001_N'
        )[1]
        factura = factura_obj.browse(cursor, uid, factura_id)
        factura2 = factura_obj.browse(cursor, uid, factura_id2)

        factura.write({'partner_bank': bank_ids[0]})
        factura2.write({'partner_bank': bank_ids[1]})

        factura = factura_obj.browse(cursor, uid, factura_id)
        factura2 = factura_obj.browse(cursor, uid, factura_id2)

        self.assertNotEquals(factura.partner_bank.id, factura2.partner_bank.id)

        ids = [factura.id, factura2.id]
        period_id = period_obj.search(cursor, uid, [], limit=1)[0]
        factura_obj.write(cursor, uid, ids, {'period_id': period_id})
        factura_obj.invoice_open(cursor, uid, ids)
        ctx = {'active_ids': ids, 'model': 'giscegas.facturacio.factura'}
        wz_agrupar_id = wz_agrupar.create(cursor, uid, {}, ctx)
        wz_agrupar = wz_agrupar.browse(cursor, uid, wz_agrupar_id, ctx)
        msg = ""
        try:
            wz_agrupar.group_invoices(context=ctx)
        except Exception as e:
            msg = e.message
        finally:
            self.assertTrue("YYYYYYYYYYYYY: [u'FG/00000005']" in msg)
            self.assertTrue("XXXXXXXXXXXXX: [u'FG/00000004']" in msg)
