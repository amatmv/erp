from datetime import datetime

def create_fiscal_year(pool, cursor, uid, fiscal_year, context=None):
    fyear_obj = pool.get('account.fiscalyear')
    fy_id = fyear_obj.create(cursor, uid, {
        'name': fiscal_year,
        'code': fiscal_year,
        'date_start': '%s-01-01' % fiscal_year,
        'date_stop': '%s-12-31' % fiscal_year
    })
    fyear_obj.create_period(cursor, uid, [fy_id])


def prepare_fiscal_year(pool, cursor, uid):
    year = 2018
    current_year = datetime.today().year
    for fyear in range(year, current_year + 1):
        create_fiscal_year(pool, cursor, uid, fyear)
