# -*- coding: utf-8 -*-
from expects import expect
from expects import raise_error
from expects import equal
from expects import contain
from expects import contain_exactly

from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

import json


class TestsAccountInvoiceBase(testing.OOTestCase):

    def test_get_model_ids_returns_correct_ids(self):
        invoice_obj = self.openerp.pool.get('account.invoice')
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            imd_ids = imd_obj.search(
                cursor, uid, [('name', 'like', 'invoice_0%')]
            )
            inv_ids = [
                res['res_id'] for res in
                imd_obj.read(cursor, uid, imd_ids, ['res_id'])
            ]
            model_dict = invoice_obj.get_factura_ids_from_invoice_id(cursor, uid, inv_ids)
            self.assertIn('giscegas.facturacio.factura', model_dict.keys())
            self.assertIn('account.invoice', model_dict.keys())
            self.assertEquals(
                factura_obj, model_dict['giscegas.facturacio.factura'][1]
            )
            self.assertEquals(
                invoice_obj, model_dict['account.invoice'][1]
            )
            fact_ids = model_dict['giscegas.facturacio.factura'][0]
            fact_ids_search = factura_obj.search(
                cursor, uid, [('invoice_id', 'in', inv_ids)]
            )
            self.assertFalse(list(set(fact_ids) - set(fact_ids_search)))


class TestsSupportFunctions(testing.OOTestCase):
    def setUp(self):
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        line_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')

        self.openerp.install_module(
            'giscegas_tarifas_peajes_{}0101'.format(datetime.now().year)
        )
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        for fact_id in fact_obj.search(cursor, uid, []):
            fact_obj.write(cursor, uid, fact_id, {'state': 'open'})

        for linia_id in line_obj.search(cursor, uid, []):
            quant = line_obj.read(
                cursor, uid, linia_id, ['quantity']
            )['quantity']
            line_obj.write(cursor, uid, linia_id, {'quantity': quant})

    def tearDown(self):
        self.txn.stop()

    def test_extra_lines_ultima_factura(self):
        extra_obj = self.openerp.pool.get('giscegas.facturacio.extra')
        imd_obj = self.openerp.pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        extra1_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'facturacio_gas_extraline_01'
        )[1]
        extra_obj.write(cursor, uid, extra1_id, {'date_from': '2018-05-01', 'date_to': '2018-12-30'})
        extra2_id = extra_obj.copy(cursor, uid, extra1_id)
        extra_obj.write(cursor, uid, extra2_id, {'date_from': '2019-02-01', 'date_to': '2019-11-01'})
        polissa_id = extra_obj.read(cursor, uid, extra1_id, ['polissa_id'])['polissa_id'][0]
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        polissa.send_signal(['validar', 'contracte'])
        polissa.write({'data_baixa': '2018-05-25'})
        for compt in polissa.comptadors:
            compt.write({
                'data_baixa': '2018-05-25',
                'active': False
            })
        polissa.send_signal(['baixa'])

        # Tenim 2 extralines no aplicades, la segona amb data inici posterior
        # al final de la factura. Com que es la ultima igualment s'aplicarà.
        res = extra_obj.get_extra_lines_from_contract_and_date(
            cursor, uid, polissa_id, '2018-05-25'
        )
        self.assertEqual(res, [extra1_id, extra2_id])

    def test_get_term_extraline(self):
        extra_obj = self.openerp.pool.get('giscegas.facturacio.extra')
        imd_obj = self.openerp.pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        extra_xml_id = 'facturacio_gas_extraline_01'
        extra_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', extra_xml_id
        )[1]
        self.assertEqual(
            extra_obj.get_next_term(
                cursor=cursor, uid=uid, id_extraline=extra_id),
            1, 'Get Next Term should return 1 if no terms have been factured'
        )

        # After check without Factures,
        #   -   Activate ExtraLine's Polissa

        polissa_id = extra_obj.read(
            cursor, uid, extra_id, ['polissa_id'])['polissa_id'][0]
        self.assertTrue(polissa_id, 'Extra line should have polissa_id')

        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

        #   -   Add "active" ModificacioContractual with older "data_final"

        modcontr_id = polissa_obj.read(
            cursor, uid, polissa_id,
            ['modcontractuals_ids'])['modcontractuals_ids']
        modcontract_obj = self.openerp.pool.get(
            'giscegas.polissa.modcontractual')
        modcontract_obj.write(
            cursor, uid, modcontr_id, {'data_final': '2018-03-31'})

        #   -   Set "GAS" journal to ExtraLine

        journal_obj = self.openerp.pool.get('account.journal')
        journal_id = journal_obj.search(
            cursor, uid, [('code', '=', 'GAS')]
        )[0]

        extra_obj.write(cursor, uid, [extra_id], {
            'journal_ids': [(6, 0, [journal_id])]
        })

        #   -   Remove "lloguer" from Comptador, so it won't fail in getting it

        comptador_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        comptador_xml_id = 'comptador_gas_0001'
        comptador_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_lectures', comptador_xml_id
        )[1]
        comptador_obj.write(cursor, uid, [comptador_id], {'lloguer': False})

        #   -   Initialize manual invoice wizard

        wiz_fact_manual_obj = self.openerp.pool.get('giscegas.wizard.manual.invoice')
        wiz_fact_manual_id = wiz_fact_manual_obj.create(cursor, uid, vals={
            'polissa_id': polissa_id,
            'date_start': datetime(
                year=2018, month=2, day=1).strftime('%Y-%m-%d'),
            'date_end': datetime(
                year=2018, month=2, day=25).strftime('%Y-%m-%d'),
            'date_invoice': datetime(
                year=2018, month=2, day=25).strftime('%Y-%m-%d'),
            'journal_id': journal_id
        })

        #   -   Get Facturacio's PriceList

        pricelist_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio',
            'pricelist_tarifas_gas'
        )[1]
        context = {'llista_preu': pricelist_id}

        #   -   Create regular invoice within ExtraLine's dates

        wiz_fact_manual_obj.action_manual_invoice(
            cursor, uid, [wiz_fact_manual_id], context)

        # Check next term without date should return a count of all factures

        self.assertEqual(
            extra_obj.get_next_term(
                cursor=cursor, uid=uid, id_extraline=extra_id),
            2, 'Get Next Term should return 2 with new factura with extraline'
        )

        # Getting next term with date should return a count of all factures
        #   before the date

        self.assertEqual(
            extra_obj.get_next_term(
                cursor=cursor, uid=uid, id_extraline=extra_id,
                date=datetime(year=2017, month=12, day=31)
            ),
            1, 'Get Next Term should return 1 with factura and date before it'
        )

        # Add a "out_refund" Factura

        wiz_fact_manual_id = wiz_fact_manual_obj.create(cursor, uid, vals={
            'polissa_id': polissa_id,
            'date_start': datetime(
                year=2018, month=2, day=1).strftime('%Y-%m-%d'),
            'date_end': datetime(
                year=2018, month=2, day=25).strftime('%Y-%m-%d'),
            'date_invoice': datetime(
                year=2018, month=2, day=25).strftime('%Y-%m-%d'),
            'journal_id': journal_id,
        })


    def test_get_max_consume_by_contract(self):
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        line_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]

        max_cons_ori = fact_obj.get_max_consume_by_contract(
            cursor, uid, polissa_id
        )
        max_cons_ori = max_cons_ori or 0

        line_ids = line_obj.search(
            cursor, uid, [
                ('tipus', '=', 'variable'),
                ('factura_id.polissa_id', '=', polissa_id)
            ]
        )

        line_obj.write(
            cursor, uid, line_ids[0], {'quantity': max_cons_ori+10}
        )
        for line_id in line_ids[1:]:
            line_obj.write(
                cursor, uid, line_id, {'quantity': 0}
            )

        max_cons_new = fact_obj.get_max_consume_by_contract(
            cursor, uid, polissa_id
        )

        expect(max_cons_new).to(equal(max_cons_ori + 10))


class TestsInvoiceValidation(testing.OOTestCase):
    def setUp(self):
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        line_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning.template'
        )
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        for fact_id in fact_obj.search(cursor, uid, []):
            fact_obj.write(cursor, uid, fact_id, {'state': 'open'})

        for linia_id in line_obj.search(cursor, uid, []):
            quant = line_obj.read(
                cursor, uid, linia_id, ['quantity']
            )['quantity']
            line_obj.write(cursor, uid, linia_id, {'quantity': quant})

        # We make sure that all warnings are active
        warn_ids = warn_obj.search(
            cursor, uid, [], context={'active_test': False}
        )
        warn_obj.write(cursor, uid, warn_ids, {'active': True})

    def tearDown(self):
        self.txn.stop()

    def model(self, model_name):
        return self.openerp.pool.get(model_name)

    def get_fixture(self, model, reference):
        imd_obj = self.model('ir.model.data')
        return imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user,
            model,
            reference
        )[1]

    def validation_warnings(self, factura_id):
        vali_obj = self.model('giscegas.facturacio.validation.validator')
        warn_obj = self.model('giscegas.facturacio.validation.warning')
        warning_ids = vali_obj.validate_invoice(
            self.txn.cursor, self.txn.user,
            factura_id
        )
        warning_vals = warn_obj.read(
            self.txn.cursor, self.txn.user,
            warning_ids,
            ['name']
        )
        return [warn['name'] for warn in warning_vals]

    def test_check_consum_by_percentage_accepts_when_correct(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        res_config = self.openerp.pool.get('res.config')

        cursor = self.txn.cursor
        uid = self.txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0007_polissa_0001_N'
        )[1]
        fact = fact_obj.browse(cursor, uid, fact_id)

        parameters = {'n_months': 14, 'overuse_percentage': 50}
        assert not vali_obj.check_consume_by_percentage(
            cursor, uid, fact, parameters
        )

    def test_check_invoiced_consume(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0008_polissa_0001_N'
        )[1]
        fact = fact_obj.browse(cursor, uid, fact_id)
        for linia in fact.linia_ids:
            if linia.tipus == 'variable':
                linia.write({'quantity': linia.quantity - 1})

        result = vali_obj.check_invoiced_consume(
            cursor, uid, fact, {}
        )
        self.assertIsNotNone(result)

    def test_check_consum_by_percentage_rejects_when_incorrect(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]

        max_cons = fact_obj.get_max_consume_by_contract(
            cursor, uid, polissa_id
        )

        fact_id = fact_obj.search(
            cursor, uid, [('polissa_id', '=', polissa_id)],
            order='data_final desc'
        )[0]

        linia_id = linia_obj.create(cursor, uid, {
            'name': 'Linia 1',
            'tipus': 'variable',
            'price_unit': 1,
            'factura_id': fact_id,
            'product_id': False,
            'account_id': 1,
            'quantity': (max_cons + 1) * 10,
        })
        fact = fact_obj.browse(cursor, uid, fact_id)

        parameters = {'n_months': 14, 'overuse_percentage': 50}
        assert vali_obj.check_consume_by_percentage(
            cursor, uid, fact, parameters
        )

    def test_validate_invoice_doesnt_create_warnings_when_correct(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        vali_tpl_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning.template'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
        )[1]
        # deactive F021 test (duplicated invoice)
        f021_ids = vali_tpl_obj.search(cursor, uid, [('code', '=', 'F021')])
        vali_tpl_obj.write(cursor, uid, f021_ids, {'active': False})

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)
        expect(len(warning_ids)).to(equal(0))

        text = vali_obj.get_invoice_warnings_text(cursor, uid, fact_id)
        expect(text).to(equal(''))

    def test_validate_invoice_creates_warnings_when_incorrect(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1]

        fact_vals = fact_obj.read(cursor, uid, fact_id, ['linia_ids'])

        # We unlink the previous lines and create the new ones with import 0
        # so that the checks for when import is 0 (energy, power and total) pop
        linia_obj.unlink(cursor, uid, fact_vals['linia_ids'])

        linia_obj.create(cursor, uid, {
            'name': 'Linia 1',
            'tipus': 'variable',
            'price_unit': 0,
            'factura_id': fact_id,
            'product_id': False,
            'account_id': 1,
            'quantity': 200000,
        })

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

        text = vali_obj.get_invoice_warnings_text(cursor, uid, fact_id)
        expect(text).not_to(equal(''))
        expect(text).to(contain('[F001]'))
        expect(text).to(contain('[F003]'))
        expect(text).to(contain('[F004]'))
        expect(text).to(contain('[F006]'))
        expect(text).to(contain('[F008]'))
        # We could add more but it's better to create separate tests

    def test_validation_F001(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1]

        fact_vals = fact_obj.read(cursor, uid, fact_id, ['linia_ids'])

        # We unlink the previous lines and create the new ones with import 0
        # so that the checks for when import is 0 (energy, power and total) pop
        linia_obj.unlink(cursor, uid, fact_vals['linia_ids'])

        linia_obj.create(cursor, uid, {
            'name': 'Linia 1',
            'tipus': 'variable',
            'price_unit': 1,
            'factura_id': fact_id,
            'product_id': False,
            'account_id': 1,
            'quantity': 200000,
        })

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )

        warning_names = [warn['name'] for warn in warning_vals]
        expect(warning_names).to(contain('F001'))

    def test_validation_F003(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        temp_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning.template'
        )
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1]

        fact_vals = fact_obj.read(cursor, uid, fact_id, ['linia_ids'])

        # We unlink the previous lines and create the new ones with import 0
        # so that the checks for when import is 0 (energy, power and total) pop
        linia_obj.unlink(cursor, uid, fact_vals['linia_ids'])

        linia_obj.create(cursor, uid, {
            'name': 'Linia 1',
            'tipus': 'variable',
            'price_unit': 1,
            'factura_id': fact_id,
            'product_id': False,
            'account_id': 1,
            'quantity': 200000,
        })

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )

        warning_names = [warn['name'] for warn in warning_vals]
        expect(warning_names).to(contain('F003'))

    def test_validation_F004(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        temp_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning.template'
        )
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1]

        fact_vals = fact_obj.read(cursor, uid, fact_id, ['linia_ids'])

        # We unlink the previous lines and create the new ones with import 0
        # so that the checks for when import is 0 (energy, power and total) pop
        linia_obj.unlink(cursor, uid, fact_vals['linia_ids'])

        linia_obj.create(cursor, uid, {
            'name': 'Linia 1',
            'tipus': 'variable',
            'price_unit': 0,
            'factura_id': fact_id,
            'product_id': False,
            'account_id': 1,
            'quantity': 200000,
        })

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )

        warning_names = [warn['name'] for warn in warning_vals]
        expect(warning_names).to(contain('F004'))

    def test_validation_F006(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        temp_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning.template'
        )
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1]

        fact_vals = fact_obj.read(cursor, uid, fact_id, ['linia_ids'])

        # We unlink the previous lines and create the new ones with import 0
        # so that the checks for when import is 0 (energy, power and total) pop
        linia_obj.unlink(cursor, uid, fact_vals['linia_ids'])

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )

        warning_names = [warn['name'] for warn in warning_vals]
        expect(warning_names).to(contain('F006'))

    def test_validation_F007(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        temp_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning.template'
        )
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1]

        fact_vals = fact_obj.read(cursor, uid, fact_id, ['linia_ids'])

        # We unlink the previous lines and create the new ones with import 0
        # so that the checks for when import is 0 (energy, power and total) pop
        linia_obj.unlink(cursor, uid, fact_vals['linia_ids'])

        linia_obj.create(cursor, uid, {
            'name': 'Linia 1',
            'tipus': 'variable',
            'price_unit': 1,
            'factura_id': fact_id,
            'product_id': False,
            'account_id': 1,
            'quantity': 200000,
        })

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )

        warning_names = [warn['name'] for warn in warning_vals]
        expect(warning_names).to(contain('F007'))

    def test_validation_F008(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        temp_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning.template'
        )
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1]

        fact_vals = fact_obj.read(cursor, uid, fact_id, ['linia_ids'])

        # We unlink the previous lines and create the new ones with import 0
        # so that the checks for when import is 0 (energy, power and total) pop
        linia_obj.unlink(cursor, uid, fact_vals['linia_ids'])

        linia_obj.create(cursor, uid, {
            'name': 'Linia 1',
            'tipus': 'variable',
            'price_unit': 1,
            'factura_id': fact_id,
            'product_id': False,
            'account_id': 1,
            'quantity': 200000,
        })

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )

        warning_names = [warn['name'] for warn in warning_vals]
        expect(warning_names).to(contain('F008'))

    def test_exceeding_days_validation_on_mensual(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validador'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]

        fact_id = fact_obj.search(
            cursor, uid, [('polissa_id', '=', polissa_id)],
            order='data_final desc'
        )[0]

        # This dates should fail
        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': '2018-01-01',
                'data_final': '2018-02-06'
            }
        )
        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)
        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )
        warning_names = [warn['name'] for warn in warning_vals]

        expect(warning_names).to(contain('F009'))

        # This shouldn't because the margin and the difference are both 5
        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': '2018-01-01',
                'data_final': '2018-02-05'
            }
        )
        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)
        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )
        warning_names = [warn['name'] for warn in warning_vals]

        expect(warning_names).not_to(contain('F009'))

    def test_exceeding_days_validation_on_bimensual(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validador'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        polissa_obj.write(cursor, uid, polissa_id, {'facturacio': 2})

        fact_id = fact_obj.search(
            cursor, uid, [('polissa_id', '=', polissa_id)],
            order='data_final desc'
        )[0]

        # This dates should fail
        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': '2018-01-01',
                'data_final': '2018-03-11'
            }
        )
        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)
        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )
        warning_names = [warn['name'] for warn in warning_vals]

        expect(warning_names).to(contain('F009'))

        # This shouldn't because the margin and the difference are both 10
        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': '2018-01-01',
                'data_final': '2018-03-10'
            }
        )
        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)
        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )
        warning_names = [warn['name'] for warn in warning_vals]

        expect(warning_names).not_to(contain('F009'))

    def test_missing_days_validation_on_mensual(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validador'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]

        fact_id = fact_obj.search(
            cursor, uid, [('polissa_id', '=', polissa_id)],
            order='data_final desc'
        )[0]

        # This dates should fail
        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': '2018-01-01',
                'data_final': '2018-01-25'
            }
        )
        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)
        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )
        warning_names = [warn['name'] for warn in warning_vals]

        expect(warning_names).to(contain('F010'))

        # This shouldn't because the margin and the difference are both 5
        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': '2018-01-01',
                'data_final': '2018-01-26'
            }
        )
        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)
        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )
        warning_names = [warn['name'] for warn in warning_vals]

        expect(warning_names).not_to(contain('F010'))

    def test_missing_days_validation_on_bimensual(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validador'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        polissa_obj.write(cursor, uid, polissa_id, {'facturacio': 2})

        fact_id = fact_obj.search(
            cursor, uid, [('polissa_id', '=', polissa_id)],
            order='data_final desc'
        )[0]

        # This dates should fail
        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': '2018-01-01',
                'data_final': '2018-02-17'
            }
        )
        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)
        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )
        warning_names = [warn['name'] for warn in warning_vals]

        expect(warning_names).to(contain('F010'))

        # This shouldn't because the margin and the difference are both 10
        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': '2018-01-01',
                'data_final': '2018-02-19'
            }
        )
        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)
        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )
        warning_names = [warn['name'] for warn in warning_vals]

        expect(warning_names).not_to(contain('F010'))

    def test_check_ending_outside_lot__afterLot(self):
        fact_obj = self.model('giscegas.facturacio.factura')
        contract_obj = self.model('giscegas.polissa')
        lot_obj = self.model('giscegas.facturacio.lot')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = self.get_fixture('giscegas_facturacio', 'factura_gas_0001_polissa_0001_N')
        fact_vals = fact_obj.read(
            cursor, uid, fact_id,
            ['data_final', 'polissa_id']
        )
        polissa_id = fact_vals['polissa_id'][0]
        polissa_vals = contract_obj.read(
            cursor, uid, polissa_id,
            ['lot_facturacio']
        )
        lot_id = polissa_vals['lot_facturacio'][0]
        lot_vals = lot_obj.read(
            cursor, uid, lot_id,
            ['data_inici', 'data_final']
        )
        lot_start_date = lot_vals['data_inici']
        lot_end_date = lot_vals['data_final']
        fact_end_date = datetime.strptime(lot_end_date, "%Y-%m-%d").date()
        fact_end_date += timedelta(1)
        fact_start_date = fact_end_date - timedelta(30)

        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': fact_start_date.strftime('%Y-%m-%d'),
                'data_final': fact_end_date.strftime('%Y-%m-%d')
            }
        )

        warnings = self.validation_warnings(fact_id)

        expect(warnings).to(contain('F013'))

    def test_check_ending_outside_lot__beforeLot(self):
        fact_obj = self.model('giscegas.facturacio.factura')
        contract_obj = self.model('giscegas.polissa')
        lot_obj = self.model('giscegas.facturacio.lot')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = self.get_fixture('giscegas_facturacio', 'factura_gas_0001_polissa_0001_N')
        fact_vals = fact_obj.read(
            cursor, uid, fact_id,
            ['data_final', 'polissa_id']
        )
        polissa_id = fact_vals['polissa_id'][0]
        polissa_vals = contract_obj.read(
            cursor, uid, polissa_id,
            ['lot_facturacio']
        )
        lot_id = polissa_vals['lot_facturacio'][0]
        lot_vals = lot_obj.read(
            cursor, uid, lot_id,
            ['data_inici', 'data_final']
        )
        lot_start_date = lot_vals['data_inici']
        lot_end_date = lot_vals['data_final']
        fact_end_date = datetime.strptime(lot_start_date, "%Y-%m-%d").date()
        fact_end_date -= timedelta(1)
        fact_start_date = fact_end_date - timedelta(30)

        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': fact_start_date.strftime('%Y-%m-%d'),
                'data_final': fact_end_date.strftime('%Y-%m-%d')
            }
        )

        warnings = self.validation_warnings(fact_id)

        expect(warnings).to(contain('F013'))

    def test_check_ending_outside_lot__inside(self):
        fact_obj = self.model('giscegas.facturacio.factura')
        contract_obj = self.model('giscegas.polissa')
        lot_obj = self.model('giscegas.facturacio.lot')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = self.get_fixture('giscegas_facturacio', 'factura_gas_0001_polissa_0001_N')
        fact_vals = fact_obj.read(
            cursor, uid, fact_id,
            ['data_final', 'polissa_id']
        )
        polissa_id = fact_vals['polissa_id'][0]
        polissa_vals = contract_obj.read(
            cursor, uid, polissa_id,
            ['lot_facturacio']
        )
        lot_id = polissa_vals['lot_facturacio'][0]
        lot_vals = lot_obj.read(
            cursor, uid, lot_id,
            ['data_inici', 'data_final']
        )
        lot_start_date = lot_vals['data_inici']
        lot_end_date = lot_vals['data_final']
        fact_end_date = datetime.strptime(lot_end_date, "%Y-%m-%d").date()
        fact_end_date -= timedelta(1)
        fact_start_date = fact_end_date - timedelta(30)

        fact_obj.write(
            cursor, uid, fact_id, {
                'data_inici': fact_start_date.strftime('%Y-%m-%d'),
                'data_final': fact_end_date.strftime('%Y-%m-%d')
            }
        )

        warnings = self.validation_warnings(fact_id)

        expect(warnings).not_to(contain('F013'))

    def test_missing_energy_lines__withoutEnergyLines(self):
        factura_id = self.get_fixture('giscegas_facturacio', 'factura_gas_0006_polissa_0001_N')
        line_obj = self.model('giscegas.facturacio.factura.linia')
        line_ids = line_obj.search(
            self.txn.cursor, self.txn.user, [
            ('factura_id', '=', factura_id),
            ('tipus', '=', 'variable'),
            ])
        self.assertFalse(line_ids,
            "Eps la factura te linies de consum d'entrada, busca una altra fixture")

        warnings = self.validation_warnings(factura_id)

        expect(warnings).to(contain('F014'))

    def test_missing_energy_lines__withEnergyLines(self):
        factura_id = self.get_fixture('giscegas_facturacio', 'factura_gas_0001_polissa_0001_N')
        line_obj = self.model('giscegas.facturacio.factura.linia')
        line_ids = line_obj.search(
            self.txn.cursor, self.txn.user, [
            ('factura_id', '=', factura_id),
            ('tipus', '=', 'variable'),
            ])
        self.assertTrue(line_ids,
            "Eps la factura no te linies de consum d'entrada, busca una altra fixture")

        warnings = self.validation_warnings(factura_id)

        expect(warnings).not_to(contain('F014'))

    # Scenario contruction helpers
    def prepare_contract(self, pol_id, data_alta, data_ultima_lectura):
        # set to True when F013 gets fixed
        self.f013_fixed = False

        contract_obj = self.model('giscegas.polissa')
        cursor = self.txn.cursor
        uid = self.txn.user

        vals = {
            'data_alta': data_alta,
            'data_baixa': False,
            'facturacio': 1,
            'data_ultima_lectura': data_ultima_lectura,
            'lot_facturacio': False
        }
        contract_obj.write(cursor, uid, pol_id, vals)
        contract_obj.send_signal(cursor, uid, [pol_id], [
            'validar', 'contracte'
        ])
        contract = contract_obj.browse(cursor, uid, pol_id)
        for meter in contract.comptadors:
            for l in meter.lectures:
                l.unlink(context={})
            meter.write({'lloguer': False})
        return contract.comptadors[0].id

    def create_measure(self, meter_id, date_measure, measure):
        measure_obj = self.model('giscegas.lectures.lectura')
        periode_id = self.get_fixture(
            'giscegas_polissa', 'p1_v_tarifa_31_gas'
        )
        origen_id = self.get_fixture('giscegas_lectures', 'origen_gas_0')

        vals = {
            'name': date_measure,
            'periode': periode_id,
            'lectura': measure,
            'comptador': meter_id,
            'observacions': '',
            'origen_id': origen_id,
            'pressio_subministrament': 1,
            'factor_k': 1,
            'pcs': 1,
        }
        return measure_obj.create(self.txn.cursor, self.txn.user, vals)

    def create_invoice(self, pol_id, meter_id, date_start, date_end, name,
                       context=None):
        journal_obj = self.model('account.journal')
        inv_obj = self.model('giscegas.facturacio.factura')
        wz_mi_obj = self.model('giscegas.wizard.manual.invoice')

        cursor = self.txn.cursor
        uid = self.txn.user

        journal_id = journal_obj.search(
            cursor, uid, [('code', '=', 'GAS')]
        )[0]
        wz_fact_id = wz_mi_obj.create(cursor, uid, {})
        wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
        wz_fact.write({
            'polissa_id': pol_id,
            'date_start': date_start,
            'date_end': date_end,
            'journal_id': journal_id
        })
        wz_fact.action_manual_invoice()
        wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
        inv_id = json.loads(wz_fact.invoice_ids)[0]

        if not context:
            context = {}
        context['number'] = name
        inv_obj.write(cursor, uid, inv_id, context)
        return inv_id

    def create_invoice_related(self, pol_id, meter_id, fact_id, context):
        inv_obj = self.model('giscegas.facturacio.factura')
        cursor = self.txn.cursor
        uid = self.txn.user

        fact_vals = inv_obj.read(cursor, uid, fact_id,
                                 ['data_inici', 'data_final', 'number'])

        head = context.pop('number_head')
        return self.create_invoice(
            pol_id,
            meter_id,
            fact_vals['data_inici'],
            fact_vals['data_final'],
            head + fact_vals['number'],
            context)

    def create_invoice_ab(self, pol_id, meter_id, fact_id):
        context = {
            'tipo_rectificadora': 'B',
            'type': 'out_refund',
            'ref': fact_id,
            'number_head': 'AB-',
        }
        return self.create_invoice_related(pol_id, meter_id, fact_id,
                                           context)

    def create_invoice_re(self, pol_id, meter_id, fact_id):
        context = {
            'tipo_rectificadora': 'R',
            'type': 'out_invoice',
            'ref': fact_id,
            'number_head': 'RE-',
        }
        return self.create_invoice_related(pol_id, meter_id, fact_id,
                                           context)

    def validation_warning_duplicated_overlaped(self, fact_id):
        validator_obj = self.model(
            'giscegas.facturacio.validation.validator')
        fact_obj = self.model('giscegas.facturacio.factura')
        cursor = self.txn.cursor
        uid = self.txn.user
        return validator_obj.check_overlapping_or_duplicated_invoice(
            cursor, uid,
            fact_obj.browse(cursor, uid, fact_id),
            {})

    def test_check_overlapping_or_duplicated_invoice__isolated_invoice_ok(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-10-01', '2018-10-28')
        self.create_measure(meter_id, '2018-10-28', 8000)
        self.create_measure(meter_id, '2018-11-28', 9000)
        inv_id = self.create_invoice(pol_id, meter_id, '2018-11-01',
                                     '2018-11-30', 'FE031415169')

        if self.f013_fixed:
            warnings = self.validation_warnings(inv_id)
            expect(warnings).not_to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv_id)
        self.assertEqual(None, result)

    def test_check_overlapping_or_duplicated_invoice__non_isolated_invoice_ok(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-04-18',
                                      '2018-05-17', 'FG/0003')

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv3_id)
            expect(warnings).not_to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv3_id)
        self.assertEqual(None, result)

    def test_check_overlapping_or_duplicated_invoice__overlaps_end_FE(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-04-10',
                                      '2018-05-10', 'FG/0003')

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).to(contain('F021'))
            warnings = self.validation_warnings(inv3_id)
            expect(warnings).to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual({'colisions': '[FG/0003]'}, result)
        result = self.validation_warning_duplicated_overlaped(inv3_id)
        self.assertEqual({'colisions': '[FG/0002]'}, result)

    def test_check_overlapping_or_duplicated_invoice__overlaps_start_FE(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-11',
                                      '2018-04-10', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-04-11',
                                      '2018-05-10', 'FG/0003')

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).to(contain('F021'))
            warnings = self.validation_warnings(inv3_id)
            expect(warnings).not_to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual({'colisions': '[FG/0002]'}, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual({'colisions': '[FG/0001]'}, result)
        result = self.validation_warning_duplicated_overlaped(inv3_id)
        self.assertEqual(None, result)

    def delete_invoices(self):
        fact_obj = self.model('giscegas.facturacio.factura')
        f_ids = fact_obj.search(self.txn.cursor, self.txn.user, [])
        fact_obj.invoice_cancel(self.txn.cursor, self.txn.user, f_ids)
        fact_obj.unlink(self.txn.cursor, self.txn.user, f_ids)

    def test_check_overlapping_or_duplicated_invoice__duplicated_FE(self):
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)

        self.delete_invoices()

        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-05-17', 'FG/0003')

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).to(contain('F021'))
            warnings = self.validation_warnings(inv3_id)
            expect(warnings).to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual({'colisions': '[FG/0003]'}, result)
        result = self.validation_warning_duplicated_overlaped(inv3_id)
        self.assertEqual({'colisions': '[FG/0002]'}, result)

    def test_check_overlapping_or_duplicated_invoice__multiple_overlaps_FE(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-20', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-03-10',
                                      '2018-05-17', 'FG/0003')

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).to(contain('F021'))
            warnings = self.validation_warnings(inv3_id)
            expect(warnings).to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual({'colisions': '[FG/0002, FG/0003]'}, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual({'colisions': '[FG/0001, FG/0003]'}, result)
        result = self.validation_warning_duplicated_overlaped(inv3_id)
        self.assertEqual({'colisions': '[FG/0001, FG/0002]'}, result)

    def test_check_overlapping_or_duplicated_invoice__with_AB_ok(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-04-10',
                                      '2018-05-10', 'FG/0003')

        iab3_id = self.create_invoice_ab(pol_id, meter_id, inv3_id)

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).not_to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual(None, result)

    def test_check_overlapping_or_duplicated_invoice__with_AB_RE_no_ok(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-04-10',
                                      '2018-05-10', 'FG/0003')

        iab3_id = self.create_invoice_ab(pol_id, meter_id, inv3_id)
        ire3_id = self.create_invoice_re(pol_id, meter_id, inv3_id)

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).to(contain('F021'))
            warnings = self.validation_warnings(ire3_id)
            expect(warnings).to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual({'colisions': '[RE-FG/0003]'}, result)
        result = self.validation_warning_duplicated_overlaped(ire3_id)
        self.assertEqual({'colisions': '[FG/0002]'}, result)

    def test_check_overlapping_or_duplicated_invoice__with_RE_no_ok(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-04-10',
                                      '2018-05-10', 'FG/0003')

        ire3_id = self.create_invoice_re(pol_id, meter_id, inv3_id)

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).to(contain('F021'))
            warnings = self.validation_warnings(ire3_id)
            expect(warnings).to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual({'colisions': '[RE-FG/0003]'}, result)
        result = self.validation_warning_duplicated_overlaped(ire3_id)
        self.assertEqual({'colisions': '[FG/0002]'}, result)

    def test_check_overlapping_or_duplicated_invoice__with_AB_RE_ok(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-04-18',
                                      '2018-05-17', 'FG/0003')

        iab3_id = self.create_invoice_ab(pol_id, meter_id, inv3_id)
        ire3_id = self.create_invoice_re(pol_id, meter_id, inv3_id)

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(ire3_id)
            expect(warnings).not_to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(ire3_id)
        self.assertEqual(None, result)

    def test_check_overlapping_or_duplicated_invoice__with_RE_ok(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-04-18',
                                      '2018-05-17', 'FG/0003')

        ire3_id = self.create_invoice_re(pol_id, meter_id, inv3_id)

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(ire3_id)
            expect(warnings).not_to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(ire3_id)
        self.assertEqual(None, result)

    def test_check_overlapping_or_duplicated_invoice__with_AB_RE_AB_RE_no_ok(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-04-10',
                                      '2018-05-10', 'FG/0003')

        iab3_id = self.create_invoice_ab(pol_id, meter_id, inv3_id)
        ire3_id = self.create_invoice_re(pol_id, meter_id, inv3_id)

        iab3b_id = self.create_invoice_ab(pol_id, meter_id, ire3_id)
        ire3b_id = self.create_invoice_re(pol_id, meter_id, ire3_id)

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).to(contain('F021'))
            warnings = self.validation_warnings(ire3b_id)
            expect(warnings).to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual({'colisions': '[RE-RE-FG/0003]'}, result)
        result = self.validation_warning_duplicated_overlaped(ire3b_id)
        self.assertEqual({'colisions': '[FG/0002]'}, result)

    def test_check_overlapping_or_duplicated_invoice__with_AB_RE_AB_RE_ok(self):
        self.delete_invoices()
        pol_id = self.get_fixture('giscegas_polissa', 'polissa_gas_0001')
        meter_id = self.prepare_contract(pol_id, '2018-01-01', '2018-02-15')
        self.create_measure(meter_id, '2018-02-15', 8000)
        self.create_measure(meter_id, '2018-03-15', 8600)
        self.create_measure(meter_id, '2018-04-15', 9000)
        self.create_measure(meter_id, '2018-05-15', 9350)
        inv1_id = self.create_invoice(pol_id, meter_id, '2018-02-18',
                                      '2018-03-17', 'FG/0001')
        inv2_id = self.create_invoice(pol_id, meter_id, '2018-03-18',
                                      '2018-04-17', 'FG/0002')
        inv3_id = self.create_invoice(pol_id, meter_id, '2018-04-18',
                                      '2018-05-17', 'FG/0003')

        iab3_id = self.create_invoice_ab(pol_id, meter_id, inv3_id)
        ire3_id = self.create_invoice_re(pol_id, meter_id, inv3_id)

        iab3b_id = self.create_invoice_ab(pol_id, meter_id, ire3_id)
        ire3b_id = self.create_invoice_re(pol_id, meter_id, ire3_id)

        if self.f013_fixed:
            warnings = self.validation_warnings(inv1_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(inv2_id)
            expect(warnings).not_to(contain('F021'))
            warnings = self.validation_warnings(ire3b_id)
            expect(warnings).not_to(contain('F021'))

        result = self.validation_warning_duplicated_overlaped(inv1_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(inv2_id)
        self.assertEqual(None, result)
        result = self.validation_warning_duplicated_overlaped(ire3b_id)
        self.assertEqual(None, result)

def test_validation_F023(self):
        vali_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        warn_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning'
        )
        fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        temp_obj = self.openerp.pool.get(
            'giscegas.facturacio.validation.warning.template'
        )
        linia_obj = self.openerp.pool.get('giscegas.facturacio.factura.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1]

        fact_vals = fact_obj.read(cursor, uid, fact_id, ['linia_ids'])

        # We unlink the previous lines and create the new ones with import 0
        # so that the checks for when import is 0 (energy, power and total) pop
        linia_obj.unlink(cursor, uid, fact_vals['linia_ids'])

        linia_obj.create(cursor, uid, {
            'name': 'Linia 1',
            'tipus': 'altres',
            'price_unit': 1,
            'factura_id': fact_id,
            'product_id': False,
            'account_id': 1,
            'quantity': 2,
            'data_desde': '2020-01-01',
            'data_fins': '2021-01-01'
        })

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )

        warning_names = [warn['name'] for warn in warning_vals]
        expect(warning_names).to(contain('F023'))


class TestsRectifyingState(testing.OOTestCase):

    def setUp(self):
        imd_obj = self.openerp.pool.get('ir.model.data')

        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        demo_payment_mode = imd_obj.get_object_reference(
            cursor, uid, 'account_payment', 'payment_mode_demo'
        )[1]
        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio',
            'factura_gas_0001_polissa_0001_N'
        )[1]
        pricelist_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio',
            'pricelist_tarifas_gas'
        )[1]
        self.demo_data = {
            'payment_mode_demo': demo_payment_mode,
            'factura_gas_0001_polissa_0001_N': fact_id,
            'pricelist_tarifas_gas': pricelist_id,
        }

        self.txn.stop()

    def test_both_start_equal(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = self.demo_data['factura_gas_0001_polissa_0001_N']

            fact_vals = factura_obj.read(
                cursor, uid, fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type'
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_invoice = factura_obj.read(
                    cursor, uid, fact_vals['ref'][0], ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('N'))

            expect(ref_invoice).to(equal(rectifying_id))
            expect(ref_invoice).to(equal(False))

    def test_changing_invoice_changes_both(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = self.demo_data['factura_gas_0001_polissa_0001_N']

            inv_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            other_fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0002_polissa_0001_N'
            )[1]
            other_inv_id = factura_obj.read(
                cursor, uid, other_fact_id, ['invoice_id']
            )['invoice_id'][0]

            invoice_obj.write(
                cursor, uid, inv_id, {
                    'rectifying_id': other_inv_id,
                    'rectificative_type': 'R'
                }
            )

            fact_vals = factura_obj.read(
                cursor, uid, fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type'
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_factura = fact_vals['ref'][0]
                ref_invoice = factura_obj.read(
                    cursor, uid, ref_factura, ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_factura = False
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            # Both of the types should be 'R'
            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('R'))

            # Both of the base id (for invoice) should be equal
            expect(ref_invoice).to(equal(rectifying_id))
            # Despite that, factura should point to the other factura and
            # invoice to the other invoice
            expect(ref_factura).to(equal(other_fact_id))
            expect(rectifying_id).to(equal(other_inv_id))

    def test_changing_invoice_from_factura_changes_both(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = self.demo_data['factura_gas_0001_polissa_0001_N']

            other_fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0002_polissa_0001_N'
            )[1]
            other_inv_id = factura_obj.read(
                cursor, uid, other_fact_id, ['invoice_id']
            )['invoice_id'][0]

            factura_obj.write(
                cursor, uid, fact_id, {
                    'rectifying_id': other_inv_id,
                    'rectificative_type': 'R'
                }
            )

            fact_vals = factura_obj.read(
                cursor, uid, fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type'
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_factura = fact_vals['ref'][0]
                ref_invoice = factura_obj.read(
                    cursor, uid, ref_factura, ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_factura = False
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            # Both of the types should be 'R'
            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('R'))

            # Both of the base id (for invoice) should be equal
            expect(ref_invoice).to(equal(rectifying_id))
            # Despite that, factura should point to the other factura and
            # invoice to the other invoice
            expect(ref_factura).to(equal(other_fact_id))
            expect(rectifying_id).to(equal(other_inv_id))

    def test_changing_factura_changes_both(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = self.demo_data['factura_gas_0001_polissa_0001_N']

            other_fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0002_polissa_0001_N'
            )[1]
            other_inv_id = factura_obj.read(
                cursor, uid, other_fact_id, ['invoice_id']
            )['invoice_id'][0]

            factura_obj.write(
                cursor, uid, fact_id, {
                    'ref': other_fact_id,
                    'tipo_rectificadora': 'R'
                }
            )

            fact_vals = factura_obj.read(
                cursor, uid, fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type'
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_factura = fact_vals['ref'][0]
                ref_invoice = factura_obj.read(
                    cursor, uid, ref_factura, ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_factura = False
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            # Both of the types should be 'R'
            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('R'))

            # Both of the base id (for invoice) should be equal
            expect(ref_invoice).to(equal(rectifying_id))
            # Despite that, factura should point to the other factura and
            # invoice to the other invoice
            expect(ref_factura).to(equal(other_fact_id))
            expect(rectifying_id).to(equal(other_inv_id))

    def test_changing_both_changes_both(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
            )[1]

            other_fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0002_polissa_0001_N'
            )[1]
            other_inv_id = factura_obj.read(
                cursor, uid, other_fact_id, ['invoice_id']
            )['invoice_id'][0]

            factura_obj.write(
                cursor, uid, fact_id, {
                    'ref': other_fact_id,
                    'rectifying_id': other_inv_id,
                    'tipo_rectificadora': 'R',
                    'rectificative_type': 'R',
                }
            )

            fact_vals = factura_obj.read(
                cursor, uid, fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type'
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_factura = fact_vals['ref'][0]
                ref_invoice = factura_obj.read(
                    cursor, uid, ref_factura, ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_factura = False
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            # Both of the types should be 'R'
            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('R'))

            # Both of the base id (for invoice) should be equal
            expect(ref_invoice).to(equal(rectifying_id))
            # Despite that, factura should point to the other factura and
            # invoice to the other invoice
            expect(ref_factura).to(equal(other_fact_id))
            expect(rectifying_id).to(equal(other_inv_id))

    def test_creating_on_both_sets_both(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        factura_base = {
            'date_invoice': '2018-01-01',
            'polissa_id': 1,
            'account_id': 1,
            'company_id': 1,
            'date_boe': '2018-01-01',
            'journal_id': 1,
            'cups_id': 1,
            'facturacio': 1,
            'currency_id': 1,
            'address_invoice_id': 1,
            'pressio': 3,
            'tarifa_acces_id': 1,
            'llista_preu': self.demo_data['pricelist_tarifas_gas'],
            'partner_id': 1,
            'periode_liquidacio': 1,
            'data_inici': '2018-01-01',
            'data_final': '2018-01-31',
            'name': 'FP999',
        }

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            other_fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0002_polissa_0001_N'
            )[1]
            other_inv_id = factura_obj.read(
                cursor, uid, other_fact_id, ['invoice_id']
            )['invoice_id'][0]

            factura_base.update(
                {
                    'ref': other_fact_id,
                    'rectifying_id': other_inv_id,
                    'tipo_rectificadora': 'R',
                    'rectificative_type': 'R',
                    'payment_mode_id': self.demo_data['payment_mode_demo']
                }
            )
            fact_id = factura_obj.create(
                cursor, uid, factura_base
            )

            fact_vals = factura_obj.read(
                cursor, uid, fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type'
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_factura = fact_vals['ref'][0]
                ref_invoice = factura_obj.read(
                    cursor, uid, ref_factura, ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_factura = False
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            # Both of the types should be 'R'
            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('R'))

            # Both of the base id (for invoice) should be equal
            expect(ref_invoice).to(equal(rectifying_id))
            # Despite that, factura should point to the other factura and
            # invoice to the other invoice
            expect(ref_factura).to(equal(other_fact_id))
            expect(rectifying_id).to(equal(other_inv_id))

    def test_creating_on_invoice_sets_both(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        factura_base = {
            'date_invoice': '2018-01-01',
            'polissa_id': 1,
            'account_id': 1,
            'company_id': 1,
            'date_boe': '2018-01-01',
            'journal_id': 1,
            'cups_id': 1,
            'facturacio': 1,
            'currency_id': 1,
            'address_invoice_id': 1,
            'pressio': 3,
            'tarifa_acces_id': 1,
            'llista_preu': self.demo_data['pricelist_tarifas_gas'],
            'partner_id': 1,
            'periode_liquidacio': 1,
            'data_inici': '2018-01-01',
            'data_final': '2018-01-31',
            'name': 'FP999',
        }

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            other_fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0002_polissa_0001_N'
            )[1]
            other_inv_id = factura_obj.read(
                cursor, uid, other_fact_id, ['invoice_id']
            )['invoice_id'][0]

            factura_base.update(
                {
                    'rectifying_id': other_inv_id,
                    'rectificative_type': 'R',
                    'payment_mode_id': self.demo_data['payment_mode_demo']
                }
            )
            fact_id = factura_obj.create(
                cursor, uid, factura_base
            )

            fact_vals = factura_obj.read(
                cursor, uid, fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type'
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_factura = fact_vals['ref'][0]
                ref_invoice = factura_obj.read(
                    cursor, uid, ref_factura, ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_factura = False
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            # Both of the types should be 'R'
            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('R'))

            # Both of the base id (for invoice) should be equal
            expect(ref_invoice).to(equal(rectifying_id))
            # Despite that, factura should point to the other factura and
            # invoice to the other invoice
            expect(ref_factura).to(equal(other_fact_id))
            expect(rectifying_id).to(equal(other_inv_id))

    def test_creating_on_factura_sets_both(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        factura_base = {
            'date_invoice': '2018-01-01',
            'polissa_id': 1,
            'account_id': 1,
            'company_id': 1,
            'date_boe': '2018-01-01',
            'journal_id': 1,
            'cups_id': 1,
            'facturacio': 1,
            'currency_id': 1,
            'address_invoice_id': 1,
            'pressio': 3,
            'tarifa_acces_id': 1,
            'llista_preu': self.demo_data['pricelist_tarifas_gas'],
            'partner_id': 1,
            'periode_liquidacio': 1,
            'data_inici': '2018-01-01',
            'data_final': '2018-01-31',
            'name': 'FP999',
        }

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            other_fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0002_polissa_0001_N'
            )[1]
            other_inv_id = factura_obj.read(
                cursor, uid, other_fact_id, ['invoice_id']
            )['invoice_id'][0]

            factura_base.update(
                {
                    'ref': other_fact_id,
                    'tipo_rectificadora': 'R',
                    'payment_mode_id': self.demo_data['payment_mode_demo']
                }
            )
            fact_id = factura_obj.create(
                cursor, uid, factura_base
            )

            fact_vals = factura_obj.read(
                cursor, uid, fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type'
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_factura = fact_vals['ref'][0]
                ref_invoice = factura_obj.read(
                    cursor, uid, ref_factura, ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_factura = False
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            # Both of the types should be 'R'
            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('R'))

            # Both of the base id (for invoice) should be equal
            expect(ref_invoice).to(equal(rectifying_id))
            # Despite that, factura should point to the other factura and
            # invoice to the other invoice
            expect(ref_factura).to(equal(other_fact_id))
            expect(rectifying_id).to(equal(other_inv_id))

    def test_creating_without_anything_sets_both_to_default(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')

        factura_base = {
            'date_invoice': '2018-01-01',
            'polissa_id': 1,
            'account_id': 1,
            'company_id': 1,
            'date_boe': '2018-01-01',
            'journal_id': 1,
            'cups_id': 1,
            'facturacio': 1,
            'currency_id': 1,
            'address_invoice_id': 1,
            'pressio': 3.5,
            'tarifa_acces_id': 1,
            'llista_preu': self.demo_data['pricelist_tarifas_gas'],
            'partner_id': 1,
            'periode_liquidacio': 1,
            'data_inici': '2018-01-01',
            'data_final': '2018-01-31',
            'name': 'FP999',
            'payment_mode_id': self.demo_data['payment_mode_demo']
        }

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = factura_obj.create(
                cursor, uid, factura_base
            )

            fact_vals = factura_obj.read(
                cursor, uid, fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type',
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_factura = fact_vals['ref'][0]
                ref_invoice = factura_obj.read(
                    cursor, uid, ref_factura, ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_factura = False
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            # Both of the types should be 'N' (the default)
            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('N'))

            # Both of the base id (for invoice) should be equal
            expect(ref_invoice).to(equal(rectifying_id))
            # And by default all should be False
            expect(ref_factura).to(equal(False))
            expect(rectifying_id).to(equal(False))

    def test_copying_on_factura_sets_both(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = self.demo_data['factura_gas_0001_polissa_0001_N']

            other_fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0002_polissa_0001_N'
            )[1]
            other_inv_id = factura_obj.read(
                cursor, uid, other_fact_id, ['invoice_id']
            )['invoice_id'][0]

            default = {
                'ref': other_fact_id,
                'tipo_rectificadora': 'R',
            }
            new_fact_id = factura_obj.copy(
                cursor, uid, fact_id, default
            )

            fact_vals = factura_obj.read(
                cursor, uid, new_fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type'
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_factura = fact_vals['ref'][0]
                ref_invoice = factura_obj.read(
                    cursor, uid, ref_factura, ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_factura = False
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            # Both of the types should be 'R'
            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('R'))

            # Both of the base id (for invoice) should be equal
            expect(ref_invoice).to(equal(rectifying_id))
            # Despite that, factura should point to the other factura and
            # invoice to the other invoice
            expect(ref_factura).to(equal(other_fact_id))
            expect(rectifying_id).to(equal(other_inv_id))

    def test_copying_on_both_sets_both(self):
        factura_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = self.demo_data['factura_gas_0001_polissa_0001_N']

            other_fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0002_polissa_0001_N'
            )[1]
            other_inv_id = factura_obj.read(
                cursor, uid, other_fact_id, ['invoice_id']
            )['invoice_id'][0]

            default = {
                'ref': other_fact_id,
                'tipo_rectificadora': 'R',
                'rectifying_id': other_inv_id,
                'rectificative_type': 'R',
            }
            new_fact_id = factura_obj.copy(
                cursor, uid, fact_id, default
            )

            fact_vals = factura_obj.read(
                cursor, uid, new_fact_id, [
                    'ref',
                    'rectifying_id',
                    'tipo_rectificadora',
                    'rectificative_type'
                ]
            )

            tipo_rectificadora = fact_vals['tipo_rectificadora']
            rectificative_type = fact_vals['rectificative_type']
            if fact_vals['ref']:
                ref_factura = fact_vals['ref'][0]
                ref_invoice = factura_obj.read(
                    cursor, uid, ref_factura, ['invoice_id']
                )['invoice_id'][0]
            else:
                ref_factura = False
                ref_invoice = False
            if fact_vals['rectifying_id']:
                rectifying_id = fact_vals['rectifying_id'][0]
            else:
                rectifying_id = False

            # Both of the types should be 'R'
            expect(tipo_rectificadora).to(equal(rectificative_type))
            expect(tipo_rectificadora).to(equal('R'))

            # Both of the base id (for invoice) should be equal
            expect(ref_invoice).to(equal(rectifying_id))
            # Despite that, factura should point to the other factura and
            # invoice to the other invoice
            expect(ref_factura).to(equal(other_fact_id))
            expect(rectifying_id).to(equal(other_inv_id))


class TestsGiscegasFacturacioFactura(testing.OOTestCase):

    def test_duplicate_factura(self):
        pool = self.openerp.pool
        factura_obj = pool.get('giscegas.facturacio.factura')
        imd_obj = pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
            )[1]

            expected_values = {
                'number': False,
                'state': 'draft',
                'period_id': False,
                'origin': False,
                'origin_date_invoice': False,
                'lot_facturacio': False
            }

            copy_res_id = factura_obj.copy(cursor, uid, factura_id)
            factura_data = factura_obj.read(
                cursor, uid, copy_res_id, expected_values.keys()
            )
            factura_data.pop('id')
        self.assertDictEqual(expected_values, factura_data)

    def test_anullar(self):
        pool = self.openerp.pool

        factura_obj = pool.get('giscegas.facturacio.factura')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
            )[1]
            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'lot_gas_0002'
            )[1]
    
            factura_obj.write(cursor, uid, factura_id, {
                'lot_facturacio': lot_id
            })
            factura = factura_obj.browse(cursor, uid, factura_id)
    
            # Anulem la factura
            res_id = factura.anullar(tipus='A', context={})
    
            # Comprovem els seus valors
            self.assertEqual(len(res_id), 1)
            anuladora = factura_obj.browse(cursor, uid, res_id[0])
            self.assertEqual(factura.amount_total, anuladora.amount_total)
            self.assertEqual(factura.partner_id, anuladora.partner_id)
            self.assertEqual(factura.polissa_id, anuladora.polissa_id)
            self.assertEqual(factura.data_inici, anuladora.data_inici)
            self.assertEqual(factura.data_final, anuladora.data_final)
            self.assertEqual(len(factura.linia_ids), len(anuladora.linia_ids))
            self.assertEqual(len(factura.linies_consum), len(anuladora.linies_consum))
            self.assertEqual(factura.linies_consum[0].price_subtotal, anuladora.linies_consum[0].price_subtotal)
            self.assertEqual(anuladora.rectifying_id.id, factura.invoice_id.id)
            self.assertEqual(anuladora.type, 'out_refund')
            self.assertEqual(anuladora.rectificative_type, 'A')
            self.assertFalse(anuladora.lot_facturacio)

    def test_rectificar_amb_anuladora(self):
        pool = self.openerp.pool

        factura_obj = pool.get('giscegas.facturacio.factura')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
            )[1]
            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'lot_gas_0002'
            )[1]
    
            factura_obj.write(cursor, uid, factura_id, {
                'lot_facturacio': lot_id
            })
            factura = factura_obj.browse(cursor, uid, factura_id)
            factura.lectures_ids[0].write({
                'data_anterior': factura.data_inici,
                'data_actual': factura.data_final
            })
            factura.polissa_id.write({
                'lot_facturacio': lot_id,
                'data_alta': factura.data_inici,
                'facturacio': 2
            })
            factura.polissa_id.comptadors[0].write({'lloguer': False})
            factura.polissa_id.comptadors[0].lectures[0].write({
                'name': factura.data_inici,
                'lectura': 0,
                'ajust': 0,
                'pcs': 1,
                'factor_k': 1
            })
            factura.polissa_id.comptadors[0].lectures[1].write({
                'name': factura.data_final,
                'lectura': 1,
                'ajust': 0,
                'pcs': 1,
                'factor_k': 1
            })
            factura.polissa_id.send_signal(['validar', 'contracte'])
            factura.lot_facturacio.write({
                'data_inici': factura.data_inici,
                'data_final': factura.data_final
            })
    
            # El contracte ja te unes lectures diferents a les de la factura
            # per tant la rectificadora tindrà valors diferents.
            # Rectificadora amb anuladora
            res_id = factura.rectificar(tipus='R', context={})
    
            # Comprovem els seus valors
            self.assertEqual(len(res_id), 2)
            anuladora = factura_obj.browse(cursor, uid, res_id[0])
            self.assertEqual(factura.amount_total, anuladora.amount_total)
            self.assertEqual(factura.partner_id, anuladora.partner_id)
            self.assertEqual(factura.polissa_id, anuladora.polissa_id)
            self.assertEqual(factura.data_inici, anuladora.data_inici)
            self.assertEqual(factura.data_final, anuladora.data_final)
            self.assertEqual(len(factura.linia_ids), len(anuladora.linia_ids))
            self.assertEqual(len(factura.linies_consum), len(anuladora.linies_consum))
            self.assertEqual(factura.linies_consum[0].price_subtotal, anuladora.linies_consum[0].price_subtotal)
            self.assertEqual(anuladora.rectifying_id.id, factura.invoice_id.id)
            self.assertEqual(anuladora.type, 'out_refund')
            self.assertEqual(anuladora.rectificative_type, 'B')
            self.assertFalse(anuladora.lot_facturacio)

            rectificadora = factura_obj.browse(cursor, uid, res_id[1])
            self.assertEqual(factura.partner_id, rectificadora.partner_id)
            self.assertEqual(factura.polissa_id, rectificadora.polissa_id)
            self.assertEqual(factura.data_inici, rectificadora.data_inici)
            self.assertEqual(factura.data_final, rectificadora.data_final)
            self.assertEqual(2, len(rectificadora.linia_ids))
            self.assertEqual(1, len(rectificadora.linies_consum))
            self.assertEqual(rectificadora.lectures_ids[0].data_actual, factura.data_final)
            self.assertEqual(rectificadora.lectures_ids[0].consum, 1)
            self.assertEqual(rectificadora.rectifying_id.id, factura.invoice_id.id)
            self.assertEqual(rectificadora.type, 'out_invoice')
            self.assertEqual(rectificadora.rectificative_type, 'R')
            self.assertFalse(rectificadora.lot_facturacio)

    def test_rectificar_amb_anuladora_ficticia(self):
        pool = self.openerp.pool

        factura_obj = pool.get('giscegas.facturacio.factura')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
            )[1]
            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'lot_gas_0002'
            )[1]

            # Creem un res.partner.bank per posarli a la factura original
            # Al anular hauria de agafar el nou compte i no el de la original
            factura = factura_obj.browse(cursor, uid, factura_id)
            bank_obj = self.openerp.pool.get("res.partner.bank")
            bank1_id = bank_obj.create(cursor, uid, {
                'name': "bank 1",
                'partner_id': factura.partner_id.id,
                'state': 'bank'
            })
            # Posem tipo pago 'RECIBO_CSB' perque així al anular intentara
            # copiar el partner_bank de la anulada
            payment_obj = self.openerp.pool.get("payment.type")
            payment_id = payment_obj.create(cursor, uid, {
                'name': "payment1",
                'code': "RECIBO_CSB"
            })
    
            factura_obj.write(cursor, uid, factura_id, {
                'lot_facturacio': lot_id,
                'partner_bank': bank1_id,
                'payment_type': payment_id
            })
            factura = factura_obj.browse(cursor, uid, factura_id)
            factura.lectures_ids[0].write({
                'data_anterior': factura.data_inici,
                'data_actual': factura.data_final
            })
            # Creem un altre res.partner.bank diferent i la podem al contracte,
            # la rectificadora i l'anuladora haurien de portar el nou compte
            bank2_id = bank_obj.create(cursor, uid, {
                'name': "bank 2",
                'partner_id': factura.partner_id.id,
                'state': 'bank'
            })
            factura.polissa_id.write({
                'lot_facturacio': lot_id,
                'data_alta': factura.data_inici,
                'facturacio': 2,
                'bank': bank2_id,
                'tipo_pago': payment_id
            })
            factura.polissa_id.comptadors[0].write({'lloguer': False})
            factura.polissa_id.comptadors[0].lectures[0].write({
                'name': factura.data_inici,
                'lectura': 0,
                'ajust': 0,
                'pcs': 1,
                'factor_k': 1
            })
            factura.polissa_id.comptadors[0].lectures[1].write({
                'name': factura.data_final,
                'lectura': 1,
                'ajust': 0,
                'pcs': 1,
                'factor_k': 1
            })
            factura.polissa_id.send_signal(['validar', 'contracte'])
            factura.lot_facturacio.write({
                'data_inici': factura.data_inici,
                'data_final': factura.data_final
            })
    
            # El contracte ja te unes lectures diferents a les de la factura
            # per tant la rectificadora tindrà valors diferents.
            # Rectificadora amb anuladora
            res_id = factura.rectificar_substitucio(tipus='RA', context={})
    
            # Comprovem els seus valors
            self.assertEqual(len(res_id), 2)
            anuladora = factura_obj.browse(cursor, uid, res_id[0])
            self.assertEqual(factura.amount_total, anuladora.amount_total)
            self.assertEqual(factura.partner_id, anuladora.partner_id)
            self.assertEqual(factura.polissa_id, anuladora.polissa_id)
            self.assertEqual(factura.data_inici, anuladora.data_inici)
            self.assertEqual(factura.data_final, anuladora.data_final)
            self.assertEqual(len(factura.linia_ids), len(anuladora.linia_ids))
            self.assertEqual(len(factura.linies_consum), len(anuladora.linies_consum))
            self.assertEqual(factura.linies_consum[0].price_subtotal, anuladora.linies_consum[0].price_subtotal)
            self.assertEqual(anuladora.rectifying_id.id, factura.invoice_id.id)
            self.assertEqual(anuladora.type, 'out_refund')
            self.assertEqual(anuladora.rectificative_type, 'BRA')
            self.assertEqual(anuladora.partner_bank.id, bank2_id)
            self.assertFalse(anuladora.lot_facturacio)
            rectificadora = factura_obj.browse(cursor, uid, res_id[1])
            self.assertEqual(factura.partner_id, rectificadora.partner_id)
            self.assertEqual(factura.polissa_id, rectificadora.polissa_id)
            self.assertEqual(factura.data_inici, rectificadora.data_inici)
            self.assertEqual(factura.data_final, rectificadora.data_final)
            self.assertEqual(2, len(
                [
                    line for line in rectificadora.linia_ids
                    if 'Extra' not in line.product_id.name
                ]
            ))
            self.assertEqual(1, len(rectificadora.linies_consum))
            self.assertEqual(rectificadora.lectures_ids[0].data_actual, factura.data_final)
            self.assertEqual(rectificadora.lectures_ids[0].consum, 1)
            self.assertEqual(rectificadora.rectifying_id.id, factura.invoice_id.id)
            self.assertEqual(rectificadora.type, 'out_invoice')
            self.assertEqual(rectificadora.rectificative_type, 'RA')
            self.assertEqual(rectificadora.partner_bank.id, bank2_id)
            self.assertFalse(rectificadora.lot_facturacio)
