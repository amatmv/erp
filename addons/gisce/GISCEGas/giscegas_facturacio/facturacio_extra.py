# -*- coding: utf-8 -*-

from osv import osv, fields
from tools import config, float_round
from tools import cache


class FacturacioExtra(osv.osv):

    _name = 'giscegas.facturacio.extra'

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}

        product = self.pool.get('product.product')

        if not vals.get('account_id', False):
            product_id = vals.get('product_id', False)
            if product_id:
                account_id = product.get_account(
                    cursor, uid, product_id, account_property_name='income',
                    context=context
                )
                vals.update({'account_id': account_id})

        return super(FacturacioExtra, self).create(cursor, uid, vals, context)

    def _tipus_selection(self, cursor, uid, context=None):
        """Return select as factura linia does"""
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        return linia_obj._tipus_selection(cursor, uid, context=context)

    def _get_price(self, cursor, uid, ids, name, args, context=None):

        res = {}
        for extra in self.browse(cursor, uid, ids):
            res[extra.id] = {'price_subtotal': 0,
                             'price_term': 0}
            price_subtotal = float_round(extra.price_unit * extra.quantity,
                                  int(config['price_accuracy']))
            price_term = float_round((price_subtotal -
                                extra.amount_invoiced) /
                               (extra.term * (extra.quantity or 1.0)),
                               int(config['price_accuracy']))
            res[extra.id].update({'price_subtotal': price_subtotal,
                                  'price_term': price_term})
        return res

    def _round_by_currency(self, cursor, uid, ids, value, context=None):
        '''rounds the value as a function of the currency'''
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        cur_obj = self.pool.get('res.currency')
        extra = self.browse(cursor, uid, ids, context=context)
        return cur_obj.float_round(cursor, uid,
                                   extra.factura_ids[0].currency_id,
                                   value)

    def _get_amount(self, cursor, uid, ids, name, args, context=None):
        res = {}
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        for extra in self.browse(cursor, uid, ids, context=context):
            if not extra.factura_linia_ids:
                res[extra.id] = {'amount_invoiced': 0,
                                 'total_amount_invoiced': 0,
                                 'amount_pending': extra.price_subtotal,
                                 'total_amount_pending': extra.price_subtotal}
                continue
            amount_invoiced = 0
            total_amount_invoiced = 0
            for linia in extra.factura_linia_ids:
                #Proforma and cancelled invoices do not compute amounts
                if linia.factura_id.state in ('proforma2', 'cancel'):
                    continue
                if linia.factura_id.type == 'out_invoice':
                    multiplier = 1
                elif linia.factura_id.type == 'out_refund':
                    multiplier = -1
                total_amount_invoiced += linia.price_subtotal * multiplier
                if linia.factura_id.state in ('open', 'paid'):
                    amount_invoiced += linia.price_subtotal * multiplier        
            subtotal = extra._round_by_currency(extra.price_subtotal)
            amount_pending = subtotal - amount_invoiced
            total_amount_pending = subtotal - total_amount_invoiced
            res[extra.id] = {'amount_invoiced': amount_invoiced,
                             'total_amount_invoiced': total_amount_invoiced,
                             'amount_pending': amount_pending,
                             'total_amount_pending': total_amount_pending}
        return res

    def _get_extra_from_invoice(self, cursor, uid, ids, context=None):
        '''return extra associated with linia in factura in invoice ids'''

        sql = '''SELECT distinct extra_id
              FROM giscegas_facturacio_extra_factura_linia_rel
              WHERE factura_linia_id in
              (select l.id from giscegas_facturacio_factura_linia l
              inner join giscegas_facturacio_factura f
              on f.id = l.factura_id
              where f.invoice_id in %s)'''

        cursor.execute(sql, (tuple(ids),))
        res = cursor.fetchall()
        return [x[0] for x in res]

    @cache()
    def _get_product(self, cursor, uid, context=None):
        '''returns product id of P1E 2.0A for extra values'''
        tarifa_obj = self.pool.get('giscegas.polissa.tarifa')
        search_params = [('name', '=', '3.1')]
        tarifa_id = tarifa_obj.search(cursor, uid, search_params)[0]
        return tarifa_obj.get_periodes_producte(
            cursor, uid, tarifa_id, 'te', context=context)['P1']

    def _get_extra_default_product(self, cursor, uid, context=None):
        '''returns extra default product'''
        product_obj = self.pool.get('product.product')
        search_params = [('default_code', '=', 'EXTRA')]
        product_id = product_obj.search(cursor, uid, search_params)
        if product_id:
            return product_id[0]
        return False

    def _get_extra_default_uos(self, cursor, uid, context=None):
        '''returns extra default product'''
        product_obj = self.pool.get('product.product')
        search_params = [('default_code', '=', 'EXTRA')]
        product_ids = product_obj.search(cursor, uid, search_params)
        if product_ids:
            return product_obj.read(cursor, uid, product_ids,
                                    ['uom_po_id'])[0]['uom_po_id'][0]
        return False

    def _get_default_account(self, cursor, uid, context=None):
        '''Default account will be the one associated to P1E of 2.0A'''
        product_id = self._get_product(cursor, uid)
        product = self.pool.get('product.product').browse(cursor,
                                                          uid, product_id)
        return product.product_tmpl_id.property_account_income and\
            product.product_tmpl_id.property_account_income.id or\
            product.categ_id.property_account_income_categ.id

    def _get_default_tax(self, cursor, uid, context=None):
        '''Default tax are the same of P1E product from a 2.0A'''
        product_obj = self.pool.get('product.product')
        product_id = self._get_product(cursor, uid)
        res = {}
        for tax in product_obj.browse(cursor, uid, product_id).taxes_id:
            res[tax.id] = True
        return res.keys()

    def _get_default_tipus(self, cursor, uid, context=None):
        '''Default tipus is the one with code altres'''
        tipus_obj = self.pool.get('giscegas.facturacio.factura.linia.tipus')
        search_params = [('codi', '=', 'altres')]
        tipus_id = tipus_obj.search_reader(cursor, uid,
                                           search_params,
                                           ['codi'])
        if tipus_id:
            return tipus_id[0]['codi']
        return False


    def get_extra_lines_from_contract_and_date(
            self, cursor, uid, contract_id, invoice_date, context=None
    ):
        # Search extra associated with the polissa in factura
        # and create one line per each extra found
        search_params = [('polissa_id', '=', contract_id),
                         ('date_from', '<=', invoice_date),
                         ('date_to', '>=', invoice_date)]
        extra_ids = self.search(
            cursor, uid, search_params, context=context
        )
        # Finished extra lines with pending
        search_params = [('polissa_id', '=', contract_id),
                         ('total_amount_pending', '!=', 0),
                         ('date_to', '<', invoice_date)]
        extra_ids.extend(
            self.search(cursor, uid, search_params, context=context)
        )

        # Quan el contracte està de baixa i la factura es la última s'afegeixen
        # totes les lineas extra pendents
        pol_obj = self.pool.get("giscegas.polissa")
        pol_info = pol_obj.read(cursor, uid, contract_id, ['data_baixa'], context=context)
        data_final_contracte = pol_info['data_baixa']
        if invoice_date == data_final_contracte:
            search_params = [('polissa_id', '=', contract_id),
                             ('total_amount_pending', '!=', 0),
                             ('id', 'not in', extra_ids)]
            extra_ids.extend(
                self.search(cursor, uid, search_params, context=context)
            )
        return extra_ids

    def compute_extra_lines(self, cursor, uid, factura_ids, context=None):
        '''create extra lines for each factura in factura_ids'''

        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        fpos_obj = self.pool.get('account.fiscal.position')

        if not context:
            context = {}
        context.update({'active_test': False})
        ctx = context.copy()
        ctx.update({'group_line': False})
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        for factura in factura_obj.browse(cursor, uid, factura_ids):
            # If it is a rectificadora, compute special
            # behaviour for extra lines for IET843
            proc_extras = []
            invoice_date = factura.data_final or factura.date_invoice
            extra_ids = self.get_extra_lines_from_contract_and_date(
                cursor, uid, factura.polissa_id.id, invoice_date
            )

            for extra in self.browse(cursor, uid, extra_ids,
                                     context=context):
                journal_ids = [x.id for x in extra.journal_ids]
                if (factura.tipo_rectificadora == 'N' and
                    factura.journal_id.id not in journal_ids):
                    continue
                # If it is a refund invoice, check if the rectified
                # invoice was affected by this extra, so affect
                # the new one
                if (factura.tipo_rectificadora == 'R' and
                    factura.ref.journal_id.id not in journal_ids):
                    continue
                # 6 decimals precision, but final invoiced quantity with 2
                # So check the real pending quantity when invoicing
                if float_round(extra.total_amount_pending, 2) == 0:
                    continue
                price_unit = extra.price_term
                # If is the last term, all the pending amount
                # must be invoiced.
                if factura.date_invoice >= extra.date_to:
                    price_unit = (extra.total_amount_pending /
                                        extra.quantity)

                vals = {'name': extra.name,
                        'quantity': extra.quantity,
                        'price_unit': price_unit,
                        'tipus': extra.tipus,
                        'account_id': extra.account_id.id,
                        'factura_id': factura.id,
                        'product_id': extra.product_id.id,
                        'uos_id': extra.uos_id.id,
                        'data_desde': extra.date_line_from,
                        'data_fins': extra.date_line_to
                        }
                # Map taxes using fiscal position from invoice
                extra_taxes = [x for x in extra.tax_ids]
                taxes_ids = fpos_obj.map_tax(cursor, uid,
                                             factura.fiscal_position,
                                             extra_taxes,
                                             context=context)
                vals['invoice_line_tax_id'] = [(6, 0, taxes_ids)]
                linia_id = linia_obj.create(cursor, uid, vals,
                                            context=ctx)
                # Update extra with the line just created
                extra.write({'factura_linia_ids': [(4, linia_id)],
                             'factura_ids': [(4, factura.id)]})
        return True

    def remove_lines(self, cursor, uid, ids,
                     origin='linia', context=None):
        '''remove factura lines in ids from extra'''
        factura_linia_ids = ids
        if origin == 'factura':
            linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
            #Search all the invoice lines in factura ids
            search_params = [('factura_id', 'in', ids)]
            factura_linia_ids = linia_obj.search(cursor, uid, search_params)

        sql = '''SELECT extra_id, factura_linia_id
              FROM giscegas_facturacio_extra_factura_linia_rel
              WHERE factura_linia_id in %s'''
        cursor.execute(sql, (tuple(factura_linia_ids),))
        res = cursor.fetchall()
        for value in res:
            self.write(cursor, uid, [value[0]],
                       {'factura_linia_ids': [(3, value[1])]})
        return True

    def get_next_term(self, cursor, uid, id_extraline, date=False,
                      fact_id=False, context=None):
        """
        :param cursor:          OpenERP Cursor
        :param uid:             OpenERP User ID
        :param id_extraline:    ID extra line to calc next term
        :param date:            limit date to check for other facts
        :param fact_id:         Fact ID to check the term of
        :param context:
        :return: Next term for `id_extraline` on fact[`fact_id`] before `date`
        :rtype: int
        """
        from datetime import datetime
        if isinstance(id_extraline, list):
            id_extraline = id_extraline[0]
        factures = self.read(cursor, uid, id_extraline, ['factura_ids'])
        factures = factures['factura_ids']
        if fact_id and fact_id in factures:
            factures.remove(fact_id)
        if not factures:
            return 1
        term = 1
        if date and not isinstance(date, datetime):
            date_term = datetime.strptime(date, '%Y-%m-%d')
        else:
            date_term = date
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        for fact in factura_obj.read(
                cursor, uid, factures, ['type', 'data_final']):
            if fact['type'] not in ['out_invoice', 'out_refund']:
                continue
            else:
                diff = 1 if fact['type'] == 'out_invoice' else -1
            if not date_term:
                term += diff
                continue
            data_fact = datetime.strptime(fact['data_final'], '%Y-%m-%d')
            if data_fact <= date_term:
                term += diff
        return term

    def refund(self, cursor, uid, factura_id, refund_id, context=None):
        '''manages refund invoices'''
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        factura = factura_obj.browse(cursor, uid, factura_id, context=context)
        extra_ids = self._get_extra_from_invoice(cursor, uid,
                                                 [factura.invoice_id.id],
                                                 context=context)
        for extra in self.browse(cursor, uid, extra_ids, context=context):
            for linia in extra.factura_linia_ids:
                if linia.factura_id.id == factura.id:
                    #Search in refund the corresponding line
                    search_params = [('name', '=', linia.name),
                                     ('factura_id.id', '=', refund_id),
                                     ('tipus', '=', linia.tipus),
                                     ('invoice_line_id.price_unit::text', '=',
                                         '%.6f' % linia.price_unit),
                                     ('invoice_line_id.product_id.id', '=',
                                         linia.product_id.id),
                                    ]
                    linia_ids = linia_obj.search(cursor, uid,
                                                 search_params,
                                                 context=context)
                    if linia_ids:
                        extra.write({'factura_linia_ids': [(4, linia_ids[0])],
                                     'factura_ids': [(4, refund_id)]})
        return True

    def onchange_product(self, cursor, uid, ids, product_id):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not product_id:
            res['value'].update({'uos_id': False})
            return res
        product_obj = self.pool.get('product.product')
        uos_id = product_obj.read(cursor, uid,
                                  [product_id],
                                  ['uom_po_id'])
        if uos_id:
            uos_id = uos_id[0]['uom_po_id'][0]
        else:
            uos_id = False
        res['value'].update({'uos_id': uos_id})
        return res

    def copy(self, cursor, uid, id, default=None, context=None):
        '''do not copy references to invoices'''

        if not default:
            default = {}
        default.update({'factura_linia_ids': [],
                        'factura_ids': []})
        res_id = super(FacturacioExtra,
                       self).copy(cursor,  uid, id, default=default,
                                  context=context)
        #Trigger functional fields
        self.write(cursor, uid, res_id, {})
        return res_id

    _columns = {
        'name': fields.char('Descripción', size=250),
        'polissa_id': fields.many2one('giscegas.polissa', 'Contrato',
                                      required=True, select=True),
        'product_id': fields.many2one('product.product', 'Producto',
                                      required=True),
        'uos_id': fields.many2one('product.uom', 'UoS',
                                  required=True),
        'tipus': fields.selection(_tipus_selection, 'Tipo', required=True),
        'date_from': fields.date('Desde', required=True),
        'date_to': fields.date('Hasta', required=True),
        'quantity': fields.float('Cantidad', digits=(16, 3),
                                 required=True),
        'price_unit': fields.float('Precio Unidad', required=True,
                               digits=(16, int(config['price_accuracy']))),
        'price_subtotal': fields.function(_get_price, method=True,
                            string='Subtotal', type="float",
                            multi='price',
                            digits=(16, int(config['price_accuracy'])),
                            store={'giscegas.facturacio.extra':
                           (lambda self, cr, uid, ids, c=None: ids,
                            ['quantity', 'price_unit'], 10)}),
        'term': fields.integer('Término', required=True,
                               help=u"Para pagos fraccionados de la cantidad"
                                    u" total"),
        'price_term': fields.function(_get_price, method=True,
                              string='Precio por término',
                              multi='price',
                              type='float',
                              digits=(16, int(config['price_accuracy'])),
                              store={'giscegas.facturacio.extra':
                               (lambda self, cr, uid, ids, c=None: ids,
                                ['quantity', 'price_unit', 'term'], 10)}),
        'factura_linia_ids': fields.many2many(
                                         'giscegas.facturacio.factura.linia',
                                         'giscegas_facturacio_extra_factura_linia_rel',
                                         'extra_id', 'factura_linia_id',
                                         'Líneas Asociadas',
                                         readonly=True),
        'factura_ids': fields.many2many('giscegas.facturacio.factura',
                                        'giscegas_facturacio_extra_factura_rel',
                                        'extra_id', 'factura_id',
                                        'Facturas Asociadas',
                                        readonly=True),
        'amount_invoiced': fields.function(_get_amount, method=True,
                           string="Cantidad Real facturada",
                           type="float", multi='amount',
                           digits=(16, int(config['price_accuracy'])),
                           store={'giscegas.facturacio.extra':
                           (lambda self, cr, uid, ids, c=None: ids,
                            ['factura_linia_ids', 'quantity',
                             'price_unit'], 20),
                                  'account.invoice':
                           (_get_extra_from_invoice,
                            ['state'], 20)}),
        'total_amount_invoiced': fields.function(_get_amount, method=True,
                           string="Cantidad Total facturada",
                           type="float", multi='amount',
                           digits=(16, int(config['price_accuracy'])),
                           store={'giscegas.facturacio.extra':
                           (lambda self, cr, uid, ids, c=None: ids,
                            ['factura_linia_ids', 'quantity',
                             'price_unit'], 20),
                                  'account.invoice':
                           (_get_extra_from_invoice,
                            ['state'], 20)}),
        'amount_pending': fields.function(_get_amount, method=True,
                           string="Cantidad Real pendiente",
                           type="float", multi='amount',
                           digits=(16, int(config['price_accuracy'])),
                           store={'giscegas.facturacio.extra':
                           (lambda self, cr, uid, ids, c=None: ids,
                            ['factura_linia_ids', 'quantity',
                             'price_unit'], 20),
                                  'account.invoice':
                           (_get_extra_from_invoice,
                            ['state'], 20)}),
        'total_amount_pending': fields.function(_get_amount, method=True,
                           string="Cantidad Total Pendiente",
                           type="float", multi='amount',
                           digits=(16, int(config['price_accuracy'])),
                           store={'giscegas.facturacio.extra':
                           (lambda self, cr, uid, ids, c=None: ids,
                            ['factura_linia_ids', 'quantity',
                             'price_unit'], 20),
                                  'account.invoice':
                           (_get_extra_from_invoice,
                            ['state'], 20)}),
        'account_id': fields.many2one('account.account', 'Cuenta',
                                      required=True,
                                      domain=[('type', '<>', 'view'),
                                              ('type', '<>', 'closed')]),
        'tax_ids': fields.many2many('account.tax', 'giscegas_facturacio_extra_tax_rel',
                                    'extra_id', 'tax_id', 'Impuestos'),
        'journal_ids': fields.many2many('account.journal',
                                        'giscegas_facturacio_extra_journal_rel',
                                        'extra_id', 'journal_id',
                                        'Diarios Afectados'),
        'notes': fields.text('Notas'),
        'date_line_from': fields.date('Línea de factura desde'),
        'date_line_to': fields.date('Línea de factura hasta'),
    }

    _defaults = {
        'term': lambda *a: 1,
        'quantity': lambda *a: 1,
        'account_id': _get_default_account,
        'tax_ids': _get_default_tax,
        'product_id': _get_extra_default_product,
        'uos_id': _get_extra_default_uos,
        'tipus': _get_default_tipus,
    }

FacturacioExtra()


class GiscegasFacturacioFacturaExtra(osv.osv):

    _name = 'giscegas.facturacio.factura'
    _inherit = 'giscegas.facturacio.factura'

    def get_extra_factura_lines(self, cursor, uid, ids, context=None):
        '''returns lines associated with extra in invoice'''

        query = '''
            SELECT distinct factura_linia_id
            FROM giscegas_facturacio_extra_factura_linia_rel rel
            INNER JOIN giscegas_facturacio_factura_linia fl
            ON rel.factura_linia_id = fl.id
            WHERE fl.factura_id in %s
            '''
        cursor.execute(query, (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]            

GiscegasFacturacioFacturaExtra()
