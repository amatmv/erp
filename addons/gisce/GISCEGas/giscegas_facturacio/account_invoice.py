# -*- coding: utf-8 -*-
from osv import osv


class AccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def get_model_list(self, cursor, uid, context=None):
        """
        Inherited method from account_invoice that returns the name of the
        model on a list
        """
        return super(AccountInvoice, self).get_model_list(
            cursor, uid, context=context
        ) + ['giscegas.facturacio.factura']


AccountInvoice()
