# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from .utils import tenim_mes_sequences, get_next


class GiscegasWizardTancamentAny(osv.osv_memory):
    _name = 'giscegas.wizard.tancament.any'

    def _has_next_sequence(self, cursor, uid, ids, field_name, arg,
                           context=None):
        res = dict.fromkeys(ids, False)
        for wiz in self.browse(cursor, uid, ids):
            sequences = [x.id for x in wiz.sequences]
            seq_id = wiz.sequence and wiz.sequence.id or False
            res[wiz.id] = tenim_mes_sequences(seq_id, sequences)
        return res

    def _independent(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        for wiz in self.browse(cursor, uid, ids):
            if wiz.sequence and wiz.sequence.fiscal_ids:
                res[wiz.id] = 1
        return res

    def _independent_inv(self, cursor, uid, ids, name, value, args, context=None):
        if not context:
            context = {}
        seq_obj = self.pool.get('ir.sequence')
        seq_fy = self.pool.get('account.sequence.fiscalyear')
        wiz = self.browse(cursor, uid, ids)
        if wiz.sequence and value:
            for fy in wiz.sequence.fiscal_ids:
                if fy.id == wiz.independent_fiscal_year_id.id:
                    raise osv.except_osv(
                        'Error',
                        _(u'Esta secuencia ya tiene año fiscal asignado')
                    )
            name = '%s - %s' % (wiz.sequence.name, wiz.independent_fiscal_year_id.code)
            old_seq = seq_obj.copy(cursor, uid, wiz.sequence.id, {'name': name})
            seq_fy.create(cursor, uid, {
                'sequence_id': old_seq,
                'sequence_main_id': wiz.sequence.id,
                'fiscalyear_id': wiz.independent_fiscal_year_id.id
            })
        return True

    def go_next_sequence(self, cursor, uid, ids, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        wiz = self.browse(cursor, uid, ids[0])
        sequences = [x.id for x in wiz.sequences]
        seq_id = wiz.sequence and wiz.sequence.id or False
        next_seq = get_next(seq_id, sequences)
        wiz.write({'sequence': next_seq})
        return True

    def create_fiscal_year(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context=context)
        fyear_obj = self.pool.get('account.fiscalyear')
        fy_id = fyear_obj.create(cursor, uid, {
            'name': wiz.fiscal_year,
            'code': wiz.fiscal_year,
            'date_start': '%s-01-01' % wiz.fiscal_year,
            'date_stop': '%s-12-31' % wiz.fiscal_year
        })
        fyear_obj.create_period(cursor, uid, [fy_id])
        wiz.write({'with_fiscal_year': 'use', 'fiscal_year_id': fy_id})

    def next_update_sequence(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context=context)
        wiz.go_next_sequence()

    _columns = {
        'with_fiscal_year': fields.selection(
            [('new', 'Crear un nou any fiscal'),
             ('use', 'Utilitzar un existent')], 'Año fiscal',
            required=True
        ),
        'fiscal_year': fields.char('Año fiscal', size=4),
        'fiscal_year_id': fields.many2one('account.fiscalyear', 'Año fiscal'),
        'update_invoice_sequences': fields.boolean(
            'Actualizar numeración secuencias'
        ),
        'sequences': fields.many2many('ir.sequence', 'wiz_id', 'seq_id',
                                      'Sequences'),
        'sequence': fields.many2one('ir.sequence', 'Sequence'),
        'prefix': fields.related('sequence', 'prefix', string='Prefix', type='char',
                                 size=64),
        'suffix': fields.related('sequence', 'suffix', string='Suffix', type='char',
                                 size=64),
        'number_next': fields.related('sequence', 'number_next', string='Next Number',
                                      type='integer'),
        'padding': fields.related('sequence', 'padding', string='Number padding',
                                  type='integer'),
        'has_next_sequence': fields.function(_has_next_sequence, type='boolean',
                                         method=True),
        'independent': fields.function(
            _independent, fnct_inv=_independent_inv, type='boolean',
            method=True, string='Mantener la numeración antigua independiente'
        ),
        'independent_fiscal_year_id': fields.many2one(
            'account.fiscalyear', 'Año fiscal anterior'
        ),
        'state': fields.char('Estado', size=16)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'sequence': lambda *a: False,
    }

GiscegasWizardTancamentAny()