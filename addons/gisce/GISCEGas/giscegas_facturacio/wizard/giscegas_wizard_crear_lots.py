# -*- encoding: utf-8 -*-
import wizard
import pooler
import time

def _init(self, cursor, uid, data, context=None):
    return {'year': int(time.strftime('%Y'))}

_init_form = """<?xml version="1.0"?>
<form string="Creació de lots de facturació" col="4">
  <field name="year" required="1"/>
</form>"""

_init_fields = {
  'year': {'string': 'Any', 'type': 'integer'},
}

def _crear(self, cursor, uid, data, context=None):
    lot_obj = pooler.get_pool(cursor.dbname).get('giscegas.facturacio.lot')
    lot_obj.crear_lots_mensuals(cursor, uid, data['form']['year'], context)
    return {}

_crear_form = """<?xml version="1.0"?>
<form string="Creació de lots de facturació" col="4">
  <image name="gtk-ok" />
  <label string="S'han creat correctament els lots de facturació" colspan="3" />
</form>"""

def _cleanup(self, cursor, uid, data, context=None):
    del data['form']
    return {}

class wizard_crear_lots(wizard.interface):
    states = {
        'init': {
            'actions': [_init],
            'result': {
                'type':'form',
                'arch': _init_form,
                'fields': _init_fields,
                'state':[('crear', 'Crear', 'gtk-go-forward')]}
        },
        'crear': {
            'actions': [_crear],
            'result': {
                'type': 'form',
                'arch': _crear_form,
                'fields': {},
                'state': [('cleanup', "Tancar", "gtk-quit")]
            }
        },
        'cleanup': {
            'actions': [_cleanup],
            'result': {'type': 'state', 'state': 'end'}
        },
    }
wizard_crear_lots('giscegas.facturacio.lot.crear')
