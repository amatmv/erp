from __future__ import absolute_import
from utils import tenim_mes_sequences, get_next

with description("Tancament d'any"):
    with it("Hem de trobar si tenim més sequencies"):

        assert tenim_mes_sequences(False, False) is False
        assert tenim_mes_sequences(1, False) is False
        assert tenim_mes_sequences(False, []) is False
        assert tenim_mes_sequences(False, [1]) is True
        assert tenim_mes_sequences(1, [2]) is True
        assert tenim_mes_sequences(1, [1]) is False
        assert tenim_mes_sequences(2, [1, 2, 3]) is True
        assert tenim_mes_sequences(3, [1, 2, 3]) is False

    with it("Hem de trobar la següent sequencia"):

        assert get_next(1, []) is False
        assert get_next(1, [1]) is False
        assert get_next(1, [1, 2, 3, 4]) == 2
        assert get_next(False, [1, 2, 3]) == 1