# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    Copyright (C) 2011 GISCE Enginyeria (http://gisce.net). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

"""Wizard per abonament de factures.

Aquest wizard permet anul.lar i rectificar factures.
"""

import time
from osv import fields, osv
from tools.translate import _

class GiscegasWizardRanasFactures(osv.osv_memory):
    """Factures generades pel Wizard
    """
    _name = "giscegas.wizard.ranas.factures"

    _columns = {
        'factura': fields.many2one('giscegas.facturacio.factura', 'Factura'),
        'rana_id': fields.many2one('giscegas.wizard.ranas', 'Wizard'),
    }
GiscegasWizardRanasFactures()

class GiscegasWizardRanas(osv.osv_memory):
    """Wizard class
    """
    _name = "giscegas.wizard.ranas"
    _tipus_rectificadora = [('anulladora', 'Anuladora'),
                            ('rectificadora', 'Anuladora con rectificadora')]


    def create(self, cr, user, vals, context=None):
        factura_ids = context.get('active_ids', False)
        if not factura_ids:
            return False

        factura_obj = self.pool.get('giscegas.facturacio.factura')
        refund = factura_obj.read(
            cr, user, factura_ids[0], ['refund_by_id']
        )['refund_by_id']

        if refund:
            raise osv.except_osv(_('Error!'),
                                 _(u"Factura ya rectificada anteriormente, "
                                   u"no se puede volver a rectificar!"))
        return super(
            GiscegasWizardRanas, self
        ).create(cr, user, vals, context=context)

    def action_confirmar(self, cursor, uid, ids, context):
        """Acció per escriure l'estat de confirmació
        """
        self.write(cursor, uid, ids, {'state': 'conf'})
        return True


    def action_confirmar_rect(self, cursor, uid, ids, context):
        """Acció per escriure l'estat de confirmació de la rectificadora
        """
        self.write(cursor, uid, ids, {'state': 'confrect'})
        return True

    def action_confirmar_rect_subs(self, cursor, uid, ids, context):
        """Acció per escrire l'estat de confirmació de rectificadora
        """
        self.write(cursor, uid, ids, {'state': 'confrectsubs'})
        return True


    def _action(self, cursor, uid, ids, action='anullar', context=None):
        """Acció per anul·lar o rectificar
        """
        if not context:
            context = {}
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]

        wiz = self.browse(cursor, uid, ids, context)
        context.update({'data_factura': wiz.data})

        factura_ids = context.get('active_ids', False)
        if not factura_ids:
            return False
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        factura_mem = self.pool.get('giscegas.wizard.ranas.factures')
        method = getattr(factura_obj, action)
        fanullada_ids = method(cursor, uid, factura_ids, context=context)
        for fanullada in fanullada_ids:
            factura_mem.create(cursor, uid, {'factura': fanullada,
                                             'rana_id': ids})
        self.write(cursor, uid, [ids], {'state': 'end'})
        return fanullada_ids


    def action_anullar(self, cursor, uid, ids, context):
        """Acció per anul·lar les factures
        """
        return self.pool.get('giscegas.wizard.ranas')._action(cursor, uid, ids,
                                                     'anullar', context)

    def action_rectificar(self, cursor, uid, ids, context):
        """Acció per rectificar les factures
        """
        return self.pool.get('giscegas.wizard.ranas')._action(cursor, uid, ids,
                                                     'rectificar', context)

    def action_rectificar_substitucio(self, cursor, uid, ids, context):
        """Acció per rectificar les factures
        """
        return self.pool.get('giscegas.wizard.ranas')._action(
            cursor, uid, ids, 'rectificar_substitucio', context
        )

    def action_end(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem les factures creades per repassar-les
        """
        wiz = self.browse(cursor, uid, ids[0])
        refund_ids = [a.factura.id for a in wiz.fact_generades]
        return {
            'domain': "[('id','in', %s)]" % str(refund_ids),
            'name': _('Factures generades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.facturacio.factura',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'data': fields.date('Fecha factura', required=True),
        'fact_generades': fields.one2many('giscegas.wizard.ranas.factures', 'rana_id',
                                          'Facturas generadas'),
        'state': fields.selection(
            [
                ('init', 'Init'), ('end', 'Final'), ('cancel', 'Cancel'),
                ('conf', 'Confirmar'), ('confrect', 'Confirmar rectificació'),
                ('confrectsubs', 'Confirmar rectificació per substitucio')
            ], 'Estat'
        ),
    }
    _defaults = {
        'tipus_rectificadora': lambda *a: 'anulladora',
        'data': lambda *a: time.strftime('%Y-%m-%d'),
        'state': lambda *a: 'init',
    }

GiscegasWizardRanas()
