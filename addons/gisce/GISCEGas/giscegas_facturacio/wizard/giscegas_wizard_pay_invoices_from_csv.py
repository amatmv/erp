# -*- coding: utf-8 -*-

from osv import fields, osv
import base64
import csv
from datetime import datetime
import time
from tools.translate import _
import pooler
import traceback
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO


from account_invoice_base.account_invoice import amount_grouped

class GiscegasWizardPayInvoicesFromCSV(osv.osv_memory):
    """ This class is a wizard to pay multiple invoices generated or received"""
    _name = 'giscegas.wizard.pay.invoices.from.csv'

    def _default_info(self, cursor, uid, context=None):
        # Return a description for correct use of wizard
        informacio = _(
            u"Se van a cargar facturas desde un fichero seleccionado.\n "
            u"El formato del fichero debe ser CSV con delimitador ';', UTF-8, "
            u"el fichero tiene que estar en formato de dos columnas, la "
            u"primera con el número de la factura y la segunda con el importe"
        )
        return informacio

    def _default_date(self, *args):
        return False

    def _default_file(self, *args):
        return False

    def _default_mode(self, *args):
        return False

    def _get_journal(self, cursor, uid, context=None):
        """Funció per obtenir el diari de pagament
        """
        if not context:
            context = {}
        journal_obj = self.pool.get('account.journal')
        search_params = [
            ('code', '=', 'CAJA'),
        ]
        journal_id = journal_obj.search(cursor, uid, search_params)
        if len(journal_id):
            return journal_id[0]
        return False

    def onchange_date(self, cursor, uid, ids, data_f, context=None):
        res = {'value': {}, 'warning': {}, 'domain': {}}
        if not data_f:
            return res

        if datetime.strptime(data_f, '%Y-%m-%d') <= datetime.strptime(time.strftime('%Y-%m-%d'), '%Y-%m-%d'):
            res['value'].update({'fact_date': data_f})
        else:
            res['value'].update({'fact_date': False})
            res['warning'].update({
                'title': _(u'Error fecha no válida'),
                'message': _(u"La fecha debe ser anterior a la fecha actual.")
            })

        return res

    def onchange_tipo_facturas(self, cursor, uid, ids, f_mode, context=None):
        if f_mode != 'reb':
            self.write(cursor, uid, ids, {'partner_id': False}, context)

    def go_conf_state(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'conf'}, context)

    def go_init_state(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'init'}, context)

    def seguent(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)
        if not wiz.fact_mode or not wiz.journal_id:
            raise osv.except_osv(
                _(u'Error!'), _(
                    u'Formulario inválido, Tipo de facturación y diario '
                    u'requeridos'
                )
            )
        else:
            self.write(cursor, uid, ids, {'state': 'load'}, context)

    def anterior(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'conf'}, context)

    def importar(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)

        if wiz.fact_mode == 'reb' and not wiz.partner_id:
            raise osv.except_osv(
                _(u'Error!'), _(
                    u'Formulario inválido. El campo "partner" debe ser rellenado'
                )
            )
        if not wiz.fact_date or not wiz.file_fact:
            raise osv.except_osv(
                _(u'Error!'), _(
                    u'Formulario inválido. Los campos fecha y fichero deben '
                    u'ser rellenados'
                )
            )
        self.process_csv_file(cursor, uid, ids, wiz.file_fact, context)

    def process_csv_file(self, cursor, uid, ids, csv_file=None, context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context)
        wizard_obj = self.pool.get("giscegas.facturacio.pay.invoice")
        if wiz.file_fact:
            ctx = context.copy()
            ctx.update({'data_pagament': wiz.fact_date})
            period_id = wizard_obj._get_periode(cursor, uid, context=ctx)

            all_invoices = []
            correct_invoices = []
            incorrect_invoices = []
            fact_obj = self.pool.get('giscegas.facturacio.factura')
            fitxer = wiz.file_fact
            filename = wiz.filename
            txt = base64.decodestring(str(fitxer))
            csv_file = StringIO.StringIO(txt)
            reader = csv.reader(csv_file, delimiter=';')
            for row in reader:
                try:
                    db = pooler.get_db(cursor.dbname)
                    tmp_cursor = db.cursor()
                    fact_number = row[0].strip()
                    if fact_number:
                        all_invoices.append(fact_number)
                        s_params = [
                            ('state', '=', 'open')
                        ]
                        if wiz.fact_mode == 'reb':
                            s_params += [
                                ('origin', '=', fact_number),
                                ('partner_id', '=', wiz.partner_id.id)
                            ]
                            fact_ids = fact_obj.search(tmp_cursor, uid, s_params)
                        else:
                            s_params += [
                                ('number', '=', fact_number)
                            ]
                            fact_ids = fact_obj.search(tmp_cursor, uid, s_params)
                        if len(fact_ids) == 1:
                            context = {'active_id': fact_ids[0],
                                       'active_ids': fact_ids}
                            factura = fact_obj.browse(
                                cursor, uid, fact_ids[0])
                            try:
                                amount_file = round(float(row[1]), 2)
                            except ValueError as e:
                                amount_file = round(
                                    float(row[1].replace(',', '.')), 2
                                )
                            amount_total = amount_grouped(factura)
                            if abs(amount_file) == abs(amount_total):
                                wizard_id = wizard_obj.create(tmp_cursor, uid, {
                                    'journal_id': wiz.journal_id.id,
                                    'date': wiz.fact_date,
                                    'name': 'Pago por CSV {file}'.format(file=filename),
                                    'period_id': period_id
                                }, context)
                                wizard = wizard_obj.browse(
                                    tmp_cursor, uid, wizard_id, context
                                )
                                res = wizard.action_wo_check(context)

                                fact_state = fact_obj.read(
                                    tmp_cursor, uid, fact_ids, ['state'],
                                    context=context
                                )
                                if fact_state[0]['state'] == u'paid':
                                    correct_invoices.append(fact_number)
                                    tmp_cursor.commit()
                                else:
                                    incorrect_invoices.append(fact_number)
                            else:
                                incorrect_invoices.append(fact_number)
                        else:
                            incorrect_invoices.append(fact_number)
                except Exception:
                    traceback.print_exc()
                    incorrect_invoices.append(fact_number)
                    tmp_cursor.rollback()
                finally:
                    tmp_cursor.close()
            res = self.generar_estadistiques(
                correct_invoices, incorrect_invoices, all_invoices
            )
            vals = {
                'state': 'end',
                'info': _(u"La importación de facturas ha finalizado "
                          u"correctamente.\n\n%s") % str(res)
            }
            self.write(cursor, uid, ids, vals, context)

    def generar_estadistiques(self, correct, not_correct, all_facts):
        res = u""
        res += _(u"Total fichero: %s\n") % str(len(all_facts))
        res += _(u"Total pagadas: %s\n") % str(len(correct))
        res += _(u"Total no pagadas: %s\n\n") % str(len(not_correct))
        if len(not_correct) > 0:
            res += _(u"Listado no pagadas: \n\n")
            for elem in not_correct:
                res += _(u"\t- %s\n") % str(elem)
        return res

    _columns = {
            'state': fields.selection([('init', 'Init'), ('end', 'Final'),
                                       ('conf', 'Conf'),
                                       ('load', 'Load')],
                                      'Estado'),
            'info': fields.text('Información', readonly=True),
            'file_fact': fields.binary(
                'Fichero', states={
                    'load': [('requiered', True)]
                }
            ),
            'filename': fields.char('Nom', size=1024),
            'fact_date': fields.date(
                'Fecha de cobro', states={
                    'load': [('requiered', True)]}
            ),
            'fact_mode': fields.selection(
                [('gen', 'Generadas'), ('reb', 'Rebudes')], 'Tipo de facturas',
                states={
                    'conf': [('requiered', True)],
                    'load': [('requiered', True)]
                }
            ),
            'journal_id': fields.many2one(
                'account.journal', 'Diario', domain=[('type', '=', 'cash')],
                states={
                    'conf': [('requiered', True)],
                    'load': [('requiered', True)]
                }
            ),
            'partner_id': fields.many2one('res.partner', 'Partner', select=1),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'fact_date': _default_date,
        'file_fact': _default_file,
        'fact_mode': _default_mode,
        'journal_id': _get_journal,
        'filename': lambda *a: False,
    }

GiscegasWizardPayInvoicesFromCSV()
