# -*- coding: utf-8 -*-
import time
from osv import osv
from osv import fields
from tools.translate import _
from tools import config


class GiscegasWizardUnpay(osv.osv_memory):
    '''
    Wizard per marcar com a no pagada una factura fent servir la comptabilitat
    '''
    _name = 'giscegas.wizard.unpay'
    
    def unpay(self, cursor, uid, ids, context=None):
        
        if not context:
            context = {}
        model_ids = context.get('active_ids', [])
        wiz = self.browse(cursor, uid, ids[0], context)

        if context.get('model', False):
            # Fem els diferents unpays per cada factura.
            obj = self.pool.get(context['model'])
            period_obj = self.pool.get('account.period')
            ctx = context.copy()
            ctx['date_p'] = wiz.date
            period_id = period_obj.find(cursor, uid, wiz.date)[0]
            obj.unpay(cursor, uid, model_ids, wiz.amount,
                      wiz.pay_account_id.id, period_id, wiz.pay_journal_id.id,
                      context=ctx, name=wiz.name)
        else:
            raise osv.except_osv(_('Error'),
                                 _('No se puede deshacer el pago sin un modelo!'))
        return {}

    def _default_amount(self, cursor, uid, context=None):
        if not context:
            context = {}
        if 'active_ids' in context and 'model' in context:
            obj = self.pool.get(context['model'])
            invoice_id = context['active_ids'][0]
            invoice = obj.browse(cursor, uid, invoice_id, context=context)
            if invoice.group_move_id:
                move_lines = invoice.group_move_id.line_id

                signal = {
                    'in_invoice': 1,
                    'out_invoice': 1,
                    'in_refund': -1,
                    'out_refund': -1
                }
                amount = sum(
                    l.invoice.amount_total * signal[l.invoice.type]
                        for l in move_lines
                            if l.invoice
                )
            else:
                amount = invoice.amount_total
            return amount
        else:
            return 0

    def _default_name(self, cursor, uid, context=None):
        if not context:
            context = {}
        if 'active_ids' in context and 'model' in context:
            obj = self.pool.get(context['model'])
            invoice_id = context['active_ids'][0]
            invoice = obj.browse(cursor, uid, invoice_id, context=context)
            if invoice.group_move_id:
                name = _(u"Devolución recibo agrupado {0}").format(
                    invoice.group_move_id.ref
                )
            else:
                name = _(u'Devolución recibo factura %s') % invoice.number
            return name
        else:
            return ''

    def _pay_journal_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        if 'active_ids' in context and 'model' in context:
            obj = self.pool.get(context['model'])
            invoice_id = context['active_ids'][0]
            invoice = obj.browse(cursor, uid, invoice_id, context=context)
            return invoice.payment_ids[-1].journal_id.id
        return False


    _columns = {
        'name': fields.char(
            'Descripción',
            50,
        ),
        'date': fields.date(
            'Fecha'
        ),
        'amount': fields.float(
            'Cantidad',
            digits=(16, config['price_accuracy']),
            required=True
        ),
        'pay_account_id': fields.many2one(
            'account.account',
            'Cuenta contable',
            required=True
        ),
        'pay_journal_id': fields.many2one(
            'account.journal',
            'Diario',
            required=True
        )
    }

    _defaults = {
        'date': lambda *a: time.strftime('%Y-%m-%d'),
        'amount': _default_amount,
        'name': _default_name,
        'pay_journal_id': _pay_journal_id
    }

GiscegasWizardUnpay()
