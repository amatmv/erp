# -*- encoding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from datetime import datetime, timedelta


class GiscegasWizardMarcarFacturat(osv.osv_memory):

    _name = 'giscegas.wizard.marcar.facturat'

    def action_marcar_facturat(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        clot_obj = self.pool.get("giscegas.facturacio.contracte_lot")
        clot_obj.facturar_incident(
            cursor, uid, context.get('active_ids', []), context=context
        )
        return {}

    _columns = {
        'info': fields.text(),
    }

    _defaults = {
        'info': lambda cursor, uid, ids, context={}: _(
            u'Se van a pasar a "Facturado" los contratos seleccionados que se '
            u'encuentren en estado "Facturado con incidencias" y se marcaran '
            u'como "Incidencia Revisada".\n\n. Se han seleccionado {0} contratos'
        ).format(len(context.get('active_ids', []))),
    }

GiscegasWizardMarcarFacturat()
