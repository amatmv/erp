# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscegasWizardAfegirPolissesLot(osv.osv_memory):

    def action_assignar_lot_facturacio(self, cursor, uid, ids, context=None):
        polissa_obj = self.pool.get('giscegas.polissa')
        polissa_ids = context['active_ids']
        self_fields = ['lot_facturacio']
        self_vals = self.read(
            cursor, uid, ids, self_fields, context=context
        )[0]
        params = {'lot_facturacio': self_vals['lot_facturacio']}
        polissa_obj.write(cursor, uid, polissa_ids, params, context=context)

        return {}

    _name = 'giscegas.wizard.afegir.polisses.lot'

    _columns = {
        'lot_facturacio': fields.many2one(
            'giscegas.facturacio.lot', 'Lote de facturación', required=True
        )
    }

    _defaults = {

    }


GiscegasWizardAfegirPolissesLot()
