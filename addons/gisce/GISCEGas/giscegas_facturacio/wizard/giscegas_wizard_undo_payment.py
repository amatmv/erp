# -*- coding: utf-8 -*-

from osv import osv
from osv import fields
from tools.translate import _

class GiscegasWizardUndoPayment(osv.osv_memory):
    '''
    Wizard per desfer els pagos d'una factura
    '''
    _name = 'giscegas.wizard.undo.payment'

    
    def undo_payments(self, cursor, uid, ids, context=None):
        
        if not context:
            context = {}

        model_ids = context.get('active_ids', [])

        if context.get('model',False):
            self.pool.get(context['model']).undo_payment(cursor, 
                                                uid, model_ids, context)    
        else:
            raise osv.except_osv(
                _('Error'), _('No se puede deshacer el pago sin un modelo!')
            )

        return {}

GiscegasWizardUndoPayment()
