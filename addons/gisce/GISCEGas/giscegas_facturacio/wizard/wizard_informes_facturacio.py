# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.translate import _
import pandas as pd
from StringIO import StringIO
from base64 import b64encode

INFORMES_GAS_DESC = {}

MULTI_SEQUENCE_GAS = {
    'export_factures_desglossat': True,
}

INFORMES_EXPORTABLES = {}


class GiscegasWizardInformesFacturacio(osv.osv_memory):

    _name = "giscegas.wizard.informes.facturacio"

    _get_facturacio = [
        ('1', _('Mensual')),
        ('2', _('Bimestral')),
        ('12', _('Anual')),
        ('0', _('Tot'))
    ]

    def _get_informe(self, cursor, uid, context=None):
        _opcions = []

        return _opcions

    def get_info_desc(self, cursor, informe, context=None):
        """ Retorna la informació de l'informe """
        return INFORMES_GAS_DESC.get(informe, False)

    def get_multi_sequence(self, informe):
        return MULTI_SEQUENCE_GAS.get(informe, False)

    def default_get(self, cursor, uid, fields, context=None):
        """Overridden to provide specific defaults depending on the context
           parameters.

           :param dict context: several context values will modify the behavior
                                of the wizard, cfr. the class description.
        """
        if context is None:
            context = {}

        result = super(GiscegasWizardInformesFacturacio, self).default_get(
            cursor, uid, fields, context=context
        )
        if result.get('informe'):
            report_multi_sequence = self.get_multi_sequence(result['informe'])
            result.update({'report_multi_sequence': report_multi_sequence})
        return result

    def _get_sequence(self, cursor, uid, ids, context=None):
        '''retorna una llista amb totes les seqs associades
        a un diari de facturacio'''

        journal_obj = self.pool.get('account.journal')

        sequencies = []

        search_params = [
            ('invoice_sequence_id', '!=', None),
            # Eliminem els diaris d'energia
            ('code', 'not like', '%ENERGIA%'),
            ('code', 'not like', '%CONCEPTES%')
        ]
        cursor.execute(*(
            journal_obj.q(cursor, uid).select(
                ['invoice_sequence_id.code', 'invoice_sequence_id.name']
            ).where(search_params)
        ))
        for journal in cursor.dictfetchall():
            repetit = [code for (code, _) in sequencies]
            if not journal['invoice_sequence_id.code'] in repetit:
                sequencies.append((
                    journal['invoice_sequence_id.code'],
                    journal['invoice_sequence_id.name']
                ))

        return sequencies

    def exportar_resum_factures(self, cursor, uid, ids, context=None):
        """ Exportació com a CSV """
        wizard = self.browse(cursor, uid, ids[0])

        inv_obj = self.pool.get('account.invoice')

        __, fact_txt = filter(
            lambda (codi, __): codi == wizard.facturacio, wizard._get_facturacio
        )[0]

        multi_seq = [x.invoice_sequence_id.code for x in wizard.multi_sequence]

        if wizard.include_draft:
            states = ['cancel']
        else:
            states = ['draft', 'cancel']

        search_params = [
            ('state', 'not in', states),
            ('date_invoice', '>=', wizard.data_inici),
            ('date_invoice', '<=', wizard.data_final),
            ('journal_id.invoice_sequence_id.code', 'in', multi_seq)
        ]

        inv_ids = inv_obj.search(cursor, uid, search_params)
        fact_ids = tuple(
            self.pool.get('giscegas.facturacio.factura').search(cursor, uid, [
                ('invoice_id', 'in', inv_ids)
            ])
        )
        _sqlfile = INFORMES_EXPORTABLES[wizard.informe]

        res = columns = []
        if fact_ids:
            query = open(_sqlfile).read()
            cursor.execute(query, (tuple(fact_ids),))
            res = cursor.dictfetchall()
            columns = [d.name for d in cursor.description]
        df = pd.DataFrame(res, columns=columns)
        if df.empty:
            raise osv.except_osv(
                'Error', _(u'No se han encontrado resultados.')
            )

        gen_file = StringIO()

        df.to_csv(gen_file, header=True, sep=';', encoding='utf-8-sig')

        if res:
            info = _(u"""
            Fichero CSV generado.
            Parámetros:
             * Fecha entre {f_ini} y {f_fin}
             * Facturación: {fact}
             * Secuencia/s: {sequence}
             * Incluir borradores: {draft}
            """).format(
                f_ini=wizard.data_inici, f_fin=wizard.data_final,
                fact=fact_txt, draft=(wizard.include_draft and 'Si' or 'No'),
                sequence=(
                    wizard.sequence or
                    u'\n\t\t'.join([seq.name for seq in wizard.multi_sequence])
                )
            )
        else:
            info = _(u"Error generando CSV")

        return b64encode(gen_file.getvalue()), info

    def onchange_informe(self, cursor, uid, ids, informe, context=None):
        result = {'value': {}}
        result['value']['desc'] = self.get_info_desc(cursor, informe, context)
        result['value']['report_multi_sequence'] = self.get_multi_sequence(
            informe
        )
        if informe in INFORMES_EXPORTABLES:
            result['value']['exportable'] = True
        else:
            result['value']['exportable'] = False

        return result

    def exportar(self, cursor, uid, ids, context=None):
        """
        Mètode per a escriure el resultat del resum en el cas que s'hagi
        d'importar aquest i retorna un valor booleà per a si s'ha generat o no.

        Els valors a escriure al assistent són els camps:
        {
            'file': fitxer en b64,
            'estat': 'done',
            'name': '<nom_resum>.<format>'
        }
        """
        if context is None:
            context = {}

        vals = {}

        if vals:
            self.write(cursor, uid, ids[0], vals)

        return bool(vals)

    _columns = {
        'data_inici': fields.date('Desde', required=True),
        'data_final': fields.date('Hasta', required=True),
        'facturacio': fields.selection(_get_facturacio, 'Tipo'),
        'include_draft': fields.boolean(
            'Incluir borradores', help=_(
                u"Marcar esta casilla para incluir las facturas en estado de "
                u"borrador en el resumen elegido"
            )
        ),
        'informe': fields.selection(_get_informe, 'Informe', required=True),
        'sequence': fields.selection(_get_sequence, 'Serie'),
        'multi_sequence': fields.many2many(
            'account.journal', 'account_journal', 'id', 'id', 'Secuencias',
            domain=[
                ('invoice_sequence_id', '!=', False),
                ('name', 'not ilike', '%energia%')
            ]
        ),
        'report_multi_sequence': fields.boolean('Informe multi secuencia'),
        'estat': fields.char('Estado', size=16),
        'info': fields.text('Info'),
        'name': fields.char('Fichero generado', size=64),
        'file': fields.binary('Fichero generado'),
        'desc': fields.text('Descripción', readonly=True),
        'exportable': fields.boolean('Informe exportable')
    }

    _defaults = {
        'exportable': lambda *a: False,
        'include_draft': lambda *a: False,
        'estat': lambda *x: 'init',
        'info': lambda *x: 'Fitxer CSV generado',
        'name': lambda *x: 'ResumFactures.csv',
        'report_multi_sequence': lambda *x: False,
    }


GiscegasWizardInformesFacturacio()
