# -*- coding: utf-8 -*-

from osv import fields, osv
from osv.orm import except_orm
from osv.osv import TransactionExecute, except_osv
from tools.translate import _
import pooler


class GiscegasWizardMoveInvoicesLot(osv.osv_memory):
    _name = 'giscegas.wizard.move.invoices.lot'

    def moure(self, cursor, uid, ids, context=None):
        if not context or 'active_ids' not in context:
            return False

        cont_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        lot_obj = self.pool.get('giscegas.facturacio.lot')
        pol_obj = self.pool.get('giscegas.polissa')

        wiz = self.browse(cursor, uid, ids[0], context)

        ids_lots_ori = context['active_ids']
        id_lot_desti = wiz.lot_desti.id

        estats_a_moure = []
        if wiz.moure_oberts:
            estats_a_moure.append('obert')
        if wiz.moure_esborrany:
            estats_a_moure.append('esborrany')
        if wiz.moure_a_punt:
            estats_a_moure.append('facturar')

        ids_cont_lot = cont_obj.search(cursor, uid, [
            ('lot_id', 'in', ids_lots_ori),
            ('state', 'in', estats_a_moure)
        ])
        ids_poli = cont_obj.read(
            cursor, uid, ids_cont_lot, ['polissa_id']
        )

        info = []
        errors = []
        pol = TransactionExecute(cursor.dbname, uid, 'giscegas.polissa')
        for clot in ids_poli:
            try:
                pol.moure_al_lot(
                    clot['id'], clot['polissa_id'][0], id_lot_desti
                )
                info.append(clot['polissa_id'][1])
            except (except_osv, except_orm) as e:
                errors.append(
                    _(u'Contrato ') + clot['polissa_id'][1] + ': ' + e.value
                )
        lot_obj.update_progress(cursor, uid, ids_lots_ori + [id_lot_desti])

        if info:
            text_info = ', '.join(info)
        else:
            text_info = _(u'Ningún contrato ha sido movido')

        if errors:
            text_errors = '\n'.join(errors)
        else:
            text_errors = False

        wiz.write({
            'state': 'end',
            'info': text_info,
            'errors': text_errors,
        })

        return True

    def _default_lot_origen(self, cr, uid, context=None):
        if not context or 'active_ids' not in context:
            return ''

        lot_obj = self.pool.get('giscegas.facturacio.lot')

        lots = lot_obj.read(cr, uid, context['active_ids'], ['name'])

        return ',\n'.join([lot['name'] for lot in lots])

    _columns = {
        'state': fields.selection([
            ('init', 'Inicial'),
            ('end', 'Final')
        ], 'Estado'),
        'lots_origen': fields.text('Lotes Origen', readonly=True),
        'lot_desti': fields.many2one('giscegas.facturacio.lot', 'Lote Destino',
                                     required=True,
                                     domain=[('state', '!=', 'tancat')]
                                     ),
        'moure_oberts': fields.boolean('Mover abiertas'),
        'moure_esborrany': fields.boolean('Mover borrador'),
        'moure_a_punt': fields.boolean('Mover a punto para facturar'),
        'info': fields.text(readonly=True),
        'errors': fields.text(readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'lots_origen': _default_lot_origen,
        'moure_oberts': lambda *a: True,
        'moure_esborrany': lambda *a: True,
        'moure_a_punt': lambda *a: True,
    }


GiscegasWizardMoveInvoicesLot()
