# -*- coding: utf-8 -*-
"""Facturació base
"""
# pylint: disable-msg=E1101,W0223
from __future__ import absolute_import

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import StringIO
import csv
import base64

from osv import osv, fields
from osv.expression import OOQuery
from sql import For

from tools.translate import _
from tools import config, cache, float_round
import netsvc
import pooler
import calendar

from libfacturacioatr.gas import *
from libfacturacioatr.tarifes import insert_data_traspas
from .defs import *
from base_extended.base_extended import MultiprocessBackground, NoDependency
from oorq.decorators import job, create_jobs_group
from autoworker import AutoWorker

try:
    import multiprocessing
    MP_ENABLED = True
except ImportError:
    MP_ENABLED = False

#TODO Gas canviar tarifes conforme les definides en la libfacturacioatr
TARIFES = {
    '3.1': Tarifa31,
    '3.2': Tarifa32,
    '3.3': Tarifa33,
    '3.4': Tarifa34,
}

REFUND_RECTIFICATIVE_INVOICE_TYPES = ['A', 'B', 'BRA']

RECTIFYING_RECTIFICATIVE_INVOICE_TYPES = ['R', 'RA']

ALL_RECTIFICATIVE_INVOICE_TYPES = ['A', 'B', 'R', 'RA', 'BRA']

PRODUCTES = {}

_FACTURAT_INCIDENT = 'facturat_incident'


def get_inici_final_consums(consums):
    # Dates consums
    dates_consums = {}
    for lect_tmp in consums.values():
        data_consum_inici = lect_tmp.get('anterior',
                                         {}).get('name', False)
        if data_consum_inici:
            dates_consums.setdefault('inici', data_consum_inici)
            dates_consums['inici'] = min(dates_consums['inici'],
                                         data_consum_inici)
        data_consum_final = lect_tmp.get('actual',
                                         {}).get('name', False)
        if data_consum_final:
            dates_consums.setdefault('final', data_consum_final)
            dates_consums['final'] = max(dates_consums['final'],
                                         data_consum_final)
    dates_consums.setdefault('inici', False)
    dates_consums.setdefault('final', False)
    return dates_consums


class GiscegasFacturacioFacturador(osv.osv):
    """Metaclasse per generar les factures (no té taula SQL).
    """
    _name = 'giscegas.facturacio.facturador'

    def get_tarifa_class(self, modcontractual):
        return TARIFES[modcontractual.tarifa.name]

    def get_data_boe(self, cursor, uid, date=None, pricelist=None):
        """Retorna la data del BOE per facturar segons la data que li passem.
        """
        if not date:
            date = time.strftime('%Y-%m-%d')
        if pricelist is None:
            pricelist = 'TARIFAS GAS'
        search_params = [
            ('date_start', '<=', date),
            '|',
            ('date_end', '>=', date),
            ('date_end', '=', False),
            ('pricelist_id.name', '=', pricelist)
        ]
        pl_vers_obj = self.pool.get('product.pricelist.version')
        data_boes_ids = pl_vers_obj.search(cursor, uid, search_params)
        if not data_boes_ids:
            raise osv.except_osv(
                _(u"No se han encontrado precios de BOE"), _(
                    u"No se ha encontrado ningún BOE entre las fechas "
                    u"indicadas: %s " % date
                )
            )
        versio_boe = pl_vers_obj.read(cursor, uid, data_boes_ids,
                                      ['name'])[0]['name']
        day, month, year = versio_boe.split(' - ')[1].split('/')
        return '{0}-{1}-{2}'.format(year, month, day)

    def create_invoice(self, cursor, uid, modcontractual_id, data_inici,
                       data_final, context=None):
        """Crea una factura segons la modificació contractual que se li
        passa.
        """
        if not context:
            context = {}
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        modcont_obj = self.pool.get('giscegas.polissa.modcontractual')
        lot_obj = self.pool.get('giscegas.facturacio.lot')
        cfg_obj = self.pool.get('res.config')
        if isinstance(modcontractual_id, list) or isinstance(modcontractual_id,
                                                             tuple):
            modcontractual_id = modcontractual_id[0]
        modcontract = modcont_obj.browse(cursor, uid, modcontractual_id,
                                         context)
        polissa_id = modcontract.polissa_id.id
        lot_id = context.get('lot_id', False)
        if not lot_id and modcontract.polissa_id.lot_facturacio:
            lot_id = modcontract.polissa_id.lot_facturacio.id
        if lot_id:
            lot = lot_obj.browse(cursor, uid, lot_id)
        type_invoice = context.get('type_invoice', 'out_invoice')
        partner_from_modcon = int(
            cfg_obj.get(cursor, uid, 'giscegas_partner_invoice_from_modcon', '0')
        )
        ctx = context.copy()
        if (type_invoice in ['out_invoice', 'out_refund'] and
            partner_from_modcon):
            ctx.update({'date': data_final})

        vals = factura_obj.onchange_polissa(cursor, uid, [], polissa_id,
                                             type_invoice, ctx)['value']

        if type_invoice in ['out_invoice', 'out_refund']:
            vals['llista_preu'] = modcontract.llista_preu.id

        vals['polissa_id'] = polissa_id
        if modcontract.tarifa.name == 'RE':
            pricelist = modcontract.llista_preu.name
        else:
            pricelist = None
        if context.get('factura_manual', False):
            vals['date_boe'] = context.get('date_boe', False) or \
                               self.get_data_boe(cursor, uid,
                                        context.get('data_final_factura'), pricelist)
        else:
            if lot_id:
                boe_date = lot.data_final
            else:
                boe_date = data_final
            vals['date_boe'] = context.get('date_boe', False) or \
                               self.get_data_boe(cursor, uid, boe_date, pricelist)
        vals['facturacio'] = modcontract.facturacio
        vals['data_inici'] = data_inici
        vals['data_final'] = data_final
        vals['tarifa_acces_id'] = modcontract.tarifa.id
        vals['origin'] = context.get('origin', False)
        #TODO Gas. En principi desactivem la telgestió en gas per a les tarifes 3.1 .. 3.4
        # vals['polissa_tg'] = modcontract.tg

        if 'data_factura' in context:
            vals['date_invoice'] = context['data_factura']
        elif int(cfg_obj.get(cursor, uid, 'giscegas_date_invoice_eq_lot_data_final',
                             '1')):
            vals['date_invoice'] = lot.data_final
        else:
            vals['date_invoice'] = time.strftime('%Y-%m-%d')
        # Posem la data límit de cobrament
        if 'payment_term' in vals and 'date_invoice' in vals:
            vals.update(factura_obj.onchange_payment_term_date_invoice(cursor,
                        uid, [], vals['payment_term'],
                        vals['date_invoice']).get('value', {}))
        if lot_id:
            vals['lot_facturacio'] = lot.id
        vals['type'] = type_invoice
        # Per defecte el diari és el d'energia
        journal_obj = self.pool.get('account.journal')
        jid = journal_obj.search(cursor, uid, [('code', '=', 'GAS')])
        if jid:
            jid = jid[0]
        else:
            jid = False
        if not 'journal_id' in vals:
            vals['journal_id'] = context.get('journal_id', jid)
        vals['tipo_rectificadora'] = context.get('tipo_rectificadora', 'N')
        vals['ref'] = context.get('ref', False)
        factura_id = factura_obj.create(cursor, uid, vals, context=context)
        return factura_id

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        """Facturem la pòlissa a través de les lectures.
        """
        factures_creades = []
        if isinstance(polissa_id, list) or isinstance(polissa_id, tuple):
            polissa_id = polissa_id[0]
        if not context:
            context = {}

        # Pools d'objectes que utilitzarem
        polissa_obj = self.pool.get('giscegas.polissa')
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        modcontractual_obj = self.pool.get('giscegas.polissa.modcontractual')
        comptador_obj = self.pool.get('giscegas.lectures.comptador')
        lot_obj = self.pool.get('giscegas.facturacio.lot')
        cfg_obj = self.pool.get('res.config')
        extra_obj = self.pool.get('giscegas.facturacio.extra')
        fest_obj = self.pool.get('giscedata.dfestius')
        canon_obj = self.pool.get('giscegas.canon.irc')

        inici_final_use_lot = int(cfg_obj.get(cursor, uid,
                                              'giscegas_inici_final_use_lot', '1'))
        avoid_1day_invoice = int(cfg_obj.get(cursor, uid,
                                             'giscegas_fact_avoids_1day_invoice', '0'))
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
        # Inici i final que se li ha de facturar a la pòlissa
        lid = lot_id
        if not inici_final_use_lot:
            lid = False
        if context.get('factura_manual', False):
            data_inici = context.get('data_inici_factura', False)
            data_final = context.get('data_final_factura', False)
            inici_final_use_lot = 0
        else:
            data_inici, data_final = polissa.get_inici_final_a_facturar(
                use_lot=lid,
                context=context
            )
        # Totes les modificacions contractuals que ha tingut la pòlissa en
        # aquest periode
        intervals = polissa.get_modcontractual_intervals(data_inici,
                                                         data_final)
        mod_ids = []
        mod_types = {}
        mod_dates = {}
        # Ordenem els ids de les modificacions contractuals per les dates de
        # tall
        if not intervals:
            _msg = _("No s'han trobat modificacions contractuals per la "
                   "polissa %s entre %s i %s") % (polissa.name, data_inici,
                    data_final)
            raise osv.except_osv('Error', _msg)
        for mod_data in sorted(intervals.keys()):
            mod_id = intervals[mod_data]['id']
            mod_ids.append(mod_id)
            # Afegim quin tipus de modificació és per aquesta modificació
            # contractual
            mod_types[mod_id] = intervals[mod_data]['changes']
            # Interval real que duren amb possibilitat de més d'una modificació
            # contractual
            mod_dates[mod_id] = intervals[mod_data]['dates']
        # Facturem per cada modificació contractual
        # de facturació (potencia, tarifa, etc)
        # Comptadors facturats
        comptadors_facturats = []
        # Veure: http://wiki.gisce.lan/pages/viewpage.action?pageId=5177594
        for modcontractual in modcontractual_obj.browse(cursor, uid, mod_ids,
                                                        context):
            mod_id = modcontractual.id
            tarifa = modcontractual.tarifa
            tar_id = tarifa.id
            tarifa_class = self.get_tarifa_class(modcontractual)
            base = 'mes'
            factor = modcontractual.property_unitat_terme_fix.factor
            if not factor % 365 or not factor % 366:
                base = 'dia'
                if inici_final_use_lot:
                    inici_final_use_lot = 0
                    if (context.get('tipo_rectificadora', 'N') == 'N'
                            and 'fins_lectura_fact' in context):
                        context['fins_lectura_fact'] = False
                    data_inici, data_final = polissa.get_inici_final_a_facturar(
                        use_lot=False,
                        context=context
                    )
            if tarifa.name not in PRODUCTES:
                productes = {}
                productes['variable'] = tarifa.get_periodes_producte('te')
                productes['fijo'] = tarifa.get_periodes_producte('tp')
                PRODUCTES[tarifa.name] = productes
            productes = PRODUCTES[tarifa.name]
            data_inici_periode_f = max(data_inici, mod_dates[mod_id][0])
            data_final_periode_f = min(data_final, mod_dates[mod_id][1])
            # Si facturem per dies necessitarem saber els dies envers el que
            # està previst facturar
            # Una factura manual no té lot (lid = False)
            if inici_final_use_lot and lid:
                lot = lot_obj.browse(cursor, uid, lid)
                data_inici, data_final = polissa.get_inici_final_a_facturar(
                                            use_lot=lot.id)
            if modcontractual.polissa_id.active and len(mod_ids) == 1:
                data_final_periode_f = data_final
            # Protecció factures de 0 dies.
            # Es dóna quan tenim només la primera lectura d'un comptador o només
            # la primera lectura d'una modificació contractual amb canvi de
            # tarifa. S'habilita amb res_config.avoid_1day_invoice
            if (data_inici_periode_f == data_final_periode_f
                and avoid_1day_invoice):
                    # Factura de 1 dia, no la creem
                    # TODO: Analitzar si hi ha alguna cosa a modificar pq el "bucle"
                    # continuï funcionant
                    continue

            versions = self.versions_de_preus(cursor, uid, polissa_id,
                                              data_inici_periode_f,
                                              data_final_periode_f, context)
            comptadors_actius = polissa.comptadors_actius(data_inici_periode_f,
                                                      data_final_periode_f,
                                                      order='data_alta asc')

            canons_actius = polissa.get_active_canons_by_period(
                data_inici_periode_f, data_final_periode_f,
                context=context
            )

            # Check for meters with reads not invoiced yet
            not_invoiced = polissa.meters_not_invoiced(data_inici_periode_f,
                                                       context=context)
            not_invoiced_add = list(set(not_invoiced)
                                    - set(comptadors_facturats))
            comptadors_actius.extend(not_invoiced_add)
            #Si la factura es manual, la data la fem coincidir
            #amb la data final del periode de facturacio
            if context.get('factura_manual', False):
                context.update({'data_factura': data_final_periode_f})
            factura_id = self.create_invoice(cursor, uid, modcontractual.id,
                                             data_inici_periode_f,
                                             data_final_periode_f, context)
            # Agafem la llista de preu segons la factura, ja que aquesta és
            # la que sap si la factura és de client (out) o de proveïdor (in)
            con = {}
            facturador = tarifa_class(con, data_inici_periode_f,
                                      data_final_periode_f,
                                      facturacio=modcontractual.facturacio,
                                      data_inici_periode=data_inici,
                                      data_final_periode=data_final,
                                      versions=versions, base=base)
            # config facturador
            self.config_facturador(
                cursor, uid, facturador, polissa_id, context=context
            )

            self.config_facturador_with_canons(
                cursor, uid, facturador, polissa_id, context=context
            )

            # Passar els dies festius
            fest_search = [('name', '>=', data_inici_periode_f),
                           ('name', '<=', data_final_periode_f)]
            festius = fest_obj.search_reader(cursor, uid, fest_search)
            dies_festius = [
                datetime.strptime(d['name'], '%Y-%m-%d').date()
                for d in festius
            ]
            facturador.conf['holidays'] = dies_festius
            # Per cada comptador fem els càlculs dels consums, però només
            # generarem una sola factura.
            upd_ult_lectura = False
            for compt in comptador_obj.browse(cursor, uid, comptadors_actius,
                                              context):
                # Fixem com a data final màxima per la lectura la data final
                # del lot
                context.update({'fins_lectura_fact': data_final_periode_f})
                # Obtenim consums d'activa i reactiva
                con = compt.get_consum_per_facturar(
                    tar_id, context=context
                )
                # Lectures
                lects = compt.get_lectures_per_facturar(
                    tar_id, context=context
                )
                dates_consums = get_inici_final_consums(lects)

                # Fem servir les dates de lectures reals per fer la repartició
                # d'energia
                if dates_consums['inici']:
                    # En el cas d'inicialització de lectures hem de preveure
                    # que no sumi un dia si no és necessari. És a dir si la
                    # data inicial ja és igual a la data_inici_periode_f no
                    # cal que li sumem cap dia. Si no és igual voldrà dir que
                    # venim d'alguna lectura anterior i per tant li haurem de
                    # sumar un dia.
                    if (dates_consums['inici'] != data_inici_periode_f
                            and dates_consums['inici'] != compt.data_alta):
                        d_ini = datetime.strptime(dates_consums['inici'],
                                                  '%Y-%m-%d')
                        d_ini = datetime.strftime(d_ini + timedelta(days=1),
                                                  '%Y-%m-%d')
                        dates_consums['inici'] = d_ini
                facturador.update_dates_consums(dates_consums['inici'],
                                                dates_consums['final'])
                # Actualitzem la data ultima lectura
                if dates_consums['final']:
                    upd_ult_lectura = (upd_ult_lectura and
                                       max(upd_ult_lectura,
                                           dates_consums['final'])
                                       or dates_consums['final'])

                # contract type to enable specific invoicing
                facturador.conf['contract_type'] = modcontractual.contract_type
                # if modcontractual.contract_type == '09':
                #     facturador.conf['contract_type_params'] = {
                #         'expected_consumption':
                #             modcontractual.expected_consumption * 1000,
                #     }
                # else:
                #     facturador.conf['contract_type_params'] = {}
                facturador.conf['contract_type_params'] = {}

                # Aquí hauriem de tenir les peculiaritats de cada taria
                # No en tenim

                # Calculem els termes variables
                if compt.id in comptadors_facturats:
                    from giscegas_facturacio.giscegas_polissa import INTERVAL_INVOICING_FIELDS
                    fields_to_detect = INTERVAL_INVOICING_FIELDS
                    if (set(fields_to_detect)
                         & set(mod_types.get(modcontractual.id, []))):
                        # Posem els consums d'aquests comptadors
                        facturador.update_consums(con)
                        self.fact_variable(
                            cursor, uid, facturador, factura_id,
                            context=context
                        )
                        self.crear_lectures(cursor, uid, factura_id, lects, context)
                else:
                    # Posem els consums d'aquests comptadors
                    facturador.update_consums(con)
                    self.fact_variable(
                        cursor, uid, facturador, factura_id, context=context
                    )
                    self.crear_lectures(cursor, uid, factura_id, lects, context)
                # Calculem el lloguer
                if compt.lloguer:
                    productes['lloguer'] = self.fact_lloguer(facturador,
                                                compt, data_inici_periode_f,
                                                data_final_periode_f)
                    # Ens assegurem que hi ha alguna cosa vàlida a 'lloguer'
                    if not productes['lloguer']:
                        del productes['lloguer']
                        if 'lloguer' in facturador.termes:
                            del facturador.termes['lloguer']
                else:
                    if 'lloguer' in facturador.termes:
                        del facturador.termes['lloguer']
                # Creem les línes de factura per cada tipus de terme
                for tipus in facturador.termes.keys():
                    for version_values in facturador.termes[tipus].values():
                        for item, termes in version_values.items():
                            vals = termes.copy()
                            vals['name'] = item
                            product_id = productes[tipus].get(
                                                    termes['product_id'],
                                                    False)
                            vals['product_id'] = product_id
                            if tipus == "variable":
                                self.crear_linies_consum(cursor, uid,
                                                         factura_id, vals,
                                                         context)
                            elif tipus == "lloguer":
                                self.crear_linies_lloguer(cursor, uid,
                                                          factura_id, vals,
                                                          context)
                            else:
                                self.crear_linies_altres(cursor, uid,
                                                         factura_id, vals,
                                                         context)
                # Afegim el comptador actual als comptadors facturats
                comptadors_facturats.append(compt.id)

            if canons_actius:
                # Facturem els canons
                # Puntero al valor apuntado del diccionario -> termes.value
                for canon in canon_obj.browse(cursor, uid, canons_actius, context=context):
                    productes['canon'] = self.fact_canons(
                        facturador, canon,
                        data_inici_periode_f, data_final_periode_f,
                    )
                    tus_canons = facturador.termes['canon'].values()

                    for version_values in tus_canons:
                        for item, termes in version_values.items():
                            product_id = productes[tipus].get(
                                termes['name'],
                                False)
                            vals = termes.copy()
                            vals['product_id'] = product_id

                            self.crear_linies_canon(
                                cursor, uid, factura_id, vals, context=context
                            )

            # Per les següents factures actualitzem la data de
            # l'última lectura facturada.
            if upd_ult_lectura:
                context['ult_lectura_fact'] = upd_ult_lectura
            # Calculem els termes de potència

            facturador.factura_fixe()
            for version_values, termes in facturador.termes.get('fixe', {}).items():
                vals = termes.copy()
                vals['name'] = termes['product_id']
                product_id = productes['fijo'].get(termes['product_id'],  False)
                vals['product_id'] = product_id
                self.crear_linies_terme_fix(cursor, uid, factura_id, vals, context)

            # Compute extra lines
            extra_obj.compute_extra_lines(cursor, uid, [factura_id],
                                          context=context)
            # Actualitzem la llista d'impostos
            factura_obj.button_reset_taxes(cursor, uid, [factura_id])
            factures_creades.append(factura_id)
        # Escrivim el lot de facturació a totes les factures creades
        factura_obj.write(cursor, uid, factures_creades,
                          {'lot_facturacio': lot_id})

        return factures_creades

    # Es creen funcions separades per cada tipus de línia per així poder-ho
    # sobreescriure-ho en distribució/comercialització
    def crear_linia(self, cursor, uid, factura_id, vals, context=None):
        """Funció base per crear la línia de factura.
        """
        if not context:
            context = {}
        product_obj = self.pool.get('product.product')
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        lot_obj = self.pool.get('giscegas.facturacio.lot')

        # Stuff amb dades de la factura i la pòlissa
        factura = factura_obj.browse(cursor, uid, factura_id)
        llista_preu_id = context.get(
            'force_llista_preu_id', factura.llista_preu.id
        )
        type_invoice = context.get('type_invoice', 'out_invoice')
        lot_id = context.get('lot_id', False) or \
                     factura.polissa_id.lot_facturacio
        data_final_lot = lot_obj.browse(cursor, uid, lot_id).data_final
        polissa_id = factura.polissa_id.id
        partner_id = factura.partner_id.id
        product_id = vals['product_id']
        if product_id:
            #If it is a refund invoice,
            #fiscal position will be the same
            #from the original invoice
            if factura.ref:
                fiscal_position_id = factura.ref.fiscal_position.id
            else:
                fiscal_position_id = factura.fiscal_position.id
            product = product_obj.browse(cursor, uid, product_id)
            # Agafem la uom que ens vingui en el vals i sino la per defecte
            # del producte
            uom_id = vals.get('uos_id', product.uom_id.id)
            data_preu = vals.get('data_desde', data_final_lot)
            l_vals = linia_obj.product_id_change(cursor, uid, [],
                                         llista_preu_id, data_preu,
                                         product_id, uom_id, polissa_id,
                                         vals['quantity'], vals['name'],
                                         type_invoice, partner_id,
                                         fposition_id=fiscal_position_id,
                                         context=context)
            l_vals = l_vals['value']
            l_vals['invoice_line_tax_id'] = [
                (6, 0, l_vals.get('invoice_line_tax_id', []))
            ]
            vals.update(l_vals)
        else:
            # TODO: Agonseguir els IVA's per defecte
            pass
        if 'force_price' in vals:
            vals['price_unit'] = vals['force_price']
            del vals['force_price']
        vals['factura_id'] = factura_id
        return linia_obj.create(cursor, uid, vals, context=context)

    def crear_linia_account_invoice(self, cursor, uid, factura_id, vals, context=None):
        """Funció base per crear la línia de factura.
        """
        if not context:
            context = {}
        product_obj = self.pool.get('product.product')
        factura_obj = self.pool.get('account.invoice')
        linia_obj = self.pool.get('account.invoice.line')
        # Stuff amb dades de la factura i la pòlissa
        factura = factura_obj.browse(cursor, uid, factura_id)
        type_invoice = context.get('type_invoice', 'out_invoice')
        partner_id = factura.partner_id.id
        product_id = vals['product_id']
        #If it is a refund invoice,
        #fiscal position will be the same
        #from the original invoice
        if factura.ref:
            fiscal_position_id = factura.ref.fiscal_position.id
        else:
            fiscal_position_id = factura.fiscal_position.id
        if product_id:
            product = product_obj.browse(cursor, uid, product_id)
            # Agafem la uom que ens vingui en el vals i sino la per defecte
            # del producte
            uom_id = vals.get('uos_id', product.uom_id.id)
            l_vals = linia_obj.product_id_change(
                cursor, uid, [], product.id, uom_id, vals['quantity'],
                vals['name'], type_invoice, partner_id,
                fposition_id=fiscal_position_id, context=context
            )
            l_vals = l_vals['value']
            l_vals['invoice_line_tax_id'] = [
                (6, 0, l_vals.get('invoice_line_tax_id', []))
            ]
            vals.update(l_vals)
        else:
            # TODO: Agonseguir els IVA's per defecte
            pass
        if 'force_price' in vals:
            vals['price_unit'] = vals['force_price']
            del vals['force_price']
        vals['invoice_id'] = factura_id
        return linia_obj.create(cursor, uid, vals, context=context)

    def crear_lectures(self, cursor, uid, factura_id, vals, context=None):
        """Crea les lectures
        """
        ids = []
        l_obj = self.pool.get('giscegas.facturacio.lectures')
        for lectura in vals.values():
            if not lectura.get('anterior', {}) or not lectura.get('actual', {}):
                continue
            origen_actual = lectura['actual'].get('origen_id', False)
            if origen_actual and isinstance(origen_actual, (list, tuple)):
                origen_actual = origen_actual[0]

            origen_anterior = lectura['anterior'].get('origen_id', False)
            if origen_anterior and isinstance(origen_anterior, (list, tuple)):
                origen_anterior = origen_anterior[0]

            l_vals = {
                'name': lectura['actual']['periode'][1],
                'lect_actual': lectura['actual'].get('lectura', 0),
                'consum': lectura['actual'].get('consum', 0),
                'ajust': lectura['actual'].get('ajust', 0),
                'motiu_ajust': lectura['actual'].get('motiu_ajust', 0),
                'lect_anterior': lectura['anterior'].get('lectura', 0),
                'data_actual': lectura['actual']['name'],
                'data_anterior': lectura['anterior']['name'],
                'factura_id': factura_id,
                'comptador_id': lectura['actual']['comptador'][0],
                'origen_id': origen_actual,
                'origen_anterior_id': origen_anterior,
                'tipus': 'variable',
                'tipo_lect_num_anterior': lectura['anterior'].get('tipo_lect_num'),
                'tipo_lect_num_actual': lectura['actual'].get('tipo_lect_num'),
                'factor_k': lectura['actual'].get('factor_k'),
                'pcs': lectura['actual'].get('pcs'),
                'pressio_subministrament': lectura['actual'].get('pressio_subministrament'),
            }
            ids += [l_obj.create(cursor, uid, l_vals)]
        return ids

    def crear_linies_consum(self, cursor, uid, factura_id,
                            vals, context=None):
        """Creem les línies d'energia per la factura segons els termes.
        """
        return self.crear_linia(cursor, uid, factura_id, vals, context)

    def crear_linies_terme_fix(self, cursor, uid, factura_id, vals,
                              context=None):
        """Creem les línies de potència per la factura segons els termes.
        """
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        polissa_obj = self.pool.get('giscegas.polissa')
        product_obj = self.pool.get('product.product')
        uom_obj = self.pool.get('product.uom')
        cfg = self.pool.get('res.config')
        factura = factura_obj.browse(cursor, uid, factura_id)
        # Fem un browse segons la data final de la factura
        polissa = polissa_obj.browse(cursor, uid, factura.polissa_id.id,
                                     context={'date': factura.data_final})
        uom_id = polissa.property_unitat_terme_fix.id
        uom_id = self.get_uoms_leaps(cursor, uid, uom_id, vals['data_desde'])
        vals['uos_id'] = uom_id
        return self.crear_linia(cursor, uid, factura_id, vals, context)

    def crear_linies_lloguer(self, cursor, uid, factura_id,
                             vals, context=None):
        """Creem les línies de lloguer per la factura segons els termes.
        """
        return self.crear_linia(cursor, uid, factura_id, vals, context)

    def crear_linies_lloguer_account_invoice(self, cursor, uid, factura_id,
                             vals, context=None):
        """Creem les línies de lloguer per la factura segons els termes.
        """
        return self.crear_linia_account_invoice(cursor, uid, factura_id, vals, context)

    def crear_linies_altres(self, cursor, uid, factura_id, vals, context=None):
        """Creem les línies d'altres conceptes per la factura.
        """
        vals['tipus'] = 'altres'
        return self.crear_linia(cursor, uid, factura_id, vals, context)

    def versions_de_preus_ids(self, cursor, uid, polissa_id, data_inici,
                          data_final, context=None):
        """Retorna les versions de llistes de preus entre dues dates.
        """
        if not context:
            context = {}
        if isinstance(polissa_id, list) or isinstance(polissa_id, tuple):
            polissa_id = polissa_id[0]
        polissa_obj = self.pool.get('giscegas.polissa')
        pricelist_ver_obj = self.pool.get('product.pricelist.version')
        item_obj = self.pool.get('product.pricelist.item')
        # Utilitzem el nou browse que sap navegar pel temps segons el context
        # 'date'
        ctx = context.copy()
        ctx['date'] = data_final
        polissa = polissa_obj.browse(cursor, uid, polissa_id, ctx)
        if context.get('type_invoice', '') in ('in_invoice', 'in_refund'):
            llista_preu_id = polissa.cups.distribuidora_id.\
                                property_product_pricelist_purchase.id
        else:
            llista_preu_id = polissa.llista_preu.id
        if context.get('llista_preu', False):
            llista_preu_id = context['llista_preu']
        search_params = [
            ('pricelist_id.id', '=', llista_preu_id),
            ('date_start', '<=', data_final),
            '|',
            ('date_end', '>=', data_inici),
            ('date_end', '=', False),
        ]
        ver_ids = pricelist_ver_obj.search(cursor, uid, search_params,
                                           context={'active_test': False})
        read_fields = ['id', 'date_start', 'date_end']
        res = {}
        for versio in pricelist_ver_obj.read(cursor, uid, ver_ids, read_fields,
                                             context):
            data_tall = max(data_inici, versio['date_start'])
            res[data_tall] = versio['id']

        # Si es factura per dies ens hem d'assegurar de fer bé lo de la data
        # en any de traspàs
        factor = polissa.property_unitat_terme_fix.factor
        if factor % 365 or factor % 366:
            res = insert_data_traspas(res)
            if len(res) == 1:
                year_tall = int(data_tall.split('-')[0])
                year_final = int(data_final.split('-')[0])
                if year_tall != year_final and (
                    calendar.isleap(year_tall) or calendar.isleap(year_final)
                ):
                    res['{}-01-01'.format(year_final)] = res.values()[0]
        if not res:
            raise osv.except_osv('Error', u"No se han encontrado versiones de precios")
        # Repassem els items per si qualcu depen
        # d'una llista que te versions diferents
        search_params = [('price_version_id', 'in', ver_ids)]
        item_ids = item_obj.search(cursor, uid, search_params,
                                   context=context)
        for item_id in item_ids:
            item = item_obj.browse(cursor, uid, item_id)
            processed = context.get('processed_pricelist', [])
            if (item.base != -1
                or item.base_pricelist_id.name == 'TARIFAS GAS'
                or item.base_pricelist_id.id in processed):
                continue
            processed.append(item.base_pricelist_id.id)
            context['processed_pricelist'] = processed
            dep_pricelist = item.base_pricelist_id
            ctx = context.copy()
            ctx.update({'llista_preu': dep_pricelist.id})
            dep_versions = self.versions_de_preus(cursor, uid, polissa_id,
                                                  data_inici, data_final, ctx)
            not_included = list(set(dep_versions.keys()) - set(res.keys()))
            # Si tenim versions no incloses hem de mirar
            # a quina versio de la llista original les hem de lligar
            for versio in not_included:
                ordered = sorted(res.keys())
                for i in range(len(ordered)):
                    # Si hem arribat al final, assignem la darrera de la llista
                    if i + 1 == len(ordered):
                        res[versio] = res[ordered[i]]
                        break
                    if ordered[i] < versio and ordered[i + 1] > versio:
                        res[versio] = res[ordered[i]]
                        break
        return res

    def versions_de_preus(self, cursor, uid, polissa_id, data_inici, data_final,
                          context=None):
        return self.versions_de_preus_ids(
            cursor, uid, polissa_id, data_inici, data_final, context=context
        )

    @cache()
    def get_uoms_leaps(self, cursor, uid, uom_id, date, context=None):
        """Retorna el mateix diccionari que el de versions de preus, però amb
        la UOM Corresponent.
        """
        uom_obj = self.pool.get('product.uom')
        uom = uom_obj.browse(cursor, uid, uom_id)
        # Busquem totes les UOMs que es diguin igual que uom
        uom_ids = uom_obj.search(cursor, uid, [('name', '=', uom.name)],
                                 context={'active_test': False})
        uoms = {}
        for uom in uom_obj.browse(cursor, uid, uom_ids):
            if not uom.factor % 365:
                uoms[False] = uom.id
            elif not uom.factor % 366:
                uoms[True] = uom.id

        year = int(date.split('-')[0])
        return uoms.get(calendar.isleap(year), uom_id)

    def fact_variable(self, cursor, uid, facturador, factura_id, context=None):
        """
        Invoices Energy
        :param facturador: Tariff class
        """
        facturador.factura_variable()

    def fact_lloguer(self, facturador, compt, data_inici_periode_f,
                     data_final_periode_f, context=None):
        raise osv.except_osv('Error', _('Per ser implementat en el mòdul de '
                               'distribució/comercialització.'))

    def config_facturador(
            self, cursor, uid, facturador, polissa_id, context=None):
        """
        Configs of aufit parámeters
        :param facturador: tariff instance
        :return: True
        """
        return True


GiscegasFacturacioFacturador()


class GiscegasFacturacioLot(osv.osv, netsvc.Agent):
    """Agrupació de contractes per facturació.

    Agrupem sota aquest model els contractes que s'han de facturar
    "conjuntament".
    """

    _name = 'giscegas.facturacio.lot'
    _description = "Agrupació de contractes per facturació."
    _order = 'data_final desc'

    _states_selection = [
        ('esborrany', 'Borrador'),
        ('obert', 'Abierto'),
        ('tancat', 'Cerrado'),
    ]

    def get_next(self, cursor, uid, data_final, facturacio, context=None):
        if context is None:
            context = {}
        cfg_obj = self.pool.get('res.config')
        bim_parell = int(cfg_obj.get(
            cursor, uid, 'giscegas_fact_bimestrals_sempre_lot_parell', '1')
        )
        d = datetime.strptime(data_final, '%Y-%m-%d')
        if facturacio == 2 and bim_parell and d.month % 2:
            facturacio = 1
        d += relativedelta(months=facturacio)
        lot_id = self.search(cursor, uid, [
            ('data_final', '>=', d.strftime('%Y-%m-%d'))
        ], order='data_final asc', limit=1)
        return lot_id and lot_id[0] or False

    def update_progress(self, cursor, uid, ids, context=None):
        contracte_lot_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        for lot in self.browse(cursor, uid, ids, context):
            if not lot.n_contracte_lots:
                progres = 0
                progres_val = 0
            else:
                search_params = [('state', 'in', ['facturat', 'finalitzat']),
                                 ('lot_id.id', '=', lot.id)]
                completed = contracte_lot_obj.search_count(cursor, uid,
                                                           search_params,
                                                           context=context)
                progres = (completed * 1.0) / lot.n_contracte_lots
                progres *= 100
                #Validate progress bar
                search_params = [('state', 'not in', ['esborrany', 'obert']),
                                 ('lot_id.id', '=', lot.id)]
                completed_val = contracte_lot_obj.search_count(cursor, uid,
                                                               search_params,
                                                               context=context)
                progres_val = (completed_val * 1.0) / lot.n_contracte_lots
                progres_val *= 100
            lot.write({'progres': progres,
                       'progres_validate': progres_val})
        return True

    def update_nfactures(self, cursor, uid, ids, context=None):

        contracte_lot_obj = self.pool.get('giscegas.facturacio.contracte_lot')

        sql = """SELECT clot_id, sum(nfactures)
            FROM (SELECT clot.id as clot_id,
            CASE WHEN coalesce(f.id,0) = 0 THEN 0
            ELSE 1 END as nfactures
            FROM giscegas_facturacio_contracte_lot clot
            LEFT JOIN giscegas_facturacio_factura f
            on f.polissa_id = clot.polissa_id
            and f.lot_facturacio = clot.lot_id
            and f.tipo_factura = '01'
            and f.tipo_rectificadora = 'N'
            LEFT JOIN account_invoice i
            on i.id = f.invoice_id
            INNER JOIN account_journal journal
            on journal.id = i.journal_id
            and journal.code = 'GAS'
            WHERE clot.lot_id = %s)clot_factures
            group by clot_id"""

        for lot_id in ids:
            cursor.execute(sql, (lot_id,))
            res = cursor.fetchall()
            for clot in res:
                contracte_lot_obj.write(cursor, uid, clot[0],
                                        {'n_factures': clot[1]})

        return True

    # Funcions per la creació de lots
    def crear_lots_mensuals(self, cursor, uid, year, context=None):
        """Creem els lots mensuals.
        """
        for month in range(1, 13):
            vals = {}
            vals['name'] = '%02i/%04i' % (month, year)
            vals['data_inici'] = '%04i-%02i-01' % (year, month)
            vals['data_final'] = '%04i-%02i-%02i' % (year, month,
                                        calendar.monthrange(year, month)[1])
            self.create(cursor, uid, vals, context)
        return True

    def actualitzar(self, cursor, uid, ids, context=None):

        self.update_progress(cursor, uid, ids, context)

        #Mirem si es pot tancar el lot
        self.wkf_tancat(cursor, uid, ids, context)

        return True

    def facturacio(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        lot_id = ids
        if isinstance(ids, tuple) or isinstance(ids, list):
            lot_id = lot_id[0]
        contracte_lot_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        assert isinstance(contracte_lot_obj, GiscegasFacturacioContracteLot)
        logger = netsvc.Logger()
        lot = self.browse(cursor, uid, lot_id, context)
        # Si el lot no està obert no facturem els contractes d'aquest
        if lot.state != 'obert':
            return False
        # Busquem totes les pòlisses que estan a punt per facturar
        search_params = [
            ('state', '=', 'facturar'),
            ('lot_id.id', '=', lot_id)
        ] + context.get('extra_filter', [])
        ctx = context.copy()
        ctx.update({'active_test': False})
        ids_a_facturar = contracte_lot_obj.search(
            cursor, uid, search_params, context=ctx
        )
        search_params = [
            ('state', 'not in', ['facturat', 'finalitzat']),
            ('lot_id.id', '=', lot_id)
        ]
        ids_no_facturats = contracte_lot_obj.search_count(cursor, uid,
                                                          search_params, context=context)
        if not ids_no_facturats:
            logger.notifyChannel(
                "objects", netsvc.LOG_INFO, u"El lote %s está "
                                            u"todo facturado." % lot.name
            )
            return True
        logger.notifyChannel("objects", netsvc.LOG_INFO,
                             u"Facturando el lote %s (%s de %s contratos)" % (
                                                lot.name,
                                                len(ids_a_facturar),
                                                len(lot.contracte_lot_ids)))

        jobs_ids = []
        with NoDependency():
            for cl_id in ids_a_facturar:
                j = contracte_lot_obj.facturar_async(
                    cursor, uid, [cl_id], context
                )
                jobs_ids.append(j.id)
        create_jobs_group(
            cursor.dbname, uid, _('Facturación lote {} - {} contratos').format(
                lot.name, len(ids_a_facturar)
            ), 'invoicing.make_invoices', jobs_ids
        )
        aw = AutoWorker(queue='make_invoices', default_result_ttl=24 * 3600)
        aw.work()

        return True

    @MultiprocessBackground.background()
    def facturacio_button(self, cursor, uid, ids, context=None):
        """Acció per facturar"""

        _(u"Acción para facturar")

        for lid in ids:
            self.facturacio(cursor, uid, [lid], context)
        return True

    def get_policies_from_state(
            self, cursor, uid, lot_id, clot_state, context=None):
        if context is None:
            context = {}

        clot_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        q = OOQuery(clot_obj, cursor, uid)
        q_sql = q.select(['polissa_id'], only_active=False).where([
            ('lot_id', '=', lot_id),
            ('state', '=', clot_state)
        ])
        cursor.execute(*q_sql)
        result = [x[0] for x in cursor.fetchall()]
        return result


    @MultiprocessBackground.background()
    def obrir_factures_button(self, cursor, uid, ids, context=None):
        """Acció per obrir factures del lot"""

        _(u"Acción para abrir facturas del lote")

        if not context:
            context = {}
        context.update({'sync': False})
        db = pooler.get_db_only(cursor.dbname)
        logger = netsvc.Logger()
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        conf_obj = self.pool.get('res.config')
        for lot_id in ids:
            q = OOQuery(factura_obj, cursor, uid)

            search_params = [
                ('lot_facturacio.id', '=', lot_id),
                ('invoice_id.type', '=', 'out_invoice'),
                ('state', '=', 'draft'),
            ] + context.get('extra_filter', [])
            polissa_incident = self.get_policies_from_state(
                cursor, uid, lot_id, _FACTURAT_INCIDENT
            )
            if polissa_incident:
                search_params += [('polissa_id.id', 'not in', polissa_incident)]

            q_sql = q.select(['id', 'invoice_id.date_invoice']).where(
                search_params)
            cursor.execute(*q_sql)
            fids = cursor.dictfetchall()
            sorted_factures = sorted(
                fids, key=lambda f: f['invoice_id.date_invoice']
            )
            for factura in sorted_factures:
                factura_id = factura['id']
                tmp_cr = db.cursor()
                try:
                    factura_obj.invoice_open(tmp_cr, uid,
                                             [factura_id], context=context)
                    tmp_cr.commit()
                except Exception as exc:
                    logger.notifyChannel("objects", netsvc.LOG_ERROR, exc)
                    tmp_cr.rollback()
                finally:
                    tmp_cr.close()
        return True

    def validar(self, cursor, uid, lot_id, context=None):
        """Acció per validar els contractes lots.
        """
        if context is None:
            context = {}
        clot_obj = self.pool.get('giscegas.facturacio.contracte_lot')

        #Do not validate clots previously validated
        #or with generated invoices
        not_states = (_FACTURAT_INCIDENT, 'facturat', 'finalitzat')
        search_params = [
            ('lot_id', '=', lot_id),
            ('state', 'not in', not_states)
            ] + context.get('extra_filter', [])

        clot_ids = clot_obj.search(cursor, uid, search_params, context=context)
        job_ids = []
        with NoDependency():
            for clot_id in clot_ids:
                job = clot_obj.validate_contracts(
                    cursor, uid, [clot_id], context=context
                )
                job_ids.append(job.id)

        if len(job_ids) > 1:
            lot_name = clot_obj.read(
                cursor, uid, clot_id, ['lot_id']
            )['lot_id'][1]
            task_name = _("Validar Lote {0}")
            if context.get("validar_i_facturar"):
                task_name = _(u"Validar y Facturar Lote {0}")
            create_jobs_group(
                cursor.dbname, uid, task_name.format(lot_name),
                'invoicing.validate_lot', job_ids
            )
        aw = AutoWorker(queue='validate_lot', default_result_ttl=24 * 3600)
        aw.work()

        return True

    @MultiprocessBackground.background()
    def validar_button(self, cursor, uid, ids, context=None):
        """Acció per validar en background amb multiprocés"""

        _(u"Acción para validar en segundo plano con multiproceso")

        for lid in ids:
            self.validar(cursor, uid, [lid], context=context)
        return True

    def get_previous_lot(self, cursor, uid, lot_id,
                         delta=None, context=None):
        """Get previuos lot based on delta value
        delta: dict with relativedelta paramaters
        """

        if isinstance(lot_id, (list, tuple)):
            lot_id = lot_id[0]

        lot = self.browse(cursor, uid, lot_id, context=context)

        if delta is None:
            delta = {'years': -1}

        delta_date = (datetime.strptime(lot.data_final, '%Y-%m-%d') +
                      relativedelta(**delta))
        delta_month_days = calendar.monthrange(delta_date.year,
                                               delta_date.month)[1]
        prev_lot_date = datetime(delta_date.year,
                                 delta_date.month,
                                 delta_month_days).strftime('%Y-%m-%d')
        search_params = [('data_final', '=', prev_lot_date)]
        lot_ids = self.search(cursor, uid, search_params, context=context)
        if lot_ids:
            return lot_ids[0]
        return False

    def get_id_from_date(self, cursor, uid, date=None, context=None):
        """
        Returns the identifier of the batch that includes a date.

        :param cursor: database cursor
        :param uid: user identifier
        :param date: a date included in the batch, if it is None then today \
            is used
        :param context: dict with the context
        :return: the identifier of the batch
        """

        if date is None:
            date = datetime.today()

        batch_ids = self.search(
            cursor,
            uid,
            [
                ('data_inici', '<=', date),
                ('data_final', '>=', date)
            ],
            context=context
        )
        if not batch_ids:
            raise osv.except_osv(
                _('Error!'),
                _("No hay ningún lote que incluya la fecha de hoy")
            )
        return batch_ids[0]

    # Workflow stuff
    def wkf_esborrany(self, cursor, uid, ids, context=None):
        """Estat esborrany.
        """
        self.write(cursor, uid, ids, {'state': 'esborrany'})
        return True

    def wkf_obert(self, cursor, uid, ids, context=None):
        """Estat obert del workflow.
        """
        if not context:
            context = {}
        clf_obj = self.pool.get('giscegas.facturacio.contracte_lot')

        #Actualitzem el estat dels lots
        self.write(cursor, uid, ids, {'state': 'obert'})

        #Al obrir un lot, obrim tots els contractes
        #en esborrany del lot
        search_params = [('lot_id', 'in', ids),
                         ('state', '=', 'esborrany')]

        contracte_lot_ids = clf_obj.search(cursor, uid, search_params,
                                           context=context)

        clf_obj.wkf_obert(cursor, uid, contracte_lot_ids, context)

        #Actualitzem la barra de progres del lot
        self.update_progress(cursor, uid, ids, context)

        return True

    def cnd_tancar(self, cursor, uid, ids, context=None):
        """Comprovació si un lot es pot tancar.

        Si tots els contractes d'aquest lot estan ja facturats es pot tancar
        el lot.
        """
        clot_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        for lot in self.browse(cursor, uid, ids):
            search_params = [('state', 'not in', ('facturat', 'finalitzat')),
                             ('lot_id', '=', lot.id)]
            clot_ids = clot_obj.search_count(cursor, uid, search_params)
            if clot_ids:
                return False
        return True

    def wkf_tancat(self, cursor, uid, ids, context=None):
        """Estat tancat del workflow.
        """
        for lot_id in ids:
            if self.cnd_tancar(cursor, uid, [lot_id]):
                self.write(cursor, uid, [lot_id], {'state': 'tancat'})
        return True

    # Python constraints
    def _cnt_un_obert(self, cursor, uid, ids):
        """Comprovem que només hi ha un lot obert per facturar.
        """
        conf_obj = self.pool.get('res.config')
        multiple = int(conf_obj.get(cursor, uid, 'allow_multiple_opened_lots', 0))
        if multiple:
            return True
        count = self.search_count(cursor, uid, [('state', '=', 'obert')])
        if count > 1:
            return False
        return True

    def _cnt_tancar_primer(self, cursor, uid, ids):
        """Al tancar un lot mirem que els anteriors ja estiguin tancats.
        """
        conf_obj = self.pool.get('res.config')
        aclose = int(conf_obj.get(cursor, uid, 'allow_close_without_previous_lots', 0))
        if aclose:
            return True
        for info in self.read(cursor, uid, ids, ['data_final', 'state']):
            if info['state'] != 'tancat':
                continue
            count = self.search_count(cursor, uid, [
                ('state', '=', 'obert'), ('data_final', '<', info['data_final'])
            ])
            if count != 0:
                return False
        return True

    def _get_n_contracte_lots_ids(self, cursor, uid, ids, context=None):
        """Mirem quins lots hem d'actualitzar, ens venen contractes_lot.
        """

        clot_obj = self.pool.get('giscegas.facturacio.contracte_lot')

        lot_ids = clot_obj.read(cursor, uid, ids, ['lot_id'])
        return list(set([x['lot_id'][0] for x in lot_ids]))

    def _ff_n_contracte_lots(self, cursor, uid, ids, field_name, arg,
                             context=None):
        res = {}
        cl_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        for lot_id in ids:
            res[lot_id] = cl_obj.search_count(cursor, uid,
                                              [('lot_id.id', '=', lot_id)])
        return res

    _columns = {
        'name': fields.char('Lote', size=16, required=True, readonly=True),
        'data_inici': fields.date('Fecha inicio', required=True, readonly=True),
        'data_final': fields.date('Fecha fin', required=True, readonly=True),
        'contracte_lot_ids': fields.one2many(
                                    'giscegas.facturacio.contracte_lot',
                                    'lot_id', 'A facturar'),
        'n_contracte_lots': fields.function(
            _ff_n_contracte_lots, method=True, string="Numero de contratos",
            type='integer', store={
                'giscegas.facturacio.contracte_lot': (
                    _get_n_contracte_lots_ids, ['lot_id'], 10
                ),
                'giscegas.facturacio.lot': (
                    lambda self, cr, uid, ids, c={}: ids, [], 10
                )
            }
        ),
        # TODO: fields.function per poguer fer servir els triggers
        'progres': fields.float('Progreso de facturación', readonly=True),
        'progres_validate': fields.float(
            'Progreso de validación', readonly=True
        ),
        'state': fields.selection(
            _states_selection, 'Estado', size=64, readonly=True
        ),
        'active': fields.boolean('Activo')
    }

    _defaults = {
        'active': lambda *a: 1,
        'state': lambda *a: 'esborrany',
        'progres': lambda *a: 0,
    }

    _constraints = [
        (
            _cnt_un_obert, 'Error: Solamente puede haber un contrato '
                           'abierto.', ['state']
        ),
        (
            _cnt_tancar_primer, "Error: Hay que cerrar los lotes anteriores "
                                "antes de cerrar este", ['state']
        )
    ]

    _sql_constraints = [
            ('name_uniq', 'unique (name)',
             'Este lote de facturación ya existe.'),
    ]


GiscegasFacturacioLot()


class GiscegasFacturacioContracteLot(osv.osv):
    """Agrupació de Contractes i Lots de facturació.

    Model intermig per crear una relació m2m entre contractes i lots.
    """

    _name = 'giscegas.facturacio.contracte_lot'
    _description = "Agrupación de Contratos de Gas y Lotes de facturación"

    def name_get(self, cursor, uid, ids, context=None):
        res = []
        for cl in self.browse(cursor, uid, ids, context):
            res += [(cl.id, '%s - %s' % (cl.lot_id.name, cl.polissa_id.name))]
        return res

    def unlink(self, cursor, uid, ids, context=None):
        # Quan eliminem un contracte lot, si la pòlissa el té assignat
        # el treiem perquè no hi hagi confusions.
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for cl in self.browse(cursor, uid, ids, context):
            anul = context.get('anul', False)
            if cl.polissa_id.lot_facturacio.id == cl.lot_id.id and not anul:
                ctx = context.copy()
                ctx.update({'anul': True})
                cl.polissa_id.write({'lot_facturacio': False}, ctx)
            else:
                super(GiscegasFacturacioContracteLot,
                      self).unlink(cursor, uid, [cl.id], context)
        return True

    def update_nfactures(self, cursor, uid, ids, context=None):

        factura_obj = self.pool.get('giscegas.facturacio.factura')
        query_file = ('%s/giscegas_facturacio/sql/query_nfactures.sql'
                      % config['addons_path'])
        query = open(query_file).read()
        for clot in self.browse(cursor, uid, ids):
            cursor.execute(query, (clot.polissa_id.id,
                                   clot.lot_id.id, ))
            nfactures = cursor.fetchone()[0]
            clot.write({'n_factures': nfactures})

        return True

    @job(queue='make_invoices')
    def facturar_async(self, cursor, uid, ids, context=None):
        return self.facturar(cursor, uid, ids, context=context)

    def facturar(self, cursor, uid, ids, context=None):
        """Aquesta funció ens factura el contracte d'aquest lot.
        """
        facturador = self.pool.get('giscegas.facturacio.facturador')
        valid_obj = self.pool.get('giscegas.facturacio.validation.validator')
        fields_to_read = [
            'id', 'lot_id', 'polissa_id', 'tipus_facturacio', 'state'
        ]
        q = OOQuery(self, cursor, uid)
        q_sql = q.select(fields_to_read, for_=For('UPDATE', nowait=True)).where([
            ('id', 'in', ids)
        ])
        cursor.execute(*q_sql)
        for clot_vals in cursor.dictfetchall():
            if clot_vals['state'] != 'facturar':
                continue
            lot_id = clot_vals['lot_id']
            polissa_id = clot_vals['polissa_id']
            method_name = 'fact_via_%s' % clot_vals['tipus_facturacio']
            # facturant
            #self.wkf_facturant(cursor, uid, [clot_vals['id']])
            # Facturem!
            factures_creades = getattr(facturador, method_name)(
                cursor, uid, polissa_id, lot_id
            )
            # facturat
            self.wkf_facturat(cursor, uid, [clot_vals['id']])

            warning_ids = []
            for fact_id in factures_creades:
                warning_ids += valid_obj.validate_invoice(
                    cursor, uid, fact_id, context
                )

            if warning_ids:
                self.write(
                    cursor, uid, clot_vals['id'],
                    {'state': _FACTURAT_INCIDENT}
                )

            info = ''
            for fact_id in factures_creades:
                info += valid_obj.get_invoice_warnings_text(
                    cursor, uid, fact_id, context=context
                )

            missatge_ori = self.read(
                cursor, uid, clot_vals['id'], ['status']
            )['status']
            if missatge_ori and info:
                info = missatge_ori + '\n\n' + info
            elif missatge_ori:
                info = missatge_ori
            self.write(
                cursor, uid, clot_vals['id'], {'status': info}, context=context
            )

        return True

    def facturar_incident(self, cursor, uid, ids, context=None):
        clot_vals = self.read(cursor, uid, ids, ['state'], context=context)
        for clot in clot_vals:
            if clot['state'] == _FACTURAT_INCIDENT:
                self.write(
                    cursor, uid, [clot['id']], {
                        'state': 'facturat', 'incidence_checked': True
                    }, context=context
                )

    def get_readings_dict_by_modcon(
            self, cursor, uid, clot_id, data_inici, data_final, context=None
    ):
        """
        Returns the readings for each modcon compressed by the dates.

        :return: {
            modcon_id_1: {
                comptador_id: {
                    periode: {
                        'actual': {
                            'consum': X,
                        },
                        'anterior': {
                            'consum': X,
                        },
                    }.
                },
                ...
            },
            modcon_id_2 : {
                ...
            }
        }
        """
        if context is None:
            context = {}
        ctx = context.copy()

        polissa_obj = self.pool.get('giscegas.polissa')
        modcon_obj = self.pool.get('giscegas.polissa.modcontractual')
        comptador_obj = self.pool.get('giscegas.lectures.comptador')

        clot_vals = self.read(cursor, uid, clot_id, ['polissa_id'])
        modcon_intervals = polissa_obj.get_modcontractual_intervals(
            cursor, uid, clot_vals['polissa_id'][0], data_inici, data_final
        )
        read_modcon = {}
        for modcon_inter in modcon_intervals.values():
            modcon_vals = modcon_obj.read(
                cursor, uid, modcon_inter['id'], ['tarifa']
            )
            tarifa_id = modcon_vals['tarifa'][0]
            data_inici_f = max(data_inici, modcon_inter['dates'][0])
            data_final_f = min(data_final, modcon_inter['dates'][1])
            c_actius = polissa_obj.comptadors_actius(
                cursor, uid, clot_vals['polissa_id'][0], data_inici_f,
                data_final_f, order='data_alta asc'
            )

            lects_compt = {}
            for compt_id in c_actius:
                ctx.update({'fins_lectura_fact': data_final})
                lects_compt[compt_id] = comptador_obj.get_lectures_per_facturar(
                    cursor, uid, compt_id, tarifa_id, context=ctx
                )
            read_modcon[modcon_inter['id']] = lects_compt

        return read_modcon

    def get_consume(self, cursor, uid, clot_id, data_inici, data_fi):
        polissa_obj = self.pool.get('giscegas.polissa')

        clot_vals = self.read(cursor, uid, clot_id, ['polissa_id'])
        modcon_inter = polissa_obj.get_modcontractual_intervals(
            cursor, uid, clot_vals['polissa_id'][0], data_inici, data_fi
        )
        if len(modcon_inter) != 1:
            raise Exception

        lects_modcon = self.get_readings_dict_by_modcon(
            cursor, uid, clot_id, data_inici, data_fi
        )

        consum_total = 0
        consum_periodes = {}
        periodes_lect = {}
        for lects_compt in lects_modcon.values():
            for lects in lects_compt.values():
                for pname, lectures in sorted(lects.items()):
                    lectura_act = lectures.get('actual', {})
                    lectura_ant = lectures.get('anterior', {})

                    # Acumulem consums per fer el check mes tard
                    consum_incremental = (lectura_act and
                                          lectura_act.get('consum', 0))
                    consum_total += consum_incremental

                    consum_periodes.setdefault(pname, 0)
                    consum_periodes[pname] += consum_incremental

                    # Guardem els periodes de les diferents lectures
                    if lectura_act.get('name', False):
                        data_lectura = lectura_act['name']
                        periodes_lect.setdefault(data_lectura, [])
                        periodes_lect[data_lectura].append(pname)

        return consum_total, consum_periodes, periodes_lect

    def validar_mandato_modcontractual(self, cursor, uid, modcontractual,
                                       data_final, context=None):
        """ Comprova que cada modificació contractual te associat un
        " mandato correcte:
        " * No està caducat
        " * El NIF de pagador == NIF Mandato
        " * IBAN pagament == IBAN mandato
        """
        errors = []
        mandate_obj = self.pool.get('payment.mandate')
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        polissa_obj = self.pool.get('giscegas.polissa')

        if not context:
            context = {}

        ctx = context.copy()
        modcon_date = datetime.strptime(modcontractual.data_final, "%Y-%m-%d")
        if modcon_date < datetime.today():
            pdate = modcontractual.data_final
        else:
            pdate = datetime.today().strftime("%Y-%m-%d")
        ctx.update({'date': pdate})

        polissa = polissa_obj.browse(cursor, uid, modcontractual.polissa_id.id,
                                     context=ctx)
        if not polissa.bank:
            return []
        
        #Search for mandate
        mandate_id = factura_obj.search_mandate(
            cursor, uid, polissa, context=context
        )
        if not mandate_id:
            errors.append(
                u"[V015]" + _(
                    u"No hay mandato asociado a la modificación contractual "
                    u"%s del contrato %s"
                ) % (modcontractual.name, polissa.name)
            )
        else:
            mandate = mandate_obj.browse(cursor, uid, mandate_id, context)
            if modcontractual.pagador.vat != mandate.debtor_vat:
                errors.append(
                    u"[V015]" + _(
                        u"El NIF del mandato (%s) i el del pagador "
                        u"(%s) no concorden"
                    ) % (mandate.debtor_vat, modcontractual.pagador.vat)
                )
            if mandate.date > data_final:
                errors.append(u"[V015]" + _(u"El mandato es futuro"))
            if mandate.date_end and mandate.date_end < data_final:
                errors.append(u"[V015]" + _(u"El mandato está caducado"))
        return errors

    # Helpers stuff
    def validar(self, cursor, uid, ids, context=None):
        """Aquesta funció ens validarà el contracte d'aquest lot.
        """
        if not context:
            context = {}
        ctx = context.copy()

        for clot_id in ids:
            self.validate_individual(cursor, uid, clot_id, context=ctx)

        return True

    def validate_individual(self, cursor, uid, clot_id, context=None):
        """
        Validates a contracte_lot
        :param clot_id:
        :param context:
        :return: True or False
        """
        if context is None:
            context = {}
        modcon_obj = self.pool.get('giscegas.polissa.modcontractual')
        compta_obj = self.pool.get('giscegas.lectures.comptador')
        validador_obj = self.pool.get(
            'giscegas.facturacio.validation.validator'
        )
        cfg = self.pool.get('res.config')
        # Arrepleguem variables de configuracio
        avis_no_lectures = int(
            cfg.get(cursor, uid, 'giscegas_avis_no_lectures', '1')
        )
        fact_check_consum = eval(
            cfg.get(cursor, uid, 'giscegas_fact_check_consum', '{}')
        )
        fact_check_incomplete = int(
            cfg.get(cursor, uid, 'giscegas_fact_check_incomplete', '0')
        )
        fact_check_tancament = int(
            cfg.get(cursor, uid, 'giscegas_fact_check_tancament', '0')
        )
        fact_check_mandato = int(
            cfg.get(cursor, uid, 'giscegas_fact_check_mandato', '0')
        )

        ctx = context.copy()
        fields_to_read = [
            'id', 'skip_validation', 'state', 'polissa_id', 'lot_id'
        ]
        q = OOQuery(self, cursor, uid)
        q_sql = q.select(fields_to_read, for_=For('UPDATE', nowait=True)).where(
            [
                ('id', '=', clot_id)
            ])
        cursor.execute(*q_sql)
        clot_vals = cursor.dictfetchone()

        if clot_vals['skip_validation'] or clot_vals['state'] in ('facturat', 'finalitzat'):
            return False
        # Posem l'status en blanc al començar a validar
        status = []
        pol_obj = self.pool.get("giscegas.polissa")
        polissa = pol_obj.browse(cursor, uid, clot_vals['polissa_id'], context)
        lid = clot_vals['lot_id']
        if not int(cfg.get(cursor, uid, 'giscegas_inici_final_use_lot', '1')):
            lid = False
        # Validation rescinded contract is totally invoiced
        if polissa.state == 'baixa':
            data_baixa = polissa.data_baixa
            if polissa.data_baixa and polissa.data_ultima_lectura >= data_baixa:
                status.append(
                    _(u"[V000] Contracte de baixa el {} ja facturat "
                      u"fins {}".format(data_baixa, polissa.data_ultima_lectura)
                    )
                )

        data_inici, data_final = polissa.get_inici_final_a_facturar(
            use_lot=lid, context={'validacio': True}
        )
        if data_final < data_inici and not lid:
            status.append(_(u"[V003] No té lectures entrades"))
        intervals = polissa.get_modcontractual_intervals(data_inici, data_final)
        mod_ids = []
        mod_types = {}
        mod_dates = {}
        # Ordenem els ids de les modifificcions contractuals per les dates
        # de tall
        if not intervals:
            status.append(_(u"[V007] No té cap interval a facturar"))

        for mod_data in sorted(intervals.keys()):
            mod_id = intervals[mod_data]['id']
            mod_ids.append(mod_id)
            # Afegim quin tipus de modificació és per aqueseta modificació
            # contractual
            mod_types[mod_id] = intervals[mod_data]['changes']
            # Interval real que duren amb possibilitat de més d'una
            # modificació contractual
            mod_dates[mod_id] = intervals[mod_data]['dates']

        for modcontractual in modcon_obj.browse(cursor, uid, mod_ids, context):
            mod_id = modcontractual.id
            data_inici_periode_f = max(data_inici, mod_dates[mod_id][0])
            data_final_periode_f = min(data_final, mod_dates[mod_id][1])

            if int(cfg.get(cursor, uid, 'giscegas_inici_final_use_lot', '1')):
                data_inici, data_final = polissa.get_inici_final_a_facturar(
                    use_lot=lid
                )

            if modcontractual.polissa_id.active and len(mod_ids) == 1:
                data_final_periode_f = data_final

            c_actius = polissa.comptadors_actius(
                data_inici_periode_f, data_final_periode_f,
                order='data_alta asc'
            )

            if fact_check_mandato:
                # Comprovam els mandatos associats a cada modificació
                mandato_status = self.validar_mandato_modcontractual(
                    cursor, uid, modcontractual, data_final_periode_f,
                    context=context
                )
                status.extend(mandato_status)

            if not c_actius:
                status.append(u"[V005]" + _(u"No té cap comptador actiu."))
            # Ha de tenir lectura anterir i lectura actual pels comptadors
            # actius
            tid = modcontractual.tarifa.id
            consum_total = 0
            consum_periodes = {}
            periodes_lect = {}

            if fact_check_incomplete:
                periodes_var = [
                    periode.name
                    for periode in modcontractual.tarifa.periodes
                    if periode.tipus == 'te'
                ]

            for compt in compta_obj.browse(cursor, uid, c_actius):
                # El métode get_inici_final_a_facturar no té en compte que la
                # lectura inicial de la pólissa/modcon comença el dia anterior a
                # l'activació. Per tant ara restem 1 dia a les dates que estem
                # utilitzant
                data_inici_periode_f2 = (
                    datetime.strptime(data_inici_periode_f, "%Y-%m-%d") - timedelta( days=1)
                ).strftime("%Y-%m-%d")
                ctx.update({
                    'fins_lectura_fact': data_final,
                    'ult_lectura_fact': data_inici_periode_f2
                })
                lects = compt.get_lectures_per_facturar(
                    tid, context=ctx
                )
                for pname, lectures in sorted(lects.items()):
                    lectura_act = lectures.get('actual', {})
                    lectura_ant = lectures.get('anterior', {})
                    #Acumulem consums per fer el check mes tard
                    consum_incremental = (
                        lectura_act and lectura_act.get('consum', 0)
                    )
                    consum_total += consum_incremental

                    consum_periodes.setdefault(pname, 0)
                    consum_periodes[pname] += consum_incremental

                    #Guardem els periodes de les diferents lectures
                    if lectura_act.get('name', False):
                        data_lectura = lectura_act['name']
                        periodes_lect.setdefault(data_lectura, [])
                        periodes_lect[data_lectura].append(pname)

                    # No hi ha lectura anterir
                    if (lectura_act and not lectura_ant and avis_no_lectures):
                        status.append(
                            u"[V002]" + _(
                                u"* Comptador %s | "
                                u"Periode %s | "
                                u"No té lectura anterior"
                            ) % (compt.name, pname))
                    elif (lectura_act and lectura_ant
                          and lectura_ant['name'] == lectura_act['name']
                          and avis_no_lectures):
                            # Les dues lectures són del mateix dia
                            # (cas energia 0)
                            status.append(
                                u"[V013]" + _(
                                    u"* Comptador %s | "
                                    u"Periode %s | La lectura "
                                    u"anterior i la lectura actual "
                                    u"són iguals. "
                                    u"Possible primera lectura"
                                ) % (compt.name, pname))
                    # No hi ha lectura actual
                    if not lectura_act and avis_no_lectures:
                        status.append(
                            u"[V003]" + _(
                                u"* Comptador %s | "
                                u"Periode %s | No té lectura actual"
                            ) % (compt.name, pname))
                    # Comprovem si actual < anterior (encara que no
                    # doni negatiu.
                    avis_menor = (
                        lectura_act.get('lectura', 0) + lectura_act.get('ajust', 0) <
                        lectura_ant.get('lectura', 0)
                    )
                    if avis_menor:
                        # Estem en gir si el consum afegit a
                        # lectura anterior és superior a gir
                        es_gir = ((lectura_ant.get('lectura', 0) +
                                   lectura_act.get('consum', 0)) >
                                  compt.giro)
                        if es_gir:
                            txt = u"[V001]" + _(
                                u"* Comptador %s | "
                                u"Periode %s | La lectura actual "
                                u"és inferior a l'anterior per "
                                u"possible volta de comptador. "
                                u"Cal revisar si correspon "
                                u"saltar validació"
                            )
                        else:
                            txt = u"[V006]" + _(
                                u"* Comptador %s | "
                                u"Periode %s | La lectura actual "
                                u"és inferior a l'anterior"
                            )

                        status.append(txt % (compt.name, pname))

                    # Consum negatiu
                    consum = (lectura_act.get('lectura', 0) -
                              lectura_ant.get('lectura', 0))
                    if consum < 0:
                        consum += compt.giro
                    consum += lectura_act.get('ajust', 0)
                    con_n = min(lectura_act.get('consum', 0), consum)
                    if con_n < 0:
                        status.append(
                            u"[V006]" + _(
                                u"* Comptador %s | "
                                u"Periode %s | Té consum negatiu %s"
                            ) % (compt.name, pname, con_n)
                        )
                if (fact_check_tancament and not compt.active
                    and compt.data_baixa
                    and compt.data_baixa not in periodes_lect.keys()):
                    date = datetime.strftime(
                        datetime.strptime(compt.data_baixa,'%Y-%m-%d'),
                        '%d/%m/%Y'
                    )
                    status.append(
                        u"[V014]" + _(
                            u"* Comptador %s | Baixa | "
                            u"Falta Lectura de tancament amb data %s"
                        ) % (compt.name, date))

                if fact_check_incomplete:
                    periodes_var_len = len(periodes_var)
                    for data, periodes in periodes_lect.items():
                        if periodes_var_len != len(periodes):
                            missing = sorted(
                                list(set(periodes_var) - set(periodes))
                            )
                            date = datetime.strftime(
                                datetime.strptime(data, '%Y-%m-%d'),
                                '%d/%m/%Y'
                            )
                            status.append(
                                u"[V012]" + _(
                                    u"* Comptador %s | "
                                    u"Lectura amb data %s incompleta. "
                                    u"Falta %s"
                                ) % (
                                    compt.name, date,
                                    ','.join(missing)
                                )
                            )

            tname = modcontractual.tarifa.name
            if tname in fact_check_consum and consum_total >= fact_check_consum[tname]:
                status.append(
                    u"[V010]" + _(
                        u"* El consum és superior al límit. Consum %s"
                    ) % (consum_total)
                )

            validador_obj.validate_clot(
                cursor, uid, clot_vals['id'], data_inici_periode_f,
                data_final_periode_f, context=context
            )

            status += validador_obj.get_clot_warnings_texts(
                cursor, uid, clot_vals['id'], context=context
            )

        status = '\n'.join(status) or False
        self.write(cursor, uid, clot_vals['id'], {'status': status})

    # Workflow stuff
    def ws_signal_esborrany(self, cursor, uid, ids):
        """Trigger per esborrany.
        """
        wf_service = netsvc.LocalService('workflow')
        for cl_id in ids:
            wf_service.trg_validate(uid, 'giscegas.facturacio.contracte_lot',
                                    cl_id, 'esborrany', cursor)
        return True

    def wkf_esborrany(self, cursor, uid, ids, context=None):
        """Estat esborrany del workflow.
        """
        self.write(cursor, uid, ids, {'state': 'esborrany'})
        #Transicio a obert
        self.wkf_obert(cursor, uid, ids, context)
        return True

    def cnd_obrir(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        #Si ens pasen al context la variable from_lot
        #amb valor true, vol dir que la crida la ha feta
        #la funcio obrir del lot de facturacio
        #i ens podem estalviar la comprovacio
        #de si el lot esta obert o no
        if context.get('from_lot', False):
            return True
        for contracte_lot in self.browse(cursor, uid, ids):
            if contracte_lot.lot_id.state == 'obert':
                return True
            else:
                return False

    @job(queue='validate_lot')
    def validate_contracts(self, cursor, uid, clot_ids, context=None):
        """
        Function to queue contract_lot validation. wkf_obert may not be
￼       "queued" due to job queues and overwritten functions issues
        :param clot_ids: contract_lots to validate
        :param context:
        :return: result of validation
        """
        return self.wkf_obert(
            cursor, uid, clot_ids, context=context
        )

    def wkf_obert(self, cursor, uid, clot_ids, context=None):
        """Estat obert del workflow.
        """
        if not context:
            context = {}
        if not isinstance(clot_ids, (list,tuple)):
            clot_ids = [clot_ids]
        for clot_id in clot_ids:
            # TODO: Marcar la pòlissa per facturar
            if self.cnd_obrir(cursor, uid, [clot_id], context):
                self.write(
                    cursor, uid, [clot_id], {
                        'state': 'obert', 'incidence_checked': False
                    }
                )
                # Validem
                if context.get('validate', True):
                    self.validar(cursor, uid, [clot_id], context)
                #Transicio a lectures
                if not context.get('from_lot', False):
                    self.wkf_lectures(cursor, uid, [clot_id], context)

        return True

    def cnd_lectures(self, cursor, uid, ids, context=None):
        """Condició per passar a l'estat lectures.
        """
        for clot in self.browse(cursor, uid, ids):
            if clot.status and not clot.skip_validation:
                return False
        return True

    def wkf_lectures(self, cursor, uid, ids, context=None):
        """Estat lectures.
        """
        for clot_id in ids:
            if self.cnd_lectures(cursor, uid, [clot_id], context):
                self.write(
                    cursor, uid, [clot_id], {
                        'state': 'lectures', 'tipus_facturacio': 'lectures'
                    }
                )
                #Transicio a facturar
                self.wkf_facturar(cursor, uid, [clot_id], context)
        return True

    def wkf_facturar(self, cursor, uid, ids, context=None):
        """Estat de facturar.
        """
        self.write(cursor, uid, ids, {'state': 'facturar'})
        if context.get("validar_i_facturar"):
            self.facturar(cursor, uid, ids, context=context)
        return True

    def wkf_facturant(self, cursor, uid, ids, context=None):
        """Estat facturant.
        """
        self.write(cursor, uid, ids, {'state': 'facturant'})
        return True

    def wkf_facturat(self, cursor, uid, ids, context=None):
        """Estat acabat la facturació.
        """
        # TODO: Marcar la pòlissa com a facturada
        self.write(cursor, uid, ids, {'state': 'facturat'})
        self.update_nfactures(cursor, uid, ids, context)
        return True

    def wkf_finalitzat(self, cursor, uid, ids, context=None):
        """Estat finalitzat de la facturació d'un contracte.
        """
        self.write(cursor, uid, ids, {'state': 'finalitzat'})
        return True

    _states_selection = [
        ('esborrany', 'Borrador'),
        ('obert', 'Abierto'),
        ('lectures', 'Facturación por lecturas'),
        ('facturar', 'A punto para facturar'),
        ('facturant', 'Facturando'),
        ('facturat', 'Facturado'),
        ('facturat_incident', 'Facturado con incidencias'),
        ('finalitzat', 'Finalizado')
    ]


    _columns = {
        'polissa_id': fields.many2one(
            'giscegas.polissa', 'Contrato', required=True
        ),
        'lot_id': fields.many2one(
            'giscegas.facturacio.lot', 'Lote', required=True, select=True
        ),
        'tipus_facturacio': fields.selection(
            [('lectures', 'Lectures')], "Tipo de facturación", readonly=True
        ),
        'state': fields.selection(
            _states_selection, 'Estado', size=32, required=True, readonly=True
        ),
        'factures_ids': fields.one2many(
            'giscegas.facturacio.contracte_lot.factura', 'contracte_lot_id',
            'Facturas'
        ),
        'n_factures': fields.integer('Facturas generadas'),
        'status': fields.text('Estado', readonly=True),
        'skip_validation': fields.boolean('Saltar validación'),
        'incidence_checked': fields.boolean(
            "Incidencia revisada", select=1,
            help="Indica que se ha marcado como facturado aún habiendo "
                 "facturado un una incidencia previa."
        ),
    }

    _defaults = {
        'tipus_facturacio': lambda *a: 'lectures',
        'state': lambda *a: 'esborrany',
        'skip_validation': lambda *a: 0,
        'n_factures': lambda *a: 0,
        'incidence_checked': lambda *a: 0,
    }


GiscegasFacturacioContracteLot()


class GiscegasFacturacioFactura(osv.osv):
    """Classe base de les factures
    """

    _name = 'giscegas.facturacio.factura'
    _inherits = {'account.invoice': 'invoice_id'}

    def name_get(self, cursor, uid, ids, context=None):
        res = []
        if len(ids):
            types = {
                'out_invoice': 'CI:',
                'in_invoice': 'SI:',
                'out_refund': 'CR:',
                'in_refund': 'SR:',
            }
            for factura in self.read(
                    cursor, uid, ids, ['type', 'number', 'name', 'origin'],
                    context=context
            ):
                number = ''
                fact_type = factura['type']
                if fact_type.startswith('in'):
                    number = factura['origin'] or factura['name']
                elif fact_type.startswith('out'):
                    number = factura['number']

                invoice_desc = (
                    factura['id'], '{prefix} {number}'.format(
                        prefix=types[fact_type], number=number
                    )
                )
                res.append(invoice_desc)
        return res

    def _get_base_ids(self, cursor, uid, ids, context=None):
        """Definim una funció on li passarem els ids de la nostra factura i ens
        retornarà els ids de les factures bases relacionades
        """
        inv_obj = self.pool.get('account.invoice')
        res = [
            a['invoice_id'][0]
            for a in self.read(cursor, uid, ids, ['invoice_id'], context)
        ]
        return inv_obj.search(cursor, uid, [('id', 'in', res)], order='id asc')

    def onchange_currency_id(self, cursor, uid, ids, curr_id):
        """Sobreescrivim els mètodes 'on_change'
        per tal que els agafi el model account_invoice"""

        ids = self._get_base_ids(cursor, uid, ids)
        res = self.pool.get('account.invoice').onchange_currency_id(cursor,
                                                                    uid,
                                                                    ids,
                                                                    curr_id)
        return res

    def onchange_invoice_line(self, cursor, uid, ids, lines):
        ids = self._get_base_ids(cursor, uid, ids)
        res = self.pool.get('account.invoice').onchange_invoice_line(cursor,
                                                                     uid, ids,
                                                                     lines)
        return res

    def onchange_partner_bank(self, cursor, user, ids, partner_bank):
        ids = self._get_base_ids(cursor, user, ids)
        res = self.pool.get('account.invoice').onchange_partner_bank(
                                                cursor, user, ids,
                                                partner_bank)
        return res

    # TODO: Quan es canviï de partner s'ha d'habilitar un domini sobre el camp
    # pòlissa per tal que només deixi escollir les pòlisses que aquest partner
    # sigui el pagador
    def onchange_partner_id(self, cursor, uid, ids, type, partner_id,
                            date_invoice=False, payment_term=False,
                            partner_bank=False):
        ids = self._get_base_ids(cursor, uid, ids)
        res = self.pool.get('account.invoice').onchange_partner_id(
                                                cursor, uid, ids, type,
                                                partner_id, date_invoice,
                                                payment_term, partner_bank)
        return res

    def onchange_payment_term_date_invoice(self, cursor, uid, ids,
                                           payment_term_id, date_invoice):
        ids = self._get_base_ids(cursor, uid, ids)
        res = \
        self.pool.get('account.invoice').onchange_payment_term_date_invoice(
                                            cursor, uid, ids, payment_term_id,
                                            date_invoice)
        return res

    def onchange_payment_type(self, cursor, uid, ids, payment_type,
                              partner_id, result=None):
        ids = self._get_base_ids(cursor, uid, ids)
        res = self.pool.get('account.invoice').onchange_payment_type(cursor,
                                                uid, ids, payment_type,
                                                partner_id, result)
        return res

    # Sobreescrivim el mètodes dels buttons de la factura de l'ERP original
    def button_reset_taxes(self, cursor, uid, ids, context=None):
        ids = self._get_base_ids(cursor, uid, ids, context)
        search_params = [('invoice_id', 'in', ids)]
        lines_obj = self.pool.get('account.invoice.line')
        linies = lines_obj.search(cursor, uid, search_params)
        for linia in lines_obj.browse(cursor, uid, linies):
            linia.write({}, context)
        self.pool.get('account.invoice').button_reset_taxes(cursor, uid, ids,
                                                            context)
        return True

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}

        factura_obj = self.pool.get('giscegas.facturacio.factura')

        to_write = {}
        if not vals.get('rectificative_type', False):
            if vals.get('tipo_rectificadora', False):
                vals['rectificative_type'] = vals['tipo_rectificadora']
        else:
            to_write['rectificative_type'] = vals['rectificative_type']

        if not vals.get('rectifying_id', False):
            if vals.get('ref', False):
                rectifying = factura_obj.read(
                    cursor, uid, vals['ref'], ['invoice_id']
                )['invoice_id'][0]
                vals['rectifying_id'] = rectifying
        else:
            to_write['rectifying_id'] = vals['rectifying_id']
        res = super(GiscegasFacturacioFactura, self).create(
            cursor, uid, vals, context=context
        )
        if to_write:
            # If we have set rectifying_id or rectificative_type we need to
            # re-write them because the triggers for tipo_rectificadora and ref
            # are not activated if we don't do this, meaning that they don't
            # have any value. (Using default values does work)
            self.write(cursor, uid, res, to_write)

        return res

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}

        factura_obj = self.pool.get('giscegas.facturacio.factura')

        if not vals.get('rectificative_type', False):
            if vals.get('tipo_rectificadora', False):
                vals['rectificative_type'] = vals['tipo_rectificadora']

        if not vals.get('rectifying_id', False):
            if vals.get('ref', False):
                rectifying = factura_obj.read(
                    cursor, uid, vals['ref'], ['invoice_id']
                )['invoice_id'][0]
                vals['rectifying_id'] = rectifying

        return super(GiscegasFacturacioFactura, self).write(
            cursor, uid, ids, vals, context
        )

    def copy_data(self, cursor, uid, ids, default=None, context=None):
        if default is None:
            default = {}

        if not default.get('lot_facturacio', False):
            default.update({'lot_facturacio': False})

        data, x = super(GiscegasFacturacioFactura, self).copy_data(
            cursor, uid, ids, default, context)
        data.update({
            'origin': False,
            'origin_date_invoice': False,
            'reference': False,
            'period_id': False,
            'state': 'draft',
            'number': False,
            'move_id': False, 'move_name': False, 'move_lines': []
        })
        if 'date_invoice' not in data:
            data['date_invoice'] = False
        if 'date_due' not in data:
            data['date_due'] = False
        return data, x

    def copy(self, cursor, uid, factura_id, default=None, context=None):
        """Copiem una factura.
        """
        if default is None:
            default = {}

        factura_obj = self.pool.get('giscegas.facturacio.factura')

        default = default.copy()
        default.update({'invoice_line': [], 'state': 'draft'})

        if not default.get('rectificative_type', False):
            if default.get('tipo_rectificadora', False):
                default['rectificative_type'] = default['tipo_rectificadora']

        if not default.get('rectifying_id', False):
            if default.get('ref', False):
                rectifying = factura_obj.read(
                    cursor, uid, default['ref'], ['invoice_id']
                )['invoice_id'][0]
                default['rectifying_id'] = rectifying

        res_id = super(GiscegasFacturacioFactura,
                       self).copy(cursor, uid, factura_id, default, context)
        return res_id

    def unlink(self, cursor, uid, ids, context=None):
        """Mètode que elimina la factura comptable associada a aquesta.

        S'han d'eliminar les factures compatbles (aka "les de l'ERP") per tal
        que no quedi cap penjada quan s'eliminen les factures d'electricitat.
        """
        lot_obj = self.pool.get('giscegas.facturacio.lot')
        clot_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        polissa_obj = self.pool.get('giscegas.polissa')
        lots_to_update = []
        clots_to_update = []
        ids_to_delete = []
        # Trigger per passar-lo altre cop a esborrany
        fields_to_read = ['polissa_id', 'lot_facturacio', 'tipo_factura',
                          'tipo_rectificadora', 'type']
        ctx = {'active_test': False}
        for factura in self.read(cursor, uid, ids, fields_to_read, context):
            if (not factura['lot_facturacio']
                    or factura['tipo_rectificadora'] != 'N'
                    or factura['type'] != 'out_invoice'):
                continue
            lot_id = factura['lot_facturacio'][0]
            lot = lot_obj.browse(cursor, uid, lot_id)
            polissa_id = factura['polissa_id'][0]
            # Comprovem si hi ha més factures en estat esborrany per aquest
            # lot i pòlissa. Fins que no s'hagin eliminat totes no tornarem a
            # obrir la pòlissa del contracte-lot
            search_params = [
                ('lot_facturacio.id', '=', lot_id),
                ('type', '=', factura['type']),
                ('polissa_id.id', '=', polissa_id),
                ('tipo_factura', '=', factura['tipo_factura']),
                ('id', 'not in', ids_to_delete)
            ]
            if self.search_count(cursor, uid, search_params, context=ctx) > 1:
                ids_to_delete.append(factura['id'])
                continue
            search_params = [
                ('lot_id.id', '=', lot_id),
                ('polissa_id.id', '=', polissa_id)
            ]
            clot_ids = clot_obj.search(cursor, uid, search_params, context=ctx)
            if clot_ids:
                #Marquem el contracte lot per passar a esborrany
                clots_to_update.extend(clot_ids)
                polissa_obj.write(cursor, uid, [polissa_id],
                                  {'lot_facturacio': lot_id},
                                  context={'sync': False})
                if lot.state == 'tancat' and lot_id not in lots_to_update:
                    lots_to_update.append(lot_id)

        # Busquem l'id relacionat de la factuara d'energia amb la factura base
        ids_inv = self._get_base_ids(cursor, uid, ids, context)
        if len(ids_inv):
            self.pool.get('account.invoice').unlink(cursor, uid, ids_inv,
                                                    context)
        res = super(GiscegasFacturacioFactura, self).unlink(cursor, uid, ids,
                                                              context)
        #Actualitzem l'estat dels contracte lots pendents
        clot_obj.wkf_esborrany(cursor, uid, clots_to_update)
        clot_obj.update_nfactures(cursor, uid, clots_to_update)
        # Passem els lots a 'obert' una vegada esborrades les factures
        lot_obj.wkf_obert(cursor, uid, lots_to_update, context)

        return res

    # Hem de crear les accions pels buttons que per defecte són workflow, per
    # tal que siguin del tipus object i des del nostre model activar les
    # transicions de la factura base
    def invoice_proforma2(self, cursor, uid, ids, context=None):
        """Passem la factura a proforma.
        """
        ids = self._get_base_ids(cursor, uid, ids, context)
        wf_service = netsvc.LocalService("workflow")
        for id in ids:
            wf_service.trg_validate(uid, 'account.invoice', id,
                                    'invoice_proforma2', cursor)
        return True

    def invoice_cancel(self, cursor, uid, ids, context=None):
        """Cancel·lem la factura.
        """
        ids = self._get_base_ids(cursor, uid, ids, context)
        wf_service = netsvc.LocalService("workflow")
        for id in ids:
            wf_service.trg_validate(uid, 'account.invoice', id,
                                    'invoice_cancel', cursor)
        return True

    def change_date_invoice(self, cursor, uid, ids, context=None):
        cfg_obj = self.pool.get('res.config')
        current_date = datetime.now().strftime('%Y-%m-%d')
        fields_to_read = [
            'type', 'date_invoice', 'state', 'payment_term', 'number'
        ]
        data_original = self.read(
            cursor, uid, ids, fields_to_read, context=context)
        change_date_in_invoice = int(
            cfg_obj.get(cursor, uid, 'giscegas_fact_change_date_in_invoice','0')
        )
        change_date_out_invoice = int(
            cfg_obj.get(cursor, uid, 'giscegas_fact_change_date_out_invoice', '0')
        )
        for original in data_original:
            # Change only when invoice doesn't have number
            if not original['number']:
                value = {}
                if (original['type'] in ['out_invoice', 'out_refund']
                        and change_date_out_invoice):
                    value.update({'date_invoice': current_date})
                    if original['payment_term']:
                        res = self.onchange_payment_term_date_invoice(
                            cursor, uid, [original['id']],
                            original['payment_term'][0], current_date)
                        if res and res['value']:
                            value.update(res['value'])
                elif (original['type'] in ['in_invoice', 'in_refund']
                        and change_date_in_invoice):
                    value.update({'date_invoice': current_date})
                if value:
                    self.write(cursor, uid, ids, value)
        return True

    def invoice_open(self, cursor, uid, ids, context=None):
        """Obrim la factura.
        """
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        wf_service = netsvc.LocalService("workflow")
        clot_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        # Revisem si hem de modificar la data de la factura
        self.change_date_invoice(cursor, uid, ids, context=context)

        # Passem a finalitzar la facturació del contracte ja que confirmem
        # la factura
        open_fact_ids = []
        for factura in self.browse(cursor, uid, ids, context):
            if factura.tipo_rectificadora != 'BRA':
                open_fact_ids.append(factura.id)
            if not factura.lot_facturacio or factura.tipo_rectificadora != 'N'\
                    or factura.type != 'out_invoice':
                # Pot ser que una factura de liquidació no s'hagi associat
                # a un lot de facturació
                continue
            search_params = [
                ('polissa_id.id', '=', factura.polissa_id.id),
                ('lot_id.id', '=', factura.lot_facturacio.id)
            ]
            clot_sql = clot_obj.q(cursor, uid).select(
                ['id', 'state'], only_active=False
            ).where(search_params)
            cursor.execute(*clot_sql)
            clot_data = [x for x in cursor.dictfetchall()]
            if clot_data:
                clot_data = clot_data[0]
                if clot_data['state'] == _FACTURAT_INCIDENT:
                    open_fact_ids.append(open_fact_ids.index(factura.id))
                    if len(ids) == 1:
                        raise osv.except_osv(
                            'Error',
                            _(
                                u"Esta factura está marcada como a incidencia "
                                u"en el lote de facturación {} "
                                u"por el contrato {}.".format(
                                    factura.lot_facturacio.name,
                                    factura.polissa_id.name
                                )
                              )
                        )
                    else:
                        continue
                clot_obj.wkf_finalitzat(cursor, uid, [clot_data['id']], context)

            if (factura.polissa_id.lot_facturacio and
                  factura.polissa_id.lot_facturacio == factura.lot_facturacio):
                data_final = factura.lot_facturacio.data_final
                factura.polissa_id.assignar_seguent_lot({'data_final': data_final})

        # Cridem l'acció base
        ids = self._get_base_ids(cursor, uid, open_fact_ids, context)
        for fact_id in ids:
            wf_service.trg_validate(
                uid, 'account.invoice', fact_id, 'invoice_open', cursor
            )
        return True

    def action_cancel_draft(self, cursor, uid, ids, *args):
        """Cridem la funció base 'action_cancel_draft.
        """
        ids = self._get_base_ids(cursor, uid, ids)
        self.pool.get('account.invoice').action_cancel_draft(cursor, uid, ids)
        return True

    def onchange_type(self, cursor, uid, ids, type_, context=None):
        """Es canvia el tipus de llista de preu segons el tipus de factura.

        Donat el tipus de factura in/out (compra/venda) la llista de preus
        ha de canviar també entre purchase/sale.
        """
        res = {'value': {}, 'domain': {}}
        if type_ in ('out_invoice', 'out_refund'):
            res['domain'].update({'llista_preu': [('type', '=', 'sale')]})
        else:
            res['domain'].update({'llista_preu': [('type', '=', 'purchase')]})
        return res

    def search_mandate(self, cursor, uid, polissa, context=None):

        mandate_obj = self.pool.get('payment.mandate')

        #Search for mandate
        reference = 'giscegas.polissa,%s' % polissa.id
        search_params = [('reference', '=', reference),
                         ('debtor_iban', '=', polissa.bank.iban)]
        mandate_ids = mandate_obj.search(cursor, uid, search_params, limit=1)
        if mandate_ids:
            return mandate_ids[0]
        return False

    def onchange_polissa(self, cursor, uid, ids, polissa_id, type_,
                         context=None):
        """S'actualitzen tots els camps possibles
        segons la pòlissa seleccionada"""
        if not context:
            context = {}
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if polissa_id:
            polissa = self.pool.get('giscegas.polissa').browse(cursor, uid,
                                                                polissa_id,
                                                                context)
            if type_ in ('out_invoice', 'out_refund'):
                # En cascada hauríem d'aplicar els canvis que s'apliquen quan
                # canviem de partner
                ids = self._get_base_ids(cursor, uid, ids, context)
                partner_id = polissa.pagador.id
                payment_term = polissa.pagador.property_payment_term and \
                    polissa.pagador.property_payment_term.id or False
                partner_bank = polissa.bank and polissa.bank.id or False
                if partner_bank:
                    mandate_id = self.search_mandate(cursor, uid, polissa)
                else:
                    mandate_id = False
                upd = self.onchange_partner_id(cursor, uid, ids, type_,
                                               partner_id,
                                               payment_term, partner_bank)
                res['value'].update(upd['value'])

                # Fem el nostre últim perquè les adreces de facturació i
                # notificació són les de la pòlissa i no les que ens hagin
                # vingut donades degut al canvi de partner.
                res['value'].update({
                     'partner_id': partner_id,
                     'address_invoice_id': polissa.direccio_pagament.id,
                     'address_contact_id': polissa.direccio_notificacio.id,
                     'llista_preu': polissa.llista_preu.id,
                     'payment_type': (
                         polissa.tipo_pago and polissa.tipo_pago.id or False
                     ),
                     'partner_bank': partner_bank,
                     'mandate_id': mandate_id,
                     'tarifa_acces_id': polissa.tarifa.id,
                     'payment_term': payment_term,
                     'cups_id': polissa.cups.id,
                     'facturacio': polissa.facturacio,
                     'name': polissa.name,
                })
            else:
                ids = self._get_base_ids(cursor, uid, ids, context)
                # Creem un objecte ref per accedir a tots els camps de la
                # distribuidora
                ref = polissa.cups.distribuidora_id
                partner_id = ref.id
                payment_term = ref.property_payment_term and \
                    ref.property_payment_term.id or False
                # TODO buscar funcio onchange_partner_bank
                partner_bank = False  # ref.bank and ref.bank.id or False
                upd = self.onchange_partner_id(cursor, uid, ids, type_,
                                               partner_id, payment_term,
                                               partner_bank)
                res['value'].update(upd['value'])

                # Fem el nostre últim perquè les adreces de facturació i
                # notificació són les de la pòlissa i no les que ens hagin
                # vingut donades degut al canvi de partner.
                res['value'].update({
                    'partner_id': partner_id,
                    'llista_preu': ref.property_product_pricelist_purchase.id,
                    'payment_type': ref.payment_type_supplier.id,
                    'tarifa_acces_id': polissa.tarifa.id,
                    'payment_term': (
                        ref.property_payment_term and
                        ref.property_payment_term.id or False
                    ),
                    'cups_id': polissa.cups.id,
                    'facturacio': polissa.facturacio,
                })
        else:
            res['value'].update({
                'partner_id': False,
                'address_invoice_id': False,
                'address_contact_id': False,
                'llista_preu': False,
                'payment_type': False,
                'partner_bank': False,
                'payment_term': False,
                'tarifa_acces_id': False,
                'cups_id': False,
                'facturacio': False
            })
        return res

    def anullar(self, cursor, uid, ids, tipus='A', context=None):
        """Anul·la una factura fent servir el mètode refund (account.invoice).

        Això anula la factura existent, i en crea una de nova d'anul·ladora,
        però haurem de crear les línies de la nova anul·ladora per tal de
        vincular-les a la base.
        """
        invoice_line_obj = self.pool.get('account.invoice.line')
        invoice_obj = self.pool.get('account.invoice')
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        journal_obj = self.pool.get('account.journal')
        extra_obj = self.pool.get('giscegas.facturacio.extra')
        payment_type_obj = self.pool.get('payment.type')
        payment_term_obj = self.pool.get('account.payment.term')
        refund_ids = []
        for factura in self.browse(cursor, uid, ids, context):
            # fem que la data de la factura anul·ladora sigui la què es diu al
            # wizard
            data_factura = context.get('data_factura', False)
            base_id_refund = factura.invoice_id.refund(
                date=data_factura, rectificative_type=tipus)[0]
            # Eliminem les línies de factura
            search_p = [
                ('invoice_id.id', '=', base_id_refund)
            ]
            l_ids = invoice_line_obj.search(cursor, uid, search_p)
            invoice_line_obj.unlink(cursor, uid, l_ids)
            # Fem la còpia ja amb la nova anul·lada
            default = {'invoice_id': base_id_refund}
            journal_code = factura.journal_id.code.split('.')[0]
            search_p = [
                ('code', '=', '%s.%s' % (journal_code, tipus))
            ]
            journal_id = journal_obj.search(cursor, uid, search_p)
            if not journal_id:
                raise osv.except_osv(
                    'Error', _(u"No se ha encontrado el diario para anuladoras. "
                               u"Tipo %s.%s." % (factura.journal_id.code, tipus))
                )
            journal_id = journal_id[0]
            # Copiem la factura d'electricitat
            id_refund = self.copy(cursor, uid, factura.id, default, context)
            # Id temporal accoint.invoice (per elminar després)
            old_inv_id = self.read(
                cursor, uid, id_refund, ['invoice_id']
            )['invoice_id'][0]
            # Hi assignem el base, referencia i tipus de rectificadora
            vals = {
                'ref': factura.id,
                'tipo_rectificadora': tipus,
                'invoice_id': base_id_refund,
                'journal_id': journal_id
            }
            # calculem nova data de venciment
            if factura.date_due:
                pay_term_id = factura.partner_id.property_payment_term.id
                if pay_term_id:
                    date_due_res = payment_term_obj.compute(
                        cursor, uid, pay_term_id, value=1.0
                    )
                    if (len(date_due_res) and len(date_due_res[0])
                            and date_due_res[0][0]):
                        vals.update({'date_due': date_due_res[0][0]})
            if factura.payment_type.code == 'RECIBO_CSB':
                # Busquem per fer transferència de l'anul·ladora.
                transf = payment_type_obj.search(cursor, uid, [
                    ('code', '=', 'TRANSFERENCIA_CSB')
                ])
                if transf:
                    vals['payment_type'] = transf[0]
                if factura.polissa_id and factura.polissa_id.bank:
                    vals['partner_bank'] = factura.polissa_id.bank.id
            self.write(cursor, uid, [id_refund], vals)
            # Per a cada línia de factura posar invoice_id: base_id_refund
            l_ids = self.read(cursor, uid, [id_refund],
                              ['linia_ids'])[0]['linia_ids']
            linia_obj.write(cursor, uid, l_ids, {'invoice_id': base_id_refund})
            # Eliminem la còpia d'account.invoice.
            invoice_obj.unlink(cursor, uid, [old_inv_id])
            # Recalculem taxes
            self.button_reset_taxes(cursor, uid, [id_refund], context)
            refund_ids.append(id_refund)
            # Refund extra lines
            extra_obj.refund(cursor, uid, factura.id, id_refund)
        return refund_ids

    def get_parameter_by_contract(self, cursor, uid, polissa_id, parameter,
                                  from_date=None, to_date=None, context=None):
        if not context:
            context = {}

        search_params = [
            ('polissa_id', '=', polissa_id),
            ('state', '!=', 'draft'),
        ]
        if from_date:
            search_params.append(('data_final', '>', from_date))
        if to_date:
            search_params.append(('data_final', '<', to_date))

        fact_ids = self.search(cursor, uid, search_params)

        fact_vals = self.read(
            cursor, uid, fact_ids,
            [parameter, 'data_inici', 'tipo_rectificadora']
        )
        parameter_by_date = {}
        for param in fact_vals:
            old_param = parameter_by_date.get(param['data_inici'], 0)
            if param['tipo_rectificadora'] in \
                            RECTIFYING_RECTIFICATIVE_INVOICE_TYPES + ['N']:
                new_param = old_param + param[parameter]
            else:
                new_param = old_param - param[parameter]
            parameter_by_date[param['data_inici']] = new_param

        return parameter_by_date

    def get_max_parameter_by_contract(self, cursor, uid, polissa_id, parameter,
                                      from_date=None, to_date=None,
                                      context=None):
        if not context:
            context = {}

        parameter_by_date = self.get_parameter_by_contract(
            cursor, uid, polissa_id, parameter, from_date, to_date,
            context=context
        )

        if parameter_by_date:
            return max(parameter_by_date.values())
        else:
            return 0

    def get_max_consume_by_contract(self, cursor, uid, polissa_id,
                                    from_date=None, to_date=None, context=None):
        if not context:
            context = {}

        return self.get_max_parameter_by_contract(
            cursor, uid, polissa_id, 'consumo_kwh', from_date, to_date, context
        )

    def get_mean_consume_by_contract(self, cursor, uid, polissa_id,
                                     from_date=None, to_date=None,
                                     context=None):
        if not context:
            context = {}

        consume_by_date = self.get_parameter_by_contract(
            cursor, uid, polissa_id, 'consumo_kwh', from_date, to_date,
            context=context
        )

        if consume_by_date:
            consumes = consume_by_date.values()
            return float(sum(consumes))/len(consumes)
        else:
            return 0

    def get_ultima_lectura_facturada(self, cursor, uid, factura, context=None):
        polissa_obj = self.pool.get('giscegas.polissa')
        res = polissa_obj.get_ultima_lectura_facturada(
            cursor, uid, factura.polissa_id.id, factura.data_inici,
            factura.journal_id.code, context=context
        )
        # Si el resultat és la data d'inici de la factura significa que no s'ha
        # trobat cap factura anterior i, per tant, es retorna la data de la
        # lectura inicial de la factura actual
        if res == factura.data_inici and factura.lectures_ids:
            res = min(l.data_anterior for l in factura.lectures_ids)
        return res

    def rectificar_substitucio(
            self, cursor, uid, ids, tipus='RA', context=None
    ):
        config_obj = self.pool.get('res.config')

        if context is None:
            context = {}

        res_ids = []
        if int(config_obj.get(cursor, uid, 'create_BRA', '1')):
            res_ids = self.anullar(cursor, uid, ids, 'BRA', context)

        res_ids += self.create_rectification_invoice(
            cursor, uid, ids, tipus, context
        )

        return res_ids

    def rectificar(self, cursor, uid, ids, tipus='R', context=None):
        """Anul·la la factura i la refactura fent una rectificacdora.
        """
        if not context:
            context = {}
        res_ids = []

        res_ids += self.anullar(cursor, uid, ids, 'B', context)

        res_ids += self.create_rectification_invoice(
            cursor, uid, ids, tipus, context
        )
        return res_ids

    def create_rectification_invoice(self, cursor, uid, ids, tipus='R',
                                     context=None):
        if context is None:
            context = {}

        journal_obj = self.pool.get('account.journal')
        facturador_obj = self.pool.get('giscegas.facturacio.facturador')
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        cl_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        extra_obj = self.pool.get('giscegas.facturacio.extra')

        res_ids = []

        for factura in self.browse(cursor, uid, ids, context):
            tmp_ids = []
            polissa_id = factura.polissa_id.id

            journal_code = factura.journal_id.code.split('.')[0]
            search_p = [
                ('code', '=', '{0}.{1}'.format(journal_code, tipus[0]))
            ]
            journal_id = journal_obj.search(cursor, uid, search_p)
            if not journal_id:
                raise osv.except_osv(
                    'Error', _(
                        u"No se ha encontrado el diario para rectificadoras. "
                        u"Tipo %s.%s." % (factura.journal_id.code, tipus)
                    )
                )
            journal_id = journal_id[0]

            # Si està a un lot és una factura d'energia normal
            if factura.journal_id.code.startswith('GAS'):
                if factura.lot_facturacio:
                    lot_id = factura.lot_facturacio.id
                    search_params = [('lot_id.id', '=', lot_id),
                                     ('polissa_id.id', '=', polissa_id)]
                    cl_ids = cl_obj.search(cursor, uid, search_params,
                                           context={'active_test': False})
                    if not cl_ids:
                        raise osv.except_osv(
                            'Error', _(u"No se ha encontrado la póliza "
                                       u"al lote %s."
                                       % factura.lot_facturacio.name))
                    contracte_lot = cl_obj.browse(cursor, uid, cl_ids[0])
                    method_name = 'fact_via_{0}'.format(
                        contracte_lot.tipus_facturacio
                    )
                    context['lot_id'] = factura.lot_facturacio.id
                else:
                    method_name = 'fact_via_lectures'
                    lot_id = False
                # Busquem la max data de les lectures facturades
                # Si no te lectures, passem el periode de la factura
                ulf = self.get_ultima_lectura_facturada(
                    cursor, uid, factura, context
                )
                flf = factura.data_final
                context.update({'ult_lectura_fact': ulf,
                                'fins_lectura_fact': flf,
                                'date_boe': factura.date_boe,
                                'tipo_rectificadora': tipus,
                                'ref': factura.id,
                                'journal_id': journal_id,
                                })
                tmp_ids += getattr(
                    facturador_obj, method_name
                )(cursor, uid, polissa_id, lot_id, context)

                # Les rectificadores no han de tenir lot
                factura_obj.write(
                    cursor, uid, tmp_ids, {'lot_facturacio': False}
                )
            # Si no està a un lot pot ser una factura d'accessos
            else:
                # Update some vals when copying
                default_vals = {
                    'number': False,
                    'date_invoice': context.get('data_factura', False),
                    'date_due': False,
                    'move_name': False,
                    'move_id': False,
                    'period_id': False,
                    'tipo_rectificadora': tipus,
                    'ref': factura.id,
                    'journal_id': journal_id,
                }
                refund_id = self.copy(
                    cursor, uid, factura.id, default_vals, context=context
                )
                extra_obj.refund(cursor, uid, factura.id,
                                 refund_id, context=context)
                self.button_reset_taxes(cursor, uid, [refund_id], context)
                tmp_ids.append(refund_id)
            res_ids += tmp_ids
        return res_ids

    def is_rectified(self, cursor, uid, factura_id, context=None):
        """Checks if factura_id has been rectified"""

        if isinstance(factura_id, (list, tuple)):
            factura_id = factura_id[0]

        # Search for invoices rectifiying factura_id
        search_params = [('ref', '=', factura_id),
                         ('state', 'in', ('open', 'paid'))]

        rect_ids = self.search(cursor, uid, search_params)

        if rect_ids:
            rect_vals = self.read(cursor, uid, rect_ids,
                                  ['tipo_rectificadora'])
            for rect_val in rect_vals:
                if rect_val['tipo_rectificadora'] in ('R', 'RA'):
                    rect_rectified = self.is_rectified(cursor, uid,
                                                       rect_val['id'])
                    if not rect_rectified:
                        return rect_val['id']
                    else:
                        return rect_rectified
        return False

    def defer_payment(self, cursor, uid, invoice_id, journal_id, deferred_date,
                      context=None):
        """Changes due date of an invoice and makes account moves to make
        traceability
        """
        if context is None:
            context = {}

        if isinstance(invoice_id, list):
            assert len(invoice_id) == 1
            invoice_id = invoice_id[0]

        base_id = self._get_base_ids(cursor, uid, [invoice_id])
        res = self.pool.get('account.invoice').defer_payment(
            cursor, uid, base_id, journal_id, deferred_date, context
        )

        return res

    def undo_payment(self, cursor, uid, ids, context=None):

        invoice_ids = self._get_base_ids(cursor, uid, ids)
        res = self.pool.get('account.invoice').undo_payment(cursor,
                                            uid, invoice_ids, context)

        return res

    def unpay(self, cursor, uid, ids, amount, pay_account_id, period_id,
              pay_journal_id, context=None, name=''):
        invoice_ids = self._get_base_ids(cursor, uid, ids)
        res = self.pool.get('account.invoice').unpay(cursor, uid, invoice_ids,
                                                     amount, pay_account_id,
                                                     period_id, pay_journal_id,
                                                     context, name)
        return res

    def pay_from_n57(self, cursor, uid, ids, amount, pay_journal_id=None,
                     context=None):

        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = self._get_base_ids(cursor, uid, ids)
        return invoice_obj.pay_from_n57(
            cursor, uid, invoice_ids, amount, pay_journal_id, context
        )

    def _ff_linies_de(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = {}
        linies_obj = self.pool.get('giscegas.facturacio.factura.linia')
        for factura_id in ids:
            res[factura_id] = []
            linies = linies_obj.search(
                cursor, uid, [('tipus', '=', arg['tipus']),
                              ('factura_id', '=', factura_id)]
            )
            res[factura_id] = [l for l in linies]
        return res

    def _trg_totals_linia(self, cursor, uid, ids, context=None):
        """Funció per especificar els IDs a recalcular
        """
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        linies_vals = linia_obj.read(cursor, uid, ids, ['factura_id'])
        fact_ids = {
            linia_vals['factura_id'][0] for linia_vals in linies_vals
        }
        return list(fact_ids)

    def _trg_totals(self, cursor, uid, ids, context=None):
        """Funció per especificar els IDs a recalcular
        """
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        inv_ids = [
            a['invoice_id'][0]
            for a in self.read(cursor, uid, ids, ['invoice_id'])
        ]
        return fact_obj.search(cursor, uid, [('invoice_id', 'in', inv_ids)])

    def _ff_get_rectificadora(self, cursor, uid, ids, field, arg, context=None):
        if context is None:
            context = {}

        read_fields = ['rectificative_type', 'rectifying_id']

        res = {}
        for fact_vals in self.read(cursor, uid, ids, read_fields):
            if fact_vals['rectifying_id']:
                fact_id = self.search(
                    cursor, uid,
                    [('invoice_id', '=', fact_vals['rectifying_id'][0])]
                )[0]
            else:
                fact_id = False
            vals = {
                'tipo_rectificadora': fact_vals['rectificative_type'],
                'ref': fact_id
            }
            res[fact_vals['id']] = vals

        return res

    def _ff_total_tipus(self, cursor, uid, ids, field, arg, context=None):
        if context is None:
            context = {}

        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')

        tipus_quant_map = {
            'variable': 'consumo_kwh',
        }
        tipus_total_map = {
            'variable': 'total_variable',
            'fijo': 'total_termino_fijo',
            'lloguer': 'total_lloguers',
            'altres': 'total_altres'
        }
        res = {}
        for factura_vals in self.read(cursor, uid, ids, ['linia_ids']):
            fact_totals = {
                'total_variable': 0,
                'total_termino_fijo': 0,
                'total_lloguers': 0,
                'total_altres': 0,
                'consumo_kwh': 0,
            }

            linies_vals = linia_obj.read(
                cursor, uid, factura_vals['linia_ids'],
                ['tipus', 'price_subtotal', 'quantity', 'isdiscount']
            )
            for linia_vals in linies_vals:
                if not linia_vals.get('isdiscount', False):
                    # This will still work on disti because if the field
                    # isdiscount does not exist it will return False
                    if linia_vals['tipus'] in tipus_total_map:
                        field_name = tipus_total_map[linia_vals['tipus']]
                        fact_totals[field_name] += linia_vals['price_subtotal']
                    if linia_vals['tipus'] in tipus_quant_map:
                        field_name = tipus_quant_map[linia_vals['tipus']]
                        fact_totals[field_name] += linia_vals['quantity']

            res[factura_vals['id']] = {}
            for key, item in fact_totals.items():
                if self._columns[key]._type == 'integer':
                    # If the column is an integer, we make sure that it isn't
                    # a float, because then it fails
                    res[factura_vals['id']].update({key: int(item)})
                else:
                    res[factura_vals['id']].update({key: item})

        return res

    def _ff_comptadors(self, cursor, uid, ids, field, arg, context=None):
        """Funció que retorna els diferents comptadors facturats
        """
        res = {}
        for factura in self.browse(cursor, uid, ids):
            res[factura.id] = []
            for lene in factura.lectures_ids:
                if lene.comptador_id.id not in res[factura.id]:
                    res[factura.id].append(lene.comptador_id.id)
        return res

    def total_consumo_kwh(self, cursor, uid, ids, context=None):
        """Funció que retorna les lectures i consums d'energia facturats per
        període.

        Es retorna, per cada id a la llista, un diccionari del tipus:

        >>> {'Px': (anterior, actual, consum),
        ...  'Px+1': (anterior, actual, consum), }

        """
        if context is None:
            context = {}
        res = {}
        for factura in self.browse(cursor, uid, ids, context):
            pers = {}
            for periode in factura.lectures_ids:
                if context.get('include_dates', False):
                    per_vals = (periode.data_anterior, periode.data_actual,
                            periode.consum)
                else:
                    per_vals = (periode.lect_anterior, periode.lect_actual,
                            periode.consum)
                actual = pers.get(periode.name, False)
                if actual:
                    valors = (min(per_vals[0], actual[0]),
                              max(per_vals[1], actual[1]),
                              per_vals[2] + actual[2])
                else:
                    valors = per_vals
                pers[periode.name] = valors
            res[factura.id] = pers

        return res

    def _ff_polissa_state(self, cursor, uid, ids, field, arg, context={}):

        context.update({'active_test': False})
        res = {}
        for factura in self.browse(cursor, uid, ids, context):
            res[factura.id] = factura.polissa_id.state
        return res

    def _get_factura_from_polissa(self, cursor, uid, ids, context=None):
        '''Retorna les factures associades a les polisses en ids'''
        if not context:
            context = {}
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        search_params = [('polissa_id', 'in', ids)]
        return factura_obj.search(cursor, uid, search_params)

    def _get_factura_from_invoice(self, cursor, uid, ids, context=None):
        '''Retorna les factures que tenen invoice associades en ids'''
        if not context:
            context = {}
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        search_params = [('invoice_id', 'in', ids)]
        return factura_obj.search(cursor, uid, search_params)

    def make_move_differences(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        inv_obj = self.pool.get('account.invoice')
        inv_ids = [a['invoice_id'][0] for a in self.read(cursor, uid, ids,
                                                            ['invoice_id'])]
        res = inv_obj.make_move_differences(
            cursor, uid, inv_ids, context=context
        )
        return res

    # TODO: Modificar el selection
    _POLISSA_SELECTION = [
        ('esborrany', 'Borrador'),
        ('validar', 'Validar'),
        ('pendent', 'Pendiente'),
        ('activa', 'Activa'),
        ('cancelada', 'Cancelada'),
        ('contracte', 'Activación Contrato'),
        ('novapolissa', 'Creación nuevo contrato'),
        ('modcontractual', 'Modificación Contractual'),
        ('impagament', 'Impago'),
        ('avis1', 'Avíso 1'),
        ('avis2', 'Avíso 2'),
        ('facturacio', 'Facturación'),
        ('tall', 'Corte'),
        ('baixa', 'Baja')
    ]

    _STORE_POLISSA_STATE = {
        'giscegas.facturacio.factura': (
            lambda self, cr, uid, ids, c={}: ids, ['state'], 20
        ),
        'account.invoice': (_get_factura_from_invoice, ['state'], 20),
        'giscegas.polissa': (_get_factura_from_polissa, ['state'], 20),
    }

    def _trg_self(self, cr, uid, ids, c=None):
        return ids

    def _trg_invoice(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        factura_obj = self.pool.get('giscegas.facturacio.factura')

        return factura_obj.search(cursor, uid, [('invoice_id', 'in', ids)])

    _STORE_TOTALS = {
        'giscegas.facturacio.factura.linia': (_trg_totals_linia, ['tipus'], 40),
        'giscegas.facturacio.factura': (_trg_self, ['linia_ids'], 30),
        'account.invoice.line': (_trg_totals, ['quantity'], 20),
    }

    _STORE_RECTIFICADORA = {
        'account.invoice': (
            _trg_invoice, ['rectificative_type', 'rectifying_id'], 20
        ),
    }

    def _ff_dies(self, cursor, uid, ids, field, arg, context=None):

        res = {}.fromkeys(ids, 0)

        fact_fields = ['data_inici', 'data_final']
        for factura in self.read(cursor, uid, ids, fact_fields):
            if factura['data_inici'] and factura['data_final']:
                data_inici = datetime.strptime(factura['data_inici'],
                                               '%Y-%m-%d')
                data_final = datetime.strptime(factura['data_final'],
                                               '%Y-%m-%d')
                data_diff = data_final - data_inici
                days = data_diff.days
                res[factura['id']] = days + 1

        return res

    _STORE_DAYS = {
        'giscegas.facturacio.factura': (
            lambda self, cr, uid, ids, c=None: ids, ['data_inici', 'data_final'], 20
        )
    }

    _columns = {
        'invoice_id': fields.many2one(
            'account.invoice', 'Factura Contable', required=True,
            ondelete='cascade', select=True
        ),
        'linia_ids': fields.one2many(
            'giscegas.facturacio.factura.linia', 'factura_id', 'Líneas Factura',
            readonly=True, states={'draft': [('readonly', False)]}
        ),
        'llista_preu': fields.many2one(
            'product.pricelist', 'Lista de precios', required=True,
            readonly=True, states={'draft': [('readonly', False)]}
        ),
        'polissa_id': fields.many2one(
            'giscegas.polissa', 'Contrato', required=True, readonly=True,
            select=True, states={'draft': [('readonly', False)]}
        ),
        'polissa_state': fields.function(
            _ff_polissa_state, method=True, type='selection',
            string="Estado abonado", store=_STORE_POLISSA_STATE,
            selection=_POLISSA_SELECTION, readonly=True, size=256
        ),
        'tarifa_acces_id': fields.many2one(
            'giscegas.polissa.tarifa', "Tarifa de acceso", required=True,
            readonly=True, states={'draft': [('readonly', False)]}, select=1
        ),
        'cups_id': fields.many2one(
            'giscegas.cups.ps', 'CUPS', required=True, readonly=True,
            states={'draft': [('readonly', False)]}, select=1
        ),
        'data_inici': fields.date(
            'Fecha inicio', readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'data_final': fields.date(
            'Fecha fin', readonly=True, states={'draft': [('readonly', False)]}
        ),
        'tipo_factura': fields.selection(
            TIPO_FACTURA_SELECTION, 'Tipo de factura', readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'tipo_rectificadora': fields.function(
            _ff_get_rectificadora, type='selection',
            selection=TIPO_RECTIFICADORA_SELECTION,
            string='Tipo de rectificadora', multi='rectificadora',
            readonly=True, method=True, store=_STORE_RECTIFICADORA,
        ),
        'tipo_facturacion': fields.selection(
            TIPO_FACTURACION_SELECTION, 'Tipo de facturación', readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'date_boe': fields.date(
            'Fecha B.O.E.', required=True, readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'lectures_ids': fields.one2many(
            'giscegas.facturacio.lectures', 'factura_id',
            'Lecturas', readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'facturacio': fields.selection(
            FACTURACIO_SELECTION, 'Periodicidad de facturación', required=True,
            states={'draft': [('readonly', False)]}, select=1
        ),
        'lot_facturacio': fields.many2one(
            'giscegas.facturacio.lot', 'Lote de facturación', readonly=True,
            select=True
        ),
        'total_variable': fields.function(
            _ff_total_tipus, type='float', method=True, multi='totals',
            string='Total € consumo energía', store=_STORE_TOTALS
        ),
        'total_termino_fijo': fields.function(
            _ff_total_tipus, type='float', method=True,
            string='Total € término fijo', multi='totals', store=_STORE_TOTALS
        ),
        'total_lloguers': fields.function(
            _ff_total_tipus, type='float', method=True,
            string='Total € alquileres', multi='totals', store=_STORE_TOTALS
        ),
        'total_altres': fields.function(
            _ff_total_tipus, type='float', method = True,
            string = 'Total € altres', multi = 'totals', store = _STORE_TOTALS
        ),
        'linies_consum': fields.function(
            _ff_linies_de, type='one2many',
            obj='giscegas.facturacio.factura.linia', method=True,
            string='Líneas de consumo', arg={'tipus': 'variable'}
        ),
        'linies_terme_fix': fields.function(
            _ff_linies_de, type='one2many',
            obj='giscegas.facturacio.factura.linia', method=True,
            string='Líneas de término fijo', arg={'tipus': 'fijo'}
        ),
        'linies_lloguer': fields.function(
            _ff_linies_de, type='one2many',
            obj='giscegas.facturacio.factura.linia', method=True,
            string='Líneas de alquileres', arg={'tipus': 'lloguer'}
        ),
        'comptadors': fields.function(
            _ff_comptadors, type='one2many', obj='giscegas.lectures.comptador',
            method=True, string="Contadores"
        ),
        'ref': fields.function(
            _ff_get_rectificadora, type='many2one',
            obj='giscegas.facturacio.factura', method=True,
            string='Factura ref.', readonly=True, select=2,
            store=_STORE_RECTIFICADORA, multi='rectificadora'
        ),
        'consumo_kwh': fields.function(
            _ff_total_tipus, type='float', string="Consumo facturado (kWh)",
            method=True, multi='totals', store=_STORE_TOTALS
        ),
        'dies': fields.function(
            _ff_dies, type="integer", string=u"Días facturados", method=True,
            store=_STORE_DAYS, select=True
        ),
    }

    _defaults = {
        'tipo_factura': lambda *a: '01',
        'tipo_rectificadora': lambda *a: 'N',
        'tipo_facturacion': lambda *a: '1',
    }

    # De moment ho fem per 'id', ja que 'date_invoice' no està a la taula
    # giscegas_facturacio_factura i dona error al ordenar
    _order = 'id desc'


GiscegasFacturacioFactura()


class GiscegasFacturacioContracteLotFactura(osv.osv):
    """Factures generades dins un ContracteLot.
    """
    _name = 'giscegas.facturacio.contracte_lot.factura'
    _columns = {
        'contracte_lot_id': fields.many2one(
            'giscegas.facturacio.contracte_lot', 'Contrato Lote', required=True,
            ondelete='cascade'
        ),
        'factura_id': fields.many2one(
            'giscegas.facturacio.factura', 'Factura', required=True,
            ondelete='cascade'
        )
    }


GiscegasFacturacioContracteLotFactura()

# Crearem també una extensió per les línies de la factura


class GiscegasFacturacioFacturaLinia(osv.osv):
    """Classe per les línies de factura.
    """

    _name = 'giscegas.facturacio.factura.linia'
    _inherits = {'account.invoice.line': 'invoice_line_id'}

    # Variables
    def _tipus_selection(self, cursor, uid, context=None):
        """Creem la llista de tipus seleccionables.
        """
        tipus_obj = self.pool.get('giscegas.facturacio.factura.linia.tipus')
        tipus_ids = tipus_obj.search(cursor, uid, [], context=context)
        selection = []
        for tipus in tipus_obj.read(cursor, uid, tipus_ids, ['codi', 'name'],
                                    context):
            selection.append((tipus['codi'], tipus['name']))
        return selection

    # Sobreescrivim mètodes de l'objecte base
    def onchange_account_id(self, cursor, uid, ids, fposition_id, account_id):
        res = self.pool.get('account.invoice.line').onchange_account_id(cursor,
                                            uid, ids, fposition_id, account_id)
        return res

    def product_id_change_unit_price_inv(self, cursor, uid, tax_id, price_unit,
                                         qty, address_invoice_id, product,
                                         partner_id, context=None):
        res = self.pool.get('account.invoice.line').\
            product_id_change_unit_price_inv(cursor, uid, tax_id, price_unit,
                                             qty, address_invoice_id, product,
                                             partner_id, context)
        return res

    def product_id_change(self, cursor, uid, ids, pricelist, date_invoice,
                          product, uom, polissa_id=None, qty=0, name='',
                          type_='out_invoice', partner_id=False,
                          fposition_id=False, price_unit=False,
                          address_invoice_id=False, context=None):
        """Obtenció del nou preu segons llista de preus i quantitats

        Aquí hem de tenir en compte que  no hem d'agafar el preu
        des de la fitxa del producte, sino que l'hem d'agafar des de
        la llista de preus que tenim definida

        Busquem el preu segons la llista de preus que tinguem escollida, hem
        de tenir en compte que el preu pot variar segons la quantitat, per
        tant també haurem de fer una funció per quan es canviï la quantitat
        """

        # TODO: canviar date_invoice per la data del peride de facturació
        # Primer mirem si tenim llista de preus, sino donarem un error diguent
        # que s'ha d'escollir una llista de preus
        if not pricelist:
            raise osv.except_osv(_('Falta lista de precios'),
                                 _("Hay que definir la lista de precios antes "
                                   "de añadir ninguna línea de factura."))
        # Si el a quantitat és negativa (FENOSA),
        # busquem com si fos positiva
        qty_abs = abs(qty)
        res = self.pool.get('account.invoice.line').product_id_change(
            cursor, uid, ids, product, uom, qty_abs, name, type_, partner_id,
            fposition_id, price_unit, address_invoice_id, context
        )
        # Si hi ha llista de preus i producte guardem el preu per després
        # actualitzar al valor que ens dona la fucnió oficial
        if polissa_id and type_ == 'out_invoice':
            polissa = self.pool.get('giscegas.polissa').browse(
                cursor, uid, polissa_id
            )
            n_factures = self.pool.get(
                'giscegas.facturacio.factura'
            ).search_count(
                cursor, uid, [('polissa_id.id', '=', polissa.id)],
                context={'active_test': False}
            )
            # Si no hi ha cap factura emesa (n_factures = 0)
            if not n_factures and polissa.versio_primera_factura:
                # Agafem la data de la llista de preus com a la data d'inici de
                # la versió triada
                date_invoice = polissa.versio_primera_factura.date_start
        if product:
            ctx = context.copy()
            ctx['uom'] = uom
            ctx['date'] = date_invoice or time.strftime('%Y-%m-%d')
            price = self.pool.get('product.pricelist').price_get(
                cursor, uid, [pricelist], product, qty_abs or 1.0, partner_id,
                context=ctx
            )[pricelist]
            if price is False:
                warning = {
                    'title': _('Tarifa o producto no válido'),
                    'message':
                        _("No se ha encontrato un precio adecuado para el "
                          "producto seleccionado.\n"
                          "Esto puede ser debido a:\n"
                          " * El producto elegido no está dentro de la tarifa "
                          "seleccionada.\n"
                          " * No hay ninguna tarifa vigente para la fecha "
                          "elegida.")
                }
                return {'warning': warning, 'value': {'product_id': False}}
            else:
                res['value'].update({'price_unit': price})
        return res

    # Quan creem una linia de la nostra factura també ha de quedar associada a
    # la factura base d'openerp
    def create(self, cursor, uid, values, context=None):
        if not context:
            context = {}

        defaults = self.default_get(
            cursor, uid, ['price_unit'], context
        )
        values['invoice_id'] = self.pool.get(
            'giscegas.facturacio.factura'
        ).read(cursor, uid, values['factura_id'], ['invoice_id'])['invoice_id'][0]

        values['price_unit'] = float_round(
            values.get('price_unit', defaults['price_unit']),
            int(config['price_accuracy'])
        )
        query_file = (
            '%s/giscegas_facturacio/sql/query_factura_linia.sql'
            % config['addons_path']
        )
        query = open(query_file).read()
        parameters = [
            values['name'], values['factura_id'], values['tipus'],
            '%.6f' % values.get('price_unit', defaults['price_unit']),
            values['product_id'] or 0
        ]

        group_line = context.get('group_line', True)
        if group_line:
            cursor.execute(query, tuple(parameters))
            lids = [a[0] for a in cursor.fetchall()]

        if not group_line or not lids:
            return super(
                GiscegasFacturacioFacturaLinia, self
            ).create(cursor, uid, values, context)
        categ_extra_id = self.pool.get('ir.model.data').get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'categ_extra'
        )[1]
        for linia in self.browse(cursor, uid, lids):
            # No agrupar línies de productes de categoria extra
            if (linia.product_id and linia.product_id.categ_id and
                    linia.product_id.categ_id.id == categ_extra_id):
                return super(GiscegasFacturacioFacturaLinia, self).create(
                    cursor, uid, values, context)
            values['data_desde'] = min(linia.data_desde, values['data_desde'])
            values['data_fins'] = max(linia.data_fins, values['data_fins'])
            values['quantity'] += linia.quantity
            if 'atrprice_subtotal' in values:
                values['atrprice_subtotal'] += linia.atrprice_subtotal
            linia.write(values)

            return linia.id

    # Quan eliminem una línia de la nostra factura també s'ha d'eliminar de la
    # factura d'openerp
    def unlink(self, cursor, uid, ids, context=None):
        #Remove from extra
        extra_obj = self.pool.get('giscegas.facturacio.extra')
        extra_obj.remove_lines(cursor, uid, ids, context=context)
        # Busquem els ids relacionats
        ids_inv = [a['invoice_line_id'][0] for a in self.read(cursor, uid, ids,
                                                        ['invoice_line_id'])]
        if len(ids_inv):
            self.pool.get('account.invoice.line').unlink(cursor, uid, ids_inv,
                                                         context)
        return super(GiscegasFacturacioFacturaLinia, self).unlink(cursor, uid,
                                                                ids, context)

    def write(self, cursor, uid, ids, values, context=None):
        """Sobreescrivim el write

        També s'ha de modificar el write ja que en la nostra facturació és
        diferent a la general i s'ha de fixar que el preu que el preu de la
        general sigui la operació que en la nostra aplicació té dos factors
        (ex. preu factura base = preu x menusalitat)
        """
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        orig_vals = values.copy()
        for id in ids:
            values_ = self.read(cursor, uid, [id], ['price_unit'])[0]
            price_unit = values.get('price_unit', values_['price_unit'])

            values['price_unit'] = float_round(
                price_unit, int(config['price_accuracy'])
            )
            super(GiscegasFacturacioFacturaLinia, self).write(
                cursor, uid, [id], values, context
            )
            values = orig_vals.copy()
        return True

    _columns = {
        'tipus': fields.selection(_tipus_selection, 'Tipo', required=True),
        'factura_id': fields.many2one(
            'giscegas.facturacio.factura', 'Factura', required=True,
            ondelete='cascade', select=True
        ),
        'invoice_line_id': fields.many2one(
            'account.invoice.line', 'Línea Contable', required=True,
            ondelete='cascade'
        ),
        'data_desde': fields.date('Fecha desde'),
        'data_fins': fields.date('Fecha hasta'),
    }


GiscegasFacturacioFacturaLinia()


class GiscegasFacturacioFacturaLiniaTipus(osv.osv):
    """Identifiquem quins tipus de línia hi ha en una factura."""
    _name = 'giscegas.facturacio.factura.linia.tipus'

    _columns = {
        'name': fields.char('Tipo', size=64, required=True, translate=True),
        'codi': fields.char('Código', size=16, required=True)
    }


GiscegasFacturacioFacturaLiniaTipus()


class GiscegasFacturacioFacturaLectures(osv.osv):
    """Classe per les lectures d'energia de les factures.
    """

    _name = 'giscegas.facturacio.lectures'

    _columns = {
        'name': fields.char('Período', size=16, required=True, readonly=True),
        'comptador_id': fields.many2one(
            'giscegas.lectures.comptador', 'Comptador', required=True
        ),
        'factura_id': fields.many2one(
            'giscegas.facturacio.factura', 'Factura', required=True,
            readonly=True, ondelete='cascade', select=True
        ),
        'tipo_lect_num_actual': fields.selection(
            TAULA_TIPO_DE_LECTURA_DE_NUMERADOR, 'Tipo de Lectura de Numerador Actual'
        ),
        'tipo_lect_num_anterior': fields.selection(
            TAULA_TIPO_DE_LECTURA_DE_NUMERADOR, 'Tipo de Lectura de Numerador Anterior'
        ),
        'data_actual': fields.date(
            'Fecha actual', required=True, readonly=True
        ),
        'lect_actual': fields.float(
            'Lectura actual (m³)', required=True, readonly=True, digits=(16, 6)
        ),
        'data_anterior': fields.date(
            'Fecha anterior', required=True, readonly=True
        ),
        'lect_anterior': fields.float(
            'Lectura anterior (m³)', required=True, readonly=True, digits=(16, 6)
        ),
        'consum': fields.float(
            'Consumo (kWh)', required=True, readonly=True, digits=(16, 2)
        ),
        'origen_id': fields.many2one(
            'giscegas.lectures.origen', 'Origen Actual'
        ),
        'origen_anterior_id': fields.many2one(
            'giscegas.lectures.origen', 'Origen Anterior'
        ),
        'ajust': fields.integer('Regularización'),
        'factor_k': fields.float('Factor K', digits=(16, 6)),
        'pcs': fields.float('Poder Calorífico Superior (PCS)', digits=(16, 6)),
        'motiu_ajust': fields.selection(
            TAULA_MOTIVO_REGULARIZACION_CONSUMO, 'Motivo Regularización'
        ),
        'pressio_subministrament': fields.float(
            'Presión del suministro', digits=(7, 4)
        ),
    }

    _defaults = {
        'lect_anterior': lambda *a: 0,
        'ajust': lambda *a: 0,
    }

    _order = "name asc"


GiscegasFacturacioFacturaLectures()
