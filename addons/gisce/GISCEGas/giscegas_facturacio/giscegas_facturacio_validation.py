# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from osv import osv, fields, orm
from tools.translate import _
import json


class GiscegasFacturacioValidationWarningTemplate(osv.osv):
    """
    We can create templates here. The templates have a code, a description, and
    a linked method. The name of this method should be stored in the field
    method. Also, the method should be declared in the class
    giscegas.facturacio.validation.validator in order to work.
    """

    _name = 'giscegas.facturacio.validation.warning.template'
    _desc = u'Template dels warnings de validació'
    _rec_name = 'code'

    def get_warning(self, cursor, uid, code, context=None):
        if context is None:
            context = {}

        warning_data = None
        warning_id = self.search(
            cursor, uid, [('code', '=', code)], limit=1, context=context
        )
        if warning_id:
            warning_data = self.read(
                cursor, uid, warning_id[0], ['description'], context=context
            )

        return warning_data

    def check_correct_json(self, cursor, uid, ids, parameters_text):
        try:
            parameters = json.loads(parameters_text)
        except ValueError as e:
            return {
                'warning': {
                    'title': _(u'Atenció'),
                    'message': _('Els parametres entrats no tenen un format '
                                 'correcte de JSON')
                }
            }
        if not isinstance(parameters, dict):
            return {
                'warning': {
                    'title': _(u'Atenció'),
                    'message': _('Els parametres han de ser un diccionari')
                }
            }
        return {}

    def _ff_parameters(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}

        res = {}
        for import_vals in self.read(cursor, uid, ids, ['parameters'], context):
            res[import_vals['id']] = json.dumps(
                import_vals['parameters'], indent=4, sort_keys=True
            )
        return res

    def _fi_parameters(self, cursor, uid, ids, name, value, arg, context=None):
        if not context:
            context = {}

        try:
            parameters = json.loads(value)
            self.write(cursor, uid, ids, {'parameters': parameters})
        except ValueError as e:
            pass

    TIPUS_WARNING = [
        ('lot', 'Al validar el lot'),
        ('pre-facturacio', 'Al generar la factura'),
    ]

    _columns = {
        'code': fields.char('Codi', size=4, required=True, readonly=True),
        'description': fields.char(
            u'Descripció del warning', size=1024, required=True, readonly=True,
            translate=True
        ),
        'observation': fields.text(
            u'Observació', readonly=True, translate=True
        ),
        'method': fields.char(
            u'Mètode a executar per comprovar aquest warning', size=100,
            required=True, readonly=True
        ),
        'type': fields.selection(TIPUS_WARNING, string='Tipus', readonly=True),
        'active': fields.boolean('Actiu', readonly=False),
        'parameters': fields.json('Parametres'),
        'parameters_text': fields.function(
            _ff_parameters, type='text', method=True, string='Parametres',
            fnct_inv=_fi_parameters
        ),
    }
GiscegasFacturacioValidationWarningTemplate()


class GiscegasFacturacioValidationWarning(osv.osv):
    _name = 'giscegas.facturacio.validation.warning'
    _desc = u"Warning en la validació d'una factura"

    def get_warning_text(self, cursor, uid, warning_id, context=None):
        """
        Gets Warning text from invoice warnings
        :param cursor: Database's cursor
        :param uid: User's id
        :param warning_id: Id of the warning we want the text of
        :param context: OpenERP's context
        :return: Returns a text like "[code] message"
        """
        if context is None:
            context = {}

        vals = self.read(cursor, uid, warning_id, ['name', 'message'], context)
        return '[{0}] {1}'.format(vals['name'], vals['message'])

    def create_warning_from_code(self, cursor, uid, clot_id, fact_id,
                                 warning_code, values, context=None):
        if context is None:
            context = {}
        template_obj = self.pool.get(
            'giscegas.facturacio.validation.warning.template'
        )
        warning_temp = template_obj.get_warning(
            cursor, uid, warning_code, context=context
        )
        if warning_temp:
            vals = {
                'name': warning_code,
                'message': warning_temp['description'].format(**values),
                'warning_template_id': warning_temp['id'],
                'clot_id': clot_id,
                'factura_id': fact_id,
            }
            warning_id = self.create(cursor, uid, vals)

            return warning_id
        return False

    _columns = {
        'name': fields.char(
            'Código de error', size=14, required=True, readonly=True, select=True
        ),
        'message': fields.char(
            'Mensaje de advertencia', size=2048, required=True, readonly=True
        ),
        'warning_template_id': fields.many2one(
            'giscegas.facturacio.validation.warning.template',
            string='Plantilla de la advertencia', select=1, readonly=True
        ),
        'factura_id': fields.many2one(
            'giscegas.facturacio.factura', ondelete='cascade',
            string='Factura', select=1, readonly=True, required=False
        ),
        'clot_id': fields.many2one(
            'giscegas.facturacio.contracte_lot', ondelete='cascade',
            string='Lote del contrato ', select=1, readonly=True, required=False
        ),
        'active': fields.boolean('Activo', readonly=True, select=1),
    }

    _defaults = {
        'active': lambda *a: True,
    }


GiscegasFacturacioValidationWarning()


class GiscegasFacturacioValidationValidator(osv.osv):
    _name = 'giscegas.facturacio.validation.validator'
    _desc = 'Validador de facturas'

    def get_checks(self, cursor, uid, validation_type=None, context=None):
        if context is None:
            context = {}

        template_obj = self. pool.get(
            'giscegas.facturacio.validation.warning.template'
        )

        search_parameters = [('active', '=', True)]
        if validation_type:
            search_parameters.append(('type', '=', validation_type))

        template_ids = template_obj.search(
            cursor, uid, search_parameters
        )

        return template_obj.read(
            cursor, uid, template_ids, ['code', 'method', 'parameters'],
            context
        )

    def get_invoice_warnings_text(self, cursor, uid, fact_id, context=None):
        if context is None:
            context = {}

        warning_obj = self.pool.get('giscegas.facturacio.validation.warning')

        warning_ids = warning_obj.search(
            cursor, uid, [('factura_id', '=', fact_id)]
        )
        warning_messages = [
            warning_obj.get_warning_text(cursor, uid, warn_id, context)
            for warn_id in warning_ids
        ]

        return '\n'.join(warning_messages)

    def validate_invoice(self, cursor, uid, fact_id, context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscegas.facturacio.factura')
        warn_obj = self.pool.get('giscegas.facturacio.validation.warning')

        fact = fact_obj.browse(cursor, uid, fact_id)

        warning_ids = []
        checks = self.get_checks(
            cursor, uid, validation_type='pre-facturacio', context=context
        )
        for template_vals in checks:
            vals = getattr(self, template_vals['method'])(
                cursor, uid, fact, template_vals['parameters']
            )
            if vals is not None:
                warning_ids.append(
                    warn_obj.create_warning_from_code(
                        cursor, uid, None, fact_id,
                        template_vals['code'], vals, context=context
                    )
                )

        return warning_ids

    def get_clot_warnings_texts(self, cursor, uid, clot_id, context=None):
        if context is None:
            context = {}

        warning_obj = self.pool.get('giscegas.facturacio.validation.warning')

        warning_ids = warning_obj.search(
            cursor, uid, [('clot_id', '=', clot_id)]
        )
        warning_messages = [
            warning_obj.get_warning_text(cursor, uid, warn_id)
            for warn_id in warning_ids
        ]

        return warning_messages

    def validate_clot(self, cursor, uid, clot_id, data_inici, data_fi,
                      context=None):
        if context is None:
            context = {}

        clot_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        warn_obj = self.pool.get('giscegas.facturacio.validation.warning')

        clot = clot_obj.browse(cursor, uid, clot_id)

        old_warn_ids = warn_obj.search(cursor, uid, [('clot_id', '=', clot_id)])
        warn_obj.unlink(cursor, uid, old_warn_ids)

        warning_ids = []
        checks = self.get_checks(
            cursor, uid, validation_type='lot', context=context
        )
        for template_vals in checks:
            vals = getattr(self, template_vals['method'])(
                cursor, uid, clot, data_inici, data_fi,
                template_vals['parameters']
            )
            if vals is not None:
                warning_ids.append(
                    warn_obj.create_warning_from_code(
                        cursor, uid, clot_id, None,
                        template_vals['code'], vals, context=context
                    )
                )

        return warning_ids

    #######################
    # INVOICE VALIDATIONS #
    #######################
    def check_consume_by_percentage(self, cursor, uid, fact, parameters):
        fact_obj = self.pool.get('giscegas.facturacio.factura')

        n_months = parameters['n_months']
        to_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        from_date = to_date - relativedelta(months=n_months)

        max_consume = fact_obj.get_max_consume_by_contract(
            cursor, uid, fact.polissa_id.id, from_date,
            to_date
        )

        if max_consume:
            percentage_margin = parameters['overuse_percentage']

            inv_consume = fact.consumo_kwh
            if inv_consume > (max_consume * (100.0 + percentage_margin))/100.0:
                return {
                    'invoice_consume': inv_consume,
                    'percentage': percentage_margin,
                    'maximum_consume': max_consume,
                    'n_months': n_months,
                }

        return None

    def check_consume_by_amount(self, cursor, uid, fact, parameters):
        fact_obj = self.pool.get('giscegas.facturacio.factura')

        n_months = parameters['n_months']
        to_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        from_date = to_date - relativedelta(months=n_months)

        max_consume = fact_obj.get_max_consume_by_contract(
            cursor, uid, fact.polissa_id.id, from_date, to_date
        )

        if max_consume:
            margin_amount = parameters['overuse_amount']

            inv_consume = fact.consumo_kwh
            if inv_consume > max_consume + margin_amount:
                return {
                    'invoice_consume': inv_consume,
                    'margin': margin_amount,
                    'maximum_consume': max_consume,
                    'n_months': n_months,
                }

        return None

    def check_minimum_consume_import(self, cursor, uid, fact, parametres):
        if fact.total_variable <= parametres['minimum_consume']:
            return {
                'invoice_consume': fact.total_variable,
                'minimum_consume': parametres['minimum_consume'],
            }
        return None

    def check_minimum_import(self, cursor, uid, fact, parametres):
        if fact.amount_total <= parametres['minimum_import']:
            return {
                'invoice_import': fact.amount_total,
                'minimum_import': parametres['minimum_import'],
            }
        return None

    def check_maximum_import_by_tariff(self, cursor, uid, fact, parametres):
        tarifa_name = fact.tarifa_acces_id.name
        if tarifa_name in parametres:
            if fact.amount_total > parametres[tarifa_name]:
                return {
                    'invoice_import': fact.amount_total,
                    'tariff': tarifa_name,
                    'maximum_import': parametres[tarifa_name],
                }

    def check_consume_by_both_from_mean(self, cursor, uid, fact, parameters):
        fact_obj = self.pool.get('giscegas.facturacio.factura')

        n_months = parameters['n_months']
        to_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        from_date = to_date - relativedelta(months=n_months)

        mean_consume = fact_obj.get_mean_consume_by_contract(
            cursor, uid, fact.polissa_id.id, from_date, to_date
        )

        if mean_consume:
            percentatge = parameters['percentage_margin']
            absolute = parameters['absolute_margin']

            margin = mean_consume * percentatge / 100
            if margin < absolute:
                margin = absolute

            inv_consume = fact.consumo_kwh
            in_margins = \
                mean_consume - margin < inv_consume < mean_consume + margin
            if not in_margins:
                return {
                    'invoice_consume': inv_consume,
                    'minimum_consume': mean_consume - margin,
                    'maximum_consume': mean_consume + margin,
                    'mean_consume': mean_consume,
                    'n_months': n_months,
                }

        return None

    def check_exceding_days(self, cursor, uid, fact, parameters):
        trad_months_fact = {
            1: 'n_days_mensual',
            2: 'n_days_bimensual'
        }

        months_fact = fact.polissa_id.facturacio
        increment = relativedelta(months=months_fact, days=-1)

        start_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        end_date = datetime.strptime(fact.data_final, '%Y-%m-%d')
        expected_final_date = start_date + increment

        expected = (expected_final_date - start_date).days
        actual = (end_date - start_date).days
        difference = actual - expected
        margin = parameters[trad_months_fact[months_fact]]

        if difference > margin:
            return {
                'margin': margin,
                'actual_days': actual,
                'expected_days': expected
            }

        return None

    def check_missing_days(self, cursor, uid, fact, parameters):
        trad_months_fact = {
            1: 'n_days_mensual',
            2: 'n_days_bimensual'
        }

        months_fact = fact.polissa_id.facturacio
        increment = relativedelta(months=months_fact, days=-1)

        start_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        end_date = datetime.strptime(fact.data_final, '%Y-%m-%d')
        expected_final_date = start_date + increment

        expected = (expected_final_date - start_date).days
        actual = (end_date - start_date).days
        difference = expected - actual
        margin = parameters[trad_months_fact[months_fact]]

        if difference > margin:
            return {
                'margin': margin,
                'actual_days': actual,
                'expected_days': expected
            }

        return None

    def check_ending_outside_lot(self, cursor, uid, fact, parameters):
        lot_start_date = datetime.strptime(
            fact.polissa_id.lot_facturacio.data_inici,
            '%Y-%m-%d'
        )
        lot_end_date = datetime.strptime(
            fact.polissa_id.lot_facturacio.data_final,
            '%Y-%m-%d'
        )
        end_date = datetime.strptime(fact.data_final, '%Y-%m-%d')

        if end_date < lot_start_date or lot_end_date < end_date:
            return {'end_date':end_date}
        return None

    def check_invoiced_consume(self, cursor, uid, fact, parameters):
        total_consum = 0
        for lectura in fact.lectures_ids:
            total_consum += lectura.consum
        if fact.consumo_kwh != total_consum:
            return {
                'invoiced_energy': fact.consumo_kwh,
                'measured_consumption': total_consum
            }
        return None

    def check_missing_variable_lines(self, cursor, uid, fact, parameters):
        has_variable_lines = any(
            line.tipus == 'variable'
            for line in fact.linia_ids
        )
        if not has_variable_lines:
            return dict(error=True)
        return None

    def check_overlapping_or_duplicated_invoice(self, cursor, uid, fact, parameters):
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        search_AB_params = [
            ('polissa_id', '=', fact.polissa_id.id),
            ('type', '=', 'out_refund'),
            ('tipo_rectificadora', '=', 'B'),
            ('data_final', '>=', fact.data_inici),
            ('data_inici', '<=', fact.data_final),
        ]
        ab_fact_ids = fact_obj.search(cursor, uid, search_AB_params)
        search_R_params = [
            ('polissa_id', '=', fact.polissa_id.id),
            ('type', '=', 'out_invoice'),
            ('tipo_rectificadora', '=', 'R'),
            ('data_final', '>=', fact.data_inici),
            ('data_inici', '<=', fact.data_final),
        ]
        r_fact_ids = fact_obj.search(cursor, uid, search_R_params)
        abr_fact_ids = r_fact_ids + ab_fact_ids
        no_facts_vals = fact_obj.read(cursor, uid, abr_fact_ids, ['ref'])
        no_facts_ids = [a['ref'][0] for a in no_facts_vals if a['ref']]
        no_facts_ids.append(fact.id)
        search_params = [
            ('polissa_id', '=', fact.polissa_id.id),
            ('type', '=', 'out_invoice'),
            ('data_final', '>=', fact.data_inici),
            ('data_inici', '<=', fact.data_final),
            ('id', 'not in', no_facts_ids)
        ]
        fact_ids = sorted(fact_obj.search(cursor, uid, search_params))
        if fact_ids:
            def get_text(d, a, b):
                return str(d[a]) if f[a] else str(d[b])
            fact_vals = fact_obj.read(cursor, uid, fact_ids, ['number'])
            fact_names = sorted([get_text(f, 'number', 'id') for f in fact_vals])
            colisions = "[" + ", ".join(fact_names) + "]"
            return {'colisions': colisions}
        return None

    def check_lineas_extra_posteriors(self, cursor, uid, fact, parametres):
        fdata_final = fact.data_final
        for linia in fact.linia_ids:
            if linia.tipus == "altres" and linia.data_desde > fdata_final:
                return {}
        return None

    ############################
    # CONTRACT LOT VALIDATIONS #
    ############################
    def check_minimum_consume_value(self, cursor, uid, clot, data_inici,
                                    data_fi, parametres):
        clot_obj = self.pool.get('giscegas.facturacio.contracte_lot')

        consum_total, consum_periodes, periodes_lect = clot_obj.get_consume(
            cursor, uid, clot.id, data_inici, data_fi
        )

        if consum_total <= parametres['minimum_consume']:
            return {
                'consume_limit': parametres['minimum_consume'],
                'consume_type': '',
                'consume': consum_total
            }

        return None

    def check_factors_a_0_amb_consum(self, cursor, uid, clot, data_inici, data_fi, parametres):
        ctx = {}
        compta_obj = self.pool.get('giscegas.lectures.comptador')
        polissa = clot.polissa_id
        c_actius = polissa.comptadors_actius(
            data_inici, data_fi,
            order='data_alta asc'
        )

        tid = polissa.modcontractual_activa.tarifa.id
        for compt in compta_obj.browse(cursor, uid, c_actius):
            # El métode get_inici_final_a_facturar no té en compte que la
            # lectura inicial de la pólissa/modcon comença el dia anterior a
            # l'activació. Per tant ara restem 1 dia a les dates que estem
            # utilitzant
            data_inici_periode_f2 = (
                    datetime.strptime(data_inici, "%Y-%m-%d") - timedelta(days=1)
            ).strftime("%Y-%m-%d")
            ctx.update({
                'fins_lectura_fact': data_fi,
                'ult_lectura_fact': data_inici_periode_f2
            })
            lects = compt.get_lectures_per_facturar(
                tid, context=ctx
            )
            for pname, lectures in sorted(lects.items()):
                lectura_act = lectures.get('actual', {})
                if not lectura_act:
                    continue
                consum = lectura_act.get('consum_m3', 0)
                if consum and not lectura_act.get('pressio_subministrament', 0):
                    return {
                        'lcomptador': compt.name,
                        'lname': lectura_act.get('name'),
                        'lperiod': pname,
                        'lconsum': consum,
                        'lcamp': 'pressio_subministrament'
                    }
                elif consum and not lectura_act.get('factor_k', 0):
                    return {
                        'lcomptador': compt.name,
                        'lname': lectura_act.get('name'),
                        'lperiod': pname,
                        'lconsum': consum,
                        'lcamp': 'factor_k'
                    }
                elif consum and not lectura_act.get('pcs', 0):
                    return {
                        'lcomptador': compt.name,
                        'lname': lectura_act.get('name'),
                        'lperiod': pname,
                        'lconsum': consum,
                        'lcamp': 'pcs'
                    }
        return None


GiscegasFacturacioValidationValidator()
