# -*- coding: utf-8 -*-
from gestionatr.defs_gas import *

FACTURACIO_SELECTION = [(1, 'Mensual'), (2, 'Bimestral')]

TIPO_FACTURA_SELECTION = TAULA_TIPO_FACTURA

TIPO_RECTIFICADORA_SELECTION = [('N', 'Normal'),
                                ('R', 'Rectificadora'),
                                ('A', 'Anuladora'),
                                ('B', 'Anuladora con sustituyente'),
                                ('C', 'Complementaria'),
                                ('G', 'Regularizadora'),
                                ('RA', 'Rectificadora sin anuladora'),
                                ('BRA', 'Anuladora (ficticia)')]

REFUND_RECTIFICATIVE_TYPES = ['A', 'B', 'RA']

RECTIFYING_RECTIFICATIVE_TYPES = ['R', 'RA']

SIGN = {'N': 1, 'R': 1, 'A': -1, 'B': -1, 'RA': 1, 'BRA': -1, 'G': 1, 'C': 1}

TIPO_FACTURACION_SELECTION = TAULA_TIPO_FACTURACION

MOTIUS_AJUST = TAULA_MOTIVO_REGULARIZACION_CONSUMO

RECTIFICATIVE_FROM_F1_TO_DB = {
    'R': 'RA',
}

TRANSLATE_ORIGIN_TO_NEW_F1 = {
    '11': '10',
    '21': '20',
    '31': '30'
}
