SELECT count(f.id)
FROM giscegas_facturacio_factura f
INNER JOIN account_invoice i
  ON i.id = f.invoice_id
INNER JOIN account_journal journal
  ON journal.id = i.journal_id
WHERE f.polissa_id = %s
  AND f.lot_facturacio = %s
  AND f.tipo_factura = '01'
  AND f.tipo_rectificadora = 'N'
  AND journal.code = 'GAS'
