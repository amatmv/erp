# -*- coding: utf-8 -*-
"""Classes de facturació relatives a les pòlisses"""

# pylint: disable-msg=E1101,W0223
from __future__ import absolute_import
from osv import osv, fields
from datetime import datetime, timedelta
import calendar
from tools.translate import _
from tools import config
from .defs import *
from giscegas_polissa.giscegas_polissa import (
    CONTRACT_IGNORED_STATES
)
from giscegas_facturacio.giscegas_facturacio import (
    REFUND_RECTIFICATIVE_INVOICE_TYPES
)
from ast import literal_eval


NOTIFICACIO_SELECTION = [('titular', 'Titular'),
                         ('pagador', 'Fiscal'),
                         ('altre_p', 'Altra')]

INTERVAL_INVOICING_FIELDS = ['tarifa']


def _get_polissa_from_invoice(self, cursor, uid, ids, context=None):
    """Returns ids of polissa in invoice passed as ids"""

    factura_obj = self.pool.get('giscegas.facturacio.factura')
    #Search factura associated to invoices in ids
    query = '''
        SELECT f.id
        FROM giscegas_facturacio_factura f
        INNER JOIN account_invoice i
        ON i.id = f.invoice_id
        WHERE i.id in %s
        AND i.type in ('out_invoice', 'out_refund')
        AND i.state in ('open', 'paid')
        '''
    cursor.execute(query, (tuple(ids),))
    factura_ids = [x[0] for x in cursor.fetchall()]
    vals = factura_obj.read(cursor, uid, factura_ids, ['polissa_id'])
    return [val['polissa_id'][0] for val in vals if val['polissa_id']]


def _get_polissa_from_energy_invoice(self, cursor, uid, ids, context=None):
    """Returns ids of polissa in invoice passed as ids"""

    factura_obj = self.pool.get('giscegas.facturacio.factura')
    # Search factura associated to invoices in ids
    from osv.expression import OOQuery
    q = OOQuery(factura_obj, cursor, uid)
    sql = q.select(['polissa_id']).where([
        ('type', 'in', ['out_invoice', 'out_refund']),
        ('state', 'in', ['open', 'paid']),
        ('invoice_id.journal_id.code', '=like', 'GAS%'),
        ('tipo_factura', '=', '01'),
        ('invoice_id', 'in', tuple(ids))
    ])
    cursor.execute(*sql)
    return [x[0] for x in cursor.fetchall()]


class GiscegasPolissaTarifa(osv.osv):
    _name = 'giscegas.polissa.tarifa'
    _inherit = 'giscegas.polissa.tarifa'

    def get_periodes_preus(self, cursor, uid, tarifa_id, tipus, pricelist_id,
                           context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        pricelist_obj = self.pool.get('product.pricelist')
        periodes_producte = self.get_periodes_producte(cursor, uid, tarifa_id,
                                                       tipus, context=context)
        res = {}
        for periode, producte in periodes_producte.items():
            res[periode] = pricelist_obj.price_get(
                cursor, uid, [pricelist_id], producte, 1, context=ctx
            )[pricelist_id]
        return res


GiscegasPolissaTarifa()


class GiscegasPolissa(osv.osv):
    """Extensió de la pòlissa amb les dades bàsiques de facturació.
    """
    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    _polissa_states_selection = [
        ('facturacio', 'Facturació'),
    ]
    # ORM stuff

    _pagador_selection = [('titular', 'Titular'),
                          ('altre_p', 'Altre')]

    def get_related_attachments_fields(self, cursor, uid):
        return super(GiscegasPolissa, self).get_related_attachments_fields(cursor, uid) + ['pagador']

    def __init__(self, pool, cursor):
        """Init to add new states."""
        super(GiscegasPolissa, self).__init__(pool, cursor)
        for new_selection in self._polissa_states_selection:
            if new_selection not in self._columns['state'].selection:
                self._columns['state'].selection.append(new_selection)

    def _ff_fact_endarrerida(self, cursor, uid, ids, field_name, args,
                                         context=None):

        """ Marquem una factura com a endarrerida:
                * Fa més de 1.33 * facturacio dies que no es factura
                * La pólissa no té cap factura fa 1.33 * facturacio
                  dies que està facturada
        """
        res = dict.fromkeys(ids, False)
        cfg_obj = self.pool.get('res.config')
        nom_conf = 'giscegas_periode_polissa_facturacio_endarrerida'
        periode = float(cfg_obj.get(cursor, uid, nom_conf, 1.33))

        vals_pol = self.read(
            cursor, uid, ids, ['facturacio', 'data_ultima_lectura', 'data_alta']
        )

        for pol in vals_pol:
            ult_fact = pol['data_ultima_lectura'] or pol['data_alta']
            if ult_fact:
                # calculem la data anterior
                # dies a restar
                dies = int(pol['facturacio']) * 30 * periode
                data_anterior = datetime.now() - timedelta(dies)
                if ult_fact < datetime.strftime(data_anterior, '%Y-%m-%d'):
                    res[pol['id']] = True

        return res

    def _search_fact_endarrerida(self, cursor, uid, obj, name, args,
                                 context=None):
        """
        Funció de cerca de facturacio endarrerida
        """
        trobats = []

        cfg_obj = self.pool.get('res.config')
        nom_conf = 'periode_polissa_facturacio_endarrerida'
        periode = float(cfg_obj.get(cursor, uid, nom_conf, 1.33))

        for facturacio in [1, 2]:
            dies = facturacio * 30 * periode
            data_anterior = datetime.now() - timedelta(dies)

            ids = self.search(cursor, uid,
                              [('facturacio', '=', facturacio),
                               '|',
                               '&', ('data_ultima_lectura', '<',
                                     data_anterior),
                               ('data_ultima_lectura', '!=', False),
                               '&', ('data_alta', '<', data_anterior),
                               ('data_ultima_lectura', '=', False)])
            trobats += ids

        return [('id', 'in', trobats)]

    def anullar_facturacions(self, cursor, uid, ids, context=None):
        """Anul·la les possibles facturacions posteriors de la pòlissa.

        Si es troba la pòlissa en lots de facturació futurs, es treu
        d'aquests.
        """
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        clot_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        # s'ha d'esborrar la pòlissa del lot
        for pol in self.browse(cursor, uid, ids, context):
            if pol.lot_facturacio:
                search_params = [
                    ('polissa_id', '=', pol.id),
                    ('state', '=', 'esborrany')
                ]
                if pol.data_baixa:
                    from_date = pol.data_baixa
                    search_params.append(('lot_id.data_inici', '>', from_date))
                else:
                    from_date =  pol.lot_facturacio.data_inici
                    search_params.append(('lot_id.data_inici', '>=', from_date))

                clot_id = clot_obj.search(cursor, uid, search_params,
                                          context={'active_test': False})
                clot_obj.unlink(cursor, uid, clot_id, context)
                #If something was unlinked
                if clot_id:
                    #Assign last invoiced lot. This way the polissa will
                    #reflect the real last lot. Otherwise, it will be
                    #assigned to an incorrect lot that do not contain
                    #any clot for this polissa
                    cursor.execute(
                        "SELECT clot.id "
                        "FROM giscegas_facturacio_contracte_lot clot "
                        "JOIN giscegas_facturacio_lot lot ON lot.id=clot.lot_id "
                        "WHERE clot.polissa_id = %s "
                        "AND clot.state in %s "
                        "AND lot.data_final >= %s "
                        "ORDER BY lot.data_inici desc LIMIT 1",
                        (pol.id, ('facturat', 'finalitzat'), from_date)
                    )
                    clot_ids = cursor.fetchall()
                    if len(clot_ids) and len(clot_ids[0]):
                        clot_ids = clot_ids[0]
                        clot = clot_obj.browse(cursor, uid, clot_ids[0])
                        ctx = context.copy()
                        ctx.update({'sync': False, 'from_baixa': True})
                        pol.write({'lot_facturacio': clot.lot_id.id},
                                  ctx)

    def create(self, cursor, uid, vals, context=None):
        res_id = super(GiscegasPolissa,
                       self).create(cursor, uid, vals, context)
        if vals.get('lot_facturacio', False):
            self.assignar_al_lot(cursor, uid, [res_id], vals['lot_facturacio'],
                                 context)
        return res_id

    def write(self, cursor, uid, ids, vals, context=None):
        """Quan guardem una pòlissa mirem si s'ha de reassignar a algun lot.
        """
        if 'lot_facturacio' in vals and not vals.get('lot_facturacio'):
            self.anullar_facturacions(cursor, uid, ids, context)

        if vals.get('lot_facturacio', False):
            self.assignar_al_lot(cursor, uid, ids, vals['lot_facturacio'],
                                 context)
        res = super(GiscegasPolissa, self).write(cursor, uid, ids, vals,
                                                  context)
        return res

    # Workflow stuff
    def wkf_facturacio(self, cursor, uid, ids):
        """Estat facturació."""
        for polissa in self.read(cursor, uid, ids, ['state']):
            vals = {'state': 'facturacio',
                    'state_post_facturacio': polissa['state']}
            self.write(cursor, uid, polissa['id'], vals)

    def wkf_baixa(self, cursor, uid, ids):
        """Es dona de baixa la pòlissa

        L'esborrem dels lots de facturació futurs.
        """
        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid)
        context = {'from_baixa': True}
        if user.context_lang:
            context.update({'lang': user.context_lang})                           

        #En el cas de baixes amb facturacio bimestral i
        #data de baixa del mes anterior al de la facturacio
        #anullar_facturacions la borra del lot quan no ho ha de fer
        #la tornem a assignar al lot, per que no la hem de perdre
        for polissa in self.browse(cursor, uid, ids):
            lot_facturacio = polissa.lot_facturacio
            if polissa.data_baixa <= polissa.data_ultima_lectura:
                self.anullar_facturacions(cursor, uid, [polissa.id], context=context)
            #Si es bimestral
            if polissa.facturacio == 2 and lot_facturacio:
                data_inici = datetime.strptime((lot_facturacio.
                                                data_inici),
                                               '%Y-%m-%d')
                data_final_ant = data_inici - timedelta(days=1)
                days = calendar.monthrange(data_final_ant.year,
                                           data_final_ant.month)[1]
                data_inici_ant = (data_final_ant
                                  - timedelta(days)
                                  + timedelta(days=1))
                if polissa.data_baixa >= datetime.strftime(data_inici_ant,
                                                           '%Y-%m-%d'):
                    polissa.write({
                        'lot_facturacio': lot_facturacio.id}, context=context
                    )

        super(GiscegasPolissa, self).wkf_baixa(cursor, uid, ids)

    def wkf_activa(self, cursor, uid, ids, context=None):
        '''On activate reassign the correct lot if assigned
        to a lot in the past. It is useful when reactivating
        or if an error introducing lot ocurred in new polisses.
        Only do it if is already invoiced in the assigned lot'''
        clot_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        super(GiscegasPolissa, self).wkf_activa(cursor, uid, ids)
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        facturacio_lot_obj = self.pool.get('giscegas.facturacio.lot')
        conf_obj = self.pool.get('res.config')

        polissa_lot_automatic = literal_eval(
            conf_obj.get(cursor, uid, 'giscegas_polissa_lot_automatic', 'False')
        )
        for polissa in self.browse(cursor, uid, ids):
            if polissa_lot_automatic and not polissa.lot_facturacio and polissa.data_alta:
                lot_id = facturacio_lot_obj.get_next(
                    cursor, uid, polissa.data_alta, polissa.facturacio, context=context
                )
                if lot_id:
                    params = ['state']
                    lot_vals = facturacio_lot_obj.read(
                        cursor, uid, [lot_id], params, context=context
                    )[0]
                    if lot_vals['state'] == 'tancat':
                        params = [('state', '=', 'obert')]
                        lot_obert_mes_vell_id = facturacio_lot_obj.search(
                            cursor, uid, params, limit=1, order='data_inici ASC'
                        )
                        if lot_obert_mes_vell_id:
                            lot_id = lot_obert_mes_vell_id[0]
                        else:
                            lot_id = False
                polissa.write({'lot_facturacio': lot_id}, context=context)
            else:
                lot_id = polissa.lot_facturacio.id
                #Search if we have already invoiced this polissa
                #in the assigned lot
                search_params = [('lot_id', '=', lot_id),
                                 ('polissa_id', '=', polissa.id)]
                clot_ids = clot_obj.search(cursor, uid, search_params)
                if clot_ids:
                    clot = clot_obj.browse(cursor, uid, clot_ids[0])
                    if (clot.state == 'finalitzat' and
                        polissa.lot_facturacio.data_final < today):
                        polissa.assignar_seguent_lot({'data_final': today})
                else:
                    self.assignar_al_lot(cursor, uid, ids, lot_id)

    def cnd_activa_cancelar(self, cursor, uid, ids):
        ''' Condició per comprovar que es pot cancel·lar una pòlissa activa. '''

        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        for id in ids:
            factura_ids = factura_obj.search(cursor, uid, [
                ('polissa_id', '=', id)
            ])
            if factura_ids:
                error_msg = _(u'La pòlissa té assignada una o més '
                              u'factures i no es pot cancel·lar.')
                raise osv.except_osv(_(u'Error !'), error_msg)

        return True

    def moure_al_lot(self, cursor, uid, cont_lot_ori_id, polissa_id,
                     lot_desti_id, context=None):
        cont_obj = self.pool.get('giscegas.facturacio.contracte_lot')

        self.write(cursor, uid, polissa_id, {
            'lot_facturacio': lot_desti_id,
        })

        existeix = cont_obj.search_count(cursor, uid, [
            ('id', '=', cont_lot_ori_id),
        ])

        if existeix:
            cont_obj.unlink(cursor, uid, cont_lot_ori_id)

    # Object stuff
    def assignar_al_lot(self, cursor, uid, ids, lot_id, context=None):
        """Assigna la pòlissa al lot dessitjat treient-la dels que no toqui.
        """
        if not context:
            context = {}
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        contracte_lot_obj = self.pool.get('giscegas.facturacio.contracte_lot')
        lot_facturacio_obj = self.pool.get('giscegas.facturacio.lot')
        lot_facturacio = lot_facturacio_obj.browse(cursor, uid,
                                                   lot_id, context)
        from_baixa = context.get('from_baixa', False)

        for polissa in self.browse(cursor, uid, ids):
            if not from_baixa and lot_facturacio.state == 'tancat':
                raise osv.except_osv(
                    _('Error'),
                    _("No es pot assignar al lot %s degut a que aquest lot "
                      "està en estat tancat")
                    % lot_facturacio.name
                )
            # Busquem els contractes que estiguin associats a lots més
            # antics i que estiguin en estat esborrany
            search_params = [
                ('polissa_id.id', '=', polissa.id),
                ('state', '=', 'esborrany'),
                ('lot_id.data_final', '<', lot_facturacio.data_inici),
            ]
            unlink_ids = contracte_lot_obj.search(
                cursor, uid, search_params, context={'active_test': False}
            )
            contracte_lot_obj.unlink(cursor, uid, unlink_ids, context)
            # Busquem els posteriors que estiguin en estat esborrany
            search_params = [
                ('polissa_id.id', '=', polissa.id),
                ('state', '=', 'esborrany'),
                ('lot_id.data_inici', '>', lot_facturacio.data_final),
            ]
            unlink_ids = contracte_lot_obj.search(
                cursor, uid, search_params, context={'active_test': False}
            )
            contracte_lot_obj.unlink(cursor, uid, unlink_ids, context)
            # Si no existeix ja al lot actual li assignem
            search_params = [
                ('polissa_id.id', '=', polissa.id),
                ('lot_id.id', '=', lot_facturacio.id),
            ]
            count = contracte_lot_obj.search_count(
                cursor, uid, search_params, context={'active_test': False}
            )
            if not count:
                vals = {
                    'polissa_id': polissa.id,
                    'lot_id': lot_facturacio.id,
                }
                contracte_lot_obj.create(cursor, uid, vals)
        return True

    def get_seguent_lot(self, cursor, uid, ids, data_final_factura):
        lot_obj = self.pool.get('giscegas.facturacio.lot')
        data_final = (datetime.strptime(data_final_factura, '%Y-%m-%d')
                      + timedelta(days=1)).strftime('%Y-%m-%d')
        for polissa in self.browse(cursor, uid, ids):
            # Do not move polissa if state is baixa
            if polissa.state == 'baixa' and polissa.data_ultima_lectura >= polissa.data_baixa:
                return polissa.lot_facturacio.id
            ctx = {}
            if (polissa.modcontractual_activa and
                polissa.modcontractual_activa.data_final >= data_final):
                ctx.update({'date': data_final})
            # Refresh polissa browse with updated context
            pol = self.browse(cursor, uid, polissa.id, ctx)
            return lot_obj.get_next(
                cursor, uid, data_final_factura, pol.facturacio
            )

    def assignar_seguent_lot(self, cursor, uid, ids, context=None):
        """Assigna la pòlissa al següent Lot, segons el tipus de facturació.

        Si en el context hi tenim el paràmetre data_inici el movem al lot
        següent segons data_inici. Si no existeix aquest paràmetre el mourà
        segons el lot actual que tingui assignat a la pòlissa.
        """
        if not context:
            context = {}
        data_final = context.get('data_final', False)
        for polissa in self.browse(cursor, uid, ids, context):
            if (not data_final and not polissa.lot_facturacio):
                continue
            if not data_final:
                data_final = polissa.lot_facturacio.data_final
            if (not polissa.active and polissa.data_ultima_lectura >= polissa.data_baixa):
                continue
            lot_id = polissa.get_seguent_lot(data_final)
            if not lot_id:
                raise osv.except_osv(
                    _('Error'), _(u"No s'ha trobat el lot per assignar la "
                                  u"pòlissa a %s") % data_final
                )
            if polissa.lot_facturacio.id != lot_id:
                polissa.write({'lot_facturacio': lot_id},
                              context={'sync': False})
        return True

    def get_inici_final_a_facturar(self, cursor, uid, polissa_id,
                                   use_lot=False, context=None):
        """Busca l'inici i final del periode de facturació.
        """
        if not context:
            context = {}
        if isinstance(polissa_id, list) or isinstance(polissa_id, tuple):
            polissa_id = polissa_id[0]
        polissa_obj = self.pool.get('giscegas.polissa')

        if use_lot:
            lot_obj = self.pool.get('giscegas.facturacio.lot')
            lot_facturacio = lot_obj.browse(cursor, uid, use_lot, context)
            dti = datetime.strptime(lot_facturacio.data_final, '%Y-%m-%d')
            ctx = context.copy()
            ctx['date'] = lot_facturacio.data_final
            # Utilitzem el context per tal que ens dongui una pòlissa amb els
            # valors de la data final del lot
            polissa = polissa_obj.browse(cursor, uid, polissa_id, ctx)
            facturacio = polissa.facturacio
            while facturacio:
                dies_del_mes = calendar.monthrange(dti.year, dti.month)[1]
                dti -= timedelta(days=dies_del_mes)
                facturacio -= 1
            dti += timedelta(days=1)
            data_inici = dti.strftime('%Y-%m-%d')
            data_final = lot_facturacio.data_final
        else:
            # No fem servir el les dates del lot per facturar si no que
            # utilitzem les dates de les lectures, això vol dir que sempre
            # s'haurà de sumar un dia si tenim una data anterior.
            polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
            # En anul·ladores i rectificadores tindrem les dates al context
            data_inici = context.get('ult_lectura_fact',
                                     polissa.data_ultima_lectura)
            data_final = context.get('fins_lectura_fact', False)
            if data_inici:
                comptadors = polissa.comptadors_actius(data_inici, data_final)
                dates_alta = [
                    c.data_alta for c in polissa.comptadors
                        if c.id in comptadors
                ]
                dates_alta.append(polissa.data_alta)
                if data_inici not in dates_alta:
                    # Les dates de lectures els hi hem de sumar un dia més.
                    data1 = (datetime.strptime(data_inici, '%Y-%m-%d')
                             + timedelta(days=1))
                    data1 = data1.strftime('%Y-%m-%d')
                else:
                    data1 = data_inici
                data2 = False
            else:
                data1 = False
                data2 = False
                if polissa.data_ultima_lectura_estimada:
                    data2 = (
                        datetime.strptime(polissa.data_ultima_lectura_estimada,
                                          '%Y-%m-%d') + timedelta(days=1)
                    )
                    data2 = data2.strftime('%Y-%m-%d')
            data_inici = max(data1, data2, polissa.data_alta)
            if not data_final:
                ctx = context.copy()
                ctx['data_inici_facturacio'] = data_inici
                data_final = polissa.data_utlima_lectura_entrada(ctx)

            if context.get('factura_manual', False):
                data_darrera_lect = polissa.data_utlima_lectura_entrada(context)
                if data_final > data_darrera_lect:
                    data_final = data_darrera_lect

        if data_final and data_inici and data_final < data_inici:
            if not context.get('validacio', False):
                raise osv.except_osv('Error',
                    _(u'Polissa: %s. La data final %s es mes petita que la '
                    u'data inicial %s' % (polissa.name, data_final,
                                          data_inici)))
        return (data_inici, data_final)

    def get_ultima_lectura_facturada(self, cursor, uid, polissa_id, data_inici,
                                     journal_code, context=None):
        '''Busquem la última lectura facturada. No podem mirar directament a la
        polissa per que pot ser una rectificacio anterior a factures posteriors
        i per tant aquesta data ja haura estat actualitzada. Anem a mirar
        factures anteriors fins que trobem una lectura facturada
        o en darrera instancia, tornem la data d'inici de la primera factura
        que sera igual a la data d'alta de la polissa'''
        factura_obj = self.pool.get('giscegas.facturacio.factura')

        if not context:
            context = {}
        context.update({'active_test': False})

        # Busquem totes les factures anul·ladores d'aqueseta pòlissa
        search_params = [
            ('type', '=', 'out_refund'),
            ('polissa_id', '=', polissa_id),
            ('state', 'in', ('open', 'paid')),
            ('invoice_id.journal_id.code', 'like', journal_code),
            ('tipo_rectificadora', 'in', REFUND_RECTIFICATIVE_INVOICE_TYPES)
        ]
        factura_ids = factura_obj.search(cursor, uid, search_params,
                                         context=context)
        refs = [x['ref'][0]
                for x in factura_obj.read(cursor, uid, factura_ids, ['ref'])]

        journal = journal_code
        if '.' in journal:
            journal = journal.split('.')[0]

        search_params = [
            ('data_final', '<', data_inici),
            ('type', '=', 'out_invoice'),
            ('polissa_id', '=', polissa_id),
            ('state', 'in', ('open', 'paid')),
            ('invoice_id.journal_id.code', 'ilike', journal),
        ]
        if refs:
            search_params += [('id', 'not in', refs)]
        factura_ids = factura_obj.search(cursor, uid, search_params,
                                  order='data_final desc, id desc',
                                  limit=1,
                                  context=context)
        if factura_ids:
            factura_ant = factura_obj.browse(cursor, uid, factura_ids[0])
            if factura_ant.lectures_ids:
                # D'una factura anterior la última lectura facturada serà la
                # mes gran
                ulf = max(l.data_actual for l in
                          factura_ant.lectures_ids)
            else:
                ulf = self.get_ultima_lectura_facturada(
                    cursor, uid, factura_ant.polissa_id.id,
                    factura_ant.data_inici, factura_ant.journal_id.code,
                    context=context)
        else:
            #Hem arribat a la darrera primera factura emesa sense trobar
            #lectures, per tant retornem la data d'alta de la polissa
            ulf = data_inici
        return ulf

    def get_modcontractual_intervals(self, cursor, uid, polissa_id, data_inici,
                                     data_final, context=None):
        """Obté tots els intervals a facturar d'un periode de facturació.

        Aquests són:
          * Modificacions contractuals.
        """
        if not context:
            context = {}
        if not 'ffields' in context:
            context['ffields'] = list(set(INTERVAL_INVOICING_FIELDS))
        dates_de_tall = super(GiscegasPolissa,
                               self).get_modcontractual_intervals(cursor, uid,
                                        polissa_id, data_inici, data_final,
                                        context)
        return dates_de_tall

    # on_change functions (views related)
    def onchange_titular(self, cursor, uid, ids, titular, pagador_sel, pagador,
                         tipo_pago,  notificacio=False, context=None):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not titular:
            return res
        if not pagador:
            pagador = titular

        pagador_sel = 'titular'
        vals_pagador = self.onchange_pagador_sel(
            cursor, uid, ids, pagador_sel, titular, pagador, None, tipo_pago,
            False, context=context
        )
        res.update(vals_pagador)
        if notificacio == 'titular':
            vals_notificacio = self.onchange_notificacio(
                cursor, uid, ids, 'titular', titular,
                vals_pagador['value']['pagador'], False, context=context
             )
            res['value'].update(vals_notificacio['value'])
            res['domain'].update(vals_notificacio['domain'])

        if not tipo_pago:
            partner = self.pool.get('res.partner').browse(cursor, uid, titular)
            res['value'].update({'tipo_pago': (partner.
                                               payment_type_customer.id)})
        return res

    def onchange_direccio_fiscal(self, cursor, uid, ids, direccio_pagament, context=None):
        adress_obj = self.pool.get('res.partner.address')
        res = {'value': {}, 'domain': {}, 'warning': {}}

        fields_add = adress_obj.fields_get(cursor, uid, fields=['id_municipi']).keys()

        if fields_add and direccio_pagament:
            addr = adress_obj.browse(cursor, uid, direccio_pagament, context=context)

            warrning_msg = ''
            if not addr.id_municipi:
                warrning_msg += _('Municipi de la direcció fiscal no configurat\n')
            if not addr.street:
                warrning_msg += _('Carrer de la direcció fiscal no configurat\n')
            if not addr.name:
                warrning_msg += _(
                    'Nom de la direcció fiscal no configurat\n'
                )
            if warrning_msg:
                res['warning'].update(
                    {
                        'title': _('Avís'),
                        'message': warrning_msg
                    })
        return res

    def onchange_pagador_sel(self, cursor, uid, ids, pagador_sel, titular,
                             pagador, direccio_pagament, tipo_pago,
                             notificacio, context=None):
        """Actua quan hi ha un canvi al camp pagador_sel.
        """
        partner_obj = self.pool.get('res.partner')
        res = {'value': {}, 'domain': {}, 'warning': {}}
        res['value']['pagador_sel'] = pagador_sel
        # emplenem el domain i value per la direccio de pagament
        if pagador_sel == 'titular' and titular:
            domain = [('partner_id', '=', titular)]
            dir_pago = partner_obj.address_get(cursor, uid, [titular],
                                               ['invoice'])['invoice']

            partner = self.pool.get('res.partner').browse(cursor, uid, titular)
            if not partner.payment_type_customer:
                res['warning'].update({'title': _('Avis'),
                                    'message': _(u"El pagador no té cap tipus "
                                                 u"de pagament predefinit")})
            else:
                res.get('value').update({'tipo_pago':
                                         partner.payment_type_customer.id})
            res['domain'].update({'direccio_pagament': domain,
                                  'pagador': [('id', '=', titular)]})
            res['value'].update({'direccio_pagament': dir_pago,
                                 'pagador': titular,
                                 'notificacio': pagador_sel})

            vals = self.onchange_notificacio(cursor, uid, ids, pagador_sel, titular, '', '')
            res['value'].update(vals['value'])
            res['domain'].update(vals['domain'])

        elif pagador_sel == 'altre_p':
            if pagador:
                res['value'].update({'pagador': False})
            if direccio_pagament:
                res['value'].update({'direccio_pagament': False})
            res['domain'].update({'direccio_pagament': [],
                                  'pagador': []})
        else:
            res['domain'].update({'direccio_pagament': [],
                                  'pagador': []})
            res['value'].update({'direccio_pagament': False,
                                 'pagador': False})
        # Finalment si a notificació hi tenim posat que és el pagador ho
        # modifiquem per tal que quadri
        if notificacio == 'pagador':
            res['domain'].update({'direccio_notificacio':
                                  res['domain'].get('direccio_pagament', [])})
            res['value'].update({'direccio_notificacio':
                                 res['value'].get('direccio_pagament', False)})
        return res

    def onchange_altre_pagador(self, cursor, uid, ids, altre_pagador,
                               notificacio, context=None):
        """Actua quan hi ha un canvi del camp 'altre_pagador'.
        """
        partner_obj = self.pool.get('res.partner')
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if altre_pagador:
            domain = [('partner_id', '=', altre_pagador)]
            value = partner_obj.address_get(cursor, uid, [altre_pagador],
                                            ['invoice'])['invoice']
        else:
            value = False
            domain = []

        res['value'].update({'direccio_pagament': value})
        res['domain'].update({'direccio_pagament': domain})

        # Si la direcció de notificació és "pagador" l'hem de modificar
        if notificacio == 'pagador':
            res['value'].update({'direccio_notificacio': value})
            res['domain'].update({'direccio_notificacio': domain})
        return res

    def onchange_notificacio(self, cursor, uid, ids, notificacio, titular,
                             pagador, altre_p, context=None):
        """Actua quan es canvia la persona de notificació
        """
        partner_obj = self.pool.get('res.partner')
        if notificacio == 'titular' and titular:
            domain = [('partner_id', '=', titular)]
            value = partner_obj.address_get(cursor, uid, [titular],
                                            ['contact'])['contact']
        elif notificacio == 'pagador' and pagador:
            domain = [('partner_id', '=', pagador)]
            value = partner_obj.address_get(cursor, uid, [pagador],
                                            ['contact'])['contact']
        elif notificacio == 'altre_p' and altre_p:
            domain = [('partner_id', '=', altre_p)]
            value = partner_obj.address_get(cursor, uid, [altre_p],
                                            ['contact'])['contact']
        else:
            domain = []
            value = False
        res = {'domain': {'direccio_notificacio': domain},
                'value': {'direccio_notificacio': value}}
        if notificacio != 'altre_p':
            res['value'].update({'altre_p': False})
        return res

    def onchange_altre_p(self, cursor, uid, ids, altre_p, context=None):
        """Actua quan es canvia el contacte alternatiu
        """
        partner_obj = self.pool.get('res.partner')
        if altre_p:
            domain = [('partner_id', '=', altre_p)]
            value = partner_obj.address_get(cursor, uid, [altre_p],
                                            ['contact'])['contact']
        else:
            value = False
            domain = []
        return {
            'value': {
               'direccio_notificacio': value
            },
            'domain': {
                'direccio_notificacio': domain
            }
        }

    def onchange_tipo_pago(self, cursor, uid, ids, tipo_pago, pagador, context=None):
        """
        if payment type is Recibo_CBS (Recibo domiciliado)
          if only exist one bank account automatic add it in field, 
          the same for payment group (Sepa 19)
        oterwise is cash(CAJA)
          release bank account from field.
        """
        
        res = {'warning': {}, 'value': {}, 'domain': {}}
        if tipo_pago:
            tpg = self.pool.get('payment.type').browse(cursor, uid, tipo_pago)
            if tpg.code == 'RECIBO_CSB':
                partner_bank_obj = self.pool.get('res.partner.bank')
                bank_ids = partner_bank_obj.search(cursor, uid, [
                    ('partner_id', '=', pagador)
                ])
                if len(bank_ids) == 1:
                    res['value'].update({'bank': bank_ids[0]})
            else:
                res['value'].update({'bank': False})
        return res

    def onchange_bank(
            self, cursor, uid, ids, propietari_bank, bank, context=None
    ):
        res = {}
        if not bank:
            res['propietari_bank'] = ''
        else:
            bank_obj = self.pool.get('res.partner.bank')
            owner_name = bank_obj.read(
                cursor, uid, [bank], ['owner_name']
            )[0]['owner_name']
            res['propietari_bank'] = owner_name
        return{
            'value': res
        }

    def update_mandate(self, cursor, uid, polissa_id, context=None):

        if not context:
            context = {}

        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]

        polissa = self.browse(cursor, uid, polissa_id)
        debtor_address = (polissa.direccio_pagament.
                          name_get(context=context)[0][1]).upper()
        debtor_contact = (polissa.direccio_pagament.name
                          and '%s, ' % polissa.direccio_pagament.name
                          or '').upper()
        if debtor_contact:
            debtor_address = debtor_address.replace(debtor_contact, '')
        debtor_state = (polissa.direccio_pagament.state_id
                        and polissa.direccio_pagament.state_id.name
                        or '').upper()
        debtor_country = (polissa.direccio_pagament.country_id
                          and polissa.direccio_pagament.country_id.name
                          or '').upper()
        notes = u"Contrato: %s\n" % polissa.name
        notes += u"CUPS: %s\n" % polissa.cups.name
        notes += u"Dirección de suministro: %s\n" % polissa.cups.direccio
        code_cnae = polissa.cnae and polissa.cnae.name or ''
        desc_cnae = polissa.cnae and polissa.cnae.descripcio or ''
        if code_cnae:
            notes += _(u"CNAE: (%s) %s\n") % (code_cnae, desc_cnae)

        if polissa.bank.owner_name:
            payment_data = u""
            bank_obj = self.pool.get('res.partner.bank')
            fields_bank = bank_obj.fields_get(
                cursor, uid, fields=['owner_id', 'owner_address_id']).keys()
            if fields_bank:
                if polissa.bank.owner_id:
                    payment_data += u" NIF: {0}".format(
                        polissa.bank.owner_id.vat
                    )
                else:
                    payment_data += u" NIF:"
                if polissa.bank.owner_address_id:
                    phone = polissa.bank.owner_address_id.phone
                    if not phone:
                        phone = polissa.bank.owner_address_id.mobile
                    payment_data += u" Teléfono: {0}".format(phone)
                else:
                    payment_data += u" Teléfono:"

            notes += u"Pagador:\n"
            notes += u"Nombre y apellidos: {0}{1}\n".format(
                polissa.bank.owner_name, payment_data
            )

        vals = {
            'debtor_name': polissa.pagador.name,
            'debtor_vat': polissa.pagador.vat,
            'debtor_address': debtor_address,
            'debtor_state': debtor_state,
            'debtor_country': debtor_country,
            'debtor_iban': polissa.bank.iban,
            'reference': '%s,%s' % ('giscegas.polissa', polissa_id),
            'notes': notes,
        }

        return vals

    def search_mandate(self, cursor, uid, polissa_id, iban, context=None):

        mandate_obj = self.pool.get('payment.mandate')

        # Search for mandate
        reference = 'giscegas.polissa,%s' % polissa_id
        search_params = [('reference', '=', reference),
                         ('debtor_iban', '=', iban)]
        mandate_ids = mandate_obj.search(cursor, uid, search_params, limit=1)
        if mandate_ids:
            return mandate_ids[0]
        return False

    def get_current_bank(self, cursor, uid, polissa_id, context=None):
        if context is None:
            context = {}
        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]
        res = {'partner_bank': False, 'mandate_id': False}
        polissa = self.read(cursor, uid, polissa_id, ['bank'])
        if polissa['bank']:
            partner_bank_obj = self.pool.get('res.partner.bank')
            iban = partner_bank_obj.read(
                cursor, uid, polissa['bank'][0], ['iban']
            )
            mandate_id = self.search_mandate(
                cursor, uid, polissa_id, iban['iban'], context=context
            )
            res.update(
                {
                    'partner_bank': polissa['bank'][0],
                    'mandate_id': mandate_id
                 }
            )
        return res

    def get_invoices(self, cursor, uid, polissa_id,
                     date_from, date_to, date_field='date_invoice',
                     context=None):
        '''get invoices from polissa between date_from and date_to'''

        factura_obj = self.pool.get('giscegas.facturacio.factura')
        journal_obj = self.pool.get('account.journal')

        search_params = [('code', 'like', 'GAS')]
        journal_ids = journal_obj.search(cursor, uid, search_params)

        search_params = [
            ('polissa_id', '=', polissa_id),
            ('type', '=', 'out_invoice'),
            ('state', 'in', ('open', 'paid')),
            (date_field, '>=', date_from),
            (date_field, '<=', date_to),
        ]

        factura_ids = factura_obj.search(cursor, uid, search_params)
        if not factura_ids:
            return []
        factura_vals = factura_obj.read(cursor, uid, factura_ids,
                                        ['ref', 'journal_id'])
        # Rectified invoices
        rectified_ids = [x['ref'][0] for x in factura_vals
                         if x['ref']]
        # Not GAS invoices
        not_journal_ids = [x['id'] for x in factura_vals
                           if x['journal_id'] and
                              x['journal_id'][0] not in journal_ids]
        # Return all not rectified invoices and with GAS journal
        return list(set(factura_ids) -
                    (set(rectified_ids) | set(not_journal_ids)))

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'lot_facturacio': False,
            'data_ultima_lectura': False,
            'data_ultima_lectura_estimada': False,
        }
        default.update(default_values)
        res_id = super(
            GiscegasPolissa, self).copy_data(cursor, uid, id, default, context)
        return res_id

    def _ff_prox_fact(self, cursor, uid, ids, field_name, arg, context=None):
        """Mètode per obtenir la data de pròxima facturació segons el lot.
        """
        lot_obj = self.pool.get('giscegas.facturacio.lot')

        res = {}
        fields_polissa = ['lot_facturacio']
        fields_lot = ['data_final']
        for polissa in self.read(cursor, uid, ids, fields_polissa):
            if polissa['lot_facturacio']:
                lot = lot_obj.read(cursor, uid, polissa['lot_facturacio'][0],
                                   fields_lot)
                res[polissa['id']] = lot['data_final']
            else:
                res[polissa['id']] = False
        return res

    def _get_prox_fact_ids(self, cursor, uid, ids, context=None):
        """Retorna els ids pels quals recalcular el camp proxima_facturacio
        """
        return ids

    def _ff_darrera_lectura(self, cursor, uid, ids, field_name, arg, context=None):
        '''Calcula la data de la darrera lectura facturada'''

        query_file = (u"%s/giscegas_facturacio/sql/"
                      u"darrera_lectura_facturada.sql"
                      % config['addons_path'])
        query = open(query_file).read()

        cursor.execute(query, (tuple(ids), ))
        res = dict([(x[0], x[1]) for x in cursor.fetchall()])
        #If some polissa do not have any invoices, it will not come
        #in query results. Write a false for all of them
        no_value_ids = list(set(ids) - set(res.keys()))
        no_value_res = dict.fromkeys(no_value_ids, False)
        res.update(no_value_res)
        return res

    def _ff_darrera_lectura_inv(self, cursor, uid, ids, name, value,
                            fnct_inv_arg, context=None):
        if isinstance(value, bool) and not value:
            value = None
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        query = '''
            UPDATE giscegas_polissa
            SET data_ultima_lectura = %s
            WHERE id = %s
            '''
        for id in ids:
            cursor.execute(query, (value, id))
            
        return True

    _store_darrera_lectura = {
        'account.invoice': (
            _get_polissa_from_energy_invoice, ['state'], 10
        )
    }

    def _ff_deposit(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        cfg = self.pool.get('res.config')
        product_obj = self.pool.get('product.product')
        product_id = int(cfg.get(cursor, uid, 'deposit_product_id', 0))
        if not product_id:
            return res
        acc_id = product_obj.get_account(cursor, uid, product_id, 'income')
        polisses = dict(
            (x['name'], x['id']) for x in self.read(cursor, uid, ids, ['name'])
            if x['name']
        )
        if not polisses:
            return res
        sql = (
            "select ref, sum(coalesce(debit, 0) - coalesce(credit, 0)) from account_move_line "
            "where product_id = %s and ref in %s and account_id = %s"
            "group by ref"
        )
        cursor.execute(sql, (product_id, tuple(polisses.keys()), acc_id))
        deposits = dict((polisses[x[0]], x[1]) for x in cursor.fetchall())
        res.update(deposits)
        return res

    _columns = {
        'facturacio': fields.selection(
            FACTURACIO_SELECTION, 'Facturación', readonly=True,
            states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False),
                          ('required', True)],
              'modcontractual': [('readonly', False),
                                 ('required', True)]
            }
        ),
        'state_post_facturacio': fields.char("Estado post facturación", size=64),
        'tarifa_codi': fields.related(
          'tarifa', 'name', type='char', string='Código Tarifa'
        ),
        'proxima_facturacio': fields.function(
          _ff_prox_fact, method=True, string=u'Proxima facturación',
          store={
              'giscegas.polissa': (
                  _get_prox_fact_ids,['lot_facturacio'],10
              )
          }, type='date'
        ),
        'property_unitat_terme_fix': fields.property(
            'product.uom',
            type='many2one',
            relation='product.uom',
            string=u"Unitat de facturació terme fix",
            method=True,
            view_load=True,
            domain="[('category_id.name', '=', 'Termino Fijo GAS')]",
            help=u"Amb quina unitat es vol facturar terme_fix",
            readonly=True,
            states={'esborrany': [('readonly', False)],
                    'validar': [('readonly', False), ('required', True)],
                    'modcontractual': [('readonly', False), ('required', True)]
                    }
        ),
        'lot_facturacio': fields.many2one(
          'giscegas.facturacio.lot', u'Lote de facturación'
        ),
        'data_ultima_lectura': fields.function(
          _ff_darrera_lectura, fnct_inv=_ff_darrera_lectura_inv, method=True,
          type='date', string=u'Data última real facturada',
                                store=_store_darrera_lectura
        ),
        'data_ultima_lectura_estimada': fields.date(
          u'Fecha última estimada facturada'
        ),
        'pagador_sel': fields.selection(
          _pagador_selection, 'Persona fiscal', size=64, readonly=True,
          states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False), ('required', True)],
              'modcontractual': [('readonly', False), ('required', True)]
          }
        ),
        'pagador': fields.many2one(
          'res.partner', u'Razón fiscal', select=True, readonly=True, size=40,
          states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False), ('required', True)],
              'modcontractual': [('readonly', False), ('required', True)]
          }
        ),
        'pagador_nif': fields.related(
          'pagador', 'vat', type='char', string='NIF fiscal', readonly=True
        ),
        'direccio_pagament': fields.many2one(
          'res.partner.address', u'Dirección fiscal', readonly=True,
          states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False), ('required', True)],
              'modcontractual': [('readonly', False), ('required', True)]
          }
        ),
        'notificacio': fields.selection(
          NOTIFICACIO_SELECTION, u'Persona notificación', readonly=True,
          states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False), ('required', True)],
              'modcontractual': [('readonly', False), ('required', True)]
          }
        ),
        'altre_p': fields.many2one(
          'res.partner', 'Contacto alternativo', ondelete='restrict',
          readonly=True, states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False)],
              'modcontractual': [('readonly', False)]
          }, size=40
        ),
        'direccio_notificacio': fields.many2one(
          'res.partner.address', u'Dirección notificació', readonly=True,
          states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False), ('required', True)],
              'modcontractual': [('readonly', False), ('required', True)]
          }
        ),
        'bank': fields.many2one(
          'res.partner.bank', 'Cuenta bancaria',
          ondelete='restrict', readonly=True,
          states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False)],
              'modcontractual': [('readonly', False)]
          }
        ),
        'propietari_bank': fields.related(
          'bank', 'owner_name', type='char', size=128,
          string='Propietario cuenta bancaria', readonly=True
        ),
        'tipo_pago': fields.many2one(
          'payment.type', 'Tipo de pago', readonly=True,
          states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False), ('required', True)],
              'modcontractual': [('readonly', False), ('required', True)]
          }
        ),
        'llista_preu': fields.many2one(
          'product.pricelist', 'Tarifa Comercializadora',
          domain=[('type', '=', 'sale')], readonly=True,
          states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False), ('required', True)],
              'modcontractual': [('readonly', False), ('required', True)]
          }
        ),
        'versio_primera_factura': fields.many2one(
          'product.pricelist.version', u'Versió primera facturació',
          help="Establece la versión de la lista de precios que se va a usar "
               "para la primera facturación.", readonly=True,
          states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False), ('required', False)],
              'modcontractual': [('readonly', False), ('required', False)]
          }
        ),
        'facturacio_endarrerida': fields.function(
          _ff_fact_endarrerida, method=True, type='boolean',
          string='Facturación atrasada', fnct_search=_search_fact_endarrerida,
          readonly=True
        ),
        'deposit': fields.function(
          _ff_deposit, method=True, type='float', string=u'Depósito'
        )
    }

    _defaults = {
      'facturacio': lambda *a: 1,
    }

    def _cnt_lot_data_alta(self, cursor, uid, ids, context=None):
        for polissa in self.browse(cursor, uid, ids, context=context):
            if (polissa.data_alta and
                polissa.lot_facturacio and
                polissa.data_alta >
                polissa.lot_facturacio.data_final):
                return False
        return True

    def _cnt_ccc_pagador(self, cursor, uid, ids, context=None):
        '''returns false if the ccc is not from the pagador'''
        for polissa in self.browse(cursor,  uid, ids, context=context):
            if (not polissa.state in CONTRACT_IGNORED_STATES +
                                          ['baixa', 'modcontractual'] and
                polissa.pagador and
                polissa.bank and
                polissa.pagador.id != polissa.bank.partner_id.id):
                return False
        return True

    _constraints = [
        (
            _cnt_lot_data_alta, _(u"Error: No se puede asignar un lote "
                                u"anterior a la fecha de alta del contrato"),
            ['data_alta', 'lot_facturacio']),
        (
            _cnt_ccc_pagador, _(u"Error: La cuenta bancaria no "
                                u"pertenece al pagador"),
            ['bank', 'pagador']
        ),
    ]

GiscegasPolissa()


class GiscegasPolissaModcontractual(osv.osv):

    _name = 'giscegas.polissa.modcontractual'
    _inherit = 'giscegas.polissa.modcontractual'

    _columns = {
        'facturacio': fields.selection(
            FACTURACIO_SELECTION, 'Facturación', required=True
        ),
        'property_unitat_terme_fix': fields.property(
            'product.uom',
            type='many2one',
            relation='product.uom',
            string=u"Unitat de facturació terme fix",
            method=True,
            view_load=True,
            domain="[('category_id.name', '=', 'Termino Fijo GAS')]",
            help=u"Amb quina unitat es vol facturar el terme fix",
            readonly=True,
            states={'esborrany': [('readonly', False)],
                    'validar': [('readonly', False), ('required', True)],
                    'modcontractual': [('readonly', False), ('required', True)]

                }
        ),
        'pagador': fields.many2one('res.partner', u'Razón fiscal', required=True),
        'notificacio': fields.selection(
            NOTIFICACIO_SELECTION, u'Persona notificación', required=True
        ),
        'altre_p': fields.many2one('res.partner', 'Contacto alternativo'),
        'direccio_notificacio': fields.many2one(
            'res.partner.address', u'Dirección notificación'
        ),
        'direccio_pagament': fields.many2one(
            'res.partner.address', u'Dirección fiscal'
        ),
        'tipo_pago': fields.many2one(
            'payment.type', 'Tipo de pago', required=True
        ),
        'bank': fields.many2one('res.partner.bank', 'Cuenta bancaria'),
        'llista_preu': fields.many2one(
            'product.pricelist', 'Tarifa Comercialitzadora'
        ),
    }


GiscegasPolissaModcontractual()
