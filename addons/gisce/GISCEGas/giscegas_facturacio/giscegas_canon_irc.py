# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, orm, fields
import netsvc
from time import strftime
from datetime import datetime, timedelta
from gestionatr.defs_gas import TAULA_CONCEPTO_FACTURACION
from tools.translate import _

# Codigos tabla CONCEPTOS FACTURACION
# 1302 - Canon Finca
# 1911 - Canon IRC 1
# 1912 - Canon IRC 2
# 1913 - Canon Plan 10
# 1928 - Canon IRC 3
# 1936 - Canon IRC 4

# CAMPOS A TRATAR DE UN B70
# <concepto>
#   <fecdesde>            - Fecha inicio periodo
#   <fechasta>            - Fecha final periodo
#   <unidad>              - Numero de dias?
#   <precunidad>          - Precio por dia
#   <importe>             -
#   <codconcepto>         - Codigo de la TABLA CONCEPTOS
#   <desconcepto>         - Descripcion del concepto
#   <porcentajeconcepto>  -
#   <impuestoconcepto>    - Aplicar impuesto si/no
#   <codtipoimpuesto>     -
#   <porcentajeimpcto>    -
# <concepto>


class CanonIRC(osv.osv):

    _name = 'giscegas.canon.irc'
    _description = 'Canon asociado para los b70'

    _columns = {
        'name': fields.char('Nom', size=128),
        'polissa_id': fields.many2one(
            'giscegas.polissa', 'Contracte', required=True
        ),
        'canon_product_id': fields.many2one(
            'product.product', 'Canon IRC', required=True,
            domain=[
                ('default_code', 'ilike', 'CONG%'),
                ('product_tmpl_id.name', 'ilike', 'canon%')
            ]
        ),
        'data_alta': fields.date('Data alta'),
        'data_baixa': fields.date('Data baixa'),
        'preu_dia': fields.float('Preu per dia', digits=(16, 3)),
        'active': fields.boolean('Actiu'),
        'codi_cnmc': fields.char('Codi CNMC', size=4, required=True)
    }

    _defaults = {
        'data_alta': lambda *a: strftime('%Y-%m-%d'),
        'active': lambda *a: True
    }

CanonIRC()


class GisceGasPolissa(osv.osv):

    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def get_active_canons_by_period(self, cursor, uid, ids, start_date, end_date, context=None):
        if context is None:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        select_canon_ids_from_polissa = (
            'SELECT array_agg(canon.id) AS canon_ids '
            'FROM giscegas_canon_irc AS canon '
            'WHERE canon.polissa_id = %s '
            'AND '
            '('
            '(canon.data_baixa IS NULL AND canon.data_alta <= %s) '
            'OR '
            '(canon.data_baixa IS NOT NULL AND ((%s BETWEEN canon.data_alta AND canon.data_baixa) OR (%s BETWEEN canon.data_alta AND canon.data_baixa)) )'           
            ')'
        )

        cursor.execute(select_canon_ids_from_polissa, (ids, end_date, start_date, end_date))

        canon_ids = cursor.dictfetchone()['canon_ids']

        return canon_ids

    _columns = {
        'canon_irc_ids': fields.one2many(
            'giscegas.canon.irc', 'polissa_id', 'Canons IRC', context={'active_test': False}
        )
    }

GisceGasPolissa()


class GiscegasFacturacioFactura(osv.osv):

    _name = 'giscegas.facturacio.factura'
    _inherit = 'giscegas.facturacio.factura'

    def get_active_canons_by_period(self, cursor, uid, ids, start_date, end_date, context=None):
        if context is None:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        pol_obj = self.pool.get('giscegas.polissa')

        fact_fields = self.read(
            cursor, uid, ids,
            ['polissa_id', 'data_inici', 'data_final'],
            context=context
        )

        polissa_id = fact_fields['polissa_id'][0]

        return pol_obj.get_active_canons_by_period(
            cursor, uid, polissa_id, start_date, end_date, context=context
        )


GiscegasFacturacioFactura()


class GiscegasFacturacioFacturador(osv.osv):
    """Metaclasse per generar les factures (no té taula SQL).
    """
    _name = 'giscegas.facturacio.facturador'
    _inherit = 'giscegas.facturacio.facturador'

    def config_facturador_with_canons(
            self, cursor, uid, facturador, polissa_id, context=None
    ):
        return True

    def crear_linies_canon(self, cursor, uid, factura_id, vals, context=None):
        """Creem les línies de canon per la factura segons els termes.
        """
        return self.crear_linia(cursor, uid, factura_id, vals, context)

    def crear_linies_canon_account_invoice(
            self, cursor, uid, factura_id, vals, context=None):
        """Creem les línies de canon per la factura (invoice) segons els termes.
        """
        return self.crear_linia_account_invoice(cursor, uid, factura_id, vals, context)

    def fact_canons(self, facturador, canon, data_inici_periode_f, data_final_periode_f, context=None):
        """
        Invoices Energy
        :param facturador: Tariff class
        """
        raise osv.except_osv(u"Error", _('Per ser implementat en el mòdul de '
                                         'distribució/comercialització.'))

GiscegasFacturacioFacturador()
