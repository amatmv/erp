# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from expects import *
from datetime import datetime, timedelta


class TestGiscegasLectures(testing.OOTestCase):

    def test_duplicate_polissa(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        config_obj = self.openerp.pool.get('res.config')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]

            # Set duplicated to 0, the meter should not be duplicated
            config_obj.set(cursor, uid, 'giscegas_duplicate_meters_when_contract', 0)

            default_values = {
                'comptador': False,
                'comptadors': []
            }

            copy_res_id = polissa_obj.copy(cursor, uid, polissa_id)
            polissa_data = polissa_obj.read(
                cursor, uid, copy_res_id, default_values.keys()
            )
            polissa_data.pop('id')
            self.assertDictEqual(default_values, polissa_data)

    def test_duplicate_polissa_duplicated_meters_res_select(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        config_obj = self.openerp.pool.get('res.config')
        meter_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]

            # Get polissa data
            polissa_data = polissa_obj.read(
                cursor, uid, polissa_id, ['comptador', 'comptadors'])
            comptadors_ids = polissa_data['comptadors']

            old_meter_name = polissa_data['comptador']

            # Set duplicated to 1, the meter should be duplicated
            config_obj.set(txn.cursor, txn.user,
                           'giscegas_duplicate_meters_when_contract', 1)

            # Copy, copy_res_id is new id
            copy_res_id = polissa_obj.copy(cursor, uid, polissa_id)

            new_polissa_data = polissa_obj.read(
                cursor, uid, copy_res_id, ['comptador', 'comptadors']
            )
            new_comptadors_ids = new_polissa_data['comptadors']
            new_meter_name = new_polissa_data['comptador']
            # Check different meters id's
            self.assertFalse(comptadors_ids == new_comptadors_ids)
            self.assertEqual(old_meter_name, new_meter_name)
            # Compare names of meter
            list_old_meters_names = meter_obj.read(
                cursor, uid, comptadors_ids, ['name'])

            list_old_meters_names = [a['name'] for a in list_old_meters_names]

            list_new_meters_names = meter_obj.read(
                cursor, uid, new_comptadors_ids, ['name'])

            list_new_meters_names = [a['name'] for a in list_new_meters_names]

            self.assertEqual(list_old_meters_names, list_new_meters_names)

            # Check if new meter exist
            self.assertTrue(meter_obj.read(cursor, uid, new_comptadors_ids))

    def test_are_dates_correct(self):
        compt_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            compt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'comptador_gas_0001'
            )[1]

            # End of the period can't be before start of the period
            assert not compt_obj.are_dates_correct(
                cursor, uid, compt_id, '2019-01-01', '2018-01-01'
            )
            # Checks before the start
            assert not compt_obj.are_dates_correct(
                cursor, uid, compt_id, '2017-01-01', '2017-01-01'
            )
            assert not compt_obj.are_dates_correct(
                cursor, uid, compt_id, '2017-01-01', '2018-01-01'
            )
            # Checks with start date
            assert compt_obj.are_dates_correct(  # Start day should be accepted
                cursor, uid, compt_id, '2018-01-01', '2018-01-01'
            )
            assert compt_obj.are_dates_correct(  # Correct period
                cursor, uid, compt_id, '2018-01-01', '2019-01-01'
            )
            # Same as above but with dates diffrent than the start day
            assert compt_obj.are_dates_correct(  # Single day test
                cursor, uid, compt_id, '2019-01-01', '2019-01-01'
            )
            assert compt_obj.are_dates_correct(  # Correct period
                cursor, uid, compt_id, '2019-01-01', '2020-01-01'
            )
            assert not compt_obj.are_dates_correct(  # End can't be before start
                cursor, uid, compt_id, '2019-01-01', '2018-06-01'
            )

            compt_obj.write(cursor, uid, compt_id, {'data_baixa': '2021-01-01'})

            # We check that the previous is still true
            # End of the period can't be before start of the period
            assert not compt_obj.are_dates_correct(
                cursor, uid, compt_id, '2019-01-01', '2018-01-01'
            )
            # Checks before the start
            assert not compt_obj.are_dates_correct(
                cursor, uid, compt_id, '2017-01-01', '2017-01-01'
            )
            assert not compt_obj.are_dates_correct(
                cursor, uid, compt_id, '2017-01-01', '2018-01-01'
            )
            # Checks with start date
            assert compt_obj.are_dates_correct(  # Start day should be accepted
                cursor, uid, compt_id, '2018-01-01', '2018-01-01'
            )
            assert compt_obj.are_dates_correct(  # Correct period
                cursor, uid, compt_id, '2018-01-01', '2019-01-01'
            )
            # Same as above but with dates diffrent than the start day
            assert compt_obj.are_dates_correct(  # Single day test
                cursor, uid, compt_id, '2019-01-01', '2019-01-01'
            )
            assert compt_obj.are_dates_correct(  # Correct period
                cursor, uid, compt_id, '2019-01-01', '2020-01-01'
            )
            assert not compt_obj.are_dates_correct(  # End can't be before start
                cursor, uid, compt_id, '2019-01-01', '2018-06-01'
            )
            # We do checks with end date
            assert compt_obj.are_dates_correct(  # Start day should be accepted
                cursor, uid, compt_id, '2021-01-01', '2021-01-01'
            )
            assert compt_obj.are_dates_correct(  # Correct period
                cursor, uid, compt_id, '2020-01-01', '2021-01-01'
            )
            # Checks after the end
            assert not compt_obj.are_dates_correct(
                cursor, uid, compt_id, '2022-01-01', '2022-01-01'
            )
            assert not compt_obj.are_dates_correct(
                cursor, uid, compt_id, '2022-01-01', '2023-01-01'
            )
            # Check accepts 1 day before activation
            assert compt_obj.are_dates_correct(
                cursor, uid, compt_id, '2017-12-31', '2019-01-01'
            )
            # Check doesn't accepts 2 day before activation
            assert not compt_obj.are_dates_correct(
                cursor, uid, compt_id, '2017-12-30', '2019-01-01'
            )

    def test_get_lectures_have_ajust(self):
        comptador_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        imd = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            comptador_id = imd.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'comptador_gas_0001'
            )[1]
            comptador_gas_0001 = comptador_obj.browse(cursor, uid, comptador_id)
            lectures = comptador_gas_0001.get_lectures(
                comptador_gas_0001.polissa.tarifa.id,
                comptador_gas_0001.polissa.data_alta, True
            )
            for lect_periode in lectures:
                for lectura in lectures[lect_periode]:
                    l = lectures[lect_periode][lectura]
                    self.assertIsNotNone(
                        l['ajust'], 'lectures.ajust is not inistialized'
                    )
                    self.assertIsNotNone(
                        l['motiu_ajust'],
                        'lectures.motiu_ajust is not inistialized'
                    )
                    if l['name'] == '2018-03-03':
                        self.assertEqual(
                            l['ajust'], 3,
                            'lectura with name 2018-03-03 must have ajust'
                            ' of 3 as it was declared on demo.xml'
                        )
                    elif l['name'] == '2018-04-04':
                        self.assertEqual(
                            l['ajust'], 15,
                            'lectura with name 2018-03-03 must have ajust'
                            ' of 15 as it was declared on demo.xml'
                        )
                        self.assertEqual(
                            l['motiu_ajust'], '01',
                            'lectura with name 2018-03-03 must have motiu'
                            ' ajust of "01" as it was declared on demo.xml'
                        )
                    else:
                        self.assertEqual(
                            l['ajust'], 0,
                            'All default lectures from demo.xml must have'
                            ' ajust of 0'
                        )

    def test_get_consum_returns_energy_and_ajust(self):
        comptador_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        imd = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            comptador_id = imd.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'comptador_gas_0001'
            )[1]
            comptador_gas_0001 = comptador_obj.browse(cursor, uid, comptador_id)
            lectures = comptador_gas_0001.get_lectures(
                comptador_gas_0001.polissa.tarifa.id,
                comptador_gas_0001.polissa.data_alta, True)
            test_consum = {}
            for periode in lectures.keys():
                test_consum[periode] = (
                    lectures[periode]['actual']['consum']
                    + lectures[periode]['actual']['ajust']
                )
            consum = comptador_gas_0001.get_consum(
                comptador_gas_0001.polissa.tarifa.id,
                comptador_gas_0001.polissa.data_alta, True
            )
            self.assertEqual(
                test_consum, consum,
                'get_consum must return consum + ajust'
            )

    def test_creating_giscegas_lectures_lectura_has_default_ajust_0(self):
        lectura_obj = self.openerp.pool.get('giscegas.lectures.lectura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'p1_v_tarifa_31_gas'
            )[1]
            comptador_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'comptador_gas_0001'
            )[1]

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'origen_gas_1'
            )[1]

            vals = {
                'name': '2018-01-01',
                'periode': periode_id,
                'lectura': 10,
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
                'pressio_subministrament': 1,
                'factor_k': 1,
                'pcs': 1,
            }

            lectura_id = lectura_obj.create(cursor, uid, vals)
            lectura = lectura_obj.browse(cursor, uid, lectura_id)

            assert lectura.ajust == 0
            assert not lectura.motiu_ajust
            
    def test_get_lectures_per_facturar_nou_criteri(self):
        comptador_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        lectura_obj = self.openerp.pool.get('giscegas.lectures.lectura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            comptador_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'comptador_gas_0001'
            )[1]
            comptador = comptador_obj.browse(cursor, uid, comptador_id)
            # Delete all lectures from meter
            for l in comptador.lectures:
                l.unlink(context={})
            # Set new data_alta
            polissa = comptador.polissa
            comptador.write({'data_alta': polissa.data_alta})
            # Create lectures 1 day before activation
            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'p1_v_tarifa_31_gas'
            )[1]
            comptador_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'comptador_gas_0001'
            )[1]
            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'origen_gas_1'
            )[1]
            dalta = datetime.strptime(polissa.data_alta, "%Y-%m-%d")
            vals = {
                'name': dalta - timedelta(days=1),
                'periode': periode_id,
                'lectura': 0,
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
                'factor_k': 1.0,
                'pcs': 1.0,
                'pressio_subministrament': 1,
            }
            lectura_obj.create(cursor, uid, vals)
            # Create other lects
            vals = {
                'name': dalta + timedelta(days=10),
                'periode': periode_id,
                'lectura': 10,
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
                'factor_k': 1.0,
                'pcs': 1.0,
                'pressio_subministrament': 1,
            }
            lectura_obj.create(cursor, uid, vals)
            vals = {
                'name': dalta + timedelta(days=20),
                'periode': periode_id,
                'lectura': 20,
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
                'factor_k': 1.0,
                'pcs': 1.0,
                'pressio_subministrament': 1,
            }
            lectura_obj.create(cursor, uid, vals)
            res = comptador.get_lectures_per_facturar(tarifa_id=polissa.tarifa.id)
            self.assertEqual(res['P1']['actual']['consum'], 20)
            self.assertFalse(res['P1']['anterior']['consum'])
