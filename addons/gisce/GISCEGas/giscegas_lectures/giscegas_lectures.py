# -*- coding: utf-8 -*-
"""Models per la presa de lectures
"""
from __future__ import absolute_import

from osv import osv, fields
from tools.translate import _
import time
from gestionatr.defs_gas import *
from osv.expression import OOQuery
from giscegas_polissa.giscegas_polissa import CONTRACT_IGNORED_STATES
from datetime import datetime, timedelta


def recalc_consums(lectures, giro):
    """Funció per tal d'assegurar que el consum de lectures està ben calculat.
    """
    for periode in lectures.keys():
        lectura = lectures[periode]
        if not lectura['anterior']:
            # Si no hi ha lectura anterior és que és una lectura inicial!
            # Consum 0
            consum = 0
        else:
            consum = lectura['actual'].get('consum', 0) + lectura['actual'].get('ajust', 0)
        if consum < 0:
            consum += giro
        lectura['actual']['consum'] = float(consum)
    return lectures

# TODO: Posar restriccions només una lectura per periode


class GiscegasLecturesComptador(osv.osv):
    """Comptadors
    """

    _name = 'giscegas.lectures.comptador'

    def data_ultima_lectura(self, cursor, uid, comptador_id, tarifa_id,
                            context=None):
        """Retorna la data de l'útlima lectura segons la tarifa que s'indiqui.
        """
        if context is None:
            context = {}

        if isinstance(comptador_id, (list, tuple)):
            comptador_id = comptador_id[0]

        comptador = self.browse(cursor, uid, comptador_id, context)
        periodes_obj = self.pool.get('giscegas.polissa.tarifa.periodes')
        lectures_obj = self.pool.get('giscegas.lectures.lectura')
        search_params = [
            ('tarifa.id', '=', tarifa_id),
            ('tipus', '=', 'te'),
        ]
        periodes_ids = periodes_obj.search(cursor, uid, search_params,
                                           context=context)
        search_params = [
            ('periode.id', 'in', periodes_ids),
            ('comptador.id', '=', comptador.id)
        ]

        if context.get('fins_lectura_fact', False):
            search_params.append(('name', '<=', context['fins_lectura_fact']))

        context.update({'active_test': False})
        lectures_ids = lectures_obj.search(cursor, uid, search_params, limit=1,
                                           order="name desc", context=context)
        if lectures_ids:
            data_ultima_lectura = lectures_obj.read(cursor, uid, lectures_ids,
                                                    ['name'])[0]['name']
            return data_ultima_lectura
        return False

    def get_consum_per_facturar(self, cursor, uid, comptador_id, tarifa_id,
                                periodes=None, context=None):
        """Calcula el consum pendent per facturar.
        """
        if not context:
            context = {}
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id, context)
        cdata_alta = datetime.strptime(comptador.polissa.data_alta, "%Y-%m-%d")
        cdata_alta -= timedelta(days=1)
        cdata_alta = cdata_alta.strftime("%Y-%m-%d")
        ult_lectura_fact = max(comptador.polissa.data_ultima_lectura,
                               comptador.polissa.data_ultima_lectura_estimada,
                               cdata_alta)
        ult_lectura_fact = context.get('ult_lectura_fact', ult_lectura_fact)
        return comptador.get_consum(
            tarifa_id, ult_lectura_fact, True, periodes, context)

    def get_consum(self, cursor, uid, comptador_id, tarifa_id, from_date,
                   per_facturar=False, periodes=None, context=None):
        """Calcula el consum.

        En el cas que la lectura a facturar sigui real.
        """
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        consums = {}
        lectures = self.get_lectures(cursor, uid, comptador_id, tarifa_id,
                                     from_date, per_facturar, periodes, context)
        for periode in lectures.keys():
            if lectures[periode].get('anterior'):
                act = lectures[periode]['actual']['lectura']
                ant = lectures[periode]['anterior']['lectura']
                consum = act - ant
            else:
                consum = 0
            consums[periode] = consum * lectures[periode]['actual'].get('factor_k', 0.0) * lectures[periode]['actual'].get('pcs', 0.0)
        return consums

    def get_consum_m3(self, cursor, uid, comptador_id, tarifa_id, from_date,
                   per_facturar=False, periodes=None, context=None):
        """Calcula el consum.

        En el cas que la lectura a facturar sigui real.
        """
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        consums = {}
        lectures = self.get_lectures(cursor, uid, comptador_id, tarifa_id,
                                     from_date, per_facturar, periodes, context)
        for periode in lectures.keys():
            if lectures[periode].get('anterior'):
                act = lectures[periode]['actual']['lectura']
                ant = lectures[periode]['anterior']['lectura']
                consum = act - ant
            else:
                consum = 0
            consums[periode] = consum
        return consums

    def get_lectures_per_facturar(self, cursor, uid, comptador_id, tarifa_id,
                                  periodes=None, context=None):
        """
        Retorna les lectures pendents de facturar.

        :return: {
            periode: {
                'actual': {
                    'consum': X,
                },
                'anterior': {
                    'consum': X,
                },
            }
        }
        """
        if not context:
            context = {}
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]

        comptador = self.browse(cursor, uid, comptador_id, context)
        cdata_alta = datetime.strptime(comptador.polissa.data_alta, "%Y-%m-%d")
        cdata_alta -= timedelta(days=1)
        cdata_alta = cdata_alta.strftime("%Y-%m-%d")
        ult_lectura_fact = max(
            comptador.polissa.data_ultima_lectura,
            comptador.polissa.data_ultima_lectura_estimada,
            cdata_alta
        )

        ult_lectura_fact = context.get('ult_lectura_fact', ult_lectura_fact)
        return comptador.get_lectures(
            tarifa_id, ult_lectura_fact, True, periodes, context
        )

    def get_lectures(self, cursor, uid, comptador_id, tarifa_id, from_date,
                     per_facturar=False, periodes=None, context=None):
        """
        Busca les lectures actuals i anteriors.

        :return: {
            periode: {
                'actual': {
                    'consum': X,
                },
                'anterior': {
                    'consum': X,
                },
            }
        }
        """
        if not context:
            context = {}
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id)
        lectures = {}
        periodes_obj = self.pool.get('giscegas.polissa.tarifa.periodes')
        lectures_obj = self.pool.get('giscegas.lectures.lectura')
        search_params = [
            ('tarifa.id', '=', tarifa_id),
            ('tipus', '=', 'te'),
        ]
        if not periodes:
            ctx = context.copy()
            ctx.update({'active_test': False})
            periodes_ids = periodes_obj.search(
                cursor, uid, search_params, context=ctx
            )
        else:
            periodes_ids = periodes[:]
        operator = '='
        if per_facturar:
            operator = '>'
        search_params = [
            ('comptador.id', '=', comptador_id),
            ('periode.id', 'in', periodes_ids),
            ('name', operator, from_date),
        ]
        # Si el comptador té data de baixa la lectura no pot ser més gran
        # que la data de baixa
        if comptador.data_baixa:
            search_params += [('name', '<=', comptador.data_baixa)]
        # Especial per les rectificatives, li podem dir fins a quina pot
        # arribar.
        if 'fins_lectura_fact' in context:
            search_params.extend([
                ('name', '<=', context['fins_lectura_fact'])
            ])
        lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                           context={'active_test': False})
        # Si es per facturar cerquem si hi ha darreres lectures
        # per tindre-les en compte encara que hi hagin lectures posteriors
        # dins el mateix periode
        if per_facturar and not context.get('from_perfil', False):
            search_params.append(('origen_id.codi', '=', 'DR'))
            lectures_dl_ids = lectures_obj.search(cursor, uid, search_params,
                                                  context={'active_test': False}
                                                  )
            if lectures_dl_ids:
                lectures_ids = lectures_dl_ids

        # En el cas que no hi hagi lectures ho inicialitzem pel validador
        if not lectures_ids:
            for periode in periodes_obj.browse(cursor, uid, periodes_ids):
                lectures[periode.name] = {}
                lectures[periode.name]['actual'] = {}
                lectures[periode.name]['anterior'] = {}
        for lectura in lectures_obj.browse(cursor, uid, lectures_ids, context):
            periode = lectura.periode.name
            # Si ja hem assignat la lectura per aqueset periode passem
            if periode in lectures:
                continue
            lectures[periode] = {}
            lectures[periode]['actual'] = lectures_obj.read(cursor, uid,
                                                            lectura.id)
            # Té el mateix siginificat que False pq evalua així, però podem
            # fet .get(key, default)
            lectures[periode]['anterior'] = {}
            # posem l'order per defecte (no facturar)
            order = "name desc"
            search_params = [
                ('comptador.id', '=', comptador_id),
                ('periode.id', '=', lectura.periode.id),
                ('name', '<', lectura.name),
            ]
            # Si és per facturar mirem les lectures des d'on volem facturar
            # pel cas que tingui més d'una lectura en el mateix periode.
            # L'order = "name asc" ens assegura que vindran en el bon ordre
            if per_facturar:
                search_params.append(('name', '>=', from_date))
                order = "name asc"
            lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                               order=order,
                                               context={'active_test': False})
            if lectures_ids:
                lectures[periode]['anterior'] = lectures_obj.read(
                    cursor, uid, lectures_ids[0]
                )
                # En el cas que hi hagi més d'una lectura intermitja sumem
                # els consums
                for idx in range(1, len(lectures_ids)):
                    l_tmp = lectures_obj.browse(cursor, uid, lectures_ids[idx])
                    lectures[periode]['actual']['consum'] += l_tmp.consum
        return recalc_consums(lectures, comptador.giro)

    def get_lectures_month_per_facturar(self, cursor, uid, comptador_id,
                                        tarifa_id, periodes=None, context=None):
        """Retorna les lectures pendents de facturar.
        """
        if not context:
            context = {}
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id, context)
        cdata_alta = datetime.strptime(comptador.polissa.data_alta, "%Y-%m-%d")
        cdata_alta -= timedelta(days=1)
        cdata_alta = cdata_alta.strftime("%Y-%m-%d")
        ult_lectura_fact = max(comptador.polissa.data_ultima_lectura,
                               comptador.polissa.data_ultima_lectura_estimada,
                               cdata_alta)
        ult_lectura_fact = context.get('ult_lectura_fact', ult_lectura_fact)
        return comptador.get_lectures_month(tarifa_id, ult_lectura_fact, True,
                                      periodes, context)

    def get_lectures_month(self, cursor, uid, comptador_id, tarifa_id,
                           from_date, per_facturar=False, periodes=None,
                           context=None):
        """Retorna tots els tancaments mensuals entre from_date i to_date
        """
        periodes_obj = self.pool.get('giscegas.polissa.tarifa.periodes')
        lectures_obj = self.pool.get('giscegas.lectures.lectura')

        if not context:
            context = {}
        if isinstance(comptador_id, (list, tuple)):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id)
        lectures = {}
        
        search_params = [
            ('tarifa.id', '=', tarifa_id),
            ('tipus', '=', 'te'),
        ]
        #Search periodes if not passed as a parameter
        if not periodes:
            ctx = context.copy()
            ctx.update({'active_test': False})
            periodes_ids = periodes_obj.search(cursor, uid, search_params,
                                           context=ctx)
        else:
            periodes_ids = periodes[:]
        #Define search params for lectures
        operator = '='
        if per_facturar:
            operator = '>'
        search_params = [
            ('comptador', '=', comptador_id),
            ('periode', 'in', periodes_ids),
            ('name', operator, from_date),
        ]
        # Si el comptador té data de baixa la lectura no pot ser més gran
        # que la data de baixa
        if comptador.data_baixa:
            search_params += [('name', '<=', comptador.data_baixa)]
        # Especial per les rectificatives, li podem dir fins a quina pot
        # arribar.
        if 'fins_lectura_fact' in context:
            search_params.extend([
                ('name', '<=', context['fins_lectura_fact'])
            ])
        lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                           limit=1,
                                           context={'active_test': False})
        # Si es per facturar cerquem si hi ha darreres lectures
        # per tindre-les en compte encara que hi hagin lectures posteriors
        # dins el mateix periode
        if per_facturar:
            search_params.append(('origen_id.codi', '=', 'DR'))
            lectures_dl_ids = lectures_obj.search(cursor, uid, search_params,
                                                  limit=1,
                                           context={'active_test': False})
            if lectures_dl_ids:
                lectures_ids = lectures_dl_ids
        # En el cas que no hi hagi lectures ho inicialitzem pel validador
        if not lectures_ids:
            for periode in periodes_obj.browse(cursor, uid, periodes_ids):
                lectures[periode.name] = {
                    'actual': {},
                    'anterior': {}
                }
            return [recalc_consums(lectures, comptador.giro)]
        
        to_date = lectures_obj.read(
            cursor, uid, lectures_ids[0], ['name']
        )['name']

        # Cerquem totes les dates de lectures entre from_date i to_date
        search_date = max(from_date, comptador.data_alta)
        search_params = [
            ('comptador', '=', comptador_id),
            ('periode', 'in', periodes_ids),
            ('name', '>=', search_date),
            ('name', '<=', to_date),
        ]
        ctx = context.copy()
        ctx.update({'active_test': False})
        all_lectures_ids = lectures_obj.search(
            cursor, uid, search_params, context=ctx
        )
        month_dates = sorted(list(set([
            x['name'] for x in lectures_obj.read(cursor, uid, all_lectures_ids)
        ])), reverse=True)
        totes = []
        # Comencem per la primera data
        date = month_dates[0]
        while date != month_dates[-1]:
            search_params = [
                ('comptador', '=', comptador_id),
                ('periode', 'in', periodes_ids),
                ('name', '=', date),
            ]
            lectures_ids = lectures_obj.search(
                cursor, uid, search_params, context={'active_test': False}
            )
            lectures = {}
            for lectura in lectures_obj.browse(
                    cursor, uid, lectures_ids, context
            ):
                periode = lectura.periode.name
                date_lectura = lectura.name
                # Si ja hem assignat la lectura per aqueset periode passem
                if periode in lectures:
                    continue
                lectures[periode] = {
                    'actual': lectures_obj.read(cursor, uid, lectura.id),
                    # Té el mateix siginificat que False pq evalua així, però podem
                    # fet .get(key, default)
                    'anterior': {}
                }
                # posem l'order per defecte (no facturar)
                order = "name desc"
                found = False
                while not found:
                    search_params = [
                        ('comptador.id', '=', comptador_id),
                        ('periode.id', '=', lectura.periode.id),
                        ('name', '<', lectura.name),
                    ]
                    # Si és per facturar mirem les lectures des d'on volem
                    # facturar pel cas que tingui més d'una lectura en el
                    # mateix periode.
                    # L'order = "name asc" ens assegura que vindran en el
                    # bon ordre.
                    if per_facturar:
                        # Mirem quin es el tancament anterior
                        index = month_dates.index(date_lectura) + 1
                        date_lectura = month_dates[index]
                        search_params.append(('name', '=', date_lectura))
                        order = "name asc"

                    lectures_ant_ids = lectures_obj.search(
                        cursor, uid, search_params, order=order,
                        context={'active_test': False}
                    )

                    if lectures_ant_ids or index == len(month_dates):
                        found = True

                if lectures_ant_ids:
                    lectures[periode]['anterior'] = lectures_obj.read(
                        cursor, uid, lectures_ant_ids[0]
                    )

            totes.append(recalc_consums(lectures, comptador.giro))
        # Check if we are returning something
        if not totes:
            for periode in periodes_obj.browse(cursor, uid, periodes_ids):
                lectures[periode.name] = {
                    'actual': {},
                    'anterior': {}
                }
            return [recalc_consums(lectures, comptador.giro)]
        return totes

    def are_dates_correct(self, cursor, uid, compt_id, start, end,
                          context=None):
        if context is None:
            context = {}

        if start > end:
            # If we start after the end it will be incorrect
            return False

        compt_vals = self.read(
            cursor, uid, compt_id, ['data_alta', 'data_baixa']
        )

        if compt_vals.get('data_alta'):
            alta_compt = datetime.strptime(compt_vals['data_alta'], "%Y-%m-%d")
            alta_compt -= timedelta(days=1)
            alta_compt = alta_compt.strftime("%Y-%m-%d")
            if start < alta_compt:
                return False
        if compt_vals.get('data_baixa') and compt_vals['data_baixa'] < end:
            return False

        return True

    def _default_polissa(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('polissa_id', False)

    def search_by_name(self, cursor, uid, meter_name, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx.update({'active_test': False})
        meter_ids = self.search(
            cursor, uid, [('name', '=', meter_name)], context=ctx
        )
        if meter_ids:
            return meter_ids

        # If we didn't find a counter it may be because one of the names has
        # more or less '0' at the start.
        # For example, meters '0000123' and '123' should be considered the same
        table_name = self._name.replace('.', '_')
        meter_regex = '0*' + meter_name.lstrip('0')
        sql = "SELECT id FROM {0} WHERE name ~ '{1}'".format(
            table_name, meter_regex
        )

        cursor.execute(sql)
        meter_search = cursor.fetchall()
        return [meter[0] for meter in meter_search]

    def search_with_contract(self, cursor, uid, meter_name, polissa,
                             context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx.update({'active_test': False})
        search_params = [('name', '=', meter_name), ('polissa', '=', polissa)]
        meter_ids = self.search(
            cursor, uid, search_params, context=ctx
        )
        if meter_ids:
            return meter_ids

        # If we didn't find a counter it may be because one of the names has
        # more or less '0' at the start.
        # For example, meters '0000123' and '123' should be considered the same
        table_name = self._name.replace('.', '_')
        meter_regex = '0*' + meter_name.lstrip('0')
        sql = "SELECT id FROM {0} WHERE name ~ '{1}' AND polissa = {2}".format(
            table_name, meter_regex, polissa
        )

        cursor.execute(sql)
        meter_search = cursor.fetchall()
        return [meter[0] for meter in meter_search]

    def create(self, cursor, uid, vals, context=None):
        if 'name' in vals:
            vals['name'] = vals['name'].strip()
        res_id = super(
            GiscegasLecturesComptador, self
        ).create(cursor, uid, vals, context)
        return res_id

    def write(self, cursor, uid, ids, vals, context=None):
        if 'name' in vals:
            vals['name'] = vals['name'].strip()
        return super(
            GiscegasLecturesComptador, self
        ).write(cursor, uid, ids, vals, context)

    def unlink(self, cursor, uid, ids, context=None):
        '''if a meter has reads, check if we can unlink it
        It depends on the config variable unlink_meter_with_reads'''

        config = self.pool.get('res.config')
        unlink_meter = int(
            config.get(cursor, uid, 'giscegas_unlink_meter_with_reads', '0')
        )
        for comptador in self.browse(cursor, uid, ids):
            if not unlink_meter and comptador.lectures:
                raise osv.except_osv(
                    'Error', _(u"No se pueden borrar contadores con lecturas")
                )

        return super(GiscegasLecturesComptador,
                     self).unlink(cursor, uid, ids, context=context)

    def renombrar(self, cursor, uid, comptador_id, new_name, context=None):
        if not context:
            context = {}

        if isinstance(comptador_id, (list, tuple)):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id, context=context)
        vals = {'name': new_name}
        if 'serial' in self.fields_get_keys(cursor, uid):
            stock_lot_obj = self.pool.get('stock.production.lot')
            search_params = [('name', '=', new_name)]
            lot_id = stock_lot_obj.search(cursor, uid, search_params)
            if lot_id:
                vals.update({'serial': lot_id[0]})
            else:
                vals.update({'serial': None})

        comptador.write(vals, context=context)
        return True

    def get_meter_on_date(self, cursor, uid, serial, date_read, context=None):
        '''returns id of the meter with serial'''

        if not context:
            context = {}

        # first of all, search only active meters in date_end
        # We can only find as much as one, because there is a constraint
        # that does not allow two meters with same serial being active
        search_params = [
            ('name', '=', serial),
            ('polissa.state', 'not in', CONTRACT_IGNORED_STATES),
            ('data_alta', '<=', date_read),
            # Avoids meters registered prior to policy register date.
            ('polissa.data_alta', '<=', date_read)
        ]
        sql = self.q(cursor, uid).select(['id']).where(search_params)
        cursor.execute(*sql)
        for res in cursor.fetchall():
            return res[0]

        # Search no active meters if no results.
        context.update({'active_test': False})
        # Update search params
        search_params.extend([('data_baixa', '>=', date_read),
                              ('data_alta', '<=', date_read)])
        sql = self.q(cursor, uid).select(
            ['id'], limit=1, only_active=False
        ).where(search_params)
        cursor.execute(*sql)
        for res in cursor.fetchall():
            return res[0]

        return False

    _columns = {
        'name': fields.char('Nº de serie', size=32, required=True),
        'lloguer': fields.boolean('Alquiler'),
        'polissa': fields.many2one(
            'giscegas.polissa', 'Contrato', required=True, ondelete='cascade'
        ),
        'lectures': fields.one2many(
            'giscegas.lectures.lectura', 'comptador', 'Lecturas'
        ),
        'giro': fields.integer('Gir'),
        'active': fields.boolean('Activo'),
        'data_alta': fields.date('Fecha Alta'),
        'data_baixa': fields.date('Fecha Baixa'),
        'propietat': fields.selection(
            TAULA_PROPIEDAD_CONTADOR_CORRECTOR, 'Propiedad de'
        ),
        'meter_type': fields.selection(
            TAULA_TIPO_DE_APARATO, 'Tipo de contador', required=True
        ),
        'digits': fields.integer('Dígitos del contador')
    }

    _defaults = {
        'lloguer': lambda *a: 0,
        'active': lambda *a: 1,
        'giro': lambda *a: 0,
        'data_alta': lambda *a: time.strftime('%Y-%m-%d'),
        'polissa': _default_polissa,
        'meter_type': lambda *a: u'CO',
    }

    _order = 'data_alta desc'

    _sql_constraints = [
        ('data_baixa_alta', 'CHECK (data_baixa >= data_alta)',
         _("La fecha de alta del contador no puede ser anterior a la "
           "fecha de baja.")
         ),
    ]

    def _check_comptador_actiu_unic(self, cursor, uid, ids):
        '''No mes pot haver-hi un comptador actiu
        amb el mateix numero de serie en polisses actives'''
        q = OOQuery(self, cursor, uid)
        sql = q.select(['name', 'polissa.distribuidora']).where(
            [('id', 'in', ids)]
        )
        cursor.execute(*sql)
        for comptador in cursor.dictfetchall():
            search_params = [
                ('name', '=', comptador['name']),
                ('polissa.state', '!=', 'esborrany'),
                ('active', '=', True),
                ('polissa.distribuidora', '=', comptador['polissa.distribuidora'])
            ]
            q = OOQuery(self, cursor, uid)
            sql = q.select(['id']).where(
                search_params
            )
            cursor.execute(*sql)
            if cursor.rowcount > 1:
                return False
        return True

    def _check_lloguer_required(self, cursor, uid, ids):
        '''Si el comptador es d'empresa ha de tindre lloguer'''
        cfg_obj = self.pool.get('res.config')
        if not int(cfg_obj.get(cursor, uid, 'lloguer_required', 0)):
            return True
        for comptador in self.browse(cursor, uid, ids):
            if (comptador.propietat == 'empresa'
                and not comptador.lloguer):
                return False
        return True

    _constraints = [
        (_check_comptador_actiu_unic,
         _(u'Error: Ya existe un contador activo con este número de serie.'),
         ['name']),
        (_check_lloguer_required,
         _(u"Un contador de empresa debe tener un alquiler asociado"),
         ['propietat', 'lloguer'])
                    
    ]


GiscegasLecturesComptador()


class GiscegasLecturesOrigen(osv.osv):
    """Orígens de lectures
    """

    _name = 'giscegas.lectures.origen'

    _columns = {
        'name': fields.char('Nombre', size=64, required=True, translate=True),
        'codi': fields.char('Código', size=2, required=True),
        'subcodi': fields.char('Subcodigo', size=2, required=True),
        'active': fields.boolean('Activo', readonly=True),
    }

    _order = "codi asc, subcodi asc"

    _defaults = {
        'subcodi': lambda *a: '00',
        'active': lambda *a: True,
    }


GiscegasLecturesOrigen()


class GiscegasLecturesLectura(osv.osv):
    """Lectures d'energia
    """

    _name = 'giscegas.lectures.lectura'

    def write(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}
        force_trigger = context.get('force_trigger', False)
        if 'lectura' in vals and not force_trigger:
            trigger = {True: [], False: []}
            for lec in self.read(cursor, uid, ids, ['lectura', 'ajust']):
                if lec['lectura'] != vals['lectura'] \
                        or lec['ajust'] != vals.get('ajust', 0):
                    trigger[True].append(lec['id'])
                else:
                    trigger[False].append(lec['id'])
            for trg, ids in trigger.items():
                if not ids:
                    continue
                context['trigger'] = trg
                res = super(GiscegasLecturesLectura,
                            self).write(cursor, uid, ids, vals, context)
        else:
            res = super(GiscegasLecturesLectura,
                        self).write(cursor, uid, ids, vals, context)
        return res

    def create(self, cursor, uid, vals, context=None):
        """Sobreescrivim el create per cridar el trigger.
        """
        res_id = super(GiscegasLecturesLectura, self).create(cursor, uid,
                                                              vals, context)
        if 'lectura' in vals:
            consum = self._consum(cursor, uid, [res_id], 'consum', None,
                                  context)[res_id]
            self.write(cursor, uid, [res_id], {'consum': consum}, context)
        return res_id

    def unlink(self, cursor, uid, ids, context=None):
        """Sobreescrivim el unlink per cridar el trigger del consum.

        Cal buscar la lectura posterior a la què s'esborra i cridar el trigger
        del consum per a aquesta lectura trobada.
        """
        if context is None:
            context = {}
        lect_recalc = []
        # busquem les lectures a recalcular
        for lectura in self.browse(cursor, uid, ids, context):
            search_params = [
                ('comptador.id', '=', lectura.comptador.id),
                ('name', '>', lectura.name),
                ('periode.id', '=', lectura.periode.id),
            ]
            lectura_ids = self.search(cursor, uid, search_params, limit=1,
                                      order='name asc', context=context)
            if lectura_ids:
                lect_recalc.append(lectura_ids[0])
        # esborrem les lectures
        super(GiscegasLecturesLectura, self).unlink(cursor, uid, ids, context)
        # recalculem
        ctx = context.copy()
        ctx.update({'force_trigger': True})
        if 'trigger' in ctx:
            del ctx['trigger']
        for lectura_post in self.read(cursor, uid, lect_recalc, ['lectura'],
                                      context):
            self.write(cursor, uid, [lectura_post['id']],
                       {'lectura': lectura_post['lectura']},
                       context=ctx)

    def _trg_consum(self, cursor, uid, ids, context=None):
        """Trigger que es cridarà per tal de tornar a calcular els consums.
        """
        if not context:
            context = {}
        if not context.get('trigger', True):
            return []
        context.update({'active_test': False})
        ids_to_call = []
        for lectura in self.browse(cursor, uid, ids, context):
            search_params = [
                ('comptador.id', '=', lectura.comptador.id),
                ('name', '>=', lectura.name),
                ('periode.id', '=', lectura.periode.id),
            ]
            found_ids = self.search(cursor, uid, search_params,
                                    context=context)
            ids_to_call.extend(found_ids)
        # Uniq ids
        res = {}
        for fid in ids_to_call:
            res[fid] = 1
        return res.keys()

    def _consum(self, cursor, uid, ids, field_name, arg, context=None):
        """Calcula el consum i el guardarà a la base de dades.
        """
        res = {}
        for lectura in self.browse(cursor, uid, ids, context):
            from_date = lectura.name
            tarifa_id = lectura.periode.tarifa.id
            periodes = [lectura.periode.id]
            consum_periode = lectura.comptador.get_consum(tarifa_id, from_date,
                                                          periodes=periodes,
                                                          context=context)
            res[lectura.id] = consum_periode[lectura.periode.name]
        return res

    def _consum_m3(self, cursor, uid, ids, field_name, arg, context=None):
        """Calcula el consum i el guardarà a la base de dades.
        """
        res = {}
        for lectura in self.browse(cursor, uid, ids, context):
            from_date = lectura.name
            tarifa_id = lectura.periode.tarifa.id
            periodes = [lectura.periode.id]
            consum_periode = lectura.comptador.get_consum_m3(
                tarifa_id, from_date, periodes=periodes, context=context
            )
            res[lectura.id] = consum_periode[lectura.periode.name]
        return res

    def _auto_init(self, cr, context=None):
        if context is None:
            context = {}
        res = super(GiscegasLecturesLectura, self)._auto_init(cr, context)
        cr.execute(
            "SELECT indexname "
            "FROM pg_indexes "
            "WHERE indexname = 'idx_giscegas_lectures_cnt'")
        if not cr.fetchone():
            cr.execute('CREATE INDEX idx_giscegas_lectures_cnt '
                       'ON giscegas_lectures_lectura (comptador, name)')
        cr.execute(
            "SELECT indexname "
            "FROM pg_indexes "
            "WHERE indexname = 'idx_giscegas_lectures_lectura_ntpc'")
        if not cr.fetchone():
            cr.execute(
                'CREATE INDEX idx_giscegas_lectures_lectura_ntpc '
                'ON giscegas_lectures_lectura (name,periode,comptador)'
            )
        return res

    @staticmethod
    def to_kwh(m3, k, pcs):
        return m3 * k * pcs

    def _store_lectura(self, cursor, uid, ids, context=None):
        return ids

    _columns = {
        'name': fields.date('Fecha', required=True, select=True),
        'periode': fields.many2one(
            'giscegas.polissa.tarifa.periodes', 'Periode', required=True
        ),
        'lectura': fields.integer('Lectura (m³)', select=True),
        'consum': fields.function(
            _consum, method=True, string="Consum (kWh)", type="float",
            store={
                'giscegas.lectures.lectura': (
                    _trg_consum, ['lectura', 'ajust', 'factor_k', 'pcs'], 10
                )
            }
        ),
        'consum_m3': fields.function(
            _consum_m3, method=True, string="Consum (m³)", type="float",
            store={
                'giscegas.lectures.lectura': (
                    _trg_consum, ['lectura', 'ajust', 'factor_k', 'pcs'], 10
                )
            }
        ),
        'comptador': fields.many2one(
            'giscegas.lectures.comptador', 'Contador',
            required=True, ondelete='cascade'
        ),
        'observacions': fields.text('Observaciones'),
        'origen_id': fields.many2one(
            'giscegas.lectures.origen', 'Origen', required=True
        ),
        'ajust': fields.integer('Regularización'),
        'factor_k': fields.float('Factor K', digits=(16, 6)),
        'pcs': fields.float('Poder Calorífico Superior (PCS)', digits=(16, 6)),
        'tipo_lect_num': fields.selection(
            TAULA_TIPO_DE_LECTURA_DE_NUMERADOR, 'Tipo de Lectura de Numerador'
        ),
        'motiu_ajust': fields.selection(
            TAULA_MOTIVO_REGULARIZACION_CONSUMO, 'Motivo Regularización'
        ),
        'pressio_subministrament': fields.float(
            'Presión del suministro', digits=(7, 4)
        )
    }

    def _default_comptador(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('comptador_id', False)

    _defaults = {
      'name': lambda *a: time.strftime('%Y-%m-%d'),
      'comptador': _default_comptador,
      'ajust': lambda *a: 0,
      'factor_k': lambda *a: 0,
      'pcs': lambda *a: 0,
    }

    _order = "name desc, periode asc"

    _sql_constraints = [('consum_positiu', 'CHECK (consum >= 0)',
                         _(u'El consumo debe ser positivo.')),
                        ('lectures_duplicades',
                         'UNIQUE(name, comptador, periode)',
                         _(u"No se puede entrar más de una lectura por fecha, "
                           u"contador y período."))]

    def _check_datalectura(self, cursor, uid, ids):
        '''No es poden introduir lectures posteriors a la data
        de baixa de un comptador ni anteriors a la data d'alta-1'''
        cfg_obj = self.pool.get('res.config')
        for lectura in self.browse(cursor, uid, ids):
            if not lectura.comptador.active and\
               lectura.name > lectura.comptador.data_baixa:
                return False
            data_tall = cfg_obj.get(cursor, uid, 'data_lectura_constraint',
                                    '2013-01-01')
            if lectura.comptador.data_alta:
                alta_compt = datetime.strptime(lectura.comptador.data_alta, "%Y-%m-%d")
                alta_compt -= timedelta(days=1)
                alta_compt = alta_compt.strftime("%Y-%m-%d")
                if lectura.name >= data_tall and lectura.name < alta_compt:
                    return False
        return True

    _constraints = [
        (_check_datalectura,
         _(u'Error: No se puede introducir lecturas posteriores a la fecha de '
           u'baja del contador o fechas anteriores al dia anterior a la '
           u'fecha de alta del contador.'),
         ['name']),
    ]


GiscegasLecturesLectura()
