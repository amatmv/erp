# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields


class GiscegasWizardRenombrarComptador(osv.osv_memory):

    _name = 'giscegas.wizard.renombrar.comptador'

    def action_renombrar(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])
        comptador_obj = self.pool.get('giscegas.lectures.comptador')
        comptador_obj.renombrar(cursor, uid, wizard.comptador_id.id,
                                wizard.new_name, context=context)

        wizard.write({'state': 'end'})

    _columns = {
        'comptador_id': fields.many2one('giscegas.lectures.comptador',
                                       'Contador', required=True),
        'new_name': fields.char(
            'Nº de serie nuevo', size=20, required=True, readonly=False
        ),
        'state': fields.selection(
            [('init', 'Init'), ('end', 'End')], 'Estado', select=True,
            readonly=True
        ),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

GiscegasWizardRenombrarComptador()
