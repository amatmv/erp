# # -*- coding: utf-8 -*-

from osv import fields, osv
from datetime import date


class GiscegasWizardIntroduirLecturesComptador(osv.osv):

    _name = 'giscegas.wizard.introduir.lectures.comptador'

    def _go_init_form(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {
            'estat': 'init_form',
            'comptador': context.get('active_id', False)
        })

    def _go_lectures_form(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'estat': 'lectures_form'})

    def _default_comptador(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('active_id', False)

    def _create_lectura(self, cursor, uid, ids, context=None):
        data = self.browse(cursor, uid, ids)[0]
        periodes = [periode.id for periode in data.polissa.tarifa.periodes]
        periode_id = self.pool.get('giscegas.polissa.tarifa.periodes').search(
            cursor, uid, [('id', 'in', periodes), ('tipus', '=', 'te')]
        )[0]
        vals = {
            'name': data.data,
            'periode': periode_id,
            'factor_k': data.factor_k,
            'pcs': data.pcs,
            'lectura': data.lectura_actual,
            'comptador': data.comptador.id,
            'origen_id': data.origen_id.id,
        }
        self.pool.get('giscegas.lectures.lectura').create(cursor, uid, vals)
        self.write(cursor, uid, ids, {'estat': 'end'})

    def _go_contract_form(self, cursor, uid, ids, context=None):
        compt_id = self.read(
            cursor, uid, ids, ['comptador']
        )[0]['comptador'][0]

        compt = self.pool.get('giscegas.lectures.comptador').browse(
            cursor, uid, compt_id
        )
        # We get the most recent reading
        if compt.lectures:
            lects = [(lect.name, lect) for lect in compt.lectures]
            lect_anterior = lects[0]
            for lect in lects:
                if lect[0] > lect_anterior[0]:
                    lect_anterior = lect
            lect_anterior = lect_anterior[1]
        else:
            lect_anterior = False

        self.write(cursor, uid, ids, {
            'polissa': compt.polissa.id,
            'client': compt.polissa.titular.id,
            'data': date.today(),
            'tarifa': compt.polissa.tarifa.id,
            'estat': 'contract_form',
            'lectura_anterior': lect_anterior.lectura if lect_anterior else False,
            'origen_id': lect_anterior.origen_id.id if lect_anterior else False
        })

    _states = [
        ('init_form', 'Initial Form'),
        ('contract_form', 'Contract Form'),
        ('lectures_form', 'Lectures Form'),
        ('end', 'End')
    ]

    _columns = {
        'client': fields.many2one(
            'res.partner', 'Cliente'
        ),
        'polissa': fields.many2one(
            'giscegas.polissa', 'Contrato'
        ),
        'tarifa': fields.many2one(
            'giscegas.polissa.tarifa', 'Tarifa'
        ),
        'comptador': fields.many2one(
            'giscegas.lectures.comptador', 'Contador', required=True
        ),
        'data': fields.date('Fecha'),
        'origen_id': fields.many2one(
            'giscegas.lectures.origen', 'Origen'
        ),
        'estat': fields.selection(
            _states, 'Estado', readonly=True
        ),
        'lectura_anterior': fields.float('Anterior'),
        'lectura_actual': fields.float('Actual'),
        'factor_k': fields.float('Factor K'),
        'pcs': fields.float('Poder Calorífico Superior (PCS)'),
    }

    _defaults = {
        'comptador': _default_comptador,
        'estat': lambda *a: 'init_form',
        'factor_k': lambda *a: 1,
        'pcs': lambda *a: 1,
    }


GiscegasWizardIntroduirLecturesComptador()
