# -*- coding: utf-8 -*-
{
    "name": "Lectura de comptadors de gas (Base)",
    "description": """Lectures dels comptadors de gas""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Gas",
    "depends": [
        "giscegas_polissa",
        "product",
        "stock",
        "infraestructura"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegas_lectures_demo.xml"
    ],
    "update_xml": [
        "wizard/giscegas_wizard_introduir_lectures_comptador.xml",
        "giscegas_lectures_data.xml",
        "giscegas_lectures_view.xml",
        "giscegas_polissa_view.xml",
        "giscegas_cups_view.xml",
        "wizard/giscegas_wizard_renombrar_comptador_view.xml",
        "giscegas_lectures_report.xml",
        "security/giscegas_lectures_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
