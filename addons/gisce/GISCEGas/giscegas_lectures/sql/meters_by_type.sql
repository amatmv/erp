SELECT comptador_id FROM (
  SELECT
    cu.name as cups,
    c.id as comptador_id,
    rank() over (partition by cu.name order by c.data_alta desc) as rank
  FROM giscegas_lectures_comptador c
  INNER JOIN giscegas_polissa p ON (c.polissa = p.id)
  INNER JOIN giscegas_cups_ps cu ON (p.cups = cu.id)
  INNER JOIN giscegas_polissa_modcontractual m ON (
      m.polissa_id = p.id
      AND m.data_inici <= %(end)s
      AND m.data_final >= %(start)s
      )
  WHERE
    c.data_alta <= %(end)s
    AND (c.data_baixa is null or c.data_baixa >= %(start)s)
) as sq WHERE rank = 1