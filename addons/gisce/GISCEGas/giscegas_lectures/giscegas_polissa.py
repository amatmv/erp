# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime, timedelta


class GiscegasPolissa(osv.osv):
    """Extensió per la pòlissa pel mòdul de lectures.
    """

    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def meters_not_invoiced(self, cursor, uid, polissa_id,
                            data_inici, context=None):
        '''Return ids of no active meters with not invoiced reads'''
        if not context:
            context = {}
        meter_obj = self.pool.get('giscegas.lectures.comptador')
        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]
        polissa = self.browse(cursor, uid, polissa_id)
        data_ultima_lectura = context.get('ult_lectura_fact', False)
        if not data_ultima_lectura:
            data_ultima_lectura = (polissa.data_ultima_lectura or
                                   polissa.data_alta)
        search_params = [('active', '=', False),
                         ('polissa', '=', polissa_id),
                         ('data_baixa', '<', data_inici),
                         ('data_baixa', '>=', data_ultima_lectura)]
        meter_ids = meter_obj.search(cursor, uid, search_params,
                                     context={'active_test': False})
        ctx = context.copy()
        ctx.update({'active_test': False})
        res = []
        for meter in meter_obj.browse(cursor, uid, meter_ids):
            ctx.update({'date': meter.data_baixa})
            polissa = self.browse(cursor, uid, meter.polissa.id, context=ctx)
            consums = meter.get_consum_per_facturar(polissa.tarifa.id)
            for consum in consums.values():
                if consum > 0:
                    res.append(meter.id)
                    break
        return res

    def comptadors_actius(self, cursor, uid, polissa_id, data_inici,
                          data_final='3000-01-01', order='data_alta desc'):
        """Retorna un array amb els IDs de comtpadors actius.
        """
        if isinstance(polissa_id, list) or isinstance(polissa_id, tuple):
            polissa_id = polissa_id[0]
        comptador_obj = self.pool.get('giscegas.lectures.comptador')
        search_params = [('polissa', '=', polissa_id),
                         ('data_alta', '<=', data_final),
                         '|', ('data_baixa', '>=', data_inici),
                         ('data_baixa', '=', False)]
        return comptador_obj.search(cursor, uid, search_params, order=order,
                                    context={'active_test': False})

    def data_utlima_lectura_entrada(self, cursor, uid, polissa_id,
                                    context=None):
        """Retorna la data d'última lectura per una tarifa segons tots els
        comptadors de la pòlissa.
        """
        if context is None:
            context = {}
        if isinstance(polissa_id, list) or isinstance(polissa_id, tuple):
            polissa_id = polissa_id[0]
        polissa = self.browse(cursor, uid, polissa_id, context)
        data_ult_lectura = False
        tarifa_id = polissa.tarifa.id
        comptador_obj = self.pool.get('giscegas.lectures.comptador')
        comptadors_ids = polissa.comptadors_actius(polissa.data_alta)
        for comptador in comptador_obj.browse(cursor, uid, comptadors_ids,
                                              context):
            conta_ultima = comptador.data_ultima_lectura(tarifa_id,
                                                         context=context)
            # Si es la lectura inicial la ignorem. Serà la lectura inicial si
            # la data és un dia abans de la data d'alta (criteri nou) o si la
            # data es el dia de la data d'alta i no tenim cap més lectura
            # (criteri antic)
            dalta_ant = datetime.strptime(comptador.data_alta, "%Y-%m-%d") - timedelta(days=1)
            dalta_ant = dalta_ant.strftime("%Y-%m-%d")
            if conta_ultima == dalta_ant or (conta_ultima == comptador.data_alta and len(comptador.lectures) == 1):
                continue
            data_ult_lectura = max(data_ult_lectura, conta_ultima)
        # Quan hi ha un canvi de tarifa, pot ser que encara no tinguem lectures
        # i no trobem la última lectura de la tarifa actual
        # busquem lectures agafant les tarifes de les modificacions
        # contractuals de tots els comptadors
        # Si la lectura que trobem tambe es anterior a la data inici de facturacio, tambe intentem buscar lectures de
        # una altre tarifa ja que no ens serveix de res una lectura anterior al inici del priode facturat
        data_inici = context.get("data_inici_facturacio", data_ult_lectura)
        if not data_ult_lectura or data_ult_lectura < data_inici:
            tarifes = []
            for modcons in polissa.modcontractuals_ids:
                tarifa_id = modcons.tarifa.id
                if not tarifa_id in tarifes:
                    tarifes.append(modcons.tarifa.id)
            for tarifa_id in tarifes:
                for comptador in polissa.comptadors:
                    conta_ultima = comptador.data_ultima_lectura(tarifa_id,
                                                                context=context)
                    # Si es la lectura inicial la ignorem. Serà la lectura
                    # inicial si la data és un dia abans de la data d'alta
                    # (criteri nou) o si la data es el dia de la data d'alta i
                    # no tenim cap més lectura (criteri antic)
                    dalta_ant = datetime.strptime(comptador.data_alta, "%Y-%m-%d") - timedelta(days=1)
                    dalta_ant = dalta_ant.strftime("%Y-%m-%d")
                    if conta_ultima == dalta_ant or (conta_ultima == comptador.data_alta and len(comptador.lectures) == 1):
                        continue
                    data_ult_lectura = max(data_ult_lectura, conta_ultima)
        return data_ult_lectura

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}

        cfg_obj = self.pool.get('res.config')
        dupl_meters = int(
            cfg_obj.get(cursor, uid, 'giscegas_duplicate_meters_when_contract', '0'))
        if not dupl_meters:
            default_values = {
                'comptador': False,
                'comptadors': []
            }

            default.update(default_values)
        res_id = super(
            GiscegasPolissa, self).copy_data(cursor, uid, id, default, context)
        return res_id

    def get_comptador_data(self, cursor, uid, ids, data, context=None):
        """ Retorna el comptador d'una pólissa per una data en concret
            Com que el canvi de comptador no generar una modificació
            contractual, en una modcon hi pot haver més d'un comptador i el
            camp comptador que es retorna és el més recent.
            Amb aquesta funció es retorna el comptador d'un dia exactament
            CONTEXT
            Si es passa 'multi'=True per contexte, retorna una llista amb tots
            els comptadors en comptes d'escollir el primer
            Si es passa 'active_only'=True, només selecciona els actius. Si no,
            els busca tots.
            """

        res = {}
        if context is None:
            context = {}

        multi = context.get('multi', False)
        comptador_obj = self.pool.get('giscegas.lectures.comptador')
        for polissa_id in ids:
            search_vals = [('polissa.id', '=', polissa_id),
                           ('data_alta', '<=', data)]
            if multi:
                search_vals.extend(
                    ['|', ('data_baixa', '>=', data),
                     ('data_baixa', '=', False)]
                )
            ctx = {'active_test': context.get('active_only', False)}
            limit = 0 if multi else 1
            c_id = comptador_obj.search(cursor, uid, search_vals, limit=limit,
                                        order='data_alta desc', context=ctx)
            if c_id:
                if multi:
                    res[polissa_id] = c_id
                else:
                    res[polissa_id] = c_id[0]
            else:
                res[polissa_id] = False

        if len(res) == 1:
            res = res.values()[0]

        return res

    def _ff_comptador(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna el número de comptador.
        """
        res = {}
        comptador_obj = self.pool.get('giscegas.lectures.comptador')
        for polissa_id in ids:
            c_id = comptador_obj.search(cursor, uid,
                                        [('polissa.id', '=', polissa_id)])
            if c_id:
                comp = comptador_obj.read(cursor, uid, c_id[0], ['name'])
                res[polissa_id] = comp['name']
            else:
                res[polissa_id] = False
        return res

    def _ff_comptador_search(self, cursor, uid, obj, name, args, context):
        """Busquem el comptador d'una pòlissa.
        """
        compt_obj = self.pool.get('giscegas.lectures.comptador')
        if not len(args):
            return []
        for argument in args:
            value = argument[2]
        cids = compt_obj.search(cursor, uid, [('name', '=', value)])
        polisses = []
        for comptador in compt_obj.read(cursor, uid, cids, ['polissa']):
            polisses.append(comptador['polissa'][0])
        return [('id', 'in', polisses)]

    def cnd_baixa(self, cursor, uid, ids):

        res = super(GiscegasPolissa, self).cnd_baixa(cursor, uid, ids)

        #Si altres comprovacions ja han anat malament
        if not res:
            return res
        else:
            #no podem donar de baixa polisses amb comptadors actius
            for polissa in self.browse(cursor, uid, ids):
                for comptador in polissa.comptadors:
                    if comptador.active:
                        raise osv.except_osv(
                            _(u"Error"),
                            _(u"No es poden donar de baixa"
                              u" pòlisses amb comptadors actius")
                        )
                        return False
        return True

    _columns = {
        'comptadors': fields.one2many('giscegas.lectures.comptador',
                                      'polissa', 'Contadores',
                                      context={'active_test': False},
                                      states={
                                        'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False)],
                                        'modcontractual': [('readonly', False),
                                                           ('required', True)]
                                        }),
        'comptador': fields.function(_ff_comptador, type='char', size=64,
                                     fnct_search=_ff_comptador_search,
                                     method=True, select=True,
                                     string='Contador')
    }

GiscegasPolissa()


class GiscegasPolissaModcontractual(osv.osv):
    """Afegim el comptador segons les dates de la modificació contractual.
    """
    _name = 'giscegas.polissa.modcontractual'
    _inherit = 'giscegas.polissa.modcontractual'

    def _ff_comptador(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna el número de comptador.
        """
        res = {}
        comptador_obj = self.pool.get('giscegas.lectures.comptador')
        for mod in self.browse(cursor, uid, ids, context):
            res[mod.id] = False
            c_ids = mod.polissa_id.comptadors_actius(mod.data_inici,
                                                     mod.data_final,
                                                     order='data_alta asc')
            if c_ids:
                if field_name == 'comptador':
                    c_ids = [c_ids[0]]
                coms = comptador_obj.browse(cursor, uid, c_ids)
                res[mod.id] = ', '.join(set([com.name for com in coms]))
        return res

    _columns = {
        'comptador': fields.function(_ff_comptador, type='char', size=64,
                                     method=True, string='Contador'),
        'comptadors_periode': fields.function(_ff_comptador, type='text',
                                              method=True, string='Contadores')
    }

GiscegasPolissaModcontractual()
