# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_a1_03 as a1_03
from gestionatr.defs_gas import *
from gestionatr.defs import SINO
from tools.translate import _
from datetime import datetime
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException
from giscegas_atr.utils import get_address_dict, calc_annulmentreason_from_case


class GiscegasAtrProces03(osv.osv):

    _name = 'giscegas.atr.proces'
    _inherit = 'giscegas.atr.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 initial steps depending on where we are '''

        if proces == '03':
            return ['a1', 'a2s']  # In both comer and distri

        return super(GiscegasAtrProces03,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 emisor steps depending on where we are '''
        if proces == '03':
            if where == 'distri':
                return ['a2', 'a2s']
            elif where == 'comer':
                return ['a1']

        return super(GiscegasAtrProces03,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == '03':
            return ['a2']

        return super(GiscegasAtrProces03,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        if proces == '03':
            return ['a2s']

        return super(GiscegasAtrProces03, self).get_old_company_steps(
            cursor, uid, proces, context
        )


GiscegasAtrProces03()


class GiscegasAtr03(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == '03':
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name in ('a2', 'a2s'):
                    return True
        return super(GiscegasAtr03,
                     self).get_final(cursor, uid, sw, context=context)

GiscegasAtr03()


class GiscegasAtr03A1(osv.osv):
    """ Classe pel pas a1
    """

    _name = "giscegas.atr.03.a1"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a1'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '03', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr03A1,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        heading = pas.header_id.generar_xml(pas)
        a103 = a1_03.A103()
        vals_a103 = {
            'comreferencenum': sw.codi_sollicitud,
            'reqdate': pas.header_id.date_created.split(" ")[0],
            'reqhour': pas.header_id.date_created.split(" ")[1],
            'cups': sw.cups_id.name[:20],
            'nationality': pas.nationality,
            'documenttype': pas.documenttype,
            'documentnum': pas.documentnum,
            'annulmentreason': pas.annulmentreason,
            'extrainfo': pas.extrainfo,
        }
        if pas.documenttype in ['07', '08']:
            vals_a103['titulartype'] = pas.titulartype

        a103.feed(vals_a103)
        msg = a1_03.MensajeA103()
        msg.feed({
            'heading': heading,
            'a103': a103,
        })
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        raise Exception("Importación de 03 paso a1 no implementada")

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        mandatory_fields = ['annulmentreason']
        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)
        non_completed = []
        for field in mandatory_fields:
            if not vals2.get(field, False):
                non_completed.append(field)

        if len(non_completed):
            raise GiscegasAtrException(_("Faltan datos. Campos no completados."), non_completed)

        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """
        """
        if not context:
            context = {}

        if not vals:
            return True

        self.config_step_validation(cursor, uid, ids, vals, context=context)
        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)
        self.write(cursor, uid, ids, vals2, context=context)
        return True

    def prepare_for_validation(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}

        if not vals:
            return vals
        return vals

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id

        titular_vat = polissa.titular.vat
        participant_obj = self.pool.get("giscemisc.participant")
        participant = participant_obj.get_from_partner(cursor, uid, polissa.distribuidora.id)
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, participant.codi_solicitud
        )
        casos_oberts = sw_obj.search(cursor, uid, [('cups_polissa_id', '=', polissa.id), ('state', '!=', 'done')])
        annulmentreason = '002'
        anulat_id = False
        if len(casos_oberts):
            anulat_id = casos_oberts[0]
            annulmentreason = calc_annulmentreason_from_case(self.pool, cursor, uid, anulat_id, context=context)

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'titulartype': 'J' if vat_info['is_enterprise'] else 'F',
            'nationality':
                polissa.titular.country and polissa.titular.country.code,
            'documenttype': vat_info['document_type'],
            'documentnum': vat_info['vat'],
            'annulmentreason': annulmentreason,
            'atr_anulat_id': anulat_id
        })
        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_03_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        codi = step.atr_anulat_id.codi_sollicitud if step.atr_anulat_id else "[No encontrado]"
        base_msg = _(u"Anulación para el caso {0}").format(codi)
        return base_msg

    def onchange_atr_anulat_id(self, cr, uid, ids, cas_id, context=None):
        return {'value': {'annulmentreason': calc_annulmentreason_from_case(self.pool, cr, uid, cas_id, context=context)}}

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        'titulartype': fields.selection(TAULA_TIPO_PERSONA, 'Tipo de Titular'),
        'nationality': fields.selection(TAULA_NACIONALIDAD, 'Nacionalidad'),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, 'Tipo Documento de Identificación'),
        'documentnum': fields.char('Nº Documento', size=20),
        'annulmentreason': fields.selection(TAULA_MOTIVO_ANULACION, 'Motivo de Anulación'),
        'extrainfo': fields.text('Observaciones', size=400),
        'atr_anulat_id': fields.many2one("giscegas.atr", "Caso ATR anulado")
        }

    _defaults = {
    }


GiscegasAtr03A1()


class GiscegasAtr03A2(osv.osv):
    """ Classe pel pas a2
    """

    _name = "giscegas.atr.03.a2"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a2'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '03', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr03A2,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2s']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 03 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        denegat = xml.result not in ['01', '09']

        rebuig_ids = []
        if denegat:
            rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                    context=context)

        vals.update({
            'reqcode': xml.reqcode,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'annulmentreason': xml.annulmentreason,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': denegat,
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_03_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.03.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0])
        if step['rebuig']:
            return _(u"{0}. Rechazo: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def onchange_result(self, cursor, uid, ids, result_id, context=None):
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}
        vals = {}

        if result_id in ['01', '09']:
            vals['rebuig'] = False
        else:
            vals['rebuig'] = True

        res['value'] = vals
        return res

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        'reqcode': fields.char('Código Solicitud', size=10),
        'result': fields.char('Código de Resultado', size=2),
        'resultdesc': fields.text('Descripción de Resultado', size=100),
        'nationality': fields.selection(TAULA_NACIONALIDAD, 'Nacionalidad'),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, 'Tipo Documento de Identificación'),
        'documentnum': fields.char('Nº Documento', size=20),
        'annulmentreason': fields.selection(TAULA_MOTIVO_ANULACION, 'Motivo de Anulación'),
        'extrainfo': fields.text('Observaciones de la Validación', size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motivo Rechazo", type='char',
                                        size=250, readonly=True, ),
    }

    _defaults = {
        'rebuig': lambda *a: False,
    }


GiscegasAtr03A2()


class GiscegasAtr03A2s(osv.osv):
    """ Classe pel pas a3
    """

    _name = "giscegas.atr.03.a2s"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a2s'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '03', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr03A2s,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Generacion de 03 paso a2s no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        vals.update({
            'reqcode': xml.reqcode,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'annulmentreason': xml.annulmentreason,
            'extrainfo': xml.extrainfo
        })
        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_03_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['sw_id', 'annulmentreason'])
        return _(u"{0}").format(step['annulmentreason'])

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header', required=True),
        'reqcode': fields.char('Código Solicitud', size=10),
        'result': fields.selection(TAULA_RESULTADO, 'Código de Resultado'),
        'resultdesc': fields.text('Descripción de Resultado', size=100),
        'nationality': fields.selection(TAULA_NACIONALIDAD, 'Nacionalidad'),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, 'Tipo Documento de Identificación'),
        'documentnum': fields.char('Nº Documento', size=20),
        'annulmentreason': fields.selection(TAULA_MOTIVO_ANULACION, 'Motivo de Anulación'),
        'extrainfo': fields.text('Observaciones de la Validación', size=400),
    }

    _defaults = {

    }


GiscegasAtr03A2s()
