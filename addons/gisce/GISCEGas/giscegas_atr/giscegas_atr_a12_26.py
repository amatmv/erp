# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_a1_02 as a1_02
from gestionatr.defs_gas import *
from gestionatr.defs import SINO
from tools.translate import _
from datetime import datetime
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException
from giscegas_atr.utils import get_address_dict


class GiscegasAtrProces26(osv.osv):

    _name = 'giscegas.atr.proces'
    _inherit = 'giscegas.atr.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 initial steps depending on where we are '''

        if proces == '26':
            return ['a12']  # In both comer and distri

        return super(GiscegasAtrProces26,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 emisor steps depending on where we are '''
        if proces == '26':
            if where == 'distri':
                return ['a12']
            elif where == 'comer':
                return []

        return super(GiscegasAtrProces26,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == '26':
            return []

        return super(GiscegasAtrProces26,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        if proces == '26':
            return []

        return super(GiscegasAtrProces26, self).get_old_company_steps(
            cursor, uid, proces, context
        )


GiscegasAtrProces26()


class GiscegasAtr26(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == '26':
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name in ('a12'):
                    return True

        return super(GiscegasAtr26,
                     self).get_final(cursor, uid, sw, context=context)

GiscegasAtr26()


class GiscegasAtr26A1(osv.osv):
    """ Classe pel pas a1
    """

    _name = "giscegas.atr.26.a12"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a12'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '26', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr26A1,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2']

    def generar_xml(self, cr, uid, pas_id, context=None):
        raise NotImplementedError("Exportación de 26 paso a12 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        municipi_obj = self.pool.get('res.municipi')
        municipi_id = municipi_obj.search(cursor, uid, [('ine', '=', xml.city)])
        provincia_obj = self.pool.get('res.country.state')
        provincia_id = provincia_obj.search(cursor, uid, [('code', '=', xml.province)])
        vals.update({
            # Dades Activacio
            'transfereffectivedate': xml.transfereffectivedate,
            'updatereason': xml.updatereason,
            'communicationreason': xml.communicationreason,
            # Dades Administratives
            'atrcode': xml.atrcode,
            'atrversion': xml.atrversion,
            # Dades Client
            'titulartype': xml.titulartype,
            'newcustomer': xml.newcustomer,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'firstname': xml.firstname,
            'familyname1': xml.familyname1,
            'familyname2': xml.familyname2,
            # Dades Contacte
            'telephone': xml.telephone,
            'fax': xml.fax,
            'email': xml.email,
            # Adreça CUPS
            'regularaddress': xml.regularaddress,
            'streettype': xml.streettype and xml.streettype.upper(),
            'street': xml.street,
            'streetnumber': xml.streetnumber,
            'portal': xml.portal,
            'staircase': xml.staircase,
            'floor': xml.floor,
            'door': xml.door,
            'province': len(provincia_id) and provincia_id[0] or False,
            'city': len(municipi_id) and municipi_id[0] or False,
            'zipcode': xml.zipcode,
            # Dades Tecniques
            'tolltype': xml.tolltype,
            'qdgranted': xml.qdgranted,
            'qhgranted': xml.qhgranted,
            'singlenomination': xml.singlenomination,
            'finalclientyearlyconsumption': xml.finalclientyearlyconsumption,
            'netsituation': xml.netsituation,
            'outgoingpressuregranted': xml.outgoingpressuregranted,
            # 'caecode': xml.caecode,
            # Dades Inspeccio
            'lastinspectionsdate': xml.lastinspectionsdate,
            'lastinspectionsresult': xml.lastinspectionsresult,
            # Altres Dades
            'readingtype': xml.readingtype,
            'rentingamount': xml.rentingamount,
            'rentingperiodicity': xml.rentingperiodicity,
            'canonircamount': xml.canonircamount,
            'canonircperiodicity': xml.canonircperiodicity,
            'canonircforlife': xml.canonircforlife,
            'canonircdate': xml.canonircdate,
            'canonircmonth': xml.canonircmonth,
            'othersamount': xml.othersamount,
            'othersperiodicity': xml.othersperiodicity,
            'readingperiodicitycode': xml.readingperiodicitycode,
            'transporter': xml.transporter,
            'transnet': xml.transnet,
            'gasusetype': xml.gasusetype,
        })
        # CNAE
        if xml.caecode:
            cnae_id = sw_obj.search_cnae(cursor, uid, xml.caecode, context=context)
            vals.update({'caecode': cnae_id})
        partner_ids = self.pool.get("res.partner").search(cursor, uid, [('vat', 'like', xml.documentnum)])
        if partner_ids:
            vals.update({
                'titular_id': partner_ids[0]
            })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        return True

    def dummy_create(self, cursor, uid, sw_id, context=None):
        raise NotImplementedError("Generación de 26 paso a12 no implementada")

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_26_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        # We get the contract in the state it was when solicitud was generated
        polissa = step.sw_id.cups_polissa_id
        if not polissa:
            return " "
        max_data_fi_modcons = None
        if polissa.modcontractual_activa:
            max_data_fi_modcons = polissa.modcontractual_activa.data_final
        if (max_data_fi_modcons and
                polissa.data_alta <= step.date_created <= max_data_fi_modcons):
            polissa = polissa.browse(context={'date': step.date_created})[0]

        # Info comuna per tots els updatereason
        update_reason = step.updatereason
        base_msg = u"({0}) ".format(update_reason)

        # Segons updatereason posem una informacio o una altre
        if update_reason in ['11', '12', '15', '16', '18', '19', '22', '23']:
            oldt = polissa.tarifa.name
            tarif_obj = self.pool.get("giscegas.polissa.tarifa")
            tarif_id = tarif_obj.get_tarifa_from_ocsum(
                cursor, uid, step.tolltype, context
            )
            if not tarif_id:
                return " "
            newt = tarif_obj.read(cursor, uid, tarif_id, ['name'])['name']
            base_msg += _(u"Tarifa: {0} -> {1}. ").format(oldt, newt)
        else:
            base_msg += _(u"Tarifa:{0}. ").format(polissa.tarifa.name)

        if update_reason in ['13', '14', '15', '16', '20', '21', '22', '23']:
            oldq = polissa.caudal_diario
            newq = step.qdgranted
            base_msg += _(u"Caudal Diario: {0} -> {1}. ").format(oldq, newq)
        else:
            base_msg += _(u"Caudal Diario: {0}. ").format(polissa.caudal_diario)

        if update_reason in ['09']:
            base_msg += u"CNAE: {0}. ".format(step.caecode.name)

        if update_reason in ['01', '04', '12', '14', '16', '17', '19', '21', '23'] or step.newcustomer == 'S':
            oldp = polissa.titular_nif
            newp = step.documentnum
            base_msg += u"Titular: {0} -> {1}. ".format(oldp, newp)

        return base_msg

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        # Dades Activacio
        'transfereffectivedate': fields.date('Fecha  Efecto  (inclusive)'),
        'updatereason': fields.selection(TAULA_MOTIVO_MODIFICACION, 'Motivo de Modificación'),
        'communicationreason': fields.selection(TAULA_TIPO_COMUNICACION_A12_26, 'Motivo de la comunicación'),
        # Dades administratives
        'atrcode': fields.char('Código Contrato ATR', size=24),
        'atrversion': fields.integer('Versión del Contrato ATR', size=3),
        # Dades Client
        'titular_id': fields.many2one("res.partner", "Cliente relacionado"),
        'titulartype': fields.selection(TAULA_TIPO_PERSONA, 'Tipo de Persona'),
        'newcustomer': fields.selection(TAULA_INDICATIVO_SI, 'Cliente Nuevo?'),
        'nationality': fields.selection(TAULA_NACIONALIDAD, 'Nacionalidad'),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, 'Tipo Documento de Identificación'),
        'documentnum': fields.char('Nº Documento', size=20),
        'firstname': fields.char('Nombre Cliente', size=40),
        'familyname1': fields.char('Apellido 1 / Razón Social', size=30),
        'familyname2': fields.char('Apellido 2', size=30),
        # Dades Contacte
        'telephone': fields.char('Teléfono', size=20),
        'fax': fields.char('Fax', size=20),
        'email': fields.char('Correo Electrónico', size=60),
        # Adreça CUPS
        'regularaddress': fields.selection(TAULA_INDICATIVO_SI, 'Uso de la Vivienda del PS'),
        'streettype': fields.selection(TAULA_TIPO_VIA, 'Tipo de vía del suministro'),
        'street': fields.char('Nombre Calle del Suministro', size=60),
        'streetnumber': fields.char('Nº Finca del Suministro', size=4),
        'portal': fields.char('Portal del suministro', size=5),
        'staircase': fields.char('Escalera  del suministro', size=5),
        'floor': fields.char('Piso  del suministro', size=5),
        'door': fields.char('Puerta  del suministro', size=5),
        'province': fields.many2one('res.country.state', 'Provincia del suministro'),
        'city': fields.many2one('res.municipi', 'Municipio INE del Suministro'),
        'zipcode': fields.char('Código Postal del Suministro', size=5),
        # Dades Tecniques
        'tolltype': fields.selection(TAULA_TIPO_PEAJE, 'Tipo de Peaje'),
        'qdgranted': fields.float('Qd Concedida', digits=(16, 7)),
        'qhgranted': fields.integer('Qh Concedida', size=9),
        'singlenomination': fields.selection(TAULA_INDICATIVO_SI, 'Nominación Individual'),
        'finalclientyearlyconsumption': fields.integer('Consumo Anual', size=12),
        'netsituation': fields.char('Red o Municipio de Ubicación', size=14),
        'outgoingpressuregranted': fields.float('Presión Salida Concedida', digits=(8, 3)),
        'caecode': fields.many2one('giscemisc.cnae', 'Código CAE'),
        # Dades Inspecció
        'lastinspectionsdate': fields.datetime('Fecha Ultima Inspección'),
        'lastinspectionsresult': fields.selection(TAULA_RESULTADO_INSPECCION, 'Resultado Ultima Inspección'),
        # Altres Dades
        'readingtype': fields.selection(TAULA_TIPO_LECTURA, 'Tipo de Lectura'),
        'rentingamount': fields.float('Importe Alquiler', digits=(7, 2)),
        'rentingperiodicity': fields.selection(TAULA_PERIODICIDAD_ALQUILER, 'Periodicidad Alquiler'),
        'canonircamount': fields.float('Importe Canon IRC', digits=(7, 2)),
        'canonircperiodicity': fields.selection(TAULA_PERIODICIDAD_CANON_IRC, 'Periodicidad Canon IRC'),
        'canonircforlife': fields.selection(TAULA_INDICATIVO_SI, 'Canon IRC Vitalicio'),
        'canonircdate': fields.datetime('Fecha Vigencia Canon IRC'),
        'canonircmonth': fields.selection(TAULA_MES, 'Mes Aplica Canon IRC si Anual'),
        'othersamount': fields.float('Importe Resto Conceptos', digits=(7, 2)),
        'othersperiodicity': fields.selection(TAULA_PERIODICIDAD_RESTO_CONCEPTOS, 'Periodicidad Resto de Conceptos'),
        'readingperiodicitycode': fields.selection(TAULA_PERIODICIDAD_LECTURA, 'Código Periodicidad Lectura'),
        'transporter': fields.selection(TAULA_SUJETO, 'Código de Transportista'),
        'transnet': fields.char('Red de Transporte', size=14),
        'gasusetype': fields.selection(TAULA_TIPOS_DE_USO_DEL_GAS, "Tipo de Uso del Gas"),
    }

    _defaults = {

    }


GiscegasAtr26A1()
