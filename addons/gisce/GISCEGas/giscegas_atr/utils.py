# -*- coding: utf-8 -*-
import dateutil.parser
from dateutil.relativedelta import relativedelta
from datetime import datetime
from gestionatr.defs_gas import TAULA_TIPO_VIA


def get_address_dict(address, mod="", sufix="owner"):
    if address.nv:
        via = address.nv[:60]
    else:
        via = " "
    addres_dict = {
        '{0}province{1}'.format(mod, sufix): (address.state_id and address.state_id.id) or (address.id_municipi and address.id_municipi.id and address.id_municipi.state and address.id_municipi.state.id),
        '{0}city{1}'.format(mod, sufix): address.id_municipi and address.id_municipi.id,
        '{0}zipcode{1}'.format(mod, sufix): address.zip,
        '{0}streettype{1}'.format(mod, sufix): get_tipo_via_gas(address.tv and address.tv.name),
        '{0}street{1}'.format(mod, sufix): via,
        '{0}streetnumber{1}'.format(mod, sufix): address.pnp or '0',
    }

    return addres_dict


def get_tipo_via_gas(name):
    if not name:
        return "CALLE"
    name = name.strip().lower()
    for row in TAULA_TIPO_VIA:
        if name in row[1].strip().lower():
            return row[0]
    return "CALLE"


def calc_annulmentreason_from_case(pool, cr, uid, cas_id, context=None):
    PROCES_TO_ANULMENT = {
        '02': '002',
        '04': '004',
        '41': '019',
    }
    if not cas_id:
        return False
    cas = pool.get("giscegas.atr").browse(cr, uid, cas_id)
    pname = cas.proces_id.name
    return PROCES_TO_ANULMENT.get(pname, False)


def calc_operationtype_from_case(pool, cr, uid, cas_id, context=None):
    if not cas_id:
        return False
    atr_obj = pool.get("giscegas.atr")
    step_obj = pool.get("giscegas.atr.step")
    cas = atr_obj.browse(cr, uid, cas_id)
    step_id = step_obj.search(cr, uid, [('proces_id', '=', cas.proces_id.id), ('name', '=', 'a1')])
    pas01 = atr_obj.get_pas(cr, uid, cas, step_id=step_id)
    return pas01.operationtype
