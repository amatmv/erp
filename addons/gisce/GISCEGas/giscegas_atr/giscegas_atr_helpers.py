# -*- coding: utf-8 -*-
from tools.translate import _
from osv import osv, fields, orm

PERIODICIDAD_FACTURACION_TO_PERIODICIDAD_LECTURA = {
    '01': 'M',
    '02': 'B',
    '03': 'A',
    '04': 'S',
    '05': 'D'
}


class GiscegasAtrException(Exception):
    def __init__(self, message, type_error):
        super(GiscegasAtrException, self).__init__(message)
        self.type_error = type_error
        self.error_fields = type_error


class GiscegasAtrHelpers(osv.osv):

    _name = 'giscegas.atr.helpers'
    _auto = False

    def processa_comptadors(self, cursor, uid, step, context=None):
        """
        Carreguem els comptadors des del pas A3-02
        El llistat de comptadors inclouen la lectura d'activació
        """
        pool = self.pool
        counter_obj = pool.get('giscegas.atr.counter')

        res = counter_obj.activa_comptador_a_polissa(
            cursor, uid, step
        )

        info = ''
        if res['comptadors']:
            info_tmpl = _(u" INFO: S'han creat comptadors: {}.")
            info += info_tmpl.format(','.join(res['comptadors']))

        if res['errors']:
            info_tmpl = _(u'Hi ha hagut errors processant punts de mesura: {}')
            info += info_tmpl.format(','.join(res['errors']))

        if res['baixes']:
            info_tmpl = _(u"S'han donat de baixa comptadors: {}.")
            info += info_tmpl.format(','.join(res['baixes']))

        return info

    def get_lot_facturacio(self, cursor, uid, data_lot, data_activacio):

        lot_obj = self.pool.get('giscegas.facturacio.lot')
        conf_obj = self.pool.get('res.config')
        conf_var = 'atr_gas_activacio_en_lot_actual'
        act_en_lot_actual = bool(int(conf_obj.get(cursor, uid, conf_var, '0')))

        if not act_en_lot_actual:
            search_param = [
                ('data_final', '>=', data_lot),
                ('state', '!=', 'tancat')
            ]
        else:
            search_param = [('data_final', '>=', data_activacio),
                            ('state', '!=', 'tancat')]

        lot = lot_obj.search(cursor, uid, search_param, order='data_inici')
        return lot

    def activa_polissa_from_a3n(self, cursor, uid, sw_id, context=None):
        raise NotImplementedError(_(u'Se debe instalar el módulo de comer para '
                                    u'poder activar el paso A3_02!'))

    def baixa_polissa_from_a104(self, cursor, uid, sw_id, context=None):
        """Dona de baixa el contracte d'un cas A1_04 (A3s)"""
        try:
            pool = self.pool

            sw_obj = pool.get('giscegas.atr')
            polissa_obj = pool.get('giscegas.polissa')
            factura_obj = pool.get('giscegas.facturacio.factura')
            conf_obj = pool.get('res.config')

            sw = sw_obj.browse(cursor, uid, sw_id)
            info = ''

            # Validem que el pas és final
            if not sw.finalitzat:
                # ERROR, no estem en un pas final
                info = _(u'Este caso (%s) no está finalizado '
                         u'(%s-%s)') % (sw.name, sw.proces_id.name,
                                        sw.step_id.name)
                raise GiscegasAtrException(info, _(u'ERROR'))

            if sw.rebuig:
                # ERROR, no estem en un pas final
                info = _(u'Este caso (%s) está finalizado pero es un '
                         u'rechazo (%s-%s)') % (sw.name, sw.proces_id.name,
                                               sw.step_id.name)
                raise GiscegasAtrException(info, _(u'ERROR'))

            pas02_obj = pool.get(
                "giscegas.atr.{0}.a1".format(sw.proces_id.name.lower()))

            # pas02 = pas02_obj.browse(
            #     cursor, uid,
            #     pas02_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
            # )
            pas = sw.get_pas()

            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)

            b1_impago = sw.proces_id.name in ('02', '41', '04') and polissa.state == 'tall'

            if not polissa.state == 'activa' and not b1_impago:
                # ERROR, la Pólissa ha d'estar activa
                info = _(u"Este contrato (%s) del caso '%s' tiene que "
                         u"estar activo") % (sw.cups_polissa_id.name, sw.name)
                raise GiscegasAtrException(info, _(u'ERROR'))

            # data baixa
            data_baixa = pas.transfereffectivedate

            polissa.write({'renovacio_auto': False, 'data_baixa': data_baixa})

            conf_var = 'atr_gas_allow_baixa_polissa_from_cn_without_invoice'
            allow_without_invoice = bool(
                int(conf_obj.get(cursor, uid, conf_var, '0'))
            )

            if not allow_without_invoice:
                search_vals = [
                    ('polissa_id', '=', polissa.id),
                    ('invoice_id.journal_id.code', 'ilike', 'GAS')
                ]

                factura_ids = factura_obj.search(
                    cursor, uid, search_vals, context={'active_test': False}
                )
                factura_vals = factura_obj.read(
                    cursor, uid, factura_ids, ['data_final', 'number', 'type']
                )

                tenim_fact_proveidor = tenim_fact_client = False

                for factura in factura_vals:
                    if factura['data_final'] == data_baixa:
                        if factura['type'] == 'in_invoice':
                            tenim_fact_proveidor = True
                        else:
                            tenim_fact_client = True

                if not tenim_fact_client:
                    # ERROR, no estem en un pas final
                    txt_tenim = _(u"Proveedor: %s. Cliente: %s") % (
                        tenim_fact_proveidor and 'Si' or 'No',
                        tenim_fact_client and 'Si' or 'No',
                    )
                    info += _(
                        u'Este contrato (%s) aun no tiene las facturas '
                        u"finales con fecha '%s' para el caso %s."
                        u"%s") % (
                        polissa.name, data_baixa, sw.name, txt_tenim
                    )

                    return ('INFO', info)

            # Ara ja podem donar de baixa el comptador (si en té, es clar)
            for comptador in polissa.comptadors:
                if comptador.active:
                    vals = {'active': 0}
                    if not comptador.data_baixa:
                        vals['data_baixa'] = data_baixa
                    comptador.write(vals)

            # I donem de baixa la polissa
            vals = {'data_baixa': data_baixa, 'renovacio_auto': 0}

            if not allow_without_invoice:
                if polissa.lot_facturacio:
                    vals.update({'lot_facturacio': False})

            polissa.write(vals, context={'from_baixa': True})
            polissa.send_signal(['baixa'])

            # tanquem el cas
            if not sw.case_close():
                info += _(u"Para el contrato '%s' no hemos podido cerrar "
                          u"el caso %s"
                          ) % (polissa.name, sw.name)
                return (u'ERROR', info)

            info += _(u"La baja del contrato del caso  %s "
                      u"se ha realizado correctamente.") % (sw.name)

            sw_obj.historize_additional_info(cursor, uid, sw)
            return ('OK', info)

        except osv.except_osv, e:
            info = u'%s (%s-%s) %s' % (
                sw.name, sw.proces_id.name, sw.step_id.name, unicode(e.value)
            )
            raise GiscegasAtrException(info, _(u'ERROR'))

        except orm.except_orm, e:
            info = u'%s (%s-%s) %s' % (
                sw.name, sw.proces_id.name, sw.step_id.name, unicode(e.value)
            )
            raise GiscegasAtrException(info, _(u'ERROR'))

        except Exception, e:
            raise GiscegasAtrException(e.message, _(u'ERROR'))

    def activar_A1_44_a2_close_case_if_autolectura(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}

        try:
            pool = self.pool

            sw_obj = pool.get('giscegas.atr')
            polissa_obj = pool.get('giscegas.polissa')
            factura_obj = pool.get('giscegas.facturacio.factura')
            conf_obj = pool.get('res.config')

            sw = sw_obj.browse(cursor, uid, sw_id)
            info = ''

            a2 = sw_obj.get_pas(cursor, uid, sw)

            if a2.rebuig:
                info = _(u'El caso {} relacionado con el contrato {} se ha '
                         u'quedado abierto ya que es un rechazo - '
                         u'Gestionar manualmente'
                         ).format(
                            sw.name, sw.cups_polissa_id.name
                         )
                return ('OK', info)

            tiene_auto_lectura = a2.operationtype == 'A20001'
            # cierra caso si autolectura -> operation_type = A20001
            if tiene_auto_lectura:
                if not sw.case_close():
                    info = _(u"Para el contrato '%s' no hemos podido cerrar "
                             u"el caso %s") % (sw.cups_polissa_id.name, sw.name)
                    raise GiscegasAtrException(info, _(u'ERROR'))
                info = _(u'Se ha cerrado el caso {} por el contrato {} '
                         u'ya que tiene autolectura (Operationtype: A20001)'
                         ).format(
                            sw.name, sw.cups_polissa_id.name
                        )
                return ('OK', info)

            info = _(u'El caso {} asociado al contrato {} para ser '
                     u'gestionado manualmente').format(
                sw.name, sw.cups_polissa_id.name
            )
            return ('OK', info)

        except Exception as untracked_error:
            info = u'%s (%s-%s) %s' % (
                sw.name, sw.proces_id.name, sw.step_id.name,
                unicode(untracked_error)
            )
            raise GiscegasAtrException(info, _(u'ERROR'))

    def is_cancelable_from_a103_a2(self, cursor, uid, polissa, context=None):
        # TODO This condition is temporal, fro the moment we allow to cancel
        # TODO all contracts in draft and (active and not last read) state
        '''
        :param cursor:
        :param uid:
        :param polissa: Browse record of contract giscegas.polissa
        :param context:
        :return:
        '''
        if context is None:
            context = {}

        msg = 'OK'

        cancellable = polissa.state in ('esborrany', 'activa')

        fecha_ultima_lectura = (
            polissa.data_ultima_lectura and polissa.state == 'activa'
        )

        if not cancellable:
            msg = _('No se puede cancelar el contrato {} en estado {}, '
                    'ya que no está en borrador o activo').format(polissa.name,
                                                                   polissa.state
                                                                   )
        else:
            if fecha_ultima_lectura:
                msg = _('No se puede cancelar el contrato {}, '
                        'ya que el contrato está activo y con fecha'
                        ' de última lectura'
                        ).format(
                          polissa.name, polissa.state
                        )

        return cancellable, msg

    def activar_A103_a2(self, cursor, uid, sw_id, context=None):
        '''
        Metodo para activar contrato cambios para A103 paso a2
        :param cursor:
        :param uid:
        :param sw_id: long id
        :param context:
        :return:
        '''
        if context is None:
            context = {}

        try:
            pool = self.pool

            sw_obj = pool.get('giscegas.atr')
            polissa_obj = pool.get('giscegas.polissa')
            factura_obj = pool.get('giscegas.facturacio.factura')
            conf_obj = pool.get('res.config')

            sw = sw_obj.browse(cursor, uid, sw_id)
            info = ''

            if sw.rebuig:
                info = _(u'El caso {} relacionado con el contrato {} se ha '
                         u'quedado abierto ya que es un rechazo - '
                         u'Gestionar manualment'
                         ).format(
                    sw.name, sw.cups_polissa_id.name
                )
                return ('OK', info)

            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)

            is_cancellable, cancel_msg = self.is_cancelable_from_a103_a2(
                cursor, uid, polissa, context=context
            )

            if is_cancellable:
                polissa.pre_canceling_behaviour(context=context)

                try:
                    polissa.send_signal(['cancelar'])
                except (osv.except_osv, orm.except_orm), e:
                    info = _(u"El contrato '%s' del caso '%s' no se ha podido "
                             u"cancelar: %s'") % (
                           polissa.name, sw.name, e.value)
                    raise GiscegasAtrException(info, _(u'ERROR'))
                except Exception as e:
                    info = _(u"El contrato '%s' del caso '%s' no se ha podido "
                             u"cancelar: %s'") % (
                           polissa.name, sw.name, e.message)
                    raise GiscegasAtrException(info, _(u'ERROR'))

                if not sw.case_close():
                    info = _(u"Para el contrato '%s' no hemos podido cerrar "
                             u"el caso %s") % (polissa.name, sw.name)
                    raise GiscegasAtrException(info, _(u'ERROR'))

                pasa1_obj = pool.get("giscegas.atr.{0}.a1".format(sw.proces_id.name.lower()))

                pasa1 = pasa1_obj.browse(
                    cursor, uid,
                    pasa1_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
                )
                related_case_sw_id = pasa1.atr_anulat_id and pasa1.atr_anulat_id.id

                if related_case_sw_id:
                    sw_related = sw_obj.browse(
                        cursor, uid, related_case_sw_id, context=context
                    )
                    if not sw_related.case_close():
                        info = _(u"No se ha podido cerrar el cas "
                                 u"porque el caso relacionado %s no se ha "
                                 u"podido cerrar"
                                 ) % sw_related.name
                        raise GiscegasAtrException(info, _(u'ERROR'))

                else:
                    info = _(u"No se ha encontrado el caso A1_02 o A1_41 "
                             u"relacionado con el caso '%s' ") % sw.name
                    raise GiscegasAtrException(info, _(u'ERROR'))

                info = _(u"El contrato del caso %s "
                         u"se ha cancelado correctamente.") % (sw.name)
                return ('OK', info)

            else:
                info = u'Caso {}: {}'.format(cancel_msg, sw.name)

                raise GiscegasAtrException(info, _(u'ERROR'))

        except Exception as untracked_error:
            info = u'%s (%s-%s) %s' % (
                sw.name, sw.proces_id.name, sw.step_id.name,
                unicode(untracked_error)
            )
            raise GiscegasAtrException(info, _(u'ERROR'))

    def actualitza_polissa_from_atr(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}

        pool = self.pool
        sw_obj = pool.get('giscegas.atr')
        polissa_obj = pool.get('giscegas.polissa')
        sw = sw_obj.browse(cursor, uid, sw_id)
        try:
            pas = sw.get_pas()
            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)

            # Validem que el pas és final
            if not sw.finalitzat:
                # ERROR, no estem en un pas final
                info = _(u'Este caso (%s) no está finalizado '
                         u'(%s-%s)') % (sw.name, sw.proces_id.name, sw.step_id.name)
                raise GiscegasAtrException(info, _(u'ERROR'))
            if sw.rebuig:
                # ERROR, no estem en un pas final
                info = _(u'Este caso (%s) está finalizado pero es un '
                         u'rechazo (%s-%s)') % (sw.name, sw.proces_id.name, sw.step_id.name)
                raise GiscegasAtrException(info, _(u'ERROR'))

            vals = {
                'fecha_inspeccion': pas.interventiondate,
                'resultado_inspeccion': pas.resultinspection
            }
            polissa.write(vals)

            # pwe_template = 'switching_notificacio_activacio_m1_email'
            pm_txt = self.processa_comptadors(cursor, uid, pas)
            res = sw.notifica_a_client(context=context)

            if len(res) != 2 and res[0] != 'OK':
                return res

            # tanquem el cas
            if not sw.case_close():
                info = _(u"No se ha podido cerrar el caso '%s' para el contrato '%s'") % (sw.name, polissa.name)
                return (_(u'ERROR'), info)

            info = _(u"Las modificaciones del caso  %s "
                     u"se han realizado correctamente.") % (sw.name)
            return ('OK', info)
        except osv.except_osv, e:
            info = u'%s (%s-%s) %s' % (
                sw.name, sw.proces_id.name, sw.step_id.name, unicode(e.value)
            )
            raise GiscegasAtrException(info, _(u'ERROR'))

        except orm.except_orm, e:
            info = u'%s (%s-%s) %s' % (
                sw.name, sw.proces_id.name, sw.step_id.name, unicode(e.value)
            )
            raise GiscegasAtrException(info, _(u'ERROR'))

        except Exception, e:
            raise GiscegasAtrException(e.message, _(u'ERROR'))

GiscegasAtrHelpers()
