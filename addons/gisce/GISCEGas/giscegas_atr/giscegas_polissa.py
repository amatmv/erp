# -*- coding: utf-8 -*-

from __future__ import absolute_import

from gestionatr.defs_gas import TAULA_RESULTADO_INSPECCION
import netsvc
import traceback
from ast import literal_eval as eval

from osv import osv, fields, orm
from tools.translate import _
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException
from datetime import datetime


class GiscegasPolissaInspeccio(osv.osv):
    """Funcions utilitzades pel switching
    """
    _name = 'giscegas.polissa.inspeccio'

    _columns = {
        'fecha_inspeccion': fields.datetime('Fecha Inspección', required=True),
        'resultado_inspeccion': fields.selection(
            TAULA_RESULTADO_INSPECCION, 'Resultado Inspección', required=True
        ),
        'polissa_id': fields.many2one("giscegas.polissa", "Contrato")
    }

GiscegasPolissaInspeccio()


class GiscegasPolissa(osv.osv):
    """Funcions utilitzades pel switching
    """
    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def arregla_cups(self, cursor, uid, polissa_id, context=None):
        ''' Arreglem CUPS
         això ens ho podem estalviar si ja ho arreglem al crear-lo, ja
         sigui des de web o automàticament
         FENOSA -> 1P
         ALTRES -> 0F'''
        if not polissa_id:
            return
        p_obj = self.pool.get('giscegas.polissa')
        cups_obj = self.pool.get('giscegas.cups.ps')

        pol = p_obj.browse(cursor, uid, polissa_id, context)

        # Si no hi ha CUPS no podem fer res
        if not pol.cups:
            return ''

        res = ''

        # Si es de la nostra distribuidora no el podem canviar
        # ja que trencaria la sincronitzacio
        cups_fields = cups_obj.fields_get_keys(cursor, uid)
        if 'propi' in cups_fields and pol.cups.propi:
            return res

        if len(pol.cups.name) == 20:
            # FENOSA
            cups_name = pol.cups.name
            participant_obj = self.pool.get("giscemisc.participant")
            try:
                participant = participant_obj.get_from_partner(cursor, uid, pol.cups.distribuidora_id.id)
                if participant.codi_sifco == '0022':
                    sufix = '1P'
                else:
                    sufix = '0F'
                nou_cups = cups_name + sufix
                pol.cups.write({'name': nou_cups})
                pol = p_obj.browse(cursor, uid, polissa_id, context)
                res = (_(u'CUPS modificat (%s)  %s -> %s ') %
                       (sufix, cups_name, pol.cups.name))
            except Exception, e:
                pass
        return res

    def crear_cas_atr(self, cursor, uid, polissa_id, proces=None,
                      config_vals=None, context=None):
        """ A partir d'una pólissa crea un cas ATR de Gas
        """
        if not context:
            context = {}
        log_params = {
            'origin': '',
            'pas': 'a1',
            'proces': '',
            'status': 'error',
            'info': '',
            'file': '',
            'cups': False,
            'emisor_id': False,
            'cups_text': '',
            'tipus': 'generate',
            'codi_emisor': ''
        }
        # With this param will force crate of switching logs
        context['force_log'] = True

        swlog_obj = self.pool.get('giscegas.atr.log')
        try:
            # Do this due the browse record object always pass the id as a list
            # of one element.
            if isinstance(polissa_id, (list, tuple)):
                polissa_id = polissa_id[0]

            supported_cases = ['41', '02', '05', '48', '44', '04', '03', '46']

            proces = context.get('proces', proces or '41')

            pol_obj = self.pool.get('giscegas.polissa')
            sw_obj = self.pool.get('giscegas.atr')
            swproc_obj = self.pool.get('giscegas.atr.proces')
            swpas_obj = self.pool.get('giscegas.atr.step')
            swinfo_obj = self.pool.get('giscegas.atr.step.info')

            pol = pol_obj.browse(cursor, uid, polissa_id, context)

            if proces == 'An':
                if u'proces: A1_02' in (pol.observacions or ''):
                    proces_name = '02'
                elif u'proces: A1_41' in (pol.observacions or ''):
                    proces_name = '41'
                elif u'proces: A1_38' in (pol.observacions or ''):
                    proces_name = '38'
                elif u'canvi_titular: 1' in (pol.observacions or ''):
                    proces_name = '41'
                else:
                    proces_name = '02'
            else:
                proces_name = proces

            context['proces_name'] = proces_name
            # Fill origin and pas of sw log
            log_params['origin'] = _('Generació -> Polissa')
            log_params['file'] = _('Contracte: {0}').format(pol.name)
            log_params['cups'] = pol.cups.id
            log_params['cups_text'] = pol.cups.name
            log_params['proces'] = proces_name

            where = sw_obj.whereiam(cursor, uid, pol.cups.id, context=context)
            if where == 'comer':
                partner_id = pol.distribuidora.id
            elif where == 'distri':
                partner_id = pol.comercialitzadora.id
            log_params['emisor_id'] = partner_id
            part_obj = self.pool.get('giscemisc.participant')
            emi_id = part_obj.get_id_from_partner(cursor, uid, partner_id)
            ref_emi = part_obj.read(cursor, uid, emi_id, ['codi_sifco'])
            ref_emi = ref_emi.get('codi_sifco', '')
            log_params['codi_emisor'] = ref_emi

            def formatResult(message, case_id=False):
                logger = netsvc.Logger()
                action = '(%s) %s' % (proces_name, pol.name)
                logger.notifyChannel('switching', netsvc.LOG_DEBUG,
                                     '%s: %s' % (action, message))
                return [action, message, case_id or False]

            if proces_name not in supported_cases:
                return formatResult(_(u'Caso %s no soportado') % (proces_name))

            proces_id = swproc_obj.search(cursor, uid,
                                          [('name', '=', proces_name)])[0]

            # comprovem que hi hagi CUPS
            if not pol.cups:
                msg_err = _(u'Contrato no tiene CUPS, no se puede generar caso ATR')
                log_params['info'] = msg_err
                swlog_obj.create(cursor, uid, log_params, context=context)
                return formatResult(msg_err)

            #comprovem que està en esborrany per 41
            if pol.state != 'esborrany' and proces_name in ('41'):
                msg_err = _(u'Contrato en estado %s, la saltamos') % _(pol.state)
                log_params['info'] = msg_err
                swlog_obj.create(cursor, uid, log_params, context=context)
                return formatResult(msg_err)

            #calculem l'adressa i el mail a partir del partner
            sw_params = {
                'proces_id': proces_id,
                'cups_polissa_id': polissa_id,
            }
            vals = sw_obj.onchange_polissa_id(cursor, uid, [], polissa_id, None,
                                              context=context)

            if vals['warning']:
                #Hi ha hagut algun problema buscant les dades
                warn_txt = '%s: %s' % (
                    vals['warning']['title'],
                    vals['warning']['message'],
                )
                return formatResult(warn_txt)

            sw_params.update(vals['value'])
            #si no tenim ref_contracte, ens l'inventem (de moment)
            if not sw_params['ref_contracte']:
                sw_params['ref_contracte'] = '111111111'

            # Arreglem el CUPS
            result = self.arregla_cups(cursor, uid, polissa_id, context=context)
            if result:
                #warning: s'ha modificat el CUPS
                formatResult(result)

            sw_id = None
            pas_id = None
            info_id = None
            try:
                if config_vals:
                    if proces_name == '41':
                        pas_obj = self.pool.get('giscegas.atr.41.a1')
                        pas_obj.config_step_validation(cursor, uid, [], config_vals, context=context)
                    elif proces_name == '02':
                        pas_obj = self.pool.get('giscegas.atr.02.a1')
                        pas_obj.config_step_validation(cursor, uid, [], config_vals, context=context)
                    elif proces_name == '05':
                        pas_obj = self.pool.get('giscegas.atr.05.a1')
                        pas_obj.config_step_validation(cursor, uid, [], config_vals, context=context)
                    elif proces_name == '44':
                        pas_obj = self.pool.get('giscegas.atr.44.a1')
                        pas_obj.config_step_validation(cursor, uid, [], config_vals, context=context)
                    elif proces_name == '04':
                        pas_obj = self.pool.get('giscegas.atr.04.a1')
                        pas_obj.config_step_validation(cursor, uid, [], config_vals, context=context)
                    elif proces_name == '48':
                        pas_obj = self.pool.get('giscegas.atr.48.a1')
                        pas_obj.config_step_validation(cursor, uid, [], config_vals, context=context)
                    elif proces_name == '03':
                        pas_obj = self.pool.get('giscegas.atr.03.a1')
                        pas_obj.config_step_validation(cursor, uid, [], config_vals, context=context)
                    elif proces_name == '46':
                        pas_obj = self.pool.get('giscegas.atr.46.a1')
                        pas_obj.config_step_validation(cursor, uid, [], config_vals, context=context)

                # creem el Cas
                sw_id = sw_obj.create(cursor, uid, sw_params)
                # creeem el pas
                pas_id = swpas_obj.get_step(cursor, uid, 'a1', proces_name)

                #Creant info ja crea automaticament tota la info del pas
                info_vals = {
                    'sw_id': sw_id,
                    'proces_id': proces_id,
                    'step_id': pas_id,
                }
                info_id = swinfo_obj.create(cursor, uid, info_vals)
                info = swinfo_obj.browse(cursor, uid, info_id)
                model_obj, model_id = info.pas_id.split(',')
                pas_obj = self.pool.get(model_obj)
                pas = pas_obj.browse(cursor, uid, int(model_id))

                if config_vals:
                    if proces_name in supported_cases:
                        pas_obj.config_step(
                            cursor, uid, pas.id, config_vals, context=context
                        )

                    if config_vals.get('retail_tariff'):
                        sw_params.update({
                            'tarifa_comer_id': config_vals['retail_tariff'],
                            'actualitzar_tarifa_comer': config_vals.get('change_retail_tariff', False),
                        })

                if not sw_id or not pas_id or not info_id:
                    msg_err = _(u'Error creando casp c:%s p:%s i: %s') % (
                        sw_id, pas_id, info_id
                    )
                    log_params['info'] = msg_err
                    swlog_obj.create(cursor, uid, log_params, context=context)
                    return formatResult(msg_err, sw_id)

                message = _(u'Caso %s creado correctamente') % sw_id

                if pas.validacio_pendent:
                    message += _(u'. WARN nombre: %s, %s %s') % (
                        pas.nom, pas.cognom_1, pas.cognom_2)

                result = formatResult(message, sw_id)
                # Obrir o deixar en Borrador el cas segons variable de
                # configuració
                conf_obj = self.pool.get('res.config')
                case_state = conf_obj.get(
                    cursor, uid, 'atr_gas_from_polissa_case_state_open', '1'
                )
                if case_state != '0':
                    sw_obj.case_open(cursor, uid, sw_id)
                # Update Aditional Info
                pas_obj.write(cursor, uid, pas.id, {"date_created": pas.date_created})
                log_params['info'] = message
                log_params['status'] = 'correcte'
                sw_inst = sw_obj.browse(cursor, uid, sw_id, context=context)
                log_params['request_code'] = sw_inst.codi_sollicitud or ''
                swlog_obj.create(cursor, uid, log_params, context=context)
                return result
            except GiscegasAtrException as e:
                msg_err = _(u"ERROR: No se ha creado el caso por problemas con "
                            u"la configuración del Paso a1: "
                            u"{0} -> {1}").format(e, e.error_fields)
                log_params['info'] = msg_err
                swlog_obj.create(cursor, uid, log_params, context=context)
                if sw_id:
                    # Delete created case
                    sw_obj.unlink(cursor, uid, sw_id)
                return formatResult(msg_err, sw_id)
            except Exception as e:
                traceback.print_stack()
                if sw_id:
                    # Delete created case
                    sw_obj.unlink(cursor, uid, sw_id)
                if not sw_id or not pas_id or not info_id:
                    msg_err = _(u"ERROR: No se ha creado el caso: %s") % (e,)
                    log_params['info'] = msg_err
                    swlog_obj.create(cursor, uid, log_params, context=context)
                    return formatResult(msg_err, sw_id)
                msg_err = _(u'ERROR creando el caso: (%s)') % (e,)
                log_params['info'] = msg_err
                swlog_obj.create(cursor, uid, log_params, context=context)
                return formatResult(msg_err, sw_id)
        except Exception as e:
            msg_err = _(u'ERROR creando el caso: (%s)') % (e,)
            log_params['info'] = msg_err
            swlog_obj.create(cursor, uid, log_params, context=context)
            raise e

    def get_address_with_phone(self, cursor, uid, polissa_id, context=None):
        if context is None:
            context = {}

        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]

        polissa = self.browse(cursor, uid, polissa_id, context=context)

        # First we get the addres from pagador
        direccio = polissa.direccio_pagament

        # If we don't have pagador addres or it doesn't have phones,
        # we get notification address
        if not direccio or \
                (direccio and not direccio.phone and not direccio.mobile):
            direccio = polissa.direccio_notificacio

            # If we don't have notification address or it doesn't have phones
            # we search in the addresses of the titular until we find a phone
            if not direccio or \
                    (direccio and not direccio.phone and not direccio.mobile):
                direccio = False
                for dir in polissa.titular.address:
                    if dir.phone or dir.mobile:
                        direccio = dir
                        break

        return direccio

    def escull_llista_preus(self, cursor, uid, id, llista, context=None):
        """ A partir d'una llista de preus, necessitem la tarifa més
        adequada i única. Retornem la llista si només n'hi ha una, si no,
        False. S'ha de modificar segons convingui a cada client."""
        pricelist_obj = self.pool.get('product.pricelist')
        if llista and isinstance(llista[0], int):
            llista = pricelist_obj.browse(cursor, uid, llista, context)
        if not llista or len(llista) > 1:
            llista_preus = False
        else:
            llista_preus = llista[0]

        return llista_preus

    def get_last_selfmetering(self, cursor, uid, polissa_id, context=None):
        """
         Gets last self-metering date from contract active meter
        """
        if isinstance(polissa_id, (list,tuple)):
            polissa_id = polissa_id[0]

        measure_obj = self.pool.get('giscegas.lectures.lectura.pool')
        if not measure_obj:
            return False
        # autolectura
        origen_obj = self.pool.get('giscegas.lectures.origen')
        origen_autolectura_id = origen_obj.search(cursor, uid,
                                                  [('codi', '=', '2')])[0]

        res = False
        pol_vals = self.read(cursor, uid, polissa_id, ['comptadors'],
                             context=context)

        comptadors = pol_vals['comptadors']
        search_vals = [('comptador', 'in', comptadors),
                       ('origen_id', '=', origen_autolectura_id)]
        measures_ids = measure_obj.search(cursor, uid, search_vals,
                                          order='name DESC')
        if measures_ids:
            measure_vals = measure_obj.read(cursor, uid, measures_ids[0],
                                            ['name'], context=context)
            date = measure_vals['name']
            search_vals.append(('name', '=', date))
            res = measure_obj.search(cursor, uid, search_vals,
                                     context=context)

        return res

    def wkf_baixa(self, cursor, uid, ids):
        res = super(GiscegasPolissa, self).wkf_baixa(cursor, uid, ids)
        conf_obj = self.pool.get('res.config')
        proces_obj = self.pool.get("giscegas.atr.proces")
        sw_obj = self.pool.get("giscegas.atr")

        proces_list = eval(conf_obj.get(
            cursor, uid, 'atr_gas_baixa_contract_close_cases', "[]"
        ))
        proces_ids = proces_obj.search(cursor, uid, [('name', 'in', proces_list)])

        sw_ids = sw_obj.search(
            cursor, uid,
            [('proces_id', 'in', proces_ids),
             ('state', '!=', 'done'),
             ('cups_polissa_id', 'in', ids)]
        )

        sw_obj.write(cursor, uid, sw_ids, {'state': 'done'})

        return res
      
    def pre_canceling_behaviour(self, cursor, uid, ids, context=None):
        pass

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if 'resultado_inspeccion' in vals:
            insp_vals = {
                'resultado_inspeccion': vals.get('resultado_inspeccion'),
                'fecha_inspeccion': vals.get('fecha_inspeccion', datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            }
            self.add_inspeccio(cursor, uid, ids, insp_vals, context=context)
            vals.pop('resultado_inspeccion')
            if 'fecha_inspeccion' in vals:
                vals.pop('fecha_inspeccion')
        res = super(GiscegasPolissa, self).write(cursor, uid, ids, vals, context=context)
        return res

    def add_inspeccio(self, cursor, uid, ids, insp_vals, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res_ids = []
        inspeccio_o = self.pool.get("giscegas.polissa.inspeccio")
        for pol_id in ids:
            vals = insp_vals.copy()
            vals['polissa_id'] = pol_id
            insp_id = inspeccio_o.create(cursor, uid, vals, context=context)
            self.write(cursor, uid, pol_id, {'inspeccio_ids': [(4, insp_id)]})
            res_ids.append(insp_id)
        return res_ids

    _columns = {
        'inspeccio_ids': fields.one2many('giscegas.polissa.inspeccio', 'polissa_id', 'Inspecciones')
    }


GiscegasPolissa()

