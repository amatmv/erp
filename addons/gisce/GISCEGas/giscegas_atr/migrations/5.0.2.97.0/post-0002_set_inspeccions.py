# -*- coding: utf-8 -*-
"""Passa tipus activacio de character varying(1) a character varying(2)
"""
import pooler
import logging
from tqdm import tqdm


def up(cursor, installed_version):
    logger = logging.getLogger('migration')

    if not installed_version:
        return

    logger.info('Crear les dades de inspeccions dels contractes')
    logger.info('Calcular el camp agent_accio_pendent per els casos no tancats')
    pool = pooler.get_pool(cursor.dbname)
    polissa_o = pool.get("giscegas.polissa")
    uid = 1
    cursor.execute("select id, resultado_inspeccion, fecha_inspeccion from giscegas_polissa where fecha_inspeccion is not Null")
    for pol_inf in tqdm(cursor.dictfetchall()):
        polissa_o.add_inspeccio(cursor, uid, pol_inf['id'], {
            'resultado_inspeccion': pol_inf['resultado_inspeccion'],
            'fecha_inspeccion': pol_inf['fecha_inspeccion'],
        })

    logger.info('Fet')

migrate = up
