# -*- coding: utf-8 -*-
"""Passa tipus activacio de character varying(1) a character varying(2)
"""
import pooler
import logging
from tqdm import tqdm


def up(cursor, installed_version):
    logger = logging.getLogger('migration')

    if not installed_version:
        return

    logger.info('Posar els casos tancats amb agent_accio_pendent \'comer\'')
    query = """UPDATE giscegas_atr SET accio_pendent_comerdist = 'comer' WHERE id in (
        select atr.id from giscegas_atr atr JOIN crm_case cas on cas.id=atr.case_id where cas.state in ('done', 'cancel')
    )"""
    cursor.execute(query)
    logger.info('Fet')

    logger.info('Calcular el camp agent_accio_pendent per els casos no tancats')
    pool = pooler.get_pool(cursor.dbname)
    sw_obj = pool.get("giscegas.atr")
    uid = 1
    for sw_id in tqdm(sw_obj.search(cursor, uid, [('state', 'not in', ['done', 'cancel'])])):
        sw_obj.update_ff_accio_pendent_comerdist(cursor, uid, [sw_id])

    logger.info('Fet')

migrate = up
