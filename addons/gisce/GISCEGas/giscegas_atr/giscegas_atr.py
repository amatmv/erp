# -*- coding: utf-8 -*-
from __future__ import absolute_import

import logging
import base64
import tools
import time
import calendar
import traceback
from datetime import datetime, timedelta
import sys

from addons import logger
from mongodb_backend import osv_mongodb
from osv.osv import TransactionExecute
from .rebuig import Rebuig

try:
    from collections import OrderedDict, namedtuple
except:
    from ordereddict import OrderedDict

import pooler
from osv import osv, fields, orm
from tools.translate import _
import netsvc
from tools import config

from lxml import etree

# from gestionatr.defs_gas import *
from gestionatr.input.messages import message_gas, A1_41, A1_02, A1_05, A1_44, A1_04, A1_48, A1_03, A1_46, A12_26 as A1_26, A19_45 as A1_45
from gestionatr.output.messages import sw_a1_41 as a1_41
from gestionatr.output.messages import sw_a1_02 as a1_02
from gestionatr.output.messages import sw_a1_05 as a1_05
from gestionatr.output.messages import sw_a1_44 as a1_44
from gestionatr.output.messages import sw_a1_04 as a1_04
from gestionatr.output.messages import sw_a1_48 as a1_48
from gestionatr.output.messages import sw_a1_03 as a1_03
from gestionatr.output.messages import sw_a1_46 as a1_46
from gestionatr.defs_gas import *
from gestionatr.utils import validate_xml, ValidationResult
from giscegas_polissa.giscegas_polissa import CONTRACT_IGNORED_STATES
from osv.expression import OOQuery
from giscegas_cups.dso_cups.cups import *

SUPORTED_PROCESS = ['41', '02', '05', '44', '04', '48', '03', '46', '26', '45']


class GiscegasAtrProces(osv.osv):

    _name = 'giscegas.atr.proces'
    _desc = 'Procés de atr gas'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        """Get initial steps for proces"""
        return []

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        """get steps where we are emisor"""
        return []

    def get_reject_steps(self, cursor, uid, proces, context=None):
        """get steps that can be in a rejected state"""
        return []

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        """get steps that should be sent to the old company"""
        return []

    def get_proces(self, cursor, uid, name):
        search_params = [('name', '=', name)]
        return self.search(cursor, uid, search_params)[0]

    def get_steps(self, cursor, uid, proces_id, proces='', context=None):
        if context is None:
            context = {}
        if not proces_id and proces:
            search_params = [('name', '=', proces)]
            proces_id = self.search(cursor, uid, search_params)[0]
        proces = self.browse(cursor, uid, proces_id)
        res = []
        if context.get("add_description"):
            return [(step.name, step.get_step_model(), step.description)
                    for step in proces.step_ids]
        else:
            return [(step.name, step.get_step_model())
                    for step in proces.step_ids]

    _columns = {
        'name': fields.char('Proceso', size=2, required=True),
    }

GiscegasAtrProces()


class GiscegasAtrStep(osv.osv):

    _name = 'giscegas.atr.step'
    _desc = 'Passos dels processos de atr de gas'

    def get_step(self, cursor, uid, name, proces, context=None):
        search_params = [('name', '=', name),
                         ('proces_id.name', '=', proces)]
        return self.search(cursor, uid, search_params)[0]

    def get_next_steps(self, cursor, uid, step, proces, where, context=None):
        '''returns next valid step depending on process, step
        and where we are (comer or distri)'''
        step_obj = self.pool.get('giscegas.atr.step')
        model = self.get_step_model(cursor, uid, [], step, proces,
                                    context=context)
        model_obj = self.pool.get(model)
        return model_obj.get_next_steps(cursor, uid, where,
                                       context=context)

    def get_step_model(self, cursor, uid, ids,
                       name='', proces='', context=None):

        #If a step name is passed and no ids
        #search for id with that name first
        if not ids and name and proces:
            search_params = [('name', '=', name),
                             ('proces_id.name', '=', proces)]
            ids = self.search(cursor, uid, search_params)[0]
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        step = self.browse(cursor, uid, ids, context=context)
        return 'giscegas.atr.%s.%s' % (step.proces_id.name.lower(),
                                               step.name)

    def name_get_step(self, cursor, uid, pas_ids, proces, step, context=None):
        res = []
        model_obj = self.pool.get(self.get_step_model(cursor, uid, [],
                                                      name=step,
                                                      proces=proces,
                                                      context=context))
        for pas in model_obj.browse(cursor, uid, pas_ids, context=context):
            solicitud = pas.sw_id and pas.sw_id.codi_sollicitud
            res.append((pas.id,
                        '%s (%s) %s (%s -> %s)' % (proces,
                                                   step,
                                                   solicitud,
                                                   pas.emisor_id.codi_sifco,
                                                   pas.receptor_id.codi_sifco)))
        return res

    _columns = {
        'name': fields.char('Paso', size=3, required=True),
        'proces_id': fields.many2one('giscegas.atr.proces', 'Proceso',
                                     required=True),
        'accio_pendent': fields.selection(
            string="Agent amb accio pendent",
            selection=[('comer', "Comercialitzadora"), ("distri", "Distribuidora")]
        ),
        'description': fields.char('Descripcio', size=64, required=False, translate=True),
    }

GiscegasAtrStep()


class GiscegasAtrProces2(osv.osv):

    _name = 'giscegas.atr.proces'
    _inherit = 'giscegas.atr.proces'

    _columns = {
        'step_ids': fields.one2many('giscegas.atr.step', 'proces_id',
                                    'Pasos'),
    }
GiscegasAtrProces2()


class GiscegasAtr(osv.osv):
    """Classe per gestionar el canvi de comercialitzador
    """

    _name = "giscegas.atr"
    # s'usa OrderectDict per tal que el 'name' correspongui al de crm.case
    _inherits = OrderedDict([('poweremail.conversation', 'conversation_id'),
                             ('crm.case', 'case_id')])
    # _estats_valids = {estat futur: [possibles estats actuals]}
    _order = 'id desc'

    def create(self, cursor, uid, values, context=None):
        '''Assign a name to the case if not done yet'''
        proces_obj = self.pool.get('giscegas.atr.proces')
        cups_obj = self.pool.get('giscegas.cups.ps')

        if 'cups_id' in values and values['cups_id']:
            cups_name = cups_obj.read(
                cursor, uid, values['cups_id'], ['name']
            )['name']
            values['cups_input'] = cups_name
        else:
            cups_name = values.get('cups_input', False)

        if 'name' not in values or not values['name']:
            proces_name = proces_obj.read(cursor, uid, values['proces_id'],
                                          ['name'], context=context)['name']
            case_name = _(u"Proceso %s para el cups: %s") % (proces_name,
                                                             cups_name)
            values['name'] = case_name

        return super(GiscegasAtr, self).create(cursor, uid, values,
                                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        """S'elimina el cas i tots els passos rel·lacionats
        """
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        case_obj = self.pool.get('crm.case')
        conv_obj = self.pool.get('poweremail.conversation')

        for sw in self.browse(cursor, uid, ids, context=context):
            try:
                for step in sw.step_ids:
                    step.unlink()
                case_obj.unlink(cursor, uid, [sw.case_id.id],
                                context=context)
                conv_obj.unlink(cursor, uid, [sw.conversation_id.id],
                                context=context)
            except KeyError as e:
                ids.remove(sw.id)
        super(GiscegasAtr, self).unlink(cursor, uid,
                                               ids, context=context)

    def copy(self, cursor, uid, sw_id, default=None, context=None):
        """Quan es copia un cas, se li ha d'assignar una seqüència nova"""
        # actualitzem el codi i data de sol·licitud
        data_sol = time.strftime('%Y-%m-%d'),
        codi_sol = self._get_default_codi_sollicitud(cursor, uid, context)
        vals = {'codi_sollicitud': codi_sol, 'data_sollicitud': data_sol,
                'step_ids': None}

        res_id = super(GiscegasAtr, self).copy(cursor, uid, sw_id, vals,
                                                      context)
        # hem de duplicar els pasos, perquè sinó fa una referència als del cas
        # original i no els duplica
        sw = self.browse(cursor, uid, sw_id, context)
        swi_obj = self.pool.get('giscegas.atr.step.info')
        vals = {'sw_id': res_id}
        for info in sw.step_ids:
            pas_model = self.pool.get(info.pas_id.split(',')[0])
            pas_id = info.pas_id.split(',')[1]
            nou_pas_id = pas_model.copy(cursor, uid, pas_id, vals, context)
            pas_val = '%s,%s' % (info.pas_id.split(',')[0], nou_pas_id)
            vals = {'sw_id': res_id, 'pas_id': pas_val}
            info_id = swi_obj.copy(cursor, uid, info.id, vals, context)
        return res_id

    def get_pas_model_name(
            self, cursor, uid, sw_id, step_id=False, context=None):
        """
        Return PAS Model Name of a SW Case on `step_id`
        :param cursor:  OpenERP Cursor
        :param uid:     Current User ID
        :type uid:      int
        :param sw_id:   SW Case ID
        :type sw_id:    long
        :param step_id:   SW Step ID
        :type step_id:    long
        :param context: OpenERP context
        :type context:  dict
        :return:
        """
        sw_obj = self.pool.get('giscegas.atr')
        step_obj = self.pool.get('giscegas.atr.step')
        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        if not step_id:
            step_id = sw_obj.read(
                cursor, uid, sw_id, ['step_id'], context=context)['step_id']
            if not step_id:
                return False
            step_id = step_id[0]
        return step_obj.get_step_model(cursor, uid, step_id, context=context)

    def get_pas_id(self, cursor, uid, sw_id, step_id=False, context=None):
        """
        Return current PAS ID of a SW Case
        :param cursor:  OpenERP Cursor
        :param uid:     Current User ID
        :type uid:      int
        :param sw_id:   SW Case ID
        :type sw_id:    long
        :param step_id: SW Step ID
        :type step_id:  long
        :param context: OpenERP context
        :type context:  dict
        :return:
        """
        if context is None:
            context = {}
        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        step_model = self.get_pas_model_name(
            cursor, uid, sw_id, step_id=step_id, context=context)
        pas_obj = self.pool.get(step_model)
        return pas_obj.search(cursor, uid, [('sw_id', '=', sw_id)])[0]

    def get_pas(self, cursor, uid, sw, step_id=False, context=None):
        """
        Return a Browse Record of the current PAS of a SW Case
        :param cursor:  OpenERP Cursor
        :param uid:     Current User ID
        :param sw:      Browse record of a SW Case (or ID list/tuple)
        :type sw:       browse_record, [int]
        :param step_id: ID of current SW Step
        :type step_id:  long
        :param context: OpenERP Context
        :type context:  dict
        :return:
        """
        if context is None:
            context = {}
        if isinstance(sw, (list, tuple)):
            sw = self.browse(cursor, uid, sw[0], context=context)
        pas_obj = self.pool.get(
            sw.get_pas_model_name(step_id, context=context))
        if not pas_obj:
            return False
        return pas_obj.browse(
            cursor, uid,
            sw.get_pas_id(step_id, context=context), context=context
        )

    def get_destinatari_actual(self, cursor, uid, sw_id, step_id=False,
                               context=None):
        """Retorna el id del receptor del pas actual o el pas escollit
        (step_id)"""
        if not context:
            context = {}
        sw_obj = self.pool.get('giscegas.atr')
        step_obj = self.pool.get('giscegas.atr.step')
        sw = sw_obj.browse(cursor, uid, sw_id)
        if not step_id:
            if not sw.step_id:
                raise osv.except_osv('Error', _(u"El caso no tiene ningún paso"))
            step_id = sw.step_id.id

        step_model = step_obj.get_step_model(cursor, uid, step_id)
        pas_obj = self.pool.get(step_model)
        pas_id = pas_obj.search(cursor, uid, [('sw_id', '=', sw.id)])
        pas = pas_obj.browse(cursor, uid, pas_id[0])

        return pas.receptor_id.id

    def get_passos_pendents_enviament(self, cursor, uid, sw_id, context=None):
        step_ids = []
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        proces_obj = self.pool.get('giscegas.atr.proces')
        step_info_obj = self.pool.get('giscegas.atr.step.info')

        hids = header_obj.search(
            cursor, uid, [('sw_id', '=', sw_id), ('enviament_pendent', '=', True)]
        )

        sw_br = sw_obj.browse(cursor, uid, sw_id)
        proces_name = sw_br.proces_id.name
        whereiam = sw_br.whereiam
        emisorsteps = proces_obj.get_emisor_steps(
            cursor, uid, proces_name, whereiam
        )
        if not len(emisorsteps):
            return step_ids
        q = OOQuery(step_info_obj, cursor, uid)
        sql = q.select(['pas_id', 'step_id', 'step_id.name']).where([
            ('step_id.name', 'in', emisorsteps),
            ('sw_id', '=', sw_id)
        ])
        cursor.execute(*sql)
        # mirar per cada header amb quins passos del procés encaixa
        # per obtenir el step_id
        for info in cursor.dictfetchall():
            step_model = info.get('pas_id', ",").split(',')[0]
            if step_model in [""]:
                continue
            mdl = self.pool.get(step_model)
            for hid in hids:
                encaixa = mdl.search(cursor, uid, [('header_id', '=', hid)])
                if encaixa and info['step_id.name'] in emisorsteps:
                    step_ids.append(info['step_id'])
        return step_ids

    def get_passos_enviables(self, cursor, uid, sw_id, context=None):
        step_ids = []
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        proces_obj = self.pool.get('giscegas.atr.proces')
        step_info_obj = self.pool.get('giscegas.atr.step.info')

        hids = header_obj.search(
            cursor, uid, [('sw_id', '=', sw_id)]
        )

        sw_br = sw_obj.browse(cursor, uid, sw_id)
        proces_name = sw_br.proces_id.name
        whereiam = sw_br.whereiam
        emisorsteps = proces_obj.get_emisor_steps(
            cursor, uid, proces_name, whereiam
        )
        if not len(emisorsteps):
            return step_ids
        q = OOQuery(step_info_obj, cursor, uid)
        sql = q.select(['pas_id', 'step_id', 'step_id.name']).where([
            ('step_id.name', 'in', emisorsteps),
            ('sw_id', '=', sw_id)
        ])
        cursor.execute(*sql)
        # mirar per cada header amb quins passos del procés encaixa
        # per obtenir el step_id
        for info in cursor.dictfetchall():
            step_model = info.get('pas_id', ",").split(',')[0]
            if step_model in [""]:
                continue
            mdl = self.pool.get(step_model)
            for hid in hids:
                encaixa = mdl.search(cursor, uid, [('header_id', '=', hid)])
                if encaixa and info['step_id.name'] in emisorsteps:
                    step_ids.append(info['step_id'])
        return step_ids

    def _get_crm_base_ids(self, cursor, uid, ids, context=None):
        """Definim una funció on li passarem els ids del model i ens
           retornarà els ids dels crm rel·lacionats
        """
        return [a['case_id'][0] for a in self.read(cursor, uid, ids,
                                                      ['case_id'], context)]

    def _get_conversation_base_ids(self, cursor, uid, ids, context=None):
        """Definim una funció on li passarem els ids del model i ens
           retornarà els ids dels conversations rel·lacionats
        """
        return [a['conversation_id'][0]
                    for a in self.read(cursor, uid, ids,
                                       ['conversation_id'], context)]

    def _get_crm_to_sw(self, cursor, uid, ids, context=None):
        """Retorna un diccionari en que les claus són els ids de crm i els
        valors una tupla d'ids de switching, conversation_id i pas."""

        res = {}
        for a in self.read(cursor, uid, ids, ['case_id', 'conversation_id',
                                              'pas_comm'], context):
            res.update({a['case_id'][0]: (a['id'], a['conversation_id'][0],
                                          a['pas_comm'])})
        return res

    def _get_proces_model(self, cr, uid, ids, context=None):
        '''retorna el model de l'objecte corresponent al tipus de proces'''
        if not context:
            context = {}
        #If create proces in model, return value for this key in context
        #The value of the key will be the code of the proces
        if context.get('proces', False):
            proces = context.get('proces', False)
            model_name = 'giscegas.atr.%s' % proces.lower()
            model_obj = self.pool.get(model_name)
            return model_obj
        res = {}
        for sw in self.browse(cr, uid, ids, context=context):
            model_name = 'giscegas.atr.%s' % sw.proces.lower()
            model_obj = self.pool.get(model_name)
            res[sw.id] = model_obj
        return res

    def case_close(self, cr, uid, ids, *args):
        '''close the case'''
        crm_obj = self.pool.get('crm.case')

        context = args and args[0] or {}
        crm_ids = self._get_crm_base_ids(cr, uid, ids)
        return crm_obj.case_close(cr, uid, crm_ids, *args)

    def case_pending(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = self._get_crm_base_ids(cr, uid, ids)
        return crm_obj.case_pending(cr, uid, crm_ids, *args)

    def case_log(self, cr, uid, ids, context=None, email=False, *args):
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        crm_obj = self.pool.get('crm.case')
        crm_ids = self._get_crm_base_ids(cr, uid, ids)
        return crm_obj.case_log(cr, uid, crm_ids, context=context,
                                email=email, *args)

    def case_reset(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = self._get_crm_base_ids(cr, uid, ids)
        return crm_obj.case_reset(cr, uid, crm_ids, *args)

    def case_open(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')

        context = args and args[0] or {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for sw in self.browse(cr, uid, ids, context=context):
            #If force_open in context, only open the case
            if context.get('force_open', False):
                sw.case_id.case_open()
                continue
            if sw.state in ['draft', 'pending']:
                sw.case_id.case_open()
        return True

    def localitzar_adjunt(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        if isinstance(ids, list):
            ids = ids[0]
        sw = self.browse(cr, uid, ids, context=context)
        attach_obj = self.pool.get('ir.attachment')
        search_params = [('res_id', '=', ids),
                         ('res_model', '=', 'giscegas.atr')]
        att_ids = attach_obj.search(cr, uid, search_params)
        #Referenciem la classe que correspon al proces
        #per fer el parse de l'XML
        xml_class = globals()[sw.proces_id.name]
        val = None
        for att_id in att_ids:
            att_data = attach_obj.read(cr, uid, att_id, ['datas'])
            xml = xml_class(base64.decodestring(att_data['datas']), sw.proces)
            xml.parse_xml()
            if xml.get_pas_xml().lower() == sw.pas and \
                        xml.codi_sollicitud == sw.codi_sollicitud:
                val = att_id
                break
        return val

    def poweremail_write_callback(self, cursor, uid, ids, vals, context=None):
        logger = logging.getLogger(
            'openerp.{0}.poweremail_write_callback'.format(__name__)
        )
        logger.info(
            'PE callback for mark as send ATR cases {0} and from user {1}'.format(
                ids, uid)
        )
        if context is None:
            context = {}
        meta = context.get('meta')
        attach_obj = self.pool.get('ir.attachment')
        mail_obj = self.pool.get('poweremail.mailbox')
        for sw_id in ids:
            if not meta:
                meta = {}
            model = meta[sw_id].get('model', False)
            pas_id = meta[sw_id].get('pas_id', False)
            store_attachment = meta[sw_id].get('store_attachment', True)
            mark_step_send = meta[sw_id].get('mark_step_send', True)
            if 'folder' in vals and 'date_mail' in vals:
                if vals.get('folder', False) == 'sent':
                    # marquem com a enviat el pas
                    if mark_step_send:
                        pas_obj = self.pool.get(model)
                        pas = pas_obj.browse(cursor, uid, pas_id)
                        pas.header_id.set_enviament_pendent(False)
                    if not store_attachment:
                        mail_id = context.get('pe_callback_origin_ids')
                        if mail_id:
                            mail_id = mail_id[sw_id]
                            attch_ids = mail_obj.read(
                                cursor, uid, mail_id, ['pem_attachments_ids']
                            )
                            attach_obj.unlink(
                                cursor, uid, attch_ids['pem_attachments_ids'])
                            vals.update({'pem_attachments_ids': []})
                else:
                    Exception(
                        "Error",
                        _(u"El mensage se ha creado correctamente pero no se "
                          u"ha podido enviar. Revise los errores en la pestaña "
                          u"de Comunicaciones")
                    )

        return True

    def case_log_reply(self, cursor, uid, ids, context=None,
                       email=False, *args, **kwargs):
        if not context:
            context = {}

        acc_obj = self.pool.get('poweremail.core_accounts')
        attach_obj = self.pool.get('ir.attachment')
        mail_obj = self.pool.get('poweremail.mailbox')
        crm_obj = self.pool.get('crm.case')
        crm_ids = self._get_crm_base_ids(cursor, uid, ids)
        email_cc = kwargs.get('email_cc', False)
        email_cco = kwargs.get('email_cco', False)
        #Check for some values
        for sw in self.browse(cursor, uid, ids, context=context):
            if not sw.email_from:
                raise osv.except_osv(_('Error!'),
                        _(u"You must put a Partner eMail to use this action!"))
            if not sw.user_id:
                raise osv.except_osv(_('Error!'),
                        _(u"You must define a responsible user "
                          u"for this case in order to use this action!"))
            if not sw.description:
                raise osv.except_osv(_('Error!'),
                        _(u"Can not send mail with empty body,you "
                          u"should have description in the body"))
            #Historize send action
            crm_obj._history(cursor, uid, [sw.case_id], _('Send'),
                             history=True, email=False)
            #Send using poweremail
            crm_vals = {'description': False,
                        'som': False,
                        'canal_id': False,
                       }
            sw.write(crm_vals)
            if email:
                emails = email.split(',')
            else:
                emails = [sw.email_from] + (sw.email_cc or '').split(',')
            emails = filter(None, emails)
            body = sw.description or ''
            if sw.user_id.signature:
                body += '\n\n%s' % (sw.user_id.signature)
            xml = self.exportar_xml(cursor, uid, sw.id,
                                    step_id=kwargs.get('step_id', False),
                                    context=context)
            vals = {
                'name': xml[0],
                'datas': base64.b64encode(xml[1]),
                'res_model': 'giscegas.atr',
                'res_id': sw.id
            }
            att = attach_obj.create(cursor, uid, vals, context)
            if context.get('from_account', False):
                acc_id = context.get('from_account', False)
            else:
                acc_id = acc_obj.search(cursor, uid, [('user', '=', uid),
                            ('state', '=', 'approved')], context=context)
                if not acc_id:
                    err = _(u"ERROR: No existe cuenta de correo de envio")
                    raise orm.except_orm('Error', err)
                if isinstance(acc_id, (list, tuple)):
                    acc_id = acc_id[0]
            emailfrom = acc_obj.read(cursor, uid,
                                     acc_id, ['email_id'])['email_id']
            vals = {
                'conversation_id': sw.conversation_id.id,
                'pem_from': emailfrom,
                'pem_to': ','.join(emails),
                'pem_subject': '[' + str(sw.id) + '] ' + sw.name,
                'pem_body_text': crm_obj.format_body(body),
                'pem_attachments_ids': [(4, att)],
                'pem_account_id': acc_id,
                'pem_cc': email_cc,
                'pem_bcc': email_cco,

            }
            # get step info and save to
            pas_obj = self.pool.get(sw.step_id.get_step_model(cursor, uid))
            pas_id = pas_obj.search(cursor, uid, [('sw_id', '=', sw.id)])
            meta_ctx = context.get('meta', {})
            meta_ctx.update({
                'model': sw.step_id.get_step_model(cursor, uid),
                'pas_id': pas_id[0]
            })
            context.update({
                'meta': meta_ctx,
                'src_rec_id': sw.id,
                'src_model': 'giscegas.atr'
            })
            mail_id = mail_obj.create(cursor, uid, vals, context)
            mail_obj.send_this_mail(cursor, uid, [mail_id], context)

        return True

    def send_email(
            self, cursor, uid, email_values, email_from=False, context=None):
        if not context:
            context = {}
        acc_obj = self.pool.get('poweremail.core_accounts')
        mail_obj = self.pool.get('poweremail.mailbox')
        if not email_values.get('pem_account_id', False):
            if context.get('from_account', False):
                acc_id = context.get('from_account', False)
            else:
                search_values = [('user', '=', uid), ('state', '=', 'approved')]
                if email_from:
                    search_values.append(('email_id', '=', email_from))
                acc_id = acc_obj.search(
                    cursor, uid, search_values,context=context)
                if not acc_id:
                    err = _(u"ERROR: No existe cuenta de correo de envio")
                    raise orm.except_orm('Error', err)
                if isinstance(acc_id, (list, tuple)):
                    acc_id = acc_id[0]
            email_values.update({'pem_account_id': acc_id})
        else:
            acc_id = email_values.get('pem_account_id', False)

        emailfrom = acc_obj.read(
            cursor, uid, acc_id, ['email_id'])['email_id']
        email_values.update({'pem_from': emailfrom})
        # Using new cursor to persist mail before use
        db = pooler.get_db_only(cursor.dbname)
        pbcr = db.cursor()
        mail_id = mail_obj.create(pbcr, uid, email_values, context)
        pbcr.commit()
        pbcr.close()
        mail_obj.send_this_mail(cursor, uid, [mail_id], context)

    def check_case_email(self, cursor, uid, switching_case, context=None):
        if not context:
            context = {}
        if isinstance(switching_case, int):
            sw = self.pool.get('giscegas.atr').browse(
                cursor, uid, switching_case)
        else:
            sw = switching_case
        if not sw.email_from:
            raise osv.except_osv(_('Error!'),
                    _(u"You must put a Partner eMail to use this action!"))
        if not sw.user_id:
            raise osv.except_osv(_('Error!'),
                    _(u"You must define a responsible user "
                      u"for this case in order to use this action!"))
        if not sw.description:
            raise osv.except_osv(_('Error!'),
                    _(u"Can not send mail with empty body,you "
                      u"should have description in the body"))

    def send_responsible_and_historize(self, cursor, uid, ids, context=None):
        logger = logging.getLogger(
            'openerp.{0}.send_responsible_and_historize'.format(__name__)
        )
        logger.info(
            'Send responsible message for cases {0} and from user {1}'.format(
                ids, uid)
        )
        conf_obj = self.pool.get('res.config')
        crm_obj = self.pool.get('crm.case')
        emails_cc = conf_obj.get(
            cursor, uid, 'sw_responsible_email_cc', None)
        email_from_user = conf_obj.get(
            cursor, uid, 'sw_responsible_email_from_user', None)
        if not email_from_user:
            user_obj = self.pool.get('res.users')
            user = user_obj.browse(cursor, uid, uid)
            email_from_user = user.address_id.email

        for sw in self.browse(cursor, uid, ids, context=context):
            self.check_case_email(cursor, uid, sw)
            if not sw.user_id:
                raise osv.except_osv(_('Error!'),
                        _(u"Es necessario definir un responsabñe para este "
                          u"caso para poder realizar esta acción"))
            email = sw.user_id.address_id.email
            if not email:
                raise osv.except_osv(
                    _('Error!'),
                    _(u"Es necessario que el responsable tenga un email para "
                      u"usar esta acción")
                )
            if email:
                context['meta'] = {'mark_step_send': False,
                                   'store_attachment': False}
                # get step info and save to
                pas_obj = self.pool.get(sw.step_id.get_step_model(cursor, uid))
                pas_id = pas_obj.search(cursor, uid, [('sw_id', '=', sw.id)])
                meta_ctx = {
                    'model': sw.step_id.get_step_model(cursor, uid),
                    'pas_id': pas_id[0]
                }

                context.update({
                    'meta': meta_ctx,
                    'src_rec_id': sw.id,
                    'src_model': 'giscegas.atr'
                })
                body = sw.description or ''
                if sw.user_id.signature:
                    body += '\n\n{0}'.format(sw.user_id.signature)
                subject = _(u'[{0}] {1} - Contrato: {2}').format(
                    sw.id, sw.name, sw.cups_polissa_id.name)
                vals = {
                    'conversation_id': sw.conversation_id.id,
                    'pem_to': email,
                    'pem_subject': subject,
                    'pem_body_text': crm_obj.format_body(body)
                }
                if emails_cc:
                    vals.update({'pem_cc': emails_cc})
                self.send_email(
                    cursor, uid, vals, email_from_user, context=context)
                #Historize send action
                crm_obj._history(cursor, uid, [sw.case_id], _('Send'),
                             history=True, email=False)
                sw.write({'description': False})

    def partner_map_id(self, cr, uid, cups_id, partner_id, context=None):
        """ Per cridar-lo des de OOOP"""
        if not cups_id:
            return False
        cups = self.pool.get('giscegas.cups.ps').browse(cr, uid, cups_id,
                                                         context=context)
        return self.partner_map(cr, uid, cups, partner_id, context=context)

    def partner_map(self, cr, uid, cups, partner_id, context=None):
        if not context:
            context = {}
        if not partner_id:
            return False
        # Hem de fer el get_dso a l'inversa per saber quin codi sifco posar ja que el contracte sempre estara amb la
        # distri principal.
        participant_obj = self.pool.get("giscemisc.participant")
        participant = participant_obj.get_from_partner(cr, uid, partner_id)
        if not participant.codi_sifco:
            return partner_id
        sifco = participant.codi_sifco
        if sifco in SIFCO_CODES_TRANSLATION.values():
            nou_sifco = cups.name[2:6]
            alt_sifco = cups.name[2:8]
            if nou_sifco in SIFCO_CODES_TRANSLATION.keys() or alt_sifco in EXTENDED_SIFCO_CODES_TRANSLATION.keys():
                try:
                    nou_participant = participant_obj.get_from_sifco(cr, uid, nou_sifco)
                    return nou_participant.partner_id.id
                except Exception, e:
                    return partner_id
        return partner_id

    def partner_parent(self, cr, uid, partner_id, context=None):
        '''En el cas d'empreses amb diferents codis, pero amb
        una mateixa matriu (Endesa), afegim el parent per comparar'''

        if not partner_id:
            return []
        partner_obj = self.pool.get('res.partner')
        partner = partner_obj.browse(cr, uid, partner_id)
        if partner.parent_id:
            return [partner.id, partner.parent_id.id]
        else:
            return [partner_id]

    def onchange_partner_id(self, cr, uid, ids, partner_id, email=False):
        address_obj = self.pool.get('res.partner.address')
        partner_obj = self.pool.get('res.partner')
        if not partner_id:
            data = {}
            data_dom = {}
            return {'value': data, 'domain': data_dom}
        addr = self.pool.get('res.partner').get_partner_address(cr, uid,
                                                                partner_id)
        data_val = {'partner_address_id': addr}
        if addr and not email:
            data_val['email_from'] = address_obj.browse(cr, uid, addr).email

        participant_obj = self.pool.get("giscemisc.participant")
        participant = participant_obj.get_from_partner(cr, uid, partner_id)
        if not participant.codi_sifco:
            raise osv.except_osv(_(u"Error"),
                                 _(u"Se debe configurar el código SIFCO de la "
                                   u"empresa destino"))
        data_dom = {'cups_polissa_id': "[('distribuidora', '=', partner_id)]",
                    'cups_id': "[('distribuidora_id', '=', partner_id)]"}
        return {'value': data_val, 'domain': data_dom}

    def onchange_categ_id(self, cr, uid, ids, categ, context=None):

        categ_obj = self.pool.get('crm.case.categ')
        if not context:
            context = {}
        if not categ:
            return {'value': {}}
        cat = categ_obj.browse(cr, uid, categ, context).probability
        return {'value': {'probability': cat}}

    def onchange_partner_address_id(self, cr, uid, ids,
                                    address_id, email=False):

        address_obj = self.pool.get('res.partner.address')

        if not address_id:
            return {'value': {}}
        data = {}
        if not email:
            data['email_from'] = address_obj.browse(cr, uid, address_id).email

        return {'value': data}

    def onchange_cups_id(self, cursor, uid, ids, cups_id,
                         partner_id, proces_id=None, context=None):

        cups_obj = self.pool.get('giscegas.cups.ps')
        polissa_obj = self.pool.get('giscegas.polissa')
        proces_obj = self.pool.get('giscegas.atr.proces')

        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not context:
            context = {}
        if not cups_id:
            res['value'] = {'cups_id': False,
                            'cups_input': '',
                            'cups_polissa_id': False,
                            'ref_contracte': False}
            return res
        if proces_id:
            proces_data = proces_obj.read(cursor, uid, proces_id, ['name'])
            if proces_data:
                context.update({'proces_name': proces_data['name']})
        try:
            cups_name = cups_obj.read(cursor, uid, cups_id, ['name'])['name']
            self.check_si_processant_cups(cursor, uid, cups_id, cups_name,
                                          ids=ids, context=context)
        except osv.except_osv as e:
            res['warning'] = {'title': e.name, 'message': e.value}
            res['value'] = {'cups_id': False,
                            'cups_input': '',
                            'cups_polissa_id': False,
                            'ref_contracte': False}
            return res

        cups = cups_obj.browse(cursor, uid, cups_id)
        polissa_id = self.trobar_polissa_w_cups(cursor, uid, cups_id,
                                                context=context)
        data = {'cups_input': cups.name}
        if polissa_id:
            data.update({'cups_polissa_id': polissa_id})
            ret_values = self.onchange_polissa_id(cursor, uid, ids,
                                                 polissa_id,
                                                 partner_id)
            if ret_values.get('warning', {}):
                res['warning'] = ret_values['warning']
                return res
            if ret_values.get('value', {}):
                data.update(ret_values['value'])
            #If cups_id is returned, remove from data
            if 'cups_id' in data:
                del data['cups_id']
            if not partner_id:
                partner_id = 'partner_id' in data and data['partner_id']
        where = self.whereiam(cursor, uid, cups_id, context=context)
        if not partner_id:
            if where == 'comer':
                partner_id = cups.distribuidora_id.id
            elif where == 'distri' and polissa_id:
                polissa = polissa_obj.browse(cursor, uid, polissa_id)
                partner_id = polissa.comercialitzadora.id
            if partner_id:
                data.update({'partner_id': partner_id})
                data.update(self.onchange_partner_id(cursor, uid, ids,
                                                     partner_id)['value'])
        partner_ids = self.partner_parent(cursor, uid, partner_id)
        #If in comer, check that cups belongs to partner_id in case
        if where == 'comer' and cups.distribuidora_id.id not in partner_ids:
            _msg = _(u"El CUPS no pertenece a la empresa distribuidora "
                     u"escogida")
            res['warning'] = {'title': _(u"Avís"), 'message': _msg}
            res['value'] = {'cups_id': False, 'cups_input': ''}
            return res

        res.update({'value': data})
        return res

    def onchange_polissa_id(self, cursor, uid, ids, polissa_id, partner_id,
                            proces_id=None, context=None):
        if context is None:
            context = {}
        polissa_obj = self.pool.get('giscegas.polissa')
        proces_obj = self.pool.get('giscegas.atr.proces')

        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not polissa_id:
            return res
        if proces_id:
            proces_data = proces_obj.read(cursor, uid, proces_id, ['name'])
            if proces_data:
                context.update({'proces_name': proces_data['name']})

        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        where = self.whereiam(cursor, uid, polissa.cups.id, context=context)
        try:
            if where == 'comer':
                self.check_partner_nif(cursor, uid, polissa.titular.id)
            self.check_si_processant_cups(cursor, uid, polissa.cups.id,
                                          polissa.cups.name, ids=ids,
                                          context=context)
        except osv.except_osv as e:
            res['warning'] = {'title': e.name, 'message': e.value}
            res['value'] = {'cups_polissa_id': False}
            return res
        data = {'cups_id': polissa.cups.id}
        if where == 'comer':
            data.update({'ref_contracte': polissa.ref_dist or ''})
        elif where == 'distri':
            data.update({'ref_contracte': polissa.name})

        if not partner_id:
            if where == 'comer':
                partner_id = polissa.distribuidora.id
            elif where == 'distri':
                partner_id = polissa.comercialitzadora.id
            data.update({'partner_id': partner_id})
            data.update(self.onchange_partner_id(cursor, uid, ids,
                                                 partner_id)['value'])
        partner_ids = self.partner_parent(cursor, uid, partner_id)
        if where == 'comer' and polissa.distribuidora.id not in partner_ids:
            _msg = _(u"El CUPS  del contrato no pertenece a la empresa "
                     u"distribuidora escogida")
            res['warning'] = {'title': _(u"Aviso"), 'message': _msg}
            res['value'] = {'cups_polissa_id': False,
                            'cups_id': False,
                            'ref_contracte': False}
            return res
        if where == 'distri' and polissa.comercialitzadora.id != partner_id:
            _msg = _(u"El contrato no pertenece a la empresa comercializadora "
                     u"escogida")
            res['warning'] = {'title': _(u"Aviso"), 'message': _msg}
            res['value'] = {'cups_polissa_id': False,
                            'cups_id': False,
                            'ref_contracte': False}
            return res

        res.update({'value': data})
        return res

    def generar_nom_xml(self, cr, uid, ids, emisor, receptor,
                        pas=False, context=None):
        """Retorna el nom per l'xml"""
        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        sw = self.browse(cr, uid, ids)
        if not pas:
            pas = sw.step_id.name
        date_sw = datetime.strptime(sw.data_sollicitud,
                                    '%Y-%m-%d').strftime('%Y%m')
        ctx = context.copy()
        ctx.update({'active_test': False})
        participant_obj = self.pool.get("giscemisc.participant")
        participant_id = participant_obj.get_id_from_sifco(cr, uid, receptor)
        if participant_id:
            emi = participant_obj.browse(cr, uid, participant_id)
            receptor = emi.comercial or emi.codi_sifco

        return u'%s_%s_%s_%s_%s_%s.xml' % (sw.proces_id.name,
                                           pas,
                                           emisor,
                                           receptor,
                                           sw.codi_sollicitud,
                                           sw.cups_id.name)

    def trobar_polissa_w_cups(self, cr, uid, cups_id, context=None):
        """Retorna la pòlissa activa associada al cups"""

        if not context:
            context = {}
        if not cups_id:
            return None
        polissa_obj = self.pool.get('giscegas.polissa')
        search_params = [
            ('cups', '=', cups_id),
            ('state', 'not in', CONTRACT_IGNORED_STATES + ['baixa'])
        ]
        polissa_id = polissa_obj.search(cr, uid, search_params,
                                        context=context)
        #If not found, search for polissa in esborrany or validar state
        if not polissa_id:
            search_params = [('cups', '=', cups_id),
                             ('state', 'in', CONTRACT_IGNORED_STATES)]
            polissa_id = polissa_obj.search(cr, uid, search_params,
                                            context=context)
            if not polissa_id and context.get('search_non_active', False):
                search_params = [('cups', '=', cups_id),
                                 ('state', 'in', ['baixa'])]
                context['active_test'] = False
                polissa_id = polissa_obj.search(cr, uid, search_params,
                                                context=context)
        if polissa_id:
            return polissa_id[0]
        else:
            return None

    def trobar_comer_sortint_w_polissa(self, cursor, uid, contract_id,
                                       context=None):
        """Returns current Retailer partner from contract"""
        if not context:
            context = {}

        polissa_obj = self.pool.get('giscegas.polissa')

        pol_fields = ['comercialitzadora', 'cups']
        pol_vals = polissa_obj.read(cursor, uid, contract_id, pol_fields,
                                    context=context)
        if not pol_vals:
            return False
        cups_id = pol_vals.get('cups', (False, ))[0]
        if not cups_id:
            return False
        where = self.whereiam(cursor, uid, cups_id, context)
        if where != 'distri':
            return False

        comercialitzadora = pol_vals.get('comercialitzadora', (False, ))
        if isinstance(comercialitzadora, (list, tuple)):
            return comercialitzadora[0]
        else:
            return comercialitzadora

    def vat_enterprise(self, cursor, uid, sw_id, context=None):
        '''checks if we have an enterprise or
        a person depending on vat value'''
        vat = self.browse(cursor, uid, sw_id[0]).cups_polissa_id.titular.vat
        return self.pool.get('res.partner').vat_es_empresa(cursor, uid, vat)

    def sw_vat_type(self, cursor, uid, vat, distri_ref, context=None):
        """Some DSO's needs diferent vat type (TipoDocumento) depending on
           DSO's according to CNMC TABLA_6 codification.
           STATIC FUNCTION
           Not for multiple id's"""

        partner_obj = self.pool.get('res.partner')
        if vat[:2] == 'PS':
            return '03'

        is_enterprise = partner_obj.vat_es_empresa(cursor, uid, vat)

        if not is_enterprise:
            is_nie = partner_obj.vat_es_nie(cursor, uid, vat)
            if is_nie:
                res = '04'
            else:
                res = '01'
        else:
            res = '01'
        return res

    def get_vat_info(self, cursor, uid, vat, distri_ref=None):
        """
        :param vat:
        :param distri_ref:
        :return: dict {
            'vat': Vat without prefix,
            'document_type': As defined in CNMC doc
            'is_enterprise': Wether is a bussines (True)
                             or a personal VAT (False)
        """

        document_type = self.sw_vat_type(
            cursor, uid, vat, distri_ref
        )
        partner_obj = self.pool.get('res.partner')
        is_enterprise = partner_obj.vat_es_empresa(cursor, uid, vat)
        if vat[:2] in ['ES', 'PS']:
            vat = vat[2:]

        return {
            'vat': vat,
            'document_type': document_type,
            'is_enterprise': is_enterprise,
        }

    def check_partner_nif(self, cr, uid, ids, context=None):
        '''checks if partner in ids has a vat number'''
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        partner_obj = self.pool.get('res.partner')
        partner = partner_obj.browse(cr, uid, ids)
        if not partner.vat:
            err = _(u'El partner %s especificado no tiene nif') % partner.name
            raise osv.except_osv('Error', err)

        return True

    def check_si_processant_cups(self, cursor, uid, cups_id, cups_name,
                                 ids=None, context=None):
        """Comprova si ja hi ha un cas en estat obert amb el mateix cups,
           excepte dels processos que poden ser simultanis (D1, W1 i R1)"

        """
        if not context:
            context = {}
        if not ids:
            ids = []
        process_exceptions = ['48', '03', '46']
        proces_name = context.get('proces_name')
        if proces_name in process_exceptions:
            return True

        if cups_id:
            cups_obj = self.pool.get('giscegas.cups.ps')
            cups_name = cups_obj.read(cursor, uid, cups_id, ['name'])['name']

        search_params = [('cups_input', '=', cups_name),
                         ('state', 'not in', ('done', 'cancel')),
                         ('proces_id.name', 'not in', process_exceptions),
                         ('id', 'not in', ids)]
        sw_ids = self.search(cursor, uid, search_params)
        if sw_ids:
            err = _(u"Ya existe un caso relacionado con el mismo CUPS "
                    u"'%s'") % cups_name
            raise osv.except_osv('Error', err)
        return True

    def check_destinatari_is_cur(self, cursor, uid, sw_id, pas=False, context=None):
        if not context:
            context = {}
        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        if not pas:
            sw_obj = self.pool.get('giscegas.atr')
            sw = sw_obj.browse(cursor, uid, sw_id)
            if not sw.step_id:
                raise osv.except_osv('Error', _(u"El caso no tiene paso"))
            pas = sw.get_pas()

        return pas.receptor_id.cor

    def get_casos_actiu_cups(self, cursor, uid, cups_id):
        """Retorna els ids dels casos amb la cups_id,
           excepte dels processos que poden ser simultanis (D1, W1 i R1)"""
        process_exceptions = ['D1', 'W1', 'R1']
        if cups_id:
            cups_obj = self.pool.get('giscegas.cups.ps')
            cups_name = cups_obj.read(cursor, uid, cups_id, ['name'])['name']

        search_params = [('cups_input', '=', cups_name),
                         ('state', 'not in', ('done', 'cancel')),
                         ('proces_id.name', 'not in', process_exceptions)]
        return self.search(cursor, uid, search_params)

    def generar_descripcio(self, cursor, uid, filename, context=None):
        if not context:
            context = {}
        desc = _(u"Fitxer importat manualment.\n"
                 u"Nom del fitxer: %s")
        return (desc % filename)

    def get_partner_address(self, cursor, uid, partner_id):
        partner_obj = self.pool.get('res.partner')
        address_obj = self.pool.get('res.partner.address')
        address = partner_obj.address_get(cursor, uid,
                                          [partner_id], ['contact'])
        if address['contact']:
            email = address_obj.read(cursor, uid,
                                     address['contact'], ['email'])
            return (address['contact'], email['email'])
        else:
            return (address['contact'], False)

    def anullar_sollicitud(self, cursor, uid, ids, context=None):
        """Anul·la la sol·licitud actual i accedeix al pas corresponent
           per notificar-ho a la distribuidora.
        """
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for sw in self.browse(cursor, uid, ids, context=context):
            if sw.state == 'draft':
                return False
            model_obj = self._get_proces_model(cursor, uid, [sw.id],
                                               context=context)[sw.id]
            model_obj.anullar_sollicitud(self, cursor, uid,
                                         sw, context=None)
        return True

    def find_sollicitud(self, cursor, uid, partner_id, xml, context=None):
        """Retorna el cas vinculat amb el codi de sol·licitud en cas
           d'existir. El codi ha de ser unic.
        """
        if not context:
            context = {}

        context.update({'proces': xml.tipus})
        participant_obj = self.pool.get('giscemisc.participant')
        emisor = participant_obj.get_from_sifco(cursor, uid, xml.get_codi_emisor)
        destinatari = participant_obj.get_from_sifco(cursor, uid, xml.get_codi_destinatari)

        search_params = [('codi_sollicitud', '=', xml.codi_sollicitud),
                         ('proces_id.name', '=', xml.tipus),
                         ('case_id.partner_id', 'in',
                          (emisor.partner_id.id, destinatari.partner_id.id))]

        sw_ids = self.search(cursor, uid, search_params, context=context)
        return sw_ids and sw_ids[0] or False

    def check_cas_coincident(self, cursor, uid, sw_id, xml, context=None):
        """Evalua si l'estat del cas correspon a l'xml
        """
        if not context:
            context = {}
        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        sw = self.browse(cursor, uid, sw_id, context)
        if (sw.codi_sollicitud == xml.codi_sollicitud
            and sw.data_sollicitud == xml.data_sollicitud.split(' ')[0]
            and sw.proces_id.name.upper() == xml.get_tipus_xml()
            and sw.cups_id.name == xml.cups
            and sw.step_id.name == xml.get_pas_xml().lower()):
            raise osv.except_osv("Error", _(u"Fichero processado anteriormente."))

    def get_header_vals(self, cursor, uid, sw_id, context=None):

        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        sw = self.browse(cursor, uid, sw_id)
        vals = {
            'processcode': sw.proces_id.name.upper(),
            'dispatchingcode': 'GML',
        }
        return vals

    def create_step(self, cursor, uid, sw_id, pas, pas_id, context=None):

        step_obj = self.pool.get('giscegas.atr.step')
        info_obj = self.pool.get('giscegas.atr.step.info')

        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        sw = self.browse(cursor, uid, sw_id)
        pas_model = step_obj.get_step_model(cursor, uid, [],
                                            name=pas,
                                            proces=sw.proces_id.name,
                                            context=context)

        vals = {'sw_id': sw_id,
                'step_id': step_obj.get_step(cursor, uid, pas,
                                             sw.proces_id.name),
                'proces_id': sw.proces_id.id,
                'pas_id': '%s,%s' % (pas_model,
                                     pas_id),
                }

        return info_obj.create(cursor, uid, vals)

    def check_cups(self, cursor, uid, cups_name, process, context=None):
        """Retorna cups_id del CUPS amb cups_name
        """

        cups_obj = self.pool.get('giscegas.cups.ps')
        conf_obj = self.pool.get('res.config')

        if not context:
            context = {}
        # Check len of cups to work with
        used_len = int(conf_obj.get(cursor, uid, 'check_cups_length', '20'))
        if len(cups_name) > used_len:
            cups_name = cups_name[:used_len]
        search_params = [('name', 'like', cups_name)]
        cups_ids = cups_obj.search(cursor, uid, search_params, context=context)
        return cups_ids and cups_ids[0] or None

    def check_partner(self, cursor, uid, partner_ref,
                      type='emisor', context=None):
        '''Check partner ref'''
        user_obj = self.pool.get('res.users')

        participant_obj = self.pool.get("giscemisc.participant")
        participant_id = participant_obj.get_id_from_sifco(cursor, uid, partner_ref)
        if not participant_id:
            err = _("Agente del mercado con referencia '%s' inexistente") % partner_ref
            raise osv.except_osv('Error', err)

        partner_id = participant_obj.get_partner_id(cursor, uid, participant_id)
        if type == 'receptor':
            #Check that partner_ref it is the same than our company ref
            user = user_obj.browse(cursor, uid, uid)
            if partner_id != user.company_id.partner_id.id:
                err = _(u"El destinatario del XML, con referencia '%s' no "
                        u"se corresponde a %s (REF: %s)") % (
                          partner_ref, user.company_id.partner_id.name,
                          user.company_id.partner_id.ref
                      )
                raise osv.except_osv('Error', err)

        return partner_id

    def check_motius_rebuig(self, cursor, uid, sw_id, cas, context=None):
        rebuig_obj = self.pool.get('giscegas.atr.motiu.rebuig')
        rebuig_checker = Rebuig()
        sw = self.browse(cursor, uid, sw_id, context=context)
        res = []
        for rebuig in rebuig_obj.get_all_motius(cursor, uid, cas):
            check = getattr(rebuig_checker, 'check_{0}'.format(rebuig[1]), None)
            if check is not None:
                if not check(self.pool, cursor, uid, sw):
                    res.append(rebuig)
        return res

    def notificar_pas_inesperat(self, cursor, uid, sw_id, xml, context=None):
        """Genera una excepció avisant que el pas del document no es
           l'esperat pel cas"""

        step_obj = self.pool.get('giscegas.atr.step')
        conf_obj = self.pool.get('res.config')
        if not context:
            context = {}

        sw = self.browse(cursor, uid, sw_id)
        if not context.get("rebuig"):
            context['rebuig'] = sw.rebuig
        pas_xml = xml.get_pas_xml().lower()

        validacio = conf_obj.get(cursor, uid, 'atr_gas_step_validation', 'total')

        if pas_xml == sw.step_id.name:
            raise osv.except_osv(
                'Error',
                _(u"Este paso es igual al actual {0}").format(sw.step_id.name)
            )

        if validacio == 'total':
            # already loaded test
            for step in sw.step_ids:
                if pas_xml == step.step_id.name:
                    raise osv.except_osv(
                        'Error',
                        _(u"Este paso {0} ja ha sido cargado en la "
                          u"solicitud {1}").format(pas_xml, sw.codi_sollicitud)
                    )

            next_steps = step_obj.get_next_steps(
                cursor, uid, sw.step_id.name, sw.proces_id.name,
                sw.whereiam, context=context
            )
            if pas_xml not in next_steps:
                err = _(u"El XML con código de paso '%s' no pertenece a la "
                        u"lista de passos esperados para la solicitud %s.") % \
                   (xml.get_pas_xml().lower(), sw.codi_sollicitud)
                raise osv.except_osv(_(u"Atención"), err)

        return True

    def update_from_xml(self, cursor, uid, sw_id, filename, partner_id,
                        cups_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        step_obj = self.pool.get('giscegas.atr.step')
        proces_obj = self.pool.get('giscegas.atr.proces')
        pol_obj = self.pool.get('giscegas.polissa')
        if not context:
            context = {}

        addresses = self.get_partner_address(cursor, uid, partner_id)
        desc = self.generar_descripcio(cursor, uid, filename, context)
        vals = {}
        if not sw_id:
            ctx2 = context.copy()
            contract_id = self.trobar_polissa_w_cups(
                cursor, uid, cups_id, context=ctx2
            )
            cont_ref = '0000'
            if contract_id:
                out_retailer = self.trobar_comer_sortint_w_polissa(
                    cursor, uid, contract_id, context=context
                )
                contract_name = pol_obj.read(cursor, uid, contract_id, ['name'])
                if contract_name['name']:
                    cont_ref = contract_name['name']
            else:
                out_retailer = False
            vals.update({
                    'partner_id': partner_id,
                    'partner_address_id': addresses[0],
                    'email_from': addresses[1],
                    'description': desc,
                    'codi_sollicitud': xml.codi_sollicitud,
                    'data_sollicitud': xml.data_sollicitud,
                    'cups_id': cups_id,
                    'cups_input': xml.cups,
                    'cups_polissa_id': contract_id,
                    'ref_contracte': (getattr(xml, 'contrato', False) and
                                      xml.contrato.cod_contrato or cont_ref),
                    'proces_id': proces_obj.get_proces(cursor, uid,
                                                    xml.get_tipus_xml()),
                    'step_id': step_obj.get_step(cursor, uid,
                                             xml.get_pas_xml().lower(),
                                             xml.get_tipus_xml()),
            })
            if out_retailer:
                out_participant = self.pool.get(
                    "giscemisc.participant"
                ).get_id_from_partner(cursor, uid, out_retailer)
                vals.update({'comer_sortint_id': out_participant})

            sw_id = self.create(cursor, uid, vals, context=context)

        pas_model = step_obj.get_step_model(cursor, uid, [],
                                            xml.get_pas_xml().lower(),
                                            xml.get_tipus_xml())
        pas_obj = self.pool.get(pas_model)
        pas_obj.create_from_xml(cursor, uid, sw_id, xml, context=context)

        # update deadline
        proces = pas_model.split(".")[-2].upper()
        step = pas_model.split(".")[-1]
        self.update_deadline(cursor, uid, sw_id, proces=proces, step=step)
        return sw_id

    def adjuntar_xml(self, cursor, uid, sw_id, xml, fname, context=None):
        """Desa l'xml en l'erp"""
        attach_obj = self.pool.get('ir.attachment')
        if not context:
            context = {}

        desc = _(u"Codigo de solicitud: %s, Proceso: %s, Paso: %s") % \
                                 (xml.codi_sollicitud,
                                  xml.tipus,
                                  xml.get_pas_xml().lower())
        vals = {
            'name': fname,
            'datas': base64.b64encode(xml.str_xml),
            'datas_fname': fname,
            'res_model': 'giscegas.atr',
            'res_id': sw_id,
            'description': desc,
        }
        attach_obj.create(cursor, uid, vals, context=context)
        return True

    def generar_rebuig_auto(self, cursor, uid, sw_id, checks, cas, context=None):
        pas_obj = self.pool.get(
            'giscegas.atr.{0}.02'.format(cas.lower())
        )

        pas_id = pas_obj.dummy_create_reb(cursor, uid, sw_id, checks, context)
        header_id = pas_obj.read(cursor, uid, pas_id, ['header_id'])
        header_obj = self.pool.get('giscegas.atr.step.header')
        vals = {'enviament_pendent': True}
        header_obj.write(cursor, uid, header_id['header_id'][0], vals)
        self.create_step(cursor, uid, sw_id, 'a2', pas_id, context=context)
        self.update_deadline(cursor, uid, sw_id, context=context)

    def importar_xml(self, cursor, uid, data, fname, context=None):
        """Importar un nou xml i retorna l'id del cas del crm"""
        if not context:
            context = {}
        cups_obj = self.pool.get('giscegas.cups.ps')
        proces_obj = self.pool.get('giscegas.atr.proces')
        conf_obj = self.pool.get('res.config')
        motiu_obj = self.pool.get('giscegas.atr.motiu.rebuig')
        swlog_obj = self.pool.get('giscegas.atr.log')
        participant_obj = self.pool.get("giscemisc.participant")
        cups_id = False
        partner_id = False
        emisor = ''
        cups_xml = ''
        xml = False
        fname = unicode(fname)
        # Get the file type
        try:
            cr = cursor
            db = pooler.get_db(cursor.dbname)
            cursor = db.cursor()
            zip_name = context.get('zip_name', '')

            general_xml = message_gas.MessageGas(data)
            general_xml.parse_xml(validate=False)
            xml_class = globals()["A1_"+general_xml.tipus]
            # Use the correct class for parsing xml
            xml = xml_class(data)
            xml.parse_xml(validate=False)
            emisor = xml.get_codi_emisor
            desti = xml.get_codi_destinatari
            cups_xml = xml.cups
            # We have set validate to False so by default it will try to
            # validate but if it can't it will work anyway, just setting it's
            # value of valid to false. Therefore, we can always assume that
            # the parsing should work and if it doesn't we need to raise the
            # exception

            user = self.pool.get('res.users').browse(cursor, uid, uid,
                                                     context=context)
            validacio = conf_obj.get(
                cursor, uid, 'atr_gas_step_validation', 'total'
            )
            participant = participant_obj.get_from_partner(cursor, uid, user.company_id.partner_id.id)
            if participant.codi_sifco == emisor and validacio != 'total':
                emisor, desti = desti, emisor
            self.check_partner(cursor, uid, desti, type='receptor',
                               context=context)
            partner_id = self.check_partner(
                cursor, uid, emisor, context=context
            )

            if cups_xml:
                cups_id = self.check_cups(cursor, uid, cups_xml, xml.tipus,
                                          context=context)

            sw_id = self.find_sollicitud(cursor, uid, partner_id, xml,
                                         context=context)

            where = self.whereiam(cursor, uid, cups_id, context)
            init_steps = proces_obj.get_init_steps(cursor, uid,
                                                   xml.get_tipus_xml(),
                                                   where, context=context)
            msg = []

            if sw_id:
                self.check_cas_coincident(
                    cursor, uid, sw_id, xml, context=context
                )
                self.notificar_pas_inesperat(cursor, uid, sw_id, xml,
                                             context=context)
                self.update_from_xml(cursor, uid, sw_id, fname,
                                     partner_id, cups_id,
                                     xml, context=context)
                self.case_open(cursor, uid, sw_id, {'force_open': True,
                                                    'context': context})
                msg.append(_(u"Se ha actualizado el caso con id %d") % sw_id)
            else:
                if xml.get_pas_xml().lower() not in init_steps:
                    err = _(u"No existe ningún caso relacionado con el XML.\n"
                            u"No es possible iniciar una nueva comunicación "
                            u"del tipo %s con código de paso '%s'.\n"
                            u"Código de solicitud '%s'") % (xml.get_tipus_xml(),
                                                             xml.get_pas_xml().lower(),
                                                             xml.codi_sollicitud
                                                            )
                    raise orm.except_orm(_(u"Atención"), err)
                sw_id = self.update_from_xml(cursor, uid, None, fname,
                                             partner_id, cups_id,
                                             xml, context=context)
                msg.append(_(u"Se ha creado el caso con id %d") % sw_id)

            self.adjuntar_xml(cursor, uid, sw_id, xml, fname, context=context)
            self.case_log(cursor, uid, sw_id, context=context)
            msg.insert(0, _(u"Fichero processado correctamente."))

            cas = xml.get_tipus_xml()
            checks = None
            if xml.get_pas_xml() == 'A1':
                checks = self.check_motius_rebuig(
                    cursor, uid, sw_id, cas, context=None
                )
                if checks:
                    self.generar_rebuig_auto(
                        cursor, uid, sw_id, checks, cas, context=None)

                    msg.append(_(u"\nSe ha generado rechazo automáticamente "
                                 u"por los siguientes motivos: "))

                    checks_ids = [check[0] for check in checks]
                    mot_desc = motiu_obj.read(
                        cursor, uid, checks_ids, ['name', 'text']
                    )
                    for motiu in mot_desc:
                        msg.append(
                            u"    {0} - {1}".format(motiu['name'], motiu['text'])
                        )

            if xml.get_pas_xml().lower() in init_steps:
                # Obrir o deixar en Borrador el cas segons variable de
                # configuració
                case_state_open = conf_obj.get(
                    cursor, uid, 'atr_gas_importation_case_state_open', '1'
                )
                try:
                    case_state_open = int(case_state_open)
                except:
                    case_state_open = False
                if case_state_open or checks: # Si es genera rebuig tambe s'obre
                    self.case_open(cursor, uid, [sw_id], {'context': context})

            info = '\n'.join(msg)

            cursor.commit()
            cursor.close()
            cursor = cr

            add_message = self.importar_xml_post_hook(cursor, uid, sw_id, context=context)
            info = "{0} {1}".format(info, add_message)

            params = {'origin': zip_name if zip_name else fname or '',
                      'file': fname,
                      'status': 'correcte',
                      'request_code': xml.codi_sollicitud,
                      'pas': xml.get_pas_xml().lower(),
                      'proces': xml.get_tipus_xml(),
                      'info': info,
                      'emisor_id': partner_id,
                      'cups': cups_id,
                      'cups_text': cups_xml,
                      'tipus': 'import',
                      'codi_emisor': emisor
                      }

            swlog_id = swlog_obj.create(cursor, uid, params, context=context)
            self.create_attachment(cursor, uid, swlog_id, fname, data, context=context)

            return sw_id, info
        except Exception as e:
            cursor.rollback()
            cursor.close()
            cursor = cr
            zip_name = context.get('zip_name', '')
            e_string = str(e)
            if not e_string:
                e_string = e.value
                e.message = e.value
            params = {'origin': zip_name if zip_name else fname or '',
                      'file': fname,
                      'status': 'error',
                      'info': e_string,
                      'request_code': xml.codi_sollicitud if xml else False,
                      'pas': xml.get_pas_xml().lower() if xml else False,
                      'proces': xml.get_tipus_xml() if xml else False,
                      'emisor_id': partner_id,
                      'codi_emisor': emisor,
                      'cups': cups_id or False,
                      'cups_text': cups_xml,
                      'tipus': 'import',
                      }
            db = pooler.get_db_only(cursor.dbname)
            tmp_cr = db.cursor()
            swlog_id = swlog_obj.create(tmp_cr, uid, params, context=context)
            self.create_attachment(cursor, uid, swlog_id, fname, data, context=context)
            tmp_cr.commit()
            raise e

    def create_attachment(self, cursor, uid, swlog_id, fname, data, context=None):
        ir_attachment_obj = TransactionExecute(cursor.dbname, uid, 'ir.attachment')

        att_search_values = [
            ('name', '=', fname),
            ('res_id', '=', swlog_id),
            ('res_model', '=', 'giscegas.atr.log'),
            ('datas', '=', base64.encodestring(data)),
        ]
        attach_id = ir_attachment_obj.search(att_search_values, context=context)

        if not attach_id:
            attachment_values = {
                'name': fname,
                'res_id': swlog_id,
                'res_model': 'giscegas.atr.log',
                'datas': base64.encodestring(data),
            }
            ir_attachment_obj.create(attachment_values,context=context)

    def importar_xml_post_hook(self, cursor, uid, sw_id, context=None):
        ctx = context.copy()
        ctx['activate_only_automatic'] = True
        try:
            res = self.activa_polissa_cas_atr(cursor, uid, sw_id, context=ctx)
            if res and len(res) > 1 and "no implementada/activada" in res[1]:
                res = ""
            elif res and len(res) > 1:
                res = res[1]
            else:
                res = ""
        except Exception, e:
            res = e.message
        return res

    def exportar_xml(self, cursor, uid, sw_id, step_id=False, pas_id=False, context=None):
        """
        Llença l'script per generar l'xml.
        Retorna tupla amb:
            (nom_fitxer, xml_generat, resultat_validació)

        'resultat_validació' es una namedtuple ValidationResult amb:
            (valid, missatge)

        on 'valid': boolea indicant si el XML es valid o no
        i 'error':
            - missatge d'error produït al generar el xml
            - missatge d'error al validar contra XSD.
            - missatge buit quan el XML es valid

        Segons la variable de configuracio 'sw_xsd_validation' es tracten els
        errors de validacio com errors o no.
        """
        if not context:
            context = {}
        sw_obj = self.pool.get('giscegas.atr')
        step_obj = self.pool.get('giscegas.atr.step')
        res_config = self.pool.get('res.config')
        swlog_obj = self.pool.get('giscegas.atr.log')

        sw = sw_obj.browse(cursor, uid, sw_id)
        # If no step id, use the last one of the case
        if not step_id:
            if not sw.step_id.id:
                raise osv.except_osv('Error',
                                     _(u"El caso no tiene ningún paso"))
            step_id = sw.step_id.id

        step_fields = ['name', 'proces_id.name']
        domain = [('id', '=', step_id)]
        query = OOQuery(step_obj, cursor, uid).select(step_fields).where(domain)
        cursor.execute(*query)
        step_vals = cursor.dictfetchall()[0]

        # when marking a draft as sent, open it
        if sw.state == 'cancel':
            raise osv.except_osv(
                _(u"Error Usuari"),
                _(u"El cas {0} està en estat cancelat. No es poden exportar "
                  u"casos cancelats. Si el vols exportar canvia l'estat a esborrany "
                  u"i després a obert").format(sw.codi_sollicitud)
            )
        elif sw.state == 'draft' and context.get('mark_as_sended', False):
            sw.case_open()

        step_model = step_obj.get_step_model(cursor, uid, step_id)
        pas_obj = self.pool.get(step_model)
        filters = [('sw_id', '=', sw_id)]
        if pas_id:
            filters.append(('id', '=', pas_id))
        pas_id = pas_obj.search(cursor, uid, filters)
        pas = pas_obj.browse(cursor, uid, pas_id[0])
        log_params = {
            'origin': _('Exportació ATR'),
            'pas': step_vals['name'],
            'proces': step_vals['proces_id.name'],
            'file': 'Cas: {}'.format(sw.id),
            'cups_text': sw.cups_id.name,
            'tipus': 'export',
            'codi_emisor': pas.emisor_id.ref,
            'emisor_id': pas.emisor_id.id,
            'request_code': sw.codi_sollicitud or '',
        }

        meta_ctx = context.get('meta', {})
        if meta_ctx.get('store_attachment', True):
            if pas.emisor_id.id != sw.company_id.id:
                raise osv.except_osv('Error',
                                     _(u"No es el emisor del paso actual"))
        try:
            xml = pas_obj.generar_xml(cursor, uid, pas_id, context)

            validation = xml and validate_xml(xml[1], is_gas=True)
            if not validation.valid:
                log_params['info'] = validation.error
                # For validation errors we check config to manage
                # them as errors or not
                val = int(res_config.get(cursor, uid, 'atr_gas_xsd_validation', 1))
                if not val:
                    validation = ValidationResult(True, validation.error)
            else:
                log_params['info'] = 'Cas {} exportat correctament'.format(
                    sw.id
                )
                if xml and context.get('mark_as_sended', False):
                    pas.header_id.set_enviament_pendent(False, context)

            res = (xml[0], xml[1], validation)
            log_params['status'] = 'correcte' if validation.valid else 'error'
        except osv.except_osv as e:
            res = ("", "", ValidationResult(False, "Error: " + str(e.value)))
            log_params.update({'status': 'error', 'info': res[2].error})
        swlog_obj.create(cursor, uid, log_params, context=context)
        return res

    def whereiam(self, cursor, uid, cups_id, context=None):
        '''retorna si estem a distri o a comer depenent de si el
        cups pertany a la nostra companyia o no'''
        return self.pool.get('giscegas.cups.ps').whereiam(cursor, uid, context)

    def search_address(self, cursor, uid, sw_id, direccio, context=None):
        if context is None:
            context = {}
        sw_obj = self.pool.get('giscegas.atr')
        addr_obj = self.pool.get('res.partner.address')
        tv_obj = self.pool.get('res.tipovia')
        mun_obj = self.pool.get('res.municipi')
        country_obj = self.pool.get('res.country')
        state_obj = self.pool.get('res.country.state')

        sw = sw_obj.browse(cursor, uid, sw_id)

        search_params = [('nv', '=', direccio.calle),
                         ('pnp', '=', direccio.numero_finca),
                         ('zip', '=', direccio.cod_postal),
                         ('id_municipi.ine', '=', direccio.municipio),
                         ('country_id.name', 'ilike', direccio.pais),
                         ('state_id.code', '=', direccio.provincia)]
        if direccio.aclarador_finca:
            param = ('aclarador', '=', direccio.aclarador_finca)
            search_params.append(param)
        if direccio.tipo_via:
            tv_ids = tv_obj.name_search(cursor, uid, direccio.tipo_via)
            if tv_ids:
                param = ('tv', '=', tv_ids[0][0])
                search_params.append(param)
        if direccio.escalera:
            param = ('es', '=', direccio.escalera)
            search_params.append(param)
        if direccio.piso:
            param = ('pt', '=', direccio.piso)
            search_params.append(param)
        if direccio.puerta:
            param = ('pu', '=', direccio.puerta)
            search_params.append(param)
        addr_ids = addr_obj.search(cursor, uid, search_params)
        if addr_ids:
            ares = None
            if len(addr_ids) == 1:
                ares = addr_ids[0]
            else:
                for addr in addr_obj.browse(cursor, uid, addr_ids):
                    if addr.partner_id.id == sw.cups_polissa_id.titular.id:
                        ares = addr.id
                        break
                if not ares:
                    ares = addr_ids[0]
            if context.get("new_phones", []) and ares:
                addr = addr_obj.browse(cursor, uid, ares)
                if not addr.phone and len(context.get("new_phones", [])) > 0:
                    addr.write({'phone': context.get("new_phones")[0]})
                if not addr.mobile:
                    if len(context.get("new_phones", [])) > 1:
                        addr.write({'mobile': context.get("new_phones")[1]})
                    elif len(context.get("new_phones", [])) > 0:
                        addr.write({'mobile': context.get("new_phones")[0]})
            return ares

        mun_ids = mun_obj.search(cursor, uid,
                                [('ine', '=', direccio.municipio)])
        country_ids = country_obj.search(cursor, uid,
                                [('name', '=', direccio.pais)])
        if country_ids:
            country_id = country_ids[0]
            search_params = [('country_id', '=', country_id),
                             ('code', '=', direccio.provincia)]
            state_ids = state_obj.search(cursor, uid, search_params)
            state_id = state_ids and state_ids[0] or False
        else:
            country_id = False
            state_id = False

        vals = {'nv': direccio.calle,
                'pnp': direccio.numero_finca,
                'aclarador': direccio.aclarador_finca,
                'zip': direccio.cod_postal,
                'id_municipi': mun_ids and mun_ids[0] or False,
                'country_id': country_id,
                'state_id': state_id,
                }
        if len(context.get("new_phones", [])) > 0:
            vals.update({'phone': context.get("new_phones")[0]})
        if len(context.get("new_phones", [])) > 1:
            vals.update({'mobile': context.get("new_phones")[1]})
        if direccio.tipo_via:
            tv_ids = tv_obj.name_search(cursor, uid, direccio.tipo_via)
            if tv_ids:
                vals.update({
                    'tv': tv_ids[0][0]
                })
        if direccio.escalera:
            vals.update({
                'es': direccio.escalera
            })
        if direccio.piso:
            vals.update({
                'pt': direccio.piso
            })
        if direccio.puerta:
            vals.update({
                'pu': direccio.puerta
            })

        return addr_obj.create(cursor, uid, vals)

    def create_partner_addres_from_cups(self, cursor, uid, cups_id, context=None):
        if not context:
            context = {}

        addr_obj = self.pool.get('res.partner.address')
        cups = self.pool.get("giscegas.cups.ps").browse(cursor, uid, cups_id)
        municipi_id, country_id, state_id = False, False, False
        if cups.id_municipi:
            municipi_id = cups.id_municipi.id
            if cups.id_municipi.state:
                state_id = cups.id_municipi.state.id
                if cups.id_municipi.state.country_id:
                    country_id = cups.id_municipi.state.country_id.id

        addr_vals = {
            'nv': cups.nv,
            'pnp': cups.pnp,
            'aclarador': cups.aclarador,
            'zip': cups.dp,
            'id_municipi': municipi_id,
            'country_id': country_id,
            'state_id': state_id,
            'tv': cups.tv and cups.tv.id or False,
            'es': cups.es,
            'pt': cups.pt,
            'pu': cups.pu,
            'name': context.get('new_name')
        }
        if len(context.get("new_phones", [])) > 0:
            addr_vals.update({'phone': context.get("new_phones")[0]})
        if len(context.get("new_phones", [])) > 1:
            addr_vals.update({'mobile': context.get("new_phones")[1]})
        return addr_obj.create(cursor, uid, addr_vals, context=context)

    def search_cnae(self, cursor, uid, cnae, context=None):

        cnae_obj = self.pool.get('giscemisc.cnae')
        search_params = [('name', '=', cnae)]
        cnae_ids = cnae_obj.search(cursor, uid, search_params)
        return cnae_ids and cnae_ids[0] or False

    def get_final(self, cursor, uid, sw, context=None):

        return False

    def historize_msg(self, cursor, uid, ids, msg, context=None):
        if not isinstance(ids, list):
            ids = [ids]
        crm_obj = self.pool.get('crm.case')
        crm_ids = self._get_crm_base_ids(cursor, uid, ids)
        return crm_obj.historize_msg(cursor, uid, crm_ids, msg, context=context)

    def activa_cas_atr(self, cursor, uid, sw, context=None):
         act_obj = self.pool.get("giscegas.atr.activation.config")
         helperdist_obj = self.pool.get('giscegas.atr.helpers.distri')
         helpercomer_obj = self.pool.get('giscegas.atr.helpers')
         if sw.whereiam == "distri":
             helper_obj = helperdist_obj
             whereiam = _(u'Distribuidora')
         else:
             helper_obj = helpercomer_obj
             whereiam = _(u'Comercializadora')

         method_names = act_obj.get_activation_method(cursor, uid, sw.get_pas(), context=context)
         if not method_names:
             return [_(u'Atención'),
                     _(u'Activación caso %s-%s no implementada/activada en %s.\n\n'
                       u'Puedes consultar las activaciones disponibles en:\n'
                       u'* Gas -> Gestión ATR -> Configuración -> Activaciones de Casos ATR'
                     ) % (sw.proces_id.name, sw.step_id.name, whereiam)]
         else:
            all_res = []
            all_res_description = []
            for method_name in method_names:
                method_caller = getattr(helper_obj, method_name, False)
                if not method_caller and helper_obj != helpercomer_obj:
                    # Els metodes comuns estan al de comer, busquem allà
                    method_caller = getattr(helpercomer_obj, method_name, False)
                if not method_caller:
                    raise osv.except_osv(
                        _('Error'),
                        _("No se ha podido activar el caso {0} porque no se encuentra el "
                          "método de activación {1}.").format(sw.name, method_name)
                    )
                init_str = _(u"Resultado Activacion:\n")
                db = pooler.get_db(cursor.dbname)
                tmp_cursor = db.cursor()
                r_msg = ""
                try:
                    res = method_caller(tmp_cursor, uid, sw.id, context=context)
                    if len(res) == 2:
                        msg = res[0]+": "+res[1]
                        all_res.append(res[0])
                        all_res_description.append(res[1])
                    elif len(res) == 1:
                        msg = res[0]
                        all_res.append("Info.")
                        all_res_description.append(res[0])
                    elif not res:
                        continue
                    else:
                        raise Exception("Error", _(u"La activacion no ha devuelto la informacion esperada"))
                    r_msg = init_str+msg
                    tmp_cursor.commit()
                except Exception, e:
                    r_msg = init_str+str(e)
                    tmp_cursor.rollback()
                    return ("ERROR", str(e))
                finally:
                    tmp_cursor.close()
                    self.historize_msg(cursor, uid, sw.id, r_msg, context=context)
            self.update_deadline(cursor, uid, sw.id)
            if "Error" in all_res:
                final_res = ["Error"]
            elif "Ok" in all_res:
                final_res = ["Ok"]
            else:
                final_res = [all_res[0]]
            final_res.append("\n\n".join(all_res_description))
            return final_res

    def activa_polissa_cas_atr(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}
        sw_obj = self.pool.get('giscegas.atr')

        #Cridem la funció al model que correspongui
        sw = sw_obj.browse(cursor, uid, sw_id)
        ctx = context.copy()
        ctx.update({'sync': False})
        res = self.activa_cas_atr(cursor, uid, sw, context=ctx)

        return res

    def notifica_pas_a_client(self, cursor, uid, sw_id, pas_id, step_model_name,
                              template_id=False, context=None):
        """
        Envia mail a client per a un pas d'un cas atr segons plantilla
        :param cursor:          OpenERP Cursor
        :param uid:             OpenERP User ID
        :type uid:              int
        :param sw_id:           Switching Object ID
        :type sw_id:            long
        :param template_id:     Model data ID (Poweremail Template)
        :type template_id:      int
        :param pas_id:          Switching Step Object ID
        :type pas_id:           int
        :param step_model_name: Step Model Name
        :type step_model_name:  str
        :param context:         OpenERP Cursor
        :type context:          dict
        :return:
        """
        pw_account_obj = self.pool.get('poweremail.core_accounts')
        pwetmpl_obj = self.pool.get('poweremail.templates')
        pwswz_obj = self.pool.get('poweremail.send.wizard')
        partner_obj = self.pool.get('res.partner')
        user_obj = self.pool.get('res.users')
        pol_obj = self.pool.get('giscegas.polissa')
        sw_obj = self.pool.get('giscegas.atr')
        notify_obj = self.pool.get('giscegas.atr.notify')
        step_obj = self.pool.get('giscegas.atr.step')
        pas_obj = self.pool.get(step_model_name)
        header_obj = self.pool.get('giscegas.atr.step.header')
        conf_obj = self.pool.get('res.config')

        if not context:
            context = {}
        if isinstance(pas_id, list):
            pas_id = pas_id[0]

        sw_data = sw_obj.read(
            cursor, uid, sw_id, ['cups_polissa_id', 'name', 'proces_id']
        )
        polissa_name = sw_data['cups_polissa_id'][1]
        sw_name = sw_data['name']

        # Check PAS pendent de notificar
        pas_data = pas_obj.read(cursor, uid, pas_id, ['notificacio_pendent',
                                                      'header_id'])
        if not pas_data.get('notificacio_pendent', False):
            return False, _(u'Este paso ya está notificado al cliente')

        # Si no es facilita la plantilla, la obtenim del SW.Step.Header
        if not template_id:
            try:
                header_id = pas_data['header_id'][0]
                template_id = header_obj.get_notification_mail(
                    cursor, uid, [header_id]
                )[0]
            except Exception as e:
                return (
                    False, _(
                        u"No se ha podido notificar al usuario del caso {}_\n{}"
                    ).format(sw_name, e)
                )
        template = pwetmpl_obj.browse(cursor, uid, template_id)

        mail_from = False

        # Obtenir compte de correu a utilitzar
        if template.enforce_from_account:
            mail_from = template.enforce_from_account.id
        if not mail_from or not isinstance(mail_from, int):
            mail_from = context.get('use_mail_account', False)
        if not mail_from or not isinstance(mail_from, int):
            address_mail_noti = conf_obj.get(
                cursor, uid, 'atr_gas_email_address_user_notification'
            ) or False
            if address_mail_noti:
                mail_from = pw_account_obj.search(cursor, uid, [
                    ('email_id', 'ilike', address_mail_noti)
                ])
                if mail_from:
                    mail_from = mail_from[0]
        if not mail_from or not isinstance(mail_from, int):
            return (False, _(
                u"No se ha enviado el mail de notificación al usuario del "
                u"contrato '%s' porque la plantilla del correo '%s' no tiene "
                u"ninguna cuenta asignada"
            ) % (polissa_name, template.name))

        ctx = {'active_ids': [sw_id], 'active_id': sw_id,
               'template_id': template_id, 'src_rec_ids': [sw_id],
               'src_model': 'giscegas.atr', 'from': mail_from,
               'state': 'single', 'priority': '0', 'folder': 'outbox',
               'save_async': True}
        params = {'state': 'single', 'priority': '0', 'from': ctx['from']}

        # Obtenir el llenguatge del titular
        titular_id = pol_obj.read(
            cursor, uid, sw_data['cups_polissa_id'][0], ['titular']
        )['titular'][0]
        templ_lang = partner_obj.read(cursor, uid, titular_id, ['lang'])['lang']
        # Si no en té, l'obtenim de la configuració del ERP
        if not templ_lang:
            templ_lang = config.get('language', False)
        # Si no en té, l'obtenim del context
        if not templ_lang:
            templ_lang = context.get('lang', False)
        # Si no en té, l'obtenim del usuari que realitza l'acció
        if not templ_lang:
            templ_lang = user_obj.read(
                cursor, uid, uid, ['context_lang'])['context_lang']
        # Si hem obtingut algun llenguatge, el posem al context
        if templ_lang:
            ctx.update({'lang': templ_lang})

        pas = pas_obj.browse(cursor, uid, pas_id)
        if not pas.notificacio_pendent:
            return False, _(
                u"El Proceso ATR: {} en el paso {} no está pendiente de notificar"
            ).format(sw_name, pas._nom_pas)
        proces_id = sw_data['proces_id'][0]
        # Cerca notificacio
        step_id = step_obj.search(cursor, uid, [
            ('name', '=', pas._nom_pas),
            ('proces_id', '=', proces_id)
        ])[0]

        notify_id = notify_obj.search(cursor, uid, [
            ('step_id', '=', step_id),
            ('proces_id', '=', proces_id)
        ])
        rebuig = False
        if pas.rebuig:
            if not notify_id:
                return False, _(
                    u"No hay notificaciones habilitadas para rechazos de este "
                    u"proceso: {}"
                ).format(sw_name)
            notify_dades = notify_obj.read(cursor, uid, notify_id, ['rebuig_ids'])
            for notify in notify_dades:
                for tipus_rebuig in pas.rebuig_ids:
                    if tipus_rebuig.motiu_rebuig.id in notify['rebuig_ids']:
                        if rebuig:
                            return False, _(
                                u"Hay mas de una notificación habilitada para "
                                u"el rechazo  \"{}\" en el Proceso ATR: {}"
                            ).format(tipus_rebuig.motiu_rebuig.text, sw_name)
                        rebuig = notify_obj.get_notification_text(
                            cursor, uid, notify['id'], sw_id, ctx
                        )
            if not rebuig:
                return False, _(
                    u"No hay notificaciones habilitadas para rechazos de este "
                    u"proceso: {}").format(sw_name)
        try:
            if rebuig:
                ctx['extra_render_values'] = {'notificacio_text': rebuig}
            pwswz_id = pwswz_obj.create(cursor, uid, params, ctx)
            pwemb_id = pwswz_obj.save_to_mailbox(cursor, uid, [pwswz_id], ctx)
            # If OORQ_ASYNC, pwemb_id -> Job_id
            # Else, pwemb_id -> PoweremailMailbox.id
            pas_obj.write(cursor, uid, [pas_id], {
                'notificacio_pendent': False
            })
            # Quan notifiquem 05 marcar 02 (acceptacio) notificat
            if pas._nom_pas == '05':
                step02_modelname = step_model_name.replace('05', '02')
                step02_obj = self.pool.get(step02_modelname)
                step02_id = step02_obj.search(cursor, uid, [
                    ('sw_id', '=', sw_id)
                ])
                step02_obj.write(cursor, uid, step02_id,
                                 {'notificacio_pendent': False})
            return (
                True, _(
                    u"Se ha notificado correctamente por mail al usuario del "
                    u"caso {}").format(sw_name)
            )
        except Exception as e:
            return (
                False, _(
                    u"No se ha podido notificar al usuario del caso {}_\n{}"
                ).format(sw_name, e)
            )

    def notifica_a_client(self, cursor, uid, sw_id, template=None,
                          context=None):
        """
        Envia mail a client segons plantilla
        :param cursor:      OpenERP cursor
        :param uid:         OpenERP User ID
        :type uid:          int
        :param sw_id:       Switching Object ID
        :type sw_id:        long
        :param template:    Poweremail Template Name
        :type template:     str
        :param context:     OpenERP Context
        :type context:      dict
        :return:
        """
        if not context:
            context = {}

        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]

        mdata_obj = self.pool.get('ir.model.data')
        conf_obj = self.pool.get('res.config')

        # enable/disable user mail notifications
        mail_noti = conf_obj.get(
            cursor, uid, 'atr_gas_mail_user_notification_on_activation'
        ) or False
        if not mail_noti:
            return ('OK', _(u'Envio de correo de notificación por atr'
                            u' deshabilitados'))
        try:
            sw = self.browse(cursor, uid, sw_id)
            pas = sw.get_pas()
            if not pas.notificacio_pendent:
                return 'OK', _(u'Este paso ya está notificado al cliente')
            sw_name = '{}-{}'.format(sw.proces_id.name, sw.step_id.name)
            if mail_noti != 'all' and sw_name not in mail_noti:
                return ('OK', _(u'Envio de correo de notificación por atr '
                                u'{} deshabilitados').format(sw_name))

            if template:
                # Busquem la plantilla de mail d'activació si s'ha facilitat
                search_vals = ([
                    ('model', '=', 'poweremail.templates'),
                    ('name', '=', template)
                ])

                model_data_id = mdata_obj.search(cursor, uid, search_vals)[0]
                template_id = mdata_obj.read(
                    cursor, uid, model_data_id, ['res_id'])['res_id']
            else:
                template_id = False

            state, msg = self.notifica_pas_a_client(
                cursor, uid, sw_id,  sw.get_pas_id(), sw.get_pas_model_name(),
                template_id, context
            )
            return (_('OK'), msg) if state else (_('ERROR'), msg)

        except osv.except_osv as e:
                info = u'%s' % unicode(e.value)
                return _(u'ERROR'), info

        except Exception as e:
                raise Exception(e)

    def _get_sw_from_step_info(self, cursor, uid, ids, context=None):

        info_obj = self.pool.get('giscegas.atr.step.info')
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        sw_ids = info_obj.read(cursor, uid, ids, ['sw_id'], context=context)
        return [x['sw_id'][0] for x in sw_ids]

    def _ff_get_step(self, cursor, uid, ids, field_name, arg, context=None):

        res = {}
        for sw in self.browse(cursor, uid, ids):
            actual_step = False
            actual_step_date = False
            for step in sw.step_ids:
                if step.pas_id:
                    model, pas_id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(pas_id))
                    new_date = pas.header_id.perm_read()[0]['create_date']
                    if new_date >= actual_step_date:
                        actual_step_date = new_date
                        actual_step = step.step_id.id
                else:  # we are creating a new step, so it will be the newest
                    res[sw.id] = step.step_id.id
            if sw.id not in res:
                res[sw.id] = actual_step
        return res

    def _ff_whereiam(self, cursor, uid, ids, field_name, arg, context=None):
        '''retorna si estem a distri o a comer depenent de si el
        cups pertany a la nostra companyia o no'''

        cups_obj = self.pool.get('giscegas.cups.ps')
        where = cups_obj.whereiam(cursor, uid)
        res = dict.fromkeys(ids, where)
        return res

    def _ff_get_stages(self, cursor, uid, ids, field_name, arg, context=None):
        '''returns all stages associated with switching case'''
        stage_obj = self.pool.get('giscegas.atr.stage')
        res = {}
        for sw in self.browse(cursor, uid, ids, context=context):
            if not sw.proces_id or not sw.step_id or not sw.whereiam:
                continue
            res[sw.id] = stage_obj.get_stages(cursor, uid, sw.proces_id.name,
                                              sw.step_id.name, sw.whereiam,
                                              context=context)
        return res

    def _ff_get_final(self, cursor, uid, ids, field_name, arg, context=None):
        '''returns True if the case has arrived to a final step'''

        res = {}
        for sw in self.browse(cursor, uid, ids, context=context):
            res[sw.id] = self.get_final(cursor, uid, sw, context=context)

        return res

    def _ff_enviament_pendent(self, cursor, uid, ids, field_name, arg,
                              context=None):
        """retorna False si no hi ha cap dels passos amb enviament pendent"""
        if context is None:
            context = {}

        res = dict.fromkeys(ids, False)

        for sw_id in ids:
            res[sw_id] = len(self.get_passos_pendents_enviament(cursor, uid, sw_id, context=context))

        return res

    def _ff_send_pendent_search(self, cursor, uid, obj, name, args, context):
        swh_obj = self.pool.get('giscegas.atr.step.header')
        user_obj = self.pool.get('res.users')
        q = OOQuery(user_obj, cursor, uid)
        sql = q.select(['company_id.id']).where([('id', '=', uid)])
        cursor.execute(*sql)
        emissor_id = cursor.fetchone()
        sql = OOQuery(swh_obj, cursor, uid).select(['sw_id']).where([
            ('enviament_pendent', '=', True),
            ('emisor_id.partner_id', '=', emissor_id[0] if emissor_id else False)
        ])
        cursor.execute(*sql)
        res = cursor.dictfetchall()
        operator = args[0][2] and 'in' or 'not in'
        return [('id', operator, [record['sw_id'] for record in res])]

    def _ff_document_adjunt_solicitud(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        res = dict.fromkeys(ids, False)

        swh_obj = self.pool.get('giscegas.atr.step.header')
        swh_ids = swh_obj.search(cursor, uid, [
            ('sw_id', 'in', ids),
            ('document_ids', '!=', False),
        ])
        switching_headers = swh_obj.read(cursor, uid, swh_ids, ['sw_id'])
        sw_ids = list(set([
            sw_header['sw_id'][0] for sw_header in switching_headers
        ]))
        for sw_id in sw_ids:
            res[sw_id] = True

        return res

    def update_ff_accio_pendent_comerdist(self, cursor, uid, ids, context=None):
        res = self._ff_accio_pendent_comerdist(cursor, uid, ids, False, False, context=context)
        for sw_id, val in res.items():
            self.write(cursor, uid, sw_id, {'accio_pendent_comerdist': val}, context=context)
        return True

    def _ff_accio_pendent_comerdist(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, 'comer')
        step_obj = self.pool.get('giscegas.atr.step')
        for sw_info in self.read(cursor, uid, ids, ['step_id', 'rebuig', 'state']):
            if sw_info['state'] in ['done']:
                continue
            if sw_info['step_id']:
                step_info = step_obj.read(cursor, uid, sw_info['step_id'][0], ['accio_pendent', 'description'])
                if sw_info['rebuig'] and "Aceptacion / Rechazo" in step_info['description']:
                    res[sw_info['id']] = 'comer' if step_info['accio_pendent'] == 'distri' else 'distri'
                else:
                    res[sw_info['id']] = step_info['accio_pendent']
        return res

    def _ff_notificacio_pendent(self, cursor, uid, ids, field_name, arg,
                                context=None):
        if context is None:
            context = {}

        res = dict.fromkeys(ids, False)

        swh_obj = self.pool.get('giscegas.atr.step.header')
        swh_ids = swh_obj.search(cursor, uid, [
            ('sw_id', 'in', ids), ('notificacio_pendent', '=', True)
        ])

        switching_headers = swh_obj.read(cursor, uid, swh_ids, ['sw_id'])
        sw_ids = list(set([
            sw_header['sw_id'][0] for sw_header in switching_headers
        ]))
        for sw_id in sw_ids:
            res[sw_id] = True

        return res

    def _ff_notificacio_pendent_search(
            self, cursor, uid, obj, name, args, context):
        swh_obj = self.pool.get('giscegas.atr.step.header')
        h_ids = swh_obj.search(cursor, uid, args)
        sw_ids = [
            s['sw_id'][0]
            for s in swh_obj.read(cursor, uid, h_ids, ['sw_id'])
            if s['sw_id']
        ]
        return [('id', 'in', sw_ids)]

    def _ff_get_contacte_polissa(self, cursor, uid, ids, field_name, arg,
                                 context=None):
        res = {}
        camps = {}
        swh = self.pool.get('giscegas.atr.step.header')
        for sw in self.browse(cursor, uid, ids, context=context):
            polissa = sw.cups_polissa_id
            camps = {
                'titular_polissa': False,
                'nif_titular_polissa': False,
                'dir_pagador_polissa': False,
                'tel_pagador_polissa': False,
                'mail_pagador_polissa': False
            }
            if polissa:
                camps['titular_polissa'] = polissa.titular.id
                camps['nif_titular_polissa'] = polissa.titular.vat
                dir = polissa.direccio_pagament if polissa.direccio_pagament else polissa.titular.address[0]
                camps['dir_pagador_polissa'] = dir.id
                phone = (dir.phone or dir.mobile)
                tel_str = swh.clean_tel_number(phone)
                camps['tel_pagador_polissa'] = tel_str['tel']
                camps['mail_pagador_polissa'] = dir.email

            res[sw.id] = camps[field_name]

        return res

    def _ff_titular_search(self, cursor, uid, obj, name, args,
                                         context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        else:
            ids = self.search(cursor, uid, [
                ('cups_polissa_id.titular.name', 'ilike', args[0][2])
            ])
            ids.append(-1)
            return [('id', 'in', ids)]

    def _ff_nif_titular_polissa_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        else:
            ids = self.search(cursor, uid, [
                ('cups_polissa_id.titular.vat', 'ilike', args[0][2])
            ])
            ids.append(-1)
            return [('id', 'in', ids)]

    def _ff_get_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna True si és un cas en estat rebuig. Cal que estigui en un pas
        que pugui ser de rebuig"""
        if not context:
            context = {}
        res = dict([(i, False) for i in ids])

        steps = self.read(cursor, uid, ids, ['proces_id', 'step_id',
                                             'step_ids'])
        for step in steps:
            proces = step['proces_id'][1]
            pas = step['step_id'] and step['step_id'][1] or False
            proces_obj = self.pool.get('giscegas.atr.proces')
            if pas not in proces_obj.get_reject_steps(cursor, uid, proces,
                                                      context):
                res[step['id']] = False
            else:
                info_obj = self.pool.get('giscegas.atr.step.info')
                passos = info_obj.read(cursor, uid, step['step_ids'],
                                       ['pas_id'])
                model = 'giscegas.atr.%s.%s' % (proces.lower(), pas)
                pas_id = False
                for p in passos:
                    if p['pas_id'] and p['pas_id'].split(',')[0] == model:
                        pas_id = p['pas_id'].split(',')[1]

                if pas_id:
                    rebuig = self.pool.get(model).read(cursor, uid,
                                                       int(pas_id),
                                                       ['rebuig'])['rebuig']
                else:
                    rebuig = False

                res[step['id']] = rebuig

        return res

    def _ff_rebuig_search(self, cursor, uid, obj, name, args, context=None):
        """Per poder cercar els casos en estat rebuig"""
        if not context:
            context = None

        proces_obj = self.pool.get('giscegas.atr.proces')

        processos = SUPORTED_PROCESS
        plantilla = 'giscegas.atr.%s.%s'
        ids_rebuig = []

        # busquem x models
        for p in processos:
            steps = proces_obj.get_reject_steps(cursor, uid, p)

            model_names = [plantilla % (p.lower(), s) for s in steps]
            for m in model_names:
                model_obj = self.pool.get(m)
                if not model_obj:
                    continue
                ids_r = model_obj.search(cursor, uid, [('rebuig', '=', True)])
                ids_sw_vals = model_obj.read(cursor, uid, ids_r, ['sw_id'])
                ids_sw = [i['sw_id'][0] for i in ids_sw_vals]
                ids_rebuig += ids_sw

        # rebutjats: els de la llista (in)
        # no rebutjats: els que no estan a la llista (not in)
        operacio = args[0][2] and 'in' or 'not in'
        return [('id', operacio, ids_rebuig)]

    def update_additional_info(self, cursor, uid, step_id, model, context=None):
        if context is None:
            context = {}
        pas_obj = self.pool.get(model)
        sw_id = pas_obj.read(cursor, uid, step_id, ['sw_id'])['sw_id'][0]

        if not getattr(pas_obj, 'get_additional_info', False):
            # If the step doesn't have the method 'get_additional_info' we
            # try to use step a1 info.
            # Step a1 always should have 'get_additional_info'
            pas01_mod = "giscegas.atr.{0}.a1".format(model.split(".")[2])
            pas_obj = self.pool.get(pas01_mod)
            step_id = pas_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
            if not step_id:
                return
            step_id = step_id[0]

        add_info = pas_obj.get_additional_info(cursor, uid, step_id, context=context)
        self.write(cursor, uid, [sw_id], {'additional_info': add_info})

    def update_deadline(self, cursor, uid, sw_id, proces=None,  step=None,
                        init_date=None, context=None):
        if context is None:
            context = {}

        # Check if it's reject step, then we've ended and there is no deadline
        reject = self.read(cursor, uid, sw_id, ['rebuig'])['rebuig']
        if reject:
            self.write(cursor, uid, [sw_id], {'date_deadline': False})
            return

        deadline = self.get_deadline_for_step(
            cursor, uid, sw_id, proces=proces, step=step, init_date=init_date,
            context=context
        )

        if deadline:
            self.write(cursor, uid, [sw_id], {'date_deadline': deadline})

    def get_deadline_for_step(self, cursor, uid, sw_id, proces=None,  step=None,
                              init_date=None, context=None):
        if context is None:
            context = {}

        if not proces or not step:
            # Get current proces and step
            sw = self.browse(cursor, uid, sw_id)
            proces = sw.step_id.proces_id.name
            step = sw.step_id.name

        if not init_date:
            # Get init deadline date from step
            step_model = "giscegas.atr.{0}.{1}".format(proces.lower(), step)
            step_obj = self.pool.get(step_model)
            if getattr(step_obj, 'get_init_deadline_date', False):
                init_date = step_obj.get_init_deadline_date(cursor, uid, sw_id, context)
            else:
                init_date = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
                init_date = datetime.strptime(init_date, "%Y-%m-%d %H:%M:%S")

        import gestionatr.input.messages as sm
        prefix = "A1_"
        if proces == '26':
            prefix = "A12_"
        elif proces == '45':
            prefix = "A19_"
        pas_message = getattr(sm, prefix + proces)

        modifier = None

        deadline_obj = pas_message.get_deadline(step, modifier=modifier)

        if deadline_obj:
            return deadline_obj.limit(init_date).strftime("%Y-%m-%d")
        return False

    def historize_additional_info(self, cursor, uid, sw, context=None):
        """
        Historize (write in observations) the current additional info of sw to
        its contract.

        :param sw: browse of giscegas_atr
        """
        if sw.cups_polissa_id and sw.additional_info:
            pol = sw.cups_polissa_id
            info = u"\n{0} ({1}):\n{2}\n".format(
                sw.proces_id.name, sw.codi_sollicitud, sw.additional_info
            )
            msg = u"{0}\n{1}".format(pol.observacions, info)
            pol.write({'observacions': msg}, context=context)

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        res = super(GiscegasAtr, self).write(cursor, uid, ids, vals, context=context)
        if 'step_ids' in vals:
            self.update_ff_accio_pendent_comerdist(cursor, uid, ids, context=context)
            for sw in self.browse(cursor, uid, ids):
                pas = self.get_pas(cursor, uid, sw, context=context)
                if not pas:
                    continue
                model = "giscegas.atr.{0}.{1}".format(
                    sw.proces_id.name.lower(), sw.step_id.name)
                self.update_additional_info(cursor, uid, pas.id, model, context)
        return res

    _STORE_GET_STEP = {
        'giscegas.atr': (lambda self, cr, uid, ids, c=None: ids,
                                ['step_ids'], 10),
        'giscegas.atr.step.info': (_get_sw_from_step_info, [], 10),
        }

    _STORE_WHEREIAM = {
        'giscegas.atr': (lambda self, cr, uid, ids, c=None: ids,
                                ['cups_id'], 10)
        }

    _WHEREIAM = [('comer', 'Comercializadora'),
                 ('distri', 'Distribuidora'),
                 ('ND', 'No disponible')]

    _columns = {
        'case_id': fields.many2one('crm.case', 'Caso'),
        'conversation_id': fields.many2one('poweremail.conversation',
                                           'Conversacion'),
        'accio_pendent': fields.boolean("Pendiente de acción de usuario",
                                        readonly=True),
        'accio_pendent_comerdist': fields.selection(
            string="Agente con accion pendiente",
            selection=[('comer', "Comercializadora"), ("distri", "Distribuidora")], select=2
        ),
        'validacio_pendent': fields.boolean('Pendiente de validar',
                                            readonly=True),
        'enviament_pendent': fields.function(_ff_enviament_pendent,
                                             method=True,
                                             string="Pendiente de enviar",
                                             type="boolean", readonly=True,
                                             fnct_search=_ff_send_pendent_search),
        'obrir_cas_pendent': fields.boolean("Pendeinte de abrir", readonly=True),
        'notificacio_pendent': fields.function(
            _ff_notificacio_pendent, string="Pendiente de notificar",
            method=True, type="boolean", readonly=True,
            fnct_search=_ff_notificacio_pendent_search
        ),
        'documents_solicitud_adjunt': fields.function(
            _ff_document_adjunt_solicitud, method=True, string="Documentu adjunto", type="boolean", readonly=True,
            store={'giscegas.atr': (lambda self, cr, uid, ids, c=None: ids, ['step_ids'], 10)}
        ),

        # capçalera
        'company_id': fields.many2one('giscemisc.participant', u'Compañia',
                                      readonly=True),
        'codi_sollicitud': fields.char(u'Codigo Solicitud', size=12,
                                       readonly=True),
        'data_sollicitud': fields.date(u'Fecha de solicitud', required=True,
                                       select=1),
        'cups_id': fields.many2one('giscegas.cups.ps', 'CUPS', size=22),
        'cups_input': fields.char('CUPS', size=22),
        # Cups_input es un camp per permetre guardar el codi de CUPS que no
        # existeixen a la database (utilitzat en importar CUPS incorrectes)
        'proces_id': fields.many2one('giscegas.atr.proces', u'Proceso',
                                     required=True, readonly=True,
                                     states={'draft': [('readonly', False)]}),
        'step_id': fields.function(_ff_get_step, method=True,
                                   string='Paso', type='many2one',
                                   obj='giscegas.atr.step',
                                   readonly=True,
                                   store=_STORE_GET_STEP),
        'step_description': fields.related('step_id', 'description', type='char', relation='giscegas.atr.step',
                                           string='Descripicio Pas', size=32),
        'cups_polissa_id': fields.many2one('giscegas.polissa', 'Contrato'),
        'ref_contracte': fields.char('Ref. Contracte', size=12, required=True),
        'comer_sortint_id': fields.many2one('giscemisc.participant',
                                            'Comercializadora saliente'),
        'whereiam': fields.function(_ff_whereiam, method=True,
                                 string="On som", type='selection',
                                 selection=_WHEREIAM,
                                 size=6, readonly=True,
                                 store=_STORE_WHEREIAM),
        'stage_ids': fields.function(_ff_get_stages, method=True,
                                     string='Etapes', type='many2many',
                                     relation='giscegas.atr.stage'),
        'finalitzat': fields.function(_ff_get_final, method=True,
                                      string="Finalitzat", type='boolean',
                                      store=_STORE_GET_STEP),
        #Contacte client
        'titular_polissa': fields.function(_ff_get_contacte_polissa,
                                           method=True, string='Cliente',
                                           type='many2one', obj='res.partner',
                                           readonly=True,
                                           fnct_search=_ff_titular_search),
        'nif_titular_polissa': fields.function(_ff_get_contacte_polissa,
                                               method=True, string='NIF',
                                               type='char', size='11',
                                               readonly=True,
                                               fnct_search=_ff_nif_titular_polissa_search),
        'dir_pagador_polissa': fields.function(_ff_get_contacte_polissa,
                                               method=True, type='many2one',
                                               string='Dirección Pagador',
                                               obj='res.partner.address',
                                               readonly=True),
        'tel_pagador_polissa': fields.function(_ff_get_contacte_polissa,
                                               method=True, string='Teléfono',
                                               type='char', size='20',
                                               readonly=True),
        'mail_pagador_polissa': fields.function(_ff_get_contacte_polissa,
                                                method=True, string='E-mail',
                                                type='char', size='50',
                                                readonly=True),
        # Per poder buscar casos de rebuig
        'rebuig': fields.function(_ff_get_rebuig, method=True, type='boolean',
                                  string=u'Solicitud rechazada',
                                  fnct_search=_ff_rebuig_search,
                                  readonly=True),
        # Informació adicional sobre el pas
        'additional_info': fields.char(u'Información Adicional', size=400),
    }

    def _get_id_company(self, cr, uid, context=None):
        if not context:
            context = {}
        user = self.pool.get('res.users').browse(cr, uid, uid)
        participant = self.pool.get(
            'giscemisc.participant'
        ).get_from_partner(cr, uid, user.company_id.partner_id.id)
        if not participant.codi_sifco:
            raise osv.except_osv(_(u"Error"),
                        _(u"Se debe configurar el código SIFCO de la empresa %s") %
                         user.company_id.partner_id.name)
        return participant.id

    def _get_default_section(self, cursor, uid, context=None):
        '''returns SW section id'''

        section_obj = self.pool.get('crm.case.section')
        search_params = [('code', '=', 'ATRG')]
        section_ids = section_obj.search(cursor, uid, search_params)
        if not section_ids:
            err = _(u"No se ha encontrado la sección del CRM con código 'ATRG'")
            raise osv.except_osv(u'Atenció', err)

        return section_ids[0]

    def _get_default_codi_sollicitud(self, cr, uid, context=None):
        '''Genera el codi de sol·licitud'''
        if not context:
            context = {}
        data = time.strftime('%Y%m%d')
        seq_obj = self.pool.get('ir.sequence')
        seq_code = 'giscegas.atr.sollicitud'
        codi = int(seq_obj.get_next(cr, uid, seq_code))
        return "%s%04d" % (data, codi % 10000)

    _defaults = {
        'data_sollicitud': lambda *a: time.strftime('%Y-%m-%d'),
        'codi_sollicitud': _get_default_codi_sollicitud,
        'company_id': _get_id_company,
        'accio_pendent': lambda *a: False,
        'validacio_pendent': lambda *a: False,
        'enviament_pendent': lambda *a: False,
        'state': lambda *a: 'draft',
        'notificacio_pendent': lambda *a: False,
        'section_id': _get_default_section,

    }

    def _check_comer_sortint(self, cursor, uid, ids, context=None):

        for sw in self.browse(cursor, uid, ids, context=context):
            if sw.cups_id and sw.cups_polissa_id:
                if (sw.whereiam == 'distri' and
                        sw.proces_id and
                        sw.proces_id.name in ('C1', 'C2') and
                        not sw.comer_sortint_id):
                    return False
        return True

    def _cnt_steps_proces(self, cursor, uid, ids):

        for sw in self.browse(cursor, uid, ids):
            step_ids = [x.id for x in sw.step_ids
                        if x.proces_id.id != sw.proces_id.id]
            if step_ids:
                return False
        return True

    _constraints = [(_check_comer_sortint,
                     _(u"No sw ha encontrado la comercializadora saliente"),
                     ['comer_sortint_id']),
                    (_cnt_steps_proces,
                     u"No se puede asignar un código de proceso distinto al "
                     u"asignado en los pasos",
                     ['steps_ids', 'proces_id'])]

GiscegasAtr()


class GiscegasAtrStepInfo(osv.osv):

    _name = 'giscegas.atr.step.info'
    _desc = 'Passos dels fluxes de switching'

    def create(self, cursor, uid, vals, context=None):

        proces_obj = self.pool.get('giscegas.atr.proces')
        res = super(GiscegasAtrStepInfo,
                    self).create(cursor, uid, vals, context=context)

        step = self.browse(cursor, uid, res, context=context)
        if step.pas_id:
            pas_model = self.pool.get(step.pas_id.split(',')[0])
            pas_id = int(step.pas_id.split(',')[1])
            pas_vals = {'sw_id': step.sw_id.id}
            pas_model.write(cursor, uid, [pas_id], pas_vals)
            pas_model_name = step.pas_id.split(',')[0]
            self.update_notify_info(
                cursor, uid, step.sw_id.id, pas_model_name, pas_id, context)
        else:
            pas_model_name = ('giscegas.atr.%s.%s' %
                               (step.proces_id.name.lower(),
                                step.step_id.name))
            pas_model = self.pool.get(pas_model_name)
            if hasattr(pas_model, 'dummy_create'):
                #Check if we can create this step
                emisor_steps = proces_obj.get_emisor_steps(cursor, uid,
                                                   step.proces_id.name,
                                                   step.sw_id.whereiam or "comer",
                                                   context=context)
                if step.step_id.name not in emisor_steps:
                    raise osv.except_osv('Error',
                                 _(u"No se puede crear el paso %s como "
                                   u"emisor") % (step.step_id.name))
                pas_id = pas_model.dummy_create(cursor, uid,
                                                step.sw_id.id,
                                                context=context)
                step.write({'pas_id': '%s,%s' % (pas_model_name, pas_id)})
                self.update_notify_info(
                    cursor, uid, step.sw_id.id, pas_model_name, pas_id, context)
        return res

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        res = super(GiscegasAtrStepInfo,
                    self).write(cursor, uid, ids, vals, context=context)

        for step in self.browse(cursor, uid, ids, context=context):
            if step.pas_id:
                pas_model = self.pool.get(step.pas_id.split(',')[0])
                pas_id = int(step.pas_id.split(',')[1])
                pas_vals = {'sw_id': step.sw_id.id}
                pas_model.write(cursor, uid, [pas_id], pas_vals)

        return res

    def unlink(self, cursor, uid, ids, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for step in self.read(cursor, uid, ids, ['pas_id'], context=context):
            if step.get('pas_id'):
                pas_model = self.pool.get(step.get('pas_id').split(',')[0])
                pas_id = int(step.get('pas_id').split(',')[1])
                pas_model.unlink(cursor, uid, [pas_id])
        return super(GiscegasAtrStepInfo,
                     self).unlink(cursor, uid, ids, context=context)

    def update_notify_info(
            self, cursor, uid, sw_id, model_name, pas_id, context=None):
        """
        Set default value to:
        - `enviament_pendent` of a GiscegasAtrStepHeader
            if PAS in EmisorSteps
        - `notificacio_pendent` of a GiscegasAtrStepHeader
            if PAS in config_var["sw_mail_user_notification_on_activation"]
        :param cursor:      OpenERP Cursor
        :param uid:         OpenERP User ID
        :type uid:          int
        :param sw_id:       GiscegasAtr ID
        :type sw_id:        int
        :param model_name:  GiscegasAtr Model Object Name
        :type model_name:   str
        :param pas_id:      GiscegasAtr Model ID
        :type pas_id:       int
        :param context:     OpenERP Context
        :type context:      dict
        :return:            -
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        proces_obj = self.pool.get('giscegas.atr.proces')
        conf_obj = self.pool.get('res.config')
        pas_obj = self.pool.get(model_name)

        if context is None:
            context = {}

        pas = model_name.split('.')[-1]
        sw = sw_obj.browse(cursor, uid, sw_id)

        # Set enviament on sw.step.header after pas is created
        step_emissors = proces_obj.get_emisor_steps(cursor, uid,
                                                    sw.proces_id.name,
                                                    sw.whereiam,
                                                    context)
        enviament = (pas in step_emissors)

        # Set notificacio_pendent on sw.step.header after pas is created
        notificable_cases = conf_obj.get(
            cursor, uid,
            'atr_gas_mail_user_notification_on_activation', False
        )
        if notificable_cases == 'all':
            notificacio = True
        elif notificable_cases:
            pas_name = '{}-{}'.format(
                model_name.split('.')[-2].upper(), model_name.split('.')[-1]
            )
            notificable_cases = eval(notificable_cases)
            notificacio = True if pas_name in notificable_cases else False
        else:
            notificacio = False

        header_id = pas_obj.read(
            cursor, uid, pas_id, ['header_id'])['header_id'][0]
        header_vals = {
            'enviament_pendent': enviament,
            'notificacio_pendent': notificacio,
        }
        header_obj.write(cursor, uid, header_id, header_vals)

    def _get_passos(self, cursor, uid, context=None):

        step_obj = self.pool.get('giscegas.atr.step')

        if not context:
            context = {}
        res = []
        step_ids = step_obj.search(cursor, uid, [], context=context)
        for step in step_obj.browse(cursor, uid, step_ids, context=context):
            step_model = step.get_step_model()
            step_name = '%s (%s)' % (step.proces_id.name, step.name)
            res.append((step_model, step_name))

        return res

    def _get_default_sw(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('sw_id', False)

    def _get_default_proces(self, cursor, uid, context=None):
        if not context:
            context = {}
        sw_id = context.get('sw_id', False)
        if sw_id:
            sw_obj = self.pool.get('giscegas.atr')
            return sw_obj.read(cursor, uid,
                               sw_id, ['proces_id'])['proces_id'][0]

        return False

    def _ff_get_info(self, cursor, uid, ids, field_name, arg, context=None):
        '''returns string with step info'''

        step_obj = self.pool.get('giscegas.atr.step')

        res = {}
        for step_info in self.browse(cursor, uid, ids, context=context):
            info = '%s (%s)' % (step_info.proces_id.name,
                                step_info.step_id.name)
            if step_info.pas_id:
                step_model = self.pool.get(step_info.pas_id.split(',')[0])
                step_id = int(step_info.pas_id.split(',')[1])
                step = step_model.browse(cursor, uid, step_id)
                info += ': %s (%s) -> %s (%s) ' % (step.emisor_id.name,
                                                 step.emisor_id.codi_sifco,
                                                 step.receptor_id.name,
                                                 step.receptor_id.codi_sifco)
                if 'rebuig' in step_model.fields_get_keys(cursor, uid):
                    if step.rebuig:
                        info += _(u"--Rechazo--")
                    else:
                        info += _(u"--Aceptación--")

            res[step_info.id] = info

        return res

    _columns = {
        'step_id': fields.many2one('giscegas.atr.step',
                                   'Paso', required=True),
        'proces_id': fields.many2one('giscegas.atr.proces', 'Proceso',
                                     required=True),
        'pas_id': fields.reference('Info del paso', selection=_get_passos,
                                   size=128),
        'sw_id': fields.many2one('giscegas.atr', 'Caso',
                                 ondelete="cascade",
                                 required=True, readonly=True),
        'name': fields.function(_ff_get_info, method=True,
                                type='char', size=200,
                                string='Info',
                                store=False)

    }

    _defaults = {
        'sw_id': _get_default_sw,
        'proces_id': _get_default_proces,
    }
GiscegasAtrStepInfo()


class GiscegasAtr2(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    _columns = {
        'step_ids': fields.one2many('giscegas.atr.step.info', 'sw_id',
                                    'Pasos generados',)
    }

GiscegasAtr2()


class GiscegasAtrMotiuRebuig(osv.osv):

    _name = 'giscegas.atr.motiu.rebuig'

    def get_motiu(self, cursor, uid, code, context=None):
        '''returns motiu id with name = code'''
        code = code.lstrip("0")
        search_params = [('name', '=', code)]
        motiu_ids = self.search(cursor, uid, search_params)
        if motiu_ids:
            return motiu_ids[0]
        return False

    def get_all_motius(self, cursor, uid, process_name):
        ids = self.search(cursor, uid, [('proces_ids.name', '=', process_name)])
        Motiu = namedtuple('Motiu', ['id', 'name', 'text'])

        return [Motiu(x['id'], x['name'], x['text'])
                for x in self.read(cursor, uid, ids, ['name', 'text'])]

    _columns = {
        'proces_ids': fields.many2many('giscegas.atr.proces',
                                       'atr_gas_motiu_rebuig_proces',
                                       'motiu_id', 'proces_id',
                                       'Procesos'),
        'name': fields.char('Codigo', size=3),
        'text': fields.char('Descripción', size=200),
    }

GiscegasAtrMotiuRebuig()


class GiscegasAtrStage(osv.osv):
    '''Defines staging messages'''

    _name = 'giscegas.atr.stage'
    _desc = '(SW) Etapes'
    _rec_name = 'text'

    def get_stages(self, cursor, uid, proces, step, where, context=None):
        '''Returns all stages associated'''

        search_params = [('proces_id.name', '=', proces),
                         ('step_ids.name', '=', step),
                         ('where', '=', where)]

        return self.search(cursor, uid, search_params, context=context)

    _columns = {
        'proces_id': fields.many2one('giscegas.atr.proces', 'Procéso'),
        'step_ids': fields.many2many('giscegas.atr.step',
                                     'atr_gas_stage_step',
                                     'stage_id', 'step_id',
                                     'Passos'),
        'where': fields.selection([('distri', 'Distribución'),
                                   ('comer', 'Comercialización')], 'On som'),
        'text': fields.char('Descripción', translate=True, size=250)
    }


GiscegasAtrStage()


class GiscegasAtrVariableinf(osv.osv):

    _name = 'giscegas.atr.variableinf'

    def generar_xml(self, cursor, uid, pas, context=None):
        varlist_xml = a1_48.variableinflist()
        var_list = []
        for var in pas.variableinflist:
            var_xml = a1_48.variableinf()
            var_xml.feed({
                'moreinformationtype': var.moreinformationtype,
                'description': var.description,
                'variabletype': var.variabletype,
                'variablevalue': var.variablevalue
            })
            var_list.append(var_xml)
        varlist_xml.feed({'variableinf_list': var_list})
        return varlist_xml

    _columns = {
        'moreinformationtype': fields.selection(TAULA_TIPO_INFORMACION_ADICIONAL, 'Tipo información adidcional'),
        'description': fields.text('Descripción información adidcional', size=255),
        'variabletype': fields.selection(TAULA_TIPO_VARIABLE_APORTACION_INFORMACION, 'Tipo de la variable'),
        'variablevalue': fields.char('Valor de la variable', size=45),
    }

GiscegasAtrVariableinf()


class GiscegasAtrReferenciaReclamacio(osv.osv):

    _name = 'giscegas.atr.referencia.reclamacio'
    _desc = 'Estructura Informativa para la Reclamación'

    def generar_xml(self, cursor, uid, pas, context=None):

        claims_xml = a1_48.claimreferencelist()

        claim_list = []

        for rec in pas.claimreferencelist:
            claim_xml = a1_48.claimreference()
            # Contact
            rec_contact = False
            if rec.contactname or rec.contactemail:
                rec_contact = a1_48.contact()
                tel = a1_48.contacttelephone()
                tel.feed({
                    'telephoneprefix': rec.telephoneprefix,
                    'telephonenumber': rec.telephonenumber,
                })
                rec_contact.feed({
                    'contactname': rec.contactname,
                    'contacttelephone': tel,
                    'contactemail': rec.contactemail
                })
            # rec_incidentperiod
            rec_incidentperiod = False
            if rec.incidentperiod_datefrom:
                rec_incidentperiod = a1_48.incidentperiod()
                rec_incidentperiod.feed({
                    'datefrom': rec.incidentperiod_datefrom,
                    'dateto': rec.incidentperiod_dateto,
                })
            # rec_incidentlocation
            rec_incidentlocation = False
            if rec.incidentlocationdesc:
                rec_incidentlocation = a1_48.incidentlocation()
                rec_incidentlocation.feed({
                    'incidentlocationdesc': rec.incidentlocationdesc,
                    'incidentlocationprovince': rec.incidentlocationprovince and rec.incidentlocationprovince.code,
                    'incidentlocationcity': rec.incidentlocationcity and rec.incidentlocationcity.ine,
                    'incidentlocationcitysubdivision': rec.incidentlocationcitysubdivision,
                    'incidentlocationzipcode': rec.incidentlocationzipcode,
                })
            # rec_reading
            rec_reading = False
            if rec.readingdate:
                rec_reading = a1_48.reading()
                rec_reading.feed({
                    'readingdate': rec.readingdate,
                    'readingvalue': rec.readingvalue,
                })
            # rec_incident
            rec_incident = False
            if rec.incidentdate:
                rec_incident = a1_48.incident()
                rec_incident.feed({
                    'incidentdate': rec.incidentdate,
                })
            # rec_client
            rec_client = False
            if rec.documentnum:
                cli_document = a1_48.document()
                cli_document.feed({
                    'documenttype': rec.documenttype,
                    'documentnum': rec.documentnum,
                })
                cli_name = a1_48.cname()
                if rec.firstname and rec.familyname1:
                    cli_name.feed({
                        'firstname': rec.firstname,
                        'familyname1': rec.familyname1,
                        'familyname2': rec.familyname2,
                    })
                else:
                    cli_name.feed({
                        'businessName': rec.businessname or rec.firstname,
                    })

                cli_telephone = a1_48.telephone()
                cli_telephone.feed({
                    'telephoneprefix': rec.client_telephoneprefix,
                    'telephonenumber': rec.client_telephonenumber,
                })
                cli_clientAddress = a1_48.clientAddress()
                cli_clientAddress.feed({
                    'province': rec.province and rec.province.code,
                    'city': rec.city and rec.city.ine,
                    'zipcode': rec.zipcode,
                    'streettype': rec.streettype,
                    'street': rec.street,
                    'streetnumber': rec.streetnumber,
                    'portal': rec.portal,
                    'staircase': rec.staircase,
                    'floor': rec.floor,
                    'door': rec.door,
                })
                rec_client = a1_48.client()
                rec_client.feed({
                    'document': cli_document,
                    'titulartype': rec.titulartype,
                    'cname': cli_name,
                    'telephone': cli_telephone,
                    'email': rec.email,
                    'clientAddress': cli_clientAddress,
                })

            # Reclamacio
            claim_xml.feed({
                'wrongattentiontype': rec.wrongattentiontype,
                'comreferencenum': rec.comreferencenum,
                'targetclaimcomreferencenum': rec.targetclaimcomreferencenum,
                'conceptcontract': rec.conceptcontract,
                'conceptfacturation': rec.conceptfacturation,
                'contact': rec_contact,
                'nnssexpedient': rec.nnssexpedient,
                'fraudrecordnum': rec.fraudrecordnum,
                'incidentperiod': rec_incidentperiod,
                'invoicenumber': rec.invoicenumber,
                'incidentlocation': rec_incidentlocation,
                'reading': rec_reading,
                'incident': rec_incident,
                'client': rec_client,
                'claimedcompensation': rec.claimedcompensation,
                'iban': rec.iban,
            })

            claim_list.append(claim_xml)
        claims_xml.feed({'claimreferencelist': claim_list})
        return claims_xml

    _columns = {
        'wrongattentiontype': fields.selection(TAULA_TIPOS_DE_ATENCION_INCORRECTA, 'Tipo de atención incorrecta'),
        'comreferencenum': fields.char(string='Código Sol. Comercializadora', size=12),
        'targetclaimcomreferencenum': fields.char(string='Referencia Recl. Previa', size=12),
        'conceptcontract': fields.selection(TAULA_CONCEPTOS_DE_CONTRATACION, 'Concepto de contratación'),
        'conceptfacturation': fields.selection(TAULA_CONCEPTOS_FACTURADOS, 'Concepto de facturación'),

        # contact
        'contactname': fields.char('Nombre completo contacto', size=100),
        'telephoneprefix': fields.char(string='Contacto. Prefijo teléfono', size=4),
        'telephonenumber': fields.char(string='Contacto Teléfono', size=20),
        'contactemail': fields.char(string='Contacto. Correo Electrónico', size=100),

        'nnssexpedient': fields.char(string='Código de expediente de NNSS', size=20),
        'fraudrecordnum': fields.char(string='Código de expediente de Fraude', size=20),

        # incidentperiod
        'incidentperiod_datefrom': fields.date(string='Fecha desde'),
        'incidentperiod_dateto': fields.date(string='Fecha hasta'),

        'invoicenumber': fields.char(string='Número de Factura', size=17),

        # incidentlocation
        'incidentlocationdesc': fields.char(string='Descripción de ubicación de la incidencia', size=45),
        'incidentlocationprovince': fields.many2one('res.country.state', 'Provincia'),
        'incidentlocationcity': fields.many2one('res.municipi', 'Municipio'),
        'incidentlocationcitysubdivision': fields.char(string='Código INE de Población', size=9),
        'incidentlocationzipcode': fields.char(string='Código Postal', size=5),

        # reading
        'readingdate': fields.date(string='Fecha de Lectura Actual'),
        'readingvalue': fields.float(string='Lectura Actual Contador', digits=(16,2)),

        # incident
        'incidentdate': fields.date(string='Fecha de la Incidencia'),

        # client
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, 'Tipo de Documento'),
        'documentnum': fields.char(string='Número de Documento', size=20),
        'titulartype': fields.selection(TAULA_TIPO_PERSONA, 'Tipo de Titular'),
        'firstname': fields.char(string='Nombre', size=40),
        'familyname1': fields.char(string='Apellido 1', size=30),
        'familyname2': fields.char(string='Apellido 2', size=30),
        'businessname': fields.char(string='Razón Social', size=40),
        'client_telephoneprefix': fields.char(string='Prefijo Teléfono', size=4),
        'client_telephonenumber': fields.char(string='Cliente Teléfono', size=20),
        'email': fields.char(string='Correo Electrónico', size=100),
        'province': fields.many2one('res.country.state', 'Cliente Provincia'),
        'city': fields.many2one('res.municipi', 'Cliente Municipio'),
        'zipcode': fields.char(string='Cliente CP', size=5),
        'streettype': fields.selection(TAULA_TIPO_VIA, 'Cliente Tipo de Via INE'),
        'street': fields.char(string='Cliente Via', size=60),
        'streetnumber': fields.char(string='Cliente Número de Vía', size=4),
        'portal': fields.char(string='Cliente Portal', size=5),
        'staircase': fields.char(string='Cliente Escalera', size=5),
        'floor': fields.char(string='Cliente Piso', size=5),
        'door': fields.char(string='Puerta', size=5),

        'claimedcompensation': fields.float(string='Indemnización Solicitada', digits=(16,2)),
        'iban': fields.char(string='Cuenta IBAN', size=34)
    }


GiscegasAtrReferenciaReclamacio()


class GiscegasAtrRebuig(osv.osv):
    '''Classes general per definir els models:
    * RechazoATRDistribuidoras
    * RechazoAnulacion'''

    _name = 'giscegas.atr.rebuig'

    def onchange_motiu(self, cursor, uid, ids, motiu_id, context=None):

        motiu_obj = self.pool.get('giscegas.atr.motiu.rebuig')
        res = {}
        if motiu_id:
            motiu = motiu_obj.browse(cursor, uid, motiu_id)
            res['value'] = {'desc_rebuig': motiu.text}

        return res

    def generar_xml(self, cursor, uid, pas, context=None):
        raise osv.except_osv("Atención!",
                             "Generación de rechazos XML no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        motiu_obj = self.pool.get('giscegas.atr.motiu.rebuig')

        if not context:
            context = {}

        motiu_id = motiu_obj.get_motiu(cursor, uid, xml.resultreason)
        vals = {
            'motiu_rebuig': motiu_id,
            'desc_rebuig': xml.resultreasondesc,
        }

        rids = self.create(cursor, uid, vals)
        return [rids]

    def dummy_create(self, cursor, uid, sw, motius, context=None):
        raise osv.except_osv("Atención!",
                             "Creación de rechazos XML no implementada")

    _columns = {
        'seq_rebuig': fields.integer('Sequencial', required=True),
        'motiu_rebuig': fields.many2one('giscegas.atr.motiu.rebuig',
                                        "Motiu de rebuig", required=True),
        'desc_rebuig': fields.char(u"Descripció", size=250, required=True),
    }

    _defaults = {
        'seq_rebuig': lambda *a: 1,
    }


GiscegasAtrRebuig()


class GiscegasAtrDocument(osv.osv):

    _name = 'giscegas.atr.document'
    _desc = '(SW) Documents aportats'

    def create_docs(self, cr, user, docs, context=None):
        '''Create docs
        @docs: unordered list of tuples containing (type, url)
        return list of ids created'''
        res = []
        if not docs:
            return res
        for index, doc in enumerate(docs):
            docs_vals = {
                'doctype': doc.doctype,
                'url': doc.url,
                'date': doc.date,
                'extrainfo': doc.extrainfo
            }
            created_doc_id = self.create(cr, user, docs_vals, context=context)

            res.append(created_doc_id)

        return res

    def generar_xml(self, cursor, uid, pas, context=None):

        docs_xml = a1_41.Registerdoclist()

        doc_list = []

        for doc in pas.document_ids:
            doc_xml = a1_41.Registerdoc()
            doc_xml.feed({
                'doctype': doc.doctype,
                'url': doc.url,
                'date': doc.date,
                'extrainfo': doc.extrainfo
            })

            doc_list.append(doc_xml)
        docs_xml.feed({'registerdoc_list': doc_list})
        return docs_xml

    _columns = {
        'date': fields.date("Fecha"),
        'extrainfo': fields.text("Información Extra"),
        'doctype': fields.selection(TAULA_TIPOS_DOCUMENTOS_ANEXOS, 'Tipo de Documento', required=True),
        'url': fields.char('URL del document', size=255, required=True),
    }

GiscegasAtrDocument()


class GiscegasAtrDefect(osv.osv):

    _name = 'giscegas.atr.defect'

    def create_from_xml_defect(self, cr, user, xml_elements, context=None):
        res = []
        if not xml_elements:
            return res
        for elem in xml_elements:
            vals = {
                'code': elem.code,
                'description': elem.description,
            }
            created_id = self.create(cr, user, vals, context=context)

            res.append(created_id)

        return res

    _columns = {
        'code': fields.selection(TAULA_ANOMALIA_INSTALACION, 'Código Anomalía'),
        'description': fields.text('Descripción Anomalía', size=240),
    }


GiscegasAtrDefect()


class GiscegasAtrInformation(osv.osv):

    _name = 'giscegas.atr.information'

    def create_from_xml_information(self, cr, user, xml_elements, context=None):
        res = []
        if not xml_elements:
            return res
        for elem in xml_elements:
            vals = {
                'moreinformation': elem.moreinformation,
                'moreinformationtype': elem.moreinformationtype,
                'limitsenddate': elem.limitsenddate,
            }
            created_id = self.create(cr, user, vals, context=context)

            res.append(created_id)

        return res

    _columns = {
        'moreinformation': fields.text('Más Información', size=600),
        'moreinformationtype': fields.selection(TAULA_TIPO_INFORMACION_ADICIONAL, 'Tipo de la información adicional'),
        'limitsenddate': fields.date('Fecha límite de envío de la información adicional'),
    }


GiscegasAtrInformation()


class GiscegasAtrCounter(osv.osv):
    _name = 'giscegas.atr.counter'
    _desc = 'Contador ATR Gas'

    def create_from_xml_counter(self, cr, user, counterlist, context=None):
        res = []
        if not counterlist:
            return res
        for c in counterlist:
            c_vals = {
                'countermodel': c.countermodel,
                'countertype': c.countertype,
                'counternumber': c.counternumber,
                'counterproperty': c.counterproperty,
                'reallecture': c.reallecture,
                'counterpressure': c.counterpressure,
            }
            created_id = self.create(cr, user, c_vals, context=context)

            res.append(created_id)

        return res

    def activa_comptador_a_polissa(self, cursor, uid, pas, context=None):
        """ Activem i o creem el comptador i l'associem a la pólissa
            Si s'escau, també podem carregar les lectures"""

        logger = netsvc.Logger()

        if not context:
            context = {}

        compta_obj = self.pool.get('giscegas.lectures.comptador')
        counter_obj = self.pool.get('giscegas.atr.counter')
        pol_obj = self.pool.get('giscegas.polissa')

        step_header = pas.header_id
        polissa_id = step_header.sw_id.cups_polissa_id.id
        sw = step_header.sw_id

        comptadors_afegits = []
        comptadors_baixa = []
        errors = []

        data_alta = pas.transfereffectivedate
        counter_ids = [c.id for c in step_header.counter_ids]
        for counter in counter_obj.browse(cursor, uid, counter_ids):
            polissa = pol_obj.browse(cursor, uid, polissa_id)
            # Només activem el comptadors que que són de tipus
            # o tenen lectures:
            num_serie = counter.counternumber

            search_vals = [('name', '=', num_serie),
                           ('polissa', '=', polissa.id)]
            compta_id = compta_obj.search(cursor, uid, search_vals,
                                          context={'active_test': False})
            # mirem si el mateix número de comptador (traien els zeros)
            # existeix
            if not compta_id:
                for meter in polissa.comptadors:
                    try:
                        num_serie_int = int(num_serie)
                        meter_name = int(meter.name)
                    except:
                        meter_name = False
                        num_serie_int = False

                    if meter_name and meter_name == num_serie_int:
                        compta_id = [meter.id]
                        break

            # Si la pólissa ja té un comptador de alta,
            # el donem de baixa
            actual_meters = [
                c for c in polissa.comptadors
                if c.name != num_serie and c.active
                   and c.name not in comptadors_afegits
            ]
            for actual_meter in actual_meters:
                vals = {'active': False,
                        'data_baixa': data_alta}
                actual_meter.write(vals)
                comptadors_baixa.append(actual_meter.name)
            # Creem el comptador
            vals = {'name': num_serie,
                    'polissa': polissa.id,
                    'data_alta': data_alta,
                    'active': True,
                    'propietat': counter.counterproperty,
                    'lloguer': False}

            compta_id = compta_obj.create(cursor, uid, vals)
            if compta_id:
                comptadors_afegits.append(u"{}".format(num_serie))

        txt = _(u"[{}] {} [{}] -> Se han creado los contadores: {}").format(
            sw.id, sw.name, sw.step_id.name,
            ','.join(comptadors_afegits)
        )

        logger.notifyChannel('comptadors_atr', netsvc.LOG_INFO, txt)

        res = {
            'comptadors': comptadors_afegits,
            'baixes': comptadors_baixa,
            'errors': errors
        }

        return res

    _columns = {
        'countermodel': fields.char("Marca contador", size=50),
        'countertype': fields.char("Tipo contador", size=7),
        'counternumber': fields.char("Número contador", size=18),
        'counterproperty': fields.selection(TAULA_PROPIEDAD_CONTADOR_CORRECTOR, "Propiedad del contador"),
        'reallecture': fields.integer("Lectura bruta"),
        'counterpressure': fields.float("Presión Contador", digits=(8, 3)),
    }


GiscegasAtrCounter()


class GiscegasAtrCorrector(osv.osv):

    _name = 'giscegas.atr.corrector'
    _desc = 'Corrector ATR Gas'

    def create_from_xml_corrector(self, cr, user, correctorlist, context=None):
        res = []
        if not correctorlist:
            return res
        for c in correctorlist:
            c_vals = {
                'correctormodel': c.correctormodel,
                'correctortype': c.correctortype,
                'correctornumber': c.correctornumber,
                'correctorproperty': c.correctorproperty,
                'correctedlecture': c.correctedlecture,
            }
            created_id = self.create(cr, user, c_vals, context=context)

            res.append(created_id)

        return res

    _columns = {
        'correctormodel': fields.char("Marca Corrector", size=50),
        'correctortype': fields.selection(TAULA_TIPO_CORRECTOR, "Tipo Corrector"),
        'correctornumber': fields.char("Número Corrector", size=18),
        'correctorproperty': fields.selection(TAULA_PROPIEDAD_CONTADOR_CORRECTOR, "Propiedad del corrector"),
        'correctedlecture': fields.integer("Lectura Corregida"),
    }

GiscegasAtrCorrector()


class GiscegasAtrStepHeader(osv.osv):

    _name = 'giscegas.atr.step.header'
    _rec_name = 'sw_id'

    def unlink(self, cursor, uid, ids, context=None):
        '''unlink many2many associated models'''
        stepinfo_obj = self.pool.get("giscegas.atr.step.info")
        reb_obj = self.pool.get('giscegas.atr.rebuig')
        doc_obj = self.pool.get('giscegas.atr.document')
        counter_obj = self.pool.get('giscegas.atr.counter')
        corrector_obj = self.pool.get('giscegas.atr.corrector')
        variableinf_obj = self.pool.get('giscegas.atr.variableinf')
        defect_obj = self.pool.get('giscegas.atr.defect')
        info_obj = self.pool.get('giscegas.atr.information')
        claimref_obj = self.pool.get('giscegas.atr.referencia.reclamacio')
        for header in self.browse(cursor, uid, ids, context=context):
            if header.rebuig_ids:
                reb_ids = [reb.id for reb in header.rebuig_ids]
                reb_obj.unlink(cursor, uid, reb_ids)
            if header.document_ids:
                document_ids = [doc.id for doc in header.document_ids]
                doc_obj.unlink(cursor, uid, document_ids)
            if header.counter_ids:
                counter_ids = [c.id for c in header.counter_ids]
                counter_obj.unlink(cursor, uid, counter_ids)
            if header.corrector_ids:
                corrector_ids = [c.id for c in header.corrector_ids]
                corrector_obj.unlink(cursor, uid, corrector_ids)
            if header.defect_ids:
                defect_ids = [c.id for c in header.defect_ids]
                defect_obj.unlink(cursor, uid, defect_ids)
            if header.information_ids:
                information_ids = [c.id for c in header.information_ids]
                info_obj.unlink(cursor, uid, information_ids)
            if header.claimreferencelist:
                claimref_ids = [c.id for c in header.claimreferencelist]
                claimref_obj.unlink(cursor, uid, claimref_ids)
            if header.variableinflist:
                variableinf_ids = [c.id for c in header.variableinflist]
                variableinf_obj.unlink(cursor, uid, variableinf_ids)

            # Obtenim eliminem els step_info que no tinguin pas_id
            # (en aquest punt ja s'ha eliminat el pas)
            for step in header.sw_id.step_ids:
                if not step.read(['pas_id'])[0].get('pas_id'):
                    stepinfo_obj.unlink(cursor, uid, step.id, context=context)
        return super(GiscegasAtrStepHeader,
                     self).unlink(cursor, uid, ids, context=context)

    def generar_xml(self, cursor, uid, ids, pas, context=None):
        """Retorna la sol·licitud en format XML C2, pas 07
        """
        if not context:
            context = {}
        meta_ctx = context.get('meta', {})
        if meta_ctx.get('store_attachment', True):
            if context.get('sortint', False):
                #Check for comer sortint

                if pas.sw_id.comer_sortint_id.codi_sifco != pas.receptor_id.codi_sifco:
                    raise osv.except_osv('Error',
                                _(u"La comercialzadora saliente no se corresponde "
                                  u"al receptor del paso"))
        capcalera_vals = pas.sw_id.get_header_vals()
        header = pas.header_id

        capcalera = a1_41.Heading()
        capcalera_vals.update({
            'dispatchingcompany': header.emisor_id.codi_sifco,
            'destinycompany': header.receptor_id.codi_sifco,
            'communicationsdate': header.date_created.split(" ")[0],
            'communicationshour': header.date_created.split(" ")[1],
            'messagetype': pas._nom_pas.upper(),

        })
        capcalera.feed(capcalera_vals)
        return capcalera

    def generar_xml_document(self, cursor, uid, ids, pas, context=None):

        document_obj = self.pool.get('giscegas.atr.document')
        return document_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_variableinf(self, cursor, uid, ids, pas, context=None):
        variableinf_obj = self.pool.get('giscegas.atr.variableinf')
        return variableinf_obj.generar_xml(cursor, uid, pas, context=context)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')

        data_i_hora = xml.data_sollicitud
        data_creacio = datetime.strptime(data_i_hora, '%Y-%m-%d %H:%M:%S')
        part_obj = self.pool.get("giscemisc.participant")
        emisor = sw_obj.check_partner(cursor, uid, xml.get_codi_emisor)
        emisor = part_obj.get_id_from_partner(cursor, uid, emisor)
        receptor = sw_obj.check_partner(cursor, uid, xml.get_codi_destinatari)
        receptor = part_obj.get_id_from_partner(cursor, uid, receptor)

        res = {
            'sw_id': sw_id,
            'emisor_id': emisor,
            'receptor_id': receptor,
            'date_created': data_creacio,
        }

        return res

    def create_from_xml_doc(self, cursor, uid, xml, context=None):
        doc_obj = self.pool.get('giscegas.atr.document')
        return doc_obj.create_docs(cursor, uid, xml, context=context)

    def create_from_xml_counters(self, cursor, uid, xml, context=None):
        counter_obj = self.pool.get('giscegas.atr.counter')
        return counter_obj.create_from_xml_counter(cursor, uid, xml, context=context)

    def create_from_xml_correctors(self, cursor, uid, xml, context=None):
        corrector_obj = self.pool.get('giscegas.atr.corrector')
        return corrector_obj.create_from_xml_corrector(cursor, uid, xml, context=context)

    def create_from_xml_defect(self, cursor, uid, xml, context=None):
        defect_obj = self.pool.get('giscegas.atr.defect')
        return defect_obj.create_from_xml_defect(cursor, uid, xml, context=context)

    def create_from_xml_information(self, cursor, uid, xml, context=None):
        information_obj = self.pool.get('giscegas.atr.information')
        return information_obj.create_from_xml_information(cursor, uid, xml, context=context)

    def get_telephone_number(self, cursor, uid, sw, context=None):
        ''' Gets telephone number '''
        res = {}
        if context is None:
            context = {}

        pol_obj = self.pool.get("giscegas.polissa")
        direccio = pol_obj.get_address_with_phone(
            cursor, uid, sw.cups_polissa_id.id, context=context
        )

        if direccio:
            res.update({
                'telefon': direccio.phone or direccio.mobile
            })
        return res

    def get_contact_info(self, cursor, uid, sw, context=None):
        res = {}
        if context is None:
            context = {}

        model_name = context.get('model', False)
        if not model_name:
            model_name = sw.step_id.get_step_model()
        model_obj = self.pool.get(model_name)
        if 'contactphonenumber' in model_obj.fields_get(cursor, uid).keys():
            direccio = sw.cups_polissa_id.direccio_notificacio
            if not direccio or (not direccio.phone and not direccio.mobile):
                direccio = sw.cups_polissa_id.direccio_pagament
            if direccio:
                cont_name = (direccio.partner_id and direccio.partner_id.name or
                             sw.cups_polissa_id.titular.name)
                res.update({
                    'contactphonenumber': direccio.phone or direccio.mobile,
                })
        return res

    def clean_phone(self, cursor, uid, tel_number):
        '''Per poder cridar-la des de ooop'''
        return self.clean_tel_number(tel_number)

    def clean_tel_number(self, tel_number):
        ''' Strips non-number chars from telephone number except "+"'''
        res = {'tel': tel_number, 'pre': '' , 'fuzzy': False}
        if not tel_number:
            return res
        tel_net = ''.join([c for c in tel_number if c.isdigit()])
        tel = tel_net
        pre = '34'
        fuzzy = False
        if len(tel_net) > 9:
            pre = tel_net[:-9][-2:]
            tel = tel_net[-9:]
            fuzzy = len(tel) + len(pre) < len(tel_net)

        res = {'tel': tel, 'pre': pre, 'fuzzy': fuzzy}
        return res

    def get_cognoms(self, cursor, uid, sw, context=None):
        ''' splits surnames an name'''
        if context is None:
            context = {}
        res = pas_vals = {}
        model_name = context.get('model', False)
        if not model_name:
            model_name = sw.step_id.get_step_model()
        model_obj = self.pool.get(model_name)
        if 'familyname1' in model_obj.fields_get(cursor, uid).keys() or \
                'firstname' in model_obj.fields_get(cursor, uid).keys():
            #Arreglem nom i cognoms
            partner_obj = self.pool.get('res.partner')

            nom_titular = sw.cups_polissa_id.titular.name
            nom = partner_obj.separa_cognoms(cursor, uid, nom_titular)

            vat_enterprise = sw.vat_enterprise()
            warn_nom = True
            if not vat_enterprise:
                if not nom['fuzzy']:
                    warn_nom = False
                pas_vals = {'firstname': nom['nom'],
                            'familyname1': nom['cognoms'][0],
                            'familyname2': nom['cognoms'][1]}
            else:
                """ Empresa només té nom"""
                if len(nom_titular) <= 50 and not nom['fuzzy']:
                    warn_nom = False
                pas_vals = {'firstname': '',
                            'familyname1': nom_titular,
                            'familyname2': ''}
            if warn_nom:
                pas_vals.update({'validacio_pendent': True})

            res.update(pas_vals)

        if 'newfamilyname1' in model_obj.fields_get(cursor, uid).keys():
            # Arreglem nom i cognoms
            partner_obj = self.pool.get('res.partner')

            nom_titular = sw.cups_polissa_id.titular.name
            nom = partner_obj.separa_cognoms(cursor, uid, nom_titular)

            vat_enterprise = sw.vat_enterprise()
            warn_nom = True
            if not vat_enterprise:
                if not nom['fuzzy']:
                    warn_nom = False
                pas_vals = {'newfirstname': nom['nom'],
                            'newfamilyname1': nom['cognoms'][0],
                            'newfamilyname2': nom['cognoms'][1]}
            else:
                """ Empresa només té nom"""
                if len(nom_titular) <= 50 and not nom['fuzzy']:
                    warn_nom = False
                pas_vals = {'newfirstname': '',
                            'newfamilyname1': nom_titular,
                            'newfamilyname2': ''}
            if warn_nom:
                pas_vals.update({'validacio_pendent': True})

            res.update(pas_vals)

        return res

    def dummy_create(self, cursor, uid, sw, context=None):
        '''create with dummy values'''
        sw_obj = self.pool.get('giscegas.atr')
        if not context:
            context = {}
        receptor_id = sw.partner_id.id
        part_obj = self.pool.get("giscemisc.participant")
        receptor_id = part_obj.get_id_from_partner(cursor, uid, receptor_id)
        if context.get('sortint', False):
            if not sw.comer_sortint_id:
                raise osv.except_osv('Error',
                                 _(u"Se debe completar la comercializadora"
                                   u" saliente para poder generar el paso"))
            receptor_id = sw.comer_sortint_id.id

        res = {'sw_id': sw.id,
               'emisor_id': sw.company_id.id,
               'receptor_id': receptor_id,
               }
        # Refresh sw object for pending triggers
        sw.write({})
        sw = sw_obj.browse(cursor, uid, sw.id)
        # split name an surname's
        if sw.cups_id and sw.cups_polissa_id and sw.cups_polissa_id.titular:
            pas_vals = self.get_cognoms(cursor, uid, sw, context=context)
            # import telephone number
            tel = self.get_telephone_number(
                cursor, uid, sw, context=context
            ).get('telefon', '')
            pas_vals.update({
                'telephone1': tel,
                'newtelephone1': tel
            })

            pas_vals.update(self.get_contact_info(cursor, uid, sw, context=context))
            res.update(pas_vals)

        return res

    def dummy_create_reb(self, cursor, uid, sw, motius, context=None):
        reb_obj = self.pool.get('giscegas.atr.rebuig')
        return reb_obj.dummy_create(cursor, uid, sw, motius, context=context)

    def set_enviament_pendent(self, cursor, uid, ids, pendent, context=None):
        """Marca el pas com a XML pendent d'enviar (o no)"""
        if not context:
            context = {}
        header = self.browse(cursor, uid, ids, context=context)[0]
        header.write({'enviament_pendent': pendent})

        return True

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list,tuple)):
            ids = [ids]
        res = super(GiscegasAtrStepHeader, self).write(
            cursor, uid, ids, vals, context=context)
        self.update_additional_info(cursor, uid, ids, context)
        if 'enviament_pendent' in vals:
            self.update_deadline(cursor, uid, ids, context=context)
        # Force update _ff_accio_pendent_comerdist
        sw_ids = []
        for inf in self.read(cursor, uid, ids, ['sw_id'], context=context):
            if inf['sw_id']:
                sw_ids.append(inf['sw_id'][0])
        self.pool.get("giscegas.atr").update_ff_accio_pendent_comerdist(cursor, uid, sw_ids, context=context)
        return res

    def update_additional_info(self, cursor, uid, ids, context=None):
        """
        Actualitza el camp additional_info del cas de switching associat al
        header quan el pas associat al header és el pas actual del cas de
        switching
        """
        sw_obj = self.pool.get('giscegas.atr')
        if not isinstance(ids, list):
            ids = [ids]
        info = self.get_case_of_header(cursor, uid, ids)
        for hid in info.keys():
            pas_id, model = info.get(hid)
            sw_obj.update_additional_info(cursor, uid, pas_id, model,
                                          context=context)

    def update_deadline(self, cursor, uid, ids, context=None):
        sw_obj = self.pool.get('giscegas.atr')
        if not isinstance(ids, list):
            ids = [ids]
        info = self.get_case_of_header(cursor, uid, ids)
        for hid in info.keys():
            pas_id, model = info.get(hid)
            proces = model.split(".")[-2].upper()
            step = model.split(".")[-1]
            sw_id = self.read(cursor, uid, hid, ['sw_id'])['sw_id'][0]
            sw_obj.update_deadline(cursor, uid, sw_id, proces, step,
                                   context=context)

    def get_case_of_header(self, cursor, uid, header_ids, context=None):
        """ Returns a dict of tuples:
                {header_id: ( case_id, case_model )}
            with the case info. of each header_id.
        """
        if not isinstance(header_ids, list):
            header_ids = [header_ids]
        sw_obj = self.pool.get('giscegas.atr')
        step_info_obj = self.pool.get('giscegas.atr.step.info')
        res = {}
        # Get the switching cases of the headers
        sw_ids = self.read(cursor, uid, header_ids, ['sw_id'])
        for i in range(len(header_ids)):
            header_id = header_ids[i]
            sw_id = sw_ids[i]['sw_id'][0]
            # Get all steps info of switching case
            step_ids = sw_obj.read(cursor, uid, sw_id, ['step_ids'])['step_ids']
            # Search for each step if its header_id is the current one
            for step_inf_id in step_ids:
                pinfo = step_info_obj.read(cursor, uid, step_inf_id,
                                           ['step_id', 'proces_id'])
                model = "giscegas.atr.{0}.{1}".format(
                    pinfo['proces_id'][1].lower(), pinfo['step_id'][1]
                )
                pas_obj = self.pool.get(model)
                # Search if there is an step with the current header_id
                pas_ids = pas_obj.search(cursor, uid,
                                         [('header_id', '=', header_id)])
                if len(pas_ids) > 0:
                    res.update({header_id: (pas_ids[0], model)})
                    break
        return res

    def get_notification_mail(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        template = None
        templates = []
        data_model = self.pool.get('ir.model.data')
        sw_obj = self.pool.get('giscegas.atr')
        for id in ids:
            step = self.browse(cursor, uid, id, context)
            pas = sw_obj.get_pas(cursor, uid, step.sw_id)
            template_name = pas.get_notification_mail_name()[0]
            template = data_model.get_object_reference(
                cursor, uid, 'giscegas_atr', template_name
            )[1]
            templates.append(template)
        return templates

    _columns = {
        'sw_id': fields.many2one('giscegas.atr', 'Switching'),
        'emisor_id': fields.many2one('giscemisc.participant', 'Emisor',
                                     required=True),
        'receptor_id': fields.many2one('giscemisc.participant', 'Receptor',
                                       required=True),
        'date_created': fields.datetime('Fecha creación', required=True),
        'rebuig_ids': fields.many2many('giscegas.atr.rebuig',
                                       'atr_gas_step_header_rebuig_ref',
                                       'header_id', 'rebuig_id',
                                       'Rebuig'),
        # validacions
        'validacio_pendent': fields.boolean(u'Por validar'),
        'enviament_pendent': fields.boolean(u'Por enviar'),
        'notificacio_pendent': fields.boolean(u'Por notificar'),
        'document_ids': fields.many2many(
            'giscegas.atr.document', 'atr_gas_step_header_doc_ref',
            'header_id', 'document_id', 'Documents'
        ),
        'counter_ids': fields.many2many(
            'giscegas.atr.counter', 'atr_gas_step_header_counter_ref',
            'header_id', 'counter_id', 'Contadores'
        ),
        'corrector_ids': fields.many2many(
            'giscegas.atr.corrector', 'atr_gas_step_header_corrector_ref',
            'header_id', 'corrector_id', 'Correctores'
        ),
        'claimreferencelist': fields.many2many(
            'giscegas.atr.referencia.reclamacio', 'atr_gas_header_claim_ref',
            'header_id', 'claim_id', 'Reclamaciones'
        ),
        'variableinflist': fields.many2many(
            'giscegas.atr.variableinf', 'atr_gas_header_variableinf_ref',
            'header_id', 'var_id', 'Variables Información'
        ),
        'defect_ids': fields.many2many(
            'giscegas.atr.defect', 'atr_gas_step_header_defect_ref',
            'header_id', 'defect_id', 'Anomalias'
        ),
        'information_ids': fields.many2many(
            'giscegas.atr.information', 'atr_gas_step_header_information_ref',
            'header_id', 'information_id', 'Informaciones aportadas/solicitadas'
        ),
    }

    _defaults = {
        'date_created': lambda *a: datetime.strftime(datetime.now(),
                                                     '%Y-%m-%d %H:%M:%S'),
        'validacio_pendent': lambda *a: False,
        'enviament_pendent': lambda *a: True,
    }

GiscegasAtrStepHeader()


class GiscegasAtrNotificacioPas(osv.osv):
    """
    Model per les notificacions de passos de casos ATR
    """

    _name = 'giscegas.atr.notify'

    def get_notification_text(
            self, cursor, uid, notify_id, sw_id, context=False):
        from mako.template import Template
        if not context:
            context = {}
        if isinstance(notify_id, list):
            notify_id = notify_id[0]
        sw_obj = self.pool.get('giscegas.atr')
        sw = sw_obj.browse(cursor, uid, sw_id) if sw_id else {}
        notify = self.read(
            cursor, uid, notify_id, ['notify_text', 'rebuig_text', 'step_id'],
            context=context)
        pas = sw.get_pas(step_id=notify['step_id']) if sw else {}
        t = Template(text=notify['notify_text'])
        return t.render_unicode(cas=sw, pas=pas)

    def _ff_notifica_rebuig(
            self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        proc_obj = self.pool.get('giscegas.atr.proces')
        for notify in self.browse(cursor, uid, ids):
            if notify.proces_id and notify.step_id:
                res[notify.id] = (
                    notify.step_id.name in proc_obj.get_reject_steps(
                        cursor, uid, notify.proces_id.name
                    ))
            else:
                res[notify.id] = False
        return res

    def _ff_notifica_rebuig_search(
            self, cursor, uid, obj, field_name, args, context):
        ids = []
        notify_ids = self.search(
            cursor, uid, [], context={'active_test': False})
        for notify_id in notify_ids:
            rebuig = self._ff_notifica_rebuig(
                cursor, uid, [notify_id], field_name, args, context)
            if rebuig[notify_id]:
                ids.append(notify_id)
        return [('id', 'in', ids)]

    _columns = {
        'step_id': fields.many2one(
            'giscegas.atr.step', 'Paso del proceso'),
        'proces_id': fields.many2one(
            'giscegas.atr.proces', 'Proceso ATR'),
        'notify_text': fields.text('Texto para la notificación'),
        'rebuig_ids': fields.many2many(
            'giscegas.atr.motiu.rebuig',
            'giscegas_atr_notify_rebuigs_rel',
            id1='notify_id', id2='rebuig_id', string='Rechazos aceptados'),
        'active': fields.boolean('Notificación activa'),
        'notifica_rebuig': fields.function(
            _ff_notifica_rebuig,
            method=True,
            string="Notificación con Rechazo",
            type="boolean",
            fnct_search=_ff_notifica_rebuig_search)
    }

    _defaults = {
        'active': lambda *a: False
    }

GiscegasAtrNotificacioPas()


class GiscegasAtrLog(osv_mongodb.osv_mongodb):
    """Class to store atr cases log process"""

    _name = 'giscegas.atr.log'
    _description = 'Classe log per els casos atr'
    _rec_name = 'file'

    def create(self, cursor, uid, vals, context=None):
        '''Create log line or update one'''
        if context is None:
            context = {}
        if not context.get("update_logs"):
            return super(GiscegasAtrLog, self).create(
                cursor, uid, vals, context=context
            )

        data_actual = datetime.now() - timedelta(hours=1)
        data_actual = data_actual.strftime("%Y-%m-%d %H:%M:%S")
        search_params = [
            ('origin', '=', vals.get('origin')),
            ('file', '=', vals.get('file')),
            ('request_code', '=', vals.get('request_code')),
            ('pas', '=', vals.get('pas')),
            ('proces', '=', vals.get('proces')),
            ('codi_emissor', '=', vals.get('codi_emissor')),
            ('tipus', '=', vals.get('tipus')),
            ('cups_text', '=', vals.get('cups_text')),
            ('status', '=', 'error'),
            ('case_date', '>=', data_actual)
        ]
        log_exists = self.search(cursor, uid, search_params)
        if log_exists:
            update_data = {
                'case_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'status': vals['status'],
                'info': vals['info']
            }
            self.write(cursor, uid, log_exists, update_data)
            return log_exists[0]
        else:
            return super(GiscegasAtrLog, self).create(
                cursor, uid, vals, context=context
            )

    def name_get(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        res = []

        for record in self.read(cursor, uid, ids, ['file', 'origin'], context):
            name = record['file'] or record['origin']
            res.append((record['id'], name))
        return res

    def unlink(self, cursor, uid, ids, context=None):
        ir_attachment_obj = self.pool.get('ir.attachment')
        attachment_values = [
            ('res_id', 'in', ids),
            ('res_model', '=', 'giscegas.atr.log'),
        ]
        attach_ids = ir_attachment_obj.search(cursor, uid, attachment_values)
        res = super(GiscegasAtrLog, self).unlink(cursor, uid, ids, context=context)
        ir_attachment_obj.unlink(cursor, uid, attach_ids)
        return res

    _columns = {
        'case_date': fields.datetime('Fecha creación', select=True,
                                 required=True
                                 ),
        'origin': fields.char('Origen', size=128, required=True, select=1),
        'file': fields.char('Fichero', size=128, required=True, select=1),
        'request_code': fields.char('Codigo solicitud', size=12, select=1),
        'status': fields.selection([('error', 'Error'), ('correcte', 'Correcto')],
                                   'Estat', required=True, select=1
                                   ),
        'info': fields.text('Información', readonly=True),
        'pas': fields.char('Paso', size=2, select=1),
        'proces': fields.char('Proceso', size=2, select=1),
        'emisor_id': fields.many2one('res.partner', 'Emisor', readonly=True, select=1),
        'codi_emisor': fields.char('Codigo emisor', size=4),
        'cups': fields.many2one('giscegas.cups.ps', 'CUPS', readonly=True, select=1),
        'tipus': fields.selection(
            [('import', 'Importació'), ('export', 'Exportació'),
             ('generate', 'Generació')], 'Tipus',
            readonly=True
        ),
        'cups_text': fields.char('CUPS', size=128)
    }

    _defaults = {
        'case_date': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
    }

    _order = "case_date desc, id desc"


GiscegasAtrLog()


class GiscegasAtrActivacionsConfig(osv.osv):
    """Class to store atr cases log process"""

    _name = 'giscegas.atr.activation.config'
    _description = "Configuracion de activaciones ATR"

    def get_activation_method(self, cursor, uid, pas, context=None):
        """
        :param pas: browse object of step (giscegas.atr.{proces}.{step}) to activate.
        :return: string with method name or False
        """
        if context is None:
            context = {}
        logger = logging.getLogger('openerp')
        sw = pas.sw_id
        proces_id = sw.proces_id.id
        step_id = sw.step_id.id
        whereiam = sw.whereiam

        search_params = [
            ('proces_id', '=', proces_id),
            ('step_id', '=', step_id),
            ('comerdist', 'in', ["all", whereiam]),
            ('active', '=', True)
        ]
        if context.get("activate_only_automatic"):
            search_params.append(('is_automatic', '=', True))

        sql = self.q(cursor, uid).select(['id', 'conditions', 'method']).where(search_params)
        cursor.execute(*sql)
        res = []
        for info in cursor.dictfetchall():
            try:
                if info.get("conditions", "[]") != "Aplica siempre":
                    conditions = eval(info.get("conditions", "[]"))
                else:
                    conditions = []
            except Exception, e:
                logger.error("ATR: condiciones de la activacion {0} mal definidas: {1}".format(info.get('id'), e))
                continue
            # Si no te condicions vol dir que s'utilitza sempre
            if not len(conditions):
                res.append(info.get('method'))
                continue
            # Altrament hem de comprovar si passa les condicions
            ok = True
            for pas_name, camp, operant, valor in conditions:
                if isinstance(valor, str):
                    valor = "'{0}'".format(valor)
                pas_to_check = pas
                if pas_name != pas._nom_pas:
                    pas_to_check_obj = self.pool.get("giscegas.atr.{0}.{1}".format(sw.proces_id.name.lower(), pas_name))
                    pas_to_check_id = pas_to_check_obj.search(cursor, uid, [("sw_id", "=", sw.id)])
                    if not len(pas_to_check_id):
                        continue
                    pas_to_check = pas_to_check_obj.browse(cursor, uid, pas_to_check_id[0])
                if not eval("pas_to_check.{0} {1} {2}".format(camp, operant, valor)):
                    ok = False
                    break
            # Hem passat totes les condicions
            if ok:
                res.append(info.get('method'))
        return res

    _columns = {
        'proces_id': fields.many2one('giscegas.atr.proces', 'Proceso', required=True, select=1),
        'step_id': fields.many2one('giscegas.atr.step', 'Paso', required=True, select=1),
        'description': fields.text('Descripcion', required=True, select=1),
        'method': fields.char('Metodo activacion', size=64),
        'conditions': fields.char("Condiciones Extras", size=256, help=u"Condiciones a aplicar para poder realizar la activacion. Llistat de tuplas del tipo (pas_name, campo, operador, valor)."),
        'comerdist': fields.selection([('all', 'Comer. y Distri.'), ('comer', "Comercializadora"), ('distri',"Distribuidora")], 'Tipos'),
        'active': fields.boolean("Activo"),
        'is_automatic': fields.boolean(
            "Activación automática",
            help="Si se marca esta opción, la activación se ejecutará "
                 "automáticamente al importar el XML del paso informado."
        ),
    }

    _defaults = {
        'comerdist': lambda *a: 'all',
        'conditions': lambda *a: '[]',
        'active': lambda *a: True,
        'is_automatic': lambda *a: False,
    }

    _order = "proces_id asc, step_id asc, method"

GiscegasAtrActivacionsConfig()
