# -*- coding: utf-8 -*-
import json

from osv import osv, fields, orm
from tools.translate import _
from gestionatr.defs_gas import *
from gestionatr.defs import SINO


class GiscegasAtrWizard02(osv.osv_memory):

    _name = 'giscegas.atr.a102.wizard'

    def _whereiam(self, cursor, uid, context=None):
        """ returns 'distri' or 'comer' """
        cups_obj = self.pool.get('giscegas.cups.ps')
        sw_obj = self.pool.get('giscegas.atr')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        pol_obj = self.pool.get('giscegas.polissa')
        # Created cases
        casos_ids = []
        pol_id = context.get('contract_ids', context.get('contract_id', False))
        if isinstance(pol_id, (tuple, list)):
             pol_id = pol_id[0]
        reqqd = pol_obj.read(cursor, uid, pol_id, ['caudal_diario'])['caudal_diario']
        config_vals = {
            'modeffectdate': wizard.modeffectdate,
            'reqtransferdate': wizard.reqtransferdate,
            'extrainfo': wizard.extrainfo,
            'disconnectedserviceaccepted': wizard.disconnectedserviceaccepted,
            'reqqd': reqqd,
            'reqestimatedqa': reqqd*365.0
        }

        info = ''
        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, '02', config_vals=config_vals, context=context
        )

        info += "%s: %s\n" % (res[0], res[1])
        if res[2]:
            casos_ids.append(res[2])

        casos_json = json.dumps(casos_ids)
        wizard.write({'info': info, 'state': 'done',
                      'casos_generados': casos_json})

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generados
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creados'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.atr',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'state': fields.char('Estado', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'info': fields.text('Info'),
        'casos_generados': fields.text('Casos generados'),
        # 02 specific data
        # Dades generals
        'modeffectdate': fields.selection(TAULA_MODELO_FECHA_EFECTO,
                                          "Modelo de Fecha Efecto"),
        'reqtransferdate': fields.date("Fecha de Efecto Solicitada"),
        'disconnectedserviceaccepted': fields.selection(
            SINO, "Contratación incondicional baja"),
        'extrainfo': fields.text('Comentarios', size=400),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'whereiam': _whereiam,
        'casos_generados': lambda *a: json.dumps([]),
        # Dades generals
        'modeffectdate': lambda *a: '05',
        'disconnectedserviceaccepted': lambda *a: 'N',
    }


GiscegasAtrWizard02()
