# -*- coding: utf-8 -*-
import json

from osv import osv, fields, orm
from tools.translate import _
from gestionatr.defs_gas import *
from gestionatr.defs import SINO


class GiscegasAtrWizard41(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscegas.atr.a141.wizard'

    def _get_default_titular_id(self, cursor, uid, context=None):
        if not context:
            context = {}

        titular_id = False
        if context.get('contract_id'):
            pol_obj = self.pool.get("giscegas.polissa")
            pol_id = context.get('contract_id')
            info = pol_obj.read(cursor, uid, pol_id, ['titular'], context=context)
            if info['titular']:
                titular_id = info['titular'][0]

        return titular_id

    def _get_default_newreqqd(self, cursor, uid, context=None):
        if not context:
            context = {}

        reqqd = 0
        if context.get('contract_id'):
            pol_obj = self.pool.get("giscegas.polissa")
            pol_id = context.get('contract_id')
            info = pol_obj.read(cursor, uid, pol_id, ['caudal_diario'], context=context)
            reqqd = info['caudal_diario']

        return reqqd

    def _get_default_new_titular_id(self, cursor, uid, context=None):
        if not context:
            context = {}

        titular_id = False
        if context.get('contract_id'):
            pol_obj = self.pool.get("giscegas.polissa")
            pol_id = context.get('contract_id')
            info = pol_obj.read(cursor, uid, pol_id, ['titular'],
                                context=context)
            if info.get('titular'):
                titular_id = info['titular'][0]

        return titular_id

    def _get_default_new_titular_address_id(self, cursor, uid, context=None):
        if not context:
            context = {}

        addr_id = False
        if context.get('contract_id'):
            pol_obj = self.pool.get("giscegas.polissa")
            pol_id = context.get('contract_id')
            info = pol_obj.read(cursor, uid, pol_id, ['direccio_notificacio', 'direccio_pagament'], context=context)
            if info.get('direccio_pagament'):
                addr_id = info['direccio_pagament'][0]
            elif info.get('direccio_notificacio'):
                addr_id = info['direccio_notificacio'][0]
            else:
                pol = pol_obj.browse(cursor, uid, pol_id)
                if len(pol.titular.address):
                    addr_id = pol.titular.address[0].id

        return addr_id

    def _get_default_newtelephone1(self, cursor, uid, context=None):
        if not context:
            context = {}
        phone = ""
        if context.get('contract_id'):
            pol_obj = self.pool.get("giscegas.polissa")
            pol_id = context.get('contract_id')
            addr = pol_obj.get_address_with_phone(
                cursor, uid, pol_id, context=context
            )
            if addr:
                phone = addr.phone or addr.mobile
        return phone

    def _whereiam(self, cursor, uid, context=None):
        """ returns 'distri' or 'comer' """
        cups_obj = self.pool.get('giscegas.cups.ps')
        sw_obj = self.pool.get('giscegas.atr')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids, context)[0]

        pol_obj = self.pool.get('giscegas.polissa')
        # Created cases
        casos_ids = []
        pol_id = context.get('contract_id', False)
        config_vals = wizard.read([])[0]
        for ignore_key in ['state', 'casos_generats', 'id', 'contract', 'whereiam', 'info']:
            if ignore_key in config_vals:
                config_vals.pop(ignore_key)

        info = ''
        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, '41', config_vals=config_vals, context=context
        )

        info += "%s: %s\n" % (res[0], res[1])
        if res[2]:
            casos_ids.append(res[2])

        casos_json = json.dumps(casos_ids)
        wizard.write({'info': info, 'state': 'done',
                      'casos_generats': casos_json})

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creados'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.atr',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'state': fields.char('Estado', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'info': fields.text('Info'),
        'casos_generats': fields.text('Casos generados'),
        'contract': fields.many2one('giscegas.polissa', 'Contrato'),
        # 41 specific data
        # Dades generals
        'updatereason': fields.selection(TAULA_MOTIVO_DE_MODIFICACION_EN_UN_CAMBIO_DE_COMERCIALIZADOR, "Motivo de Modificación"),
        'modeffectdate': fields.selection(TAULA_MODELO_FECHA_EFECTO, "Modelo de Fecha Efecto"),
        'reqtransferdate': fields.date("Fecha de Efecto Solicitada"),
        'disconnectedserviceaccepted': fields.selection(SINO, "Contratación incondicional baja"),
        'extrainfo': fields.text('Comentarios', size=400),
        # Dades Ampliacio Caudal
        'titular_id': fields.many2one('res.partner', "Titular actual"),
        'newreqqd': fields.float("Caudal Diario Qd", digits=(9, 7)),
        # Dades Canvi Titular
        'surrogacy': fields.selection(SINO, "Indicador de Subrogación"),
        'new_titular_id': fields.many2one('res.partner', "Titular nuevo"),
        'new_titular_address_id': fields.many2one('res.partner.address', "Dirección nuevo titular"),
        'newtelephone1': fields.char("Telefon", size=64),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'whereiam': _whereiam,
        'casos_generats': lambda *a: json.dumps([]),
        # Dades generals
        'updatereason': lambda *a: '03',
        'modeffectdate': lambda *a: '05',
        'disconnectedserviceaccepted': lambda *a: 'N',
        # Dades Ampliacio Caudal
        'titular_id': _get_default_titular_id,
        'newreqqd': _get_default_newreqqd,
        # Dades Canvi Titular
        'surrogacy': 'N',
        'new_titular_id': _get_default_new_titular_id,
        'new_titular_address_id': _get_default_new_titular_address_id,
        'newtelephone1': _get_default_newtelephone1,
    }

GiscegasAtrWizard41()
