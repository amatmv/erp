# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from gestionatr.defs_gas import *
import vatnumber
import json
from datetime import datetime, timedelta


class GiscegasAtrWizard44(osv.osv_memory):
    _name = 'giscegas.atr.a144.wizard'

    def onchange_operationtype(self, cursor, uid, ids, operationtype, context=None):
        if not context:
            context = None

        res = {'value': {}, 'domain': {}, 'warning': {}}

        descripcio = ""
        for tipus, desc in TAULA_TIPO_OPERACION_COMER:
            if tipus == operationtype:
                descripcio = desc
                break

        res['value'].update({"descripcion": descripcio})
        return res

    def onchange_direccio_notificacio(self, cursor, uid, ids, direccio_notificacio, context=None):
        if not context:
            context = None

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not direccio_notificacio:
            return res

        dir = self.pool.get("res.partner.address").browse(cursor, uid, direccio_notificacio)
        tel1 = dir.phone or dir.mobile
        tel2 = False
        if dir.phone and dir.mobile:
            tel2 = dir.mobile

        res['value'].update({"telefono1": tel1, "telefono2": tel2})
        return res

    def _get_default_titular_id(self, cursor, uid, context=None):
        if not context:
            context = {}

        titular_id = False
        if context.get('contract_id'):
            pol_obj = self.pool.get("giscegas.polissa")
            pol_id = context.get('contract_id')
            info = pol_obj.read(cursor, uid, pol_id, ['titular'], context=context)
            if info['titular']:
                titular_id = info['titular'][0]

        return titular_id

    def _get_default_direccio_notificacio(self, cursor, uid, context=None):
        if not context:
            context = {}

        dir_not = False
        if context.get('contract_id'):
            pol_obj = self.pool.get("giscegas.polissa")
            pol_id = context.get('contract_id')
            info = pol_obj.read(cursor, uid, pol_id, ['direccio_notificacio'], context=context)
            if info['direccio_notificacio']:
                dir_not = info['direccio_notificacio'][0]

        return dir_not

    def _get_default_reading_date(self, cursor, uid, context=None):
        if not context:
            context = {}
        vals = self._get_last_pool_reading_values(cursor, uid, context.get('contract_id'))
        return vals['name'] if vals else False

    def _get_default_reading_value(self, cursor, uid, context=None):
        if not context:
            context = {}
        vals = self._get_last_pool_reading_values(cursor, uid, context.get('contract_id'))
        return vals.get('lectura') if vals else False

    def _get_last_pool_reading_values(self, cursor, uid, polissa_id, context=None):
        if not context:
            context = {}
        if not polissa_id:
            return False

        polissa_obj = self.pool.get("giscegas.polissa")
        lect_pool_obj = self.pool.get("giscegas.lectures.lectura.pool")
        mid = polissa_obj.get_last_selfmetering(cursor, uid, polissa_id)
        if not mid:
            return False
        return lect_pool_obj.read(cursor, uid, mid[0], [])

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        pol_obj = self.pool.get('giscegas.polissa')
        wizard = self.browse(cursor, uid, ids[0], context)

        pol_id = context.get("contract_id")
        config_vals = wizard.read([])[0]

        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, '44', config_vals=config_vals,
            context=context
        )

        casos_ids = []
        if res[2]:
            casos_ids.append(res[2])
        casos_json = json.dumps(casos_ids)

        info = "{0}: {1}\n".format(res[0], res[1])
        wizard.write({'state': 'end', 'info': info, 'casos_generats': casos_json})

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.atr',
            'type': 'ir.actions.act_window'
        }

    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        casos = self.pool.get("giscegas.atr").browse(cursor, uid, casos_ids)
        if not context:
            context = {}
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscegas.atr',
            'report_name': 'giscegas.atr.info_cas_ATR',
            'report_webkit': "'giscegas_switching/report/info_cas_ATR.mako'",
            'webkit_header': 'report_sense_fons',
            'groups_id': casos_ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
               'casos': casos_ids
            },
        }

    _columns = {
        'state': fields.char('Estado', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'info': fields.text('Info'),
        'casos_generats': fields.text('Casos generados'),
        'operationtype': fields.selection(TAULA_TIPO_OPERACION_COMER, 'Tipo de Actuación', required=True),
        'descripcion': fields.char('Descripción Actuación', size=400, required=True),
        'sourcetype': fields.selection(TAULA_TIPO_SOLICITANTE, 'Tipo de Solicitante'),
        'titular_id': fields.many2one("res.partner", "Titular"),
        'direccio_notificacio': fields.many2one("res.partner.address", "Contacto"),
        'telefono1': fields.char(size=20, string=u'Teléfono 1'),
        'telefono2': fields.char(size=20, string=u'Teléfono 2'),
        'operationnum': fields.char('Número de Operación', size=40),
        'readingdate': fields.date('Fecha de Lectura Actual'),
        'readingvalue': fields.float('Lectura Actual', digits=(16, 2)),
        'extrainfo': fields.text('Comentarios', size=400),
    }
    _defaults = {
        'state': lambda *a: 'init',
        'casos_generats': lambda *a: json.dumps([]),
        'operationtype': lambda *a: 'A10011',
        'sourcetype': lambda *a: '06',
        'titular_id': _get_default_titular_id,
        'direccio_notificacio': _get_default_direccio_notificacio,
        'readingdate': _get_default_reading_date,
        'readingvalue': _get_default_reading_value,
    }


GiscegasAtrWizard44()
