# -*- coding: utf-8 -*-
# -*- coding: utf -*-
# Deberíamos poner un separador y quitar el label para mantener
# la estructura visual del modelo-8
import json

from osv import osv, fields, orm
from tools.translate import _
from gestionatr.defs_gas import *
from gestionatr.defs import SINO


class GiscegasAtrWizard04(osv.osv_memory):

    _name = 'giscegas.atr.a104.wizard'

    def _whereiam(self, cursor, uid, context=None):
        """ returns 'distri' or 'comer' """
        cups_obj = self.pool.get('giscegas.cups.ps')
        sw_obj = self.pool.get('giscegas.atr')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def _get_default_titular_id(self, cursor, uid, context=None):
        if not context:
            context = {}

        titular_id = False
        if context.get('contract_id'):
            pol_obj = self.pool.get("giscegas.polissa")
            pol_id = context.get('contract_id')
            info = pol_obj.read(cursor, uid, pol_id, ['titular'], context=context)
            if info['titular']:
                titular_id = info['titular'][0]
        return titular_id

    def _get_default_direccio_notificacio(self, cursor, uid, context=None):
        if not context:
            context = {}

        dir_not = False
        if context.get('contract_id'):
            pol_obj = self.pool.get("giscegas.polissa")
            pol_id = context.get('contract_id')
            info = pol_obj.read(cursor, uid, pol_id, ['direccio_notificacio'], context=context)
            if info['direccio_notificacio']:
                dir_not = info['direccio_notificacio'][0]

        return dir_not

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        pol_obj = self.pool.get('giscegas.polissa')
        # Created cases
        casos_ids = []
        pol_id = context.get('contract_ids', context.get('contract_id', False))

        config_vals = {
            'modeffectdate': wizard.modeffectdate,
            'reqcanceldate': wizard.reqcanceldate,
            'extrainfo': wizard.extrainfo,
            'cancelreason': wizard.cancelreason,
            'owner': wizard.owner and wizard.owner.id,
            'direccio_notificacio': wizard.direccio_notificacio and wizard.direccio_notificacio.id
        }

        info = ''
        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, '04', config_vals=config_vals, context=context
        )

        info += "%s: %s\n" % (res[0], res[1])
        if res[2]:
            casos_ids.append(res[2])

        casos_json = json.dumps(casos_ids)

        wizard.write(
            {'info': info, 'state': 'done',
             'casos_generados': casos_json
             }
        )

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generados
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creados'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.atr',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'state': fields.char('Estado', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'info': fields.text('Info'),
        'casos_generados': fields.text('Casos generados'),

        # 04 specific data
        # Dades generals
        'modeffectdate': fields.selection(
            TAULA_MODELO_FECHA_EFECTO, "Modelo de Fecha Efecto"
        ),
        'reqcanceldate': fields.date("Fecha de Baja Solicitada"),
        'cancelreason': fields.selection(TAULA_MOTIVO_BAJA, "Motivo baja"),
        'extrainfo': fields.text('Comentarios', size=400),
        'owner': fields.many2one('res.partner', 'Nuevo Titular'),
        'direccio_notificacio': fields.many2one(
            'res.partner.address', u'Adreça notificació'
        ),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'whereiam': _whereiam,
        'casos_generados': lambda *a: json.dumps([]),
        # Dades generals
        'modeffectdate': lambda *a: '05',
        # Dades mdg
        'cancelreason': lambda *a: '05',
        'owner': _get_default_titular_id,
        'direccio_notificacio': _get_default_direccio_notificacio
    }


GiscegasAtrWizard04()
