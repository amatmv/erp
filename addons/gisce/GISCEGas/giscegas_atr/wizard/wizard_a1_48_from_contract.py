# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from gestionatr.defs_gas import *
import vatnumber
import json
from datetime import datetime, timedelta
import xml.etree.ElementTree as ET
from gestionatr.input.messages.A1_48 import get_minimum_fields
from giscegas_atr.utils import get_address_dict

GATR_TO_ERP_TIPO_DOC = {
    'NI': '01',
    'PS': '03',
    'NE': '04',
    'OT': '08',
}


class GiscegasAtrWizard48(osv.osv_memory):
    _name = 'giscegas.atr.a148.wizard'

    def _get_default_polissa(self, cursor, uid, context=None):
        if context is None:
            context = {}
        if context.get("from_model", False) == "giscegas.atr":
            sw_obj = self.pool.get("giscegas.atr")
            sw_id = context.get('active_id', False)
            pol_id = sw_obj.read(
                cursor, uid, sw_id, ['cups_polissa_id'], context=context
            )['cups_polissa_id']
            return pol_id and pol_id[0] or False
        else:
            return context.get('contract_id')

    def onchange_subtipus(self, cr, uid, ids, subtipus, context=None):
        if not context:
            context = {}
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subinfo = subtipus_obj.read(cr, uid, subtipus, ['type'])
        data = {
            'tipus': subinfo['type']
        }
        return {'value': data}

    def onchange_polissa(self, cursor, uid, ids, polissa_id, context=None):
        res = {'warning': {}}
        sw_obj = self.pool.get("giscegas.atr")
        proces_obj = self.pool.get("giscegas.atr.proces")
        proces_r1_id = proces_obj.search(cursor, uid, [('name', '=', '48')])[0]
        r1_oberts = sw_obj.search(
            cursor, uid, [('proces_id', '=', proces_r1_id),
                          ('cups_polissa_id', '=', polissa_id),
                          ('state', '!=', 'done')]
        )
        if r1_oberts:
            pol_obj = self.pool.get("giscegas.polissa")
            info = pol_obj.read(cursor, uid, polissa_id, ['name'], context=context)
            res['warning'] = {
                'title': _(u"Atención"),
                'message': _(u"Este contrato ({0}) ya tienen reclamaciones "
                             u"abiertas con ids {1}.").format(info['name'], r1_oberts)
            }
        return res

    def action_subtype_fields_view(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0])
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'giscegas.atr.a148.wizard.generic',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': "{{ "
                       "'subtipus_id':{0}, "
                       "'contract_id': {1} "
                       "}}".format(wizard.subtipus_id.id, wizard.polissa_id.id)
        }

    _columns = {
        'polissa_id': fields.many2one('giscegas.polissa', 'Contracte'),
        'tipus': fields.selection(TAULA_TIPO_DE_RECLAMACION, u"Tipus", required=True,
                                  help=u"El tipo se completa automaticamente "
                                       u"al seleccionar un subtipo"),
        'subtipus_id': fields.many2one("giscedata.subtipus.reclamacio",
                                       u"Subtipo", required=True,
                                       domain="[('sector','in', ('t', 'g'))]"),
    }
    _defaults = {
        'polissa_id': _get_default_polissa,
    }


GiscegasAtrWizard48()


# This dictionari translate the field names of the R1 from gestionatr to the
# names used in the R1 from ERP
# The names used in gestionatr are the names used by the CNMC in the file about
# "Campos Minimos" with underscores format
A1_48_TO_ERP = {
    'nif_cliente': ['client_documenttype', 'client_documentnum'],
    'nombre_cliente': ['client_businessname', 'client_firstname', 'client_familyname1', 'client_titulartype',
                       'client_familyname2', 'ADDRES_GROUP'],
    'telefono_contacto': ['client_client_telephoneprefix',
                          'client_client_telephonenumber', 'client_email',
                          'claim_cont_contactname', 'claim_cont_telephoneprefix',
                          'claim_cont_telephonenumber', 'claim_cont_contactemail'],
    'cups': [],  # The sw case will generate it
    'fecha_incidente': ['incidencia_incidentperiod_datefrom',
                        'incidencia_incidentperiod_dateto', 'claim_incidentdate'],
    'comentarios': ['extrainfo'],
    'codigo_incidencia': [],
    'persona_de_contacto': ['claim_cont_contactname',
                            'claim_cont_telephoneprefix',
                            'claim_cont_telephonenumber',
                            'claim_cont_contactemail'],
    'num_fact': ['claim_invoicenumber'],
    'tipo_concepto_facturado': ['claim_conceptfacturation'],
    'lectura': ['claim_readingdate'],
    'fecha_de_lectura': ['claim_readingvalue'],
    'ubicacion_incidencia': ['incidencia_incidentlocationdesc',
                             'incidencia_incidentlocationprovince',
                             'incidencia_incidentlocationcity',
                             'incidencia_incidentlocationcitysubdivision',
                             'incidencia_incidentlocationzipcode'],
    'codigo_de_solicitud': ['claim_comreferencenum'],
    'concepto_contratacion': ['claim_conceptcontract'],
    'cta_banco': ['claim_iban'],
    'sol_nuevos_suministro': ['claim_nnssexpedient'],
    'cod_reclam_anterior': ['claim_targetclaimcomreferencenum'],
    'importe_reclamado': ['claim_claimedcompensation'],
    'tipo_atencion_incorrecta': ['claim_wrongattentiontype'],
    'numero_expediente_fraude': ['claim_fraudrecordnum'],
}


class GiscegasAtrWizard48Generic(osv.osv_memory):
    _name = 'giscegas.atr.a148.wizard.generic'

    BASE_VIEW = """<form string="Crear A1_48 (Reclamación)">
                    <notebook>
                        <page string="Datos Solicitud">
                            <separator string="Tipo Solicitud" colspan="4"/>
                            <field name="tipus" readonly="1"/>
                            <field name="subtipus_id" readonly="1"/>
                            <separator string="Dades Administratives" colspan="4"/>
                            <field name="originreference"/>
                            <newline/>
                            <field name="priority"/>
                            <field name="legallimitdate"/>
                            <newline/>
                            <separator string="Reclamante" colspan="4"/>
                            <field name="claimertype" colspan="4"/>
                        </page>
                        <page string="Reclamante" attrs="{'invisible':[('claimertype','=','06')]}">
                            <group string="Datos Reclamante" colspan="4">
                                <field name="claimerdocumenttype"/>
                                <field name="claimerdocumentnum"/>
                                <group attrs="{'invisible':[('claimerpersona','!=','F')]}" colspan="4" col="4">
                                    <field name="claimerfirstname"/>
                                    <field name="claimerlastname"/>
                                    <field name="claimerbusinessname"/>
                                </group>
                                <group attrs="{'invisible':[('claimerpersona','=','F')]}" colspan="4" col="4">
                                    <field name="claimerbusinessname"/>
                                </group>
                            </group>
                            <separator colspan="4"/>
                            <group string="Contacto Reclamante" colspan="4">
                                <field name="claimerprefixtel1"/>
                                <field name="claimertelephone1"/>
                                <field name="claimeremail" colspan="4"/>
                            </group>
                        </page>
                    </notebook>
                    <group colspan="4" col="4">
                        <button icon="gtk-ok" name="action_create_a1_48_case" string="Crear Caso" type="object"/>
                        <button special="cancel" string="Tancar" icon="gtk-no"/>
                    </group>
                </form>"""

    ADDRES_GROUP = """<group col="4" colspan="4">
                                <separator colspan="4" string="Dirección"/>
                                <field name="client_province"/>
                                <field name="client_city"/>
                                <field name="client_zipcode"/>
                                <field name="client_streettype"/>
                                <field name="client_street"/>
                                <field name="client_streetnumber"/>
                                <field name="client_portal"/>
                                <field name="client_staircase"/>
                                <field name="client_floor"/>
                                <field name="client_door"/>
                            </group>"""

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        pol_obj = self.pool.get('giscegas.polissa')
        wizard = self.browse(cursor, uid, ids[0], context)

        pol_id = wizard.contract.id
        config_vals = wizard.read([])[0]

        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, '48', config_vals=config_vals,
            context=context
        )

        casos_ids = []
        if res[2]:
            casos_ids.append(res[2])
        casos_json = json.dumps(casos_ids)

        info = "{0}: {1}\n".format(res[0], res[1])
        wizard.write({'state': 'end', 'info': info, 'casos_generats': casos_json})

    def action_create_a1_48_case(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wiz_values = self.read(cursor, uid, ids[0], [])[0]
        res = self.action_create_a1_48_case_from_dict(
            cursor, uid, wiz_values['polissa_id'], wiz_values
        )

        return {
            'domain': "[('id','=', {0})]".format(str(res[-1])),
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.atr',
            'type': 'ir.actions.act_window'
        }

    def action_create_a1_48_case_from_dict(self, cursor, uid, polissa_id, dict_fields, context=None):
        contract_obj = self.pool.get('giscegas.polissa')
        res = contract_obj.crear_cas_atr(cursor, uid, polissa_id, proces='48')
        if not isinstance(res[-1], long):
            # We don't have the sw_id, something went wrong
            raise osv.except_osv(u"ERROR", res[:-1])

        sw_obj = self.pool.get("giscegas.atr")
        a148 = sw_obj.get_pas(cursor, uid, [res[-1]])

        main_values = {}
        llista_reclamacions = []
        reclamacio_values = {}
        lectures_values = {}
        dicts_factures = []
        for field, value in dict_fields.items():
            if not value or field in ['id', 'polissa_id', 'state']:
                continue
            elif field == 'subtipus_id':
                field = 'claimsubtype'
            elif field == 'tipus':
                field = 'claimtype'
            # If field is reclamacio_num_factura we will check all invoices
            if field == "claim_invoicenumber":
                fact_obj = self.pool.get('giscegas.facturacio.factura')
                for factura in fact_obj.read(cursor, uid, value, ['origin'], context=context):
                    dicts_factures.append({field[6:]: factura['origin']})

            elif field.startswith("claim_"):
                reclamacio_values.update({field[6:]: value})
            elif field.startswith("client_"):
                reclamacio_values.update({field[7:]: value})
            elif field.startswith("incidencia_"):
                reclamacio_values.update({field[11:]: value})
            elif field.startswith("claim_cont_"):
                reclamacio_values.update({field[11:]: value})
            else:
                main_values.update({field: value})

        # Create reclamacio
        reclama_obj = self.pool.get("giscegas.atr.referencia.reclamacio")
        if not len(dicts_factures):
            if reclamacio_values.items():
                rec_id = reclama_obj.create(cursor, uid, reclamacio_values, context=context)
                llista_reclamacions.append(rec_id)
        else:
            for dict_f in dicts_factures:
                dict_f.update(reclamacio_values)
                rec_id = reclama_obj.create(cursor, uid, dict_f, context=context)
                llista_reclamacions.append(rec_id)
        main_values.update({'claimreferencelist': [(6, 0, llista_reclamacions)]})

        a148.write(main_values)

        # Update additional info
        sw_obj.write(cursor, uid, res[-1], {'additional_info': a148.get_additional_info()})
        return res

    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        casos = self.pool.get("giscegas.atr").browse(cursor, uid, casos_ids)
        if not context:
            context = {}
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscegas.atr',
            'report_name': 'giscegas.atr.info_cas_ATR',
            'report_webkit': "'giscegas_atr/report/info_cas_ATR.mako'",
            'webkit_header': 'report_sense_fons',
            'groups_id': casos_ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
               'casos': casos_ids
            },
        }

    def _get_default_polissa(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('contract_id')

    def fields_view_get(self, cursor, uid, view_id=None, view_type='form',
                        context=None, toolbar=False):
        res = super(GiscegasAtrWizard48Generic, self).fields_view_get(
            cursor, uid, view_id, view_type, context=context, toolbar=toolbar
        )
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subtipus = subtipus_obj.browse(cursor, uid, context.get('subtipus_id'))

        root = ET.fromstring(self.BASE_VIEW)
        notebook = root.find("notebook")

        for min_field in get_minimum_fields(subtipus.name):
            current_page = None
            needed_fields = []
            for field in A1_48_TO_ERP[min_field]:
                # If ends with '*', add all fields which starts with field
                if field.endswith("*"):
                    needed_fields.extend([x for x in self._columns.keys() if x.startswith(field[:-1])])
                else:
                    needed_fields.append(field)
            for needed_field in needed_fields:
                # Search for the page where fiels will be added
                # If the page doesn't exist we add it
                current_page = None
                field_page = self.get_page_of_field(needed_field.lower())
                for page in notebook.findall("page"):
                    if page.get("string") == field_page:
                        current_page = page
                if not current_page:
                    ET.SubElement(notebook, "page", {'string': field_page})
                    current_page = notebook.findall("page")[-1]

                if needed_field.endswith("_GROUP"):
                    # Parse and add group
                    group = ET.fromstring(getattr(self, needed_field))
                    current_page.append(group)
                else:
                    # Add the field to the page
                    attrs = {'name': needed_field}
                    if needed_field.endswith("_ids"):
                        attrs.update({'widget': "one2many_list"})
                    if needed_field == 'claim_invoicenumber':
                        attrs.update({'nolabel': "1", 'colspan': "4"})
                    ET.SubElement(current_page, "field", attrs)
        # For some reason, the many2many fields are not added to 'fields'
        # as the other fields, so we add it
        domain = [('type', 'in', ['in_invoice', 'in_refund'])]
        if context:
            pol_id = context.get('polissa_id')
            if pol_id:
                domain.append(('polissa_id', '=', pol_id))

        res['fields'].update({
            'claim_invoicenumber': {'domain': domain, 'relation': 'giscegas.facturacio.factura', 'string': u'Factures per reclamar', 'context': '', 'views': {}, 'type': 'many2many'}
        })
        res['arch'] = ET.tostring(root)
        return res

    @staticmethod
    def get_page_of_field(field_name):
        if field_name.startswith("claim_cont_"):
            return _(u"Contacto")
        elif field_name.startswith("claim_"):
            return _(u"Detalles Reclamacion")
        elif field_name == 'extrainfo':
            return _(u"Comentarios")
        elif field_name.startswith("incidencia_"):
            return _(u"Incidencia")
        else:
            return _(u"Cliente")

    def default_get(self, cursor, uid, fields, context=None):
        if context is None:
            context = {}

        res = super(GiscegasAtrWizard48Generic, self).default_get(
            cursor, uid, fields, context
        )

        # For some reason 'default_get' it's called two times: one when wizard
        # is created (correct) and another when action_create_r1_case is called
        # (why?). This second call makes that all data introduced by user is
        # overwrited by default values, so we have to check if this is the
        # second time that default_get is called and avoid overwriting these
        # values. In the first call we get all fields in "fields" and in the
        # second we only recive "subtipus_id" and "tipus_id". This is why we
        # check if there is "polissa_id" in fields.
        polissa_id = context.get("polissa_id", False)
        if not polissa_id:
            polissa_id = res.get("polissa_id", False)
        if not polissa_id:
            return res
        dvals = self.default_values_from_contract(
            cursor, uid, polissa_id, context=context
        )
        for f in fields:
            if f in dvals:
                res[f] = dvals.get(f)
        return res

    def default_values_from_contract(self, cursor, uid, polissa_id, context=None):
        pol_obj = self.pool.get("giscegas.polissa")
        polissa = pol_obj.browse(cursor, uid, polissa_id)

        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subtipus = subtipus_obj.browse(cursor, uid, context.get('subtipus_id'))
        res = {}
        default_values = {}
        default_values.update(
            self.default_client_values(cursor, uid, polissa, context))
        default_values.update(
            self.default_reclamacio_values(cursor, uid, polissa, context))
        default_values.update(
            self.default_comment_values(cursor, uid, polissa, context))

        needed_fields = []
        for min_field in get_minimum_fields(subtipus.name):
            for field in A1_48_TO_ERP[min_field]:
                if field == "ADDRES_GROUP":
                    # We want all measure files
                    field = "client_*"
                if field.endswith("*"):
                    needed_fields.extend([x for x in self._columns.keys()
                                          if x.startswith(field[:-1])])
                else:
                    needed_fields.append(field)

        # Get default value for each field
        for field_name in needed_fields:
            if default_values.get(field_name, False):
                res.update({field_name: default_values.get(field_name)})
        return res

    def default_client_values(self, cr, uid, polissa, context=None):
        if not polissa.titular:
            return {}
        part_obj = self.pool.get("res.partner")
        res = {
            'client_documenttype': GATR_TO_ERP_TIPO_DOC.get(polissa.titular.get_vat_type()),
            'client_documentnum': polissa.titular.vat[2:],
            'client_titulartype': 'J',
            'client_businessname': polissa.titular.name,
            'client_client_telephoneprefix': '34',
        }
        titular_addres = polissa.get_address_with_phone()
        if titular_addres:
            res.update({
                'client_client_telephonenumber': titular_addres.phone or titular_addres.mobile,
                'client_email': titular_addres.email
            })
            res.update(get_address_dict(titular_addres, mod="client_", sufix=""))
        if not polissa.titular.has_enterprise_vat():
            names_dict = part_obj.separa_cognoms(cr, uid, polissa.titular.name)
            res.update({
                'client_titulartype': 'F',
                'client_firstname': names_dict['nom'],
                'client_familyname1': names_dict['cognoms'][0],
                'client_familyname2': names_dict['cognoms'][1],
            })
        return res

    def default_reclamacio_values(self, cr, uid, polissa, context=None):
        res = {
            'claim_iban': polissa.bank and polissa.bank.iban,
            'incidencia_incidentlocationprovince': polissa.cups.id_provincia.id,
            'incidencia_incidentlocationcity': polissa.cups.id_municipi.id,
        }
        if polissa.direccio_notificacio:
            addres = polissa.direccio_notificacio
            res.update({
                'claim_cont_contactname': addres.partner_id.name,
                'claim_cont_telephoneprefix': '34',
                'claim_cont_telephonenumber': addres.phone or addres.mobile,
                'claim_cont_contactemail': addres.email,
            })

        # Search if there is any non A1_48 opened case with same contract
        sw_obj = self.pool.get("giscegas.atr")
        sw_id = sw_obj.search(cr, uid,
                              [('cups_polissa_id', '=', polissa.id),
                               ('proces_id.name', '!=', '48'),
                               ('state', 'in', ['open', 'pending'])], limit=1, order="date")
        if sw_id:
            sw = sw_obj.browse(cr, uid, sw_id[0])
            res.update({
                'claim_comreferencenum': sw.codi_sollicitud
            })

        # Search the last R1 with same contract
        sw_id = sw_obj.search(cr, uid,
                              [('cups_polissa_id', '=', polissa.id),
                               ('proces_id.name', '=', '48'),
                               ('state', 'in', ['open', 'pending'])], limit=1, order="date")
        if sw_id:
            sw = sw_obj.browse(cr, uid, sw_id[0])
            res.update({
                'claim_targetclaimcomreferencenum': sw.codi_sollicitud
            })

        facts = context.get('claim_invoicenumber', False)
        if facts:
            res['claim_invoicenumber'] = facts[str(polissa.id)]

        return res

    def default_comment_values(self, cr, uid, polissa, context=None):
        if not context or not context.get("subtipus_id"):
            return {}

        res = {}
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subtipus = subtipus_obj.browse(cr, uid, context.get('subtipus_id'))
        if subtipus.name == "006":
            ultima_lect = None
            if polissa.comptadors:
                lectures_pool = polissa.comptadors[0].pool_lectures
                lectures_pool = [x.name for x in lectures_pool]
                if len(lectures_pool):
                    ultima_lect = max(lectures_pool)
                else:
                    ultima_lect = polissa.data_alta

            if ultima_lect or polissa.data_alta:
                comment_tmpl = _(u"De este suministro no tenemos lectures desde {0}")
                res.update({
                    'extrainfo': comment_tmpl.format(ultima_lect or polissa.data_alta)
                })
        return res

    def _get_default_tipus(self, cr, uid, context=None):
        if not context or not context.get("subtipus_id"):
            return 0
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subinfo = subtipus_obj.read(cr, uid, context.get('subtipus_id'), ['type'])
        return subinfo['type']

    _columns = {
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
        'polissa_id': fields.many2one('giscegas.polissa', 'Contracte'),
        'casos_generats': fields.text('Casos generados'),
        # Datos Solicitud
        'tipus': fields.selection(TAULA_TIPO_DE_RECLAMACION, u"Tipo", required=True),
        'subtipus_id': fields.many2one("giscedata.subtipus.reclamacio", u"Subtipo", required=True),
        'claimertype': fields.selection(TAULA_TIPO_SOLICITANTE, 'Tipo de Reclamante'),
        'originreference': fields.char(string='Referencia del organismo que reclama', size=35),
        'legallimitdate': fields.date(string='Fecha Límite Legal'),
        'priority': fields.selection([('0', 'Prioridad Normal'), ('1', 'Prioridad Alta')], 'Prioridad de Reclamación'),
        # Claimer
        'claimerdocumenttype': fields.selection(TAULA_TIPO_DOCUMENTO, 'Tipo de Documento del Reclamante'),
        'claimerdocumentnum': fields.char(string='Nº Documento del Reclamante', size=20),
        'claimerfirstname': fields.char(string='Nombre Cliente del Reclamante', size=40),
        'claimerlastname': fields.char(string='Apellido 1 / Razón social del Reclamante', size=30),
        'claimersecondname': fields.char(string='Apellido 2 del Reclamante', size=30),
        'claimerbusinessname': fields.char(string='Razón Social', size=40),
        'claimerprefixtel1': fields.char(string='Prefijo Nacional del Núero de Teléfono', size=4),
        'claimertelephone1': fields.char(string='Teléfono del Reclamante', size=20),
        'claimeremail': fields.char(string='Correo Electrónico del Reclamante', size=100),
        'claimerpersona': fields.selection(TAULA_TIPO_PERSONA, 'Tipo de persona'),
        # Comentarios
        'extrainfo': fields.text(string='Comentarios', size=400),
        # Camps de una claimreference
        'claim_wrongattentiontype': fields.selection(TAULA_TIPOS_DE_ATENCION_INCORRECTA, 'Tipo de atención incorrecta'),
        'claim_comreferencenum': fields.char(string='Código Sol. Comercializadora', size=12),
        'claim_targetclaimcomreferencenum': fields.char(string='Referencia Recl. Previa', size=12),
        'claim_conceptcontract': fields.selection(TAULA_CONCEPTOS_DE_CONTRATACION, 'Concepto de contratación'),
        'claim_conceptfacturation': fields.selection(TAULA_CONCEPTOS_FACTURADOS, 'Concepto de facturación'),
        'claim_claimedcompensation': fields.integer(string='Indemnización Solicitada'),
        'claim_iban': fields.char(string='Cuenta IBAN', size=34),
        'claim_nnssexpedient': fields.char(string='Código de expediente de NNSS', size=20),
        'claim_fraudrecordnum': fields.char(string='Código de expediente de Fraude', size=20),
        'claim_invoicenumber': fields.many2many('giscegas.facturacio.factura',
                                    'gas_factures_reclamacions_rel', 'wiz_id',
                                    'factura_id', 'Facturas a reclamar'),
        # contact
        'claim_cont_contactname': fields.char('Nombre completo contacto', size=100),
        'claim_cont_telephoneprefix': fields.char(string='Contacto. Prefijo teléfono', size=4),
        'claim_cont_telephonenumber': fields.char(string='Contacto Teléfono', size=20),
        'claim_cont_contactemail': fields.char(string='Contacto. Correo Electrónico', size=100),
        # incidentperiod
        'incidencia_incidentperiod_datefrom': fields.date(string='Fecha desde'),
        'incidencia_incidentperiod_dateto': fields.date(string='Fecha hasta'),
        # incidentlocation
        'incidencia_incidentlocationdesc': fields.char(string='Descripción de ubicación de la incidencia', size=45),
        'incidencia_incidentlocationprovince': fields.many2one('res.country.state', 'Provincia'),
        'incidencia_incidentlocationcity': fields.many2one('res.municipi', 'Municipio'),
        'incidencia_incidentlocationcitysubdivision': fields.char(string='Código INE de Población', size=9),
        'incidencia_incidentlocationzipcode': fields.char(string='Código Postal', size=5),
        # reading
        'claim_readingdate': fields.date(string='Fecha de Lectura Actual'),
        'claim_readingvalue': fields.integer(string='Lectura Actual Contador'),
        # incident
        'claim_incidentdate': fields.date(string='Fecha de la Incidencia'),
        # client
        'client_documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, 'Tipo de Documento'),
        'client_documentnum': fields.char(string='Número de Documento', size=20),
        'client_titulartype': fields.selection(TAULA_TIPO_PERSONA, 'Tipo de Titular'),
        'client_firstname': fields.char(string='Nombre', size=40),
        'client_familyname1': fields.char(string='Apellido 1', size=30),
        'client_familyname2': fields.char(string='Apellido 2', size=30),
        'client_businessname': fields.char(string='Razón Social', size=40),
        'client_client_telephoneprefix': fields.char(string='Prefijo Teléfono', size=4),
        'client_client_telephonenumber': fields.char(string='Cliente Teléfono', size=20),
        'client_email': fields.char(string='Correo Electrónico', size=100),
        'client_province': fields.many2one('res.country.state', 'Cliente Provincia'),
        'client_city': fields.many2one('res.municipi', 'Cliente Municipio'),
        'client_zipcode': fields.char(string='Cliente CP', size=5),
        'client_streettype': fields.selection(TAULA_TIPO_VIA, 'Cliente Tipo de Via INE'),
        'client_street': fields.char(string='Cliente Via', size=60),
        'client_streetnumber': fields.char(string='Cliente Número de Vía', size=4),
        'client_portal': fields.char(string='Cliente Portal', size=5),
        'client_staircase': fields.char(string='Cliente Escalera', size=5),
        'client_floor': fields.char(string='Cliente Piso', size=5),
        'client_door': fields.char(string='Cliente Puerta', size=5),
    }
    _defaults = {
        'tipus': _get_default_tipus,
        'subtipus_id': lambda obj, cr, uid, ctx: ctx.get('subtipus_id'),
        'state': lambda *a: 'init',
        'polissa_id': _get_default_polissa,
        'claimertype': lambda *a: '06',
    }


GiscegasAtrWizard48Generic()
