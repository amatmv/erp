# -*- coding: utf-8 -*-
from osv import osv, fields, orm
from osv.osv import TransactionExecute
from tools.translate import _
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException


class GiscegasAtrPolissaActivacioWizard(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscegas.atr.polissa.activacio.wizard'

    def _get_multipol(self, cursor, uid, context=None):
        if not context:
            context = {}
        return len(context['active_ids']) > 1

    def _get_info_default(self, cursor, uid, context=None):
        if not context:
            context = {}
        return _(
            u"Només de les pólisses que:\n"
            u" * Estan en esborrany per 41 (C2) (pas a3)\n"
            u" * Estan en esborrany per 02 (C1) (pas a3)\n"
        )

    def action_activar_a_polissa(self, cursor, uid, ids, context=None):
        """Activa les modificacions a la pólissa i/o activa pólissa
        """
        sw_obj = self.pool.get('giscegas.atr')

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        sw_ids = context.get('active_ids', [])
        if not wizard.multipol:
            sw_ids = [context.get('active_id')]

        info = ''
        for sw_id in sw_ids:
            try:
                sw = TransactionExecute(cursor.dbname, uid, 'giscegas.atr')
                res = sw.activa_polissa_cas_atr(sw_id, context=context)
            except GiscegasAtrException as e:
                res = e.type_error, e.message

            info += "%s: %s\n\n" % (res[0], res[1])

        wizard.write({'info': info, 'state': 'done'})

        return

    _columns = {
        'state': fields.char('State', size=16),
        'multipol': fields.boolean('Multipol'),
        'info': fields.text('Info'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'multipol': _get_multipol,
        'info': _get_info_default,
    }

GiscegasAtrPolissaActivacioWizard()