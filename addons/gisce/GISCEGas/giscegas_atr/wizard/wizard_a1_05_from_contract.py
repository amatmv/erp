# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from gestionatr.defs_gas import *
import vatnumber
import json
from datetime import datetime, timedelta
# ('updatereason', 'not in', ())
CANVIS_TITULAR = ['01', '12', '14', '16', '17', '19', '21', '23']
CANVIS_ADMINISTRATIUS = ['04', '09', '10', '17', '18', '19', '20', '21', '22', '23']
CANVIS_TECNICS = ['11', '12', '13', '14', '15', '16', '18', '19', '20', '21', '22', '23']


class GiscegasAtrWizard05(osv.osv_memory):
    _name = 'giscegas.atr.a105.wizard'

    def onchange_updatereason(self, cursor, uid, ids, updatereason, contract, generate_new_contract, new_contract, context=None):
        if not context:
            context = None

        pol_obj = self.pool.get('giscegas.polissa')
        if updatereason in CANVIS_TITULAR and contract:
            if not new_contract and generate_new_contract == u"exists":
                pol = pol_obj.browse(cursor, uid, contract, context=context)
                search_params = [
                    ('cups', '=', pol.cups.id),
                    ('state', '=', 'esborrany'),
                    ('id', '!=', pol.id)
                ]
                new_pol_ids = pol_obj.search(cursor, uid, search_params)
                if len(new_pol_ids):
                    new_contract = new_pol_ids[0]

            res = self.onchange_new_contract(
                cursor, uid, [], contract, generate_new_contract,
                new_contract, context
            )
            res['value']['generate_new_contract'] = generate_new_contract
            res['value']['new_contract'] = new_contract
            return res
        elif contract:
            pol = pol_obj.browse(cursor, uid, contract, context=context)
            return {
                'value': self.update_vals_from_contract(
                    cursor, uid, pol, context=context
                ),
                'domain': {}, 'warning': {}
            }

        return

    def onchange_new_contract(self, cursor, uid, ids, old_contract, contract_type, contact_id, context=None):
        if context is None:
            context = {}

        if contract_type == "exists" and contact_id:
            pol_id = contact_id
        else:
            pol_id = old_contract

        if not pol_id:
            return

        pol_obj = self.pool.get('giscegas.polissa')
        pol = pol_obj.browse(cursor, uid, pol_id, context=context)
        return {'value': self.update_vals_from_contract(cursor, uid, pol, context=context), 'domain': {}, 'warning': {}}

    def onchange_pagador(self, cursor, uid, ids, pagador_id, context=None):
        if not context:
            context = None

        res = {'value': {}, 'domain': {}, 'warning': {}}

        part_obj = self.pool.get('res.partner')
        pagador = part_obj.browse(cursor, uid, pagador_id)

        if pagador.address:
            res['value']['direccio_pagament'] = pagador.address[0].id

        res['value']['bank'] = False

        return res

    def onchange_contact(self, cursor, uid, ids, contact_id, context=None):
        if not context:
            context = None

        res = {'value': {}, 'domain': {}, 'warning': {}}

        part_obj = self.pool.get('res.partner')
        contact = part_obj.browse(cursor, uid, contact_id)

        if contact.address:
            res['value']['direccio_notificacio'] = contact.address[0].id

        return res

    def onchange_tariff(self, cursor, uid, ids, tariff, contract_id,
                        change_retail_tariff=None, retail_tariff=None,
                        context=None):

        res = {'value': {}, 'domain': {}, 'warning': {}}

        warning_titles = set()
        warning_messages = set()

        if not tariff:
            return res

        # The tariff
        tariff_obj = self.pool.get('giscegas.polissa.tarifa')
        contract_obj = self.pool.get('giscegas.polissa')
        tariff_id = tariff_obj.get_tarifa_from_ocsum(cursor, uid, tariff)

        tariff_domain, price_lists = self.get_tariff_domain(
            cursor, uid, tariff_id, context=context
        )

        if price_lists:
            default_retail_tariff = self._get_default_retail_tariff(
                cursor, uid, context=context)
            correct_default = default_retail_tariff in price_lists
            if not change_retail_tariff and correct_default:
                if default_retail_tariff in price_lists:
                    res['value'].update(
                        {'retail_tariff': default_retail_tariff}
                    )
            else:
                res['domain'].update({'retail_tariff': tariff_domain})
                if contract_id and (retail_tariff not in price_lists or not retail_tariff):
                    new_retail_tariff = contract_obj.escull_llista_preus(
                        cursor, uid, contract_id, price_lists, context=context
                    )
                    if new_retail_tariff and new_retail_tariff.id in price_lists:
                        res['value'].update(
                            {'retail_tariff': new_retail_tariff.id,
                             'change_retail_tariff': True
                             }
                        )
                    else:
                        new_retails = {'retail_tariff': False,
                                       'change_retail_tariff': True,
                                       }
                        res['value'].update(new_retails)
        else:
            res['value'].update({
                'retail_tariff': False,
                'change_retail_tariff': True,
            })
            warning_titles.add(_(u"Tarifa ATR incorrecte"))
            warning_messages.add(_(u"No hi ha cap Tarifa Comercialitzadora "
                                      u"per a aquesta tarifa ATR"))
        if warning_titles:
            res['warning'] = {
                'title': ', '.join(warning_titles),
                'message': '\n'.join(warning_messages)
            }

        return res

    def onchange_change_retail_tariff(self, cursor, uid, ids,
                                      change_retail_tariff, tariff,
                                      context=None):
        """ Search the partner with the new VAT, if exists, and fills
            owner field and vat_kind"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        tariff_obj = self.pool.get('giscegas.polissa.tarifa')

        if not tariff:
            return res

        tariff_id = tariff_obj.get_tarifa_from_ocsum(cursor, uid, tariff)
        tariff_domain, llistes_preus = self.get_tariff_domain(
            cursor, uid, tariff_id, context
        )

        if not change_retail_tariff:
            default_retail_tariff = self._get_default_retail_tariff(
                cursor, uid, context=context)
            if default_retail_tariff in llistes_preus:
                res['value'].update({'retail_tariff': default_retail_tariff,
                                     'change_retail_tariff': False}
                                    )
            else:
                res['value'].update({'change_retail_tariff': True})
                res['warning'] = {
                    'title': _(u'Tarifa Comercialitzadora Incorrecte'),
                    'message': _(u'La tarifa de comercialitzadora original '
                                 u'és incompatible amb la nova tarifa ATR'),
                }
        else:
            res['value'].update({'retail_tariff': False,
                                 'change_retail_tariff': True}
                                )
        res['domain'] = {'retail_tariff': tariff_domain}

        return res

    def onchange_check_retail_tariff(self, cursor, uid, ids,
                                      change_retail_tariff,
                                      retail_tariff,
                                      context=None):
        """ If selected retail tariff is the actual one, unckeck
        change_retail_tariff field"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if context is None:
            context = {}
        tarifa_actual = self._get_default_retail_tariff(cursor, uid,
                                                        context=context)
        same_tariff = tarifa_actual == retail_tariff
        if change_retail_tariff and same_tariff:
            res['value'].update({'change_retail_tariff': False})

        return res

    def get_tariff_domain(self, cursor, uid, tariff_id, context=None):
        """return the sale pricelists domain compatible with a tariff"""
        tariff_obj = self.pool.get('giscegas.polissa.tarifa')
        mdata_obj = self.pool.get('ir.model.data')

        tariff_fields = ['llistes_preus_comptatibles']
        tariff_vals = tariff_obj.read(cursor, uid, tariff_id, tariff_fields)

        price_lists = tariff_vals.get('llistes_preus_comptatibles', [])

        tariff_domain = [('type', '=', 'sale')]
        if price_lists:
            # We don't need atr tariff pricelist
            atr_price_list_id = mdata_obj.get_object_reference(
                cursor, uid,
                'giscegas_facturacio',
                'pricelist_tarifas_gas'
            )[1]
            price_lists.remove(atr_price_list_id)
            tariff_domain.append(('id', 'in', price_lists))

        return tariff_domain, price_lists

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        pol_obj = self.pool.get('giscegas.polissa')
        wizard = self.browse(cursor, uid, ids[0], context)

        pol_id = wizard.contract.id
        config_vals = wizard.read([])[0]

        config_vals['reqtransferdate'] = self.calc_reqtransferdate(
            cursor, uid, wizard.contract, wizard.modeffectdate,
            wizard.reqtransferdate, context=context
        )
        updatereason = wizard.updatereason

        if updatereason in CANVIS_TITULAR and wizard.generate_new_contract == "create":
            config_vals['new_contract_values'] = {
                'titular': config_vals['owner'],
                'cnae': config_vals['cnae'],
                'pagador': config_vals['pagador'],
                'pagador_sel': 'titular',
                'direccio_pagament': config_vals['direccio_pagament'],
                'notificacio': 'titular',
                'altre_p': False,
                'direccio_notificacio': config_vals['direccio_notificacio'],
                'bank': config_vals['bank'],
                'tipo_pago': config_vals['tipo_pago'],
                'payment_mode_id': config_vals['payment_mode_id'],
                'data_firma_contracte': datetime.today().strftime("%Y-%m-%d %H:%M:%S"),
            }

            if config_vals['owner'] != config_vals['pagador']:
                config_vals['new_contract_values']['pagador_sel'] = 'altre_p'

            if config_vals['contact'] == config_vals['pagador']:
                config_vals['new_contract_values']['notificacio'] = 'pagador'
            elif config_vals['contact'] != config_vals['owner']:
                config_vals['new_contract_values']['notificacio'] = 'altre_p'
                config_vals['new_contract_values']['altre_p'] = config_vals['contact']
            config_vals['new_contract_values'].update(context.get("new_contract_extra_vals", {}))
        elif updatereason in CANVIS_TITULAR and wizard.generate_new_contract == "exists":
            config_vals['new_contract_to_link'] = wizard.new_contract.id

        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, '05', config_vals=config_vals,
            context=context
        )

        casos_ids = []
        if res[2]:
            casos_ids.append(res[2])
        casos_json = json.dumps(casos_ids)

        info = "{0}: {1}\n".format(res[0], res[1])
        wizard.write({'state': 'end', 'info': info, 'casos_generats': casos_json})

    def calc_reqtransferdate(self, cursor, uid, polissa, activacio, reqtransferdate=None, context=None):
        if context is None:
            context = {}

        if activacio == '04':
            return reqtransferdate

        data_ultima_lect = polissa.data_ultima_lectura
        if not data_ultima_lect:
            data_ultima_lect = polissa.data_alta or datetime.today().strftime("%Y-%m-%d")

        if activacio == '05':
            return data_ultima_lect
        else:
            dateplus1 = datetime.strptime(data_ultima_lect, "%Y-%m-%d") + timedelta(days=1)
            return dateplus1.strftime("%Y-%m-%d")

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.atr',
            'type': 'ir.actions.act_window'
        }

    def default_get(self, cursor, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(GiscegasAtrWizard05, self).default_get(
            cursor, uid, fields, context=context
        )

        pol_obj = self.pool.get('giscegas.polissa')

        pol_id = context['contract_id']

        pol = pol_obj.browse(cursor, uid, pol_id, context=context)

        res.update({
            'cups': pol.cups.name,
            'contract': pol.id,
            'cnae': pol.cnae.id,
            'owner_pre': pol.titular.id
        })

        if res.get('updatereason') in CANVIS_TITULAR:
            res['generate_new_contract'] = u"exists"
            search_params = [
                ('cups', '=', pol.cups.id),
                ('state', '=', 'esborrany')
            ]
            new_pol_ids = pol_obj.search(cursor, uid, search_params)
            if len(new_pol_ids):
                res['new_contract'] = new_pol_ids[0]
                pol2 = pol_obj.browse(
                    cursor, uid, res['new_contract'], context=context
                )
                if pol.llista_preu != pol2.llista_preu:
                    res['change_retail_tariff'] = True
                    res['retail_tariff'] = pol2.llista_preu.id
                pol = pol2

        res.update(self.update_vals_from_contract(cursor, uid, pol, context=context))

        retorn = dict([(field, res[field]) for field in fields if res.get(field, False)])
        ir_obj = self.pool.get('ir.values')
        for field, value in retorn.items():
            change_default = getattr(self._columns[field], 'change_default')
            if change_default:
                event = '{}={}'.format(field, value)
                for default_value in ir_obj.get(
                        cursor, uid, 'default', event, [[self._name, False]]
                ):
                    retorn[default_value[1]] = default_value[2]

        return retorn

    def update_vals_from_contract(self, cursor, uid, pol, context=None):
        if context is None:
            context = {}

        res = {}
        # Tarif and pots.
        res['tariff'] = pol.tarifa.codi_ocsum
        res['newreqqd'] = pol.caudal_diario
        res.update(self.onchange_tariff(cursor, uid, [], pol.tarifa.codi_ocsum, pol.id, context=context)['value'])

        # Contact
        res['contact'] = pol.direccio_notificacio.partner_id.id
        res['direccio_notificacio'] = pol.direccio_notificacio.id
        # Owner (titular)
        owner = pol.titular
        res['owner'] = owner.id
        # Rao fiscal
        res['pagador'] = pol.pagador.id
        res['direccio_pagament'] = pol.direccio_pagament.id

        # Pagaments info
        res['bank'] = pol.bank and pol.bank.id or False
        res['tipo_pago'] = pol.tipo_pago and pol.tipo_pago.id or False
        res['payment_mode_id'] = pol.payment_mode_id and pol.payment_mode_id.id or False

        res['cnae'] = pol.cnae.id
        return res

    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        casos = self.pool.get("giscegas.atr").browse(cursor, uid, casos_ids)
        if not context:
            context = {}
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscegas.atr',
            'report_name': 'giscegas.atr.info_cas_ATR',
            'report_webkit': "'giscegas_switching/report/info_cas_ATR.mako'",
            'webkit_header': 'report_sense_fons',
            'groups_id': casos_ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
               'casos': casos_ids
            },
        }

    def _get_default_retail_tariff(self, cursor, uid, context=None):
        if context is None:
            context = {}

        pol_id = context.get('contract_id', False)
        if not pol_id:
            return
        pol_obj = self.pool.get('giscegas.polissa')

        contract = pol_obj.browse(cursor, uid, pol_id, context=context)
        return contract.llista_preu.id

    _columns = {
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
        'updatereason': fields.selection(TAULA_MOTIVO_MODIFICACION, "Motivo de Modificación"),
        'cups': fields.char('CUPS', size=22),
        'cnae': fields.many2one('giscemisc.cnae', 'CNAE'),
        'surrogacy': fields.selection(TAULA_SI_NO, "Indicador de Subrogación"),
        'newreqqd': fields.float("Nueva Qd", digits=(16, 7)),
        'tariff': fields.selection(sorted(TAULA_TIPO_PEAJE, key=lambda t: t[1]),
                                   string="Tarifa ATR"),
        'retail_tariff': fields.many2one('product.pricelist',
                                         domain=[('type', '=', 'sale')],
                                         string=u"Tarifa Comercialización"),
        'change_retail_tariff': fields.boolean(
            u'Cambiar Tarifa Comercialización'
        ),
        'contact': fields.many2one('res.partner', 'Contacto'),
        'direccio_notificacio': fields.many2one('res.partner.address', u'Dirección notificació'),
        'comments': fields.text('Comentarios', size=120),
        # owner change
        'owner': fields.many2one('res.partner', 'Nuevo Titular'),
        'owner_pre': fields.many2one('res.partner', 'Titular Actual'),
        'contract': fields.many2one('giscegas.polissa', 'Contracte'),
        'info': fields.text('Info'),
        'modeffectdate': fields.selection(TAULA_MODELO_FECHA_EFECTO, u"Activación", required=True),
        'reqtransferdate': fields.date(u"Fecha Efecto Solicitud"),
        # New contract fields
        'new_contract': fields.many2one('giscegas.polissa', 'Contracte Nou'),
        'generate_new_contract': fields.selection(
            [(u"create", u"Crear contrato nuevo en borrador"),
             (u"exists", u"Utilitzar contrato existente")],
            "Contrato nou"
        ),
        'pagador': fields.many2one('res.partner', u'Razón fiscal'),
        'direccio_pagament': fields.many2one('res.partner.address', u'Dirección fiscal'),
        'bank': fields.many2one('res.partner.bank', 'Cuenta bancaria'),
        'tipo_pago': fields.many2one('payment.type', 'Tipo de pago'),
        'payment_mode_id': fields.many2one('payment.mode', 'Grupo de pago'),
        # Casos Generats
        'casos_generats': fields.text('Casos generados'),
    }
    _defaults = {
        'state': lambda *a: 'init',
        'retail_tariff': _get_default_retail_tariff,
        'change_retail_tariff': lambda *a: False,
        'modeffectdate': lambda *a: '05',
        'casos_generats': lambda *a: json.dumps([]),
        'generate_new_contract': lambda *a: u"exists",
        'new_contract': lambda *a: False,
        'surrogacy': lambda *a: 'S',
        'updatereason': lambda *a: '15',
    }

GiscegasAtrWizard05()
