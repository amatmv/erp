# -*- coding: utf-8 -*-
import json

from osv import osv, fields, orm
from tools.translate import _
from gestionatr.defs_gas import *
from giscegas_atr.utils import calc_operationtype_from_case


class GiscegasAtrWizard46(osv.osv_memory):

    _name = 'giscegas.atr.a146.wizard'

    def _whereiam(self, cursor, uid, context=None):
        """ returns 'distri' or 'comer' """
        cups_obj = self.pool.get('giscegas.cups.ps')
        sw_obj = self.pool.get('giscegas.atr')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        pol_obj = self.pool.get('giscegas.polissa')
        # Created cases
        casos_ids = []
        pol_id = context.get('contract_ids', context.get('contract_id', False))
        config_vals = {
            'atr_anulat_id': wizard.atr_anulat_id.id,
            'operationtype': wizard.operationtype,
            'extrainfo': wizard.extrainfo
        }

        info = ''
        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, '46', config_vals=config_vals, context=context
        )

        info += "%s: %s\n" % (res[0], res[1])
        if res[2]:
            casos_ids.append(res[2])

        casos_json = json.dumps(casos_ids)
        wizard.write({'info': info, 'state': 'end',
                      'casos_generados': casos_json})

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generados
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creados'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.atr',
            'type': 'ir.actions.act_window'
        }

    def _get_atr_anulat_id(self, cursor, uid, context):
        sw_obj = self.pool.get('giscegas.atr')
        pol_id = context.get('contract_id', False)
        if not pol_id:
            return False
        casos_oberts = sw_obj.search(cursor, uid, [('cups_polissa_id', '=', pol_id), ('state', '!=', 'done'), ('proces_id.name', '!=', '44')])
        anulat_id = False
        if len(casos_oberts):
            anulat_id = casos_oberts[0]
        return anulat_id

    def _get_operationtype(self, cursor, uid, context):
        sw_obj = self.pool.get('giscegas.atr')
        pol_id = context.get('contract_id', False)
        if not pol_id:
            return False
        casos_oberts = sw_obj.search(cursor, uid, [('cups_polissa_id', '=', pol_id), ('state', '!=', 'done'), ('proces_id.name', '!=', '44')])
        operationtype = False
        if len(casos_oberts):
            anulat_id = casos_oberts[0]
            operationtype = calc_operationtype_from_case(self.pool, cursor, uid, anulat_id, context=context)
        return operationtype

    def onchange_atr_anulat_id(self, cr, uid, ids, cas_id, context=None):
        return {'value': {'operationtype': calc_operationtype_from_case(self.pool, cr, uid, cas_id, context=context)}}

    _columns = {
        'state': fields.char('Estado', size=16),
        'info': fields.text('Info'),
        'casos_generados': fields.text('Casos generados'),
        # 46 specific data
        'atr_anulat_id': fields.many2one("giscegas.atr", "Caso ATR anulado"),
        'operationtype': fields.selection(TAULA_TIPO_OPERACION_COMER, 'Tipo de Actuación'),
        'extrainfo': fields.text('Comentarios', size=400),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'casos_generados': lambda *a: json.dumps([]),
        # Dades generals
        'atr_anulat_id': _get_atr_anulat_id,
        'operationtype': _get_operationtype,
    }


GiscegasAtrWizard46()
