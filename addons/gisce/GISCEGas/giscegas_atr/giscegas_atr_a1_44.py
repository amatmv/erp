# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_a1_44 as a1_44
from gestionatr.defs_gas import *
from gestionatr.defs import SINO
from tools.translate import _
from datetime import datetime
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException
from giscegas_atr.utils import get_address_dict

NECESSITEN_CLIENT = ['A10001', 'A10002', 'A10003', 'A10005', 'A10010',
                     'A10011', 'A10030', 'A20001', 'A20004']
NECESSITEN_CONTACTE = ['A10001', 'A10002', 'A10003', 'A10005', 'A10010',
                       'A10011', 'A10030', 'A20004']
NECESSITEN_LECTURES = ['A20001']

class GiscegasAtrProces44(osv.osv):

    _name = 'giscegas.atr.proces'
    _inherit = 'giscegas.atr.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 initial steps depending on where we are '''

        if proces == '44':
            return ['a1']  # In both comer and distri

        return super(GiscegasAtrProces44,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 emisor steps depending on where we are '''
        if proces == '44':
            if where == 'distri':
                return ['a2', 'a25', 'a3', 'a4']
            elif where == 'comer':
                return ['a1']

        return super(GiscegasAtrProces44,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == '44':
            return ['a2', 'a4']

        return super(GiscegasAtrProces44,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        if proces == '44':
            return []

        return super(GiscegasAtrProces44, self).get_old_company_steps(
            cursor, uid, proces, context
        )


GiscegasAtrProces44()


class GiscegasAtr44(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == '44':
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == 'a2':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                elif step_name in ('a3', 'a4'):
                    return True

        return super(GiscegasAtr44,
                     self).get_final(cursor, uid, sw, context=context)

GiscegasAtr44()


class GiscegasAtr44A1(osv.osv):
    """ Classe pel pas a1
    """

    _name = "giscegas.atr.44.a1"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a1'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '44', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr44A1,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML 44, pas a1
        """
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        heading = pas.header_id.generar_xml(pas)
        a144 = a1_44.A144()
        register_doc_obj = self.pool.get('giscegas.atr.document')
        docs_xml = register_doc_obj.generar_xml(cr, uid, pas, context=context)
        tipus = pas.operationtype
        vals_a144 = {
            'reqdate': pas.header_id.date_created.split(" ")[0],
            'reqhour': pas.header_id.date_created.split(" ")[1],
            'comreferencenum': sw.codi_sollicitud,
            'cups': sw.cups_id.name[:20],
            'extrainfo': pas.extrainfo,
            'registerdoclist': docs_xml,
            'operationtype': pas.operationtype,
            'description': pas.description,
            'operationnum': pas.operationnum,
        }
        if tipus in NECESSITEN_CLIENT:
            vals_a144.update({
                'firstname': pas.firstname,
                'lastname': pas.lastname,
                'secondname': pas.secondname,
                'sourcetype': pas.sourcetype,
            })
        if tipus in NECESSITEN_CONTACTE:
            vals_a144.update({
                'prefixtel1': pas.prefixtel1 if pas.telephone1 else False,
                'telephone1': pas.telephone1,
                'prefixtel2': pas.prefixtel2 if pas.telephone2 else False,
                'telephone2': pas.telephone2,
            })
        if tipus in NECESSITEN_LECTURES:
            vals_a144.update({
                'readingdate': pas.readingdate.split(" ")[0],
                'readingvalue': pas.readingvalue,
            })
        a144.feed(vals_a144)
        msg = a1_44.MensajeA144()
        msg.feed({
            'heading': heading,
            'a1': a144,
        })
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        raise Exception("Importación de 44 paso a1 no implementada")

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        mandatory_fields = ['operationtype', 'description']
        if vals.get('operationtype') in NECESSITEN_CLIENT:
            mandatory_fields += ['firstname', 'lastname', 'sourcetype']
        if vals.get('operationtype') in NECESSITEN_CONTACTE:
            mandatory_fields += ['telephone1', 'prefixtel1']
        if vals.get('operationtype') in NECESSITEN_LECTURES:
            mandatory_fields += ['readingdate', 'readingvalue']

        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)

        non_completed = []
        for field in mandatory_fields:
            if not vals2.get(field, False):
                non_completed.append(field)

        if len(non_completed):
            raise GiscegasAtrException(_("Faltan datos. Campos no completados."), non_completed)

        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """
        """
        if not context:
            context = {}

        if not vals:
            return True

        self.config_step_validation(cursor, uid, ids, vals, context=context)
        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)
        self.write(cursor, uid, ids, vals2, context=context)
        return True

    def prepare_for_validation(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}

        if not vals:
            return vals

        part_obj = self.pool.get("res.partner")
        addr_obj = self.pool.get("res.partner.address")
        sw_obj = self.pool.get("giscegas.atr")
        vals2 = {
            'sourcetype': vals.get('sourcetype'),
            'operationnum': vals.get('operationnum'),
            'operationtype': vals.get('operationtype'),
            'description': vals.get('description'),
            'readingdate': vals.get('readingdate'),
            'readingvalue': vals.get('readingvalue'),
            'extrainfo': vals.get('extrainfo'),
        }
        if not vals.get('description'):
            descripcio = ""
            for tipus, desc in TAULA_TIPO_OPERACION_COMER:
                if tipus == vals.get('operationtype'):
                    descripcio = desc
                    break
            vals2['description'] = descripcio

        if vals.get("titular_id"):
            titular = part_obj.browse(cursor, uid, vals.get("titular_id"))
            vat_info = sw_obj.get_vat_info(cursor, uid, titular.vat, False)
            es_empresa = vat_info['is_enterprise']
            nom = part_obj.separa_cognoms(cursor, uid, titular.name)
            if not es_empresa:
                vals2.update({
                    'firstname': nom['nom'],
                    'lastname': nom['cognoms'][0],
                    'secondname': nom['cognoms'][1],
                })
            else:
                vals2.update({
                    'firstname': '',
                    'lastname': titular.name,
                    'secondname': '',
                })
        if vals.get("telefono1"):
            vals2.update({
                'telephone1': vals.get("telefono1"),
                'prefixtel1': '34'
            })
        if vals.get("telefono2"):
            vals2.update({
                'telephone2': vals.get("telefono2"),
                'prefixtel2': '34'
            })
        if vals.get("direccio_notificacio") and not vals.get("telefono1"):
            addr = addr_obj.browse(cursor, uid, vals.get("direccio_notificacio"), context)
            vals2.update({
                'telephone1': addr.phone or addr.mobile,
                'prefixtel1': '34'
            })
            if addr.mobile and addr.phone:
                vals2.update({
                    'telephone2': addr.mobile,
                    'prefixtel2': '34'
                })
        return vals2

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'sourcetype': '06',  # Comer
            'operationtype': 'A10011',  # Corte
            'description': 'Cierre por falta de pago',
            'firstname': vals['firstname'],
            'lastname': vals['familyname1'],
            'secondname': vals['familyname2'],
        })
        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_44_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        base_msg = u"{0}: {1}".format(step.operationtype, step.description)
        if step.lastame:
            nom_complet = step.lastame
            if step.firstname:
                nom_complet = step.firstname + " " + nom_complet
            if step.secondname:
                nom_complet = nom_complet + " " + step.secondname
            base_msg += u" ({0})".format(nom_complet)
        return base_msg + ". "

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        # Solicitante. Por defecto '06' -> Comercializadora
        'sourcetype': fields.selection(TAULA_TIPO_SOLICITANTE, 'Tipo de Solicitante'),
        # Datos titular del suministro
        'firstname': fields.char('Nombre Cliente', size=40),
        'lastname': fields.char('Apellido 1 / Razón Social', size=30),
        'secondname': fields.char('Apellido 2', size=30),
        'prefixtel1': fields.char('Prefijo', size=4),
        'telephone1': fields.char('Teléfono', size=20),
        'prefixtel2': fields.char('Prefijo 2', size=4),
        'telephone2': fields.char('Teléfono 2', size=20),
        # Datos actuacion solicitada. Operacion por defecto: 'A10011' -> Corte
        'operationtype': fields.selection(TAULA_TIPO_OPERACION_COMER, 'Tipo de Actuación'),
        'description': fields.text('Descripción de la Actuación Solicitada', size=400),
        'operationnum': fields.char('Número de Operación', size=40),
        'readingdate': fields.datetime('Fecha de Lectura Actual'),
        'readingvalue': fields.float('Lectura Actual', digits=(16, 2)),
        'extrainfo': fields.text('Comentarios', size=400),
    }

    _defaults = {
        'sourcetype': lambda *a: '06',
        'operationtype': lambda *a: 'A10011',
        'description': lambda *a: 'Cierre por falta de pago',
        'prefixtel1': lambda *a: '34',
        'prefixtel2': lambda *a: '34',
    }


GiscegasAtr44A1()


class GiscegasAtr44A2(osv.osv):
    """ Classe pel pas a2
    """

    _name = "giscegas.atr.44.a2"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a2'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '44', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr44A2,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a25', 'a3', 'a4']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 44 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        denegat = xml.result not in ['01', '09']

        rebuig_ids = []
        if denegat:
            rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                    context=context)

        vals.update({
            'reqcode': xml.reqcode,
            'operationtype': xml.operationtype,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'srcode': xml.srcode,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': denegat,
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_44_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.44.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0])
        if step['rebuig']:
            return _(u"{0}Rechazo: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def onchange_result(self, cursor, uid, ids, result_id, context=None):
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}
        vals = {}

        if result_id in ['01', '09']:
            vals['rebuig'] = False
        else:
            vals['rebuig'] = True

        res['value'] = vals
        return res

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header', required=True),
        'reqcode': fields.char('Código Solicitud', size=10),
        'operationtype': fields.selection(TAULA_TIPO_OPERACION_COMER, u'Tipo de Actuación'),
        'result': fields.selection(TAULA_RESULTADO, 'Código de Resultado'),
        'resultdesc': fields.text('Descripción de Resultado', size=100),
        'srcode': fields.char('Código de Petición del Distribuidor', size=20),
        'extrainfo': fields.text('Observaciones de la Respuesta de Actuación', size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motivo Rechazo", type='char',
                                        size=250, readonly=True, ),
    }

    _defaults = {
        'rebuig': lambda *a: False,
    }


GiscegasAtr44A2()


class GiscegasAtr44A3(osv.osv):
    """ Classe pel pas a3
    """

    _name = "giscegas.atr.44.a3"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a3'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '44', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr44A3,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 44 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")
        counter_ids = header_obj.create_from_xml_counters(cursor, uid, xml.counterlist, context=context)
        doc_ids = header_obj.create_from_xml_doc(cursor, uid, xml.registerdoclist, context=context)

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        vals.update({
            'reqcode': xml.reqcode,
            'operationtype': xml.operationtype,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'activationtype': xml.activationtype,
            'activationtypedesc': xml.activationtypedesc,
            'closingtype': xml.closingtype,
            'closingtypedesc': xml.closingtypedesc,
            'reqdescription': xml.reqdescription,
            'resultinspection': xml.resultinspection,
            'resultinspectiondesc': xml.resultinspectiondesc,
            'operationnum': xml.operationnum,
            'visitnumber': xml.visitnumber,
            'counterchange': xml.counterchange,
            'removallecture': xml.removallecture,
            'supplystatus': xml.supplystatus,
            'supplystatusdesc': xml.supplystatusdesc,
            'servicestatus': xml.servicestatus,
            'servicestatusdesc': xml.servicestatusdesc,
            'extrainfo': xml.extrainfo,
            'conceptnumber': xml.conceptnumber,
            'counter_ids': [(6, 0, counter_ids)],
            'document_ids': [(6, 0, doc_ids)]
        })
        if xml.interventiondate:
            inter_from = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhour,
                "%Y-%m-%d %H:%M:%S"
            )
            vals.update({
                'interventiondate': inter_from,
            })
        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_44_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header', required=True),
        'reqcode': fields.char('Código Solicitud', size=10),
        'operationtype': fields.selection(TAULA_TIPO_OPERACION_COMER, 'Tipo de Actuación'),
        'result': fields.selection(TAULA_RESULTADO, 'Código de Resultado'),
        'resultdesc': fields.text('Descripción de Resultado', size=100),
        'activationtype': fields.selection(TAULA_TIPO_DE_ACTIVACION, 'Tipo de Activación'),
        'activationtypedesc': fields.text('Descripción de Tipo de Activación', size=100),
        'closingtype': fields.selection(TAULA_TIPO_CIERRE, 'Tipo de Cierre'),
        'closingtypedesc': fields.text('Descripción del Tipo de Cierre', size=100),
        'reqdescription': fields.text('Descripción Detallada de la Respuesta de la Actuación.', size=255),
        'interventiondate': fields.datetime('Fecha Intervención'),
        'resultinspection': fields.selection(TAULA_RESULTADO_INSPECCION, 'Resultado Inspección'),
        'resultinspectiondesc': fields.text('Descripción de Resultado Inspección', size=250),
        'operationnum': fields.char('Número de Operación Generado', size=40),
        'visitnumber': fields.integer('Número de Visita', size=3),
        'counterchange': fields.selection(TAULA_INDICATIVO_SI, 'Cambio de Contador'),
        'removallecture': fields.integer('Lectura de Levantamiento', size=9),
        'supplystatus': fields.selection(TAULA_SITUACION_SUMINISTRO, 'Situación del Suministro'),
        'supplystatusdesc': fields.text('Descripción Situación del Suministro', size=100),
        'servicestatus': fields.selection(TAULA_SITUACION_SERVICIO, 'Situación del Servicio'),
        'servicestatusdesc': fields.text('Descripción Situación del Servicio', size=100),
        'extrainfo': fields.text('Observaciones de la intervención', size=400),
        'conceptnumber': fields.integer('Número de Conceptos', size=2),
    }

    _defaults = {

    }


GiscegasAtr44A3()


class GiscegasAtr44A4(osv.osv):
    """ Classe pel pas a4
    """

    _name = "giscegas.atr.44.a4"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a4'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '44', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr44A4,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 44 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                context=context)
        counter_ids = header_obj.create_from_xml_counters(cursor, uid, xml.counterlist, context=context)
        doc_ids = header_obj.create_from_xml_doc(cursor, uid, xml.registerdoclist, context=context)
        vals.update({
            'reqcode': xml.reqcode,
            'operationtype': xml.operationtype,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'closingtype': xml.closingtype,
            'closingtypedesc': xml.closingtypedesc,
            'reqdescription': xml.reqdescription,
            'resultinspection': xml.resultinspection,
            'resultinspectiondesc': xml.resultinspectiondesc,
            'operationnum': xml.operationnum,
            'visitnumber': xml.visitnumber,
            'counterchange': xml.counterchange,
            'removallecture': xml.removallecture,
            'supplystatus': xml.supplystatus,
            'supplystatusdesc': xml.supplystatusdesc,
            'servicestatus': xml.servicestatus,
            'servicestatusdesc': xml.servicestatusdesc,
            'conceptnumber': xml.conceptnumber,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': True,
            'counter_ids': [(6, 0, counter_ids)],
            'document_ids': [(6, 0, doc_ids)]
        })
        if xml.interventiondate:
            inter_from = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhour,
                "%Y-%m-%d %H:%M:%S"
            )
            vals.update({
                'interventiondate': inter_from,
            })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_44_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.44.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0])
        if step['rebuig']:
            return _(u"{0}Rechazo: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        'reqcode': fields.char('Código Solicitud', size=10),
        'comreferencenum': fields.char('Nº Referencia Solicitud Comercializadora. ', size=12),
        'operationtype': fields.selection(TAULA_TIPO_OPERACION_COMER, 'Tipo de Actuación'),
        'result': fields.selection(TAULA_RESULTADO, 'Código de Resultado'),
        'resultdesc': fields.text('Descripción de Resultado', size=100),
        'closingtype': fields.selection(TAULA_TIPO_CIERRE, 'Tipo de Cierre'),
        'closingtypedesc': fields.text('Descripción del Tipo de Cierre', size=100),
        'reqdescription': fields.text('Descripción Detallada de la Respuesta de la Actuación.', size=255),
        'interventiondate': fields.datetime('Fecha Intervención'),
        'resultinspection': fields.selection(TAULA_RESULTADO_INSPECCION, 'Resultado Inspección'),
        'resultinspectiondesc': fields.text('Descripción de Resultado Inspección', size=250),
        'operationnum': fields.char('Número de Operación Generado', size=40),
        'visitnumber': fields.integer('Número de Visita', size=3),
        'counterchange': fields.selection(TAULA_INDICATIVO_SI, 'Cambio de Contador'),
        'removallecture': fields.integer('Lectura de Levantamiento', size=9),
        'supplystatus': fields.selection(TAULA_SITUACION_SUMINISTRO, 'Situación del Suministro'),
        'supplystatusdesc': fields.text('Descripción Situación del Suministro', size=100),
        'servicestatus': fields.selection(TAULA_SITUACION_SERVICIO, 'Situación del Servicio'),
        'servicestatusdesc': fields.text('Descripción Situación del Servicio', size=100),
        'conceptnumber': fields.integer('Número de Conceptos', size=2),
        'extrainfo': fields.text('Comentarios', size=255),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motivo Rechazo", type='char',
                                        size=250, readonly=True, ),
    }

    _defaults = {
        'rebuig': lambda *a: True,
    }


GiscegasAtr44A4()
