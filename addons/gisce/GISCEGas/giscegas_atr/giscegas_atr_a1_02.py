# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_a1_02 as a1_02
from gestionatr.defs_gas import *
from gestionatr.defs import SINO
from tools.translate import _
from datetime import datetime
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException
from giscegas_atr.utils import get_address_dict


class GiscegasAtrProces02(osv.osv):

    _name = 'giscegas.atr.proces'
    _inherit = 'giscegas.atr.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 initial steps depending on where we are '''

        if proces == '02':
            return ['a1', 'a2s', 'a3s']  # In both comer and distri

        return super(GiscegasAtrProces02,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 emisor steps depending on where we are '''
        if proces == '02':
            if where == 'distri':
                return ['a2', 'a2s', 'a25', 'a3', 'a3s', 'a4', 'a4s']
            elif where == 'comer':
                return ['a1']

        return super(GiscegasAtrProces02,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == '02':
            return ['a2', 'a4', 'a4s']

        return super(GiscegasAtrProces02,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        if proces == '02':
            return ['a2s', 'a3s', 'a4s']

        return super(GiscegasAtrProces02, self).get_old_company_steps(
            cursor, uid, proces, context
        )


GiscegasAtrProces02()


class GiscegasAtr02(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == '02':
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == 'a2':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                elif step_name in ('a3', 'a4', 'a3s', 'a4s'):
                    return True

        return super(GiscegasAtr02,
                     self).get_final(cursor, uid, sw, context=context)

GiscegasAtr02()


class GiscegasAtr02A1(osv.osv):
    """ Classe pel pas a1
    """

    _name = "giscegas.atr.02.a1"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a1'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '02', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr02A1,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        heading = pas.header_id.generar_xml(pas)
        a102 = a1_02.A102()
        vals_a102 = {}
        modeffectdate = pas.modeffectdate
        documenttype = pas.documenttype

        vals_a102.update({
            'nationality': pas.nationality,
            'documenttype': pas.documenttype,
            'documentnum': pas.documentnum,
        })

        if modeffectdate in ['03', '04']:
            vals_a102['reqtransferdate'] = pas.reqtransferdate

        if documenttype in ['07', '08']:
            vals_a102['titulartype'] = pas.titulartype

        register_doc_obj = self.pool.get('giscegas.atr.document')
        docs_xml = register_doc_obj.generar_xml(cr, uid, pas, context=context)

        vals_a102.update({
            'comreferencenum': sw.codi_sollicitud,
            'reqdate': pas.header_id.date_created.split(" ")[0],
            'reqhour': pas.header_id.date_created.split(" ")[1],
            'cups': sw.cups_id.name[:20],
            'modeffectdate': modeffectdate,
            'reqestimatedqa': pas.reqestimatedqa,
            'disconnectedserviceaccepted': pas.disconnectedserviceaccepted,
            'registerdoclist': docs_xml,
            'extrainfo': pas.extrainfo,
        })
        if pas.reqqd:
            vals_a102.update({
                'reqqd': pas.reqqd,
            })
        a102.feed(vals_a102)
        msg = a1_02.MensajeA102()
        msg.feed({
            'heading': heading,
            'a102': a102,
        })
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        raise Exception("Importación de 02 paso a1 no implementada")

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        if not vals.get("reqestimatedqa", 0):
            raise GiscegasAtrException(
                _("El caudal anual  no puede ser 0. Revisad el caudal diario."), "reqestimatedqa"
            )
        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """
        """
        if not context:
            context = {}
        self.config_step_validation(cursor, uid, ids, vals, context=context)
        if not vals:
            return True
        self.write(cursor, uid, ids, vals, context=context)
        return True

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        titular_vat = polissa.titular.vat
        participant_obj = self.pool.get("giscemisc.participant")
        participant = participant_obj.get_from_partner(cursor, uid, polissa.distribuidora.id)
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, participant.codi_sifco
        )

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'reqtransferdate': today,
            'modeffectdate': '05',  # Cuanto antes
            'disconnectedserviceaccepted': "N",
            'nationality':
                polissa.titular.country and polissa.titular.country.code,
            'documenttype': vat_info['document_type'],
            'documentnum': vat_info['vat'],
            'reqqd': polissa.caudal_diario,
            'reqestimatedqa': polissa.caudal_diario*365,
        })
        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_02_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        data_02 = self.read(cursor, uid, step_id, ['reqtransferdate', 'modeffectdate', 'sw_id', 'reqestimatedqa'])
        accio = data_02['modeffectdate']
        for elem in TAULA_MODELO_FECHA_EFECTO:
            if elem[0] == data_02['modeffectdate']:
                accio = elem[1]
                break
        data_acc = ''
        if data_02['modeffectdate']:
            data_acc = _(u". Fecha prevista: {0}").format(data_02['reqtransferdate'])

        sw = self.pool.get("giscegas.atr").browse(cursor, uid, data_02['sw_id'][0])
        extra_info = _(u"Caudal Anual: {0}. ").format(data_02['reqestimatedqa'])
        if sw.cups_polissa_id:
            pol = sw.cups_polissa_id
            extra_info += _(u"Tarifa:{0}").format(pol.tarifa.name)

        return _(u"Activación: {0}{1}. {2}").format(accio, data_acc, extra_info)

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        'titulartype': fields.selection(TAULA_TIPO_PERSONA, "Tipo de Titular"),
        'nationality': fields.selection(TAULA_NACIONALIDAD, "Nacionalidad"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO,
                                         "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        'reqqd': fields.float("Caudal Diario Qd", digits=(16, 7)),
        'reqestimatedqa': fields.integer("Caudal Estimado Anual Qa", digits=12),
        'modeffectdate': fields.selection(TAULA_MODELO_FECHA_EFECTO,
                                          "Modelo de Fecha Efecto"),
        'reqtransferdate': fields.date("Fecha de Efecto Solicitada"),
        'disconnectedserviceaccepted': fields.selection(
            SINO, "Contratación incondicional baja"),
        'extrainfo': fields.text("Observaciones de la Solicitud", size=400),
    }

    _defaults = {

    }


GiscegasAtr02A1()


class GiscegasAtr02A2(osv.osv):
    """ Classe pel pas a2
    """

    _name = "giscegas.atr.02.a2"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a2'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '02', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr02A2,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2s', 'a25', 'a3', 'a4']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        if not context:
            context = {}

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        msg = a1_02.MensajeA102()

        msg.feed({})
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        denegat = xml.result not in ['01', '09']

        rebuig_ids = []
        if denegat:
            rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                    context=context)

        vals.update({
            'reqcode': xml.reqcode,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'tolltype': xml.tolltype,
            'qdgranted': xml.qdgranted,
            'outgoingpressuregranted': xml.outgoingpressuregranted,
            'singlenomination': xml.singlenomination,
            'netsituation': xml.netsituation,
            'newmodeffectdate': xml.newmodeffectdate,
            'foreseentransferdate': xml.foreseentransferdate,
            'StatusPS': xml.StatusPS,
            'extrainfo': xml.extrainfo,
            'rebuig': denegat,
            'rebuig_ids': [(6, 0, rebuig_ids)],
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_02_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.02.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rechazo: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def onchange_result(self, cursor, uid, ids, result_id, context=None):
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}
        vals = {}

        if result_id in ['01', '09']:
            vals['rebuig'] = False
        else:
            vals['rebuig'] = True

        res['value'] = vals
        return res

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        'reqcode': fields.char("Código Solicitud", size=10),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'nationality': fields.selection(TAULA_NACIONALIDAD,
                                        "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO,
                                         "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        'tolltype': fields.selection(TAULA_TIPO_PEAJE, "Tipo de Peaje"),
        'qdgranted': fields.float("Qd Concedida", digits=(16, 7)),
        'outgoingpressuregranted': fields.float("Presión Salida Concedida",
                                                digits=(8, 3)),
        'singlenomination': fields.selection(TAULA_SI_NO,
                                             "Nominación Individual"),
        'netsituation': fields.char("Red o Municipio de Ubicación", size=14),
        'newmodeffectdate': fields.selection(TAULA_MODELO_FECHA_EFECTO_PREVISTA,
                                             "Modelo de fecha efecto a aplicar "
                                             "para la solicitud"),
        'foreseentransferdate': fields.date("Fecha Prevista de Activación"),
        'StatusPS': fields.selection(TAULA_ESTADO_PUNTO_DE_SUMINISTRO,
                                     "Situación del PS"),
        'extrainfo': fields.text("Observaciones de la Validación", size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motivo Rechazo", type='char',
                                        size=250, readonly=True, ),
    }

    _defaults = {
        'rebuig': lambda *a: False,
    }


GiscegasAtr02A2()


class GiscegasAtr02A3(osv.osv):
    """ Classe pel pas a3
    """

    _name = "giscegas.atr.02.a3"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a3'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '02', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr02A3,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        if not context:
            context = {}

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        msg = a1_02.MensajeA102()

        msg.feed({})
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        counter_ids = header_obj.create_from_xml_counters(
            cursor, uid, xml.counterlist, context=context)
        corrector_ids = header_obj.create_from_xml_correctors(
            cursor, uid, xml.correctorlist, context=context)

        # CNAE
        if xml.caecode:
            cnae_id = sw_obj.search_cnae(cursor, uid, xml.caecode, context=context)
            vals.update({'caecode': cnae_id})

        vals.update({
            'reqcode': xml.reqcode,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'atrcode': xml.atrcode,
            'transfereffectivedate': xml.transfereffectivedate,
            'telemetering': xml.telemetering,
            'finalclientyearlyconsumption': xml.finalclientyearlyconsumption,
            'gasusetype': xml.gasusetype,
            'titulartype': xml.titulartype,
            'canonircperiodicity': xml.canonircperiodicity,
            'lastinspectionsdate': xml.lastinspectionsdate,
            'lastinspectionsresult': xml.lastinspectionsresult,
            'StatusPS': xml.StatusPS,
            'readingtype': xml.readingtype,
            'lectureperiodicity': xml.lectureperiodicity,
            'extrainfo': xml.extrainfo,
            'counter_ids': [(6, 0, counter_ids)],
            'corrector_ids': [(6, 0, corrector_ids)]
        })

        partner_ids = partner_obj.search(cursor, uid, [('vat', 'like',
                                                        xml.documentnum)])
        if partner_ids:
            vals.update({
                'titular_id': partner_ids[0]
            })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_02_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header',
                                     required=True),
        # Dades Activacio
        'atrcode': fields.char("Código Contrato ATR", size=24),
        'reqcode': fields.char("Código Solicitud", size=10),
        'transfereffectivedate': fields.date("Fecha Efecto Solicitud (inclusive)"),
        'extrainfo': fields.text("Observaciones de la Realización", size=400),
        # Dades Client
        'titular_id': fields.many2one("res.partner", "Cliente relacionado"),
        'nationality': fields.selection(TAULA_NACIONALIDAD,
                                        "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO,
                                         "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        'titulartype': fields.selection(TAULA_TIPO_PERSONA, "Tipo de Persona"),
        # Dades Facturacio
        'telemetering': fields.selection(SINO, "Dispone de Telemedida"),
        'finalclientyearlyconsumption': fields.integer( "Caudal Anual Actual Final"),
        'gasusetype': fields.selection(TAULA_TIPOS_DE_USO_DEL_GAS,
                                       "Tipo de Uso del Gas"),
        'canonircperiodicity': fields.selection(TAULA_PERIODICIDAD_CANON_IRC,
                                                "Periodicidad Canon IRC"),
        'StatusPS': fields.selection(TAULA_ESTADO_PUNTO_DE_SUMINISTRO,
                                     "Situación del PS"),
        'readingtype': fields.selection(TAULA_TIPO_LECTURA, "Tipo de Lectura"),
        'lectureperiodicity': fields.selection(TAULA_PERIODICIDAD_LECTURA,
                                               "Periodicidad de Lectura"),
        # Dades Incidencia
        'lastinspectionsdate': fields.date("Fecha Ultima Inspección"),
        'lastinspectionsresult': fields.selection(
            TAULA_RESULTADO_INSPECCION, "Resultado Ultima Inspección"),
        'caecode': fields.many2one('giscemisc.cnae', 'Nuevo Código CAE'),
    }

    _defaults = {

    }


GiscegasAtr02A3()


class GiscegasAtr02A3s(osv.osv):
    """ Classe pel pas a3
    """

    _name = "giscegas.atr.02.a3s"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a3s'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '02', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr02A3s,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        if not context:
            context = {}

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        msg = a1_02.MensajeA102()

        msg.feed({})
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        counter_ids = header_obj.create_from_xml_counters(
            cursor, uid, xml.counterlist, context=context
        )
        corrector_ids = header_obj.create_from_xml_correctors(
            cursor, uid, xml.correctorlist, context=context
        )

        vals.update({
            'reqcode': xml.reqcode,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'transfereffectivedate': xml.transfereffectivedate,
            'previousatrcode': xml.previousatrcode,
            'readingtype': xml.readingtype,
            'extrainfo': xml.extrainfo,
            'counter_ids': [(6, 0, counter_ids)],
            'corrector_ids': [(6, 0, corrector_ids)],
        })

        partner_ids = partner_obj.search(cursor, uid, [('vat', 'like',
                                                        xml.documentnum)])
        if partner_ids:
            vals.update({
                'titular_id': partner_ids[0]
            })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_02_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        sw_obj = self.pool.get("giscegas.atr")
        step = self.read(cursor, uid, step_id, ['sw_id', 'transfereffectivedate'])
        if sw_obj.whereiam(cursor, uid, False, context=context) == 'comer':
            return _(u"Fecha Activación: {0}").format(step['transfereffectivedate'])
        else:
            sw_id = step['sw_id'][0]
            step01 = self.pool.get('giscegas.atr.02.a1')
            step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
            antinf = ''
            if len(step01_id) > 0:
                antinf = step01.get_additional_info(cursor, uid,  step01_id[0])
                antinf += "."
            return antinf

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header',
                                     required=True),
        # Dades Activacio
        'previousatrcode': fields.char("Código Contrato ATR", size=24),
        'reqcode': fields.char("Código Solicitud", size=10),
        'transfereffectivedate': fields.date(
            "Fecha Efecto Solicitud (inclusive)"),
        'extrainfo': fields.text("Observaciones de la Realización", size=400),
        # Dades Client
        'titular_id': fields.many2one("res.partner", "Cliente relacionado"),
        'nationality': fields.selection(TAULA_NACIONALIDAD,
                                        "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO,
                                         "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        # Dades Facturacio
        'readingtype': fields.selection(TAULA_TIPO_LECTURA, "Tipo de Lectura"),
    }

    _defaults = {

    }


GiscegasAtr02A3s()


class GiscegasAtr02A4(osv.osv):
    """ Classe pel pas a4
    """

    _name = "giscegas.atr.02.a4"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a4'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '02', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr02A4,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        if not context:
            context = {}

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        msg = a1_02.MensajeA102()

        msg.feed({})
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                context=context)
        vals.update({
            'reqcode': xml.reqcode,
            'titulartype': xml.titulartype,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': True,
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_02_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.02.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rechazo: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        'reqcode': fields.char("Código Solicitud", size=10),
        'nationality': fields.selection(TAULA_NACIONALIDAD,
                                        "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO,
                                         "Tipo Documento de Identificación"),
        'titulartype': fields.selection(TAULA_TIPO_PERSONA, "Tipo de Titular"),
        'documentnum': fields.char("Nº Documento", size=20),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'extrainfo': fields.text("Observaciones de No Realización", size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motivo Rechazo", type='char',
                                        size=250, readonly=True, ),
    }

    _defaults = {
        'rebuig': lambda *a: True,
    }


GiscegasAtr02A4()
