from enerdata.contracts.normalized_power import NormalizedPower
from enerdata.contracts.tariff import get_tariff_by_code, are_powers_ascending
from enerdata.contracts.tariff import NotNormalizedPower, IncorrectMaxPower
from osv import osv
from .utils import *
from gestionatr.defs import *
from gestionatr.input.messages.R1 import *
import vatnumber
import base64


class Rebuig(object):

    pass
