# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime


class Test05(TestSwitchingImport):

    def test_creation_a105(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '05', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.05.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            a105 = step_obj.browse(cursor, uid, step_id)
            a1_05 = sw_obj.browse(cursor, uid, a105.sw_id.id)
            participant_obj = self.openerp.pool.get('giscemisc.participant')
            participant_partner = participant_obj.get_from_partner(cursor, uid, a1_05.partner_id.id)
            self.assertEqual(a1_05.proces_id.name, '05')
            self.assertEqual(a1_05.step_id.name, 'a1')
            self.assertEqual(participant_partner.codi_sifco, '1234')
            self.assertEqual(a1_05.company_id.codi_sifco, '4321')
            self.assertEqual(a1_05.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(a105.reqtransferdate, datetime.today().strftime("%Y-%m-%d"))
            self.assertEqual(a105.modeffectdate, '05')
            self.assertEqual(a105.updatereason, '11')
            self.assertEqual(a105.nationality, 'CH')
            self.assertEqual(a105.documenttype, '01')
            self.assertEqual(a105.documentnum, 'B82420654')
            self.assertEqual(a105.newtolltype, "31")
            self.assertEqual(len(a105.document_ids), 0)
            self.assertEqual(
                a1_05.additional_info,
                u'(11) Tarifa: 3.1 -> 3.1. Caudal Diario: 1.5. '
            )

    def test_export_a105_11(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '05', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.05.a1')
            register_doc_obj = self.openerp.pool.get('giscegas.atr.document')

            docs_vals_1 = {
                'doctype': '03',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-02',
                'extrainfo': '404pagenotfound',
            }
            created_doc_1 = register_doc_obj.create(cursor, uid, docs_vals_1)
            docs_vals_2 = {
                'doctype': '01',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-03',
                'extrainfo': '404pagenotfound',
            }
            created_doc_2 = register_doc_obj.create(cursor, uid, docs_vals_2)

            a105 = step_obj.browse(cursor, uid, step_id)
            a105.write({
                'updatereason': "11",
                'newtolltype': "32",
                'document_ids': [(6, 0, [created_doc_1, created_doc_2])],
            })
            a105.header_id.write({'date_created': a105.header_id.date_created})
            a105 = step_obj.browse(cursor, uid, step_id)
            self.assertEqual(
                a105.sw_id.additional_info,
                u'(11) Tarifa: 3.1 -> 3.2. Caudal Diario: 1.5. '
            )

            a105_01_generat = a105.generar_xml()[1]
            a105_01_generat = a105_01_generat.replace(' ', '').replace('\n', '')

            a105_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '05_a1_11.xml'
            )
            with open(a105_01_xml_path, 'r') as f:
                a105_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a105.sw_id.codi_sollicitud
                data_emissio = a105.header_id.date_created.split(" ")[0]
                hora_emissio = a105.header_id.date_created.split(" ")[1]
                a105_01_xml = a105_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a105_01_generat, a105_01_xml)

    def test_export_a105_01(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '05', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.05.a1')

            a105 = step_obj.browse(cursor, uid, step_id)
            a105.write({
                'updatereason': "01",
                'nationality': "CH",
                'documenttype': "01",
                'documentnum': "00000000T",
                'modeffectdate': "05",
                'newnationality': "AF",
                'newdocumenttype': "03",
                'newdocumentnum': "00000000T",
                'newfirstname': "Gas",
                'newfamilyname1': "Al",
                'newfamilyname2': "Matalas",
                'newtitulartype': "F",
                'newregularaddress': "S",
                'newtelephone': "999888777",
                'newfax': "111222333",
                'newemail': "gasalmatalas@atr",
            })
            a105.header_id.write({'date_created': a105.header_id.date_created})
            a105 = step_obj.browse(cursor, uid, step_id)
            self.assertEqual(
                a105.sw_id.additional_info,
                u'(01) Tarifa:3.1. Caudal Diario: 1.5. Titular: ESB82420654 -> 00000000T. '
            )

            a105_01_generat = a105.generar_xml()[1]
            a105_01_generat = a105_01_generat.replace(' ', '').replace('\n', '')

            a105_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '05_a1_01.xml'
            )
            with open(a105_01_xml_path, 'r') as f:
                a105_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a105.sw_id.codi_sollicitud
                data_emissio = a105.header_id.date_created.split(" ")[0]
                hora_emissio = a105.header_id.date_created.split(" ")[1]
                a105_01_xml = a105_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a105_01_generat, a105_01_xml)

    def test_export_a105_23(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '05', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.05.a1')
            province_obj = self.openerp.pool.get("res.country.state")
            province_owner_id = province_obj.search(
                cursor, uid, [('code', '=', '19')]
            )[0]

            city_obj = self.openerp.pool.get("res.municipi")
            city_owner_id = city_obj.search(
                cursor, uid, [('ine', '=', '49221')]
            )[0]

            a105 = step_obj.browse(cursor, uid, step_id)
            a105.write({
                'updatereason': '23',
                'surrogacy': 'S',
                'newtolltype': '31',
                'newreqqd': 4321.123,
                'newnationality': 'AF',
                'newdocumenttype': '03',
                'newdocumentnum': '00000000T',
                'newfirstname': 'Gas',
                'newfamilyname1': 'Al',
                'newfamilyname2': 'Matalas',
                'newtitulartype': 'F',
                'newregularaddress': 'S',
                'newtelephone': '999888777',
                'newfax': '111222333',
                'newemail': 'gasalmatalas@atr',
                'newprovinceowner': province_owner_id,
                'newcityowner': city_owner_id,
                'newzipcodeowner': '17002',
                'newstreettypeowner': 'ACCE',
                'newstreetowner': 'Carrer inventat',
                'newstreetnumberowner': '1',
                'newportalowner': '2',
                'newstaircaseowner': '3',
                'newfloorowner': '4',
                'newdoorowner': '5',
                'extrainfo': 'Comentari',
            })
            a105.header_id.write({'date_created': a105.header_id.date_created})
            a105 = step_obj.browse(cursor, uid, step_id)
            self.assertEqual(
                a105.sw_id.additional_info,
                u'(23) Tarifa: 3.1 -> 3.1. Caudal Diario: 1.5 -> 4321.123. Titular: ESB82420654 -> 00000000T. '
            )

            a105_01_generat = a105.generar_xml()[1]
            a105_01_generat = a105_01_generat.replace(' ', '').replace('\n', '')

            a105_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '05_a1_23.xml'
            )
            with open(a105_01_xml_path, 'r') as f:
                a105_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a105.sw_id.codi_sollicitud
                data_emissio = a105.header_id.date_created.split(" ")[0]
                hora_emissio = a105.header_id.date_created.split(" ")[1]
                a105_01_xml = a105_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a105_01_xml = a105_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a105_01_generat, a105_01_xml)

    def test_import_a2_05(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '05_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.05.a1')
            # header_obj = self.pool.get('giscegas.atr.step.header')
            # rebuig_obj = self.pool.get('giscegas.atr.rebuig')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '05', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '05_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '05'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.updatereason, '11')
            self.assertEqual(a2.finaltolltypegranted, '31')
            self.assertEqual(a2.qdgranted, 984321.12367)
            self.assertEqual(a2.newmodeffectdate, '04')
            self.assertEqual(a2.foreseentransferdate, '2018-06-01')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertFalse(a2.rebuig)

    def test_reject_import_a2_05(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '05_a2_rej.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.05.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '05', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '05_a1_02.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '05'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertTrue(cas_atr.rebuig)
            self.assertTrue(a2.rebuig)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '05')
            self.assertEqual(a2.resultdesc, 'Denegada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.updatereason, '11')
            self.assertEqual(a2.finaltolltypegranted, '31')
            self.assertEqual(a2.qdgranted, 987321.1567)
            self.assertEqual(a2.newmodeffectdate, '04')
            self.assertEqual(a2.foreseentransferdate, '2018-06-01')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(a2.rebuig_ids[0].motiu_rebuig.name, 'R32')
            self.assertEqual(a2.rebuig_ids[0].desc_rebuig,
                             'Fecha efecto solicitada anterior al dia actual.')
            self.assertTrue(a2.rebuig)
            self.assertTrue(
                cas_atr.additional_info,
                u'(11) Tarifa: 3.1 -> 3.1. Caudal Diario: 1.5. Rechazo: Fecha efecto solicitada anterior al dia actual.'
            )

    def test_import_a3_05(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '05_a2.xml'
        )
        a3_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '05_a3.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.05.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '05', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '05_a2.xml')

            a3_xml = a3_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a3_xml, '05_a3.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '05'),
                ('step_id.name', '=', 'a3'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3 = sw_obj.get_pas(cursor, uid, cas_atr)

            corrector_ids = a3.corrector_ids
            counter_ids = a3.counter_ids

            counter_test = counter_ids[0]
            self.assertEqual(counter_test.countermodel, 'marca1')
            self.assertEqual(counter_test.countertype, 'tipo1')
            self.assertEqual(counter_test.counternumber, 'B123456')
            self.assertEqual(counter_test.counterproperty, '04')
            self.assertEqual(counter_test.reallecture, 2000)
            self.assertEqual(counter_test.counterpressure, 54321.123)

            counter_test = counter_ids[1]
            self.assertEqual(counter_test.countermodel, 'marca2')
            self.assertEqual(counter_test.countertype, 'tipo2')
            self.assertEqual(counter_test.counternumber, 'C123456')
            self.assertEqual(counter_test.counterproperty, '06')
            self.assertEqual(counter_test.reallecture, 3000)
            self.assertEqual(counter_test.counterpressure, 13245.321)

            corrector_test = corrector_ids[0]
            self.assertEqual(corrector_test.correctormodel, 'modelo1')
            self.assertEqual(corrector_test.correctortype,  '01')
            self.assertEqual(corrector_test.correctornumber,  'D123456')
            self.assertEqual(corrector_test.correctorproperty,  '01')
            self.assertEqual(corrector_test.correctedlecture,  2200)

            corrector_test = corrector_ids[1]
            self.assertEqual(corrector_test.correctormodel, 'modelo2')
            self.assertEqual(corrector_test.correctortype,  '02')
            self.assertEqual(corrector_test.correctornumber,  'E654321')
            self.assertEqual(corrector_test.correctorproperty,  '02')
            self.assertEqual(corrector_test.correctedlecture,  3300)

            self.assertEqual(a3.result, '09')
            self.assertEqual(a3.resultdesc, 'Realizada')
            self.assertEqual(a3.reqcode, '0123456789')
            self.assertEqual(a3.atrcode, '000111222333444555666777')
            self.assertEqual(a3.transfereffectivedate, '2018-07-01')
            self.assertEqual(a3.updatereason, '11')
            self.assertEqual(a3.tolltype, '32')
            self.assertEqual(a3.nationality, 'ES')
            self.assertEqual(a3.documenttype, '01')
            self.assertEqual(a3.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a3.firstname, 'Gas')
            self.assertEqual(a3.familyname1, 'Al')
            self.assertEqual(a3.familyname2, 'Matalas')
            self.assertEqual(a3.telephone, '999888777')
            self.assertEqual(a3.fax, '666555444')
            self.assertEqual(a3.email, 'gasalmatalas@atr.cat')
            self.assertEqual(a3.caecode.name, '9820')
            self.assertEqual(a3.qdgranted, 984321.12345)
            self.assertEqual(a3.provinceowner.code, u'19')
            self.assertEqual(a3.cityowner.ine, u'49221')
            self.assertEqual(a3.zipcodeowner, '17002')
            self.assertEqual(a3.streettypeowner, 'ACCE')
            self.assertEqual(a3.streetowner, 'Carrer inventat')
            self.assertEqual(a3.streetnumberowner, '1')
            self.assertEqual(a3.portalowner, '2')
            self.assertEqual(a3.staircaseowner, '3')
            self.assertEqual(a3.floorowner, '4')
            self.assertEqual(a3.doorowner, 'v')
            self.assertEqual(a3.titulartype, 'F')
            self.assertEqual(a3.regularaddress, 'S')
            self.assertEqual(a3.extrainfo, 'comentarios extras')

    def test_import_a4_05(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '05_a2.xml'
        )
        a4_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '05_a4.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a4_xml_path, 'r') as f:
            a4_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.05.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '05', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '05_a2.xml')

            a4_xml = a4_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a4_xml = a4_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a4_xml = a4_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a4_xml = a4_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a4_xml = a4_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a4_xml, '05_a4.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '05'),
                ('step_id.name', '=', 'a4'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a4 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertEqual(a4.reqcode, '0123456789')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a4.updatereason, '11')
            self.assertEqual(a4.result, '13')
            self.assertEqual(a4.resultdesc, 'No Realizada')
            self.assertEqual(a4.rebuig_ids[0].motiu_rebuig.name, 'R32')
            self.assertEqual(a4.rebuig_ids[0].desc_rebuig,
                             'Fecha efecto solicitada anterior al dia actual.')
            self.assertEqual(a4.extrainfo, 'comentarios extras')
            self.assertTrue(a4.rebuig)
            self.assertTrue(
                cas_atr.additional_info,
                u'(11) Tarifa: 3.1 -> 3.1. Caudal Diario: 1.5. Rechazo: Fecha efecto solicitada anterior al dia actual.'
            )
