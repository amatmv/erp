# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime
import calendar
from destral.patch import PatchNewCursors


class Test02(TestSwitchingImport):

    def test_creation_a102(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            a102 = step_obj.browse(cursor, uid, step_id)
            a1_02 = sw_obj.browse(cursor, uid, a102.sw_id.id)

            participant_obj = self.openerp.pool.get('giscemisc.participant')
            participant_partner = participant_obj.get_from_partner(cursor, uid, a1_02.partner_id.id)
            self.assertEqual(a1_02.proces_id.name, '02')
            self.assertEqual(a1_02.step_id.name, 'a1')
            self.assertEqual(participant_partner.codi_sifco, '1234')
            self.assertEqual(a1_02.company_id.codi_sifco, '4321')
            self.assertEqual(a1_02.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(a102.reqtransferdate, datetime.today()
                             .strftime("%Y-%m-%d"))
            self.assertEqual(a102.modeffectdate, '05')
            self.assertEqual(a102.disconnectedserviceaccepted, 'N')
            self.assertEqual(a102.nationality, 'CH')
            self.assertEqual(a102.documenttype, '01')
            self.assertEqual(a102.documentnum, 'B82420654')
            self.assertEqual(a102.reqqd, 1.50)
            self.assertEqual(
                a1_02.additional_info,
                u'Activación: Cuanto antes. Fecha prevista: {}. Caudal Anual: '
                u'547. Tarifa:3.1'.format(datetime.today().strftime("%Y-%m-%d"))
            )

    def test_export_a102(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')
            register_doc_obj = self.openerp.pool.get('giscegas.atr.document')

            a102 = step_obj.browse(cursor, uid, step_id)
            a102.write({
                'modeffectdate': '03',
                'disconnectedserviceaccepted': 'N',
                'extrainfo': 'comentarios extras',
                'nationality': 'ES',
                'documenttype': '07',
                'documentnum': '11111111H',
                'reqqd': 54321.12345,
                'reqestimatedqa': 12345678,
                'titulartype': 'F',
                'reqtransferdate': '2018-06-01',
            })

            a102_01_generat = a102.generar_xml()[1]
            a102_01_generat = a102_01_generat.replace(' ', '').replace('\n', '')

            a102_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '02_a1.xml'
            )
            with open(a102_01_xml_path, 'r') as f:
                a102_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a102.sw_id.codi_sollicitud
                data_emissio = a102.header_id.date_created.split(" ")[0]
                hora_emissio = a102.header_id.date_created.split(" ")[1]
                a102_01_xml = a102_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a102_01_xml = a102_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a102_01_xml = a102_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a102_01_xml = a102_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a102_01_xml = a102_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a102_01_generat, a102_01_xml)

    def test_export_a102_no_reqqd(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')
            a102 = step_obj.browse(cursor, uid, step_id)
            a102.write({
                'modeffectdate': '03',
                'disconnectedserviceaccepted': 'N',
                'extrainfo': 'comentarios extras',
                'nationality': 'ES',
                'documenttype': '07',
                'documentnum': '11111111H',
                'reqqd': 0,
                'reqestimatedqa': 12345678,
                'titulartype': 'F',
                'reqtransferdate': '2018-06-01',
            })

            a102_01_generat = a102.generar_xml()[1]
            a102_01_generat = a102_01_generat.replace(' ', '').replace('\n', '')

            a102_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '02_a1_no_reqqd.xml'
            )
            with open(a102_01_xml_path, 'r') as f:
                a102_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a102.sw_id.codi_sollicitud
                data_emissio = a102.header_id.date_created.split(" ")[0]
                hora_emissio = a102.header_id.date_created.split(" ")[1]
                a102_01_xml = a102_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a102_01_xml = a102_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a102_01_xml = a102_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a102_01_xml = a102_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a102_01_xml = a102_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a102_01_generat, a102_01_xml)

    def test_import_a2_02(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')
            # header_obj = self.pool.get('giscegas.atr.step.header')
            # rebuig_obj = self.pool.get('giscegas.atr.rebuig')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            a2 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a2.sw_id.codi_sollicitud
            data_emissio = a2.date_created.split(" ")[0]
            hora_emissio = a2.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '02_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '02'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.tolltype, '31')
            self.assertEqual(a2.qdgranted, 0.5)
            self.assertEqual(a2.singlenomination, 'S')
            self.assertEqual(a2.netsituation, 'red municipio')
            self.assertEqual(a2.newmodeffectdate, '04')
            self.assertEqual(a2.foreseentransferdate, '2018-06-01')
            self.assertEqual(a2.StatusPS, '1')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(
                cas_atr.additional_info,
                u'Activación: Cuanto antes. Fecha prevista: {}. '
                u'Caudal Anual: 547. Tarifa:3.1.'.format(datetime.today().strftime("%Y-%m-%d"))
            )

    def test_reject_import_a2_02(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a2_rej.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            a2 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a2.sw_id.codi_sollicitud
            data_emissio = a2.date_created.split(" ")[0]
            hora_emissio = a2.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '02_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '02'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertTrue(cas_atr.rebuig)
            self.assertTrue(a2.rebuig)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '05')
            self.assertEqual(a2.resultdesc, 'Denegada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.foreseentransferdate, '2018-06-01')
            self.assertEqual(a2.StatusPS, '1')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(a2.rebuig_ids[0].motiu_rebuig.name, 'R32')
            self.assertEqual(a2.rebuig_ids[0].desc_rebuig,
                             'Fecha efecto solicitada anterior al dia actual.')
            self.assertEqual(
                cas_atr.additional_info,
                u'Activación: Cuanto antes. Fecha prevista: {}. Caudal Anual: '
                u'547. Tarifa:3.1. Rechazo: Fecha efecto solicitada anterior al dia actual.'.format(datetime.today().strftime("%Y-%m-%d"))
            )

    def test_import_a3_02(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a2.xml'
        )
        a3_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a3.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '02_a2.xml')

            a3_xml = a3_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a3_xml, '02_a3.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '02'),
                ('step_id.name', '=', 'a3'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3 = sw_obj.get_pas(cursor, uid, cas_atr)

            corrector_ids = a3.corrector_ids
            counter_ids = a3.counter_ids

            counter_test = counter_ids[0]
            self.assertEqual(counter_test.countermodel, 'marca1')
            self.assertEqual(counter_test.countertype, 'tipo1')
            self.assertEqual(counter_test.counternumber, 'B123456')
            self.assertEqual(counter_test.counterproperty, '04')
            self.assertEqual(counter_test.reallecture, 2000)
            self.assertEqual(counter_test.counterpressure, 54321.123)

            counter_test = counter_ids[1]
            self.assertEqual(counter_test.countermodel, 'marca2')
            self.assertEqual(counter_test.countertype, 'tipo2')
            self.assertEqual(counter_test.counternumber, 'C123456')
            self.assertEqual(counter_test.counterproperty, '06')
            self.assertEqual(counter_test.reallecture, 3000)
            self.assertEqual(counter_test.counterpressure, 13245.321)

            corrector_test = corrector_ids[0]
            self.assertEqual(corrector_test.correctormodel, 'modelo1')
            self.assertEqual(corrector_test.correctortype, '01')
            self.assertEqual(corrector_test.correctornumber, 'D123456')
            self.assertEqual(corrector_test.correctorproperty, '01')
            self.assertEqual(corrector_test.correctedlecture, 2200)

            corrector_test = corrector_ids[1]
            self.assertEqual(corrector_test.correctormodel, 'modelo2')
            self.assertEqual(corrector_test.correctortype, '02')
            self.assertEqual(corrector_test.correctornumber, 'E654321')
            self.assertEqual(corrector_test.correctorproperty, '02')
            self.assertEqual(corrector_test.correctedlecture, 3300)

            self.assertEqual(a3.reqcode, '0123456789')
            self.assertEqual(a3.nationality, 'ES')
            self.assertEqual(a3.documenttype, '07')
            self.assertEqual(a3.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a3.atrcode, '000111222333444555666777')
            self.assertEqual(a3.transfereffectivedate, '2018-07-01')
            self.assertEqual(a3.telemetering, 'S')
            self.assertEqual(a3.finalclientyearlyconsumption, 5000)
            self.assertEqual(a3.gasusetype, '03')
            self.assertEqual(a3.canonircperiodicity, '01')
            self.assertEqual(a3.lastinspectionsdate, '2017-01-01')
            self.assertEqual(a3.lastinspectionsresult, '01')
            self.assertEqual(a3.StatusPS, '1')
            self.assertEqual(a3.readingtype, '1')
            self.assertEqual(a3.lectureperiodicity, '02')
            self.assertEqual(a3.extrainfo, 'comentarios extras')
            self.assertEqual(a3.caecode.name, '9820')

    def test_import_a3s_02(self):

        a3s_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a3s.xml'
        )

        with open(a3s_xml_path, 'r') as f:
            a3s_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')

            sw_obj.importar_xml(cursor, uid, a3s_xml, '02_a3s.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '02'),
                ('step_id.name', '=', 'a3s'),
                ('codi_sollicitud', '=', '0123456789')
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3s = sw_obj.get_pas(cursor, uid, cas_atr)

            corrector_ids = a3s.corrector_ids
            counter_ids = a3s.counter_ids

            counter_test = counter_ids[0]
            self.assertEqual(counter_test.countermodel, 'marca1')
            self.assertEqual(counter_test.countertype, 'tipo1')
            self.assertEqual(counter_test.counternumber, 'B123456')
            self.assertEqual(counter_test.counterproperty, '04')
            self.assertEqual(counter_test.reallecture, 2000)
            self.assertEqual(counter_test.counterpressure, 54321.123)

            counter_test = counter_ids[1]
            self.assertEqual(counter_test.countermodel, 'marca2')
            self.assertEqual(counter_test.countertype, 'tipo2')
            self.assertEqual(counter_test.counternumber, 'C123456')
            self.assertEqual(counter_test.counterproperty, '06')
            self.assertEqual(counter_test.reallecture, 3000)
            self.assertEqual(counter_test.counterpressure, 13245.321)

            corrector_test = corrector_ids[0]
            self.assertEqual(corrector_test.correctormodel, 'modelo1')
            self.assertEqual(corrector_test.correctortype, '01')
            self.assertEqual(corrector_test.correctornumber, 'D123456')
            self.assertEqual(corrector_test.correctorproperty, '01')
            self.assertEqual(corrector_test.correctedlecture, 2200)

            corrector_test = corrector_ids[1]
            self.assertEqual(corrector_test.correctormodel, 'modelo2')
            self.assertEqual(corrector_test.correctortype, '02')
            self.assertEqual(corrector_test.correctornumber, 'E654321')
            self.assertEqual(corrector_test.correctorproperty, '02')
            self.assertEqual(corrector_test.correctedlecture, 3300)

            self.assertEqual(a3s.reqcode, '0123456789')
            self.assertEqual(a3s.nationality, 'ES')
            self.assertEqual(a3s.documenttype, '07')
            self.assertEqual(a3s.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a3s.previousatrcode, '000111222333444555666777')
            self.assertEqual(a3s.transfereffectivedate, '2018-07-01')
            self.assertEqual(a3s.readingtype, '1')
            self.assertEqual(a3s.extrainfo, 'comentarios extras')
            self.assertEqual(
                cas_atr.additional_info, u'Fecha Activación: 2018-07-01'
            )

    def test_import_a4_02(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a2.xml'
        )
        a4_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a4.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a4_xml_path, 'r') as f:
            a4_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '02_a2.xml')

            a4_xml = a4_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a4_xml = a4_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a4_xml = a4_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a4_xml = a4_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a4_xml = a4_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a4_xml, '02_a4.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '02'),
                ('step_id.name', '=', 'a4'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a4 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertEqual(a4.reqcode, '0123456789')
            self.assertEqual(a4.titulartype, 'F')
            self.assertEqual(a4.nationality, 'ES')
            self.assertEqual(a4.documenttype, '07')
            self.assertEqual(a4.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a4.result, '13')
            self.assertEqual(a4.resultdesc, 'Resultado Descripcion')
            self.assertEqual(a4.rebuig_ids[0].motiu_rebuig.name, 'R32')
            self.assertEqual(a4.rebuig_ids[0].desc_rebuig,
                             'Result reason description')
            self.assertEqual(a4.extrainfo, 'comentarios extras')
            self.assertEqual(
                cas_atr.additional_info,
                u'Activación: Cuanto antes. Fecha prevista: {}. Caudal Anual: '
                u'547. Tarifa:3.1. Rechazo: Result reason description'.format(datetime.today().strftime("%Y-%m-%d"))
            )

    def test_activate_a3s02_cancel_contract_correctly(self):
        a3s_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a3s.xml'
        )

        with open(a3s_xml_path, 'r') as f:
            a3s_xml = f.read()

        sw_obj = self.openerp.pool.get('giscegas.atr')
        pol_obj = self.openerp.pool.get('giscegas.polissa')

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(cursor, uid, a3s_xml, '02_a3s.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '02'),
                ('step_id.name', '=', 'a3s'),
                ('codi_sollicitud', '=', '0123456789')
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3s = sw_obj.get_pas(cursor, uid, cas_atr)

            pol_state = pol_obj.read(
                cursor, uid, contract_id, ['state']
            )['state']

            self.assertEqual(pol_state, 'activa')
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, cas_atr)

            pol_state = pol_obj.read(
                cursor, uid, contract_id, ['state']
            )['state']

            self.assertEqual(pol_state, 'baixa')