# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime
from destral.patch import PatchNewCursors


class Test41(TestSwitchingImport):

    def test_creation_a141(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            a141 = step_obj.browse(cursor, uid, step_id)
            a1_41 = sw_obj.browse(cursor, uid, a141.sw_id.id)

            participant_obj = self.openerp.pool.get('giscemisc.participant')
            participant_partner = participant_obj.get_from_partner(cursor, uid, a1_41.partner_id.id)
            self.assertEqual(a1_41.proces_id.name, '41')
            self.assertEqual(a1_41.step_id.name, 'a1')
            self.assertEqual(participant_partner.codi_sifco, '1234')
            self.assertEqual(a1_41.company_id.codi_sifco, '4321')
            self.assertEqual(a1_41.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(a141.reqtransferdate, datetime.today()
                             .strftime("%Y-%m-%d"))
            self.assertEqual(a141.modeffectdate, '05')
            self.assertEqual(a141.updatereason, '02')
            self.assertEqual(a141.disconnectedserviceaccepted, 'N')
            self.assertEqual(a141.nationality, 'CH')
            self.assertEqual(a141.documenttype, '01')
            self.assertEqual(a141.documentnum, 'B82420654')
            self.assertEqual(a141.newreqqd, 1.50)
            self.assertEqual(len(a141.document_ids), 0)

    def test_export_a141_01(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')
            register_doc_obj = self.openerp.pool.get('giscegas.atr.document')

            docs_vals_1 = {
                'doctype': '03',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-02',
                'extrainfo': '404pagenotfound',
            }
            created_doc_1 = register_doc_obj.create(cursor, uid, docs_vals_1)
            docs_vals_2 = {
                'doctype': '01',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-03',
                'extrainfo': '404pagenotfound',
            }
            created_doc_2 = register_doc_obj.create(cursor, uid, docs_vals_2)

            province_obj = self.openerp.pool.get("res.country.state")
            province_owner_id = province_obj.search(
                cursor, uid, [('code', '=', '19')]
            )[0]

            city_obj = self.openerp.pool.get("res.municipi")
            city_owner_id = city_obj.search(
                cursor, uid, [('ine', '=', '49221')]
            )[0]

            a141 = step_obj.browse(cursor, uid, step_id)
            a141.write({
                'updatereason': '01',
                'modeffectdate': '05',
                'disconnectedserviceaccepted': 'N',
                'extrainfo': 'Aqui hi van els comentaris.',
                'nationality': '',
                'documenttype': '',
                'documentnum': '',
                'newreqqd': '',
                'surrogacy': 'S',
                'newnationality': 'ES',
                'newdocumenttype': '07',
                'newdocumentnum': '00000000A',
                'newfirstname': 'MIGUEL',
                'newfamilyname1': 'CERVANTES',
                'newfamilyname2': 'SAAVEDRA',
                'newtitulartype': 'F',
                'newregularaddress': 'S',
                'newtelephone1': '999999999',
                'newtelephone2': '666666666',
                'newemail': 'test@gisce.net',
                'newlanguage': '01',
                'newprovinceowner': province_owner_id,
                'newcityowner': city_owner_id,
                'newzipcodeowner': '17002',
                'newstreettypeowner': 'PL',
                'newstreetowner': 'Mela Mutermilch',
                'newstreetnumberowner': '2',
                'newportal': '1',
                'newstaircase': '3',
                'newfloor': '4',
                'newdoor': '5',
                'document_ids': [(6, 0, [created_doc_1, created_doc_2])],
            })
            a141.header_id.write({'date_created': a141.header_id.date_created})
            a141 = step_obj.browse(cursor, uid, step_id)
            self.assertEqual(
                a141.sw_id.additional_info,
                u'(01) Tarifa:3.1. Caudal Diario: 1.5. Titular: ESB82420654 -> 00000000A. '
            )

            a141_01_generat = a141.generar_xml()[1]
            a141_01_generat = a141_01_generat.replace(' ', '').replace('\n', '')

            a141_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '41_a1_01.xml'
            )
            with open(a141_01_xml_path, 'r') as f:
                a141_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a141.sw_id.codi_sollicitud
                data_emissio = a141.header_id.date_created.split(" ")[0]
                hora_emissio = a141.header_id.date_created.split(" ")[1]
                a141_01_xml = a141_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a141_01_xml = a141_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a141_01_xml = a141_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a141_01_xml = a141_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a141_01_xml = a141_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a141_01_generat, a141_01_xml)

    def test_export_a141_02(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')
            register_doc_obj = self.openerp.pool.get('giscegas.atr.document')

            docs_vals_1 = {
                'doctype': '03',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-02',
                'extrainfo': '404pagenotfound',
            }
            created_doc_1 = register_doc_obj.create(cursor, uid, docs_vals_1)
            docs_vals_2 = {
                'doctype': '01',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-03',
                'extrainfo': '404pagenotfound',
            }
            created_doc_2 = register_doc_obj.create(cursor, uid, docs_vals_2)

            a141 = step_obj.browse(cursor, uid, step_id)
            a141.write({
                'updatereason': '02',
                'modeffectdate': '03',
                'disconnectedserviceaccepted': 'N',
                'extrainfo': 'Aqui van els comentaris.',
                'nationality': 'ES',
                'documenttype': '01',
                'documentnum': '11111111H',
                'newreqqd': 4321.1234,
                'surrogacy': '',
                'newnationality': '',
                'newdocumenttype': '',
                'newdocumentnum': '11111111H',
                'newfirstname': '',
                'newfamilyname1': '',
                'newfamilyname2': '',
                'newtitulartype': '',
                'newregularaddress': '',
                'newtelephone1': '',
                'newtelephone2': '',
                'newemail': '',
                'newlanguage': '',
                'newzipcodeowner': '',
                'newstreettypeowner': '',
                'newstreetowner': '',
                'newstreetnumberowner': '',
                'newportal': '',
                'newstaircase': '',
                'newfloor': '',
                'newdoor': '',
                'document_ids': [(6, 0, [created_doc_1, created_doc_2])],
            })
            a141.header_id.write({'date_created': a141.header_id.date_created})
            a141 = step_obj.browse(cursor, uid, step_id)
            self.assertEqual(
                a141.sw_id.additional_info,
                u'(02) Tarifa:3.1. Caudal Diario: 1.5 -> 4321.1234. '
            )

            a141_02_generat = a141.generar_xml()[1]
            a141_02_generat = a141_02_generat.replace(' ', '').replace('\n', '')

            a141_02_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '41_a1_02.xml'
            )
            with open(a141_02_xml_path, 'r') as f:
                a141_02_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a141.sw_id.codi_sollicitud
                data_emissio = a141.header_id.date_created.split(" ")[0]
                hora_emissio = a141.header_id.date_created.split(" ")[1]
                reqtransferdate = a141.reqtransferdate.split(" ")[0]
                a141_02_xml = a141_02_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a141_02_xml = a141_02_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a141_02_xml = a141_02_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a141_02_xml = a141_02_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a141_02_xml = a141_02_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )
                a141_02_xml = a141_02_xml.replace(
                    "<reqtransferdate>2018-06-01</reqtransferdate>",
                    "<reqtransferdate>{0}</reqtransferdate>"
                    .format(reqtransferdate)
                )

            self.assertEqual(a141_02_generat, a141_02_xml)

    def test_export_a141_03(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')
            register_doc_obj = self.openerp.pool.get('giscegas.atr.document')

            docs_vals_1 = {
                'doctype': '03',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-02',
                'extrainfo': '404pagenotfound',
            }
            created_doc_1 = register_doc_obj.create(cursor, uid, docs_vals_1)
            docs_vals_2 = {
                'doctype': '01',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-03',
                'extrainfo': '404pagenotfound',
            }
            created_doc_2 = register_doc_obj.create(cursor, uid, docs_vals_2)

            province_obj = self.openerp.pool.get("res.country.state")
            province_owner_id = province_obj.search(
                cursor, uid, [('code', '=', '19')]
            )[0]

            city_obj = self.openerp.pool.get("res.municipi")
            city_owner_id = city_obj.search(
                cursor, uid, [('ine', '=', '49221')]
            )[0]

            a141 = step_obj.browse(cursor, uid, step_id)
            a141.write({
                'updatereason': '03',
                'modeffectdate': '04',
                'disconnectedserviceaccepted': 'N',
                'extrainfo': 'Aixo es un comentari extra normal i corrent',
                'newreqqd': '54321.12345',
                'surrogacy': 'S',
                'newnationality': 'AF',
                'newdocumenttype': '07',
                'newdocumentnum': '00000000A',
                'newfirstname': ' JOHANOT',
                'newfamilyname1': 'MARTORELL',
                'newfamilyname2': 'TIRANT LO BLANC',
                'newtitulartype': 'F',
                'newregularaddress': 'S',
                'newtelephone1': '999888777',
                'newtelephone2': '666555444',
                'newemail': 'gasalmatalas@atr',
                'newlanguage': '02',
                'newprovinceowner': province_owner_id,
                'newcityowner': city_owner_id,
                'newzipcodeowner': '17002',
                'newstreettypeowner': 'PL',
                'newstreetowner': 'Mela Mutermilch',
                'newstreetnumberowner': '2',
                'newportal': '1',
                'newstaircase': '3',
                'newfloor': '4',
                'newdoor': '5',
                'document_ids': [(6, 0, [created_doc_1, created_doc_2])],
            })
            a141.header_id.write({'date_created': a141.header_id.date_created})
            a141 = step_obj.browse(cursor, uid, step_id)
            self.assertEqual(
                a141.sw_id.additional_info,
                u'(03) Tarifa:3.1. Caudal Diario: 1.5 -> 54321.12345. Titular: ESB82420654 -> 00000000A. '
            )

            a141_03_generat = a141.generar_xml()[1]
            a141_03_generat = a141_03_generat.replace(' ', '').replace('\n', '')

            a141_03_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '41_a1_03.xml'
            )
            with open(a141_03_xml_path, 'r') as f:
                a141_03_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a141.sw_id.codi_sollicitud
                data_emissio = a141.header_id.date_created.split(" ")[0]
                hora_emissio = a141.header_id.date_created.split(" ")[1]
                reqtransferdate = a141.reqtransferdate.split(" ")[0]

                a141_03_xml = a141_03_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a141_03_xml = a141_03_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a141_03_xml = a141_03_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a141_03_xml = a141_03_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a141_03_xml = a141_03_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )
                a141_03_xml = a141_03_xml.replace(
                    "<reqtransferdate>2018-06-01</reqtransferdate>",
                    "<reqtransferdate>{0}</reqtransferdate>"
                    .format(reqtransferdate)
                )

            self.assertEqual(a141_03_generat, a141_03_xml)

    def test_import_a2_41(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '41_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')
            # header_obj = self.pool.get('giscegas.atr.step.header')
            # rebuig_obj = self.pool.get('giscegas.atr.rebuig')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '41_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '41'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.updatereason, '01')
            self.assertEqual(a2.reqqd, 54321.12345)
            self.assertEqual(a2.reqqh, 54321)
            self.assertEqual(a2.reqestimatedqa, 6332211)
            self.assertEqual(a2.reqoutgoingpressure, 5321.123)
            self.assertEqual(a2.tolltype, '31')
            self.assertEqual(a2.qdgranted, 984321.12367)
            self.assertEqual(a2.singlenomination, 'S')
            self.assertEqual(a2.netsituation, 'red municipio')
            self.assertEqual(a2.newmodeffectdate, '04')
            self.assertEqual(a2.foreseentransferdate, '2018-06-01')
            self.assertEqual(a2.StatusPS, '1')
            self.assertEqual(a2.extrainfo, 'comentarios extras')

    def test_reject_import_a2_41(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '41_a2_rej.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '41_a1_02.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '41'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertTrue(cas_atr.rebuig)
            self.assertTrue(a2.rebuig)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '05')
            self.assertEqual(a2.resultdesc, 'Denegada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.updatereason, '01')
            self.assertEqual(a2.reqqd, 94321.123467)
            self.assertEqual(a2.reqqh, 984321)
            self.assertEqual(a2.reqestimatedqa, 6653211)
            self.assertEqual(a2.reqoutgoingpressure, 5321.123)
            self.assertEqual(a2.tolltype, '31')
            self.assertEqual(a2.qdgranted, 987321.1567)
            self.assertEqual(a2.singlenomination, 'S')
            self.assertEqual(a2.netsituation, 'red municipio')
            self.assertEqual(a2.newmodeffectdate, '04')
            self.assertEqual(a2.foreseentransferdate, '2018-06-01')
            self.assertEqual(a2.StatusPS, '1')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(a2.rebuig_ids[0].motiu_rebuig.name, 'R32')
            self.assertEqual(a2.rebuig_ids[0].desc_rebuig,
                             'Fecha efecto solicitada anterior al dia actual.')
            self.assertEqual(
                cas_atr.additional_info,
                u'(02) Tarifa:3.1. Caudal Diario: 1.5 -> 1.5. Rechazo: Fecha efecto solicitada anterior al dia actual.'
            )

    def test_import_a3_41(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '41_a2.xml'
        )
        a3_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '41_a3.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '41_a2.xml')

            a3_xml = a3_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a3_xml, '41_a3.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '41'),
                ('step_id.name', '=', 'a3'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3 = sw_obj.get_pas(cursor, uid, cas_atr)

            corrector_ids = a3.corrector_ids
            counter_ids = a3.counter_ids

            counter_test = counter_ids[0]
            self.assertEqual(counter_test.countermodel, 'marca1')
            self.assertEqual(counter_test.countertype, 'tipo1')
            self.assertEqual(counter_test.counternumber, 'B123456')
            self.assertEqual(counter_test.counterproperty, '04')
            self.assertEqual(counter_test.reallecture, 2000)
            self.assertEqual(counter_test.counterpressure, 54321.123)

            counter_test = counter_ids[1]
            self.assertEqual(counter_test.countermodel, 'marca2')
            self.assertEqual(counter_test.countertype, 'tipo2')
            self.assertEqual(counter_test.counternumber, 'C123456')
            self.assertEqual(counter_test.counterproperty, '06')
            self.assertEqual(counter_test.reallecture, 3000)
            self.assertEqual(counter_test.counterpressure, 13245.321)

            corrector_test = corrector_ids[0]
            self.assertEqual(corrector_test.correctormodel, 'modelo1')
            self.assertEqual(corrector_test.correctortype,  '01')
            self.assertEqual(corrector_test.correctornumber,  'D123456')
            self.assertEqual(corrector_test.correctorproperty,  '01')
            self.assertEqual(corrector_test.correctedlecture,  2200)

            corrector_test = corrector_ids[1]
            self.assertEqual(corrector_test.correctormodel, 'modelo2')
            self.assertEqual(corrector_test.correctortype,  '02')
            self.assertEqual(corrector_test.correctornumber,  'E654321')
            self.assertEqual(corrector_test.correctorproperty,  '02')
            self.assertEqual(corrector_test.correctedlecture,  3300)

            self.assertEqual(a3.reqcode, '0123456789')
            self.assertEqual(a3.nationality, 'ES')
            self.assertEqual(a3.documenttype, '01')
            self.assertEqual(a3.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a3.atrcode, '000111222333444555666777')
            self.assertEqual(a3.transfereffectivedate, '2018-07-01')
            self.assertEqual(a3.telemetering, 'S')
            self.assertEqual(a3.finalqdgranted, 984321.12345)
            self.assertEqual(a3.finalqhgranted, 984321)
            self.assertEqual(a3.finalclientyearlyconsumption, 65432211)
            self.assertEqual(a3.gasusetype, '01')
            self.assertEqual(a3.updatereason, '01')
            self.assertEqual(a3.result, '09')
            self.assertEqual(a3.resultdesc, 'Realizada')
            self.assertEqual(a3.activationtype, '001')
            self.assertEqual(a3.activationtypedesc,
                             'Realizada puesta en servicio')
            self.assertEqual(a3.closingtype, '001')
            self.assertEqual(
                a3.closingtypedesc,
                u'La Actualización de Datos se ha realizado correctamente')
            self.assertEqual(a3.interventionhourfrom, '2016-01-01 09:00:00')
            self.assertEqual(a3.interventionhourto, '2016-01-01 12:00:00')
            self.assertEqual(a3.visitnumber, 987)
            self.assertEqual(a3.firstname, 'Gas')
            self.assertEqual(a3.familyname1, 'Al')
            self.assertEqual(a3.familyname2, 'Matalas')
            self.assertEqual(a3.titulartype, 'F')
            self.assertEqual(a3.regularaddress, 'S')
            self.assertEqual(a3.telephone1, '999888777')
            self.assertEqual(a3.telephone2, '666555444')
            self.assertEqual(a3.email, 'gasalmatalas@atr.cat')
            self.assertEqual(a3.language, '01')
            self.assertEqual(a3.provinceowner.code, u'19')
            self.assertEqual(a3.cityowner.ine, u'49221')
            self.assertEqual(a3.zipcodeowner, '17002')
            self.assertEqual(a3.streettypeowner, 'ACCE')
            self.assertEqual(a3.streetowner, 'Carrer inventat')
            self.assertEqual(a3.streetnumberowner, '1')
            self.assertEqual(a3.portalowner, '2')
            self.assertEqual(a3.staircaseowner, '3')
            self.assertEqual(a3.floorowner, '4')
            self.assertEqual(a3.doorowner, 'v')
            self.assertEqual(a3.canonircperiodicity, '01')
            self.assertEqual(a3.lastinspectionsdate, '2017-01-01')
            self.assertEqual(a3.lastinspectionsresult, '01')
            self.assertEqual(a3.StatusPS, '1')
            self.assertEqual(a3.readingtype, '1')
            self.assertEqual(a3.lectureperiodicity, '02')
            self.assertEqual(a3.extrainfo, 'comentarios extras')

    def test_import_a3s_41(self):

        a3s_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '41_a3s.xml'
        )

        with open(a3s_xml_path, 'r') as f:
            a3s_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')

            sw_obj.importar_xml(cursor, uid, a3s_xml, '41_a3s.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '41'),
                ('step_id.name', '=', 'a3s'),
                ('codi_sollicitud', '=', '0123456789')
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3s = sw_obj.get_pas(cursor, uid, cas_atr)

            corrector_ids = a3s.corrector_ids
            counter_ids = a3s.counter_ids

            counter_test = counter_ids[0]
            self.assertEqual(counter_test.countermodel, 'marca1')
            self.assertEqual(counter_test.countertype, 'tipo1')
            self.assertEqual(counter_test.counternumber, 'B123456')
            self.assertEqual(counter_test.counterproperty, '04')
            self.assertEqual(counter_test.reallecture, 2000)
            self.assertEqual(counter_test.counterpressure, 54321.123)

            counter_test = counter_ids[1]
            self.assertEqual(counter_test.countermodel, 'marca2')
            self.assertEqual(counter_test.countertype, 'tipo2')
            self.assertEqual(counter_test.counternumber, 'C123456')
            self.assertEqual(counter_test.counterproperty, '06')
            self.assertEqual(counter_test.reallecture, 3000)
            self.assertEqual(counter_test.counterpressure, 13245.321)

            corrector_test = corrector_ids[0]
            self.assertEqual(corrector_test.correctormodel, 'modelo1')
            self.assertEqual(corrector_test.correctortype,  '01')
            self.assertEqual(corrector_test.correctornumber,  'D123456')
            self.assertEqual(corrector_test.correctorproperty,  '01')
            self.assertEqual(corrector_test.correctedlecture,  2200)

            corrector_test = corrector_ids[1]
            self.assertEqual(corrector_test.correctormodel, 'modelo2')
            self.assertEqual(corrector_test.correctortype,  '02')
            self.assertEqual(corrector_test.correctornumber,  'E654321')
            self.assertEqual(corrector_test.correctorproperty,  '02')
            self.assertEqual(corrector_test.correctedlecture,  3300)

            self.assertEqual(a3s.reqcode, '0123456789')
            self.assertEqual(a3s.nationality, 'ES')
            self.assertEqual(a3s.documenttype, '01')
            self.assertEqual(a3s.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a3s.previousatrcode, '000111222333444555666777')
            self.assertEqual(a3s.transfereffectivedate, '2018-07-01')
            self.assertEqual(a3s.readingtype, '1')
            self.assertEqual(a3s.extrainfo, 'comentarios extras')
            self.assertEqual(
                cas_atr.additional_info, u'Fecha Activación: 2018-07-01'
            )

    def test_import_a4_41(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '41_a2.xml'
        )
        a4_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '41_a4.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a4_xml_path, 'r') as f:
            a4_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '41_a2.xml')

            a4_xml = a4_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a4_xml = a4_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a4_xml = a4_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a4_xml = a4_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a4_xml = a4_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a4_xml, '41_a4.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '41'),
                ('step_id.name', '=', 'a4'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a4 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertEqual(a4.reqcode, '0123456789')
            self.assertEqual(a4.nationality, 'ES')
            self.assertEqual(a4.documenttype, '01')
            self.assertEqual(a4.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a4.updatereason, '01')
            self.assertEqual(a4.result, '13')
            self.assertEqual(a4.resultdesc, 'No Realizada')
            self.assertEqual(a4.rebuig_ids[0].motiu_rebuig.name, 'R32')
            self.assertEqual(a4.rebuig_ids[0].desc_rebuig,
                             'Fecha efecto solicitada anterior al dia actual.')
            self.assertEqual(a4.closingtype, '064')
            self.assertEqual(a4.closingtypedesc, 'No quiere gas')
            self.assertEqual(a4.interventionhourfrom, '2016-01-01 09:00:00')
            self.assertEqual(a4.interventionhourto, '2016-01-01 12:00:00')
            self.assertEqual(a4.visitnumber, 987)
            self.assertEqual(a4.extrainfo, 'comentarios extras')
            self.assertEqual(
                cas_atr.additional_info,
                u'(02) Tarifa:3.1. Caudal Diario: 1.5 -> 1.5. Rechazo: Fecha efecto solicitada anterior al dia actual.'
            )

    def test_activate_a3s41_cancel_contract_correctly(self):
        a3s_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '41_a3s.xml'
        )

        with open(a3s_xml_path, 'r') as f:
            a3s_xml = f.read()

        sw_obj = self.openerp.pool.get('giscegas.atr')
        pol_obj = self.openerp.pool.get('giscegas.polissa')

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(cursor, uid, a3s_xml, '41_a3s.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '41'),
                ('step_id.name', '=', 'a3s'),
                ('codi_sollicitud', '=', '0123456789')
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3s = sw_obj.get_pas(cursor, uid, cas_atr)

            pol_state = pol_obj.read(
                cursor, uid, contract_id, ['state']
            )['state']

            self.assertEqual(pol_state, 'activa')
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, cas_atr)

            pol_state = pol_obj.read(
                cursor, uid, contract_id, ['state']
            )['state']

            self.assertEqual(pol_state, 'baixa')
