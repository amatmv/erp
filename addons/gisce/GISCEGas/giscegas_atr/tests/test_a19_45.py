# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime
from destral.patch import PatchNewCursors


class Test45(TestSwitchingImport):

    def test_import_a19_45(self):
        a19_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '45_a19.xml'
        )
        with open(a19_xml_path, 'r') as f:
            a19_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')
            pol_obj = self.openerp.pool.get("giscegas.polissa")
            contract_id = self.get_contract_id(txn)

            sw_obj = self.openerp.pool.get('giscegas.atr')

            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)

            sw_obj.importar_xml(cursor, uid, a19_xml, '45_a19.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '45'),
                ('step_id.name', '=', 'a19'),
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a19 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(a19.operationtype, 'A10009')
            self.assertEqual(a19.operationnum, '5838204')
            self.assertEqual(a19.result, '13')
            self.assertEqual(a19.resultdesc, 'No Realizada')
            self.assertEqual(a19.closingtype, '028')
            self.assertEqual(a19.closingtypedesc, 'No realizada por causas imputables al cliente')
            self.assertEqual(a19.activationtype, '001')
            self.assertEqual(a19.activationtypedesc, 'Descripcion')
            self.assertEqual(a19.interventiondate, '2018-12-11 07:44:32')
            self.assertEqual(a19.resultinspection, '03')
            self.assertEqual(a19.resultinspectiondesc, 'AUSENTE')
            self.assertEqual(a19.visitnumber, 1)
            self.assertEqual(a19.counterchange, 'N')
            self.assertEqual(a19.removallecture, 50)
            self.assertEqual(a19.supplystatus, '01')
            self.assertEqual(a19.supplystatusdesc, 'Desc.Suply')
            self.assertEqual(a19.servicestatus, '00')
            self.assertEqual(a19.servicestatusdesc, 'EN SERVICIO')
            self.assertEqual(a19.extrainfo, 'COMENTARIIIS')
            self.assertEqual(a19.communicationreason, '08')
            self.assertEqual(a19.conceptnumber, 2)
            self.assertEqual(a19.extrainfo, 'COMENTARIIIS')
            self.assertEqual(a19.inspectionorig, '0')
            self.assertEqual(len(a19.document_ids), 2)
            self.assertEqual(len(a19.defect_ids), 2)
            self.assertEqual(len(a19.counter_ids), 2)
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, cas_atr)
            pol = pol_obj.browse(cursor, uid, contract_id)
            self.assertEqual(pol.inspeccio_ids[0].fecha_inspeccion, a19.interventiondate)
            self.assertEqual(pol.inspeccio_ids[0].resultado_inspeccion, a19.resultinspection)
