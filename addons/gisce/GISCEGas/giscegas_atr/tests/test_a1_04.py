# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime


class Test04(TestSwitchingImport):

    # ----------------------------TESTS A1_04-----------------------------------
    def test_creation_a104(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '04', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.04.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            a104 = step_obj.browse(cursor, uid, step_id)
            a1_04 = sw_obj.browse(cursor, uid, a104.sw_id.id)

            participant_obj = self.openerp.pool.get('giscemisc.participant')
            participant_partner = participant_obj.get_from_partner(
                cursor, uid, a1_04.partner_id.id
            )

            self.assertEqual(a1_04.proces_id.name, '04')
            self.assertEqual(a1_04.step_id.name, 'a1')
            self.assertEqual(participant_partner.codi_sifco, '1234')
            self.assertEqual(a1_04.company_id.codi_sifco, '4321')
            self.assertEqual(a1_04.cups_id.name, 'ES1234000000000001JN0F')
            # self.assertEqual(a1_04.cups_polissa_id.name, '0001')
            self.assertIn(a1_04.cups_polissa_id.name, ('0001', '0001C'))

            self.assertEqual(a104.reqcanceldate, datetime.today()
                             .strftime("%Y-%m-%d"))

            self.assertEqual(a104.cancelreason, '01')
            self.assertEqual(a104.modeffectdate, '05')
            self.assertEqual(a104.nationality, 'CH')
            self.assertEqual(a104.documenttype, '01')
            self.assertEqual(a104.documentnum, 'B82420654')
            self.assertEqual(a104.contactphonenumber, '555123456')

            today = datetime.strftime(datetime.now(), '%Y-%m-%d')

            expected_info = ("(01): A petición del cliente por cese de "
                             "su actividad. Baja: Cuanto antes {0}").format(today)

            self.assertEqual(a1_04.additional_info, expected_info)

    def test_export_a104(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '04', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.04.a1')
            register_doc_obj = self.openerp.pool.get('giscegas.atr.document')

            a104 = step_obj.browse(cursor, uid, step_id)
            a104.write({
                'modeffectdate': '03',
                'extrainfo': (
                    'Información adicional con la ubicación del '
                    'tesoro de Mary Read'
                ),
                'nationality': 'ES',
                'documenttype': '07',
                'documentnum': '11111111H',
                'cancelreason': '04',
                'titulartype': 'F',
                'reqcanceldate': '2018-03-08',
            })

            a104_01_generat = a104.generar_xml()[1]
            a104_01_generat = a104_01_generat.replace(' ', '').replace('\n', '')

            a104_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '04_a1.xml'
            )
            with open(a104_01_xml_path, 'r') as f:
                a104_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a104.sw_id.codi_sollicitud
                data_emissio = a104.header_id.date_created.split(" ")[0]
                hora_emissio = a104.header_id.date_created.split(" ")[1]
                a104_01_xml = a104_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a104_01_xml = a104_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a104_01_xml = a104_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a104_01_xml = a104_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a104_01_xml = a104_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a104_01_generat, a104_01_xml)

    # ----------------------------TESTS A2_04-----------------------------------

    def test_import_a2_04(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '04_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.04.a1')
            # header_obj = self.pool.get('giscegas.atr.step.header')
            # rebuig_obj = self.pool.get('giscegas.atr.rebuig')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '04', 'a1'
            )

            a2 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a2.sw_id.codi_sollicitud
            data_emissio = a2.date_created.split(" ")[0]
            hora_emissio = a2.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '04_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '04'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')

            self.assertEqual(a2.newmodeffectdate, '04')
            self.assertEqual(a2.cancelreason, '04')

            self.assertEqual(
                a2.extrainfo,
                'Información adicional con la ubicación del tesoro de Mary Read'
            )

            self.assertEqual(
                cas_atr.additional_info,
                u'(01): A petición del cliente por cese de su '
                u'actividad. Baja: Cuanto antes {0}.'.format(
                    datetime.today().strftime("%Y-%m-%d")
                )
            )

    def test_import_a2_04_reject(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '04_a2_rej.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.04.a1')
            # header_obj = self.pool.get('giscegas.atr.step.header')
            # rebuig_obj = self.pool.get('giscegas.atr.rebuig')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '04', 'a1'
            )

            a2 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a2.sw_id.codi_sollicitud
            data_emissio = a2.date_created.split(" ")[0]
            hora_emissio = a2.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '04_a2_rej.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '04'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertTrue(a2.rebuig)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '05')
            self.assertEqual(a2.resultdesc, 'Denegada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')

            self.assertEqual(a2.newmodeffectdate, '04')
            self.assertEqual(a2.cancelreason, '04')

            self.assertEqual(
                a2.extrainfo,
                'Información adicional con la ubicación del tesoro de Mary Read'
            )

            self.assertEqual(
                cas_atr.additional_info,
                u'(01): A petición del cliente por cese '
                u'de su actividad. Baja: Cuanto antes {0}. '
                u'Rechazo: Fecha efecto solicitada anterior al dia '
                u'actual.'.format(
                    datetime.today().strftime("%Y-%m-%d")
                )
            )
            self.assertTrue(a2.rebuig_ids)
            self.assertEqual(
                a2.rebuig_ids[0].desc_rebuig,
                u'Fecha efecto solicitada anterior al dia actual.'
            )
            self.assertEqual(a2.rebuig_ids[0].motiu_rebuig.name, u'R32')
            self.assertEqual(
                a2.rebuig_ids[0].motiu_rebuig.text,
                u'Fecha efecto solicitada anterior al día actual.'
            )

    # ----------------------------TESTS A3_04-----------------------------------
    def test_import_a3_04(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '04_a2.xml'
        )
        a3_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '04_a3.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.04.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '04', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '04_a2.xml')

            a3_xml = a3_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>".format(
                    hora_emissio
                )
            )

            a3_xml = a3_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a3_xml, '04_a3.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '04'),
                ('step_id.name', '=', 'a3'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3 = sw_obj.get_pas(cursor, uid, cas_atr)

            corrector_ids = a3.corrector_ids
            counter_ids = a3.counter_ids

            counter_test = counter_ids[0]
            self.assertEqual(counter_test.countermodel, 'marca1')
            self.assertEqual(counter_test.countertype, 'tipo1')
            self.assertEqual(counter_test.counternumber, 'B123456')
            self.assertEqual(counter_test.counterproperty, '04')
            self.assertEqual(counter_test.reallecture, 2000)
            self.assertEqual(counter_test.counterpressure, 54321.123)

            counter_test = counter_ids[1]
            self.assertEqual(counter_test.countermodel, 'marca2')
            self.assertEqual(counter_test.countertype, 'tipo2')
            self.assertEqual(counter_test.counternumber, 'C123456')
            self.assertEqual(counter_test.counterproperty, '06')
            self.assertEqual(counter_test.reallecture, 3000)
            self.assertEqual(counter_test.counterpressure, 13245.321)

            corrector_test = corrector_ids[0]
            self.assertEqual(corrector_test.correctormodel, 'modelo1')
            self.assertEqual(corrector_test.correctortype,  '01')
            self.assertEqual(corrector_test.correctornumber,  'D123456')
            self.assertEqual(corrector_test.correctorproperty,  '01')
            self.assertEqual(corrector_test.correctedlecture,  2200)

            corrector_test = corrector_ids[1]
            self.assertEqual(corrector_test.correctormodel, 'modelo2')
            self.assertEqual(corrector_test.correctortype,  '02')
            self.assertEqual(corrector_test.correctornumber,  'E654321')
            self.assertEqual(corrector_test.correctorproperty,  '02')
            self.assertEqual(corrector_test.correctedlecture,  3300)

            self.assertEqual(a3.result, '09')
            self.assertEqual(a3.resultdesc, 'Realizada')
            self.assertEqual(a3.reqcode, '0123456789')
            self.assertEqual(a3.atrcode, '000111222333444555666777')
            self.assertEqual(a3.transfereffectivedate, '2018-07-01')
            self.assertEqual(a3.nationality, 'ES')
            self.assertEqual(a3.documenttype, '01')
            self.assertEqual(a3.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a3.titulartype, 'F')
            self.assertEqual(
                a3.extrainfo,
                'Información adicional con la ubicación del tesoro de Mary Read'
            )
            self.assertEqual(a3.activationtype, '001')
            self.assertEqual(
                a3.activationtypedesc,
                'Realizada puesta en servicio'
            )
            self.assertEqual(
                a3.operationnum,
                '4444448877787879894898189881848844894894'
            )

    # ----------------------------TESTS A4_04-----------------------------------

    def test_import_a4_04(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '04_a2.xml'
        )
        a4_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '04_a4.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a4_xml_path, 'r') as f:
            a4_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.04.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '04', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '04_a2.xml')

            a4_xml = a4_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a4_xml = a4_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a4_xml = a4_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a4_xml, '04_a4.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '04'),
                ('step_id.name', '=', 'a4'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a4 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertEqual(a4.reqcode, '0123456789')
            self.assertEqual(a4.titulartype, 'F')
            self.assertEqual(a4.nationality, 'ES')
            self.assertEqual(a4.documenttype, '01')
            self.assertEqual(a4.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a4.result, '13')
            self.assertEqual(a4.resultdesc, 'No Realizada')
            self.assertEqual(a4.rebuig_ids[0].motiu_rebuig.name, 'R32')
            self.assertEqual(a4.rebuig_ids[0].desc_rebuig,
                             'Fecha efecto solicitada anterior al dia actual.')
            self.assertEqual(
                a4.operationnum,
                '4444448877787879894898189881848844894894'
            )
            self.assertEqual(
                a4.extrainfo,
                'Información adicional con la ubicación del tesoro de Mary Read'
            )

            self.assertEqual(
                cas_atr.additional_info,
                u'(01): A petición del cliente por cese de su '
                u'actividad. Baja: Cuanto antes {0} '
                u'Rechazo: Fecha efecto solicitada anterior al dia '
                u'actual.'.format(datetime.today().strftime("%Y-%m-%d"))
            )
            self.assertTrue(a4.rebuig_ids)
            self.assertEqual(
                a4.rebuig_ids[0].desc_rebuig,
                u'Fecha efecto solicitada anterior al dia actual.'
            )
            self.assertEqual(a4.rebuig_ids[0].motiu_rebuig.name, u'R32')
            self.assertEqual(
                a4.rebuig_ids[0].motiu_rebuig.text,
                u'Fecha efecto solicitada anterior al día actual.'
            )
