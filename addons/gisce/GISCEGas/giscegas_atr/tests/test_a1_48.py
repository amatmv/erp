# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime


class Test48(TestSwitchingImport):

    def test_export_a1_48(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '48', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.48.a1')
            register_doc_obj = self.openerp.pool.get('giscegas.atr.document')

            docs_vals_1 = {
                'doctype': 'CC',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-02',
                'extrainfo': '404 page not found',
            }
            created_doc_1 = register_doc_obj.create(cursor, uid, docs_vals_1)
            docs_vals_2 = {
                'doctype': '01',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-03',
                'extrainfo': 'Comments',
            }
            created_doc_2 = register_doc_obj.create(cursor, uid, docs_vals_2)

            province_obj = self.openerp.pool.get("res.country.state")
            province_owner_id = province_obj.search(
                cursor, uid, [('code', '=', '19')]
            )[0]

            city_obj = self.openerp.pool.get("res.municipi")
            city_owner_id = city_obj.search(
                cursor, uid, [('ine', '=', '49221')]
            )[0]

            claimref_obj = self.openerp.pool.get('giscegas.atr.referencia.reclamacio')
            claimerreflist_id = claimref_obj.create(cursor, uid, {
                'wrongattentiontype': '01',
                'comreferencenum': '0000001',
                'targetclaimcomreferencenum': '9999998',
                'conceptcontract': '01',
                'conceptfacturation': '02',
                'contactname': 'mortdegana',
                'telephoneprefix': '+34',
                'telephonenumber': '666555444',
                'contactemail': 'matalas@gas',
                'nnssexpedient': '45666666',
                'fraudrecordnum': '888888888',
                'incidentperiod_datefrom': '2018-09-21',
                'incidentperiod_dateto': '2018-09-21',
                'invoicenumber': 'F5555',
                'incidentlocationdesc': 'calle pequeña',
                'incidentlocationprovince': province_owner_id,
                'incidentlocationcity': city_owner_id,
                'incidentlocationcitysubdivision': '20 AA',
                'incidentlocationzipcode': '17888',
                'readingdate': '2018-09-21',
                'readingvalue': 4.89,
                'incidentdate': '2018-09-21',
                'documenttype': '01',
                'documentnum': 'ES11111111T',
                'titulartype': 'F',
                'firstname': 'nom',
                'familyname1': 'cognom',
                'familyname2': 'cognom 2',
                'client_telephoneprefix': '34',
                'client_telephonenumber': '999111222',
                'email': 'a@a',
                'province': province_owner_id,
                'city': city_owner_id,
                'zipcode': '16001',
                'streettype': 'ACCE',
                'street': 'inventat',
                'streetnumber': '4_ce',
                'portal': '5_TE2',
                'staircase': '5_mIh',
                'floor': '5_e6A',
                'door': '5_40T',
                'claimedcompensation': 6666.24,
                'iban': 'ES00000000000000000000000',
            })

            a148 = step_obj.browse(cursor, uid, step_id)
            a148.write({
                'claimertype': '01',
                'claimtype': '01',
                'claimsubtype': '001',
                'originreference': 'AB999888',
                'claimreferencelist': [(6, 0, [claimerreflist_id])],
                'legallimitdate': '2018-09-21',
                'priority': '1',
                'extrainfo': 'comentarios extra',
                'document_ids': [(6, 0, [created_doc_1, created_doc_2])],
                'claimerdocumenttype': '01',
                'claimerdocumentnum': 'ES00000000T',
                'claimerfirstname': 'gas',
                'claimerlastname': 'al',
                'claimersecondname': 'matalas',
                'claimerprefixtel1': '34',
                'claimertelephone1': '999888777',
                'claimeremail': 'gas@matalas',
            })
            a148 = step_obj.browse(cursor, uid, step_id)

            a148_01_generat = a148.generar_xml()[1]
            a148_01_generat = a148_01_generat.replace(' ', '').replace('\n', '')

            a148_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '48_a1.xml'
            )
            with open(a148_01_xml_path, 'r') as f:
                a148_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a148.sw_id.codi_sollicitud
                data_emissio = a148.header_id.date_created.split(" ")[0]
                hora_emissio = a148.header_id.date_created.split(" ")[1]
                a148_01_xml = a148_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a148_01_xml = a148_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a148_01_xml = a148_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a148_01_xml = a148_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a148_01_xml = a148_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a148_01_generat, a148_01_xml)

    def test_import_a2_48(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '48_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.48.a1')
            # header_obj = self.pool.get('giscegas.atr.step.header')
            # rebuig_obj = self.pool.get('giscegas.atr.rebuig')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '48', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '48_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '48'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.claimtype, '01')
            self.assertEqual(a2.claimsubtype.name, '001')
            self.assertEqual(a2.srcode, '00010')
            self.assertEqual(a2.extrainfo, 'comentarios extras')

    def test_reject_import_a2_48(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '48_a2_rej.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.48.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '48', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '48_a1_02.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '48'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertTrue(cas_atr.rebuig)
            self.assertTrue(a2.rebuig)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '05')
            self.assertEqual(a2.resultdesc, 'Denegada')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(a2.rebuig_ids[0].motiu_rebuig.name, 'R32')
            self.assertEqual(a2.rebuig_ids[0].desc_rebuig, 'Fecha efecto solicitada anterior al dia actual.')

    def test_import_a25_48(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '48_a2.xml'
        )
        a25_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '48_a25.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a25_xml_path, 'r') as f:
            a25_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.48.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '48', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '48_a2.xml')

            a25_xml = a25_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a25_xml = a25_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a25_xml = a25_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a25_xml = a25_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a25_xml = a25_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a25_xml, '48_a25.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '48'),
                ('step_id.name', '=', 'a25'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a25 = sw_obj.get_pas(cursor, uid, cas_atr)

            counter_ids = a25.counter_ids
            defect_ids = a25.defect_ids
            information_ids = a25.information_ids
            doc_ids = a25.document_ids

            counter_test = counter_ids[0]
            self.assertEqual(counter_test.countermodel, 'model')
            self.assertEqual(counter_test.countertype, '55')
            self.assertEqual(counter_test.counternumber, '9999')
            self.assertEqual(counter_test.counterproperty, '04')
            self.assertEqual(counter_test.reallecture, 6810)
            self.assertEqual(counter_test.counterpressure, 8.1)
            defect_test = defect_ids[0]
            self.assertEqual(defect_test.code, '001')
            self.assertEqual(defect_test.description, 'FUGA DE GAS')
            information_test = information_ids[0]
            self.assertEqual(information_test.moreinformation, 'moer information')
            self.assertEqual(information_test.moreinformationtype, '01')
            self.assertEqual(information_test.limitsenddate, '2018-09-24')
            doc_test = doc_ids[0]
            self.assertEqual(doc_test.date, '2018-09-24')
            self.assertEqual(doc_test.doctype, 'CC')
            self.assertEqual(doc_test.url, 'gasalatalas.com')
            self.assertEqual(doc_test.extrainfo, 'extra')

            self.assertEqual(a25.reqcode, '123456789')
            self.assertEqual(a25.claimtype, '01')
            self.assertEqual(a25.claimsubtype.name, '001')
            self.assertEqual(a25.srcode, '99988')
            self.assertEqual(a25.interventiontype, '01')
            self.assertEqual(a25.newclaimtype, '01')
            self.assertEqual(a25.newclaimsubtype.name, '001')
            self.assertEqual(a25.operationnum, '741')
            self.assertEqual(a25.visitdatetime, '2018-09-24 15:25:18')
            self.assertEqual(a25.informationtype, '002')
            self.assertEqual(a25.resultreasonintervention, 'R01')
            self.assertEqual(a25.interventionhourfrom, '2018-09-24 15:25:18')
            self.assertEqual(a25.interventionhourto, '2018-09-24 15:25:18')
            self.assertEqual(a25.resultinspection, '01')
            self.assertEqual(a25.visitnumber, 590)
            self.assertEqual(a25.extrainfo, 'coments')
            self.assertEqual(a25.conceptnumber, 74)

    def test_export_a26_48(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '48', 'a26'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.48.a26')
            register_doc_obj = self.openerp.pool.get('giscegas.atr.document')

            docs_vals_1 = {
                'doctype': 'CC',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-02',
                'extrainfo': '404 page not found',
            }
            created_doc_1 = register_doc_obj.create(cursor, uid, docs_vals_1)
            docs_vals_2 = {
                'doctype': '01',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-03',
                'extrainfo': 'Comments',
            }
            created_doc_2 = register_doc_obj.create(cursor, uid, docs_vals_2)

            province_obj = self.openerp.pool.get("res.country.state")
            province_owner_id = province_obj.search(
                cursor, uid, [('code', '=', '19')]
            )[0]

            city_obj = self.openerp.pool.get("res.municipi")
            city_owner_id = city_obj.search(
                cursor, uid, [('ine', '=', '49221')]
            )[0]

            claimref_obj = self.openerp.pool.get('giscegas.atr.referencia.reclamacio')
            claimerreflist_id = claimref_obj.create(cursor, uid, {
                'wrongattentiontype': '01',
                'comreferencenum': '0000001',
                'targetclaimcomreferencenum': '9999998',
                'conceptcontract': '01',
                'conceptfacturation': '02',
                'contactname': 'mortdegana',
                'telephoneprefix': '+34',
                'telephonenumber': '666555444',
                'contactemail': 'matalas@gas',
                'nnssexpedient': '45666666',
                'fraudrecordnum': '888888888',
                'incidentperiod_datefrom': '2018-09-21',
                'incidentperiod_dateto': '2018-09-21',
                'invoicenumber': 'F5555',
                'incidentlocationdesc': 'calle pequeña',
                'incidentlocationprovince': province_owner_id,
                'incidentlocationcity': city_owner_id,
                'incidentlocationcitysubdivision': '20 AA',
                'incidentlocationzipcode': '17888',
                'readingdate': '2018-09-21',
                'readingvalue': 4.89,
                'incidentdate': '2018-09-21',
                'documenttype': '01',
                'documentnum': 'ES11111111T',
                'titulartype': 'F',
                'firstname': 'nom',
                'familyname1': 'cognom',
                'familyname2': 'cognom 2',
                'client_telephoneprefix': '34',
                'client_telephonenumber': '999111222',
                'email': 'a@a',
                'province': province_owner_id,
                'city': city_owner_id,
                'zipcode': '16001',
                'streettype': 'ACCE',
                'street': 'inventat',
                'streetnumber': '4_ce',
                'portal': '5_TE2',
                'staircase': '5_mIh',
                'floor': '5_e6A',
                'door': '5_40T',
                'claimedcompensation': 6666.24,
                'iban': 'ES00000000000000000000000',
            })

            variableinf_obj = self.openerp.pool.get('giscegas.atr.variableinf')
            variableinf_id = variableinf_obj.create(cursor, uid, {
                'moreinformationtype': '01',
                'description': 'desc',
                'variabletype': '01',
                'variablevalue': 'val',
            })

            a148 = step_obj.browse(cursor, uid, step_id)
            a148.write({
                'reqcode': '7777',
                'informationdate': '2018-09-24',
                'informationtype': '01',
                'claimreferencelist': [(6, 0, [claimerreflist_id])],
                'variableinflist': [(6, 0, [variableinf_id])],
                'document_ids': [(6, 0, [created_doc_1, created_doc_2])],
                'extrainfo': 'comentarios extra',
            })
            a2648 = step_obj.browse(cursor, uid, step_id)

            a2648_01_generat = a2648.generar_xml()[1]
            a2648_01_generat = a2648_01_generat.replace(' ', '').replace('\n', '')

            a2648_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '48_a26.xml'
            )
            with open(a2648_01_xml_path, 'r') as f:
                a2648_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a2648.sw_id.codi_sollicitud
                data_emissio = a2648.header_id.date_created.split(" ")[0]
                hora_emissio = a2648.header_id.date_created.split(" ")[1]
                a2648_01_xml = a2648_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a2648_01_xml = a2648_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a2648_01_xml = a2648_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a2648_01_xml = a2648_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a2648_01_xml = a2648_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a2648_01_generat, a2648_01_xml)

    def test_import_a3_48(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '48_a2.xml'
        )
        a3_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '48_a3.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.48.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '48', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '48_a2.xml')

            a3_xml = a3_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a3_xml, '48_a3.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '48'),
                ('step_id.name', '=', 'a3'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3 = sw_obj.get_pas(cursor, uid, cas_atr)

            doc_ids = a3.document_ids
            doc_test = doc_ids[0]
            self.assertEqual(doc_test.date, '2018-09-24')
            self.assertEqual(doc_test.doctype, 'CC')
            self.assertEqual(doc_test.url, 'gasalatalas.com')
            self.assertEqual(doc_test.extrainfo, 'extra')

            self.assertEqual(a3.claimtype, '01')
            self.assertEqual(a3.claimsubtype.name, '001')
            self.assertEqual(a3.srcode, '55')
            self.assertEqual(a3.result, '01')
            self.assertEqual(a3.resultdesc, 'Procedente / Favorable')
            self.assertEqual(a3.resolutiondetail, '0010101')
            self.assertEqual(a3.resolutiondetaildesc, 'Se piden disculpas')
            self.assertEqual(a3.reqdescription, 'rdesc')
            self.assertEqual(a3.creditedcompensation, 88.02)
            self.assertEqual(a3.anomalyfraudrecordnum, '885522')
            self.assertEqual(a3.movementdate, '2018-09-24 00:00:00')
            self.assertEqual(a3.extrainfo, 'extra')

    #
    # def test_import_a3s_48(self):
    #
    #     a3s_xml_path = get_module_resource(
    #         'giscegas_atr', 'tests', 'fixtures', '48_a3s.xml'
    #     )
    #
    #     with open(a3s_xml_path, 'r') as f:
    #         a3s_xml = f.read()
    #
    #     with Transaction().start(self.database) as txn:
    #         uid = txn.user
    #         cursor = txn.cursor
    #         self.switch(txn, 'comer')
    #
    #         sw_obj = self.openerp.pool.get('giscegas.atr')
    #
    #         sw_obj.importar_xml(cursor, uid, a3s_xml, '48_a3s.xml')
    #
    #         res = sw_obj.search(cursor, uid, [
    #             ('proces_id.name', '=', '48'),
    #             ('step_id.name', '=', 'a3s'),
    #             ('codi_sollicitud', '=', '0123456789')
    #         ])
    #
    #         cas_atr = sw_obj.browse(cursor, uid, res[0])
    #         a3s = sw_obj.get_pas(cursor, uid, cas_atr)
    #
    #         corrector_ids = a3s.corrector_ids
    #         counter_ids = a3s.counter_ids
    #
    #         counter_test = counter_ids[0]
    #         self.assertEqual(counter_test.countermodel, 'marca1')
    #         self.assertEqual(counter_test.countertype, 'tipo1')
    #         self.assertEqual(counter_test.counternumber, 'B123456')
    #         self.assertEqual(counter_test.counterproperty, '04')
    #         self.assertEqual(counter_test.reallecture, 2000)
    #         self.assertEqual(counter_test.counterpressure, 54321.123)
    #
    #         counter_test = counter_ids[1]
    #         self.assertEqual(counter_test.countermodel, 'marca2')
    #         self.assertEqual(counter_test.countertype, 'tipo2')
    #         self.assertEqual(counter_test.counternumber, 'C123456')
    #         self.assertEqual(counter_test.counterproperty, '06')
    #         self.assertEqual(counter_test.reallecture, 3000)
    #         self.assertEqual(counter_test.counterpressure, 13245.321)
    #
    #         corrector_test = corrector_ids[0]
    #         self.assertEqual(corrector_test.correctormodel, 'modelo1')
    #         self.assertEqual(corrector_test.correctortype,  '01')
    #         self.assertEqual(corrector_test.correctornumber,  'D123456')
    #         self.assertEqual(corrector_test.correctorproperty,  '01')
    #         self.assertEqual(corrector_test.correctedlecture,  2200)
    #
    #         corrector_test = corrector_ids[1]
    #         self.assertEqual(corrector_test.correctormodel, 'modelo2')
    #         self.assertEqual(corrector_test.correctortype,  '02')
    #         self.assertEqual(corrector_test.correctornumber,  'E654321')
    #         self.assertEqual(corrector_test.correctorproperty,  '02')
    #         self.assertEqual(corrector_test.correctedlecture,  3300)
    #
    #         self.assertEqual(a3s.reqcode, '0123456789')
    #         self.assertEqual(a3s.nationality, 'ES')
    #         self.assertEqual(a3s.documenttype, '01')
    #         self.assertEqual(a3s.documentnum, '11111111H')
    #         self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
    #         self.assertEqual(a3s.previousatrcode, '000111222333444555666777')
    #         self.assertEqual(a3s.transfereffectivedate, '2018-07-01')
    #         self.assertEqual(a3s.readingtype, '1')
    #         self.assertEqual(a3s.extrainfo, 'comentarios extras')
    #         self.assertEqual(
    #             cas_atr.additional_info, u'Fecha Activación: 2018-07-01'
    #         )
    #
    # def test_import_a4_48(self):
    #     a2_xml_path = get_module_resource(
    #         'giscegas_atr', 'tests', 'fixtures', '48_a2.xml'
    #     )
    #     a4_xml_path = get_module_resource(
    #         'giscegas_atr', 'tests', 'fixtures', '48_a4.xml'
    #     )
    #     with open(a2_xml_path, 'r') as f:
    #         a2_xml = f.read()
    #
    #     with open(a4_xml_path, 'r') as f:
    #         a4_xml = f.read()
    #
    #     with Transaction().start(self.database) as txn:
    #         uid = txn.user
    #         cursor = txn.cursor
    #         self.switch(txn, 'comer')
    #
    #         sw_obj = self.openerp.pool.get('giscegas.atr')
    #         step_obj = self.openerp.pool.get('giscegas.atr.48.a1')
    #
    #         contract_id = self.get_contract_id(txn)
    #         self.change_polissa_comer(txn)
    #         self.update_polissa_distri(txn)
    #         step_id = self.create_case_and_step(
    #             cursor, uid, contract_id, '48', 'a1'
    #         )
    #
    #         a1 = step_obj.browse(cursor, uid, step_id)
    #
    #         # Change the dates & id from the XML.
    #         codi_sol = a1.sw_id.codi_sollicitud
    #         data_emissio = a1.date_created.split(" ")[0]
    #         hora_emissio = a1.date_created.split(" ")[1]
    #
    #         a2_xml = a2_xml.replace(
    #             "<communicationsdate>2018-05-01</communicationsdate>",
    #             "<communicationsdate>{0}</communicationsdate>"
    #             .format(data_emissio)
    #         )
    #         a2_xml = a2_xml.replace(
    #             "<communicationshour>12:00:00</communicationshour>",
    #             "<communicationshour>{0}</communicationshour>"
    #             .format(hora_emissio)
    #         )
    #         a2_xml = a2_xml.replace(
    #             "<reqdate>2018-05-01</reqdate>",
    #             "<reqdate>{0}</reqdate>".format(data_emissio)
    #         )
    #         a2_xml = a2_xml.replace(
    #             "<reqhour>12:00:00</reqhour>",
    #             "<reqhour>{0}</reqhour>".format(hora_emissio)
    #         )
    #         a2_xml = a2_xml.replace(
    #             "<comreferencenum>000123456789</comreferencenum>",
    #             "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
    #         )
    #
    #         sw_obj.importar_xml(cursor, uid, a2_xml, '48_a2.xml')
    #
    #         a4_xml = a4_xml.replace(
    #             "<communicationsdate>2018-05-01</communicationsdate>",
    #             "<communicationsdate>{0}</communicationsdate>"
    #             .format(data_emissio)
    #         )
    #         a4_xml = a4_xml.replace(
    #             "<communicationshour>12:00:00</communicationshour>",
    #             "<communicationshour>{0}</communicationshour>"
    #             .format(hora_emissio)
    #         )
    #         a4_xml = a4_xml.replace(
    #             "<reqdate>2018-05-01</reqdate>",
    #             "<reqdate>{0}</reqdate>".format(data_emissio)
    #         )
    #         a4_xml = a4_xml.replace(
    #             "<reqhour>12:00:00</reqhour>",
    #             "<reqhour>{0}</reqhour>".format(hora_emissio)
    #         )
    #         a4_xml = a4_xml.replace(
    #             "<comreferencenum>000123456789</comreferencenum>",
    #             "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
    #         )
    #
    #         sw_obj.importar_xml(cursor, uid, a4_xml, '48_a4.xml')
    #
    #         res = sw_obj.search(cursor, uid, [
    #             ('proces_id.name', '=', '48'),
    #             ('step_id.name', '=', 'a4'),
    #             ('codi_sollicitud', '=', codi_sol)
    #         ])
    #
    #         cas_atr = sw_obj.browse(cursor, uid, res[0])
    #         a4 = sw_obj.get_pas(cursor, uid, cas_atr)
    #
    #         self.assertEqual(a4.reqcode, '0123456789')
    #         self.assertEqual(a4.nationality, 'ES')
    #         self.assertEqual(a4.documenttype, '01')
    #         self.assertEqual(a4.documentnum, '11111111H')
    #         self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
    #         self.assertEqual(a4.updatereason, '01')
    #         self.assertEqual(a4.result, '13')
    #         self.assertEqual(a4.resultdesc, 'No Realizada')
    #         self.assertEqual(a4.rebuig_ids[0].motiu_rebuig.name, 'R32')
    #         self.assertEqual(a4.rebuig_ids[0].desc_rebuig,
    #                          'Fecha efecto solicitada anterior al dia actual.')
    #         self.assertEqual(a4.closingtype, '064')
    #         self.assertEqual(a4.closingtypedesc, 'No quiere gas')
    #         self.assertEqual(a4.interventionhourfrom, '2016-01-01 09:00:00')
    #         self.assertEqual(a4.interventionhourto, '2016-01-01 12:00:00')
    #         self.assertEqual(a4.visitnumber, 987)
    #         self.assertEqual(a4.extrainfo, 'comentarios extras')
    #         self.assertEqual(
    #             cas_atr.additional_info,
    #             u'(02) Tarifa:3.1. Caudal Diario: 1.5 -> 1.5. Rechazo: Fecha efecto solicitada anterior al dia actual.'
    #         )
