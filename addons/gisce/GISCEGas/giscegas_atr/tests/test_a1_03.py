# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime
import calendar
from destral.patch import PatchNewCursors


class Test03(TestSwitchingImport):

    def test_creation_a103(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '03', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.03.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            a103 = step_obj.browse(cursor, uid, step_id)
            a1_03 = sw_obj.browse(cursor, uid, a103.sw_id.id)

            participant_obj = self.openerp.pool.get('giscemisc.participant')
            participant_partner = participant_obj.get_from_partner(cursor, uid, a1_03.partner_id.id)
            self.assertEqual(a1_03.proces_id.name, '03')
            self.assertEqual(a1_03.step_id.name, 'a1')
            self.assertEqual(participant_partner.codi_sifco, '1234')
            self.assertEqual(a1_03.company_id.codi_sifco, '4321')
            self.assertEqual(a1_03.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(a103.nationality, 'CH')
            self.assertEqual(a103.documenttype, '01')
            self.assertEqual(a103.documentnum, 'B82420654')
            self.assertEqual(a103.titulartype, 'J')
            self.assertEqual(
                a1_03.additional_info,
                u'Anulación para el caso {0}'.format(a1_03.codi_sollicitud)
            )

    def test_export_a103(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '03', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.03.a1')

            a103 = step_obj.browse(cursor, uid, step_id)
            a103.write({
                'extrainfo': 'comentarios extras',
                'nationality': 'ES',
                'documenttype': '07',
                'documentnum': '11111111H',
                'titulartype': 'F',
                'annulmentreason': '002',
            })

            a103_01_generat = a103.generar_xml()[1]
            a103_01_generat = a103_01_generat.replace(' ', '').replace('\n', '')

            a103_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '03_a1.xml'
            )
            with open(a103_01_xml_path, 'r') as f:
                a103_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a103.sw_id.codi_sollicitud
                data_emissio = a103.header_id.date_created.split(" ")[0]
                hora_emissio = a103.header_id.date_created.split(" ")[1]
                a103_01_xml = a103_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a103_01_xml = a103_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a103_01_xml = a103_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a103_01_xml = a103_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a103_01_xml = a103_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a103_01_generat, a103_01_xml)

    def test_import_a2_03(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '03_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.03.a1')
            # header_obj = self.pool.get('giscegas.atr.step.header')
            # rebuig_obj = self.pool.get('giscegas.atr.rebuig')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '03', 'a1'
            )

            a2 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a2.sw_id.codi_sollicitud
            data_emissio = a2.date_created.split(" ")[0]
            hora_emissio = a2.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '03_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '03'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.annulmentreason, '002')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(
                cas_atr.additional_info,
                u'Anulación para el caso {0}'.format(cas_atr.codi_sollicitud)
            )

    def test_reject_import_a2_03(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '03_a2_rej.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.03.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '03', 'a1'
            )

            a2 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a2.sw_id.codi_sollicitud
            data_emissio = a2.date_created.split(" ")[0]
            hora_emissio = a2.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '03_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '03'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertTrue(cas_atr.rebuig)
            self.assertTrue(a2.rebuig)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '05')
            self.assertEqual(a2.resultdesc, 'Denegada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.annulmentreason, '002')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(a2.rebuig_ids[0].motiu_rebuig.name, 'R32')
            self.assertEqual(a2.rebuig_ids[0].desc_rebuig,
                             'Fecha efecto solicitada anterior al dia actual.')
            self.assertEqual(
                cas_atr.additional_info,
                u'Anulación para el caso {0}. Rechazo: Fecha efecto solicitada anterior al dia actual.'.format(cas_atr.codi_sollicitud)
            )

    def test_import_a2s_03(self):

        a2s_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '03_a2s.xml'
        )

        with open(a2s_xml_path, 'r') as f:
            a2s_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')

            sw_obj.importar_xml(cursor, uid, a2s_xml, '03_a2s.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '03'),
                ('step_id.name', '=', 'a2s'),
                ('codi_sollicitud', '=', '0123456789')
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2s = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertEqual(a2s.reqcode, '0123456789')
            self.assertEqual(a2s.nationality, 'ES')
            self.assertEqual(a2s.documenttype, '07')
            self.assertEqual(a2s.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2s.annulmentreason, '002')

    def test_activation_paso_a2_A1_03_cancel_contract(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '03_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            motiu_obj = self.openerp.pool.get('giscedata.motiu.canvi')

            if motiu_obj:
                mt_id = motiu_obj.create(cursor, uid,
                                         {'codi': 'CDO',
                                          'name': 'Desistimiento de cliente - Otros',
                                          'type': 'cancelacio'
                                          }
                                         )
                self.assertTrue(mt_id)

            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.03.a1')
            pol_obj = self.openerp.pool.get('giscegas.polissa')
            # header_obj = self.pool.get('giscegas.atr.step.header')
            # rebuig_obj = self.pool.get('giscegas.atr.rebuig')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '03', 'a1'
            )

            a2 = step_obj.browse(cursor, uid, step_id)
            pas_a1 = a2

            # Change the dates & id from the XML.
            codi_sol = a2.sw_id.codi_sollicitud
            data_emissio = a2.date_created.split(" ")[0]
            hora_emissio = a2.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '03_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '03'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            # a2.activa_cas_atr()
            step_41_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )

            step_41_obj = self.openerp.pool.get('giscegas.atr.41.a1')

            st_41 = step_41_obj.browse(cursor, uid, step_41_id)
            pas_a1.write({'atr_anulat_id': st_41.sw_id.id})
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, cas_atr)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.nationality, 'ES')
            self.assertEqual(a2.documenttype, '01')
            self.assertEqual(a2.documentnum, '11111111H')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.annulmentreason, '002')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(
                cas_atr.additional_info,
                u'Anulación para el caso {0}'.format(
                    cas_atr.codi_sollicitud)
            )

            polissa = pol_obj.browse(cursor, uid, contract_id)
            self.assertEqual(polissa.state, 'cancelada')

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(cas_atr.state, 'done')

            st_41 = step_41_obj.browse(cursor, uid, step_41_id)
            self.assertEqual(st_41.sw_id.state, 'done')
