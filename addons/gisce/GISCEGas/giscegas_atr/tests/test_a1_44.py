# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport

from destral.transaction import Transaction
from addons import get_module_resource
from destral.patch import PatchNewCursors


class Test44(TestSwitchingImport):

    def test_creation_a144(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '44', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.44.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            a144 = step_obj.browse(cursor, uid, step_id)
            a1_44 = sw_obj.browse(cursor, uid, a144.sw_id.id)

            participant_obj = self.openerp.pool.get('giscemisc.participant')
            participant_partner = participant_obj.get_from_partner(cursor, uid, a1_44.partner_id.id)
            self.assertEqual(a1_44.proces_id.name, '44')
            self.assertEqual(a1_44.step_id.name, 'a1')
            self.assertEqual(participant_partner.codi_sifco, '1234')
            self.assertEqual(a1_44.company_id.codi_sifco, '4321')
            self.assertEqual(a1_44.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(a144.sourcetype, '06')
            self.assertEqual(a144.operationtype, 'A10011')
            self.assertEqual(a144.lastname, a1_44.cups_polissa_id.titular.name)
            self.assertEqual(a144.telephone1, a1_44.cups_polissa_id.direccio_pagament.phone)
            self.assertEqual(a144.sw_id.additional_info, u'A10011: Cierre por falta de pago. ')

    def test_export_a144_corte(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '44', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.44.a1')
            register_doc_obj = self.openerp.pool.get('giscegas.atr.document')

            docs_vals_1 = {
                'doctype': '02',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-02',
                'extrainfo': '404pagenotfound',
            }
            created_doc_1 = register_doc_obj.create(cursor, uid, docs_vals_1)
            docs_vals_2 = {
                'doctype': '01',
                'url': 'http://www.gasalmatalas.com',
                'date': '2018-05-03',
                'extrainfo': '404pagenotfound',
            }
            created_doc_2 = register_doc_obj.create(cursor, uid, docs_vals_2)

            a144 = step_obj.browse(cursor, uid, step_id)
            a144.write({
                'extrainfo': 'Aqui hi van els comentaris.',
                'firstname': 'Gas',
                'lastname': 'Al',
                'secondname': 'Matalas',
                'telephone1': '555123456',
                'document_ids': [(6, 0, [created_doc_1, created_doc_2])],
            })
            a144.header_id.write({'date_created': a144.header_id.date_created})
            a144 = step_obj.browse(cursor, uid, step_id)
            self.assertEqual(
                a144.sw_id.additional_info,
                u'A10011: Cierre por falta de pago. '
            )

            a144_01_generat = a144.generar_xml()[1]
            a144_01_generat = a144_01_generat.replace(' ', '').replace('\n', '')

            a144_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '44_a1_A10011.xml'
            )
            with open(a144_01_xml_path, 'r') as f:
                a144_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a144.sw_id.codi_sollicitud
                data_emissio = a144.header_id.date_created.split(" ")[0]
                hora_emissio = a144.header_id.date_created.split(" ")[1]
                a144_01_xml = a144_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a144_01_xml = a144_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a144_01_xml = a144_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a144_01_xml = a144_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a144_01_xml = a144_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a144_01_generat, a144_01_xml)

    def test_export_a144_lectura_aportada(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '44', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.44.a1')

            a144 = step_obj.browse(cursor, uid, step_id)
            a144.write({
                'lastname': 'Camptocamp',
                'telephone1': '555123456',
                'operationtype': 'A20001',
                'description': 'Lectura facilitada',
                'readingdate': '2018-01-01',
                'readingvalue': 100.0,
            })
            a144.header_id.write({'date_created': a144.header_id.date_created})
            a144 = step_obj.browse(cursor, uid, step_id)
            self.assertEqual(
                a144.sw_id.additional_info,
                u'A20001: Lectura facilitada. '
            )

            a144_01_generat = a144.generar_xml()[1]
            a144_01_generat = a144_01_generat.replace(' ', '').replace('\n', '')

            a144_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '44_a1_A20001.xml'
            )
            with open(a144_01_xml_path, 'r') as f:
                a144_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a144.sw_id.codi_sollicitud
                data_emissio = a144.header_id.date_created.split(" ")[0]
                hora_emissio = a144.header_id.date_created.split(" ")[1]
                a144_01_xml = a144_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a144_01_xml = a144_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a144_01_xml = a144_01_xml.replace(
                    "<communicationshour>12:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a144_01_xml = a144_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a144_01_xml = a144_01_xml.replace(
                    "<reqhour>12:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a144_01_generat, a144_01_xml)

    def test_import_a2_44(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '44_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.44.a1')
            # header_obj = self.pool.get('giscegas.atr.step.header')
            # rebuig_obj = self.pool.get('giscegas.atr.rebuig')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '44', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '44_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '44'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.operationtype, 'A10011')
            self.assertEqual(a2.srcode, '0015')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertFalse(a2.rebuig)
            self.assertFalse(cas_atr.rebuig)
            self.assertEqual(
                cas_atr.additional_info,
                u'A10011: Cierre por falta de pago. '
            )

    def test_reject_import_a2_44(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '44_a2_rej.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.44.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '44', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '44_a1_02.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '44'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '05')
            self.assertEqual(a2.resultdesc, 'Rechazada')
            self.assertEqual(a2.operationtype, 'A10011')
            self.assertEqual(a2.srcode, '0015')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertTrue(a2.rebuig)
            self.assertTrue(cas_atr.rebuig)
            self.assertEqual(a2.rebuig_ids[0].motiu_rebuig.name, 'R34')
            self.assertEqual(a2.rebuig_ids[0].desc_rebuig, 'No existe contrato de ATR en vigor.')
            self.assertEqual(
                cas_atr.additional_info,
                u'A10011: Cierre por falta de pago. Rechazo: No existe contrato de ATR en vigor.'
            )

    def test_import_a3_44(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '44_a2.xml'
        )
        a3_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '44_a3.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.44.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '44', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '05_a2.xml')

            a3_xml = a3_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a3_xml, '44_a3.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '44'),
                ('step_id.name', '=', 'a3'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3 = sw_obj.get_pas(cursor, uid, cas_atr)

            doc_ids = a3.document_ids
            counter_ids = a3.counter_ids

            counter_test = counter_ids[0]
            self.assertEqual(counter_test.countermodel, 'marca1')
            self.assertEqual(counter_test.countertype, 'tipo1')
            self.assertEqual(counter_test.counternumber, 'B123456')
            self.assertEqual(counter_test.counterproperty, '04')
            self.assertEqual(counter_test.reallecture, 2000)
            self.assertEqual(counter_test.counterpressure, 54321.123)

            counter_test = counter_ids[1]
            self.assertEqual(counter_test.countermodel, 'marca2')
            self.assertEqual(counter_test.countertype, 'tipo2')
            self.assertEqual(counter_test.counternumber, 'C123456')
            self.assertEqual(counter_test.counterproperty, '06')
            self.assertEqual(counter_test.reallecture, 3000)
            self.assertEqual(counter_test.counterpressure, 13245.321)

            doc_test = doc_ids[0]
            self.assertEqual(doc_test.date, '2018-01-01')
            self.assertEqual(doc_test.doctype, '01')
            self.assertEqual(doc_test.url, 'http://gasalmatalas.com')
            self.assertFalse(doc_test.extrainfo)

            doc_test = doc_ids[1]
            self.assertEqual(doc_test.date, '2018-01-01')
            self.assertEqual(doc_test.doctype, '02')
            self.assertEqual(doc_test.url, 'http://gasalmatalas.com')
            self.assertEqual(doc_test.extrainfo, 'Comments')

            self.assertEqual(a3.reqcode, '0123456789')
            self.assertEqual(a3.operationtype, 'A10011')
            self.assertEqual(a3.result, '09')
            self.assertEqual(a3.resultdesc, 'Realizada')
            self.assertEqual(a3.activationtype, '001')
            self.assertEqual(a3.activationtypedesc, 'Realizada puesta en servicio')
            self.assertEqual(a3.closingtype, '001')
            self.assertEqual(a3.closingtypedesc, u'La Actualización de Datos se ha realizado correctamente')
            self.assertEqual(a3.reqdescription, u'Desc')
            self.assertEqual(a3.interventiondate, u'2016-01-01 09:00:00')
            self.assertEqual(a3.resultinspection, u'01')
            self.assertEqual(a3.resultinspectiondesc, u'Correcto')
            self.assertEqual(a3.operationnum, u'9874')
            self.assertEqual(a3.visitnumber, 0)
            self.assertEqual(a3.counterchange, u'S')
            self.assertEqual(a3.removallecture, 56)
            self.assertEqual(a3.supplystatus, u'05')
            self.assertEqual(a3.supplystatusdesc, u'CORTE POR FALTA DE PAGO')
            self.assertEqual(a3.servicestatus, u'00')
            self.assertEqual(a3.servicestatusdesc, u'EN SERVICIO')
            self.assertEqual(a3.extrainfo, u'Coment')
            self.assertEqual(a3.conceptnumber, 2)
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')

    def test_import_a4_44(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '44_a2.xml'
        )
        a4_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '44_a4.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a4_xml_path, 'r') as f:
            a4_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.44.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '44', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '44_a2.xml')

            a4_xml = a4_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a4_xml = a4_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a4_xml = a4_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a4_xml = a4_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a4_xml = a4_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a4_xml, '44_a4.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '44'),
                ('step_id.name', '=', 'a4'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a4 = sw_obj.get_pas(cursor, uid, cas_atr)

            doc_ids = a4.document_ids
            counter_ids = a4.counter_ids

            counter_test = counter_ids[0]
            self.assertEqual(counter_test.countermodel, 'marca1')
            self.assertEqual(counter_test.countertype, 'tipo1')
            self.assertEqual(counter_test.counternumber, 'B123456')
            self.assertEqual(counter_test.counterproperty, '04')
            self.assertEqual(counter_test.reallecture, 2000)
            self.assertEqual(counter_test.counterpressure, 54321.123)

            counter_test = counter_ids[1]
            self.assertEqual(counter_test.countermodel, 'marca2')
            self.assertEqual(counter_test.countertype, 'tipo2')
            self.assertEqual(counter_test.counternumber, 'C123456')
            self.assertEqual(counter_test.counterproperty, '06')
            self.assertEqual(counter_test.reallecture, 3000)
            self.assertEqual(counter_test.counterpressure, 13245.321)

            doc_test = doc_ids[0]
            self.assertEqual(doc_test.date, '2018-01-01')
            self.assertEqual(doc_test.doctype, '01')
            self.assertEqual(doc_test.url, 'http://gasalmatalas.com')
            self.assertFalse(doc_test.extrainfo)

            doc_test = doc_ids[1]
            self.assertEqual(doc_test.date, '2018-01-01')
            self.assertEqual(doc_test.doctype, '02')
            self.assertEqual(doc_test.url, 'http://gasalmatalas.com')
            self.assertEqual(doc_test.extrainfo, 'Comments')

            self.assertEqual(a4.reqcode, '0123456789')
            self.assertEqual(a4.operationtype, 'A10001')
            self.assertEqual(a4.result, '13')
            self.assertEqual(a4.resultdesc, 'No Realizada')
            self.assertTrue(a4.rebuig)
            self.assertTrue(cas_atr.rebuig)
            self.assertEqual(a4.rebuig_ids[0].motiu_rebuig.name, 'R01')
            self.assertEqual(a4.rebuig_ids[0].desc_rebuig, 'Cliente suministrado desde planta satélite.')
            self.assertEqual(a4.reqdescription, u'Desc')
            self.assertEqual(a4.closingtype, '001')
            self.assertEqual(a4.closingtypedesc, u'La Actualización de Datos se ha realizado correctamente')
            self.assertEqual(a4.interventiondate, u'2016-01-01 09:00:00')
            self.assertEqual(a4.resultinspection, u'01')
            self.assertEqual(a4.resultinspectiondesc, u'Correcto')
            self.assertEqual(a4.operationnum, u'9874')
            self.assertEqual(a4.visitnumber, 0)
            self.assertEqual(a4.counterchange, u'S')
            self.assertEqual(a4.removallecture, 56)
            self.assertEqual(a4.supplystatus, u'05')
            self.assertEqual(a4.supplystatusdesc, u'CORTE POR FALTA DE PAGO')
            self.assertEqual(a4.servicestatus, u'00')
            self.assertEqual(a4.servicestatusdesc, u'EN SERVICIO')
            self.assertEqual(a4.extrainfo, u'Coment')
            self.assertEqual(a4.conceptnumber, 2)
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')

    def test_activar_A1_44_a2_close_case_if_autolectura(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '44_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.44.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '44', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)
            a1.write({'operationtype': 'A20001'})

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            # A20001 indicate is autolectura -> condition to close case
            a2_xml = a2_xml.replace(
                "<operationtype>A10011</operationtype>",
                "<operationtype>A20001</operationtype>"
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '44_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '44'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, cas_atr)

            cas_atr = sw_obj.browse(cursor, uid, res[0])

            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.operationtype, 'A20001')
            self.assertEqual(a2.srcode, '0015')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(cas_atr.state, 'done')
            self.assertFalse(a2.rebuig)
            self.assertFalse(cas_atr.rebuig)
            self.assertTrue(
                cas_atr.additional_info.startswith(u'A20001:')
            )

    def test_activar_A1_44_a2_dont_close_case_if_not_autolectura(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '44_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.44.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '44', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)
            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '44_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '44'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, cas_atr)

            cas_atr = sw_obj.browse(cursor, uid, res[0])

            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.operationtype, 'A10011')
            self.assertEqual(a2.srcode, '0015')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(cas_atr.state, 'open')
            self.assertFalse(a2.rebuig)
            self.assertFalse(cas_atr.rebuig)
            self.assertTrue(
                cas_atr.additional_info.startswith(u'A10011:')
            )

    def test_activar_A1_44_a2_dont_close_case_if_rebuig_and_instruct_user(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '44_a2_rej.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.44.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '44', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)
            a1.write({'operationtype': 'A20001'})

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            # A20001 indicate is autolectura -> condition to close case
            a2_xml = a2_xml.replace(
                "<operationtype>A10011</operationtype>",
                "<operationtype>A20001</operationtype>"
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '44_a2_rej.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '44'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            with PatchNewCursors():
                pres = sw_obj.activa_cas_atr(cursor, uid, cas_atr)

            self.assertIn(
                u'ha quedado abierto ya que es un rechazo - Gestionar manualmente',
                pres[1]
            )

            cas_atr = sw_obj.browse(cursor, uid, res[0])

            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '05')
            self.assertEqual(a2.resultdesc, 'Rechazada')
            self.assertEqual(a2.operationtype, 'A20001')
            self.assertEqual(a2.srcode, '0015')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(cas_atr.state, 'open')
            self.assertTrue(a2.rebuig)
            self.assertTrue(cas_atr.rebuig)
            self.assertTrue(
                cas_atr.additional_info.startswith(u'A20001:')
            )
