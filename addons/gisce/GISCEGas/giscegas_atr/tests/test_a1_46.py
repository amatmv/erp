# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime
import calendar


class Test46(TestSwitchingImport):

    def test_creation_a146(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '46', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.46.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            a146 = step_obj.browse(cursor, uid, step_id)
            a1_46 = sw_obj.browse(cursor, uid, a146.sw_id.id)

            participant_obj = self.openerp.pool.get('giscemisc.participant')
            participant_partner = participant_obj.get_from_partner(cursor, uid, a1_46.partner_id.id)
            self.assertEqual(a1_46.proces_id.name, '46')
            self.assertEqual(a1_46.step_id.name, 'a1')
            self.assertEqual(participant_partner.codi_sifco, '1234')
            self.assertEqual(a1_46.company_id.codi_sifco, '4321')
            self.assertEqual(a1_46.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(a146.operationtype, 'A10011')
            self.assertEqual(
                a1_46.additional_info,
                u'Anulación para el caso [No encontrado]'
            )

    def test_export_a146(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '46', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.46.a1')

            a146 = step_obj.browse(cursor, uid, step_id)
            a146.write({
                'extrainfo': 'comentarios extras',
                'operationtype': 'A10002',
            })

            a146_01_generat = a146.generar_xml()[1]
            a146_01_generat = a146_01_generat.replace(' ', '').replace('\n', '')

            a146_01_xml_path = get_module_resource(
                'giscegas_atr', 'tests', 'fixtures', '46_a1.xml'
            )
            with open(a146_01_xml_path, 'r') as f:
                a146_01_xml = f.read().replace('\n', '').replace(' ', '')
                codi_sol = a146.sw_id.codi_sollicitud
                data_emissio = a146.header_id.date_created.split(" ")[0]
                hora_emissio = a146.header_id.date_created.split(" ")[1]
                a146_01_xml = a146_01_xml.replace(
                    "<comreferencenum>000123456789</comreferencenum>",
                    "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
                )
                a146_01_xml = a146_01_xml.replace(
                    "<communicationsdate>2018-05-01</communicationsdate>",
                    "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
                )
                a146_01_xml = a146_01_xml.replace(
                    "<communicationshour>13:00:00</communicationshour>",
                    "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
                )
                a146_01_xml = a146_01_xml.replace(
                    "<reqdate>2018-05-01</reqdate>",
                    "<reqdate>{0}</reqdate>".format(data_emissio)
                )
                a146_01_xml = a146_01_xml.replace(
                    "<reqhour>13:00:00</reqhour>",
                    "<reqhour>{0}</reqhour>".format(hora_emissio)
                )

            self.assertEqual(a146_01_generat, a146_01_xml)

    def test_import_a2_46(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '46_a2.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.46.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '46', 'a1'
            )

            a2 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a2.sw_id.codi_sollicitud
            data_emissio = a2.date_created.split(" ")[0]
            hora_emissio = a2.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '46_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '46'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '01')
            self.assertEqual(a2.resultdesc, 'Aceptada')
            self.assertEqual(a2.operationtype, 'A10002')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(
                cas_atr.additional_info,
                u'Anulación para el caso [No encontrado]'.format(cas_atr.codi_sollicitud)
            )

    def test_reject_import_a2_46(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '46_a2_rej.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.46.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '46', 'a1'
            )

            a2 = step_obj.browse(cursor, uid, step_id)

            # Change the dates & id from the XML.
            codi_sol = a2.sw_id.codi_sollicitud
            data_emissio = a2.date_created.split(" ")[0]
            hora_emissio = a2.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '46_a2.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '46'),
                ('step_id.name', '=', 'a2'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a2 = sw_obj.get_pas(cursor, uid, cas_atr)

            self.assertTrue(cas_atr.rebuig)
            self.assertTrue(a2.rebuig)
            self.assertEqual(a2.reqcode, '0123456789')
            self.assertEqual(a2.result, '05')
            self.assertEqual(a2.resultdesc, 'Denegada')
            self.assertEqual(cas_atr.cups_input, 'ES1234000000000001JN0F')
            self.assertEqual(a2.operationtype, 'A10002')
            self.assertEqual(a2.reqcodetoanul, '0123456790')
            self.assertEqual(a2.extrainfo, 'comentarios extras')
            self.assertEqual(a2.rebuig_ids[0].motiu_rebuig.name, 'R32')
            self.assertEqual(a2.rebuig_ids[0].desc_rebuig, 'Fecha efecto solicitada anterior al dia actual.')
            self.assertEqual(
                cas_atr.additional_info,
                u'Anulación para el caso [No encontrado]. Rechazo: Fecha efecto solicitada anterior al dia actual.'
            )
