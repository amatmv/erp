# -*- coding: utf-8 -*-
import unittest

from destral import testing
from destral.transaction import Transaction
from expects import expect, raise_error
from osv.orm import except_orm
from addons import get_module_resource
import time
from tools.misc import cache
from workdays import workday
from datetime import datetime
from destral.patch import PatchNewCursors
from osv import osv


class GiscedataGas(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    def importar_xml(self, cursor, uid, data, fname, context=None):
        if not context:
            context = {}
        with PatchNewCursors():
            res = super(GiscedataGas, self).importar_xml(cursor, uid, data, fname, context)
        return res

GiscedataGas()


class BaseTest(testing.OOTestCase):
    ''' Test Polissa Tarifa '''

    require_demo_data = True

    def test_get_old_company_steps_returns_correct_steps(self):
        sw_proc_obj = self.openerp.pool.get('giscegas.atr.proces')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            assert sw_proc_obj.get_old_company_steps(
                cursor, uid, '41'
            ) == ['a2s', 'a3s', 'a4s']

    def test_all_rebuig_codes(self):
        rebuig_obj = self.openerp.pool.get('giscegas.atr.motiu.rebuig')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            user = txn.user
            llista_motius = rebuig_obj.get_all_motius(cursor, user, '41')
            llista_no_hi_son = []
            self.assertEqual(len(llista_motius), 32)
            for i in llista_no_hi_son:
                self.assertNotIn(i, llista_motius)


class TestSwitchingImport(testing.OOTestCase):

    def get_contract_id(self, txn, xml_id='polissa_gas_0001'):
        uid = txn.user
        cursor = txn.cursor
        imd_obj = self.openerp.pool.get('ir.model.data')

        return imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', xml_id
        )[1]

    def get_invoice_id(self, txn, xml_id='factura_0001'):
        uid = txn.user
        cursor = txn.cursor
        imd_obj = self.openerp.pool.get('ir.model.data')

        return imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', xml_id
        )[1]

    def switch(self, txn, where, other_id_name='res_partner_agrolait'):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner_obj = self.openerp.pool.get('res.partner')
        cups_obj = self.openerp.pool.get('giscegas.cups.ps')
        contract_obj = self.openerp.pool.get('giscegas.polissa')
        part_obj = self.openerp.pool.get('giscemisc.participant')
        partner_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'main_partner'
        )[1]
        part_partner_id = part_obj.get_id_from_partner(
            cursor, uid, partner_id
        )
        other_id = imd_obj.get_object_reference(
            cursor, uid, 'base',  other_id_name
        )[1]
        part_other_id = part_obj.get_id_from_partner(
            cursor, uid, other_id
        )
        another_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_c2c'
        )[1]
        part_another_id = part_obj.get_id_from_partner(
            cursor, uid, another_id
        )
        codes = {'distri': '1234', 'comer': '4321'}
        part_obj.write(cursor, uid, [part_partner_id], {
            'codi_sifco': codes.pop(where)
        })
        part_obj.write(cursor, uid, [part_other_id], {
            'codi_sifco': codes.values()[0]
        })
        part_obj.write(cursor, uid, [part_another_id], {
            'codi_sifco': '5555'
        })
        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_cups', 'giscegas_cups_01'
        )[1]
        distri_ids = {'distri': partner_id, 'comer': other_id}
        cups_obj.write(cursor, uid, [cups_id], {
            'distribuidora_id': distri_ids[where]
        })
        self.update_titular_vat(txn)
        cache.clean_caches_for_db(cursor.dbname)

    def activar_polissa_CUPS(self, txn, context=None):
        if context is None:
            context = {}
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscegas.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        if context.get("extra_vals"):
            polissa_obj.write(cursor, uid, polissa_id, context.get("extra_vals"))
        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def baixa_tall_polissa_CUPS(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        compt_obj = self.openerp.pool.get('giscegas.lectures.comptador')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]

        polissa_obj.write(cursor, uid, [polissa_id], {
            'renovacio_auto': True,
            'data_baixa': time.strftime('%Y-%m-%d')
        })

        compt_id = polissa_obj.read(
            cursor, uid, polissa_id, ['comptadors']
        )['comptadors'][0]

        compt_obj.write(cursor, uid, [compt_id], {
            'data_baixa': time.strftime('%Y-%m-%d'),
            'active': False
        })

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'impagament', 'tallar', 'baixa'
        ])

    def update_polissa_distri(self, txn, pol_id=False):
        """
        Sets the distribuidora_id field in contract as the same of related cups
        """
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        pol_obj = self.openerp.pool.get('giscegas.polissa')
        cups_obj = self.openerp.pool.get('giscegas.cups.ps')
        if not pol_id:
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
        else:
            contract_id = pol_id
        cups_id = pol_obj.read(cursor, uid, contract_id, ['cups'])['cups'][0]
        distri_id = cups_obj.read(
            cursor, uid, cups_id, ['distribuidora_id']
        )['distribuidora_id'][0]
        pol_obj.write(cursor, uid, contract_id, {'distribuidora': distri_id})

    def update_titular_vat(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        pol_obj = self.openerp.pool.get('giscegas.polissa')
        part_obj = self.openerp.pool.get('res.partner')

        contract_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        titular_id = pol_obj.read(cursor, uid, contract_id, ['titular'])['titular'][0]
        vat = part_obj.read(
            cursor, uid, titular_id, ['vat']
        )['vat']
        if not vat:
            part_obj.write(cursor, uid, titular_id, {'vat': "ES00000000T"})

    def change_distri_code(self, new_ref, txn, polissa_name='polissa_gas_0001'):
        """
            Sets the ref of the distri related with the CUPS of
            polissa_name to 'new_ref'
        """
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        pol_obj = self.openerp.pool.get('giscegas.polissa')
        cups_obj = self.openerp.pool.get('giscegas.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')

        contract_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', polissa_name
        )[1]
        cups_id = pol_obj.read(cursor, uid, contract_id, ['cups'])['cups'][0]
        distri_id = cups_obj.read(
            cursor, uid, cups_id, ['distribuidora_id']
        )['distribuidora_id'][0]
        participant_obj = self.openerp.pool.get('giscemisc.participant')
        participant_partner = participant_obj.get_from_partner(cursor, uid, distri_id)
        participant_partner.write(cursor, uid, distri_id, {'codi_sifco': new_ref})

    def change_polissa_comer(self, txn, pol_id=False):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')
        new_comer_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_c2c'
        )[1]
        pol_obj = self.openerp.pool.get('giscegas.polissa')
        if not pol_id:
            pol_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
        pol_obj.write(cursor, uid, [pol_id], {
            'comercialitzadora': new_comer_id
        })

    def crear_modcon(self, txn, potencia, ini, fi):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})
        wz_crear_mc_obj = pool.get('giscegas.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def set_nova_tarifa(self, txn, tarif_name):
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = txn.cursor
        uid = txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        tarif_obj = self.openerp.pool.get('giscegas.polissa.tarifa')
        tarif_id = tarif_obj.search(cursor, uid, [('name', '=', tarif_name)])
        polissa_obj.write(cursor, uid, [polissa_id], {
            'tarifa': tarif_id[0],
        })

    def create_case_and_step(
            self, cursor, uid, contract_id, proces_name, step_name, context=None
    ):
        """
        Creates case and step
        :param proces_name:
        :param step_name:
        :param context:
        :return:
        """
        if context is None:
            context = {'proces_name': proces_name}

        sw_obj = self.openerp.pool.get('giscegas.atr')

        swproc_obj = self.openerp.pool.get('giscegas.atr.proces')
        swpas_obj = self.openerp.pool.get('giscegas.atr.step')
        swinfo_obj = self.openerp.pool.get(
            'giscegas.atr.step.info'
        )
        contract_obj = self.openerp.pool.get('giscegas.polissa')

        proces_id = swproc_obj.search(
            cursor, uid, [('name', '=', proces_name)]
        )[0]

        sw_params = {
            'proces_id': proces_id,
            'cups_polissa_id': contract_id,
        }

        vals = sw_obj.onchange_polissa_id(
            cursor, uid, [], contract_id, None, context=context
        )

        sw_params.update(vals['value'])
        # si no tenim ref_contracte, ens l'inventem (de moment)
        if not sw_params.get('ref_contracte', False):
            sw_params['ref_contracte'] = '111111111'

        sw_id = sw_obj.create(cursor, uid, sw_params)
        # TODO: tambe per al proces 48?
        if proces_name in ['41']:
            out_retail = contract_obj.read(
                cursor, uid, contract_id, ['comercialitzadora']
            )['comercialitzadora'][0]
            out_retail = self.openerp.pool.get(
                "giscemisc.participant"
            ).get_id_from_partner(cursor, uid, out_retail)
            sw_obj.write(cursor, uid, sw_id, {'comer_sortint_id': out_retail})

        # creeem el pas
        pas_id = swpas_obj.get_step(cursor, uid, step_name, proces_name)
        #Creant info ja crea automaticament tota la info del pas
        info_vals = {
            'sw_id': sw_id,
            'proces_id': proces_id,
            'step_id': pas_id,
        }
        info_id = swinfo_obj.create(cursor, uid, info_vals, context=context)
        info = swinfo_obj.browse(cursor, uid, info_id)
        model_obj, model_id = info.pas_id.split(',')

        return int(model_id)

    def create_step(self, cursor, uid, sw, proces_name, step_name, context=None):
        if not context:
            context = {}
        sw_obj = self.openerp.pool.get('giscegas.atr')
        pas_obj = self.openerp.pool.get(
            'giscegas.atr.{0}.{1}'.format(proces_name.lower(), step_name)
        )

        pas_id = pas_obj.dummy_create(cursor, uid, sw.id, context)
        return sw_obj.create_step(cursor, uid, sw.id, step_name, pas_id, context=context)


class TestSwitchingFeatures(TestSwitchingImport):

    def test_get_step_id(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscegas.atr')
            self.switch(txn, 'comer')
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            # Generema1_41 i es crearà objecte switching i pas A1
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )
            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')
            a141 = step_obj.browse(cursor, uid, step_id)
            sw_id = a141.sw_id.id

            # Comprovem que sw.step_id retorna el pas a1
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.assertEqual(sw.step_id.name, 'a1')

            # Creem el pas 03 amb una data posterior a pas a3
            self.create_step(cursor, uid, sw, '41', 'a3')
            # Comprovem que sw.step_id retorna el pas a3
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.assertEqual(sw.step_id.name, 'a3')

            # Creem el pas 02 amb una data posterior a pas 03
            self.create_step(cursor, uid, sw, '41', 'a2')
            # Comprovem que sw.step_id retorna el pas 02
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.assertEqual(sw.step_id.name, 'a2')

    def test_a1_41_from_active_contract_fails(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            contract_obj = self.openerp.pool.get('giscegas.polissa')

            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            contract_id = self.get_contract_id(txn)

            contract_data = contract_obj.read(
                cursor, uid, contract_id, ['name', 'state']
            )
            res = contract_obj.crear_cas_atr(
                cursor, uid, contract_id, proces='41'
            )
            self.assertEqual(res[0], '(41) {0}'.format(contract_data['name']))
            self.assertEqual(
                res[1],
                u'Contrato en estado {0}, la saltamos'.format(contract_data['state'])
            )
            self.assertEqual(res[2], False)

    def test_creation_from_polissa_conf_var(self):
        with Transaction().start(self.database) as txn:
            polissa_obj = self.openerp.pool.get('giscegas.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            cursor = txn.cursor
            uid = txn.user

            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]

            # Change config var for case state in creation from polissa to
            # set new cases to 'draft'
            conf_obj = self.openerp.pool.get('res.config')
            var_id = conf_obj.search(
                cursor, uid, [('name', '=', 'atr_gas_from_polissa_case_state_open')]
            )[0]
            conf_obj.write(cursor, uid, var_id, {'value': '0'})
            context = {'proces': '41'}
            res = polissa_obj.crear_cas_atr(cursor, uid, polissa_id, context=context)
            sw_id = res[2]
            sw_info = sw_obj.read(cursor, uid, sw_id, ['state'])
            self.assertEqual(sw_info['state'], 'draft')

    def test_baixa_polissa_close_cases(self):
        with Transaction().start(self.database) as txn:
            polissa_obj = self.openerp.pool.get('giscegas.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            cursor = txn.cursor
            uid = txn.user

            self.switch(txn, 'comer')
            self.update_polissa_distri(txn)

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]

            # Change config var
            conf_obj = self.openerp.pool.get('res.config')
            var_id = conf_obj.search(
                cursor, uid, [('name', '=', 'atr_gas_baixa_contract_close_cases')]
            )[0]
            conf_obj.write(cursor, uid, var_id, {'value': "['41']"})
            context = {'proces': '41'}
            res = polissa_obj.crear_cas_atr(cursor, uid, polissa_id,
                                            context=context)
            sw_id = res[2]

            # Baixa polissa
            polissa_obj.wkf_baixa(cursor, uid, [polissa_id])
            # Check state
            sw_info = sw_obj.read(cursor, uid, sw_id, ['state'])
            self.assertEqual(sw_info['state'], 'done')

    def test_get_case_of_header(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )
            header_obj = self.openerp.pool.get('giscegas.atr.step.header')
            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')
            a141 = step_obj.browse(cursor, uid, step_id)
            sw = a141.sw_id

            headers = []
            # Obtenim el header del step 01
            step = sw.get_pas()
            s1 = step.id
            hid1 = step.header_id.id
            headers.append(hid1)

            # Creem el pas 02
            self.create_step(cursor, uid, sw, '41', 'a2')
            # Obtenim el header del step 02
            step = sw.get_pas()
            s2 = step.id
            hid2 = step.header_id.id
            headers.append(hid2)

            # Creem el pas 03
            self.create_step(cursor, uid, sw, '41', 'a3')
            # Obtenim el header del step 03
            step = sw.get_pas()
            s3 = step.id
            hid3 = step.header_id.id
            headers.append(hid3)

            # Comprovem que s'ens retornin el step_id i models adequats i que el
            # header inventat no esta al diccionari
            res = header_obj.get_case_of_header(cursor, uid, headers)
            info = res.get(hid1)
            self.assertEqual(s1, info[0])
            self.assertEqual("giscegas.atr.41.a1", info[1])
            info = res.get(hid2)
            self.assertEqual(s2, info[0])
            self.assertEqual("giscegas.atr.41.a2", info[1])
            info = res.get(hid3)
            self.assertEqual(s3, info[0])
            self.assertEqual("giscegas.atr.41.a3", info[1])

    def test_unlink_pas(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            a102 = step_obj.browse(cursor, uid, step_id)
            sw = sw_obj.browse(cursor, uid, a102.sw_id.id)

            # Si eliminem el pas directament tambe s'hauria d'eliminar el
            # objecte step_info que el te com a pas_id
            self.assertEqual(len(sw.step_ids), 1)
            self.assertTrue(sw.step_id)
            sw.get_pas().unlink()
            sw = sw.browse()[0]
            self.assertEqual(len(sw.step_ids), 0)
            self.assertFalse(sw.step_id)

    def test_unlink_step_info(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')

            a102 = step_obj.browse(cursor, uid, step_id)
            sw = sw_obj.browse(cursor, uid, a102.sw_id.id)

            # Si eliminem el pas directament tambe s'hauria d'eliminar el
            # objecte step_info que el te com a pas_id
            self.assertEqual(len(sw.step_ids), 1)
            self.assertTrue(sw.step_id)
            sw.step_ids[0].unlink()
            sw = sw.browse()[0]
            self.assertEqual(len(sw.step_ids), 0)
            self.assertFalse(sw.step_id)


class TestSwitchingNotificationMail(TestSwitchingImport):
    _template_base_name = 'notification_atr_{}_{}'
    _model_base_name = 'giscegas.atr.{}.{}'

    def check_rebuig_step(self, process, step):
        if step == 'a2':
            return True
        if process in ['41']:
            if step in ['a4', 'a4s']:
                return True
        return False

    def create_init_step(self, process, step):
        pre_step = 'a1'
        pool = self.openerp.pool
        if self.txn is None:
            raise ValueError("Can't create previous step without Transaction")
        if pre_step == 'a1':
            self.switch(self.txn, 'comer', other_id_name='res_partner_asus')
        uid = self.txn.user
        cursor = self.txn.cursor
        contract_id = self.get_contract_id(self.txn)
        # Check for previous step emisor

        step_id = self.create_case_and_step(
            cursor, uid, contract_id, str.upper(process), pre_step
        )

        model = self._model_base_name.format(str.lower(process), pre_step)
        model_obj = pool.get(model)
        sw = model_obj.browse(cursor, uid, step_id).sw_id
        if pre_step == 'a1':
            self.switch(self.txn, 'distri')
        return sw

    def check_template_for_process_and_step(
            self, cursor, uid, contract_id, process, step_name, rebuig=False):
        pool = self.openerp.pool
        data_obj = pool.get('ir.model.data')

        rebuig_step = self.check_rebuig_step(process, step_name)
        template_name = self._template_base_name.format(
            str.upper(process), step_name
        )
        model = self._model_base_name.format(str.lower(process), step_name)
        try:
            # Try to create step without previous step
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, str.upper(process), step_name
            )
        except:
            # If create case and step fails, first create the case with a 01
            sw_case = self.create_init_step(process, step_name)
            self.create_step(cursor, uid, sw_case, process, step_name)
            step_id = sw_case.get_pas().id

        model_obj = pool.get(model)

        if rebuig_step:
            if rebuig:
                template_name += '_rebuig'
                model_obj.write(cursor, uid, [step_id], {
                    'rebuig': rebuig
                })
            else:
                template_name += '_acceptacio'

        step = model_obj.browse(cursor, uid, step_id)

        # Test template name getter
        sw_template_name = step.get_notification_mail_name()
        self.assertTrue(sw_template_name,
                        msg='Step {}-{} does not return template_name'.format(
                            process, step_name))
        self.assertEqual(
            sw_template_name[0],
            template_name,
            msg=(
                'Get Notification Mail from SW Step {}-{}'
                ' does not return a correct template name'.format(
                    process, step_name
                )
            )
        )

        # Get template resource directly
        # Must exist in DB a template in "giscegas_atr" module
        # With XML id name as "notification_atr_{ProcesName}_{StepName}
        # If rebuig step, append "_acceptacio" or "_rebuig"
        template = data_obj.get_object_reference(
            cursor, uid, 'giscegas_atr', template_name
        )[1]

        # Compare template got directly and the one got by step method
        assert step.header_id.get_notification_mail()[0] == template

        return step_id

    def check_default_notificacio_pendent(self, conf_var='all',
                                          case_name='41', step_name='a1',
                                          expected_notify=True):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            contract_id = self.get_contract_id(txn)
            sw_obj = pool.get('giscegas.atr')
            pas_model_name = 'giscegas.atr.{}.{}'.format(
                case_name, step_name).lower()
            pas_obj = pool.get(pas_model_name)
            config_obj = pool.get('res.config')
            sw_notify_steps_id = config_obj.search(
                cursor, uid,
                [('name', '=', 'atr_gas_mail_user_notification_on_activation')]
            )
            self.assertTrue(
                sw_notify_steps_id,
                'Config Var `atr_gas_mail_user_notification_on_activation`'
                ' not initialized!'
            )
            config_obj.write(
                cursor, uid, sw_notify_steps_id, {'value': conf_var}
            )

            # Create the sw step
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, case_name, step_name
            )
            pas = pas_obj.browse(cursor, uid, step_id)
            self.assertEqual(
                pas.notificacio_pendent,
                expected_notify,
                'NotificacioPendent on {}-{} should be {}'
                ' when conf_var is `{}`'.format(
                    case_name, step_name, expected_notify, conf_var
                )
            )

    def test_default_notificacio_pendent(self):

        # When conf_var = 'all', new steps should be 'notificacio_pendent'=True
        self.check_default_notificacio_pendent()

        # When conf_var = ['A3-01'],
        # - new A301 should have 'notificacio_pendent'=True
        # - new steps (not A301) should have 'notificacio_pendent'=False
        conf_var_value = "['41-a1']"
        self.check_default_notificacio_pendent(conf_var=conf_var_value)
