# -*- coding: utf-8 -*-
"""
PROCESO 04 - Solicitud de baja
--------------------------------------------------------------------------------

- PASO A1: Envio de solicitud de baja (comer -> distri).

- PASO A2 (sol. aceptada): Mensaje de aceptación de solicitud (distri -> comer).

- PASO A2 (sol. rechazada): Mensaje de rechazo de solicitud (distri -> comer).

- PASO A25: Mensaje unidireccional, no espera respuesta, informacion de los
            estados de trabajos en campo (distri -> comer).

- PASO A3: Comunicación de la activación de la baja (distri -> comer).

- PASO A4: Comunicación del rechazo de la baja (distri -> comer).
________________________________________________________________________________

TABLA MOTIVOS BAJA - Tabla 1.8.14-29 - GESTIONATR-->GAS-->TAULA_MOTIVO_BAJA
--------------------------------------------------------------------------------
- 01: A petición del cliente por cese de su actividad.
- 02: A solicitud del comercializador por impago del cliente.
- 03: A solicitud del comercializador por cese de actividad del mismo.(OUTDATED)
- 04: A solicitud del comercializador por problemas de seguridad derivados de
      inspección.
- 05: A solicitud del comercializador por fin de contrato de energía.(OUTDATED)
- 06: A solicitud del comercializador por otros motivos.(OUTDATED)
- 07: A petición del cliente por traslado.(OUTDATED)

"""


from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.defs_gas import *


from gestionatr.output.messages import sw_a1_04 as sw_04
from gestionatr.defs import SINO
from tools.translate import _
from datetime import datetime
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException
from giscegas_atr.utils import get_address_dict


class GiscegasAtrProces04(osv.osv):
    _name = 'giscegas.atr.proces'
    _inherit = 'giscegas.atr.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 initial steps depending on where we are '''

        if proces == '04':
            # if where == 'comer':
            return ['a1']

        return super(GiscegasAtrProces04,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        ''' returns emisor steps depending on where we are '''
        if proces == '04':
            if where == 'distri':
                return ['a2', 'a25', 'a3', 'a4']
            elif where == 'comer':
                return ['a1']

        return super(GiscegasAtrProces04,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == '04':
            return ['a2', 'a4']

        return super(GiscegasAtrProces04,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        if proces == '04':
            return []

        return super(GiscegasAtrProces04, self).get_old_company_steps(
            cursor, uid, proces, context
        )


GiscegasAtrProces04()


class GiscegasAtr04(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == '04':
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == 'a2':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                elif step_name in ('a3', 'a4'):
                    return True

        return super(GiscegasAtr04,
                     self).get_final(cursor, uid, sw, context=context)

GiscegasAtr04()


class GiscegasAtr04A1(osv.osv):
    """
    Classe paso A1:
      - Envio de solicitud de baja (comer -> distri).
    """

    _name = "giscegas.atr.04.a1"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a1'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '04', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(
            GiscegasAtr04A1, self).unlink(
            cursor, uid, ids, context=context
        )
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_04_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2']

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        # TODO
        raise Exception("Importación de 04 paso a1 no implementada")

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML 05, pas a1
        """
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id
        heading = pas.header_id.generar_xml(pas)
        a104 = sw_04.A104()

        vals_a104 = {}
        documenttype = pas.documenttype

        vals_a104.update({
            'comreferencenum': sw.codi_sollicitud,
            'reqdate': pas.header_id.date_created.split(" ")[0],
            'reqhour': pas.header_id.date_created.split(" ")[1],
            'nationality': pas.nationality,
            'documenttype': pas.documenttype,
            'documentnum': pas.documentnum,
            'cups': sw.cups_id.name[:20],
            'contactphonenumber': pas.contactphonenumber,
            'cancelreason': pas.cancelreason,
            'cancelhour': pas.cancelhour,
            'extrainfo': pas.extrainfo,
            'modeffectdate': pas.modeffectdate,
            'reqcanceldate': pas.reqcanceldate
        })

        # Si el tipo de documento es NIVA o Otros el campo es requerido
        if documenttype in ['07', '08']:
            vals_a104['titulartype'] = pas.titulartype

        a104.feed(vals_a104)
        msg = sw_04.MensajeA104()
        msg.feed({
            'heading': heading,
            'a104': a104,
        })
        msg.build_tree()
        fname = sw.generar_nom_xml(
            pas.emisor_id.codi_sifco,
            pas.receptor_id.codi_sifco,
            pas=self._nom_pas
        )
        return (fname, str(msg))

    def prepare_for_validation(self, cursor, uid, ids, vals, context=None):
        """Preparacio validacions del pas"""
        if not context:
            context = {}

        if not vals:
            return vals

        vals2 = vals.copy()
        part_obj = self.pool.get("res.partner")
        addr_obj = self.pool.get("res.partner.address")
        sw_obj = self.pool.get("giscegas.atr")

        if vals.get('owner'):
            vals2.pop('owner')
            titular = part_obj.browse(cursor, uid, vals.get('owner'))
            vat_info = sw_obj.get_vat_info(cursor, uid, titular.vat, False)
            vals2.update({
                'nationality': titular.country and titular.country.code,
                'documenttype': vat_info['document_type'],
                'documentnum': vat_info['vat'],
            })
        if vals.get('direccio_notificacio'):
            vals2.pop('direccio_notificacio')
            addr = addr_obj.browse(
                cursor, uid, vals.get('direccio_notificacio')
            )
            vals2.update({
                'contactphonenumber': addr.phone or addr.mobile
            })
        return vals2

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        """Validacions del pas"""

        mandatory_fields = ['nationality', 'documenttype', 'documentnum',
                            'cancelreason', 'modeffectdate',
                            'contactphonenumber'
                            ]

        fields_not_filled = []

        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)

        if vals2.get('documenttype', False) in ('07', '08'):
            mandatory_fields.append('titulartype')

        for field in mandatory_fields:
            if not vals2.get(field, False):
                fields_not_filled.append(field)

        if len(fields_not_filled):
            raise GiscegasAtrException(
                _("Faltan datos. Campos no completados."), fields_not_filled)

        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """Validacions del pas"""
        if not context:
            context = {}

        if not vals:
            return True

        self.config_step_validation(cursor, uid, ids, vals, context=context)
        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)
        self.write(cursor, uid, ids, vals2, context=context)
        return True

    def dummy_create(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        titular_vat = polissa.titular.vat
        participant_obj = self.pool.get("giscemisc.participant")
        participant = participant_obj.get_from_partner(
            cursor, uid, polissa.distribuidora.id
        )
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, participant.codi_sifco
        )

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        vals.update({
            'nationality': (
                polissa.titular.country and polissa.titular.country.code
            ),
            'documenttype': vat_info['document_type'],
            'documentnum': vat_info['vat'],
            'modeffectdate': '05',  # lo antes possible
            'cancelreason': '01',   # a peticion del cliente
            'contactphonenumber': vals.get('telephone1'),
            'reqcanceldate': today
        })

        return self.create(cursor, uid, vals, context=context)

    def get_additional_info(self, cursor, uid, step_id, context=None):
        data_02 = self.read(
            cursor, uid, step_id,
            ['reqcanceldate', 'modeffectdate', 'sw_id', 'cancelreason']
        )

        accio = data_02['modeffectdate']
        for elem in TAULA_MODELO_FECHA_EFECTO:
            if elem[0] == data_02['modeffectdate']:
                accio = elem[1]
                break

        data_acc = ''
        if data_02['modeffectdate']:
            data_acc = _(u"{0}").format(
                data_02['reqcanceldate']
            )

        sw = self.pool.get("giscegas.atr").browse(
            cursor, uid, data_02['sw_id'][0]
        )

        motivo_cancelacion = data_02['cancelreason']
        desc_motivo = ''

        if motivo_cancelacion:
            for motiv in TAULA_MOTIVO_BAJA:
                if motiv[0] == motivo_cancelacion:
                    desc_motivo = motiv[1]
                    break

        cancelacion_info = _(u"({0}): {1}").format(motivo_cancelacion, desc_motivo)

        return _(u"{0}. Baja: {1} {2}").format(cancelacion_info, accio, data_acc)

    _columns = {
        'header_id': fields.many2one(
            'giscegas.atr.step.header', 'Header', required=True
        ),
        # comreferencenum, req_date, reqhour las sacamos del header
        'titulartype': fields.selection(
            TAULA_TIPO_PERSONA, "Tipo de titular"
        ),
        'nationality': fields.selection(
            TAULA_NACIONALIDAD, "Nacionalidad documento"
        ),
        'documenttype': fields.selection(
            TAULA_TIPO_DOCUMENTO, "Tipo de documento de identificacion"
        ),
        'documentnum': fields.char("Nº Documento", size=20),
        'cancelreason': fields.selection(TAULA_MOTIVO_BAJA, "Motivo baja"),
        'modeffectdate': fields.selection(
            TAULA_MODELO_FECHA_EFECTO, "Modelo de Fecha Efecto"
        ),
        # reqcanceldate & cancelhour no son requeridos
        'reqcanceldate': fields.date("Fecha de Baja Solicitada"),
        'cancelhour': fields.datetime("Hora de Cierre"),
        'contactphonenumber': fields.char("Telefono del cliente", size=20),
        'extrainfo': fields.text("Observaciones de la Solicitud", size=400),

    }

    _defaults = {

    }

GiscegasAtr04A1()


class GiscegasAtr04A2(osv.osv):
    """ Classe para el paso a2:
        - PASO A2 (sol. aceptada): Aceptación de solicitud (distri -> comer).
        - PASO A2 (sol. rechazada): Rechazo de solicitud (distri -> comer).
    """

    _name = "giscegas.atr.04.a2"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a2'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(
            cursor, uid, ids, '04', self._nom_pas, context=context
        )

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr04A2, self).unlink(
            cursor, uid, ids, context=context
        )
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a25', 'a3', 'a4']

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_04_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(
            cursor, uid, sw_id, xml, context=context
        )

        # 01 aceptada, 09 realizada(realment no crec que arribi mai un 09 al A2)
        rebuig = xml.result not in ['01', '09']

        rebuig_ids = []
        if rebuig:
            rebuig_ids = rebuig_obj.create_from_xml(
                cursor, uid, sw_id, xml, context=context
            )

        vals.update({
            'reqcode': xml.reqcode,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'cancelreason': xml.cancelreason,
            'newmodeffectdate': xml.newmodeffectdate,
            'foreseentransferdate': xml.foreseentransferdate,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': rebuig,
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(
            cursor, uid, sw_id, self._nom_pas, pas_id, context=context
        )

        return pas_id

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Genera l'xml del pas"""
        if not context:
            context = {}

        raise Exception("Exportación del proceso 04 paso a2 no implementada")

        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id
        msg = sw_04.MensajeA104()

        msg.feed({})
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(
            pas.emisor_id.codi_sifco,
            pas.receptor_id.codi_sifco,
            pas=self._nom_pas
        )

        return (fname, str(msg))

    def dummy_create(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(
            cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'],
            context=context
        )
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.04.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antifa = ''
        if len(step01_id) > 0:
            antifa = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rechazo: {1}").format(antifa, step['motiu_rebuig'])
        else:
            return antifa

    # Additional functions #############################################
    def onchange_result(self, cursor, uid, ids, result_id, context=None):
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}
        vals = {}

        if result_id in ['01', '09']:
            vals['rebuig'] = False
        else:
            vals['rebuig'] = True

        res['value'] = vals
        return res

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    _columns = {
        'header_id': fields.many2one(
            'giscegas.atr.step.header', 'Header', required=True
        ),
        'reqcode': fields.char("Código Solicitud", size=10),
        'responsedate': fields.date('Fecha respuesta'),
        'responsehour': fields.datetime('Hora respuesta'),
        # 01 -> aceptada; 05 -> rechazada
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'nationality': fields.selection(
            TAULA_NACIONALIDAD, "Nacionalidad documento"
        ),
        'documenttype': fields.selection(
            TAULA_TIPO_DOCUMENTO, "Tipo de documento de identificacion"
        ),
        'documentnum': fields.char("Nº Documento", size=20),
        'cancelreason': fields.selection(TAULA_MOTIVO_BAJA, "Motivo baja"),
        'newmodeffectdate': fields.selection(
            TAULA_MODELO_FECHA_EFECTO_PREVISTA,
            "Modelo de fecha efecto a aplicar para la solicitud"
        ),
        'foreseentransferdate': fields.date("Fecha Prevista efecto solicitud"),

        'extrainfo': fields.text("Observaciones de la Validación", size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(
            _ff_motiu_rebuig, method=True, string=u"Motivo Rechazo",
            type='char', size=250, readonly=True
        ),


    }

    _defaults = {
        'rebuig': lambda *a: False,
    }

GiscegasAtr04A2()


class GiscegasAtr04A3(osv.osv):
    _name = "giscegas.atr.04.a3"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a3'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '04', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr04A3, self).unlink(
            cursor, uid, ids, context=context
        )
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        """A3 es un paso final"""
        return []

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_04_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        vals = header_obj.create_from_xml(
            cursor, uid, sw_id, xml, context=context
        )

        counter_ids = header_obj.create_from_xml_counters(
            cursor, uid, xml.counterlist, context=context
        )
        corrector_ids = header_obj.create_from_xml_correctors(
            cursor, uid, xml.correctorlist, context=context
        )

        vals.update({
            'reqcode': xml.reqcode,
            'titulartype': xml.titulartype,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'activationtype': xml.activationtype,
            'activationtypedesc': xml.activationtypedesc,
            'atrcode': xml.atrcode,
            'operationnum': xml.operationnum,
            'moreinformation': xml.moreinformation,
            'transfereffectivedate': xml.transfereffectivedate,
            'extrainfo': xml.extrainfo,
            'counter_ids': [(6, 0, counter_ids)],
            'corrector_ids': [(6, 0, corrector_ids)],
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Genera l'xml del pas"""
        # TODO
        raise Exception("Generación xml de 04 paso a3 no implementada")

    def dummy_create(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    # def get_additional_info(self, cursor, uid, step_id, context=None):

    _columns = {
        'header_id': fields.many2one(
            'giscegas.atr.step.header', 'Header', required=True
        ),
        'reqcode': fields.char("Código Solicitud", size=10),
        'responsedate': fields.date('Fecha respuesta'),
        'responsehour': fields.datetime('Hora respuesta'),
        'titulartype': fields.selection(
            TAULA_TIPO_PERSONA, "Tipo de titular"
        ),
        'nationality': fields.selection(
            TAULA_NACIONALIDAD, "Nacionalidad documento"
        ),
        'documenttype': fields.selection(
            TAULA_TIPO_DOCUMENTO, "Tipo de documento de identificacion"
        ),
        'documentnum': fields.char("Nº Documento", size=20),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'activationtype': fields.selection(TAULA_TIPO_DE_ACTIVACION,
                                           "Tipo de Activación"),
        'activationtypedesc': fields.char(
            "Descripción de Tipo de Activación", size=100),
        'atrcode': fields.char("Código Contrato ATR", size=24),
        'operationnum': fields.char("Número de Operación Generado", size=40),
        'moreinformation': fields.selection(
            SINO, 'Indicador de más Información de Detalle'
        ),
        'transfereffectivedate': fields.date(
            "Fecha Efecto Solicitud (inclusive)"
        ),
        'extrainfo': fields.text("Observaciones de la Realización", size=400),
    }

    _defaults = {

    }

GiscegasAtr04A3()


class GiscegasAtr04A4(osv.osv):
    _name = "giscegas.atr.04.a4"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a4'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '04', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr04A4, self).unlink(
            cursor, uid, ids, context=context
        )
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_04_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        vals = header_obj.create_from_xml(
            cursor, uid, sw_id, xml, context=context
        )

        rebuig_obj = self.pool.get('giscegas.atr.rebuig')
        rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                context=context)

        doc_ids = header_obj.create_from_xml_doc(cursor, uid,
                                                 xml.registerdoclist,
                                                 context=context)

        vals.update({
            'reqcode': xml.reqcode,
            'titulartype': xml.titulartype,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'operationnum': xml.operationnum,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': True,
            'document_ids': [(6, 0, doc_ids)]
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Genera l'xml del pas"""
        # TODO
        raise Exception("Generación xml de 04 paso a4 no implementada")

    def dummy_create(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.04.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antifa = ''
        if len(step01_id) > 0:
            antifa = step01.get_additional_info(cursor, uid, step01_id[0])
        if step['rebuig']:
            return _(u"{0} Rechazo: {1}").format(antifa, step['motiu_rebuig'])
        else:
            return antifa

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    _columns = {
        'header_id': fields.many2one(
            'giscegas.atr.step.header', 'Header', required=True
        ),
        'reqcode': fields.char("Código Solicitud", size=10),
        'responsedate': fields.date('Fecha respuesta'),
        'responsehour': fields.datetime('Hora respuesta'),
        'titulartype': fields.selection(
            TAULA_TIPO_PERSONA, "Tipo de titular"
        ),
        'nationality': fields.selection(
            TAULA_NACIONALIDAD, "Nacionalidad documento"
        ),
        'documenttype': fields.selection(
            TAULA_TIPO_DOCUMENTO, "Tipo de documento de identificacion"
        ),
        'documentnum': fields.char("Nº Documento", size=20),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'operationnum': fields.char("Número de Operación Generado", size=40),
        'extrainfo': fields.text("Observaciones de la Realización", size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(
            _ff_motiu_rebuig, method=True, string=u"Motivo Rechazo",
            type='char', size=250, readonly=True
        ),

    }

    _defaults = {

    }

GiscegasAtr04A4()
