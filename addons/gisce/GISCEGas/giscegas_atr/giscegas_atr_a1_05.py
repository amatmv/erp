# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_a1_05 as a1_05
from gestionatr.defs_gas import *
from gestionatr.defs import SINO
from tools.translate import _
from datetime import datetime
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException
from giscegas_atr.utils import get_address_dict


class GiscegasAtrProces05(osv.osv):

    _name = 'giscegas.atr.proces'
    _inherit = 'giscegas.atr.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 initial steps depending on where we are '''

        if proces == '05':
            return ['a1']  # In both comer and distri

        return super(GiscegasAtrProces05,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 emisor steps depending on where we are '''
        if proces == '05':
            if where == 'distri':
                return ['a2', 'a25', 'a3', 'a4']
            elif where == 'comer':
                return ['a1']

        return super(GiscegasAtrProces05,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == '05':
            return ['a2', 'a4']

        return super(GiscegasAtrProces05,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        if proces == '05':
            return []

        return super(GiscegasAtrProces05, self).get_old_company_steps(
            cursor, uid, proces, context
        )


GiscegasAtrProces05()


class GiscegasAtr05(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == '05':
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == 'a2':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                elif step_name in ('a3', 'a4'):
                    return True

        return super(GiscegasAtr05,
                     self).get_final(cursor, uid, sw, context=context)

GiscegasAtr05()


class GiscegasAtr05A1(osv.osv):
    """ Classe pel pas a1
    """

    _name = "giscegas.atr.05.a1"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a1'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '05', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr05A1,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML 05, pas a1
        """
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        heading = pas.header_id.generar_xml(pas)
        a105 = a1_05.A105()

        # Common values for all updatereason
        update_reason = pas.updatereason
        register_doc_obj = self.pool.get('giscegas.atr.document')
        docs_xml = register_doc_obj.generar_xml(cr, uid, pas, context=context)
        vals_a105 = {
            'reqdate': pas.header_id.date_created.split(" ")[0],
            'reqhour': pas.header_id.date_created.split(" ")[1],
            'comreferencenum': sw.codi_sollicitud,
            'cups': sw.cups_id.name[:20],
            'updatereason': pas.updatereason,
            'modeffectdate': pas.modeffectdate,
            'reqtransferdate': pas.reqtransferdate and pas.modeffectdate in ['03', '04'],
            'extrainfo': pas.extrainfo,
            'nationality': pas.nationality,
            'documenttype': pas.documenttype,
            'documentnum': pas.documentnum,
            'registerdoclist': docs_xml,
        }
        if update_reason in ['11', '12', '15', '16', '18', '19', '22', '23']:
            vals_a105.update({
                'newtolltype': pas.newtolltype,
            })

        if update_reason in ['13', '14', '15', '16', '20', '21', '22', '23']:
            vals_a105.update({
                'newreqqd': pas.newreqqd,
            })

        if update_reason in ['09']:
            vals_a105.update({
                'newcaecode': pas.newcaecode.name,
            })

        if update_reason in ['01', '04', '12', '14', '16', '17', '19', '21', '23']:
            vals_a105.update({
                'surrogacy': pas.surrogacy,
                'newnationality': pas.newnationality,
                'newdocumenttype': pas.newdocumenttype,
                'newdocumentnum': pas.newdocumentnum,
                'newfirstname': pas.newfirstname,
                'newfamilyname1': pas.newfamilyname1,
                'newfamilyname2': pas.newfamilyname2,
                'newtitulartype': pas.newtitulartype,
                'newregularaddress': pas.newregularaddress,
                'newtelephone': pas.newtelephone,
                'newemail': pas.newemail,
                'newfax': pas.newfax,
            })

        if update_reason in ['04', '10', '17', '18', '19', '20', '21', '22', '23']:
            vals_a105.update({
                'newprovinceowner': pas.newprovinceowner and pas.newprovinceowner.code,
                'newcityowner': pas.newcityowner and pas.newcityowner.ine,
                'newzipcodeowner': pas.newzipcodeowner,
                'newstreettypeowner': pas.newstreettypeowner,
                'newstreetowner': pas.newstreetowner,
                'newstreetnumberowner': pas.newstreetnumberowner,
                'newportalowner': pas.newportalowner,
                'newstaircaseowner': pas.newstaircaseowner,
                'newfloorowner': pas.newfloorowner,
                'newdoorowner': pas.newdoorowner,
            })

        a105.feed(vals_a105)
        msg = a1_05.MensajeA105()
        msg.feed({
            'heading': heading,
            'a105': a105,
        })
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        raise Exception("Importación de 05 paso a1 no implementada")

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        mandatory_fields = ['modeffectdate', 'updatereason', 'nationality',
                            'documenttype', 'documentnum']

        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)
        if vals2.get('modeffectdate') in ('03', '04'):
            mandatory_fields.append('reqtransferdate')
        if vals2.get('updatereason') in ['11', '12', '15', '16', '18', '19', '22', '23']:
            mandatory_fields.extend(['newtolltype'])
        if vals2.get('updatereason') in ['13', '14', '15', '16', '20', '21', '22', '23']:
            mandatory_fields.append('newreqqd')
        if vals2.get('updatereason') in ['09']:
            mandatory_fields.append('newcaecode')
        if vals2.get('updatereason') in ['01', '04', '12', '14', '16', '17', '19', '21', '23']:
            mandatory_fields.extend([
                'surrogacy', 'newnationality', 'newdocumenttype',
                'newdocumentnum', 'newfamilyname1', 'newtelephone',
                'newprovinceowner', 'newcityowner', 'newzipcodeowner',
                'newstreettypeowner', 'newstreetowner', 'newstreetnumberowner'
            ])
            if vals2.get('newtitulartype') == 'F':
                mandatory_fields.extend([
                    'newfirstname', 'newregularaddress'
                ])
            if vals2.get('newdocumenttype') in ('07', '08'):
                mandatory_fields.append('newtitulartype')

        non_completed = []
        for field in mandatory_fields:
            if not vals2.get(field, False):
                non_completed.append(field)

        if len(non_completed):
            raise GiscegasAtrException(_("Faltan datos. Campos no completados."), non_completed)

        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """
        """
        if not context:
            context = {}

        if not vals:
            return True

        self.config_step_validation(cursor, uid, ids, vals, context=context)
        if vals.get("new_contract_values"):
            pas = self.browse(cursor, uid, ids, context=context)
            pol_obj = self.pool.get('giscegas.polissa')
            pol_id = pol_obj.copy(cursor, uid, pas.sw_id.cups_polissa_id.id, context)
            pol_obj.write(cursor, uid, pol_id, vals.get("new_contract_values"))
            pas.sw_id.write({
                'ref': "giscegas.polissa,{0}".format(pol_id)
            })
        elif vals.get("new_contract_to_link"):
            pas = self.browse(cursor, uid, ids, context=context)
            new_con_id = vals.get("new_contract_to_link")
            pas.sw_id.write({'ref': "giscegas.polissa,{0}".format(new_con_id)})

        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)
        self.write(cursor, uid, ids, vals2, context=context)
        return True

    def prepare_for_validation(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}

        if not vals:
            return vals
        vals2 = {}
        part_obj = self.pool.get("res.partner")
        addr_obj = self.pool.get("res.partner.address")
        sw_obj = self.pool.get("giscegas.atr")
        if vals.get("owner_pre"):
            titular = part_obj.browse(cursor, uid, vals.get("owner_pre"))
            vat_info = sw_obj.get_vat_info(cursor, uid, titular.vat, False)
            vals2.update({
                'nationality': titular.country and titular.country.code,
                'documenttype': vat_info['document_type'],
                'documentnum': vat_info['vat'],
            })

        if vals.get("owner"):
            titular = part_obj.browse(cursor, uid, vals.get("owner"))
            vat_info = sw_obj.get_vat_info(cursor, uid, titular.vat, False)

            es_empresa = vat_info['is_enterprise']
            nom = part_obj.separa_cognoms(cursor, uid, titular.name)
            if not es_empresa:
                vals2.update({
                    'newfirstname': nom['nom'],
                    'newfamilyname1': nom['cognoms'][0],
                    'newfamilyname2': nom['cognoms'][1],
                    'newtitulartype': 'F',
                    'newregularaddress': 'S',
                })
            else:
                vals2.update({
                    'newfirstname': '',
                    'newfamilyname1': titular.name,
                    'newfamilyname2': '',
                    'newtitulartype': 'J'
                })

            vals2.update({
                'newnationality': titular.country and titular.country.code,
                'newdocumenttype': vat_info['document_type'],
                'newdocumentnum': vat_info['vat'],
            })
        if vals.get("direccio_notificacio"):
            addr = addr_obj.browse(cursor, uid, vals.get("direccio_notificacio"), context)
            vals2.update({
                'newtelephone': addr.phone or addr.mobile,
                'newemail': addr.email
            })
            vals2.update(get_address_dict(addr, mod="new"))

        vals2['newcaecode'] = vals.get("cnae")
        vals2['newtolltype'] = vals.get("tariff")
        vals2['extrainfo'] = vals.get("comments")
        vals2['reqtransferdate'] = vals.get("reqtransferdate")
        vals2['modeffectdate'] = vals.get("modeffectdate")
        vals2['newreqqd'] = vals.get("newreqqd")
        vals2['surrogacy'] = vals.get("surrogacy")
        vals2['updatereason'] = vals.get("updatereason")
        return vals2

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        titular_vat = polissa.titular.vat
        participant_obj = self.pool.get("giscemisc.participant")
        participant = participant_obj.get_from_partner(cursor, uid, polissa.distribuidora.id)
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, participant.codi_sifco
        )

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'updatereason': '11',  # Modificación peaje
            'reqtransferdate': today,
            'modeffectdate': '05',  # Cuanto antes
            # updatereason = 11
            'nationality': polissa.titular.country and polissa.titular.country.code,
            'documenttype': vat_info['document_type'],
            'documentnum': vat_info['vat'],
            'newtolltype': polissa.tarifa.codi_ocsum,
        })
        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_05_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        # We get the contract in the state it was when solicitud was generated
        polissa = step.sw_id.cups_polissa_id
        if not polissa:
            return " "
        max_data_fi_modcons = None
        if polissa.modcontractual_activa:
            max_data_fi_modcons = polissa.modcontractual_activa.data_final
        if (max_data_fi_modcons and
                polissa.data_alta <= step.date_created <= max_data_fi_modcons):
            polissa = polissa.browse(context={'date': step.date_created})[0]

        # Info comuna per tots els updatereason
        update_reason = step.updatereason
        base_msg = u"({0}) ".format(update_reason)

        # Segons updatereason posem una informacio o una altre
        if update_reason in ['11', '12', '15', '16', '18', '19', '22', '23']:
            oldt = polissa.tarifa.name
            tarif_obj = self.pool.get("giscegas.polissa.tarifa")
            tarif_id = tarif_obj.get_tarifa_from_ocsum(
                cursor, uid, step.newtolltype, context
            )
            if not tarif_id:
                return " "
            newt = tarif_obj.read(cursor, uid, tarif_id, ['name'])['name']
            base_msg += _(u"Tarifa: {0} -> {1}. ").format(oldt, newt)
        else:
            base_msg += _(u"Tarifa:{0}. ").format(polissa.tarifa.name)

        if update_reason in ['13', '14', '15', '16', '20', '21', '22', '23']:
            oldq = polissa.caudal_diario
            newq = step.newreqqd
            base_msg += _(u"Caudal Diario: {0} -> {1}. ").format(oldq, newq)
        else:
            base_msg += _(u"Caudal Diario: {0}. ").format(polissa.caudal_diario)

        if update_reason in ['09']:
            base_msg += u"CNAE: {0}. ".format(step.newcaecode.name)

        if update_reason in ['01', '04', '12', '14', '16', '17', '19', '21', '23']:
            oldp = polissa.titular_nif
            newp = step.newdocumentnum
            base_msg += u"Titular: {0} -> {1}. ".format(oldp, newp)

        return base_msg

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        # Camps comuns per qualsevol updatereason
        'updatereason': fields.selection(TAULA_MOTIVO_MODIFICACION, "Motivo de Modificación"),
        'modeffectdate': fields.selection(TAULA_MODELO_FECHA_EFECTO, "Modelo de Fecha Efecto"),
        'reqtransferdate': fields.date("Fecha de Efecto Solicitada"),
        'extrainfo': fields.text("Observaciones de la Solicitud", size=400),
        'nationality': fields.selection(TAULA_NACIONALIDAD, "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        # updatereason in [11, 12, 15, 16, 18, 19, 22, 23]
        'newtolltype': fields.selection(TAULA_TIPO_PEAJE, "Nuevo Peaje"),
        # updatereason in [13, 14, 15, 16, 20, 21, 22, 23]
        'newreqqd': fields.float("Nueva Qd", digits=(16, 7)),
        # updatereason in [09]
        'newcaecode': fields.many2one('giscemisc.cnae', 'Nuevo Código CAE'),
        # updatereason in [01, 04, 12, 14, 16, 17, 19, 21, 23]
        'surrogacy': fields.selection(SINO, "Indicador de Subrogación"),
        'newnationality': fields.selection(TAULA_NACIONALIDAD, "Nueva Nacionalidad"),
        'newdocumenttype': fields.selection(TAULA_TIPO_DOCUMENTO, "Nuevo Tipo Documento de Identificación"),
        'newdocumentnum': fields.char("Nuevo Nº Documento", size=20),
        'newfirstname': fields.char("Nuevo Nombre Cliente", size=40),
        'newfamilyname1': fields.char("Nuevo Apellido 1 / Razón Social", size=30),
        'newfamilyname2': fields.char("Nuevo Apellido 2", size=30),
        'newtitulartype': fields.selection(TAULA_TIPO_PERSONA, "Nuevo Tipo de Persona"),
        'newregularaddress': fields.selection(SINO, "Nuevo Uso de la Vivienda del PS"),
        'newtelephone': fields.char("Nuevo Teléfono", size=20),
        'newemail': fields.char("Nuevo Correo Electrónico", size=60),
        'newfax': fields.char("Nuevo Fax", size=60),
        # updatereason in [04, 10, 17, 18, 19, 20, 21, 22, 23]
        'newprovinceowner': fields.many2one('res.country.state', 'Nueva Provincia'),
        'newcityowner': fields.many2one('res.municipi', 'Nuevo Municipio'),
        'newzipcodeowner': fields.char("Nuevo Código Postal", size=5),
        'newstreettypeowner': fields.selection(TAULA_TIPO_VIA, "Nuevo Tipo de Vía"),
        'newstreetowner': fields.char("Nuevo Nombre Calle", size=60),
        'newstreetnumberowner': fields.char("Nuevo Nº Finca", size=4),
        'newportalowner': fields.char("Nuevo Portal", size=5),
        'newstaircaseowner': fields.char("Nueva Escalera", size=5),
        'newfloorowner': fields.char("Nuevo Piso", size=5),
        'newdoorowner': fields.char("Nueva Puerta", size=5),
    }

    _defaults = {

    }


GiscegasAtr05A1()


class GiscegasAtr05A2(osv.osv):
    """ Classe pel pas a2
    """

    _name = "giscegas.atr.05.a2"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a2'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '05', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr05A2,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a25', 'a3', 'a4']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 05 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        denegat = xml.result not in ['01', '09']

        rebuig_ids = []
        if denegat:
            rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                    context=context)

        vals.update({
            'reqcode': xml.reqcode,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'updatereason': xml.updatereason,
            'finaltolltypegranted': xml.finaltolltypegranted,
            'qdgranted': xml.qdgranted,
            'newmodeffectdate': xml.newmodeffectdate,
            'foreseentransferdate': xml.foreseentransferdate,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': denegat,
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_05_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.05.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0])
        if step['rebuig']:
            return _(u"{0}Rechazo: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def onchange_result(self, cursor, uid, ids, result_id, context=None):
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}
        vals = {}

        if result_id in ['01', '09']:
            vals['rebuig'] = False
        else:
            vals['rebuig'] = True

        res['value'] = vals
        return res

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header', required=True),
        'reqcode': fields.char("Código Solicitud", size=10),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'nationality': fields.selection(TAULA_NACIONALIDAD, "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        'updatereason': fields.selection(
            TAULA_MOTIVO_MODIFICACION, "Motivo de Modificación"
        ),
        'finaltolltypegranted': fields.selection(TAULA_TIPO_PEAJE, "Tipo de Peaje"),
        'qdgranted': fields.float("Qd Concedida", digits=(16, 7)),
        'newmodeffectdate': fields.selection(
            TAULA_MODELO_FECHA_EFECTO_PREVISTA, "Modelo de fecha a aplicar"
        ),
        'foreseentransferdate': fields.date("Fecha Prevista de Activación"),
        'extrainfo': fields.text("Observaciones de la Validación", size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motivo Rechazo", type='char',
                                        size=250, readonly=True, ),
    }

    _defaults = {
        'rebuig': lambda *a: False,
    }

GiscegasAtr05A2()


class GiscegasAtr05A3(osv.osv):
    """ Classe pel pas a3
    """

    _name = "giscegas.atr.05.a3"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a3'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '05', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr05A3,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 05 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        counter_ids = header_obj.create_from_xml_counters(cursor, uid, xml.counterlist, context=context)
        corrector_ids = header_obj.create_from_xml_correctors(cursor, uid, xml.correctorlist, context=context)

        municipi_obj = self.pool.get('res.municipi')
        municipi_id = municipi_obj.search(cursor, uid, [('ine', '=', xml.cityowner)])
        provincia_obj = self.pool.get('res.country.state')
        provincia_id = provincia_obj.search(cursor, uid, [('code', '=', xml.provinceowner)])

        # CNAE
        if xml.caecode:
            cnae_id = sw_obj.search_cnae(cursor, uid, xml.caecode, context=context)
            vals.update({'caecode': cnae_id})

        vals.update({
            'reqcode': xml.reqcode,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'atrcode': xml.atrcode,
            'transfereffectivedate': xml.transfereffectivedate,
            'updatereason': xml.updatereason,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'firstname': xml.firstname,
            'familyname1': xml.familyname1,
            'familyname2': xml.familyname2,
            'titulartype': xml.titulartype,
            'regularaddress': xml.regularaddress,
            'telephone': xml.telephone,
            'fax': xml.fax,
            'email': xml.email,
            'provinceowner': len(provincia_id) and provincia_id[0] or False,
            'cityowner': len(municipi_id) and municipi_id[0] or False,
            'zipcodeowner': xml.zipcodeowner,
            'streettypeowner': xml.streettypeowner and xml.streettypeowner.upper(),
            'streetowner': xml.streetowner,
            'streetnumberowner': xml.streetnumberowner,
            'portalowner': xml.portalowner,
            'staircaseowner': xml.staircaseowner,
            'floorowner': xml.floorowner,
            'doorowner': xml.doorowner,
            'extrainfo': xml.extrainfo,
            'counter_ids': [(6, 0, counter_ids)],
            'corrector_ids': [(6, 0, corrector_ids)],
            'tolltype': xml.tolltype,
            'qdgranted': xml.qdgranted
        })

        partner_ids = partner_obj.search(cursor, uid, [('vat', 'like', xml.documentnum)])
        if partner_ids:
            vals.update({
                'titular_id': partner_ids[0]
            })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_05_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header', required=True),
        # Dades Activacio
        'atrcode': fields.char("Código Contrato ATR", size=24),
        'reqcode': fields.char("Código Solicitud", size=10),
        'transfereffectivedate': fields.date("Fecha Efecto Solicitud (inclusive)"),
        'updatereason': fields.selection(TAULA_MOTIVO_MODIFICACION, "Motivo de Modificación"),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'extrainfo': fields.text("Observaciones de la Realización", size=400),
        # Dades Client
        'titular_id': fields.many2one("res.partner", "Cliente relacionado"),
        'nationality': fields.selection(TAULA_NACIONALIDAD, "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        'firstname': fields.char("Nombre Cliente", size=40),
        'familyname1': fields.char("Apellido 1 / Razón Social", size=30),
        'familyname2': fields.char("Apellido 2", size=30),
        'titulartype': fields.selection(TAULA_TIPO_PERSONA, "Tipo de Persona"),
        'regularaddress': fields.selection(SINO, "Uso de la Vivienda del PS"),
        'telephone': fields.char("Teléfono 1", size=20),
        'fax': fields.char("Fax", size=20),
        'email': fields.char("Correo Electrónico", size=60),
        'provinceowner': fields.many2one('res.country.state', 'Provincia'),
        'cityowner': fields.many2one('res.municipi', 'Municipio'),
        'zipcodeowner': fields.char("Código Postal", size=5),
        'streettypeowner': fields.selection(TAULA_TIPO_VIA, "Tipo de Vía"),
        'streetowner': fields.char("Nombre Calle", size=60),
        'streetnumberowner': fields.char("Nº Finca", size=4),
        'portalowner': fields.char("Portal", size=5),
        'staircaseowner': fields.char("Escalera", size=5),
        'floorowner': fields.char("Piso", size=5),
        'doorowner': fields.char("Puerta", size=5),
        'caecode': fields.many2one('giscemisc.cnae', 'Nuevo Código CAE'),
        # Dades Facturacio
        'tolltype': fields.selection(TAULA_TIPO_PEAJE, "Nuevo Peaje"),
        'qdgranted': fields.float("Caudal Diario Concedido", digits=(16, 7)),
    }

    _defaults = {

    }


GiscegasAtr05A3()


class GiscegasAtr05A4(osv.osv):
    """ Classe pel pas a4
    """

    _name = "giscegas.atr.05.a4"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a4'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '05', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr05A4,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 05 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                context=context)
        vals.update({
            'reqcode': xml.reqcode,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'updatereason': xml.updatereason,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': True,
        })
        if xml.interventiondate:
            inter_from = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhourfrom,
                "%Y-%m-%d %H:%M:%S"
            )
            inter_to = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhourto,
                "%Y-%m-%d %H:%M:%S"
            )
            vals.update({
                'interventionhourfrom': inter_from,
                'interventionhourto': inter_to,
            })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_05_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.05.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0])
        if step['rebuig']:
            return _(u"{0}Rechazo: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        'reqcode': fields.char("Código Solicitud", size=10),
        'updatereason': fields.selection(TAULA_MOTIVO_MODIFICACION, "Motivo de Modificación"),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'extrainfo': fields.text("Observaciones de No Realización", size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motivo Rechazo", type='char',
                                        size=250, readonly=True, ),
    }

    _defaults = {
        'rebuig': lambda *a: True,
    }


GiscegasAtr05A4()
