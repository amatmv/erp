# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_a1_41 as a1_41
from gestionatr.defs_gas import *
from gestionatr.defs import SINO
from tools.translate import _
from datetime import datetime
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException
from giscegas_atr.utils import get_address_dict


class GiscegasAtrProces41(osv.osv):

    _name = 'giscegas.atr.proces'
    _inherit = 'giscegas.atr.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 initial steps depending on where we are '''

        if proces == '41':
            return ['a1', 'a2s', 'a3s']  # In both comer and distri

        return super(GiscegasAtrProces41,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 emisor steps depending on where we are '''
        if proces == '41':
            if where == 'distri':
                return ['a2', 'a2s', 'a25', 'a3', 'a3s', 'a4', 'a4s']
            elif where == 'comer':
                return ['a1']

        return super(GiscegasAtrProces41,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == '41':
            return ['a2', 'a4', 'a4s']

        return super(GiscegasAtrProces41,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        if proces == '41':
            return ['a2s', 'a3s', 'a4s']

        return super(GiscegasAtrProces41, self).get_old_company_steps(
            cursor, uid, proces, context
        )


GiscegasAtrProces41()


class GiscegasAtr41(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == '41':
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == 'a2':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                elif step_name in ('a3', 'a4', 'a3s', 'a4s'):
                    return True

        return super(GiscegasAtr41,
                     self).get_final(cursor, uid, sw, context=context)

GiscegasAtr41()


class GiscegasAtr41A1(osv.osv):
    """ Classe pel pas a1
    """

    _name = "giscegas.atr.41.a1"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a1'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '41', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr41A1,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        heading = pas.header_id.generar_xml(pas)
        a141 = a1_41.A141()
        vals_a141 = {}
        update_reason = pas.updatereason
        modeffectdate = pas.modeffectdate
        tipus_persona = pas.newtitulartype
        tipus_document = pas.newdocumenttype

        if update_reason == '02':
            vals_a141.update({
                'nationality': pas.nationality,
                'documenttype': pas.documenttype,
                'documentnum': pas.documentnum,
            })

        if update_reason in ['01', '03']:
            vals_a141.update({
                'newtelephone1': pas.newtelephone1,
                'newtelephone2': pas.newtelephone2,
                'newemail': pas.newemail,
                'newlanguage': pas.newlanguage,
                'newprovinceowner': pas.newprovinceowner
                and pas.newprovinceowner.code,
                'newcityowner': pas.newcityowner and pas.newcityowner.ine,
                'newzipcodeowner': pas.newzipcodeowner,
                'newstreettypeowner': pas.newstreettypeowner,
                'newstreetowner': pas.newstreetowner,
                'newstreetnumberowner': pas.newstreetnumberowner,
                'newportal': pas.newportal,
                'newstaircase': pas.newstaircase,
                'newfloor': pas.newfloor,
                'newdoor': pas.newdoor,
                'newfamilyname1': pas.newfamilyname1,
                'surrogacy': pas.surrogacy,
                'newnationality': pas.newnationality,
                'newdocumenttype': tipus_document,
                'newdocumentnum': pas.newdocumentnum,
            })

        if update_reason in ['01', '03'] and tipus_persona == 'F':
            vals_a141.update({
                'newfamilyname2': pas.newfamilyname2,
                'newregularaddress': pas.newregularaddress,
                'newfirstname': pas.newfirstname,
            })

        if modeffectdate in ['03', '04']:
            vals_a141['reqtransferdate'] = pas.reqtransferdate

        if update_reason in ['01', '03'] and tipus_document in ['07', '08']:
            vals_a141.update({
                'newtitulartype': tipus_persona,
            })

        if update_reason in ['02', '03']:
            vals_a141['newreqqd'] = pas.newreqqd

        register_doc_obj = self.pool.get('giscegas.atr.document')
        docs_xml = register_doc_obj.generar_xml(cr, uid, pas, context=context)

        vals_a141.update({
            'reqdate': pas.header_id.date_created.split(" ")[0],
            'reqhour': pas.header_id.date_created.split(" ")[1],
            'comreferencenum': sw.codi_sollicitud,
            'cups': sw.cups_id.name[:20],
            'updatereason': update_reason,
            'modeffectdate': modeffectdate,
            'disconnectedserviceaccepted': pas.disconnectedserviceaccepted,
            'registerdoclist': docs_xml,
            'extrainfo': pas.extrainfo,
        })

        a141.feed(vals_a141)
        msg = a1_41.MensajeA141()
        msg.feed({
            'heading': heading,
            'a141': a141,
        })
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        raise Exception("Importación de 41 paso a1 no implementada")

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        mandatory_fields = ['modeffectdate', 'updatereason',
                            'disconnectedserviceaccepted']
        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)
        if vals2.get('modeffectdate') in ('03', '04'):
            mandatory_fields.append('reqtransferdate')

        if vals2.get('updatereason') in ('02'):
            mandatory_fields.extend([
                'nationality', 'documenttype', 'documentnum'
            ])
        if vals2.get('updatereason') in ('02', '03'):
            mandatory_fields.append('newreqqd')
        if vals2.get('updatereason') in ('01', '03'):
            mandatory_fields.extend([
                'surrogacy', 'newnationality', 'newdocumenttype',
                'newdocumentnum', 'newfamilyname1', 'newtelephone1',
                'newprovinceowner', 'newcityowner', 'newzipcodeowner',
                'newstreettypeowner', 'newstreetowner', 'newstreetnumberowner'
            ])
            if vals2.get('newtitulartype') == 'F':
                mandatory_fields.extend([
                    'newfirstname', 'newregularaddress'
                ])
            if vals2.get('newdocumenttype') in ('07', '08'):
                mandatory_fields.append('newtitulartype')

        non_completed = []
        for field in mandatory_fields:
            if not vals2.get(field, False):
                non_completed.append(field)

        if len(non_completed):
            raise GiscegasAtrException(_("Faltan datos. Campos no completados."), non_completed)

        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """
        """
        if not context:
            context = {}

        if not vals:
            return True

        self.config_step_validation(cursor, uid, ids, vals, context=context)
        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)
        self.write(cursor, uid, ids, vals2, context=context)
        return True

    def prepare_for_validation(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}

        if not vals:
            return vals
        part_obj = self.pool.get("res.partner")
        addr_obj = self.pool.get("res.partner.address")
        sw_obj = self.pool.get("giscegas.atr")
        if vals.get("titular_id"):
            titular = part_obj.browse(cursor, uid, vals.get("titular_id"))
            vat_info = sw_obj.get_vat_info(cursor, uid, titular.vat, False)
            vals.update({
                'nationality': titular.country and titular.country.code,
                'documenttype': vat_info['document_type'],
                'documentnum': vat_info['vat'],
            })
            vals.pop("titular_id")

        if vals.get("new_titular_id"):
            titular = part_obj.browse(cursor, uid, vals.get("new_titular_id"))
            vat_info = sw_obj.get_vat_info(cursor, uid, titular.vat, False)

            es_empresa = vat_info['is_enterprise']
            nom = part_obj.separa_cognoms(cursor, uid, titular.name)
            if not es_empresa:
                vals.update({
                    'newfirstname': nom['nom'],
                    'newfamilyname1': nom['cognoms'][0],
                    'newfamilyname2': nom['cognoms'][1],
                    'newtitulartype': 'F',
                    'newregularaddress': 'S',
                })
            else:
                vals.update({
                    'newfirstname': '',
                    'newfamilyname1': titular.name,
                    'newfamilyname2': '',
                    'newtitulartype': 'J'
                })

            vals.update({
                'newnationality': titular.country and titular.country.code,
                'newdocumenttype': vat_info['document_type'],
                'newdocumentnum': vat_info['vat'],
                'newlanguage': '01',
            })
            vals.pop("new_titular_id")
        if vals.get("new_titular_address_id"):
            addr = addr_obj.browse(cursor, uid, vals.get("new_titular_address_id"), context)
            vals.update({
                'newemail': addr.email
            })
            if not vals.get('newtelephone1'):
                vals.update({
                    'newtelephone1': addr.phone or addr.mobile,
                })
            vals.update(get_address_dict(addr, mod="new"))
            vals.pop("new_titular_address_id")
        return vals

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        titular_vat = polissa.titular.vat
        participant_obj = self.pool.get("giscemisc.participant")
        participant = participant_obj.get_from_partner(cursor, uid, polissa.distribuidora.id)
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, participant.codi_sifco
        )

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'reqtransferdate': today,
            'modeffectdate': '05',  # Cuanto antes
            'updatereason': '02',  # Modificación caudal
            'disconnectedserviceaccepted': "N",
            # updatereason = 02
            'nationality': polissa.titular.country and polissa.titular.country.code,
            'documenttype': vat_info['document_type'],
            'documentnum': vat_info['vat'],
            'newreqqd': polissa.caudal_diario,
        })
        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_41_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        # We get the contract in the state it was when solicitud was generated
        polissa = step.sw_id.cups_polissa_id
        if not polissa:
            return " "
        max_data_fi_modcons = None
        if polissa.modcontractual_activa:
            max_data_fi_modcons = polissa.modcontractual_activa.data_final
        if (max_data_fi_modcons and
                polissa.data_alta <= step.date_created <= max_data_fi_modcons):
            polissa = polissa.browse(context={'date': step.date_created})[0]

        # Info comuna per tots els updatereason
        update_reason = step.updatereason
        base_msg = u"({0}) ".format(update_reason)

        # Segons updatereason posem una informacio o una altre
        base_msg += _(u"Tarifa:{0}. ").format(polissa.tarifa.name)
        if update_reason in ['02', '03']:
            oldq = polissa.caudal_diario
            newq = step.newreqqd
            base_msg += _(u"Caudal Diario: {0} -> {1}. ").format(oldq, newq)
        else:
            base_msg += _(u"Caudal Diario: {0}. ").format(polissa.caudal_diario)

        if update_reason in ['01', '03']:
            oldp = polissa.titular_nif
            newp = step.newdocumentnum
            base_msg += u"Titular: {0} -> {1}. ".format(oldp, newp)

        return base_msg

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        # Camps comuns per qualsevol updatereason
        'updatereason': fields.selection(TAULA_MOTIVO_DE_MODIFICACION_EN_UN_CAMBIO_DE_COMERCIALIZADOR, "Motivo de Modificación"),
        'modeffectdate': fields.selection(TAULA_MODELO_FECHA_EFECTO, "Modelo de Fecha Efecto"),
        'reqtransferdate': fields.date("Fecha de Efecto Solicitada"),
        'disconnectedserviceaccepted': fields.selection(SINO, "Contratación incondicional baja"),
        'extrainfo': fields.text("Observaciones de la Solicitud", size=400),
        # Camps de titular. Quan updatereason=02 (ampliacion caudal) son camps
        # del titular actual, altrament fan referencia al titular nou. En tot
        # cas serà el titular que tinguem al contracte.
        # updatereason = 02
        'nationality': fields.selection(TAULA_NACIONALIDAD, "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        'newreqqd': fields.float("Caudal Diario Qd", digits=(16, 7)),
        # updatereason in [01, 03]
        'surrogacy': fields.selection(SINO, "Indicador de Subrogación"),
        'newnationality': fields.selection(TAULA_NACIONALIDAD, "Nacionalidad documento"),
        'newdocumenttype': fields.selection(TAULA_TIPO_DOCUMENTO, "Tipo Documento de Identificación"),
        'newdocumentnum': fields.char("Nº Documento", size=20),
        'newfirstname': fields.char("Nombre Cliente", size=40),
        'newfamilyname1': fields.char("Apellido 1 / Razón Social", size=30),
        'newfamilyname2': fields.char("Apellido 2", size=30),
        'newtitulartype': fields.selection(TAULA_TIPO_PERSONA, "Tipo de Persona"),
        'newregularaddress': fields.selection(SINO, "Vivienda habitual"),
        'newtelephone1': fields.char("Teléfono 1", size=20),
        'newtelephone2': fields.char("Teléfono 2", size=20),
        'newemail': fields.char("Correo Electrónico", size=60),
        'newlanguage': fields.selection(TAULA_IDIOMA, "Idioma"),
        'newprovinceowner': fields.many2one('res.country.state', 'Provincia'),
        'newcityowner': fields.many2one('res.municipi', 'Municipio'),
        'newzipcodeowner': fields.char("Código Postal", size=5),
        'newstreettypeowner': fields.selection(TAULA_TIPO_VIA, "Tipo de Vía"),
        'newstreetowner': fields.char("Nombre Calle", size=60),
        'newstreetnumberowner': fields.char("Nº Finca", size=4),
        'newportal': fields.char("Portal", size=5),
        'newstaircase': fields.char("Escalera", size=5),
        'newfloor': fields.char("Piso", size=5),
        'newdoor': fields.char("Puerta", size=5),
    }

    _defaults = {

    }


GiscegasAtr41A1()


class GiscegasAtr41A2(osv.osv):
    """ Classe pel pas a2
    """

    _name = "giscegas.atr.41.a2"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a2'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '41', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr41A2,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2s', 'a25', 'a3', 'a4']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 41 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        denegat = xml.result not in ['01', '09']

        rebuig_ids = []
        if denegat:
            rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                    context=context)

        vals.update({
            'reqcode': xml.reqcode,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'updatereason': xml.updatereason,
            'reqqd': xml.reqqd,
            'reqqh': xml.reqqh,
            'reqestimatedqa': xml.reqestimatedqa,
            'reqoutgoingpressure': xml.reqoutgoingpressure,
            'tolltype': xml.tolltype,
            'qdgranted': xml.qdgranted,
            'singlenomination': xml.singlenomination,
            'netsituation': xml.netsituation,
            'newmodeffectdate': xml.newmodeffectdate,
            'foreseentransferdate': xml.foreseentransferdate,
            'StatusPS': xml.StatusPS,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': denegat,
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_41_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.41.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0])
        if step['rebuig']:
            return _(u"{0}Rechazo: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def onchange_result(self, cursor, uid, ids, result_id, context=None):
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}
        vals = {}

        if result_id in ['01', '09']:
            vals['rebuig'] = False
        else:
            vals['rebuig'] = True

        res['value'] = vals
        return res

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        'reqcode': fields.char("Código Solicitud", size=10),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'nationality': fields.selection(TAULA_NACIONALIDAD,
                                        "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO,
                                         "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        'updatereason': fields.selection(
            TAULA_MOTIVO_DE_MODIFICACION_EN_UN_CAMBIO_DE_COMERCIALIZADOR,
            "Motivo de Modificación"),
        'reqqd': fields.float("Caudal Diario Qd", digits=(16, 7)),
        'reqqh': fields.integer("Caudal Horario Qh", digits=9),
        'reqestimatedqa': fields.integer("Caudal Estimado Anual Qa", digits=12),
        'reqoutgoingpressure': fields.float("Presión Salida", digits=(8, 3)),
        'tolltype': fields.selection(TAULA_TIPO_PEAJE, "Tipo de Peaje"),
        'qdgranted': fields.float("Qd Concedida", digits=(16, 7)),
        'singlenomination': fields.selection(TAULA_SI_NO,
                                             "Nominación Individual"),
        'netsituation': fields.char("Red o Municipio de Ubicación", size=14),
        'newmodeffectdate': fields.selection(TAULA_MODELO_FECHA_EFECTO_PREVISTA,
                                             "Modelo de fecha a aplicar"),
        'foreseentransferdate': fields.date("Fecha Prevista de Activación"),
        'StatusPS': fields.selection(TAULA_ESTADO_PUNTO_DE_SUMINISTRO,
                                     "Situación del PS"),
        'extrainfo': fields.text("Observaciones de la Validación", size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motivo Rechazo", type='char',
                                        size=250, readonly=True, ),
    }

    _defaults = {
        'rebuig': lambda *a: False,
    }


GiscegasAtr41A2()


class GiscegasAtr41A3(osv.osv):
    """ Classe pel pas a3
    """

    _name = "giscegas.atr.41.a3"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a3'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '41', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr41A3,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 41 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        counter_ids = header_obj.create_from_xml_counters(cursor, uid, xml.counterlist, context=context)
        corrector_ids = header_obj.create_from_xml_correctors(cursor, uid, xml.correctorlist, context=context)

        municipi_obj = self.pool.get('res.municipi')
        municipi_id = municipi_obj.search(cursor, uid, [('ine', '=', xml.cityowner)])
        provincia_obj = self.pool.get('res.country.state')
        provincia_id = provincia_obj.search(cursor, uid, [('code', '=', xml.provinceowner)])

        vals.update({
            'reqcode': xml.reqcode,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'atrcode': xml.atrcode,
            'transfereffectivedate': xml.transfereffectivedate,
            'telemetering': xml.telemetering,
            'finalqdgranted': xml.finalqdgranted,
            'finalqhgranted': xml.finalqhgranted,
            'finalclientyearlyconsumption': xml.finalclientyearlyconsumption,
            'gasusetype': xml.gasusetype,
            'updatereason': xml.updatereason,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'activationtype': xml.activationtype,
            'activationtypedesc': xml.activationtypedesc,
            'closingtype': xml.closingtype,
            'closingtypedesc': xml.closingtypedesc,
            'visitnumber': xml.visitnumber,
            'firstname': xml.firstname,
            'familyname1': xml.familyname1,
            'familyname2': xml.familyname2,
            'titulartype': xml.titulartype,
            'regularaddress': xml.regularaddress,
            'telephone1': xml.telephone1,
            'telephone2': xml.telephone2,
            'email': xml.email,
            'language': xml.language,
            'provinceowner': len(provincia_id) and provincia_id[0] or False,
            'cityowner': len(municipi_id) and municipi_id[0] or False,
            'zipcodeowner': xml.zipcodeowner,
            'streettypeowner': xml.streettypeowner and xml.streettypeowner.upper(),
            'streetowner': xml.streetowner,
            'streetnumberowner': xml.streetnumberowner,
            'portalowner': xml.portalowner,
            'staircaseowner': xml.staircaseowner,
            'floorowner': xml.floorowner,
            'doorowner': xml.doorowner,
            'canonircperiodicity': xml.canonircperiodicity,
            'lastinspectionsdate': xml.lastinspectionsdate,
            'lastinspectionsresult': xml.lastinspectionsresult,
            'StatusPS': xml.StatusPS,
            'readingtype': xml.readingtype,
            'lectureperiodicity': xml.lectureperiodicity,
            'extrainfo': xml.extrainfo,
            'counter_ids': [(6, 0, counter_ids)],
            'corrector_ids': [(6, 0, corrector_ids)]
        })

        partner_ids = partner_obj.search(cursor, uid, [('vat', 'like', xml.documentnum)])
        if partner_ids:
            vals.update({
                'titular_id': partner_ids[0]
            })

        if xml.interventiondate:
            inter_from = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhourfrom,
                "%Y-%m-%d %H:%M:%S"
            )
            inter_to = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhourto,
                "%Y-%m-%d %H:%M:%S"
            )
            vals.update({
                'interventionhourfrom': inter_from,
                'interventionhourto': inter_to,
            })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_41_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header', required=True),
        # Dades Activacio
        'atrcode': fields.char("Código Contrato ATR", size=24),
        'reqcode': fields.char("Código Solicitud", size=10),
        'transfereffectivedate': fields.date("Fecha Efecto Solicitud (inclusive)"),
        'updatereason': fields.selection(TAULA_MOTIVO_DE_MODIFICACION_EN_UN_CAMBIO_DE_COMERCIALIZADOR, "Motivo de Modificación"),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'activationtype': fields.selection(TAULA_TIPO_DE_ACTIVACION, "Tipo de Activación"),
        'activationtypedesc': fields.char("Descripción de Tipo de Activación", size=100),
        'extrainfo': fields.text("Observaciones de la Realización", size=400),
        # Dades Client
        'titular_id': fields.many2one("res.partner", "Cliente relacionado"),
        'nationality': fields.selection(TAULA_NACIONALIDAD, "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        'firstname': fields.char("Nombre Cliente", size=40),
        'familyname1': fields.char("Apellido 1 / Razón Social", size=30),
        'familyname2': fields.char("Apellido 2", size=30),
        'titulartype': fields.selection(TAULA_TIPO_PERSONA, "Tipo de Persona"),
        'regularaddress': fields.selection(SINO, "Uso de la Vivienda del PS"),
        'telephone1': fields.char("Teléfono 1", size=20),
        'telephone2': fields.char("Teléfono 2", size=20),
        'email': fields.char("Correo Electrónico", size=60),
        'language': fields.char("Idioma", size=2),
        'provinceowner': fields.many2one('res.country.state', 'Provincia'),
        'cityowner': fields.many2one('res.municipi', 'Municipio'),
        'zipcodeowner': fields.char("Código Postal", size=5),
        'streettypeowner': fields.selection(TAULA_TIPO_VIA, "Tipo de Vía"),
        'streetowner': fields.char("Nombre Calle", size=60),
        'streetnumberowner': fields.char("Nº Finca", size=4),
        'portalowner': fields.char("Portal", size=5),
        'staircaseowner': fields.char("Escalera", size=5),
        'floorowner': fields.char("Piso", size=5),
        'doorowner': fields.char("Puerta", size=5),
        # Dades Facturacio
        'telemetering': fields.selection(SINO, "Dispone de Telemedida"),
        'finalqdgranted': fields.float("Caudal Diario Concedido Final", digits=(16, 7)),
        'finalqhgranted': fields.integer("Caudal Horario Concedido Final"),
        'finalclientyearlyconsumption': fields.integer("Caudal Anual Actual Final"),
        'gasusetype': fields.selection(TAULA_TIPOS_DE_USO_DEL_GAS, "Tipo de Uso del Gas"),
        'canonircperiodicity': fields.selection(TAULA_PERIODICIDAD_CANON_IRC, "Periodicidad Canon IRC"),
        'StatusPS': fields.selection(TAULA_ESTADO_PUNTO_DE_SUMINISTRO, "Situación del PS"),
        'readingtype': fields.selection(TAULA_TIPO_LECTURA, "Tipo de Lectura"),
        'lectureperiodicity': fields.selection(TAULA_PERIODICIDAD_LECTURA, "Periodicidad de Lectura"),
        # Dades Incidencia
        'closingtype': fields.selection(TAULA_TIPO_CIERRE, "Tipo de Cierre"),
        'closingtypedesc': fields.char("Descripción del Tipo de Cierre", size=100),
        'interventionhourfrom': fields.datetime("Hora de inicio de intervención"),
        'interventionhourto': fields.datetime("Hora de fin de intervención"),
        'visitnumber': fields.integer("Número de Visita"),
        'lastinspectionsdate': fields.date("Fecha Ultima Inspección"),
        'lastinspectionsresult': fields.selection(TAULA_RESULTADO_INSPECCION, "Resultado Ultima Inspección"),
    }

    _defaults = {

    }


GiscegasAtr41A3()


class GiscegasAtr41A3s(osv.osv):
    """ Classe pel pas a3
    """

    _name = "giscegas.atr.41.a3s"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a3s'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '41', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr41A3s,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 41 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        counter_ids = header_obj.create_from_xml_counters(
            cursor, uid, xml.counterlist, context=context
        )
        corrector_ids = header_obj.create_from_xml_correctors(
            cursor, uid, xml.correctorlist, context=context
        )

        vals.update({
            'reqcode': xml.reqcode,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'transfereffectivedate': xml.transfereffectivedate,
            'previousatrcode': xml.previousatrcode,
            'readingtype': xml.readingtype,
            'extrainfo': xml.extrainfo,
            'counter_ids': [(6, 0, counter_ids)],
            'corrector_ids': [(6, 0, corrector_ids)],
        })

        partner_ids = partner_obj.search(cursor, uid, [('vat', 'like',
                                                        xml.documentnum)])
        if partner_ids:
            vals.update({
                'titular_id': partner_ids[0]
            })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_41_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        sw_obj = self.pool.get("giscegas.atr")
        step = self.read(cursor, uid, step_id,
                         ['sw_id', 'transfereffectivedate'])
        if sw_obj.whereiam(cursor, uid, False, context=context) == 'comer':
            return _(u"Fecha Activación: {0}").format(step['transfereffectivedate'])
        else:
            sw_id = step['sw_id'][0]
            step01 = self.pool.get('giscegas.atr.a1.a1')
            step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
            antinf = ''
            if len(step01_id) > 0:
                antinf = step01.get_additional_info(cursor, uid, step01_id[0])
            return antinf

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header',
                                     required=True),
        # Dades Activacio
        'previousatrcode': fields.char("Código Contrato ATR", size=24),
        'reqcode': fields.char("Código Solicitud", size=10),
        'transfereffectivedate': fields.date(
            "Fecha Efecto Solicitud (inclusive)"),
        'extrainfo': fields.text("Observaciones de la Realización", size=400),
        # Dades Client
        'nationality': fields.selection(TAULA_NACIONALIDAD,
                                        "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO,
                                         "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        # Dades Facturacio
        'readingtype': fields.selection(TAULA_TIPO_LECTURA, "Tipo de Lectura"),
    }

    _defaults = {

    }


GiscegasAtr41A3s()


class GiscegasAtr41A4(osv.osv):
    """ Classe pel pas a4
    """

    _name = "giscegas.atr.41.a4"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a4'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '41', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr41A4,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 41 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if not context:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                context=context)
        vals.update({
            'reqcode': xml.reqcode,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'nationality': xml.nationality,
            'documenttype': xml.documenttype,
            'documentnum': xml.documentnum,
            'updatereason': xml.updatereason,
            'closingtype': xml.closingtype,
            'closingtypedesc': xml.closingtypedesc,
            'visitnumber': xml.visitnumber,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': True,
        })
        if xml.interventiondate:
            inter_from = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhourfrom,
                "%Y-%m-%d %H:%M:%S"
            )
            inter_to = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhourto,
                "%Y-%m-%d %H:%M:%S"
            )
            vals.update({
                'interventionhourfrom': inter_from,
                'interventionhourto': inter_to,
            })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_41_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.41.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0])
        if step['rebuig']:
            return _(u"{0}Rechazo: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        'reqcode': fields.char("Código Solicitud", size=10),
        'nationality': fields.selection(TAULA_NACIONALIDAD, "Nacionalidad documento"),
        'documenttype': fields.selection(TAULA_TIPO_DOCUMENTO, "Tipo Documento de Identificación"),
        'documentnum': fields.char("Nº Documento", size=20),
        'updatereason': fields.selection(TAULA_MOTIVO_DE_MODIFICACION_EN_UN_CAMBIO_DE_COMERCIALIZADOR, "Motivo de Modificación"),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'closingtype': fields.selection(TAULA_TIPO_CIERRE, "Tipo de Cierre"),
        'closingtypedesc': fields.char("Descripción del Tipo de Cierre", size=100),
        'interventionhourfrom': fields.datetime("Hora de inicio de intervención"),
        'interventionhourto': fields.datetime("Hora de fin de intervención"),
        'visitnumber': fields.integer("Número de Visita"),
        'extrainfo': fields.text("Observaciones de No Realización", size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motivo Rechazo", type='char',
                                        size=250, readonly=True, ),
    }

    _defaults = {
        'rebuig': lambda *a: True,
    }


GiscegasAtr41A4()
