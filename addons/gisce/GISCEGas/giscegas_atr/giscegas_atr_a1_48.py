# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_a1_48 as a1_48
from gestionatr.defs_gas import *
from tools.translate import _
from datetime import datetime
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException
from giscegas_atr.utils import get_address_dict


class GiscegasAtrProces48(osv.osv):

    _name = 'giscegas.atr.proces'
    _inherit = 'giscegas.atr.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 initial steps depending on where we are '''

        if proces == '48':
            return ['a1']  # In both comer and distri

        return super(GiscegasAtrProces48,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 emisor steps depending on where we are '''
        if proces == '48':
            if where == 'distri':
                return ['a2', 'a25', 'a3']
            elif where == 'comer':
                return ['a1', 'a26']

        return super(GiscegasAtrProces48,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == '48':
            return ['a2']

        return super(GiscegasAtrProces48,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        if proces == '48':
            return []

        return super(GiscegasAtrProces48, self).get_old_company_steps(
            cursor, uid, proces, context
        )


GiscegasAtrProces48()


class GiscegasAtr48(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == '48':
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == 'a2':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                elif step_name in ('a3'):
                    return True

        return super(GiscegasAtr48,
                     self).get_final(cursor, uid, sw, context=context)

GiscegasAtr48()


class GiscegasAtr48A1(osv.osv):
    """ Classe pel pas a1
    """

    _name = "giscegas.atr.48.a1"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a1'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '48', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr48A1,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        if context is None:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        heading = pas.header_id.generar_xml(pas)
        a148 = a1_48.A148()
        vals_a148 = {}

        register_doc_obj = self.pool.get('giscegas.atr.document')
        docs_xml = register_doc_obj.generar_xml(cr, uid, pas, context=context)

        claimref_obj = self.pool.get('giscegas.atr.referencia.reclamacio')
        claimreferencelist_xml = claimref_obj.generar_xml(
            cr, uid, pas, context=context
        )

        vals_a148.update({
            'reqdate': pas.header_id.date_created.split(" ")[0],
            'reqhour': pas.header_id.date_created.split(" ")[1],
            'comreferencenum': sw.codi_sollicitud,
            'claimertype': pas.claimertype,
            'claimtype': pas.claimtype,
            'claimsubtype': pas.claimsubtype.name,
            'originreference': pas.originreference,
            'claimreferencelist': claimreferencelist_xml,
            'cups': sw.cups_id.name[:20],
            'legallimitdate': pas.legallimitdate,
            'priority': pas.priority,
            'registerdoclist': docs_xml,
            'extrainfo': pas.extrainfo,
        })

        if pas.claimertype != '06':
            claimerid = a1_48.claimerid()
            claimerid.feed({
                'claimerdocumenttype': pas.claimerdocumenttype,
                'claimerdocumentnum': pas.claimerdocumentnum,
            })
            claimername = a1_48.claimername()
            if pas.claimerfirstname and pas.claimerlastname:
                claimername.feed({
                    'claimerfirstname': pas.claimerfirstname,
                    'claimerlastname': pas.claimerlastname,
                    'claimersecondname': pas.claimersecondname,
                })
            else:
                claimername.feed({
                    'claimerbusinessname': pas.claimerbusinessname,
                })

            claimertelephone = a1_48.claimertelephone()
            claimertelephone.feed({
                'claimerprefixtel1': pas.claimerprefixtel1,
                'claimertelephone1': pas.claimertelephone1,
            })
            claimer = a1_48.claimer()
            claimer.feed({
                'claimerid': claimerid,
                'claimername': claimername,
                'claimertelephone': claimertelephone,
                'claimeremail': pas.claimeremail
            })
            vals_a148.update({'claimer': claimer})

        a148.feed(vals_a148)
        msg = a1_48.MensajeA148()
        msg.feed({
            'heading': heading,
            'a1': a148,
        })
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        raise Exception("Importación de 48 paso a1 no implementada")

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        mandatory_fields = []
        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)

        non_completed = []
        for field in mandatory_fields:
            if not vals2.get(field, False):
                non_completed.append(field)

        if len(non_completed):
            raise GiscegasAtrException(_("Faltan datos. Campos no completados."), non_completed)

        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """
        """
        if context is None:
            context = {}

        if not vals:
            return True

        self.config_step_validation(cursor, uid, ids, vals, context=context)
        vals2 = self.prepare_for_validation(cursor, uid, ids, vals, context=context)
        self.write(cursor, uid, ids, vals2, context=context)
        return True

    def prepare_for_validation(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}

        if not vals:
            return vals
        return vals

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id

        titular_vat = polissa.titular.vat
        participant_obj = self.pool.get("giscemisc.participant")
        participant = participant_obj.get_from_partner(cursor, uid, polissa.distribuidora.id)
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, participant.codi_sifco
        )

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({})
        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_48_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        base_msg = u""

        return base_msg

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header', required=True),
        'claimertype': fields.selection(TAULA_TIPO_SOLICITANTE, 'Tipo de Reclamante'),
        'claimtype': fields.selection(TAULA_TIPO_DE_RECLAMACION, 'Tipo de Reclamación'),
        'claimsubtype': fields.many2one("giscedata.subtipus.reclamacio", u"Subtipo"),
        'originreference': fields.char(string='Referencia del organismo que reclama', size=35),
        'legallimitdate': fields.date(string='Fecha Límite Legal'),
        'priority': fields.selection([('0', 'Prioridad Normal'), ('1', 'Prioridad Alta')], 'Prioridad de Reclamación'),
        'extrainfo': fields.text(string='Comentarios', size=400),
        'claimerdocumenttype': fields.selection(TAULA_TIPO_DOCUMENTO, 'Tipo de Documento de Identificación del Reclamante'),
        'claimerdocumentnum': fields.char(string='Nº Documento del Reclamante', size=20),
        'claimerfirstname': fields.char(string='Nombre Cliente del Reclamante', size=40),
        'claimerlastname': fields.char(string='Apellido 1 / Razón social del Reclamante', size=30),
        'claimersecondname': fields.char(string='Apellido 2 del Reclamante', size=30),
        'claimerprefixtel1': fields.char(string='Prefijo Nacional del Núero de Teléfono', size=4),
        'claimertelephone1': fields.char(string='Teléfono del Reclamante', size=20),
        'claimeremail': fields.char(string='Correo Electrónico del Reclamante', size=100),
        'claimerbusinessname': fields.char(string='Razón Social', size=40),
    }

    _defaults = {

    }


GiscegasAtr48A1()


class GiscegasAtr48A2(osv.osv):
    """ Classe pel pas a2
    """

    _name = "giscegas.atr.48.a2"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a2'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '48', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr48A2,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a25', 'a26', 'a3']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 48 paso a2 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        rebuig_obj = self.pool.get('giscegas.atr.rebuig')

        if context is None:
            context = {}

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        denegat = xml.result not in ['01', '09']

        rebuig_ids = []
        if denegat:
            rebuig_ids = rebuig_obj.create_from_xml(cursor, uid, sw_id, xml,
                                                    context=context)
        subt = self.pool.get("giscedata.subtipus.reclamacio").get_subtipus(cursor, uid, xml.claimsubtype)
        vals.update({
            'claimtype': xml.claimtype,
            'claimsubtype': subt,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'srcode': xml.srcode,
            'reqcode': xml.reqcode,
            'extrainfo': xml.extrainfo,
            'rebuig_ids': [(6, 0, rebuig_ids)],
            'rebuig': denegat,
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_48_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscegas.atr.48.a1')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0])
        if step['rebuig']:
            return _(u"{0}Rechazo: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def onchange_result(self, cursor, uid, ids, result_id, context=None):
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}
        vals = {}

        if result_id in ['01', '09']:
            vals['rebuig'] = False
        else:
            vals['rebuig'] = True

        res['value'] = vals
        return res

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header',
                                     'Header', required=True),
        'claimtype': fields.selection(TAULA_TIPO_DE_RECLAMACION, 'Tipo de Reclamación'),
        'claimsubtype': fields.many2one("giscedata.subtipus.reclamacio", u"Subtipo"),
        'reqcode': fields.char("Código Solicitud", size=10),
        'srcode': fields.char('Código de Petición del Distribuidor', size=20),
        'result': fields.selection(TAULA_RESULTADO, "Código de Resultado"),
        'resultdesc': fields.char("Descripción de Resultado", size=100),
        'extrainfo': fields.text("Observaciones de la Validación", size=400),
        'rebuig': fields.boolean("Rechazado"),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motivo Rechazo", type='char',
                                        size=250, readonly=True, ),
    }

    _defaults = {
        'rebuig': lambda *a: False,
    }


GiscegasAtr48A2()


class GiscegasAtr48A3(osv.osv):
    """ Classe pel pas a3
    """

    _name = "giscegas.atr.48.a3"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a3'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '48', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr48A3,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 48 paso a3 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        subt = self.pool.get("giscedata.subtipus.reclamacio").get_subtipus(cursor, uid, xml.claimsubtype)
        doc_ids = header_obj.create_from_xml_doc(cursor, uid, xml.registerdoclist, context=context)
        vals.update({
            'claimtype': xml.claimtype,
            'claimsubtype': subt,
            'srcode': xml.srcode,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'resolutiondetail': xml.resolutiondetail,
            'resolutiondetaildesc': xml.resolutiondetaildesc,
            'reqdescription': xml.reqdescription,
            'creditedcompensation': xml.creditedcompensation,
            'anomalyfraudrecordnum': xml.anomalyfraudrecordnum,
            'movementdate': xml.movementdate,
            'extrainfo': xml.extrainfo,
            'document_ids': [(6, 0, doc_ids)],
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_48_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header', required=True),
        'claimtype': fields.selection(TAULA_TIPO_DE_RECLAMACION, 'Tipo de Reclamación'),
        'claimsubtype': fields.many2one("giscedata.subtipus.reclamacio", u"Subtipo"),
        'srcode': fields.char('Código de Petición del Distribuidor', size=20),
        'result': fields.selection(TAULA_RESULTADO_RECLAMACION, 'Código de Resultado'),
        'resultdesc': fields.text('Descripción de Resultado', size=100),
        'resolutiondetail': fields.selection(TAULA_DETALLE_DE_RESOLUCION, 'Detalle de la Resolución'),
        'resolutiondetaildesc': fields.text('Descripción del detalle de la resolución.', size=100),
        'reqdescription': fields.text('Descripción Detallada de la Respuesta de la Actuación.', size=255),
        'creditedcompensation': fields.float('Indemnización Abonada', digits=(11, 2)),
        'anomalyfraudrecordnum': fields.char('Número de expediente de anomalía o fraude', size=20),
        'movementdate': fields.datetime('Fecha dactivación'),
        'extrainfo': fields.text('Comentarios', size=4000),
    }

    _defaults = {

    }


GiscegasAtr48A3()


class GiscegasAtr48A25(osv.osv):
    """ Classe pel pas a3
    """

    _name = "giscegas.atr.48.a25"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a25'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '48', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr48A25,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a25', 'a26', 'a3']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        raise Exception("Importación de 48 paso a1 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        sub_obj = self.pool.get("giscedata.subtipus.reclamacio")
        subt = sub_obj.get_subtipus(cursor, uid, xml.claimsubtype)
        nsubt = sub_obj.get_subtipus(cursor, uid, xml.claimsubtype)
        counter_ids = header_obj.create_from_xml_counters(cursor, uid, xml.counterlist, context=context)
        doc_ids = header_obj.create_from_xml_doc(cursor, uid, xml.registerdoclist, context=context)
        defect_ids = header_obj.create_from_xml_defect(cursor, uid, xml.defectlist, context=context)
        inf_ids = header_obj.create_from_xml_information(cursor, uid, xml.informationlist, context=context)
        visit = False
        if xml.visitdate and xml.visithour:
            visit = datetime.strptime(
                xml.visitdate + " " + xml.visithour,
                "%Y-%m-%d %H:%M:%S"
            )
        if xml.interventiondate:
            inter_from = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhourfrom,
                "%Y-%m-%d %H:%M:%S"
            )
            inter_to = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhourto,
                "%Y-%m-%d %H:%M:%S"
            )
            vals.update({
                'interventionhourfrom': inter_from,
                'interventionhourto': inter_to,
            })
        vals.update({
            'reqcode': xml.reqcode,
            'claimtype': xml.claimtype,
            'claimsubtype': subt,
            'srcode': xml.srcode,
            'interventiontype': xml.interventiontype,
            'newclaimtype': xml.newclaimtype,
            'newclaimsubtype': nsubt,
            'operationnum': xml.operationnum,
            'visitdatetime': visit,
            'informationtype': xml.informationtype,
            'resultreasonintervention': xml.resultreasonintervention,
            'resultinspection': xml.resultinspection,
            'visitnumber': xml.visitnumber,
            'extrainfo': xml.extrainfo,
            'conceptnumber': xml.conceptnumber,
            'counter_ids': [(6, 0, counter_ids)],
            'document_ids': [(6, 0, doc_ids)],
            'defect_ids': [(6, 0, defect_ids)],
            'information_ids': [(6, 0, inf_ids)]
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_48_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header', required=True),
        'claimtype': fields.selection(TAULA_TIPO_DE_RECLAMACION, 'Tipo de Reclamación'),
        'claimsubtype': fields.many2one("giscedata.subtipus.reclamacio", u"Subtipo"),
        'reqcode': fields.char("Código Solicitud", size=10),
        'srcode': fields.char('Código de Petición del Distribuidor', size=20),
        'interventiontype': fields.selection(TAULA_TIPO_INTERVENCION, 'Tipo de Intervención'),
        'newclaimtype': fields.selection(TAULA_TIPO_DE_RECLAMACION, 'Tipo de Reclamación'),
        'newclaimsubtype': fields.many2one("giscedata.subtipus.reclamacio", u"Subtipo"),
        'operationnum': fields.char('Número de Operación Generado', size=40),
        'visitdatetime': fields.datetime('Fecha Generación Información Visita'),
        'informationtype': fields.selection(TAULA_TIPO_DE_INFORMACION_INCIDENCIA, 'Tipo de Información o Incidencia'),
        'resultreasonintervention': fields.selection(TAULA_MOTIVO_RECHAZO_OCSUM, 'Motivo Resultado Intervención'),
        'interventionhourfrom': fields.datetime('Hora de inicio de intervención'),
        'interventionhourto': fields.datetime('Hora de fin de intervención'),
        'resultinspection': fields.selection(TAULA_RESULTADO_INSPECCION, 'Resultado Inspección'),
        'visitnumber': fields.integer('Número de Visita', size=3),
        'extrainfo': fields.text('Observaciones de la intervención', size=400),
        'conceptnumber': fields.integer('Número de Conceptos', size=2),
    }

    _defaults = {

    }


GiscegasAtr48A25()


class GiscegasAtr48A26(osv.osv):
    """ Classe pel pas a3
    """

    _name = "giscegas.atr.48.a26"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a26'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '48', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr48A26,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a25', 'a26', 'a3']

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        if context is None:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cr, uid, pas_id, context)
        sw = pas.sw_id
        heading = pas.header_id.generar_xml(pas)
        a2648 = a1_48.A2648()
        vals_a2648 = {}

        register_doc_obj = self.pool.get('giscegas.atr.document')
        docs_xml = register_doc_obj.generar_xml(cr, uid, pas, context=context)

        claimref_obj = self.pool.get('giscegas.atr.referencia.reclamacio')
        claimreferencelist_xml = claimref_obj.generar_xml(
            cr, uid, pas, context=context
        )
        variableinf_obj = self.pool.get('giscegas.atr.variableinf')
        variableinf_xml = variableinf_obj.generar_xml(
            cr, uid, pas, context=context
        )

        vals_a2648.update({
            'reqcode': pas.reqcode,
            'reqdate': pas.header_id.date_created.split(" ")[0],
            'reqhour': pas.header_id.date_created.split(" ")[1],
            'comreferencenum': sw.codi_sollicitud,
            'sequential': '01',
            'informationdate': pas.informationdate,
            'informationtype': pas.informationtype,
            'claimreferencelist': claimreferencelist_xml,
            'variableinflist': variableinf_xml,
            'cups': sw.cups_id.name[:20],
            'registerdoclist': docs_xml,
            'extrainfo': pas.extrainfo,
        })

        a2648.feed(vals_a2648)
        msg = a1_48.MensajeA2648()
        msg.feed({
            'heading': heading,
            'a26': a2648,
        })
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.codi_sifco,
                                   pas.receptor_id.codi_sifco,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        vals.update({
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_48_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header',
                                     required=True),
        'reqcode': fields.char('Código Solicitud', size=10),
        'informationdate': fields.date('Fecha de la información adicional aportada'),
        'informationtype': fields.selection(TAULA_TIPO_INFORMACION_ADICIONAL_APORTADA, 'Tipo de Información Adicional Aportada'),
        'extrainfo': fields.text('Comentarios', size=4000),
    }

    _defaults = {

    }


GiscegasAtr48A26()
