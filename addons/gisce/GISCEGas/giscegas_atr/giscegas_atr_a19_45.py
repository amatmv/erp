# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_a1_02 as a1_02
from gestionatr.defs_gas import *
from gestionatr.defs import SINO
from tools.translate import _
from datetime import datetime
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException
from giscegas_atr.utils import get_address_dict
from gestionatr.utils import get_description_gas


class GiscegasAtrProces45(osv.osv):

    _name = 'giscegas.atr.proces'
    _inherit = 'giscegas.atr.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 initial steps depending on where we are '''

        if proces == '45':
            return ['a19']  # In both comer and distri

        return super(GiscegasAtrProces45,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 emisor steps depending on where we are '''
        if proces == '45':
            if where == 'distri':
                return ['a19']
            elif where == 'comer':
                return []

        return super(GiscegasAtrProces45,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == '45':
            return []

        return super(GiscegasAtrProces45,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        if proces == '45':
            return []

        return super(GiscegasAtrProces45, self).get_old_company_steps(
            cursor, uid, proces, context
        )


GiscegasAtrProces45()


class GiscegasAtr45(osv.osv):

    _name = 'giscegas.atr'
    _inherit = 'giscegas.atr'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == '45':
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name in ('a19'):
                    return True

        return super(GiscegasAtr45,
                     self).get_final(cursor, uid, sw, context=context)

GiscegasAtr45()


class GiscegasAtr45A1(osv.osv):
    """ Classe pel pas a1
    """

    _name = "giscegas.atr.45.a19"
    _inherits = {'giscegas.atr.step.header': 'header_id'}
    _nom_pas = 'a19'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscegas.atr.step')
        return step_obj.name_get_step(cursor, uid, ids, '45', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscegas.atr.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscegasAtr45A1,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['a2']

    def generar_xml(self, cr, uid, pas_id, context=None):
        raise NotImplementedError("Exportación de 45 paso a19 no implementada")

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscegas.atr')
        header_obj = self.pool.get('giscegas.atr.step.header')
        partner_obj = self.pool.get("res.partner")
        counter_ids = header_obj.create_from_xml_counters(cursor, uid, xml.counterlist, context=context)
        doc_ids = header_obj.create_from_xml_doc(cursor, uid, xml.registerdoclist, context=context)
        defect_ids = header_obj.create_from_xml_defect(cursor, uid, xml.defectlist, context=context)
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml, context=context)
        vals.update({
            'operationtype': xml.operationtype,
            'operationnum': xml.operationnum,
            'result': xml.result,
            'resultdesc': xml.resultdesc,
            'closingtype': xml.closingtype,
            'closingtypedesc': xml.closingtypedesc,
            'activationtype': xml.activationtype,
            'activationtypedesc': xml.activationtypedesc,
            'resultinspection': xml.resultinspection,
            'resultinspectiondesc': xml.resultinspectiondesc,
            'visitnumber': xml.visitnumber,
            'counterchange': xml.counterchange,
            'removallecture': xml.removallecture,
            'supplystatus': xml.supplystatus,
            'supplystatusdesc': xml.supplystatusdesc,
            'servicestatus': xml.servicestatus,
            'servicestatusdesc': xml.servicestatusdesc,
            'extrainfo': xml.extrainfo,
            'conceptnumber': xml.conceptnumber,
            'inspectionorig': xml.inspectionorig,
            'communicationreason': xml.communicationreason,
            'counter_ids': [(6, 0, counter_ids)],
            'defect_ids': [(6, 0, defect_ids)],
            'document_ids': [(6, 0, doc_ids)]
        })
        if xml.interventiondate:
            inter_from = datetime.strptime(
                xml.interventiondate + " " + xml.interventionhour,
                "%Y-%m-%d %H:%M:%S"
            )
            vals.update({
                'interventiondate': inter_from,
            })
        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id, context=context)
        return pas_id

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        return True

    def dummy_create(self, cursor, uid, sw_id, context=None):
        raise NotImplementedError("Generación de 45 paso a19 no implementada")

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_45_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        # We get the contract in the state it was when solicitud was generated
        polissa = step.sw_id.cups_polissa_id
        if not polissa:
            return " "
        step = self.browse(cursor, uid, step_id)
        base_msg = u"{0}: {1}".format(step.operationtype, get_description_gas(step.operationtype, 'TAULA_TIPO_OPERACION_TODOS'))
        return base_msg + ". "

    _columns = {
        'header_id': fields.many2one('giscegas.atr.step.header', 'Header', required=True),
        'operationtype': fields.selection(TAULA_TIPO_OPERACION_TODOS, 'Tipo de Actuación'),
        'operationnum': fields.char('Número de Operación Generado', size=40),
        'result': fields.selection(TAULA_RESULTADO, 'Código de Resultado'),
        'resultdesc': fields.text('Descripción de Resultado', size=100),
        'closingtype': fields.selection(TAULA_TIPO_CIERRE, 'Tipo de Cierre'),
        'closingtypedesc': fields.text('Descripción del Tipo de Cierre', size=100),
        'activationtype': fields.selection(TAULA_TIPO_DE_ACTIVACION, 'Tipo de Activación'),
        'activationtypedesc': fields.text('Descripción de Tipo de Activación', size=100),
        'interventiondate': fields.datetime('Fecha Intervención'),
        'resultinspection': fields.selection(TAULA_RESULTADO_INSPECCION, 'Resultado Inspección'),
        'resultinspectiondesc': fields.text('Descripción de Resultado Inspección', size=250),
        'visitnumber': fields.integer('Número de Visita', size=3),
        'counterchange': fields.selection(TAULA_INDICATIVO_SI, 'Cambio de Contador'),
        'removallecture': fields.integer('Lectura de Levantamiento', size=9),
        'supplystatus': fields.selection(TAULA_SITUACION_SUMINISTRO, 'Situación del Suministro'),
        'supplystatusdesc': fields.text('Descripción Situación del Suministro', size=100),
        'servicestatus': fields.selection(TAULA_SITUACION_SERVICIO, 'Situación del servicio'),
        'servicestatusdesc': fields.text('Descripción Situación del Servicio', size=100),
        'extrainfo': fields.text('Observaciones de la Comunicación', size=400),
        'communicationreason': fields.selection(TAULA_TIPO_COMUNICACION_A19_45, 'Causa de la comunicación'),
        'conceptnumber': fields.integer('Número de Conceptos', size=2),
        'inspectionorig': fields.selection(TAULA_ORIGEN_INSPECCION, 'Origen de la inspección'),
    }

    _defaults = {

    }


GiscegasAtr45A1()
