# -*- coding: utf-8 -*-

from osv import osv
from tools.translate import _


class ResPartner(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    def vat_change(self, cr, uid, ids, value, context={}):
        """
        Quan es canvii el document d'identitat del partner, interessa omplir
        el camp country_id d'aquest per indicar la nacionalitat del document.
        """
        if context is None:
            context = {}
        res = super(ResPartner, self).vat_change(cr, uid, ids,
                                                 value, context={})
        country_id = False
        if value:
            preffix = value[:2].upper()
            if preffix == 'PS':
                preffix = 'ES'
            if preffix:
                countr_obj = self.pool.get('res.country')
                country_id = countr_obj.q(cr, uid).read(['id']).where([
                    ('code', 'like', preffix)
                ])
                if country_id:
                    country_id = country_id[0]['id']
        res['value'].update({'country_id': country_id})
        return res

    def fields_get(self, cursor, uid, fields=None, context=None):
        """
        Afegir atribut "help" per el camp country_id
        """

        res = super(ResPartner, self).fields_get(
            cursor, uid, fields=fields, context=context
        )
        if 'country_id' in res:
            res['country_id'].update({
                'name': _('Nacionalidad VAT'),
                'help': _('Nacionalidad del documento de identidad')
            })
        return res


ResPartner()
