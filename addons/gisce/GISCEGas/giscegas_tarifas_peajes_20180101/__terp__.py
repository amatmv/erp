# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes Enero 2018 Gas",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 312 - 22/12/2017.
 ETU/1283/2017
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends":[
        "giscegas_polissa",
        "giscegas_facturacio",
        "product",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegas_tarifas_peajes_20180101_data.xml"
    ],
    "active": False,
    "installable": True
}
