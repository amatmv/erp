# -*- coding: utf-8 -*-
{
    "name": "Pólizas de los clientes (Comercialitzadora)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends":[
        "giscegas_polissa",
        "giscegas_cups_comer",
        "account_payment_extension"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscegas_polissa_comer_demo.xml"
    ],
    "update_xml":[
        "giscegas_polissa_comer_data.xml",
        "giscegas_polissa_view.xml",
        "res_company_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
