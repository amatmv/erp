# -*- coding: utf-8 -*-
"""Módulo giscegas_polissa (comercialitzadora)."""
from osv import osv, fields
from tools.translate import _
from tools.safe_eval import safe_eval as eval
from gestionatr.defs_gas import TAULA_PERIODICIDAD_LECTURA_FACTURACION


class GiscegasPolissa(osv.osv):
    """Póliza (Comercialitzadora)."""

    _name = "giscegas.polissa"
    _inherit = 'giscegas.polissa'
    _description = "Póliza de un Cliente"

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'ref_dist': False
        }
        default.update(default_values)
        res_id = super(
            GiscegasPolissa, self).copy_data(cursor, uid, id, default, context)
        return res_id

    def default_comercialitzadora(self, cursor, uid, context=None):
        """Obté el valor per defecte de la comercialitzadora.
        """
        user = self.pool.get('res.users').browse(cursor, uid, uid, context)
        return user.company_id.partner_id.id

    def _get_pol_ids(self, cursor, uid, ids, context=None):
        pol_obj = self.pool.get('giscegas.polissa')
        if not isinstance(ids, list):
            ids = [ids]
        return pol_obj.search(
            cursor, uid, [('titular', 'in', ids)], context=context)

    def _check_cnae_and_vat_corrects(self, cursor, uid, ids):
        res_obj = self.pool.get('res.partner')
        conf_obj = self.pool.get('res.config')

        constraint_enabled = conf_obj.get(
            cursor, uid, 'cnae_constrain_enabled', '0')

        if eval(constraint_enabled):
            for con in self.browse(cursor, uid, ids):
                if con.cnae.name == '9820' and res_obj.is_enterprise_vat(
                        con.titular.vat):
                    return False
        return True

    _columns = {
        'name': fields.char('Contrato', size=60, readonly=True, select=True,
                            states={'esborrany':[('readonly', False)]}),
        'ref_dist': fields.char('Referencia distribuidora', size=60),
        'distribuidora': fields.many2one('res.partner', 'Distribuidora',
                                    domain=[('supplier', '=', 1)],
                                    readonly=True,
                                    states={
                                        'esborrany':[('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', True)],
                                        'modcontractual': [('readonly', False),
                                                           ('required', True)]
                                    }),
        # Aquest camp és especial per la sincronització, té un valor constant
        'comercialitzadora': fields.many2one(
            'res.partner', 'Comercialitzadora', readonly=True, required=True
        ),
        'facturacio_distri': fields.selection(
            TAULA_PERIODICIDAD_LECTURA_FACTURACION,
            u'Facturación de distribuïdora', readonly=True,
            states={'esborrany': [('readonly', False)],
                    'validar': [('readonly', False)],
                    'modcontractual': [('readonly', False)],
                    }, help=u"Periodicidad de facturación de distribuidora"
        ),
    }

    _defaults = {
        'comercialitzadora': default_comercialitzadora,
    }

    _constraints = [
        (_check_cnae_and_vat_corrects,
         _("Las pólizas con CNAE 9820 no pueden tener VAT de empresa."),
         ['cnae']),
    ]

GiscegasPolissa()


class GiscegasPolissaModContractual(osv.osv):
    """Modificació contractual (comercialitzadora)."""

    _name = 'giscegas.polissa.modcontractual'
    _inherit = 'giscegas.polissa.modcontractual'

    _columns = {
        'distribuidora': fields.many2one(
            'res.partner', 'Distribuidora', domain=[('supplier', '=', 1)],
            required=True
        ),
        'ref_dist': fields.char('Referència distribuidora', size=60),
        'facturacio_distri': fields.selection(
            TAULA_PERIODICIDAD_LECTURA_FACTURACION,
            u'Facturación de distribuidora',
            help=u"Periodicidad de facturación de distribuidora"
        ),

    }

GiscegasPolissaModContractual()
