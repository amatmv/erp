# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes Enero 2019 Gas",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 308 - 22/12/2018.
 TEC/1367/2018
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends":[
        "giscegas_polissa",
        "giscegas_facturacio",
        "product",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegas_tarifas_peajes_20190101_data.xml"
    ],
    "active": False,
    "installable": True
}
