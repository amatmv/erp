# -*- coding: utf-8 -*-
{
    "name": "Unificador de seqüències de les factures",
    "description": """
        Mòdul que unifica les seqüències de les factures de Gas i Electricitat
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends": [
        "giscegas_facturacio",
        "giscedata_facturacio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscegas_facturacio_data.xml"
    ],
    "active": False,
    "installable": True
}
