# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _

from motius_canvis_base.giscedata_polissa import tipus_canvis


class GiscegasPolissaMotiuCanvi(osv.osv):

    _name = 'giscegas.polissa.motiu.canvi'

    _columns = {
        'motiu_id': fields.many2one(
            'giscedata.motiu.canvi', u'Motivo Cambio', required=True
        ),
        'tipus_motiu': fields.selection(
            tipus_canvis, 'Tipo cambio', required=True, select=1
        ),
        'observacions':  fields.text(u"Observaciones"),
        'polissa_id': fields.many2one('giscegas.polissa', u'Contrato'),
    }


GiscegasPolissaMotiuCanvi()


class GiscegasPolissa(osv.osv):

    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def wkf_cancelar(self, cursor, uid, ids):
        if isinstance(ids, list):
            ids = ids[0]

        motiu_canvi_obj = self.pool.get('giscegas.polissa.motiu.canvi')
        polissa_vals = self.read(
            cursor, uid, ids, ['motius_canvis_ids']
        )
        motius_canvis_vals = motiu_canvi_obj.read(
            cursor, uid, polissa_vals['motius_canvis_ids'], ['tipus_motiu']
        )
        can_be_canceled = False
        for motiu_canvi_vals in motius_canvis_vals:
            can_be_canceled = motiu_canvi_vals['tipus_motiu'] == 'cancelacio'
            if can_be_canceled:
                break

        if can_be_canceled:
            return super(GiscegasPolissa, self).wkf_cancelar(
                cursor, uid, ids
            )
        else:
            raise osv.except_osv(
                _('Error'), _(
                    u'No se puede cancelar el contrato porque no tiene un '
                    u'motivo de cancelación.'
                )
            )

    def _check_repeated_types(self, cursor, uid, ids, context=None):
        if isinstance(ids, list):
            ids = ids[0]
        motiu_canvi_obj = self.pool.get('giscegas.polissa.motiu.canvi')
        polissa_vals = self.read(
            cursor, uid, ids, ['motius_canvis_ids'], context=context
        )
        motius_canvis_vals = motiu_canvi_obj.read(
            cursor, uid, polissa_vals['motius_canvis_ids'], ['tipus_motiu'],
            context=context
        )
        elems = [motiu['tipus_motiu'] for motiu in motius_canvis_vals]

        return len(elems) == len(set(elems))

    def _get_polissa_origen(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for pol_info in self.read(cursor, uid, ids, ['polissa_anterior_id'], context=context):
            if pol_info['polissa_anterior_id']:
                res[pol_info['id']] = self.calc_polissa_origen(
                    cursor, uid, pol_info['polissa_anterior_id'][0],
                    context=context
                )
            else:
                res[pol_info['id']] = False
        return res

    def calc_polissa_origen(self, cursor, uid, polissa_id, context=None):
        pol_ant = self.read(
            cursor, uid, polissa_id, ['polissa_anterior_id'], context=context
        )['polissa_anterior_id']
        if not pol_ant or (pol_ant and pol_ant[0] == polissa_id):
            return polissa_id
        return self.calc_polissa_origen(cursor, uid, pol_ant[0], context=context)

    _columns = {
        'motius_canvis_ids': fields.one2many(
            'giscegas.polissa.motiu.canvi', 'polissa_id', 'Motivos Cambios'
        ),
        'polissa_anterior_id': fields.many2one(
            "giscegas.polissa", u"Contrato Anterior",
            domain=[('state', '=', 'cancelada')]
        ),
        'polissa_origen_id': fields.function(
            _get_polissa_origen, method=True, type='many2one',
            string=u"Contrato Original", relation='giscegas.polissa', store={
                'giscegas.polissa': (
                    lambda self, cursor, uid, ids, c=None: ids,
                    ['polissa_anterior_id'], 10
                )
            })
    }

    _constraints = [
        (_check_repeated_types,
         _(u'Hay dos motivos de cambio con el mismo tipo de cambio.'),
         ['motius_canvis_ids'])
    ]
    

GiscegasPolissa()
