# -*- coding: utf-8 -*-
from __future__ import absolute_import

from giscegas_atr.tests.common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction


class TestA105Gas(TestSwitchingImport):
    def test_a1_05_rellena_contrato_anterior(self):
        pol_obj = self.openerp.pool.get('giscegas.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        sw_obj = self.openerp.pool.get('giscegas.atr')
        wiz_obj = self.openerp.pool.get('giscegas.atr.a105.wizard')
        partner_obj = self.openerp.pool.get('res.partner')
        addr_obj = self.openerp.pool.get('res.partner.address')

        with Transaction().start(self.database) as txn:

            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            # REAL TEST
            contract = pol_obj.browse(cursor, uid, contract_id)

            new_contract_id = self.get_contract_id(
                txn, xml_id="polissa_gas_0003"
            )

            new_contract = pol_obj.browse(cursor, uid, new_contract_id)

            new_contract.write({'cups': contract.cups.id})

            other_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]
            another_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_c2c'
            )[1]

            partner_obj.write(
                cursor, uid, another_id, {'name': 'Escobar Gaviria, Pablo'}
            )

            addr_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_address_c2c_1'
            )[1]

            addr2_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_address_8'
            )[1]

            addr_obj.write(cursor, uid, [addr_id, addr2_id], {
                'state_id':
                    self.openerp.pool.get("res.country.state").search(cursor,
                                                                      uid, [],
                                                                      limit=1)[
                        0],
                'id_municipi':
                    self.openerp.pool.get("res.municipi").search(cursor, uid,
                                                                 [], limit=1)[0]
            })

            wiz_params = {
                'generate_new_contract': 'exists',
                'owner': other_id,
                'pagador': another_id,
                'retail_tariff': False,
                'updatereason': '01',
                'new_contract': new_contract_id
            }

            wiz_id = wiz_obj.create(
                cursor, uid, wiz_params, context={'contract_id': contract_id}
            )

            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            vals = wiz.onchange_updatereason(
                '01', wiz.contract.id, 'exists',
                wiz.new_contract and wiz.new_contract.id or False
            )

            wiz.write(vals['value'])

            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            vals = wiz.onchange_new_contract(
                wiz.contract.id, wiz.generate_new_contract,
                wiz.new_contract and wiz.new_contract.id or False
            )

            wiz.write(vals['value'])

            wiz.genera_casos_atr({'contract_id': contract_id})

            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.state, 'end')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '05'),
                ('step_id.name', '=', 'a1'),
                ('ref', 'like', 'giscegas.polissa')
            ])
            self.assertEqual(len(res), 1)

            sw = sw_obj.browse(cursor, uid, res[0])

            pol = pol_obj.browse(cursor, uid, int(sw.ref.split(",")[1]))

            self.assertNotEqual(pol.id, contract_id)
            self.assertEqual(pol.polissa_anterior_id.id, contract_id)
