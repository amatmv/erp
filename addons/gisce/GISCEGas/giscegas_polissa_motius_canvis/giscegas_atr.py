# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv
from giscegas_atr.wizard.wizard_a1_05_from_contract import CANVIS_TITULAR


class GiscegasAtr05A1(osv.osv):
    """ Inheritance from 05A1 to:
        - Fill contrato anterior
    """
    _name = 'giscegas.atr.05.a1'
    _inherit = 'giscegas.atr.05.a1'

    def config_step(self, cursor, uid, ids, vals, context=None):
        pol_obj = self.pool.get('giscegas.polissa')
        res = super(GiscegasAtr05A1, self).config_step(
            cursor, uid, ids, vals, context=context
        )
        update_reason = vals['updatereason']
        if update_reason in CANVIS_TITULAR:
            pas = self.browse(cursor, uid, ids, context=context)

            polissa_anterior_id = pas.sw_id.cups_polissa_id.id

            polissa_nova = pas.sw_id.ref

            polissa_nova_id = int(polissa_nova.split(',')[1]) if polissa_nova else False

            if polissa_nova_id:
                pol_obj.write(
                    cursor, uid, polissa_nova_id,
                    {'polissa_anterior_id': polissa_anterior_id},
                    context=context
                )

        return res


GiscegasAtr05A1()