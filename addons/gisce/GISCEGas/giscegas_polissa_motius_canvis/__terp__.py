# -*- coding: utf-8 -*-
{
    "name": "Motius de alta, baixa i tall per pólisses de Gas",
    "description": """Afageix els motius de alta, baixa i tall per a les pólisses de Gas""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Gas",
    "depends": [
        "giscegas_polissa",
        "motius_canvis_base",
        "giscegas_atr"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscegas_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
