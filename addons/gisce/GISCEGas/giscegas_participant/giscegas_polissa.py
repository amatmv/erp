# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscegasPolissa(osv.osv):
    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    _columns = {
        'distribuidora': fields.many2one('res.partner', 'Distribuidora',
                                         domain=[('supplier', '=', 1), ('energy_sector', 'in', ['gas', 'elegas'])],
                                         readonly=True,
                                         states={
                                             'esborrany': [('readonly', False)],
                                             'validar': [('readonly', False),
                                                         ('required', True)],
                                             'modcontractual': [
                                                 ('readonly', False),
                                                 ('required', True)]
                                         }),
    }

GiscegasPolissa()
