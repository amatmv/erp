from osv import osv


class GiscegasPolissa(osv.osv):
    _name = "giscegas.polissa"
    _inherit = "giscegas.polissa"

    def internal_related_attachments(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = super(GiscegasPolissa, self).internal_related_attachments(
            cursor, uid, ids, field_name, arg, context=context
        )

        attach_obj = self.pool.get('ir.attachment')
        mandate_obj = self.pool.get('payment.mandate')

        for polissa_id in ids:
            # Recuperar els mandatos adjunts
            mandatos_ids = mandate_obj.search(cursor, uid, [
                ('reference', '=', 'giscedata.polissa,{}'.format(polissa_id)),
            ])
            mandatos_attach_ids = attach_obj.search(cursor, uid, [
                ('res_model', '=', 'payment.mandate'),
                ('res_id', 'in', mandatos_ids)
            ])
            res[polissa_id] = list(set(res[polissa_id] + mandatos_attach_ids))

        return res


GiscegasPolissa()
