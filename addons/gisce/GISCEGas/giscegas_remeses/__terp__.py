# -*- coding: utf-8 -*-
{
    "name": "GISCE Gas Remeses",
    "description": """Modificació de remeses de facturació de gas""",
    "version": "0-dev",
    "author": "GISCE-TI, SL",
    "category": "Facturació",
    "depends": [
        "remeses_base",
        "giscegas_facturacio",
    ],
    "init_xml": [],
    "demo_xml": [
        "payment_order_demo.xml"
    ],
    "update_xml": [
        "giscegas_facturacio_view.xml",
        "payment_view.xml",
        "wizard/afegir_factures_remesa_view.xml",
        "wizard/wizard_group_invoices_payment_view.xml",
        "wizard/wizard_resum_factures_remesa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
