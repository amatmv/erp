# -*- coding: utf-8 -*-


from destral import testing
from destral.transaction import Transaction
from datetime import datetime
from dateutil.relativedelta import relativedelta


class TestWizardAfegirFacturesRemesa(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_afegeix_factura_a_remesa(self):
        """Aquest test comprova que l'assistent afegeix correctament
        una factura sense payment_order_id a una remesa"""
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        factura_obj = pool.get('giscegas.facturacio.factura')
        remesa_obj = pool.get('payment.order')
        wz_afegir_factures = pool.get('giscegas.wizard.afegir.factures.remesa')
        imd_obj = pool.get('ir.model.data')
        id_factura = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0004_polissa_0001_N'
        )[1]
        remesa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_remeses', 'remesa_gas_0001'
        )[1]
        factura_obj.invoice_open(cursor, uid, [id_factura])
        remesa = remesa_obj.browse(cursor, uid, remesa_id)

        ctx = {'model': 'giscegas.facturacio.factura'}
        wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
        wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

        wiz.write({
            'order': remesa.id
        })
        ctx.update({'active_ids': [id_factura]})
        res = wz_afegir_factures.action_afegir_factures(
            cursor, uid, [wz_id], ctx
        )

        factura = factura_obj.browse(cursor, uid, id_factura)
        self.assertEqual(
            factura.payment_order_id.id,
            remesa_id
        )
        self.assertEqual(
            len(factura.payment_order_history_ids),
            1
        )
        self.assertDictContainsSubset(
            {
                'payment_order_id': remesa_id,
                'modify_date': datetime.now().strftime('%Y-%m-%d')
            },
            factura.payment_order_history_ids[0].read(
                load='_classic_write'
            )[0]
        )

    def test_afegeix_varies_factures_a_remesa(self):
        """Aquest test comprova que l'assistent afegeix correctament
        vàries factures sense payment_order_id a una remesa"""
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        factura_obj = pool.get('giscegas.facturacio.factura')
        remesa_obj = pool.get('payment.order')
        wz_afegir_factures = pool.get('giscegas.wizard.afegir.factures.remesa')
        imd_obj = pool.get('ir.model.data')
        ids = []
        ids.append(imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0004_polissa_0001_N'
        )[1])
        ids.append(imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0005_polissa_0001_N'
        )[1])
        ids.append(imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1])
        remesa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_remeses', 'remesa_gas_0001'
        )[1]

        factura_obj.invoice_open(cursor, uid, ids)
        remesa = remesa_obj.browse(cursor, uid, remesa_id)

        ctx = {'model': 'giscegas.facturacio.factura'}
        wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
        wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

        wiz.write({
            'order': remesa.id
        })
        ctx.update({'active_ids': ids})
        res = wz_afegir_factures.action_afegir_factures(
            cursor, uid, [wz_id], ctx
        )
        for elem in ids:
            factura = factura_obj.read(
                cursor, uid, elem, ['payment_order_id'])
            self.assertEqual(
                factura['payment_order_id'][0],
                remesa_id
            )

    def afegir_factures_a_remesa(self, cursor, uid, multi):
        pool = self.openerp.pool
        factura_obj = pool.get('giscegas.facturacio.factura')
        remesa_obj = pool.get('payment.order')
        wz_afegir_factures = pool.get('giscegas.wizard.afegir.factures.remesa')
        imd_obj = pool.get('ir.model.data')
        ids = []
        if multi:
            ids.append(imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0004_polissa_0001_N'
            )[1])
            ids.append(imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0005_polissa_0001_N'
            )[1])
            ids.append(imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
            )[1])
        else:
            ids.append(imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0004_polissa_0001_N'
            )[1])
        remesa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_remeses', 'remesa_gas_0001'
        )[1]

        factura_obj.invoice_open(cursor, uid, ids)
        remesa = remesa_obj.browse(cursor, uid, remesa_id)

        ctx = {'model': 'giscegas.facturacio.factura'}
        wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
        wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

        wiz.write({
            'order': remesa.id
        })
        ctx.update({'active_ids': ids})
        res = wz_afegir_factures.action_afegir_factures(
            cursor, uid, [wz_id], ctx
        )
        return ids, remesa_id

    def tractar_factura_ja_remesada(self):
        pool = self.openerp.pool
        factura_obj = pool.get('giscegas.facturacio.factura')
        remesa_obj = pool.get('payment.order')
        wz_afegir_factures = pool.get('giscegas.wizard.afegir.factures.remesa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        id_fact = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0007_polissa_0001_N'
        )[1]

        remesa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_remeses', 'remesa_gas_0002'
        )[1]
        factura_obj.invoice_open(cursor, uid, [id_fact])
        remesa = remesa_obj.browse(cursor, uid, remesa_id)

        ctx = {'model': 'giscegas.facturacio.factura'}
        wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
        wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

        wiz.write({
            'order': remesa.id
        })
        ctx.update({'active_ids': [id_fact]})
        res = wz_afegir_factures.action_afegir_factures(
            cursor, uid, [wz_id], ctx
        )
        return id_fact, wiz, remesa_id

    def test_sobreescriure_remesa(self):
        """Aquest test comprova si l'assistent sobreescriu la remesa
        de la factura per la nova remesa especificada per l'usuari."""
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        factura_obj = pool.get('giscegas.facturacio.factura')
        remesa_obj = pool.get('payment.order')
        wz_afegir_factures = pool.get('giscegas.wizard.afegir.factures.remesa')
        factura_id, wizard, remesa_id = self.tractar_factura_ja_remesada()

        # Sobreescrivim la remesa
        ctx = {'model': 'giscegas.facturacio.factura'}
        res = wz_afegir_factures.override_payment_order(
            cursor, uid, [wizard.id], ctx
        )
        factura = factura_obj.browse(cursor, uid, factura_id)

        # Comprovem que la remesa de la factura coincideix amb la nova

        remesa = remesa_obj.browse(cursor, uid, remesa_id)
        self.assertEqual(
            factura.payment_order_id.id,
            remesa.id
        )

    def test_actualitzar_iban_sobreescriure_remesa(self):
        """
        Aquest test comprova si l'assistent informa a l'usuari
        quan s'intenta afegir una factura a una remesa quan aquesta ja està
        remesada
        """
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        factura_obj = pool.get('giscegas.facturacio.factura')
        partner_bank_obj = pool.get('res.partner.bank')
        wz_afegir_factures = pool.get('giscegas.wizard.afegir.factures.remesa')
        factura_id, wizard, remesa_id = self.tractar_factura_ja_remesada()
        factura = factura_obj.browse(cursor, uid, factura_id)

        # Comprovem que l'assistent esta en estat "pendent", és a dir, que
        # està informant a l'usuari de que la factura ja te remesa
        self.assertEqual(
            wizard.state,
            'pendents'
        )
        bank_id = partner_bank_obj.create(cursor, uid, {
            'partner_id': factura.polissa_id.pagador.id,
            'state': 'iban',
            'acc_country_id': 1,
            'iban': 'ES7712341234161234567890',
            'name': 'Test d'
        })
        factura.polissa_id.write({'bank': bank_id})
        # Sobreescrivim la remesa
        ctx = {'model': 'giscegas.facturacio.factura'}
        wizard.write({'update_iban': True})
        res = wz_afegir_factures.override_payment_order(
            cursor, uid, [wizard.id], ctx
        )
        factura = factura_obj.browse(cursor, uid, factura_id)
        self.assertEqual(bank_id, factura.partner_bank.id)


    def test_informar_factura_remesada(self):
        """Aquest test comprova si l'assistent informa a l'usuari
        quan s'intenta afegir una factura a una remesa quan aquesta ja està
        remesada"""
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        factura_obj = pool.get('giscegas.facturacio.factura')
        factura_id, wizard, remesa_id = self.tractar_factura_ja_remesada()
        factura = factura_obj.browse(cursor, uid, factura_id)

        # Comprovem que l'assistent esta en estat "pendent", és a dir, que
        # està informant a l'usuari de que la factura ja te remesa

        self.assertEqual(
            wizard.state,
            'pendents'
        )

        # Comprovem que la factura surt al recuadre de factures ja remesades

        self.assertIn(
            factura,
            wizard.remesades
        )

    def test_mix_remesades_i_no_remesades(self):
        """Aquest test comprova si l'assistent assigna la remesa 2
        a les factures 1, 2 i 3 i deixa la remesa 1 a la factura 04,
        és a dir, que no la sobreescrigui."""
        pool = self.openerp.pool
        factura_obj = pool.get('giscegas.facturacio.factura')
        remesa_obj = pool.get('payment.order')
        wz_afegir_factures = pool.get('giscegas.wizard.afegir.factures.remesa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        f1_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0004_polissa_0001_N'
        )[1]
        f2_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0005_polissa_0001_N'
        )[1]
        f3_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1]
        f4_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0007_polissa_0001_N'
        )[1]

        ids = [f1_id, f2_id, f3_id, f4_id]

        remesa1_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_remeses', 'remesa_gas_0001'
        )[1]
        remesa2_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_remeses', 'remesa_gas_0002'
        )[1]
        factura_obj.invoice_open(cursor, uid, ids)
        remesa2 = remesa_obj.browse(cursor, uid, remesa2_id)

        ctx = {'model': 'giscegas.facturacio.factura'}
        wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
        wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

        wiz.write({
            'order': remesa2.id
        })
        ctx.update({'active_ids': ids})
        res = wz_afegir_factures.action_afegir_factures(
            cursor, uid, [wz_id], ctx
        )

        # Eliminem la factura 4 del llistat de remesades de l'assistent per
        # evitar que la sobreescrigui

        wiz.write({
            'remesades': [[6, 0, []]]
        })

        res = wz_afegir_factures.override_payment_order(
            cursor, uid, [wz_id], ctx
        )

        f1 = factura_obj.browse(cursor, uid, f1_id)
        f2 = factura_obj.browse(cursor, uid, f2_id)
        f3 = factura_obj.browse(cursor, uid, f3_id)
        f4 = factura_obj.browse(cursor, uid, f4_id)

        self.assertEqual(
            f1.payment_order_id.id,
            remesa2_id
        )
        self.assertEqual(
            f2.payment_order_id.id,
            remesa2_id
        )
        self.assertEqual(
            f3.payment_order_id.id,
            remesa2_id
        )
        self.assertEqual(
            f4.payment_order_id.id,
            remesa1_id
        )

    def test_validar_domini_factures_remesades(self):
        """Aquest test valida el domain del camp "remesades" many2many."""
        pool = self.openerp.pool
        factura_obj = pool.get('giscegas.facturacio.factura')
        remesa_obj = pool.get('payment.order')
        wz_afegir_factures = pool.get('giscegas.wizard.afegir.factures.remesa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        ids = []
        ids.append(imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0004_polissa_0001_N'
        )[1])
        ids.append(imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0005_polissa_0001_N'
        )[1])
        ids.append(imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1])

        remesa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_remeses', 'remesa_gas_0001'
        )[1]

        factura_obj.invoice_open(cursor, uid, ids)
        remesa = remesa_obj.browse(cursor, uid, remesa_id)

        ctx = {'model': 'giscegas.facturacio.factura'}
        wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
        wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

        wiz.write({
            'order': remesa.id
        })
        ctx.update({'active_ids': ids})
        res = wz_afegir_factures.action_afegir_factures(
            cursor, uid, [wz_id], ctx
        )
        # Tornem a afegir les factures a la mateixa remesa per forçar l'estat
        # "pendent" al wizard

        wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)
        wiz.write({
            'order': remesa.id
        })
        res = wz_afegir_factures.action_afegir_factures(
            cursor, uid, [wz_id], ctx
        )

        for elem in ids:
            self.assertIn(
                str(elem),
                wiz.factures_remesades_ids
            )


class TestChangeDatePlanned(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_update_date_plannned(self):
        """
        This test validate that when date planned is update on payment_order
        ours payment_line will update their date if payment_order date_prefered
        is "fixed"
        """
        pool = self.openerp.pool
        factura_obj = pool.get('giscegas.facturacio.factura')
        order_obj = pool.get('payment.order')
        pl_obj = pool.get('payment.line')
        wz_afegir_factures = pool.get('giscegas.wizard.afegir.factures.remesa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        ids = []
        ids.append(imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0004_polissa_0001_N'
        )[1])
        ids.append(imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0005_polissa_0001_N'
        )[1])
        ids.append(imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0006_polissa_0001_N'
        )[1])

        remesa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_remeses', 'remesa_gas_0001'
        )[1]

        factura_obj.invoice_open(cursor, uid, ids)
        remesa = order_obj.browse(cursor, uid, remesa_id)

        ctx = {'model': 'giscegas.facturacio.factura'}
        wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
        wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

        wiz.write({
            'order': remesa.id
        })
        ctx.update({'active_ids': ids})
        res = wz_afegir_factures.action_afegir_factures(
            cursor, uid, [wz_id], ctx
        )
        # read date from payment lines
        pl_ids = pl_obj.search(cursor, uid, [('order_id', '=', remesa_id)])

        # Change to 'due' date_prefered and not change original dates
        initial_dates = [
            x['date'] for x in pl_obj.read(cursor, uid, pl_ids, ['date'])
            ]
        remesa.write({'date_prefered': 'due', 'date_planned': '2017-01-10'})
        new_dates = [
            x['date'] for x in pl_obj.read(cursor, uid, pl_ids, ['date'])
        ]
        self.assertListEqual(initial_dates, new_dates)

        # Change to 'fixed' date_prefered and change original dates
        remesa.write({'date_prefered': 'fixed', 'date_planned': '2017-02-01'})
        new_dates = [
            x['date'] for x in pl_obj.read(cursor, uid, pl_ids, ['date'])
            ]
        spected_dates = ['2017-02-01' for x in new_dates]
        self.assertListEqual(spected_dates, new_dates)


class TestFeatures(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_get_invoice(self):
        pool = self.openerp.pool
        factura_obj = pool.get('giscegas.facturacio.factura')
        order_obj = pool.get('payment.order')
        period_obj = pool.get('account.period')
        wz_afegir_factures = pool.get('giscegas.wizard.afegir.factures.remesa')
        wz_agrupar = pool.get('giscegas.wizard.group.invoices.payment')
        bank_obj = pool.get('res.partner.bank')
        invoice_obj = pool.get('account.invoice')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user

        # First we get an invoice (factura_id) and we assign two bank_ids to
        # its partner
        factura_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_facturacio', 'factura_gas_0004_polissa_0001_N'
        )[1]
        factura = factura_obj.browse(cursor, uid, factura_id)
        partner = factura.partner_id
        bank_ids = bank_obj.search(cursor, uid, [])
        partner.write({'bank_ids':  [(6, 0, bank_ids)]})

        # Assign second bank_id to factura_id
        factura.write({'partner_bank': bank_ids[-1]})
        factura = factura_obj.browse(cursor, uid, factura_id)

        # Group the factura_id with another invoice
        # First, we want the two invoices we will choose to have the same
        # partner_bank than the first invoice. Otherwise, it will fail
        cursor.execute(*(
            factura_obj.q(cursor, uid).select(['invoice_id.partner_bank']).where([
                ('id', '=', factura_id)
            ])
        ))
        partner_bank = cursor.fetchone()[0]
        additional_invoices = [
            imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio',
                'factura_gas_0005_polissa_0001_N'
            )[1],
            imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio',
                'factura_gas_0006_polissa_0001_N'
            )[1]
        ]
        inv_additional_invoices = [
            fact['invoice_id'][0] for fact in factura_obj.read(
                cursor, uid, additional_invoices, ['invoice_id']
            )
        ]

        invoice_obj.write(cursor, uid, inv_additional_invoices, {
            'partner_bank': partner_bank
        })
        ids = [factura_id, additional_invoices[0]]

        period_id = period_obj.search(cursor, uid, [], limit=1)[0]
        factura_obj.write(cursor, uid, ids, {'period_id': period_id})
        factura_obj.invoice_open(cursor, uid, ids)
        ctx = {'active_ids': ids, 'model': 'giscegas.facturacio.factura'}
        wz_agrupar_id = wz_agrupar.create(cursor, uid, {}, ctx)
        wz_agrupar = wz_agrupar.browse(cursor, uid, wz_agrupar_id, ctx)
        wz_agrupar.group_invoices(context=ctx)

        # Make a payment order with a third invoice
        ids = [factura_id, additional_invoices[1]]
        remesa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_remeses', 'remesa_gas_0001'
        )[1]

        factura_obj.invoice_open(cursor, uid, ids)
        remesa = order_obj.browse(cursor, uid, remesa_id)

        ctx = {'model': 'giscegas.facturacio.factura'}
        wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
        wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

        wiz.write({
            'order': remesa.id
        })
        ctx.update({'active_ids': ids})
        wz_afegir_factures.action_afegir_factures(
            cursor, uid, [wz_id], ctx
        )

        # Check if payline of factura_id in the payment order has the correct
        # bank_id (the second of the partner, the one assigned to factura_id)
        found = False
        for line in remesa.line_ids:
            # If there is no invoice then it's the group of invoices
            if not line.move_line_id.invoice:
                found = True
                self.assertEqual(line.bank_id.id, factura.partner_bank.id)
        self.assertTrue(found)

    def test_update_mandate_partner(self):
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        res_partner_obj = pool.get("res.partner")
        agrolait_id = res_partner_obj.search(cursor, uid, [("name", "=", "Agrolait")])[0]
        res_partner_obj.browse(cursor, uid, agrolait_id).bank_ids[0].write({"iban": "ES2600000000000000", "default_bank": True})

        res = res_partner_obj.update_mandate(cursor, uid, agrolait_id)

        self.assertEquals(res.get("debtor_name"), "Agrolait")
        self.assertEquals(res.get("debtor_vat"), "ESB83677229")
        self.assertEquals(res.get("debtor_address"), "SYLVIE LELITRE, 69 RUE DE CHIMAY 5478 WAVRE")
        self.assertEquals(res.get("debtor_state"), "")
        self.assertEquals(res.get("debtor_country"), "BELGIUM")
        self.assertEquals(res.get("debtor_iban"), "ES7921000813610123456789")
        self.assertEquals(res.get("reference"), "res.partner,3")
