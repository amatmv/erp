# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _
from datetime import datetime
from collections import Counter
import json


_TYPES = [
    ('payable', 'A pagar'),
    ('receivable', 'A cobrar')
]


_STATES = [('init', 'Inicio'),
           ('pendents', 'Pendientes'),
           ('digest', 'Estadisticas'),
           ('end', 'Final')]
"""States of the wizard
"""


VALID_MODELS = [
    'giscegas.facturacio.lot',
    'giscegas.facturacio.factura',
]
"""Valid models to use this wizard
"""


class GiscegasWizardAfegirFacturesRemesa(osv.osv_memory):
    """Wizard per afegir factures a una remesa des del lot
    """
    _name = 'giscegas.wizard.afegir.factures.remesa'
    _description = __doc__

    def get_invoices(self, cursor, uid, context=None):
        """
        Gets selected invoices
        :return: list with invoices ids
        """
        if context is None:
            context = {}

        model = context.get('model')
        factura_ids = []
        if model == 'giscegas.facturacio.lot':
            fact_obj = self.pool.get('giscegas.facturacio.factura')
            lot_id = context.get('active_ids', False)
            if lot_id:
                lot_id = lot_id[0]
            factura_ids = fact_obj.search(
                cursor, uid, [('lot_facturacio', '=', lot_id)]
            )
        elif model == 'giscegas.facturacio.factura':
            factura_ids = context.get('active_ids', [])

        return factura_ids

    def default_over_limit_amount(self, cursor, uid, context=None):
        """
        Reads over limit amount config value
        :return: The valu¡e
        """
        conf_obj = self.pool.get('res.config')

        value = int(conf_obj.get(
            cursor, uid, 'add_to_payorder_over_limit_amount', 10000
        ))

        return value

    def get_grouped_invoices_ids(self, cursor, uid, fact_ids, context=None):
        result = []
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        for fact_id in fact_ids:
            group_move_id = fact_obj.read(cursor, uid, fact_id, ['group_move_id'])['group_move_id']
            if group_move_id:
                invoice_ids = fact_obj.search(
                    cursor, uid, [
                        ('group_move_id', '=', group_move_id[0]),
                        ('id', '!=', fact_id)
                    ]
                )
                result += invoice_ids
        return result

    def get_invoices_info(self, cursor, uid, invoice_ids, context=None):
        """
         Gets digest info of selected invoices to audit the result and solve
         errors before payment order creation
        :return: dict with statistical info
        {
            'total_num': Number of selected invoices
            'total_amount': Total invoices amount
            'max_amount': the highest amount of selected invoices
            'over_limit_invoices': Num invoices with amount over limit €
            'negative': Num invoices with negative amount
            'max_date_due': More recent date due
            'min_date_due': Oldest date due
            'states_digest': dict with per state invoice count
        }
        """
        if context is None:
            context = {}

        inv_obj = self.pool.get('giscegas.facturacio.factura')

        res = {
            'total_num': 0,
            'total_amount': 0.0,
            'max_amount': 0.0,
            'over_limit_invoices': 0,
            'negative': 0,
            'refund': 0,
            'max_due_date': '2010-01-01',
            'min_due_date': '2100-01-01',
            'origin_filled': 0,
            'states_digest': Counter(),
            'payment_type_digest': Counter(),
        }

        max_due_date = '2010-01-01'
        min_due_date = '2100-01-01'
        max_amount = 0.0

        invoice_fields = ['residual', 'date_due', 'state', 'type',
                          'payment_type', 'origin']
        grouped_ids = self.get_grouped_invoices_ids(
            cursor, uid, invoice_ids, context
        )
        invoices_to_check_ids = grouped_ids + invoice_ids
        invoices_vals = inv_obj.read(
            cursor, uid, invoices_to_check_ids, invoice_fields, context
        )
        over_limit_amount = self.default_over_limit_amount(
            cursor, uid, context=context
        )
        digest = Counter()
        for invoice_vals in invoices_vals:
            digest += Counter({
                'total_num': 1,
                'total_amount': invoice_vals['residual'],
                'over_limit_invoices': (
                    invoice_vals['residual'] > over_limit_amount and 1 or 0
                ),
                'negative': invoice_vals['residual'] < 0 and 1 or 0,
                'refund': (
                    invoice_vals['type'] in ('out_refund', 'in_refund') and 1
                    or 0
                ),
                'origin_filled': (
                    len(invoice_vals['origin'] or '') > 0 and 1 or 0
                ),
            })

            max_amount = max(max_amount, invoice_vals['residual'])
            max_due_date = max(max_due_date, invoice_vals['date_due'])
            min_due_date = min(min_due_date, invoice_vals['date_due'])

            if not invoice_vals['payment_type']:
                payment_type = _('No definit')
            else:
                payment_type = invoice_vals['payment_type'][1]

            res['states_digest'] += Counter({invoice_vals['state']: 1})
            res['payment_type_digest'] += Counter(
                {payment_type: 1}
            )

        res.update(
            dict(
                max_amount=max_amount,
                max_due_date=max_due_date,
                min_due_date=min_due_date,
            )
        )
        res.update(digest)
        return dict(res)

    def default_model(self, cursor, uid, context=None):
        if not context:
            context = {}

        model = context.get('model')

        if model not in VALID_MODELS:
            raise osv.except_osv(
                'Error',
                u'Modelo {0} no válido para usar este asistente'.format(
                    model
                )
            )

        return model

    def update_ibans_invoices(self, cursor, uid, factures_ids, context=None):
        if context is None:
            context = {}
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        polissa_obj = self.pool.get('giscegas.polissa')
        for factura in fact_obj.read(cursor, uid, factures_ids, ['polissa_id']):
            polissa_id = factura['polissa_id'][0]
            polissa_info = polissa_obj.get_current_bank(
                cursor, uid, polissa_id, context=context
            )
            if polissa_info['partner_bank']:
                fact_obj.write(cursor, uid, [factura['id']], polissa_info)

        return True

    def action_afegir_factures(self, cursor, uid, ids, context):
        """Afegeix les factures del lot a la remesa
        """
        if not context:
            context = {}
        remesades = False
        model = context.get('model')
        if model not in VALID_MODELS:
            raise osv.except_osv(
                'Error',
                u'Modelo {0} no válido para usar este asistente'.format(
                    model
                )
            )
        if isinstance(ids, list):
            ids = ids[0]
        wiz = self.browse(cursor, uid, ids, context)

        if model == 'giscegas.facturacio.lot':
            lot_id = context.get('active_ids', False)
            if lot_id:
                lot_id = lot_id[0]
            lot_obj = self.pool.get('giscegas.facturacio.lot')
            lot = lot_obj.browse(cursor, uid, lot_id, context)
            ctx = context.copy()
            if wiz.invoices_limit:
                ctx['invoices_limit'] = wiz.invoices_limit
            if wiz.not_ordered:
                ctx['not_ordered'] = wiz.not_ordered
            if wiz.use_order_payment_type:
                ctx['use_order_payment_type'] = wiz.use_order_payment_type
            if wiz.work_async:
                ctx['work_async'] = wiz.work_async
                info = _(u"Se ha iniciado el proceso de añadir facturas a la "
                         u"remesa en segundo plano.\nPuedes seguir su proceso "
                         u"desde el menú 'Tareas de Facturación'")
            else:
                info = ""
            try:
                lot.afegir_factures_remesa(wiz.order.id, context=ctx)
            except osv.except_osv as e:
                # It's the exeption raised when background is called, ignore it
                if e.name != u'Procéso en segundo plano':
                    raise e
            wiz.write({'state': 'end', 'info': info})

        elif model == 'giscegas.facturacio.factura':
            invoice_ids = context.get('active_ids', [])
            factura_obj = self.pool.get('giscegas.facturacio.factura')
            none_ids = factura_obj.search(
                cursor, uid,
                ['&', ('payment_order_id', '=', False),
                 ('id', 'in', invoice_ids)]
            )
            remesades_ids = list(set(invoice_ids) - set(none_ids))
            if remesades_ids:
                # Existeixen factures ja remesades
                str_list = []
                for elem in remesades_ids:
                    str_list.append(str(elem))
                remesades_ids_str = ','.join(str_list)
                remesades = True
                info = _(u"Hay facturas abiertas que ya tienen remesa. Sería "
                         u"conveniente revisar si són correctas.\n\n"
                         u"Las facturas incorrectas deben suprimirse del "
                         u"listado.")
                wiz.write({'remesades': [[6, 0, remesades_ids]],
                           'info': info,
                           'factures_remesades_ids': str(remesades_ids_str)})
                wiz.write({'state': 'pendents'})
            else:
                factura_obj.afegeix_a_remesa(
                    cursor, uid, invoice_ids, wiz.order.id
                )
                wiz.write({'state': 'end'})

    def show_payment_order(self, cursor, uid, ids, context=None):
        if isinstance(ids, list):
            ids = ids[0]
        wiz = self.browse(cursor, uid, ids, context)
        return {
            "type": "ir.actions.act_window",
            "name": _("Ordre de cobrament: {0}".format(wiz.order.name)),
            "res_model": "payment.order",
            "domain": [('id', '=', wiz.order.id)],
            "view_mode": "tree,form",
            "view_type": "form"
        }

    def get_groupable_invoices(self, cursor, uid, ids, context=None):
        factures_lot_ids = self.get_invoices(cursor, uid, context)

        fact_obj = self.pool.get('giscegas.facturacio.factura')

        fact_to_show_ids = []
        fields_to_read = ['partner_id', 'amount_total']
        for factura in fact_obj.read(cursor, uid, factures_lot_ids,
                                     fields_to_read):
            partner_id = factura['partner_id'] and factura['partner_id'][0]
            if partner_id:
                fact_id = factura['id']
                amount_total_actual = factura['amount_total']
                search_params = [
                    # que no tingui cap remesa
                    ('payment_order_id', '=', False),
                    # que no estigui pagada
                    ('reconciled', '=', False),
                    # que sigui del mateix partner
                    ('partner_id', '=', partner_id),
                    # amount_total + amount_total_actual > 0
                    ('amount_total', '>', -amount_total_actual),
                    # que no sigui la mateixa factura
                    ('id', '!=', fact_id)
                ]
                fact_to_show_ids += fact_obj.search(cursor, uid, search_params)

        fact_to_show_ids += factures_lot_ids
        fact_to_show_ids = list(set(fact_to_show_ids))

        res = {
            'name': _('Facturas agrupables'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscegas.facturacio.factura',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % str(tuple(fact_to_show_ids)),
        }

        return res

    def go_back(self, cursor, uid, ids, context):
        if not context:
            context = {}
        vals = {
            'state': 'init'
        }
        self.write(cursor, uid, ids, vals, context)

    def override_payment_order(self, cursor, uid, ids, context=None):
        if isinstance(ids, list):
            ids = ids[0]
        invoice_ids = context.get('active_ids', [])
        fact_obj = self.pool.get('giscegas.facturacio.factura')
        wiz = self.browse(cursor, uid, ids, context)
        none_ids = fact_obj.search(
            cursor, uid,
            ['&', ('payment_order_id', '=', False),
             ('id', 'in', invoice_ids)]
        )
        if none_ids:
            fact_obj.afegeix_a_remesa(cursor, uid, none_ids, wiz.order.id)
        for elem in wiz.remesades:
            if wiz.update_iban:
                self.update_ibans_invoices(
                    cursor, uid, [elem.id], context=context
                )
            fact_obj.afegeix_a_remesa(cursor, uid, elem.id, wiz.order.id)
        wiz.write({'state': 'end'})

    def onchange_tipus(self, cursor, uid, ids, tipus, context=None):
        res = {'domain': {}}
        res['domain'].update(
            {'order': [('type', '=', tipus), ('state', '=', 'draft')]}
        )
        return res

    _columns = {
        'order': fields.many2one('payment.order', 'Remesa', required=True),
        'state': fields.selection(_STATES, 'Estados', required=True),
        'tipus': fields.selection(_TYPES, 'Tipo', required=True),
        'factures_remesades_ids': fields.json('IDS facturas ya remesadas'),
        'info': fields.text(_('Información'), readonly=True),
        'remesades': fields.many2many(
            'giscegas.facturacio.factura', 'payment_order', 'payment_order_id',
            'id', 'Facturas ya remesadas'
        ),
        'model': fields.char('Origin model', size=255),
        # digest fields
        'total_num': fields.integer('Num. Facturas', readonly=True),
        'total_amount': fields.float(
            'Total Facturas', digits=(9, 3), readonly=True
        ),
        'max_amount': fields.float(
            'Factura más alta', digits=(9, 3), readonly=True
        ),
        'over_limit_invoices': fields.integer(
            'Facturas demasiado grandes', readonly=True
        ),
        'over_limit_amount': fields.float(
            'Límite facturas demasiado grandes [€]', digits=(9, 3), readonly=True
        ),
        'negative': fields.integer('Facturas negativas', readonly=True),
        'refund': fields.integer('Facturas abonadoras', readonly=True),
        'max_due_date': fields.date('Fecha de vencimiento máxima', readonly=True),
        'min_due_date': fields.date('Fecha de vencimiento mínima', readonly=True),
        'origin_filled': fields.integer(
            'Facturas con comentarios', readonly=True,
            help=u"Facturas que tienen el campo origin con contenido. En algunos "
                 u"casos se marcan las facturas a vigilar rellenando el campo "
                 u"origen"
        ),
        'states_digest': fields.text("Resum de estados", readonly=True),
        'payment_type_digest': fields.text("Resumen tipo de pago", readonly=True),
        'invoices_limit': fields.integer(
            'Límite de facturas',
            help=u'Indica el num. máximo de facturas a remesar. Si se deja a 0 '
                 u'se considerará sin límite de facturas'
        ),
        'not_ordered': fields.boolean(
            'No remesadas',
            help=u'Solamente selecciona facturas que no tengan remesa'
        ),
        'update_iban': fields.boolean(
            'Actualizar iban',
            help=u"Si se marca esta opción, se actualiza el IBAN de las "
                 u"facturas que ya estaban remesadas. "
        ),
        'work_async': fields.boolean(
            'Trabajo en segundo plano',
            help=u"Se realizará el proceso de añadir facturas a la remesa en "
                 u"segundo plano. Puedes seguir su progreso desde el menú "
                 u"'Tareas de Facturación'"
        ),
    }

    def default_get(self, cursor, uid, fields, context=None):
        """
        Sets Statistical fields values
        :return:
        """

        res = super(GiscegasWizardAfegirFacturesRemesa, self).default_get(
            cursor, uid, fields, context
        )

        if context.get('model') == 'giscegas.facturacio.lot':
            return res

        invoice_ids = self.get_invoices(cursor, uid, context=context)
        info = self.get_invoices_info(
            cursor, uid, invoice_ids, context=context
        )
        if info:
            res.update(info)
            states_digest_txt = '\n'.join(
                [': '.join([_(k), str(v)])
                 for k, v in info['states_digest'].items()]
            )
            payment_type_digest_txt = '\n'.join(
                [': '.join([k, str(v)])
                 for k, v in info['payment_type_digest'].items()
                 ]
            )

            res.update({
                'states_digest': states_digest_txt,
                'payment_type_digest': payment_type_digest_txt
            })

        return res

    _defaults = {
        'model': default_model,
        'tipus': lambda *a: 'receivable',
        'over_limit_amount': default_over_limit_amount,
        'state': lambda *a: 'init',
        'total_num': lambda *a: 0,
        'total_amount': lambda *a: 0.0,
        'max_amount': lambda *a: 0.0,
        'over_limit_invoices': lambda *a: 0,
        'negative': lambda *a: 0,
        'refund': lambda *a: 0,
        'max_due_date': lambda *a: '2010-01-01',
        'min_due_date': lambda *a: '2100-01-01',
        'origin_filled': lambda *a: 0,
        'invoices_limit': lambda *a: 0,
        'not_ordered': lambda *a: True,
        'work_async': lambda *a: False
    }

GiscegasWizardAfegirFacturesRemesa()
