# -*- coding: utf-8 -*-

from osv import osv
from base_extended.base_extended import MultiprocessBackground, NoDependency
from oorq.decorators import job, create_jobs_group
from autoworker import AutoWorker
from tools.translate import _


class GiscegasFacturacioFactura(osv.osv):
    """Modificació de la factura per poder-la afegir a una remesa
    """
    _name = 'giscegas.facturacio.factura'
    _inherit = 'giscegas.facturacio.factura'

    def afegeix_a_remesa(self, cursor, uid, ids, order_id, context=None):
        """Afegeix les factures 'ids' a la remesa 'order_id'
        """
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        inv_obj = self.pool.get('account.invoice')
        res = []
        fields_to_read = ['invoice_id']
        for fact in self.read(cursor, uid, ids, fields_to_read):
            res_fact = inv_obj.afegeix_a_remesa(
                cursor, uid, [fact['invoice_id'][0]], order_id, context=context
            )
            if res_fact:
                res.append(res_fact[0])
        return res

    @job(queue='add_invoices_to_remesa')
    def afegeix_a_remesa_async(self, cursor, uid, ids, order_id, context=None):
        return self.afegeix_a_remesa(cursor, uid, ids, order_id, context=context)

    def copy_data(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default.update({'payment_order_id': False})
        return super(GiscegasFacturacioFactura, self).copy_data(
            cr, uid, id, default, context
        )


GiscegasFacturacioFactura()


class GiscegasFacturacioLot(osv.osv):
    """Modificació del Lot per remesar les factures generades.
    """

    _name = 'giscegas.facturacio.lot'
    _inherit = 'giscegas.facturacio.lot'

    def afegir_factures_remesa(self, cursor, uid, ids, order_id, context=None):
        """Afegeix les factures generades pel lot a una remesa

        Filtra les factures d'acord amb la remesa destí.
        """
        if not context:
            context = {}
        if isinstance(ids, list):
            ids = ids[0]
        order_obj = self.pool.get('payment.order')
        fac_obj = self.pool.get('giscegas.facturacio.factura')
        order = order_obj.browse(cursor, uid, order_id)
        limit = context.get('invoices_limit', None)
        not_ordered = context.get('not_ordered', False)

        search_params = [('lot_facturacio', '=', ids),
                         ('type', '=', 'out_invoice'),
                         ('payment_type', '=', order.mode.type.id)]
        if 'payment_mode_id' in fac_obj.fields_get_keys(cursor, uid):
            search_params.append(('payment_mode_id', '=', order.mode.id))
        if not_ordered:
            search_params.append(('payment_order_id', '=', False))

        fids = fac_obj.search(
            cursor, uid, search_params, limit=limit, order='number, id'
        )

        if context.get("work_async"):
            self.afegir_factures_remesa_async(cursor, uid, fids, order_id, ids, context=context)
        else:
            fac_obj.afegeix_a_remesa(cursor, uid, fids, order_id, context)

    @MultiprocessBackground.background()
    def afegir_factures_remesa_async(self, cursor, uid, fids, order_id, lot_id, context=None):
        """Acció per afegir factures a la remesa"""
        _(u"Acción para añadir facturas a la remesa")
        order_obj = self.pool.get('payment.order')
        fac_obj = self.pool.get('giscegas.facturacio.factura')
        lot_obj = self.pool.get('giscegas.facturacio.lot')
        lot_info = lot_obj.read(cursor, uid, lot_id, ['name'])
        order_info = order_obj.read(cursor, uid, order_id, ['name'])
        jobs_ids = []
        with NoDependency():
            for fid in fids:
                j = fac_obj.afegeix_a_remesa_async(cursor, uid, [fid], order_id,
                                                   context)
                jobs_ids.append(j.id)
        create_jobs_group(
            cursor.dbname, uid,
            _(u'Facturación lote {} - añadiendo {} facturas a la remesa {}').format(
                lot_info['name'], len(fids), order_info['name']
            ), 'invoicing.add_invoices_to_remesa', jobs_ids
        )
        aw = AutoWorker(queue='add_invoices_to_remesa')
        aw.work()

GiscegasFacturacioLot()

