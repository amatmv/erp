# -*- coding: utf-8 -*-
{
    "name": "Factures Abonadores amb signe",
    "description": """Afegeix el signe negatiu a les factures abonadores""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends": [
        "account_invoice_refund_signed",
        "giscegas_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscegas_facturacio_view.xml"
    ],
    "active": False,
    "installable": True
}
