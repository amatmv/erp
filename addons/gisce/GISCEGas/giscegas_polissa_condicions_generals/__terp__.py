# -*- coding: utf-8 -*-
{
    "name": "Condiciones generales",
    "description": """Este módulo agrega las siguientes funcionalidades:
    * Condiciones generales pólizas
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends":[
        "base",
        "giscegas_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegas_polissa_condicions_generals_view.xml",
        "giscegas_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
