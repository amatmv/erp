from osv import osv, fields


class GiscegasPolissa(osv.osv):
    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    _columns = {
        'condicions_generals_id': fields.many2one(
            'giscegas.polissa.condicions.generals', 'Condiciones generales'
        )
    }


GiscegasPolissa()


class GiscegasPolissaModcontractual(osv.osv):
    _name = 'giscegas.polissa.modcontractual'
    _inherit = 'giscegas.polissa.modcontractual'

    _columns = {
        'condicions_generals_id': fields.many2one(
            'giscegas.polissa.condicions.generals', 'Condiciones generales'
        )
    }


GiscegasPolissaModcontractual()
