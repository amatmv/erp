# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscegasPolissaCondicionsGenerals(osv.osv):
    _name = 'giscegas.polissa.condicions.generals'

    _columns = {
        'name': fields.char(u'Descripción', size=256, required=True),
        'attachment_id': fields.many2one(
            'ir.attachment', 'Documento'
        )
    }


GiscegasPolissaCondicionsGenerals()
