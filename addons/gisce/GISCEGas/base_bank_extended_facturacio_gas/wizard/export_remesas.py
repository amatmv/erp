# -*- encoding: utf-8 -*-
from osv import osv

import sepa19_facturacio


class WizardExportPaymentFile(osv.osv_memory):
    _name = 'wizard.payment.file.spain'
    _inherit = 'wizard.payment.file.spain'

    def get_engine(self, cursor, uid, ids, tipo):
        csb = super(WizardExportPaymentFile, self).get_engine(
            cursor, uid, ids, tipo)

        if tipo == 'sepa19':
            csb = sepa19_facturacio.Sepa19Core()

        return csb

WizardExportPaymentFile()

