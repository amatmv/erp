# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *

from collections import namedtuple


class BaseBankExtendedFacturacioTests(testing.OOTestCase):

    def test_update_mandate(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner_obj = self.openerp.pool.get('res.partner')
        par_adr_obj = self.openerp.pool.get('res.partner.address')
        par_bnk_obj = self.openerp.pool.get('res.partner.bank')
        polissa_obj = self.openerp.pool.get('giscegas.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_id = partner_obj.create(
                cursor, uid,
                {
                    'name': 'Smith, John',
                    'vat': 'ES12291804Y'
                }
            )
            address_id = par_adr_obj.create(
                cursor, uid,
                {
                    'partner_id': partner_id,
                    'nv': 'Street',
                    'pnp': 'Num',
                    'street': 'Street'
                }
            )

            ctx = {'partner_id': partner_id}
            default_vals = par_bnk_obj.default_get(
                cursor, uid, ['owner_id', 'owner_address_id'], context=ctx)
            default_vals.update({
                'owner_address_id': address_id
            })
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]

            polissa = polissa_obj.browse(cursor, uid, polissa_id)

            default_vals.update({
                'state': 'iban',
                'iban': 'ES9431830800809999999999',
                'partner_id': polissa.pagador.id
            })

            bank_id = par_bnk_obj.create(
                cursor, uid, default_vals
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]

            polissa_obj.write(
                cursor, uid, [polissa_id], {'bank': bank_id}
            )
            vals = polissa_obj.update_mandate(cursor, uid, polissa_id)
            self.assertEqual(vals['debtor_name'], 'Smith, John')
            self.assertEqual(vals['debtor_vat'], 'ES12291804Y')
            self.assertEqual(vals['debtor_iban'], 'ES9431830800809999999999')

    def test_wizard_new_engine(self):
        wiz_obj = self.openerp.pool.get(
            'wizard.payment.file.spain'
        )
        from base_bank_extended_facturacio_gas.wizard.sepa19_facturacio import (
            Sepa19Core
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            wiz_id = wiz_obj.create(cursor, uid, {})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            csb = wiz_obj.get_engine(cursor, uid, wiz_id, 'sepa19')
            # Test from Wizard object
            self.assertTrue(isinstance(csb, Sepa19Core))
            # Test from Wizard browse
            csb = wiz.get_engine('sepa19')
            self.assertTrue(isinstance(csb, Sepa19Core))

    def test_correct_sepa_core_get_entity_returned(self):
        from base_bank_extended_facturacio.wizard.sepa19_facturacio import (
            Sepa19Core
        )
        from l10n_ES_remesas.wizard.sepa19 import Sepa19Core as Sepa19CoreBase
        from l10n_ES_remesas.wizard.converter import to_ascii

        Bank = namedtuple('MockBank', 'owner_name')
        Partner = namedtuple('MockPartner', 'name')
        bank = Bank('Smith, John')
        partner = Partner('Qwertyn Tarantino')

        line = {'bank_id': bank, 'partner_id': partner}

        sepa = Sepa19Core()
        value = sepa._get_debtor_entity(line)
        self.assertEqual(to_ascii(bank.owner_name), value)

        sepa = Sepa19CoreBase()
        value = sepa._get_debtor_entity(line)
        self.assertEqual(to_ascii(partner.name), value)
