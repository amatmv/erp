# -*- coding: utf-8 -*-
{
    "name": "Base Bank extended",
    "description": """
 * Add ResPartner as Owner
 * Add ResPartnerAddress as Owner Address """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules",
    "depends":[
        "base_bank_extended",
        "giscegas_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
