# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from ..defs import *
from datetime import datetime, timedelta


class TestGiscegasLectures(testing.OOTestCase):
    def test_inserta_lectures_copies_ajust_value(self):
        lectura_obj = self.openerp.pool.get('giscegas.lectures.lectura')
        compt_pool_obj = self.openerp.pool.get('giscegas.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            (periode_name, periode_id) = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'p1_v_tarifa_31_gas'
            )
            (comptador_name, comptador_id) = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'comptador_gas_0001'
            )

            (origen_name, origen_id) = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'origen_gas_1'
            )
            meter_fields = compt_pool_obj.read(
                cursor, uid, comptador_id, ['data_alta']
            )
            meter_fecha_alta = meter_fields['data_alta']

            data_mas_2 = datetime.strptime(meter_fields['data_alta'],
                                           '%Y-%m-%d') + timedelta(days=2)
            data_mas_2 = data_mas_2.strftime('%Y-%m-%d')

            vals = {
                'name': data_mas_2,
                'periode': (periode_id, periode_name),
                'lectura': 1,
                'comptador': (comptador_id, comptador_name),
                'observacions': '',
                'origen_comer_id': [None],
                'origen_id': (origen_id, origen_name),
                'ajust': 5,
                'motiu_ajust': MOTIUS_AJUST[0][0],
                'factor_k': 0.2,
                'pcs': 0.3,
                'tipo_lect_num': 'BR',
                'pressio_subministrament': 1,
            }

            res = compt_pool_obj.inserta_lectures(cursor, uid, [vals], [])
            assert res['lectures'] == 1
            lectura_id = lectura_obj.search(
                cursor, uid, [
                    ('name', '=', data_mas_2),
                    ('periode', '=', periode_id),
                    ('comptador', '=', comptador_id),
                    ('observacions', '=', ''),
                    ('origen_id', '=', origen_id),
                    ('ajust', '=', 5),
                    ('motiu_ajust', '=', MOTIUS_AJUST[0][0]),
                    ('lectura', '=', 1),
                    ('factor_k', '=', 0.2),
                    ('pcs', '=', 0.3),
                    ('tipo_lect_num', '=', 'BR'),
                ]
            )
            assert len(lectura_id) == 1
            lectura = lectura_obj.browse(cursor, uid, lectura_id[0])

            assert lectura.ajust == 5
            assert lectura.motiu_ajust == MOTIUS_AJUST[0][0]
            assert lectura.lectura == 1
            assert lectura.factor_k == 0.2

    def test_creating_giscegas_lectures_lectura_pool_has_default_ajust_0(self):
        lectura_obj = self.openerp.pool.get('giscegas.lectures.lectura.pool')
        imd_obj = self.openerp.pool.get('ir.model.data')
        meter_obj = self.openerp.pool.get('giscegas.lectures.comptador')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'p1_v_tarifa_31_gas'
            )[1]
            comptador_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'comptador_gas_0001'
            )[1]

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_lectures', 'origen_gas_1'
            )[1]

            vals = {
                'name': '2016-01-01',
                'periode': periode_id,
                'lectura': 10,
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
                'pressio_subministrament': 1,
                'factor_k': 1,
                'pcs': 1,
            }
            meter_fields = meter_obj.read(cursor, uid, comptador_id,
                                          ['data_alta'])

            with self.assertRaises(Exception) as ex_lectura_posterior:
                lectura_id = lectura_obj.create(cursor, uid, vals)
            self.assertIn(
                ('No se puede introducir lecturas posteriores a '
                 'la fecha de baja del contador'
                 ),
                ex_lectura_posterior.exception.message
            )

            meter_fecha_alta = meter_fields['data_alta']

            data_mas_2 = datetime.strptime(meter_fields['data_alta'], '%Y-%m-%d') + timedelta(days=2)
            data_mas_2 = data_mas_2.strftime('%Y-%m-%d')

            vals = {
                'name': data_mas_2,
                'periode': periode_id,
                'lectura': 10,
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
                'pressio_subministrament': 1,
                'factor_k': 1,
                'pcs': 1,
            }
            lectura_id = lectura_obj.create(cursor, uid, vals)
            lectura = lectura_obj.browse(cursor, uid, lectura_id)

            assert lectura.ajust == 0
            assert not lectura.motiu_ajust
