# -*- coding: utf-8 -*-
{
    "name": "Pool de Lectures Gas",
    "description": "",
    "version": "0-dev",
    "author": "Gisce-TI",
    "category": "GISCEMaster",
    "depends":[
        "giscegas_lectures",
        "giscegas_lectures_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegas_lectures_data.xml",
        "wizard/giscegas_lectures_pool_wizard.xml",
        "giscegas_lectures_view.xml",
        "security/ir.model.access.csv",
        "giscegas_lectures_pool_cronjobs.xml"
    ],
    "active": False,
    "installable": True
}
