# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields, orm
from oorq.decorators import job
from tools.translate import _
from tools import config

import pooler
import netsvc

from datetime import datetime, timedelta
from oorq.decorators import split_job
from oorq.oorq import JobsPool
from osconf import config_from_environment
from giscegas_lectures.giscegas_lectures import recalc_consums


class PoolLecturesLectura(osv.osv):
    """" Lectures del pool de lectures """
    _name = 'giscegas.lectures.lectura.pool'
    _inherit = 'giscegas.lectures.lectura'

    def __init__(self, pool, cursor):
        res = super(PoolLecturesLectura, self).__init__(pool, cursor)
        for i in range(len(self._sql_constraints)):
            constraint = self._sql_constraints[i]
            if constraint[0] == 'consum_positiu':
                del self._sql_constraints[i]
                break
        return res

    def _trg_consum(self, cursor, uid, ids, context=None):
        """Trigger que es cridarà per tal de tornar a calcular els consums.
        """
        if not context:
            context = {}
        if not context.get('trigger', True):
            return []
        lpool_o = self.pool.get("giscegas.lectures.lectura.pool")
        context.update({'active_test': False})
        ids_to_call = []
        for lectura in lpool_o.browse(cursor, uid, ids, context):
            search_params = [
                ('comptador.id', '=', lectura.comptador.id),
                ('name', '>=', lectura.name),
                ('periode.id', '=', lectura.periode.id),
            ]
            found_ids = lpool_o.search(cursor, uid, search_params, context=context)
            ids_to_call.extend(found_ids)
        # Uniq ids
        res = {}
        for fid in ids_to_call:
            res[fid] = 1
        return res.keys()

    def _consum(self, cursor, uid, ids, field_name, arg, context=None):
        """Calcula el consum pool i el guardarà a la base de dades.
        """
        lpool_o = self.pool.get("giscegas.lectures.lectura.pool")
        res = {}
        for lectura in lpool_o.browse(cursor, uid, ids, context):
            from_date = lectura.name
            tarifa_id = lectura.periode.tarifa.id
            periodes = [lectura.periode.id]
            consum_periode = lectura.comptador.get_consum_pool(tarifa_id, from_date, periodes=periodes, context=context)
            res[lectura.id] = consum_periode[lectura.periode.name]
        return res

    def _consum_m3(self, cursor, uid, ids, field_name, arg, context=None):
        """Calcula el consum pool i el guardarà a la base de dades.
        """
        lpool_o = self.pool.get("giscegas.lectures.lectura.pool")
        res = {}
        for lectura in lpool_o.browse(cursor, uid, ids, context):
            from_date = lectura.name
            tarifa_id = lectura.periode.tarifa.id
            periodes = [lectura.periode.id]
            consum_periode = lectura.comptador.get_consum_m3_pool(
                tarifa_id, from_date, periodes=periodes, context=context
            )
            res[lectura.id] = consum_periode[lectura.periode.name]
        return res

    _columns = {
        'consum': fields.function(_consum, method=True, string="Consum (kW)",
                                  type="float",
                                  store={'giscegas.lectures.lectura.pool': (
                                         _trg_consum, ['lectura', 'ajust', 'factor_k', 'pcs'], 10)}),
        'consum_m3': fields.function(_consum_m3, method=True, string="Consum (m3)",
                                  type="float",
                                  store={'giscegas.lectures.lectura.pool': (
                                         _trg_consum, ['lectura', 'ajust', 'factor_k', 'pcs'], 10)}),
    }

    _order = "name desc, periode asc"


PoolLecturesLectura()


class PoolLecturesComptador(osv.osv):
    """" Lectures del pool de lectures """
    _name = 'giscegas.lectures.comptador'
    _inherit = 'giscegas.lectures.comptador'

    def get_consum_pool(self, cursor, uid, comptador_id, tarifa_id, from_date,
                   per_facturar=False, periodes=None, context=None):
        """Calcula el consum de lectures de pool.

        En el cas que la lectura a facturar sigui real.
        """
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        consums = {}
        lectures = self.get_lectures_pool(cursor, uid, comptador_id, tarifa_id, from_date, per_facturar, periodes, context)
        for periode in lectures.keys():
            if lectures[periode].get('anterior'):
                act = lectures[periode]['actual']['lectura']
                ant = lectures[periode]['anterior']['lectura']
                consum = act - ant
            else:
                consum = 0

            if consum < 0:
                giro = self.pool.get("giscegas.lectures.comptador").read(cursor, uid, comptador_id, ['giro'])['giro']
                consum += giro

            consums[periode] = consum * lectures[periode]['actual'].get('factor_k', 0.0) * lectures[periode]['actual'].get('pcs', 0.0)
        return consums

    def get_consum_m3_pool(self, cursor, uid, comptador_id, tarifa_id, from_date,
                   per_facturar=False, periodes=None, context=None):
        """Calcula el consum.

        En el cas que la lectura a facturar sigui real.
        """
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        consums = {}
        lectures = self.get_lectures_pool(cursor, uid, comptador_id, tarifa_id, from_date, per_facturar, periodes, context)
        for periode in lectures.keys():
            if lectures[periode].get('anterior'):
                act = lectures[periode]['actual']['lectura']
                ant = lectures[periode]['anterior']['lectura']
                consum = act - ant
            else:
                consum = 0

            if consum < 0:
                giro = self.pool.get("giscegas.lectures.comptador").read(cursor, uid, comptador_id, ['giro'])['giro']
                consum += giro

            consums[periode] = consum
        return consums

    def get_lectures_pool(self, cursor, uid, comptador_id, tarifa_id, from_date,
                     per_facturar=False, periodes=None, context=None):
        """
        Busca les lectures pool actuals i anteriors.

        :return: {
            periode: {
                'actual': {
                    'consum': X,
                },
                'anterior': {
                    'consum': X,
                },
            }
        }
        """
        if not context:
            context = {}
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id)
        lectures = {}
        periodes_obj = self.pool.get('giscegas.polissa.tarifa.periodes')
        lectures_obj = self.pool.get('giscegas.lectures.lectura.pool')
        search_params = [
            ('tarifa.id', '=', tarifa_id),
            ('tipus', '=', 'te'),
        ]
        if not periodes:
            ctx = context.copy()
            ctx.update({'active_test': False})
            periodes_ids = periodes_obj.search(
                cursor, uid, search_params, context=ctx
            )
        else:
            periodes_ids = periodes[:]
        operator = '='
        if per_facturar:
            operator = '>'
        search_params = [
            ('comptador.id', '=', comptador_id),
            ('periode.id', 'in', periodes_ids),
            ('name', operator, from_date),
        ]
        # Si el comptador té data de baixa la lectura no pot ser més gran
        # que la data de baixa
        if comptador.data_baixa:
            search_params += [('name', '<=', comptador.data_baixa)]
        # Especial per les rectificatives, li podem dir fins a quina pot
        # arribar.
        if 'fins_lectura_fact' in context:
            search_params.extend([
                ('name', '<=', context['fins_lectura_fact'])
            ])
        lectures_ids = lectures_obj.search(cursor, uid, search_params, context={'active_test': False})
        # Si es per facturar cerquem si hi ha darreres lectures
        # per tindre-les en compte encara que hi hagin lectures posteriors
        # dins el mateix periode
        if per_facturar and not context.get('from_perfil', False):
            search_params.append(('origen_id.codi', '=', 'DR'))
            lectures_dl_ids = lectures_obj.search(cursor, uid, search_params, context={'active_test': False})
            if lectures_dl_ids:
                lectures_ids = lectures_dl_ids

        # En el cas que no hi hagi lectures ho inicialitzem pel validador
        if not lectures_ids:
            for periode in periodes_obj.browse(cursor, uid, periodes_ids):
                lectures[periode.name] = {}
                lectures[periode.name]['actual'] = {}
                lectures[periode.name]['anterior'] = {}
        for lectura in lectures_obj.browse(cursor, uid, lectures_ids, context):
            periode = lectura.periode.name
            # Si ja hem assignat la lectura per aqueset periode passem
            if periode in lectures:
                continue
            lectures[periode] = {}
            lectures[periode]['actual'] = lectures_obj.read(cursor, uid, lectura.id)
            # Té el mateix siginificat que False pq evalua així, però podem
            # fet .get(key, default)
            lectures[periode]['anterior'] = {}
            # posem l'order per defecte (no facturar)
            order = "name desc"
            search_params = [
                ('comptador.id', '=', comptador_id),
                ('periode.id', '=', lectura.periode.id),
                ('name', '<', lectura.name),
            ]
            # Si és per facturar mirem les lectures des d'on volem facturar
            # pel cas que tingui més d'una lectura en el mateix periode.
            # L'order = "name asc" ens assegura que vindran en el bon ordre
            if per_facturar:
                search_params.append(('name', '>=', from_date))
                order = "name asc"
            lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                               order=order,
                                               context={'active_test': False})
            if lectures_ids:
                lectures[periode]['anterior'] = lectures_obj.read(
                    cursor, uid, lectures_ids[0]
                )
                # En el cas que hi hagi més d'una lectura intermitja sumem
                # els consums
                for idx in range(1, len(lectures_ids)):
                    l_tmp = lectures_obj.browse(cursor, uid, lectures_ids[idx])
                    lectures[periode]['actual']['consum'] += l_tmp.consum
        return recalc_consums(lectures, comptador.giro)

    def get_lectura_estimada(self, cursor, uid, ids, data):
        return []

    def dates_limit(self, cursor, uid, comptador_id, context=None):
        """
        Busquem els dies limit
        """
        conf_obj = self.pool.get('res.config')
        min_dies = int(conf_obj.get(cursor, uid,
                                    'gas_pool_min_periode_entre_lectures', '0'))
        llindar_estimacio = int(conf_obj.get(cursor, uid,
                                             'gas_pool_llindar_estimacio_lectures',
                                             '1000'))
        return min_dies, llindar_estimacio

    def select_better_origen(self, cursor, uid, lectures):
        """ De totes les lectures, escull les millors en funció de l'origen.
            Pot rebre una llista de lectures provinent d'un read o els ids
            de les lectures
            Retorna la data de la lectura(es) que es consideren vàlides
        """
        if not lectures:
            return []
        pool_lect_obj = self.pool.get('giscegas.lectures.lectura.pool')
        if isinstance(lectures[0], int):
            lectures = pool_lect_obj.read(cursor, uid, lectures)

        # diferents dates
        dates = list(set([l['name'] for l in lectures]))

        return dates

    def inserta_lectures(self, cursor, uid, lectures,
                         pool=False, prefix_observacions='',
                         context=None):
        dies_lect = []
        lect_carregades = 0

        res = {'lectures': lect_carregades, 'dies': dies_lect}

        if pool:
            # Carreguem les lectures al Pool de lectures
            lect_obj = self.pool.get('giscegas.lectures.lectura.pool')
        else:
            # Carreguem les lectures al les lectures de facturació directament
            lect_obj = self.pool.get('giscegas.lectures.lectura')
        meter_obj = self.pool.get('giscegas.lectures.comptador')

        for lect in lectures:
            observacions = prefix_observacions + (lect.get('observacions', '') or '')
            # protecció gir de comptador
            id_comptador = lect['comptador'][0]
            meter_vals = meter_obj.read(cursor, uid, id_comptador, ['giro'])

            gir = int(meter_vals['giro'] or 0)
            lectura = lect['lectura']
            #Validem que la lectura sigui "coherent" amb el gir
            if gir <= lectura:
                gir = int('1' + '0' * len(str(lectura or 0)))
                meter_vals['giro'] = gir
                meter_obj.write(cursor, uid, id_comptador, {'giro': gir})
            if gir:
                lectura %= gir

            vals = {'comptador': id_comptador,
                    'name': lect['name'],
                    'periode': lect['periode'][0],
                    'lectura': lectura,
                    'observacions': observacions,
                    'origen_comer_id': lect['origen_comer_id'][0],
                    'origen_id': lect['origen_id'][0],
                    'ajust': lect.get('ajust', 0),
                    'motiu_ajust': lect.get('motiu_ajust', False),
                    'factor_k': lect.get('factor_k'),
                    'pcs': lect.get('pcs'),
                    'tipo_lect_num': lect.get('tipo_lect_num'),
                    'pressio_subministrament': lect.get('pressio_subministrament')
                    }
            # comprovem que no hi és
            search_vals = [('comptador', '=', vals['comptador']),
                           ('name', '=', vals['name']),
                           ('periode', '=', vals['periode'])
                           ]
            if not lect_obj.search(cursor, uid, search_vals):
                dies_lect += [vals['name']]
                lect_carregades += 1
                lect_obj.create(cursor, uid, vals)

        res.update({'lectures': lect_carregades, 'dies': list(set(dies_lect))})

        return res

    @split_job(queue='import_xml', result_ttl=24 * 3600)
    def get_lectures_from_pool(self, cursor, uid, ids, for_date=None, context=None):
        """Busca les lectures del pool a carregar segons un seguit de criteris
           establerts. De moment, totes les que faltin fins a la data passada"""

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        if context is None:
            context = {}

        logger = netsvc.Logger()

        polissa_obj = self.pool.get('giscegas.polissa')
        lect_obj = self.pool.get('giscegas.lectures.lectura')
        pool_lect_obj = self.pool.get('giscegas.lectures.lectura.pool')
        conf_obj = self.pool.get('res.config')

        # Controla si carreguem lectures noves o no (1:no, 0:si)
        no_new_lect = int(conf_obj.get(cursor, uid,
                                       'gas_pool_no_lectures_noves_per_facturar',
                                       '0'))

        if not for_date:
            for_date = datetime.today()
        else:
            for_date = datetime.strptime(for_date, '%Y-%m-%d')

        res = {'num_comptadors': len(ids), 'estimades': 0, 'carregades': 0,
               'for_date': for_date.strftime('%Y-%m-%d'), 'errors': 0,
               'comptadors': dict([(c, '') for c in ids])}

        comptadors_dades = self.read(cursor, uid, ids, ['polissa', 'name',
                                                        'data_baixa', 'active'])
        processats = 0
        for comptador_dades in comptadors_dades:
            db = pooler.get_db_only(cursor.dbname)
            nou_cr = db.cursor()
            try:
                comptador_id = comptador_dades['id']
                res['comptadors'][comptador_id] = ('%s: ' %
                                                   comptador_dades['name'])
                polissa_id = comptador_dades['polissa'][0]
                polissa_dades = polissa_obj.read(nou_cr, uid, polissa_id,
                                                 ['tarifa',
                                                  'data_ultima_lectura',
                                                  'data_alta', 'data_baixa',
                                                  'state', 'name'])

                min_dies, llindar_estimacio = self.dates_limit(cursor, uid,
                                                               comptador_id)

                if polissa_dades['state'] == 'esborrany':
                    txt = _(u'Polissa %s en esborrany') % polissa_dades['name']
                    res['comptadors'][comptador_id] += txt
                    continue

                dalta = datetime.strptime(polissa_dades['data_alta'], "%Y-%m-%d")
                dalta = dalta - timedelta(days=1)
                dalta = dalta.strftime("%Y-%m-%d")
                data_ult_lect_facturada = (
                    context.get('data_ultima_lectura', False) or
                    polissa_dades['data_ultima_lectura'] or dalta
                )

                if no_new_lect:
                    # Mirem si tenim lectures de facturació no facturades i si
                    # en tenim, no carreguem lectures
                    search_params = [
                        ('comptador.id', '=', comptador_id),
                        ('name', '>', data_ult_lect_facturada)
                    ]
                    lect_ids = lect_obj.search(nou_cr, uid, search_params,
                                               context={'active_test': False})
                    if lect_ids:
                        txt = _(u"S'han trobat %s lectures no facturades i "
                                u"no es permet carregar-ne de noves" %
                                len(lect_ids))
                        res['comptadors'][comptador_id] += txt
                        continue

                """Cerquem lectures de com a mínim min_dies dies posteriors a
                    la última lectura"""
                inici_cerca = (datetime.strptime(data_ult_lect_facturada,
                                                 '%Y-%m-%d')
                               + timedelta(days=min_dies)).strftime('%Y-%m-%d')

                """A partir de llindar_estimacio dies des de la última lectura
                   de la tarifa actual, es generarà una lectura estimada"""
                inici_estimacio = (datetime.strptime(data_ult_lect_facturada,
                                                     '%Y-%m-%d') +
                                   timedelta(days=llindar_estimacio)
                                   ).strftime('%Y-%m-%d')

                if inici_cerca > for_date.strftime('%Y-%m-%d'):
                    #Encara no cal carregar cap lectura
                    txt = _(u'Encara no han passat el mínim de dies (%s) des '
                            u'de la última lectura facturada' % min_dies)
                    res['comptadors'][comptador_id] += txt
                    continue

                # Mirem les modificacions contractuals per veure si tenim algun
                # canvi de tarifa.
                modcons = polissa_obj.get_modcontractual_intervals(
                    cursor, uid, polissa_id, data_ult_lect_facturada, for_date,
                    context={'ffields': ['tarifa']})

                inici_carrega = inici_cerca
                if modcons > 1:
                    #Busquem totes les tarifes de l'interval
                    for modcon in modcons.items():
                        '''On acaba la modificació contractual'''
                        data_tarifa = modcon[1]['dates'][1]
                        # Quan creem les lectures, buscarem a partir d'aquesta
                        # data
                        inici_carrega = min(data_tarifa, inici_carrega)

                search_params = [
                    ('comptador.id', '=', comptador_id),
                    ('name', '>=', inici_carrega),
                    ('name', '<=', for_date.strftime('%Y-%m-%d')),
                    ('name', '<=', inici_estimacio),
                ]

                lect_ids = pool_lect_obj.search(nou_cr, uid, search_params,
                                                context={'active_test': False})

                lectures = pool_lect_obj.read(nou_cr, uid, lect_ids, [])
                lect_dins_interval = [l for l in lectures
                                      if (inici_cerca <= l['name'] <=
                                          inici_estimacio)]
                dates_ok_carga = self.select_better_origen(nou_cr, uid,
                                                     lect_dins_interval)

                dates_dins_interval = [d for d in dates_ok_carga
                                       if inici_cerca <= d <= inici_estimacio]

                # Agafem totes les lectures excepte les extretes de dins
                # l'interval de càrrega
                dates_ok = [l['name'] for l in lectures
                            if l['name'] < inici_cerca]
                dates_ok += dates_ok_carga

                ''' Si no trobem lectura i ja hem passat la data limit
                    d'estimació inici_estimacio, estimem la lectura '''
                lectures_estimades = []
                if (not dates_dins_interval
                        and polissa_dades['state'] not in ['baixa']
                        and comptador_dades['active']):

                    if for_date.strftime('%Y-%m-%d') > inici_estimacio:
                        '''Estimem lectura'''
                        # Cerquem qualsevo[l lectura encara que estigui en el
                        # futur (fins a for_date)
                        lectures_estimades = self.get_lectura_estimada(cursor,
                            uid, comptador_id, for_date.strftime('%Y-%m-%d'))

                res['estimades'] += len(lectures_estimades)

                lectures = [l for l in lectures if l['name'] in dates_ok]
                lectures += lectures_estimades
                # Ordenem per dates. No faria falta però és més òptim
                sorted(lectures, key=lambda lectura: lectura['name'])
                #Ara ja podem crear les lectures
                prefix = u"From Pool."
                ins_res = self.inserta_lectures(nou_cr, uid, lectures,
                                                prefix_observacions=prefix)
                res['carregades'] += ins_res['lectures']

                if ins_res['lectures'] > 0:
                    txt_tmpl = _(u"Carregades %s lectures pels "
                                 u"dies '%s' (%s estimades)")
                    txt = txt_tmpl % (ins_res['lectures'],
                                      ','.join(list(set(ins_res['dies']))),
                                      len(lectures_estimades))
                else:
                    txt = _(u"No s'han carregat lectures")
                res['comptadors'][comptador_id] += txt
                nou_cr.commit()
            except Exception as e:
                txt_tmpl = _(u"Error carregant lectures a '%s': %s")
                txt = txt_tmpl % (comptador_dades['name'], str(e))
                res['comptadors'][comptador_id] += txt
                res['errors'] += 1
                nou_cr.rollback()
            finally:
                nou_cr.close()
                processats += 1
                marcador = "[%d/%d] " % (processats, len(res['comptadors']))
                logger.notifyChannel('carrega_lectures', netsvc.LOG_INFO,
                                     marcador + res['comptadors'][comptador_id])
        return res

    def processa_resultat_carrega(self, cursor, uid, ids, dades):

        if not dades:
            return ''

        txt_head = _(u'Data processat: %(for_date)s\n'
                     u'Processats %(num_comptadors)d comptadors\n'
                     u'Errors: %(errors)d\n'
                     u'Carregades: %(carregades)d\n'
                     u'Lectures estimades: %(estimades)d/%(num_comptadors)d\n'
                     u'Detall:\n\n') % dades

        detall = ''
        for comptador in dades['comptadors'].items():
            detall_tmp = _(u"* ['%s']: %s\n") % comptador
            detall += detall_tmp

        return txt_head + detall

    def _cronjob_lectures_load(self, cursor, uid, data=None, context=None):

        if not context:
            context = {}
        if not data:
            data = {}

        emails_to = filter(lambda a: bool(a),
                           map(str.strip, data.get('emails_to', '').split(',')))
        requests_to = filter(lambda a: bool(a),
                             map(str.strip,
                                 data.get('requests_to', '').split(',')))

        logger = netsvc.Logger()

        lot_obj = self.pool.get('giscegas.facturacio.lot')
        polissa_obj = self.pool.get('giscegas.polissa')
        modcon_obj = self.pool.get('giscegas.polissa.modcontractual')
        conf_obj = self.pool.get('res.config')

        #Busquem el lot actual
        lot_id = lot_obj.search(cursor, uid, [('state', '=', 'obert')])[0]
        lot_dades = lot_obj.read(cursor, uid, lot_id,
                                 ['data_inici', 'data_final', 'name',
                                  'contracte_lot_ids'])
        # Totes les pólisses dins el lot amb data_ultima_lectura abans de
        # min_dies o sense data última factura
        min_dies = int(conf_obj.get(cursor, uid,
                                    'gas_pool_min_periode_entre_lectures', '1'))
        limit = (datetime.today() - timedelta(days=min_dies)).strftime("%Y-%m-%d")
        polisses_ids = polissa_obj.search(cursor, uid,
                                          [('lot_facturacio', '=', lot_id),
                                           ('state', 'not in', ['esborrany']),
                                           ],
                                           context={'active_test': False})

        search_vals = [('polissa', 'in', polisses_ids)]
        comptadors_ids = self.search(cursor, uid, search_vals,
                                     context={'active_test': False})

        logger.notifyChannel('carrega_lectures', netsvc.LOG_INFO,
                             (u'Carregant lectures per %s comptadors' %
                              len(comptadors_ids)))
        async_mode = config_from_environment('OORQ').get('async', True)
        if async_mode:
            j_pool = JobsPool()
            jobs = self.get_lectures_from_pool(cursor, uid, comptadors_ids)
            for j in jobs:
                j_pool.add_job(j)
            j_pool.join()
            res = {}
        else:
            res = self.get_lectures_from_pool(
                cursor, uid, comptadors_ids
            ).result
        txt = self.processa_resultat_carrega(cursor, uid, comptadors_ids, res)

        subject_tmpl = _(u"Càrrega de lectures pel lot '%s' pel dia %s")
        subject = subject_tmpl % (lot_dades['name'], res['for_date'])

        message = subject + '\n'
        message += ('=' * len(subject)) + '\n\n'
        message += txt

        logger.notifyChannel('carrega_lectures', netsvc.LOG_INFO, txt)

        modcon_obj.send_action_message(cursor, uid, emails_to, requests_to,
                                       subject, message)

        return True

    _columns = {
        'pool_lectures': fields.one2many('giscegas.lectures.lectura.pool',
                                         'comptador', u'Pool Lectures'),
    }

PoolLecturesComptador()
