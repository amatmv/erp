
# -*- coding: utf-8 -*-

from osv import osv, fields, orm


class GiscegasFacturacioLotPool(osv.osv):
    """" Amplicació amb el progres de càrrega de lectures des del pool"""

    _name = "giscegas.facturacio.lot"
    _inherit = "giscegas.facturacio.lot"

    _columns = {
        'progres_lectures': fields.float('Procés Càrrega Lectures',
                                         readonly=True)
    }

    _defaults = {
        'progres_lectures': lambda *a: 0,
    }