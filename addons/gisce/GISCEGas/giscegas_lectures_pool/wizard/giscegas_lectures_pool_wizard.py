# -*- coding: utf-8 -*-
from collections import Counter

from osv import osv, fields, orm
from tools.translate import _
from datetime import datetime

import pooler
from oorq.oorq import JobsPool
from osconf import config_from_environment


class GiscegasLecturesPoolWizard(osv.osv_memory):
    """Wizard per facturar via Q1"""
    _name = 'giscegas.lectures.pool.wizard'

    def _default_model(self, cursor, uid, context=None):
        if not context:
            context = {}

        model = context.get('model', 'giscegas.lectures.comptador')

        return model

    def _default_info(self, cursor, uid, context=None):

        if not context:
            context = {}

        conf_obj = self.pool.get('res.config')

        min_lect = conf_obj.get(cursor, uid,
                                'gas_pool_min_periode_entre_lectures')
        inici_estim = conf_obj.get(cursor, uid,
                                   'gas_pool_llindar_estimacio_lectures')

        model = self.get_comptadors(cursor, uid, context=context)

        txt = _(u"Per el/la %s %s:\n"
                u'Aquest assistent carrega les lectures per facturar tenint '
                u'en compte els paràmetres de configuració\n'
                u'* Mínim entre lectures: %s dies\n'
                u'* Estimació de lectures: a partir de %s dies')

        return txt % (model['model'], model['instancia_txt'],
                      min_lect, inici_estim)

    def get_comptadors(self, cursor, uid, context):
        ''' En funció del model retorna els comptadors a calcular '''
        if not context:
            context = {}

        default_model = 'giscegas.lectures.comptador'

        model = context.get('model', default_model)
        ids_model = context.get('active_ids', [0])
        model_obj = self.pool.get(model)
        vals_model = model_obj.read(cursor, uid, ids_model[:5], ['name'])
        comptadors_ids = list(ids_model)

        instancies_txt = ','.join(["'" + x['name'] + "'" for x in vals_model])
        if len(ids_model) > 5:
            instancies_txt += '...'

        if model != default_model:
            comptadors_ids = model_obj.get_comptadors(cursor, uid, ids_model)

        return {'model': model, 'model_id': ids_model[:10],
                'comptadors': comptadors_ids,
                'instancia_txt': instancies_txt}

    def action_carrega_lectures(self, cursor, uid, ids, context=None):
        '''Carrega les lectures a les lectures de facturació segons
            pool i estimades'''

        wizard = self.browse(cursor, uid, ids[0])

        comptador_obj = self.pool.get('giscegas.lectures.comptador')

        dades = self.get_comptadors(cursor, uid, context)

        comptador_ids = dades['comptadors']

        async_mode = config_from_environment('OORQ').get('async', True)
        if async_mode:
            j_pool = JobsPool()
            jobs = comptador_obj.get_lectures_from_pool(
                cursor, uid, comptador_ids, wizard.date
            )
            for j in jobs:
                j_pool.add_job(j)
            j_pool.join()
            res = Counter()
            res['comptadors'] = {}
            for result in j_pool.results.values():
                aggr_k = ('num_comptadors', 'estimades', 'carregades', 'errors')
                for k in aggr_k:
                    res[k] += result[k]
                res['comptadors'].update(result['comptadors'])
                res['for_date'] = res['for_date']
        else:
            res = comptador_obj.get_lectures_from_pool(
                cursor, uid, comptador_ids, wizard.date
            )
        info = comptador_obj.processa_resultat_carrega(cursor, uid,
                                                       comptador_ids, res)
        wizard.write({'info': info,
                      'state': 'done'})

    _columns = {
        'state': fields.char('State', size=16),
        'date': fields.date('Data de càrrega'),
        'info': fields.text('Info'),
        'model': fields.text('Model contenidor'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'date': lambda *a: datetime.today().strftime('%Y-%m-%d'),
        'info': _default_info,
        'model': _default_model,
    }

GiscegasLecturesPoolWizard()


class GiscegasLecturesPoolAFActuracioWizard(osv.osv_memory):
    """Wizard copiar les lectures del pool a facturació"""
    _name = 'giscegas.wizard.copiar.lectura.pool.a.fact'

    def action_copia_lectura(self, cursor, uid, ids, context=None):
        ''' Copia la lectura del pool a facturació '''
        wizard = self.browse(cursor, uid, ids[0])

        overwrite = wizard.overwrite

        lecturapool_obj = self.pool.get("giscegas.lectures.lectura.pool")
        lectura_obj = self.pool.get("giscegas.lectures.lectura")

        comptador_obj = self.pool.get("giscegas.lectures.comptador")

        lectures_id = context.get('active_id', False)
        if lectures_id:
            lectura = lecturapool_obj.read(cursor, uid, lectures_id,
                                           ['comptador', 'name'])
            # busquem totes les lectures d'aquesta data
            search_vals = [('name', '=', lectura['name']),
                           ('comptador', '=', lectura['comptador'][0])]

            lectures_ids = lecturapool_obj.search(cursor, uid, search_vals)
            lectures_vals = lecturapool_obj.read(cursor, uid, lectures_ids)

            prefix = "Manual from Pool."
            res = comptador_obj.inserta_lectures(cursor, uid, lectures_vals,
                                                 prefix_observacions=prefix,
                                                 context=context)

            info_tmpl = _(u"S'han carregat %s lectures a "
                          u"lectures de facturació pel dia %s")
            info = info_tmpl % ("%s/%s" % (res['lectures'], len(lectures_ids)),
                                lectura['name'])
        else:
            info = _(u'No has escollit lectura')

        wizard.write({'state': 'end', 'info': info})

    def _default_info(self, cursor, uid, context=None):
        '''INformació de les lectures trobades'''
        lecturapool_obj = self.pool.get("giscegas.lectures.lectura.pool")

        lectures_id = context.get('active_id', False)
        if lectures_id:
            lectura = lecturapool_obj.read(cursor, uid, lectures_id,
                                           ['comptador', 'name'])
            # busquem totes les lectures d'aquesta data
            search_vals = [('name', '=', lectura['name']),
                           ('comptador', '=', lectura['comptador'][0])]

            lectures_ids = lecturapool_obj.search(cursor, uid, search_vals)


            info = _(u"S'han trobat %d lectures del dia %s al "
                     u"comptador %s. Vols copiar-les a les lectures de "
                     u"facturació?") % (len(lectures_ids),
                                         lectura['name'],
                                         lectura['comptador'][1])
        else:
            info = _(u'No has escollit lectura')

        return info

    def _default_comptador(self, cursor, uid, context=None):

        if not context:
            context = {}

        lectura_obj = self.pool.get("giscegas.lectures.lectura.pool")

        lectures_id = context.get('active_id', False)
        if not lectures_id:
            return ''

        lectura = lectura_obj.read(cursor, uid, lectures_id, ['comptador'])

        return lectura['comptador'][1]

    def _default_data(self, cursor, uid, context=None):

        if not context:
            context = {}

        lectura_obj = self.pool.get("giscegas.lectures.lectura.pool")

        lectures_id = context.get('active_id', False)
        if not lectures_id:
            return ''

        lectura = lectura_obj.browse(cursor, uid, lectures_id, ['name'])

        return lectura['name']

    _columns = {
        'state': fields.char('State', size=16),
        'data': fields.date('Data lectura'),
        'comptador': fields.char('Comptador', size=32),
        'info': fields.text('Info'),
        'overwrite': fields.boolean('Sobreescriu lectura si existeix')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'comptador': _default_comptador,
        'data': _default_data,
        'info': _default_info,
        'overwrite': lambda *a: False,
    }

GiscegasLecturesPoolAFActuracioWizard()
