# -*- coding: utf-8 -*-
{
    "name": "Callejero cups",
    "description": """Afegeix callejero al cups""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscegas_cups",
        "giscemisc_callejero"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegas_cups_view.xml",
    ],
    "active": False,
    "installable": True
}