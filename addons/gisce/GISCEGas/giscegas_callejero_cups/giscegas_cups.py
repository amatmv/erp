# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscegasCupsPs(osv.osv):

    _name = 'giscegas.cups.ps'
    _inherit = 'giscegas.cups.ps'

    def onchange_callejero(self, cursor, uid, ids, callejero_id, context=None):
        res = {'value': {}, 'warning': {}, 'domain': {}}
        if not callejero_id:
            return res
        callejero_obj = self.pool.get('giscemisc.callejero')
        state_obj = self.pool.get('res.municipi')
        callejero_reads = callejero_obj.read(
            cursor, uid, callejero_id,
            ['tipo_via', 'municipi', 'calle'],
            context=context
        )
        municipio_id = callejero_reads['municipi'][0]
        municipio = callejero_reads['municipi'] and callejero_reads['municipi'][
            1] or ''
        tipo_via = callejero_reads['tipo_via'][0]
        calle = callejero_reads['calle']
        state = state_obj.read(
            cursor, uid, municipio_id, ['state'], context=context
        )['state']
        country = False
        if state:
            state = state[0]
            state_obj = self.pool.get('res.country.state')
            contr = state_obj.read(
                cursor, uid, state, ['country_id'], context=context
            )['country_id']
            if contr:
                country = contr[0]
        res['value'].update(
            {'id_municipi': municipio_id,
             'tv': tipo_via,
             'nv': calle,
             'id_provincia': state
             }
        )
        return res

    _columns = {
        'callejero_id': fields.many2one('giscemisc.callejero', 'Callejero')
    }
    _defaults = {
        'callejero_id': lambda *a: False
    }


GiscegasCupsPs()
