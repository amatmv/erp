# -*- coding: utf-8 -*-
{
    "name": "Lecturas de contadores (Comercialitzadora)",
    "description": """Lecturas de los contadores""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends": [
        "base",
        "giscegas_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscegas_lectures_view.xml",
        "giscegas_lectures_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
