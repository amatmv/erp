# -*- coding: utf-8 -*-
from osv import osv, fields

class GiscegasLecturesComptador(osv.osv):

    _name = 'giscegas.lectures.comptador'
    _inherit = 'giscegas.lectures.comptador'
    
    _columns = {
        'name': fields.char('Nº de serie', size=32, required=True),
    }
    
GiscegasLecturesComptador()


class GiscegasLecturesOrigenComer(osv.osv):
    """Orígens de lectures per comercialitzadora
    """

    _name = 'giscegas.lectures.origen_comer'

    def get_origin_by_code(self, cursor, uid, code, context=None):
        """
        :param code: Origin code
        :return: Returns origin id
        """
        origin_ids = self.search(
            cursor, uid, [('codi', '=', code)], context=context
        )

        if not origin_ids:
            return False

        return origin_ids[0]

    _columns = {
        'name': fields.char('Nombre', size=64, required=True, translate=True),
        'codi': fields.char('Código', size=2, required=True),
    }

    _order = "codi asc"


GiscegasLecturesOrigenComer()


class GiscegasLecturesLectura(osv.osv):

    _name = 'giscegas.lectures.lectura'
    _inherit = 'giscegas.lectures.lectura' 
    
    def _get_default_origen(self, cursor, uid, context=None):
        """Retorna l'ID de l'orígen manual (codi MA)
        """
        origens = self.pool.get('giscegas.lectures.origen_comer')
        origen_id = origens.search(cursor, uid, [('codi', '=', 'MA')])
        if origen_id:
            return origen_id[0]
        return False

    _columns = {
        'origen_comer_id': fields.many2one('giscegas.lectures.origen_comer', 
                                           'Origen Comer.', required=True),
    }
    
    _defaults = {
        'origen_comer_id': _get_default_origen,
    }

GiscegasLecturesLectura()
