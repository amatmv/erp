# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from tools.translate import _

class GiscegasWizardRenombrarComptadorComer(osv.osv_memory):

    _name = 'giscegas.wizard.renombrar.comptador'
    _inherit = 'giscegas.wizard.renombrar.comptador'

    def action_renombrar(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])
        polissa = wizard.comptador_id.polissa
        if 'propi' in polissa.fields_get_keys() and polissa.propi:
            raise osv.except_osv('Error',
                                 _(u"No se puede renombrar un contador propio "
                                   u"desde comercializadora."))
        return super(
            GiscegasWizardRenombrarComptadorComer, self
        ).action_renombrar(cursor, uid, ids, context=context)

GiscegasWizardRenombrarComptadorComer()