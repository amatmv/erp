# -*- coding: utf-8 -*-
{
    "name": "GISCE Gas CUPS (Comercialitzadora)",
    "description": """Codi Universal de Punt de Provisionament""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CUPS",
    "depends":[
        "base",
        "giscegas_cups"
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "giscegas_cups_comer_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
