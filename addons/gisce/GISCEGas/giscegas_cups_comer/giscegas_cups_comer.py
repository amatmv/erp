# -*- coding: utf-8 -*-
"""Classes pel mòdul giscegas_cups (Comercialitzadora)."""
from osv import osv, fields
from tools.translate import _

class GiscegasCupsPs(osv.osv):
    """Classe d'un CUPS (Punt de servei)."""

    _name = 'giscegas.cups.ps'
    _inherit = 'giscegas.cups.ps'

    def whereiam(self, cursor, uid, context=None):
        '''retorna si estem a distri o a comer depenent de si el
            cups pertany a la nostra companyia o no'''
        return 'comer'

    _columns = {
        'distribuidora_id': fields.many2one(
            'res.partner', 'Distribuidora', required=True
        )
    }

GiscegasCupsPs()
