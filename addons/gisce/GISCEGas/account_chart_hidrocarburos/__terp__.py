# -*- coding: utf-8 -*-
{
    "name": "Account Chart Hidrocarburos",
    "description": """
    Mòdulo para crear las cuentas e impuestos especiales para el gas natural
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Account",
    "depends": [
        "base",
        "l10n_chart_ES",
        "account",
        "account_chart_update",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "taxes_data.xml",
    ],
    "active": False,
    "installable": True
}
