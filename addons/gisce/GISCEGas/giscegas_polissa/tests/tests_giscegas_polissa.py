# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
import utils
from osv import osv


class TestsGiscegasPolissa(testing.OOTestCase):

    def test_afegir_log_polissa(self):
        """ Aquest test comprova que s'ha creat correctament el log per al
         canvi d'estat de la pòlissa"""

        pool = self.openerp.pool

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            polissa_obj = pool.get('giscegas.polissa')
            model_obj = pool.get('ir.model.data')
            log_obj = pool.get('giscegas.polissa.log')

            polissa_id = model_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
            polissa_actual = polissa_obj.read(cursor, uid, polissa_id, ['state'])

            polissa_obj.create_log(
                cursor, uid, polissa_actual['id'], polissa_actual['state']
            )

            self.assertTrue(log_obj.search(cursor, uid, [
                ('polissa_id', '=', polissa_actual['id']),
                ('state', '=', polissa_actual['state']),
            ]))

    def test_duplicate_polissa(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]

            default_values = {
                'name': False,
                'modcontractuals_ids': [],
                'modcontractual_activa': False,
                'log_ids': [],
                'data_alta': False,
                'data_baixa': False,
                'data_firma_contracte': False,
            }

            copy_res_id = polissa_obj.copy(cursor, uid, polissa_id)
            polissa_data = polissa_obj.read(
                cursor, uid, copy_res_id, default_values.keys()
            )
            #  Check for the log_ids out of the default.
            #  Ensure only the ID of the creation log is there
            self.assertTrue(len(polissa_data['log_ids']) == 1)

            polissa_data.pop('id')
            polissa_data.pop('log_ids')
            default_values.pop('log_ids')
            self.assertDictEqual(default_values, polissa_data)

    def test_onchange_tarif_when_caudal_diari_is_0(self):
        # When caudal diari is 0, the onchange of the tarif should return
        # a new value for caudal diari.
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscegas.polissa')
        tarif_obj = self.openerp.pool.get('giscegas.polissa.tarifa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
            tarifa = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'tarifa_32_gas'
            )[1]
            tarif_info = tarif_obj.read(cursor, uid, tarifa, ["consumo_max"])
            cmax = round(tarif_info["consumo_max"] / 365.0, 7)

            contract_obj.write(cursor, uid, contract_id,
                               {"caudal_diario": 0, "tarifa": tarifa})
            res = contract_obj.onchange_tarifa(cursor, uid, contract_id, tarifa)
            self.assertEqual(res['value']["caudal_diario"], cmax)

    def test_onchange_tarif_when_caudal_diari_is_lower_than_consum_minim(self):
        # When caudal diari is 0, the onchange of the tarif should return
        # a new value for caudal diari.
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscegas.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]
            tarifa = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'tarifa_32_gas'
            )[1]

            contract_obj.write(cursor, uid, contract_id,
                               {"caudal_diario": 10, "tarifa": tarifa})
            res = contract_obj.onchange_tarifa(cursor, uid, contract_id, tarifa)
            self.assertFalse(res['value'].get("caudal_diario"))

    def test_activate_polissa_pressure_above_maximum(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            imd_obj = self.openerp.pool.get('ir.model.data')
            contract_obj = self.openerp.pool.get('giscegas.polissa')

            # Importing contract 1
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]

            # Make sure the pressure is above the maximum of the current tariff
            sql = contract_obj.q(cursor, uid).select(
                ['tarifa.presion_max']
            ).where([('id', '=', contract_id)])
            cursor.execute(*sql)
            vals = {'pressio': cursor.fetchone()[0] + 1}
            contract_obj.write(cursor, uid, contract_id, vals)

            # Let's activate de policy and expect catch the max exception
            def activate_polissa():
                utils.activar_polissa(pool, cursor, uid, contract_id)

            xpctd_mssg = ".*La presión no debe ser superior a la máxima de la tarifa.*"
            self.assertRaisesRegexp(osv.except_osv, xpctd_mssg, activate_polissa)

    def test_activate_polissa_negative_pressure(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            imd_obj = self.openerp.pool.get('ir.model.data')
            contract_obj = self.openerp.pool.get('giscegas.polissa')

            # Importing contract 1
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
            )[1]

            # Make sure the pressure is negative
            vals = {'pressio': -1}
            contract_obj.write(cursor, uid, contract_id, vals)

            # Let's activate de policy and expect catch the max exception
            def activate_polissa():
                utils.activar_polissa(pool, cursor, uid, contract_id)

            # Let's activate de policy and expect catch the negative exception
            contract_obj.write(cursor, uid, contract_id, vals)
            xpctd_mssg = ".*La presión debe ser estrictamente positiva.*"
            self.assertRaisesRegexp(osv.except_osv, xpctd_mssg, activate_polissa)
