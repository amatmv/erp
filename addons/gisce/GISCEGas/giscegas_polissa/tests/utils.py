# -*- coding: utf-8 -*-


def crear_modcon(pool, cursor, uid, polissa_id, values, ini, fi):
    polissa_obj = pool.get('giscegas.polissa')
    pol = polissa_obj.browse(cursor, uid, polissa_id)
    pol.send_signal(['modcontractual'])
    polissa_obj.write(cursor, uid, polissa_id, values)
    wz_crear_mc_obj = pool.get('giscegas.polissa.crear.contracte')

    ctx = {'active_id': polissa_id}
    params = {'duracio': 'nou'}

    wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
    wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
    res = wz_crear_mc_obj.onchange_duracio(
        cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio, ctx
    )
    wiz_mod.write({
        'data_inici': ini,
        'data_final': fi
    })
    wiz_mod.action_crear_contracte(ctx)

def crear_polissa(pool, cursor, uid):
    polissa_obj = pool.get('giscegas.polissa')
    modcon_obj = pool.get('giscegas.polissa.modcontractual')
    imd_obj = pool.get('ir.model.data')

    polissa_id = imd_obj.get_object_reference(
        cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
    )[1]
    pol = polissa_obj.browse(cursor, uid, polissa_id)
    modcon_id = pol.modcontractuals_ids[0].id
    modcon_obj.write(cursor, uid, modcon_id, {'data_final': '2018-03-01'})

    # Creem la segona modificació contractual fent un canvi de pressio

    crear_modcon(
        pool, cursor, uid, polissa_id, {
            'pressio': 3.000
        }, '2018-03-02', '2018-05-02'
    )

    # Creem la tercera modificació contractual fent un canvi de pressio

    crear_modcon(
        pool, cursor, uid, polissa_id, {
            'pressio': 3.250
        }, '2018-05-03', '2019-01-01'
    )

    return polissa_obj.browse(cursor, uid, polissa_id)

def activar_polissa(pool, cursor, uid, polissa_id):
    polissa_obj = pool.get('giscegas.polissa')

    polissa_obj.send_signal(cursor, uid, [polissa_id], [
        'validar', 'contracte'
    ])
