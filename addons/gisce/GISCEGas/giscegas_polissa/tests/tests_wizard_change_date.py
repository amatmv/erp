# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from datetime import datetime
from dateutil.relativedelta import relativedelta

from utils import *


class TestsWizardChangeDates(testing.OOTestCase):

    def setUp(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def tearDown(self):
        self.txn.stop()

    def to_date(self, date):
        """
        :param date: datetime in format: '%Y-%m-%d %H:%M:%S'
        :return: '%Y-%m-%d' formatted date
        """
        assert type(date) is str
        return datetime.strptime(date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')

    def test_data_baixa_modcon_es_modifica(self):
        """Aquest test comprova que la data de baixa de la modificació
            contractual és modificada per la nova data de baixa
            de l'assistent. També comprova que la data de baixa de la
            pòlissa (si en te) també es modifica (si es modifica la ultima
            modificació contractual)."""
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        pol = crear_polissa(pool, cursor, uid)
        wz_canviar_dates = pool.get('giscegas.wizard.canviar.dates')
        modcon = pol.modcontractuals_ids[0]
        ctx = {'active_id': modcon.id}

        wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)
        wiz.write({
            'data_final': '2019-02-01',
            'data_inici': '2018-04-03',
        })
        polissa_obj.write(cursor, uid, polissa_id, {
            'renovacio_auto': False,
            'data_baixa': '2019-01-01'
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id], ctx)
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon = pol.modcontractuals_ids[0]

        # Comprovació de resultats

        self.assertEqual(
            modcon.data_final,
            wiz.data_final
        )
        self.assertEqual(
            wiz.data_final,
            pol.data_baixa
        )

    def test_data_alta_modcon_es_modifica(self):
        """Aquest test comprova que la data d'alta de la modificació
        contractual és modificada per la nova data d'alta de l'assistent.
        També comprova que la data d'alta de la pòlissa és modificada si
        la modificació contractual modificaca és la primera."""
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        pol = crear_polissa(pool, cursor, uid)
        wz_canviar_dates = pool.get('giscegas.wizard.canviar.dates')
        modcon = pol.modcontractuals_ids[2]
        ctx = {'active_id': modcon.id}

        wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)
        wiz.write({
            'data_inici': '2018-02-01',
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id], ctx)
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon = pol.modcontractuals_ids[2]

        # Comprovació de resultats

        self.assertEqual(
            modcon.data_inici,
            wiz.data_inici
        )
        self.assertEqual(
            pol.data_alta,
            wiz.data_inici
        )

    def test_altres_modcons_no_es_solapen(self):
        """Aquest test comprova que les noves dates d'alta i baixa
        d'una modificació contractual no es solapen amb les respectives
        modificacions contractuals anterior i seguent. També comprova que
        la modificació contractual ha sigut modificada amb les dades
        entrades a l'assistent."""
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        pol = crear_polissa(pool, cursor, uid)
        wz_canviar_dates = pool.get('giscegas.wizard.canviar.dates')
        modcon = pol.modcontractuals_ids[1]
        ctx = {'active_id': modcon.id}

        wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)
        wiz.write({
            'data_final': '2018-07-02',
            'data_inici': '2018-04-01',
            'data_firma': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id], ctx)
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon = pol.modcontractuals_ids[1]

        data_modcon_ant = wiz.data_inici
        data_modcon_ant = datetime.strptime(data_modcon_ant, '%Y-%m-%d')
        data_modcon_ant += relativedelta(days=-1)
        data_modcon_ant = data_modcon_ant.strftime('%Y-%m-%d')

        data_modcon_seg = wiz.data_final
        data_modcon_seg = datetime.strptime(data_modcon_seg, '%Y-%m-%d')
        data_modcon_seg += relativedelta(days=1)
        data_modcon_seg = data_modcon_seg.strftime('%Y-%m-%d')

        # Comprovació de resultats

        self.assertEqual(
            modcon.data_final,
            wiz.data_final
        )
        self.assertEqual(
            modcon.data_inici,
            wiz.data_inici
        )
        self.assertEqual(
            data_modcon_ant,
            modcon.modcontractual_ant.data_final
        )
        self.assertEqual(
            data_modcon_seg,
            modcon.modcontractual_seg.data_inici
        )

    def test_data_firma_modcon_es_modifica(self):
        """Aquest test comprova que la data de firma de la modificació
        contractual és igual a la de l'assistent. També comprova
        que la data de firma del contracte es modifica (només si és la primera
        modificació contractual)."""
        pool = self.openerp.pool
        polissa_obj = pool.get('giscegas.polissa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'polissa_gas_0001'
        )[1]
        # Canviem la data de firma de la primera modificació contractual.
        # Com que no està activa, no ha de modificar la data de firma de la
        # pòlissa
        pol = crear_polissa(pool, cursor, uid)
        wz_canviar_dates = pool.get('giscegas.wizard.canviar.dates')
        modcon = pol.modcontractuals_ids[2]
        ctx = {'active_id': modcon.id}

        wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)
        wiz.write({
            'data_firma': '2018-03-21 00:00:00',
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id], ctx)
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon_primera = pol.modcontractuals_ids[2]

        # Canviem la data de firma de la ultima modificació contractual.
        # Com que és la modificació contractual activa, la data de
        # firma del contracte s'hauria de modificar a la pòlissa i
        # també a la modificació contractual.
        modcon_ultima = pol.modcontractuals_ids[0]
        ctx = {'active_id': modcon_ultima.id}

        wz_id_ultima = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz_ultima = wz_canviar_dates.browse(cursor, uid, wz_id_ultima, ctx)
        wiz_ultima.write({
            'data_firma': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id_ultima], ctx)
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon_ultima = pol.modcontractuals_ids[0]

        # Comprovació de resultats

        self.assertEqual(
            self.to_date(modcon_primera.data_firma_contracte),
            self.to_date(wiz.data_firma)
        )
        self.assertEqual(
            self.to_date(pol.data_firma_contracte),
            self.to_date(wiz_ultima.data_firma)
        )
        self.assertEqual(
            modcon_ultima.data_firma_contracte,
            wiz_ultima.data_firma
        )