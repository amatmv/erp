# -*- coding: utf-8 -*-
{
    "name": "Pólizas de los clientes de Gas (Base)",
    "description": """Añade las polizas de gas para clients """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends":[
        "base",
        "c2c_webkit_report",
        "giscegas_cups",
        "product",
        "jasper_reports",
        "giscemisc_cnae",
        "base_vat",
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegas_polissa_demo.xml"
    ],
    "update_xml": [
        "giscegas_polissa_workflow.xml",
        "wizard/giscegas_wizard_canviar_dates_view.xml",
        "giscegas_polissa_wizard.xml",
        "wizard/giscegas_wizard_crear_contracte_view.xml",
        "giscegas_polissa_view.xml",
        "partner_view.xml",
        "giscegas_polissa_data.xml",
        "giscegas_polissa_sequence.xml",
        "giscegas_polissa_cronjobs.xml",
        "wizard/giscegas_wizard_update_data_alta_view.xml",
        "wizard/giscegas_wizard_update_data_firma_contracte_view.xml",
        "giscegas_cups_view.xml",
        "security/giscegas_polissa_security.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
