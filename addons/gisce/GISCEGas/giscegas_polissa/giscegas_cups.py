# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscegasCupsPs(osv.osv):
    """Model de Punt de servei (CUPS)."""

    _name = 'giscegas.cups.ps'
    _inherit = 'giscegas.cups.ps'

    _polissa_not_in_states = ('validar', 'esborrany', 'cancelada', 'baixa')

    def _get_polissa(self, cursor, uid, cups_id):
        """Retorna la pòlissa activa del CUPS."""
        po_ids = self.pool.get('giscegas.polissa').search(cursor, uid,
                                [('cups', '=', cups_id),
                                 ('state', 'not in',
                                  self._polissa_not_in_states)])
        if po_ids:
            return self.pool.get('giscegas.polissa').browse(
                cursor, uid, po_ids[0]
            )
        else:
            return False

    def _polissa_client(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna l'abonat actual del CUPS."""
        res = {}
        cursor.execute("""\
select
  c.id,
  coalesce(pa.name, '')
from
  giscegas_cups_ps c
  left join giscegas_polissa p on (p.cups = c.id and p.active = True
                                    and p.state not in ('%s'))
  left join res_partner pa on (p.titular = pa.id)
where
  c.id in (%s)""" % ("','".join(self._polissa_not_in_states),
                     ','.join(map(str, map(int, ids)))))
        for cups in cursor.fetchall():
            res[cups[0]] = cups[1]
        return res

    def _polissa_client_search(self, cursor, uid, obj, name, args, context=None):
        """Permet buscar un CUPS segons abonat."""
        if not len(args):
            return []
        else:
            pol_ids = self.pool.get('giscegas.polissa').search(cursor, uid,
                [('titular', args[0][1], args[0][2]), ('cups', '!=', False)])
            if not len(pol_ids):
                return [('id', '=', '0')]
            else:
                ids = []
                for polissa in self.pool.get('giscegas.polissa').browse(cursor,
                                                                uid, pol_ids):
                    ids.append(polissa.cups.id)
                return [('id', 'in', ids)]

    def _polissa_polissa(self, cursor, uid, ids, field_name, arg, context):
        """Número de pòlissa actual del CUPS."""
        res = {}
        # TODO GAS: millorar format dels strings
        cursor.execute("""\
            select
              c.id,
              p.id,
              p.name
            from
              giscegas_cups_ps c
              left join giscegas_polissa p on (
                p.cups = c.id and p.active = True and p.state not in ('%s'))
            where c.id in (%s)""" % ("','".join(self._polissa_not_in_states),
                                     ','.join(map(str, map(int, ids)))))
        for cups in cursor.fetchall():
            if cups[1] and cups[2]:
                res[cups[0]] = (cups[1], cups[2])
            elif cups[1]:
                res[cups[0]] = (cups[1], 'ID: %i' % cups[1])
            else:
                res[cups[0]] = False
        return res

    def _get_active_polissa_id(self, cr, uid, ids, context=None):
        if not context:
            context = {}

        polissa_obj = self.pool.get('giscegas.polissa')
        polissa_vals = polissa_obj.read(cr, uid, ids, ['cups'])
        return [x['cups'][0] for x in polissa_vals if x.get('cups', False)]

    _STORE_POLISSA = {
        'giscegas.polissa': (
            _get_active_polissa_id, ['cups', 'state'], 10
        ),
        'giscegas.cups.ps': (
            lambda self, cursor, uid, ids, c=None: ids, [], 20
        )
    }

    _columns = {
        'polissa_polissa': fields.function(
            _polissa_polissa, method=True, type='many2one',
            relation='giscegas.polissa', store=_STORE_POLISSA, string='Póliza'
        ),
        'polissa_pressio': fields.related(
            'polissa_polissa', 'pressio',
            string="Presión contratada (bares)",
            type='float', readonly=True,
        ),
        'polisses': fields.one2many(
            'giscegas.polissa.modcontractual', 'cups', 'Histórico Pólizas',
            context={'active_test': False}, readonly=True
        ),
    }


GiscegasCupsPs()
