# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class ResPartnerPolissa(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    def deactivate_partner(self, cr, uid, ids, context=None):
        '''funcio que desactiva un partner'''

        polissa_obj = self.pool.get('giscegas.polissa')
        invoice_obj = self.pool.get('account.invoice')        

        for partner in self.browse(cr, uid, ids):
            #comprobamos que no este ni como titular, ni como pagador 
            #ni como notificacion de ninguna poliza
            search_params = [('titular','=', partner.id),]
            polissa_ids = polissa_obj.search(cr, uid, search_params)
            if polissa_ids:
                raise osv.except_osv('Error',
                        _(u"El cliente %s está como titular "
                          u"de algun contrato" %(partner.name)))
            search_params = [('pagador','=', partner.id),]
            polissa_ids = polissa_obj.search(cr, uid, search_params)
            if polissa_ids:
                raise osv.except_osv('Error',
                         _(u"El cliente %s está como pagador "
                           u"de algun contrato" %(partner.name)))
            search_params = [('altre_p','=', partner.id),]
            polissa_ids = polissa_obj.search(cr, uid, search_params)
            if polissa_ids:
                raise osv.except_osv('Error',
                          _(u"El cliente %s está como notificación "
                            u"de algun contrato" %(partner.name)))
            
            for address in partner.address:
                address.write({'active': False})
            partner.write({'active': False})
    
        return True

    def _fnc_polisses_ids(self, cursor, uid, ids, field_name, arg,
                          context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        polissa_obj = self.pool.get('giscegas.polissa')
        for partner_id in ids:
            res[partner_id] = polissa_obj.search(cursor, uid, [
                '|',
                '|',
                ('titular', '=', partner_id),
                ('pagador', '=', partner_id),
                ('altre_p', '=', partner_id)
            ])

        return res

    def _fnc_polisses_ids_search(self, cursor, uid, obj, name, args,
                                 context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        else:
            operator = args[0][1]
            polissa_id = args[0][2]
            polissa_obj = self.pool.get('giscegas.polissa')
            fields_to_read = ['titular', 'pagador', 'altre_p']
            polissa = polissa_obj.read(cursor, uid, polissa_id, fields_to_read)
            partner_ids = list(
                set(polissa[f][0] for f in fields_to_read if polissa[f])
            )
            return [('id', 'in', partner_ids)]

    _columns = {
        'polisses_gas_ids': fields.function(
            _fnc_polisses_ids, method=True, type='one2many',
            relation='giscegas.polissa', fnct_search=_fnc_polisses_ids_search
        )
    }

ResPartnerPolissa()


class ResPartnerAddress(osv.osv):
    """Sobreescrivim mètode name_get
    """
    _name = 'res.partner.address'
    _inherit = 'res.partner.address'

    def name_get(self, cursor, user, ids, context=None):
        """Afegim el camp street2.
        """
        if not len(ids):
            return []
        if not context:
            context = {}
        res = []
        for r in self.read(cursor, user, ids, ['name', 'zip', 'city',
                                               'partner_id', 'street',
                                               'street2']):
            if context.get('contact_display', 'contact') == 'partner' and \
            r['partner_id']:
                res.append((r['id'], r['partner_id'][1]))
            else:
                addr = r['name'] or ''
                if r['name'] and (r['zip'] or r['city']):
                    addr += ', '
                addr += (r['street'] or '') +' '+(r['street2'] or '')
                addr += ' ' + (r['zip'] or '') + ' ' + (r['city'] or '')
                res.append((r['id'], addr.strip() or '/'))
        return res

ResPartnerAddress()
