# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class GiscegasWizardUpdateDataAlta(osv.osv_memory):

    _name = 'giscegas.wizard.update.data.alta'

    def default_polissa_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('active_id', False)

    def check_we_can(self, cursor, uid, ids, context=None):
        '''do some checks prior to update the date'''

        modcont_obj = self.pool.get('giscegas.polissa.modcontractual')
        factura_obj = self.pool.get('giscegas.facturacio.factura')
        if factura_obj is None:
            return True
        wizard = self.browse(cursor, uid, ids[0])

        # As much as one modcontractual
        search_params = [('polissa_id', '=', wizard.polissa_id.id)]
        modcontractual_number = modcont_obj.search_count(cursor, uid,
                                                         search_params,
                                                         context=context)
        if modcontractual_number > 1:
            raise osv.except_osv(_('Error'),
                                 _(u"No se puede actualizar un contrato "
                                   u"con más de una modificación contractual."))

        search_params.append(('invoice_id.journal_id.code', '=', 'GAS'))

        # No invoices allowed
        factura_number = factura_obj.search_count(cursor, uid,
                                                  search_params,
                                                  context=context)
        if factura_number > 0 and wizard.check_invoices:
            raise osv.except_osv(_('Error'),
                                 _(u"No se puede actualizar una póliza con "
                                   u"facturas generadas."))
        return True

    def action_update_data(self, cursor, uid, ids, context=None):
        '''update data_alta in polissa
        and data_inici in modcontractual'''

        modcont_obj = self.pool.get('giscegas.polissa.modcontractual')
        wizard = self.browse(cursor, uid, ids[0])
        polissa = wizard.polissa_id

        if context is None:
            context = {}

        context.update({'sync': False,
                        'active_test': False})
        # Do some checks
        self.check_we_can(cursor, uid, ids, context=context)

        polissa.write({'data_alta': wizard.data_alta},
                      context=context)
        # Update data_inici and data_final in modcontractual
        if polissa.modcontractual_activa:
            modcontractual = polissa.modcontractual_activa
            data_inici = wizard.data_alta
            data_final = modcont_obj.get_data_final(cursor, uid,
                                                    data_inici, context)
            modcontractual.write({'data_inici': data_inici,
                                  'data_final': data_final},
                                  context=context)

        wizard.write({'state': 'end'})

    _columns = {
        'data_alta': fields.date('Fecha alta'),
        'polissa_id': fields.many2one('giscegas.polissa', 'Contrato'),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')], 'State'),
        'check_invoices': fields.boolean('Comprovar factures',
                            help=u"Desmarque la casilla si no quiere comprobar "
                                 u"si el contrato tiene facturas emitidas."),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'check_invoices': lambda *a: True,
        'polissa_id': default_polissa_id
    }


GiscegasWizardUpdateDataAlta()
