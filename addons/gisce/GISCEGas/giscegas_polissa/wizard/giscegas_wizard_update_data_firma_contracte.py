# -*- encoding: utf-8 -*-

from osv import osv, fields


class GiscegasWizardUpdateDataFirmaContracte(osv.osv_memory):

    _name = 'giscegas.wizard.update.data.firma.contracte'

    def default_polissa_id(self, cursor, uid, context=None):
        '''Llegim la pólissa'''
        polissa_id = False
        if context:
            polissa_id = context.get('active_id', False)

        return polissa_id

    def default_data_firma_contracte(self, cursor, uid, context=None):
        ''' Agafem la data firma contracte actual'''
        data_firma_contracte = False
        if context:
            polissa_id = context.get('active_id', False)
            if polissa_id:
                pol_obj = self.pool.get('giscegas.polissa')
                pol_vals = pol_obj.read(cursor, uid, polissa_id,
                                        ['data_firma_contracte'])
                data_firma_contracte = pol_vals['data_firma_contracte']

        return data_firma_contracte

    def action_update_data_firma_contracte(self, cursor, uid, ids,
                                           context=None):
        '''update data_firma_contracte in polissa
        and data_inici in modcontractual'''

        wizard = self.browse(cursor, uid, ids[0])
        polissa = wizard.polissa_id

        if not context:
            context = {}
        context.update({'sync': False,
                        'active_test': False})
        data_firma = wizard.data_firma_contracte
        polissa.write({'data_firma_contracte': data_firma},
                      context=context)
        # Update data_firma_contracte in modcontractual
        if polissa.modcontractual_activa:
            modcontractual = polissa.modcontractual_activa
            modcontractual.write({'data_firma_contracte': data_firma},
                                 context=context)

        wizard.write({'state': 'end'})

    _columns = {
        'data_firma_contracte': fields.datetime('Fecha firma contrato'),
        'polissa_id': fields.many2one('giscegas.polissa', 'Póliza'),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')], 'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'polissa_id': default_polissa_id,
        'data_firma_contracte': default_data_firma_contracte
    }


GiscegasWizardUpdateDataFirmaContracte()
