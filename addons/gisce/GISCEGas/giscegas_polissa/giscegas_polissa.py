# -*- coding: utf-8 -*-
"""Modul giscegas_polissa (Base)."""

from osv import osv, fields
from tools.translate import _
from tools import email_send, cache
from gestionatr.defs_gas import (
    TAULA_TIPOS_DE_USO_DEL_GAS, TAULA_PRESION_COMUNITARIA
)
from datetime import date, datetime, timedelta
import calendar
import netsvc
import time
import pooler
from gestionatr.defs import TABLA_9
from osv.expression import OOQuery


CONTRACT_STATES = [
    ('esborrany', 'Borrador'),
    ('validar', 'Validar'),
    ('pendent', 'Pendiente'),
    ('activa', 'Activa'),
    ('cancelada', 'Cancelada'),
    ('contracte', 'Activación Contrato'),
    ('novapolissa', 'Creación nueva póliza'),
    ('modcontractual', 'Modificación Contractual'),
    ('impagament', 'Impago'),
    ('tall', 'Corte'),
    ('baixa', 'Baja')
]

CONTRACT_LOGGABLE_STATES = [
    'esborrany',
    'validar',
    'pendent',
    'activa',
    'impagament',
    'modcontractual',
    'cancelada',
    'tall',
    'baixa'
]

DEFAULT_STATES_MODCON = {
    'esborrany': [
        ('readonly', False)
    ],
    'validar': [
        ('readonly', False),
        ('required', True)
    ],
    'modcontractual': [
        ('readonly', False),
        ('required', True)
    ]
}

CONTRACT_IGNORED_STATES = [
    'validar',
    'esborrany',
    'cancelada'
]

# TODO GAS: Distintos tipos de contrato para los clientes de Gas?
CONTRACT_TYPES = TABLA_9


TAULA_TIPOS_DE_USO_DEL_GAS_REDUCIDA = [
    (str(n + 1).zfill(2), tipo[1]) for n, tipo in enumerate(
        sorted(set(TAULA_TIPOS_DE_USO_DEL_GAS), key=lambda elem: elem[1])
    )
]


class GiscegasPolissaTarifa(osv.osv):
    """Tarifa d'accés de les pòlisses."""
    _name = 'giscegas.polissa.tarifa'
    _description = "Tarifa d'acces del client"

    def get_tarifa_from_ocsum(self, cursor, uid, codi_ocsum, tarifa_lb=False,
                              context=None):
        """Retorna la tarifa a partir del codi ocsum"""
        search_vals = [('codi_ocsum', '=', codi_ocsum)]
        tarifa_id = self.search(cursor, uid, search_vals)

        if not tarifa_id:
            return False

        if tarifa_id:
            tarifa_id = tarifa_id[0]

        return tarifa_id

    def get_num_periodes(self, cursor, uid, ids, tipus='te', context=None):
        """Retorna el número de periodes reals (no agrupats de la tarifa).
        """
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'active_test': False})
        periodes_obj = self.pool.get('giscegas.polissa.tarifa.periodes')
        for tid in ids:
            search_params = [('tarifa.id', '=', tid),
                             ('tipus', '=', tipus)]
            if context.get('agrupar_periodes', True):
                search_params.extend([('agrupat_amb', '=', False)])
            return periodes_obj.search_count(cursor, uid, search_params,
                                             context=ctx)

    def get_periodes(self, cursor, uid, tarifa_id, tipus='te', context=None):
        """Retorna un diccionari amb Periode -> Id.
        """
        tipus_valids = ('tp', 'te')
        if tipus not in tipus_valids:
            txt = _(u'El tipo solo puede ser: {0}').format(
                ','.join(tipus_valids)
            )
            raise osv.except_osv(_(u'Tipo no válido'), txt)
        if context is None:
            context = {}
        periodes = {}
        if isinstance(tarifa_id, list) or isinstance(tarifa_id, tuple):
            tarifa_id = tarifa_id[0]
        tarifa = self.browse(cursor, uid, tarifa_id, context)
        for periode in tarifa.periodes:
            if periode.tipus != tipus:
                continue
            periodes[periode.name] = periode.id
        return periodes

    def get_periodes_producte(self, cursor, uid, tarifa_id, tipus,
                              context=None):
        """Retorna un diccionari amb Periode -> Id producte.
        """
        tipus_valids = ('tp', 'te')
        if tipus not in tipus_valids:
            raise osv.except_osv(
                _(u'Tipo no válido'),
                _(u'El tipo solo puede ser: %s') % ','.join(tipus_valids)
            )
        productes = {}
        if isinstance(tarifa_id, list) or isinstance(tarifa_id, tuple):
            tarifa_id = tarifa_id[0]
        tarifa = self.browse(cursor, uid, tarifa_id, context)
        for periode in tarifa.periodes:
            if periode.tipus == tipus:
                productes[periode.name] = periode.product_id.id
        return productes

    def get_grouped_periods(self, cursor, uid, tarifa_id, context=None):
        if isinstance(tarifa_id, list) or isinstance(tarifa_id, tuple):
            tarifa_id = tarifa_id[0]
        tarifa = self.browse(cursor, uid, tarifa_id, context)
        grouped = {}
        for periode in tarifa.periodes:
            if periode.agrupat_amb:
                grouped[periode.name] = periode.agrupat_amb.name
        return grouped

    _columns = {
        'name': fields.char('Código', size=10, required=True),
        'descripcio': fields.text('Descripción'),
        'periodes': fields.one2many(
            'giscegas.polissa.tarifa.periodes', 'tarifa', 'Períodos'
        ),
        'codi_ocsum': fields.char('Código OCSUM', size=3, readonly=True),
        'autogen_periodes_pot': fields.boolean('Autogenerar períodos'),
        'consumo_min': fields.float('Consumo min. (kWh/año)'),
        'consumo_max': fields.float('Consumo max. (kWh/año)'),
        'presion_min': fields.float('Presión min. (bares)'),
        'presion_max': fields.float('Presión max. (bares)'),
        'active': fields.boolean('Activa'),
    }

    _defaults = {
        'autogen_periodes_pot': lambda *a: True,
        'active': lambda *a: 1,
    }

    _order = "name asc"

    _sql_constraints = [('name_uniq', 'unique (name)',
                         'Este código de tarifa ya existe.'), ]


GiscegasPolissaTarifa()


class GiscegasPolissaTarifaPeriodes(osv.osv):
    """Periodes de les Tarifes."""
    _name = 'giscegas.polissa.tarifa.periodes'
    _inherits = {'product.product': 'product_id'}

    def name_get(self, cursor, uid, ids, context=None):
        """Retornem una combinació Periode (Nom Tarifa)."""
        if not len(ids):
            return []
        res = []
        for periode in self.browse(cursor, uid, ids):
            res.append((periode.id, '%s (%s)' % (periode.tarifa.name,
                                                 periode.name)))
        return res

    _columns = {
      'tipus': fields.selection([('te', 'Termino Variable'),
                                 ('tp', 'Termino Fijo')], 'Tipo',
                                 required=True),
      'tarifa': fields.many2one('giscegas.polissa.tarifa',
                                'Tarifa a la que pertenece'),
      'product_id': fields.many2one('product.product', 'Producto',
                                    ondelete='restrict'),
      'agrupat_amb': fields.many2one('giscegas.polissa.tarifa.periodes',
                                     'Agrupado con'),
      'agrupats': fields.one2many('giscegas.polissa.tarifa.periodes',
                                  'agrupat_amb', 'Períodos'),
    }

    _defaults = {
      'type': lambda *a: 'service',
      'procure_method': lambda *a: 'make_to_stock',
      'standard_price': lambda *a: 0.000000,
    }

GiscegasPolissaTarifaPeriodes()


class GiscegasPolissaNocutoff(osv.osv):
    """Subministro no cortable."""
    _name = 'giscegas.polissa.nocutoff'
    _description = "Opción suministro no cortable para comer y distri."
    _rec_name = 'motiu'

    _columns = {
        'motiu': fields.text('Motivo'),
        'descripcio': fields.text('Descripción del Motivo'),
        'boe_check': fields.boolean('BOE', readonly=True),
    }


GiscegasPolissaNocutoff()


class GiscegasPolissa(osv.osv):
    """Póliza."""
    _name = 'giscegas.polissa'
    _description = "Contrato de Gas"

    _polissa_states_selection = CONTRACT_STATES

    def get_related_attachments_fields(self, cursor, uid):
        return ['titular', 'cups']

    @cache(timeout=5 * 60)
    def exact_search(self, cursor, uid, context=None):
        if context is None:
            context = {}
        exact = int(self.pool.get('res.config').get(
            cursor, uid, 'giscegas_polissa_cerca_exacte', '1')
        )
        return exact

    def search(self, cr, user, args, offset=0, limit=None, order=None,
               context=None, count=False):
        """Funció per fer cerques per name exacte, enlloc d'amb 'ilike'.
        """
        exact = self.exact_search(cr, user, context=context)
        for idx, arg in enumerate(args):
            if len(arg) == 3:
                field, operator, match = arg
                if field == 'name' and isinstance(match, (unicode, str)):
                    if exact and not '%' in match:
                        operator = '='
                    args[idx] = (field, operator, match)
        return super(GiscegasPolissa, self).search(cr, user, args, offset,
                                                    limit, order, context,
                                                    count)

    def write(self, cursor, user, ids, vals, context=None):
        """Actualitzem els valors a la base de dades.
        """
        if vals.get('data_baixa', False):
            new_vals = self.onchange_data_baixa(cursor, user, ids,
                                                vals['data_baixa'], context)
            vals.update(new_vals['value'])
        if vals.get('renovacio_auto', False):
            new_vals = self.onchange_renovacio_auto(cursor, user, ids,
                                                    vals['renovacio_auto'],
                                                    context)
            vals.update(new_vals['value'])
        if vals.get('state', False):
            self.create_log(cursor, user, ids, vals['state'])
        res = super(GiscegasPolissa, self).write(cursor, user, ids,
                                                  vals, context)
        return res

    def create(self, cursor, user, vals, context=None):
        """Creem els valors a la base de dades.
        """
        if vals.get('data_baixa', False):
            new_vals = self.onchange_data_baixa(cursor, user, [],
                                                vals['data_baixa'], context)
            vals.update(new_vals['value'])
        if vals.get('renovacio_auto', False):
            new_vals = self.onchange_renovacio_auto(cursor, user, [],
                                                    vals['renovacio_auto'],
                                                    context)
            vals.update(new_vals['value'])
        res_id = super(GiscegasPolissa, self).create(cursor, user, vals,
                                                      context)
        return res_id

    def unlink(self, cursor, uid, ids, context=None):
        """Comprovem que es pot eliminar la Póliza.
        """
        msg_error = ''
        for polissa in self.browse(cursor, uid, ids, context):
            if polissa.state not in CONTRACT_IGNORED_STATES:
                msg_error += _(
                    u'El contrato %s no se puede eliminar.\n'
                    u'Está en estado %s.\n' % (
                        polissa.name or 'id#' + str(polissa.id), polissa.state
                    )
                )
        if msg_error:
            raise osv.except_osv('Error', msg_error)
        res = super(GiscegasPolissa, self).unlink(cursor, uid, ids, context)
        return res

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'name': False,
            'modcontractuals_ids': [],
            'modcontractual_activa': False,
            'log_ids': [],
            'data_alta': False,
            'data_baixa': False,
            'data_firma_contracte': False,
            'related_attachments': [],
        }
        default.update(default_values)
        res_id = super(
            GiscegasPolissa, self).copy_data(cursor, uid, id, default, context)
        return res_id

    def browse(self, cursor, uid, ids, context=None, list_class=None,
               fields_process=None):
        """Sobreescrivm el browse per fer-lo compatible segons el temps.
        """
        if not context:
            context = {}
        res = super(GiscegasPolissa, self).browse(cursor, uid, ids, context)
        if 'date' not in context:
            return res
        contracte_obj = self.pool.get('giscegas.polissa.modcontractual')
        fields_to_read = contracte_obj.get_fields_for_polissa(cursor, uid)
        # Busquem la modificació contractual que toqui segons la data
        date_mod = context['date']
        if isinstance(ids, (int, long)):
            res = [res]
        for pol in res:
            contracte_id = self._get_modcon_date(cursor, uid, pol['id'],
                                                 date_mod)
            contracte = contracte_obj.browse(cursor, uid, contracte_id)
            for field_apply in fields_to_read:
                value = getattr(contracte, field_apply)
                setattr(pol, field_apply, value)
        if isinstance(ids, (int, long)):
            res = res[0]
        return res

    def read(self, cursor, uid, ids, fields=None, context=None,
             load='_classic_read'):
        if not context:
            context = {}
        res = super(GiscegasPolissa, self).read(cursor, uid, ids, fields,
                                                 context, load)
        if 'date' not in context:
            return res
        contracte_obj = self.pool.get('giscegas.polissa.modcontractual')
        fields_to_read = contracte_obj.get_fields_for_polissa(cursor, uid)
        if fields:
            fields_to_read = list(set(fields) & set(fields_to_read))
        # Busquem la modificació contractual que toqui segons la data
        date_mod = context['date']
        if isinstance(ids, (int, long)):
            res = [res]
        for pol in res:
            contracte_id = self._get_modcon_date(cursor, uid, pol['id'],
                                                 date_mod)
            contracte = contracte_obj.read(cursor, uid, contracte_id,
                                           fields_to_read)
            for field_apply in fields_to_read:
                pol[field_apply] = contracte[field_apply]
        if isinstance(ids, (int, long)):
            res = res[0]
        return res

    def _get_modcon_date(self, cursor, uid, polissa_id, date_mod):
        contracte_obj = self.pool.get('giscegas.polissa.modcontractual')
        polissa = self.browse(cursor, uid, polissa_id)
        if polissa.state == 'baixa' and date_mod > polissa.data_baixa:
            date_mod = polissa.data_baixa
        search_params = [('data_inici', '<=', date_mod),
                         ('data_final', '>=', date_mod),
                         ('polissa_id.id', '=', polissa_id)]
        c_ids = contracte_obj.search(cursor, uid, search_params, limit=1,
                                     context={'active_test': False})
        if not c_ids:
            raise osv.except_osv(
                'Error', u'No existe ninguna modificación contractual para el'
                         u' contrato (id:%s) en la fecha %s' % (
                    polissa_id, date_mod
                )
            )
        return c_ids[0]

    def get_changes(self, cursor, uid, polissa_id, context=None):
        """Comprova les diferències entre el que hem modificat i el contracte
        actual o el que li passem pel context via modcon_id."""
        if not context:
            context = {}
        if isinstance(polissa_id, tuple) or isinstance(polissa_id, list):
            polissa_id = polissa_id[0]
        polissa = self.browse(cursor, uid, polissa_id)
        contracte_obj = self.pool.get('giscegas.polissa.modcontractual')
        fields_to_read = contracte_obj.get_fields_for_polissa(cursor, uid)
        polissa_vals = self.read(cursor, uid, [polissa_id], fields_to_read,
                                 context)[0]
        modcon_id = context.get('modcon_id', polissa.modcontractual_activa.id)
        contracte_vals = contracte_obj.read(cursor, uid, [modcon_id],
                                            fields_to_read, context)[0]
        del polissa_vals['id']
        del contracte_vals['id']
        res = {}
        for field in fields_to_read:
            if polissa_vals.get(field) != contracte_vals.get(field):
                res.setdefault(field, {'old': contracte_vals.get(field),
                                       'new': polissa_vals.get(field)})
        return res

    def get_modcontractual_intervals(self, cursor, uid, polissa_id, data_inici,
                                     data_final, context=None):
        """Obté tots els intervals de la pòlissa amb la data de tall i les
        modificacions.
        """
        if isinstance(polissa_id, list) or isinstance(polissa_id, tuple):
            polissa_id = polissa_id[0]
        if not context:
            context = {}
        context.update({'active_test': False})
        modcontractual_obj = self.pool.get('giscegas.polissa.modcontractual')
        dates_de_tall = {}
        search_params = [
            ('polissa_id', '=', polissa_id),
            '|',
            '|',
            '&',
            ('data_inici', '<=', data_inici),
            ('data_final', '>=', data_final),
            '&',
            ('data_inici', '>=', data_inici),
            ('data_inici', '<=', data_final),
            '&',
            ('data_final', '>=', data_inici),
            ('data_final', '<=', data_final),
        ]
        modc_ids = modcontractual_obj.search(cursor, uid, search_params,
                                             context=context,
                                             order="data_inici asc")
        modsf = []
        changes = {}
        dates = {}
        if len(modc_ids) > 1:
            # Per defecte qualsevol canvi és una nou interval
            ffields = context.get('ffields',
                                  self.fields_get(cursor, uid).keys())
            diffs = {}
            fields_to_read = ffields + ['data_inici', 'data_final']
            for modc in sorted(modcontractual_obj.read(cursor, uid, modc_ids,
                                                       fields_to_read),
                               key=lambda k: k['data_inici']):
                # Si és la primera l'afegim
                if not diffs:
                    modsf.append(modc['id'])
                    dates.setdefault(modc['id'],
                                     (modc['data_inici'], modc['data_final']))
                    data_inici = modc['data_inici']
                for ffield in ffields:
                    # Comprovem si tenim inicialitzat:
                    if diffs.get(ffield, False):
                        if diffs.get(ffield, False) != modc.get(ffield, False):
                            if modc['id'] not in modsf:
                                modsf.append(modc['id'])
                            # Posem el camp a les modificacions
                            changes.setdefault(modc['id'], [])
                            changes[modc['id']].append(ffield)
                    diffs[ffield] = modc.get(ffield, False)
                if modc['id'] in changes:
                    # Si hi ha canvis afegim la nova modificació a les dates
                    dates[modc['id']] = (modc['data_inici'], modc['data_final'])
                else:
                    # Si no hi ha canvis agafem les dates de l'última
                    data_inici1, data_final1 = dates[modsf[-1]]
                    dates[modsf[-1]] = (min(data_inici1, modc['data_inici']),
                                        max(data_final1, modc['data_final']))
            modc_ids = modsf[:]
        for modc in sorted(
                modcontractual_obj.read(
                    cursor, uid, modc_ids, ['data_inici', 'data_final'], context
                ), key=lambda k: k['data_inici']
        ):
            data_tall = max(data_inici, modc['data_inici'])
            dates_de_tall[data_tall] = {
                'id': modc['id'],
                'changes': changes.get(modc['id'], []),
                'dates': dates.get(
                    modc['id'], (modc['data_inici'], modc['data_final'])
                )
            }
        return dates_de_tall

    def onchange_data_baixa(self, cursor, user, ids, data_baixa, context=None):
        """Canvis a realitzar quan es posa data de baixa a una pòlissa."""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not data_baixa:
            res['value'].update({'renovacio_auto': 1})
        else:
            res['value'].update({'renovacio_auto': 0})
        return res

    def onchange_tarifa(self, cursor, uid, ids, tarifa_id, context=None):
        """Canvis a realitzar quan es posa data de baixa a una pòlissa."""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not tarifa_id:
            return res

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        cdiari = self.read(cursor, uid, ids, ['caudal_diario'])['caudal_diario']
        canual = (cdiari or 0.0) * 365
        canual = round(canual, 7)

        tarif_obj = self.pool.get("giscegas.polissa.tarifa")
        tarif_info = tarif_obj.read(cursor, uid, tarifa_id, ['consumo_min', 'consumo_max'])
        cmin = tarif_info['consumo_min']
        cmax = tarif_info['consumo_max']

        # Si no tenim caudal diari informat posem maxim de la tarifa
        if not canual:
            res['value'].update({'caudal_diario': round(cmax/365.0, 7)})
        return res

    def onchange_renovacio_auto(self, cursor, user, ids, renovacio_auto,
                                context=None):
        """Canvis a realitzar quan es posa renovació automàtica."""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if renovacio_auto:
            res['value'].update({'data_baixa': False})
        return res

    def undo_last_modcontractual(self, cursor, uid, ids, context=None):
        """Undo last modcontractual and reactivate the one that remains"""

        if context is None:
            context = {}

        ctx = context.copy()
        ctx.update({'sync': False})

        modcon_model = 'giscegas.polissa.modcontractual'
        modcon_obj = self.pool.get(modcon_model)
        wkf_instance = self.pool.get('workflow.instance')
        wkf_item = self.pool.get('workflow.workitem')
        wkf_activity = self.pool.get('workflow.activity')

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        for polissa in self.browse(cursor, uid, ids, context=context):
            # If only one modcon or no modcon, do not do nothing
            if len(polissa.modcontractuals_ids) <= 1:
                continue
            # If not actiu, cannot make the change
            if polissa.state != 'activa':
                raise osv.except_osv(
                    'Error', _(
                        u"No se puede deshacer la última modifiación si el "
                        u"contrato no está en estado activo"
                    )
                )
            # Locate modcons implicated
            mod_act = polissa.modcontractual_activa
            mod_ant = polissa.modcontractual_activa.modcontractual_ant
            mod_act_date = mod_act.data_final
            mod_act_state = mod_act.state
            # Write the correct state for unlinking modcon
            mod_act.write({'state': 'pendent'}, context=ctx)
            # Assign mod_ant as the active one
            polissa.write({'modcontractual_activa': mod_ant.id},
                          context=ctx)
            modcon_obj.unlink(cursor, uid, [mod_act.id],
                              context=ctx)
            # Lets reactivate mod_ant
            # Search for workflow instance
            search_params = [('res_type', '=', modcon_model),
                             ('res_id', '=', mod_ant.id)]
            instance_id = wkf_instance.search(cursor, uid, search_params)[0]
            # Search for workflow item
            search_params = [('inst_id', '=', instance_id)]
            item_id = wkf_item.search(cursor, uid, search_params)
            # Search for workflow activity id
            search_params = [('wkf_id.osv', '=', modcon_model),
                             ('name', '=', mod_act_state)]
            activity_id = wkf_activity.search(cursor, uid, search_params)[0]
            # Reactivate workflow for mod_ant
            wkf_item.write(cursor, uid, item_id, {'act_id': activity_id,
                                                  'state': 'complete'})
            # Write new values for mod_ant
            mod_ant.write({'state': mod_act_state,
                           'active': True,
                           'data_final': mod_act_date},
                          context=ctx)
            # Adapt the contract to the new activated modcon
            signals = ['modcontractual', 'undo_modcontractual']
            self.send_signal(cursor, uid, [polissa.id], signals)

        return True

    # Workflow Stuff
    def activar_xmlrpc(self, cursor, uid, ids):
        """Activa un pòlissa a través d'XML-RPC."""
        wf_service = netsvc.LocalService('workflow')
        for p_id in ids:
            wf_service.trg_validate(uid, 'giscegas.polissa', p_id, 'validar',
                                    cursor)
            wf_service.trg_validate(uid, 'giscegas.polissa', p_id, 'activar',
                                    cursor)
        return True

    def send_signal(self, cursor, uid, ids, signals):
        """Enviem el signal al workflow de la pòlissa.
        """
        wf_service = netsvc.LocalService('workflow')
        if not isinstance(signals, list) and not isinstance(signals, tuple):
            signals = [signals]
        for p_id in ids:
            for signal in signals:
                wf_service.trg_validate(uid, 'giscegas.polissa', p_id, signal,
                                        cursor)
        return True

    def esborrany_xmlrpc(self, cursor, uid, ids):
        """Passa a esborrany una pòlissa a través d'XML-RPC."""
        wf_service = netsvc.LocalService('workflow')
        for p_id in ids:
            wf_service.trg_validate(uid, 'giscegas.polissa', p_id,
                                    'esborrany', cursor)
        return True

    def wkf_esborrany(self, cursor, uid, ids):
        """Passem a esborrany una pòlissa."""
        modcont_obj = self.pool.get('giscegas.polissa.modcontractual')
        # Eliminem tots els contractes (si n'hi ha)
        for polissa in self.read(cursor, uid, ids, ['modcontractuals_ids']):
            unlink_ids = polissa['modcontractuals_ids']
            if unlink_ids:
                modcont_obj.unlink(cursor, uid, unlink_ids)
        # Escrivim esborrany a l'estat
        self.write(cursor, uid, ids,
                   {'state': 'esborrany', 'active': True},
                   {'sync': False})
        return True

    def wkf_validar(self, cursor, uid, ids):
        """Acció dins l'estat validar.
        """
        self.write(cursor, uid, ids,
                   {'state': 'validar'},
                   {'sync': False})
        return True

    def wkf_cancelar(self, cursor, uid, ids):
        """ Acció per a cancel·lar una pòlissa. """
        self.write(cursor, uid, ids,
                   {'state': 'cancelada', 'active': False,
                    'data_baixa': datetime.today().strftime("%Y-%m-%d")},
                   {'sync': False})
        return True

    def wkf_contracte(self, cursor, uid, ids):
        """Acció quan es passa a l'estat contracte.

        En aquest estat del workflow generem el contracte segons els valors
        que tingui la pòlissa. Ja sigui perquè és una pòlissa nova o perquè
        venim d'una modificació contractual.
        """
        contracte_obj = self.pool.get('giscegas.polissa.modcontractual')

        for polissa in self.browse(cursor, uid, ids):
            # Hem de crear un conctracte amb tots els valors que té la
            # pòlissa en l'actualitat
            vals = contracte_obj.get_vals_from_polissa(cursor, uid,
                                                       polissa.id)
            contracte_obj.create(cursor, uid, vals)
        self.write(cursor, uid, ids,
                   {'state': 'contracte'},
                   {'sync': False})
        return True

    def cnd_novapolissa(self, cursor, uid, ids):
        """Comprovem si s'ha de crear una nova pòlissa o no."""
        for polissa_id in ids:
            changes = self.get_changes(cursor, uid, polissa_id)
            # TODO: Poder configurar quins canvis impliquen nova pòlissa
            return changes.has_key('titular')

    def wkf_novapolissa(self, cursor, uid, ids):
        """Acció quan es passa a l'estat contracte.

        Coses a fer:
        * Crear una nova pòlissa amb els valors de l'actual copy() + defaults
        * Aplicar els valors de la modificació contractual actual per deixar
        la pòlissa tal i com estava
        """
        contracte_obj = self.pool.get('giscegas.polissa.modcontractual')
        for polissa in self.browse(cursor, uid, ids):
            modcontractual_activa = polissa.modcontractual_activa

            contract_vals = contracte_obj.get_vals_from_polissa(cursor, uid,
                                                                polissa.id)
            polissa_vals = contract_vals.copy()
            polissa_vals['data_alta'] = polissa_vals['data_inici']
            del polissa_vals['data_inici']
            del polissa_vals['data_final']
            del polissa_vals['tipus']
            fields_list = self.fields_get_keys(cursor, uid)
            polissa_vals.update(self.default_get(cursor, uid, fields_list))
            novapolissa_id = self.copy(cursor, uid, polissa.id, polissa_vals)
            # Enviem el signal per activar la nova pòlissa (contracte)
            wf_service = netsvc.LocalService('workflow')
            wf_service.trg_validate(uid, 'giscegas.polissa', novapolissa_id,
                                    'activar', cursor)
            contracte_obj.aplicar_modificacio(cursor, uid,
                                              modcontractual_activa.id,
                                              polissa.id)
            polissa.write(
                {'data_baixa': modcontractual_activa.data_final,
                 'renovacio_auto': 0})
            # Enllacem modificacions contractuals
            novapolissa = self.browse(cursor, uid, novapolissa_id)
            novapolissa.modcontractuals_ids[-1].write(
                {'modcontractual_ant': modcontractual_activa.id}
            )
            modcontractual_activa.write(
                {'modcontractual_seg': novapolissa.modcontractuals_ids[-1].id}
            )

        self.write(cursor, uid, ids, {'state': 'novapolissa'})
        return True

    def cnd_contracte_actiu(self, cursor, uid, ids):
        """Comprovem la condició de contracte actiu."""
        avui = time.strftime('%Y-%m-%d')
        for polissa in self.browse(cursor, uid, ids):
            # Si la data d'inici <= que avui ho podem activar
            if polissa.modcontractual_activa and \
                            polissa.modcontractual_activa.data_inici <= avui:
                return True
            else:
                return False

    def create_log(self, cursor, uid, ids, state, context=None):
        """ Crea un registre per a un canvi en la polissa. """
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        if state in CONTRACT_LOGGABLE_STATES:
            log_obj = self.pool.get('giscegas.polissa.log')
            for polissa_id in ids:
                log_obj.create(cursor, uid, {
                    'polissa_id': polissa_id,
                    'state': state,
                    'change_date': datetime.now(),
                    'user_id': uid
                })
        return True

    def get_contract_sequence(self, cursor, uid):
        return self.pool.get('ir.sequence').get(cursor, uid, 'giscegas.polissa')

    def wkf_pendent(self, cursor, uid, ids):
        """Estat pendent d'una pòlissa."""
        self.write(cursor, uid, ids,
                   {'state': 'pendent'},
                   {'sync': False})
        return True

    def wkf_activa(self, cursor, uid, ids):
        """Activem una pòlissa."""
        modcontractual_obj = self.pool.get('giscegas.polissa.modcontractual')
        for polissa in self.browse(cursor, uid, ids):
            polissa.check_limit_pressure()
            polissa.check_positive_pressure()

            # Creem un diccionari amb els valors que posarem a la pólissa
            vals = {'state': 'activa', 'active': 1}

            # If contract type is eventual (02 or 09) should keep low date and
            # deactivate auto renewal.
            # list_dat_fi store dicts with id's and contract types
            if polissa.contract_type not in (u'02', u'09'):
                vals.update({'data_baixa': False, 'renovacio_auto': 1})

            # Hem de numerar la pòlissa si tenim activat el gen_num_auto i
            # la pòlisa no té cap número assignat
            if polissa.name_auto and not polissa.name:
                vals.update({'name': self.get_contract_sequence(cursor, uid)})
            if polissa.modcontractual_activa:
                if not polissa.modcontractual_activa.active:
                    # Això és que venim d'una reactivació enviem el signal
                    # de reactivar a la modificació contractual
                    dfpb = polissa.modcontractual_activa.data_final_pre_baixa
                    polissa.modcontractual_activa.write({'data_final': dfpb})
                    wf_service = netsvc.LocalService('workflow')
                    wf_service.trg_validate(uid,
                                            'giscegas.polissa.modcontractual',
                                            polissa.modcontractual_activa.id,
                                            'reactivar', cursor)
                modcontractual_obj.aplicar_modificacio(cursor, uid,
                                                       polissa.modcontractual_activa.id,
                                                       polissa.id)
            # Actualitzem la pòlissa
            self.write(cursor, uid, [polissa.id],
                       vals, {'sync': False})
        return True

    def wkf_modcontractual(self, cursor, uid, ids):
        """Fem una modificació contractual."""
        self.write(cursor, uid, ids,
                   {'state': 'modcontractual'},
                   {'sync': False})
        return True

    def wkf_undo_modcontractual(self, cursor, uid, ids):
        """Desfem una modificació contractual."""
        # Amb la transició automatica a actiu, ja s'aplica
        # la modificacio contractual activa

        return True

    def wkf_overwrite_modcontractual(self, cursor, uid, ids):
        """Apliquem directament la modificació contractual.
        """
        mc_obj = self.pool.get('giscegas.polissa.modcontractual')
        for polissa in self.browse(cursor, uid, ids):
            vals = mc_obj.get_vals_from_polissa(cursor, uid, polissa.id)
            mca = polissa.modcontractual_activa
            vals.update({'data_inici': mca.data_inici,
                         'data_final': mca.data_final,
                         'data_firma_contracte': mca.data_firma_contracte,
                         'tipus': mca.tipus,
                         'name': mca.name})
            mca.write(vals)
            mca = polissa.modcontractual_activa
            mca.write(vals)
        return True

    def cnd_activa_cancelar(self, cursor, uid, ids):
        """ Condició per comprovar que es pot cancel·lar una pòlissa activa. """
        # Implemented on giscegas_facturacio
        return True

    def cnd_impagament(self, cursor, uid, ids):
        """Condició per comprovar la transició a impagament.

        Aquesta haurà de ser sobreescrita pels mòduls de facturació.
        """
        return True

    def wkf_impagament(self, cursor, uid, ids):
        """Estat d'impagament."""
        self.write(cursor, uid, ids,
                   {'state': 'impagament'},
                   {'sync': False})
        return True

    def impagament_xmlrpc(self, cursor, uid, ids):
        """Passa a impagament una pòlissa a través d'XML-RPC."""
        wf_service = netsvc.LocalService('workflow')
        for p_id in ids:
            wf_service.trg_validate(uid, 'giscegas.polissa', p_id,
                                    'impagament', cursor)
        return True

    def wkf_tall(self, cursor, uid, ids):
        """Estat de tall."""
        self.write(cursor, uid, ids,
                   {'state': 'tall'},
                   {'sync': False})
        return True

    def tall_xmlrpc(self, cursor, uid, ids):
        """Passa a tall una pòlissa a través d'XML-RPC."""
        wf_service = netsvc.LocalService('workflow')
        for p_id in ids:
            wf_service.trg_validate(uid, 'giscegas.polissa', p_id,
                                    'tallar', cursor)
        return True

    def cnd_baixa(self, cursor, uid, ids):
        """Condició per passar a baixa.

        Que cada mòdul que exingui polissa faci les comprovacions que s'hagin
        de fer.
        """
        for polissa in self.browse(cursor, uid, ids):
            today = time.strftime('%Y-%m-%d')
            if polissa.renovacio_auto:
                raise osv.except_osv(
                    _(u"Error"), _(
                        u"No se pueden dar de baja contratos con renovación "
                        u"automática"
                    )
                )
            elif not polissa.data_baixa:
                raise osv.except_osv(
                    _(u"Error"), _(
                        u"No se pueden dar de baja contratos sin una fecha de baja"
                    )
                )
            elif polissa.data_baixa and polissa.data_baixa > today:
                raise osv.except_osv(
                    _(u"Error"), _(
                        u"No se pueden dar de baja contratos con una "
                        u"fecha de baja posterior a hoy"
                    )
                )
            elif (polissa.data_baixa and
                  polissa.data_baixa < polissa.modcontractual_activa.data_inici):
                raise osv.except_osv(
                    _(u"Error"), _(
                        u"No se pueden dar de baja contratos con una fecha "
                        u"anterior a la fecha de inicio de la última "
                        u"modificación contractual"
                    )
                )
        return True

    def wkf_baixa(self, cursor, uid, ids):
        """Passem a l'estat de baixa."""
        for polissa in self.browse(cursor, uid, ids):
            data_final = polissa.data_baixa or time.strftime('%Y-%m-%d')
            if polissa.modcontractual_activa:
                data_final_pre_baixa = polissa.modcontractual_activa.data_final
                polissa.modcontractual_activa.write(
                    {'data_final': data_final,
                     'data_final_pre_baixa': data_final_pre_baixa})
                mod_id = polissa.modcontractual_activa.id
                wf_service = netsvc.LocalService('workflow')
                wf_service.trg_validate(uid, 'giscegas.polissa.modcontractual',
                                        mod_id, 'baixa', cursor)
        self.write(cursor, uid, ids, {'state': 'baixa',
                                      'data_baixa': data_final,
                                      'renovacio_auto': False,
                                      'active': 0},
                   {'sync': False})
        return True

    def _cnt_un_cups_per_polissa_activa(self, cursor, uid, ids):
        """Comprova que només hi ha una pòlissa 'activa' per CUPS.
        """
        states_ok = tuple(CONTRACT_IGNORED_STATES + ['pendent', 'baixa'])
        for polissa in self.browse(cursor, uid, ids):
            if polissa.state in states_ok:
                continue
            search_params = [('cups.id', '=', polissa.cups.id),
                             ('id', '!=', polissa.id),
                             ('state', 'not in', states_ok)]
            if self.search_count(cursor, uid, search_params):
                return False
        return True

    def check_positive_pressure(self, cursor, uid, ids):
        """Comprova que la pressió sigui estrictament positiva
        """
        for polissa_vals in self.read(cursor, uid, ids, ['pressio']):
            if polissa_vals['pressio'] < 0:
                raise osv.except_osv(
                    _(u"Error"),
                    _(u"La presión debe ser estrictamente positiva.")
                )
        return True

    def _cups_cp(self, cursor, uid, ids, field_name, arg, context=None):
        return dict.fromkeys(ids, False)

    def _cups_cp_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        else:
            return [('cups.dp', '=', args[0][2])]

    def _cups_poblacio(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)

        return res

    def _cups_poblacio_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        cups_obj = self.pool.get('giscegas.cups.ps')
        cups_ids = cups_obj.search(cursor, uid, [
            ('id_poblacio.name', 'ilike', args[0][2])
        ])
        cups_ids.append(-1)
        return [('cups.id', 'in', cups_ids)]

    def _fnc_firmat(self, cursor, uid, ids, name, arg, context=None):
        """Ens retorna si un contracte està firmat o no.
        """
        res = {}
        for pol in self.read(cursor, uid, ids, ['data_firma_contracte']):
            res[pol['id']] = bool(pol['data_firma_contracte'])
        return res

    def _fnc_firmat_search(self, cursor, uid, obj, name, args, context=None):
        if not args:
            return []
        else:
            firmat = bool(args[0][2])
            if firmat:
                domain = [('data_firma_contracte', '>', '0001-01-01')]
            else:
                domain = [('data_firma_contracte', '=', False)]
            return domain

    def update_observacions(self, cursor, uid, ids, text, title='',
                            context=None):
        """Adds a new comment on 'observacions' field. If titol is passed, it
        generate's a header with date and title """
        if context is None:
            context = {}

        if isinstance(ids, (tuple, list)):
            ids = ids[0]

        contract_vals = self.read(cursor, uid, ids, ['observacions'])

        comments = []

        if title:
            cur_date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
            user_vals = self.pool.get('res.users').read(
                cursor, uid, uid, ['name', 'login']
            )
            title_txt = u'**** {0} [{1} - {2} ({3})] ****'.format(
                title, cur_date, user_vals['name'], user_vals['login']
            )
            comments.append(title_txt)

        comments.append(text)
        new_comment = "\n".join(comments)

        if contract_vals['observacions']:
            comment = "{0}\n{1}".format(
                new_comment,
                contract_vals['observacions']
            )
        else:
            comment = new_comment

        return self.write(cursor, uid, ids, {'observacions': comment})

    def _get_polissa_ids(self, cursor, uid, ids, context=None):
        """Retorna els ids que s'han de recalcular.

        Les pòlisses que s'han de recalcular són les que s'hagin modificat les
        modificacions contractuals actives, sino no cal recalcular la pòlissa.
        """
        polissa_obj = self.pool.get('giscegas.polissa')
        return polissa_obj.search(cursor, uid,
                                  [('modcontractual_activa', 'in', ids)])

    def _correct_cnae(self, cursor, uid, ids):
        """Comprova que el cnae es de 4 digits
        """
        res_config = self.pool.get('res.config')
        check_cnae = int(res_config.get(cursor, uid, 'giscegas_check_cnae', 0))
        if check_cnae:
            cnae_obj = self.pool.get('giscemisc.cnae')
            for polissa_vals in self.read(cursor, uid, ids, ['cnae']):
                if polissa_vals['cnae']:
                    cnae_id = polissa_vals['cnae'][0]
                    vals = cnae_obj.read(cursor, uid, cnae_id, ['name'])
                    return len(vals['name']) == 4
        return True

    def check_limit_pressure(self, cursor, uid, ids):
        """
        Comprova que la pressió de la pòlissa no sigui superior a la permesa per
        la seva tarifa.
        """
        for polissa_id in ids:
            sql = self.q(cursor, uid).select(
                ['pressio', 'tarifa.presion_max']
            ).where([('id', '=', polissa_id)])
            cursor.execute(*sql)
            res = cursor.fetchone()
            if res:
                pressio, max_pressio_per_tarifa = res
                if pressio > max_pressio_per_tarifa:
                    raise osv.except_osv(
                        _(u"Error"),
                        _(u"La presión no debe ser superior a la máxima de la "
                          u"tarifa")
                    )
        return True

    def _cnt_limit_caudal_diario(self, cursor, uid, ids):
        """
        Comprova que el caudal dari de la pòlissa no sigui el permes per
        la seva tarifa.
        """
        # ######################################################################
        # De moment la desactivem perque sembla que no sempre es compleix.
        # Les distris per exemple envien caudals anuals de 5630 per tarifes 3.1
        # ######################################################################
        # sql = self.q(cursor, uid).select(
        #     ['caudal_diario', 'tarifa.consumo_min', 'tarifa.consumo_max']
        # ).where([('id', 'in', ids), ('state', 'not in', CONTRACT_IGNORED_STATES)])
        # cursor.execute(*sql)
        # for res in cursor.fetchall():
        #     if res:
        #         caudal_diari, canualmin, canualmax = res
        #         if not (canualmin <= caudal_diari*365 <= canualmax):
        #             return False
        return True

    def _auto_init(self, cr, context={}):
        if context is None:
            context = {}
        res = super(GiscegasPolissa, self)._auto_init(cr, context)
        cr.execute(
            "SELECT indexname "
            "FROM pg_indexes "
            "WHERE indexname = 'idx_giscegas_polissa_dadbc'")
        if not cr.fetchone():
            cr.execute('CREATE INDEX idx_giscegas_polissa_dadbc '
                       'ON giscegas_polissa (data_alta,data_baixa,cups)')
        return res

    def create_contracte(self, cursor, uid, ids, data_inici, duracio='actual', context=None):
        """
        Crea un nou contracte fent servir l'assistent de modificació
        contractual.
        :param cursor:
        :param uid:
        :param ids:
        :param data_inici: Data de inici del nou contracte
        :param duracio: duració del nou contracte. Possibles valors: 'actual' i
        'nou'.
        :type: str
        :param context:
        :return:
        """

        if context is None:
            context = {}

        if not isinstance(ids, list):
            ids = [ids]

        for polissa in self.browse(cursor, uid, ids, context=context):
            wz_crear_contracte_obj = self.pool.get(
                'giscegas.polissa.crear.contracte'
            )
            params = {'duracio': duracio, 'accio': 'nou'}
            context.update({'active_id': polissa.id})

            try:
                wz_id = wz_crear_contracte_obj.create(
                    cursor, uid, params, context=context
                )
            except Exception as e:
                raise osv.except_osv(
                    _(u"ERROR"),
                    _(u"Verifica que el contrato %s esté activo.") %
                    (polissa.name,)
                )

            wz_inst = wz_crear_contracte_obj.browse(
                cursor, uid, wz_id, context=context
            )
            res = wz_crear_contracte_obj.onchange_duracio(
                cursor, uid, [wz_id], data_inici, wz_inst.duracio,
                context=context
            )

            if res.get('warning', False):
                info = _(
                    u"Hay un error con las fechas al crear la modificación"
                    u" contractual del contrato '%s'. %s: %s."
                ) % (polissa.name, res['warning']['title'],
                     res['warning']['message'])
                polissa.send_signal('undo_modcontractual')
                raise osv.except_osv(_(u"ERROR"), info)

            params = {
                'data_final': res['value']['data_final'] or '',
                'data_inici': data_inici
            }
            wz_inst.write(params, context=context)
            wz_inst = wz_crear_contracte_obj.browse(
                cursor, uid, wz_id, context=context
            )
            wz_inst.action_crear_contracte(context=context)

    def modify_contracte(self, cursor, uid, ids, context=None):
        """
        Actualitza el contracte utilitzant l'assistent de modificació
        contractual.
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return:
        """
        if context is None:
            context = {}

        params = {'accio': 'modificar'}

        if not isinstance(ids, list):
            ids = [ids]

        for pol_id in ids:
            wz_crear_contracte_obj = self.pool.get(
                'giscegas.polissa.crear.contracte'
            )
            context.update({'active_id': pol_id})
            try:
                wz_id = wz_crear_contracte_obj.create(
                    cursor, uid, params, context=context
                )
            except Exception as e:
                raise osv.except_osv(_(u"ERROR"), _(
                    u"Verifica que la pòlissa estigui activada"))

            wz_inst = wz_crear_contracte_obj.browse(
                cursor, uid, wz_id, context=context
            )
            wz_inst.action_crear_contracte(context=context)

    def change_bank_account(self, cursor, uid, ids, iban, data, pagador_id,
                            mandate_scheme, account_owner_inst,
                            owner_address_inst, payment_mode_inst,
                            context=None):
        def _check_data(polissa_inst, account_iban):

            if polissa_inst.payment_mode_id.require_bank_account:
                bank = polissa_inst.bank
                if not bank:
                    raise osv.except_osv(_(u"ERROR"), _(
                        u"El contrato debe tener una cuenta asociada."
                    ))

                bank_old_iban = bank.iban.replace(" ", "")

                if account_iban == bank_old_iban:
                    raise osv.except_osv(_(u"ERROR"), _(
                        u"La cuenta bancaria a cambiar debe ser distinta a "
                        u"la actual"
                    ))

            if polissa_inst.state != 'activa':
                raise osv.except_osv(_(u"ERROR"), _(
                    u"El contrato debe ser activo."
                ))

        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscegas.polissa')
        mandate_obj = self.pool.get('payment.mandate')
        bank_obj = self.pool.get('res.partner.bank')

        data_str = datetime.strptime(data, '%Y-%m-%d')

        polissa = polissa_obj.browse(cursor, uid, ids, context=context)[0]

        # -- Verificar que les dades del wizard son coherents.
        _check_data(polissa, iban)

        mandate_reference = "giscegas.polissa," + str(polissa.id)
        mandate_ids = mandate_obj.search(
            cursor, uid,
            [('reference', '=', mandate_reference),
             ('date_end', '=', None)],
            context=context
        )

        if mandate_ids:
            # -- Finalitzar el mandato
            mandate_obj._close_mandate(
                cursor, uid, mandate_ids, data_str - timedelta(days=1),
                context=context
            )

        # -- Crear modificació contractual
        polissa.send_signal('modcontractual')

        # ---- Guardar les noves dades bancaries
        bank_new_id = bank_obj._find_or_create_bank(
            cursor, uid, pagador_id, iban, account_owner_inst,
            owner_address_inst, context=context
        )
        params = {
            'bank': bank_new_id,
            'payment_mode_id': payment_mode_inst.id,
            'tipo_pago': payment_mode_inst.type.id
        }
        polissa.write(params, context=context)

        # -- Cear contracte
        polissa.create_contracte(data, context=context)

        # -- Crear un nou mandat
        mandate_new_id = mandate_obj._create_mandate(
            cursor, uid, data_str, mandate_reference, mandate_scheme,
            context=context
        )
        return mandate_new_id

    def _related_attachments(self, cursor, uid, ids, field_name, arg,
                             context=None):
        return self.internal_related_attachments(
            cursor, uid, ids, field_name, arg, context=context)

    def internal_related_attachments(self, cursor, uid, ids, field_name, arg,
                             context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        attach_obj = self.pool.get('ir.attachment')
        fields = list(set(self.get_related_attachments_fields(cursor, uid)))
        def_fields = self.fields_get(cursor, uid, fields)
        for polissa in self.read(cursor, uid, ids, fields, context=context):
            attach_ids = attach_obj.search(cursor, uid, [
                ('res_model', '=', 'giscegas.polissa'),
                ('res_id', '=', polissa['id'])
            ])
            for field in fields:
                model = def_fields[field].get('relation')
                if model and polissa[field]:
                    attach_ids += attach_obj.search(cursor, uid, [
                        ('res_model', '=', model),
                        ('res_id', '=', polissa[field][0])
                    ])
            res[polissa['id']] = list(set(attach_ids))

        return res

    def check_data_inici_anterior_modcon_activa(self, cursor, uid, ids,
                                                data_inici, context=None):
        if not isinstance(ids, list):
            ids = [ids]
        polissa_querier = OOQuery(self.pool.get(self._name), cursor, uid)
        polissa_query = polissa_querier.select(
            ['id', 'modcontractual_activa.data_inici']
        ).where(
            [('id', 'in', ids)]
        )
        cursor.execute(*polissa_query)
        vals = cursor.dictfetchall()
        res = {}
        single_res = {
            'title': _(u"Error"),
            'message': _(u"La data inicial no pot ser inferior a la "
                         u"inicial del contracte actual.")
        }
        for polissa_vals in vals:
            polissa_id = polissa_vals['id']
            if data_inici < polissa_vals['modcontractual_activa.data_inici']:
                res[polissa_id] = single_res
            else:
                res[polissa_id] = False

        return res

    def check_data_inici_igual_modcon_activa(self, cursor, uid, ids, data_inici,
                                             context=None):
        if not isinstance(ids, list):
            ids = [ids]
        polissa_querier = OOQuery(self.pool.get(self._name), cursor, uid)
        polissa_query = polissa_querier.select(
            ['id', 'modcontractual_activa.data_inici']
        ).where(
            [('id', 'in', ids)]
        )
        cursor.execute(*polissa_query)
        vals = cursor.dictfetchall()
        res = {}
        single_res = {
            'title': _(u"Atención"),
            'message': _(u"Ya existe una modificación contractual activa con "
                         u"esta fecha. Se modificará la modificación "
                         u"contractual actual.")
        }
        for polissa_vals in vals:
            polissa_id = polissa_vals['id']
            if data_inici == polissa_vals['modcontractual_activa.data_inici']:
                res[polissa_id] = single_res
            else:
                res[polissa_id] = False

        return res

    _columns = {
        'name': fields.char('Contrato', size=60, readonly=True, select=True,
                            states={'esborrany': [('readonly', False)],
                                    'validar': [('readonly', False)]}),
        'name_auto': fields.boolean('Auto'),
        'active': fields.boolean('Activa', required=True, readonly=True),
        'renovacio_auto': fields.boolean('Renovación Automática'),
        'cups': fields.many2one('giscegas.cups.ps', 'CUPS', readonly=True,
                                states={'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', True)]}),
        'cups_cp': fields.function(
            _cups_cp, type='char', string='Código Postal',
            fnct_search=_cups_cp_search
        ),
        'cups_direccio': fields.related('cups', 'direccio', type='char',
                                        relation='giscegas.cups.ps',
                                        string="Dirección CUPS", store=False,
                                        readonly=True, size=256),
        'cups_poblacio': fields.function(
            _cups_poblacio, string=u'Población', type='char',
            fnct_search=_cups_poblacio_search
        ),
        'observacions': fields.text('Observaciones'),
        'pressio': fields.float('Presión contratada (bares)', digits=(16, 3),
                                 readonly=True,
                                 states={
                                     'esborrany': [('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)],
                                     'modcontractual': [('readonly', False),
                                                        ('required', True)]
                                 }),
        'caudal_diario': fields.float(
            'Caudal diario (KWh/dia)', digits=(16, 7),
            readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False),
                            ('required', True)],
                'modcontractual': [('readonly', False),
                                   ('required', True)]
            }
        ),
        'tarifa': fields.many2one('giscegas.polissa.tarifa', "Tarifa de acceso",
                                  readonly=True,
                                  states={
                                      'esborrany': [('readonly', False)],
                                      'validar': [('readonly', False),
                                                  ('required', True)],
                                      'modcontractual': [('readonly', False),
                                                         ('required', True)]
                                  }),
        'titular': fields.many2one('res.partner', 'Titular', readonly=True,
                                   states={
                                       'esborrany': [('readonly', False)],
                                       'validar': [('readonly', False),
                                                   ('required', True)],
                                   }, size=40),
        'titular_nif': fields.related('titular', 'vat', type='char',
                                      string='NIF titular', readonly=True),
        'state': fields.selection(_polissa_states_selection, 'Estat',
                                  required=True, readonly=True),
        'data_alta': fields.date('Fecha alta', readonly=True,
                                 states={
                                     'esborrany': [('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)]
                                 }),
        'data_baixa': fields.date('Fecha baja'),
        'data_firma_contracte': fields.datetime('Fecha firma contrato',
                                            readonly=True,
                                            states={
                                                'esborrany': [
                                                    ('readonly', False)],
                                                'validar': [('readonly', False),
                                                            ('required', True)]
                                            }),
        'modcontractuals_ids': fields.one2many(
            'giscegas.polissa.modcontractual',
            'polissa_id',
            'Modificaciones contractuales',
            context={'active_test': False},
            readonly=True),
        'cnae': fields.many2one('giscemisc.cnae', 'CNAE', ondelete='restrict',
                                readonly=True,
                                states={
                                    'esborrany': [('readonly', False)],
                                    'validar': [('readonly', False),
                                                ('required', True)],
                                    'modcontractual': [('readonly', False),
                                                       ('required', True)],
                                }),

        'comercialitzadora': fields.many2one('res.partner',
                                             'Comercializadora'),
        'distribuidora': fields.many2one('res.partner', 'Distribuidora'),
        'firmat': fields.function(_fnc_firmat, type='boolean',
                                  string="Contrato Firmado", method=True,
                                  fnct_search=_fnc_firmat_search),
        'tipus_vivenda': fields.selection(
            [('habitual', 'Habitual'), ('no_habitual', 'No habitual')],
            string='Tipus vivenda', readonly=True, states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)],
                'modcontractual': [('readonly', False)],
            }
        ),
        'persona_fisica': fields.related('titular', 'cifnif',
                                         type='char', size=2),
        'nocutoff': fields.many2one('giscegas.polissa.nocutoff',
                                    'Suministro no cortable',
                                    readonly=True,
                                    states={
                                        'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False)],
                                        'modcontractual': [
                                            ('readonly', False)]},
                                    write=[
                                        'giscegas_polissa.group_giscegas_polissa_cutoff'
                                    ]),
        'log_ids': fields.one2many(
            'giscegas.polissa.log', 'polissa_id', u'Històric', readonly=True
        ),
        'contract_type': fields.selection(
            CONTRACT_TYPES, 'Tipo de contrato', readonly=True,
            help="Distintos tipos de contratos. Ver REAL DECRETO 1164/2001",
            states=DEFAULT_STATES_MODCON
        ),
        'related_attachments': fields.function(
            _related_attachments,
            method=True,
            string='Adjuntos relacionados',
            type='one2many',
            relation='ir.attachment'
        ),
        'tipo_uso_gas': fields.selection(
            TAULA_TIPOS_DE_USO_DEL_GAS_REDUCIDA, 'Uso suministro',
            help="Distintos tipos de usos del suministro de gas. "
                 "Ver: CNMC - G - V1.0 2016.12.20",
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False),
                            ('required', True)]
            }
        ),
        'tipo_presion': fields.selection(
            TAULA_PRESION_COMUNITARIA, 'Tipo presión'
        ),
        'netsituation': fields.char('Red o municipio de ubicación', size=14)
    }

    _defaults = {
        'active': lambda *a: 1,
        'state': lambda *a: 'esborrany',
        'name_auto': lambda *a: 1,
        'renovacio_auto': lambda *a: 1,
        'tipus_vivenda': lambda *a: 'habitual',
        'contract_type': lambda *a: '01',
        'netsituation': lambda *a: False
    }

    _sql_constraints = [
        ('name_unic', 'unique (name)',
         _('Este número de póliza ya existe.')),
    ]

    _constraints = [
        (
            _cnt_un_cups_per_polissa_activa,
            u'Error: ya existe una póliza con este CUPS.',
            ['cups']
        ),
        (
            _correct_cnae,
            u'Error: El CNAE elegido no es válido. '
            u'Solamente se permiten CNAE de 4 dígitos.',
            ['cnae']
        ),
        (
            _cnt_limit_caudal_diario,
            u'Error: el caudal anual calculado (caudal diario * 365) debe estar en los limites permitidos de la tarifa',
            ['caudal_diario']
        )
    ]


GiscegasPolissa()


class GiscegasPolissaLog(osv.osv):
    """ Log dels estats de la Póliza """
    _name = 'giscegas.polissa.log'

    _polissa_states_selection = CONTRACT_STATES

    _columns = {
        'polissa_id': fields.many2one(
            'giscegas.polissa', 'Póliza', required=True, ondelete='cascade'
        ),
        'state': fields.selection(
            _polissa_states_selection, 'Estado', required=True
        ),
        'change_date': fields.datetime(
            'Fecha de cambio'
        ),
        'user_id': fields.many2one(
            'res.users', 'Usuario', required=True
        )
    }


GiscegasPolissaLog()


class GiscegasPolissaModcontractual(osv.osv):
    """Modificación Contractual d'una Póliza."""
    _name = 'giscegas.polissa.modcontractual'

    _modcontractual_states_selection = [
        ('esborrany', "Borrador"),
        ('actiu', "Activo"),
        ('pendent', "Pendiente de activación"),
        ('baixa', "Baja"),
        ('baixa2', "Baja por modificación"),
        ('baixa3', "Baja por renovación"),
        ('baixa4', "Baja por nueva póliza")
    ]

    _tipus_modificacio_contractual = [
        ('alta', 'Alta'),
        ('baixa', 'Baja'),
        ('mod', 'Modificación'),
        ('reno', 'Renovación')
    ]

    def unlink(self, cursor, uid, ids, context=None):
        """Comprovem que es pot eliminar la modificació contractual.
        """
        msg_error = ''
        for modcon in self.browse(cursor, uid, ids, context):
            if modcon.state != 'pendent':
                msg_error += _(u'- La modificación contractual %s de la póliza '
                               u'%s no se puede eliminar. Esta en estado %s.\n' %
                               (modcon.name, modcon.polissa_id.name,
                                modcon.state))
        if msg_error:
            raise osv.except_osv('Error', msg_error, 'error')
        res = super(GiscegasPolissaModcontractual, self).unlink(cursor, uid,
                                                                 ids, context)
        return res

    def get_fields_for_polissa(self, cursor, uid, context=None):
        """Retorna una llista de camps per llegir a la pòlissa."""
        res_fields = self.fields_get_keys(cursor, uid)
        fields_to_delete = ['id',
                            'name',
                            'polissa_id',
                            'modcontractual_ant',
                            'modcontractual_seg',
                            'active',
                            'state',
                            'tipus',
                            'observacions']

        for field in fields_to_delete:
            if field in res_fields:
                res_fields.remove(field)

        polissa_obj = self.pool.get('giscegas.polissa')
        polissa_fields = polissa_obj.fields_get_keys(cursor, uid)

        # Es converteix a llista perquè esborrem items de la llista que
        # estem iterant. D'aquesta forma tenim una copia
        for field in list(res_fields):
            if field not in polissa_fields:
                res_fields.remove(field)

        return res_fields

    def get_data_final(self, cursor, uid, data_inici, context=None):
        '''Obte la data final d'una modificacio contractual'''

        # Hi sumem 364 o 365 dies segons lo acordat amb Sóller
        # (per mostrar intèrvals tancats)
        dies_durada_contracte = int(self.pool.get('res.config').get(
            cursor, uid, 'giscegas_polissa_contracte_durada', 364)
        )
        year, month, day = map(int, data_inici.split('-'))
        data_final = (date(year, month, day)
                      + timedelta(days=dies_durada_contracte))
        days = (calendar.isleap(year + 1)
                and data_final >= date(year + 1, 2, 28)
                and dies_durada_contracte + 1
                or dies_durada_contracte)
        data_final = (date(year, month, day)
                      + timedelta(days=days)).strftime('%Y-%m-%d')
        return data_final

    def get_vals_from_polissa(self, cursor, uid, polissa_id, context=None):
        """Obté els valors per emplenar una modificació contractual.

        Valors generals que comparteixen tant distribuidora com comercialitzado-
        ra. Després cada mòdul especific pot modificar aquesta funció per esta-
        blir com s'han de copiar els camps de la modificació contractual.
        """
        fields_to_read = self.get_fields_for_polissa(cursor, uid, context)
        polissa_obj = self.pool.get('giscegas.polissa')
        vals = polissa_obj.read(cursor, uid, [polissa_id], fields_to_read,
                                context)[0]
        # Transformem els many2one (id, name) a id
        for field, value in vals.items():
            if isinstance(value, type(())):
                vals[field] = value[0]
            if (isinstance(value, list) and
                        self.fields_get(cursor, uid, [field])[field][
                            'type'] == 'many2many'):
                vals[field] = [(6, 0, value)]
        # Assignem 'id' a 'polissa_id' i eliminem la clau 'id'
        vals['polissa_id'] = vals['id']
        del vals['id']

        polissa = polissa_obj.browse(cursor, uid, polissa_id, context)


        # Assignem data_inici a la modificació contractual
        if (polissa.modcontractual_activa
            and polissa.modcontractual_activa.data_final):
            year, month, day = map(int,
                                   polissa.modcontractual_activa.data_final.split(
                                       '-'))
            data_inici = (date(year, month, day)
                          + timedelta(days=1)).strftime('%Y-%m-%d')
        else:
            data_inici = polissa.data_alta

        # Si té data de baixa ja li assignem (p. exemple contractes de fires)
        if polissa.data_baixa:
            data_final = polissa.data_baixa
        else:
            data_final = self.get_data_final(cursor, uid, data_inici, context)

        vals['data_inici'] = data_inici
        vals['data_final'] = data_final

        # Mirem si és del tipus 'alta' o 'modificacio'
        if polissa.modcontractual_activa and len(polissa.modcontractuals_ids):
            vals['tipus'] = 'mod'
        else:
            vals['tipus'] = 'alta'

        # Hem de posar la seqüencia que serà el número de modificacions que
        # tinguem fetes
        vals['name'] = len(polissa.modcontractuals_ids) + 1

        return vals

    def undo_last_modcontractual(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        ctx = context.copy()

        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res = False
        polissa_model = 'giscegas.polissa'
        polissa_obj = self.pool.get(polissa_model)

        # If not last modcon, cannot make the change

        fields_to_read = ['modcontractual_seg', 'polissa_id']
        modcon = self.read(cursor, uid, ids, fields_to_read)[0]

        if modcon['modcontractual_seg']:
            raise osv.except_osv('Error',
                                 _(u"No se puede borrar esta "
                                   u"modificación contractual porque no es"
                                   u"la última."))
        else:
            id_pol = modcon['polissa_id'][0]
            res = polissa_obj.undo_last_modcontractual(
                cursor, uid, [id_pol], context
            )

        return res

    def aplicar_modificacio(self, cursor, uid, mod_id, polissa_id=None):
        """Aplica els canvis d'una modificació contractual a una pòlissa."""
        polissa_obj = self.pool.get('giscegas.polissa')
        fields_to_read = self.get_fields_for_polissa(cursor, uid)

        changes = self.read(cursor, uid, [mod_id], fields_to_read)[0]
        # Eliminem la clau 'id' que sempre la retorna al fer un read
        changes['modcontractual_activa'] = changes['id']
        del changes['id']
        # Hem de tenir en compte que els many2one retornen sempre
        # tuple(id, name)
        for field, value in changes.items():
            if isinstance(value, type(())):
                changes[field] = value[0]
            if (isinstance(value, list) and
                        self.fields_get(cursor, uid, [field])[field][
                            'type'] == 'many2many'):
                changes[field] = [(6, 0, value)]

        if not polissa_id:
            polissa_id = self.browse(cursor, uid, mod_id).polissa_id.id

        # Apliquem aquests canvis
        polissa_obj.write(cursor, uid, [polissa_id], changes)
        return True

    def append_modificacio(self, cursor, uid, mod_id):
        """Afegeix una modificació contractual a la llista i actualitza els
        camps anterior i següent."""
        modcontractual = self.browse(cursor, uid, mod_id)
        if modcontractual.polissa_id.modcontractual_activa:
            mod_activa = modcontractual.polissa_id.modcontractual_activa
            mod_activa.write({'modcontractual_seg': mod_id})
            modcontractual.write({'modcontractual_ant': mod_activa.id})
        return True

    def renovar(self, cursor, uid, ids, context=None):
        """Renovem el contracte amb els valors actuals de la pòlissa.
        """
        fields_read = ['polissa_id', 'observacions', 'data_final']
        for modcontract in self.read(cursor, uid, ids, fields_read, context):
            vals = self.get_vals_from_polissa(cursor, uid,
                                              modcontract['polissa_id'][0],
                                              context)
            if modcontract['observacions']:
                modcontract['observacions'] += '\n'
            else:
                modcontract['observacions'] = ''
            modcontract['observacions'] += _(
                u'* Renovación automatica a %s. Fecha final anterior %s. '
                u'Nueva fecha final %s' % (
                    '/'.join(reversed(vals['data_inici'].split('-'))),
                    '/'.join(reversed(modcontract['data_final'].split('-'))),
                    '/'.join(reversed(vals['data_final'].split('-'))),
                )
            )
            update = {
                'data_final': vals['data_final'],
                'observacions': modcontract['observacions']
            }
            self.write(cursor, uid, modcontract['id'],
                       update, {'sync': False})
        return True

    # Workflow Stuff
    def wkf_esborrany(self, cursor, uid, ids):
        """Estat esborrany."""
        for modcontractual in self.browse(cursor, uid, ids):
            self.append_modificacio(cursor, uid, modcontractual.id)
        self.write(cursor, uid, ids,
                   {'state': 'esborrany'},
                   {'sync': False})
        return True

    def wkf_pendent(self, cursor, uid, ids):
        """Estat pendent."""
        self.write(cursor, uid, ids,
                   {'state': 'pendent'},
                   {'sync': False})
        return True

    def wkf_actiu(self, cursor, uid, ids):
        """Activem la modificació contractual."""
        for modcontractual in self.browse(cursor, uid, ids):
            mod_id = modcontractual.id
            modcontractual.polissa_id.write({'modcontractual_activa': mod_id},
                                            {'sync': False})
            # Escrivim l'estat per tal que si hi ha modcontractual_ant vegi
            # que hem activat la següent
            modcontractual.write({'state': 'actiu', 'active': 1},
                                 {'sync': False})
            # Enviem un signal de guardar a l'anterior si n'hi ha per tal
            # que comprovi les condicions del workflow
            if modcontractual.modcontractual_ant:
                modcontractual.modcontractual_ant.write({})

        return True

    def cnd_activar_contracte(self, cursor, uid, ids):
        """Comprovem la condició de contracte actiu."""
        for mod_contractual in self.browse(cursor, uid, ids):
            if mod_contractual.data_inici <= time.strftime('%Y-%m-%d'):
                return True
            else:
                return False

    def cnd_baixa(self, cursor, uid, ids):
        """Comprovem que la pòlissa es pot donar de baixa.

        La baixa normal és quan no hi ha cap modificació contractual
        següent."""
        for modcontractual in self.browse(cursor, uid, ids):
            if not modcontractual.modcontractual_seg:
                return True
            else:
                return False

    def wkf_baixa(self, cursor, uid, ids):
        """Estat baixa."""
        self.write(cursor, uid, ids,
                   {'state': 'baixa', 'active': 0},
                   {'sync': False})
        return True

    def cnd_baixa2(self, cursor, uid, ids):
        """Comprovació baixa2 (Baja per modificació).

        Si la modificació té una següent és del tipus 'modificació' vol dir
        que aquesta és baixa per modificació
        """
        for modcontractual in self.browse(cursor, uid, ids):
            mod_seg = modcontractual.modcontractual_seg
            if mod_seg and mod_seg.state == 'actiu' and mod_seg.tipus == 'mod':
                return True
            else:
                return False

    def wkf_baixa2(self, cursor, uid, ids):
        """Estat baixa2  (Baja per modificació)."""
        self.write(cursor, uid, ids,
                   {'state': 'baixa2', 'active': 0},
                   {'sync': False})
        return True

    def cnd_baixa3(self, cursor, uid, ids):
        """Comprovem que és baixa per renovació.

        La pòlissa ha de tenir una modificació següent i aquesta ha de ser del
        tipus Renovació (reno)."""
        for modcontractual in self.browse(cursor, uid, ids):
            mod_seg = modcontractual.modcontractual_seg
            if mod_seg and mod_seg.state == 'actiu' and mod_seg.tipus == 'reno':
                return True
            else:
                return False

    def wkf_baixa3(self, cursor, uid, ids):
        """Estat baixa3 ((Baja per renovació)."""
        self.write(cursor, uid, ids,
                   {'state': 'baixa3', 'active': 0},
                   {'sync': False})
        return True

    def cnd_baixa4(self, cursor, uid, ids):
        """Comprovació baixa per Nova pòlissa.

        La pòlissa ha de tenir una modificació següent que apunti a una altre
        pòlissa i que sigui del tipus alta."""
        for modcontractual in self.browse(cursor, uid, ids):
            mod_seg = modcontractual.modcontractual_seg
            if mod_seg and mod_seg.tipus == 'alta' and \
                            mod_seg.state == 'actiu' and \
                            mod_seg.polissa_id.id != modcontractual.polissa_id.id:
                return True
            else:
                return False

    def wkf_baixa4(self, cursor, uid, ids):
        """Estat baixa4 ((Baja per Póliza nova)."""
        self.write(cursor, uid, ids,
                   {'state': 'baixa4', 'active': 0},
                   {'sync': False})
        return True

    def wkf_cancel(self, cursor, uid, ids):
        """Estat cancel·lat."""
        self.write(cursor, uid, ids,
                   {'state': 'cancel'},
                   {'sync': False})
        return True

    def get_modcon_strings(self, cursor, uid, ids):
        """Plantilla pel missatge d'accions a les modificacions contractuals.
        """
        msg = []
        for mod in self.browse(cursor, uid, ids):
            msg += [_(u"* Póliza %s - %s" % (mod.polissa_id.name,
                                              mod.polissa_id.titular.name))]
            msg += [_(u"    Modificaciónn contractual %s. Inicio: %s Final: %s "
                      % (mod.name, mod.data_inici, mod.data_final))]
        return msg

    def send_action_message(self, cursor, uid, emails_to, requests_to, subject,
                            message):
        """Envia el missatge per correu o request.
        """
        user_obj = self.pool.get('res.users')
        req_obj = self.pool.get('res.request')
        if emails_to:
            email_from = user_obj.browse(cursor, uid, uid).address_id.email
            email_send(email_from, emails_to, subject, message)
        if requests_to:
            vals = {
                'name': subject,
                'body': message,
                'act_from': uid,
            }
            for login in requests_to:
                act_to = user_obj.search(cursor, uid, [('login', '=', login)])
                if act_to:
                    vals.update({'act_to': act_to[0]})
                    req_obj.create(cursor, uid, vals)

    def get_contractes_action_message(self, cursor, uid, context=None):
        """Escrivim el missatge per les accions que volem fer.

        El missatge retornarà diferents resultats segons les accions que es
        vulguin fer. Per això s'haurà de passar via context un clau 'actions'
        amb un string amb les inicials de les accions que es volen dur a terme.
        'A': Altes, 'B': Baixes, 'R': Renovació. (Ex. {'actions': 'RAB'} per
        fer-les totes)
        """
        if not context:
            context = {}
        actions = context.get('actions', '')
        if not actions:
            return ''
        msg = []
        if 'B' in actions:
            # Missatge per les baixes que es faran
            ids = self.get_contractes_baixa(cursor, uid, context)
            if ids:
                if msg:
                    msg += ['', '']
                msg += [_(u'**** BAJAS (%s) ****' % len(ids))]
                msg += self.get_modcon_strings(cursor, uid, ids)
        if 'A' in actions:
            # Missatge per les altes que es faran
            ids = self.get_contractes_activar(cursor, uid, context)
            if ids:
                if msg:
                    msg += ['', '']
                msg += [_(u'**** ALTES (%s) ****' % len(ids))]
                msg += self.get_modcon_strings(cursor, uid, ids)
        if 'R' in actions:
            # Missatge per les renovacions que es faran
            ids = self.get_contractes_renovar(cursor, uid, context)
            if ids:
                if msg:
                    msg += ['', '']
                msg += [_(u'**** RENOVACIONES (%s) ****' % len(ids))]
                msg += self.get_modcon_strings(cursor, uid, ids)
        return '\n'.join(msg)

    def _cronjob_message(self, cursor, uid, data=None, context=None):
        """CronJob per enviar un correu amb les accions que es portaran a terme.
        """
        if not data:
            data = {}
        if not context:
            context = {}
        emails_to = filter(lambda a: bool(a),
                           map(str.strip, data.get('emails_to', '').split(',')))
        requests_to = filter(lambda a: bool(a),
                             map(str.strip,
                                 data.get('requests_to', '').split(',')))
        execute_date = (datetime.now()
                        + timedelta(days=context.get('delay_days', 0)))
        subject = _(u"Accions planificades ERP")
        message = _(u"Aquestes accions es produiran a partir de la data %s\n\n"
                    % execute_date.strftime('%d/%m/%Y'))
        message += self.get_contractes_action_message(cursor, uid, context)
        self.send_action_message(cursor, uid, emails_to, requests_to, subject,
                                 message)
        return True

    def _cronjob_batch_actions(self, cursor, uid, data=None, context=None):
        """CronJob per fer totes les accions batch sobre les pòlisses.

        Ho fem amb un sol cronjob per evitar problemes en les transaccions a
        la base de dades. Amb una sola funció i un sol cron es farà tot amb la
        mateixa transacció."""
        if not data:
            data = {}
        if not context:
            context = {}
        emails_to = filter(lambda a: bool(a),
                           map(str.strip, data.get('emails_to', '').split(',')))
        requests_to = filter(lambda a: bool(a),
                             map(str.strip,
                                 data.get('requests_to', '').split(',')))
        subject = _(u"Resultat accions batch en modificacions contractuals")
        msg = []
        # 1. Donem de baixa els contractes que s'han configurat
        msg += [_(u'**** BAIXES ****')]
        msg += self._cronjob_baixa_contractes(cursor, uid, data, context)
        # 2. Activem els nous
        msg += ['', '']
        msg += [_(u'**** ALTES ****')]
        msg += self._cronjob_activar_contractes(cursor, uid, data, context)
        # 3. Renovem els actius que caduquin
        msg += ['', '']
        msg += [_(u'**** RENOVACIONS ****')]
        msg += self._cronjob_renovar_contractes(cursor, uid, data, context)
        message = u'\n'.join(msg)
        self.send_action_message(cursor, uid, emails_to, requests_to, subject,
                                 message)
        return True

    def get_contractes_activar(self, cursor, uid, context=None):
        """Busquem quins contractes compleixen la condició que s'activaran.

        """
        if not context:
            context = {}
        delay = context.get('delay_days', 1)
        act_date = (datetime.now() + timedelta(days=delay)).strftime('%Y-%m-%d')
        search_params = [('state', '=', 'pendent'),
                         ('data_inici', '<=', act_date),
                         ('polissa_id.active', '=', 1)]
        ids = self.search(cursor, uid, search_params, order="data_inici asc",
                          context={'active_test': False})
        return ids

    # Cada dia a les 00:00 es cridarà aquesta funció per comprovar si hi ha
    # contractes pendents d'aplicar, en cas que n'hi hagi desactivarà el vell
    # i aplicarà el nou.
    def _cronjob_activar_contractes(self, cursor, uid, data=None, context=None):
        """Activem totes les modificacions contractuals pendents."""
        if not data:
            data = {}
        if not context:
            context = {}
        ids = self.get_contractes_activar(cursor, uid,
                                          context={'delay_days': 0})
        # Obrim un cursor per cada un i així podem avancem
        msg = []
        for mc_id in ids:
            cursor_tmp = pooler.get_db_only(cursor.dbname).cursor()
            try:
                self.write(cursor_tmp, uid, [mc_id], {'active': 1},
                           context={'sync': False})
                self.aplicar_modificacio(cursor_tmp, uid, mc_id)
                cursor_tmp.commit()
            except Exception as e:
                msg += self.get_modcon_strings(cursor, uid, [mc_id])
                msg += [u'    ERROR: %s' % e]
                cursor_tmp.rollback()
            finally:
                cursor_tmp.close()
        if not msg:
            msg = [_(u"Acció feta correctament.")]
        return msg

    def get_contractes_renovar(self, cursor, uid, context=None):
        """Retorna els ids dels contractes que es renovaran.
        """
        if not context:
            context = {}
        # Això ens dirà que s'han de renovar en aquests moments
        delay = context.get('delay_days', 0)
        day = (datetime.now() + timedelta(days=delay)).strftime('%Y-%m-%d')
        search_params = [('state', '=', 'actiu'),
                         ('data_final', '<=', day),
                         ('polissa_id.active', '=', 1),
                         ('polissa_id.renovacio_auto', '=', 1),
                         ('modcontractual_seg', '=', False)]
        ids = self.search(cursor, uid, search_params, order="data_inici asc",
                          context={'active_test': False})
        return ids

    # Cada dia a les 00:00 es cridarà aquesta funció per comrpvoar si hi ha
    # contractes que caduquin, en aquest cas es crearà un contracte nou de
    # renovació
    def _cronjob_renovar_contractes(self, cursor, uid, data=None, context=None):
        """Busquetem totes les modificacions contractuals que caduguin avui per
        renovar-les."""
        # Busquem totes els modificacions contractuals que estiguin actives i
        # que data_final sigui ahir i que tinguin activada la renovació auto-
        # màtica. Com que el cronjob s'executa a les 00:00 esteriem perdent
        # un dia de contracte
        if not data:
            data = {}
        if not context:
            context = {}
        ids = self.get_contractes_renovar(cursor, uid,
                                          context={'delay_days': -1})
        msg = []
        for mc_id in ids:
            cursor_tmp = pooler.get_db_only(cursor.dbname).cursor()
            try:
                self.renovar(cursor_tmp, uid, [mc_id], context={'sync': False})
                cursor_tmp.commit()
            except Exception as e:
                msg += self.get_modcon_strings(cursor, uid, [mc_id])
                msg += [u'    ERROR: %s' % e]
                cursor_tmp.rollback()
            finally:
                cursor_tmp.close()
        if not msg:
            msg = [_(u"Acció feta correctament.")]
        return msg

    def get_contractes_baixa(self, cursor, uid, context=None):
        """Retorna els contractes que s'han de donar de baixa.
        """
        if not context:
            context = {}
        delay = context.get('delay_days', 0)
        day = (datetime.now() + timedelta(days=delay)).strftime('%Y-%m-%d')
        search_params = [('state', '=', 'actiu'),
                         ('data_final', '<=', day),
                         ('polissa_id.active', '=', 1),
                         ('polissa_id.renovacio_auto', '=', 0)]
        ids = self.search(cursor, uid, search_params, order="data_final asc",
                          context={'active_test': False})
        return ids

    def _cronjob_baixa_contractes(self, cursor, uid, data=None, context=None):
        """Busquem totes les modificacions contractuals que s'han de donar
        de baixa automàticament."""
        if not data:
            data = {}
        if not context:
            context = {}
        ids = self.get_contractes_baixa(cursor, uid, context={'delay_days': -1})
        msg = []
        for modcontractual in self.read(cursor, uid, ids, ['polissa_id']):
            cursor_tmp = pooler.get_db_only(cursor.dbname).cursor()
            try:
                polissa_id = modcontractual['polissa_id'][0]
                wf_service = netsvc.LocalService('workflow')
                wf_service.trg_validate(uid, 'giscegas.polissa', polissa_id,
                                        'baixa', cursor_tmp)
                cursor_tmp.commit()
            except Exception as e:
                msg += self.get_modcon_strings(cursor, uid,
                                               [modcontractual['id']])
                msg += [u'    ERROR: %s' % e]
                cursor_tmp.rollback()
            finally:
                cursor_tmp.close()
        if not msg:
            msg = [_(u"Acció feta correctament.")]
        return msg

    # Constraints
    def _cnt_check_date(self, cursor, uid, ids):
        """Comprova que no hi hagi dates superposades."""
        for mod_contrac in self.browse(cursor, uid, ids):
            # Busquem si ja hi ha un contracte solapat
            # (Fem servir la funció OVERLAPS de PostgreSQL)
            for contracte in mod_contrac.polissa_id.modcontractuals_ids:
                if contracte.id != mod_contrac.id:
                    cursor.execute("""select (date %s, date %s) 
                    overlaps (date %s, date %s)""", (mod_contrac.data_inici,
                                                     mod_contrac.data_final,
                                                     contracte.data_inici,
                                                     contracte.data_final))
                    res = cursor.fetchone()
                    if res and res[0]:
                        return False
        return True

    def _cnt_check_gaps(self, cursor, uid, ids):
        """Comprova que no hi hagi forats entre contractes."""
        for mod_contract in self.browse(cursor, uid, ids):
            # Només ho comprovem si la modificació contractual
            # té contracte anterior
            if mod_contract.modcontractual_ant:
                # Restem la data_final de l'anterior de la data_inici
                # de l'actual si és més gran que 1 vol dir que hi ha
                # més d'un dia de diferència
                cursor.execute("select date %s - date %s",
                               (mod_contract.data_inici,
                                mod_contract.modcontractual_ant.data_final))
                dies = cursor.fetchone()[0]
                if dies > 1:
                    return False
        return True

    # TODO: Fer tests
    def _cnt_check_cups(self, cursor, uid, ids):
        for mod_contract in self.browse(cursor, uid, ids):
            if mod_contract.state == 'active':
                for contracte in mod_contract.cups.modcontractuals_ids:
                    if (mod_contract.id != contracte.id
                        and contracte.state in ('actiu', 'baixa',
                                                'baixa2', 'baixa3')):
                        year, month, day = map(int,
                                               contracte.data_final.split('-'))
                        data_final = (date(year, month, day)
                                      + timedelta(days=1)).strftime('%Y-%m-%d')
                        cursor.execute("""select (date %s, date %s) 
                    overlaps (date %s, date %s)""", (mod_contract.data_inici,
                                                     mod_contract.data_final,
                                                     contracte.data_inici,
                                                     data_final))
                        res = cursor.fetchone()
                        if res and res[0]:
                            return False
        return True

    _STORE_PROC_AGREE_FIELDS = {
        'giscegas.polissa.modcontractual': (lambda self, cursor, uid, ids,
                                                    context=None: ids,
                                             ['pressio', 'tarifa'], 20),
    }

    _columns = {
        'name': fields.char('Código modificació', size=64, readonly=True,
                            required=True),
        'polissa_id': fields.many2one('giscegas.polissa', 'Póliza',
                                      required=True, ondelete='cascade',
                                      select=True),
        'modcontractual_ant': fields.many2one(
            'giscegas.polissa.modcontractual',
            'Modificación anterior'),
        'modcontractual_seg': fields.many2one(
            'giscegas.polissa.modcontractual',
            'Modificación següent', ondelete="set null"),
        'active': fields.boolean('Activa', readonly=True, required=True),
        'state': fields.selection(_modcontractual_states_selection, 'Estado',
                                  required=True, readonly=True),
        'tipus': fields.selection(_tipus_modificacio_contractual, 'Tipus',
                                  required=True),
        # Camps comuns entre comercialitzadora i distribuidora
        'cups': fields.many2one('giscegas.cups.ps', 'CUPS', required=True),
        'titular': fields.many2one('res.partner', 'Titular'),
        'tarifa': fields.many2one('giscegas.polissa.tarifa', 'Tarifa',
                                  required=True),
        'pressio': fields.float('Presión contratada (bares)', digits=(16, 3),
                                 required=True),
        'caudal_diario': fields.float(
            'Caudal diario (KWh/dia)', digits=(16,7), required=True,
        ),
        'observacions': fields.text('Observacions'),
        'data_inici': fields.date('Data inici', required=True, select=True),
        'data_final': fields.date('Data final', select=True),
        'data_final_pre_baixa': fields.date('Data final (pre-baixa)'),
        'cnae': fields.many2one('giscemisc.cnae', 'CNAE', ondelete='restrict'),
        'data_firma_contracte': fields.datetime('Data firma contracte'),
        'nocutoff': fields.many2one('giscegas.polissa.nocutoff',
                                    'Suministro no cortable'),
        'contract_type': fields.selection(
            CONTRACT_TYPES, 'Tipus de contracte', required=True,
            help="Distintos tipos de contratos. Ver REAL DECRETO 1164/2001"
        ),
        'tipus_vivenda': fields.selection(
            [('habitual', 'Habitual'), ('no_habitual', 'No habitual')],
            string='Tipus vivenda', readonly=True,
        )
    }

    _constraints = [
        (_cnt_check_date, 'Ja existeix un contracte en aquestes dates',
         ['data_inici', 'data_final']),
        (_cnt_check_gaps, 'Hi ha dies de separació entre contractes',
         ['data_inici']),
        (_cnt_check_cups,
         'Ja hi ha un CUPS en aquestes dates a un altre contracte', ['cups']),
    ]

    _sql_constraints = [
        ('date_coherence',
         "CHECK (data_inici <= coalesce(data_final, '2038-01-01'))",
         'La fecha de inicio debe ser anterior a la fecha final'),
    ]

    _defaults = {
        'active': lambda *a: 1,
        'state': lambda *a: 'esborrany',
        'contract_type': lambda *a: '01',
    }

    _order = 'data_final desc'


GiscegasPolissaModcontractual()


# Un cop creada la taula modificació contractual podem posar l'activa a la
# pòlissa, sino per dependències no ens deixa crear les taules
class GiscegasPolissaExtend(osv.osv):
    """Extensió per afegir el la modificació contractual activa."""
    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    _columns = {
        'modcontractual_activa': fields.many2one(
            'giscegas.polissa.modcontractual',
            'Modificación contractual actual',
            readonly=True),
    }


GiscegasPolissaExtend()
