# -*- coding: utf-8 -*-
{
    "name": "Esfinge GAS",
    "description": """ Esfinge GAS
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscegas_facturacio",
        "giscedata_administracio_publica"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_esfinge_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
