# -*- coding: utf-8 -*-report
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from datetime import datetime
import base64
from giscegas_esfinge.esfinge.esfinge import generate_esfinge_from_data
from tools import config


class WizardEsfinge(osv.osv_memory):

    _name = 'wizard.esfinge'

    def build_report(self, cursor, uid, ids, context=None):
        """ Report """

        wizard = self.read(cursor, uid, ids, context=context)[0]
        start_date = wizard['start_date']
        end_date = wizard['end_date']
        user = self.pool.get("res.users").browse(cursor, uid, uid)

        filename = 'esfinge_{0}_{1}.xml'.format(end_date, start_date)
        query_file = (u"%s/giscegas_esfinge/data/esfinge.sql" % config['addons_path'])
        query = open(query_file).read()

        cursor.execute(query, {"limit_active_date": end_date, "start_date": start_date, "end_date": end_date})
        res = cursor.fetchall()
        report = generate_esfinge_from_data(
            {
                '9': {
                    'descripcio': "GAS NATURAL",
                    'codi_distribucio': "3",
                    "codi_unitats": "20",
                    'data': res
                }
            },
            vat=user.company_id.partner_id.vat,
            year=datetime.strptime(end_date, "%Y-%m-%d").year
        )

        self.write(cursor, uid, ids, {
            'report': base64.b64encode(str(report)),
            'filename_report': filename,
        })

        return True

    _columns = {
        'filename_report': fields.char('Nombre fichero', size=256),
        'start_date': fields.date('Fecha inicio'),
        'end_date': fields.date('Fecha fin'),
        'report': fields.binary('Report'),
    }

    _defaults = {
        'start_date': lambda *a: "{0}-01-01".format(datetime.today().year-1),
        'end_date': lambda *a: "{0}-12-31".format(datetime.today().year-1),

    }

WizardEsfinge()
