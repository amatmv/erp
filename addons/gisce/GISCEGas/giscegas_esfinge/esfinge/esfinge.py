from libcomxml.core import XmlModel, XmlField
from datetime import datetime
import pandas as pd


class FORMULARIO(XmlModel):

    _sort_order = ('FORMULARIO', 'CABECERA', 'EMPRESA', 'CUESTIONARIO')

    def __init__(self):
        self.FORMULARIO = XmlField('FORMULARIO')
        self.CABECERA = CABECERA()
        self.EMPRESA = EMPRESA()
        self.CUESTIONARIO = CUESTIONARIO()
        super(FORMULARIO, self).__init__('FORMULARIO', 'FORMULARIO')


class CABECERA(XmlModel):

    _sort_order = ('CABECERA', 'VERSION_XML', 'ANIO')

    def __init__(self):
        self.CABECERA = XmlField('CABECERA')
        self.VERSION_XML = XmlField('VERSION_XML')
        self.ANIO = XmlField('ANIO')
        super(CABECERA, self).__init__('CABECERA', 'CABECERA')


class EMPRESA(XmlModel):

    _sort_order = ('EMPRESA', 'CIF_VAT')

    def __init__(self):
        self.EMPRESA = XmlField('EMPRESA')
        self.CIF_VAT = XmlField('CIF_VAT')
        super(EMPRESA, self).__init__('EMPRESA', 'EMPRESA')


class CUESTIONARIO(XmlModel):

    _sort_order = ('CUESTIONARIO', 'LISTA_GASES', 'OBSERVACIONES')

    def __init__(self):
        self.CUESTIONARIO = XmlField('CUESTIONARIO')
        self.LISTA_GASES = LISTA_GASES()
        self.OBSERVACIONES = XmlField('OBSERVACIONES')
        super(CUESTIONARIO, self).__init__('CUESTIONARIO', 'CUESTIONARIO')


class LISTA_GASES(XmlModel):

    _sort_order = ('LISTA_GASES', 'GAS')

    def __init__(self):
        self.LISTA_GASES = XmlField('LISTA_GASES')
        self.GAS = []
        super(LISTA_GASES, self).__init__('LISTA_GASES', 'LISTA_GASES')


class GAS(XmlModel):

    _sort_order = ('GAS', 'CODIGO_DISTRIBUCION', 'PROVINCIA')

    def __init__(self, attrs=None):
        if not attrs:
            attrs = {'CODIGO': '9', 'DESCRIPCION': 'GAS NATURAL'}
        self.GAS = XmlField('GAS', attributes=attrs)
        self.CODIGO_DISTRIBUCION = XmlField('CODIGO_DISTRIBUCION')
        self.PROVINCIA = []
        super(GAS, self).__init__('GAS', 'GAS')


class PROVINCIA(XmlModel):

    _sort_order = ('PROVINCIA', 'CNAE')

    def __init__(self, attrs=None):
        if not attrs:
            attrs = {'CODIGO': '', 'DESCRIPCION': ''}
        self.PROVINCIA = XmlField('PROVINCIA', attributes=attrs)
        self.CNAE = []
        super(PROVINCIA, self).__init__('PROVINCIA', 'PROVINCIA')


class CNAE(XmlModel):

    _sort_order = ('CNAE', 'NUM_CLIENTES', 'ENERGIA_COMERCIALIZADA', 'VALOR', 'CODIGO_UNIDADES')

    def __init__(self, attrs=None):
        if not attrs:
            attrs = {'CODIGO': '', 'DESCRIPCION': ''}
        self.CNAE = XmlField('CNAE', attributes=attrs)
        self.NUM_CLIENTES = XmlField('NUM_CLIENTES')
        self.ENERGIA_COMERCIALIZADA = XmlField('ENERGIA_COMERCIALIZADA')
        self.VALOR = XmlField('VALOR')
        self.CODIGO_UNIDADES = XmlField('CODIGO_UNIDADES')
        super(CNAE, self).__init__('CNAE', 'CNAE')


def generate_esfinge_from_data(gases_data_dict, vat, xml_version="1.0.0", year=None):
    """
    :param gases_data_dict: es tracte de un diccionari amb:
        * key: codi del tipus de gas, per exemple, "9" si es tracte de gas natural
        * value: diccionari amb {
            "descripcio": descripcio del tipus de gas, per exemple "GAS NATURAL",
            "codi_distribucio":  per "GAS NATURAL" es el "3", que vol dir canalizacion,
            "codi_unitats: "20" per mW
            "data": llistat de diccionaris amb:
                {
                    "PROVINCIA_CODIGO",
                    "PROVINCIA_DESCRIPCION",
                    "CNAE_CODIGO",
                    "CNAE_DESCRIPCION",
                    "NUM_CLIENTES",
                    "ENERGIA_COMERCIALIZADORA",
                    "VALOR"
                }
                Cada un d'aquests diccionaris esta destinat a ser un Node PROVINCIA/CNAE,
                es a dir que les dades ja han de venir agrupades per PROVINCIA/CNAE
        }
    :return: XML object
    """
    if not year:
        year = datetime.now().year

    if not vat:
        raise Exception("ERROR", "No s'ha proporcionat CIF/NIF")

    # Capcalera
    capcalera = CABECERA()
    capcalera.feed({
        'VERSION_XML': xml_version,
        'ANIO': year
    })

    # Empresa
    empresa = EMPRESA()
    empresa.feed({
        'CIF_VAT': vat
    })

    # Fem un node GAS per cada gas diferent
    llista_gasos = []
    for gas, gas_data in gases_data_dict.iteritems():
        df = pd.DataFrame(
            gas_data.get("data", []),
            columns=['PROVINCIA_CODIGO', 'PROVINCIA_DESCRIPCION', 'CNAE_CODIGO', 'CNAE_DESCRIPCION', 'NUM_CLIENTES', 'ENERGIA_COMERCIALIZADA', 'VALOR']
        )
        # Un node per cada provincia. com que les dades ja venen agrupades no ens hem de preocupar de ajuntar res
        provincia_nodes = []
        for provincia in df['PROVINCIA_CODIGO'].unique().tolist():
            prov_desc = df.loc[df['PROVINCIA_CODIGO'] == provincia]["PROVINCIA_DESCRIPCION"].tolist()[0]
            cnae_nodes = []
            # Un node per cada CNAE, com que les dades ja venen agrupades no ens hem de preocupar de ajuntar res
            for index, aux in df.loc[df['PROVINCIA_CODIGO'] == provincia].iterrows():
                new_cnae = CNAE({"CODIGO": aux["CNAE_CODIGO"], "DESCRIPCION": aux["CNAE_DESCRIPCION"]})
                new_cnae.feed({
                    "NUM_CLIENTES": aux["NUM_CLIENTES"],
                    "ENERGIA_COMERCIALIZADA": str(aux["ENERGIA_COMERCIALIZADA"]).replace('.', ','),
                    "VALOR": str(aux["VALOR"]).replace('.', ','),
                    "CODIGO_UNIDADES": gas_data.get("codi_unitats", "20")
                })
                cnae_nodes.append(new_cnae)

            new_provincia = PROVINCIA({"CODIGO": provincia, "DESCRIPCION": prov_desc})
            new_provincia.feed({"CNAE": cnae_nodes})
            provincia_nodes.append(new_provincia)

        gas = GAS(attrs={"CODIGO": gas, "DESCRIPCION": gas_data.get('descripcio', "GAS NATURAL")})
        gas.feed({'PROVINCIA': provincia_nodes, "CODIGO_DISTRIBUCION": gas_data.get("codi_distribucio", "3")})
        llista_gasos.append(gas)

    lg = LISTA_GASES()
    lg.feed({"GAS": llista_gasos})
    cuestionari = CUESTIONARIO()
    cuestionari.feed({'LISTA_GASES': lg})

    # Finalment ho ajuntem tot
    res = FORMULARIO()
    res.feed({
        "CABECERA": capcalera,
        "EMPRESA": empresa,
        "CUESTIONARIO": cuestionari
    })
    res.build_tree()
    return res
