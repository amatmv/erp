SELECT
    provincia.code as "PROVINCIA_CODIGO",
    upper(provincia.name) as "PROVINCIA_DESCRIPCION",

    CASE WHEN cnae.name  similar to '(01|02|03)%' THEN '1'
    WHEN cnae.name  similar to '(05|099)%' THEN '2'
    WHEN cnae.name  similar to '(06|091)%' THEN '3'
    WHEN cnae.name  similar to '(721|2446|353)%' THEN '4'
    WHEN cnae.name  similar to '(191)%' THEN '5'
    WHEN cnae.name  similar to '(192)%' THEN '6'
    WHEN cnae.name  similar to '(351)%' THEN '7'
    WHEN cnae.name  similar to '(352)%' THEN '8'
    WHEN cnae.name  similar to '(07|08)%' and not cnae.name similar to '(0721)%' THEN '9'
    WHEN cnae.name  similar to '(24)%' and not cnae.name similar to '(244|2453|2454)%' THEN '10'
    WHEN cnae.name  similar to '(244|2453|2454)%' THEN '11'
    WHEN cnae.name  similar to '(231)%' THEN '12'
    WHEN cnae.name  similar to '(235)%' THEN '13'
    WHEN cnae.name  similar to '(23)%' and not cnae.name similar to '(231|235)%' THEN '14'
    WHEN cnae.name  similar to '(20|21)%' THEN '15'
    WHEN cnae.name  similar to '(25|26|27|28)%' THEN '16'
    WHEN cnae.name  similar to '(29|309)%' THEN '17'
    WHEN cnae.name  similar to '(301|3315)%' THEN '18'
    WHEN cnae.name  similar to '(302|303|304)%' THEN '19'
    WHEN cnae.name  similar to '(10|11|12)%' THEN '20'
    WHEN cnae.name  similar to '(13|14|15)%' THEN '21'
    WHEN cnae.name  similar to '(16)%' THEN '22'
    WHEN cnae.name  similar to '(17)%' THEN '23'
    WHEN cnae.name  similar to '(18|581)%' THEN '24'
    WHEN cnae.name  similar to '(22|31|32|33)%' and not cnae.name similar to '(3315)%' THEN '25'
    WHEN cnae.name  similar to '(41|42|43)%' THEN '26'
    WHEN cnae.name  similar to '(491|492)%' THEN '27'
    WHEN cnae.name  similar to '(4939|494)%' THEN '28'
    WHEN cnae.name  similar to '(4931|4932|495|50|51|52)%' THEN '29'
    WHEN cnae.name  similar to '(55|56)%' THEN '30'
    WHEN cnae.name  similar to '(36|37|38|39|53|60|61|72|84|85|86|87|88|91|99)%' and not cnae.name similar to '(855|856)%' THEN '31'
    WHEN cnae.name  similar to '(473)%' THEN '32'
    WHEN cnae.name  similar to '(45|46|47|582|59|62|63|64|65|66|67|68|69|70|71|75|76|77|82|83|84|855|856|90|92|93|94|95|96)%' THEN '33'
    WHEN cnae.name  similar to '(97|98)%' THEN '34'
    ELSE '35'
    END AS "CNAE_CODIGO",

    CASE WHEN cnae.name  similar to '(01|02|03)%' THEN 'AGRICULTURA, GANADERIA, SILVICULTURA, CAZA Y PESCA'
    WHEN cnae.name  similar to '(05|099)%' THEN 'EXTRACCION Y AGLOMERACION DE CARBONES'
    WHEN cnae.name  similar to '(06|091)%' THEN 'EXTRACCION DE PETROLEO Y GAS'
    WHEN cnae.name  similar to '(721|2446|353)%' THEN 'COMBUSTIBLES NUCLEARES Y OTRAS ENERGIAS'
    WHEN cnae.name  similar to '(191)%' THEN 'COQUERIAS'
    WHEN cnae.name  similar to '(192)%' THEN 'REFINERIAS DE PETROLEO'
    WHEN cnae.name  similar to '(351)%' THEN 'PRODUCCION Y DISTRIBUCION DE ENERGIA ELECTRICA'
    WHEN cnae.name  similar to '(352)%' THEN 'FABRICAS DE GAS-DISTRIBUCION DE GAS'
    WHEN cnae.name  similar to '(07|08)%' and not cnae.name similar to '(0721)%' THEN 'MINAS Y CANTERAS (NO ENERGETICAS)'
    WHEN cnae.name  similar to '(24)%' and not cnae.name similar to '(244|2453|2454)%' THEN 'SIDERURGIA Y FUNDICION'
    WHEN cnae.name  similar to '(244|2453|2454)%' THEN 'METALURGIA NO FERREA'
    WHEN cnae.name  similar to '(231)%' THEN 'INDUSTRIA DEL VIDRIO'
    WHEN cnae.name  similar to '(235)%' THEN 'CEMENTO, CALES Y YESOS'
    WHEN cnae.name  similar to '(23)%' and not cnae.name similar to '(231|235)%' THEN 'OTROS MATERIALES DE CONSTRUCCION (LOZA, PORCELANA, REFRACTARIOS, ETC.)'
    WHEN cnae.name  similar to '(20|21)%' THEN 'QUIMICA Y PETROQUIMICA'
    WHEN cnae.name  similar to '(25|26|27|28)%' THEN 'MAQUINAS Y TRANSFORMADOS METALICOS'
    WHEN cnae.name  similar to '(29|309)%' THEN 'CONSTRUCCION DE VEHÍCULOS A MOTOR. MOTOCICLETAS Y BICICLETAS'
    WHEN cnae.name  similar to '(301|3315)%' THEN 'CONSTRUCCION Y REPARACION NAVAL'
    WHEN cnae.name  similar to '(302|303|304)%' THEN 'CONSTRUCCION DE OTROS MEDIOS DE TRANSPORTE'
    WHEN cnae.name  similar to '(10|11|12)%' THEN 'ALIMENTACION, BEBIDAS Y TABACO'
    WHEN cnae.name  similar to '(13|14|15)%' THEN 'IND. TEXTIL, CONFECCION, CUERO Y CALZADO'
    WHEN cnae.name  similar to '(16)%' THEN 'IND. DE MADERA Y CORCHO (EXC. FABRICACION DE MUEBLES)'
    WHEN cnae.name  similar to '(17)%' THEN 'PASTAS PAPELERAS, PAPEL, CARTON, MANIPULADOS'
    WHEN cnae.name  similar to '(18|581)%' THEN 'ARTES GRAFICAS Y EDICION'
    WHEN cnae.name  similar to '(22|31|32|33)%' and not cnae.name similar to '(3315)%' THEN 'IND. CAUCHO, MAT. PLASTICAS Y OTRAS NO ESPECIFICADAS'
    WHEN cnae.name  similar to '(41|42|43)%' THEN 'CONSTRUCCION Y OBRAS PUBLICAS'
    WHEN cnae.name  similar to '(491|492)%' THEN 'TRANSPORTE INTERURBANO POR FF. CC.'
    WHEN cnae.name  similar to '(4939|494)%' THEN 'TRANSPORTE INTERURBANO POR CARRETERA (VIAJEROS, MERCANCIAS)'
    WHEN cnae.name  similar to '(4931|4932|495|50|51|52)%' THEN 'OTRAS EMPRESAS DE TRANSPORTE'
    WHEN cnae.name  similar to '(55|56)%' THEN 'HOSTELERIA'
    WHEN cnae.name  similar to '(36|37|38|39|53|60|61|72|84|85|86|87|88|91|99)%' and not cnae.name similar to '(855|856)%' THEN 'ADMINISTRACION Y OTROS SERVICIOS PUBLICOS'
    WHEN cnae.name  similar to '(473)%' THEN 'COMERCIO AL POR MENOR DE COMBUSTIBLE PARA LA AUTOMOCIÓN EN ESTABLECIMIENTOS ESPECIALIZADOS'
    WHEN cnae.name  similar to '(45|46|47|582|59|62|63|64|65|66|67|68|69|70|71|75|76|77|82|83|84|855|856|90|92|93|94|95|96)%' THEN 'COMERCIO Y SERVICIOS'
    WHEN cnae.name  similar to '(97|98)%' THEN 'USOS DOMESTICOS'
    ELSE 'NO ESPECIFICADOS'
    END AS "CNAE_DESCRIPCION",

--Numero de clients actius a final d'any

    (
        select count(*)
        from
            giscegas_polissa_modcontractual m2
            join giscegas_cups_ps cups2 ON cups2.id = m2.cups
            left join res_municipi municipi2 ON cups2.id_municipi = municipi2.id
            left join res_country_state provincia2 ON provincia2.id = municipi2.state
        where
            m2.data_final >= '2019-12-31'
            and provincia2.code = provincia.code
            and m2.cnae=cnae.id

    ) as "NUM_CLIENTES",

--Numero de clients actius durant d'any

/*    (
        select count(*)
        from
            giscegas_polissa_modcontractual m2
            join giscegas_cups_ps cups2 ON cups2.id = m2.cups
            left join res_municipi municipi2 ON cups2.id_municipi = municipi2.id
            left join res_country_state provincia2 ON provincia2.id = municipi2.state
        where
            (m2.data_final >= '2019-12-31' or m2.data_final between '2019-01-01' AND '2019-12-31')
            and provincia2.code = provincia.code
            and m2.cnae=cnae.id

    ) as "NUM_CLIENTES",
*/
    ROUND(SUM(fact.consumo_kwh) * 0.001, 2) as "ENERGIA_COMERCIALIZADA",
    ROUND(SUM(fact.total_variable), 2) as "VALOR"
FROM
    giscegas_facturacio_factura fact
    LEFT JOIN account_invoice inv ON inv.id=fact.invoice_id
    LEFT JOIN giscegas_cups_ps cups ON cups.id=fact.cups_id
    LEFT JOIN res_municipi municipi ON cups.id_municipi=municipi.id
    LEFT JOIN res_country_state provincia ON provincia.id=municipi.state
    LEFT JOIN giscegas_polissa pol ON fact.polissa_id=pol.id
    JOIN giscemisc_cnae cnae ON cnae.id=pol.cnae
WHERE
    inv.type in ('out_invoice')
    AND inv.date_invoice BETWEEN '2019-01-01' AND '2019-12-31'
GROUP BY
    provincia.code,
    provincia.name,
    cnae.name,
    cnae.id
ORDER BY provincia.code, cnae.name
