SELECT 
    prov.code AS PROVINCIA_CODIGO, 
    UPPER(prov.name) AS PROVINCIA_DESCRIPCION, 
    COALESCE(cnae.name, '35') AS CNAE_CODIGO,
    COALESCE(cnae.descripcio, 'NO ESPECIFICADOS') AS CNAE_DESCRIPCION,
    COUNT(p.id) AS NUM_CLIENTES,
    ROUND(COALESCE(MAX(factures.energia), 0.0) * 0.001, 2) AS ENERGIA_COMERCIALIZADORA,
    ROUND(COALESCE(MAX(factures.valor), 0,0), 2) AS VALOR--,
--    STRING_AGG(p.name,',')
FROM giscegas_polissa AS p
LEFT JOIN (
  SELECT
    id AS id,
    CASE WHEN cnaetrad.name  similar to '(01|02|03)%%' THEN '1'
    WHEN cnaetrad.name  similar to '(05|099)%%' THEN '2'
    WHEN cnaetrad.name  similar to '(06|091)%%' THEN '3'
    WHEN cnaetrad.name  similar to '(721|2446|353)%%' THEN '4'
    WHEN cnaetrad.name  similar to '(191)%%' THEN '5'
    WHEN cnaetrad.name  similar to '(192)%%' THEN '6'
    WHEN cnaetrad.name  similar to '(351)%%' THEN '7'
    WHEN cnaetrad.name  similar to '(352)%%' THEN '8'
    WHEN cnaetrad.name  similar to '(07|08)%%' and not cnaetrad.name similar to '(0721)%%' THEN '9'   
    WHEN cnaetrad.name  similar to '(24)%%' and not cnaetrad.name similar to '(244|2453|2454)%%' THEN '10'
    WHEN cnaetrad.name  similar to '(244|2453|2454)%%' THEN '11'
    WHEN cnaetrad.name  similar to '(231)%%' THEN '12'
    WHEN cnaetrad.name  similar to '(235)%%' THEN '13'
    WHEN cnaetrad.name  similar to '(23)%%' and not cnaetrad.name similar to '(231|235)%%' THEN '14'
    WHEN cnaetrad.name  similar to '(20|21)%%' THEN '15'
    WHEN cnaetrad.name  similar to '(25|26|27|28)%%' THEN '16'
    WHEN cnaetrad.name  similar to '(29|309)%%' THEN '17'
    WHEN cnaetrad.name  similar to '(301|3315)%%' THEN '18'
    WHEN cnaetrad.name  similar to '(302|303|304)%%' THEN '19'
    WHEN cnaetrad.name  similar to '(10|11|12)%%' THEN '20'
    WHEN cnaetrad.name  similar to '(13|14|15)%%' THEN '21'
    WHEN cnaetrad.name  similar to '(16)%%' THEN '22'
    WHEN cnaetrad.name  similar to '(17)%%' THEN '23'
    WHEN cnaetrad.name  similar to '(18|581)%%' THEN '24'
    WHEN cnaetrad.name  similar to '(22|31|32|33)%%' and not cnaetrad.name similar to '(3315)%%' THEN '25'
    WHEN cnaetrad.name  similar to '(41|42|43)%%' THEN '26'
    WHEN cnaetrad.name  similar to '(491|492)%%' THEN '27'
    WHEN cnaetrad.name  similar to '(4939|494)%%' THEN '28'
    WHEN cnaetrad.name  similar to '(4931|4932|495|50|51|52)%%' THEN '29'
    WHEN cnaetrad.name  similar to '(55|56)%%' THEN '30'
    WHEN cnaetrad.name  similar to '(36|37|38|39|53|60|61|72|84|85|86|87|88|91|99)%%' and not cnaetrad.name similar to '(855|856)%%' THEN '31'
    WHEN cnaetrad.name  similar to '(473)%%' THEN '32'
    WHEN cnaetrad.name  similar to '(45|46|47|582|59|62|63|64|65|66|67|68|69|70|71|75|76|77|82|83|84|855|856|90|92|93|94|95|96)%%' THEN '33'
    WHEN cnaetrad.name  similar to '(97|98)%%' THEN '34'
    ELSE '35'
    END AS name,
--
    CASE WHEN cnaetrad.name  similar to '(01|02|03)%%' THEN 'AGRICULTURA, GANADERIA, SILVICULTURA, CAZA Y PESCA'
    WHEN cnaetrad.name  similar to '(05|099)%%' THEN 'EXTRACCION Y AGLOMERACION DE CARBONES'
    WHEN cnaetrad.name  similar to '(06|091)%%' THEN 'EXTRACCION DE PETROLEO Y GAS'
    WHEN cnaetrad.name  similar to '(721|2446|353)%%' THEN 'COMBUSTIBLES NUCLEARES Y OTRAS ENERGIAS'
    WHEN cnaetrad.name  similar to '(191)%%' THEN 'COQUERIAS'
    WHEN cnaetrad.name  similar to '(192)%%' THEN 'REFINERIAS DE PETROLEO'
    WHEN cnaetrad.name  similar to '(351)%%' THEN 'PRODUCCION Y DISTRIBUCION DE ENERGIA ELECTRICA'
    WHEN cnaetrad.name  similar to '(352)%%' THEN 'FABRICAS DE GAS-DISTRIBUCION DE GAS'
    WHEN cnaetrad.name  similar to '(07|08)%%' and not cnaetrad.name similar to '(0721)%%' THEN 'MINAS Y CANTERAS (NO ENERGETICAS)'
    WHEN cnaetrad.name  similar to '(24)%%' and not cnaetrad.name similar to '(244|2453|2454)%%' THEN 'SIDERURGIA Y FUNDICION'
    WHEN cnaetrad.name  similar to '(244|2453|2454)%%' THEN 'METALURGIA NO FERREA'
    WHEN cnaetrad.name  similar to '(231)%%' THEN 'INDUSTRIA DEL VIDRIO'
    WHEN cnaetrad.name  similar to '(235)%%' THEN 'CEMENTO, CALES Y YESOS'
    WHEN cnaetrad.name  similar to '(23)%%' and not cnaetrad.name similar to '(231|235)%%' THEN 'OTROS MATERIALES DE CONSTRUCCION (LOZA, PORCELANA, REFRACTARIOS, ETC.)'
    WHEN cnaetrad.name  similar to '(20|21)%%' THEN 'QUIMICA Y PETROQUIMICA'
    WHEN cnaetrad.name  similar to '(25|26|27|28)%%' THEN 'MAQUINAS Y TRANSFORMADOS METALICOS'
    WHEN cnaetrad.name  similar to '(29|309)%%' THEN 'CONSTRUCCION DE VEHÍCULOS A MOTOR. MOTOCICLETAS Y BICICLETAS'
    WHEN cnaetrad.name  similar to '(301|3315)%%' THEN 'CONSTRUCCION Y REPARACION NAVAL'
    WHEN cnaetrad.name  similar to '(302|303|304)%%' THEN 'CONSTRUCCION DE OTROS MEDIOS DE TRANSPORTE'
    WHEN cnaetrad.name  similar to '(10|11|12)%%' THEN 'ALIMENTACION, BEBIDAS Y TABACO'
    WHEN cnaetrad.name  similar to '(13|14|15)%%' THEN 'IND. TEXTIL, CONFECCION, CUERO Y CALZADO'
    WHEN cnaetrad.name  similar to '(16)%%' THEN 'IND. DE MADERA Y CORCHO (EXC. FABRICACION DE MUEBLES)'
    WHEN cnaetrad.name  similar to '(17)%%' THEN 'PASTAS PAPELERAS, PAPEL, CARTON, MANIPULADOS'
    WHEN cnaetrad.name  similar to '(18|581)%%' THEN 'ARTES GRAFICAS Y EDICION'
    WHEN cnaetrad.name  similar to '(22|31|32|33)%%' and not cnaetrad.name similar to '(3315)%%' THEN 'IND. CAUCHO, MAT. PLASTICAS Y OTRAS NO ESPECIFICADAS'
    WHEN cnaetrad.name  similar to '(41|42|43)%%' THEN 'CONSTRUCCION Y OBRAS PUBLICAS'
    WHEN cnaetrad.name  similar to '(491|492)%%' THEN 'TRANSPORTE INTERURBANO POR FF. CC.'
    WHEN cnaetrad.name  similar to '(4939|494)%%' THEN 'TRANSPORTE INTERURBANO POR CARRETERA (VIAJEROS, MERCANCIAS)'
    WHEN cnaetrad.name  similar to '(4931|4932|495|50|51|52)%%' THEN 'OTRAS EMPRESAS DE TRANSPORTE'
    WHEN cnaetrad.name  similar to '(55|56)%%' THEN 'HOSTELERIA'
    WHEN cnaetrad.name  similar to '(36|37|38|39|53|60|61|72|84|85|86|87|88|91|99)%%' and not cnaetrad.name similar to '(855|856)%%'  THEN 'ADMINISTRACION Y OTROS SERVICIOS PUBLICOS'
    WHEN cnaetrad.name  similar to '(473)%%' THEN 'COMERCIO AL POR MENOR DE COMBUSTIBLE PARA LA AUTOMOCIÓN EN ESTABLECIMIENTOS ESPECIALIZADOS'
    WHEN cnaetrad.name  similar to '(45|46|47|582|59|62|63|64|65|66|67|68|69|70|71|75|76|77|82|83|84|855|856|90|92|93|94|95|96)%%' THEN 'COMERCIO Y SERVICIOS'
    WHEN cnaetrad.name  similar to '(97|98)%%' THEN 'USOS DOMESTICOS'
    ELSE 'NO ESPECIFICADOS'
    END AS descripcio
  FROM giscemisc_cnae AS cnaetrad
) as cnae ON cnae.id=p.cnae
LEFT JOIN giscegas_cups_ps cups ON cups.id=p.cups
LEFT JOIN res_municipi muni ON muni.id=cups.id_municipi
LEFT JOIN res_country_state prov ON prov.id=muni.state
LEFT JOIN (
    SELECT p.cnae AS cnae, prov.id AS provincia,
    SUM(COALESCE(f.consumo_kwh*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END),0.0)) AS energia,
    SUM(COALESCE(f.total_variable*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END),0.0)) AS valor
    FROM giscegas_facturacio_factura AS f
    LEFT JOIN account_invoice i on f.invoice_id=i.id
    LEFT JOIN giscegas_facturacio_factura_linia fl ON (fl.factura_id=f.id and fl.tipus='energia')
    LEFT JOIN account_invoice_line il on (fl.invoice_line_id=il.id)
    LEFT JOIN giscegas_polissa p on p.id=f.polissa_id
    LEFT JOIN giscegas_cups_ps cups ON cups.id=p.cups
    LEFT JOIN res_municipi muni ON muni.id=cups.id_municipi
    LEFT JOIN res_country_state prov ON prov.id=muni.state
    WHERE i.type in ('out_refund','out_invoice') AND i.date_invoice >= %(start_date)s AND i.date_invoice < %(end_date)s
    GROUP BY p.cnae, prov.id
) factures ON factures.cnae=cnae.id AND factures.provincia=muni.state
-- Activos a fecha final
--WHERE ((p.data_baixa IS NOT NULL AND p.data_baixa > %(limit_active_date)s AND data_alta<=%(limit_active_date)s)
--      OR (p.data_alta <= %(limit_active_date)s AND (p.data_baixa >= %(limit_active_date)s OR data_baixa IS NULL))) AND p.state in ('activa', 'baixa')
-- Activos durante periodo
WHERE ((p.data_baixa >= %(start_date)s AND p.data_baixa < %(end_date)s)
      OR (p.data_alta < %(end_date)s AND (p.data_baixa >= %(start_date)s OR data_baixa IS NULL)))
AND p.state not in ('esborrany','cancelada')
GROUP BY prov.code, prov.name, cnae.name, cnae.descripcio
ORDER BY prov.code, prov.name, cnae.name, cnae.descripcio;
