# -*- coding: utf-8 -*-
{
    "name": "Switching Multi Change (Gas)",
    "description": """Cambiar múltiples casos de ATR de gas""",
    "version": "0-dev",
    "author": "GISCEGas",
    "category": "CRM",
    "depends": [
        "crm_multi_close",
        "giscegas_atr"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/wizard_atr_multi_change_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
