# -*- coding: utf-8 -*-

from __future__ import absolute_import
import sys

from osv import osv, fields
from tools.translate import _
from ast import literal_eval
import netsvc
from datetime import datetime, timedelta
# from giscegas_atr.giscegas_atr_validacions_lectures import ValidadorLecturesATR


class GiscegasAtr(osv.osv):
    _name = 'giscegas.atr'
    _inherit = "giscegas.atr"

    def escull_tarifa_comer(self, cursor, uid, ids, tarifa_atr,
                            context=None):

        if context is None:
            context = {}

        sw_id = ids
        if isinstance(ids, (tuple, list)):
            sw_id = ids[0]

        tarifa_obj = self.pool.get('giscegas.polissa.tarifa')
        polissa_obj = self.pool.get('giscegas.polissa')

        sw_fields = ['tarifa_comer_id', 'name', 'cups_polissa_id',
                     'actualitzar_tarifa_comer']
        sw_vals = self.read(cursor, uid, sw_id, sw_fields, context)

        polissa_id = sw_vals['cups_polissa_id'][0]
        polissa_info = polissa_obj.read(
            cursor, uid, polissa_id, ['tarifa', 'llista_preu'], context=context
        )

        tarifa = tarifa_obj.browse(cursor, uid, tarifa_atr)

        if (not sw_vals.get('actualitzar_tarifa_comer')
            and polissa_info.get('llista_preu')
            and tarifa.id == polissa_info.get('tarifa')[0]):
            return polissa_info['llista_preu'][0]

        llistes_preus = [l for l in tarifa.llistes_preus_comptatibles
                         if l.type == 'sale']
        if not llistes_preus:
            #ERROR, No hem trobat llista de preus
            info = _(u"No hem trobat llista de preus per la tarifa ATR"
                     u" '%s' del cas '%s'") % (tarifa.name, sw_vals['name'])
            raise osv.except_osv('Error', info)

        # Si arribem a aquest punt significa que hem d'escollir la tarifa o
        # perque ho hem seleccionat o perque la tarifa ATR del contracte es
        # diferent a la del cas ATR
        if sw_vals.get('tarifa_comer_id'):
            tarifa_comer_id = sw_vals['tarifa_comer_id'][0]
            if tarifa_comer_id not in [lp.id for lp in llistes_preus]:
                #ERROR, La llista de preus no és compatible
                tarifa_comer_name = sw_vals['tarifa_comer_id'][1]
                info_tmpl = _(u"La llista de preus '{0}' no és compatible "
                              u"amb la tarifa ATR '{1}' del cas '{2}'")
                info = info_tmpl.format(
                    tarifa_comer_name, tarifa.name, sw_vals['name']
                )
                raise osv.except_osv('Error', info)

            return tarifa_comer_id

        # funció per escollir la tarifa única (si es pot)
        llista_preus = polissa_obj.escull_llista_preus(
            cursor, uid, polissa_id, llistes_preus, context
        )

        if not llista_preus:
            #ERROR, No hem trobat una tarifa única
            tarifes_comer = [t.name for t in llistes_preus]
            txt = _(u"No hem pogut escollir una tarifa automàticament"
                    u" per la tarifa ATR '%s' del cas '%s' perquè hem"
                    u" trobat %d tarifes: %s")
            info = txt % (tarifa.name, sw_vals['name'], len(llistes_preus),
                          tarifes_comer)
            raise osv.except_osv('Error', info)

        return llista_preus.id

    _columns = {
        'tarifa_comer_id': fields.many2one('product.pricelist',
                                           'Tarifa Comercialitzadora',
                                           help=u"Tarifa a aplicar si hi ha "
                                                u"canvi de tarifa d'accés"),
        'actualitzar_tarifa_comer': fields.boolean(
            "Actualitzar llista de preus",
            help=u"Si està marcat al activar el cas ATR s'actualitzarà la "
                 u"tarifa de comercializadora del contracte amb la tarifa "
                 u"associada al cas ATR"
        )
    }

    _defaults = {
        'tarifa_comer_id': lambda *a: False,
        'actualitzar_tarifa_comer': lambda *a: False,
    }

GiscegasAtr()
