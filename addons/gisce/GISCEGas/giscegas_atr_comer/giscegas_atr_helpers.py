# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, orm
from tools.translate import _
from datetime import datetime
from dateutil.relativedelta import relativedelta
import calendar
from giscegas_atr.giscegas_atr_helpers import GiscegasAtrException, \
    PERIODICIDAD_FACTURACION_TO_PERIODICIDAD_LECTURA


class GiscegasAtrHelpers(osv.osv):

    _name = 'giscegas.atr.helpers'
    _inherit = 'giscegas.atr.helpers'

    # Funció per activar una pólissa a partir d'un cas de atr
    # a302, a341
    def activa_polissa_from_a3n(self, cursor, uid, sw_id, context=None):
        """Activa les modificacions d'un cas A1_02/41 (a3)"""
        try:
            pool = self.pool

            sw_obj = pool.get('giscegas.atr')
            polissa_obj = pool.get('giscegas.polissa')
            tarifa_obj = pool.get('giscegas.polissa.tarifa')
            tarVer_obj = pool.get('product.pricelist.version')
            mandate_obj = pool.get('payment.mandate')
            conf_obj = pool.get('res.config')
            info = ''

            sw = sw_obj.browse(cursor, uid, sw_id)

            # Validem que el pas és final
            if not sw.finalitzat:
                # ERROR, no estem en un pas final
                info = _(
                    u'Este caso (%s) no esta finalizado (%s-%s)'
                ) % (sw.name, sw.proces_id.name, sw.step_id.name)
                raise GiscegasAtrException(info, _(u'ERROR'))
            # Validem que el pas no és un rebuig (a2)
            if sw.rebuig:
                # ERROR, no estem en un pas final
                info = _(
                    u'Este caso (%s) esta finalizado pero es un rechazo (%s-%s)'
                ) % (sw.name, sw.proces_id.name, sw.step_id.name)
                raise GiscegasAtrException(info, _(u'ERROR'))

            pas02_obj = pool.get("giscegas.atr.{0}.a2".format(sw.proces_id.name.lower()))
            pas02 = pas02_obj.browse(
                cursor, uid, pas02_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
            )
            pas = sw.get_pas()

            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)
            if not polissa.state == 'esborrany':
                # ERROR, la Pólissa ha d'estar activa
                info = _(
                    u"Este contrato (%s) del caso '%s' tiene que "
                    u"estar en borrador"
                ) % (sw.cups_polissa_id.name, sw.name)
                raise GiscegasAtrException(info, _(u'ERROR'))

            # data activació
            data_activacio = pas.transfereffectivedate

            # Configuració per saber si la periodicitat de facturació
            # ha de ser la mateixa que la de distri
            conf_id = 'atr_gas_activacio_facturacio_comer_segons_pas'
            per_fact_de_pas = bool(int(conf_obj.get(cursor, uid, conf_id, '0')))

            # El lot de facturació dependrà de la data d'activació i
            # de la periodicitat de facturació
            # Agafarem el primer lot NO tancat a partir de la data
            # calculada
            fact_distri = PERIODICIDAD_FACTURACION_TO_PERIODICIDAD_LECTURA.get(pas.lectureperiodicity)
            per_fact = pas.lectureperiodicity
            if per_fact != '02':
                per_fact = '01'
            per_fact = int(per_fact)
            # El lot de facturació el calculem segons el període de facturació
            # que té (o tindrà) la pólissa a comercialitzadora.
            # Si aquest període és el mateix que el del pas, agafem el del pas
            per_fact_lot = (per_fact_de_pas and per_fact
                            or polissa.facturacio
                            or per_fact)
            data_lot = (
                    datetime.strptime(data_activacio, '%Y-%m-%d') +
                    relativedelta(months=+per_fact_lot)
            ).strftime('%Y-%m-%d')

            lot = self.get_lot_facturacio(cursor, uid, data_lot, data_activacio)

            if not lot:
                # ERROR, No hem trobat el lot amb la data corresponent
                info = _(u"No se ha encontrado ningun LOTE de facturación "
                         u"abierto "
                         u"para el contrato (%s) del caso '%s' desde "
                         u"la fecha '%s'") % (sw.cups_polissa_id.name,
                                                sw.name, data_lot)
                raise GiscegasAtrException(info, _(u'ERROR'))

            lot_id = lot[0]

            vals = {}
            canvi_tarifa = canvi_caudal = canvi_presio = False
            # actualitzem tarifa (només si canvia)
            tarifa_pas_id = tarifa_obj.get_tarifa_from_ocsum(cursor, uid, pas02.tolltype)

            if tarifa_pas_id != polissa.tarifa.id:
                tarifa_pas_nom = tarifa_obj.read(cursor, uid, tarifa_pas_id, ['name'])['name']
                canvi_tarifa_txt = "(%s -> %s)" % (polissa.tarifa.name, tarifa_pas_nom)
                vals.update({'tarifa': tarifa_pas_id})
                canvi_tarifa = True

            npres = pas02.outgoingpressuregranted or pas02.reqoutgoingpressure
            if polissa.pressio != npres:
                if tarifa_pas_id:
                    limits_tarifa = tarifa_obj.read(cursor, uid, tarifa_pas_id, ['presion_min', 'presion_max'])
                    if npres < limits_tarifa['presion_min']:
                        npres = limits_tarifa['presion_min']
                    elif npres > limits_tarifa['presion_max']:
                        npres = limits_tarifa['presion_max']
                canvi_pressio_txt = "(%s -> %s)" % (polissa.pressio, npres)
                vals.update({'pressio': npres})
                canvi_presio = True

            ndays = 366.0 if calendar.isleap(datetime.now().year) else 365.0
            qdcalc = pas.finalclientyearlyconsumption / ndays
            qdcalc = round(qdcalc, 7)
            qd = pas.finalqdgranted or pas02.qdgranted
            if qd != polissa.caudal_diario or qdcalc != polissa.caudal_diario:
                canvi_qd_txt = "(%s -> %s)" % (polissa.caudal_diario, qd or qdcalc)
                if qd != qdcalc:
                    canvi_qd_txt += _(
                        u"\nCaudal diario calculado a parir del anual y caudal "
                        u"diario informado no coinciden: {0} != {1}"
                    ).format(qdcalc, qd)
                vals.update({'caudal_diario': qdcalc})
                canvi_caudal = True

            if canvi_caudal or canvi_tarifa or canvi_presio:
                polissa.write(vals)
                polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)

            tarifa_id = tarifa_pas_id

            # Si la pólissa no té tarifa, l'escollim
            # Calculem la tarifa. S'ha de fer particular de cada empresa
            # De moment, com si només n'hi hagués una per tarifa ATR
            if polissa.llista_preu and not canvi_tarifa:
                tarifa_comer = polissa.llista_preu
            else:
                tarifa = tarifa_obj.browse(cursor, uid, tarifa_id)
                llistes_preus = [l for l in tarifa.llistes_preus_comptatibles
                                 if l.type == 'sale']
                if not llistes_preus:
                    #ERROR, No hem trobat llista de preus
                    info = _(u"No hemos encontrado lista de precios para la "
                             u"tarifa ATR"
                             u" '%s' del cas '%s'") % (tarifa.name, sw.name)
                    raise GiscegasAtrException(info, _(u'ERROR'))
                elif polissa.llista_preu in llistes_preus:
                    # Si la tarifa_comer que tenim al contracte es compatible,
                    # la utilitzem
                    llista_preus = polissa.llista_preu
                else:
                    # funció per escollir la tarifa única (si es pot)
                    llista_preus = polissa.escull_llista_preus(llistes_preus)

                if not llista_preus:
                    # ERROR, No hem trobat una tarifa única
                    tarifes_comer = [t.name for t in llistes_preus]
                    txt = _(u"No hemos podido escojer una tarifa "
                            u"automaticamente"
                            u" para la tarifa ATR '%s' del caso '%s' perque "
                            u"hemos encontrado %d tarifes: %s")
                    info = txt % (tarifa.name, sw.name, len(llistes_preus),
                                  tarifes_comer)
                    raise GiscegasAtrException(info, _(u'ERROR'))

                tarifa_comer = llista_preus

            # versió primera facturació segons data activació
            search_vals = [
                ('pricelist_id', '=', tarifa_comer.id),
                ('date_start', '<=', data_activacio),
                '|',
                ('date_end', '>=', data_activacio),
                ('date_end', '=', False)
            ]

            ver_prim_fact = tarVer_obj.search(cursor, uid,
                                              search_vals)
            if not ver_prim_fact:
                info = _(u"para el contrato '%s' no hemos"
                         u"encontrado ninguna versión de precios para "
                         u"la tarifa '%s' para la fecha '%s' "
                         u"del caso '%s'") % (polissa.name,
                                             tarifa_comer.name,
                                             data_activacio,
                                             sw.name)
                raise GiscegasAtrException(info, _(u'ERROR'))

            vals = {
                'data_alta': data_activacio,
                'lot_facturacio': lot_id,
                'llista_preu': tarifa_comer.id,
                'versio_primera_factura': ver_prim_fact[0],
                'ref_dist': pas.atrcode or '',
                'facturacio_distri': fact_distri,
                'netsituation': pas02.netsituation,
                'fecha_inspeccion': pas.lastinspectionsdate,
                'resultado_inspeccion': pas.lastinspectionsresult
            }

            if per_fact_de_pas:
                vals.update({'facturacio': per_fact})

            data_firma = polissa.data_firma_contracte
            # data firma la d'activació si no n'hi ha
            if not data_firma:
                data_firma = datetime.strftime(
                    datetime.strptime(data_activacio, '%Y-%m-%d'),
                    '%Y-%m-%d %H:%M:%S'
                )
                vals.update({'data_firma_contracte': data_firma})

            try:
                polissa.write(vals)
                # tornem a llegir la pólissa
                polissa.send_signal(['validar', 'contracte'])
            except (osv.except_osv, orm.except_orm) as e:
                info = _(u"La pólissa '%s' del cas '%s' no s'ha pogut "
                         u"activar: %s'") % (polissa.name, sw.name, e.value)
                raise GiscegasAtrException(info, _(u'ERROR'))
            except Exception as e:
                info = _(u"La pólissa '%s' del cas '%s' no s'ha pogut "
                         u"activar: %s'") % (polissa.name, sw.name, e.message)
                raise GiscegasAtrException(info, _(u'ERROR'))

            # Aprofitem per crear el "mandato"
            if polissa.payment_mode_id and polissa.payment_mode_id.require_bank_account:
                reference = 'giscegas.polissa,%s' % polissa.id
                search_mandates = [('reference', '=', reference),
                                   ('date_end', '=', False)]
                mandates_ids = mandate_obj.search(cursor, uid, search_mandates)
                if not mandates_ids:
                    mandate_obj.create(
                        cursor, uid,
                        {'reference': reference,
                         'date': polissa.data_firma_contracte or data_firma}
                    )

            # Carreguem els comptadors des del pas A302
            pm_txt = self.processa_comptadors(cursor, uid, pas)

            res = sw.notifica_a_client(context=context)

            if len(res) != 2 and res[0] != 'OK':
                return res

            # tanquem el cas
            if not sw.case_close():
                info = _(u"Per la pólissa '%s' no hem pogut tancar "
                         u"el cas %s") % (polissa.name, sw.name)
                raise GiscegasAtrException(info, _(u'ERROR'))

            info = _(u"Les modificacions del cas  %s "
                     u"s'han realitzat correctament.") % (sw.name)
            if canvi_tarifa:
                info += _(u" INFO: La tarifa ha canviat %s.") % canvi_tarifa_txt
            if canvi_caudal:
                info += _(u" INFO: El caudal diario ha cambiado %s.") % canvi_qd_txt
            if canvi_presio:
                info += _(u" INFO: La presion ha cambiado %s.") % canvi_pressio_txt
            if pm_txt:
                info += pm_txt

            return 'OK', info

        except osv.except_osv as e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscegasAtrException(info, _(u'ERROR'))
        except orm.except_orm as e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscegasAtrException(info, _(u'ERROR'))

        except Exception as e:
            raise GiscegasAtrException(e.message, _(u'ERROR'))

    def activa_modificacions_polissa_from_atr(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}

        pool = self.pool
        sw_obj = pool.get('giscegas.atr')
        polissa_obj = pool.get('giscegas.polissa')
        sw = sw_obj.browse(cursor, uid, sw_id)
        try:
            pas = sw.get_pas()
            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)

            # Validem que el pas és final
            if not sw.finalitzat:
                # ERROR, no estem en un pas final
                info = _(u'Este caso (%s) no está finalizado '
                         u'(%s-%s)') % (sw.name, sw.proces_id.name, sw.step_id.name)
                raise GiscegasAtrException(info, _(u'ERROR'))
            if sw.rebuig:
                # ERROR, no estem en un pas final
                info = _(u'Este caso (%s) está finalizado pero es un '
                         u'rechazo (%s-%s)') % (sw.name, sw.proces_id.name, sw.step_id.name)
                raise GiscegasAtrException(info, _(u'ERROR'))
            if not polissa.state == 'activa':
                # ERROR, la Pólissa ha d'estar activa
                info = _(u"Aquesta pólissa (%s) del cas '%s' ha "
                         u"d'estar activa") % (sw.cups_polissa_id.name, sw.name)
                return (_(u'ERROR'), info)

            vals = self.get_vals_canvis_tecnics(cursor, uid, pas, polissa, context=context)
            data_activacio = pas.transfereffectivedate
            polissa.send_signal('modcontractual')
            polissa.write(vals)

            # activem la modificacio contractual
            wz_crear_mc_obj = pool.get('giscegas.polissa.crear.contracte')
            ctx = {
                'active_id': polissa.id,
                'activacio_from_atr': context.get('activacio_from_atr', False)
            }
            params = {
                'duracio': context.get('periode_modcon', 'actual'),
                'accio': 'nou'
            }
            wz_id = wz_crear_mc_obj.create(cursor, uid, params, ctx)
            wiz = wz_crear_mc_obj.browse(cursor, uid, wz_id, ctx)
            res = wz_crear_mc_obj.onchange_duracio(
                cursor, uid, [wz_id], data_activacio, wiz.duracio, ctx
            )
            if res.get('warning', False) and polissa.modcontractual_activa.data_inici != data_activacio:
                info = _(
                    u"Erro en las fechas al crear la "
                    u"modificación contractual del contrato '{polissa_name}' "
                    u"para el caso '{cas_sw}'. Fecha activación '{data_activacio}'. "
                    u"{warning_title}: {warning_message}. "
                    u"ESTE CAMBIO YA ESTA ACTIVADO?"
                ).format(**{
                    'polissa_name': polissa.name,
                    'cas_sw': sw.name,
                    'data_activacio': data_activacio,
                    'warning_title': res['warning']['title'],
                    'warning_message': res['warning']['message']
                })
                polissa.send_signal('undo_modcontractual')
                return (_(u'ERROR'), info)

            wiz.write({'data_final': res['value']['data_final'] or '', 'data_inici': data_activacio})
            wiz.action_crear_contracte(ctx)

            # pwe_template = 'switching_notificacio_activacio_m1_email'
            pm_txt = self.processa_comptadors(cursor, uid, pas)
            res = sw.notifica_a_client(context=context)

            if len(res) != 2 and res[0] != 'OK':
                return res

            # tanquem el cas
            if not sw.case_close():
                info = _(u"No se ha podido cerrar el caso '%s' para el contrato '%s'") % (sw.name, polissa.name)
                return (_(u'ERROR'), info)

            info = _(u"Las modificaciones del caso  %s "
                     u"se han realizado correctamente.") % (sw.name)
            return ('OK', info)
        except osv.except_osv, e:
            info = u'%s (%s-%s) %s' % (
                sw.name, sw.proces_id.name, sw.step_id.name, unicode(e.value)
            )
            raise GiscegasAtrException(info, _(u'ERROR'))

        except orm.except_orm, e:
            info = u'%s (%s-%s) %s' % (
                sw.name, sw.proces_id.name, sw.step_id.name, unicode(e.value)
            )
            raise GiscegasAtrException(info, _(u'ERROR'))

        except Exception, e:
            raise GiscegasAtrException(e.message, _(u'ERROR'))

    def get_vals_canvis_tecnics(self, cursor, uid, pas, polissa, context=None):
        """
        :param cursor:
        :param uid:
        :param pas: browse del pas 05 del m1
        :param context:
        :return: retorna un diccionari  amb valors per fer el write al contracte
        """
        pool = self.pool
        tarifa_obj = pool.get('giscegas.polissa.tarifa')
        canvi_tarifa = canvi_caudal = canvi_presio = False
        vals = {}

        if pas.communicationreason == '06' or pas.updatereason in ['11', '12', '15', '16', '18', '19', '22', '23']:
            canvi_tarifa = True

        if canvi_tarifa:
            tarifa_pas_id = tarifa_obj.get_tarifa_from_ocsum(cursor, uid, pas.tolltype)
            vals.update({'tarifa': tarifa_pas_id})
            # Si la pólissa no té tarifa, l'escollim
            # Calculem la tarifa. S'ha de fer particular de cada empresa
            # De moment, com si només n'hi hagués una per tarifa ATR
            if polissa.llista_preu and not canvi_tarifa:
                tarifa_comer = polissa.llista_preu
            else:
                tarifa = tarifa_obj.browse(cursor, uid, tarifa_pas_id)
                llistes_preus = [l for l in tarifa.llistes_preus_comptatibles if l.type == 'sale']
                if not llistes_preus:
                    #ERROR, No hem trobat llista de preus
                    info = _(u"No hemos encontrado lista de precios para la "
                             u"tarifa ATR '%s' del cas '%s'") % (tarifa.name, pas.sw_id.name)
                    raise GiscegasAtrException(info, _(u'ERROR'))
                elif polissa.llista_preu in llistes_preus:
                    # Si la tarifa_comer que tenim al contracte es compatible,
                    # la utilitzem
                    llista_preus = polissa.llista_preu
                else:
                    # funció per escollir la tarifa única (si es pot)
                    llista_preus = polissa.escull_llista_preus(llistes_preus)

                if not llista_preus:
                    # ERROR, No hem trobat una tarifa única
                    tarifes_comer = [t.name for t in llistes_preus]
                    txt = _(u"No hemos podido escojer una tarifa "
                            u"automaticamente"
                            u" para la tarifa ATR '%s' del caso '%s' perque "
                            u"hemos encontrado %d tarifes: %s")
                    info = txt % (tarifa.name, pas.sw_id.name, len(llistes_preus),
                                  tarifes_comer)
                    raise GiscegasAtrException(info, _(u'ERROR'))
                tarifa_comer = llista_preus
            vals.update({'llista_preu': tarifa_comer.id})

        if pas.lastinspectionsdate and pas.lastinspectionsresult:
            vals.update({
                'fecha_inspeccion': pas.lastinspectionsdate,
                'resultado_inspeccion': pas.lastinspectionsresult
            })

        if pas.lectureperiodicity:
            fact_distri = PERIODICIDAD_FACTURACION_TO_PERIODICIDAD_LECTURA.get(pas.lectureperiodicity)
            vals.update({
                'facturacio_distri': fact_distri,
            })

        if pas.atrcode:
            vals.update({
                'ref_dist': pas.atrcode or '',
            })

        if pas.netsituation:
            vals.update({
                'netsituation': pas.netsituation,
            })

        return vals

GiscegasAtrHelpers()
