# -*- coding: utf-8 -*-
{
    "name": "ATR Gas Comer",
    "description": """ATR Gas per Comercialitzadores
    Processos útils i exclusius per la gestió de atr_gas (gestió ATR)
    per a empreses comercialitzadores""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends":[
        "giscegas_atr",
        "giscegas_polissa_comer",
        "giscegas_lectures_comer",
        "giscegas_lectures_pool",
        "giscegas_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegas_atr_data.xml",
        "giscegas_atr_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
