# -*- coding: utf-8 -*-
from __future__ import absolute_import

from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from giscegas_atr.tests.common_tests import TestSwitchingImport

import json
from base64 import b64decode
import zipfile
import io


class TestSwitchingWizards(TestSwitchingImport):

    def test_create_wizard_41_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            atr_obj = self.openerp.pool.get('giscegas.atr')
            wiz_obj = self.openerp.pool.get(
                'giscegas.atr.a141.wizard'
            )
            contract_obj = self.openerp.pool.get('giscegas.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'contract_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.updatereason, '03')
            self.assertEqual(wiz.modeffectdate, '05')
            self.assertEqual(wiz.disconnectedserviceaccepted, 'N')
            self.assertEqual(wiz.titular_id.id, contract.titular.id)
            self.assertEqual(wiz.newreqqd, contract.caudal_diario)
            self.assertEqual(wiz.surrogacy, 'N')
            self.assertEqual(wiz.new_titular_id.id, contract.titular.id)
            self.assertEqual(wiz.new_titular_address_id.id, contract.direccio_notificacio.id)

            wiz.write({'updatereason': '01'})
            contract.titular.write({
                'name': "C1 C2, N1", 'vat': "ES11111111H"
            })
            contract.direccio_notificacio.write({
                # Dummy ids, we just want to fill them
                'state_id': self.openerp.pool.get("res.country.state").search(cursor, uid, [], limit=1)[0],
                'id_municipi': self.openerp.pool.get("res.municipi").search(cursor, uid, [], limit=1)[0]
            })
            wiz.genera_casos_atr({'contract_id': contract_id})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            casos_ids = json.loads(wiz.casos_generats)
            atr = atr_obj.browse(cursor, uid, casos_ids[0])
            a141 = atr.get_pas()

            self.assertEqual(a141.updatereason, '01')
            self.assertEqual(a141.newnationality, 'CH')
            self.assertEqual(a141.disconnectedserviceaccepted, 'N')
            self.assertEqual(a141.newfamilyname1, "C1")

    def test_create_switching_wizard_comer_instance_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'giscegas.atr.wizard'
            )

            contract_id = self.get_contract_id(txn)

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={
                    'active_id': contract_id,
                    'active_ids': [contract_id]
                }
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertIsNotNone(wiz)

    def test_create_switching_wizard_distri_instance_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'giscegas.atr.wizard'
            )

            contract_id = self.get_contract_id(txn)

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={
                    'active_id': contract_id,
                    'active_ids': [contract_id]
                }
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertIsNotNone(wiz)

    def test_create_wizard_02_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            atr_obj = self.openerp.pool.get('giscegas.atr')
            wiz_obj = self.openerp.pool.get(
                'giscegas.atr.a102.wizard'
            )
            contract_obj = self.openerp.pool.get('giscegas.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'contract_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.modeffectdate, '05')
            self.assertEqual(wiz.disconnectedserviceaccepted, 'N')

            wiz.write({
                'disconnectedserviceaccepted': 'S',
                'extrainfo': "Comentari",
                'modeffectdate': "02"
            })
            wiz.genera_casos_atr({'contract_id': contract_id})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            casos_ids = json.loads(wiz.casos_generados)
            atr = atr_obj.browse(cursor, uid, casos_ids[0])
            a102 = atr.get_pas()

            self.assertEqual(a102.documenttype, '01')
            self.assertEqual(a102.documentnum, u'B82420654')
            self.assertEqual(a102.reqqd, 1.5)
            self.assertEqual(a102.reqestimatedqa, int(a102.reqqd*365))
            self.assertEqual(a102.modeffectdate, "02")
            self.assertEqual(a102.disconnectedserviceaccepted, "S")
            self.assertEqual(a102.extrainfo, "Comentari")

    def test_create_wizard_02_falta_reqestimatedqa(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            atr_obj = self.openerp.pool.get('giscegas.atr')
            wiz_obj = self.openerp.pool.get(
                'giscegas.atr.a102.wizard'
            )
            contract_obj = self.openerp.pool.get('giscegas.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.write({"caudal_diario":  0})
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'contract_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.modeffectdate, '05')
            self.assertEqual(wiz.disconnectedserviceaccepted, 'N')

            wiz.write({
                'disconnectedserviceaccepted': 'S',
                'extrainfo': "Comentari",
                'modeffectdate': "02"
            })
            wiz.genera_casos_atr({'contract_id': contract_id})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            casos_ids = json.loads(wiz.casos_generados)
            self.assertEqual(len(casos_ids), 0)

    def test_create_wizard_05_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            atr_obj = self.openerp.pool.get('giscegas.atr')
            wiz_obj = self.openerp.pool.get(
                'giscegas.atr.a105.wizard'
            )
            contract_obj = self.openerp.pool.get('giscegas.polissa')
            contract_id = self.get_contract_id(txn)
            pol = contract_obj.browse(cursor, uid, contract_id)
            new_contract_id = self.get_contract_id(txn, xml_id="polissa_gas_0003")
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            imd_obj = self.openerp.pool.get('ir.model.data')
            other_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]
            another_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_c2c'
            )[1]
            new_contract.write({
                'cups': pol.cups.id,
                'tarifa': 2,
                'caudal_diario': 3.8,
                'titular': other_id,
                'pagador': another_id,
                'cnae': 4
            })
            new_contract.titular.write({
                'name': "C1 C2, N1",
                "vat": "ES00000000T"
            })
            new_contract.direccio_notificacio.write({
                # Dummy ids, we just want to fill them
                'state_id': self.openerp.pool.get("res.country.state").search(cursor, uid, [], limit=1)[0],
                'id_municipi': self.openerp.pool.get("res.municipi").search(cursor, uid, [], limit=1)[0]
            })
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)

            # The default values should be the same than the contract ones
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'contract_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.modeffectdate, '05')
            self.assertEqual(wiz.cups, pol.cups.name)
            self.assertEqual(wiz.cnae.id, pol.cnae.id)
            self.assertEqual(wiz.tariff, pol.tarifa.codi_ocsum)
            self.assertEqual(wiz.newreqqd, pol.caudal_diario)
            self.assertEqual(wiz.contact.id, pol.direccio_notificacio.partner_id.id)
            self.assertEqual(wiz.owner.id, pol.titular.id)
            self.assertEqual(wiz.pagador.id, pol.pagador.id)
            self.assertEqual(wiz.bank.id, pol.bank and pol.bank.id or False)

            # Now we select a new contract and the values should be updated
            # We simulate the onchange_updatereason
            wiz.write({'updatereason': '23'})
            vals = wiz.onchange_updatereason(
                '23', wiz.contract.id, 'exists',
                wiz.new_contract and wiz.new_contract.id or False
            )
            wiz.write(vals['value'])
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.modeffectdate, '05')
            self.assertEqual(wiz.cups, new_contract.cups.name)
            self.assertEqual(wiz.cnae.id, new_contract.cnae.id)
            self.assertEqual(wiz.tariff, new_contract.tarifa.codi_ocsum)
            self.assertEqual(wiz.newreqqd, new_contract.caudal_diario)
            self.assertEqual(wiz.contact.id, new_contract.direccio_notificacio.partner_id.id)
            self.assertEqual(wiz.owner.id, new_contract.titular.id)
            self.assertEqual(wiz.pagador.id, new_contract.pagador.id)
            self.assertEqual(wiz.bank.id, new_contract.bank and new_contract.bank.id or False)
            self.assertEqual(wiz.generate_new_contract, 'exists')

            wiz.genera_casos_atr({'contract_id': contract_id})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            casos_ids = json.loads(wiz.casos_generats)

            # Check if the info of the case is correctlly created
            atr = atr_obj.browse(cursor, uid, casos_ids[0])
            a105 = atr.get_pas()
            self.assertEqual(a105.modeffectdate, '05')
            self.assertEqual(a105.updatereason, '23')
            self.assertEqual(a105.modeffectdate, wiz.modeffectdate)
            self.assertEqual(a105.newcaecode.id, new_contract.cnae.id)
            self.assertEqual(a105.newtolltype, new_contract.tarifa.codi_ocsum)
            self.assertEqual(a105.newreqqd, new_contract.caudal_diario)
            self.assertEqual(a105.newtelephone, new_contract.direccio_notificacio.phone)
            self.assertEqual("ES"+a105.newdocumentnum, new_contract.titular.vat)

    def test_create_wizard_05_generates_new_contract_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            atr_obj = self.openerp.pool.get('giscegas.atr')
            wiz_obj = self.openerp.pool.get(
                'giscegas.atr.a105.wizard'
            )
            contract_obj = self.openerp.pool.get('giscegas.polissa')
            contract_id = self.get_contract_id(txn)
            pol = contract_obj.browse(cursor, uid, contract_id)
            new_contract_id = self.get_contract_id(txn, xml_id="polissa_gas_0003")
            contract_obj.write(cursor, uid, new_contract_id, {'cups': pol.cups.id})
            imd_obj = self.openerp.pool.get('ir.model.data')
            other_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]
            another_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_c2c'
            )[1]
            pol.write({
                'tarifa': 2,
                'caudal_diario': 3.8,
                'titular': other_id,
                'pagador': another_id,
                'cnae': 4
            })
            pol = contract_obj.browse(cursor, uid, pol.id)
            pol.titular.write({
                'name': "C1 C2, N1",
                "vat": "ES00000000T"
            })
            pol.direccio_notificacio.write({
                # Dummy ids, we just want to fill them
                'state_id': self.openerp.pool.get("res.country.state").search(cursor, uid, [], limit=1)[0],
                'id_municipi': self.openerp.pool.get("res.municipi").search(cursor, uid, [], limit=1)[0]
            })
            pol = contract_obj.browse(cursor, uid, pol.id)

            # The default values should be the same than the contract ones
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'contract_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.modeffectdate, '05')
            self.assertEqual(wiz.cups, pol.cups.name)
            self.assertEqual(wiz.cnae.id, pol.cnae.id)
            self.assertEqual(wiz.tariff, pol.tarifa.codi_ocsum)
            self.assertEqual(wiz.contact.id, pol.direccio_notificacio.partner_id.id)
            self.assertEqual(wiz.owner.id, pol.titular.id)
            self.assertEqual(wiz.pagador.id, pol.pagador.id)
            self.assertEqual(wiz.bank.id, pol.bank and pol.bank.id or False)

            # We have a new_contract but we select "create" so it should still
            # use the old contract values.
            # We simulate the onchange_updatereason
            wiz.write({'updatereason': '23'})
            vals = wiz.onchange_updatereason(
                '23', wiz.contract.id, 'create',
                wiz.new_contract and wiz.new_contract.id or False
            )
            wiz.write(vals['value'])
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.modeffectdate, '05')
            self.assertEqual(wiz.cups, pol.cups.name)
            self.assertEqual(wiz.cnae.id, pol.cnae.id)
            self.assertEqual(wiz.tariff, pol.tarifa.codi_ocsum)
            self.assertEqual(wiz.newreqqd, pol.caudal_diario)
            self.assertEqual(wiz.contact.id, pol.direccio_notificacio.partner_id.id)
            self.assertEqual(wiz.owner.id, pol.titular.id)
            self.assertEqual(wiz.pagador.id, pol.pagador.id)
            self.assertEqual(wiz.bank.id, pol.bank and pol.bank.id or False)
            wiz.write({'newreqqd': 1})
            wiz.write({'pagador': other_id})

            wiz.genera_casos_atr({'contract_id': contract_id})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            casos_ids = json.loads(wiz.casos_generats)

            # Check if the info of the case is correctlly created
            atr = atr_obj.browse(cursor, uid, casos_ids[0])
            a105 = atr.get_pas()
            self.assertEqual(a105.modeffectdate, '05')
            self.assertEqual(a105.updatereason, '23')
            self.assertEqual(a105.modeffectdate, wiz.modeffectdate)
            self.assertEqual(a105.newcaecode.id, pol.cnae.id)
            self.assertEqual(a105.newtolltype, pol.tarifa.codi_ocsum)
            self.assertEqual(a105.newreqqd, 1)
            self.assertEqual(a105.newtelephone, pol.direccio_notificacio.phone)
            self.assertEqual("ES" + a105.newdocumentnum, pol.titular.vat)

            # Check if we have a new contract
            generated_contract_id = int(atr.ref.split(",")[1])
            generated = contract_obj.browse(cursor, uid, generated_contract_id)
            self.assertEqual(generated.titular.id, wiz.owner.id)
            self.assertEqual(generated.pagador.id, wiz.pagador.id)
            self.assertEqual(generated.tarifa.codi_ocsum, wiz.tariff)
            self.assertEqual(generated.direccio_pagament.id, wiz.direccio_pagament.id)
            self.assertEqual(generated.direccio_notificacio.id, wiz.direccio_notificacio.id)

    def test_create_wizard_44_ok(self):
        self.openerp.install_module('giscegas_lectures_pool')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            lect_pool_obj = self.openerp.pool.get("giscegas.lectures.lectura.pool")
            atr_obj = self.openerp.pool.get('giscegas.atr')
            wiz_obj = self.openerp.pool.get(
                'giscegas.atr.a144.wizard'
            )
            contract_obj = self.openerp.pool.get('giscegas.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            origen_obj = self.openerp.pool.get('giscegas.lectures.origen')
            origen_autolectura_id = origen_obj.search(cursor, uid, [('codi', '=', '2')])[0]
            lect_pool_obj.create(cursor, uid, {
                'name': '2019-01-01',
                'lectura': 100,
                'periode': 1,
                'origen_id': origen_autolectura_id,
                'comptador': contract.comptadors[0].id,
                'pressio_subministrament': 1,
                'factor_k': 1,
                'pcs': 1,
            })

            contract.titular.write({
                'name': "C1 C2, N1", 'vat': "ES11111111H"
            })
            contract.direccio_notificacio.write({
                'phone': '987654321'
            })
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'contract_id': contract_id}
            )
            wiz = wiz_obj.read(
                cursor, uid, wiz_id,
                ['operationtype', 'sourcetype', 'titular_id',
                 'direccio_notificacio', 'readingdate', 'readingvalue']
            )[0]

            self.assertEqual(wiz['operationtype'], 'A10011')
            self.assertEqual(wiz['sourcetype'], '06')
            self.assertEqual(wiz['titular_id'], contract.titular.id)
            self.assertEqual(wiz['direccio_notificacio'], contract.direccio_notificacio.id)
            self.assertEqual(wiz['readingdate'], '2019-01-01')
            self.assertEqual(wiz['readingvalue'], 100)
            wiz_obj.write(cursor, uid, [wiz_id], {'sourcetype': '07', 'operationnum': '1111', 'extrainfo': "LOL"})

            wiz_obj.genera_casos_atr(cursor, uid, [wiz_id], {'contract_id': contract_id})
            wiz = wiz_obj.read(cursor, uid, [wiz_id], ['casos_generats'])[0]
            casos_ids = json.loads(wiz['casos_generats'])
            atr = atr_obj.browse(cursor, uid, casos_ids[0])
            a141 = atr.get_pas()

            self.assertEqual(a141.firstname, 'N1')
            self.assertEqual(a141.lastname, 'C1')
            self.assertEqual(a141.secondname, 'C2')
            self.assertEqual(a141.telephone1, "987654321")
            self.assertEqual(a141.sourcetype, "07")
            self.assertEqual(a141.operationnum, "1111")
            self.assertEqual(a141.extrainfo, "LOL")
            self.assertEqual(a141.readingvalue, 100)

    def test_create_wizard_04_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            atr_obj = self.openerp.pool.get('giscegas.atr')
            wiz_obj = self.openerp.pool.get(
                'giscegas.atr.a104.wizard'
            )
            contract_obj = self.openerp.pool.get('giscegas.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)

            contract.titular.write({
                'name': "I de Inglaterra, Isabel"
            })
            contract.direccio_notificacio.write({
                'phone': '987654321'
            })
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'contract_id': contract_id}
            )
            wiz = wiz_obj.read(
                cursor, uid, wiz_id,
                ['owner', 'direccio_notificacio']
            )[0]

            self.assertEqual(wiz['owner'], contract.titular.id)
            self.assertEqual(
                wiz['direccio_notificacio'], contract.direccio_notificacio.id
            )

            extra_info = b64decode("""
            TnVuY2EgaGFicsOhIHVuYSBSZWluYSBzZW50YWRhIGVuIG3DrSBwdWVzdG8gY29uI
            G3DoXMgY2VsbyBwb3IgbWkgcGHDrXMsIG3DoXMgcHJlb2N1cGFkYSBwb3IgbWlzIHP
            DumJkaXRvcyB5IHF1ZSBtw6FzIHByb250byB5IGNvbiBtw6FzIHZvbHVudGFkI
            HZhIGEgYXJyaWVzZ2FyIHN1IHZpZGEgcG9yIHN1IGJpZW5lc3RhciB5IHN1
            IHNlZ3VyaWRhZCBxdWUgcG9yIG3DrSBtaXNtYS4=""")

            wiz_obj.write(cursor, uid, [wiz_id], {
                'extrainfo': extra_info
            })

            wiz_obj.genera_casos_atr(cursor, uid, [wiz_id], {'contract_id': contract_id})
            wiz = wiz_obj.read(cursor, uid, [wiz_id], ['casos_generados'])[0]

            casos_ids = json.loads(wiz['casos_generados'])
            atr = atr_obj.browse(cursor, uid, casos_ids[0])
            a104 = atr.get_pas()

            self.assertEqual(a104.contactphonenumber, "987654321")
            self.assertEqual(a104.extrainfo, extra_info)

    def test_create_wizard_03_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            step_obj = self.openerp.pool.get('giscegas.atr.41.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            # Creem i tanquem un cas
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )
            a141 = step_obj.browse(cursor, uid, step_id)
            a141.sw_id.case_close()

            # Creem un segon cas que deixem obert
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '41', 'a1'
            )
            open_a141 = step_obj.browse(cursor, uid, step_id).sw_id

            # Ara creem u A1_03 que hauria de trobar el cas obert
            wiz_obj = self.openerp.pool.get('giscegas.atr.a103.wizard')
            contract_id = self.get_contract_id(txn)
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'contract_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.annulmentreason, '019')
            self.assertEqual(wiz.atr_anulat_id.id, open_a141.id)

            wiz.genera_casos_atr({'contract_id': contract_id})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            casos_ids = json.loads(wiz.casos_generados)
            atr = sw_obj.browse(cursor, uid, casos_ids[0])
            a103 = atr.get_pas()

            self.assertEqual("ES"+a103.documentnum, atr.cups_polissa_id.titular.vat)
            self.assertEqual(a103.titulartype, "J")
            self.assertEqual(a103.documenttype, "01")
            self.assertEqual(a103.nationality, "CH")
            self.assertEqual(a103.annulmentreason, '019')
            self.assertEqual(a103.atr_anulat_id.id, open_a141.id)

    def export_multiple_xml_divided_by_process(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            s03_obj = self.openerp.pool.get('giscegas.atr.03.a1')
            s46_obj = self.openerp.pool.get('giscegas.atr.46.a1')
            sw_obj = self.openerp.pool.get('giscegas.atr')
            imd_obj = self.openerp.pool.get('ir.model.data')
            conf_obj = self.openerp.pool.get('res.config')
            uid = txn.user
            cursor = txn.cursor

            # No validar xmls generats. Nomes per tests
            conf_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_atr', "atr_gas_xsd_validation"
            )[1]
            conf_obj.write(cursor, uid, conf_id, {'value': 0})
            # Obtenir el segon receptor
            distri2_id = imd_obj.get_object_reference(
                cursor, uid, 'giscemisc_participant', "participant_res_partner_c2c"
            )[1]
            # Preparar contracte
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            # Creem casos per la distri 1: dos 44 i un 46
            sid = self.create_case_and_step(
                cursor, uid, contract_id, '03', 'a1'
            )
            cas1_03_distri1 = s03_obj.browse(cursor, uid, sid).sw_id.id
            sid = self.create_case_and_step(
                cursor, uid, contract_id, '03', 'a1'
            )
            cas2_03_distri1 = s03_obj.browse(cursor, uid, sid).sw_id.id
            sid = self.create_case_and_step(
                cursor, uid, contract_id, '46', 'a1'
            )
            cas1_46_distri1 = s46_obj.browse(cursor, uid, sid).sw_id.id
            # Creem casos per la distri 2: un 44
            sid = self.create_case_and_step(
                cursor, uid, contract_id, '03', 'a1'
            )
            s03_obj.write(cursor, uid, sid, {'receptor_id': distri2_id})
            cas1_03_distri2 = s03_obj.browse(cursor, uid, sid).sw_id.id
            # Exportem tots els casos. El zip resultant ha de tenir l'estructura:
            # zip_general
            #   .
            #   ..distri1
            #   .      ..03
            #   .      .   ..cas1_03_distri1
            #   .      .   ..cas2_03_distri1
            #   .      .
            #   .      ..46
            #   .         ..cas1_46_distri1
            #   .
            #   ..distri2
            #         ..03
            #            ..cas1_44_distri2
            wiz_atr = self.openerp.pool.get("giscegas.atr.wizard")
            ctx = {
                'active_ids': [cas1_03_distri1, cas1_03_distri2, cas1_46_distri1, cas2_03_distri1],
                'active_id': cas1_03_distri1
            }
            wiz = wiz_atr.create(cursor, uid, {}, context=ctx)
            wiz_atr.action_exportar_xml(cursor, uid, [wiz], context=ctx)
            wiz = wiz_atr.browse(cursor, uid, wiz)
            self.assertEqual(len(wiz.cas), 4)
            self.assertFalse(len(wiz.cas_err))

            # zip_general
            zip_general = zipfile.ZipFile(io.BytesIO(b64decode(wiz.file)), "r")
            fileinfo = zip_general.namelist()
            self.assertEqual(len(fileinfo), 2)
            distri1_name, distri2_name = fileinfo

            # distri1
            distri1 = zipfile.ZipFile(io.BytesIO(zip_general.read(distri1_name)), "r")
            fileinfo = distri1.namelist()
            self.assertEqual(len(fileinfo), 2)
            distri1_46_name, distri1_03_name = fileinfo

            # distri1_03
            distri1_03 = zipfile.ZipFile(io.BytesIO(distri1.read(distri1_03_name)), "r")
            fileinfo = distri1_03.namelist()
            self.assertEqual(len(fileinfo), 2)

            # distri1_46
            distri1_46 = zipfile.ZipFile(io.BytesIO(distri1.read(distri1_46_name)), "r")
            fileinfo = distri1_46.namelist()
            self.assertEqual(len(fileinfo), 1)

            # distri2
            distri2 = zipfile.ZipFile(io.BytesIO(zip_general.read(distri2_name)), "r")
            fileinfo = distri2.namelist()
            self.assertEqual(len(fileinfo), 1)
            distri2_03_name = fileinfo[0]

            # distri2_03
            distri2_03 = zipfile.ZipFile(io.BytesIO(distri2.read(distri2_03_name)), "r")
            fileinfo = distri2_03.namelist()
            self.assertEqual(len(fileinfo), 1)
