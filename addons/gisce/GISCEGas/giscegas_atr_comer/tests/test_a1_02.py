# -*- coding: utf-8 -*-
from __future__ import absolute_import

from giscegas_atr.tests.common_tests import TestSwitchingImport

from destral.transaction import Transaction
from addons import get_module_resource
from destral.patch import PatchNewCursors


class Test02(TestSwitchingImport):

    def test_activate_a3_02(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a2.xml'
        )
        a3_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a3.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            mandate_obj = self.openerp.pool.get('payment.mandate')
            tarif_obj = self.openerp.pool.get("giscegas.polissa.tarifa")
            pricelist_obj = self.openerp.pool.get("product.pricelist")
            tarVer_obj = self.openerp.pool.get('product.pricelist.version')
            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Esborrem les dades aixi ens asegurem al fer el test que han
            # set emplenades per el ATR
            polissa = a1.sw_id.cups_polissa_id.browse()[0]
            polissa.write({
                'lot_facturacio': False,
                'llista_preu': False,
                'pressio': 1,
                'tarifa': False,
                'caudal_diario': 1
            })
            polissa.payment_mode_id.write({'require_bank_account': True})
            reference = 'giscegas.polissa,%s' % polissa.id
            search_mandates = [('reference', '=', reference),
                               ('date_end', '=', False)]
            mandates_ids = mandate_obj.search(cursor, uid, search_mandates)
            mandate_obj.unlink(cursor, uid, mandates_ids)

            prid = pricelist_obj.search(cursor, uid, [])[0]
            pricelist_obj.write(cursor, uid, prid, {'type': 'sale'})
            tarif_ids = tarif_obj.search(cursor, uid, [])
            tarif_obj.write(cursor, uid, tarif_ids,
                            {'llistes_preus_comptatibles': [(4, prid), ]})
            tids = tarVer_obj.search(cursor, uid, [('pricelist_id', '=', prid)])
            if len(tids) > 1:
                tarVer_obj.unlink(cursor, uid, tids[1:])
            tarVer_obj.write(cursor, uid, tids[0],
                             {'date_start': '2018-01-01', 'date_end': False})

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '02_a2.xml')
            a2 = a1.sw_id.get_pas()

            a3_xml = a3_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a3_xml, '02_a3.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '02'),
                ('step_id.name', '=', 'a3'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3 = sw_obj.get_pas(cursor, uid, cas_atr)

            # Activem el cas ATR
            with PatchNewCursors():
                res = sw_obj.activa_cas_atr(cursor, uid, cas_atr)
            polissa = cas_atr.cups_polissa_id.browse()[0]

            # Comprovem que s'hagi activat correctament
            self.assertEqual(polissa.data_alta, a3.transfereffectivedate)
            self.assertTrue(polissa.lot_facturacio)
            self.assertGreaterEqual(polissa.lot_facturacio.data_inici,
                                    a3.transfereffectivedate)
            self.assertEqual(polissa.facturacio_distri, 'B')
            self.assertEqual(polissa.tarifa.codi_ocsum, a2.tolltype)
            self.assertEqual(polissa.pressio, a2.outgoingpressuregranted)
            self.assertEqual(polissa.caudal_diario, round(a3.finalclientyearlyconsumption/365.0, 7))
            self.assertEqual(polissa.inspeccio_ids[0].resultado_inspeccion, a3.lastinspectionsresult)
            # Comprovem que s'ha creat un mandato
            reference = 'giscegas.polissa,%s' % polissa.id
            search_mandates = [('reference', '=', reference),
                               ('date_end', '=', False)]
            mandates_ids = mandate_obj.search(cursor, uid, search_mandates)
            self.assertTrue(len(mandates_ids))

            # Comprovem que s'han creat els comptadors correctament
            comptadors = [c for c in polissa.comptadors if c.active]
            self.assertEquals(len(comptadors), len(a3.header_id.counter_ids))
            self.assertEquals(comptadors[0].name,
                              a3.header_id.counter_ids[0].counternumber)
            self.assertEquals(comptadors[0].propietat,
                              a3.header_id.counter_ids[0].counterproperty)
            self.assertEquals(comptadors[0].data_alta, a3.transfereffectivedate)
            self.assertEquals(comptadors[1].name,
                              a3.header_id.counter_ids[1].counternumber)
            self.assertEquals(comptadors[1].propietat,
                              a3.header_id.counter_ids[1].counterproperty)
            self.assertEquals(comptadors[1].data_alta, a3.transfereffectivedate)

    def test_activate_a3_02_with_qdcalc(self):
        a2_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a2.xml'
        )
        a3_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '02_a3.xml'
        )
        with open(a2_xml_path, 'r') as f:
            a2_xml = f.read()

        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            mandate_obj = self.openerp.pool.get('payment.mandate')
            tarif_obj = self.openerp.pool.get("giscegas.polissa.tarifa")
            pricelist_obj = self.openerp.pool.get("product.pricelist")
            tarVer_obj = self.openerp.pool.get('product.pricelist.version')
            sw_obj = self.openerp.pool.get('giscegas.atr')
            step_obj = self.openerp.pool.get('giscegas.atr.02.a1')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, '02', 'a1'
            )

            a1 = step_obj.browse(cursor, uid, step_id)

            # Esborrem les dades aixi ens asegurem al fer el test que han
            # set emplenades per el ATR
            polissa = a1.sw_id.cups_polissa_id.browse()[0]
            polissa.write({
                'lot_facturacio': False,
                'llista_preu': False,
                'pressio': 1,
                'tarifa': False,
                'caudal_diario': 1
            })
            polissa.payment_mode_id.write({'require_bank_account': True})
            reference = 'giscegas.polissa,%s' % polissa.id
            search_mandates = [('reference', '=', reference),
                               ('date_end', '=', False)]
            mandates_ids = mandate_obj.search(cursor, uid, search_mandates)
            mandate_obj.unlink(cursor, uid, mandates_ids)

            prid = pricelist_obj.search(cursor, uid, [])[0]
            pricelist_obj.write(cursor, uid, prid, {'type': 'sale'})
            tarif_ids = tarif_obj.search(cursor, uid, [])
            tarif_obj.write(cursor, uid, tarif_ids,
                            {'llistes_preus_comptatibles': [(4, prid), ]})
            tids = tarVer_obj.search(cursor, uid, [('pricelist_id', '=', prid)])
            if len(tids) > 1:
                tarVer_obj.unlink(cursor, uid, tids[1:])
            tarVer_obj.write(cursor, uid, tids[0],
                             {'date_start': '2018-01-01', 'date_end': False})

            # Change the dates & id from the XML.
            codi_sol = a1.sw_id.codi_sollicitud
            data_emissio = a1.date_created.split(" ")[0]
            hora_emissio = a1.date_created.split(" ")[1]

            a2_xml = a2_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a2_xml = a2_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a2_xml = a2_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )
            a2_xml = a2_xml.replace(
                "<qdgranted>0.5</qdgranted>",
                "<qdgranted>0</qdgranted>"
            )

            sw_obj.importar_xml(cursor, uid, a2_xml, '02_a2.xml')
            a2 = a1.sw_id.get_pas()

            a3_xml = a3_xml.replace(
                "<communicationsdate>2018-05-01</communicationsdate>",
                "<communicationsdate>{0}</communicationsdate>"
                    .format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<communicationshour>12:00:00</communicationshour>",
                "<communicationshour>{0}</communicationshour>"
                    .format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqdate>2018-05-01</reqdate>",
                "<reqdate>{0}</reqdate>".format(data_emissio)
            )
            a3_xml = a3_xml.replace(
                "<reqhour>12:00:00</reqhour>",
                "<reqhour>{0}</reqhour>".format(hora_emissio)
            )
            a3_xml = a3_xml.replace(
                "<comreferencenum>000123456789</comreferencenum>",
                "<comreferencenum>{0}</comreferencenum>".format(codi_sol)
            )

            sw_obj.importar_xml(cursor, uid, a3_xml, '02_a3.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '02'),
                ('step_id.name', '=', 'a3'),
                ('codi_sollicitud', '=', codi_sol)
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a3 = sw_obj.get_pas(cursor, uid, cas_atr)

            # Activem el cas ATR
            with PatchNewCursors():
                res = sw_obj.activa_cas_atr(cursor, uid, cas_atr)
            polissa = cas_atr.cups_polissa_id.browse()[0]

            # Comprovem que s'hagi activat correctament
            self.assertEqual(polissa.data_alta, a3.transfereffectivedate)
            self.assertTrue(polissa.lot_facturacio)
            self.assertGreaterEqual(polissa.lot_facturacio.data_inici,
                                    a3.transfereffectivedate)
            self.assertEqual(polissa.facturacio_distri, 'B')
            self.assertEqual(polissa.tarifa.codi_ocsum, a2.tolltype)
            self.assertEqual(polissa.pressio, a2.outgoingpressuregranted)
            self.assertEqual(polissa.caudal_diario,
                             round(a3.finalclientyearlyconsumption / 365.0, 7))
            # Comprovem que s'ha creat un mandato
            reference = 'giscegas.polissa,%s' % polissa.id
            search_mandates = [('reference', '=', reference),
                               ('date_end', '=', False)]
            mandates_ids = mandate_obj.search(cursor, uid, search_mandates)
            self.assertTrue(len(mandates_ids))
