# -*- coding: utf-8 -*-
from __future__ import absolute_import

from giscegas_atr.tests.common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime
from destral.patch import PatchNewCursors


class Test26(TestSwitchingImport):

    def test_import_a12_26(self):
        a12_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '26_a12.xml'
        )
        with open(a12_xml_path, 'r') as f:
            a12_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')
            pol_obj = self.openerp.pool.get("giscegas.polissa")
            contract_id = self.get_contract_id(txn)

            sw_obj = self.openerp.pool.get('giscegas.atr')
            tarif_obj = self.openerp.pool.get("giscegas.polissa.tarifa")
            pricelist_obj = self.openerp.pool.get("product.pricelist")
            tarVer_obj = self.openerp.pool.get('product.pricelist.version')

            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            prid = pricelist_obj.search(cursor, uid, [])[0]
            pricelist_obj.write(cursor, uid, prid, {'type': 'sale'})
            tarif_ids = tarif_obj.search(cursor, uid, [])
            tarif_obj.write(cursor, uid, tarif_ids, {'llistes_preus_comptatibles': [(4, prid), ]})
            tids = tarVer_obj.search(cursor, uid, [('pricelist_id', '=', prid)])
            if len(tids) > 1:
                tarVer_obj.unlink(cursor, uid, tids[1:])
            tarVer_obj.write(cursor, uid, tids[0], {'date_start': '2018-01-01', 'date_end': False})

            sw_obj.importar_xml(cursor, uid, a12_xml, '26_a12.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '26'),
                ('step_id.name', '=', 'a12'),
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a12 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(a12.date_created, '2018-05-01 12:00:00'),
            self.assertEqual(a12.atrcode, '24_0mVgXP2mQWbjDIW3Y7DFf'),
            self.assertEqual(a12.atrversion, 2),
            self.assertEqual(cas_atr.cups_id.name, 'ES1234000000000001JN0F'),
            self.assertEqual(a12.nationality, 'ES'),
            self.assertEqual(a12.documenttype, '01'),
            self.assertEqual(a12.documentnum, '11111111H'),
            self.assertEqual(a12.firstname, 'Gas'),
            self.assertEqual(a12.familyname1, 'Al'),
            self.assertEqual(a12.familyname2, 'Matalas'),
            self.assertEqual(a12.telephone, '999888777'),
            self.assertEqual(a12.fax, '666555444'),
            self.assertEqual(a12.newcustomer, 'S'),
            self.assertEqual(a12.email, 'gasalmatalas@atr.cat'),
            self.assertEqual(a12.streettype, 'ACCE'),
            self.assertEqual(a12.street, 'Carrer inventat'),
            self.assertEqual(a12.streetnumber, '1'),
            self.assertEqual(a12.portal, '2'),
            self.assertEqual(a12.staircase, '3'),
            self.assertEqual(a12.floor, '4'),
            self.assertEqual(a12.door, 'v'),
            self.assertEqual(a12.province.code, '19'),
            self.assertEqual(a12.city.ine, '49221'),
            self.assertEqual(a12.zipcode, '17002'),
            self.assertEqual(a12.tolltype, '32'),
            self.assertEqual(a12.qdgranted, 321.123),
            self.assertEqual(a12.qhgranted, 321),
            self.assertEqual(a12.singlenomination, 'S'),
            self.assertEqual(a12.transfereffectivedate, '2018-07-01'),
            self.assertEqual(a12.finalclientyearlyconsumption, 65432211),
            self.assertEqual(a12.netsituation, '1'),
            self.assertEqual(a12.outgoingpressuregranted, 10),
            self.assertEqual(a12.lastinspectionsdate, '2017-01-01 00:00:00'),
            self.assertEqual(a12.lastinspectionsresult, '01'),
            self.assertEqual(a12.readingtype, '1'),
            self.assertEqual(a12.rentingamount, 40770.27),
            self.assertEqual(a12.rentingperiodicity, '01'),
            self.assertEqual(a12.canonircamount, 62529.35),
            self.assertEqual(a12.canonircperiodicity, '01'),
            self.assertEqual(a12.canonircforlife, 'S'),
            self.assertEqual(a12.canonircdate, '2019-05-16 00:00:00'),
            self.assertEqual(a12.canonircmonth, '01'),
            self.assertEqual(a12.othersamount, 51589.82),
            self.assertEqual(a12.othersperiodicity, '01'),
            self.assertEqual(a12.readingperiodicitycode, '01'),
            self.assertEqual(a12.transporter, '0001'),
            self.assertEqual(a12.transnet, 'alguna cosa'),
            self.assertEqual(a12.gasusetype, '01'),
            self.assertEqual(a12.caecode.name, '9820'),
            self.assertEqual(a12.updatereason, '11'),
            self.assertEqual(a12.communicationreason, '06'),
            self.assertEqual(a12.titulartype, 'F'),
            self.assertEqual(a12.regularaddress, 'S'),
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, cas_atr)
            pol = pol_obj.browse(cursor, uid, contract_id)
            self.assertEqual(pol.tarifa.codi_ocsum, a12.tolltype)
            self.assertEqual(pol.modcontractual_activa.data_inici, a12.transfereffectivedate)

    def test_import_a12_26_activat_on_data_alta(self):
        a12_xml_path = get_module_resource(
            'giscegas_atr', 'tests', 'fixtures', '26_a12.xml'
        )
        with open(a12_xml_path, 'r') as f:
            a12_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')
            pol_obj = self.openerp.pool.get("giscegas.polissa")
            contract_id = self.get_contract_id(txn)

            sw_obj = self.openerp.pool.get('giscegas.atr')
            tarif_obj = self.openerp.pool.get("giscegas.polissa.tarifa")
            pricelist_obj = self.openerp.pool.get("product.pricelist")
            tarVer_obj = self.openerp.pool.get('product.pricelist.version')

            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            prid = pricelist_obj.search(cursor, uid, [])[0]
            pricelist_obj.write(cursor, uid, prid, {'type': 'sale'})
            tarif_ids = tarif_obj.search(cursor, uid, [])
            tarif_obj.write(cursor, uid, tarif_ids, {'llistes_preus_comptatibles': [(4, prid), ]})
            tids = tarVer_obj.search(cursor, uid, [('pricelist_id', '=', prid)])
            if len(tids) > 1:
                tarVer_obj.unlink(cursor, uid, tids[1:])
            tarVer_obj.write(cursor, uid, tids[0], {'date_start': '2018-01-01', 'date_end': False})

            sw_obj.importar_xml(cursor, uid, a12_xml, '26_a12.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', '26'),
                ('step_id.name', '=', 'a12'),
            ])

            cas_atr = sw_obj.browse(cursor, uid, res[0])
            a12 = sw_obj.get_pas(cursor, uid, cas_atr)
            self.assertEqual(a12.date_created, '2018-05-01 12:00:00'),
            self.assertEqual(a12.atrcode, '24_0mVgXP2mQWbjDIW3Y7DFf'),
            self.assertEqual(a12.atrversion, 2),
            self.assertEqual(cas_atr.cups_id.name, 'ES1234000000000001JN0F'),
            self.assertEqual(a12.nationality, 'ES'),
            self.assertEqual(a12.documenttype, '01'),
            self.assertEqual(a12.documentnum, '11111111H'),
            self.assertEqual(a12.firstname, 'Gas'),
            self.assertEqual(a12.familyname1, 'Al'),
            self.assertEqual(a12.familyname2, 'Matalas'),
            self.assertEqual(a12.telephone, '999888777'),
            self.assertEqual(a12.fax, '666555444'),
            self.assertEqual(a12.newcustomer, 'S'),
            self.assertEqual(a12.email, 'gasalmatalas@atr.cat'),
            self.assertEqual(a12.streettype, 'ACCE'),
            self.assertEqual(a12.street, 'Carrer inventat'),
            self.assertEqual(a12.streetnumber, '1'),
            self.assertEqual(a12.portal, '2'),
            self.assertEqual(a12.staircase, '3'),
            self.assertEqual(a12.floor, '4'),
            self.assertEqual(a12.door, 'v'),
            self.assertEqual(a12.province.code, '19'),
            self.assertEqual(a12.city.ine, '49221'),
            self.assertEqual(a12.zipcode, '17002'),
            self.assertEqual(a12.tolltype, '32'),
            self.assertEqual(a12.qdgranted, 321.123),
            self.assertEqual(a12.qhgranted, 321),
            self.assertEqual(a12.singlenomination, 'S'),
            self.assertEqual(a12.transfereffectivedate, '2018-07-01'),
            self.assertEqual(a12.finalclientyearlyconsumption, 65432211),
            self.assertEqual(a12.netsituation, '1'),
            self.assertEqual(a12.outgoingpressuregranted, 10),
            self.assertEqual(a12.lastinspectionsdate, '2017-01-01 00:00:00'),
            self.assertEqual(a12.lastinspectionsresult, '01'),
            self.assertEqual(a12.readingtype, '1'),
            self.assertEqual(a12.rentingamount, 40770.27),
            self.assertEqual(a12.rentingperiodicity, '01'),
            self.assertEqual(a12.canonircamount, 62529.35),
            self.assertEqual(a12.canonircperiodicity, '01'),
            self.assertEqual(a12.canonircforlife, 'S'),
            self.assertEqual(a12.canonircdate, '2019-05-16 00:00:00'),
            self.assertEqual(a12.canonircmonth, '01'),
            self.assertEqual(a12.othersamount, 51589.82),
            self.assertEqual(a12.othersperiodicity, '01'),
            self.assertEqual(a12.readingperiodicitycode, '01'),
            self.assertEqual(a12.transporter, '0001'),
            self.assertEqual(a12.transnet, 'alguna cosa'),
            self.assertEqual(a12.gasusetype, '01'),
            self.assertEqual(a12.caecode.name, '9820'),
            self.assertEqual(a12.updatereason, '11'),
            self.assertEqual(a12.communicationreason, '06'),
            self.assertEqual(a12.titulartype, 'F'),
            self.assertEqual(a12.regularaddress, 'S'),

            pol = pol_obj.browse(cursor, uid, contract_id)
            a12.write({'transfereffectivedate': pol.data_alta})
            a12 = a12.browse()[0]
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, cas_atr)
            pol = pol_obj.browse(cursor, uid, contract_id)
            self.assertEqual(pol.tarifa.codi_ocsum, a12.tolltype)
            self.assertEqual(pol.modcontractual_activa.data_inici, a12.transfereffectivedate)
