# -*- coding: utf-8 -*-
{
    "name": "Base (Gas)",
    "description": """Mòdul base per als mòduls de gas que codifica 
    comportaments específics d'aquests.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends": [
        "base",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
