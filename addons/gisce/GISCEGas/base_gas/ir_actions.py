# -*- encoding: utf-8 -*-
from osv import osv
from tools.translate import _


class act_window(osv.osv):

    _name = 'ir.actions.act_window'
    _inherit = 'ir.actions.act_window'

    def read(self, cursor, uid, ids, fields=None, context=None,
             load='_classic_read'):
        if context is None:
            context = {}
        res = super(act_window, self).read(
            cursor, uid, ids, fields, context=context, load=load
        )
        for values in ([res] if not isinstance(res, (tuple, list)) else res):
            # If we want to read the name, we must check if it is a giscegas
            # action
            if 'name' not in values:
                continue

            # We try to get the model if it's in the values; if there is not,
            # we will read it
            if 'res_model' in values:
                res_model = values['res_model']
            else:
                res_model = self.q(cursor, uid).read(['res_model']).where([
                    ('id', '=', values['id'])
                ])[0]['res_model']

            # Finally, if it's a giscegas action, we re-write the name to be
            # returned
            if 'giscegas' in res_model:
                values['name'] = '{prefix} {original_name}'.format(
                    prefix=_('Gas:'), original_name=values['name']
                )
        return res


act_window()
