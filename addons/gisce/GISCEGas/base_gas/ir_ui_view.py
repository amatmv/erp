# -*- encoding: utf-8 -*-
from osv import osv
from tools.translate import _


class view_sc(osv.osv):
    """
    Extensió del model view_sc per a crear vistes amb el nom de l'action en
    comptes del nom del res_id.
    """

    _name = 'ir.ui.view_sc'
    _inherit = 'ir.ui.view_sc'

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if vals.get('resource') == 'ir.ui.menu':
            menu_obj = self.pool.get('ir.ui.menu')
            action = menu_obj.read(cr, uid, vals['res_id'],
                                   ['action'])['action'].split(',')
            action_model = self.pool.get(action[0])
            action_id = int(action[1])
            vals['name'] = action_model.read(cr, uid, action_id,
                                             ['name'])['name']
        return super(view_sc, self).create(cr, uid, vals, context=context)


view_sc()
