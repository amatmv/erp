# -*- coding: utf-8 -*-

SIFCO_CODES_TRANSLATION = {
    # NEDGIA GALICIA
    # Gas la Coruña
    '0223': '0224',

    # NEDGIA CASTILLA LA MANCHA
    # Transmanchega
    '0235': '0220',

    # NEDGIA CASTILLA Y LEON
    # Inverduero Gas
    '0233': '0221',

    # REDEXIS GAS
    # Redexis Gas Aragon
    '0208': '0238',
    # redexis Gas Baleares
    '0209': '0238',
    # Distribuidora Regional Del Gas
    '0204': '0238',
    # Gas Alicante
    '0206': '0238',
    # Gas Merida
    '0228': '0238',
    # Gas de Figueres
    '0202': '0238',
    # Redexis Gas Distribucion
    '0205': '0238',

    # NED ESPAÑA DISTRIBUCION GAS
    # Gas de Asturias
    '0201': '0242',
    # Gas Energia Distribucion Cantabria
    '0219': '0242',

    # NORTEGAS
    # Gas de Euskadi
    '0210': '0229',
    # Bilbogas
    '0211': '0229',
    # Gas Natural de Alava
    '0212': '0229',
    # Donosti Gas
    '0213': '0229',
    # Gas Hernani
    '0214': '0229',
    # Gas Pasaia
    '0215': '0229',
    '0217': '0230',

    # MADRILEÑA RED DE GAS
    # Madrileña Red de Gas II
    '0236': '0234',
}

EXTENDED_SIFCO_CODES_TRANSLATION = {
    # Naturgas -> Ned España Distribución Gas
    '022901': '0242',
    '022902': '0242',
}


def get_dso(cups):
    distri = cups[2:6]
    return SIFCO_CODES_TRANSLATION.get(
        distri, EXTENDED_SIFCO_CODES_TRANSLATION.get(cups[2:8], distri)
    )
