# -*- coding: utf-8 -*-
{
    "name": "GISCE Gas CUPS",
    "description": """Codigo Universal de Punto de Provisionamiento""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CUPS",
    "depends": [
        "base_gas",
        "base_extended",
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegas_cups_demo.xml"
    ],
    "update_xml": [
        "giscegas_cups_view.xml",
        "giscegas_cups_data.xml",
        "security/giscegas_cups_security.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
