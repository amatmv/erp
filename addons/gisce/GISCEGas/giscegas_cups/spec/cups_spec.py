# -*- coding: utf-8 -*-
from expects import *

from giscegas_cups.dso_cups.cups import get_dso

with description("Checking get_dso function"):
    with it("Checking 0223 is 0224"):
        dso = get_dso('ES022355CCCCCCCCCCEENT')
        expect(dso).to(equal('0224'))

    with it("Checking 0210 is 0229"):
        dso = get_dso('ES021055CCCCCCCCCCEENT')
        expect(dso).to(equal('0229'))

    with it("Checking 022901 is 0242"):
        dso = get_dso('ES022901CCCCCCCCCCEENT')
        expect(dso).to(equal('0242'))
