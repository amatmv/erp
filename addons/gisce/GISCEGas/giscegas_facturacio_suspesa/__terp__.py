# -*- coding: utf-8 -*-
{
    "name": "Facturació de Gas Suspesa",
    "description": """Facturació Suspesa.
        Per aturar la facturació de Gas de certes pólisses a través del lot de facturació.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends":[
        "giscegas_facturacio",
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "giscegas_polissa_view.xml",
        "giscegas_facturacio_validation_data.xml"
    ],
    "active": False,
    "installable": True
}
