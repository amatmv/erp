# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscegasFacturacioLot(osv.osv):

    _name = 'giscegas.facturacio.lot'
    _inherit = 'giscegas.facturacio.lot'

    def validar(self, cursor, uid, lot_id, context=None):
        if context is None:
            context = {}

        extra_filter = context.get('extra_filter', [])
        extra_filter += [
            ('polissa_id.facturacio_suspesa', "!=", True)
        ]
        ctx2 = context.copy()
        ctx2['extra_filter'] = extra_filter
        ctx2['active_test'] = False
        return super(GiscegasFacturacioLot, self).validar(cursor, uid, lot_id, context=ctx2)

    def facturacio(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        extra_filter = context.get('extra_filter', [])
        extra_filter += [
            ('polissa_id.facturacio_suspesa', "!=", True)
        ]
        ctx2 = context.copy()
        ctx2['extra_filter'] = extra_filter
        ctx2['active_test'] = False
        return super(GiscegasFacturacioLot, self).facturacio(cursor, uid, ids, context=ctx2)

GiscegasFacturacioLot()


class GiscedataFacturacioValidationValidator(osv.osv):
    _name = 'giscegas.facturacio.validation.validator'
    _inherit = 'giscegas.facturacio.validation.validator'

    def check_facturacio_suspesa_lot(self, cursor, uid, clot, data_inici, data_fi, parametres, context=None):
        return self.check_facturacio_suspesa(cursor, uid, clot.polissa_id, context=context)

    def check_facturacio_suspesa_pre_facturacio(self, cursor, uid, fact, parameters, context=None):
        return self.check_facturacio_suspesa(cursor, uid, fact.polissa_id, context=context)

    def check_facturacio_suspesa(self, cursor, uid, polissa, context=None):
        return {} if polissa.facturacio_suspesa else None

GiscedataFacturacioValidationValidator()