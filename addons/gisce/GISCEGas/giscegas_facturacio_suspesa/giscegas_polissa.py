# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscegasPolissa(osv.osv):

    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    _columns = {
        'facturacio_suspesa': fields.boolean(string="Facturació Suspesa")
    }

    _defaults = {
        'facturacio_suspesa': lambda *a: False
    }


GiscegasPolissa()
