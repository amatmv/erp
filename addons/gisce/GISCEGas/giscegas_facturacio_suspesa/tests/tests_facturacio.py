# -*- coding: utf-8 -*-
from destral import testing
from expects import *
from destral.transaction import Transaction


class TestsInvoiceValidationSuspesa(testing.OOTestCase):

    def test_validation_V011(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            vali_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.validator'
            )
            warn_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.warning'
            )
            pol_obj = self.openerp.pool.get('giscegas.polissa')
            clot_obj = self.openerp.pool.get('giscegas.facturacio.contracte_lot')
            imd_obj = self.openerp.pool.get('ir.model.data')

            clot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'cont_lot_gas_0001'
            )[1]

            vals = clot_obj.read(cursor, uid, clot_id, ['polissa_id'])
            pol_obj.write(cursor, uid, vals['polissa_id'][0], {'facturacio_suspesa': True})

            # Desactivem els altres checks
            template_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.warning.template'
            )
            search_parameters = [('active', '=', True), ('code', '!=', "V011")]
            template_ids = template_obj.search(
                cursor, uid, search_parameters, order='code'
            )
            template_obj.write(cursor, uid, template_ids, {'active': False})

            # Validem
            warning_ids = vali_obj.validate_clot(cursor, uid, clot_id, False, False, None)

            warning_vals = warn_obj.read(
                cursor, uid, warning_ids,
                ['name', 'warning_template_id']
            )

            warning_names = [warn['name'] for warn in warning_vals]
            expect(warning_names).to(contain('V011'))

    def test_validation_V011_no_salta(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            vali_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.validator'
            )
            warn_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.warning'
            )
            pol_obj = self.openerp.pool.get('giscegas.polissa')
            clot_obj = self.openerp.pool.get(
                'giscegas.facturacio.contracte_lot')
            imd_obj = self.openerp.pool.get('ir.model.data')

            clot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'cont_lot_gas_0001'
            )[1]

            vals = clot_obj.read(cursor, uid, clot_id, ['polissa_id'])
            pol_obj.write(cursor, uid, vals['polissa_id'][0], {'facturacio_suspesa': False})

            # Desactivem els altres checks
            template_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.warning.template'
            )
            search_parameters = [('active', '=', True), ('code', '!=', "V011")]
            template_ids = template_obj.search(
                cursor, uid, search_parameters, order='code'
            )
            template_obj.write(cursor, uid, template_ids, {'active': False})

            # Validem
            warning_ids = vali_obj.validate_clot(cursor, uid, clot_id, False, False, None)

            warning_vals = warn_obj.read(
                cursor, uid, warning_ids,
                ['name', 'warning_template_id']
            )

            warning_names = [warn['name'] for warn in warning_vals]
            expect(warning_names).to_not(contain('V011'))

    def test_validation_F022(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            vali_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.validator'
            )
            warn_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.warning'
            )
            fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
            pol_obj = self.openerp.pool.get('giscegas.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
            )[1]

            fact_vals = fact_obj.read(cursor, uid, fact_id, ['polissa_id'])
            pol_obj.write(cursor, uid, fact_vals['polissa_id'][0], {'facturacio_suspesa': True})

            # Desactivem els altres checks
            template_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.warning.template'
            )
            search_parameters = [('active', '=', True), ('code', '!=', "F022")]
            template_ids = template_obj.search(
                cursor, uid, search_parameters, order='code'
            )
            template_obj.write(cursor, uid, template_ids, {'active': False})

            # Validem
            warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

            warning_vals = warn_obj.read(
                cursor, uid, warning_ids,
                ['name', 'warning_template_id']
            )

            warning_names = [warn['name'] for warn in warning_vals]
            expect(warning_names).to(contain('F022'))

    def test_validation_F022_no_salta(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            vali_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.validator'
            )
            warn_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.warning'
            )
            fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
            pol_obj = self.openerp.pool.get('giscegas.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscegas_facturacio', 'factura_gas_0001_polissa_0001_N'
            )[1]

            fact_vals = fact_obj.read(cursor, uid, fact_id, ['polissa_id'])
            pol_obj.write(cursor, uid, fact_vals['polissa_id'][0], {'facturacio_suspesa': False})

            # Desactivem els altres checks
            template_obj = self.openerp.pool.get(
                'giscegas.facturacio.validation.warning.template'
            )
            search_parameters = [('active', '=', True), ('code', '!=', "F022")]
            template_ids = template_obj.search(
                cursor, uid, search_parameters, order='code'
            )
            template_obj.write(cursor, uid, template_ids, {'active': False})

            # Validem
            warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

            warning_vals = warn_obj.read(
                cursor, uid, warning_ids,
                ['name', 'warning_template_id']
            )

            warning_names = [warn['name'] for warn in warning_vals]
            expect(warning_names).to_not(contain('F022'))
