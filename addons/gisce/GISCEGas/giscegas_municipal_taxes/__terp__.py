# -*- coding: utf-8 -*-
{
    "name": "Municipal taxes GAS",
    "description": """ Municipal taxes GAS
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_extended",
        "giscegas_cups",
        "giscegas_polissa",
        "account",
        "giscegas_polissa_category",
        "giscegas_facturacio_atr",
        "giscedata_administracio_publica"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "res_municipi_view.xml",
        "giscegas_municipal_taxes_data.xml",
        "wizard/wizard_municipal_taxes_view.xml",
        "wizard/wizard_municipal_taxes_invoicing_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
