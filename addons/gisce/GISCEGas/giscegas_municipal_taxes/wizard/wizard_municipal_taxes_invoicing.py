# -*- coding: utf-8 -*-report
from osv import osv, fields
from tools.translate import _
from datetime import datetime
import base64
from addons.giscegas_municipal_taxes.taxes.municipal_taxes_invoicing \
    import MunicipalTaxesInvoicingReport


class GiscegasMunicipalTaxesInvoicingWizard(osv.osv_memory):
    """ Municipal taxes - Invoicing - Wizard """

    _name = 'giscegas.municipal.taxes.invoicing.wizard'

    def build_report(self, cursor, uid, ids, context=None):
        """ Report """

        def valid_date(d):
            try:
                datetime.strptime(d, '%Y-%m-%d')
                return True
            except ValueError:
                return False

        if context is None or 'active_ids' not in context:
            return False  # We need the active_ids to execute the wizard
        wizard = self.browse(cursor, uid, ids[0], context=context)
        start_date = wizard.start_date
        end_date = wizard.end_date
        tax = wizard.tax
        file_type = wizard.file_type
        inv_list = wizard.invoice_list

        valid_dates = valid_date(start_date) and valid_date(end_date)
        if not valid_dates or not isinstance(tax, float):
            return False

        active_ids = context['active_ids']
        polissa_categ_imu_ex_id = (
            self.pool.get('ir.model.data').get_object_reference(
                cursor, uid, 'giscegas_municipal_taxes', 'contract_categ_imu_ex'
            )[1]
        )
        whereiam = self.pool.get('giscegas.cups.ps').whereiam(cursor, uid)
        taxes_invoicing_report = MunicipalTaxesInvoicingReport(
            cursor, uid, start_date, end_date, tax, file_type, inv_list,
            polissa_categ_imu_ex_id, whereiam
        )

        filename = 'municipal_taxes_invoicing.{0}'.format(file_type)
        report = taxes_invoicing_report.build_report_taxes(active_ids)

        wizard.write({
            'report': base64.b64encode(report),
            'filename_report': filename,
        })

        if wizard.provider_detail:
            providers = taxes_invoicing_report.build_report_providers()
            wizard.write({
                'proveidor': base64.b64encode(providers)
            })

        if wizard.client_detail:
            clients = taxes_invoicing_report.build_report_clients()
            wizard.write({
                'client': base64.b64encode(clients)
            })

        return True

    def default_get(self, cr, uid, fields_list, context=None):
        # We use default_get as directly assigning the value in defaults like
        # the rest doesn't seem to work for tax (no idea why)
        res = super(GiscegasMunicipalTaxesInvoicingWizard, self).default_get(
            cr, uid, fields_list, context
        )

        res.update({
            'tax': 1.5
        })

        for val in res.keys():
            if val not in fields_list:
                del res[val]

        return res

    _columns = {
        'filename_report': fields.char('Nom report taxes', size=256),
        'filename_proveidor': fields.char('Nom report proveidor', size=256),
        'filename_client': fields.char('Nom report client', size=256),
        'start_date': fields.date('Data d\'inici'),
        'end_date': fields.date('Data de fi'),
        'tax': fields.float('Impost Aplicat'),
        'invoice_list': fields.boolean('Afegir llista factures'),
        'file_type': fields.selection(
            [('csv', 'CSV'), ('xls', 'Excel'), ('xml', 'XML')],
            string='Tipus fitxer',
            help=u'Només afectarà al fitxer de taxes'
        ),
        'provider_detail': fields.boolean('Generar detalls proveidor'),
        'client_detail': fields.boolean('Generar detalls client'),
        'report': fields.binary('Taxes report'),
        'proveidor': fields.binary('Detall proveidor'),
        'client': fields.binary('Detall client'),
    }

    _defaults = {
        'filename_proveidor': lambda *a: _('detall_proveidor.csv'),
        'filename_client': lambda *a: _('detall_client.csv'),
        'tax': lambda *a: 1.5,
        'invoice_list': lambda *a: False,
        'file_type': lambda *a: 'csv',
        'provider_detail': lambda *a: False,
        'client_detail': lambda *a: False,
    }

GiscegasMunicipalTaxesInvoicingWizard()
