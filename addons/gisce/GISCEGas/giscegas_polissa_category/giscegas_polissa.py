# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscegasPolissaCategory(osv.osv):
    _description = 'Polissa de categories'
    _name = 'giscegas.polissa.category'

    def name_get(self, cursor, uid, ids, context=None):
        if not len(ids):
            return []
        reads = self.read(
            cursor, uid, ids, ['name', 'parent_id'], context=context
        )
        res = []
        for record in reads:
            name = record['name']
            if record['parent_id']:
                name = record['parent_id'][1] + ' / ' + name
            res.append((record['id'], name))
        return res

    def _name_get_fnc(self, cursor, uid, ids, prop, unknow_none, context=None):
        res = self.name_get(cursor, uid, ids, context=context)
        return dict(res)

    def _check_recursion(self, cursor, uid, ids):
        level = 100
        while len(ids):
            cursor.execute('SELECT DISTINCT parent_id '
                           'FROM giscegas_polissa_category WHERE id IN %s',
                           (tuple(ids),))
            ids = filter(None, map(lambda x: x[0], cursor.fetchall()))
            if not level:
                return False
            level -= 1
        return True

    _columns = {
        'name': fields.char(
            'Nombre Categoría', required=True, size=64, translate=True),
        'parent_id': fields.many2one(
            'giscegas.polissa.category', 'Categoría Padre', select=True),
        'complete_name': fields.function(
            _name_get_fnc, method=True, type="char", string='Nombre completo'),
        'child_ids': fields.one2many(
            'giscegas.polissa.category', 'parent_id', 'Categorías Hijo'
        ),
        'active': fields.boolean(
            'Activa', help=u"El campo activo permite esconder la categoria "
                           u"sin borrarla."
        ),
        'code': fields.char('Código', size=16),
    }
    _constraints = [
        (
            _check_recursion,
            _(u'Error ! No puedes crear categorías recursivas.'),
            ['parent_id']
        )
    ]
    _defaults = {
        'active': lambda *a: 1,
    }
    _order = 'parent_id,name'


GiscegasPolissaCategory()


class GiscegasPolissa(osv.osv):
    """ Add categories
    """
    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def search(self, cursor, user, args, offset=0, limit=None, order=None,
               context=None, count=False):

        def child_browse(c_id):
            contract_category_obj = self.pool.get('giscegas.polissa.category')
            search_params = [('parent_id', '=', c_id)]
            c_ids = contract_category_obj.search(
                cursor, user, search_params, context={'active_test': False})
            match = []
            if c_ids:
                for c_id in c_ids:
                    match.append(c_id)
                    match += child_browse(c_id)
            return match

        for idx, arg in enumerate(args):
            if len(arg) == 3:
                field, operator, match = arg
                if field == 'category_id' and isinstance(match, (unicode, str)):
                    contract_category_obj = self.pool.get(
                        'giscegas.polissa.category'
                    )

                    search_params = [('name', operator, match)]
                    c_ids = contract_category_obj.search(
                        cursor, user, search_params,
                        context={'active_test': False}
                    )

                    match = []
                    for c_id in c_ids:
                        match.append(c_id)
                        match += child_browse(c_id)

                    if match:
                        operator = 'in'
                        args[idx] = (field, operator, list(set(match)))

        return super(GiscegasPolissa, self).search(
            cursor, user, args, offset, limit, order, context, count
        )

    _columns = {
        'category_id': fields.many2many(
            'giscegas.polissa.category', 'giscegas_polissa_category_rel',
            'polissa_id', 'category_id', 'Categorías', select=True
        )
    }


GiscegasPolissa()
