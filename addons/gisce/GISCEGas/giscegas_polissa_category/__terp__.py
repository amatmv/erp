# -*- coding: utf-8 -*-
{
    "name": "Categories per contractes",
    "description": """
	Categories per contractes
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGas",
    "depends": [
        "base",
        "giscegas_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscegas_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
