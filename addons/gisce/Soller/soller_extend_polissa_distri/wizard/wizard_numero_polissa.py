# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from soller_extend_polissa.registre_polissa import GET_STATES_REGISTRE

class WizardNumeroPolissa(osv.osv_memory):
    
    _name = 'wizard.numero.polissa'

    def action_search(self, cr, uid, ids, context=None):
        '''Search for repeated values and warn if true'''

        registre_obj = self.pool.get('registre.polissa')
        polissa_obj = self.pool.get('giscedata.polissa')
        wizard = self.browse(cr, uid, ids[0])

        polissa_ids = context.get('active_ids', [])

        tmp_notes = ''
        error = False
        for polissa in polissa_obj.browse(cr, uid, polissa_ids):
            if wizard.action != 'add':
                continue
            num_pol = ''
            #Search for already assigned numbers
            search_params = [('polissa_id', '=', polissa.id),
                             ('state', '=', 'add')]
            registre_ids = registre_obj.search(cr, uid, search_params)
            assigned = registre_obj.read(cr, uid, registre_ids, ['name'])
            num_pol = ', '.join([x['name'] for x in assigned if x['name'] != polissa.codi])
            if num_pol:
                num_pol += ', %s' % polissa.codi
            else:
                num_pol = polissa.codi 
            if num_pol:
                tmp_notes += u'L\'abonat %s ja té el número de pòlissa '\
                             u'%s assignat\n' % (polissa.name, num_pol)
                error = True

        if not tmp_notes:
            tmp_notes = u'Totes les comprovacions han sigut correctes'

        if error:
            wizard.write({'notes': tmp_notes,
                          'state': 'error'})
        else:
            wizard.write({'notes': tmp_notes,
                          'state': 'end'})
        return True
    
    def action_polissa(self, cr, uid, ids, context=None):
            
        polissa_obj = self.pool.get('giscedata.polissa')
        wizard = self.browse(cr, uid, ids[0])
        
        polissa_ids = context.get('active_ids',[])

        for polissa in polissa_obj.browse(cr, uid, polissa_ids):
            polissa.assign_codi(mode=wizard.action, context=context)

        return {}

    
    _columns = {
        'action': fields.selection(GET_STATES_REGISTRE, 'Acció',
                                   required=True),
        'notes': fields.text('Notes'),
        'state': fields.selection([('init', 'Init'),
                                   ('error', 'Error'),
                                   ('end', 'End')],
                                  'Estat', select=True,
                                  readonly=True)
    }

    _defaults = {
        'state': lambda *a: 'init',
    }
WizardNumeroPolissa()
