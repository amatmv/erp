# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time
from giscedata_comerdist import OOOPPool, Sync

class RegistrePolissaGiscedataDistri(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def get_codi(self, cr, uid, context={}):
        '''funcio per obtindre un número de polissa (codi)
        de forma automatica'''

        return self.pool.get('ir.sequence').get(cr, uid,
                                                'soller.giscedata.polissa')

    def assign_codi(self, cr, uid, ids, mode='add', context={}):
        '''funcio per assignar o llevar un numero de polissa'''
        
        if mode == 'add':
            #Hem d'assignar un numero de polissa(codi)
            for polissa in self.browse(cr, uid, ids):
                numpol = polissa.get_codi()
                polissa.write({'codi': numpol})
                #Actualitzem el llibre registre
                polissa.update_registre(numpol, mode=mode)
        elif mode == 'del':
            #Hem de llevar el numero de polissa(codi)
            #Mirar si tenim contracte creat a comer
            for polissa in self.browse(cr, uid, ids):
                #Si no te numero de polissa(codi), ja no hem de fer res
                if not polissa.codi:
                    continue
                if polissa.must_be_synched(context):
                    config_id = polissa.get_config().id
                    ooop = OOOPPool.get_ooop(cr, uid, config_id)
                    sync = Sync(cr, uid, ooop, 'giscedata.polissa', config=config_id)
                    obj = sync.get_object(polissa.id)
                    if obj:
                        #Si trobem la polissa a comercialitzadora
                        #l'esborrem
                        if obj.state == 'esborrany':
                            obj.unlink()
                        else:
                            raise osv.except_osv('Error',
                                  'El contracte de comercialitzadora\
                                   no es pot esborrar')
                #Actualitzem el registre
                polissa.update_registre(polissa.codi, mode=mode)
                #Esborrem el codi                
                polissa.write({'comercialitzadora': None,
                               'codi': None})
                        
        return True

RegistrePolissaGiscedataDistri()              
