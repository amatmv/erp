# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields


class GiscedataPolissaSollerExtend(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy(self, cr, uid, id, default=None, context=None):
        """Canviem la funcionalitat del copy() per adaptar-ho a la pòlissa de Soller.
           Camps especifics de distribucio"""

        default.update({'comercialitzadora': False,
                        'talls': [],
                        'codi': False,
                        'ref_comer': False,
                       })
        
        res_id = super(GiscedataPolissaSollerExtend, self).copy(cr, uid, id, default,
                                                                context)
        return res_id

    def _get_uid(self, cr, uid, ids, name, arg, context=None):
        res={}
        for id in ids:
            res[id]=uid
        return res

    _columns = {
        'user_id': fields.function(_get_uid, method=True, type='integer',
                                   string='Usuari', store=False,
                                   readonly=True),
        'comer_actua': fields.selection([('S', 'Substitut'),
                                         ('M', 'Mandatari')],
                                        'Actua com',
                                        readonly=True,
                                        states={
                                        'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', True)],
                                        'modcontractual': [('readonly', False),
                                                           ('required', True)]
                                    }),
        
    }

    _defaults = {
        'comer_actua': lambda *a: 'S',
    }

    def _check_comercialitzadora(self, cr, uid, ids, context={}):
        '''funcio que retorna un error quan posem comercialitzadora
        i no hem posat data de firma de contracte o no tenim num polissa
        Evita errors al posar la comercialitzadora quan no toca'''

        for polissa in self.browse(cr, uid, ids):
            if polissa.comercialitzadora:
                if not polissa.data_firma_contracte\
                   or not polissa.codi:
                    return False
            
        return True            
    
    _constraints = [
        (_check_comercialitzadora, 
         'Error: No es pot assignar comercialitzadora sense data de firma de contracte o Num. pòlissa', 
         ['comercialitzador','data_firma_contracte']),
    ]
     

GiscedataPolissaSollerExtend()


class GiscedataPolissaModContractual(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'comer_actua': fields.selection([('S', 'Substitut'),
                                         ('M', 'Mandatari')],
                                        'Actua com'),
    }

    _defaults = {
        'comer_actua': lambda *a: 'S',
    }

GiscedataPolissaModContractual()

