# -*- coding: utf-8 -*-

import jasper_reports

def mandato(cr, uid, ids, data, context):
    return {
        'ids': ids,
        'parameters': {'REPORT_LOCALE': 'es_ES'},
    }
    
jasper_reports.report_jasper(
   'report.report_mandato',
   'payment.mandate',
   parser=mandato
)