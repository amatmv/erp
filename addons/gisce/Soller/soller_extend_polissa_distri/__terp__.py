# -*- coding: utf-8 -*-
{
    "name": "Soller Extend Polissa",
    "description": """
Funcionalitats extra per polissa de distribució
    * Adaptació del copy de la pòlissa als camps específics de distribució
    * Afegir el camp user_id
    * Report de cédula i butlletí
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_polissa_distri",
        "giscedata_facturacio_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_extend_polissa_distri_seq.xml",
        "soller_extend_polissa_distri_view.xml",
        "soller_extend_polissa_distri_report.xml",
        "wizard/wizard_numero_polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
