# -*- coding: utf-8 -*-

import jasper_reports



def carta_cortado(cr, uid, ids, data, context):
    return {
        'ids': ids,
        'parameters': {'REPORT_LOCALE': 'es_ES'},
    }

jasper_reports.report_jasper(
   'report.report_orden_carta_cortado',
   'crm.case',
   parser=carta_cortado
)

jasper_reports.report_jasper(
   'report.report_listado_orden_corte',
   'crm.case',
   parser=carta_cortado
)