# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardCRMTall(osv.osv_memory):

    """Wizard para pasar a estado cortado las polizas
    asociadas a los casos en ordenes de corte"""

    _name = "wizard.crm.tall"

    def tallar(self, cursor, uid, ids, context=None):
        
        case_obj = self.pool.get('crm.case')

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])
        case_ids = context.get('active_ids', [])

        res = case_obj.tallar_polissa(cursor, uid, case_ids,
                                      context=context)
        vals = {'state': 'end'}
        if res:
            vals.update({'notes': res})
            wizard.write(vals)
        else:
            mes = _(u"El procés ha finalitzat correctament")
            vals.update({'notes': mes})

        wizard.write(vals)
        return True        
        
    _columns = {
        'notes': fields.text('Resultat', readonly=True),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')], 'Estat'),
                
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardCRMTall()
