# -*- coding: utf-8 -*-
{
    "name": "Extensió CRM Soller distribució",
    "description": """
  * Report de carta de tall
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "crm_generic",
        "giscedata_polissa_crm",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_crm_dist_report.xml",
        "wizard/wizard_crm_tallar.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
