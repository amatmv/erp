# -*- coding: utf-8 -*-

from osv import osv, fields
import netsvc
from tools.translate import _
import pooler

class CrmCaseDist(osv.osv):

    _name = 'crm.case'
    _inherit = 'crm.case'

    def tallar_polissa(self, cursor, uid, ids, context=None):
        '''passa a estat tall les polisses associades als casos'''

        wf_service = netsvc.LocalService('workflow')

        db = pooler.get_db_only(cursor.dbname)
        errors = ''
        for case_id in ids:
            case = self.browse(cursor, uid, case_id)
            polissa = case.polissa_id
            if polissa.state not in ('impagament', 'activa'):
                errors += (_(u"No es pot tallar la pòlissa %s\n")
                           % polissa.name)
            if polissa.state == 'activa':
                wf_service.trg_validate(uid, 'giscedata.polissa', polissa.id,
                                    'impagament', cursor)
                wf_service.trg_validate(uid, 'giscedata.polissa', polissa.id,
                                    'tallar', cursor)
            elif polissa.state == 'impagament':
                wf_service.trg_validate(uid, 'giscedata.polissa', polissa.id,
                                    'tallar', cursor)
        return errors

CrmCaseDist()
