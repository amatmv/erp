# -*- coding: utf-8 -*-

import jasper_reports
import pooler
from datetime import datetime

def certificado(cr, uid, ids, data, context):

    pool = pooler.get_pool(cr.dbname)

    relacion_obj = pool.get('acciones.relacion.titular')
    intervalo_obj = pool.get('acciones.intervalo')
    titular_obj = pool.get('acciones.titular')
    titular_ids = data['form']['ids']

    titulares = []
    titulares.extend(titular_ids)
    for titular in titular_obj.browse(cr, uid, titular_ids):
        if titular.grupo:
            continue
        #search_params = [('titular_id','=',titular.id),]
        #relacion_ids = relacion_obj.search(cr, uid, search_params)
        #for relacion in relacion_obj.browse(cr, uid, relacion_ids):
        for relacion in titular.relacion_titular_ids:
            titulares.append(relacion.titular_asociado_id.id)
    #eliminamos los duplicados
    titulares = list(set(titulares))
    #eliminamos aquellos que no tienen acciones 
    #compartidas si son un grupo
    #no compartidas si son individuales
    for titular in titular_obj.browse(cr, uid, titulares):
        search_params = [('titular_id','=',titular.id),
                         ('tipo_relacion_id.shared','=',titular.grupo),]
        intervalo_ids = intervalo_obj.search(cr, uid, search_params)
        if not intervalo_ids:
            titulares.remove(titular.id)
       
    return {'ids': titulares,
            'parameters': {'fecha_certificado':datetime.strptime(data['form']['fecha_certificado'],'%Y-%m-%d'),
                           'nominal': data['form']['nominal'],
                          },
           }

jasper_reports.report_jasper(
   'report.report_acciones_titular_certificado',
   'acciones.titular',
   parser=certificado
)
