# -*- coding: utf-8 -*-
{
    "name": "Soller Accionariado",
    "description": """
Gestión del accionariado
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "base_vat",
        "account",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "acciones_sequence.xml",
        "security/acciones_security.xml",
        "acciones_data.xml",
        "acciones_view.xml",
        "movimientos_view.xml",
        "liquidaciones_view.xml",
        "libro_registro_view.xml",
        "acciones_report.xml",
        "wizard/wizard_importar_acciones_view.xml",
        "wizard/wizard_crear_acciones_view.xml",
        "wizard/wizard_mostrar_totales_view.xml",
        "wizard/wizard_renombrar_acciones_view.xml",
        "wizard/wizard_ligar_acciones_view.xml",
        "wizard/wizard_generar_libro_view.xml",
        "wizard/wizard_imprimir_certificado_view.xml",
        "wizard/wizard_liquidar_view.xml",
        "wizard/wizard_importar_cupones_view.xml",
        "wizard/wizard_titular_movimiento_view.xml",
        "wizard/wizard_mostrar_factures_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
