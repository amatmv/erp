# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time
from itertools import *

class AccionesMovimiento(osv.osv):
    '''
    Movimiento de las acciones
    '''
    _name = 'acciones.movimiento'
    _description = 'Movimiento'

    _order = 'date desc'

    def create(self, cr, uid, vals, context={}):

        #damos un numero de secuencia 
        if not vals.get('name', False):
            #Si es interno le damos una secuencia diferente
            if vals.get('interno', False):
                vals['name'] = self.pool.get('ir.sequence').\
                                get(cr, uid, 'acciones.movimiento.interno')
            else:
                vals['name'] = self.pool.get('ir.sequence').\
                                get(cr, uid, 'acciones.movimiento')
        return super(AccionesMovimiento, self).create(cr, uid, vals, context)

    def action_borrador(self, cr, uid, ids, context={}):
        '''pasar a borrador un movimiento'''
        
        return self.write(cr, uid, ids, {'state': 'borrador'})

    def action_validar(self, cr, uid, ids, context={}):
        '''validar un movimiento'''
        
        for movimiento in self.browse(cr,uid, ids):
            for linea in movimiento.linea_ids:
                linea.validar_linea()

        return self.write(cr, uid, ids, {'state': 'validar'})

    def action_confirmar(self, cr, uid, ids, context={}):
        '''confirmar un movimiento
        Se tienen que realizar todos los movimientos necesarios'''
        
        #Agregamos al contexto la fecha del movimiento
        #para propagarlo al generar el historico de relaciones
        #y al crear las relaciones nuevas
        
        for movimiento in self.browse(cr, uid, ids):
            for linea in movimiento.linea_ids:
                linea.confirmar_linea(context={'fecha': movimiento.date})
            movimiento.write({'state': 'confirmado'})
        return True

    _columns = {
        'name':fields.char('Nº Movimiento', size=64, readonly=True),
        'date': fields.date('Fecha', required=True,
                            states={'validar':[('readonly', True)],
                                        'confirmado':[('readonly', True)],
                                       },),   
        'state':fields.selection([('borrador','Borrador'),
                                  ('validar','Validar'),
                                  ('confirmado','Confirmado'),],
                                 'Estado', select=True, readonly=True),
        'interno':fields.boolean('Interno', required=False,
                                 states={'validar':[('readonly', True)],
                                        'confirmado':[('readonly', True)],
                                        },),        
        
    }
    _defaults = {
        'state': lambda *a: 'borrador',
        'interno': lambda *a: False,
        
    }

    
AccionesMovimiento()


class AccionesMovimientoLinea(osv.osv):
    '''
    Lineas de los movimientos de las acciones
    '''

    _name = 'acciones.movimiento.linea'
    _description = 'Línea'
    _rec_name = 'movimiento_id'

    def crear_linea(self, cr, uid, movimiento,
                    titular_origen, tipo_origen, 
                    acciones, auto=False,
                    titular_destino=None, tipo_destino=None,
                    context={}):
        '''funcion para crear una linea de movimiento'''

        intervalo_obj = self.pool.get('acciones.movimiento.intervalo')
        #sino nos pasan titular_destino, igual al origen
        if not titular_destino:
            titular_destino = titular_origen
        
        vals = {'movimiento_id': movimiento,
                'titular_origen_id': titular_origen,
                'titular_destino_id': titular_destino,
                'relacion_origen_id': tipo_origen,
                'relacion_destino_id': tipo_destino,
                'auto': auto,
               }
        #Si ya existe una linea con estos mismos valores,
        #no la volvemos a crear
        search_params = [('movimiento_id','=',movimiento),
                         ('titular_origen_id','=', titular_origen),
                         ('titular_destino_id','=', titular_destino),
                         ('relacion_origen_id','=', tipo_origen),
                         ('relacion_destino_id','=', tipo_destino),
                         ('auto','=', auto),]
        if self.search_count(cr, uid, search_params):
            return False
        linea_id = self.create(cr, uid, vals)
        intervalo_obj.crear_intervalos(cr, uid, linea_id, 
                                        tipo_origen, acciones)

        return linea_id

    def validar_linea(self, cr, uid, ids, context={}):
        '''validar la linea del movimiento'''

        for linea in self.browse(cr, uid, ids):
            if linea.auto:
                continue
            '''Sino hay relaciones de origen ni de destino
            la linea no es valida'''
            if not linea.relacion_origen_id.id and not linea.relacion_destino_id.id:
                raise osv.except_osv('Error al validar',
                            'Error validando la linea del titular %s.\n\
                             No tiene tipo origen ni destino.'\
                             %(linea.titular_origen_id.name))
            
        return True
    
    def confirmar_linea(self, cr, uid, ids, context={}):
        '''realiza los movimientos de las acciones 
        dentro de los intervalos'''

        relacion_obj = self.pool.get('acciones.relacion')

        acciones = []
        for linea in self.browse(cr, uid, ids):
            #sacamos todas las acciones implicadas en la linea
            for intervalo in linea.intervalo_ids:
                acciones.extend(intervalo.get_acciones())
            #Movemos al historico todas las relaciones de estas
            #acciones del titular origen
            relacion_obj.mover_historico(cr, uid, 
                        linea.titular_origen_id.id,
                        acciones, context=context)
            #creamos todas las relaciones nuevas
            #solo si tenemos un valor en relacion_destino
            if linea.relacion_destino_id.id:
                relacion_obj.crear_relacion(cr, uid,
                            linea.titular_destino_id.id,
                            linea.relacion_destino_id.id,
                            acciones, context=context)
            #Rehacemos los intervalos de los titulares 
            #origen y destino
            linea.titular_origen_id.generar_intervalos()
            #solo regeneramos el intervalo destino 
            #si los titulares son distintos
            if linea.titular_origen_id.id != linea.titular_destino_id.id:
                linea.titular_destino_id.generar_intervalos()
        return True    

    def onchange_titular(self, cr, uid, ids, titular_id,
                                relacion_id, modo='origen', context={}):

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not titular_id:
            return res

        titular_obj = self.pool.get('acciones.titular')
        tipo_relacion_obj = self.pool.get('acciones.tipo.relacion')
    
        if titular_obj.browse(cr, uid, titular_id).grupo:
            #Si el titular es un grupo, solo se permiten 
            #tipos de relaciones shared
            '''res['domain'].update({'relacion_%s_id'%(modo):
                                    [('shared','=', True),],
                                 })'''
            #Si hemos ya hemos introducido un tipo y no es shared, lo borramos
            if relacion_id:
                if not tipo_relacion_obj.browse(cr, uid, relacion_id).shared:
                    #res['value'].update({'relacion_%s_id'%(modo): None})
                    res['warning'].update({'title': 'Error',
                    'message': 'No se pueden asociar relaciones individuales a titulares grupo',})
        else:
            #Si el titular no es un grupo, solo se permiten
            #tipos de relaciones no shared
            '''res['domain'].update({'relacion_%s_id'%(modo):
                                    [('shared','=',False),],
                                 })'''
            #Si hemos ya hemos introducido un tipo y es shared, lo borramos
            if relacion_id:
                if tipo_relacion_obj.browse(cr, uid, relacion_id).shared:
                    #res['value'].update({'relacion_%s_id'%(modo): None})
                    res['warning'].update({'title': 'Error',
                    'message': 'No se pueden asociar relaciones compartidas a titulares individuales',})
        return res 

    def onchange_relacion(self, cr, uid, ids, titular_id,
                                relacion_id, modo='origen', context={}):

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not relacion_id:
            res['domain'].update({'titular_%s_id'%(modo): []})
            return res

        titular_obj = self.pool.get('acciones.titular')
        tipo_relacion_obj = self.pool.get('acciones.tipo.relacion')

        if tipo_relacion_obj.browse(cr, uid, relacion_id).shared:
            #Si la relacion es shared, solo se permiten 
            #titulares grupo
            '''res['domain'].update({'titular_%s_id'%(modo):
                                    [('grupo','=', True),],
                                 })'''
            #Si hemos ya hemos introducido un tipo y no es shared, lo borramos
            if titular_id:
                if not titular_obj.browse(cr, uid, titular_id).grupo:
                    #res['value'].update({'titular_%s_id'%(modo): None})
                    res['warning'].update({'title': 'Error',
                    'message': 'No se pueden asociar titulares individuales a relaciones compartidas',})
        else:
            #Si la relacion no es shared, solo se permiten
            #titulares grupo
            '''res['domain'].update({'titular_%s_id'%(modo):
                                    [('grupo','=', False),],
                                 })'''
            #Si ya hemos introducido un titular y es grupo, lo borramos
            if titular_id:
                if titular_obj.browse(cr, uid, titular_id).grupo:
                    #res['value'].update({'titular_%s_id'%(modo): None})
                    res['warning'].update({'title': 'Error',
                    'message': 'No se pueden asociar titulares grupo a relaciones individuales',})
        return res             

    _columns = {
        'movimiento_id':fields.many2one('acciones.movimiento', 'Movimiento',
                                         required=True, ondelete='cascade'),
        'titular_origen_id':fields.many2one('acciones.titular', 
                                           'Titular origen', required=True),
        'titular_destino_id':fields.many2one('acciones.titular', 
                                           'Titular destino', required=True),
        'relacion_origen_id':fields.many2one('acciones.tipo.relacion', 
                                   'Tipo origen', required=False,),
        'relacion_destino_id':fields.many2one('acciones.tipo.relacion', 
                                           'Tipo destino', required=False),
        'observaciones': fields.text('Observaciones'),
        'auto':fields.boolean('Automática', required=False, readonly=True),
           
    }

    _defaults = {
        'auto': lambda *a: False,
        
    }
   
AccionesMovimientoLinea()

class AccionesMovimiento2(osv.osv):

    _name = 'acciones.movimiento'
    _inherit = 'acciones.movimiento'

    def _comprueba_duplicados(self, cr, uid, ids, context={}):
        '''constraint que comprueba que no hay 
        acciones duplicadas en el movimiento'''

        for movimiento in self.browse(cr, uid, ids):
            acciones = []
            for linea in movimiento.linea_ids:
                for intervalo in linea.intervalo_ids:
                    acciones.extend([x for x in\
                                    range(intervalo.accion_desde, 
                                          intervalo.accion_hasta+1)])
            if len(acciones) <> len(set(acciones)):
                return False
            
        return True

    _columns = {
        'linea_ids':fields.one2many('acciones.movimiento.linea', 
                                'movimiento_id', 'Lineas', required=False,
                                states={'validar':[('readonly', True)],
                                        'confirmado':[('readonly', True)],
                                       },),
        
    }

    _constraints = [
        (_comprueba_duplicados, 
         'Existen acciones duplicadas en el movimiento!', 
         ['linea_ids']),
    ]

AccionesMovimiento2()

class AccionesMovimientoIntervalos(osv.osv):

    _name = 'acciones.movimiento.intervalo'
    _description = 'Intervalos'

    def crear_intervalos(self, cr, uid, linea_id, 
                            tipo_id, acciones, context={}):
        '''funcion para crear los intervalos de una linea'''

        for intervalo in self.generar_intervalos(cr, uid, acciones):
            values = {'linea_id': linea_id,
                      'tipo_relacion_id': tipo_id,
                      'accion_desde': intervalo['accion_desde'],
                      'accion_hasta': intervalo['accion_hasta'],}
            self.create(cr, uid, values)

        return True

    def generar_intervalos(self, cr, uid, acciones, context={}):
        '''funcion que recibe una lista de acciones y devuelve
        una lista con los intervalos correspondientes'''

        res = []

        #Buscamos las posiciones en las que las acciones no son consecutivas
        pos = [x for x in ifilter(lambda x: acciones[x]+1 <> acciones[x+1],\
                                             range(len(acciones)-1))]
        pos2 = []
        #A cada posicion encontrada le agregamos la posicion siguiente
        for i in pos:
            pos2.extend([i,i+1])
        #Agregamos la primera y ultima posicion para cerrar los intervalos
        pos2.insert(0,0)
        pos2.append(len(acciones)-1)

        #Para cada intervalo de dos posiciones, 
        #generamos el intervalo de acciones correspondiente
        for i in range(0,len(pos2)-1,2):
            res.append({'accion_desde': acciones[pos2[i]],
                    'accion_hasta': acciones[pos2[i+1]],})
        return res

    def get_acciones(self, cr, uid, ids, context={}):
        '''devuelve una lista con los numeros de las acciones de un intervalo'''

        intervalo = self.browse(cr, uid, ids[0])
        acciones = [str(x).zfill(7) for x in\
                           range(intervalo.accion_desde, 
                                 intervalo.accion_hasta+1)]
        return acciones
    
    def copiar_intervalos_titular(self, cr, uid, titular_id, 
                        tipo_id, linea_id, context={}):
        '''copiamos los intervalos del titular a la linea'''

        int_tit_obj = self.pool.get('acciones.intervalo')
    
        #Buscamos los intervalos del titular
        search_params = [('titular_id','=', titular_id),
                         ('tipo_relacion_id','=', tipo_id),]
        int_tit_ids = int_tit_obj.search(cr, uid, search_params)
        #Para cada relacion encontrada, la copiamos
        for int_titular in int_tit_obj.browse(cr, uid, int_tit_ids):
            vals = {'linea_id': linea_id,
                    'accion_desde': int_titular.accion_desde,
                    'accion_hasta': int_titular.accion_hasta,
                    'total': int_titular.accion_hasta - int_titular.accion_desde + 1,
                    'tipo_relacion_id': tipo_id,
                    }
            self.create(cr, uid, vals)

        return True

    def onchange_rango(self, cr, uid, ids, accion_desde,
                       accion_hasta, context={}):

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not accion_desde or not accion_hasta:
            res['value'].update({'total': 0})
            return res

        if accion_hasta < accion_desde:
            res['warning'].update({'title': 'Error',
                    'message': 'El numero de acciones del intervalo debe ser positivo',})
        else:
            res['value'].update({'total': accion_hasta - accion_desde + 1})

        return res    
        

    def _acciones_total(self, cr, uid, ids, name, arg, context=None):

        res = {}
        for intervalo in self.browse(cr, uid, ids):
            res[intervalo.id] = (intervalo.accion_hasta - intervalo.accion_desde) + 1
        return res

    _columns = {
        'linea_id':fields.many2one('acciones.movimiento.linea', 
                                   'Linea', required=False, ondelete='cascade'),        
        'accion_desde': fields.integer('Desde', required=True),
        'accion_hasta': fields.integer('Hasta', required=True),
        'tipo_relacion_id':fields.many2one('acciones.tipo.relacion', 
                                           'Relación', required=True),
        'total': fields.integer('Total', required=True),
        #'total': fields.function(_acciones_total, method=True, readonly=True,
        #                         type='integer', string='Total', store=False),
    }

    _defaults = {
        'total': lambda *a: 0,
        
    }

    _sql_constraints = [
        ('total_positivo', 'CHECK (total > 0)', 
         'El numero de acciones del intervalo debe ser positivo'),
        ('total_comprobar', 'CHECK (total = accion_hasta - accion_desde + 1)', 
         'El total no se ha calculado correctamente'),
        
    ]
AccionesMovimientoIntervalos()

class AccionesMovimientoLinea2(osv.osv):

    _name = 'acciones.movimiento.linea'
    _inherit = 'acciones.movimiento.linea'

    def borrar_intervalos(self, cr, uid, ids, context={}):
        '''funcion para borrar todos los intervalos 
        de las lineas que nos pasan en ids'''

        intervalo_obj = self.pool.get('acciones.movimiento.intervalo')

        search_params = [('linea_id','in', ids)]
        intervalo_ids = intervalo_obj.search(cr, uid, search_params)
        return intervalo_obj.unlink(cr, uid, intervalo_ids)

    def copiar_intervalos(self, cr, uid, ids, context={}):
        '''funcion para copiar los intervalos
        del titular origen a la linea de movimiento'''

        intervalo_obj = self.pool.get('acciones.movimiento.intervalo')

        #Para cada linea
        for linea in self.browse(cr, uid, ids):
            #borramos los intervalos anteriores
            linea.borrar_intervalos()
            #Copiamos todos los intervalos del titular
            #segun el tipo de relacion seleccionado
            intervalo_obj.copiar_intervalos_titular(cr, uid, 
                                                    linea.titular_origen_id.id,
                                                    linea.relacion_origen_id.id,
                                                    linea.id)
        return True

    def _linea_total(self, cr, uid, ids, name, arg, context=None):

        res = {}
        for linea in self.browse(cr, uid, ids):
            res[linea.id] = sum([inter.total for inter in linea.intervalo_ids])
        return res

    def _get_linea_from_intervalo(self, cr, uid, ids, context=None):
        '''funcion que devuelve los ids de las lineas 
        asociadas a los intervalos que nos pasan'''

        intervalo_obj = self.pool.get('acciones.movimiento.intervalo')
        
        vals = intervalo_obj.read(cr, uid, ids, ['linea_id'], context)
        return [x['linea_id'][0] for x in vals]
            

    _columns = {
        'intervalo_ids':fields.one2many('acciones.movimiento.intervalo', 
                                    'linea_id', 'Intervalos', required=False),
        'total': fields.function(_linea_total, method=True, readonly=True,
                 type='integer', string='Total', 
                 store={'acciones.movimiento.intervalo':\
                        (_get_linea_from_intervalo, 
                         ['accion_desde','accion_hasta'], 20),
                        'acciones.movimiento.linea':\
                        (lambda self, cr, uid, ids, c={}: ids, 
                         [], 20)
                       },)
            
    }

    def _comprueba_intervalos(self, cr, uid, ids, context={}):
        '''constraint que comprueba que el intervalo que se crea
        este dentro de los limites del titular'''

        relacion_obj = self.pool.get('acciones.relacion')

        #Para cada linea
        for linea in self.browse(cr, uid, ids):
            #Si la linea es automatica, no la comprobamos
            if linea.auto:
                continue
            for intervalo in linea.intervalo_ids:
                #generamos una lista 
                #con los nombres de las acciones del intervalo
                acciones = intervalo.get_acciones()
                #contamos todas las relaciones asociadas
                search_params = [('titular_id','=',linea.titular_origen_id.id),
                             ('tipo_relacion_id','=',linea.relacion_origen_id.id),
                             ('accion_id.name','in', acciones), ]
                cuenta_relaciones = relacion_obj.search(cr, uid, search_params,
                                          offset=0, limit=None, 
                                          order=None, context=None, count=True)
                if len(acciones) <> cuenta_relaciones:
                    return False
        return True

    
    _constraints = [
        (_comprueba_intervalos, 
         'Intervalo de acciones fuera del rango del cliente !', 
         ['intervalo_ids']),
    ]
                
AccionesMovimientoLinea2()
