# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time

class AccionesLibroRegistro(osv.osv):

    _name = 'acciones.libro'
    _description = 'Libro registro'

    
    _columns = {
        'name': fields.date('Fecha de creación'),
        'notas': fields.char('Notas', size=250),
        'valor_teorico': fields.float('Valor teórico', digits=(16,2))        
    }

    _defaults = {
        'name': lambda *a: time.strftime('%Y-%m-%d'),
                
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)', '¡No puede haber dos libros con la misma fecha!'),
        
    ]
AccionesLibroRegistro()


class AccionesLibroRegistroLinea(osv.osv):
    
    _name = 'acciones.libro.linea'
    _description = 'Linea libro registro'

    _order = 'inscrito desc, id'

    def enlibro(self, cr, uid, titular_id, libro_id, context={}):
        '''Devuelve el id si ya existe una linea en el libro
        con el mismo origen_id'''

        search_params = [('origen_id','=', titular_id),
                         ('libro_id','=',libro_id),]
        linea_ids = self.search(cr, uid, search_params)
        if linea_ids:
            return linea_ids
        else:
            return False
    
    _columns = {
        'name': fields.date('Fecha de creación', readonly=True,),
        'libro_id':fields.many2one('acciones.libro', 'Libro', 
                                   required=True, ondelete='cascade',),
        #Indica el titular individual o grupo referido por esta linea
        'origen_id':fields.many2one('acciones.titular', 'Titular', 
                                   required=True),
        'inscrito': fields.boolean('Inscrito', required=True, readonly=True,),
        
        
    }

    _defaults = {
        'name': lambda *a: time.strftime('%Y-%m-%d'),
        'inscrito': lambda *a: True,
        
    }
AccionesLibroRegistroLinea()

class AccionesLibroRegistro2(osv.osv):

    _name = 'acciones.libro'
    _inherit = 'acciones.libro'

    _columns = {
        'linea_ids':fields.one2many('acciones.libro.linea', 'libro_id', 'Líneas', required=False),
        
    }

    def _comprueba_duplicados(self, cr, uid, ids, context={}):
        '''constraint que comprueba que no hay 
        acciones duplicadas en el libro'''

        for libro in self.browse(cr, uid, ids):
            acciones = []
            for linea in libro.linea_ids:
                for intervalo in linea.intervalo_ids:
                    acciones.extend([x for x in\
                                    range(intervalo.accion_desde, 
                                          intervalo.accion_hasta+1)])
            if len(acciones) <> len(set(acciones)):
                return False
            
        return True

    _constraints = [
        (_comprueba_duplicados, 
         'Existen acciones duplicadas en el libro!', 
         ['linea_ids']),
    ]

AccionesLibroRegistro2()

class AccionesLibroRelacion(osv.osv):

    _name = 'acciones.libro.relacion'
    _rec_name = 'id'

    _order = 'cabecera desc, tipo_relacion_id'

    _columns = {
        'cabecera':fields.boolean('Cabecera', required=False),
        'titular_id':fields.many2one('acciones.titular', 'Titular', 
                                     required=True, ondelete='cascade'),
        'titular_id_grupo': fields.related('titular_id','grupo', 
                                type='boolean', relation='acciones.titular', 
                                string='Grupo', store=False),
        'partner_vat': fields.related('titular_id','partner_id','vat', type='char', 
                                      relation='res.partner', 
                                      string='NIF/CIF', store=False,
                                      readonly=True),
        'partner_country_id': fields.many2one('res.country', 'Nacionalidad', required=True),
        'fiscal_address_id': fields.many2one('res.partner.address', 'Dirección fiscal', required=True),
        'postal_address_id': fields.many2one('res.partner.address', 'Dirección postal', required=True),
        'bank_id':fields.many2one('res.partner.bank', 'CCC', required=False),
        'tipo_relacion_id':fields.many2one('acciones.tipo.relacion', 'Tipo de relacion', required=True),
        'linea_id':fields.many2one('acciones.libro.linea', 'Linea', 
                                   required=True, ondelete='cascade'),
        
    }
    
    _defaults = {
        'cabecera': lambda *a: False,
        
    }

AccionesLibroRelacion()

class AccionesLibroIntervalo(osv.osv):

    _name = 'acciones.libro.intervalo'

    def _acciones_total(self, cr, uid, ids, name, arg, context=None):

        res = {}
        for intervalo in self.browse(cr, uid, ids):
            res[intervalo.id] = (intervalo.accion_hasta - intervalo.accion_desde) + 1
        return res

    _columns = {
        'linea_id':fields.many2one('acciones.libro.linea', 'Linea', 
                                   required=False, ondelete='cascade'),        
        'accion_desde': fields.integer('Desde', required=True),
        'accion_hasta': fields.integer('Hasta', required=True),
        'total': fields.function(_acciones_total, method=True, readonly=True,
                                 type='integer', string='Total', 
                                 store={'acciones.libro.intervalo': (lambda self, cr, uid, ids, c={}: ids, ['accion_desde','accion_hasta'], 20),
                                    }),
    }

AccionesLibroIntervalo()

class AccionesLibroRegistroLinea2(osv.osv):

    _name = 'acciones.libro.linea'
    _inherit = 'acciones.libro.linea'

    def _acciones_total(self, cr, uid, ids, name, arg, context=None):
        
        res = {}
        for linea in self.browse(cr, uid, ids):
            res[linea.id] = sum([intervalo.total for intervalo in linea.intervalo_ids])
        return res

    def _get_linea_from_intervalo(self, cr, uid, ids, context={}):
        
        intervalo_obj = self.pool.get('acciones.libro.intervalo')
        linea_ids = intervalo_obj.read(cr, uid, ids, ['linea_id'])

        return [x['linea_id'][0] for x in linea_ids]

    def _get_cabecera(self, cr, uid, ids, name, arg, context=None):
        '''Funcion que devuelve el titular de cabecera y su relacion'''

        relacion_obj = self.pool.get('acciones.libro.relacion')

        res = {}
        for linea_id in ids:
            search_params = [('linea_id','=',linea_id),
                             ('cabecera','=',True),]
            rel_cabecera_id = relacion_obj.search(cr, uid, search_params)
            if rel_cabecera_id:
                relacion = relacion_obj.browse(cr, uid, rel_cabecera_id[0])
                res[linea_id] = {'titular_cabecera': relacion.titular_id.name_get()[0][1],
                                 'relacion_cabecera': relacion.tipo_relacion_id.name_get()[0][1]}
            else:
                res[linea_id] = {'titular_cabecera': None,
                                 'relacion_cabecera': None}

        return res

    def _get_linea_from_relacion(self, cr, uid, ids, context={}):
        
        relacion_obj = self.pool.get('acciones.libro.relacion')
        linea_ids = relacion_obj.read(cr, uid, ids, ['linea_id'])

        return [x['linea_id'][0] for x in linea_ids]

    _columns = {
        'intervalo_ids':fields.one2many('acciones.libro.intervalo', 'linea_id', 'Intervalos', required=False),
        'relacion_ids':fields.one2many('acciones.libro.relacion', 'linea_id', 'Relaciones', required=False),
        'total': fields.function(_acciones_total, method=True, readonly=True,
                                 type='integer', string='Total', 
                                 store={'acciones.libro.linea': (lambda self, cr, uid, ids, c={}: ids, [], 20),
                                        'acciones.libro.intervalo': (_get_linea_from_intervalo, ['accion_desde','accion_hasta'], 20),
                                    }),
        'titular_cabecera': fields.function(_get_cabecera, method=True, readonly=True,
                                 type='char',size=128, string='Titular cabecera', 
                                 store={'acciones.libro.linea': (lambda self, cr, uid, ids, c={}: ids, ['relacion_ids'], 20),
                                        'acciones.libro.relacion': (_get_linea_from_relacion, ['cabecera'], 20),
                                    }, multi='cabecera'),
        'relacion_cabecera': fields.function(_get_cabecera, method=True, readonly=True,
                                 type='char',size=128, string='Relacion cabecera', 
                                 store={'acciones.libro.linea': (lambda self, cr, uid, ids, c={}: ids, ['relacion_ids'], 20),
                                        'acciones.libro.relacion': (_get_linea_from_relacion, ['cabecera'], 20),
                                    }, multi='cabecera'),

    }

AccionesLibroRegistroLinea2()
