# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time

class AccionesLiquidacionTipo(osv.osv):

    _name = 'acciones.liquidacion.tipo'

    '''Los impuestos, se calculan sobre el importe bruto.
    Quiere decir, que restan sobre el total a la hora de hacer las remesas.
    Para calcular impuestos restando del total, se puede utilizar la funcion
    del objeto account.tax compute(self, cr, uid, tax_ids, bruto, cantidad=1)'''

    _get_type = [('payable','A pagar'),
                 ('receivable','A cobrar')]

    _get_calc_type = [('value','Valor fijo'),
                      ('percent','Porcentaje')]

    def update_taxes(self, cr, uid, ids, context={}):
        '''Funcion para actualizar los impuestos de las liquidaciones
        pendientes asociadas a un tipo'''

        self.pool.get('ir.model.access').check(cr, uid, 
                                self._name, 'write', context=context)

        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')

        for tipo in self.browse(cr, uid, ids):
            #Actualizamos todas las facturas en borrador
            #con alguna linea del tipo asociado
            search_params = [('origin','=',tipo.journal_id.code),
                             ('invoice_id.state','=','draft'),]
            invoice_line_ids = invoice_line_obj.search(cr, uid, search_params)
            invoice_ids = [x['invoice_id'][0] for x in 
                        invoice_line_obj.read(cr, uid, invoice_line_ids, ['invoice_id'])]
            #Quitamos los posibles duplicados
            invoice_ids = list(set(invoice_ids))
            taxes_ids = [x.id for x in tipo.tax_ids]
            invoice_line_obj.write(cr, uid, invoice_line_ids, 
                {'invoice_line_tax_id': [(6,0,taxes_ids)],})
            invoice_obj.button_reset_taxes(cr, uid, invoice_ids)

        return True

    def calc_liquidacion(self, cr, uid, tipo_id, date, context= {}):
        '''Calcula el importe de una liquidacion 
        segun un numero de acciones'''

        serie_obj = self.pool.get('acciones.serie')
        nominal = serie_obj.get_nominal(cr, uid, date, context=context)
        
        if isinstance(tipo_id, (list,tuple)):
            tipo_id = tipo_id[0]
        tipo = self.browse(cr, uid, tipo_id)

        if tipo.calc_type == 'percent':
            res = round((nominal * tipo.calc_value/100),2)
        elif tipo.calc_type == 'value':
            res = tipo.calc_value

        return res

    def get_invoice_values(self, cr, uid, tipo_id, context={}):
        '''Devuelve los datos de factura 
        relativos a un tipo de liquidacion tales como 
        journal_id, impuestos, tipo de factura'''
    
        res = {}
        tipo = self.browse(cr, uid, tipo_id)
        if tipo.journal_id.type == 'sale':
            #la liquidacion es a cobrar
            invoice_type = 'out_invoice'
            cuenta_origen = tipo.journal_id.default_debit_account_id.id
        elif tipo.journal_id.type == 'purchase':
            #la liquidacion es a pagar
            invoice_type = 'in_invoice'
            cuenta_origen = tipo.journal_id.default_credit_account_id.id
        res.update({'type': invoice_type,
                    'tax_ids': [x.id for x in tipo.tax_ids],
                    'journal_id': tipo.journal_id.id,
                    'account_id': cuenta_origen,
                    'name': tipo.name,
                    'origin': tipo.code,
                    'line_origin': tipo.journal_id.code})

        return res

    
    _columns = {
        'name':fields.char('Nombre', size=64, 
                           required=True),
        'code':fields.char('Código', size=10, 
                           required=True, readonly=False),        
        #'cuenta_origen_id':fields.many2one('account.account', 'CC Origen', 
        #                             required=True,
        #                             help='Cuenta contable de origen'),
        #'cuenta_destino_id':fields.many2one('account.account', 'CC Destino', 
        #                             required=True,
        #                             help='Cuenta contable de destino'),
        'tax_ids':fields.many2many('account.tax', 
                                    'acciones_liquidacion_tipo_tax_rel', 
                                    'liquidacion_tipo_id', 'tax_id', 
                                    'Impuestos'),
        'journal_id':fields.many2one('account.journal', 'Diario', required=True),
        'calc_type': fields.selection(_get_calc_type, 'Tipo de cálculo', required=True),
        'calc_value': fields.float('Valor', digits=(16,2), required=True),
        #'type': fields.selection(_get_type, 'Tipo', required=True),
        'tipo_relacion_ids':fields.many2many('acciones.tipo.relacion', 
                                             'acciones_tipoliq_tiporel_rel', 
                                             'tipo_liquidacion_id', 
                                             'tipo_relacion_id', 
                                             'Tipos de relación',),
        
        
    }

AccionesLiquidacionTipo()


class AccionesLiquidacionLibro(osv.osv):

    _name = 'acciones.libro'
    _inherit = 'acciones.libro'

    _columns = {
        'liquidacion_id': fields.many2one('acciones.liquidacion.tipo',
                                          'Liquidacion asociada')
    }
    

AccionesLiquidacionLibro()


class AccionesLiquidacionLibroLinea(osv.osv):

    _name = 'acciones.libro.linea'
    _inherit = 'acciones.libro.linea'

    def search_cabecera(self, cr, uid, linea, 
                        tipo_liquidacion_id, context={}):
        '''devuelve el titular a utilizar en la liquidacion'''

        libro_rel_obj = self.pool.get('acciones.libro.relacion')
        tipo_liq_obj = self.pool.get('acciones.liquidacion.tipo')
        
        tipo_liq = tipo_liq_obj.browse(cr, uid, tipo_liquidacion_id)        
        tipo_relacion_ids = [x.id for x in tipo_liq.tipo_relacion_ids]

        #Buscamos la relacion de cabecera de la linea
        search_params = [('linea_id','=',linea.id),
                         ('cabecera','=',True),
                         ('tipo_relacion_id','in', tipo_relacion_ids)]
        titular_cabecera_id = libro_rel_obj.search(cr, uid, search_params)
        if titular_cabecera_id:
            return libro_rel_obj.browse(cr, uid, titular_cabecera_id[0])
        else:
            #En caso contrario, buscamos la primera relacion de la linea
            #de uno de los tipos permitidos, con ccc a ser posible
            search_params = [('linea_id','=',linea.id),
                             ('cabecera','=',False),
                             ('tipo_relacion_id','in', tipo_relacion_ids),
                             ('bank_id','!=', False),]
            titular_ids = libro_rel_obj.search(cr, uid, search_params)
            if titular_ids:
                return libro_rel_obj.browse(cr, uid, titular_ids[0])
            #Si no hemos encontrado con ccc, cogemos el primero sin mirar 
            search_params = [('linea_id','=',linea.id),
                             ('cabecera','=',False),
                             ('tipo_relacion_id','in', tipo_relacion_ids),]
            titular_ids = libro_rel_obj.search(cr, uid, search_params)
            if titular_ids:
                return libro_rel_obj.browse(cr, uid, titular_ids[0])
            else:
                raise osv.except_osv('Error', 
                            "No se puede encontrar un titular para liquidar\
                            la linea de %s"%(linea.titular_cabecera))

    def string_intervalos(self, cr, uid, linea, context={}):
        '''devuelve un texto con el total de acciones de la linea
        y el detalle de los intervalos'''

        text = 'Total de acciones: %i\n'%(linea.total)
        text += 'Detalle de las acciones:\n'
        for intervalo in linea.intervalo_ids:
            text += 'del %i al %i, '%(intervalo.accion_desde,
                                        intervalo.accion_hasta)
        return text

    def create_invoice(self, cr, uid, linea,
                       tipo_liquidacion_id, context={}):
        '''funcion para crear una factura de liquidacion'''
        
        libro_rel_obj = self.pool.get('acciones.libro.relacion')
        tipo_liq_obj = self.pool.get('acciones.liquidacion.tipo')
        payment_type_obj = self.pool.get('payment.type')
        invoice_obj = self.pool.get('account.invoice')

        invoice_values = tipo_liq_obj.get_invoice_values(cr, uid, 
                                                    tipo_liquidacion_id)

        amount_unit = tipo_liq_obj.calc_liquidacion(cr, uid, 
                            tipo_liquidacion_id,
                            linea.libro_id.name)
        
        
        #Buscamos la relacion de cabecera de la linea
        #search_params = [('linea_id','=',linea.id),
        #                 ('cabecera','=',True),]
        #titular_cabecera_id = libro_rel_obj.search(cr, uid, search_params)
        #titular_cabecera = libro_rel_obj.browse(cr, uid, titular_cabecera_id[0])
        titular_cabecera = self.search_cabecera(cr, uid, linea, 
                                        tipo_liquidacion_id, context=context)

        invoice_vals = {
                'state': 'draft',
                'type': invoice_values['type'],
                'journal_id': invoice_values['journal_id'],
                'partner_id': titular_cabecera.titular_id.partner_id.id,
                'address_invoice_id': titular_cabecera.fiscal_address_id.id,
                'address_contact_id': titular_cabecera.postal_address_id.id,
                'origin': invoice_values['origin'],
                'name': titular_cabecera.titular_id.name,
                'partner_bank': titular_cabecera.bank_id.id,
            }
        #Buscamos la forma de pago
        #Si tenemos cuenta, es domiciliado, sino oficinas
        if titular_cabecera.bank_id:
            search_params = [('code','=','RECIBO_CSB')]
        else:
            search_params = [('code','=','CAJA')]
        payment_type_id = payment_type_obj.search(cr, uid, search_params)[0]
        partner = titular_cabecera.titular_id.partner_id
 
        #Buscamos la cuenta a utilizar, segun sea out o in invoice
        if invoice_values['type'] == 'in_invoice':
            account_id = partner.property_account_payable.id
        else:
            account_id = partner.property_account_receivable.id
        fiscal_position_id = partner.property_account_position\
                          and partner.property_account_position.id or False
        invoice_vals.update({'payment_type': payment_type_id,
                             'account_id': account_id,
                             'fiscal_position': fiscal_position_id})

        #En el caso de liquidaciones de cupones
        #el total no sera el total de la linea del libro, 
        #sino el total de los cupones. Este dato lo pasaremos en el context
        #para salvar este caso especial
        if not context.get('quantity', False):
            quantity = linea.total
        else:
            quantity = context.get('quantity')
        
        invoice_line_vals = {'account_id': invoice_values['account_id'],
                             'name': invoice_values['name'],
                             'origin': invoice_values['line_origin'],
                             'quantity': quantity,
                             'price_unit': amount_unit ,
                             'invoice_line_tax_id': [(6,0,invoice_values['tax_ids'])],}
        invoice_vals.update({'invoice_line': [(0,0,invoice_line_vals)]})

        #Agregamos en los comentarios de la factura 
        #el detalle de las acciones
        invoice_vals.update({'comment': self.string_intervalos(cr, uid, linea)})        

        invoice_id = invoice_obj.create(cr, uid, invoice_vals)
        #Despues de crear la factura calculamos los impuestos
        if not isinstance(invoice_id, list):
            invoice_obj.button_reset_taxes(cr, uid, [invoice_id])
        else:
            invoice_obj.button_reset_taxes(cr, uid, invoice_id)
        #Actualizamos el campo check_total para hacerlo coincidir 
        #con amount_total. Necesario para las facturas de tipo in_invoice
        invoice_obj.write(cr, uid, invoice_id, {'check_total': invoice_obj.browse(cr, uid, 
                                            invoice_id).amount_total})
            
        return invoice_id

AccionesLiquidacionLibroLinea()  
