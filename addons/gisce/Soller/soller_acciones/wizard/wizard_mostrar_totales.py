
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

class WizardMostrarTotales(osv.osv_memory):


    _name = 'wizard.mostrar.totales'

    
    def default_get(self, cr, uid, fields, context={}):

        res = super(WizardMostrarTotales, self).default_get(cr, uid, fields, context)

        tipo_obj = self.pool.get('acciones.tipo.relacion')
        
        if context.get('active_id', False):
            #Inicializamos los valores a 0
            tipo_ids = tipo_obj.search(cr, uid, [])
            total = {}
            for tipo_id in tipo_ids:
                total[tipo_id] = 0 
            intervalo_obj = self.pool.get('acciones.intervalo')

            titular_id = context.get('active_id')

            search_params = [('titular_id','=',titular_id),]
            intervalo_ids = intervalo_obj.search(cr, uid, search_params)
        
            
            for intervalo in intervalo_obj.browse(cr, uid, intervalo_ids):
                total[intervalo.tipo_relacion_id.id] += intervalo.total
        
            for tipo in tipo_obj.browse(cr, uid, tipo_ids):
                res['tipo_%s'%(tipo.id)] = total and total[tipo.id] or 0

        else:
            for tipo in tipo_obj.browse(cr, uid, tipo_ids):
                res['tipo_%s'%(tipo.id)] = 0

        return res

    def fields_get(self, cr, uid, fields=None, context=None, read_access=True):
        '''sobreescribimos esta funcion para generar tantos campos
        como tipos de relaciones existan'''

        res = super(WizardMostrarTotales, self).fields_get(cr, uid, fields, context)

        tipo_obj = self.pool.get('acciones.tipo.relacion')
        tipo_ids = tipo_obj.search(cr, uid, [])

        for tipo in tipo_obj.browse(cr, uid, tipo_ids):
            res['tipo_%i'%tipo.id] = {'string': tipo.name, 'type': 'integer', 'readonly': True}  
        return res  

    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context={}, toolbar=False):
        '''sobreescribimos el metodo para agregar los campos de forma dinamica'''

        result = super(WizardMostrarTotales, self).fields_view_get(cr, uid, view_id, 
                                                    view_type, context=context, toolbar=toolbar)

        tipo_obj = self.pool.get('acciones.tipo.relacion')
        tipo_ids = tipo_obj.search(cr, uid, [])

        xml = '''<?xml version="1.0"?>\n<%s string="Totales">\n\t''' % (view_type,)

        for tipo in tipo_obj.browse(cr, uid, tipo_ids):
            xml += '''<field name="tipo_%i" colspan="4"/>\n''' %(tipo.id)

        xml += '''<group colspan="4" col="2">'''
        xml += '''<button special="cancel" string="Cerrar" icon="gtk-cancel"/>'''
        #xml += '''<button name="action_total" string="Totales" type="object" icon="gtk-go-forward"/>'''
        xml += '''</group>'''

        xml += '''</%s>'''% (view_type,)

        result['arch'] = xml
        result['fields'] = self.fields_get(cr, uid)
        return result 

    
WizardMostrarTotales()
