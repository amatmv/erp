# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import base64
import StringIO
import csv
import netsvc

class WizardImportarAcciones(osv.osv_memory):
    
    _name = 'wizard.importar.acciones'


    def import_file(self, cr, uid, ids, context=None):
        
        # CSV
        # row[0] -> nombre titular
        # row[1] -> domicilio titular
        # row[2] -> poblacion
        # row[3] -> codigo postal
        # row[4] -> provincia
        # row[5] -> telefono
        # row[6] -> fax
        # row[7] -> email
        # row[8] -> tipo documento de identidad
        # row[9] -> vat
        # row[10] -> forma cobro
        # row[11] -> banco
        # row[12] -> oficina
        # row[13] -> dc
        # row[14] -> cuenta
        # row[15] -> fecha de alta
        # row[16] -> accion_desde
        # row[17] -> accion_hasta
        # row[18] -> numero titular
        # row[19] -> numero de titulo

        logger = netsvc.Logger()
        
        wizard = self.browse(cr, uid, ids[0])
        partner_obj = self.pool.get('res.partner')
        address_obj = self.pool.get('res.partner.address')
        titular_obj = self.pool.get('acciones.titular')
        tipo_obj = self.pool.get('acciones.tipo.relacion')
        relacion_obj = self.pool.get('acciones.relacion')
        bank_obj = self.pool.get('res.partner.bank')
        category_obj = self.pool.get('res.partner.category')


        file = StringIO.StringIO(base64.b64decode(wizard.archivo))
        reader = csv.reader(file, delimiter=';')
        #inicializamos las observaciones    
        observaciones = {}
        titulares_creados = {}
        for row in reader:
            logger.notifyChannel('objects', netsvc.LOG_INFO, 
                                 'Procesando %s, desde %s hasta %s' %(row[0], row[16], row[17]))
            nombre_titular = row[0]
            #miramos si tiene prefijos para eliminarlos de la busqueda
            if nombre_titular[:2] == '(P':
                nombre_titular_limpio = nombre_titular[4:]
            else:
                nombre_titular_limpio = nombre_titular
            direccion_titular = row[1]
            codigo_titular = row[18].zfill(6)
            #buscamos si ya existe el titular con ese codigo
            #search_params = [('partner_id.name','=', nombre_titular_limpio),]
            #titular_id = titular_obj.search(cr, uid, search_params)
            if not titulares_creados.get(row[18], False):
                #Buscamos el partner
                search_params = [('name','=',nombre_titular_limpio),]
                partner_id = partner_obj.search(cr, uid, search_params)
                if not partner_id:
                    vals = {}
                    vals['name'] = nombre_titular_limpio
                    partner_id = partner_obj.create(cr, uid, vals)
                else:
                    partner_id = partner_id[0]
                #miramos el tipo de documento de identidad
                #para ver el prefijo que hay que ponerle
                vat_valido = True
                if row[8] in ('C','D','T'):
                    if not partner_obj.check_vat_es(row[9].replace(' ', '')):
                        logger.notifyChannel('objects', netsvc.LOG_WARNING, 
                            '¡El documento de identidad de %s (%s) no es valido!'%(nombre_titular, row[9].replace(' ', '')))
                        vat_valido = False
                        vat = ''
                    else:
                        vat = 'ES%s' %(row[9].replace(' ', ''))
                else:
                    vat = 'PS%s' %(row[9].replace(' ', ''))
                partner = partner_obj.browse(cr, uid, partner_id)
                if not partner.vat and vat_valido:
                    partner_obj.write(cr, uid, partner_id, {'vat': vat})           
                if partner.vat and partner.vat != vat and vat_valido:
                    logger.notifyChannel('objects', netsvc.LOG_WARNING, 
                        '¡El documento de identidad de %s (%s) es diferente del guardado!'%(nombre_titular, vat))
                else:
                    partner_obj.write(cr, uid, partner_id, {'vat': vat})           
                #Buscamos la direccion
                search_params = [('partner_id','=',partner_id),
                                 ('name','=',nombre_titular_limpio),
                                 ('street','=',direccion_titular),]
                address_id = address_obj.search(cr, uid, search_params)
                if not address_id:
                    #Creamos la direccion directamente
                    vals = {'name': nombre_titular_limpio,
                        'street': direccion_titular,
                        'zip': row[3],
                        'city': row[2],
                        'phone': row[5],
                        'fax': row[6],
                        'email': row[7],
                        'partner_id': partner_id,
                        }
                    address_id = address_obj.create(cr, uid, vals)
                else:
                    address_id = address_id[0]
                #Asociamos la forma de pago y creamos la cuenta
                #bancaria si es necesario
                if row[10] == 'D':
                    #Es domiciliado
                    search_params = [('partner_id','=',partner_id),
                                     ('acc_number','=','%s %s %s %s' %(row[11], row[12], 
                                                      row[13], row[14])),]
                    bank_id = bank_obj.search(cr, uid, search_params)
                    if not bank_id:
                        vals = {'state': 'bank',    
                                'acc_number': '%s%s%s%s' %(row[11], row[12], 
                                                      row[13], row[14]),
                                'partner_id': partner_id,
                               }
                        datos_bank = bank_obj.onchange_banco(cr, uid, [], 
                                            vals['acc_number'], 67,context)
                        vals['acc_number'] = datos_bank['value']['acc_number']
                        vals['bank'] = datos_bank['value']['bank']
                        bank_id = bank_obj.create(cr, uid, vals)
                    else:
                        bank_id = bank_id[0]
                    forma_pago = 'domiciliado'
                else:
                    bank_id = False
                    forma_pago = 'oficinas'
                #Creamos el titular de las acciones
                vals = {'partner_id': partner_id,
                        'address_id': address_id,
                        'fiscal_address_id': address_id,
                        #'forma_pago_id': forma_pago_id,
                        'bank_id': bank_id,
                        #'name': codigo_titular,
                        'forma_pago': forma_pago,
                       }
                titular_id = titular_obj.create(cr, uid, vals)
            else:   
                #titular_id = titular_id[0]
                titular_id = titulares_creados[row[18]]
            titulares_creados[row[18]] = titular_id
            #Agregamos a las notas el numero de titular anterior
            if observaciones.get(titular_id, False):
                #Si ya tiene observaciones, comprobamos que no este insertado el registro
                if 'titular anterior %s'%(row[18]) not in observaciones[titular_id]:
                    observaciones[titular_id] += 'Número de titular anterior %s\n'%(row[18])
            else:
                observaciones[titular_id] = 'Número de titular anterior %s\n'%(row[18])   
            #Agregamos a las notas el numero de titulo
            if observaciones.get(titular_id, False):
                #Si ya tiene observaciones, comprobamos que no este insertado el registro
                if 'titulo anterior %s'%(row[19]) not in observaciones[titular_id]:
                    observaciones[titular_id] += 'Número de titulo anterior %s\n'%(row[19])
            else:
                observaciones[titular_id] = 'Número de titulo anterior %s\n'%(row[19])   
            titular_obj.write(cr, uid, titular_id, {'observaciones': observaciones[titular_id],})
            #miramos las categorias que tenemos que agregar
            #Si el nombre del titular comienza por (PC), pendiente de cambio
            categorias = []
            if nombre_titular[:4] == '(PC)':
                search_params = [('name','=','Pendiente de cambio'),]
                categorias.extend(category_obj.search(cr, uid, search_params))
            if nombre_titular[:4] == '(PN)':
                search_params = [('name','=','Pendiente nominativas'),]
                categorias.extend(category_obj.search(cr, uid, search_params))
            if len(categorias):
                titular_obj.write(cr, uid, titular_id, {'category_ids': [(6,0,categorias)]})
                
            '''miramos el tipo de relacion que tenemos que crear
            si en la provincia aparece la palabra "usufruc"
            sera usufructuario
            si en el nombre aparece "y otros"
            sera copropiedad
            En cualquier otro caso, pleno derecho'''
            if 'usufruc' in row[4].lower():
                tipo = 'Usufructuario'
                #en este caso marcaremos el titular como grupo
                titular_obj.write(cr, uid, titular_id, {'grupo': True})
            elif 'y ot' in nombre_titular.lower():
                tipo = 'Copropietario'
                #en este caso marcaremos el titular como grupo
                titular_obj.write(cr, uid, titular_id, {'grupo': True})
            else:
                tipo = 'Propietario'
            search_params = [('name','=',tipo),]
            tipo_id = tipo_obj.search(cr, uid, search_params)[0]
            #Generamos una lista con las acciones del intervalo
            #y generamos los intervalos
            acciones = [str(x).zfill(7) for x in\
                           range(int(row[16]), int(row[17])+1)]
            #formateamos la fecha que nos la pasan como dd/mm/yyyy
            fecha1 = row[15].split('/')
            fecha = '%s-%s-%s' %(fecha1[2], fecha1[1], fecha1[0])
            relacion_obj.crear_relacion(cr, uid, titular_id, tipo_id,
                                        acciones, context={'fecha': fecha})            
            
        return {}

    def action_limpiar(self, cr, uid, ids, context={}):
        '''funcion para eliminar las relaciones y los titulares creados'''

        relacion_obj  = self.pool.get('acciones.relacion')
        relacion_ids = relacion_obj.search(cr, uid, [])
        relacion_obj.unlink(cr, uid, relacion_ids)

        titular_obj = self.pool.get('acciones.titular')
        titular_ids = titular_obj.search(cr, uid, [])
        titular_obj.unlink(cr, uid, titular_ids)

        return {}
    
    def action_intervalos(self, cr, uid, ids, context={}):
        '''funcion para generar todos los intervalos de los titulares'''

        titular_obj = self.pool.get('acciones.titular')
        titular_ids = titular_obj.search(cr, uid, [])
        titular_obj.generar_intervalos(cr, uid, titular_ids)

        return {}
        
        

    _columns = {      
        'archivo':fields.binary('Archivo', required=False, filters=None),
                                      
    }              
        
               
WizardImportarAcciones()
