# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from datetime import date

class WizardGenerarLibro(osv.osv_memory):
    
    _name = 'wizard.generar.libro'
    _description = 'Libro Registro'

    def action_generar(self, cr, uid, ids, context={}):
        '''funcion que generar el libro registro de accionistas segun los datos
        del momento en el que se genera'''

        libro_obj = self.pool.get('acciones.libro')
        libro_linea_obj = self.pool.get('acciones.libro.linea')
        libro_intervalo_obj = self.pool.get('acciones.libro.intervalo')
        libro_rel_obj = self.pool.get('acciones.libro.relacion')
        intervalo_obj = self.pool.get('acciones.intervalo')
        titular_obj = self.pool.get('acciones.titular')

        wizard = self.browse(cr, uid, ids[0])

        #Primero generamos el libro y determinamos 
        #para que titulares tenemos que generar la informacion
        if wizard.libro_id:
            #Si nos pasan un libro, es porque queremos incorporar lineas al libro
            #Por tanto cogemos los ids del context
            titular_ids = context.get('active_ids',[])
            libro_id = wizard.libro_id.id
        else:
            libro_id = libro_obj.create(cr, uid, {})
            #buscamos todos los titulares individuales
            #search_params = [('grupo','=',False)]
            search_params = []
            titular_ids = titular_obj.search(cr, uid, search_params)

        for titular in titular_obj.browse(cr, uid, titular_ids):
            #miramos si ya existe en el libro
            linea_existente_id = libro_linea_obj.enlibro(cr, uid, titular.id, libro_id)
            if linea_existente_id:
                    #Borramos la linea que corresponde a este titular
                    #Para poder generarla de nuevo
                    libro_linea_obj.unlink(cr, uid, linea_existente_id)
            if not titular.grupo:
                search_params = [('tipo_relacion_id.shared','=',False),
                                 ('titular_id','=', titular.id),]
            else:
                search_params = [('tipo_relacion_id.shared','=',True),
                                 ('titular_id','=', titular.id),]
            intervalo_ids = intervalo_obj.search(cr, uid, search_params)
            #Si tiene acciones
            if intervalo_ids:
                tipo_relacion_id = intervalo_obj.read(cr, uid, intervalo_ids, ['tipo_relacion_id'])[0]['tipo_relacion_id'][0]
                #Creamos una linea en el libro registro
                vals_linea = {'libro_id': libro_id,
                              'origen_id': titular.id,}
                #Si no tiene el cambio realizado, lo marcamos
                if not 'CANVI REALITZAT' in [category.name for category in titular.category_ids]:
                    vals_linea.update({'inscrito': False})
                libro_linea_id = libro_linea_obj.create(cr, uid, vals_linea)
                #Creamos la cabecera   
                vals_rel = {'linea_id': libro_linea_id,
                            'cabecera': True,
                            'titular_id': titular.id,
                            'tipo_relacion_id': tipo_relacion_id,
                            'bank_id': titular.bank_id.id,
                            'partner_country_id': titular.partner_id.country_id.id,
                            'fiscal_address_id': titular.fiscal_address_id.id,
                            'postal_address_id': titular.address_id.id,}
                libro_rel_obj.create(cr, uid, vals_rel)
                #Ahora creamos los intervalos de acciones de esta linea
                for intervalo in intervalo_obj.browse(cr, uid, intervalo_ids):
                    vals = {'linea_id': libro_linea_id,
                            'accion_desde': intervalo.accion_desde,
                            'accion_hasta': intervalo.accion_hasta,
                           }
                    libro_intervalo_obj.create(cr, uid, vals)
                #Si es grupo, creamos una relacion para cada asociado
                if not titular.grupo:
                    continue
                vals_rel = {}
                for relgrupo in titular.relacion_titular_ids: 
                    vals_rel[relgrupo.titular_asociado_id.id] = {'linea_id': libro_linea_id,
                                      'titular_id': relgrupo.titular_asociado_id.id,
                                      'tipo_relacion_id': relgrupo.tipo_relacion_id.id,
                                      'bank_id': relgrupo.titular_asociado_id.bank_id.id,
                                      'partner_country_id': relgrupo.titular_asociado_id.partner_id.country_id.id,
                                      'fiscal_address_id': relgrupo.titular_asociado_id.fiscal_address_id.id,
                                      'postal_address_id': relgrupo.titular_asociado_id.address_id.id,}
                for vals in vals_rel.values():
                    libro_rel_obj.create(cr, uid, vals)
                    
            
        return {
            'name': 'Libro',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'acciones.libro',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', '=', %s)]" %(libro_id),
        }

    _columns = {
        'libro_id':fields.many2one('acciones.libro', 'Libro', required=False,
                                   help='Dejarlo en blanco para crear un libro nuevo a fecha de hoy',),
        
    }
        
WizardGenerarLibro()
