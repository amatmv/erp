# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time

class WizardAccionesLiquidar(osv.osv_memory):
    
    _name = 'wizard.acciones.liquidar'

    def action_liquidar(self, cr, uid, ids, context={}):
        
        libro_linea_obj = self.pool.get('acciones.libro.linea')

        wizard = self.browse(cr, uid, ids[0])

        linea_ids = context.get('active_ids',[])
        invoice_ids = []
        for linea in libro_linea_obj.browse(cr, uid, linea_ids):
            invoice_ids.append(libro_linea_obj.create_invoice(cr, uid, linea, 
                                wizard.tipo_liquidacion_id.id, context=context))

        return {
            'name': 'Facturas',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" %(invoice_ids),
        }

    _columns = {
        'tipo_liquidacion_id':fields.many2one('acciones.liquidacion.tipo',
                                'Tipo de liquidación', required=True),
        
    }
WizardAccionesLiquidar()
