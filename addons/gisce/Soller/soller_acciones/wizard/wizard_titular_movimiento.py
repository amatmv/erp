# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

class WizardTitularMovimiento(osv.osv_memory):
    
    _name = 'wizard.titular.movimiento'
    
    _ORIGEN_SELECTION = [('origen','Origen'),
                         ('destino','Destino'),
                         ('todos','Todos')]

    def action_mostrar(self, cr, uid, ids, context={}):
        
        titular_obj = self.pool.get('acciones.titular')
        movimiento_obj = self.pool.get('acciones.movimiento')
        wizard = self.browse(cr, uid, ids[0])

        movimiento_ids = context.get('active_ids',[])
        
        titular_ids = []
        for movimiento in movimiento_obj.browse(cr, uid, movimiento_ids):
            if wizard.mostrar == 'origen':
                titular_ids = [x.titular_origen_id.id for x in movimiento.linea_ids]
            elif wizard.mostrar == 'destino':
                titular_ids = [x.titular_destino_id.id for x in movimiento.linea_ids]
            else:
                titular_ids.extend([x.titular_origen_id.id for x in movimiento.linea_ids])
                titular_ids.extend([x.titular_destino_id.id for x in movimiento.linea_ids])

        return {
            'name': 'Titulares',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'acciones.titular',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" %(titular_ids),
        }
    
    _columns = {
        'mostrar': fields.selection(_ORIGEN_SELECTION, 'Mostrar titulares', required=True),
        
        
    }
WizardTitularMovimiento()
