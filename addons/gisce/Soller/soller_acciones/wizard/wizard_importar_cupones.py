# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import base64
import StringIO
import csv
import netsvc

class WizardImportarCupones(osv.osv_memory):
    
    _name = 'wizard.importar.cupones'


    def import_file(self, cr, uid, ids, context=None):
        
        # CSV
        # row[0] -> nombre titular cabecera (codigo) nombre
        # row[1] -> cupones a vender
        # row[2] -> cupones a comprar
        
        logger = netsvc.Logger()
        
        libro_linea_obj = self.pool.get('acciones.libro.linea')
        wizard = self.browse(cr, uid, ids[0])

        tipo_venta = wizard.tipo_venta_id.id
        tipo_compra = wizard.tipo_compra_id.id
        
        file = StringIO.StringIO(base64.b64decode(wizard.archivo))
        reader = csv.reader(file, delimiter=';')
        invoice_ids = {'compra':[],
                       'venta':[],}
        for row in reader:
            search_params = [('titular_cabecera','=',row[0]),
                             ('libro_id','=',wizard.libro_id.id)]
            libro_linea_id = libro_linea_obj.search(cr, uid, search_params)
            if len(libro_linea_id) == 1:
                linea = libro_linea_obj.browse(cr, uid, libro_linea_id[0])
                if int(row[2]) <> 0:
                    context.update({'quantity': int(row[2])})
                    invoice_ids['compra'].append(libro_linea_obj.create_invoice(cr, uid, linea, 
                                tipo_compra, context=context))
                elif int(row[1]) <> 0:
                    context.update({'quantity': int(row[1])})
                    invoice_ids['venta'].append(libro_linea_obj.create_invoice(cr, uid, linea, 
                                tipo_venta, context=context))
            else:
                logger.notifyChannel('objects', netsvc.LOG_INFO, 
                                 'No se puede procesar %s' %(row[0]))
        
        return {}

        
    _columns = {      
        'archivo':fields.binary('Archivo', required=True, filters=None),
        'libro_id':fields.many2one('acciones.libro', 'Libro', required=True),
        'tipo_venta_id':fields.many2one('acciones.liquidacion.tipo', 'Tipo de venta', required=True),
        'tipo_compra_id':fields.many2one('acciones.liquidacion.tipo', 'Tipo de compra', required=True),
        
                                      
    }              
        
               
WizardImportarCupones()
