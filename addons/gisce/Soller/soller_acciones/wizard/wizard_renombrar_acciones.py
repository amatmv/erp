# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import netsvc

class WizardRenombrarAcciones(osv.osv_memory):
  
    _name = 'wizard.renombrar.acciones'

    def action_renombrar(self, cr, uid, ids, context={}):

        logger = netsvc.Logger()
    
        wizard = self.browse(cr, uid, ids[0])
        accion_obj = self.pool.get('acciones.accion')
        intervalo_obj = self.pool.get('acciones.intervalo')
        titular_obj = self.pool.get('acciones.titular')
        relacion_obj = self.pool.get('acciones.relacion')

        #inicializamos la ultima accion
        ultima_accion = 1

        #buscamos todos los titulares
        search_params = []
        titular_ids = titular_obj.search(cr, uid, search_params,
                                         0, None, 'name')
        totales = []
        #y generamos una lista con los totales por cada uno de ellos
        for titular in titular_obj.browse(cr, uid, titular_ids):
            logger.notifyChannel('acciones', netsvc.LOG_INFO, 
                                 'Procesando %s, %s' %(titular.name, titular.partner_id.name))
            #print 'Procesando %s, %s' %(titular.name, titular.partner_id.name)
            titular_dict = {}
            titular_dict['total'] = 0
            titular_dict['titular'] = titular.id
            titular_dict['tipo'] = 0
            for intervalo in titular.intervalo_ids:
                #si el tipo de intervalo no coincide con el tipo de titular
                #no lo contabilizamos
                if intervalo.tipo_relacion_id.shared <> titular.grupo:
                    continue
                titular_dict['tipo'] = intervalo.tipo_relacion_id.id
                titular_dict['total'] += intervalo.total
                #sacamos las acciones del intervalo y las movemos al historico
                acciones = [str(x).zfill(7) for x in range(intervalo.accion_desde, intervalo.accion_hasta+1)]
                #relacion_obj.mover_historico(cr, uid, titular.id, acciones)
                #hack para dejar a el gas con las ultimas acciones
                #borramos las relaciones directamente para no poner 
                #mierda en el historico
                search_params = [('accion_id','in',accion_obj.get_ids(cr, uid, acciones))]
                relacion_ids = relacion_obj.search(cr, uid, search_params)
                relacion_obj.unlink(cr, uid, relacion_ids)
            totales.append(titular_dict)
        #creamos las nuevas relaciones de forma secuencial
        for titular in totales:
            logger.notifyChannel('acciones', netsvc.LOG_INFO, 
                                 'Procesando titular %i, total %i, tipo %i'\
                                 %(titular['titular'], titular['total'], titular['tipo']))
            '''print 'Procesando titular %i, total %i, tipo %i'\
                    %(titular['titular'], titular['total'], titular['tipo'])'''
            acciones = [str(x).zfill(7) 
                        for x in range(ultima_accion, ultima_accion+titular['total'])]
            ultima_accion = ultima_accion+titular['total']
            if titular['total'] > 0:
                relacion_obj.crear_relacion(cr, uid, 
                                        titular['titular'], 
                                        titular['tipo'],
                                        acciones)
                titular_obj.generar_intervalos(cr, uid, [titular['titular']])
        
        return {}

    
WizardRenombrarAcciones()
