# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import netsvc

class WizardLigarAcciones(osv.osv_memory):
    
    _name = 'wizard.ligar.acciones'
    _description = 'Ligar acciones'
    
    def action_ligar(self, cr, uid, ids, context={}):
        '''funcion para ligar los numeros nuevos de acciones 
        con su nombre antes del cambio'''

        logger = netsvc.Logger()
    
        wizard = self.browse(cr, uid, ids[0])
        accion_obj = self.pool.get('acciones.accion')
        rel_hist_obj = self.pool.get('acciones.relacion.historico')
        rel_obj = self.pool.get('acciones.relacion')
        titular_obj = self.pool.get('acciones.titular')

        #Buscamos todas las relaciones historicas con fecha final 14/07/2011
        #Vamos a dividir la busqueda en trozos de 3000 para ir mas rapido en las consultas
        cr.execute("select count(id) from acciones_relacion_historico where fecha_hasta = '2011-07-14'")
        total = cr.fetchone()
        if (total[0] % 3000) > 0:
            trozos = int(round(total[0]/3000,0)) + 1
        else:
            trozos = int(round(total[0]/3000,0))
        logger.notifyChannel('acciones', netsvc.LOG_INFO, 
                                 'Se van a calcular %s trozos' %(trozos))
        lista = {}
        for i in range(trozos):
            logger.notifyChannel('acciones', netsvc.LOG_INFO, 
                                 'Buscando relaciones de %s a %s' %(i*3000, ((i+1)*3000)-1))
       
            cr.execute("select id from acciones_relacion_historico where fecha_hasta = '2011-07-14'\
                        order by id limit 3000 offset %s", (i*3000,))
        
           
            relaciones = cr.fetchall()
            #ya_asignadas = []
            for rel_hist in rel_hist_obj.browse(cr, uid, [x[0] for x in relaciones]):
                #Buscamos a ver si este numero ya ha sido asignado a una accion
                #cr.execute("select count(id) from acciones_accion where name_anterior = %s", 
                #           (rel_hist.accion_id.name,))
                #if cr.fetchone()[0]:
                #    continue
                #Si ya la hemos tratado nos la saltamos
                #if rel_hist.accion_id.id in ya_asignadas:
                #    continue
                #Si no coincide tipo de titular y tipo de relacion nos la saltamos
                if rel_hist.titular_id.grupo <> rel_hist.tipo_relacion_id.shared:
                    #print "Salto la accion %s del titular %s (%s)" %(rel_hist.accion_id.name, rel_hist.titular_id.name, rel_hist.titular_id.grupo)
                    continue
                #buscamos la primera accion sin equivalencia del titular del historico
                #para asignar una
                #hack para salvar un renombre de acciones que se hizo despues del renombrado
                if rel_hist.titular_id.id == 316:
                    titular_id = 51
                else:
                    titular_id = rel_hist.titular_id.id
                if titular_id not in lista.keys():
                    lista[titular_id] = []
                lista[titular_id].append(rel_hist.accion_id.name)
                continue
                cr.execute("select acc.id,acc.name from acciones_relacion rel inner join acciones_accion acc on\
                            rel.accion_id = acc.id\
                            where rel.titular_id = %s and acc.name_anterior = '0000000' limit 1", 
                            (titular_id,)) 
                #Escribimos en la accion que hemos encontrado el nombre de la vieja
                accion_id = cr.fetchone()
                if accion_id:
                    cr.execute("update acciones_accion set name_anterior = %s where id = %s",
                               (rel_hist.accion_id.name, accion_id[0],))
                    #accion_obj.write(cr, uid, accion_id[0], 
                    #                  {'name_anterior': rel_hist.accion_id.name})
                    ya_asignadas.append(rel_hist.accion_id.id)
                    #logger.notifyChannel('acciones', netsvc.LOG_INFO, 
                    #             'Numero %s - %s' %(accion_id[1], rel_hist.accion_id.name))
                #else:
                    #logger.notifyChannel('acciones', netsvc.LOG_INFO,
                    #             'La consulta para el titular %s y accion %s no ha dado resultados' 
                    #             %(rel_hist.titular_id.id, rel_hist.accion_id.name))
                    #raise osv.except_osv('Error',
                    #                'La consulta para el titular %s y accion %s no ha dado resultados' 
                    #             %(rel_hist.titular_id.id, rel_hist.accion_id.name))
            #cr.commit()
        total_acciones = []
        for titular, acciones in lista.items():
            tit = titular_obj.browse(cr, uid, titular)
            #Buscamos todas las acciones actuales del titular
            search_params = [('titular_id','=',titular),
                             ('tipo_relacion_id.shared','=',tit.grupo),]
            rel_ids = rel_obj.search(cr, uid, search_params)
            acciones_nuevas = rel_obj.get_acciones(cr, uid, rel_ids, 'int')
            logger.notifyChannel('acciones', netsvc.LOG_INFO, 
                         'Procesando el titular %s, %s - %s' %(titular, len(acciones_nuevas), len(acciones)))
            if len(acciones_nuevas) <> len(acciones):
                logger.notifyChannel('acciones', netsvc.LOG_INFO, 
                         'Discrepancia para el titular %s, %s - %s' %(titular, len(rel_ids), len(acciones)))
                continue
            #recorremos las dos listas que tienen la misma longitud
            #asignando a la accion nueva el nombre viejo
            for i in range(len(acciones)):
                accion_obj.write(cr, uid, acciones_nuevas[i], {'name_anterior':acciones[i]})    
            
                
        return {}
            

WizardLigarAcciones()
