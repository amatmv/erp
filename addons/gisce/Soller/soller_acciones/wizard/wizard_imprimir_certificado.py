# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import time

class WizardImprimirCertificado(osv.osv_memory):

    """Wizard para imprimir el certificado 
    pudiendo cambiar la fecha de expedicion"""

    _name = "wizard.imprimir.certificado"

    def imprimir(self, cr, uid, ids, context=None):

        serie_obj = self.pool.get('acciones.serie')

        wizard = self.browse(cr, uid, ids[0])

        nominal = serie_obj.get_nominal(cr, uid, wizard.fecha_certificado,
                                        context=context)

        datas = {'form':{'fecha_certificado':wizard.fecha_certificado,
                         'nominal': int(nominal),
                         'ids': context.get('active_ids', []),
                        }
                }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'report_acciones_titular_certificado',
            'datas': datas,
        }

    _columns = {
        'fecha_certificado': fields.date('Fecha certificado', required=True),        
                
    }

    _defaults = {
        'fecha_certificado': lambda *a: time.strftime('%Y-%m-%d'),
        
    }


WizardImprimirCertificado()
