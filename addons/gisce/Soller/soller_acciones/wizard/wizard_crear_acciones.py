# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

class WizardCrearAcciones(osv.osv_memory):
    '''
    Open ERP Model
    '''
    _name = 'wizard.crear.acciones'

    def action_crear(self, cr, uid, ids, context={}):
    
        wizard = self.browse(cr, uid, ids[0])
        accion_obj = self.pool.get('acciones.accion')
        
        accion_obj.crear_accion(cr, uid, wizard.accion_desde,
                                wizard.accion_hasta)
    
        return {}

        
    
    _columns = {
        'accion_desde': fields.integer('Desde'),
        'accion_hasta': fields.integer('Hasta'),
        
    }
WizardCrearAcciones()
