# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields


class WizardMostrarFactures(osv.osv_memory):

    _name = 'wizard.mostrar.factures.libro'    

    def mostrar_factures(self, cr, uid, ids, context=None):
        """Devuelve una vista con las facturas del libro"""

        factura_obj = self.pool.get('account.invoice')
        linea_obj = self.pool.get('acciones.libro.linea')
        linea_ids = context.get('active_ids', [])

        titulares = []
        for linia in linea_obj.browse(cr, uid, linea_ids):
            titular = linia.titular_cabecera.split(')')[0].replace('(', '')
            titulares.append(titular)

        vals_view = {
                'name': 'Facturas',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.invoice',
                'limit': 500,
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': "[('name', 'in', %s)]" %(titulares),
                }
        
        return vals_view

WizardMostrarFactures()
