# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from itertools import *
import time
from datetime import date, datetime

class AccionesTipoRelacion(osv.osv):
    '''
    Tipos de relacion entre un titular y una accion
    '''
    _name = 'acciones.tipo.relacion'
    _description = 'Tipo de relación'

    _order = 'name'
    
    _columns = {
        'name':fields.char('Tipo de relación', size=64, required=True),
        'shared':fields.boolean('Compartido', required=False),        
        
    }

    _defaults = {
        'shared': lambda *a: False,
        
    }
AccionesTipoRelacion()

class AccionesTitular(osv.osv):
    '''
    Ficha del titular
    '''
    _name = 'acciones.titular'
    _description = 'Titular'

    def create(self, cr, uid, vals, context={}):

        #damos un numero de secuencia 
        if not vals.get('name', False):
            vals['name'] = self.pool.get('ir.sequence').\
                                get(cr, uid, 'acciones.titular')
        return super(AccionesTitular, self).create(cr, uid, vals, context)

    def name_get(self, cr, uid, ids, context={}):
        
        res = []
        for titular in self.browse(cr, uid, ids):
            res.append((titular.id, 
                        '(%s) %s' %(titular.name, titular.partner_id.name)))
        return res

    def propagar(self, cr, uid, titular_id, context={}):
        '''un titular tiene que propagar los cambios a otros titulares 
        siempre que sea un grupo y tenga relaciones activas
        Devolvemos un lista con tupla titular asociado y tipo de relacion'''

        if isinstance(titular_id, list) or isinstance(titular_id, tuple):
            titular_id = titular_id[0]

        titular = self.browse(cr, uid, titular_id)
        titular_asociado_ids = []
        if titular.grupo:
            for asociado in titular.relacion_titular_ids:
                if asociado.state == 'activa':
                    titular_asociado_ids.append([asociado.titular_asociado_id.id,
                                                  asociado.tipo_relacion_id.id])    
        return titular_asociado_ids     

    def _get_certificable(self, cr, uid, ids, name, arg, context=None):
        '''devuelve si es certificable o no'''

        res = {}
        for titular in self.browse(cr, uid, ids):
            res[titular.id] = True
            for category in titular.category_ids:
                if category.name.startswith('Pendiente'):
                    res[titular.id] = False
                    continue
        return res
    
    
    _columns = {
        'name':fields.char('Código', size=64, readonly=True),
        'active':fields.boolean('Activo', required=False),               
        'partner_id':fields.many2one('res.partner', 'Titular', required=True),
        'partner_vat': fields.related('partner_id','vat', type='char', 
                                      relation='res.partner', 
                                      string='NIF/CIF', store=False,
                                      readonly=True,),
        'address_id':fields.many2one('res.partner.address', 'Dirección', required=True),
        'fiscal_address_id':fields.many2one('res.partner.address', 
                                            'Dirección Fiscal', required=True),     
        'bank_id':fields.many2one('res.partner.bank', 'CCC', required=False),
        #'payment_type_id':fields.many2one('payment.type', 'Forma de pago'),
        'observaciones': fields.text('Observaciones'),
        'grupo':fields.boolean('Grupo', required=False),
        'category_ids':fields.many2many('res.partner.category', 
                                       'acciones_titular_category_rel', 
                                       'titular_id', 'category_id', 'Categorias'),
        'forma_pago':fields.selection([('domiciliado','Domicilidado'),
                                       ('oficinas','Oficinas'),],
                                       'Forma de pago', select=True, required=True),
        'certificable': fields.function(_get_certificable, method=True, 
                                        type='boolean', string='Certificable', store=True),        
        
        
    }

    _defaults = {
        'active': lambda *a: True,
        'grupo': lambda *a: False,
   
    }

AccionesTitular()

class AccionesAccion(osv.osv):

    _name = 'acciones.accion'
    _description = 'Accion'
    _order = "name"

    def create(self, cr, uid, vals, context={}):

        #damos un numero de secuencia 
        if not vals.get('name', False):
            vals['name'] = self.pool.get('ir.sequence').\
                                get(cr, uid, 'acciones.accion')
        return super(AccionesAccion, self).create(cr, uid, vals, context)

    def ordenar(self, cr, uid, ids, context={}):
        '''funcion para ordenar las acciones por numero de accion'''
        '''devuelve una lista con los numeros de accion ordenados'''

        search_params = [('id','in',ids)]        
        accion_ids =  self.search(cr, uid, search_params)
        acciones = self.read(cr, uid, accion_ids, ['name'])
        return [int(accion['name']) for accion in acciones]

    def crear_accion(self, cr, uid, desde, hasta, context={}):
        '''funcion para crear todas las acciones entre el intervalo'''
              
        for i in range(desde, hasta+1):
            self.create(cr, uid, {'name': str(i).zfill(7)})

        return True

    def get_ids(self, cr, uid, acciones, context={}):
        '''devuelve los ids de las acciones segun su nombre'''

        return self.search(cr, uid, [('name','in',acciones)])

    
    _columns = {
        'name': fields.char('Nº acción', size=64, required=False, readonly=True),
        'name_anterior': fields.char('Nº acción anterior', size=64, required=False, readonly=True),
        
    }

    _defaults = {
        'name_anterior': lambda *a: '0000000',
        
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'El número de la acción debe ser único!'),
        
    ]
AccionesAccion()

class AccionesRelacion(osv.osv):
    '''
    Relacion entre un titular y una accion
    '''
    _name = 'acciones.relacion'
    _description = 'Relacion'

    def get_acciones(self, cr, uid, ids, mode='int', context={}):
        '''devuelve una lista de acciones asociadas
        a las relaciones en ids'''
    
        accion_obj = self.pool.get('acciones.accion')

        acciones_ids = [x['accion_id'][0] for x in self.read(cr, uid, 
                       ids, ['accion_id'], context)]
        
        if mode == 'int':
            return accion_obj.ordenar(cr, uid, acciones_ids)
        else:
            return [x[1] for x in accion_obj.name_get(cr, uid, acciones_ids)]


    def mover_historico(self, cr, uid, 
                        titular_id, acciones, context={}):
        '''Mover relaciones al histórico basadas
        en un titular y una lista de acciones'''
        
        historico_obj = self.pool.get('acciones.relacion.historico')

        #Tenemos que sacar la fecha de fin
        #Si nos la pasan en el context la usamos, sino, ahora
        if context.get('fecha', False):
            fecha = context['fecha']
        else:
            fecha = date.today()        

        search_params = [('titular_id','=',titular_id),
                         ('accion_id.name','in',acciones),]
        relaciones_ids = self.search(cr, uid, search_params)

        #Para cada valor, lo creamos en el historico
        for relacion in self.browse(cr, uid, relaciones_ids):
            values = {'titular_id': relacion.titular_id.id,
                     'accion_id': relacion.accion_id.id,
                     'tipo_relacion_id': relacion.tipo_relacion_id.id,
                     'fecha_desde': relacion.fecha_desde,   
                     'fecha_hasta': fecha,
                    }
            historico_obj.create(cr, uid, values, context)
        self.unlink(cr, uid, relaciones_ids)
        #Si el titular es un grupo, tenemos que realizar el mismo proceso 
        #para cada uno de los titulares asociados
        titular_obj = self.pool.get('acciones.titular')
        for asociado in titular_obj.propagar(cr, uid, titular_id):
            self.mover_historico(cr, uid, 
                            asociado[0], 
                            acciones, context)
           
        return True

    def crear_relacion(self, cr, uid, titular_id, tipo_id,
                                acciones, context={}):
        '''funcion para crear las relaciones con los titulares'''

        accion_obj = self.pool.get('acciones.accion')
        #Tenemos que sacar la fecha de creacion para guardarla
        #Si nos la pasan en el context, la usamos, sino, ahora
        if context.get('fecha', False):
            fecha = context['fecha']
        else:
            fecha = date.today()
        if isinstance(titular_id, list) or isinstance(titular_id, tuple):
            titular_id = titular_id[0]

        for accion in accion_obj.get_ids(cr, uid, acciones):
            vals = {'titular_id': titular_id,
                    'accion_id': accion,
                    'tipo_relacion_id':tipo_id,
                    'fecha_desde': fecha,
                    }
            self.create(cr, uid, vals)
    
        #Si el titular es un grupo, tenemos que realizar el mismo proceso 
        #para cada uno de los titulares asociados, segun la relacion
        #que tenga con el grupo
        titular_obj = self.pool.get('acciones.titular')
        for asociado in titular_obj.propagar(cr, uid, titular_id):
            self.crear_relacion(cr, uid, asociado[0], asociado[1], 
                                acciones, context)
    
        return True
  
    _columns = {
        'titular_id':fields.many2one('acciones.titular', 'Titular', required=True, ondelete='cascade'),
        'accion_id':fields.many2one('acciones.accion', 'Acción', required=True),        
        'tipo_relacion_id':fields.many2one('acciones.tipo.relacion', 'Relación', required=True),
        'fecha_desde': fields.date('Desde', required=True),        
        
    }

    _sql_constraints = [
        ('titular_accion_uniq', 
         'unique (titular_id, accion_id)', 
         'Ya existe una relacion entre titular y acción !'),
        
    ]

AccionesRelacion()

class AccionesRelacionHistorico(osv.osv):
    '''
    Historico de relacion entre un titular y una accion
    '''
    _name = 'acciones.relacion.historico'
    _description = 'Histórico Relacion'
    
    _columns = {
        'titular_id':fields.many2one('acciones.titular', 
                                     'Titular', required=True, ondelete='cascade'),
        'accion_id':fields.many2one('acciones.accion', 
                                    'Acción', required=True),        
        'tipo_relacion_id':fields.many2one('acciones.tipo.relacion', 
                                           'Relación', required=True),
        'fecha_desde': fields.date('Desde', required=True),
        'fecha_hasta': fields.date('Hasta', required=True),
        
    }

AccionesRelacionHistorico()

class AccionesAccion2(osv.osv):

    _name = 'acciones.accion'
    _inherit = 'acciones.accion'
    
    _columns = {
        'relacion_ids':fields.one2many('acciones.relacion', 'accion_id', 
                                       'Relaciones', required=False),
        'relacion_historico_ids':fields.one2many('acciones.relacion.historico', 
                                    'accion_id', 'Histórico', required=False),
                
    }

AccionesAccion2()

class AccionesIntervalos(osv.osv):

    _name = 'acciones.intervalo'
    _description = 'Intervalos de acciones'

    def _acciones_total(self, cr, uid, ids, name, arg, context=None):

        res = {}
        for intervalo in self.browse(cr, uid, ids):
            res[intervalo.id] = (intervalo.accion_hasta - intervalo.accion_desde) + 1
        return res

    _columns = {
        'titular_id':fields.many2one('acciones.titular', 
                                     'Titular', required=False, ondelete='cascade',),        
        'accion_desde': fields.integer('Desde', required=True),
        'accion_hasta': fields.integer('Hasta', required=True),
        'tipo_relacion_id':fields.many2one('acciones.tipo.relacion', 
                                           'Relación', required=True),
        'total': fields.function(_acciones_total, method=True, 
                                 type='integer', string='Total', store=True),
    }
AccionesIntervalos()

class AccionesRelacionTitular(osv.osv):

    _name = 'acciones.relacion.titular'
    _description = 'Relaciones entre titulares'

    def activar_asociacion(self, cr, uid, ids, context={}):
        '''Una vez creada la relacion con un titular, crea
        la asociacion y las relaciones con las acciones'''

        titular_obj = self.pool.get('acciones.titular')
        relacion_acc_obj = self.pool.get('acciones.relacion')

        #Si es un grupo, tenemos que crear las relaciones en el asociado 
        #con el tipo de la asociacion
        #Primero buscamos todas las relaciones del titular

        for relacion in self.browse(cr, uid, ids):
            if relacion.titular_id.grupo:
                #Buscamos si la relacion inversa ya existe
                #y sino hay que crearla
                search_params = [('titular_id','=',relacion.titular_asociado_id.id),
                                 ('titular_asociado_id','=',relacion.titular_id.id)]
                rel_ids = self.search(cr, uid, search_params)
                if not len(rel_ids):
                    vals = {'name': relacion.name,
                            'titular_id': relacion.titular_asociado_id.id,
                            'titular_asociado_id': relacion.titular_id.id,
                            'tipo_relacion_id': relacion.tipo_relacion_id.id,
                            'relacion_asociada_id': relacion.id,
                            'state': 'activa',
                            }
                    rel_destino_id = self.create(cr, uid, vals)
                    #Guardamos la relacion asociada
                    relacion.write({'relacion_asociada_id': rel_destino_id, 'state': 'activa'})
                #Creamos las relaciones con las acciones en el titular asociado
                search_params = [('titular_id','=',relacion.titular_id.id),
                                ]
                relacion_acc_ids = relacion_acc_obj.search(cr, uid, search_params)
                acciones = relacion_acc_obj.get_acciones(cr, uid, relacion_acc_ids, 'str')
                relacion_acc_obj.crear_relacion(cr, uid, 
                                            relacion.titular_asociado_id.id, 
                                            relacion.tipo_relacion_id.id, 
                                            acciones, context={})
            #Finalmente generamos los intervalos del asociado
            titular_obj.generar_intervalos(cr, uid, [relacion.titular_asociado_id.id])

        return True

    def desactivar_asociacion(self, cr, uid, ids, context={}):
        '''Antes de borrar una asociacion tendremos que mover al historico
        todas las relaciones del asociado'''

        relacion_acc_obj = self.pool.get('acciones.relacion')
        titular_obj = self.pool.get('acciones.titular')
        
        for relacion in self.browse(cr, uid, ids):
            if relacion.titular_id.grupo:
                search_params = [('titular_id','=', relacion.titular_id.id),
                                ]
                relacion_acc_ids = relacion_acc_obj.search(cr, uid, search_params)
                acciones = relacion_acc_obj.get_acciones(cr, uid, relacion_acc_ids, 'str')
                #movemos al historico las relaciones del asociado
                relacion_acc_obj.mover_historico(cr, uid, 
                                             relacion.titular_asociado_id.id,
                                             acciones)
                #regeneramos los intervalos del titular asociado
                titular_obj.generar_intervalos(cr, uid, [relacion.titular_asociado_id.id])
                #borramos la asociacion en el destino
                self.unlink(cr, uid, [relacion.relacion_asociada_id.id])
                #borramos la relacion 
                relacion.write({'relacion_asociada_id': None, 'state': 'inactiva',})

        return True
                 
        
    _columns = {
        'name': fields.date('Fecha'),
        'titular_id':fields.many2one('acciones.titular', 'Titular', required=True),        
        'titular_asociado_id':fields.many2one('acciones.titular', 
                                'Asociado', required=True),
        'titular_asociado_grupo_id': fields.related('titular_asociado_id','grupo', 
                                type='boolean', relation='acciones.titular', 
                                string='Grupo', store=False),        
        'tipo_relacion_id': fields.many2one('acciones.tipo.relacion', 
                                'Tipo relación', required=True),        
        'relacion_asociada_id':fields.many2one('acciones.relacion.titular', 
                                'Relación asociada', required=False), 
        'state':fields.selection([('inactiva','Inactiva'),
                                  ('activa','Activa'),],'Estado', select=True, readonly=True),       
        
    }

    _defaults = {
        'name': lambda *a: time.strftime('%Y-%m-%d'),
        'state': lambda *a: 'inactiva',
        
    }

    #TODO constraint para evitar que se puedan crear relaciones entre dos grupos
    def _comprueba_relacion(self, cr, uid, ids, context={}):
        '''constraint que comprueba que no hay 
        relaciones entre dos grupos'''

        for relacion in self.browse(cr, uid, ids):
            if relacion.titular_id.grupo and relacion.titular_asociado_id.grupo:
                return False
            
        return True

    _constraints = [
        (_comprueba_relacion, 
         'No se puede crear una relacion entre dos grupos!', 
         ['titular_id','titular_asociado_id']),
    ]

AccionesRelacionTitular()

class AccionesTitular2(osv.osv):

    _name = 'acciones.titular'
    _inherit = 'acciones.titular'

    def borrar_intervalos(self, cr, uid, ids, context={}):
        '''funcion para borrar todos los intervalos 
        de los titulares que nos pasan en ids'''

        intervalo_obj = self.pool.get('acciones.intervalo')

        search_params = [('titular_id','in', ids)]
        intervalo_ids = intervalo_obj.search(cr, uid, search_params)
        intervalo_obj.unlink(cr, uid, intervalo_ids)

        #Si el titular es un grupo, tenemos que realizar el mismo proceso 
        #para cada uno de los titulares asociados activos
        for titular_id in ids:
            for asociado in self.propagar(cr, uid, titular_id):
                self.borrar_intervalos(cr, uid, [asociado[0]])

        return True

    def generar_intervalos(self, cr, uid, ids, context={}):
        '''funcion para generar los intervalos de acciones
        Los intervalos se generan por tipo de relacion'''

        relacion_obj = self.pool.get('acciones.relacion')
        tipo_obj = self.pool.get('acciones.tipo.relacion')
        intervalo_obj = self.pool.get('acciones.intervalo')
        accion_obj = self.pool.get('acciones.accion')

        #Para cada titular
        for id in ids:
            #borramos los intervalos anteriores
            self.borrar_intervalos(cr, uid, [id])
            #buscamos las relaciones asociadas
            #por tipo de relacion
            for tipo in tipo_obj.search(cr, uid, []):
                search_params = [('titular_id','=',id),
                             ('tipo_relacion_id','=',tipo),]
                relacion_ids = relacion_obj.search(cr, uid, search_params)
                if not len(relacion_ids):
                    continue
                #sacamos las acciones asociadas a estas relaciones
                #ordenadas por numero de accion
                acciones = relacion_obj.get_acciones(cr, uid, relacion_ids)
                #Buscamos las posiciones en las que las acciones no son consecutivas
                pos = [x for x in ifilter(lambda x: acciones[x]+1 <> acciones[x+1],\
                                             range(len(acciones)-1))]
                pos2 = []
                #A cada posicion encontrada le agregamos la posicion siguiente
                for i in pos:
                    pos2.extend([i,i+1])
                #Agregamos la primera y ultima posicion para cerrar los intervalos
                pos2.insert(0,0)
                pos2.append(len(acciones)-1)

                #Para cada intervalo de dos posiciones, 
                #generamos el intervalo de acciones correspondiente
                for i in range(0,len(pos2)-1,2):
                    vals={'titular_id': id,
                          'accion_desde': acciones[pos2[i]],
                          'accion_hasta': acciones[pos2[i+1]],
                          'tipo_relacion_id': tipo,
                         }
                    intervalo_obj.create(cr, uid, vals)
                #Si el titular es un grupo, tenemos que realizar el mismo proceso 
                #para cada uno de los titulares asociados
            for asociado in self.propagar(cr, uid, id):
                self.generar_intervalos(cr, uid, [asociado[0]])
        
        return True

    def _get_distinct_tipos(self, cr, uid, ids, name, arg, context={}):
        '''devuelve los diferentes tipos de intervalos'''
        
        res = {}
        for titular in self.browse(cr, uid, ids):
            tipos = [x.tipo_relacion_id.name for x in titular.intervalo_ids]
            res[titular.id] = ','.join(list(set(tipos)))

        return res

    def _get_titular_from_intervalo(self, cr, uid, ids, context={}):
        
        intervalo_obj = self.pool.get('acciones.intervalo')
        titular_ids = intervalo_obj.read(cr, uid, ids, ['titular_id'])

        return [x['titular_id'][0] for x in titular_ids]

    def _get_distinct_tipos_search(self, cr, uid, obj, name, args, context):
        '''Buscamos en los tipos'''

        intervalo_obj = self.pool.get('acciones.intervalo')
        tipo_obj = self.pool.get('acciones.tipo.relacion')

        if not len(args):
            return []
        search_params = [('tipo_relacion_id.name', args[0][1], args[0][2])]
        intervalo_ids = intervalo_obj.search(cr, uid, search_params)
        titulares = intervalo_obj.read(cr, uid, intervalo_ids, ['titular_id'])
        if len(titulares):
            titular_ids = list(set([x['titular_id'][0] for x in titulares]))
        else:
            titular_ids = []

        return [('id','in',titular_ids),]
        
    
    _columns = {
        'intervalo_ids':fields.one2many('acciones.intervalo', 'titular_id', 
                                       'Intervalos', required=False),
        'tipos_relacion': fields.function(_get_distinct_tipos, method=True, 
                            type='char', string='Tipos', size=128,
                            store={'acciones.intervalo': (_get_titular_from_intervalo, ['tipo_relacion_id'], 20),
                                   'acciones.titular': (lambda self, cr, uid, ids, c={}: ids, ['intervalo_ids'], 20),
                                },
                            fnct_search=_get_distinct_tipos_search),
        'relacion_titular_ids':fields.one2many('acciones.relacion.titular', 
                                'titular_id', 'Relaciones', required=False),
        
    }

AccionesTitular2()

class AccionesSerie(osv.osv):
    '''Modelo para controlar el precio de las acciones por series'''

    _name = 'acciones.serie'
    _descrition = 'Serie'

    def get_nominal(self, cursor, uid, date, context=None):
        '''returns nominal value depending on date'''

        search_params = [('fecha_creacion', '<=', date)]
        serie_ids = self.search(cursor, uid, search_params, limit=1,
                                order='fecha_creacion desc')
        if not serie_ids:
            date_print = datetime.strptime(date,
                                           '%Y-%m-%d').strftime('%d/%m/%Y')
            raise osv.except_osv('Error',
                                 u"No se ha encontrado ninguna "
                                 u"serie en la fecha %s" % date_print)
        serie_vals = self.read(cursor, uid, serie_ids[0], ['valor'])
        return serie_vals['valor']

    def _acciones_total(self, cr, uid, ids, name, arg, context=None):

        res = {}
        for serie in self.browse(cr, uid, ids):
            res[serie.id] = (serie.accion_hasta - serie.accion_desde) + 1
        return res

    _columns = {
        'name':fields.char('Nombre', size=64, required=True, readonly=False),
        'accion_desde': fields.integer('Desde'),
        'accion_hasta': fields.integer('Hasta'),
        'valor': fields.float('Valor nominal', digits=(16,2)),
        'fecha_creacion': fields.date('Fecha de creación'),
        'total': fields.function(_acciones_total, method=True, readonly=True,
                                 type='integer', string='Total', store=True),
            
    }

AccionesSerie()


