# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* soller_crm
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2016-03-14 08:48:33+0000\n"
"PO-Revision-Date: 2016-03-14 08:48:33+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: soller_crm
#: field:crm.case,delivered:0
msgid "Entregat"
msgstr "Entregat"

#. module: soller_crm
#: model:ir.model,name:soller_crm.model_wizard_soller_create_order
msgid "wizard.soller.create.order"
msgstr "wizard.soller.create.order"

#. module: soller_crm
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "XML incorrecte per l'arquitectura de vista!"

#. module: soller_crm
#: field:crm.case,delivered_user_id:0
msgid "Entregat a"
msgstr "Entregat a"

#. module: soller_crm
#: field:crm.case,polissa_anterior_id:0
msgid "Abonat anterior"
msgstr "Abonat anterior"

#. module: soller_crm
#: model:ir.module.module,description:soller_crm.module_meta_information
msgid "\n"
"  * Report de ordre de feina i canvi de comptador\n"
"  * Referencia al segon abonat directament per contractació\n"
"    "
msgstr "\n"
"  * Report de ordre de feina i canvi de comptador\n"
"  * Referencia al segon abonat directament per contractació\n"
"    "

#. module: soller_crm
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: soller_crm
#: constraint:crm.case.section:0
msgid "Error ! You cannot create recursive sections."
msgstr "Error ! No pot crear seccions recursives"

#. module: soller_crm
#: field:crm.case,polissa_id:0
msgid "Abonat"
msgstr "Abonat"

#. module: soller_crm
#: field:crm.case,delivered_date:0
msgid "Data d'entrega"
msgstr "Data d'entrega"

#. module: soller_crm
#: model:ir.actions.report.xml,name:soller_crm.report_crm_case_ordre_canvi
msgid "Ordre canvi"
msgstr "Ordre canvi"

#. module: soller_crm
#: model:ir.module.module,shortdesc:soller_crm.module_meta_information
msgid "Extensió CRM Soller"
msgstr "Extensió CRM Soller"

#. module: soller_crm
#: model:crm.case.section,name:soller_crm.crm_case_section_orden_general
msgid "Orden General"
msgstr "Orden General"

#. module: soller_crm
#: field:crm.case,ref_name:0
msgid "Referència"
msgstr "Referència"

#. module: soller_crm
#: field:crm.case,create_uid:0
msgid "User"
msgstr "User"

