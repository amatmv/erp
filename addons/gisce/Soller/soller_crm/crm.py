# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields


class soller_crm(osv.osv):

    _name = 'crm.case'
    _inherit = 'crm.case'

    _order = 'id desc'

    def create(self, cr, uid, vals, context=None):
        """Asignamos el valor de polissa anterior si no nos lo pasan
        """

        if not vals.get('polissa_anterior_id', False):
            vals['polissa_anterior_id'] = vals.get('polissa_id', False)
        res_id = super(soller_crm,
                       self).create(cr, uid, vals, context)
        return res_id

    def create_case_generic(self, cursor, uid, model_ids, context=None,
                                  description='Generic Case',
                                  section='CASE',
                                  category=None,
                                  assign=None,
                                  extra_vals=None):

        case_ids = super(soller_crm, self).create_case_generic(
                                  cursor, uid, model_ids, context,
                                  description, section,
                                  category, assign, extra_vals)

        if not context:
            context = {}
        model = context.get('model', False)

        if model == 'giscedata.polissa':
            #Si el model es una polissa, afegim la referencia
            #del abonat anterior
            for case in self.browse(cursor, uid, case_ids, context):
                if not case.polissa_anterior_id:
                    polissa_id = case.ref.partition(',')[2]
                    case.write({'polissa_anterior_id': polissa_id})

        return case_ids

    def _get_ref_name(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        results = dict.fromkeys(ids, '')
        for case in self.browse(cursor, uid, ids, context):
            if case.ref:
                ref_info = case.ref.split(',')
                referenced_model = self.pool.get(ref_info[0])
                referenced_id = int(ref_info[1])
                referenced_name = referenced_model.name_get(
                    cursor, uid, (referenced_id,)
                )
                results[case.id] = referenced_name[0][1]
        return results

    _columns = {
        'polissa_anterior_id': fields.many2one('giscedata.polissa',
                                               'Abonat anterior',
                                               required=False),
        'delivered': fields.boolean('Entregat'),
        'delivered_date': fields.datetime("Data d'entrega"),
        'delivered_user_id': fields.many2one('res.users', 'Entregat a',
                                             required=False),
        'ref_name': fields.function(
            _get_ref_name, string='Referència', method=True, type="char",
            readonly=True,
        ),
    }

soller_crm()
