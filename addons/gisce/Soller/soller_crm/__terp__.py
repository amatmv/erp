# -*- coding: utf-8 -*-
{
    "name": "Extensió CRM Soller",
    "description": """
  * Report de ordre de feina i canvi de comptador
  * Referencia al segon abonat directament per contractació
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "crm_generic",
        "giscedata_polissa_crm",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_crm_data.xml",
        "soller_crm_view.xml",
        "soller_crm_report.xml"
    ],
    "active": False,
    "installable": True
}
