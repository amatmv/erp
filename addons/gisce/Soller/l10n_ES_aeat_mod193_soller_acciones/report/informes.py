# -*- coding: utf-8 -*-

import jasper_reports

def _mod193_report(cr, uid, ids, data, context):
    return {
        'ids': data['form']['ids'],
        'parameters': {'date': data['form']['date'],
                       'signer': data['form']['signer'],
                       'signer_vat': data['form']['signer_vat']},
            }

jasper_reports.report_jasper(
   'report.report_mod193_record',
   'l10n.es.aeat.mod193.record',
   parser=_mod193_report
)