# -*- coding: utf-8 -*-
{
    "name": "Modelo 193 desde acciones",
    "description": """
    This module provide :
        * Generación del modelo 193 a través de acciones.
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Localisation/Accounting",
    "depends":[
        "base",
        "soller_acciones",
        "l10n_ES_aeat_mod193"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "mod193_report.xml",
        "wizard/compute_mod193_view.xml",
        "wizard/wizard_mod193_record_report_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
