# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.misc import cache
import re

class WizardComputeMod193(osv.osv_memory):

    _name = 'wizard.compute.mod193'

    @cache()
    def _get_tipo(self, cursor, uid, code, context=None):

        liq_obj = self.pool.get('acciones.liquidacion.tipo')

        if not code:
            return False
        search_params = [('code', '=', code)]
        liq_ids = liq_obj.search(cursor, uid, search_params)
        return liq_ids and liq_ids[0] or False

    def _get_percent(self, cursor, uid, invoice, context=None):

        tax_obj = self.pool.get('account.tax')
        taxes = {}
        for tax_line in invoice.tax_line:
            search_params = [('name', '=', tax_line.name)]
            tax_ids = tax_obj.search(cursor, uid, search_params,
                                     context=context)
            if tax_ids:
                tax = tax_obj.browse(cursor, uid, tax_ids[0])
                return abs(tax.amount * 100)
        return False

    def action_compute(self, cursor, uid, ids, context=None):

        report_obj = self.pool.get('l10n.es.aeat.mod193.report')
        record_obj = self.pool.get('l10n.es.aeat.mod193.record')
        invoice_obj = self.pool.get('account.invoice')
        libro_linea_obj = self.pool.get('acciones.libro.linea')
        state_obj = self.pool.get('res.country.state')
        titular_obj = self.pool.get('acciones.titular')

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        report_id = context.get('report_id', 0)
        report = report_obj.browse(cursor, uid, report_id,
                                   context=context)
        date_start = report.fiscalyear_id.date_start
        date_end = report.fiscalyear_id.date_stop
        if wizard.recompute:
            search_params = [('report_id', '=', report_id)]
            record_ids = record_obj.search(cursor, uid, search_params)
            record_obj.unlink(cursor, uid, record_ids)
        #For each line in the libro search for invoices
        common_vals = {
                       'report_id': report.id,
                       'lr_vat': '',
                       'mediator': False,
                       'code_key': '1',
                       'code_value': report.company_vat,
                       'incoming_key': 'A',
                       'penalty': 0,
                       'nature': '02',
                       'payment': '1',
                       'code_type': 'O',
                       'code_bank': '',
                       'pending': ' ',
                       'fiscal_year_id': report.fiscalyear_id.id,
                       'incoming_type': '1'
                       }
        vat_records = {}
        #Search for all out invoices payed in the report fiscal year
        search_params = [('state', 'in', ('paid', 'open')),
                         ('type', '=', 'in_invoice'),
                         ('date_invoice', '>=', date_start),
                         ('date_invoice', '<=', date_end),]
        invoice_ids = invoice_obj.search(cursor, uid, search_params,
                                         context=context)
        for invoice_id in invoice_ids:
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            #Search for tipo liquidacion
            tipoliq_id = self._get_tipo(cursor, uid, invoice.origin,
                                         context=context)
            #Search for associated linea libro
            search_params = [('libro_id.liquidacion_id.id',
                              '=', tipoliq_id),
                             ('titular_cabecera', 'like', invoice.name)]
            linea_ids = libro_linea_obj.search(cursor, uid,
                                              search_params)
            if not linea_ids:
                continue
            linea_id = linea_ids[0]
            linea  = libro_linea_obj.browse(cursor, uid, linea_id)
            cabecera = libro_linea_obj.search_cabecera(cursor, uid,
                                linea, tipoliq_id, context=context)
            
            percent = self._get_percent(cursor, uid,
                            invoice, context=context)
            
            vals = {'amount': invoice.amount_untaxed,
                    'amount_base': invoice.amount_untaxed,
                    'amount_tax': abs(invoice.amount_tax)}
            #If copropietario we have to divide amount in 
            #as many lines as relations in line
            if linea.relacion_cabecera == 'Copropietario':
                counter = 0
                total = len([x.id for x in linea.relacion_ids
                             if not x.cabecera])
                tmp_vals = vals.copy()
                ind_amount = round(vals['amount'] / total, 2)
                ind_base = round(vals['amount_base'] / total, 2)
                ind_tax = round(vals['amount_tax'] / total, 2)
                for relacion in linea.relacion_ids:
                    #Do nothing for cabecera relacion
                    if relacion.cabecera:
                        continue
                    record_vals = common_vals.copy()
                    counter += 1
                    tmp_vals['amount'] -= ind_amount
                    tmp_vals['amount_base'] -= ind_base
                    tmp_vals['amount_tax'] -= ind_tax
                    if counter < total:
                        record_vals.update({'amount': ind_amount,
                                       'amount_base': ind_base,
                                       'amount_tax': ind_tax,
                                       })
                    else:
                        #The last one receives the adjust
                        #from decimal rounding 
                        record_vals.update({
                               'amount': ind_amount + tmp_vals['amount'],
                               'amount_base': ind_base + tmp_vals['amount_base'],
                               'amount_tax': ind_tax + tmp_vals['amount_tax'],
                                       })
                    #Search for state_id
                    if (relacion.fiscal_address_id.state_id and
                        relacion.fiscal_address_id.country_id.code == 'ES'):
                        state_id = (relacion.fiscal_address_id.
                                    state_id.id)
                    else:
                        #Search for default state_id.
                        #Code 07 and country code ES
                        search_params = [('code', '=', '07'),
                                         ('country_id.code', '=', 'ES')]
                        state_id = state_obj.search(cursor, uid, search_params,
                                                    context=context)[0]
                        
                    record_vals.update({
                       'partner_id': relacion.titular_id.partner_id.id,
                       'partner_vat': (relacion.partner_vat and
                                       re.match("(ES){0,1}(.*)",
                                        relacion.partner_vat).groups()[1]),
                       'state_id': state_id,
                       'tax_percent': percent,
                                   })
                    if relacion.partner_vat in vat_records:
                        vat = relacion.partner_vat
                        vat_vals = {
                            'amount': (vat_records[vat]['amount'] +
                                       record_vals['amount']),
                            'amount_base': (vat_records[vat]['amount_base'] +
                                            record_vals['amount_base']),
                            'amount_tax': (vat_records[vat]['amount_tax'] +
                                           record_vals['amount_tax']),
                            'tax_percent': record_vals['tax_percent'],
                            'state_id': record_vals['state_id'],
                            }
                        vat_records[vat].update(vat_vals)
                    else:
                        vat_records[relacion.partner_vat] = record_vals.copy()
                    #record_obj.create(cursor, uid, record_vals, context=context)
            else:
                #Search for state_id
                if (cabecera.fiscal_address_id.state_id and
                    cabecera.fiscal_address_id.country_id.code == 'ES'):
                    state_id = (cabecera.fiscal_address_id.
                                state_id.id)
                else:
                    #Search for default state_id.
                    #Code 07 and country code ES
                    search_params = [('code', '=', '07'),
                                     ('country_id.code', '=', 'ES')]
                    state_id = state_obj.search(cursor, uid, search_params,
                                                context=context)[0]
                record_vals = common_vals.copy()
                record_vals.update({
                        'amount': vals['amount'],
                        'amount_base': vals['amount_base'],
                        'amount_tax': vals['amount_tax'],
                        'partner_id': cabecera.titular_id.partner_id.id,
                        'partner_vat': (cabecera.partner_vat and
                                        re.match("(ES){0,1}(.*)",
                                        cabecera.partner_vat).groups()[1]),
                        'state_id': state_id,
                        'tax_percent': percent,
                                       })
                if cabecera.partner_vat in vat_records:
                    vat = cabecera.partner_vat
                    vat_vals = {
                        'amount': (vat_records[vat]['amount'] +
                                   record_vals['amount']),
                        'amount_base': (vat_records[vat]['amount_base'] +
                                        record_vals['amount_base']),
                        'amount_tax': (vat_records[vat]['amount_tax'] +
                                       record_vals['amount_tax']),
                        'tax_percent': record_vals['tax_percent'],
                        'state_id': record_vals['state_id'],
                        }
                    vat_records[vat].update(vat_vals)
                else:
                    vat_records[cabecera.partner_vat] = record_vals.copy()
                #record_obj.create(cursor, uid, record_vals, context=context)   

        for vat, values in vat_records.iteritems():
            #search addresses to store in record
            search_params = [('partner_vat', '=', vat),
                             ('grupo', '=', False)]
            titular_ids = titular_obj.search(cursor, uid, search_params,
                                             context=context)
            if titular_ids:
                read = titular_obj.read(cursor, uid, titular_ids,
                                        ['address_id', 'fiscal_address_id'])
                values.update({'postal_address_id': read[0]['address_id'][0],
                               'fiscal_address_id':
                                read[0]['fiscal_address_id'][0],
                               })
            record_obj.create(cursor, uid, values, context=context) 
        #Write state computed in report
        report.write({'state': 'computed'})
        return {}

    _columns = {
        'recompute': fields.boolean('Recompute',
                                    help=u"If checked, all the records in the "
                                         u"report will be deleted before "
                                         u"creating new ones ")
    }
WizardComputeMod193()