# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import time


class WizardMod193RecordReport(osv.osv_memory):

    _name = "wizard.mod193.record.report"

    def _get_default_signer(self, cursor, uid, context=None):
        '''search for partner with category signer'''
        partner_obj = self.pool.get('res.partner')
        search_params = [('category_id.name', '=', 'signer')]

        partner_ids = partner_obj.search(cursor, uid, search_params)
        if partner_ids:
            return partner_obj.read(cursor, uid, partner_ids,
                                    ['name'])[0]['name']
        return False

    def _get_default_signer_vat(self, cursor, uid, context=None):
        '''search for partner with category signer'''
        partner_obj = self.pool.get('res.partner')
        search_params = [('category_id.name', '=', 'signer')]

        partner_ids = partner_obj.search(cursor, uid, search_params)
        if partner_ids:
            return partner_obj.read(cursor, uid, partner_ids,
                                    ['vat'])[0]['vat'][2:]
        return False

    def action_print(self, cr, uid, ids, context=None):
        
        wizard = self.browse(cr, uid, ids[0])

        datas = {'form':{'date': wizard.date,
                         'signer': wizard.signer,
                         'signer_vat': wizard.signer_vat,
                         'ids': context.get('active_ids', []),
                        }
                }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'report_mod193_record',
            'datas': datas,
        }

    _columns = {
        'date': fields.char('Date', size=64, required=True),
        'signer': fields.char('Signer', size=64, required=True),
        'signer_vat': fields.char('Signer vat', size=12, required=True),
                
    }

    _defaults = {
        'signer': _get_default_signer,
        'signer_vat': _get_default_signer_vat,
    }

WizardMod193RecordReport()
