# -*- coding: utf-8 -*-
{
    "name": "Soller Contractació distribució",
    "description": """
Contractacio de distribució:
    * Report de contracte de pòlissa
    * Control i càlcul de conceptes
    * Facturació de conceptes per series de facturació
    * Report resum per electricistes
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/soller_contractacio_distri_security.xml",
        "security/ir.model.access.csv",
        "soller_contractacio_distri_sequence.xml",
        "giscedata_contractes_acces_data.xml",
        "soller_contractacio_distri_data.xml",
        "soller_contractacio_distri_report.xml",
        "wizard/wizard_soller_contractacio_distri_calcular_view.xml",
        "wizard/wizard_config_taxes_view.xml",
        "soller_contractacio_distri_view.xml"
    ],
    "active": False,
    "installable": True
}
