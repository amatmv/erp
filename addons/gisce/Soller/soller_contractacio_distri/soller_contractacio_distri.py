# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time
from datetime import datetime


class SollerContractacioDistri(osv.osv):
    '''
    Modul de contractacio de distribucio
    '''
    _name = 'soller.contractacio.distri'

    def check_prefianza(self, cr, uid, ids, context=None):
        '''Fem algunes comprovacions abans de calcular la fianza'''

        for contractacio in self.browse(cr, uid, ids):
            #La data de la contractacio es fa servir pel calcul
            if not contractacio.date:
                raise osv.except_osv('Error',
                            'Falta la data de la contractacio')
            #La tarifa i la llista de preus son obligatories
            if not contractacio.polissa_id.tarifa:
                raise osv.except_osv('Error',
                            'Falta la tarifa d\'accés a la pòlissa')
            if not contractacio.polissa_id.llista_preu:
                raise osv.except_osv('Error',
                            'Falta la llista de preus a la pòlissa')

        return True

    def calcul_fianza(self, cr, uid, ids, context=None):
        '''Calculem la fianza'''

        polissa_obj = self.pool.get('giscedata.polissa')
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        product_obj = self.pool.get('product.product')

        if not context:
            context = {}

        #Fem algunes comprovacions
        self.check_prefianza(cr, uid, ids, context)

        #Cerquem el id del producte fianza
        search_params = [('default_code', '=', 'FIANZA')]
        product_id = product_obj.search(cr, uid, search_params)[0]

        calcul = {'2.0A': ['P1'],
                  '2.0DHA': ['P1', 'P2'],
                  '2.1A': ['P1'],
                  '2.1DHA': ['P1', 'P2'],
                  '3.0A': ['P2'],
                  '3.1A': ['P2'],
                  '6.1': ['P3'],
                  '6.1A': ['P3'],
                  '6.1B': ['P3'],
                 }

        res = 0
        result = {}
        for contractacio in self.browse(cr, uid, ids):
            context['date'] = contractacio.date
            polissa = polissa_obj.browse(cr, uid, contractacio.polissa_id.id)
            #calculem el terme de potencia
            search_params = [('tarifa', '=', polissa.tarifa.id),
                         ('name', 'in', calcul[polissa.tarifa.name]),
                         ('tipus', '=', 'tp')]
            periodes_pot_ids = periode_obj.search(cr, uid, search_params)
            for period in periode_obj.browse(cr, uid, periodes_pot_ids):
                tarifa = polissa.llista_preu
                preu = tarifa.price_get(period.product_id.id,
                                        1,
                                        polissa.titular.id,
                                        context=context)[tarifa.id]
                res += (preu / 12) * contractacio.potencia_nova
            #calculem el terme de energia sobre una base de 50 kws
            search_params = [('tarifa', '=', polissa.tarifa.id),
                         ('name', 'in', calcul[polissa.tarifa.name]),
                         ('tipus', '=', 'te')]
            periodes_ener_ids = periode_obj.search(cr, uid, search_params)
            for period in periode_obj.browse(cr, uid, periodes_ener_ids):
                tarifa = polissa.llista_preu
                preu = tarifa.price_get(period.product_id.id,
                                        1,
                                        polissa.titular.id,
                                        context=context)[tarifa.id]
                if len(periodes_ener_ids) > 1:
                    res += contractacio.potencia_nova * 25 * preu
                else:
                    res += contractacio.potencia_nova * 50 * preu
            result[contractacio.id] = {'preu': res, 'product_id': product_id}

        return result

    def _get_potencia(self, cr, uid, ids, name, arg, context=None):
        '''funcio que retorna la diferencia entre
        la potencia nova i la anterior'''

        res = {}
        for contractacio in self.browse(cr, uid, ids):
            res[contractacio.id] = (contractacio.potencia_nova
                                    - contractacio.potencia_anterior
                                    - contractacio.potencia_anterior2)
        return res

    def get_preu(self, cr, uid, ids, dret, context=None):
        '''Preu de quota de extensio, access i drets
        de enganche,verificacio i actuacio
        retorna un diccionari amb preu i product_id'''

        polissa_obj = self.pool.get('giscedata.polissa')
        product_obj = self.pool.get('product.product')

        _map_drets = {
                  'extensio': ['DEBT', 'DEAT0', 'DEAT1', 'DEAT2'],
                  'acces': ['DABT', 'DAAT0', 'DAAT1', 'DAAT2'],
                  'enganche': ['DENBT', 'DENAT0', 'DENAT1', 'DENAT2'],
                  'verificacio': ['DVBT', 'DVAT0', 'DVAT1', 'DVAT2'],
                  'actuacio': ['DAMICBT', 'DAMICAT0', 'DAMICAT1', 'DAMICAT2'],
                  'cedidas': ['DSBT', 'DSLAT', 'DSAT1', 'DSAT2'],
                     }

        res = {}
        for contractacio in self.browse(cr, uid, ids):
            if (contractacio.polissa_id.tarifa.tipus == 'BT'):
                search_params = [('default_code', '=', _map_drets[dret][0])]
                product_id = product_obj.search(cr, uid, search_params)[0]
                preu = product_obj.read(cr, uid, product_id,
                                        ['list_price'], context)['list_price']
                res[contractacio.id] = {'preu': preu, 'product_id': product_id}
            else:
                tensio = contractacio.polissa_id.tensio
                if (tensio >= 0) and (tensio <= 36000):
                    search_params = [('default_code',
                                      '=', _map_drets[dret][1])]
                elif (tensio > 36000) and (tensio <= 72500):
                    search_params = [('default_code',
                                      '=', _map_drets[dret][2])]
                else:
                    search_params = [('default_code',
                                      '=', _map_drets[dret][3])]
                product_id = product_obj.search(cr, uid, search_params)[0]
                preu = product_obj.read(cr, uid, product_id,
                                        ['list_price'], context)['list_price']
                res[contractacio.id] = {'preu': preu, 'product_id': product_id}
        return res

    def get_preu_vequip(self, cr, uid, ids, context=None):
        '''Preu de la verificacio del equip de
        messura segons si es trifasic o monofasic'''

        polissa_obj = self.pool.get('giscedata.polissa')
        product_obj = self.pool.get('product.product')

        res = {}
        for contractacio in self.browse(cr, uid, ids):
            tensio = contractacio.polissa_id.tensio_normalitzada.name
            if tensio.startswith('3x'):
                #tensio trifasica
                search_params = [('default_code', '=', 'DVCT')]
            else:
                #tensio monofasica
                search_params = [('default_code', '=', 'DVCM')]
            product_id = product_obj.search(cr, uid, search_params)[0]
            preu = product_obj.read(cr, uid, product_id,
                                    ['list_price'], context)['list_price']
            res[contractacio.id] = {'preu': preu, 'product_id': product_id}
        return res

    def get_preu_vicp(self, cr, uid, ids, context=None):
        '''Preu de la verificacio del ICP'''

        polissa_obj = self.pool.get('giscedata.polissa')
        product_obj = self.pool.get('product.product')

        res = {}
        for contractacio in self.browse(cr, uid, ids):
            search_params = [('default_code', '=', 'DVICP')]
            product_id = product_obj.search(cr, uid, search_params)[0]
            preu = product_obj.read(cr, uid, product_id,
                                    ['list_price'], context)['list_price']
            res[contractacio.id] = {'preu': preu, 'product_id': product_id}
        return res

    def get_preu_complocal(self, cr, uid, ids, context=None):
        '''Preu de la compensacio local'''

        polissa_obj = self.pool.get('giscedata.polissa')
        product_obj = self.pool.get('product.product')

        res = {}
        for contractacio in self.browse(cr, uid, ids):
            search_params = [('default_code', '=', 'COMPL')]
            product_id = product_obj.search(cr, uid, search_params)[0]
            preu = product_obj.read(cr, uid, product_id,
                                    ['list_price'], context)['list_price']
            res[contractacio.id] = {'preu': preu, 'product_id': product_id}
        return res

    def get_tipus(self, cr, uid, contractacio_id, context=None):
        '''Treiem els diferents tipus de linies que hem generat'''

        if isinstance(contractacio_id, (list, tuple)):
            contractacio_id = contractacio_id[0]

        contractacio = self.browse(cr, uid, contractacio_id)
        res = {}
        for linia in contractacio.linies_ids:
            res[linia.tipus_id.id] = linia.tipus_id.journal_id.id
        return res

    def get_product_taxes(self, cr, uid, product_id, context=None):
        '''Retorna un diccionari amb els impostos associats a un producte'''

        product_obj = self.pool.get('product.product')
        if isinstance(product_id, list) or isinstance(product_id, tuple):
            product_id = product_id[0]
        res = {}
        for tax in product_obj.browse(cr, uid, product_id).taxes_id:
            res[tax.id] = True
        return res.keys()

    def check_prefacturar(self, cr, uid, ids, context=None):
        '''funcio per fer comprovacions abans de facturar'''

        for contractacio in self.browse(cr, uid, ids):
            #Comprovem si ja tenim factures emeses
            if contractacio.factures_ids:
                raise osv.except_osv('Error',
                                     u"Ja hi han factures emeses "
                                     u"d\'aquesta contractació")
            #Check if we already have extra lines created
            if contractacio.extra_ids:
                raise osv.except_osv('Error',
                                     u"Ja hi han línies extra "
                                     u"d\'aquesta contractació")
            #Comprovem que tenim pagador i adreça de pagador
            if contractacio.factura_comer:
                if not contractacio.polissa_id.pagador\
                    or not contractacio.polissa_id.direccio_pagament:
                    raise osv.except_osv('Error',
                                         u"Falten les dades de la "
                                         u"comercialitzadora per facturar")
            else:
                if not contractacio.titular_id\
                    or not contractacio.direccio_factura_id:
                    raise osv.except_osv('Error',
                            'Falten les dades del pagador per facturar')

        return True

    def action_facturar_extra(self, cursor, uid, contractacio_id,
                              context=None):
        '''genera linies extra de factures per incloure la contractacio dins
        la factura electrica i no fer factures separades'''

        if not context:
            context = {}
        if isinstance(contractacio_id, (list, tuple)):
            contractacio_id = contractacio_id[0]

        contractacio = self.browse(cursor, uid, contractacio_id,
                                   context=context)
        extra_obj = self.pool.get('giscedata.facturacio.extra')
        journal_obj = self.pool.get('account.journal')
        #Search for default journal. The one with code ENERGIA
        search_params = [('code', '=', 'ENERGIA')]
        journal_id = journal_obj.search(cursor, uid, search_params)
        #For each line create an extra line
        extra_ids = []
        polissa = contractacio.polissa_id
        date = datetime.strftime(datetime.now(), '%Y-%m-%d')
        for linia in contractacio.linies_ids:
            product = linia.product_id
            account_id = (product.property_account_income and
                         product.property_account_income.id
                         or (product.categ_id.
                             property_account_income_categ.id))
            tax_ids = self.get_product_taxes(cursor, uid, product.id,
                                             context=context)
            vals = {'name': product.name,
                    'polissa_id': contractacio.polissa_id.id,
                    'product_id': product.id,
                    'uos_id': product.uom_id.id,
                    'date_from': date,
                    'date_to': date,
                    'quantity': linia.quantitat,
                    'price_unit': linia.preu_unitari,
                    'term': 1,
                    'account_id': account_id,
                    'tax_ids': [(6, 0, tax_ids)],
                    'journal_ids': [(6, 0, journal_id)]
                    }
            extra_ids.append(extra_obj.create(cursor, uid, vals))
        #Store created extra lines
        contractacio.write({'extra_ids': [(6, 0, extra_ids)]})

        return True

    def action_facturar_giscedata(self, cr, uid, ids, context=None):
        '''generem les factures segons les linies generades en el calcul
        utilitzant el model de factura de gisce'''

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura_linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        inv_line_obj = self.pool.get('account.invoice.line')
        payment_term_obj = self.pool.get('account.payment.term')
        today = time.strftime('%Y-%m-%d')

        #Fem les comprovacions previes
        self.check_prefacturar(cr, uid, ids, context)

        for contractacio in self.browse(cr, uid, ids):
            tipus_ids = contractacio.get_tipus(contractacio.id)
            polissa = contractacio.polissa_id
            factures_ids = {}
            # If only FIANZA to invoice, change tipo_factura to 05
            tipo_factura = '04'
            concepts = [x.code for x in contractacio.conceptes_ids]
            if len(concepts) == 1 and concepts[0] == 'fianza':
                tipo_factura = '05'
            for tipus in tipus_ids:
                #Per cada tipus generem una factura
                vals = {'date_invoice': today,
                        'date_due': today,
                        'data_inici': today,
                        'data_final': today,
                        'polissa_id': polissa.id,
                        'type': 'out_invoice',
                        'journal_id': tipus_ids[tipus],
                        'partner_id': contractacio.titular_id.id,
                        'address_invoice_id': (contractacio.
                                               direccio_factura_id.id),
                        'account_id': (polissa.titular.
                                       property_account_receivable.id),
                        'tarifa_acces_id': polissa.tarifa.id,
                        'llista_preu': polissa.llista_preu.id,
                        'date_boe': facturador_obj.get_data_boe(cr, uid),
                        'facturacio': 1,
                        'cups_id': polissa.cups.id,
                        'potencia': polissa.potencia,
                        'tipo_factura': tipo_factura,
                       }
                # Si hem marcat que hem de facturar a la comercialitzadora
                # hem d'agafar aquestes per facturar
                if contractacio.factura_comer:
                    vals['partner_id'] = polissa.pagador.id
                    vals['address_invoice_id'] = polissa.direccio_pagament.id
                partner_vals = factura_obj.onchange_partner_id(cr, uid, [],
                                                               'out_invoice',
                                                               vals['partner_id'])
                vals.update(partner_vals['value'])
                factures_ids[tipus] = factura_obj.create(cr, uid, vals,
                                                         context)
            
            #Una volta generades les factures, anem creant les linies
            for linia in contractacio.linies_ids:
                linia_vals = {
                    'data_desde': today,
                    'data_fins': today,
                    'factura_id': factures_ids[linia.tipus_id.id],
                    'product_id': linia.product_id.id,
                    'quantity': linia.quantitat,
                    'tipus': 'altres',
                    'multi': 1,
                }
                linia_vals.update(inv_line_obj.product_id_change(cr, uid, [],
                    linia.product_id.id,
                    linia.product_id.uom_id.id,
                    linia.quantitat,
                    partner_id=polissa.titular.id,
                    fposition_id=(polissa.titular.
                                  property_account_position.id))['value'])
                linia_vals['price_unit_multi'] = linia.preu_unitari
                product_taxes = self.get_product_taxes(cr, uid,
                                                       linia.product_id.id)
                linia_vals['invoice_line_tax_id'] = [(6, 0, product_taxes)]
                factura_linia_obj.create(cr, uid, linia_vals, context)
            # Despues de crear les linies hem de cridar
            # la funcio per calcular els impostos
            factura_obj.button_reset_taxes(cr, uid, factures_ids.values())
            # Guardem les factures generades al
            # nostre model per no perdre la relacio
            contractacio.write({'factures_ids': [(6, 0,
                                                  factures_ids.values())]})
        return True

    def action_facturar(self, cr, uid, ids, context=None):
        '''generem les factures segons les linies generades en el calcul'''

        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')

        for contractacio in self.browse(cr, uid, ids):
            tipus_ids = contractacio.get_tipus(contractacio.id)
            polissa = contractacio.polissa_id
            invoice_ids = {}
            for tipus in tipus_ids:
                #Per cada tipus generem una factura
                vals = {'date_invoice': contractacio.date,
                        'reference': polissa.name,
                        'type': 'out_invoice',
                        'journal_id': tipus_ids[tipus],
                        'partner_id': contractacio.titular_id.id,
                        'address_invoice_id': (contractacio.
                                               direccio_factura_id.id),
                        'origin': polissa.name,
                        'account_id': (polissa.titular.
                                       property_account_receivable.id),
                       }
                invoice_ids[tipus] = invoice_obj.create(cr, uid,
                                                        vals, context)
            #Una volta generades les factures, anem creant les linies
            for linia in contractacio.linies_ids:
                linia_vals = {
                    'invoice_id': invoice_ids[linia.tipus_id.id],
                    'product_id': linia.product_id.id,
                    'quantity': linia.quantitat,
                }
                linia_vals.update(invoice_line_obj.product_id_change(cr, uid,
                    [],
                    linia.product_id.id,
                    linia.product_id.uom_id.id,
                    linia.quantitat,
                    partner_id=polissa.titular.id,
                    fposition_id=(polissa.titular.
                                  property_account_position.id)['value']))
                linia_vals['price_unit'] = linia.preu_unitari
                product_taxes = self.get_product_taxes(cr, uid,
                                                       linia.product_id.id)
                linia_vals['invoice_line_tax_id'] = [(6, 0, product_taxes)]
                invoice_line_obj.create(cr, uid, linia_vals, context)
        # Despues de crear les linies hem de cridar
        # la funcio per calcular els impostos
        invoice_obj.button_reset_taxes(cr, uid, invoice_ids.values())

        # Guardem les factures generades al nostre
        # model per no perdre la relacio
        contractacio.write({'factures_ids': [(6, 0, invoice_ids.values())]})

    def get_canvi_id(self, cr, uid, ids, canvis, context=None):
        '''retorna el id del canvi segons la cadena que li passem'''
        canvi_obj = self.pool.get('soller.contractacio.distri.canvi')

        ids = []
        for canvi in canvis:
            search_params = [('name', '=', canvi)]
            canvi_id = canvi_obj.search(cr, uid, search_params)
            if canvi_id:
                ids.append(canvi_id[0])
        return ids

    def action_get_changes(self, cr, uid, ids, context=None):
        '''detecta els canvis de tensio, titular
        i potencia entre dues polisses'''

        contractacio = self.browse(cr, uid, ids[0])
        polissa_obj = self.pool.get('giscedata.polissa')

        canvis = []

        if not contractacio.polissa_anterior_id:
            canvis.append('Nuevo')
        else:
            polissa = contractacio.polissa_id
            polissa_ant = contractacio.polissa_anterior_id
            if polissa.titular != polissa_ant.titular:
                canvis.append('Cambio de titular')
            if polissa.potencia != polissa_ant.potencia:
                canvis.append('Cambio de potencia')
            if polissa.tensio_normalitzada != polissa_ant.tensio_normalitzada:
                canvis.append('Cambio de tensión')
        contractacio.write({'canvis_ids': [(6, 0,
                                    contractacio.get_canvi_id(canvis))]})

    def action_mostrar_factures(self, cr, uid, ids, context=None):

        contractacio_obj = self.pool.get('soller.contractacio.distri')
        contractacio_id = ids[0]
        contractacio = contractacio_obj.browse(cr, uid, contractacio_id)

        factures = []
        for factura in contractacio.factures_ids:
            factures.append(factura.id)

        vals = {
            'name': 'Factures creades',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'view_id': False,
            'type': 'ir.actions.act_window',
        }
        vals['domain'] = [('id', 'in', factures)]

        return vals

    def action_mostrar_extra(self, cursor, uid, ids, context=None):

        extra_ids = []
        for contractacio in self.browse(cursor, uid, ids, context=context):
            extra_ids.extend([x.id for x in contractacio.extra_ids])

        return {
            'name': 'Línies creades',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.extra',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', extra_ids)]
        }

    def action_informe(self, cr, uid, ids, context=None):
        '''accio per imprimir el informe de la contractacio'''

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'report_informe_contractacio_webkit',
        }

    def action_ordre(self, cr, uid, ids, context=None):
        '''accio per crear una ordre de feina'''

        crm_case_obj = self.pool.get('crm.case')
        crm_section_obj = self.pool.get('crm.case.section')

        #la seccio de moment sera sempre Ordre General (OG)
        search_params = [('code', '=', 'OG')]
        section_id = crm_section_obj.search(cr, uid, search_params)[0]

        for contractacio in self.browse(cr, uid, ids):
            vals = {'name': contractacio.canvis_str,
                    'description': contractacio.name,
                    'section_id': section_id,
                    'polissa_id': contractacio.polissa_id.id,
                   }
            if contractacio.polissa_anterior_id.id:
                vals['polissa_anterior_id'] = (contractacio.
                                               polissa_anterior_id.id)
            else:
                vals['polissa_anterior_id'] = False
            if contractacio.electricista_id.id:
                vals['partner_id'] = contractacio.electricista_id.id
            if contractacio.contact_electricista_id.id:
                vals['partner_address_id'] = (contractacio.
                                              contact_electricista_id.id)
            case_id = crm_case_obj.create(cr, uid, vals, context)

        vals = {
            'name': 'Ordre creada',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.case',
            'view_id': False,
            'type': 'ir.actions.act_window',
        }
        vals['domain'] = [('id', '=', case_id)]

        return vals

    def _get_titular(self, cr, uid, context=None):

        polissa_id = context.get('polissa_id', False)
        if polissa_id:
            return self.pool.get('giscedata.polissa').browse(cr,
                                                uid, polissa_id).titular.id

        return False

    _columns = {
        'name': fields.char('Nom', size=64, required=True, readonly=False),
        'polissa_id': fields.many2one('giscedata.polissa', 'Abonat',
                                      required=True),
        'polissa_anterior_id': fields.many2one('giscedata.polissa',
                                               'Primer Abonat Anterior',
                                               required=False),
        'polissa_anterior2_id': fields.many2one('giscedata.polissa',
                                                'Segon Abonat Anterior',
                                                required=False),
        'num_it': fields.char('SS', size=64, required=False, readonly=False),
        'titular_id': fields.many2one('res.partner', 'Titular',
                                      required=False),
        'direccio_factura_id': fields.many2one('res.partner.address',
                                               'Adreça factura',
                                               required=False),
        'date': fields.date('Data'),
        'potencia_nova': fields.related('polissa_id', 'potencia',
                            type='float', digits=(16, 3),
                            relation='giscedata.polissa', readonly=True,
                            string='Potència nova', store=False),
        'potencia_anterior': fields.related('polissa_anterior_id',
                            'potencia', type='float', digits=(16, 3),
                            relation='giscedata.polissa', readonly=True,
                            string='Pot. anterior', store=False),
        'potencia_anterior2': fields.related('polissa_anterior2_id',
                            'potencia', type='float', digits=(16, 3),
                            relation='giscedata.polissa', readonly=True,
                            string='Pot2. anterior', store=False),
        'potencia_resultant': fields.function(_get_potencia, method=True,
                                   type='float', digits=(16, 3),
                                   string='Potència resultant', store=False),
        'observacions': fields.text('Observacions'),
        'factura_comer': fields.boolean('Facturar a comercialitzadora',
                                       required=False),
        'electricista_id': fields.many2one('res.partner', 'Instal·lador',
                              required=False,
                              domain=[('category_id', '=', 'Instalador')]),
        'contact_electricista_id': fields.many2one('res.partner.address',
                                                   'Contacte Inst.',
                                                   required=False),
    }

    _defaults = {
        'potencia_anterior': lambda *a: 0,
        'potencia_anterior2': lambda *a: 0,
        'factura_comer': lambda *a: False,
        'titular_id': _get_titular,
    }

SollerContractacioDistri()


class SollerContractacioDistriTipus(osv.osv):
    '''Tipus de concepte de contractacio'''

    _name = 'soller.contractacio.distri.tipus'

    _columns = {
        'name': fields.char('Nom', size=64, required=True, readonly=False),
        'journal_id': fields.many2one('account.journal', 'Diari facturació',
                                     required=True),
    }

SollerContractacioDistriTipus()


class SollerContractacioDistriConcepte(osv.osv):
    '''Diferents conceptes per facturar al contractar una polissa'''

    _name = 'soller.contractacio.distri.concepte'

    _columns = {
        'name': fields.char('Nom', size=64, required=True, readonly=False),
        'code': fields.char('Codi', size=20, required=True, readonly=False),
        'tipus_id': fields.many2one('soller.contractacio.distri.tipus',
                                    'Tipus', required=True),
    }

SollerContractacioDistriConcepte()


class SollerContractacioDistriCanvis(osv.osv):
    '''Diferents canvis que poden tindre lloc'''

    _name = 'soller.contractacio.distri.canvi'

    _columns = {
        'name': fields.char('Nom', size=64, required=True, readonly=False),
    }

SollerContractacioDistriCanvis()


class SollerContractacioDistriLinia(osv.osv):

    _name = 'soller.contractacio.distri.linia'

    def on_change_preu(self, cr, uid, ids, quantitat,
                       preu_unitari, context=None):
        """Calculamos el total si se cambia la quantitat o el preu_unitari"""
        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not quantitat or not preu_unitari:
            res['warning'].update({'title': 'Avís',
                                   'message': u"La quantitat o el preu"
                                              u" no poden estar en blanc"})
        else:
            res['value'].update({'total': quantitat * preu_unitari})
        return res

    def _get_impost_linia(self, cr, uid, ids, name, arg, context=None):

        tax_obj = self.pool.get('account.tax')
        res = {}
        for linia in self.browse(cr, uid, ids, context):
            res[linia.id] = {'impost': 0,
                             'totalimpost': 0}
            for tax in linia.product_id.taxes_id:
                res[linia.id]['impost'] = (res[linia.id]['impost'] +
                                           round(linia.total * tax.amount, 2))
            res[linia.id]['totalimpost'] = (linia.total +
                                            res[linia.id]['impost'])

        return res

    _columns = {
        'auto': fields.boolean('Automàtica', required=False,
                              help=u"Desmarqui aquesta casella si "
                                   u"introdueix una línia manualment",),
        'product_id': fields.many2one('product.product', 'Producto',
                                     required=False),
        'contractacio_id': fields.many2one('soller.contractacio.distri',
                                          'Contractacio', required=True,
                                          ondelete='cascade'),
        'quantitat': fields.float('Quantitat', digits=(16, 3)),
        'preu_unitari': fields.float('Preu', digits=(16, 6)),
        'total': fields.float('Subtotal', digits=(16, 2)),
        'tipus_id': fields.many2one('soller.contractacio.distri.tipus',
                                   'Tipus', required=True),
        'impost': fields.function(_get_impost_linia,
                       method=True, type='float',
                       digits=(16, 2), string='Impost',
                       store={'soller.contractacio.distri.linia':
                              (lambda self, cr, uid, ids, c={}: ids,
                               ['quantitat', 'preu_unitari', 'total'], 20),
                              },
                       multi='impostos'),
        'totalimpost': fields.function(_get_impost_linia,
                       method=True, type='float',
                       digits=(16, 2), string='Total',
                       store={'soller.contractacio.distri.linia':
                              (lambda self, cr, uid, ids, c={}: ids,
                               ['quantitat', 'preu_unitari', 'total'], 20),
                             },
                       multi='impostos'),
    }

    _defaults = {
        'auto': lambda *a: True,
    }

SollerContractacioDistriLinia()


class SollerContractacioDistri(osv.osv):

    _name = 'soller.contractacio.distri'
    _inherit = 'soller.contractacio.distri'

    def _get_canvis_str(self, cr, uid, ids, name, arg, context=None):
        '''Retorna una cadena amb els canvis associats a una contractacio'''

        res = {}
        for contractacio in self.browse(cr, uid, ids):
            str_canvis = ''
            for canvi in contractacio.canvis_ids:
                str_canvis += '%s, ' % (canvi.name.upper())
            res[contractacio.id] = str_canvis

        return res

    _columns = {
        'linies_ids': fields.one2many('soller.contractacio.distri.linia',
                                     'contractacio_id', 'Drets',
                                     required=False),
        'conceptes_ids': fields.many2many(
                         'soller.contractacio.distri.concepte',
                         'soller_contractacio_distri_contractacio_concepte',
                         'contractacio_id', 'concepte_id', 'Conceptes'),
        'canvis_ids': fields.many2many('soller.contractacio.distri.canvi',
                           'soller_contractacio_distri_contractacio_canvi_rel',
                           'contractacio_id', 'canvi_id', 'Canvis'),
        'factures_ids': fields.many2many('giscedata.facturacio.factura',
                         'soller_contractacio_distri_contractacio_factura_rel',
                         'contractacio_id', 'factura_id',
                         'Factures', readonly=True),
        'extra_ids': fields.many2many('giscedata.facturacio.extra',
                                      'soller_contractacio_distri_extra_rel',
                                      'contractacio_id', 'extra_id',
                                      'Línies extra', readonly=True),
        'canvis_str': fields.function(_get_canvis_str, method=True,
                                      type='char', size=128,
                                       string='Nom canvis', store=False),

    }

SollerContractacioDistri()
