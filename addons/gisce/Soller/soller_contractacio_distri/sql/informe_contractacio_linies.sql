SELECT
  line.id           AS line_id,
  line.impost       AS impost,
  line.preu_unitari AS preu_unitari,
  line.quantitat    AS quantitat,
  line.total        AS total,
  line.totalimpost  AS totalimpost,
  pt.name           AS descripcion,
  tipus.name        AS concepto,
  tax.id            AS tax_id
FROM soller_contractacio_distri_linia line
  INNER JOIN product_product product ON line.product_id = product.id
  INNER JOIN product_template pt ON product.product_tmpl_id = pt.id
  INNER JOIN soller_contractacio_distri_tipus tipus ON line.tipus_id = tipus.id
  LEFT JOIN product_taxes_rel tax_rel ON pt.id = tax_rel.prod_id
  LEFT JOIN account_tax tax ON tax_rel.tax_id = tax.id
WHERE contractacio_id = %s
