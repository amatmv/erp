SELECT
  contract.id                                AS contract_id,
  contract.date                              AS date,
  contract.factura_comer                     AS factura_comer,
  contract.num_it                            AS num_it,
  contract.observacions                      AS observacions,
  COALESCE(electricista_addr.mobile, '')     AS electricista_mobile,
  COALESCE(electricista_addr.phone, '')      AS electricista_phone,
  COALESCE(electricista.name, '')            AS electricista_name,
  invoice_addr_municipi.name                 AS direccio_factura_municipi,
  polissa_anterior.name                      AS polissa_anterior_name,
  polissa_anterior2.name                     AS polissa_anterior2_name,
  polissa.name                               AS polissa_name,
  polissa.potencia                           AS potencia_nova,
  polissa_cups.direccio                      AS polissa_cups_direccio,
  polissa_cups.dp                            AS polissa_cups_dp,
  polissa_cups.name                          AS polissa_cups_name,
  COALESCE(polissa_cups.ref_catastral, '')   AS polissa_cups_ref_catastral,
  polissa_cups_municipi.name                 AS polissa_cups_municipi,
  polissa_cups_poblacio.name                 AS polissa_cups_poblacio,
  polissa_cups_state.name                    AS polissa_cups_state,
  CASE WHEN contract.factura_comer
    THEN polissa_payer.name
  ELSE titular.name END                      AS titular_name,
  CASE WHEN contract.factura_comer
    THEN polissa_payer.vat
  ELSE titular.vat END                       AS titular_vat,
  CASE WHEN contract.factura_comer
    THEN polissa_payment_addr.street
  ELSE invoice_addr.street END               AS direccio_factura_street,
  CASE WHEN contract.factura_comer
    THEN polissa_payment_addr.zip
  ELSE invoice_addr.zip END                  AS direccio_factura_zip,
  CASE WHEN contract.factura_comer
    THEN polissa_payment_poblacio.name
  ELSE invoice_addr_poblacio.name END        AS direccio_factura_poblacio,
  CASE WHEN contract.factura_comer
    THEN polissa_payment_municipi.name
  ELSE invoice_addr_municipi.name END        AS direccio_factura_municipi,
  CASE WHEN contract.factura_comer
    THEN polissa_payment_state.name
  ELSE invoice_addr_state.name END           AS direccio_factura_state,
  CASE WHEN contract.factura_comer
    THEN polissa_payment_country.name
  ELSE invoice_addr_country.name END         AS direccio_factura_country,
  CASE WHEN contract.factura_comer
    THEN COALESCE(polissa_payment_addr.phone, '')
  ELSE COALESCE(invoice_addr.phone, '') END  AS direccio_factura_phone,
  CASE WHEN contract.factura_comer
    THEN COALESCE(polissa_payment_addr.fax, '')
  ELSE COALESCE(invoice_addr.fax, '') END    AS direccio_factura_fax,
  CASE WHEN contract.factura_comer
    THEN COALESCE(polissa_payment_addr.mobile, '')
  ELSE COALESCE(invoice_addr.mobile, '') END AS direccio_factura_mobile,
  CASE WHEN contract.factura_comer
    THEN COALESCE(polissa_payment_addr.email, '')
  ELSE COALESCE(invoice_addr.email, '') END  AS direccio_factura_email,
  polissa_anterior.potencia                  AS anterior_potencia
FROM soller_contractacio_distri contract
  LEFT JOIN res_partner_address electricista_addr ON contract.contact_electricista_id = electricista_addr.id
  LEFT JOIN res_partner electricista ON contract.electricista_id = electricista.id
  INNER JOIN res_partner_address invoice_addr ON contract.direccio_factura_id = invoice_addr.id
  INNER JOIN res_country invoice_addr_country ON invoice_addr.country_id = invoice_addr_country.id
  INNER JOIN res_poblacio invoice_addr_poblacio ON invoice_addr.id_poblacio = invoice_addr_poblacio.id
  INNER JOIN res_municipi invoice_addr_municipi ON invoice_addr.id_municipi = invoice_addr_municipi.id
  INNER JOIN res_country_state invoice_addr_state ON invoice_addr.state_id = invoice_addr_state.id
  LEFT JOIN giscedata_polissa polissa_anterior ON contract.polissa_anterior_id = polissa_anterior.id
  LEFT JOIN giscedata_polissa polissa_anterior2 ON contract.polissa_anterior2_id = polissa_anterior2.id
  INNER JOIN giscedata_polissa polissa ON contract.polissa_id = polissa.id
  INNER JOIN giscedata_cups_ps polissa_cups ON polissa.cups = polissa_cups.id
  INNER JOIN res_municipi polissa_cups_municipi ON polissa_cups.id_municipi = polissa_cups_municipi.id
  INNER JOIN res_poblacio polissa_cups_poblacio ON polissa_cups.id_poblacio = polissa_cups_poblacio.id
  INNER JOIN res_country_state polissa_cups_state ON polissa_cups_municipi.state = polissa_cups_state.id
  INNER JOIN res_partner_address polissa_payment_addr ON polissa.direccio_pagament = polissa_payment_addr.id
  INNER JOIN res_country polissa_payment_country ON polissa_payment_addr.country_id = polissa_payment_country.id
  INNER JOIN res_poblacio polissa_payment_poblacio ON polissa_payment_addr.id_poblacio = polissa_payment_poblacio.id
  INNER JOIN res_municipi polissa_payment_municipi ON polissa_payment_addr.id_municipi = polissa_payment_municipi.id
  INNER JOIN res_country_state polissa_payment_state ON polissa_payment_addr.state_id = polissa_payment_state.id
  INNER JOIN res_partner polissa_payer ON polissa.pagador = polissa_payer.id
  INNER JOIN res_partner titular ON contract.titular_id = titular.id
WHERE contract.id = %s
