# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time

class SollerContractacioDistriPolissa(osv.osv):
    '''Ampliem la polissa per relacionar aquesta amb el model de contractacio'''
    
    _name = "giscedata.polissa"
    _inherit = "giscedata.polissa"

    def copy(self, cr, uid, id, default=None, context=None):
        """Canviem la funcionalitat del copy() per adaptar-ho a la pòlissa de Soller.
           No copiem les contractacions quan es copia una pòlissa"""

        default.update({'contractacio_id': []})
        res_id = super(SollerContractacioDistriPolissa, self).copy(cr, uid, id, default,
                                                    context)
        return res_id

    _columns = {
        
        'contractacio_id':fields.one2many('soller.contractacio.distri', 'polissa_id', 'Contractacio', required=False),
        
    }
    

SollerContractacioDistriPolissa()

