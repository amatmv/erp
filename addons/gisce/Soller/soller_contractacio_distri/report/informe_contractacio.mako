<%
    from tools import config
    from datetime import datetime
    from babel.dates import format_date
    from babel.numbers import format_currency, format_decimal
    from collections import defaultdict, deque
    from tools import float_round

    addons_path = config['addons_path']
    cursor = objects[0]._cr
    pool = objects[0].pool
    uid = user.id
    module_name = 'soller_contractacio_distri'
    locale = 'es_ES'
    decimal_pattern = '0.000000'
    short_decimal_pattern = '0.000'

    contract_sql_path = '{}/{}/sql/informe_contractacio.sql'.format(addons_path, module_name)
    contract_lines_sql_path = '{}/{}/sql/informe_contractacio_linies.sql'.format(addons_path, module_name)
    contract_sql = ''
    contract_linies_sql = ''

    with open(contract_sql_path) as f:
        contract_sql = f.read()

    with open(contract_lines_sql_path) as f:
        contract_linies_sql = f.read()
%>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link href="${addons_path}/${module_name}/report/static/font/roboto.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="${addons_path}/${module_name}/report/static/css/print.css"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/${module_name}/report/static/css/table.css"/>
        <link rel="stylesheet" type="text/css"
              href="${addons_path}/${module_name}/report/static/css/informe_contractacio.css"/>
    </head>
    <body>
        %for index, contract in enumerate(objects):
            <%
                cursor.execute(contract_sql, (contract.id,))
                contract_dict = cursor.dictfetchall()[0]

                cursor.execute(contract_linies_sql, (contract.id, ))
                lines = cursor.dictfetchall()

                anterior_name = [contract_dict['polissa_anterior_name'], contract_dict['polissa_anterior2_name']]
                anterior_name = [x for x in anterior_name if x]

                anterior_name_str = ''
                if len(anterior_name) > 0:
                    anterior_name_str = ', '.join(anterior_name)

                ref_catastral = contract_dict['polissa_cups_ref_catastral']

                contract_date = datetime.strptime(contract_dict['date'], '%Y-%m-%d')
                contract_date_str = format_date(contract_date, format='long', locale=locale)
            %>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <img style="width: 100%; margin: -25px auto 0 auto; display: block;"
                         src="${addons_path}/${module_name}/report/vse_solo.jpg"/>
                    <p style="margin-left: 32px;" class="text-extra-small">
                        ${company.rml_header1}
                    </p>
                    <p style="margin-left: 32px;" class="text-extra-small">
                        ${company.rml_footer1}
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <h5 class="text-center text-uppercase title">Informe de solicitud de acceso a red</h5>
                    <table class="table full-border table-adjust-space">
                        <tbody>
                            <tr>
                                <th>Referencia contrato</th>
                                <td>
                                    ${contract_dict['polissa_name']}
                                </td>

                                <th>Número SS</th>
                                <td>
                                    ${contract_dict['num_it']}
                                </td>

                                <th>Anterior</th>
                                <td>${anterior_name_str}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <h5>Datos del cliente</h5>
                    <table class="table full-border table-adjust-space">
                        <tbody>
                            <tr>
                                <th>Nombre/Razón</th>
                                <td colspan="3">
                                    ${contract_dict['titular_name']}
                                </td>

                                <th>NIF/CIF</th>
                                <td>
                                    ${contract_dict['titular_vat']}
                                </td>
                            </tr>
                            <tr>
                                <th>Dirección</th>
                                <td colspan="5">
                                    ${contract_dict['direccio_factura_street']}
                                </td>
                            </tr>
                            <tr>
                                <th>Cod. Postal</th>
                                <td>
                                    ${contract_dict['direccio_factura_zip']}
                                </td>

                                <th>Localidad</th>
                                <td class="text-uppercase">
                                    ${contract_dict['direccio_factura_poblacio']}
                                </td>

                                <th>Municipio</th>
                                <td class="text-uppercase">
                                    ${contract_dict['direccio_factura_municipi']}
                                </td>
                            </tr>
                            <tr>
                                <th>Provincia</th>
                                <td colspan="3" class="text-uppercase">
                                    ${contract_dict['direccio_factura_state']}
                                </td>

                                <th>País</th>
                                <td class="text-uppercase">
                                    ${contract_dict['direccio_factura_country']}
                                </td>
                            </tr>

                            <tr>
                                <th>Teléfono</th>
                                <td>
                                    ${contract_dict['direccio_factura_phone']}
                                </td>

                                <th>Fax</th>
                                <td>
                                    ${contract_dict['direccio_factura_fax']}
                                </td>

                                <th>Móvil</th>
                                <td>
                                    ${contract_dict['direccio_factura_mobile']}
                                </td>
                            </tr>

                            <tr>
                                <th>Correo electrónico</th>
                                <td colspan="5">
                                    ${contract_dict['direccio_factura_email']}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <h5>Datos del punto de suministro</h5>
                    <table class="table full-border table-adjust-space">
                        <tbody>
                            <tr>
                                <th>Dirección</th>
                                <td colspan="5">
                                    ${contract_dict['polissa_cups_direccio']}
                                </td>
                            </tr>

                            <tr>
                                <th>Cod. Postal</th>
                                <td>
                                    ${contract_dict['polissa_cups_dp']}
                                </td>

                                <th>Localidad</th>
                                <td class="text-uppercase">
                                    ${contract_dict['polissa_cups_poblacio']}
                                </td>

                                <th>Municipio</th>
                                <td class="text-uppercase">
                                    ${contract_dict['polissa_cups_municipi']}
                                </td>
                            </tr>

                            <tr>
                                <th>Provincia</th>
                                <td  colspan="5" class="text-uppercase">
                                    ${contract_dict['polissa_cups_state']}
                                </td>
                            </tr>

                            <tr>
                                <th>CUPS</th>
                                <td>
                                    ${contract_dict['polissa_cups_name']}
                                </td>

                                <th>Referencia Catastral</th>
                                <td colspan="3">
                                    ${ref_catastral}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <h5>Datos del instalador</h5>
                    <table class="table full-border table-adjust-space">
                        <tbody>
                            <tr>
                                <th>Instalador</th>
                                <td>
                                    ${contract_dict['electricista_name']}
                                </td>

                                <th>Tel.</th>
                                <td>
                                    ${contract_dict['electricista_phone']}
                                </td>

                                <th>Móvil</th>
                                <td>
                                    ${contract_dict['electricista_mobile']}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <h5>Datos técnicos</h5>
                    <table class="table full-border table-adjust-space">
                        <tbody>
                            <tr>
                                <th>Potencia (kW)</th>
                                <td class="text-left">
                                    ${format_decimal(contract_dict['potencia_nova'], locale=locale,
                                                     format=short_decimal_pattern)}
                                </td>

                                <th>Potencia anterior (kW)</th>
                                <td class="text-left">
                                    ${format_decimal(contract_dict['anterior_potencia'], locale=locale,
                                                     format=short_decimal_pattern)}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <h5>Datos económicos</h5>
                    <table class="table full-border table-adjust-space">
                        <thead>
                            <th>Concepto</th>
                            <th>Descripción</th>
                            <th class="text-right">Unidades</th>
                            <th class="text-right">Precio U.</th>
                            <th class="text-right">Importe</th>
                        </thead>
                        <tbody>
                            <%
                                taxes = defaultdict(lambda: deque())
                            %>
                            %for line in lines:
                                <%
                                    taxes[line['tax_id']].append(float_round(line['quantitat'] * line['preu_unitari'], 2))
                                %>
                                <tr>
                                    <td>
                                        ${line['concepto']}
                                    </td>

                                    <td>
                                        ${line['descripcion']}
                                    </td>

                                    <td class="text-right">
                                        ${format_decimal(line['quantitat'], locale=locale, format=short_decimal_pattern)}
                                    </td>

                                    <td class="text-right">
                                        ${format_decimal(line['preu_unitari'], locale=locale, format=decimal_pattern)} €
                                    </td>

                                    <td class="text-right">
                                        ${format_currency(line['total'], 'EUR', locale=locale)}
                                    </td>
                                </tr>
                            %endfor
                            <%
                                tax = 0
                                total = 0

                                for tax_id in taxes:
                                    for price in taxes[tax_id]:
                                        if tax_id is None:
                                            total += price
                                        else:
                                            total += price * 1.21
                                            tax += price * 0.21

                            %>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><strong>Impuestos</strong></td>
                                <td class="text-right"><strong>${format_currency(tax, 'EUR', locale=locale)}</strong></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><strong>Total</strong></td>
                                <td class="text-right"><strong>${format_currency(total, 'EUR', locale=locale)}</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="observations-box">
                        <h5>Observaciones</h5>
                        <p>
                            ${contract_dict['observacions']}
                        </p>
                    </div>
                </div>
                <div class="col-xs-6 small-top-margin bottom-signature-space">
                    <div class="signature-box">
                        Soller, a ${contract_date_str}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <ol class="text-extra-small">
                        <li>
                            <span>
                                El total de los costes derivados de esta contratación deberán ser liquidados antes o en
                                el momento de formalizar el acceso a red.
                            </span>
                        </li>

                        <li>
                            <span>
                                Tarifas aplicadas según la orden ITC/3519/2009
                            </span>
                        </li>

                        <li>
                            <span>
                                Trascurridos 3 meses de la presente notificación, se considerará anulada.
                            </span>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <p class="text-unreadable text-justify">
                        Conforme a lo previsto en la Ley Orgánica de Protección de Datos de Carácter Personal,
                        Ley 15/1999 de 13 de diciembre (en adelante LOPD), Vall de Sóller Energia le informa que los
                        datos obtenidos a través de este formulario o contrato están incluidos en varios fichero mixtos,
                        y cuya Responsable de los Ficheros es Vall de Sóller Energia SLU con CIF B57308538 con domicilio
                        en C/Sa Mar 146, 07100 - Sóller, Islas Baleares. Le informamos que de acuerdo con la LOPD usted
                        podrá ejercer gratuitamente sus derechos de acceso, rectificación, cancelación y oposición de
                        conformidad con los art.15, 16 y 17 dirigiendo un escrito a Vall de Sóller Energia SLU con CIF
                        B57308538 con domicilio en C/Sa Mar 146, 07100 - Sóller, Islas Baleares. En este sentido
                        informarle que de conformidad con lo dispuesto en la Ley 16/2009 de 13 de noviembre de servicios
                        de pago le será de aplicación lo dispuesto en la misma, de conformidad con el art.49,2 de la Ley
                        Orgánica de protección de datos, concretamente en lo dispuestos en su apartado segundo y
                        tercero, reconociendo el cliente darse por enterado de todos estos extremos. En virtud de lo
                        establecido en la LOPD y en el RLOPD, el Responsable de los Ficheros permitirá el acceso del
                        Encargado del Tratamiento a los datos de carácter personal contenidos en los Ficheros, para que
                        este realice el tratamiento de los mismos con el objeto de dar el cumplimiento prestación al
                        contrato de prestación de servicios suscrito entre las partes. Siendo en este caso aplicable
                        Articulo 12 de la LOPD en relación con los artículos 4 de la LOPD calidad de los datos, art. 5
                        de la LOPD derecho de información en la recogida de los datos, art. 6 de la LOPD consentimiento
                        del afectado, art. 7 de la LOPD datos especialmente protegidos, art. 8 de la LOPD datos
                        relativos a salud, art. 9 de la LOPD seguridad de los datos, art. 10 de la LOPD deber de
                        secreto, art. 11 comunicación de los datos y art. 12 acceso de datos por cuenta de terceros.
                        Todo ello de conformidad con la legislación aplicable; en cualquier caso será de aplicación el
                        art. 11.2 de la LOPD y art. 10.22 del reglamento de la LOPD, en relación a la ORDEN ITC
                        3860/2007 de 28 de DICIEMBRE y le será aplicable específicamente el art. 41, 44,45,47 de la
                        Ley 54/1997 de 27 de noviembre del sector eléctrico y posterior modificaciones con la Ley
                        17/2007 en cuanto a las cesiones y accesos de datos de los clientes, de las empresas
                        comercializadoras y distribuidoras, en el sistema de información de puntos de suministro.
                    </p>
                </div>
            </div>
        </div>

        % if index + 1 != len(objects):
            <p class="page-break"></p>
        % endif
        %endfor
    </body>
</html>