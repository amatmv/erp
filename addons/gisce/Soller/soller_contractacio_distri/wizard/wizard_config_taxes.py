# -*- coding: utf-8 -*-
from osv import fields,osv

class ConfigTaxesContractacio(osv.osv_memory):

    _name = 'wizard.config.taxes.contractacio'

    def set_taxes(self, cursor, uid, ids, context=None):

        product_obj = self.pool.get('product.product')
        data_obj = self.pool.get('ir.model.data')
        wizard = self.browse(cursor, uid, ids[0])
        tax_ids = [x.id for x in wizard.tax_ids]
        sup_tax_ids = [x.id for x in wizard.supplier_tax_ids]
        model_data_id = data_obj._get_id(cursor, uid,
                                          'soller_contractacio_distri',
                                          'categ_drets_escomesa')
        parent_categ_id = data_obj.browse(cursor, uid,
                                          model_data_id).res_id
        #Search all products in child categories
        search_params = [('categ_id', 'child_of', [parent_categ_id])]
        product_ids = product_obj.search(cursor, uid, search_params)
        for product in product_obj.browse(cursor, uid, product_ids):
            #Fianza product do not have taxes
            if product.default_code == 'FIANZA':
                product.write({'taxes_id': [(6, 0, [])],
                               'supplier_taxes_id': [(6, 0, [])]})
                continue
            product.write({'taxes_id': [(6, 0, tax_ids)],
                           'supplier_taxes_id': [(6, 0, sup_tax_ids)]})
        
        
        return {'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'ir.actions.configuration.wizard',
                'type': 'ir.actions.act_window',
                'target':'new',
                }

    _columns = {
        'tax_ids': fields.many2many('account.tax',
                                    'config_taxes_contractacio_tax_rel',
                                    'wizard_config_id',
                                    'tax_id', 'Impuestos de venta'),
        'supplier_tax_ids': fields.many2many('account.tax',
                                    'config_taxes_contractacio_sup_tax_rel',
                                    'wizard_config_id',
                                    'tax_id', 'Impuestos de compra')
    }

ConfigTaxesContractacio()