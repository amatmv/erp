# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import time


class WizardSCDCalcular(osv.osv_memory):
    '''Wizard per fer els calculs dels diferents drets i quotes'''

    _name = 'wizard.soller.contractacio.distri.calcular'

    _columns = {
        'obra': fields.float('Obra', digits=(16, 2)),
        'desp':fields.many2one('product.product', 'Desplaçament', required=False),
        'obracanvi': fields.float('Obra', digits=(16, 2)),
        'despcanvi':fields.many2one('product.product', 'Desplaçament', required=False),
            
    }

    def fields_get(self, cr, uid, fields=None, context=None, read_access=True):
        '''sobreescrivim el metode per afegir els camps dinamicament'''
        '''els camps que hem d'afegir de forma dinamica son els de ma de obra i desplacament'''

        result = super(WizardSCDCalcular, self).fields_get(cr, uid, fields, context)
        contractacio_obj = self.pool.get('soller.contractacio.distri')
        contractacio_id = context.get('active_ids', [])[0]
        contractacio = contractacio_obj.browse(cr, uid, contractacio_id)

        dic = {}
        for concepte in contractacio.conceptes_ids:
            dic[concepte.id] = concepte.code
        if 'obra' in dic.values():
            #si hi ma de obra hem de afegir els camps de quantitat i desplacament
            result['obra'] = {'string': 'Quantitat', 'type': 'float', 'digits':'(16,2)'}
            result['desp'] = {'string': 'Desplaçament', 'type': 'many2one', 'relation':'product.product'}
        if 'obracanvi' in dic.values():
            #si hi ha ma de obra de canvi hem de afegir els camps quantitat i desplacament
            result['obracanvi'] = {'string': 'Quantitat', 'type': 'float', 'digits': '(16,2)'}
            result['despcanvi'] = {'string': 'Desplaçament', 'type': 'many2one', 'relation':'product.product'}
        
        return result

    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context={}, toolbar=False):
        '''sobreescrivim el metode per afegir els camps dinamicament'''
        '''els camps que hem d'afegir de forma dinamica son els de ma de obra i desplacament'''

        result = super(WizardSCDCalcular, self).fields_view_get(cr, uid, view_id, 
                                                    view_type, context=context, toolbar=toolbar)

        contractacio_obj = self.pool.get('soller.contractacio.distri')
        contractacio_id = context.get('active_ids', [])[0]
        contractacio = contractacio_obj.browse(cr, uid, contractacio_id)
        fields = {}
        dic = {}
        for concepte in contractacio.conceptes_ids:
            dic[concepte.id] = concepte.code
                
        xml = '''<?xml version="1.0"?>\n<%s string="Dades">\n\t''' % (view_type,)

        if 'obra' in dic.values():
            #si hi ma de obra hem de afegir els camps de quantitat i desplacament
            xml += '''<separator string="Ma de obra" colspan="4"/>'''
            xml += '''<field name="obra"/>\n'''
            xml += '''<field name="desp" domain="[('default_code', 'like', 'DSP']"/>\n'''

        if 'obracanvi' in dic.values():
            #si hi ma de obra hem de afegir els camps de quantitat i desplacament
            xml += '''<separator string="Ma de obra per comptador" colspan="4"/>'''
            xml += '''<field name="obracanvi"/>\n'''
            xml += '''<field name="despcanvi" domain="[('default_code', 'like', 'DSP')]"/>\n'''

        if 'obra' not in dic.values() and 'obracanvi' not in dic.values():
            #si no hem afegit cap camp afegim una etiqueta indicant que no fan falta mes dades
            xml += '''<label string="No fan falta dades extra per fer els càlculs" colspan="4" rowspan="4"/>''' 

        xml += '''<group colspan="4" col="2">'''
        xml += '''<button special="cancel" string="Cancel·lar" icon="gtk-cancel"/>'''
        xml += '''<button name="action_calcular" string="Calcular" type="object" icon="gtk-go-forward"/>'''
        xml += '''</group>'''

        xml += '''</%s>'''% (view_type,)

        result['arch'] = xml
        result['fields'] = self.fields_get(cr, uid, fields, context)
        return result

    def get_preu_mo(self, cr, uid, ids, context=None):
        '''Treiem el preu de la ma de obra'''

        product_obj = self.pool.get('product.product')
        res = {}

        #Treiem el preu del producte
        search_params = [('default_code','=','MO')]
        product_id = product_obj.search(cr, uid, search_params)[0]
        preu = product_obj.read(cr, uid, product_id, ['list_price'],context)['list_price']

        contractacio_id = context.get('active_ids', [])[0]
        res[contractacio_id] = {'preu': preu, 'product_id': product_id}
        return res

    def get_preu_desp(self, cr, uid, ids, desp_id, context=None):
        '''Treiem el preu del desplacament'''

        product_obj = self.pool.get('product.product')
        res = {}

        search_params = [('id','=',desp_id)]
        product_id = product_obj.search(cr, uid, search_params)[0]
        preu = product_obj.read(cr, uid, product_id, ['list_price'],context)['list_price']
        
        contractacio_id = context.get('active_ids', [])[0]
        res[contractacio_id] = {'preu': preu, 'product_id': product_id}
        return res
        
    def action_calcular(self, cr, uid, ids, context=None):
        '''segons els conceptes hem de generar les linies amb els calculs fets'''

        wizard = self.browse(cr, uid, ids[0])
        contractacio_obj = self.pool.get('soller.contractacio.distri')
        contractacio_linia_obj = self.pool.get('soller.contractacio.distri.linia')
        contractacio_id = context.get('active_ids', [])[0]
        contractacio = contractacio_obj.browse(cr, uid, contractacio_id)

        #Si ja tenim factures emeses, no es pot recalcular
        if contractacio.factures_ids:
            raise osv.except_osv('Error','Ja hi han factures emeses d\'aquesta contractació')
            return {}
        #Si hi ha linies automatiques calculades, les hem de eliminar
        for linia in contractacio.linies_ids:
            if linia.auto:
                contractacio_linia_obj.unlink(cr, uid, linia.id, context)

        for concepte in contractacio.conceptes_ids:
            if concepte.code in ['extensio', 'acces']:
                #la quota de extensio i acces es calculen segons la potencia resultant                
                preu = contractacio.get_preu(concepte.code)
                vals = {'product_id': preu[contractacio_id]['product_id'],
                        'contractacio_id': contractacio_id,
                        'quantitat': contractacio.potencia_resultant,
                        'preu_unitari': preu[contractacio_id]['preu'],
                        'total': preu[contractacio_id]['preu'] * contractacio.potencia_resultant,
                        'tipus_id': concepte.tipus_id.id,
                       }
                contractacio_linia_obj.create(cr, uid, vals)
            elif concepte.code == 'local':
                #La compensacio local es calcula segons la potencia solicitada total
                preu = contractacio.get_preu_complocal()
                vals = {'product_id': preu[contractacio_id]['product_id'],
                        'contractacio_id': contractacio_id,
                        'quantitat': contractacio.potencia_resultant,
                        'preu_unitari': preu[contractacio_id]['preu'],
                        'total': preu[contractacio_id]['preu'] * contractacio.potencia_nova,
                        'tipus_id': concepte.tipus_id.id,
                       }
                contractacio_linia_obj.create(cr, uid, vals)
            elif concepte.code == 'obra':
                #Calculem el preu de la ma de obra i desplacament
                preu = self.get_preu_mo(cr, uid, ids, context)
                vals1 = {'product_id': preu[contractacio_id]['product_id'],
                        'contractacio_id': contractacio_id,
                        'quantitat': wizard.obra,
                        'preu_unitari': preu[contractacio_id]['preu'],
                        'total': preu[contractacio_id]['preu'] * wizard.obra,
                        'tipus_id': concepte.tipus_id.id,
                       }
                contractacio_linia_obj.create(cr, uid, vals1)
                if wizard.desp.id:
                    preu = self.get_preu_desp(cr, uid, ids, wizard.desp.id, context)
                    vals2 = {'product_id': preu[contractacio_id]['product_id'],
                            'contractacio_id': contractacio_id,
                            'quantitat': 1,
                            'preu_unitari': preu[contractacio_id]['preu'],
                            'total': preu[contractacio_id]['preu'],
                            'tipus_id': concepte.tipus_id.id,
                           }
                    contractacio_linia_obj.create(cr, uid, vals2)
            elif concepte.code == 'obracanvi':
                #Calculem el preu de la ma de obra i desplacament
                preu = self.get_preu_mo(cr, uid, ids, context)
                vals1 = {'product_id': preu[contractacio_id]['product_id'],
                        'contractacio_id': contractacio_id,
                        'quantitat': wizard.obracanvi,
                        'preu_unitari': preu[contractacio_id]['preu'],
                        'total': preu[contractacio_id]['preu'] *  wizard.obracanvi,
                        'tipus_id': concepte.tipus_id.id,
                       }
                contractacio_linia_obj.create(cr, uid, vals1)
                if wizard.despcanvi.id:
                    preu = self.get_preu_desp(cr, uid, ids, wizard.despcanvi.id, context)
                    vals2 = {'product_id': preu[contractacio_id]['product_id'],
                            'contractacio_id': contractacio_id,
                            'quantitat': 1,
                            'preu_unitari': preu[contractacio_id]['preu'],
                            'total': preu[contractacio_id]['preu'],
                            'tipus_id': concepte.tipus_id.id,
                           }
                    contractacio_linia_obj.create(cr, uid, vals2)
            else:
                #tots aquestos conceptes es facturen per unitats sempre
                if concepte.code in ['enganche', 'verificacio',
                                     'actuacio', 'cedidas']:
                    preu = contractacio.get_preu(concepte.code)
                elif concepte.code == 'vequip':
                    preu = contractacio.get_preu_vequip()
                elif concepte.code == 'vicp':
                    preu = contractacio.get_preu_vicp()
                elif concepte.code == 'fianza':
                    preu = contractacio.calcul_fianza()
                vals = {'product_id': preu[contractacio_id]['product_id'],
                        'contractacio_id': contractacio_id,
                        'quantitat': 1,
                        'preu_unitari': preu[contractacio_id]['preu'],
                        'total': preu[contractacio_id]['preu'],
                        'tipus_id': concepte.tipus_id.id,
                       }
                contractacio_linia_obj.create(cr, uid, vals)
        
        return {}
            
                     

WizardSCDCalcular()
