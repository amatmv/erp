<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="factura_generica_giscedata" language="groovy" pageWidth="595" pageHeight="842" columnWidth="529" leftMargin="33" rightMargin="33" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.610510000000001"/>
	<property name="ireport.x" value="54"/>
	<property name="ireport.y" value="48"/>
	<subDataset name="Impuestos">
		<parameter name="INVOICE_ID" class="java.lang.Integer">
			<defaultValueExpression><![CDATA[1]]></defaultValueExpression>
		</parameter>
		<queryString>
			<![CDATA[SELECT
    tax.invoice_id AS invoice_id,
    tax.name AS tax_name,
    tax.base AS tax_base,
    tax.amount AS tax_amount,
    tax.sequence AS tax_sequence
FROM
    account_invoice_tax tax
WHERE
    tax.invoice_id = $P!{INVOICE_ID}
ORDER BY
    tax.sequence]]>
		</queryString>
		<field name="invoice_id" class="java.lang.Integer"/>
		<field name="tax_name" class="java.lang.String"/>
		<field name="tax_base" class="java.math.BigDecimal"/>
		<field name="tax_amount" class="java.math.BigDecimal"/>
		<field name="tax_sequence" class="java.lang.Integer"/>
	</subDataset>
	<parameter name="IDS2" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{REPORT_PARAMETERS_MAP}.IDS.toString().replace("{","(").replace("}",")")]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
    factura.id as factura_id,
    invoice.id as invoice_id,
    invoice.number as numfactura,
    invoice.date_invoice as fechafactura,
    invoice.state as estat,
    invoice.comment as observaciones,
    polissa.name as polissa,
    cups.name as cups,
    tarifa.name as tarifa,
    factura.potencia,
    dirpago.name as nombrepagador,
    substring(pagador.vat,3) as nif,
    dirpago.street as calle,
    coalesce(dirpago.city,'') as poblacio,
    dirpago.zip as cp,
    provincia.name as provincia,
    pais.name as pais,
    liniainv.name as descripcio,
    liniainv.quantity as cantidad,
    liniainv.price_unit as precio,
    liniainv.price_subtotal as subtotal,
    invoice.amount_untaxed as base,
    invoice.amount_tax as impuestos,
    invoice.type as tipo_factura,
    tax.name as nomimpuesto,
    CASE
          WHEN invoice.type = 'out_refund' THEN invoice.amount_total*(-1)
          ELSE invoice.amount_total
     END as total,
     coalesce(pago.name,'') as formapago,
     CASE
         WHEN coalesce(pago.name,'') = 'RECIBO_CSB' THEN substring(coalesce(bank.printable_iban,''),1,length(bank.printable_iban)-5)||'*****'
         WHEN coalesce(pago.code,'') = 'TRANSFERENCIA_CSB' THEN empresa_ccc.printable_iban
         ELSE coalesce(pago.note,'')
     END as textopago,
     invoice.name as descripcion_factura,
     substring(empresa.vat, 3) as cif_empresa,
     company.rml_header1,
     company.rml_footer1

FROM
    giscedata_facturacio_factura factura
LEFT JOIN account_invoice invoice on factura.invoice_id = invoice.id
LEFT JOIN account_invoice_tax tax on tax.invoice_id = invoice.id
LEFT JOIN giscedata_facturacio_factura_linia liniafac on factura.id = liniafac.factura_id
LEFT JOIN account_invoice_line liniainv on liniafac.invoice_line_id = liniainv.id
LEFT JOIN giscedata_polissa polissa on factura.polissa_id = polissa.id
LEFT JOIN payment_type pago on pago.id = invoice.payment_type
LEFT JOIN res_partner pagador on invoice.partner_id = pagador.id
LEFT JOIN res_partner_address dirpago on invoice.address_invoice_id = dirpago.id
LEFT JOIN res_country_state provincia on dirpago.state_id = provincia.id
LEFT JOIN res_country pais on dirpago.country_id = pais.id
LEFT JOIN res_users uid ON factura.create_uid = uid.id
LEFT JOIN res_company company ON company.id = uid.company_id
LEFT JOIN res_partner empresa ON company.partner_id = empresa.id
LEFT JOIN res_partner_bank empresa_ccc ON empresa_ccc.partner_id = empresa.id and empresa_ccc.default_bank = True
LEFT JOIN res_partner_bank bank ON invoice.partner_bank = bank.id
LEFT JOIN giscedata_cups_ps cups on factura.cups_id = cups.id
LEFT JOIN giscedata_polissa_tarifa tarifa on factura.tarifa_acces_id = tarifa.id
WHERE
     factura.id in $P!{IDS2}]]>
	</queryString>
	<field name="factura_id" class="java.lang.Integer"/>
	<field name="invoice_id" class="java.lang.Integer"/>
	<field name="numfactura" class="java.lang.String"/>
	<field name="fechafactura" class="java.sql.Date"/>
	<field name="estat" class="java.lang.String"/>
	<field name="observaciones" class="java.lang.String"/>
	<field name="polissa" class="java.lang.String"/>
	<field name="cups" class="java.lang.String"/>
	<field name="tarifa" class="java.lang.String"/>
	<field name="potencia" class="java.math.BigDecimal"/>
	<field name="nombrepagador" class="java.lang.String"/>
	<field name="nif" class="java.lang.String"/>
	<field name="calle" class="java.lang.String"/>
	<field name="poblacio" class="java.lang.String"/>
	<field name="cp" class="java.lang.String"/>
	<field name="provincia" class="java.lang.String"/>
	<field name="pais" class="java.lang.String"/>
	<field name="descripcio" class="java.lang.String"/>
	<field name="cantidad" class="java.math.BigDecimal"/>
	<field name="precio" class="java.math.BigDecimal"/>
	<field name="subtotal" class="java.math.BigDecimal"/>
	<field name="base" class="java.math.BigDecimal"/>
	<field name="impuestos" class="java.math.BigDecimal"/>
	<field name="tipo_factura" class="java.lang.String"/>
	<field name="nomimpuesto" class="java.lang.String"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="formapago" class="java.lang.String"/>
	<field name="textopago" class="java.lang.String"/>
	<field name="descripcion_factura" class="java.lang.String"/>
	<field name="cif_empresa" class="java.lang.String"/>
	<field name="rml_header1" class="java.lang.String"/>
	<field name="rml_footer1" class="java.lang.String"/>
	<group name="factura_id" isStartNewPage="true">
		<groupExpression><![CDATA[$F{factura_id}]]></groupExpression>
		<groupHeader>
			<band height="163">
				<image scaleImage="FillFrame">
					<reportElement x="2" y="0" width="527" height="108"/>
					<imageExpression class="java.lang.String"><![CDATA[$P{REPORT_PARAMETERS_MAP}.SUBREPORT_DIR + "vse_solo.jpg"]]></imageExpression>
				</image>
				<textField>
					<reportElement x="12" y="108" width="517" height="11"/>
					<textElement lineSpacing="Single">
						<font size="7" isItalic="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{rml_header1}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="false">
					<reportElement mode="Transparent" x="12" y="119" width="517" height="11" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="7" isBold="false" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{cif_empresa} + " " + $F{rml_footer1}]]></textFieldExpression>
				</textField>
			</band>
			<band height="176">
				<rectangle radius="10">
					<reportElement mode="Transparent" x="2" y="37" width="220" height="90"/>
				</rectangle>
				<staticText>
					<reportElement mode="Transparent" x="395" y="159" width="66" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<text><![CDATA[Precio U.]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="461" y="159" width="68" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<text><![CDATA[Importe]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="0" y="159" width="329" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<text><![CDATA[Concepto]]></text>
				</staticText>
				<staticText>
					<reportElement x="12" y="76" width="59" height="12"/>
					<box leftPadding="3">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="true"/>
					</textElement>
					<text><![CDATA[Referencia]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="329" y="159" width="66" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<text><![CDATA[Unidades]]></text>
				</staticText>
				<staticText>
					<reportElement x="12" y="64" width="59" height="12"/>
					<box leftPadding="3">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="true"/>
					</textElement>
					<text><![CDATA[Fecha]]></text>
				</staticText>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="71" y="52" width="139" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[($F{estat}=="proforma2")?"PROFORMA":$F{numfactura}]]></textFieldExpression>
				</textField>
				<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="71" y="64" width="139" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.util.Date"><![CDATA[$F{fechafactura}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="71" y="76" width="139" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{polissa}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="254" y="40" width="275" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{nombrepagador}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="254" y="52" width="275" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{calle}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="309" y="64" width="220" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{poblacio}.toUpperCase()]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="254" y="64" width="55" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{cp}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="254" y="76" width="275" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{provincia}.toUpperCase() + " - " + $F{pais}.toUpperCase()]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="71" y="40" width="139" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{nif}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="0" y="141" width="529" height="12" forecolor="#000000" backcolor="#FFFFFF">
						<printWhenExpression><![CDATA[$F{tipo_factura} == "out_refund"]]></printWhenExpression>
					</reportElement>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{descripcion_factura}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="12" y="88" width="59" height="12"/>
					<box leftPadding="3">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="true"/>
					</textElement>
					<text><![CDATA[CUPS]]></text>
				</staticText>
				<staticText>
					<reportElement x="12" y="100" width="59" height="12"/>
					<box leftPadding="3">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="true"/>
					</textElement>
					<text><![CDATA[Tarifa]]></text>
				</staticText>
				<staticText>
					<reportElement x="12" y="112" width="59" height="12"/>
					<box leftPadding="3">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="true"/>
					</textElement>
					<text><![CDATA[Pot. (kW)]]></text>
				</staticText>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="71" y="88" width="139" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{cups}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="71" y="100" width="139" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{tarifa}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.000" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="71" y="112" width="139" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{potencia}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="12" y="40" width="59" height="12"/>
					<box leftPadding="3">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="true"/>
					</textElement>
					<text><![CDATA[NIF/CIF]]></text>
				</staticText>
				<staticText>
					<reportElement x="12" y="52" width="59" height="12"/>
					<box leftPadding="3">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="true"/>
					</textElement>
					<text><![CDATA[Factura]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="56">
				<staticText>
					<reportElement positionType="Float" mode="Transparent" x="395" y="42" width="66" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<bottomPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<text><![CDATA[Total €]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="395" y="14" width="66" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<bottomPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<text><![CDATA[Base]]></text>
				</staticText>
				<textField pattern="#,##0.00" isBlankWhenNull="false">
					<reportElement mode="Transparent" x="461" y="14" width="68" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{base}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="false">
					<reportElement positionType="Float" mode="Transparent" x="461" y="42" width="68" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{total}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="395" y="10" width="134" height="1"/>
				</line>
				<componentElement>
					<reportElement x="0" y="28" width="529" height="14"/>
					<jr:list xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" printOrder="Vertical">
						<datasetRun subDataset="Impuestos">
							<datasetParameter name="INVOICE_ID">
								<datasetParameterExpression><![CDATA[$F{invoice_id}]]></datasetParameterExpression>
							</datasetParameter>
							<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
						</datasetRun>
						<jr:listContents height="14" width="529">
							<textField pattern="" isBlankWhenNull="false">
								<reportElement mode="Transparent" x="0" y="0" width="302" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
								<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
									<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
								</textElement>
								<textFieldExpression class="java.lang.String"><![CDATA[$F{tax_name}]]></textFieldExpression>
							</textField>
							<textField pattern="#,##0.00" isBlankWhenNull="false">
								<reportElement mode="Transparent" x="327" y="0" width="68" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
								<box leftPadding="3"/>
								<textElement verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
									<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
								</textElement>
								<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{tax_base}]]></textFieldExpression>
							</textField>
							<textField pattern="#,##0.00" isBlankWhenNull="false">
								<reportElement mode="Transparent" x="461" y="0" width="68" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
								<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
									<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
								</textElement>
								<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{tax_amount}]]></textFieldExpression>
							</textField>
							<staticText>
								<reportElement mode="Transparent" x="302" y="0" width="25" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
								<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
									<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
								</textElement>
								<text><![CDATA[Base]]></text>
							</staticText>
							<staticText>
								<reportElement mode="Transparent" x="395" y="0" width="66" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
								<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
									<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
								</textElement>
								<text><![CDATA[Importe]]></text>
							</staticText>
						</jr:listContents>
					</jr:list>
				</componentElement>
			</band>
			<band height="50">
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="0" y="0" width="529" height="46" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{observaciones}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band height="400" splitType="Stretch">
			<printWhenExpression><![CDATA[$F{tipo_factura} == "out_refund"]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="274" width="529" height="108" forecolor="#EDECEB"/>
				<textElement textAlignment="Center" lineSpacing="Single">
					<font size="78" isBold="false"/>
				</textElement>
				<text><![CDATA[ABONO]]></text>
			</staticText>
		</band>
	</background>
	<detail>
		<band height="14" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="0" y="0" width="329" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{descripcio}]]></textFieldExpression>
			</textField>
			<textField pattern="###0.000" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="329" y="0" width="66" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{cantidad}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.000000" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="395" y="0" width="66" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{precio}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="461" y="0" width="68" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{subtotal}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="17">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="0" y="0" width="529" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Forma de pago: " + $F{formapago} + " " + $F{textopago}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
