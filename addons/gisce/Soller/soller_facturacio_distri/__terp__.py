# -*- coding: utf-8 -*-
{
    "name": "Soller Facturacio Distribucio",
    "description": """
    Funcionalitats extras de facturacio de distribucio
    * Report de factura i rebut genèric per giscefactures
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_facturacio_distri",
        "giscedata_comerdist_facturacio_dist",
        "jasper_reports",
        "comerdist_f1"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_facturacio_distri_report.xml"
    ],
    "active": False,
    "installable": True
}
