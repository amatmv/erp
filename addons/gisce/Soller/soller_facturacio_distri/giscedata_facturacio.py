# -*- coding: utf-8 -*-

from osv import osv, fields
from giscedata_comerdist import OOOPPool
from base_extended.base_extended import MultiprocessBackground
import pooler
import netsvc

class GiscedataFacturacioSollerDistri(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def export_to_comer_autoci(self, cursor, uid, ids, force=False,
                               context=None):
        """Export and afterwards check if it is a employee invoice
        """

        res = super(GiscedataFacturacioSollerDistri,
                    self).export_to_comer_autoci(cursor, uid, ids,
                                                 force=force,
                                                 context=context)
        # Search for all invoices in comer related to ids in distri
        remote_ids = [f['remote_id'] for f in self.read(cursor, uid, ids,
                                                        ['remote_id'])]
        factura = self.browse(cursor, uid, ids[0])
        config_id = factura.get_config().id
        proxy = OOOPPool.get_ooop(cursor, uid, config_id)
        factura_proxy = proxy.GiscedataFacturacioFactura
        factura_proxy.apply_employee(res)

        return res

GiscedataFacturacioSollerDistri()

class GiscedataFacturacioLotSollerDistri(osv.osv):

    _inherit = 'giscedata.facturacio.lot'

    @MultiprocessBackground.background()
    def obrir_factures_button(self, cursor, uid, ids, context=None):
        '''
            Overwrite function obrir_factures_button
        '''
        super(GiscedataFacturacioLotSollerDistri,
              self).obrir_factures_button(cursor, uid, ids, context=context)

        self.other_invoice_treatment(cursor, uid, ids, context=context)

    def other_invoice_treatment(self, cursor, uid, lot_ids, context=None):
        '''
            Search for "facturas de derechos" in draw with the same policy as
            invoice in lot.
        '''
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        comerdist_obj = self.pool.get('comerdist.f1')

        db = pooler.get_db_only(cursor.dbname)
        logger = netsvc.Logger()

        search_params = [('tipo_factura', 'in', ('04', '05')),
                         ('state', '=', 'draft')]
        invoice_ids = invoice_obj.search(cursor, uid, search_params)

        for invoice_id in invoice_ids:
            read_fields = ['polissa_id']
            invoice_vals = invoice_obj.read(cursor, uid, invoice_id,
                                            read_fields)

            # Search for an invoice in lot with the same policy
            search_params = [('polissa_id', '=', invoice_vals['polissa_id'][0]),
                             ('lot_facturacio', 'in', lot_ids)]

            invoice_in_lot_ids = invoice_obj.search(cursor, uid, search_params)
            if invoice_in_lot_ids:
                read_fields = ['date_invoice']
                invoice_in_lot_vals = invoice_obj.read(cursor, uid,
                                                       invoice_in_lot_ids[0],
                                                       read_fields)
                try:
                    tmp_cr = db.cursor()
                    # Rewrite invoice date
                    vals = {
                        'date_invoice': invoice_in_lot_vals['date_invoice']
                    }
                    invoice_obj.write(tmp_cr, uid, invoice_id, vals)

                    # Export invoices to comer
                    invoice_obj.invoice_open(tmp_cr, uid, [invoice_id],
                                             context=context)
                    tmp_cr.commit()
                    comerdist_obj.export_f1_to_comer(cursor, uid, invoice_id,
                                                     True, context=context)
                except Exception, exc:
                    logger.notifyChannel("objects", netsvc.LOG_ERROR, exc)
                    tmp_cr.rollback()
                finally:
                    tmp_cr.close()


GiscedataFacturacioLotSollerDistri()
