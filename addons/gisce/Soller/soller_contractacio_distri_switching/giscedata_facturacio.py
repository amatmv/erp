# -*- encoding: utf-8 -*-

from __future__ import absolute_import
from osv import osv
from gestionatr.output.messages import sw_f1 as new_f1


class GiscedataFacturacioFactura(osv.osv):

    _inherit = 'giscedata.facturacio.factura'

    def generate_general_data_others(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        general_data_others = new_f1.DatosGeneralesOtrasFacturas()
        supply_address = self.generate_supply_address(
            cursor, uid, fact, context
        )
        client = self.generate_client(
            cursor, uid, fact, context
        )
        general_invoice_data = self.generate_general_invoice_data(
            cursor, uid, fact, context
        )

        general_data_others.feed(
            {
                'direccion_suministro': supply_address,
                'cliente': client,
                'cod_contrato': fact.polissa_id.name,
                'datos_generales_factura': general_invoice_data,
                'fecha_boe': fact.date_boe,
            }
        )

        return general_data_others

    def generate_atr_invoices(self, cursor, uid, fact, context=None):

        if context is None:
            context = {}

        if fact.tipo_factura != '01':
            # If it is not an atr invoice return none
            return None

        return super(GiscedataFacturacioFactura,
                     self).generate_atr_invoices(cursor, uid, fact, context)

    def generate_other_invoices(self, cursor, uid, fact, context=None):

        if context is None:
            context = {}

        if fact.tipo_factura == '01':
            # If it is a normal invoice return None
            return None

        other_invoices = new_f1.OtrasFacturas()
        general_data_others = self.generate_general_data_others(
            cursor, uid, fact, context
        )
        repercussible_concept = self.generate_repercussible_concepts(
            cursor, uid, fact, context
        )
        iva = self.generate_iva(cursor, uid, fact, context)
        reduced_iva = self.generate_reduced_iva(cursor, uid, fact, context)

        other_invoices.feed(
            {
                'datos_generales_otras_facturas': general_data_others,
                'concepto_repercutible': repercussible_concept,
                'iva': iva,
                'iva_reducido': reduced_iva,
            }
        )

        return other_invoices

GiscedataFacturacioFactura()
