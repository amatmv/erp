# -*- coding: utf-8 -*-
{
    "name": "Contractació distribució Switching",
    "description": """
Contractació de distribució associats als conceptes de factures F1
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_facturacio_distri",
        "soller_contractacio_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "contractacio_distri_switching_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
