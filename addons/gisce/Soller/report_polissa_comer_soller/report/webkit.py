from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)

        blank = 'report_giscedata_polissa_empty_administrative_data_webkit' in \
                name

        self.localcontext.update({
            'blank': blank
        })


webkit_report.WebKitParser(
    'report.report_giscedata_polissa_administrative_data_webkit',
    'giscedata.polissa',
    'report_polissa_comer_soller/report/administrative_data.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_giscedata_polissa_empty_administrative_data_webkit',
    'giscedata.polissa',
    'report_polissa_comer_soller/report/administrative_data.mako',
    parser=report_webkit_html
)