<%
    from datetime import date
    from tools import config

    addons_path = config['addons_path']
    cursor = objects[0]._cr
    pool = objects[0].pool
    uid = user.id
    company = user.company_id

    company_header = "{} {}".format(company.partner_id.vat, company.rml_footer1)[2:]
    today_str = date.today().strftime("%d %B %Y")
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/report_polissa_comer_soller/report/static/css/meyer_reset.css"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/report_polissa_comer_soller/report/static/css/polissa_comer_soller.css"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/report_polissa_comer_soller/report/static/css/print.css"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/report_polissa_comer_soller/report/static/css/extra.css"/>
    </head>
    <body>
        %for polissa in objects:
            <%
                cnae = "{} {}".format(polissa.cnae.name, polissa.cnae.descripcio)
            %>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                    <div class="row">
                        <div class="col-xs-12">
                            <img style="width: 100%;" src="${addons_path}/report_polissa_comer_soller/report/static/img/cabecera_es_nueva.png">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 horizontal-margins">
                            <p class="small italic">${user.company_id.rml_header1}</p>
                            <p class="small italic">${company_header}</p>
                        </div>
                    </div>
                    <div class="row big-break">
                        <div class="col-xs-12">
                            <h1 class="upcase text-center">Datos administrativos</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>Contrato</th>
                                        <td>${polissa.name}</td>
                                        <th>Abonado</th>
                                        <td>${polissa.abonat}</td>
                                        <th>Póliza</th>
                                        <td>${polissa.ref_dist}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row up-break">
                        <div class="col-xs-12">
                            <h2>Punto de suministro</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>CUPS</th>
                                        <td>${polissa.cups.name}</td>
                                    </tr>
                                    <tr>
                                        <th>Dirección</th>
                                        <td>${polissa.cups.direccio}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row up-break">
                        <div class="col-xs-12">
                            <h2>Datos del titular</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- This is a table with 6 cols -->
                            <!-- Cols are defined by the row with more cols -->
                            <table>
                                <tbody>
                                    <tr>
                                        <th>Nombre / Razón</th>
                                        %if blank:
                                            <td colspan="3"></td>
                                        %else:
                                            <td colspan="3">${polissa.titular.name or ''}</td>
                                        %endif

                                        <th>NIF / CIF</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.titular.vat}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Dirección</th>
                                        %if blank:
                                            <td colspan="5"></td>
                                        %else:
                                            <td colspan="5">${polissa.direccio_titular.street or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Código postal</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_titular.zip}</td>
                                        %endif

                                        <th>Localidad</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td class="upcase">${polissa.direccio_titular.id_poblacio.name or ''}</td>
                                        %endif

                                        <th>Municipio</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td class="upcase">${polissa.direccio_titular.id_municipi.name or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Provincia</th>
                                        %if blank:
                                            <td colspan="2"></td>
                                        %else:
                                            <td colspan="2" class="upcase">${polissa.direccio_titular.state_id.name or ''}</td>
                                        %endif

                                        <th>País</th>
                                        %if blank:
                                            <td colspan="2"></td>
                                        %else:
                                            <td colspan="2" class="upcase">${polissa.direccio_titular.country_id.name or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Teléfono</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_titular.phone or ''}</td>
                                        %endif

                                        <th>Fax</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_titular.fax or ''}</td>
                                        %endif

                                        <th>Móvil</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_titular.mobile or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Correo electrónico</th>
                                        %if blank:
                                            <td colspan="5"></td>
                                        %else:
                                            <td colspan="5">${polissa.direccio_titular.email or ''}</td>
                                        %endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row up-break">
                        <div class="col-xs-12">
                            <h2>Datos del pagador</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- This is a table with 6 cols -->
                            <!-- Cols are defined by the row with more cols -->
                            <table>
                                <tbody>
                                    <tr>
                                        <th>Nombre / Razón</th>
                                        %if blank:
                                            <td colspan="3"></td>
                                        %else:
                                            <td colspan="3">${polissa.direccio_pagament.name}</td>
                                        %endif

                                        <th>NIF / CIF</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.pagador.vat}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Dirección</th>
                                        %if blank:
                                            <td colspan="5"></td>
                                        %else:
                                            <td colspan="5">${polissa.direccio_pagament.street or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Código postal</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_pagament.zip or ''}</td>
                                        %endif

                                        <th>Localidad</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td classname="upcase">${polissa.direccio_pagament.id_poblacio.name or ''}</td>
                                        %endif

                                        <th>Municipio</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td class="upcase">${polissa.direccio_pagament.id_municipi.name or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Provincia</th>
                                        %if blank:
                                            <td colspan="2"></td>
                                        %else:
                                            <td class="upcase" colspan="2">${polissa.direccio_pagament.state_id.name or ''}</td>
                                        %endif

                                        <th>País</th>
                                        %if blank:
                                            <td colspan="2"></td>
                                        %else:
                                            <td colspan="2" class="upcase">${polissa.direccio_pagament.country_id.name or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Teléfono</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_pagament.phone or ''}</td>
                                        %endif

                                        <th>Fax</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_pagament.fax or ''}</td>
                                        %endif

                                        <th>Móvil</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_pagament.mobile or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Correo electrónico</th>
                                        %if blank:
                                            <td colspan="5"></td>
                                        %else:
                                            <td colspan="5">${polissa.direccio_pagament.email or ''}</td>
                                        %endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row up-break">
                        <div class="col-xs-12">
                            <h2>Otros datos de contacto</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>Nombre / Razón</th>
                                        %if blank:
                                            <td colspan="5"></td>
                                        %else:
                                            <td colspan="5">${polissa.direccio_notificacio.name}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Dirección</th>
                                        %if blank:
                                            <td colspan="5"></td>
                                        %else:
                                            <td colspan="5">${polissa.direccio_notificacio.street or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Código postal</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_notificacio.zip}</td>
                                        %endif

                                        <th>Localidad</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td class="upcase">${polissa.direccio_notificacio.id_poblacio.name or ''}</td>
                                        %endif

                                        <th>Municipio</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td class="upcase">${polissa.direccio_notificacio.id_municipi.name or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Provincia</th>
                                        %if blank:
                                            <td colspan="2"></td>
                                        %else:
                                            <td colspan="2" class="upcase">${polissa.direccio_notificacio.state_id.name or ''}</td>
                                        %endif

                                        <th>País</th>
                                        %if blank:
                                            <td colspan="2"></td>
                                        %else:
                                            <td colspan="2" class="upcase">${polissa.direccio_notificacio.country_id.name or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Teléfono</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_notificacio.phone or ''}</td>
                                        %endif

                                        <th>Fax</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_notificacio.fax or ''}</td>
                                        %endif

                                        <th>Móvil</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${polissa.direccio_notificacio.mobile or ''}</td>
                                        %endif
                                    </tr>
                                    <tr>
                                        <th>Correo electrónico</th>
                                        %if blank:
                                            <td colspan="5"></td>
                                        %else:
                                            <td colspan="5">${polissa.direccio_notificacio.email or ''}</td>
                                        %endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row up-break">
                        <div class="col-xs-12">
                            <h2>Otros datos</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>CNAE</th>
                                        %if blank:
                                            <td></td>
                                        %else:
                                            <td>${cnae}</td>
                                        %endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row up-break big-bottom-break">
                        <div class="col-xs-12">
                            <p style="margin-right: 60px; font-size: 11px;" class="text-right">Soller, a ${today_str}</p>
                        </div>
                    </div>

                    <footer>
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="extra-small">${_('''Conforme a lo previsto en la Ley Orgánica de Protección de Datos de Carácter
                                    Personal, Ley 15/1999 de 13 de diciembre (en adelante LOPD), Eléctrica Sollerense
                                    le informa que los datos obtenidos a través de este formulario o contrato están
                                    incluidos en varios fichero mixtos, y cuya Responsable de los Ficheros es Eléctrica
                                    Sollerense con CIF A57048332 con domicilio en C/Sa Mar 146, 07100 - Sóller, Islas
                                    Baleares. Le informamos que de acuerdo con la LOPD usted podrá ejercer gratuitamente
                                    sus derechos de acceso, rectificación, cancelación y oposición de conformidad con
                                    los art.15, 16 y 17 dirigiendo un escrito a Eléctrica Sollerense con CIF A57048332
                                    con domicilio en C/Sa Mar 146, 07100 - Sóller, Islas Baleares. En virtud de lo
                                    establecido en la LOPD y en el RLOPD, el Responsable de los Ficheros permitirá el
                                    acceso del Encargado del Tratamiento a los datos de carácter personal contenidos en
                                    los Ficheros, para que este realice el tratamiento de los mismos con el objeto de
                                    dar el cumplimiento prestación al contrato de prestación de servicios suscrito entre
                                    las partes. Siendo en este caso aplicable Articulo 12 de la LOPD en relación con
                                    los artículos 4 de la LOPD calidad de los datos, art. 5 de la LOPD derecho de
                                    información en la recogida de los datos, art. 6 de la LOPD consentimiento del
                                    afectado, art. 7 de la LOPD datos especialmente protegidos, art. 8 de la LOPD datos
                                    relativos a salud, art. 9 de la LOPD seguridad de los datos, art. 10 de la LOPD
                                    deber de secreto, art. 11 comunicación de los datos y art. 12 acceso de datos por
                                    cuenta de terceros. Todo ello de conformidad con la legislación aplicable; en
                                    cualquier caso será de aplicación el art. 11.2 de la LOPD y art. 10.22 del
                                    reglamento de la LOPD, en relación a la ORDEN ITC 3860/2007 de 28 de DICIEMBRE y
                                    le será aplicable específicamente el art. 41, 44, 45, 47 de la Ley 54/1997 de 27 de
                                    noviembre del sector eléctrico y posterior modificaciones con la Ley 17/2007 en
                                    cuanto a las cesiones y accesos de datos de los clientes, de las empresas
                                    comercializadoras y distribuidoras, en el sistema de información de puntos de
                                    suministro. Y así mismo el cliente consiente de manera expresa en que el encargado
                                    del tratamiento realice las cesiones de los datos contenidos en los ficheros a las
                                    entidades y organismos oficiales competentes en la materia de conformidad con la
                                    legislación aplicable.''')}</p>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
            % if objects.index(polissa) + 1 != len(objects):
                <p class="page-break"></p>
            % endif
        %endfor
    </body>
</html>
