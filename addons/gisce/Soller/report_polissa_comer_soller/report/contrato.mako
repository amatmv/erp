<%

from tools import config
from datetime import datetime
addons_path = config['addons_path']
cursor = objects[0]._cr
uid = user.id
pool = objects[0].pool
address_obj = pool.get('res.partner.address')
polissa_obj = pool.get('giscedata.polissa')
modcon_obj = pool.get('giscedata.polissa.modcontractual')
account_obj = pool.get('res.partner.bank')
async_obj = pool.get('polissa.modcontractual.async')

#Company data
company = user.company_id.partner_id
company_addresses = company.address_get(adr_pref=['contact'])
if 'contact' in company_addresses:
    company_address = address_obj.read(cursor, uid, company_addresses['contact'])
else:
    company_address = {}
#Company account
search_params = [('partner_id', '=', company.id),
                 ('default_bank', '=', True)]
company_account_id = account_obj.search(cursor, uid, search_params)[0]
company_account = account_obj.browse(cursor, uid, company_account_id)

def get_city(address):
    city = address.city
    if address.id_municipi and address.id_poblacio:
        city = '(%s) %s' % (address.id_poblacio.name,
                            address.id_municipi.name)
    elif address.id_poblacio:
        city = address.id_poblacio.name

    return city.upper()

def get_prices(contract, date=None):
    pricelist_obj = pool.get('product.pricelist')
    uom_obj = pool.get('product.uom')
    res = {}
    tarifa_id = contract.llista_preu.id
    if date is None:
        date = contract.versio_primera_factura.date_start
    if contract.data_firma_contracte > date:
        date = contract.data_firma_contracte
    for periode in contract.tarifa.periodes:
        if (periode.tipus == 'te'
            and contract.llista_preu.agrupacio
            and contract.llista_preu.get_product_group(periode.product_id.id)):
            continue
        if periode.agrupat_amb:
            continue
        tipus = periode.tipus
        res.setdefault(tipus, [])
        if tipus == 'tp':
            uom_name = contract.property_unitat_potencia.name
        else:
            uom_name = 'kWh'
        search_params = [('name', '=', uom_name)]
        uom_id = uom_obj.search(cursor, uid, search_params)[0]        
        try:
            price = pricelist_obj.price_get(cursor, uid, [tarifa_id],
                                        periode.product_id.id,
                                        100,
                                        contract.pagador.id,
                                        {'date': date,
                                         'uom': uom_id
                                        })[tarifa_id]
        except:
            price = 0
        res[tipus].append((periode.name, {'price': price, 'uom': uom_name}))
    return res

%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>

         <link rel="stylesheet" type="text/css" href="${addons_path}/report_polissa_comer_soller/report/static/css/print.css"/>

</head>
<body>
%for object in objects:
<%
if data['model'] == 'giscedata.polissa.modcontractual':
    modcon = modcon_obj.browse(cursor, uid, object.id)
    contract = polissa_obj.browse(cursor, uid, modcon.polissa_id.id,
                                  context={'date': modcon.data_final})
    date = modcon.data_inici
    modcon_activa = modcon.name
    data_firma = contract.data_firma_contracte
elif (data['model'] == 'giscedata.polissa' and
      object.modcontractual_activa):
    date = datetime.now().strftime('%Y-%m-%d')
    contract = polissa_obj.browse(cursor, uid, object.id,
                                  context={'date': date})
    modcon_activa = contract.modcontractual_activa.name
    data_firma = contract.data_firma_contracte
elif data['model'] == 'polissa.modcontractual.async':
    date = data_firma = object.date_start
    contract = async_obj.polissa_browse(cursor, uid, object.id)
    if object.mod_id:
        modcon_activa = object.mod_id.name
    else:
        modcon_activa = str(int(contract.modcontractual_activa.name) + 1)
else:
    contract = object
    date = None
    modcon_activa = '1'
    data_firma = contract.data_firma_contracte

setLang(contract.pagador.lang)

dir_titular = contract.direccio_titular
dir_pagador = contract.direccio_pagament

#Update periodes potencia if esborrany
if contract.state == 'esborrany':
    contract.generar_periodes_potencia(context={'sync': False})

contract_title = _("CONDICIONS PARTICULARS DEL CONTRACTE DE SUBMINISTRAMENT D'ENERGIA ELÈCTRICA")
if contract.tensio > 1000:
    contract_title_tensio = _("D'ALTA TENSIÓ")
else:
    contract_title_tensio = _("DE BAIXA TENSIÓ")

prices =  get_prices(contract, date)
prices_energia = dict(prices['te'])
prices_pot = dict(prices['tp'])
periodes_potencia = dict([(x.periode_id.name, x.potencia)
                     for x in contract.potencies_periode])
periodes = sorted(prices_energia.keys())

if contract.comptadors:
    comptador = contract.comptadors[0]
    if comptador.lloguer:
        if 'dia' in comptador.uom_id.name:
            comptador_uom_name = _('dia')
        else: # supposing unit of mesaurement its always by day or month
            comptador_uom_name = _('mes')
else:
    comptador = False
%>
<div id="wrap">
<div class="container">
    <div class="row top-padding-fix">
        <div class="col-xs-5">    
           <div class="panel panel-default">
               <div class="panel-heading">
                   <h4>${_('Dades d\'empresa')}</h4>
               </div>
               <div class="panel-body invoice-data">
                   <p><b><dfn>${company.name.upper()}</dfn></b> (${company.vat.upper()})</p>
                   <p>${'%s, %s - %s' % (company_address['street'], company_address['zip'], company_address['city'].upper())}</p>
                   <p>${'%s - %s' %(company_address['state_id'][1].upper(), company_address['country_id'][1].upper())}</p>
                   <p>${_('Telèfon')} ${company_address['phone']}, ${_('Fax')} ${company_address['fax']}</p>
                   <p>${company.website}</p>
                   <p>${company_address['email']}</p>
                   <p style="font-size: 8px;">${user.company_id.rml_footer1}</p>
               </div>
           </div>
        </div>
        <div class="col-xs-7" style="min-height: 148px;">
            <img style="width: 558px; float:right;" src="${addons_path}/report_polissa_comer_soller/report/cabecera-facturas-nueva.png"/>
        </div>
        <div class="col-xs-12">
            <p class="contract-header">${'%s %s' % (contract_title, contract_title_tensio)}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Referència del contracte')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <table>
                        <tr>
                            <td><b>${_('Referència')}</b>: ${'%s/%s' % (contract.name, modcon_activa)}</td>
                            % if contract.abonat:
                                <td><b>${_('Abonat')}</b>: ${contract.abonat}</td>
                            % endif
                            % if contract.ref_dist:
                                <td><b>${_('Pòlissa')}</b>: ${contract.ref_dist}</td>
                            % endif
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Dades del titular')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <p><b>${_('Titular')}:</b> ${contract.titular.name}</p>
                    <p><b>${_('NIF/CIF')}:</b> ${contract.titular.vat}</p>
                    <p><b>${_('Adreça')}:</b> ${dir_titular.street}</p>
                    <p><b>${_('Codi postal')}:</b> ${dir_titular.zip}</p>
                    <p><b>${_('Població')}:</b> ${get_city(dir_titular)}</p>
                    <p><b>${_('Província')}:</b> ${dir_titular.state_id and dir_titular.state_id.name.upper() or ''}</p>
                    <p><b>${_('Pais')}:</b> ${dir_titular.country_id and dir_titular.country_id.name.upper() or ''}</p>
                    <p><b>${_('Telèfon')}:</b> ${dir_titular.phone or ''}</p>
                    <p><b>${_('Fax')}:</b> ${dir_titular.fax or ''}</p>
                    <p><b>${_('Mòbil')}:</b> ${dir_titular.mobile or ''}</p>
                    <p><b>${_('E-mail')}:</b> ${dir_titular.email or ''}</p>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Dades del pagador')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <p><b>${_('Pagador')}:</b> ${contract.pagador.name}</p>
                    <p><b>${_('NIF/CIF')}:</b> ${contract.pagador.vat}</p>
                    <p><b>${_('Adreça')}:</b> ${dir_pagador.street}</p>
                    <p><b>${_('Codi postal')}:</b> ${dir_pagador.zip}</p>
                    <p><b>${_('Població')}:</b> ${get_city(dir_pagador)}</p>
                    <p><b>${_('Província')}:</b> ${dir_pagador.state_id and dir_pagador.state_id.name.upper() or ''}</p>
                    <p><b>${_('Pais')}:</b> ${dir_pagador.country_id and dir_pagador.country_id.name.upper() or ''}</p>
                    <p><b>${_('Telèfon')}:</b> ${dir_pagador.phone or ''}</p>
                    <p><b>${_('Fax')}:</b> ${dir_pagador.fax or ''}</p>
                    <p><b>${_('Mòbil')}:</b> ${dir_pagador.mobile or ''}</p>
                    <p><b>${_('E-mail')}:</b> ${dir_pagador.email or ''}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Dades del punt de subministrament')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <p><b>${_('CUPS')}:</b> ${contract.cups.name}</p>
                    %if contract.cups.direccio:
                        <p><b>${_('Adreça')}:</b> ${contract.cups.direccio}</p>
                    %endif
                    <p><b>${_('Municipi')}:</b> ${contract.cups.id_municipi.name.upper()}</p>
                    <p><b>${_('Provìncia')}:</b> ${contract.cups.id_provincia.name.upper() or ''}</p>
                    <p><b>${_('Referència Cadastral')}:</b> ${contract.cups.ref_catastral or 'ND'}</p>
                    <p><b>${_('Distribuïdora')}:</b> ${contract.cups.distribuidora_id.name.upper()}</p>
                    <p><b>${_('Tensió (V)')}:</b> ${contract.tensio}</p>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Producte contractat')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <p><b>${_('Producte')}:</b> ${contract.llista_preu.name}</p>
                    <p><b>${_('Tarifa d\'accés')}:</b> ${contract.tarifa.name}</p>
                    % for pot in contract.potencies_periode:
                        <p><b>${'%s %s' % (_('Potència'), pot.periode_id.name)}:</b> ${formatLang(pot.potencia, digits=3)} kW</p>
                    % endfor
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Dades de pagament')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    % if contract.tipo_pago.code == 'CAJA':
                        <p><b>${_('Forma de pagament')}:</b> ${'%s (%s)' % (_('Oficines'), contract.payment_mode_id.name)}</p>
                    % elif contract.tipo_pago.code == 'RECIBO_CSB':
                        <table>
                            <tr>
                                <td><b>${_('Forma de pagament')}:</b> ${_('Rebut domiciliat')}</td>
                                <td><b>${_('IBAN')}:</b> ${contract.bank.printable_iban}</td>
                                % if data_firma < '2013-05-01' or 'postal' in contract.enviament:
                                    <td><b>${_('Enviament de factura')}:</b> ${_('Paper')}</td>
                                % else:
                                    <td><b>${_('Enviament de factura')}:</b> ${_('Electrònic')}</td>
                               % endif 
                            </tr>
                            <tr>
                                <td><b>${_("Titular de l'IBAN")}:</b> ${contract.pagador.name}</td>
                                <td><b>${_('NIF/CIF')}:</b> ${contract.pagador.vat}</td>
                            </tr>
                        </table>
                    % elif contract.tipo_pago.code == 'TRANSFERENCIA_CSB':
                        <table>
                            <tr>
                                <td><b>${_('Forma de pagament')}:</b> ${_('Transferència')}</td>
                                <td><b>${_('IBAN')}:</b> ${company_account.printable_iban}</td>
                            </tr>
                        </table>
                    % endif
                </div>
            </div>
        </div>
    </div>
    % if contract.condicions_especials:
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Condicions especials')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <p>${contract.text_condicions}</p>        
                </div>
            </div>
        </div>
    </div>
    % endif
    <div class="row">
        <div class="col-xs-12">
            <p class="long_text">${_("Aquestes condicions del Contracte de subministrament, junt amb les Generals i l'annex de preus que s'adjunten a aquest document, han estat llegits pel client qui, estant conforme amb el contingut de les mateixes les subscriu.")}</p>
            <p class="long_text">${_("A tal efecte, autoritza expressament a ELÈCTRICA SOLLERENSE SAU a que contracti amb la empresa distribuïdora d'accés a tercers a la xarxa (ATR), en els termes indicats a la condició general primera, a fin i efecte de que ELÈCTRICA SOLLERENSE SAU passi a ser el seu nou subministrador.")}</p>
            <p class="long_text">${_("El client declara igualment que les dades i la informació reflexada en les presents condicions son més completes i verídiques autoritzant a l'empresa a fer les comprovacions de les mateixes.")}</p>
        </div>
    </div>
</div>
</div>
<div id="footer">
    <div class="container">
        <div class="row">
            <table style="width: 100%;">
                <tr>
                    <td></td>
                    <td style="text-align: center;">Sóller a ${formatLang(data_firma, date=True)}</td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: center;"><img style="width: 180px;" src="${addons_path}/report_polissa_comer_soller/report/firma_afb.jpg"/></td>
                </tr>
                <tr>
                    <td style="text-align: center;">${_('Firma del client')}</td>
                    <td style="text-align: center;">${company.name}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<p class="page-break"></p>
<div class="container">
    <div class="row top-padding-fix">
        <div class="col-xs-12">
            <p class="contract-header">${_('ANEXE DE PREUS')}</p>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Referència del contracte')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <table>
                        <tr>
                            <td><b>${_('Referència')}</b>: ${'%s/%s' % (contract.name, modcon_activa)}</td>
                            % if contract.abonat:
                                <td><b>${_('Abonat')}</b>: ${contract.abonat}</td>
                            % endif
                            % if contract.ref_dist:
                                <td><b>${_('Pòlissa')}</b>: ${contract.ref_dist}</td>
                            % endif
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Dades del titular')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <p><b>${_('Titular')}:</b> ${contract.titular.name}</p>
                    <p><b>${_('NIF/CIF')}:</b> ${contract.titular.vat}</p>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Dades del pagador')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <p><b>${_('Pagador')}:</b> ${contract.pagador.name}</p>
                    <p><b>${_('NIF/CIF')}:</b> ${contract.pagador.vat}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Dades del punt de subministrament')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <div class="col-xs-6">
                        <p><b>${_('CUPS')}:</b> ${contract.cups.name}</p>
                        %if contract.cups.direccio:
                            <p><b>${_('Adreça')}:</b> ${contract.cups.direccio}</p>
                        %endif
                        <p><b>${_('Municipi')}:</b> ${contract.cups.id_municipi.name.upper()}</p>
                        <p><b>${_('Provìncia')}:</b> ${contract.cups.id_provincia.name.upper() or ''}</p>
                    </div>
                    <div class="col-xs-6">
                        <p><b>${_('Referència Cadastral')}:</b> ${contract.cups.ref_catastral or 'ND'}</p>
                        <p><b>${_('Distribuïdora')}:</b> ${contract.cups.distribuidora_id.name.upper()}</p>
                        <p><b>${_('Tensió (V)')}:</b> ${contract.tensio}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Detall de preus')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <table style="text-align: center;">
                        <tr>
                            <td></td>
                            <td><b>${_('Potència')}</b></td>
                            <td><b>${_('Preu potència')}</b></td>
                            <td><b>${_('Preu energia')}</b></td>
                        <tr>
                    % for periode in periodes:
                        <tr>
                            % if periode in prices_energia:
                                <td>${'%s %s' % (_('Període'), periode)}</td>
                            % else:
                                <td></td>
                            % endif
                            % if periode in periodes_potencia:
                                <td>${formatLang(periodes_potencia[periode], digits=3)} kW</td>
                            % else:
                                <td> </td>
                            % endif
                            % if periode in prices_pot:
                                <td>${formatLang(prices_pot[periode]['price'], digits=6)} ${prices_pot[periode]['uom']}</td>
                            % else:
                                <td> </td>
                            % endif
                            % if periode in prices_energia:
                                <td>${formatLang(prices_energia[periode]['price'], digits=6)} ${prices_energia[periode]['uom']}</td>
                            % else:
                                <td> </td>
                            % endif
                        </tr>
                    % endfor
                    </table>
                    % if comptador and comptador.lloguer and comptador.preu_lloguer > 0:
                        <p class="long_text"><b>${_("Lloguer d'equips de mesura")}</b>: ${'%s %s €/%s' % (comptador.descripcio_lloguer.upper(), formatLang(comptador.preu_lloguer, digits=6), comptador_uom_name)}</p>
                        <p class="long_text small-text">${_("Preu subjecte a modificació segons la disposició que sigui d'aplicació")}</p>
                    %else:
                        <p class="long_text"><b>${_("Lloguer d'equips de mesura")}</b>: ${_("En el cas que el CLIENT disposi d'un Equip de Mesura i Control de lloguer, el règim, així com el preu del mateix són els que estableix la normativa vigent en tot moment.")}</p>
                    % endif
                </div>
            </div>
        </div>
    </div>
    % if len(prices_energia) > 1:
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Calendari d\'aplicació per períodes')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <table>
                    % if 'DHA' in contract.tarifa.name and not contract.llista_preu.agrupacio:
                        <tr>
                            <td style="text-align: center;"><b>${_('Hivern')}</b></td>
                            <td>${_('Període')} P1: 12-22</td>
                            <td style="text-align: center;"><b>${_('Estiu')}</b></td>
                            <td>${_('Període')} P1: 13-23</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>${_('Període')} P2: 0-12 / 22-24</td>
                            <td></td>
                            <td>${_('Període')} P2: 0-13 / 23-24</td>
                        </tr>    
                    % elif contract.tarifa.name == '3.0A' and not contract.llista_preu.agrupacio:
                        <tr>
                            <td style="text-align: center;"><b>${_('Hivern')}</b></td>
                            <td>${_('Període')} P1: 18-22</td>
                            <td style="text-align: center;"><b>${_('Estiu')}</b></td>
                            % if contract.cups.id_provincia and contract.cups.id_provincia.code != '07':
                                <td>${_('Període')} P1: 11-15</td>
                            % else:
                                <td>${_('Període')} P1: 18-22</td>
                            % endif    
                        </tr>
                        <tr>
                            <td></td>
                            <td>${_('Període')} P2: 8-18 / 22-24</td>
                            <td></td>
                            % if contract.cups.id_provincia and contract.cups.id_provincia.code != '07':
                                <td>${_('Període')} P2: 8-11 / 15-24</td>
                            % else:
                                <td>${_('Període')} P2: 8-18 / 22-24</td>
                            % endif
                        </tr>
                        <tr>
                            <td></td>
                            <td>${_('Període')} P3: 0-8</td>
                            <td></td>
                            <td>${_('Període')} P3: 0-8</td>
                        </tr>
                    % elif contract.tarifa.name == '3.1A' and not contract.llista_preu.agrupacio:
                        <tr>
                            <td colspan="2" style="text-align: center;"><b>${_('Hivern')}</b></td>
                            <td colspan="2" style="text-align: center;"><b>${_('Estiu')}</b></td>
                        </tr>
                        <tr>
                            <td><b>${_('Dissabtes, diumenges i festius nacionals')}</b></td>
                            <td>${_('Període')} P1: ${_('No aplica')}</td>
                            <td><b>${_('Dissabtes, diumenges i festius nacionals')}</b></td>
                            <td>${_('Període')} P1: ${_('No aplica')}</td>
                        <tr>
                        <tr>
                            <td></td>
                            <td>${_('Període')} P2: 18-24</td>
                            <td></td>
                            <td>${_('Període')} P2: 18-24</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>${_('Període')} P3: 0-18</td>
                            <td></td>
                            <td>${_('Període')} P3: 0-18</td>
                        </tr>
                        <tr>
                            <td><b>${_('Resta de dies')}</b></td>
                            <td>${_('Període')} P1: 17-23</td>
                            <td><b>${_('Resta de dies')}</b></td>
                            % if contract.cups.id_provincia and contract.cups.id_provincia.code != '07':
                                <td>${_('Període')} P1: 10-16</td>
                            % else:
                                <td>${_('Període')} P1: 17-23</td>
                            % endif
                        <tr>
                        <tr>
                            <td></td>
                            <td>${_('Període')} P2: 8-17 / 23-24</td>
                            <td></td>
                            % if contract.cups.id_provincia and contract.cups.id_provincia.code != '07':
                                <td>${_('Període')} P2: 8-10 / 16-24</td>
                            % else:
                                <td>${_('Període')} P2: 8-17 / 23-24</td>
                            % endif
                        </tr>
                        <tr>
                            <td></td>
                            <td>${_('Període')} P3: 0-8</td>
                            <td></td>
                            <td>${_('Període')} P3: 0-8</td>
                        </tr>
                    % elif contract.tarifa.name.startswith('6.') and not contract.llista_preu.agrupacio:
                        <tr>
                            <td></td>
                            <td colspan="6" style="text-align: center;"><b>${_('Tipus de dia')}</b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="text-align: center;">A</td>
                            <td style="text-align: center;">A1</td>
                            <td style="text-align: center;">B</td>
                            <td style="text-align: center;">B1</td>
                            <td style="text-align: center;">C</td>
                            <td style="text-align: center;">D</td>
                        </tr>
                        <tr>
                            <td>${_('Període')} P1</td>
                            % if contract.cups.id_provincia and contract.cups.id_provincia.code != '07':
                                <td class="table-center table-border">10-13 / 18-21</td>
                            % else:
                                <td class="table-center table-border">11-14 / 18-21</td>
                            % endif
                            <td class="table-center table-border">11-19</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>${_('Període')} P2</td>
                            % if contract.cups.id_provincia and contract.cups.id_provincia.code != '07':
                                <td class="table-center table-border">8-10 / 13-18 / 21-24</td>
                            % else:
                                <td class="table-center table-border">8-11 / 14-18 / 21-24</td>
                            % endif
                            <td class="table-center table-border">8-11 / 19-24</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>${_('Període')} P3</td>
                            <td ></td>
                            <td ></td>
                            <td class="table-center table-border">9-15</td>
                            <td class="table-center table-border">16-22</td>
                            <td ></td>
                            <td ></td>
                        </tr>
                        <tr>
                            <td>${_('Període')} P4</td>
                            <td ></td>
                            <td ></td>
                            <td class="table-center table-border">8-9 / 15-24</td>
                            <td class="table-center table-border">8-16 / 22-24</td>
                            <td></td>
                            <td ></td>
                        </tr>
                        <tr>
                            <td>${_('Període')} P5</td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td class="table-center table-border">8-24</td>
                            <td ></td>
                        </tr>
                        <tr>
                            <td>${_('Període')} P6</td>
                            <td class="table-center table-border">0-8</td>
                            <td class="table-center table-border">0-8</td>
                            <td class="table-center table-border">0-8</td>
                            <td class="table-center table-border">0-8</td>
                            <td class="table-center table-border">0-8</td>
                            <td class="table-center table-border">0-24</td>
                        </tr>
                    % endif
                    </table>
                    % if contract.tarifa.name.startswith('6.') and not contract.llista_preu.agrupacio:
                    % if contract.cups.id_provincia and contract.cups.id_provincia.code != '07':
                    <p class="long_text">Tipo A: De lunes a viernes no festivos de temporada alta con punta de mañana y tarde. Tipo A1: De lunes a viernes no festivos de temporada alta con punta de mañana. Tipo B: De lunes a viernes no festivos de temporada media con punta de mañana. Tipo B1: De lunes a viernes no festivos de temporada media con punta de tarde. Tipo C: De lunes a viernes no festivos de temporada baja, excepto agosto. Tipo D: Sábados, domingos, festivos y agosto.</p>
                    <p class="long_text">Temporada alta con punta de mañana y tarde: Diciembre, enero y febrero. Temporada alta con punta de mañana: Segunda quincena de Junio y Julio. Temporada media con punta de mañana: Primera quincena de junio y septiembre. Temporada media con punta de tarde: Noviembre y marzo. Temporada baja: Abril, mayo, agosto y octubre.</p>
                    % else:
                    <p class="long_text">Tipo A: De lunes a viernes no festivos de temporada alta con punta de mañana y tarde. Tipo A1: De lunes a viernes no festivos de temporada alta con punta de mañana. Tipo B: De lunes a viernes no festivos de temporada media con punta de mañana. Tipo B1: De lunes a viernes no festivos de temporada media con punta de tarde. Tipo C: De lunes a viernes no festivos de temporada baja, excepto abril. Tipo D: Sábados, domingos, festivos y abril.</p>
                    <p class="long_text">Temporada alta con punta de mañana y tarde: Junio, julio, Agosto y septiembre. Temporada media con punta de tarde: Enero, febrero, mayo y octubre. Temporada baja: Marzo, abril, noviembre y diciembre.</p>
                    % endif
                    % endif
                    <p class="long_text">Según orden ITC 2794/2007 de 27 de septiembre. El cambio de estación coincide con el cambio de hora oficial.</p>
                </div>
            </div>
        </div>
    </div>
    % endif
     <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Observacions')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <p class="long_text">Facturación de las tarifas de acceso según reglamentación vigente en cada momento</p>
                    <p class="long_text">Por la facturación de la energía reactiva, si es el caso, será de aplicación aquello establecido en el R.D. 1164/2001 y/o normativa reglamentaria.</p>
                    <p class="long_text">Cada uno de los precios incluidos en este anexo no traen incorporados los correspondientes impuestos.</p>
                </div>
        </div>
    </div>
</div>
<p class="page-break"></p>
<div class="container">
    <div class="row top-padding-fix" style="padding-right: 20px">
        <div class="col-xs-4">
            <h5>1. OBJETO DEL CONTRATO</h5>
            <p class="conditions">El objeto del presente contrato (<b>“Contrato de Suministro”</b>) es el suministro de electricidad por Eléctrica Sollerense S.A.U. (<b>“el Comercializador”</b>) a las instalaciones del Cliente en el punto de conexión (<b>“Punto de suministro”</b>) indicado en las Condiciones Particulares del presente Contrato.</p>
            <p class="conditions">Con la firma del presente Contrato de Suministro, y  salvo que el Cliente no manifieste expresamente su propósito de contratar el acceso a la red de distribución (<b>“Contrato de Acceso”</b>) directamente con la Empresa Distribuidora de la zona donde radica la instalación del Cliente (<b>“Empresa Distribuidora”</b>), el Cliente acepta expresamente la contratación conjunta de la adquisición de la energía y el acceso a la red a través del Comercializador. Por ello, le autoriza expresamente para que, en su nombre, suscriba el Contrato de Acceso con la Empresa Distribuidora, asumiendo su posición jurídica en el mencionado Contrato de Acceso. El Cliente manifiesta que se encuentra al corriente de pago con relación al suministro de electricidad objeto del presente contrato y se obliga a no rescindir el mencionado Contrato de Acceso que el Comercializador formalice con la Empresa Distribuidora, mientras sea vigente este Contrato de Suministro.</p>
            <p class="conditions">Este Contrato de Suministro es de carácter personal, manifestando el Cliente ser el efectivo usuario de la energía, no pudiendo utilizarla en lugar distinto para la que fue contratada, ni cederla, ni venderla a terceros.</p>
            <h5>2. PUNTO DE SUMINISTRO Y CONDICIONES TÉCNICAS</h5>
            <p class="conditions">A los efectos del presente Contrato, se entiende por Punto de Suministro el punto de conexión o entrega situado en la instalación del Cliente, en el que se efectúa la medida del consumo de la energía eléctrica suministrada por el Comercializador.</p>
            <p class="conditions">La tensión nominal y las potencias contratadas con las que se efectúa el suministro son las que figuran en las Condiciones Particulares, de conformidad con lo dispuesto por la normativa vigente y, en particular, conforme al Real Decreto 1164/2001, de 26 de octubre.</p>
            <h5>3. DURACIÓN DEL CONTRATO, ENTRADA EN VIGOR E INICIO DEL SUMINISTRO</h5>
            <p class="conditions">La duración de este Contrato es de un año, a contar a partir de la fecha de inicio del suministro, pudiendo prorrogarse de forma automática por períodos de la misma duración, salvo que cualquiera de las partes comunique su voluntad de resolverlo con una antelación mínima de 15 días naturales a la fecha de su vencimiento o de cualquiera de sus prórrogas, mediante cualquier medio que permita tener constancia de la identidad y de la voluntad del interesado.</p>
            <p class="conditions">El Contrato entrará en vigor en el momento de su firma, aunque su efectividad queda condicionada al otorgamiento por parte de la Empresa Distribuidora del acceso a la red de distribución.</p>
            <h5>4. EQUIPOS DE MEDIDA Y CONTROL, E INSTALACIONES</h5>
            <p class="conditions">El Cliente deberá disponer en el Punto de Suministro, durante la vigencia de este Contrato de Suministro, de un equipo de medida y control de la energía eléctrica suministrada (“Equipo de Medida y Control”) que cumpla los requisitos técnicos legalmente establecidos en la normativa vigente de aplicación y, particularmente, en el Real Decreto 1110/2007, de 24 de agosto, y en la normativa que lo modifique o sustituya.</p>
            <p class="conditions">El Equipo de Medida y Control podrá ser propiedad del Cliente o alquilado a la Empresa Distribuidora. En caso de alquiler, el precio a facturar por este concepto será aplicado por la Empresa Distribuidora como propietaria de los equipos, según el precio que venga establecido por la normativa vigente.</p> 
            <p class="conditions">El Cliente es el responsable de sus instalaciones y de la custodia de los equipos que miden el consumo, y del cumplimiento de las demás obligaciones establecidas por la legislación vigente. En particular, el Cliente se obliga a no manipular ninguno de los componentes de la instalación, y en especial el Equipo de Medida y Control, según lo dispuesto en la normativa vigente. En caso de manipulación, el Comercializador quedará exonerado de cualquier eventualidad que pudiera derivarse del incumplimiento de esta obligación, sin perjuicio de las responsabilidades que legalmente le fueran exigibles al Cliente por dicha manipulación, y sin perjuicio de las refacturaciones que en su caso procedan de acuerdo con la normativa establecida al efecto.</p>
        </div>
        <div class="col-xs-4">
            <p class="conditions">El Cliente, de conformidad con la normativa vigente, deberá garantizar el acceso físico a las instalaciones de medida, en horas hábiles o de normal funcionamiento con el exterior, a la Empresa Distribuidora, o a los empleados o contratistas de éstos, de modo que puedan realizar los trabajos de instalación, lectura, inspección, comprobación, verificación, mantenimiento, precintado u otros que con carácter general resulten necesarios para una prestación eficaz del servicio objeto del presente Contrato de Suministro.</p>
            <p class="conditions">El Cliente se obliga a dotar sus instalaciones interiores o receptoras con mecanismos de protección contra sobreintensidades o sobretensiones, de conformidad con lo establecido en el Art. 16.3 del Reglamento Electrotécnico de Baja Tensión vigente, y según las prescripciones definidas en las instrucciones técnicas ITC-BT-022 e ITC-BT-023 del referido Reglamento.</p>
            <h5>5. PRECIO DEL CONTRATO</h5>
            <p class="conditions">El precio del suministro será el establecido en el Anexo relativo a las Condiciones Particulares, que incluye el importe correspondiente al precio de la energía suministrada, de acuerdo con los precios Anexos a este Contrato, el precio del peaje de acceso y demás componentes regulados por la normativa aplicable que corresponde percibir a la Empresa Distribuidora. Los precios no incluyen los  tributos, o recargos vigentes en cada momento que graven el suministro y/o las actividades necesarias para la operación del suministro eléctrico, que serán exigibles al Cliente.</p>
            <p class="conditions">Asimismo, serán a cargo del Cliente todos aquellos gastos, costes o recargos que resulten legalmente exigibles como consecuencia de la suscripción del Contrato de Suministro y del Contrato de Acceso.</p>
            <p class="conditions">Cualquier variación en los precios en los peajes de acceso, u otros componentes regulados que corresponda percibir a la Empresa Distribuidora, será trasladada de forma automática a los Precios del contrato, sin que constituya modificación de las Condiciones económicas del mismo. En tal caso, la variación de precios será comunicada al Cliente en la facturación siguiente a su aplicación, quedando éste facultado para resolver el Contrato, debiéndose comunicar tal circunstancia al Comercializador en un plazo máximo de 15 días.</p>
            <p class="conditions">En caso de errores administrativos o de funcionamiento incorrecto del Equipo de Medida y Control, o en caso de manipulación de las instalaciones o fraude en la medida del consumo, el Cliente deberá abonar el importe correspondiente a las refacturaciones que por estos conceptos puedan girarse, de conformidad con lo establecido en la normativa vigente.</p>
            <h5>6. LECTURA, FACTURACIÓN Y PAGO</h5>
            <p class="conditions">La periodicidad de la facturación será la indicada en las Condiciones Particulares. La facturación de los consumos de electricidad efectuados se efectuará de acuerdo con las medidas de consumo real facilitadas por la Empresa Distribuidora, en la periodicidad  y conforme a lo dispuesto en la normativa de aplicación, en concreto, para los consumidores en Baja Tensión hasta 15 kW de potencia contratada, el Real Decreto 1718/2012, de 28 de diciembre.</p>
            <p class="conditions">No obstante, en caso de que no se pueda acceder al Equipo de Medida para realizar la lectura, y  el Cliente no ponga a disposición de la Empresa Distribuidora la lectura de su equipo, el Cliente faculta expresamente al Comercializador a facturar según las lecturas estimadas que le facilite la Empresa Distribuidora, en función del procedimiento recogido en la normativa vigente en cada momento. Todo ello, sin perjuicio de la regularización anual que se realice en base a lecturas reales y, en caso de que no sea facilitada a la Empresa Distribuidora por parte del Cliente, en base a estimaciones.</p>
            <p class="conditions">El Comercializador emitirá la factura y detallará el precio y el importe correspondiente a la energía consumida y la potencia contratada, con imputación de la parte correspondiente de los peajes, el precio del alquiler de los equipos de medida y control, y el precio de otros conceptos aplicables, así como los impuestos, recargos, tasas y/o tributos aplicables. La factura también detallará los descuentos y/o refacturaciones complementarias que, en su caso, resulten de aplicación, así como otros costes que la Administración pueda aprobar durante la vigencia del Contrato de Suministro. La facturación de energía reactiva, cuando proceda, se realizará según lo establecido en las Condiciones Particulares, conforme se determina en la legislación vigente.</p>
            <p class="conditions">El pago se realizará según el modo y plazo establecidos en las Condiciones Particulares. La fecha en la que se debe realizar el pago de los recibos domiciliados es aquella en que la entidad bancaria en la que se ha realizado la domiciliación reciba la comunicación con el importe a abonar por el Cliente.</p>
        </div>
        <div class="col-xs-4">
            <h5>7. CALIDAD DEL SUMINISTRO Y DESCUENTOS EN CASO DE INCUMPLIMIENTO</h5>
            <p class="conditions">El suministro se realizará en las condiciones de  continuidad y calidad  previstas en el Real Decreto 1955/2000, de 1 de diciembre (Arts. 101 a 103)  o normativa que lo sustituya.</p>
            <p class="conditions">El incumplimiento por parte de la Empresa Distribuidora de los  niveles de calidad individual  dará lugar a los descuentos y/o bonificaciones correspondientes, siendo repercutidos al Cliente, una vez aplicados por la Empresa Distribuidora, en los plazos reglamentariamente previstos.</p>
            <h5>8. MODIFICACIÓN DE LAS CONDICIONES DEL CONTRATO</h5>
            <p class="conditions">Las condiciones contractuales del presente Contrato podrán ser modificadas por el Comercializador de conformidad con el procedimiento establecido en la presente Condición.</p>
            <p class="conditions">En tal caso, el Comercializador notificará al Cliente la fecha concreta de aplicación de la modificación, con una antelación mínima de un mes a la entrada en vigor de la modificación, informándole de su derecho de resolver el Contrato de suministro sin penalización alguna, en caso de no estar de acuerdo con la modificación propuesta, notificándolo con una antelación mínima de 15 días a la fecha de entrada en vigor de la modificación.</p>
            <p class="conditions">No obstante, en caso de que se trate de una modificación de las Condiciones contractuales como consecuencia directa de la normativa aplicable o de la decisión vinculante de órganos administrativos o judiciales, la comunicación al Cliente se realizará en el momento de su aplicación, pudiendo igualmente el Cliente resolver el Contrato sin ninguna penalización, conforme lo establecido en el apartado anterior.</p>
            <h5>9. DERECHOS DE ACOMETIDA Y DEPÓSITO DE GARANTÍA</h5>
            <p class="conditions">Los trabajos que originen pagos por derechos de acceso, extensión, enganche, o supervisión de instalaciones cedidas, así como los relativos a la verificación o reconexión de instalaciones que corresponda percibir a la Empresa distribuidora, serán a cargo del Cliente en los términos y condiciones previstos reglamentariamente.</p>
            <p class="conditions">Si a la suscripción del Contrato de Acceso a la red de distribución, la Empresa distribuidora solicita el establecimiento de un depósito de garantía, de conformidad con lo establecido en el Art. 79.7 del RD 1955/2000,  éste será repercutido al Cliente. La devolución de este depósito de garantía se realizará en el momento de la resolución del Contrato, sin perjuicio de la facultad de la Empresa Distribuidora de aplicar la parte correspondiente de este depósito al saldo de las cantidades pendientes de pago en el momento de la resolución contractual, de conformidad con lo establecido en el Real Decreto 1955/2000, o normativa que lo sustituya.</p>
            <h5>10. SUSPENSIÓN DEL SUMINISTRO Y RESOLUCION DEL CONTRATO</h5>
            <p class="conditions">En caso de impago por parte del Cliente de una factura vencida, el Comercializador podrá tramitar la suspensión del suministro, si transcurridos 20 días naturales desde la presentación al cobro a la entidad financiera donde el Cliente tenga domiciliado el pago de la factura, o, en el caso de facturas no domiciliadas, transcurrida la fecha límite de pago indicada en la misma, ésta no hubiera sido satisfecha íntegramente. En este sentido, el Comercializador requerirá al Cliente para que, en el plazo de 10 días desde la comunicación, proceda a hacer efectivo el pago de la deuda vencida, así como adicionalmente de los intereses generados. Estos importes serán cuantificados y comunicados al Cliente previamente.</p>
            <p class="conditions">Una vez finalizado el plazo concedido al Cliente para hacer efectivo el pago sin que el mismo se haya hecho efectivo, el Comercializador podrá solicitar a la Empresa Distribuidora que proceda a suspender el suministro eléctrico, comunicándose al Cliente la fecha prevista para la suspensión del suministro, con una antelación mínima de 5 días.</p>
            <p class="conditions">El ejercicio por el Comercializador del derecho a suspender el suministro no exime al Cliente de la obligación de pagar el importe de la facturación pendiente, conforme a lo establecido en el Contrato.</p>
            <p class="conditions">En caso de que el Comercializador opte por suspender el suministro, y el Cliente satisfaga la totalidad de la deuda correspondiente, incluidos los correspondientes a la reposición del suministro, el Comercializador ordenará el restablecimiento del suministro al día siguiente a aquél en que se haya realizado el pago.</p>
        </div>
    </div>
</div>
<p class="page-break"></p>
<div class="container">
    <div class="row top-padding-fix" style="padding-left: 20px;">
        <div class="col-xs-4">
            <p class="conditions">Así mismo, el suministro podrá ser suspendido:</p>
            <p class="conditions">a) En caso de fuerza mayor y caso fortuito.</p>
            <p class="conditions">b) En caso de fraude o manipulación, o de instalaciones peligrosas que supongan riesgo para la seguridad de personas o bienes.</p>
            <p class="conditions">c) En general, en los supuestos previstos en la normativa eléctrica vigente y, especialmente, por incumplimiento de las obligaciones establecidas en el presente Contrato.</p>
            <p class="conditions">Son causas de resolución del Contrato, con independencia de las señaladas en la legislación vigente, las siguientes:</p>
            <p class="conditions">a) La finalización del plazo de vigencia del Contrato o, en su caso, de cualquiera de sus prórrogas.</p>
            <p class="conditions">b) El incumplimiento, por cualquiera de las partes, de cualquier obligación derivada del Contrato, en especial la falta de pago  de cualquiera de las facturas por parte del Cliente.</p>
            <p class="conditions">c) El ejercicio por parte del Cliente del derecho de resolver el contrato, como consecuencia de las modificaciones contractuales por parte del Comercializador.</p>
            <p class="conditions">En caso de rescisión unilateral del Contrato por parte del Cliente con anterioridad a la fecha de finalización del mismo (primer año), que no venga motivada por una modificación contractual por parte del Comercializador, podrá comportar una penalización económica consistente en el 5% del precio de la energía estimada pendiente de suministro. La rescisión de las eventuales prórrogas del Contrato por parte del Cliente no dará lugar a ningún tipo de penalización.</p>
            <h5>11. FUERZA MAYOR Y CASO FORTUITO</h5>
            <p class="conditions">No responderán ni el Cliente ni el Comercializador de los eventuales incumplimientos del presente Contrato de Suministro en casos de fuerza mayor y caso fortuito. Se consideran causa de fuerza mayor los hechos imprevisibles o que previstos sean inevitables e irresistibles, ajenos a las partes, desde el momento en que los mismos imposibiliten el cumplimiento de las obligaciones del presente contrato de Suministro. En particular, no responderá el Comercializador si existe una imposibilidad por parte del Comercializador de adquirir o hacer llegar la energía eléctrica al Cliente, por causas no imputables a él, o por intervención directa o indirecta de terceros.</p>
            <h5>12. CESIÓN Y SUBROGACIÓN DEL CONTRATO</h5>
            <p class="conditions">El Cliente, siempre que esté al corriente de pago, podrá traspasar su contrato a otro consumidor que vaya a utilizarlo en el mismo emplazamiento y para el mismo uso, subrogándose el nuevo usuario en las mismas condiciones. Asimismo, deberá acreditarse la conformidad del nuevo cliente y aportar sus datos personales, CIF o NIF/NIE y los datos de cuenta bancaria.</p>
            <p class="conditions">El Cliente autoriza al Comercializador a ceder el presente Contrato y los derechos y obligaciones dimanantes del  mismo a cualquier sociedad participada, vinculada o sucesora que pueda prestar en un futuro el servicio objeto del presente Contrato, bastando a tal efecto la oportuna comunicación al Cliente.</p>
            <h5>13. DESISTIMIENTO</h5>
            <p class="conditions">En caso de que el Contrato se haya celebrado a distancia (por teléfono o por Internet) o fuera de establecimiento mercantil, y que el Cliente tenga la condición de consumidor y usuario, de conformidad con el Real Decreto Legislativo 1/2007, de 16 de noviembre, el Cliente podrá desistir del presente Contrato durante el período de 14 días naturales siguientes a su celebración, sin necesidad de justificar su decisión y sin ninguna penalización.</p>
            <p class="conditions">Para ejercer el derecho de desistimiento, el Cliente deberá notificar a la Comercializadora de forma inequívoca, a la dirección que figura más abajo, su decisión de desistir del Contrato. El Cliente puede solicitar el modelo de formulario de desistimiento a la Comercializadora.</p>
            <p class="conditions">En caso de ejercer su derecho de desistimiento, serán devueltos al Cliente todos los pagos en su caso efectuados, y, como muy tarde, 14 días naturales a partir de la fecha en la que se informe de su decisión de desistir del Contrato. En ningún caso el Cliente incurrirá en gasto alguno como consecuencia del reembolso.</p>
            <p class="conditions">En caso de que el Cliente previamente hubiera solicitado que el suministro de energía eléctrica se iniciase durante el período de desistimiento, el Cliente estará obligado a abonar un importe proporcional a la parte ya prestada del servicio en el momento que comunique el desistimiento, en relación con el objeto total del Contrato.</p>
        </div>
        <div class="col-xs-4">
            <h5>14. SERVICIO DE ATENCIÓN AL CLIENTE Y VÍAS DE SOLUCIÓN DE CONFLICTOS</h5>
            <p class="conditions">Para cuestiones relacionadas con el presente Contrato, o para cualquier incidencia o reclamación en relación con el servicio contratado, el Cliente puede dirigirse al Servicio de Atención del Cliente del Comercializador, de manera presencial en C/ Sa Mar 146, 07100 Sóller, Illes Balears, llamando al número de teléfono gratuito 900373417, o bien puede dirigirse a la dirección de correo electrónico oficines@electricasollerense.es.</p>
            <p class="conditions">En caso de que el Cliente presente una reclamación, de no resolverse en el plazo de un (1) mes, o si resuelta, no fuera estimada, el Cliente podrá presentar su reclamación ante la Direccion General de Salud Pública y Consumo de Baleares, a los efectos de la tramitación de los servicios de mediación y sistema arbitral de consumo que, en su caso, sean de aplicación, para los supuestos en los que el Comercializador se adhiera.</p>
            <p class="conditions">Para el supuesto de que la controversia no se someta a ninguna entidad de resolución alternativa de litigios en materia de consumo, o que éstas no resulten competentes para la resolución del conflicto, el Cliente que sea persona física podrá someter la controversia al Ministerio de Industria, Energía y Turismo, cuando tales controversias se refieran  a sus derechos específicos como usuarios finales. En este caso, el procedimiento aplicable será aprobado por Orden del Ministerio de Industria, Energía y Turismo, sin que puedan ser objeto del mismo las controversias que se encuentren reguladas por normativa distinta de la de protección específica de los usuarios finales de energía eléctrica.</p>
            <h5>15. LEGISLACIÓN Y JURISDICCIÓN</h5>
            <p class="conditions">El presente Contrato de Suministro estará regido y será interpretado de acuerdo con la ley española aplicable y, en particular, con la Ley 24/2013, de 26 de Diciembre, del Sector Eléctrico, el Real Decreto 1955/2000, de 1 de diciembre y, en su caso, normativa que la sustituya, y conforme su normativa de desarrollo.</p>
            <p class="conditions">En el caso de que se produzca cualquier discrepancia o controversia con motivo de la interpretación, aplicación o ejecución del presente Contrato, el Comercializador y el Cliente, se someten a la jurisdicción de los Juzgados y Tribunales correspondientes al lugar donde radica la instalación de la que es titular el Cliente y en la que se presta el suministro.</p>
            <h5>16. PROTECCIÓN DE DATOS DE CARÁCTER PERSONAL</h5>
            <p class="conditions">A los efectos de lo dispuesto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, y en la normativa vigente relativa a tratamientos de datos de carácter personal, el Cliente queda informado y presta su consentimiento para que sus datos sean incorporados en un fichero creado bajo la responsabilidad del Comercializador, y le autoriza para el tratamiento de estos datos con la finalidad de realizar el mantenimiento y la gestión de la relación contractual con el Cliente, así como de las labores de información y comercialización de los servicios ofrecidos por el Comercializador u otras empresas del Grupo.</p>
            <p class="conditions">El Cliente es responsable de la veracidad de los datos comunicados en todo momento al Comercializador, y se compromete a solicitar su modificación cuando sea necesario, a fin de garantizar una correcta prestación de los servicios contratados.</p>
            <p class="conditions">Del mismo modo, el Cliente presta su consentimiento para el tratamiento de los datos contenidos en el fichero de aquellas empresas cuya intervención sea necesaria para la prestación del Servicio. Según dispone la Ley Orgánica 15/1999, de 13 de diciembre, el Comercializador se compromete a su deber de guardar secreto de los datos de carácter personal, y adoptará las medidas legalmente previstas y necesarias para evitar su alteración, pérdida, tratamiento o acceso no autorizado. En particular, el Cliente queda informado que los datos necesarios para gestionar el Acceso a la Red, serán comunicadas a la Empresa Distribuidora de la zona, y quedarán incorporados en el fichero del Sistema de Información de Puntos de Suministro, del que es responsable la Empresa Distribuidora, al que podrán tener acceso la Comisión Nacional de los Mercados y la Competencia y las empresas comercializadoras que lo soliciten. El Cliente podrá prohibir expresamente la difusión de estos datos mediante escrito dirigido a la Empresa Distribuidora, excepto en el caso de que se encuentre en situación de impago.</p>
        </div>
        <div class="col-xs-4">
            <p class="conditions">A su vez, y excepto manifestación en contrario en cualquier momento a la suscripción del Contrato, el Cliente consiente expresamente el tratamiento de los datos de carácter personal contenidos en el fichero para que el Comercializador pueda remitir por cualquier medio información comercial de los productos y servicios comercializados por el Grupo y sus filiales o terceros relacionados con el suministro de energía, el mantenimiento energético y de equipos, las telecomunicaciones e internet, los servicios financieros, la asistencia en hogar, o para realizar prospecciones relacionadas con los productos o servicios mencionados, así como la cesión a las empresas del Grupo o a sus filiales con la misma finalidad.</p>
            <p class="conditions">El Cliente queda informado que podrá revocar su consentimiento a la cesión de datos así como ejercer sus derechos de acceso, de rectificación, cancelación de los datos contenidos en el Fichero del Comercializador a través de comunicación escrita dirigida al responsable del Fichero Eléctrica Sollerense SAU con CIF A57048332 y domicilio en C/ Sa Mar 146, 07100 Sóller, Illes Balears.</p>
        </div>
    </div>
</div>
% if objects.index(object) + 1 != len(objects):
    <p class="page-break"></p>
% endif
</body>
%endfor
</html>
