# -*- coding: utf-8 -*-

import jasper_reports

def datos_administrativos(cr, uid, ids, data, context):

    return {
        'ids': ids,
        'parameters': {'REPORT_LOCALE': 'es_ES',
                       'blank': False},
    }

def datos_administrativos_blanco(cr, uid, ids, data, context):

    return {
        'ids': ids,
        'parameters': {'REPORT_LOCALE': 'es_ES',
                       'blank': True},
    }


jasper_reports.report_jasper(
   'report.report_polissa_comer_soller_dades_administratives',
   'giscedata.polissa',
   parser=datos_administrativos
)

jasper_reports.report_jasper(
   'report.report_polissa_comer_soller_dades_administratives_blanc',
   'giscedata.polissa',
   parser=datos_administrativos_blanco
)