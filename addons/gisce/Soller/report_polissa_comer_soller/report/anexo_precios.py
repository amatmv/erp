# -*- coding: utf-8 -*-

import jasper_reports

def anexo_precios(cr, uid, ids, data, context):
    data['form']['parameters'].update({'REPORT_LOCALE': 'es_ES'})
    return {
        'ids': data['form']['parameters']['ids'],
        'parameters': data['form']['parameters'],
    }

jasper_reports.report_jasper(
   'report.annexe_preus',
   'giscedata.polissa',
   parser=anexo_precios
)

