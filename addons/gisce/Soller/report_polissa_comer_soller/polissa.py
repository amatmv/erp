# -*- coding: utf-8 -*-

from osv import osv, fields


class SollerModcontractualContract(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    def action_print_contract(self, cursor, uid, ids, context=None):

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'report_contrato_webkit',
        }

SollerModcontractualContract()