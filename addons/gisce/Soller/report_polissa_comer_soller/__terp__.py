# -*- coding: utf-8 -*-
{
    "name": "Reports Pòlissa Sóller (Comercialitzadora)",
    "description": """
Modificació dels reports de pòlissa de comercialitzadora:
  * Contracte
  * Annexe de preus
  * Autorització domiciliació Bancaria
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "base",
        "product",
        "giscedata_polissa_comer",
        "jasper_reports",
        "c2c_webkit_report"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_polissa_comer_soller_report.xml",
        "report_polissa_comer_soller_wizard.xml",
        "report_polissa_comer_soller_view.xml"
    ],
    "active": False,
    "installable": True
}
