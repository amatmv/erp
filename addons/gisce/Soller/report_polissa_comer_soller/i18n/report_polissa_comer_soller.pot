# #-#-#-#-#  report_polissa_comer_soller.pot.old (OpenERP Server 5.0.14)  #-#-#-#-#
# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 	* report_polissa_comer_soller
#
# #-#-#-#-#  reports_webkit.pot (PROJECT VERSION)  #-#-#-#-#
# Translations template for PROJECT.
# Copyright (C) 2018 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"#-#-#-#-#  report_polissa_comer_soller.pot.old (OpenERP Server 5.0.14)  #-#-"
"#-#-#\n"
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2018-01-25 12:58\n"
"PO-Revision-Date: 2018-01-25 12:58\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"
"#-#-#-#-#  reports_webkit.pot (PROJECT VERSION)  #-#-#-#-#\n"
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2018-01-25 12:58+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.5.3\n"

#. module: report_polissa_comer_soller
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: report_polissa_comer_soller
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr ""
"The Object name must start with x_ and not contain any special character !"

#. module: report_polissa_comer_soller
#: model:ir.module.module,shortdesc:report_polissa_comer_soller.module_meta_information
msgid "Reports Pòlissa Sóller (Comercialitzadora)"
msgstr "Reports Pòlissa Sóller (Comercialitzadora)"

#. module: report_polissa_comer_soller
#: model:ir.actions.wizard,name:report_polissa_comer_soller.report_polissa_comer_soller_wizard
msgid "Annexe de preus"
msgstr "Annexe de preus"

#. module: report_polissa_comer_soller
#: model:ir.model,name:report_polissa_comer_soller.model_wizard_anulacio_banc
msgid "wizard.anulacio.banc"
msgstr "wizard.anulacio.banc"

#. module: report_polissa_comer_soller
#: view:giscedata.polissa.modcontractual:0
msgid "Imprimir"
msgstr "Imprimir"

#. module: report_polissa_comer_soller
#: model:ir.module.module,description:report_polissa_comer_soller.module_meta_information
msgid ""
"\n"
"Modificació dels reports de pòlissa de comercialitzadora:\n"
"  * Contracte\n"
"  * Annexe de preus\n"
"  * Autorització domiciliació Bancaria\n"
"    "
msgstr ""
"\n"
"Modificació dels reports de pòlissa de comercialitzadora:\n"
"  * Contracte\n"
"  * Annexe de preus\n"
"  * Autorització domiciliació Bancaria\n"
"    "

#. module: report_polissa_comer_soller
#: model:ir.actions.report.xml,name:report_polissa_comer_soller.report_giscedata_polissa_empty_administrative_data_webkit
msgid "Dades administratives en blanc"
msgstr "Dades administratives en blanc"

#. module: report_polissa_comer_soller
#: view:giscedata.polissa.modcontractual:0
#: model:ir.actions.report.xml,name:report_polissa_comer_soller.report_contrato_webkit
msgid "Contracte Nou"
msgstr "Contracte Nou"

#. module: report_polissa_comer_soller
#: model:ir.actions.report.xml,name:report_polissa_comer_soller.report_giscedata_polissa_administrative_data_webkit
msgid "Dades administratives"
msgstr "Dades administratives"

#: rml:report_polissa_comer_soller/report/administrative_data.mako:426
msgid ""
"Conforme a lo previsto en la Ley Orgánica de Protección de Datos de "
"Carácter\n"
"                                    Personal, Ley 15/1999 de 13 de diciembre "
"(en adelante LOPD), Eléctrica Sollerense\n"
"                                    le informa que los datos obtenidos a "
"través de este formulario o contrato están\n"
"                                    incluidos en varios fichero mixtos, y "
"cuya Responsable de los Ficheros es Eléctrica\n"
"                                    Sollerense con CIF A57048332 con "
"domicilio en C/Sa Mar 146, 07100 - Sóller, Islas\n"
"                                    Baleares. Le informamos que de acuerdo "
"con la LOPD usted podrá ejercer gratuitamente\n"
"                                    sus derechos de acceso, rectificación, "
"cancelación y oposición de conformidad con\n"
"                                    los art.15, 16 y 17 dirigiendo un "
"escrito a Eléctrica Sollerense con CIF A57048332\n"
"                                    con domicilio en C/Sa Mar 146, 07100 - "
"Sóller, Islas Baleares. En virtud de lo\n"
"                                    establecido en la LOPD y en el RLOPD, el "
"Responsable de los Ficheros permitirá el\n"
"                                    acceso del Encargado del Tratamiento a "
"los datos de carácter personal contenidos en\n"
"                                    los Ficheros, para que este realice el "
"tratamiento de los mismos con el objeto de\n"
"                                    dar el cumplimiento prestación al "
"contrato de prestación de servicios suscrito entre\n"
"                                    las partes. Siendo en este caso "
"aplicable Articulo 12 de la LOPD en relación con\n"
"                                    los artículos 4 de la LOPD calidad de "
"los datos, art. 5 de la LOPD derecho de\n"
"                                    información en la recogida de los datos, "
"art. 6 de la LOPD consentimiento del\n"
"                                    afectado, art. 7 de la LOPD datos "
"especialmente protegidos, art. 8 de la LOPD datos\n"
"                                    relativos a salud, art. 9 de la LOPD "
"seguridad de los datos, art. 10 de la LOPD\n"
"                                    deber de secreto, art. 11 comunicación "
"de los datos y art. 12 acceso de datos por\n"
"                                    cuenta de terceros. Todo ello de "
"conformidad con la legislación aplicable; en\n"
"                                    cualquier caso será de aplicación el "
"art. 11.2 de la LOPD y art. 10.22 del\n"
"                                    reglamento de la LOPD, en relación a la "
"ORDEN ITC 3860/2007 de 28 de DICIEMBRE y\n"
"                                    le será aplicable específicamente el "
"art. 41, 44, 45, 47 de la Ley 54/1997 de 27 de\n"
"                                    noviembre del sector eléctrico y "
"posterior modificaciones con la Ley 17/2007 en\n"
"                                    cuanto a las cesiones y accesos de datos "
"de los clientes, de las empresas\n"
"                                    comercializadoras y distribuidoras, en "
"el sistema de información de puntos de\n"
"                                    suministro. Y así mismo el cliente "
"consiente de manera expresa en que el encargado\n"
"                                    del tratamiento realice las cesiones de "
"los datos contenidos en los ficheros a las\n"
"                                    entidades y organismos oficiales "
"competentes en la materia de conformidad con la\n"
"                                    legislación aplicable."
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:124
msgid ""
"CONDICIONS PARTICULARS DEL CONTRACTE DE SUBMINISTRAMENT D'ENERGIA ELÈCTRICA"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:126
msgid "D'ALTA TENSIÓ"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:128
msgid "DE BAIXA TENSIÓ"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:141
msgid "dia"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:143
msgid "mes"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:153
msgid "Dades d'empresa"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:159
#: rml:report_polissa_comer_soller/report/contrato.mako:209
#: rml:report_polissa_comer_soller/report/contrato.mako:229
msgid "Telèfon"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:159
#: rml:report_polissa_comer_soller/report/contrato.mako:210
#: rml:report_polissa_comer_soller/report/contrato.mako:230
msgid "Fax"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:177
#: rml:report_polissa_comer_soller/report/contrato.mako:358
msgid "Referència del contracte"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:182
#: rml:report_polissa_comer_soller/report/contrato.mako:363
msgid "Referència"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:184
#: rml:report_polissa_comer_soller/report/contrato.mako:365
msgid "Abonat"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:187
#: rml:report_polissa_comer_soller/report/contrato.mako:368
msgid "Pòlissa"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:199
#: rml:report_polissa_comer_soller/report/contrato.mako:380
msgid "Dades del titular"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:202
#: rml:report_polissa_comer_soller/report/contrato.mako:383
msgid "Titular"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:203
#: rml:report_polissa_comer_soller/report/contrato.mako:223
#: rml:report_polissa_comer_soller/report/contrato.mako:291
#: rml:report_polissa_comer_soller/report/contrato.mako:384
#: rml:report_polissa_comer_soller/report/contrato.mako:395
msgid "NIF/CIF"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:204
#: rml:report_polissa_comer_soller/report/contrato.mako:224
#: rml:report_polissa_comer_soller/report/contrato.mako:245
#: rml:report_polissa_comer_soller/report/contrato.mako:409
msgid "Adreça"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:205
#: rml:report_polissa_comer_soller/report/contrato.mako:225
msgid "Codi postal"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:206
#: rml:report_polissa_comer_soller/report/contrato.mako:226
msgid "Població"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:207
#: rml:report_polissa_comer_soller/report/contrato.mako:227
msgid "Província"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:208
#: rml:report_polissa_comer_soller/report/contrato.mako:228
msgid "Pais"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:211
#: rml:report_polissa_comer_soller/report/contrato.mako:231
msgid "Mòbil"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:212
#: rml:report_polissa_comer_soller/report/contrato.mako:232
msgid "E-mail"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:219
#: rml:report_polissa_comer_soller/report/contrato.mako:391
msgid "Dades del pagador"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:222
#: rml:report_polissa_comer_soller/report/contrato.mako:394
msgid "Pagador"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:241
#: rml:report_polissa_comer_soller/report/contrato.mako:404
msgid "Dades del punt de subministrament"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:244
#: rml:report_polissa_comer_soller/report/contrato.mako:408
msgid "CUPS"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:246
#: rml:report_polissa_comer_soller/report/contrato.mako:410
msgid "Municipi"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:247
#: rml:report_polissa_comer_soller/report/contrato.mako:411
msgid "Provìncia"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:248
#: rml:report_polissa_comer_soller/report/contrato.mako:414
msgid "Referència Cadastral"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:249
#: rml:report_polissa_comer_soller/report/contrato.mako:415
msgid "Distribuïdora"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:250
#: rml:report_polissa_comer_soller/report/contrato.mako:416
msgid "Tensió (V)"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:257
msgid "Producte contractat"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:260
msgid "Producte"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:261
msgid "Tarifa d'accés"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:263
#: rml:report_polissa_comer_soller/report/contrato.mako:432
msgid "Potència"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:273
msgid "Dades de pagament"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:277
#: rml:report_polissa_comer_soller/report/contrato.mako:281
#: rml:report_polissa_comer_soller/report/contrato.mako:297
msgid "Forma de pagament"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:277
msgid "Oficines"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:281
msgid "Rebut domiciliat"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:282
#: rml:report_polissa_comer_soller/report/contrato.mako:298
msgid "IBAN"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:284
#: rml:report_polissa_comer_soller/report/contrato.mako:286
msgid "Enviament de factura"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:284
msgid "Paper"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:286
msgid "Electrònic"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:290
msgid "Titular de l'IBAN"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:297
msgid "Transferència"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:311
msgid "Condicions especials"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:322
msgid ""
"Aquestes condicions del Contracte de subministrament, junt amb les Generals "
"i l'annex de preus que s'adjunten a aquest document, han estat llegits pel "
"client qui, estant conforme amb el contingut de les mateixes les subscriu."
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:323
msgid ""
"A tal efecte, autoritza expressament a ELÈCTRICA SOLLERENSE SAU a que "
"contracti amb la empresa distribuïdora d'accés a tercers a la xarxa (ATR), "
"en els termes indicats a la condició general primera, a fin i efecte de que "
"ELÈCTRICA SOLLERENSE SAU passi a ser el seu nou subministrador."
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:324
msgid ""
"El client declara igualment que les dades i la informació reflexada en les "
"presents condicions son més completes i verídiques autoritzant a l'empresa a "
"fer les comprovacions de les mateixes."
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:342
msgid "Firma del client"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:353
msgid "ANEXE DE PREUS"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:426
msgid "Detall de preus"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:433
msgid "Preu potència"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:434
msgid "Preu energia"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:439
#: rml:report_polissa_comer_soller/report/contrato.mako:481
#: rml:report_polissa_comer_soller/report/contrato.mako:483
#: rml:report_polissa_comer_soller/report/contrato.mako:487
#: rml:report_polissa_comer_soller/report/contrato.mako:489
#: rml:report_polissa_comer_soller/report/contrato.mako:494
#: rml:report_polissa_comer_soller/report/contrato.mako:497
#: rml:report_polissa_comer_soller/report/contrato.mako:499
#: rml:report_polissa_comer_soller/report/contrato.mako:504
#: rml:report_polissa_comer_soller/report/contrato.mako:507
#: rml:report_polissa_comer_soller/report/contrato.mako:509
#: rml:report_polissa_comer_soller/report/contrato.mako:514
#: rml:report_polissa_comer_soller/report/contrato.mako:516
#: rml:report_polissa_comer_soller/report/contrato.mako:525
#: rml:report_polissa_comer_soller/report/contrato.mako:527
#: rml:report_polissa_comer_soller/report/contrato.mako:531
#: rml:report_polissa_comer_soller/report/contrato.mako:533
#: rml:report_polissa_comer_soller/report/contrato.mako:537
#: rml:report_polissa_comer_soller/report/contrato.mako:539
#: rml:report_polissa_comer_soller/report/contrato.mako:543
#: rml:report_polissa_comer_soller/report/contrato.mako:546
#: rml:report_polissa_comer_soller/report/contrato.mako:548
#: rml:report_polissa_comer_soller/report/contrato.mako:553
#: rml:report_polissa_comer_soller/report/contrato.mako:556
#: rml:report_polissa_comer_soller/report/contrato.mako:558
#: rml:report_polissa_comer_soller/report/contrato.mako:563
#: rml:report_polissa_comer_soller/report/contrato.mako:565
#: rml:report_polissa_comer_soller/report/contrato.mako:582
#: rml:report_polissa_comer_soller/report/contrato.mako:595
#: rml:report_polissa_comer_soller/report/contrato.mako:608
#: rml:report_polissa_comer_soller/report/contrato.mako:617
#: rml:report_polissa_comer_soller/report/contrato.mako:626
#: rml:report_polissa_comer_soller/report/contrato.mako:635
msgid "Període"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:462
msgid "Lloguer d'equips de mesura"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:463
msgid "Preu subjecte a modificació segons la disposició que sigui d'aplicació"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:474
msgid "Calendari d'aplicació per períodes"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:480
#: rml:report_polissa_comer_soller/report/contrato.mako:493
#: rml:report_polissa_comer_soller/report/contrato.mako:520
msgid "Hivern"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:482
#: rml:report_polissa_comer_soller/report/contrato.mako:495
#: rml:report_polissa_comer_soller/report/contrato.mako:521
msgid "Estiu"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:524
#: rml:report_polissa_comer_soller/report/contrato.mako:526
msgid "Dissabtes, diumenges i festius nacionals"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:525
#: rml:report_polissa_comer_soller/report/contrato.mako:527
msgid "No aplica"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:542
#: rml:report_polissa_comer_soller/report/contrato.mako:544
msgid "Resta de dies"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:570
msgid "Tipus de dia"
msgstr ""

#: rml:report_polissa_comer_soller/report/contrato.mako:664
msgid "Observacions"
msgstr ""
