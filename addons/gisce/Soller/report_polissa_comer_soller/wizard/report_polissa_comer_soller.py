# -*- coding: utf-8 -*-

import wizard
import pooler

class WizardPolissaComerSoller(wizard.interface):
    """Wizard per llençar el report dels preus del contracte.

    Es fa servir per passar paràmetres al Jasper.
    """

    def _get_params(self, cr, uid, data, context=None):
        """Funció per cercar i retornar els paràmetres.
        """
        if not context:
            context = {}

        pool = pooler.get_pool(cr.dbname)
        ids = []
        res = {'ids': data['ids']}
        keys = {'te': 'E', 'tp': 'P'}
        contractes = pool.get('giscedata.polissa').browse(cr, uid, data['ids'])

        for contracte in contractes:
            tarifa = contracte.versio_primera_factura.pricelist_id
            versio_date = contracte.versio_primera_factura.date_start
            if contracte.data_firma_contracte > versio_date:
                context['date'] = contracte.data_firma_contracte
            else:
                context['date'] = versio_date
            for period in contracte.tarifa.periodes:
                producte = period.product_id
                p_name = 'P%s' % (producte.name[-1],)
                if (tarifa.agrupacio and
                    tarifa.get_product_group(producte.id)):
                    continue
                else:
                    key = '%s%s' % (p_name, keys[period.tipus])
                    res[key] = tarifa.price_get(producte.id, 100,
                                                contracte.titular.id,
                                                context=context)[tarifa.id]
            for period in contracte.potencies_periode:
                producte = period.periode_id
                p_name = 'P%s' % (producte.product_tmpl_id.name[-1],)
                res['%sW' % (p_name,)] = period.potencia
        res.update({'tarifa_agrupada': tarifa.agrupacio})
        return {'parameters': res}

    states = {
        'init': {
            'actions': [_get_params],
            'result': {
                'type': 'print',
                'name': 'Llista de preus de contracte',
                'report': 'annexe_preus',
                'xml': 'report_polissa_comer_soller/report/anexo_precios.jrxml',
                'state': 'end',
            }
        }
    }

WizardPolissaComerSoller('wizard_polissa_comer_soller')
