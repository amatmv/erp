# -*- coding: utf-8 -*-

from osv import osv, fields


class ContinuousInvoicingInvoicerEmployee(osv.osv):

    _inherit = 'continuous.invoicing.invoicer'

    def get_contracts(self, cursor, uid, date, context=None):
        '''
            get contracts to invoice except retiree contracts
        '''
        contract_obj = self.pool.get('giscedata.polissa')

        if not context:
            context = {}

        contract_ids = super(ContinuousInvoicingInvoicerEmployee, self
                             ).get_contracts(cursor, uid, date, context=context)

        ctx = context.copy()
        ctx.update({'active_test': False})

        # filter retiree contracts
        search_params = [('lot_facturacio', '=', None),
                         ('employee', '=', True),
                         ('employee_group', '=', 'group1'),
                         ]

        retiree_ids = contract_obj.search(cursor, uid, search_params, context=ctx)

        return list(set(contract_ids) - set(retiree_ids))


ContinuousInvoicingInvoicerEmployee()