# -*- coding: utf-8 -*-

from osv import osv, fields

employee_group_sel = [('group1', 'Group 1'),
                      ('group2', 'Group 2'),
                      ('group3', 'Group 3'),
                      ('group4', 'Group 4')]


class SollerFacturacioPolissaEmployee(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _states = {'esborrany': [('readonly', False)],
               'validar': [('readonly', False)],
               'modcontractual': [('readonly', False)]
               }

    _group_help = "1- Retired\n2- VSE\n3- ES\n4- Board"

    _columns = {
        'employee': fields.boolean('Employee', readonly=True,
                                   states=_states),
        'employee_group': fields.selection(employee_group_sel, 'Group',
                                           readonly=True, states=_states,
                                           help=_group_help)
    }

    _defaults = {
        'employee': lambda *a: False,
    }

SollerFacturacioPolissaEmployee()


class SollerFacturacioModcontractualEmployee(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'employee': fields.boolean('Employee'),
        'employee_group': fields.selection(employee_group_sel, 'Group')
    }

    _defaults = {
        'employee': lambda *a: False,
    }

SollerFacturacioModcontractualEmployee()
