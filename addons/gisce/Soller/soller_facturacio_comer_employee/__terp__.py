# -*- coding: utf-8 -*-
{
    "name": "Employee invoicing",
    "description": """
    Apply employee invoicing rules
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_facturacio_comer",
        "continuous_invoice"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
