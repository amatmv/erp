# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataFacturacioSollerEmployee(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def apply_employee_reduction(self, cursor, uid, ids, context=None):

        line_obj = self.pool.get('giscedata.facturacio.factura.linia')

        for factura_id in ids:
            factura = self.browse(cursor, uid, factura_id)
            for line in factura.linies_energia:
                new_vals = {'invoice_line_tax_id': [(6, 0, [])],
                            'tipus': 'altres',
                            'price_unit_multi': line.price_unit_multi * -1,
                            'name': 'Descuento empleado %s' % line.name}
                line_obj.copy(cursor, uid, line.id, new_vals)
            self.button_reset_taxes(cursor, uid, [factura_id])

        return True

    def apply_employee(self, cursor, uid, ids, context=None):
        '''Apply employee rules based on employee group
        Group 1 - No invoice
        Group 2 and Group 3 - Energy with no taxes refund
        Group 4 - Nothing to do'''

        polissa_obj = self.pool.get('giscedata.polissa')

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        values = self.read(cursor, uid, ids, ['polissa_id', 'data_final'],
                           context=context)

        for value in values:
            factura_id = value['id']
            polissa_id = value['polissa_id'][0]
            data_final = value['data_final']
            polissa_vals = polissa_obj.read(cursor, uid, polissa_id,
                                            ['employee', 'employee_group'],
                                            context={'date': data_final})
            if not polissa_vals['employee']:
                continue

            # Group 1
            if polissa_vals['employee_group'] == 'group1':
                self.unlink(cursor, uid, [factura_id])

            # Group 2 and 3
            if polissa_vals['employee_group'] in ('group2', 'group3'):
                self.apply_employee_reduction(cursor, uid, [factura_id])

        return True

GiscedataFacturacioSollerEmployee()


class GiscedataFacturadorSollerEmployee(osv.osv):

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):

        factura_obj = self.pool.get('giscedata.facturacio.factura')

        res = super(GiscedataFacturadorSollerEmployee,
                    self).fact_via_lectures(cursor, uid, polissa_id, lot_id,
                                            context=context)
        factura_obj.apply_employee(cursor, uid, res, context=context)

        return res

GiscedataFacturadorSollerEmployee()
