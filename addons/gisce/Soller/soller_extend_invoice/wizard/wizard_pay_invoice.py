# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    Copyright (C) 2011 GISCE Enginyeria (http://gisce.net). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _

class SollerWizardPayInvoice(osv.osv_memory):

    _name = "facturacio.pay.invoice"
    _inherit = "facturacio.pay.invoice"

    def default_get(self, cr, uid, fields, context={}):
        
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        journal_obj = self.pool.get('account.journal')

        res = super(SollerWizardPayInvoice, self).default_get(cr, uid, fields, context)
        #Si res no conte el camp journal_id, no hem de fer res
        #Per no sobreescriure els valors
        if not res.get('journal_id',False):
            return res

        factura_id = context.get('active_id',False)
        factura = factura_obj.browse(cr, uid, factura_id)
    
        search_params = [('type','=','cash'),
                         ('sequence_id','=',factura.journal_id.invoice_sequence_id.id),
                        ]
        journal_id = journal_obj.search(cr, uid, search_params)
        if journal_id:
            if isinstance(journal_id,list) or isinstance(journal_id,tuple):
                journal_id = journal_id[0]
            res['journal_id'] = journal_id
        else:
            if not res.get('journal_id',False):
                res['journal_id'] = self._get_journal(cr, uid, context)

        return res
        

SollerWizardPayInvoice()
