# -*- coding: utf-8 -*-
{
    "name": "Extensió Invoice Soller",
    "description": """
Extensió de les funcionalitats de factura
    * Wizard per pagar grup de factures
    * Ampliació del wizard de pagar factures per selecció automàtica del diari de pago
    * Reports de factura i rebut genèrics de distribució
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_extend_invoice_report.xml",
        "wizard/wizard_pay_invoice_group_view.xml"
    ],
    "active": False,
    "installable": True
}
