# -*- coding: utf-8 -*-
{
    "name": "Soller invoicing validation",
    "description": """
    Soller invoicing validation
  """,
    "version": "0-dev",
    "author": "Toni Llado",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "giscedata_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_validation_data.xml"
    ],
    "active": False,
    "installable": True
}
