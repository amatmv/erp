# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime

class SollerFacturacioValidationValidator(osv.osv):
    _inherit = 'giscedata.facturacio.validation.validator'

    def check_mandate(self, cursor, uid, invoice, parameters):
        '''
            Check if there are mandate associated to invoice
        '''
        if (invoice.polissa_id.tipo_pago.code == 'RECIBO_CSB' and
                invoice.polissa_id.bank.iban and not invoice.mandate_id):
            return {}
        return None

    def check_reads_for_periods(self, cursor, uid, invoice, parameters):
        '''
            Check if there are read for all periods and meters
        '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        policy = invoice.polissa_id
        meter_ids = policy.comptadors_actius(invoice.data_inici,
                                             invoice.data_final)

        periods = self.get_invoice_periods(cursor, uid, invoice, 'te')

        meter_list = []
        period_list = []
        for meter_id in meter_ids:
            for period in periods:
                found = False
                for read in invoice.lectures_energia_ids:
                    if period in read.name and read.comptador_id.id == meter_id:
                        found = True

                if not found:
                    read_fields = ['name']
                    meter_vals = meter_obj.read(cursor, uid, meter_id,
                                                read_fields)
                    meter_list.append(meter_vals['name'])
                    period_list.append(period)

        if meter_list and period_list:
            meter_list = list(set(meter_list))
            period_list = list(set(period_list))
            return {
                'meter': ','.join(meter_list),
                'period': ','.join(period_list)
            }

        return None

    def get_invoice_periods(self, cursor, uid, invoice, type):
        rate_obj = self.pool.get('giscedata.polissa.tarifa')

        rate_id = invoice.polissa_id.tarifa.id
        periods = rate_obj.get_periodes(cursor, uid, rate_id, type)

        # If there are grouped periods, get new periods
        if invoice.llista_preu and invoice.llista_preu.agrupacio:
            new_periods = []
            for period in periods:
                period_name = '{} ({})'.format(invoice.polissa_id.tarifa.name,
                                               period)
                new_period = invoice.llista_preu.get_periode_group(period_name)
                if new_period:
                    new_periods.append(new_period[0])
                else:
                    new_periods.append(period)

            periods = list(set(new_periods))

        return periods

    def check_actual_previous_read(self, cursor, uid, invoice, parameters):
        '''
            Check if there are a previous read higher than his actual read
        '''
        meter_list = []
        type_list = []
        period_list = []
        for read in invoice.lectures_energia_ids:
            if read.lect_actual < read.lect_anterior:
                meter_list.append(read.comptador_id.name)
                type_list.append(read.tipus)
                period_list.append(read.name)

        if meter_list and type_list and period_list:
            meter_list = list(set(meter_list))
            type_list = list(set(type_list))
            period_list = list(set(period_list))
            return {
                'meter': ','.join(meter_list),
                'type': ','.join(type_list),
                'period': ','.join(period_list),
            }

    def check_read_inactive_meter(self, cursor, uid, invoice, parameters):
        '''
            Check if there are an inactive meter without end read for inactive date
        '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        reads_obj = self.pool.get('giscedata.facturacio.lectures.energia')

        policy = invoice.polissa_id
        meter_ids = policy.comptadors_actius(invoice.data_inici,
                                             invoice.data_final)
        for meter in meter_obj.browse(cursor, uid, meter_ids):
            if (not meter.active and
                invoice.data_inici <= meter.data_baixa <= invoice.data_final):

                search_params = [('comptador_id', '=', meter.id),
                                 ('data_actual', '=', meter.data_baixa)]

                read_ids = reads_obj.search(cursor, uid, search_params)

                if not read_ids:
                    date = datetime.strftime(datetime.strptime(
                        meter.data_baixa, '%Y-%m-%d'), '%d/%m/%Y')
                    return {
                        'meter': meter.name,
                        'date': date
                    }

    def check_maximeter_all_periods(self, cursor, uid, invoice, parameters):
        '''
            Check if there are maximeter for all periods and meters
        '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        policy = invoice.polissa_id
        data_inici = invoice.data_inici
        data_final = invoice.data_final
        meter_ids = policy.comptadors_actius(data_inici, data_final)

        periods = self.get_invoice_periods(cursor, uid, invoice, 'tp')

        meter_list = []
        period_list = []
        for meter_id in meter_ids:
            for period in periods:
                found = False
                for read in invoice.lectures_potencia_ids:
                    if (period in read.name and
                            read.comptador_id.id == meter_id):
                        found = True

                if not found:
                    read_fields = ['name']
                    meter_vals = meter_obj.read(cursor, uid, meter_id,
                                                read_fields)
                    meter_list.append(meter_vals['name'])
                    period_list.append(period)

        if meter_list and period_list:
            meter_list = list(set(meter_list))
            period_list = list(set(period_list))
            return {
                'meter': ','.join(meter_list),
                'period': ','.join(period_list)
            }

    def check_limit_maximeter(self, cursor, uid, invoice, parameters):
        '''
            Check if maximeters not over a limit
        '''
        max_limit = parameters['max_limit']

        for potence_read in invoice.lectures_potencia_ids:
            if (potence_read.pot_contract * max_limit <
                    potence_read.pot_maximetre):
                return {
                    'meter': potence_read.comptador_id.name,
                    'period': potence_read.name,
                    'max_limit': max_limit,
                    'potence': potence_read.pot_contract,
                    'maximeter': potence_read.pot_maximetre
                }

        return None

    def check_limit_reactive(self, cursor, uid, invoice, parameters):
        max_limit = parameters['max_limit']

        for read in invoice.lectures_energia_ids:
            if 'reactiva' == read.tipus:
                for energy_read in invoice.lectures_energia_ids:
                    if (('activa' == energy_read) and
                            (read.name == energy_read.name) and
                            (read.comptador_id.id ==
                                 energy_read.comptador_id.id) and
                            (read.consum > energy_read.consum * max_limit)):

                        return {
                            'meter': read.comptador_id.name,
                            'period': read.name,
                            'max_limit': max_limit,
                            'reactive': read.consum,
                            'active': energy_read.consum
                        }

        return None

    def check_active_meter(self, cursor, uid, invoice, parameters):
        '''
            Check if there are a active meter between start and end date
        '''
        policy = invoice.polissa_id
        c_actius = policy.comptadors_actius(invoice.data_inici,
                                            invoice.data_final)
        if not c_actius:
            return {
                'start_date': invoice.data_inici,
                'end_date': invoice.data_final
            }

        return None

    def check_multiple_meter(self, cursor, uid, invoice, parameters):
        '''
            Check if there are more than one meter active at same time
        '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        policy = invoice.polissa_id
        c_actiu_ids = policy.comptadors_actius(invoice.data_inici,
                                               invoice.data_final)

        read_fields = ['data_alta', 'data_baixa']
        c_actiu_vals = meter_obj.read(cursor, uid, c_actiu_ids, read_fields)

        for c_val in c_actiu_vals:
            for meter_val in c_actiu_vals:
                if (c_val['id'] != meter_val['id'] and
                    c_val['data_alta'] < meter_val['data_alta'] and
                    (not c_val['data_baixa'] or
                        c_val['data_baixa'] > meter_val['data_alta'])):
                    return {
                        'start_date': invoice.data_inici,
                        'end_date': invoice.data_final
                    }
        return None

SollerFacturacioValidationValidator()