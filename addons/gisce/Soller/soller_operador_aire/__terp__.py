# -*- coding: utf-8 -*-
{
    "name": "Soller Operador Aire Networks",
    "description": """
    Module for interaction with api of Aire Networks
      """,
    "version": "0-dev",
    "author": "Toni Lladó",
    "category": "Soller",
    "depends":[
        "soller_operador",
        "partner_birthday"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_operador_aire_view.xml",
        "soller_operador_aire_data.xml",
        "product_view.xml",
        "schedule.xml",
        "wizard/wizard_download_mobile_cdr.xml",
        "wizard/wizard_new_number_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
