# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
from datetime import datetime, timedelta
from tools.translate import _
from tools import config
import netsvc
from defs import *
from oorq.decorators import job
from babel.dates import format_date, format_datetime

import base64
import requests
from PyPDF2 import PdfFileMerger, PdfFileReader, PdfFileWriter
import StringIO
import pooler
import traceback


def unaccent(text):
    src_chars = """àáäâÀÁÄÂèéëêÈÉËÊìíïîÌÍÏÎòóöôÒÓÖÔùúüûÙÚÜÛñÑçÇ"""
    src_chars = unicode(src_chars, 'utf-8')
    dst_chars = """aaaaAAAAeeeeEEEEiiiiIIIIooooOOOOuuuuUUUUnNcC"""
    dst_chars = unicode(dst_chars, 'utf-8')

    if isinstance(text, str):
        text = unicode(text, 'utf-8')
    elif not isinstance(text, unicode):
        return text

    output = text
    for c in xrange(len(src_chars)):
        output = output.replace(src_chars[c], dst_chars[c])
    return output.strip('_').encode('utf-8')

def get_vat_number(vat):
    if not vat:
        msg = _('Dni is not informed in the partner')
        raise osv.except_osv('error', msg)
    if vat.startswith('ES'):
        return vat[2:]
    else:
        return vat


class SollerOperadorAire(osv.osv):
    _name = 'soller.operador.aire'

    _auto = False

    def make_portability(self, cursor, uid, linia_id, context=None):
        '''
            Make the portability to aire networks
        '''
        linia_obj = self.pool.get('soller.operador.contrato.linea')

        if isinstance(linia_id, (list, tuple)):
            linia_id = linia_id[0]

        linia = linia_obj.browse(cursor, uid, linia_id, context=context)

        # If the case not referenced to a phone product return false
        if linia.product_id.categ_id.code not in 'MOV':
            raise osv.except_osv('Error',
                                 _('Only automatic portabilities availability '
                                   'for mobile phones'))

        if linia.client_donant_id:
            titular = linia.client_donant_id
        else:
            titular = linia.contrato_id.titular_id

        subscriber_type = self.get_subscribertype(cursor, uid, titular.vat)

        vat_number = get_vat_number(titular.vat)

        # Create client if not exist
        self.client_treatment(cursor, uid, vat_number, linia_id, titular.id,
                              context=context)

        # Make portability
        rate_codes = eval(linia.product_id.aire_product)
        self.validate_portability(cursor, uid, linia, rate_codes)
        vals = {
            'tarifa': rate_codes['tarifa'],
            'subscriberType': subscriber_type,
            'nif': vat_number,
            'icc': linia.sim[:len(linia.sim)-1],
            'digito_control': linia.sim[len(linia.sim)-1:],
            'telefono': linia.detall,
            'modalidad_actual': linia.modalidad_donant,
            'responseNsolicitud': 'S'
        }

        if linia.sim_donant:
            vals.update({
                'icc_origen': linia.sim_donant[:len(linia.sim_donant) - 1],
                'dc_origen': linia.sim_donant[len(linia.sim_donant) - 1:],
            })

        if 'datos' in rate_codes:
            vals.update({'bonos_d': [rate_codes['datos']]})

        if 'voz' in rate_codes:
            vals.update({'bonos_v': [rate_codes['voz']]})

        # Remove accents
        unaccent_vals = {}
        for key, value in vals.iteritems():
            unaccent_vals[key] = unaccent(value)

        port_response = requests.post(
            '{}/api/portabilidades/'.format(config.get('api_aire_url')),
            json=unaccent_vals,
            auth=(config.get('api_aire_user'), config.get('api_aire_pass'))
        ).json()

        if self.has_error(port_response):
            raise osv.except_osv('Error', port_response['error_message'])

        # Attach portability document
        self.attach_document(cursor, uid, port_response, linia.contrato_id.id,
                             linia.detall, PORTABILITY_TYPE, ATTACH_CODE_PORTA)

        # Attach contract document
        self.attach_document(cursor, uid, port_response, linia.contrato_id.id,
                             linia.detall, CONTRACT_TYPE, ATTACH_CODE_CONTRACT)

        # Write description in case
        descrip = _('Portability registered in aire')
        self.write_case(cursor, uid, linia.contrato_id.id, linia.id, descrip)

        return _('Portability ends correctly')

    def client_treatment(self, cursor, uid, vat_number, linia_id, titular_id,
                         context=None):
        '''
            Check client and create client if not exist
        '''
        # Check if client exist
        response = requests.get(
            '{}/api/clientes/'.format(config.get('api_aire_url')),
            params=dict(fiscalId=vat_number),
            auth=(config.get('api_aire_user'), config.get('api_aire_pass'))
        ).json()

        if len(response) > 2:
            msg = _('There are more than one partner associated to dni {}'
                    ).format(vat_number)
            raise osv.except_osv('Error', msg)
        elif len(response) == 0:
            # create client in supplier
            self.create_client(cursor, uid, linia_id, titular_id,
                               context=context)
        return True

    def create_client(self, cursor, uid, linia_id, titular_id, context=None):
        '''
            Create client in aire
        '''
        partner_obj = self.pool.get('res.partner')
        linia_obj = self.pool.get('soller.operador.contrato.linea')
        conf_obj = self.pool.get('res.config')

        linia = linia_obj.browse(cursor, uid, linia_id, context=context)
        titular = partner_obj.browse(cursor, uid, titular_id)
        invoice_address = linia.contrato_id.direccion_factura_id
        document_type = self.get_document_type(cursor, uid, titular.vat,
                                               context)
        # Get dni attachment from customer
        doc_data = self.get_customer_attachment(cursor, uid, titular.id,
                                                context)
        subscriber_type = self.get_subscribertype(cursor, uid, titular.vat)

        vat_number = get_vat_number(titular.vat)

        if not doc_data:
            msg = _('Can\'t find dni attach in partner {}'.format(
                vat_number))
            raise osv.except_osv('error', msg)

        if titular.birthday:
            birthday = datetime.strptime(titular.birthday, "%Y-%m-%d")
            birthday = datetime.strftime(birthday.date(), '%d/%m/%Y')
        else:
            msg = _('It\'s mandatory the birthday for partner {}').format(
                vat_number)
            raise osv.except_osv('error', msg)

        self.validate_address(invoice_address, vat_number)

        vals = {
            'subscriberType': subscriber_type,
            'marketingConsent': conf_obj.get(cursor, uid, 'api_aire_marketing'),
            'documentType': document_type,
            'fiscalId': vat_number,
            'birthday': birthday,
            'contactPhone': linia.detall,
            'addressProvince': invoice_address.id_municipi.state.name,
            'addressCity': invoice_address.city,
            'addressPostalCode': invoice_address.zip,
            'addressStreet': invoice_address.nv,
            'addressNumber': invoice_address.pnp,
        }

        if partner_obj.vat_es_empresa(cursor, uid, titular.vat):
            if not linia.apoderat_id:
                msg = _('It\'s mandatory the proxy for companies')
                raise osv.except_osv('Error', msg)

            # Check if proxy has dni attached
            contact_dni_vals = self.get_customer_attachment(
                cursor, uid, linia.apoderat_id.id, context)

            if contact_dni_vals:
                docs_list = [doc_data, contact_dni_vals]
                doc_data = self.join_documents(cursor, uid, docs_list)

            contact_name = linia.apoderat_id.name
            contact_vat_number = get_vat_number(linia.apoderat_id.vat)
            contact_doc_type = self.get_document_type(cursor, uid,
                                                      linia.apoderat_id.vat,
                                                      context)
            vals.update({
                'name': titular.name,
                'contactFiscalId': contact_vat_number,
                'contactDocumentType': contact_doc_type
            })
        else:
            contact_name = titular.name

        contact_name = self.split_name(cursor, uid, contact_name)
        vals.update({
            'contactName': contact_name[0],
            'contactFamilyName1': contact_name[1],
            'documento_name': 'dni.pdf',
            'documento': base64.b64encode(base64.b64decode(doc_data)),
        })
        # Remove accents
        unaccent_vals = {}
        for key, value in vals.iteritems():
            unaccent_vals[key] = unaccent(value)

        response = requests.post(
            '{}/api/clientes/'.format(config.get('api_aire_url')),
            json=unaccent_vals,
            auth=(config.get('api_aire_user'), config.get('api_aire_pass'))
        ).json()

        if self.has_error(response):
            raise osv.except_osv('Error', response['error_message'])

    def write_case(self, cursor, uid, contract_id, line_id, description):
        '''
            Write description in product case
        '''
        crm_obj = self.pool.get('crm.case')
        linea_obj = self.pool.get('soller.operador.contrato.linea')

        search_params = [
            ('ref', '=', 'soller.operador.contrato,{}'.format(contract_id)),
            ('ref2', '=', 'soller.operador.contrato.linea,{}'.format(line_id))
        ]
        crm_ids = crm_obj.search(cursor, uid, search_params)

        if len(crm_ids) > 1:
            msg = _('There are two or more cases associated to product')
            raise osv.except_osv('Error', msg)

        if not crm_ids:
            # create_case
            crm_ids = linea_obj.b_create_phone_case(cursor, uid, line_id)

        vals = {
            'description': description
        }
        crm_obj.write(cursor, uid, crm_ids, vals)
        crm_obj.case_log(cursor, uid, crm_ids)

        return True

    def split_name(self, cursor, uid, name):
        '''
            Split name and surname separated for comma
        '''
        if ',' in name:
            split_name = name.split(',')
            return [split_name[1], split_name[0]]
        else:
            msg = _('It\'s mandatory use character , for separate name and '
                    'surname: {}').format(name)
            raise osv.except_osv('Error', msg)

    def join_documents(self, cursor, uid, docs_list):

        merger = PdfFileMerger(strict=False)
        merge_file = StringIO.StringIO()
        for doc in docs_list:
            doc = base64.b64decode(doc)
            temp_file = StringIO.StringIO(doc)
            merger.append(temp_file)
        # Write all documents where control_pages match
        merger.write(merge_file)
        merge_file.seek(0)
        result = merge_file.read()

        # Close mergers
        merger.close()
        merge_file.close()

        return base64.b64encode(result)

    def validate_portability(self, cursor, uid, linia, rate_codes):
        conf_obj = self.pool.get('res.config')
        # phone number
        if (not linia.detall
            or not (linia.detall.startswith('6')
                    or linia.detall.startswith('7'))):
                msg = _('The phone number {} is not correct'
                        ).format(linia.detall)
                raise osv.except_osv('Error', msg)

        # operator sim
        if len(linia.sim) != int(conf_obj.get(cursor, uid, 'api_aire_sim_size',
                                              20)):
            msg = _('The sim number {} is not correct').format(linia.sim)
            raise osv.except_osv('Error', msg)

        # rate code
        if 'tarifa' not in rate_codes:
            msg = _('The rate code is not defined in aire product field of '
                    'product')
            raise osv.except_osv('Error', msg)

    def validate_address(self, address, vat_number):
        if not address.id_municipi:
            msg = _('The municipality is not defined for partner {}'
                    ).format(vat_number)
            raise osv.except_osv('Error', msg)

        if not address.city:
            msg = _('The city is not defined for partner {}').format(vat_number)
            raise osv.except_osv('Error', msg)

        if not address.zip:
            msg = _('The postal code is not defined for partner {}'
                    ).format(vat_number)
            raise osv.except_osv('Error', msg)

        if not address.nv:
            msg = _('The street is not defined for partner {}'
                    ).format(vat_number)
            raise osv.except_osv('Error', msg)

        if not address.pnp:
            msg = _('The street number is not defined for partner {}'
                    ).format(vat_number)
            raise osv.except_osv('Error', msg)

        return True

    def attach_document(self, cursor, uid, request_code, res_id, phone_number,
                        document_type, attach_code):
        '''
            Attach document in contract
        '''
        attach_obj = self.pool.get('ir.attachment')
        categ_obj = self.pool.get('ir.attachment.category')
        vals = {
            'codigo_linea': request_code,  # request code
            'tipo_documento': document_type,
        }

        response = requests.get(
            '{}/api/documentos/'.format(config.get('api_aire_url')),
            params=vals,
            auth=(config.get('api_aire_user'), config.get('api_aire_pass'))
        ).json()

        if self.has_error(response):
            raise osv.except_osv('Error', response['error_message'])

        search_params = [('code', '=', attach_code)]
        category_ids = categ_obj.search(cursor, uid, search_params)
        vals = {
            'name': '{} - {}'.format(document_type, phone_number),
            'datas_fname': '{} - {}.pdf'.format(document_type, phone_number),
            'res_id': res_id,
            'res_model': 'soller.operador.contrato',
            'category_id': category_ids[0],
            'datas': base64.b64encode(base64.b64decode(response)),
        }
        attach_obj.create(cursor, uid, vals)

    def get_customer_attachment(self, cursor, uid, titular_id, context=None):
        '''
            Get dni document attached in partner
        '''
        attach_obj = self.pool.get('ir.attachment')
        category_obj = self.pool.get('ir.attachment.category')

        search_params = [('code', 'in', (ATTACH_CODE_DNI,
                                         ATTACH_CODE_ESCRIPTURA,
                                         ATTACH_CODE_PODERES))]
        category_ids = category_obj.search(cursor, uid, search_params)

        search_params = [('res_model', '=', 'res.partner'),
                         ('res_id', '=', titular_id),
                         ('category_id', 'in', category_ids)]
        attach_ids = attach_obj.search(cursor, uid, search_params)
        if attach_ids:
            docs_vals = attach_obj.read(cursor, uid, attach_ids, ['datas'])
            docs_list = [x['datas'] for x in docs_vals]
            return self.join_documents(cursor, uid, docs_list)

        return False

    def get_document_type(self, cursor, uid, vat_number, context=None):
        '''
            return document type: 0-DNI, 1-NIE, 4-CIF
        '''
        partner_obj = self.pool.get('res.partner')
        if partner_obj.vat_es_empresa(cursor, uid, vat_number):
            return 4
        elif partner_obj.vat_es_nie(cursor, uid, vat_number):
            return 1
        else:
            return 0

    def upload_documents(self, cursor, uid, linia_id, context=None):
        linia_obj = self.pool.get('soller.operador.contrato.linea')

        if isinstance(linia_id, (list, tuple)):
            linia_id = linia_id[0]

        linia = linia_obj.browse(cursor, uid, linia_id, context=context)
        if linia.client_donant_id:
            titular = linia.client_donant_id
        else:
            titular = linia.contrato_id.titular_id

        # Get number of request
        requests_response = self.get_aire_requests(cursor, uid, linia.detall)

        msg = ''
        for request in requests_response:
            if request['tipo'] == 'CAMBIO DE TITULAR':
                # Upload owner change document
                contract_attach = self.get_attachment(cursor,
                                                      uid,
                                                      linia.contrato_id.id,
                                                      ATTACH_CODE_CHANGE_OWNER_S,
                                                      linia.detall, context)

                if not contract_attach:
                    contract_attach = self.get_attachment_from_mail(
                        cursor, uid, linia.contrato_id.id,
                        linia.contrato_id.data_firma_contracte, context)
                    if not contract_attach:
                        msg = _('Attachment not found in cetified email\n')
                        raise osv.except_osv('Error', msg)

                msg += self.upload_document(cursor, uid, titular.vat,
                                            request['cod'],
                                            contract_attach,
                                            CONTRACT_TYPE, context)

                # Write description in case
                descrip = _('Upload documents in aire: {}').format(msg)
                self.write_case(cursor, uid, linia.contrato_id.id, linia.id,
                                descrip)
            else:
                # Upload portability and contract document
                porta_attach = self.get_attachment(cursor, uid,
                                                   linia.contrato_id.id,
                                                   ATTACH_CODE_PORTA_S,
                                                   linia.detall, context)

                contract_attach = self.get_attachment(cursor, uid,
                                                      linia.contrato_id.id,
                                                      ATTACH_CODE_CONTRACT_S,
                                                      linia.detall, context)

                if not porta_attach and not contract_attach:
                    contract_attach = self.get_attachment_from_mail(
                        cursor, uid, linia.contrato_id.id,
                        linia.contrato_id.data_firma_contracte, context)
                    if not contract_attach:
                        msg = _('Attachment not found in cetified email\n')
                        raise osv.except_osv('Error', msg)

                if not porta_attach:
                    porta_attach = contract_attach
                elif not contract_attach:
                    contract_attach = porta_attach

                if request['tipo'] == 'PORTABILIDAD':
                    msg += self.upload_document(cursor, uid, titular.vat,
                                                request['cod'],
                                                porta_attach,
                                                PORTABILITY_TYPE, context)

                msg += self.upload_document(cursor, uid, titular.vat,
                                            request['cod'],
                                            contract_attach,
                                            CONTRACT_TYPE, context)

                # Write description in case
                descrip = _('Upload documents in aire: {}').format(msg)
                self.write_case(cursor, uid, linia.contrato_id.id, linia.id,
                                descrip)

        return msg

    def get_aire_requests(self, cursor, uid, phone_number):
        '''
            Get active requests in aire from phone number
        '''
        vals = {
            'telefono': phone_number,
            'ordenacion': True
        }

        sol_response = requests.get(
            '{}/api/solicitudes/'.format(config.get('api_aire_url')),
            params=vals,
            auth=(config.get('api_aire_user'), config.get('api_aire_pass'))
        ).json()

        if self.has_error(sol_response):
            raise osv.except_osv('Error', sol_response['error_message'])

        # Api method return values in elementos key
        if 'elementos' in sol_response:
            sol_response = sol_response['elementos']

        # Remove inactive requests
        sol_response = [x for x in sol_response if x['estado_n'] < 2]

        if not sol_response:
            msg = _('Active request not found')
            raise osv.except_osv('Error', msg)

        return sol_response

    def change_owner(self, cursor, uid, linia_id, context=None):
        linia_obj = self.pool.get('soller.operador.contrato.linea')

        if isinstance(linia_id, (list, tuple)):
            linia_id = linia_id[0]

        linia = linia_obj.browse(cursor, uid, linia_id, context=context)

        if linia.client_donant_id:
            titular = linia.client_donant_id
        else:
            titular = linia.contrato_id.titular_id

        subscriber_type = self.get_subscribertype(cursor, uid, titular.vat)

        vat_number = get_vat_number(titular.vat)

        # Check if client exist
        self.client_treatment(cursor, uid, vat_number, linia_id, titular.id,
                              context=context)

        # Make change owner request
        rate_codes = eval(linia.product_id.aire_product)
        vals = {
            'tarifa': rate_codes['tarifa'],
            'subscriberType': subscriber_type,
            'nif': vat_number,
            'telefono': linia.detall,
            'responseNsolicitud': 'S'
        }

        if 'datos' in rate_codes:
            vals.update({'bonos_d': [rate_codes['datos']]})

        if 'voz' in rate_codes:
            vals.update({'bonos_v': [rate_codes['voz']]})

        response = requests.post(
            '{}/api/cambio/titular/'.format(config.get('api_aire_url')),
            data=vals,
            auth=(config.get('api_aire_user'), config.get('api_aire_pass'))
        ).json()

        if self.has_error(response):
            raise osv.except_osv('Error', response['error_message'])

        # Attach portability document
        self.attach_document(cursor, uid, response, linia.contrato_id.id,
                             linia.detall, CONTRACT_TYPE,
                             ATTACH_CODE_CHANGE_OWNER)
        return True

    def download_documents(self, cursor, uid, linia_id, context=None):
        linia_obj = self.pool.get('soller.operador.contrato.linea')

        if isinstance(linia_id, (list, tuple)):
            linia_id = linia_id[0]

        linia = linia_obj.browse(cursor, uid, linia_id, context=context)

        requests_response = self.get_aire_requests(cursor, uid, linia.detall)

        for request_resp in requests_response:
            if request_resp['tipo'] == 'CAMBIO DE TITULAR':
                # Attach portability document
                self.attach_document(cursor, uid, request_resp['cod'],
                                     linia.contrato_id.id, linia.detall,
                                     CONTRACT_TYPE, ATTACH_CODE_CHANGE_OWNER)
            else:
                if request_resp['tipo'] == 'PORTABILIDAD':
                    # Attach portability document
                    self.attach_document(cursor, uid, request_resp['cod'],
                                         linia.contrato_id.id, linia.detall,
                                         PORTABILITY_TYPE, ATTACH_CODE_PORTA)

                # Attach contract document
                self.attach_document(cursor, uid, request_resp['cod'],
                                     linia.contrato_id.id, linia.detall,
                                     CONTRACT_TYPE, ATTACH_CODE_CONTRACT)

    def get_new_phone_numbers(self, cursor, uid, context=None):
        '''
            Return new phone numbers from aire.
        '''
        NUM_NUMBERS = 6
        response = requests.get(
            '{}/api/msisdn/'.format(config.get('api_aire_url')),
            auth=(config.get('api_aire_user'), config.get('api_aire_pass'))
        ).json()

        if self.has_error(response):
            raise osv.except_osv('Error', response['error_message'])

        number_list = []
        for i in range(1, NUM_NUMBERS+1):
            number_list.append('{}-{}'.format(response['n{}'.format(i)],
                                              response['codigo_reserva']))
        # New number + code for new phone request
        return number_list

    def new_phone_request(self, cursor, uid, linia_id, lock_code, context=None):

        linia_obj = self.pool.get('soller.operador.contrato.linea')

        if isinstance(linia_id, (list, tuple)):
            linia_id = linia_id[0]

        linia = linia_obj.browse(cursor, uid, linia_id, context=context)

        titular = linia.contrato_id.titular_id
        subscriber_type = self.get_subscribertype(cursor, uid, titular.vat)
        vat_number = get_vat_number(titular.vat)

        # Create client if not exist
        self.client_treatment(cursor, uid, vat_number, linia_id, titular.id,
                              context=context)

        # Make new phone request
        rate_codes = eval(linia.product_id.aire_product)
        self.validate_portability(cursor, uid, linia, rate_codes)
        vals = {
            'tarifa': rate_codes['tarifa'],
            'subscriberType': subscriber_type,
            'nif': vat_number,
            'icc': linia.sim[:len(linia.sim) - 1],
            'digito_control': linia.sim[len(linia.sim) - 1:],
            'telefono': linia.detall,
            'codigo_reserva': int(lock_code),
            'responseNsolicitud': 'S'
        }

        if 'datos' in rate_codes:
            vals.update({'bonos_d': [rate_codes['datos']]})

        if 'voz' in rate_codes:
            vals.update({'bonos_v': [rate_codes['voz']]})

        # Remove accents
        unaccent_vals = {}
        for key, value in vals.iteritems():
            unaccent_vals[key] = unaccent(value)

        port_response = requests.post(
            '{}/api/lineas/'.format(config.get('api_aire_url')),
            json=unaccent_vals,
            auth=(config.get('api_aire_user'), config.get('api_aire_pass'))
        ).json()

        if self.has_error(port_response):
            raise osv.except_osv('Error', port_response['error_message'])

        # Attach contract document
        self.attach_document(cursor, uid, port_response, linia.contrato_id.id,
                             linia.detall, CONTRACT_TYPE, ATTACH_CODE_CONTRACT)

        # Write description in case
        descrip = _('New phone registered in aire')
        self.write_case(cursor, uid, linia.contrato_id.id, linia.id, descrip)

        return True

    def get_attachment_from_mail(self, cursor, uid, contract_id, start_date,
                                 context=None):
        '''
            Get certified attachment
        '''
        powermail_obj = self.pool.get('poweremail.mailbox')
        history_obj = self.pool.get('mc.history')
        attach_obj = self.pool.get('ir.attachment')

        reference = 'soller.operador.contrato,{}'.format(contract_id)
        search_params = [('reference', '=', reference),
                         ('mc_type', '!=', 'nocert')]
        mails_ids = powermail_obj.search(cursor, uid, search_params,
                                         order='create_date desc')
        if not mails_ids:
            return False

        for mail in powermail_obj.browse(cursor, uid, mails_ids):
            search_params = [('name', '=', mail.mc_message_id),
                             ('status', 'in', ('Aceptado', 'Firmado')),
                             ('date', '>=', start_date)]
            hist_ids = history_obj.search(cursor, uid, search_params)
            if not hist_ids:
                continue

            # Get certified signed contract
            if not powermail_obj.mc_get_certificate(cursor, uid, [mail.id]):
                msg = _('Error getting certificate {}\n'
                        ).format(mail.mc_message_id)
                raise osv.except_osv('Error', msg)

            search_params = [('datas_fname', 'ilike', mail.mc_message_id),
                             ('res_model', '=', 'poweremail.mailbox'),
                             ('res_id', '=', mail.id)]
            attach_ids = attach_obj.search(cursor, uid, search_params)
            if attach_ids:
                read_fields = ['name', 'datas']
                return attach_obj.read(cursor, uid, attach_ids[0], read_fields)
            break

        return False

    def upload_document(self, cursor, uid, vat_number, request_code,
                        attachment, document_type, context=None):
        '''
            Upload document in aire system
        '''

        subscriber_type = self.get_subscribertype(cursor, uid, vat_number)

        if vat_number.startswith('ES'):
            vat_number_aire = vat_number[2:]
        else:
            vat_number_aire = vat_number

        if attachment:
            vals = {
                'nif_cliente': vat_number_aire,
                'subscriberType': subscriber_type,
                'codigo_solicitud': request_code,
                'documento_name': '{}.pdf'.format(document_type),
                'documento': attachment['datas'],
                'tipo_documento': document_type
            }

            response = requests.post(
                '{}/api/documentos/'.format(config.get('api_aire_url')),
                json=vals,
                auth=(config.get('api_aire_user'), config.get('api_aire_pass'))
            ).json()

            if self.has_error(response):
                msg = _('Error during upload file {} {}\n').format(
                    document_type, response['error_message'])
                raise osv.except_osv('Error', msg)
        else:
            msg = _('Attachment not found for file {}\n').format(document_type)
            raise osv.except_osv('Error', msg)

        return _('File {} upload correctly\n').format(document_type)

    def get_attachment(self, cursor, uid, res_id, code, phone, context=None):
        '''
            Get document attached
        '''
        attach_obj = self.pool.get('ir.attachment')
        category_obj = self.pool.get('ir.attachment.category')

        search_params = [('code', '=', code)]
        category_ids = category_obj.search(cursor, uid, search_params)

        search_params = [('name', 'like', phone),
                         ('res_model', '=', 'soller.operador.contrato'),
                         ('res_id', '=', res_id),
                         ('category_id', '=', category_ids[0])]
        attach_ids = attach_obj.search(cursor, uid, search_params)

        if len(attach_ids) > 1:
            msg = _('There are more than one attachment for phone {} and '
                    'category {}\n').format(phone, code)
            raise osv.except_osv('Error', msg)

        if attach_ids:
            read_fields = ['name', 'datas']
            return attach_obj.read(cursor, uid, attach_ids, read_fields)[0]

        return False

    def get_subscribertype(self, cursor, uid, vat_number):
        '''
            Get subscriber type: 0-residencial, 1-empresa, 2- extranjero
        '''
        partner_obj = self.pool.get('res.partner')
        if partner_obj.vat_es_empresa(cursor, uid, vat_number):
            return 1
        elif partner_obj.vat_es_nie(cursor, uid, vat_number):
            return 2
        else:
            return 0

    def has_error(self, response):
        '''
            return if the message response has errors
        '''
        return 'error_message' in response and response['error_message']

    def get_partner(self, cursor, uid, context=None):
        '''get partner with code airenetworks'''

        partner_obj = self.pool.get('res.partner')
        search_params = [('ref', '=', 'airenetworks')]
        partner_id = partner_obj.search(cursor, uid, search_params)[0]
        return partner_obj.browse(cursor, uid, partner_id)

    @job(queue='operador_facturacio', timeout=3600)
    def download_mobile_cdr(self, cursor, uid, date=None, context=None):
        '''downloads cdr for the selected date.
        * date: date with or without date with format YYYY-MM-DD'''

        data_obj = self.pool.get('soller.operador.cdr.data')
        cdr_obj = self.pool.get('soller.operador.cdr')
        mail_obj = self.pool.get('poweremail.mailbox')

        # Default download day is the day before of execution
        if date is None:
            date_dt = datetime.now() - timedelta(days=1)
            date = date_dt.strftime('%Y-%m-%d')

        url = '{}/api/cdr/'.format(config.get('api_aire_url'))
        auth = (config.get('api_aire_user'), config.get('api_aire_pass'))
        year, month, day = date.split('-')
        partner = self.get_partner(cursor, uid, context)
        logger = netsvc.Logger()
        for type in ('SMS', 'MOVIL', 'MOVIL_ENTRANTE', 'DATOS'):
            msg = u"Begin import {} ({}) file from {}".format(
                    type, date, partner.name)
            logger.notifyChannel("objects", netsvc.LOG_INFO, msg)
            elementos = []
            page_size = 100

            msg = u"Fetching first page."
            logger.notifyChannel("objects", netsvc.LOG_INFO, msg)

            params = dict(dia=day, mes=month, anyo=year,
                          tipo_servicio=type, pagina=1, registros=100)
            response = requests.get(url, params=params, auth=auth).json()

            elementos.extend(response['elementos'])
            total_elements = response['registros_totales']
            total_pages = total_elements / page_size

            if total_elements % page_size:
                total_pages += 1

            msg = u"Got {} elements on {} pages".format(total_elements,
                                                        total_pages)
            logger.notifyChannel("objects", netsvc.LOG_INFO, msg)

            for page in xrange(2, total_pages + 1):
                msg = u"Processing page {:>3} of {:>3}".format(page,
                                                               total_pages)
                logger.notifyChannel("objects", netsvc.LOG_INFO, msg)
                params['pagina'] = page
                response = requests.get(url, params=params, auth=auth).json()
                elementos.extend(response['elementos'])

            msg = u"Fetched {} elements. Adding them to OpenERP".format(
                len(elementos))
            logger.notifyChannel("objects", netsvc.LOG_INFO, msg)

            # build a list of ; separated for each record
            # This way we can use the same function as importing the cdr file
            if type == 'DATOS':
                line_format = '{};{};{};{};{}'
                lines = []
                for record in elementos:
                    line = line_format.format(record['fecha'],
                                              record['origen'],
                                              record['segundos'],
                                              record['importe'],
                                              record['patron'])
                    lines.append(line)
                report = data_obj.import_data_consumption(cursor, uid, lines,
                                                          partner.id,
                                                          context=context)
            else:
                import_type = 'default'
                if type == 'MOVIL_ENTRANTE':
                    import_type = 'incoming'
                elif type == 'SMS':
                    import_type = 'sms'

                line_format = ';{};{};{};{};{};{}'
                lines = []
                for record in elementos:
                    line = line_format.format(record['fecha'],
                                              record['origen'],
                                              record['destino'],
                                              record['descripcion'],
                                              record['segundos'],
                                              record['importe'])
                    lines.append(line)
                report = cdr_obj.import_call(cursor, uid, import_type,
                                             lines, partner.id,
                                             context=context)

            subject = _('End import {} ({}) file from {}').format(
                        type, date, partner.name)
            
            logger.notifyChannel("objects", netsvc.LOG_INFO, subject)

            email_from = config['email_from']
            mail_obj.send_mail_generic(cursor, uid, email_from, subject,
                                       report, context=context)

        return True

    def get_activated_mobiles(self, cursor, uid, date, context=None):
        """
        It returns a list with all phones that were activated in a given
        date.
        """
        contract_obj = self.pool.get('soller.operador.contrato')

        url = '{}/api/lineas'.format(config.get('api_aire_url'))
        auth = (config.get('api_aire_user'), config.get('api_aire_pass'))
        page_size = 50
        params = {
            'fecha': date,
            'activo': 'SI',
            'registros': page_size,
            'pagina': 1
        }
        lines = []

        # First, get all the matching lines information from Aire
        response = requests.get(url, params=params, auth=auth).json()
        lines.extend(response['elementos'])

        if response['registros_totales'] > page_size:
            pages = response['registros_totales'] / page_size

            if response['registros_totales'] % page_size != 0:
                pages += 1

            for page in xrange(2, pages + 1):
                params.update({'pagina': page})
                response = requests.get(url, params=params, auth=auth).json()
                lines.extend(response['elementos'])

        phones = [line['telefono'] for line in lines]
        return phones

    def send_resume_mail(self, cursor, uid, activated_lines, errors,
                         context=None):

        mail_obj = self.pool.get('poweremail.mailbox')
        line_obj = self.pool.get('soller.operador.contrato.linea')
        user_obj = self.pool.get('res.users')
        locale = user_obj.browse(cursor, uid, uid).context_lang
        currentdate = format_datetime(datetime.now(), locale=locale,
                                      format='short')

        email_from = config['email_from']
        subject = _('{} Automatic mobile activation').format(currentdate)
        lines = line_obj.browse(cursor, uid, activated_lines)
        body = u''

        # If there are activated lines, write them one by one
        if activated_lines:
            body += _('The following lines has been activated:\n\n')

            for line in lines:
                line_date = datetime.strptime(line.data_alta, "%Y-%m-%d")
                body_date = format_date(line_date, format='short',
                                        locale=locale)
                body += _('  - {} is now active from {}\n').format(line.detall,
                                                                   body_date)
            body += '\n'

        # If there are errors, write them one by one
        if errors:
            body += _(u'The automatic activation failed with the following '
                      u'phones:\n')
            for error in errors:
                body += _('  - Phone {}\n').format(error['phone'])
                body += '  - {}\n'.format(error['message'])
                body += '\n'
            body += '\n'

        # Send the email
        mail_obj.send_mail_generic(cursor, uid, email_from, subject, body,
                                   context=context)

    def activate_mobile(self, cursor, uid, date=None, context=None):
        if date is None:
            date = datetime.now().strftime('%Y-%m-%d')

        if context is None:
            context = {}

        linea_obj = self.pool.get('soller.operador.contrato.linea')
        contract_obj = self.pool.get('soller.operador.contrato')
        crm_obj = self.pool.get('crm.case')
        user_obj = self.pool.get('res.users')
        locale = user_obj.browse(cursor, uid, uid).context_lang
        db = pooler.get_db_only(cursor.dbname)
        activated_lines = []
        errors = []

        # Get a list with all activated phones in the given date
        phones = self.get_activated_mobiles(cursor, uid, date, context=context)
        for phone in phones:
            ctx = context.copy()
            ctx.update({
                'return_line_id': True,
                'contract_states': ('cancelat')
            })
            try:
                tmp_cr = db.cursor()
                # Get associated line with the given phone
                line_id = contract_obj.get_contract_from_phone_date(tmp_cr,
                                                                    uid,
                                                                    phone,
                                                                    date,
                                                                    ctx)
                line = linea_obj.browse(tmp_cr, uid, line_id)

                # If line has already been activated, we skip it
                if line.facturar and line.data_alta == date:
                    continue

                # Mark line as billable, active and set the start date
                line_vals = {
                    'facturar': True,
                    'active': True,
                    'data_alta': date
                }

                linea_obj.write(tmp_cr, uid, [line_id], line_vals, context)

                # Get portability orders associated to this line
                crm_ids = linea_obj.get_portability_orders(tmp_cr,
                                                           uid,
                                                           line_id,
                                                           context)

                # Check that only one portability order exists for this
                # line, if not, mark it as an error.
                if len(crm_ids) != 1:
                    raise osv.except_osv('Error', _(u'Problems searching for '
                                                    u'portability orders.'))

                # Write a successful activation message in the order's history
                date_datetime = datetime.strptime(date, '%Y-%m-%d')
                crm_date = format_date(date_datetime, format='short',
                                       locale=locale)
                crm_vals = {
                    'date_deadline': date,
                    'description': _(u'Phone {} activated'
                                     u'on {}').format(phone, crm_date)
                }

                crm_obj.write(tmp_cr, uid, crm_ids, crm_vals, context=context)
                crm_obj.case_log(tmp_cr, uid, crm_ids)

                if line.contrato_id.direccion_factura_id.email:
                    crm_vals = {
                        'description': _(u'An activation email has been sent')
                    }
                    # Send an email to the client to confirm the activation
                    linea_obj.send_activation_mail(tmp_cr, uid, crm_ids[0],
                                                   context)
                else:
                    crm_vals = {
                        'description': _(u'Activation email not sent. The '
                                         u'customer has no associated email')
                    }
                crm_obj.write(tmp_cr, uid, crm_ids, crm_vals, context=context)
                crm_obj.case_log(tmp_cr, uid, crm_ids)

                # If contract state is draft, then validate and activate it
                if line.contrato_id.state == 'esborrany':
                    contract_id = line.contrato_id.id
                    contract_obj.b_soc_validar(tmp_cr, uid, contract_id,
                                               context=context)
                    contract_obj.b_soc_activar(tmp_cr, uid, contract_id,
                                               context=context)

                # If line is not actived, with activate it. Also, we add it
                # to activated_lines to keep track of all activated lines
                activated_lines.append(line_id)

                tmp_cr.commit()
            except Exception:
                # Store the exception and the associated phone to notify by
                # email
                errors.append({
                    'phone': phone,
                    'message': '\n\t'.join(traceback.format_exc().splitlines())
                })
                tmp_cr.rollback()
            finally:
                tmp_cr.close()

        # If something was processed, then send an brief by email
        if activated_lines or errors:
            self.send_resume_mail(cursor, uid, activated_lines, errors)


SollerOperadorAire()


class SollerOperadorAireLinia(osv.osv):

    _inherit = 'soller.operador.contrato.linea'

    def b_make_portability(self, cursor, uid, ids, context=None):
        aire_obj = self.pool.get('soller.operador.aire')
        return aire_obj.make_portability(cursor, uid, ids, context)

    def b_upload_documents(self, cursor, uid, ids, context=None):
        aire_obj = self.pool.get('soller.operador.aire')
        return aire_obj.upload_documents(cursor, uid, ids, context)

    def b_change_owner(self, cursor, uid, ids, context=None):
        aire_obj = self.pool.get('soller.operador.aire')
        return aire_obj.change_owner(cursor, uid, ids, context)

    def b_download_documents(self, cursor, uid, ids, context=None):
        aire_obj = self.pool.get('soller.operador.aire')
        return aire_obj.download_documents(cursor, uid, ids, context)

SollerOperadorAireLinia()