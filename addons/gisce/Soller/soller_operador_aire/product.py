# -*- encoding: utf-8 -*-

from osv import fields, osv

class ProductAire(osv.osv):
    _inherit = 'product.product'

    _columns = {
        'aire_product': fields.char('Aire product', size=150)
    }
ProductAire()