# -*- encoding: utf-8 -*-

from osv import osv, fields

class WizardNewNumberAire(osv.osv_memory):
    _name = 'wizard.download.mobile.cdr.aire'

    def action_download_mobile_cdr(self, cursor, uid, ids, context=None):
        soller_operador_aire_obj = self.pool.get('soller.operador.aire')
        wizard = self.browse(cursor, uid, ids[0], context=context)
        date = wizard.date or None
        soller_operador_aire_obj.download_mobile_cdr(cursor, uid,
                                                     date=date,
                                                     context=context)

        return {}

    _columns = {
        'date': fields.date(string="Date")
    }

WizardNewNumberAire()