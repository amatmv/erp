# -*- encoding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
from tools.translate import _


class WizardNewNumberAire(osv.osv_memory):
    """
    Wizard for create a new number request in Aire
    """

    _name = "wizard.new.number.aire"

    def action_new_phone_request(self, cursor, uid, ids, context=None):
        '''
            Create product in erp and make request for new phone number in aire
        '''
        linea_obj = self.pool.get('soller.operador.contrato.linea')
        operador_aire_obj = self.pool.get('soller.operador.aire')

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        contract_ids = context.get('active_ids', [])

        # Phone number + lock code for new number request
        phone_number = wizard.phone_number.split("-")[0]
        lock_code = wizard.phone_number.split("-")[1]

        #create product
        vals = {
            'product_id': wizard.product_id.id,
            'quantitat': 1,
            'contrato_id': contract_ids[0],
            'facturar': False,
            'data_alta': wizard.start_date,
            'detall': phone_number,
            'linea_partner_id': wizard.partner_id.id,
            'active': True,
            'sim': wizard.sim,
            'apoderat_id': wizard.representative_id.id,

        }
        linea_id = linea_obj.create(cursor, uid, vals)

        # New phone request
        operador_aire_obj.new_phone_request(cursor, uid, linea_id, lock_code,
                                            context=context)
        return True

    def _get_numbers(self, cursor, uid, context=None):
        try:
            operador_aire_obj = self.pool.get('soller.operador.aire')
            numbers_list = operador_aire_obj.get_new_phone_numbers(cursor, uid,
                                                               context=context)

            # phone number + code for new phone request
            return [(x, x.split('-')[0]) for x in numbers_list]
        except Exception, e:
            return [('error', _('Error in new numbers request'))]

    _columns = {
        'product_id': fields.many2one('product.product', 'Product',
                                      required=True),
        'phone_number': fields.selection(_get_numbers, 'Phone number',
                                         select=True, readonly=False),
        'start_date': fields.date('Start date'),
        'partner_id': fields.many2one('res.partner', 'Operator',
                                      required=False),
        'sim': fields.char('Operator sim', size=32),
        'representative_id': fields.many2one('res.partner', 'Representative',
                                             required=False),
    }

    _defaults = {
        'start_date': lambda *a: datetime.now().strftime('%Y-%m-%d'),
    }

WizardNewNumberAire()