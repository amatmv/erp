# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardContractExpirationDates(osv.osv_memory):
    _name = 'wizard.contract.expiration.dates'

    def action_show_expiration_contracts(self, cursor, uid, ids, context=None):
        contract_obj = self.pool.get('giscedata.polissa')
        wizard = self.browse(cursor, uid, ids[0])

        contract_ids = contract_obj.get_contracts_to_expire(
            cursor, uid, wizard.start_date, wizard.end_date, context
        )
        domain = [('id', 'in', contract_ids)]

        view_list = contract_obj.fields_view_get(cursor, uid, view_type='tree')
        view_form = contract_obj.fields_view_get(cursor, uid)

        return{
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree, form',
            'res_model': 'giscedata.polissa',
            'target': 'current',
            'context': context,
            'domain': domain,
            'view_id': False,
            'views': [
                (view_list['view_id'], 'tree'), (view_form['view_id'], 'form')
            ]
        }

    _columns = {
        'start_date': fields.date('Start date', required=True),
        'end_date': fields.date('End date', required=True),
    }

WizardContractExpirationDates()
