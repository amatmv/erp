# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* contract_status_checks
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2016-04-01 07:29:14+0000\n"
"PO-Revision-Date: 2016-04-01 07:29:14+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: contract_status_checks
#: model:ir.module.module,description:contract_status_checks.module_meta_information
msgid "\n"
"This module adds some functionalities to help to check the status\n"
"of the contracts and their contract modifications:\n"
"    * Widget to search for contracts that will expire between two dates.\n"
"    * Cron task to inform by email the contracts that are going to expire in the following X days\n"
"    * Cron daily task to inform  by email about the contract modifications done\n"
"  "
msgstr "\n"
"This module adds some functionalities to help to check the status\n"
"of the contracts and their contract modifications:\n"
"    * Widget to search for contracts that will expire between two dates.\n"
"    * Cron task to inform by email the contracts that are going to expire in the following X days\n"
"    * Cron daily task to inform  by email about the contract modifications done\n"
"  "

#. module: contract_status_checks
#: view:wizard.contract.expiration.dates:0
msgid "Search for contracts that will expire soon"
msgstr "Search for contracts that will expire soon"

#. module: contract_status_checks
#: code:addons/contract_status_checks/contract_status_checks.py:69
#, python-format
msgid "Coming soon will expire {} contracts\n"
""
msgstr "Coming soon will expire {} contracts\n"
""

#. module: contract_status_checks
#: code:addons/contract_status_checks/contract_status_checks.py:191
#, python-format
msgid "\n"
"... and {} more contract modifications not listed in the email\n"
""
msgstr "\n"
"... and {} more contract modifications not listed in the email\n"
""

#. module: contract_status_checks
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Invalid model name in the action definition."

#. module: contract_status_checks
#: model:ir.actions.act_window,name:contract_status_checks.action_wizard_contract_expiration_dates
#: model:ir.ui.menu,name:contract_status_checks.menu_wizard_contract_expiration_dates
msgid "Filter contracts by expiration date"
msgstr "Filter contracts by expiration date"

#. module: contract_status_checks
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: contract_status_checks
#: field:wizard.contract.expiration.dates,start_date:0
msgid "Start date"
msgstr "Start date"

#. module: contract_status_checks
#: code:addons/contract_status_checks/contract_status_checks.py:64
#, python-format
msgid "It seems there are no contracts that will expire the next {} days."
msgstr "It seems there are no contracts that will expire the next {} days."

#. module: contract_status_checks
#: constraint:ir.cron:0
msgid "Invalid arguments"
msgstr "Invalid arguments"

#. module: contract_status_checks
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: contract_status_checks
#: code:addons/contract_status_checks/contract_status_checks.py:164
#, python-format
msgid "It seems that yesterday({}) any contract modification was created or updated"
msgstr "It seems that yesterday({}) any contract modification was created or updated"

#. module: contract_status_checks
#: field:wizard.contract.expiration.dates,end_date:0
msgid "End date"
msgstr "End date"

#. module: contract_status_checks
#: code:addons/contract_status_checks/contract_status_checks.py:176
#, python-format
msgid "{} Contract modification number {} of the contract {} {}\n"
""
msgstr "{} Contract modification number {} of the contract {} {}\n"
""

#. module: contract_status_checks
#: model:ir.module.module,shortdesc:contract_status_checks.module_meta_information
msgid "Contract Status Checks"
msgstr "Contract Status Checks"

#. module: contract_status_checks
#: code:addons/contract_status_checks/contract_status_checks.py:92
#, python-format
msgid "\n"
"... and other {} contracts not listed\n"
""
msgstr "\n"
"... and other {} contracts not listed\n"
""

#. module: contract_status_checks
#: code:addons/contract_status_checks/contract_status_checks.py:79
#, python-format
msgid "Contract {} of the holder {} will expire on {}\n"
""
msgstr "Contract {} of the holder {} will expire on {}\n"
""

#. module: contract_status_checks
#: model:ir.model,name:contract_status_checks.model_wizard_contract_expiration_dates
msgid "wizard.contract.expiration.dates"
msgstr "wizard.contract.expiration.dates"

#. module: contract_status_checks
#: view:wizard.contract.expiration.dates:0
msgid "Accept"
msgstr "Accept"

#. module: contract_status_checks
#: code:addons/contract_status_checks/contract_status_checks.py:169
#, python-format
msgid "Yesterday a total of {} contract modifications had been created or updated\n"
"\n"
""
msgstr "Yesterday a total of {} contract modifications had been created or updated\n"
"\n"
""

#. module: contract_status_checks
#: code:addons/contract_status_checks/contract_status_checks.py:158
#, python-format
msgid "({})List of contract modifications created or updated yesterday ({})"
msgstr "({})List of contract modifications created or updated yesterday ({})"

#. module: contract_status_checks
#: view:wizard.contract.expiration.dates:0
msgid "Cancel"
msgstr "Cancel"

#. module: contract_status_checks
#: code:addons/contract_status_checks/contract_status_checks.py:57
#, python-format
msgid "({})Contracts that will expire in {} days (from {} to {})"
msgstr "({})Contracts that will expire in {} days (from {} to {})"

