# -*- coding: utf-8 -*-
{
    "name": "Contract Status Checks",
    "description": """
This module adds some functionalities to help to check the status
of the contracts and their contract modifications:
    * Widget to search for contracts that will expire between two dates.
    * Cron task to inform by email the contracts that are going to expire in the following X days
    * Cron daily task to inform  by email about the contract modifications done
  """,
    "version": "0-dev",
    "author": "Miquel Frontera",
    "category": "Soller",
    "depends":[
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "contract_status_checks_scheduler.xml",
        "wizard/wizard_contract_expiration_dates_view.xml"
    ],
    "active": False,
    "installable": True
}
