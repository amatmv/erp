# -*- encoding: utf-8 -*-


from osv import osv
from datetime import date, timedelta, datetime
from tools.translate import _


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def get_contracts_to_expire(self, cr, uid, start_date, end_date,
                                context=None):
        contract_mod_obj = self.pool.get('giscedata.polissa.modcontractual')
        contract_ids = []

        search_params = [
            ('data_final', '>=', start_date), ('data_final', '<=', end_date)
        ]
        contract_mod_ids = contract_mod_obj.search(cr, uid, search_params)

        mod_contracts = contract_mod_obj.read(
            cr, uid, contract_mod_ids, ['polissa_id']
        )

        for mod_contract in mod_contracts:
            contract_ids.append(mod_contract['polissa_id'][0])
        return contract_ids

    def notify_contracts_to_expire(self, cursor, uid, email_from, email_to,
                                   days_limit=30, contract_limit=100,
                                   context=None):
        acc_obj = self.pool.get('poweremail.core_accounts')
        user_obj = self.pool.get('res.users')

        # Search for the account = email_from
        search_params = [('email_id', '=', email_from)]
        acc_id = acc_obj.search(cursor, uid, search_params)[0]

        user = user_obj.browse(cursor, uid, uid)
        whereiam = user.company_id.partner_id.name

        if not context:
            context = {}
        # Set lang of the user in the context to translate correctly:
        context.update({'lang': user.context_lang})

        today = date.today()
        last_day = today + timedelta(days=days_limit)
        contract_ids = self.get_contracts_to_expire(
            cursor, uid, today.strftime('%Y-%m-%d'),
            last_day.strftime('%Y-%m-%d')
        )

        subject = _(
            u"({})Contracts that will expire in {} days (from {} to {})"
        ).format(
            whereiam, days_limit, today.strftime('%d-%m-%Y'),
            last_day.strftime('%d-%m-%Y')
        )

        message = _(
                u"It seems there are no contracts that will expire "
                u"the next {} days."
        ).format(days_limit)
        if len(contract_ids) > 0:
            message = _(
                u"Coming soon will expire {} contracts\n"
            ).format(len(contract_ids))

        contracts_notified = 0
        # reversed to order contracts from lowest expiration to later expiration
        for contract in reversed(self.browse(cursor, uid, contract_ids)):
            contract_due_date = datetime.strptime(
                contract.modcontractual_activa.data_final, "%Y-%m-%d"
            )
            message += _(
                u"Contract {} of the holder {} will expire on {}\n"
            ).format(
                contract.name, contract.titular.name,
                contract_due_date.strftime('%d-%m-%Y')
            )

            contracts_notified += 1
            if contracts_notified == contract_limit:
                # Avoid to print too many contracts in the mail.
                break

        if len(contract_ids) > contract_limit:
            message += _(
                u"\n... and other {} contracts not listed\n"
            ).format(
                len(contract_ids) - contract_limit
            )

        acc_obj.send_mail(
            cursor, uid, [acc_id], {'To': email_to}, subject, {'text': message}
        )
        return True

GiscedataPolissa()

class GiscedataPolissaModContractual(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    def contract_modifications_done(self, cursor, uid, date,
                                    context=None):
        if not context:
            context = {}
        context.update({'active_test': False})  # Show not active created

        start = datetime.strptime(date, '%Y-%m-%d')
        end = datetime(start.year, start.month, start.day, 23, 59, 59)
        created_search = [
            ('create_date', '>=', start.strftime('%Y-%m-%d %H:%M:%S')),
            ('create_date', '<=', end.strftime('%Y-%m-%d %H:%M:%S')),
        ]
        modified_search = [
            ('write_date', '>=', start.strftime('%Y-%m-%d %H:%M:%S')),
            ('write_date', '<=', end.strftime('%Y-%m-%d %H:%M:%S'))
        ]

        contractmod_created = set(self.search(
            cursor, uid, created_search, context=context
        ))
        contractmod_modified = set(self.search(
            cursor, uid, modified_search, context=context
        ))

        return list(contractmod_created | contractmod_modified)

    def notify_yesterday_created_contractmods(self, cursor, uid, email_from,
                                              email_to, modcontract_limit=100,
                                              context=None):
        acc_obj = self.pool.get('poweremail.core_accounts')
        user_obj = self.pool.get('res.users')

        # Search for the account = email_from
        search_params = [('email_id', '=', email_from)]
        acc_id = acc_obj.search(cursor, uid, search_params)[0]

        user = user_obj.browse(cursor, uid, uid)

        if not context:
            context = {}
        # Set lang of the user in the context to translate correctly:
        context.update({'lang': user.context_lang})

        yesterday = datetime.today() - timedelta(days=1)
        contractmod_ids = self.contract_modifications_done(
            cursor, uid, yesterday.strftime('%Y-%m-%d')
        )

        subject = _(
            u"({})List of contract modifications created or updated"
            u" yesterday ({})"
        ).format(
            user.company_id.partner_id.name, yesterday.strftime('%d-%m-%Y')
        )
        message = _(
                u"It seems that yesterday({}) any contract modification"
                u" was created or updated"
        ).format(yesterday.strftime('%d-%m-%Y'))
        if len(contractmod_ids) > 0:
            message = _(
                u'Yesterday a total of {} contract modifications had been '
                u'created or updated\n\n'
            ).format(len(contractmod_ids))

        modcontract_notified = 0
        for contractmod in self.browse(cursor, uid, contractmod_ids):
            message += _(
                u"{} Contract modification number {} of the"
                u" contract {} {}\n"
            ).format(
                "*"*30, contractmod.polissa_id.name, contractmod.name, "*"*30
            )
            message += u"{}\n".format(contractmod.observacions)
            message += "{}\n\n".format("*"*90)

            modcontract_notified += 1
            if modcontract_notified == modcontract_limit:
                # Avoid to inform too many contract modifications
                break

        if len(contractmod_ids) > modcontract_limit:
            message += _(
                u"\n... and {} more contract modifications not listed"
                u" in the email\n"
            ).format(len(contractmod_ids) - modcontract_limit)

        acc_obj.send_mail(
            cursor, uid, [acc_id], {'To': email_to}, subject, {'text': message}
        )
        return True

GiscedataPolissaModContractual()
