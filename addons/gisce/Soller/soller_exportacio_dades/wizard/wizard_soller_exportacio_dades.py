# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import time
import csv
import base64
import StringIO


class WizardSollerExportacioDades(osv.osv_memory):

    _name = 'wizard.soller.exportacio.dades'

    def action_exportar(self, cr, uid, ids, context=None):
        '''funcio que exporta dades per la gent del departament tecnic'''
    
        wizard = self.browse(cr, uid, ids[0])
        
        query = "select cups.name as cups, cups.et as ct,p.name as abonado, titular.name as titular,\
                    p.data_alta as fecha_alta, coalesce(icp.intensitat,0) as icp, tensio.name as tension,\
                    activa.consumo as totalactiva, reactiva.consumo as totalreactiva,\
                    p.potencia as potencia\
                    FROM giscedata_polissa p\
                    INNER JOIN giscedata_cups_ps cups ON p.cups = cups.id\
                    INNER JOIN giscedata_facturacio_factura f ON f.polissa_id = p.id\
                    INNER JOIN account_invoice i on i.id = f.invoice_id\
                    INNER JOIN giscedata_tensions_tensio tensio ON tensio.id = p.tensio_normalitzada\
                    INNER JOIN res_partner titular on titular.id = p.titular\
                    LEFT JOIN giscedata_polissa_icp icp ON p.id = icp.polissa_id\
                    LEFT JOIN\
                    (SELECT\
                        giscedata_facturacio_lectures_energia.factura_id,\
                        sum(case\
                      	when giscedata_facturacio_lectures_energia.lect_actual > giscedata_facturacio_lectures_energia.lect_anterior\
	                    then giscedata_facturacio_lectures_energia.lect_actual - giscedata_facturacio_lectures_energia.lect_anterior\
                    	when giscedata_facturacio_lectures_energia.lect_actual < giscedata_facturacio_lectures_energia.lect_anterior\
                    	then (giscedata_lectures_comptador.giro - giscedata_facturacio_lectures_energia.lect_anterior) + giscedata_facturacio_lectures_energia.lect_actual\
                    	else 0\
                        end) as consumo\
                    FROM\
                    giscedata_facturacio_lectures_energia\
                    INNER JOIN\
                    giscedata_lectures_comptador\
                    ON\
                    giscedata_facturacio_lectures_energia.comptador_id = giscedata_lectures_comptador.id\
                    WHERE giscedata_facturacio_lectures_energia.tipus = 'activa'\
                    GROUP BY giscedata_facturacio_lectures_energia.factura_id) activa on activa.factura_id = f.id\
                    LEFT JOIN\
                    (SELECT\
                        giscedata_facturacio_lectures_energia.factura_id,\
                        sum(case\
                        when giscedata_facturacio_lectures_energia.lect_actual > giscedata_facturacio_lectures_energia.lect_anterior\
                        then giscedata_facturacio_lectures_energia.lect_actual - giscedata_facturacio_lectures_energia.lect_anterior\
                        when giscedata_facturacio_lectures_energia.lect_actual < giscedata_facturacio_lectures_energia.lect_anterior\
                        then (giscedata_lectures_comptador.giro - giscedata_facturacio_lectures_energia.lect_anterior) + giscedata_facturacio_lectures_energia.lect_actual\
                        else 0\
                        end) as consumo\
                    FROM\
                    giscedata_facturacio_lectures_energia\
                    INNER JOIN\
                    giscedata_lectures_comptador\
                    ON\
                    giscedata_facturacio_lectures_energia.comptador_id = giscedata_lectures_comptador.id\
                    WHERE giscedata_facturacio_lectures_energia.tipus = 'reactiva'\
                    GROUP BY giscedata_facturacio_lectures_energia.factura_id) reactiva on reactiva.factura_id = f.id\
                    WHERE\
                    i.date_invoice between '%s' and '%s'\
                    ORDER BY cups.name" %(wizard.data_inici, wizard.data_final)

        cr.execute(query)
        
        #arxiu = open('arxiu.csv', 'wb')
        output = StringIO.StringIO()
        csvwriter = csv.writer(output, delimiter=';')
        
        for resultat in cr.fetchall():
            #codificamos el titular en ascii ignorando los caracteres no valido
            resultat_list = list(resultat)
            resultat_list[3] = resultat_list[3].encode('ascii', 'ignore')
            csvwriter.writerow(resultat_list)

        out = base64.b64encode(unicode(output.getvalue()))
        
        return wizard.write({'arxiu': out, 'state': 'end'})
        
        
    
    _columns = {
        'data_inici': fields.date('Data inici'),
        'data_final': fields.date('Data final'),
        'state':fields.selection([('init','Init'),
                                  ('end','End'),],
                                 'Estat', readonly=True),
        'arxiu':fields.binary('Arxiu', filters=None),
            
    }
    
    _defaults = {
        'state': lambda *a: 'init',
        
    }

WizardSollerExportacioDades()
