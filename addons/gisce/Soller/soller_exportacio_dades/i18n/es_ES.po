# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
#   <misern@gisce.net>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2012-03-28 16:07:27+0000\n"
"PO-Revision-Date: 2012-04-16 13:46+0000\n"
"Last-Translator: misern <misern@gisce.net>\n"
"Language-Team: Spanish (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: es_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: soller_exportacio_dades
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: soller_exportacio_dades
#: selection:wizard.soller.exportacio.dades,state:0
msgid "End"
msgstr "End"

#. module: soller_exportacio_dades
#: model:ir.actions.act_window,name:soller_exportacio_dades.action_wizard_soller_exportar_dades_form
#: model:ir.ui.menu,name:soller_exportacio_dades.menu_wizard_soller_exportar_dades
msgid "Exportar dades"
msgstr "Exportar datos"

#. module: soller_exportacio_dades
#: field:wizard.soller.exportacio.dades,arxiu:0
msgid "Arxiu"
msgstr "Archivo"

#. module: soller_exportacio_dades
#: field:wizard.soller.exportacio.dades,data_final:0
msgid "Data final"
msgstr "Fecha final"

#. module: soller_exportacio_dades
#: view:wizard.soller.exportacio.dades:0
msgid "Finalitzar"
msgstr "Finalizar"

#. module: soller_exportacio_dades
#: selection:wizard.soller.exportacio.dades,state:0
msgid "Init"
msgstr "Init"

#. module: soller_exportacio_dades
#: view:wizard.soller.exportacio.dades:0
msgid "Cancel·lar"
msgstr "Cancelar"

#. module: soller_exportacio_dades
#: field:wizard.soller.exportacio.dades,data_inici:0
msgid "Data inici"
msgstr "Fecha inicio"

#. module: soller_exportacio_dades
#: field:wizard.soller.exportacio.dades,state:0
msgid "Estat"
msgstr "Estado"

#. module: soller_exportacio_dades
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Invalid model name in the action definition."

#. module: soller_exportacio_dades
#: model:ir.module.module,shortdesc:soller_exportacio_dades.module_meta_information
msgid "Soller Exportació de dades"
msgstr "Soller Exportación de datos"

#. module: soller_exportacio_dades
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: soller_exportacio_dades
#: model:ir.module.module,description:soller_exportacio_dades.module_meta_information
msgid ""
"\n"
"Exportació de dades\n"
"    *Per fer informes del departament tècnic\n"
"    "
msgstr "\nExportación de datos\n    *Para hacer informes del departamento tècnico\n    "

#. module: soller_exportacio_dades
#: model:ir.model,name:soller_exportacio_dades.model_wizard_soller_exportacio_dades
msgid "wizard.soller.exportacio.dades"
msgstr "wizard.soller.exportacio.dades"

#. module: soller_exportacio_dades
#: view:wizard.soller.exportacio.dades:0
msgid "Exportar"
msgstr "Exportar"
