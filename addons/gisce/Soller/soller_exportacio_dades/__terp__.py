# -*- coding: utf-8 -*-
{
    "name": "Soller Exportació de dades",
    "description": """
Exportació de dades
    *Per fer informes del departament tècnic
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_soller_exportacio_dades_view.xml"
    ],
    "active": False,
    "installable": True
}
