# -*- coding: utf-8 -*-
{
    "name": "Soller Importacio comptadors",
    "description": """
    * Importacio de models de comptadors del programa anterior
    * Eliminar ICP's i actualitzar limitador de telegestió
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_importar_comptadors_view.xml",
        "wizard/wizard_icp_telegestion_view.xml"
    ],
    "active": False,
    "installable": True
}
