# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import base64
import StringIO
import csv


class WizardImportarComptadors(osv.osv_memory):
    
    _name = 'wizard.importar.comptadors'


    def import_file(self, cr, uid, ids, context=None):
        
        wizard = self.browse(cr, uid, ids[0])
        lot_obj = self.pool.get('stock.production.lot')
        product_obj = self.pool.get('product.product')
        categ_obj = self.pool.get('product.category')
        polissa_obj = self.pool.get('giscedata.polissa')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        #buscamos la categoria de contadores
        search_params = [('name','=','Contadores')]
        categ_id = categ_obj.search(cr, uid, search_params)[0]
                
        file = StringIO.StringIO(base64.b64decode(wizard.arxiu))
        reader = csv.reader(file, delimiter=';')
        for row in reader:
            polissa = row[0]
            serial = row[1]
            producto = row[3]
            #buscamos si ya existe el producto
            search_params = [('name','=', producto)]
            product_id = product_obj.search(cr, uid, search_params)
            if not product_id:
                #tenemos que crear el producto
                vals = {"name": producto,
                        "categ_id": categ_id,
                        "type": 'product',
                        "procure_method": "make_to_stock",
                        "supply_method": 'buy',
                        "standard_price": 0.0,
                        "list_price": 0.0,
                        "uom_id": 1,
                        "uom_po_id": 1,
                        }
                product_id = product_obj.create(cr, uid, vals)
            if isinstance(product_id, tuple) or isinstance(product_id,list):
                product_id = product_id[0]
            #buscamos si ya existe el lote
            search_params = [('name','=',serial)]
            lot_id = lot_obj.search(cr, uid, search_params)
            if not lot_id:
                lot_id = lot_obj.create(cr, uid, {'name': serial, 'product_id': product_id})
            if isinstance(lot_id, tuple) or isinstance(lot_id,list):
                lot_id = lot_id[0]
            if lot_obj.browse(cr, uid, lot_id).product_id.id <> product_id:
                #El lote existia per no se corresponde con el mismo producto
                #no hacemos nada mas porque esto es un error
                continue
            
            #buscamos la poliza
            search_params = [('name','=',polissa)]
            polissa_id = polissa_obj.search(cr, uid, search_params,
                                            0, None, None,
                                            context={'active_test': False})
            if polissa_id:
                polissa_id = polissa_id[0]
            else:
                polissa_id = 0;
            #buscamos el contador dentro de la poliza que hemos encontrado
            #si ya tenia un lote asignado no haremos nada                   
            search_params = [('name','=',serial),
                             ('polissa','=',polissa_id),
                             ('serial','=', None),]
            comptador_id = comptador_obj.search(cr, uid, search_params,
                                                0, None, None,
                                                context={'active_test': False})
            if comptador_id:
                #asignamos el lote al contador
                #print 'Asignando producto %s al contador %s de la poliza %s' %(producto, serial, polissa_obj.browse(cr, uid, polissa_id).name)
                comptador_obj.write(cr, uid, comptador_id, 
                                    {'serial': lot_id}, 
                                    context={'active_test': False, 'sync': False})                 
            
        return {}
        

    _columns = {      
        'arxiu':fields.binary('Arxiu', required=True, filters=None),
                                      
    }              
        
               
WizardImportarComptadors()
