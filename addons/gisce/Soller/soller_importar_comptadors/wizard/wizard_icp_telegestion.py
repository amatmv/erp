# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

class WizardICPTelegestio(osv.osv_memory):
    '''
    Wizard para eliminar aquellos icps de contratos en borrador
    y actualizar los valores del campo telegestion del contador asociado
    '''
    _name = 'wizard.icp.telegestio'

    def action_actualitzar(self, cr, uid, ids, context={}):
        
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        icp_obj = self.pool.get('giscedata.polissa.icp')
        polissa_obj = self.pool.get('giscedata.polissa')
    
        #Cerquem totes les polisses que hem de processar
        search_params = [('codi','=',False),
                         ('comercialitzadora','=',False),
                         ('state','=','esborrany'),
                        ]
        polissa_ids = polissa_obj.search(cr, uid, search_params)

        for polissa in polissa_obj.browse(cr, uid, polissa_ids):
            print '%s, %s, %s, %s' %(polissa.name, polissa.state, polissa.comercialitzadora.id, polissa.codi)

        print len(polissa_ids)

        #eliminem tots els icps d'aquestes polisses
        search_params = [('polissa_id','in',polissa_ids),]
        icp_ids = icp_obj.search(cr, uid, search_params)
        print len(icp_ids)
        icp_obj.unlink(cr, uid, icp_ids)

        #Actualitzem els comptadors amb les dades de telegestió
        search_params = [('polissa','in',polissa_ids),]
        comptador_ids = comptador_obj.search(cr, uid, search_params)
        print len(comptador_ids)
        for comptador in comptador_obj.browse(cr, uid, comptador_ids):
            comptador.write({'telegestio': True,
                             'tel_icp_actiu': True,
                             'tel_icp_potencia': comptador.polissa.potencia,
                            })
        return {}
        
    
WizardICPTelegestio()
