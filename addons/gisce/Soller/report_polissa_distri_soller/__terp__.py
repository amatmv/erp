# -*- coding: utf-8 -*-
{
    "name": "Reports Pòlissa Sóller (Distribució)",
    "description": """
Modificació dels reports de pòlissa de distribució:
  * Pòlissa
  * Fitxa de pòlissa
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "base",
        "product",
        "giscedata_polissa_distri",
        "jasper_reports",
        "soller_contractacio_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_polissa_distri_soller_report.xml"
    ],
    "active": False,
    "installable": True
}
