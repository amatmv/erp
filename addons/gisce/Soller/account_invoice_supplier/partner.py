# -*- encoding: utf-8 -*-

from osv import osv, fields


class ResPartnerSupplier(osv.osv):

    _inherit = 'res.partner'

    _columns = {
        'supplier_fiscal_position_id': fields.many2one('account.fiscal.position',
                                                       'Supplier Fiscal Position'),
    }

ResPartnerSupplier()