# -*- coding: utf-8 -*-
{
    "name": "Account Invoice supplier",
    "description": """
Add helpers for introducing supplier invoices
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Accounting",
    "depends":[
        "account",
        "l10n_ES_aeat_sii"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "supplier_data.xml",
        "partner_view.xml",
        "invoice_view.xml",
        "wizard/wizard_invoice_supplier_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
