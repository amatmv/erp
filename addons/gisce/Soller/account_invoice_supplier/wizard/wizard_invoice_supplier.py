# -*- encoding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
from sii.models.invoices_record import CRE_FACTURAS_RECIBIDAS
from account.invoice import RECTIFICATIVE_TYPE_SELECTION
from tools.translate import _

class WizardInvoiceSupplier(osv.osv_memory):

    _name= 'wizard.invoice.supplier'

    def create_invoice(self, cursor, uid, ids, context=None):
        '''Create supplier invoice from wizard data'''

        if not context:
            context = {}

        invoice_obj = self.pool.get('account.invoice')
        invline_obj = self.pool.get('account.invoice.line')
        partner_obj = self.pool.get('res.partner')

        wizard = self.browse(cursor, uid, ids[0])
        # Check if invoice exist
        search_params = [('origin', '=', wizard.invoice_number),
                         ('partner_id', '=', wizard.partner_id.id)]
        invoice_ids = invoice_obj.search(cursor, uid, search_params)
        if invoice_ids:
            msg = _('The invoice number already exist')
            raise osv.except_osv('Error', msg)

        type = 'in_invoice'
        if wizard.r_type in ('A', 'B'):
            type = 'in_refund'

        # Force invoice address is the same contact address
        address = partner_obj.address_get(cursor, uid, [wizard.partner_id.id],
                                      ['contact'])

        invoice_data = invoice_obj.onchange_partner_id(cursor, uid, [],
                                                       'in_invoice',
                                                       wizard.partner_id.id)
        invoice_data = invoice_data['value']
        invoice_data.update({
            'partner_id': wizard.partner_id.id,
            'account_id': wizard.account_id.id,
            'check_total': wizard.amount_total,
            'type': type,
            'date_invoice': wizard.accounting_date,
            'origin_date_invoice': wizard.invoice_date,
            'journal_id': wizard.journal_id.id,
            'origin': wizard.invoice_number,
            'reference': wizard.reference,
            'rectificative_type': wizard.r_type,
            'rectifying_id': wizard.r_id.id,
            'address_invoice_id': address['contact']
        })
        invoice_id = invoice_obj.create(cursor, uid, invoice_data,
                                        context=context)

        # Write sii key to overwrite the one in the creation
        invoice_obj.write(cursor, uid, [invoice_id],
                          {'sii_in_clave_regimen_especial': wizard.sii_cre})

        for line in wizard.line_ids:
            tax_ids = [x.id for x in line.tax_ids]
            line_data = {
                'invoice_id': invoice_id,
                'name': line.description,
                'account_id': line.account_id.id,
                'price_unit': line.amount,
                'invoice_line_tax_id': [(6, 0, tax_ids)],
                'quantity': 1,
            }
            invoice_line_id = invline_obj.create(cursor, uid, line_data)

        invoice_obj.button_reset_taxes(cursor, uid, [invoice_id],
                                       context=context)

        invoice = invoice_obj.browse(cursor, uid, invoice_id)
        if invoice.amount_total != wizard.amount_total:
            message = 'Totals do not match\nLines: {:.2f} vs Total: {:.2f}'
            message.format(invoice.amount_total, wizard.amount_total)
            raise osv.except_osv('Error', message.format(invoice.amount_total,
                                                         wizard.amount_total))

        return invoice_id

    def action_create_invoice(self, cursor, uid, ids, context=None):

        invoice_obj = self.pool.get('account.invoice')

        invoice_id = self.create_invoice(cursor, uid, ids, context)
        invoice = invoice_obj.browse(cursor, uid, invoice_id)
        type = 'in_invoice'
        if invoice.rectificative_type in ('A', 'B'):
            type = 'in_refund'

        res = {
            'type': 'ir.actions.act_window',
            'name': 'Created Invoice',
            'res_model': 'account.invoice',
            'view_type':'form',
            'view_mode': 'tree,form',
            'domain': [('type', '=', type), ('id', '=', invoice_id)],
            'context': {'type': type},
        }

        return res

    def onchange_partner_id(self, cursor, uid, ids, partner_id,
                            context=None):

        if not context:
            context = {}

        partner_obj = self.pool.get('res.partner')

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if partner_id:
            partner = partner_obj.browse(cursor, uid, partner_id)
            account_id = partner.property_account_payable.id
            res['value'].update({'account_id': account_id})

        return res

    def _get_default_journal(self, cursor, uid, context=None):

        journal_obj = self.pool.get('account.journal')

        search_params = [('code', '=', 'SUPPLIER')]
        journal_ids = journal_obj.search(cursor, uid, search_params)

        return journal_ids[0]

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Supplier',
                                      required=True,
                                      domain=[('supplier', '=', True)]),
        'invoice_date': fields.date('Supplier Date', required=True),
        'accounting_date': fields.date('Accounting Date', required=True),
        'account_id': fields.many2one('account.account', 'Account',
                                      required=True,
                                      domain=[('type', '<>', 'view')]),
        'journal_id': fields.many2one('account.journal', 'Journal',
                                      required=True,
                                      domain=[('type', '=', 'purchase')]),
        'amount_total': fields.float('Total Amount', digits=(16, 2),
                                      required=True),
        'invoice_number': fields.char('Number', size=40, required=True),
        'reference': fields.char('Reference', size=50),
        'line_ids': fields.one2many('wizard.invoice.supplier.line',
                                    'wizard_id',
                                    'Lines'),
        'r_type': fields.selection(RECTIFICATIVE_TYPE_SELECTION,
                                   'Rectificative type', required=True),
        'r_id': fields.many2one('account.invoice',
                                'Ref. invoice'),
        'sii_cre': fields.selection(CRE_FACTURAS_RECIBIDAS,
                                    required=True,
                                    string='SII Key'),
    }

    _defaults = {
        'accounting_date': lambda *a: datetime.now().strftime('%Y-%m-%d'),
        'sii_cre': lambda *a: '01',
        'journal_id': _get_default_journal,
        'r_type': lambda *a: 'N',
    }

WizardInvoiceSupplier()


class WizardInvoiceSupplierLine(osv.osv_memory):

    _name = 'wizard.invoice.supplier.line'

    def _get_default_taxes(self, cursor, uid, context=None):

        if not context:
            context = {}
        if context.get('partner_id', 0):
            partner_id = context['partner_id']
            partner_obj = self.pool.get('res.partner')
            partner = partner_obj.browse(cursor, uid, partner_id)
            if not partner.supplier_fiscal_position_id:
                tax_ids = []
            else:
                fiscal_position = partner.supplier_fiscal_position_id
                tax_ids = [tax.tax_src_id.id
                           for tax in fiscal_position.tax_ids
                           if not tax.tax_dest_id]
        return tax_ids

    _columns = {
        'wizard_id': fields.many2one('wizard.invoice.supplier',
                                     'Wizard', required=True),
        'account_id': fields.many2one('account.account', 'Account',
                                      required=True,
                                      domain=[('type', '<>', 'view')]),
        'amount': fields.float('Amount', digits=(16, 2)),
        'description': fields.char('Description', size=250),
        'tax_ids': fields.many2many('account.tax', 'wizard_supplier_line_tax',
                                    'line_id', 'tax_id', 'Taxes',
                                    domain=[('type_tax_use', '=', 'purchase')]),
    }

    _defaults = {
        'tax_ids': _get_default_taxes,
    }

WizardInvoiceSupplierLine()
