SELECT
  move.id
FROM stock_move move
INNER JOIN stock_picking picking
  ON picking.id = move.picking_id
WHERE
  move.product_id = %s
  AND
  picking.type = %s
  AND
  picking.origin_date >= %s
  AND
  picking.origin_date <= %s