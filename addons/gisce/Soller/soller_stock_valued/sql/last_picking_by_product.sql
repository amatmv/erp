SELECT
  move.id
FROM stock_move move
INNER JOIN stock_picking picking
  ON picking.id = move.picking_id
WHERE
  move.product_id = %s
  AND
  picking.type = %s
ORDER BY picking.origin_date desc
LIMIT 1