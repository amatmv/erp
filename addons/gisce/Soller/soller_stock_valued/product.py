# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools import float_round
from addons import get_module_resource
from datetime import datetime, timedelta


class ProductProduct(osv.osv):

    _inherit = 'product.product'

    def get_pickings(self, cursor, uid, product_id, type='in',
                     date_from=None, date_to=None, context=None):
        '''Get all type pickings where product_id has been included
        '''

        move_obj = self.pool.get('stock.move')

        if isinstance(product_id, (list, tuple)):
            product_id = product_id[0]

        if date_from is None:
            date_from = '1900-01-01'

        if date_to is None:
            date_to = '3000-01-01'

        sql_file = get_module_resource('soller_stock_valued', 'sql',
                                       'pickings_by_product.sql')
        sql = open(sql_file, 'r').read()
        cursor.execute(sql, (product_id, type, date_from, date_to))

        move_ids = [x[0] for x in cursor.fetchall()]
        # If something found return it
        if move_ids:
            return move_ids
        # Else search for the last picking and return it
        sql_file = get_module_resource('soller_stock_valued', 'sql',
                                       'last_picking_by_product.sql')
        sql = open(sql_file, 'r').read()
        cursor.execute(sql, (product_id, type))

        return [cursor.fetchone()[0]]

    def compute_cost(self, cursor, uid, ids, date=None, context=None):
        '''Compute product cost based on picking in history
        The formula we have choosen is weighted cost based
        on 14 months (400 days for simplicity)
        ((q1*p1)+(q2*p2)+....+(Qn*Pn))/(q1+q2+...Qn)
        '''

        if not context:
            context = {}

        move_obj = self.pool.get('stock.move')

        if date is None:
            date_dt = datetime.now()
        else:
            date_dt = datetime.strptime(date, '%Y-%m-%d')

        days_back = 400

        date_from_dt = date_dt - timedelta(days=400)
        date_from = date_from_dt.strftime('%Y-%m-%d')

        res = dict.fromkeys(ids, 0)
        for product_id in ids:
            product = self.browse(cursor, uid, product_id)
            move_ids = self.get_pickings(cursor, uid, product_id,
                                         date_from=date_from,
                                         date_to=date,
                                         context=context)
            if not move_ids:
                res[product_id] = 0
                continue
            move_vals = move_obj.read(cursor, uid, move_ids,
                                      ['price_subtotal', 'product_qty'])
            up = down = 0
            for move_val in move_vals:
                price_unit = (move_val['price_subtotal'] /
                              move_val['product_qty'])
                up += price_unit * move_val['product_qty']
                down += move_val['product_qty']

            weighted_cost = float_round(up / down, 2)
            res[product_id] = weighted_cost

        if context.get('str_keys', False):
            new_res = dict([(str(key), value)
                            for key, value in res.iteritems()])
            return new_res
        return res

    _columns = {
        'computed_cost': fields.float('Computed cost', digits=(16, 2)),
    }

ProductProduct()
