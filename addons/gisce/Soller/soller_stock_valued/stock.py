# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools import float_round


class StockMove(osv.osv):

    _inherit = 'stock.move'

    def onchange_product_id(self, cursor, uid, ids, prod_id=False,
                            loc_id=False, loc_dest_id=False, address_id=False):

        product_obj = self.pool.get('product.product')

        res = super(StockMove,
                    self).onchange_product_id(cursor, uid, ids,
                                              prod_id=prod_id,
                                              loc_id=loc_id,
                                              loc_dest_id=loc_dest_id,
                                              address_id=address_id)
        res.setdefault('value', {})
        if prod_id:
            names = product_obj.name_get(cursor, uid, [prod_id])[0][1]
            res['value'].update({'description': names})
        else:
            res['value'].update({'description': ''})

        return res

    def _get_move_prices(self, cursor, uid, ids, name, arg, context=None):

        res = dict.fromkeys(ids, {'price_net': 0,
                                  'price_subtotal': 0})

        fields = ['price_unit', 'product_qty', 'price_discount']
        vals = self.read(cursor, uid, ids, fields, context=context)

        for val in vals:
            move_id = val['id']

            net = val['price_unit'] - (val['price_unit'] *
                                       val['price_discount'] / 100)
            round_net = float_round(net, 2) 

            subtotal = net * val['product_qty']
            round_subtotal = float_round(subtotal, 2)
 
            res[move_id] = {'price_net': round_net,
                            'price_subtotal': round_subtotal}
        return res

    _store_move_price = {
        'stock.move': (lambda self, cr, uid, ids, c=None: ids, [], 20),
    }

    _columns = {
        'price_unit': fields.float('Price unit', digits=(16, 4)),
        'price_discount': fields.float('Discount (%)', digits=(16, 2)),
        'price_net': fields.function(_get_move_prices, method=True,
                                     type='float', digits=(16, 2),
                                     string="Price net",
                                     store=_store_move_price,
                                     multi='stock_prices'),
        'price_subtotal': fields.function(_get_move_prices, method=True,
                                          type='float', digits=(16, 2),
                                          string="Price subtotal",
                                          store=_store_move_price,
                                          multi='stock_prices'),
        'description': fields.char('Description', size=240),
    }

    _defaults = {
        'price_unit': lambda *a: 0,
        'price_discount': lambda *a: 0,
    }

StockMove()


class StockPicking(osv.osv):

    _inherit = 'stock.picking'

    def _get_picking_total(self, cursor, uid, ids, name, arg, context=None):

        move_obj = self.pool.get('stock.move')

        vals = self.read(cursor, uid, ids, ['move_lines'])

        res = dict.fromkeys(ids, 0)
        for val in vals:
            picking_id = val['id']
            line_ids = val['move_lines']
            if not line_ids:
                continue
            line_vals = move_obj.read(cursor, uid, line_ids,
                                      ['price_subtotal'])
            picking_sum = sum([x['price_subtotal'] for x in line_vals])
            res[picking_id] = float_round(picking_sum, 2)
        return res

    def _get_picking_from_moves(self, cursor, uid, ids, context=None):

        move_obj = self.pool.get('stock.move')
        vals = move_obj.read(cursor, uid, ids, ['picking_id'])
        picking_ids = [x['picking_id'][0] for x in vals
                       if x['picking_id']]
        return list(set(picking_ids))

    _store_picking_subtotal = {
        'stock.picking': (lambda self, cr, uid, ids, c=None: ids, [], 20),
        'stock.move': (_get_picking_from_moves,
                       ['price_unit', 'price_discount', 'product_qty'], 20),
     }

    _columns = {
        'price_total': fields.function(_get_picking_total, method=True,
                                       type='float', digits=(16, 2),
                                       string='Total price',
                                       store=_store_picking_subtotal),
    }

StockPicking()


class StockInventoryLine(osv.osv):

    _inherit = 'stock.inventory.line'

    def _get_line_prices(self, cursor, uid, ids, name, arg, context=None):

        res = dict.fromkeys(ids, 0)

        fields = ['price_unit', 'product_qty']
        vals = self.read(cursor, uid, ids, fields, context=context)

        for val in vals:
            line_id = val['id']

            subtotal = val['price_unit'] * val['product_qty']
            round_subtotal = float_round(subtotal, 2)
 
            res[line_id] = round_subtotal
        return res

    _store_move_price = {
        'stock.inventory.line': (lambda self, cr, uid, ids, c=None: ids, [], 20),
    }

    _columns = {
        'price_unit': fields.float('Price unit', digits=(16, 4)),
        'price_subtotal': fields.function(_get_line_prices, method=True,
                                          type='float', digits=(16, 2),
                                          string="Price subtotal",
                                          store=_store_move_price),
    }

    _default = {
        'price_unit': lambda *a: 0,
    }

StockInventoryLine()


class StockInventory(osv.osv):

    _inherit = 'stock.inventory'

    def action_compute_price(self, cursor, uid, ids, context=None):

        product_obj = self.pool.get('product.product')
        line_obj = self.pool.get('stock.inventory.line')

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        vals = self.read(cursor, uid, ids, ['inventory_line_id', 'date'])

        for val in vals:
            inventory_id = val['id']
            line_ids = val['inventory_line_id']
            date = val['date'][:10]
            if not line_ids:
                continue
            line_vals = line_obj.read(cursor, uid, line_ids,
                                      ['product_id', 'product_qty'])
            for line_val in line_vals:
                product_id = line_val['product_id'][0]
                product_qty = line_val['product_qty']
                compute_cost = product_obj.compute_cost(cursor, uid,
                                                        [product_id],
                                                        date=date,
                                                        context=context)
                product_cost = compute_cost[product_id]
                line_obj.write(cursor, uid, [line_val['id']],
                               {'price_unit': product_cost})
        return True

    def _get_inventory_total(self, cursor, uid, ids, name, arg, context=None):

        line_obj = self.pool.get('stock.inventory.line')

        vals = self.read(cursor, uid, ids, ['inventory_line_id'])

        res = dict.fromkeys(ids, 0)
        for val in vals:
            inventory_id = val['id']
            line_ids = val['inventory_line_id']
            if not line_ids:
                continue
            line_vals = line_obj.read(cursor, uid, line_ids,
                                      ['price_subtotal'])
            inventory_sum = sum([x['price_subtotal'] for x in line_vals])
            res[inventory_id] = float_round(inventory_sum, 2)
        return res

    def _get_inventory_from_lines(self, cursor, uid, ids, context=None):

        line_obj = self.pool.get('stock.inventory.line')
        vals = line_obj.read(cursor, uid, ids, ['inventory_id'])
        inventory_ids = [x['inventory_id'][0] for x in vals
                         if x['inventory_id']]
        return list(set(inventory_ids))

    _store_picking_subtotal = {
        'stock.inventory': (lambda self, cr, uid, ids, c=None: ids, [], 20),
        'stock.inventory.line': (_get_inventory_from_lines,
                                 ['price_unit', 'product_qty'], 20),
     }

    _columns = {
        'price_total': fields.function(_get_inventory_total, method=True,
                                       type='float', digits=(16, 2),
                                       string='Total price',
                                       store=_store_picking_subtotal),
    }

StockInventory()
