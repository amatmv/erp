<%
    from tools import config
    from itertools import groupby

    addons_path = config['addons_path']
    cursor = objects[0]._cr
    pool = objects[0].pool
    uid = user.id
    module_name = 'soller_stock_valued'

    user_obj = pool.get('res.users')
    address_obj = pool.get('res.partner.address')
    inventory_line_obj = pool.get('stock.inventory.line')
    category_obj = pool.get('product.category')
    company = user_obj.browse(cursor, uid, uid).company_id
    company_addresses = company.partner_id.address_get(adr_pref=['contact'])
    locale = user.context_lang

    if 'contact' in company_addresses:
        company_address = address_obj.read(cursor, uid, company_addresses['contact'])
    else:
        company_address = {}

%>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/${module_name}/report/static/css/print.css"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/${module_name}/report/static/css/extra.css"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/${module_name}/report/static/css/stock_inventory.css"/>
    </head>
    <body>
        <div class="container">
            %for inventory in objects:
                <%
                    inventory_line_data = []
                    total_cost = 0
                    categ_cost = {}
                    for line in inventory.inventory_line_id:
                        line_data = {
                            'product_name': line.product_id.name_get()[0][1],
                            'category_name': line.product_id.categ_id.complete_name,
                            'product_qty': line.product_qty,
                            'unit_price': line.price_unit,
                            'price_subtotal': line.price_subtotal,
                        }
                        inventory_line_data.append(line_data)
                        total_cost += line.price_subtotal
                        categ_cost.setdefault(line_data['category_name'], 0)
                        categ_cost[line_data['category_name']] += line.price_subtotal

                    sorted_lines = sorted(inventory_line_data, key=lambda line: line['category_name'])
                    grouped_lines = groupby(sorted_lines, lambda line: line['category_name'])
                %>
                <!-- Row: Sender Data + VSE Logo -->
                <div class="row top-padding-fix">
                    <!-- Col: Sender Data -->
                    <div class="col-xs-5">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            </div>
                            <div class="panel-body invoice-data">
                                <p><b><dfn>${company.partner_id.name.upper()}</dfn></b> (${company.partner_id.vat.upper()})
                                </p>
                                <p>${'{}, {} - {}'.format(company_address['street'], company_address['zip'], company_address['city'].upper())}</p>
                                <p>${'{} - {}'.format(company_address['state_id'][1].upper(), company_address['country_id'][1].upper())}</p>
                                <p>${_('Phone')} ${company_address['phone']}</p>
                                %if company.partner_id.website:
                                    <p>${company.partner_id.website}</p>
                                %endif
                                %if company_address['email']:
                                    <p>${company_address['email']}</p>
                                %endif
                                <p style="font-size: 8px;">${company.rml_footer1}</p>
                            </div>
                        </div>
                    </div>
                        <!-- Col: VSE Logo -->
                        <div class="col-xs-5 col-xs-offset-2">
                            <img style="width: 80%; margin: 0 auto" src="${addons_path}/${module_name}/report/static/img/vse_solo.jpg"/>
                        </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>Líneas de inventario</h4>
                            </div>
                            <div class="panel-body invoice-data">
                                <table>
                                    <thead>
                                        <tr>
                                            <th class="table-cell-shrink"></th>
                                            <th class="table-cell-shrink">Cantidad</th>
                                            <th class="table-cell-shrink">Precio/unidad</th>
                                            <th class="table-cell-shrink">Precio total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        %for categ_name, lines in grouped_lines:
                                            <tr>
                                                <td colspan="5" style="font-weight: bold;">${categ_name} (${formatLang(categ_cost[categ_name], digits=2)} €)</td>
                                            </tr>
                                            %for line in lines:
                                                <tr>
                                                    <td class="table-cell-expand">${line['product_name']}</td>
                                                    <td class="text-right table-cell-shrink">${formatLang(line['product_qty'], digits=0)}</td>
                                                    <td class="text-right table-cell-shrink">${formatLang(line['unit_price'], digits=2)} €</td>
                                                    <td class="text-right table-cell-shrink">${formatLang(line['price_subtotal'], digits=2)} €</td>
                                                </tr>
                                            %endfor
                                        %endfor
                                        <tr>
                                            <td class="table-cell-expand"></td>
                                            <th colspan="2" class="table-cell-shrink">Suma total</th>
                                            <td class="text-right table-cell-shrink">${formatLang(total_cost, digits=2)} €</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            %endfor
        </div>
    </body>
</html>