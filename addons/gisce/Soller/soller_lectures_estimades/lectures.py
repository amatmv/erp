# -*- coding: utf-8 -*-

from osv import osv, fields
from statistics import mean, pstdev, stdev

from datetime import datetime, timedelta, date
import calendar
from enerdata import contracts
from enerdata.calendars import REECalendar
from tools.translate import _
from collections import OrderedDict
from tools import float_round

class GiscedataLecturesComptador(osv.osv):

    _inherit = 'giscedata.lectures.comptador'

    def create_read_estimate_tg(self, cursor, uid, meter_id,
                                date, context=None):
        '''Creates an estimate read from last meter read'''

        polissa_obj = self.pool.get('giscedata.polissa')
        origin_obj = self.pool.get('giscedata.lectures.origen')
        config_obj = self.pool.get('res.config')
        read_obj = self.pool.get('giscedata.lectures.lectura')

        meter = self.browse(cursor, uid, meter_id)
        polissa = polissa_obj.browse(cursor, uid, meter.polissa.id,
                                     context={'date': date})

        last_read_date = meter.data_ultima_lectura(polissa.tarifa.id)
        if not last_read_date:
            last_read_date = meter.data_alta

        days = (datetime.strptime(date, '%Y-%m-%d') -
                datetime.strptime(last_read_date, '%Y-%m-%d')).days

        consumption, method = polissa_obj.estimate_consumption(
            cursor,
            uid,
            polissa.id,
            polissa.lot_facturacio.id,
            days
        )
        if method == 'historic':
            subcodi = '01'
        else:
            subcodi = '02'
        origin_search = [
            ('codi', '=', '40'),
            ('subcodi', '=', subcodi)
        ]
        origen_id = origin_obj.search(cursor, uid, origin_search)[0]

        last_measures = meter.get_lectures(
            polissa.tarifa.id, last_read_date, tipus='A'
        )

        for period, values in last_measures.items():
            vals = {
                'name': date,
                'origen_id': origen_id,
                'lectura': (
                    values['actual']['lectura'] +
                    consumption[period]
                ),
                'periode': values['actual']['periode'][0],
                'tipus': 'A',
                'comptador': meter.id
            }
            read_obj.create(cursor, uid, vals, context=context)
        # Check if we have to insert a reactiva measure
        tg_insert_reactiva = polissa.reactive_conf

        if tg_insert_reactiva == 'zero':
            last_measures = meter.get_lectures(
                polissa.tarifa.id, last_read_date, tipus='R'
            )

            for period, values in last_measures.items():
                vals = {
                    'name': date,
                    'origen_id': origen_id,
                    'lectura': 0,
                    'periode': values['actual']['periode'][0],
                    'tipus': 'R',
                    'comptador': meter.id
                }
                read_obj.create(cursor, uid, vals, context=context)

        return True

    def hour_change(self, date):
        '''
            Add or substract 1 day for summer and winter hour change.
        '''
        if date.month in (3, 10):
            final_day = calendar.monthrange(date.year,
                                            date.month)[1]
            final_date = datetime(date.year, date.month, final_day)
            while final_date.weekday() != 6:
                final_date -= timedelta(days=1)
            if final_date == date and date.month == 3:
                return -1
            elif final_date == date and date.month == 10:
                return 1

        return 0

    def get_30A_days(self, date_start, date_stop, start_include=False):
        '''
            Get number the working and weekend days between two dates
        '''
        if start_include:
            day = date_start
        else:
            day = date_start + timedelta(days=1)
        working = 0
        weekend = 0
        hour_mod = 0
        ree_calendar = REECalendar()
        while day <= date_stop:
            if ree_calendar.is_working_day(day):
                working += 1
            else:
                weekend += 1

            hour_mod += self.hour_change(day)

            day += timedelta(days=1)

        return working, weekend, hour_mod

    def get_30A_hours(self, ocsum_code, period, date_start, date_stop,
                      start_include=False):
        '''
            Get number of hours between two dates for a period
        '''
        working, weekend, hour_change = self.get_30A_days(date_start,
                                                          date_stop,
                                                          start_include)
        modifier = 0
        if period in ('P6'):
            modifier = hour_change

        hours_period = self.get_hours_period(ocsum_code, period)
        if period in ('P1', 'P2', 'P3'):
            hours = hours_period * working
        else:
            hours = hours_period * weekend + modifier

        return hours

    def get_hours_period(self, ocsum_code, period_code):
        '''
            Get number of hours for a period and rate
        '''
        if  ocsum_code == '003':
            # 3.0A
            rate = contracts.T30A()
        elif ocsum_code == '011':
            # 3.1A
            rate = contracts.T31A()
        else:
            raise osv.except_osv('Error', _('Tarifa distinta de 3.0A o 3.1A'))

        for period in rate.periods:
            if period.code == period_code:
                # Summer hours and winter hours are the same
                return period.total_summer_hours



    def create_read_estimate_tg_type_four(self, cursor, uid, meter_id,
                                          date_end, context=None):
        '''
            Creates an estimate read type 4
        '''
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        read_obj = self.pool.get('giscedata.lectures.lectura')
        rate_obj = self.pool.get('giscedata.polissa.tarifa')
        origin_obj = self.pool.get('giscedata.lectures.origen')
        power_obj = self.pool.get('giscedata.lectures.potencia')

        meter = self.browse(cursor, uid, meter_id)

        search_params = [
            ('data_final', '<', date_end),
            ('state', 'in', ('open', 'paid')),
            ('polissa_id', '=', meter.polissa.id)
        ]

        factura_ids = factura_obj.search(cursor, uid,search_params, limit=4,
                                         order='data_final desc')

        lects = {}
        for factura in factura_obj.browse(cursor, uid, factura_ids):

            ocsum_code = factura.tarifa_acces_id.codi_ocsum

            for lectura in factura.lectures_energia_ids:
                if lectura.tipus != 'activa':
                    read_type = 'R'
                else:
                    read_type = 'A'
                date_start = datetime.strptime(lectura.data_anterior,
                                               '%Y-%m-%d')
                date_stop = datetime.strptime(lectura.data_actual,
                                              '%Y-%m-%d')
                period = lectura.name.split('(')[1][:2]
                lects.setdefault(period, {})
                lects[period].setdefault(read_type, [])
                working, weekend, hour_change = self.get_30A_days(date_start,
                                                                  date_stop)
                hours = self.get_30A_hours(ocsum_code, period, date_start,
                                           date_stop)
                consumption = round(float(lectura.consum) / hours, 3)
                lects[period][read_type].append({
                    'total': lectura.consum,
                    'working': working,
                    'weekend': weekend,
                    'data1': lectura.data_anterior,
                    'data2': lectura.data_actual,
                    'period_hours': self.get_hours_period(ocsum_code, period),
                    'total_hours': hours,
                    'consumption_hour': consumption
                })
        lects = OrderedDict(sorted(lects.items(), key=lambda t: t[0]))

        rate_id = meter.polissa.tarifa.id
        last_read_date = meter.data_ultima_lectura(rate_id)
        if not last_read_date:
            last_read_date = meter.data_alta

        ocsum_code = meter.polissa.tarifa.codi_ocsum
        for period, data1 in lects.iteritems():
            for read_type, data in data1.iteritems():
                measures = [x['consumption_hour'] for x in data]
                amean = mean(measures)
                dev = stdev(measures)

                margin_low = amean - (2 * dev)
                margin_top = amean + (2 * dev)

                final_mean = mean(filter(
                    lambda a: margin_low <= a <= margin_top, measures))
                date_start = datetime.strptime(last_read_date, '%Y-%m-%d')
                date_stop = datetime.strptime(date_end, '%Y-%m-%d')
                hours = self.get_30A_hours(ocsum_code, period, date_start,
                                           date_stop, start_include=True)
                periods = rate_obj.get_periodes(cursor, uid,
                                                meter.polissa.tarifa.id,
                                                tipus='te')
                # origin = historic
                origin_search = [
                    ('codi', '=', '40'),
                    ('subcodi', '=', '01')
                ]
                origin_id = origin_obj.search(cursor, uid, origin_search)[0]

                consumption = float_round(final_mean * hours, 0)
                last_measures = meter.get_lectures(rate_id, last_read_date,
                                                   tipus=read_type)
                read = last_measures[period]['actual']['lectura'] + consumption
                vals = {
                    'name': date_end,
                    'origen_id': origin_id,
                    'lectura': read,
                    'periode': periods[period],
                    'tipus': read_type,
                    'comptador': meter.id
                }
                read_obj.create(cursor, uid, vals, context=context)

            # Create maximeter with value = 0 (Maximeters never estimate)
            periods = rate_obj.get_periodes(cursor, uid,
                                            meter.polissa.tarifa.id,
                                            tipus='tp')
            vals = {
                'name': date_end,
                'lectura': 0,
                'periode': periods[period],
                'comptador': meter.id
            }
            power_obj.create(cursor, uid, vals, context=context)

GiscedataLecturesComptador()
