# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from osv import osv
from osv import fields
from tools.translate import _


class WizardLecturesEstimades(osv.osv_memory):

    _name = 'wizard.lectures.estimades'

    def action_lectures(self, cursor, uid, ids, context=None):

        reparto = {1: {'P1': 1},
                   6: {'P1': 0.17, 'P2': 0.55, 'P3': 0.28,
                        'P4': 0, 'P5': 0, 'P6': 0}}

        lect_act_obj = self.pool.get('giscedata.lectures.lectura')
        lect_pot_obj = self.pool.get('giscedata.lectures.potencia')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        #Totes les lectures tindran origen Estimada
        #que es el codi 40
        origen_obj = self.pool.get('giscedata.lectures.origen')
        search_params = [('codi', '=', '40')]
        origen_id = origen_obj.search(cursor, uid, search_params)[0]

        wizard = self.browse(cursor, uid, ids[0])

        if not wizard.comptador_id.polissa.tarifa:
            raise osv.except_osv(_(u'Error'),
                                 _(u'La pòlissa del comptador no '
                                   u'te tarifa associada'))
        meter = wizard.comptador_id
        # If it is a type 5 PRIME meter, estimate using boe
        if meter.tg_cnc_conn and meter.polissa.potencia < 15:
            meter_obj.create_read_estimate_tg(cursor, uid,
                                              meter.id,
                                              wizard.data_lectura,
                                              context=context)
        elif meter.tg_cnc_conn and meter.polissa.agree_tipus == '04':
            meter_obj.create_read_estimate_tg_type_four(cursor, uid,
                                                        meter.id,
                                                        wizard.data_lectura,
                                                        context=context)
        else:
            num_per = len(wizard.comptador_id.polissa.tarifa.periodes) / 2
            total_hores = wizard.hours * wizard.days
            potencia = wizard.comptador_id.polissa.potencia
            comptador_id = wizard.comptador_id.id
            for periode in wizard.comptador_id.polissa.tarifa.periodes:
                if periode.tipus == 'tp':
                    #Lectura de maximetre igual que la potencia
                    #per cobrar tota la potencia contractada
                    #en el cas de facturar per maximetre
                    vals = {'name': wizard.data_lectura,
                            'periode': periode.id,
                            'comptador': comptador_id,
                            'observacions': u'Estimació per hores d\'ús',
                            'origen_id': origen_id,
                            'lectura': potencia,
                            'exces': 0,
                            }
                    lect_pot_obj.create(cursor, uid, vals)
                else:
                    #Per cada periode de energia creem
                    #una lectura de activa i una de reactiva
                    consum = round(potencia *
                                   reparto[num_per][periode.name] *
                                   total_hores)
                    vals_activa = {
                            'name': wizard.data_lectura,
                            'periode': periode.id,
                            'tipus': 'A',
                            'comptador': comptador_id,
                            'observacions': u'Estimació per hores d\'ús',
                            'origen_id': origen_id,
                            'lectura': consum,
                            }
                    lect_act_obj.create(cursor, uid, vals_activa)
                    vals_reactiva = {
                            'name': wizard.data_lectura,
                            'periode': periode.id,
                            'tipus': 'R',
                            'comptador': comptador_id,
                            'observacions': u'Estimació per hores d\'ús',
                            'origen_id': origen_id,
                            'lectura': 0,
                            }
                    lect_act_obj.create(cursor, uid, vals_reactiva)

        wizard.write({'state': 'end'})

    _columns = {
        'comptador_id': fields.many2one('giscedata.lectures.comptador',
                                        'Comptador', required=True),
        'data_lectura': fields.date('Data de la lectura', required=True,),
        'days': fields.integer('Dies', required=False),
        'hours': fields.integer('Hores per dia', required=False),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')],
                                  'Estat'),
    }

    _defaults = {
        'days': lambda *a: 1,
        'hours': lambda *a: 8,
        'state': lambda *a: 'init',
    }

WizardLecturesEstimades()
