# -*- coding: utf-8 -*-
{
    "name": "Lectures estimades",
    "description": """
Càlcul de lectures per hores segons la potència contractada
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_lectures_telegestio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_lectures_estimades_view.xml",
        "giscedata_lectures_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
