# -*- coding: utf-8 -*-
{
    "name": "Reports Soller Operador",
    "description": """
  Módulo con los reports para el control de un operador de internet:
    * Contratos
    * Facturas
      """,
    "version": "0-dev",
    "author": "Toni Llado",
    "category": "Soller",
    "depends":[
        "soller_operador",
        "phantom_reports",
        "c2c_webkit_report"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_operador_report.xml"
    ],
    "active": False,
    "installable": True
}
