<%

from tools import config
from itertools import groupby
import ast
from bankbarcode.cuaderno57 import Recibo507
from datetime import datetime, timedelta

addons_path = config['addons_path']
cursor = objects[0]._cr
uid = user.id
pool = objects[0].pool
address_obj = pool.get('res.partner.address')
contrato_obj = pool.get('soller.operador.contrato')
account_obj = pool.get('res.partner.bank')
call_obj = pool.get('soller.operador.cdr')
data_obj = pool.get('soller.operador.cdr.data')
product_obj = pool.get('product.product')

#Company data
company = objects[0].company_id.partner_id
company_addresses = company.address_get(adr_pref=['contact'])
if 'contact' in company_addresses:
    company_address = address_obj.read(cursor, uid, company_addresses['contact'])
else:
    company_address = {}
#Company account
search_params = [('partner_id', '=', company.id),
                 ('default_bank', '=', True)]
company_account_id = account_obj.search(cursor, uid, search_params)[0]
company_account = account_obj.browse(cursor, uid, company_account_id)

%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>

         <link rel="stylesheet" type="text/css" href="${addons_path}/soller_operador_report/report/static/css/print.css"/>

</head>
<body>
%for factura in objects:
<%
setLang(factura.lang_partner)

# Look for invoice number depending on factura state
invoice_number = '<b>%s:</b> %s' % (_('Número'), factura.number)
if factura.state == 'proforma2':
    invoice_number = '<b>%s</b>' % _('FACTURA PROFORMA')
elif factura.state == 'draft':
    invoice_number = '<b>%s</b>' % _('FACTURA BORRADOR')

search_params = [('name', '=', factura.origin)]
contrato_id = contrato_obj.search(cursor, uid, search_params,
                                   context={'active_test': False})[0]
contrato = contrato_obj.browse(cursor, uid, contrato_id)

# Text for duplicates, proformas and refunds
extra_text = []
if factura.type == 'out_refund':
    extra_text.append(_('ABONAMENT'))
if factura.state == 'proforma2':
    extra_text.append(_('PROFORMA'))
if duplicate:
    extra_text.append(_('DUPLICAT'))
extra_text_invoice = ' - '.join(extra_text)

# Professional
profesional = False
for line in contrato.lineas_ids:
    if line.product_id.variants == 'PROFESIONAL':
        profesional = True
        if line.detall:
            ip_address = line.detall
        
# Phone details
phone_calls = {}
phone_totals = {}
data_consumption = {}
#Out of rate calls product
out_of_rate_ids = product_obj.search(cursor, uid, [('default_code', 'in', ('LLAM', 'EXTLLAMMOV'))])

for line in factura.invoice_line:
    if (line.product_id.categ_id.code in ('TLF', 'MOV') and line.origin
    and line.product_id.id not in out_of_rate_ids):
        values = ast.literal_eval(line.origin)
        # Search for calls
        datetime_from = values['date_from'] + ' 00:00:00'
        datetime_to = values['date_to'] + ' 23:59:59'
        search_params = [
                         ('contract_id', '=', contrato_id),
                         ('cdr_number', '=', values['phone']),
                         ('timestamp', '>=', datetime_from),
                         ('timestamp', '<=', datetime_to),
                         ]
        call_ids = call_obj.search(cursor, uid, search_params)
        field_list = ['origin', 'destination', 'description',
                      'timestamp', 'duration', 'invoice_price', 'call_type']
        calls = call_obj.read(cursor, uid, call_ids, field_list)
        phone_calls.setdefault(values['phone'], [])
        phone_calls[values['phone']].extend(calls)
        phone_totals.setdefault(values['phone'], {})

        total_price = phone_totals[values['phone']].get('price', 0)
        total_calls = phone_totals[values['phone']].get('number', 0)
        total_duration = phone_totals[values['phone']].get('time', 0)
        for call in calls:
            if call['destination'] == '016':
                continue # hide 016 calls from invoice
            total_price += call['invoice_price']
            total_duration += call['duration']
        phone_totals[values['phone']]['price'] = total_price
        phone_totals[values['phone']]['time'] = total_duration
        phone_totals[values['phone']]['number'] = total_calls + len(calls)

        #Search for data consumption
        search_params = [
                         ('contract_id', '=', contrato_id),
                         ('cdr_number', '=', values['phone']),
                         ('timestamp', '>=', datetime_from),
                         ('timestamp', '<=', datetime_to),
                         ]
        data_ids = data_obj.search(cursor, uid, search_params)
        year, month, day = values['date_from'].split('-')
        if data_ids:
            field_list = ['description', 'timestamp', 'consumption', 'invoice_price']
            datas = data_obj.read(cursor, uid, data_ids, field_list)
            # Sum all data found by type
            for key, group_values in groupby(datas, lambda a: a['description']):
            	all_vals = list(group_values)
            	total_data = sum([x['consumption'] for x in all_vals])
            	total_price = sum([x['invoice_price'] for x in all_vals])
            	data_consumption.setdefault(values['phone'], [])
            	data_consumption[values['phone']].append({'description': key,
            										      'consumption': total_data,
            										      'invoice_price': total_price,
            										      'timestamp': '{}/{}'.format(month, year)})
            	

for phone_id in phone_totals:
    total_duration = phone_totals[phone_id]['time']
    hours = total_duration / 3600
    minutes = ((total_duration % 3600) / 60)
    seconds = ((total_duration % 3600) % 60)
    phone_totals[phone_id]['time'] = '%sh %sm %ss' % (hours, minutes, seconds)

if profesional:
    contact_phone = '900 494 182'
    timetable = _('De dilluns a diumenge de 7 a 21 hores')
else:
    contact_phone = '900 373 417'
    timetable = _('De dilluns a divendres no festius de 7 a 14 hores')

# Bank barcode
ref = '{}'.format(factura.id).zfill(11) # invoice id
entity = ''.join([c for c in company.vat if c.isdigit()])
suffix = '002' # code for operator invoice
notice = '000000'
amount = '{0:.2f}'.format(factura.amount_total) # invoice amount
recibo507 = Recibo507(entity, suffix, ref, notice, amount)
%>
<div class="container">
    <div class="row top-padding-fix">
        <div class="col-xs-5">    
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Dades de l\'emisor')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <p><b><dfn>${company.name.upper()}</dfn></b> (${company.vat.upper()})</p>
                        <p>${'%s, %s - %s' % (company_address['street'], company_address['zip'], company_address['city'].upper())}</p>
                        <p>${'%s - %s' %(company_address['state_id'][1].upper(), company_address['country_id'][1].upper())}</p>
                        <p>${_('Telèfon')} ${company_address['phone']}, ${_('Fax')} ${company_address['fax']}</p>
                        <p>${company.website}</p>
                        <p>${company_address['email']}</p>
                        <p style="font-size: 8px;">${factura.company_id.rml_footer1}</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Dades de factura')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <p>${invoice_number}</p>
                        % if factura.type == 'out_refund':
                            <p>${_('Abonament de la factura')}: ${factura.rectifying_id.number}</p>
                        % elif factura.rectificative_type == 'R':
                            <p>${_('Rectificativa de la factura')}: ${factura.rectifying_id.number}</p>
                        % endif
                        <p><b>${_('Data')}:</b> ${formatLang(factura.date_invoice.val, date=True)}</p>
                        <p><b>${_('NIF/CIF')}:</b> ${factura.partner_id.vat}</p>
                        <p><b>${_('Forma de pagament')}:</b> ${factura.payment_type.name}</p>
                        % if factura.payment_type.code == 'TRANSFERENCIA_CSB':
                        <p><b>IBAN:</b> ${company_account.printable_iban}</p>
                        % elif factura.payment_type.code == 'RECIBO_CSB':
                        <p><b>IBAN:</b> ${factura.partner_bank.printable_iban[:-4] + '****'}</p>
                        % endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-7">
            <div class="col-xs-12" style="min-height: 148px;">
                <img style="width: 558px; float:right;" src="${addons_path}/soller_operador_report/report/cabecera-facturas-nueva.png"/>
            </div>
            <div class="col-xs-12">
                <div class="invoice-address">
                        <p>${factura.partner_id.name.upper()}</p>
                        <p>${factura.address_invoice_id.street.upper()}</p>
                        % if factura.address_invoice_id.zip:
                        <p>${'%s - %s' % (factura.address_invoice_id.zip, factura.address_invoice_id.city and factura.address_invoice_id.city.upper() or '')}</p>
                        % else:
                        <p>${factura.address_invoice_id.city.upper()}</p>
                        % endif
                        %if factura.address_invoice_id.state_id and factura.address_invoice_id.country_id:
                        <p>${'%s - %s' % (factura.address_invoice_id.state_id.name.upper(), factura.address_invoice_id.country_id.name.upper())}</p>
                        % elif factura.address_invoice_id.state_id:
                        <p>${factura.address_invoice_id.state_id.name.upper()}</p>
                        % elif factura.address_invoice_id.country_id:
                        <p>${factura.address_invoice_id.country_id.name.upper()}</p>
                        % endif
                </div>
            </div>
        </div>
        % if extra_text:
            <div class="col-xs-12"><p class="duplicado">${extra_text_invoice}</p></div>
        % endif
        %if (factura.payment_type.code == 'CAJA' or factura.estat_impagat != 'correcte') and factura.state in ('open', 'paid'):
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Reservado para la oficina de cobros</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <div class="col-xs-6 ">
                        ${recibo507.svg(writer_options={'module_height': 10, 'font_size': 7, 'text_distance': 4, 'module_width': 0.4})}
                        </div>
                        <div class="col-xs-6">
                            <table style="width: 100%" align="center">
                                <tr>
                                    <td><b>Emisora:</b></td>
                                    <td><b>Referencia:</b></td>
                                    <td><b>Identificación:</b></td>
                                </tr>
                                <tr>
                                    <td>${'{} {}'.format(entity, suffix)}</td>
                                    <td>${'{}{}'.format(ref, recibo507.checksum())}</td>
                                    <td>${notice}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        % endif
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Dades del contracte')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <div class="col-xs-6">
                        <p><b>${_('Titular')}:</b> ${contrato.titular_id.name}</p>
                        <p><b>${_('NIF/CIF Titular')}:</b> ${contrato.nif_titular}</p>
                        <p><b>${_('Adreça')}:</b> ${contrato.direccion_titular_id.street}</p>
                        <p><b>${_('Població')}:</b> ${contrato.direccion_titular_id.city.upper()}</p>
                    </div>
                    <div class="col-xs-6">
                        <p><b>${_('Contracte')}:</b> ${contrato.name}</p>
                        <p><b>${_('Telèfon avaries')}:</b> ${contact_phone}</p>
                        <p><b>${_('Horari')}:</b> ${timetable}</p>
                        % if profesional:
                            <p><b>${_('Adreça IP')}:</b> ${ip_address}</p>
                        % endif
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Conceptes')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <table>
                        % for line in factura.invoice_line:
                        <tr>
                            <td>${line.name}</td>
                            <td>${formatLang(line.quantity, digits=3)}</td>
                            <td class="amount currency">${formatLang(line.price_unit, digits=2)}</td>
                            <td class="amount currency">${formatLang(line.price_subtotal, digits=2)}</td>
                        </tr>
                        % endfor    
                    </table>
                    <br/>
                    <table style="width: 30%;float: right;">
                        % for tax in factura.tax_line:
                        <tr>
                            <td class="amount">${_('%s sobre %s%s') % (tax.name, formatLang(tax.base, digits=2), '&euro;')}</td>
                            <td class="amount currency">${formatLang(tax.amount, digits=2)}</td>
                        </tr>
                        % endfor
                    </table>
                </div>
                <div class="panel-footer">
                    <table style="width: 100%;">
                        <tr>
                            <td><h4>${_('Import total')}</h4></td>
                            % if factura.type == 'out_refund':
                                <td class="amount currency"><b>${formatLang(factura.amount_total * -1, digits=2)}</b></td>
                            % else:
                                <td class="amount currency"><b>${formatLang(factura.amount_total, digits=2)}</b></td>
                            % endif    
                        <tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    % if data_consumption:

        <div class="row top-padding-fix">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Detall de dades')}</h4>
                    </div>
                     <div class="panel-body invoice-data">
                        <table>
                            % for phone_number, datas in data_consumption.iteritems():
                                <tr>
                                    <td colspan="5">${_('Consum de dades des del número %s') % phone_number}</td>
                                </tr>
                                <tr>
                                    <th style="text-align: left;">${_('Descripció')}</th>
                                    <th style="text-align: left;">${_('Data')}</th>
                                    <th>${_('Consum (MB)')}</th>
                                    <th class="amount">${_('Import')}</th>
                                <tr>
                                % for data in datas:
                                    <tr>
                                        <td>${data['description']}</td>
                                        <td>${data['timestamp']}</td>
                                        <td style="text-align: center;">${data['consumption']}</td>
                                        <td class="amount currency">${formatLang(data['invoice_price'], digits=4)}</td>
                                    </tr>
                                % endfor
                            % endfor
                        </table>
                    </div>
                </div>
            </div>
        </div>
    % endif

    % if phone_calls:
    <p class="page-break"></p>
     <%
        index = 1
        max_calls = 66
    %>
    <div class="row top-padding-fix">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Detall de cridades')}</h4>
                </div>
                 <div class="panel-body invoice-data">
                    <table>
                        % for phone_number, calls in phone_calls.iteritems():
                        <tr>
                            <td colspan="6">${_('Cridades realitzades des del número %s') % phone_number}</td>
                        </tr>
                        <tr>
                            <th style="text-align: left;">${_('Origen')}</th>
                            <th style="text-align: left;">${_('Destí')}</th>
                            <th style="text-align: left;">${_('Descripció')}</th>
                            <th style="text-align: left;">${_('Inici')}</th>
                            <th class="amount">${_('Durada (s)')}</th>
                            <th class="amount">${_('Import')}</th>
                        <tr>
                        % for call in calls:
                            % if call['destination'] == '016':
                                <% continue %> ## hide 016 calls from invoice
                            % endif
                            <% description =  call['description']%>
                            % if call['call_type'] == 'sms':
                                <% description += ' (SMS)' %>
                            % endif
                            <tr>
                                <td>${call['origin']}</td>
                                <td>${call['destination']}</td>
                                <td>${description}</td>
                                <td>${formatLang(call['timestamp'], date_time=True)}</td>
                                <td class="amount">${call['duration']}</td>
                                <td class="amount currency">${formatLang(call['invoice_price'], digits=4)}</td>
                            </tr>

                            ## Insert a page-break
                            % if index % max_calls == 0:
                                </table>
                                </div></div></div></div>
                                <p class="page-break"></p>
                                 <div class="row top-padding-fix">
                                    <div class="col-xs-12">
                                        <div class="panel panel-default">
                                             <div class="panel-body invoice-data">
                                                <table>
                            % endif
                            <% index = index +1 %>
                        % endfor
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>${_('Total de cridades')} ${phone_totals[phone_number]['number']}</td>
                            <td class="amount">${phone_totals[phone_number]['time']}</td>
                            <td class="amount currency">${formatLang(phone_totals[phone_number]['price'], digits=2)}</td>
                        </tr>
                        % endfor
                    </table>
                </div>     
            </div>
        </div>
    </div>
    % endif

</div>
% if objects.index(factura) + 1 != len(objects):
    <p class="page-break"></p>
% endif
%endfor
</body>
</html>
