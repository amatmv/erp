# coding=utf-8

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools.translate import _
from osv import osv

import netsvc
import pooler


class SollerOperadorContratoWebkitParser(webkit_report.WebKitParser):

    def create_single_pdf(self, cursor, uid, ids, data, report_xml,
                          context=None):
        pool = pooler.get_pool(cursor.dbname)
        contract_obj = pool.get('soller.operador.contrato')
        contract_data = contract_obj.read(cursor, uid, ids, ['state'])
        any_draft = [contract['state'] == 'esborrany'
                     for contract in contract_data]

        if any(any_draft):
            raise osv.except_osv('Error',
                                 _(u'No es pot imprimir un contracte quan '
                                   u'està en esborrany'))

        return super(SollerOperadorContratoWebkitParser,
                     self).create_single_pdf(cursor, uid, ids, data, report_xml,
                                             context=context)


SollerOperadorContratoWebkitParser(
    'report.report_contrato_operador_mako',
    'soller.operador.contrato',
    'soller_operador_report/report/contrato.mako',
    parser=report_sxw.rml_parse
)
