# -*- coding: utf-8 -*-

import jasper_reports
import pooler
from phantom_reports import phantom_report


def informes_operador(cr, uid, ids, data, context):

    return {
        'ids': ids,
        'parameters': {'REPORT_LOCALE': 'es_ES'}
    }


def informes_facturacio_operador(cr, uid, ids, data, context):
    return {
        'parameters': {
            'REPORT_LOCALE': 'es_ES',
            'date_from': data['date_from'],
            'date_to': data['date_to'],
        }
    }


def _get_phantom_report_factura_operador(cr, uid, ids, data, context):

    pool = pooler.get_pool(cr.dbname)
    factura_obj = pool.get('account.invoice')
    factura_vals = factura_obj.read(cr, uid, ids, ['date_invoice'])
    res = {}
    for factura_val in factura_vals:
        date = factura_val['date_invoice']
        if date < '2014-10-01':
            report_name = 'account.invoice.generic'
        else:
            report_name = 'report_factura_operador_webkit'
        res.setdefault(report_name, [])
        res[report_name].append(factura_val['id'])
    return res


jasper_reports.report_jasper(
   'report.report_contrato_operador',
   'soller.operador.contrato',
   parser=informes_operador
)

jasper_reports.report_jasper(
   'report.report_contrato_operador_ficha',
   'soller.operador.contrato',
   parser=informes_operador
)

jasper_reports.report_jasper(
   'report.report_contrato_operador_lista',
   'soller.operador.contrato',
   parser=informes_operador
)

jasper_reports.report_jasper(
   'report.report_facturacio_operador_producte',
   'account.invoice',
   parser=informes_facturacio_operador
)

jasper_reports.report_jasper(
   'report.report_facturacio_operador_categoria_producte',
   'account.invoice',
   parser=informes_facturacio_operador
)

phantom_report.phantom_report(
   'report.report_phantom_factura_operador',
   parser=_get_phantom_report_factura_operador
)
