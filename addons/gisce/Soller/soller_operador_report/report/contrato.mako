<%

from tools import config
from datetime import datetime
addons_path = config['addons_path']
cursor = objects[0]._cr
uid = user.id
pool = objects[0].pool
address_obj = pool.get('res.partner.address')
account_obj = pool.get('res.partner.bank')
linea_obj = pool.get('soller.operador.contrato.linea')

#Company data
company = user.company_id.partner_id
company_addresses = company.address_get(adr_pref=['contact'])
if 'contact' in company_addresses:
    company_address = address_obj.read(cursor, uid, company_addresses['contact'])
else:
    company_address = {}
#Company account
search_params = [('partner_id', '=', company.id),
                 ('default_bank', '=', True)]
company_account_id = account_obj.search(cursor, uid, search_params)[0]
company_account = account_obj.browse(cursor, uid, company_account_id)

def get_city(address):
    city = address.city
    if address.id_municipi and address.id_poblacio:
        city = '(%s) %s' % (address.id_poblacio.name,
                            address.id_municipi.name)
    elif address.id_poblacio:
        city = address.id_poblacio.name

    return city.upper()


%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
         <link rel="stylesheet" type="text/css" href="${addons_path}/soller_operador_report/report/static/css/print.css"/>
</head>
<body>
%for contract in objects:
<%
setLang(contract.pagador_id.lang)

dir_titular = contract.direccion_titular_id
dir_pagador = contract.direccion_factura_id

lineas = [linea for linea in contract.lineas_ids if linea.active]

%>
    <div id="wrap">
        <div class="container">
            <div class="row top-padding-fix">
                <div class="col-xs-5">
                   <div class="panel panel-default">
                       <div class="panel-heading">
                           <h4>${_('Dades d\'empresa')}</h4>
                       </div>
                       <div class="panel-body invoice-data">
                           <p><b><dfn>${company.name.upper()}</dfn></b> (${company.vat.upper()})</p>
                           <p>${'%s, %s - %s' % (company_address['street'], company_address['zip'], company_address['city'].upper())}</p>
                           <p>${'%s - %s' %(company_address['state_id'][1].upper(), company_address['country_id'][1].upper())}</p>
                           <p>${_('Telèfon')} ${company_address['phone']}, ${_('Fax')} ${company_address['fax']}</p>
                           <p>${company.website}</p>
                           <p>${company_address['email']}</p>
                           <p style="font-size: 8px;">${user.company_id.rml_footer1}</p>
                       </div>
                   </div>
                </div>
                <div class="col-xs-7" style="min-height: 148px;">
                    <img style="width: 558px; float:right;" src="${addons_path}/soller_operador_report/report/cabecera-facturas-nueva.png"/>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="contract-header">${_('CONTRACTE')}</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <table>
                                <tr>
                                    <td><b>${_('Referència del contracte')}</b>: ${contract.name}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>${_('Dades d\'instal·lació')}</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <p><b>${_('Nom/Raó')}:</b> ${contract.titular_id.name}</p>
                            <p><b>${_('NIF/CIF')}:</b> ${contract.titular_id.vat}</p>
                            <p><b>${_('Adreça')}:</b> ${dir_titular.street}</p>
                            <p><b>${_('Codi postal')}:</b> ${dir_titular.zip}</p>
                            <p><b>${_('Població')}:</b> ${get_city(dir_titular)}</p>
                            <p><b>${_('Província')}:</b> ${dir_titular.state_id and dir_titular.state_id.name.upper() or ''}</p>
                            <p><b>${_('Pais')}:</b> ${dir_titular.country_id and dir_titular.country_id.name.upper() or ''}</p>
                            <p><b>${_('Telèfon')}:</b> ${dir_titular.phone or ''}</p>
                            <p><b>${_('Mòbil')}:</b> ${dir_titular.mobile or ''}</p>
                            <p><b>${_('E-mail')}:</b> ${dir_titular.email or ''}</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>${_('Dades del pagador')}</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <p><b>${_('Pagador')}:</b> ${contract.pagador_id.name}</p>
                            <p><b>${_('NIF/CIF')}:</b> ${contract.pagador_id.vat}</p>
                            <p><b>${_('Adreça')}:</b> ${dir_pagador.street}</p>
                            <p><b>${_('Codi postal')}:</b> ${dir_pagador.zip}</p>
                            <p><b>${_('Població')}:</b> ${get_city(dir_pagador)}</p>
                            <p><b>${_('Província')}:</b> ${dir_pagador.state_id and dir_pagador.state_id.name.upper() or ''}</p>
                            <p><b>${_('Pais')}:</b> ${dir_pagador.country_id and dir_pagador.country_id.name.upper() or ''}</p>
                            <p><b>${_('Telèfon')}:</b> ${dir_pagador.phone or ''}</p>
                            <p><b>${_('Mòbil')}:</b> ${dir_pagador.mobile or ''}</p>
                            <p><b>${_('E-mail')}:</b> ${dir_pagador.email or ''}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>${_('Dades de pagament')}</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            % if contract.tipo_pago_id.code == 'CAJA':
                                <p><b>${_('Forma de pagament')}:</b> ${'%s (%s)' % (_('Oficines'), contract.payment_mode_id.name)}</p>
                            % elif contract.tipo_pago_id.code == 'RECIBO_CSB':
                                <table>
                                    <tr>
                                        <td><b>${_('Forma de pagament')}:</b> ${_('Rebut domiciliat')}</td>
                                        <td><b>${_('IBAN')}:</b> ${contract.bank_id.printable_iban}</td>
                                    </tr>
                                    <tr>
                                        <td><b>${_("Titular de l'IBAN")}:</b> ${contract.pagador_id.name}</td>
                                        <td><b>${_('NIF/CIF')}:</b> ${contract.pagador_id.vat}</td>
                                    </tr>
                                </table>
                            % elif contract.tipo_pago_id.code == 'TRANSFERENCIA_CSB':
                                <table>
                                    <tr>
                                        <td><b>${_('Forma de pagament')}:</b> ${_('Transferència')}</td>
                                        <td><b>${_('IBAN')}:</b> ${company_account.printable_iban}</td>
                                    </tr>
                                </table>
                            % endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>${_('Productes contractats')}</h4>
                        </div>
                        <div>
                            <table width="100%" class="panel-body">
                                <tr>
                                    <td><b>${_('Descripció')}</b></td>
                                    <td style="text-align: center"><b>${_('Quantitat')}</b></td>
                                    <td style="text-align: center"><b>${_('Quota mensual')}</b></td>
                                    %if contract.multitarifa_id.id != 0:
                                        <td style="text-align: center"><b>${_('Quota reduïda')}</b></td>
                                    %endif
                                </tr>
                                %for linea in lineas:
                                    <tr>
                                        <td>
                                            %if linea.detall:
                                                <p>${'{} - {}'.format(linea.product_id.name, linea.detall)}</p>
                                            %else:
                                                <p>${linea.product_id.name}</p>
                                            %endif
                                        </td>
                                        <td style="text-align: center">${linea.quantitat}</td>
                                        %if contract.multitarifa_id.id != 0:
                                            <td style="text-align: center" class="amount currency">${formatLang(linea.product_price)}</td>
                                            <td style="text-align: center" class="amount currency">${formatLang(linea.multiproduct_price)}</td>
                                        %else:
                                            <td style="text-align: center" class="amount currency">${formatLang(linea.product_price)}</td>
                                        %endif
                                    </tr>
                                %endfor
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>${_('Condicions particulars')}</h4>
                        </div>
                        <div>
                            <div class="panel-body invoice-data">
                                <p><b>${_('Nivell de servei')}: </b>${'{} {}'.format(contract.sla, (_('hores')))}</p>
                                %if contract.condicions_especials:
                                    <p><b>${_('Condicions')}: </b><br/>${contract.text_condicions.replace('\n','<br/>')}</p>
                                %endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="container">
            <div class="row">
                <table style="width: 100%;">
                    <tr>
                        <td></td>
                        <td style="text-align: center;">Sóller a ${formatLang(contract.data_firma_contracte, date=True)}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="text-align: center;"><img style="width: 180px;" src="${addons_path}/soller_operador_report/report/firma_afb.jpg"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">${_('Firma del client')}</td>
                        <td style="text-align: center;">${company.name}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <p class="page-break"></p>
    <div class="container">
        <div class="row top-padding-fix" style="padding-left: 20px;">
            <div class="col-xs-6">
                <p class="conditions"><b>CONDICIONES GENERALES</b></p>
                <p class="conditions"><b>1. PRODUCTOS / SERVICIOS</b></p>
                <p class="conditions"><b>1.1 Descripción de nuestros productos</b></p>
                <p class="conditions">ELECTRICA SOLLERENSE, S.A.U. (en este contrato, “nosotros” o  E.S.) prestará al cliente (en este contrato, “Usted”) un servicio de conexión a Internet y de telefonía fija (éste último, de carácter opcional).</p>
                <p class="conditions"><b>1.2 Condiciones aplicables</b></p>
                <p class="conditions"><b>1.2.1 Requisitos técnicos para la prestación del servicio</b></p>
                <p class="conditions">Deben cumplirse los requisitos técnicos que E.S. le comunique, y que se refieren entre otros a la compatibilidad, los sistemas operativos, la capacidad de memoria, los navegadores instalados, la resolución de pantalla, la necesidad de disponer de una copia del sistema operativo, etc…, los cuales deberán ser verificados por Usted. En cualquier caso, E.S. le comunicará las limitaciones existentes así como los requisitos específicos.</p>
                <p class="conditions">En caso de detectarse, con carácter previo a la instalación de los equipos o la activación del servicio, incompatibilidades o incumplimientos de los requisitos técnicos referidos anteriormente, E.S. podrá rechazar la solicitud y denegar la prestación del servicio, en tanto no se resuelvan dichas incompatibilidades o incumplimientos..</p>
                <p class="conditions"><b>1.2.2 Utilización de los productos / servicios</b></p>
                <p class="conditions">Los productos contratados no incluyen la posibilidad de su reventa o utilización para un terminal público, ni su utilización por cuenta o en beneficio de terceros ya sea mediante cualquier tipo de contraprestación económica o gratuidad. Usted será responsable del buen uso y utilización de los productos contratados únicamente para su uso personal y particular, sin poder cederlos sin consentimiento expreso de E.S., y comprometiéndose a controlar el acceso al mismo; a título de ejemplo y en relación con el servicio de Internet, se entenderá que Usted incurre en esta circunstancia si lleva a cabo, para un uso distinto del personal, entre otras, las actividades de establecimiento y mantenimiento de servidores para correo electrónico, http, ftp, telnet, intercambio de ficheros o IRC. Usted se compromete a utilizar los productos de acuerdo a la buena fe y a las prácticas comúnmente aceptadas como de correcta utilización de los mismos, y en ningún momento podrá vulnerar la legislación vigente, entre la que destaca aquella establecida para proteger la propiedad industrial o intelectual, los datos de carácter personal, la seguridad, el derecho al honor, a la intimidad personal y familiar y a la propia imagen, o los servicios de la sociedad de información y el comercio electrónico; pudiendo E.S., en estos casos, dar por terminado el presente contrato.</p>
                <p class="conditions">En particular, Usted no deberá utilizar el servicio de Internet para enviar correos no solicitados o masivos (“spam”).</p>
                <p class="conditions">En caso de que Usted incumpla la limitación de uso prevista en el párrafo anterior u otras de carácter legal, E.S., previa notificación, podrá bloquearle el servicio.</p>
                <p class="conditions">El servicio se le ofrece como usuario final y según las reglas del uso razonable y con sentido común. Como tal, no está autorizado a realizar reventa del tráfico telefónico, comercialización por cualquier medio del tráfico telefónico y comunicación con números dedicados al encaminamiento de llamadas. En caso contrario, E.S. se reserva la facultad de resolver el contrato.</p>
                <p class="conditions"><b>1.2.3 Inicio de uso del servicio telefónico</b></p>
                <p class="conditions">Formalizado el contrato, la línea telefónica asignada por E.S. estará a su disposición des del momento en que se produzca la instalación del servicio.</p>
                <p class="conditions">En el caso de portabilidad, le informaremos mediante un correo electrónico de la fecha prevista de activación del servicio.</p>
                <p class="conditions"><b>2. TARIFAS Y MODIFICACIÓN DEL CONTRATO</b></p>
                <p class="conditions">De conformidad con la legislación vigente E.S. podrá modificar las condiciones contractuales aplicables a este contrato en el supuesto de que se produzcan cambios normativos o tecnológicos o cambios en los servicios contratados y/o en las tarifas aplicables a los mismos. Para ello, le comunicaremos mediante cualquier medio válido en derecho, entre los que se contempla su publicación en la página web de E.S., con una antelación mínima de un mes, cualquier modificación en las tarifas, servicios u otros aspectos del mismo. Si no está de acuerdo con las modificaciones propuestas Usted podrá dar por terminado este contrato, sin penalización alguna, mediante comunicación previa, con una antelación mínima de dos días, tal y como aparece en el apartado “Duración y extinción del contrato”. Si, por el contrario, Usted no manifiesta su voluntad de terminar el contrato, se entenderá que Usted opta por mantener su vigencia con las modificaciones notificadas (en particular, con los nuevos precios). En cualquier caso, Usted siempre dispondrá de información actualizada de nuestras tarifas, servicios y condiciones contractuales, visitando nuestra página web, o a través de correo electrónico.</p>
                <p class="conditions"><b>3. EQUIPOS</b></p>
                <p class="conditions">E.S. le detallará los equipos a instalar en su domicilio, requeridos para la prestación del servicio. Al producirse la extinción del contrato, cualquiera que fuera su causa, Usted deberá devolvernos en buen estado y antes de treinta días, los equipos que hayan sido provistos por E.S. y cuya devolución le solicitemos.</p>
                <p class="conditions"><b>4. SERVICIOS ADICIONALES SOBRE LOS PRODUCTOS CONTRATADOS</b></p>
                <p class="conditions"><b>4.1 Instalaciones y mantenimiento</b></p>
                <p class="conditions">La entrega de los equipos se llevará a cabo en el plazo máximo de 30 días a contar desde la contratación del servicio, pudiendo realizarse la activación por Usted en cualquier momento.</p>
                <p class="conditions">Las reclamaciones derivadas de la instalación que realicemos deberá realizarlas en un plazo de treinta días desde la fecha de realización de dicha instalación.</p>
                <p class="conditions">Usted deberá permitir la entrada a su domicilio de nuestro personal técnico o de otra empresa subcontratada por E.S., con objeto de realizar los trabajos de conexión, desconexión o mantenimiento de la Red, así como para realizar altas, bajas, modificaciones, inspecciones de la instalación, operaciones de mantenimiento, localización o reparación de averías, desmontaje o retirada de equipos propiedad de E.S., y en cualquier caso que sea necesario.</p>
                <p class="conditions"><b>4.2 Asistencia técnica</b></p>
                <p class="conditions">Usted podrá  solicitar asistencia técnica si detecta cualquier anomalía o deficiencia llamando al teléfono de atención al cliente 900373417, mediante correo electrónico a atencionalcliente@electricasollerense.es o utilizando el formulario de contacto que puede encontrar en www.electricasollerense.es/contacto</p>
                <p class="conditions"><b>4.3 Servicio de Atención al Cliente y oficinas comerciales</b></p>
                <p class="conditions">Usted podrá dirigir cualquier reclamación relacionada con los productos contratados a nuestro Servicio de Atención al Cliente. Por teléfono en el 900373417, mediante correo electrónico a atencionalcliente@electricasollerense.es o utilizando el formulario de contacto que puede encontrar en www.electricasollerense.es/contacto</p>
                <p class="conditions">Cuando presente una reclamación, queja o realice cualquier gestión con incidencia contractual, le comunicaremos el número de referencia asignado a la misma. Si no recibe una respuesta satisfactoria Usted podrá dirigirse, en el plazo de tres meses desde nuestra respuesta o la finalización del plazo para responder, a la Secretaría de Estado de Telecomunicaciones y para la Sociedad de la Información, quien deberá dictar una resolución en el plazo de seis meses sobre la cuestión planteada.</p>
                <p class="conditions"><b>5. COMPROMISOS Y GARANTÍAS</b></p>
                <p class="conditions"><b>5.1 Compromisos y garantías asumidos por E.S.</b></p>
                <p class="conditions">Cualquier solicitud suya derivada de una avería será atendida según el nivel de servicio contratado.</p>
                <p class="conditions">En el caso de que los tiempos de interrupción del servicio superen el nivel de servicio contratado en el periodo de facturación, se abonará la cuota mensual.</p>
                <p class="conditions">Dadas las características del entorno Internet, no podemos garantizarle en todo momento la velocidad máxima que pueda haber contratado. Asimismo, no somos responsables del contenido de cualesquiera informaciones accesibles a través de los servicios de Internet, ni de la fiabilidad de las bases de datos disponibles por este medio, la idoneidad de los cuestionarios o métodos de búsqueda de información, ni la adecuación de los servicios de Internet, o sus contenidos, a sus expectativas o necesidades. Tampoco asumiremos responsabilidad alguna en relación con las consecuencias que para Usted puedan derivarse del uso de los servicios de Internet, ya sea por causa de la información que Usted mismo introduzca en la red o de la utilización de los servicios de transacción o intercambio, o por cualquier otro motivo relacionado con el contenido de los servicios de Internet.</p>
                <p class="conditions">No asumiremos responsabilidad alguna en relación con las consecuencias que para Usted puedan derivarse del uso de los dispositivos suministrados.</p>
            </div>
            <div class="col-xs-6">
                <p class="conditions"><b>5.2 Compromisos y garantías asumidos por Usted</b></p>
                <p class="conditions">Usted se compromete a respetar cualquier normativa aplicable en la utilización del servicio contratado, y será responsable con carácter exclusivo, en particular, de cualquier infracción de derechos de propiedad intelectual o propiedad industrial (patentes, marcas, “copyright” u otros), intromisión en comunicaciones privadas, ataque informático o acto preparatorio de éste, obtención de información confidencial, así como cualquier otro acto ilícito o, en general, que cause o pueda causar daños o perjuicios para nosotros o para terceros y que se produzca con ocasión del uso por Usted de los productos contratados, o del uso del servicio que ponemos a su disposición por terceras personas. Se considerará que existe un incumplimiento del contrato por su parte si Usted, o terceras personas, utilizan el servicio contratado de forma que puedan perjudicar nuestra imagen, incurren en una utilización abusiva del servicio que exceda los parámetros habituales del mercado para un uso personal (por ejemplo, descarga masiva) o cometen cualquiera de las infracciones mencionadas en este contrato. Para el servicio de acceso a Internet, Usted deberá adoptar las precauciones necesarias a efectos de preservar sus ficheros y sistemas informáticos de eventuales accesos no deseados por parte de terceros. Teniendo en cuenta las características de Internet, no nos podemos responsabilizar de la privacidad de los mensajes que Usted reciba o transmita a través de los servicios de Internet.</p>
                <p class="conditions">Usted se compromete a poner a disposición de E.S. toda la documentación requerida para poder disfrutar de los servicios contratados.
                <p class="conditions"><b>6. PAGO POR LOS PRODUCTOS CONTRATADOS</b></p>
                <p class="conditions"><b>6.1 Facturación y pago</b></p>
                <p class="conditions">E.S. le facturará el importe del servicio de acceso a Internet y de telefonía fija, en su caso, de acuerdo con las tarifas vigentes. El pago de las mismas se efectuará según la domiciliación bancaria facilitada. Unicamente se enviará factura en papel en caso de que Ud. nos lo solicite mediante nuestra oficina virtual.</p>
                <p class="conditions">La facturación de los distintos consumos y/o servicios será mensual, y si por razones técnicas no fuera posible la facturación en el mes inmediatamente posterior al devengo, E.S. podrá facturarlo en los meses posteriores.</p>
                <p class="conditions"><b>6.2 Suspensión e interrupción de los servicios.</b></p>
                <p class="conditions">En caso de impago / devolución del recibo se le notificará para que proceda a su pago en el plazo de cinco días. De no satisfacer el importe adeudado en dicho plazo, se suspenderá el servicio contratado hasta que se tenga conocimiento del pago, sin deducción en la cuota del mes en curso.</p>
                <p class="conditions">Si el retraso en el pago total o parcial de nuestra factura alcanza un mes a partir de la fecha del impago / devolución, podremos interrumpir definitivamente el servicio y dar por terminado este contrato sin perjuicio de nuestro derecho a reclamarle las cantidades impagadas, junto con los intereses y los daños y perjuicios que pudiesen ser procedentes. La interrupción definitiva del servicio se llevará a cabo conforme al procedimiento establecido para la suspensión del servicio descrito anteriormente. Asimismo, el servicio podrá ser interrumpido conforme a la normativa vigente cuando se produzca una deficiencia que afecte a la integridad o seguridad de la Red o a la prestación de servicios a otros clientes y, en todo caso, cesará la medida de interrupción cuando Usted efectúe y nos comunique la desconexión del equipo objeto de la deficiencia.</p>
                <p class="conditions">Podremos interrumpir ocasionalmente la prestación de nuestros servicios con objeto de realizar trabajos de mejora, labores de reparación, cambios de equipamiento o por motivos análogos, si bien dichas interrupciones serán lo más breves posibles y se realizarán, preferentemente y siempre que sea posible, en horarios de 7 a 14 horas. Usted acepta la necesidad de consentir tales interrupciones y que nosotros no estaremos obligados a compensarle en manera alguna por las mismas salvo en lo previsto en el apartado “Compromisos y Garantías” de este contrato.</p>
                <p class="conditions"><b>7. CAMBIOS DE DOMICILIO</b></p>
                <p class="conditions">Si Usted desea que cambie el lugar en el que le prestamos los servicios deberá comunicárselo a nuestro Servicio de Atención al Cliente. Por razones técnicas, habrá casos en los que no podamos satisfacer su petición. En este caso, Usted podrá dar por terminado el contrato según las indicaciones que figuran posteriormente sobre la extinción del contrato. Si el cambio de domicilio es técnicamente posible, Usted únicamente deberá abonar la tarifa vigente por cambio de domicilio.</p>
                <p class="conditions">Si Usted solicita que le prestemos servicio en un domicilio adicional al que figura en este contrato, deberá suscribir un nuevo contrato.</p>
                <p class="conditions"><b>8. DURACIÓN Y EXTINCIÓN DEL CONTRATO</b></p>
                <p class="conditions">Este contrato tiene carácter indefinido, salvo que se produzca su extinción, respecto de alguno o todos los servicios, por alguna de las causas siguientes:</p>
                <p class="conditions">- Usted quiere darlo por terminado: tiene derecho a hacerlo en cualquier momento siempre que nos lo comunique previamente, con una antelación mínima de dos días hábiles al momento en que ha de surtir efectos, por correo electrónico. Usted tiene derecho a que le facilitemos un número de referencia que le permitirá verificar el día de la solicitud de baja y tener constancia de la misma. En un plazo máximo de 10 días desde la notificación E.S. pasará a retirar los equipos instalados en su día. Salvo que sea por causa imputable a Usted, no le facturaremos las cantidades devengadas con posterioridad a la retirada de los equipos.</p>
                <p class="conditions">- Razones técnicas: Nosotros podremos dar por terminado en cualquier momento el contrato si detectamos que ha existido una manipulación o alteración de los equipos por su parte que causen anomalías o modificaciones en el servicio.</p>
                <p class="conditions">- Incumplimiento: Si alguna de las partes, Usted o nosotros, incumple las obligaciones a las que se compromete en este contrato, la otra parte podrá dar por terminado inmediatamente el contrato e interrumpir los servicios, comunicándolo a la parte incumplidora y sin perjuicio de la reclamación de los daños y perjuicios causados o el ejercicio de otras acciones que le pudieran corresponder. Si Usted solicita la baja por existir un incumplimiento contractual por nuestra parte, la eficacia de su solicitud será inmediata y no le facturaremos las cantidades que se devenguen con posterioridad a la fecha de la misma, salvo que exista una causa imputable a Usted. La suspensión o interrupción del servicio por retrasos en el pago de facturas, o en la constitución del depósito en garantía, se producirá según lo dispuesto en el apartado “Suspensión e interrupción del servicio”.</p>
                <p class="conditions">- Igualmente, previo cumplimiento de la legislación vigente, nosotros podremos dar por terminado el presente contrato avisándole de ello con una antelación no inferior a un mes.</p>
                <p class="conditions">En cualquier caso, la terminación del contrato no le exonera a Usted de cumplir con sus obligaciones de pago de las cantidades devengadas por los servicios prestados hasta la fecha señalada de terminación efectiva.</p>
                <p class="conditions">Además, E.S. podrá, previo cumplimiento de la legislación vigente, ceder el presente contrato sin necesidad de obtener su consentimiento y únicamente mediante el envío de una comunicación al respecto.</p>
                <p class="conditions"><b>9. SECRETO DE LAS COMUNICACIONES E INFORMACIÓN Y AUTORIZACIÓN PARA EL TRATAMIENTO DE LOS DATOS DE CARÁCTER PERSONAL</b></p>
                <p class="conditions">Nosotros le garantizamos la confidencialidad de los mensajes transmitidos y el secreto de las comunicaciones, así como la protección de sus datos personales, conforme a la normativa vigente.</p>
                <p class="conditions">Le informamos que los datos de carácter personal recogidos en este contrato, y los generados por la prestación de los servicios contratados, serán incorporados a un fichero del que Eléctrica Sollerense, S.A.U. (con domicilio social en la calle Sa Mar, 146, 07100 Sóller) es responsable y titular, y serán tratados con la finalidad de gestionar la prestación de los servicios contratados, incluido el control para la prevención del fraude en dichos servicios, realizar la facturación y los pagos de las interconexiones. La cumplimentación de los datos solicitados en este contrato es obligatoria para la prestación de los servicios objeto del mismo. Usted autoriza que sus datos personales, teléfono móvil y dirección de correo electrónico, sean utilizados para la comunicación y notificaciones de E.S.</p>
                <p class="conditions">En cualquier caso, podrá dirigir sus comunicaciones, revocar los consentimientos otorgados y ejercitar sus derechos de oposición, acceso, rectificación y cancelación de sus datos personales, en cualquier momento, dirigiendo una solicitud por escrito junto con una fotocopia de su DNI, pasaporte o documento identificativo, a la siguiente dirección de correo electrónico: protecciondatos@electricasollerense.es.</p>
                <p class="conditions"><b>10. COMUNICACIONES</b></p>
                <p class="conditions">Todas las comunicaciones que Usted deba realizar como consecuencia de este contrato deberá dirigirlas por teléfono o vía  correo electrónico a nuestro Servicio de Atención al Cliente, cuyos datos están indicados en el encabezamiento del anverso de este contrato o en nuestra página web. Las comunicaciones que nosotros debamos realizar se las enviaremos preferentemente a la dirección de correo electrónico indicada en el anverso de este contrato</p>
			</div>
        </div>
         <div id="footer">
             <div id="container">
                 <div class="row">
                    <table style="width: 100%;">
                        <tr>
                            <td></td>
                            <td style="text-align: center;">Sóller a ${formatLang(contract.data_firma_contracte, date=True)}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="text-align: center;"><img style="width: 180px;" src="${addons_path}/soller_operador_report/report/firma_afb.jpg"/></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">${_('Firma del client')}</td>
                            <td style="text-align: center;">${company.name}</td>
                        </tr>
                    </table>
                </div>
             </div>
         </div>
    </div>

%for linea in lineas:
    %if (linea.product_id.categ_id.code == 'TLF' and linea.detall and linea.detall.isdigit()):

<%
owner_address_id = False
owner_address = False
if linea.client_donant_id:
    owner_address_id = linea.client_donant_id
elif linea.apoderat_id:
    owner_address_id = linea.apoderat_id

if owner_address_id:
    owner_address = owner_address_id.address_get(adr_pref=['contact'])
    if 'contact' in owner_address:
        owner_address = address_obj.browse(cursor, uid, owner_address['contact'])
    else:
        owner_address = {}

%>
    <p class="page-break"></p>

    <div id="wrap">
        <div class="container">
            <div class="row top-padding-fix">
                 <div class="col-xs-6" style="min-height: 148px;">
                    <img style="width: 558px;" src="${addons_path}/soller_operador_report/report/cabecera-facturas-nueva.png"/>
                </div>
                <div class="col-xs-6">
                    <p class="contract-header">Solicitud de Portabilidad</p>
                    <div class="col-xs-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                 <h4>Nº de Documento</h4>
                            </div>
                            <div class="panel-body">
                                <br/>
                            </div>
                       </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                 <h4>Código de Cliente</h4>
                            </div>
                            <div class="panel-body">
                                <br/>
                            </div>
                       </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                 <h4>Fecha Documento</h4>
                            </div>
                            <div class="panel-body">
                                <p>${formatLang(contract.data_firma_contracte, date=True)}</p>
                            </div>
                       </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Datos del Cliente</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <table>
                                <tr>
                                    <td><b>Nombre del Cliente:</b>  ${contract.titular_id.name}</td>
                                    <td><b>C.P.:</b> ${get_city(dir_titular)}</td>
                                </tr>
                                <tr>
                                    <td><b>NIF / CIF:</b> ${contract.titular_id.vat}</td>
                                    <td><b>Localidad:</b> ${get_city(dir_titular)}</td>
                                </tr>
                                <tr>
                                    <td><b>Dirección:</b> ${dir_titular.street}</td>
                                    <td><b>Provincia:</b> ${dir_titular.state_id and dir_titular.state_id.name.upper() or ''}</td>
                                </tr>
                                <tr>
                                    <td><b>Teléfono:</b> ${dir_titular.phone or ''}</td>
                                    <td><b>E-Mail:</b> ${dir_titular.email or ''}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Datos del Titular de la línea telefónica*</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <table>
                                <tr>
                                    <td><b>Nombre del Cliente:</b>
                                        %if linea.client_donant_id:
                                             ${linea.client_donant_id.name}
                                        %endif
                                    </td>
                                    <td><b>NIF / CIF:</b>
                                        %if linea.client_donant_id:
                                             ${linea.client_donant_id.vat}
                                        %endif
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>Persona Apoderada:</b>
                                        %if linea.apoderat_id:
                                             ${linea.apoderat_id.name}
                                        %endif
                                    </td>
                                    <td><b>NIF / CIF:</b>
                                        %if linea.apoderat_id:
                                             ${linea.apoderat_id.vat}
                                        %endif
                                    </td>
                                </tr>

                                %if owner_address:
                                    <tr>
                                        <td><b>Dirección:</b> ${owner_address.street}</td>
                                        <td><b>Población:</b> ${get_city(owner_address)}</td>
                                    </tr>
                                    <tr>
                                        <td><b>C.P.:</b> ${owner_address.zip}</td>
                                        <td><b>Provincia:</b> ${owner_address.state_id and owner_address.state_id.name.upper() or ''}</td>
                                    </tr>
                                %else:
                                    <tr>
                                        <td><b>Dirección:</b></td>
                                        <td><b>Población:</b></td>
                                    </tr>
                                    <tr>
                                        <td><b>C.P.:</b></td>
                                        <td><b>Provincia:</b></td>
                                    </tr>
                                %endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <p>* Sólo si son diferentes a los datos del Cliente</p>
            <br/>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Numeración a portar</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <p><b>Número/s de teléfono: </b>${linea.detall}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Datos del Operador</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <table>
                                <tr>
                                    <td><b>Operador Donante:</b></td>
                                    <td><b>Operador Receptor:</b> NRM 843</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Tipo de Acceso</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <table>
                                <tr>
                                    <td><b>· Individual analógico</b></td>
                                    <td>· Acceso básico RDSI</td>
                                    <td>· Numeración Red Inteligente</td>
                                    <td>· Enlace Analógico</td>
                                    <td>· Acceso primario RDSI</td>
                                    <td>· ISPBX</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Horario Preferido para el cambio (sólo para telefonía fija, bajo disponibilidad del operador donante)</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <p><b>hora de inicio:   (de lunes a viernes de 9:00 horas a 14:00 horas y de 17:00 a 19:00 horas)</b></p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="long_text">Les comunico que deseo causar baja en los servicios que actualmente tengo contratados con Uds. (operador donante) correspondientes a los números de teléfono que se detallan en este escrito, y que deseo conservar esta numeración por lo que igualmente les solicito que al producirse la baja mis números de teléfono sean portados a mi nuevo proveedor del servicio de telefonía (operador receptor). Acepto la posible interrupción o limitación en la prestación del servicio durante el tiempo mínimo indispensable para realizar los trabajos de cambio de operador, que será de un máximo de 4 horas. He sido informado de que la conservación de números es una facilidad que afecta a múltiples operadores, cuyo funcionamiento es complejo y cuya implantación no ha sido suficientemente probada, por lo que pueden producirse deficiencias en su prestación. El abajo firmante, con sujeción a lo previsto en la normativa vigente, autoriza la transferencia a los operadores involucrados en el proceso de conservación de la numeración, de los datos personales que sean necesarios para poder llevar a cabo ambos procesos. La negativa o revocación de esta autorización impedirá llevar a cabo la desagregación del bucle y/o la portabilidad solicitada de los números arriba indicados. Podré ejercer mis derechos de acceso, rectificación, cancelación y oposición sobre los datos tratados por el operador receptor: Oficina de Atención al Cliente.</p>
        </div>
    </div>
    <div id="footer">
        <div class="container">
            <div class="row">
                <table style="width: 100%;">
                     <tr>
                         <td></td>
                        <td></td>
                        <td style="text-align: center;"><img style="width: 180px;" src="${addons_path}/soller_operador_report/report/firma_afb.jpg"/></td>
                     </tr>
                     <tr>
                        <td style="text-align: center;">Firma Cliente<br/>(Titular/es de la/s línea/s a portar)</td>
                        <td style="text-align: center;">Firma Abonado</td>
                        <td style="text-align: center;">Firma Operador Receptor</td>
                     </tr>
                </table>
            </div>
        </div>
    </div>

    <p class="page-break"></p>
    <div id="wrap">
        <div class="container">
            <div class="row top-padding-fix">
                 <div class="col-xs-6" style="min-height: 148px;">
                    <img style="width: 558px;" src="${addons_path}/soller_operador_report/report/cabecera-facturas-nueva.png"/>
                </div>
                <div class="col-xs-6">
                    <p class="contract-header">Solicitud de Portabilidad</p>
                    <div class="col-xs-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                 <h4>Nº de Documento</h4>
                            </div>
                            <div class="panel-body">
                                <br/>
                            </div>
                       </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                 <h4>Código de Cliente</h4>
                            </div>
                            <div class="panel-body">
                                <br/>
                            </div>
                       </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                 <h4>Fecha Documento</h4>
                            </div>
                            <div class="panel-body">
                                <p>${formatLang(contract.data_firma_contracte, date=True)}</p>
                            </div>
                       </div>
                    </div>
                </div>
            </div>
            <div class="row top-padding-fix migrateConditions" style="padding-left: 20px;">
                <div class="col-xs-12">
                    <p style="font-size: 18px; text-align: center"><b>Condiciones generales de solicitud de conservación de numeración.</b></p>
                    <br/>
                </div>
                <div class="col-xs-6">
                    <p style="text-decoration: underline; font-size: 16px">Condiciones para el Cliente:</p>
                    <ol>
                        <li>Darse previamente de alta con el Operador, mediante la suscripción del correspondiente contrato de prestación de servicios.</li>
                        <li>Cumplimentar correctamente la presente solicitud para tramitar la baja con el Operador Donante, en la que manifieste asimismo su deseo de conservar su numeración con el nuevo operador.</li>
                        <li>No encontrarse en situación de suspensión o interrupción del servicio con el Operador Donante por impago.</li>
                        <li>El Cliente acepta la posible interrupción o limitación en la prestación del servicio durante el tiempo mínimo indispensable para realizar los trabajos de  cambio de operador, que tendrán lugar en horario comercial de lunes a viernes de 9:00 horas a 14:00 horas y  de 17:00 horas a 19:00 horas siendo la duración máxima de cuatro horas desde su inicio, exceptuando los días de fiesta nacional, fiestas de la Comunidad y fiestas locales, sin que ello le dé derecho a ningún tipo de indemnización.</li>
                        <li>El cliente/abonado autoriza al Operador a portar toda la numeración asociada a los números de cabecera, en caso de que el cliente haya identificado como número a portar un número asociado a un número de cabecera.</li>
                    </ol>

                    <p style="text-decoration: underline; font-size: 16px">Condiciones para el Operador:</p>
                    <ol>
                        <li>Tramitar la conservación de la numeración solicitada por el Cliente siempre que:
                           <ol type="i">
                                <li>El Cliente se dé de alta con el Operador mediante el correspondiente contrato de prestación de servicios.</li>
                                <li>Simultáneamente, solicite al Operador que le tramite la baja con el Operador Donante, conservando la numeración (mediante la presente solicitud).</li>
                           </ol>
                        </li>
                        <li>Proporcionar la conservación de la numeración cuando haya cambio de operador de “red telefónica pública fija”, pero no haya modificación de servicio, ni de ubicación física.</li>
                        <li>El Operador responderá de los daños que se causen al Cliente en el transcurso de un proceso de cambio de operador correctamente solicitado por el Cliente, salvo que los daños:
                            <ol type="i">
                                <li>No sean imputables a acciones u omisiones del operador.</li>
                                <li>Hayan sido causados por el propio Cliente o por terceros operadores.</li>
                                <li>Se produzcan como consecuencia de la posible interrupción o limitación en la prestación del servicio durante el tiempo mínimo indispensable para realizar los trabajos de cambio de operador, y/o cuando se den como consecuencia de causas de fuerza mayor o caso fortuito.</li>
                            </ol>
                        </li>
                        <li>Notificar al Cliente con al menos 3 días de antelación, cuándo será efectiva la conservación de la numeración.</li>
                    </ol>
                </div>
                <div class="col-xs-6">
                    <p style="text-decoration: underline; font-size: 16px">Causas por las que no se podrá llevar a cabo la conservación de la numeración:</p>
                    <br/>
                    <p style="font-size: 16px">El Operador no será responsable de la imposibilidad de proporcionar la conservación de la numeración en los siguientes casos:</p>
                    <ol>
                        <li>Por cambio de ubicación física del Cliente.</li>
                        <li>Cuando el abonado del donante se encuentre con el servicio suspendido o interrumpido en los términos establecidos en la legislación vigente.</li>
                        <li>Por datos incompletos o erróneos sobre la identificación del Cliente, del Operador Donante o del Operador Receptor en la presente solicitud.</li>
                        <li>Por falta de correspondencia entre numeración y Cliente identificado por su NIF/CIF.</li>
                        <li>Porque transcurrido un mes desde que el Cliente causó baja con el operador que en ese momento le proveía el servicio, no solicitó la conservación del número con el Operador receptor.</li>
                        <li>Por la indicación de la Entidad de Referencia de que ya existe un proceso de portabilidad de dicha numeración con otro operador.</li>
                        <li>Por causas de fuerza mayor o técnicas siempre que sean justificadas (tipo de acceso erróneo, numeración asociada a grupos de salto).</li>
                        <li>Cualquier otra causa que pueda ser acordada voluntariamente entre los operadores dentro del marco legal.</li>
                        <li>Porque el prefijo asociado al número que se va a portar, que sirve a las redes de dominio de encaminamiento de portabilidad para enrutar adecuadamente a la red receptora las llamadas realizadas a dicho número portado, no esté abierto en interconexión.</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="container">
            <div class="row">
                <table style="width: 100%;">
                    <tr>
                        <td></td>
                        <td></td>
                        <td style="text-align: center;"><img style="width: 180px;" src="${addons_path}/soller_operador_report/report/firma_afb.jpg"/></td>
                     </tr>
                    <tr>
                        <td style="text-align: center;">Firma Cliente<br/>(Titular/es de la/s línea/s a portar)</td>
                        <td style="text-align: center;">Firma Abonado</td>
                        <td style="text-align: center;">Firma Operador Receptor</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

 %endif
%endfor
% if objects.index(contract) + 1 != len(objects):
<p class="page-break"></p>
% endif
%endfor
</body>
</html>