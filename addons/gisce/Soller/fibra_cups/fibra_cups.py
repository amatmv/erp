# -*- encoding: utf-8 -*-

from osv import osv, fields


class FibraCups(osv.osv):

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _columns = {
        'fiber': fields.boolean('Fiber', propi="no"),
        'ct_code': fields.char('CT', size=10, readonly=True),
        'fiber_notes': fields.text('Fiber Notes', propi="no"),
        'ct_fiber': fields.char('CT Fiber', size=10, propi="no")
    }

    _defaults = {
        'fiber': lambda *a: False,
    }

FibraCups()
