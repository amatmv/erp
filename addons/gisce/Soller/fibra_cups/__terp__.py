# -*- coding: utf-8 -*-
{
    "name": "Fibra CUPS",
    "description": """Add fiber functionalities
    * Fiber coverage
    * Associated CT""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "fibra",
    "depends":[
        "base",
        "giscedata_cups_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "fibra_cups_view.xml"
    ],
    "active": False,
    "installable": True
}
