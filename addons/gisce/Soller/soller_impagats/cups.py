# -*- encoding: utf-8 -*-

from osv import osv, fields


class CupsImpagat(osv.osv):

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def name_get(self, cursor, uid, ids, context=None):

        lang_obj = self.pool.get('res.lang')
        user_obj = self.pool.get('res.users')
        conf_obj = self.pool.get('res.config')

        # Get original result
        res = super(CupsImpagat,
                    self).name_get(cursor, uid, ids, context=context)
        if not int(conf_obj.get(cursor, uid, 'show_pending', '0')):
            return res
        # Search for lang
        lang_code = context.get('lang', False)
        if not lang_code:
            lang_code = user_obj.browse(cursor, uid, uid).context_lang
        lang_id = lang_obj.search(cursor, uid, [('code', '=', lang_code)])

        # Read pending from all cups in ids
        vals = self.read(cursor, uid, ids, ['pendent'])
        pending_vals = dict([(x['id'], x['pendent']) for x in vals])

        # pending_vals = self.get_pending(cursor, uid, ids)

        mod_res = []
        for id, str in res:
            prefix = ''
            # Only modify original result if pending amount
            if pending_vals.get(id, 0):
                raw_debt = pending_vals.get(id, 0)
                if lang_id:
                    debt = lang_obj.format(cursor, uid, lang_id,
                                           '%.2f', raw_debt)
                else:
                    debt = '{0:.2f}'.format(raw_debt)
                prefix = '**{0}** '.format(debt)
            mod_res.append((id, prefix + str))

        return mod_res

    def get_pending(self, cursor, uid, ids, context=None):

        polissa_obj = self.pool.get('giscedata.polissa')
        contract_obj = self.pool.get('soller.operador.contrato')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'active_test': False})

        res = dict.fromkeys(ids, 0)
        # Search for all electric contracts associated to cups
        search_params = [('cups', 'in', ids)]
        polissa_ids = polissa_obj.search(cursor, uid, search_params,
                                         context=ctx)
        if not polissa_ids:
            return res

        for polissa in polissa_obj.browse(cursor, uid, polissa_ids):
            res.setdefault(polissa.cups.id, 0)
            res[polissa.cups.id] += polissa.pendent

        # Search for operador contracts associated to cups
        search_params = [('cups_id', 'in', ids)]
        contract_ids = contract_obj.search(cursor, uid, search_params,
                                           context=ctx)
        if not contract_ids:
            return res

        for contract in contract_obj.browse(cursor, uid, contract_ids):
            cups_id = contract.cups_id.id
            res.setdefault(cups_id, 0)
            res[cups_id] += contract.pending

        return res

    def _get_pending_amount(self, cursor, uid, ids, name, arg, context=None):

        return self.get_pending(cursor, uid, ids, context=context)

    def _get_cups_from_invoice(self, cursor, uid, ids, context=None):
        """Returns ids of cups in invoice passed as ids"""

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        #Search factura associated to invoices in ids
        query = '''
            SELECT distinct f.cups_id
            FROM giscedata_facturacio_factura f
            INNER JOIN account_invoice i
            ON i.id = f.invoice_id
            WHERE i.id in %s
            AND i.type in ('out_invoice', 'out_refund')
            '''

        cursor.execute(query, (tuple(ids),))
        cups_ids = [x[0] for x in cursor.fetchall()]

        # Also search for operador invoice cups
        query = '''
            SELECT distinct contrato.cups_id
            FROM account_invoice i
            INNER JOIN account_journal journal
            ON journal.id = i.journal_id
            INNER JOIN soller_operador_contrato contrato
            ON contrato.name = i.origin
            WHERE journal.code = 'OPERADOR'
            AND i.id in %s'''

        cursor.execute(query, (tuple(ids),))
        op_cups_ids = [x[0] for x in cursor.fetchall()]
        return list(set(cups_ids) | set(op_cups_ids))

    def _get_cups_from_factura(self, cursor, uid, ids, context=None):
        """Return ids of cups associated to factura in ids"""

        factura_obj = self.pool.get('giscedata.facturacio.factura')

        vals = factura_obj.read(cursor, uid, ids, ['cups_id'])
        return [x['cups_id'][0] for x in vals
                if x['cups_id']]

    _store_pendent = {
        'giscedata.facturacio.factura': (_get_cups_from_factura,
                                         ['estat_impagat'], 30),
        'account.invoice': (_get_cups_from_invoice,
                            ['state', 'residual'], 30),
        'giscedata.cups.ps': (lambda self, cr, uid, ids, c=None: ids, [], 30),
    }

    _columns = {
        'pendent': fields.function(_get_pending_amount, method=True,
                                   type='float', digits=(16, 2),
                                   string='Pendent', store=_store_pendent,
                                   ),
    }

CupsImpagat()
