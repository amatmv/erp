# -*- coding: utf-8 -*-
{
    "name": "Gestió d'impagats",
    "description": """
Mostra la quantitat pendent i el pitjor estat d'impagat de les 
factures pendents a la polissa
    * Redefinir la funció copy pels camps específics d'impagats
    * Wizard per recalcular els impagats
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_facturacio_comer",
        "giscedata_polissa",
        "soller_operador"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_view.xml",
        "giscedata_facturacio_view.xml",
        "giscedata_facturacio_report.xml",
        "data.xml",
        "wizard/wizard_canvi_estat_impagat_view.xml",
        "wizard/wizard_carta_impagat_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
