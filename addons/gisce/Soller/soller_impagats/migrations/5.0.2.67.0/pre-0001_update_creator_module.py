# coding=utf-8
import netsvc


def migrate(cursor, installed_version):
    logger= netsvc.Logger()
    if not installed_version:
        return

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Deleting old references to wizard models')

    models = ('wizard.canvi.estat.impagat',
              'wizard.carta.retirada',
              'wizard.carta.tall')

    sql = '''
        DELETE FROM ir_model
        WHERE model in %s
    '''

    cursor.execute(sql, (models, ))

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Succesfully deleted')
