# coding=utf-8
from oopgrade import oopgrade
import netsvc

def up(cursor, installed_version):
    logger = netsvc.Logger()
    if not installed_version:
        return

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        'Copy value of estat_impagat field from giscedata_facturacio_factura to '
        'account_invoice.'
    )
    cursor.execute("""
        update account_invoice as ai
        set estat_impagat = f.estat_impagat
        from giscedata_facturacio_factura as f
        where f.invoice_id = ai.id
        """)

    oopgrade.drop_columns(
        cursor, [('giscedata_facturacio_factura', 'estat_impagat')]
    )

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Succesfully!')


def down(cursor, installed_version):
    logger = netsvc.Logger()
    if not installed_version:
        return

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        'Copy value of estat_impagat field from account_invoice to '
        'giscedata_facturacio_factura.'
    )
    cursor.execute("""
            update giscedata_facturacio_factura as f
            set estat_impagat = ai.estat_impagat
            from account_invoice as ai
            where f.invoice_id = ai.id
            """)

    oopgrade.drop_columns(
        cursor, [('account_invoice', 'estat_impagat')]
    )

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Succesfully!')

migrate = up