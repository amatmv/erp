<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="carta_pendiente" language="groovy" pageWidth="595" pageHeight="842" columnWidth="483" leftMargin="56" rightMargin="56" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.4641000000000008"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<subDataset name="Facturas_pendientes">
		<parameter name="polissa_id" class="java.lang.Integer">
			<defaultValueExpression><![CDATA[1]]></defaultValueExpression>
		</parameter>
		<queryString>
			<![CDATA[SELECT
    gp.id as polissa,
    ai.number as numero_factura,
    ai.date_invoice as fecha_factura,
    ai.amount_total as total,
    ai.state,
    ai.estat_impagat
FROM account_invoice ai
INNER JOIN giscedata_facturacio_factura gff on ai.id = gff.invoice_id
INNER JOIN giscedata_polissa gp on gp.id = gff.polissa_id
WHERE gff.polissa_id = $P{polissa_id}
and ai.state = 'open' and ai.estat_impagat <> 'correcte'
order by ai.date_invoice]]>
		</queryString>
		<field name="polissa" class="java.lang.Integer"/>
		<field name="numero_factura" class="java.lang.String"/>
		<field name="fecha_factura" class="java.sql.Date"/>
		<field name="total" class="java.math.BigDecimal"/>
		<field name="state" class="java.lang.String"/>
		<field name="estat_impagat" class="java.lang.String"/>
	</subDataset>
	<subDataset name="Total_facturas_pendientes">
		<parameter name="polissa_id" class="java.lang.Integer">
			<defaultValueExpression><![CDATA[1]]></defaultValueExpression>
		</parameter>
		<queryString>
			<![CDATA[SELECT
    sum(ai.amount_total) as total
FROM account_invoice ai
INNER JOIN giscedata_facturacio_factura gff on ai.id = gff.invoice_id
INNER JOIN giscedata_polissa gp on gp.id = gff.polissa_id
WHERE gp.id = $P{polissa_id}
AND ai.state = 'open' AND ai.estat_impagat <> 'correcte'
group by gp.id]]>
		</queryString>
		<field name="total" class="java.math.BigDecimal"/>
	</subDataset>
	<subDataset name="dataset1"/>
	<parameter name="data_carta" class="java.lang.String"/>
	<parameter name="drets_connexio" class="java.lang.String"/>
	<parameter name="IDS2" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{REPORT_PARAMETERS_MAP}.IDS.toString().replace("{","(").replace("}",")")]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
    gff.id as factura_id,
    pagador.name as pagador,
    pagador.street,
    coalesce(pagador.zip,'') as zip, pagador.city,
    provincia.name as provincia,
    pais.name as pais,
    gp.id as polissa_id,
    gp.name as abonado,
    gp.state as estado,
    titular.name as titular,
    cups.name as cups,
    cups.direccio,
    poblacio.name,
    comercialitzadora.name as comercialitzadora,
    company.rml_header1 as header1,
    company.rml_footer1 as header2,
    substring(company_partner.vat,3) as company_vat,
    coalesce(company_bank.printable_iban, '') as company_ccc,
    bank.name as company_bank_name,
    replace(substring(product.price_extra::varchar,0, 6), '.', ',') as recon
FROM giscedata_facturacio_factura gff
INNER JOIN account_invoice ai on ai.id = gff.invoice_id
INNER JOIN giscedata_polissa gp on gff.polissa_id = gp.id
INNER JOIN res_partner_address pagador on pagador.id = gp.direccio_pagament
LEFT JOIN res_country_state provincia on provincia.id = pagador.state_id
LEFT JOIN res_country pais on pais.id = pagador.country_id
LEFT JOIN res_partner titular on titular.id = gp.titular
LEFT JOIN giscedata_cups_ps cups on cups.id = gp.cups
LEFT JOIN res_poblacio poblacio on poblacio.id = cups.id_poblacio
LEFT JOIN res_partner comercialitzadora on comercialitzadora.id = gp.comercialitzadora
LEFT JOIN res_users uid on uid.id = gff.create_uid
LEFT JOIN res_company company on company.id = uid.company_id
LEFT JOIN res_partner company_partner on company_partner.id = company.partner_id
LEFT JOIN res_partner_bank company_bank on company_bank.partner_id = company_partner.id and company_bank.default_bank = True
LEFT JOIN res_bank bank on bank.id = company_bank.bank
LEFT JOIN product_product product on product.default_code = 'RECON'
WHERE
    gff.id in $P!{IDS2}
ORDER BY
    gff.id, lpad(gp.name, 10, '0')]]>
	</queryString>
	<field name="factura_id" class="java.lang.Integer"/>
	<field name="pagador" class="java.lang.String"/>
	<field name="street" class="java.lang.String"/>
	<field name="zip" class="java.lang.String"/>
	<field name="city" class="java.lang.String"/>
	<field name="provincia" class="java.lang.String"/>
	<field name="pais" class="java.lang.String"/>
	<field name="polissa_id" class="java.lang.Integer"/>
	<field name="abonado" class="java.lang.String"/>
	<field name="estado" class="java.lang.String"/>
	<field name="titular" class="java.lang.String"/>
	<field name="cups" class="java.lang.String"/>
	<field name="direccio" class="java.lang.String"/>
	<field name="name" class="java.lang.String"/>
	<field name="comercialitzadora" class="java.lang.String"/>
	<field name="header1" class="java.lang.String"/>
	<field name="header2" class="java.lang.String"/>
	<field name="company_vat" class="java.lang.String"/>
	<field name="company_ccc" class="java.lang.String"/>
	<field name="company_bank_name" class="java.lang.String"/>
	<field name="recon" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<detail>
		<band height="594" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="469" width="483" height="10"/>
				<textElement verticalAlignment="Middle" lineSpacing="Single">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[DATOS DEL SUMINISTRO]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="0" y="479" width="52" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Titular]]></text>
			</staticText>
			<textField>
				<reportElement x="52" y="479" width="231" height="16"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement verticalAlignment="Middle" lineSpacing="Single">
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{titular}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="0" y="495" width="52" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Dirección]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="283" y="479" width="38" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[CUPS]]></text>
			</staticText>
			<textField>
				<reportElement x="321" y="479" width="162" height="16"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement verticalAlignment="Middle" lineSpacing="Single">
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{cups}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="52" y="495" width="431" height="16"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement verticalAlignment="Middle" lineSpacing="Single">
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{direccio}]]></textFieldExpression>
			</textField>
			<textField pattern="&apos;Sóller, &apos;d &apos;de&apos; MMMMM &apos;de&apos; yyyy" isBlankWhenNull="true">
				<reportElement x="256" y="196" width="227" height="15"/>
				<textElement textAlignment="Right" lineSpacing="Single"/>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="256" y="116" width="227" height="14"/>
				<textElement verticalAlignment="Middle" lineSpacing="Single">
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{pagador}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="256" y="130" width="227" height="14"/>
				<textElement verticalAlignment="Middle" lineSpacing="Single">
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{street}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="256" y="144" width="227" height="14"/>
				<textElement verticalAlignment="Middle" lineSpacing="Single">
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[(($F{zip}=="")?$F{city}.toUpperCase():$F{zip} + " " + $F{city}.toUpperCase())]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="256" y="158" width="227" height="14"/>
				<textElement verticalAlignment="Middle" lineSpacing="Single">
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{provincia}.toUpperCase() + " - " + $F{pais}.toUpperCase()]]></textFieldExpression>
			</textField>
			<textField pattern="">
				<reportElement x="0" y="221" width="483" height="116"/>
				<textElement textAlignment="Justified" lineSpacing="Single" markup="styled">
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Muy Sr./a Nuestro/a:\n\n\nPor la presente le comunicamos que a dia de hoy existen recibos pendientes de pago por el importe abajo indicado.\n\nEl pago de dicho importe se puede hacer efectivo en nuestras oficinas, o realizar un ingreso (indicando el número de contrato abajo indicado) en la cuenta de " + $F{company_bank_name} + " número " + $F{company_ccc} +".\n\n"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="-13" y="70" width="512" height="10"/>
				<textElement lineSpacing="Single">
					<font size="7" isItalic="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{header1}]]></textFieldExpression>
			</textField>
			<image scaleImage="FillFrame">
				<reportElement x="-28" y="-7" width="533" height="72"/>
				<imageExpression class="java.lang.String"><![CDATA[$P{REPORT_PARAMETERS_MAP}.SUBREPORT_DIR + "cabecera_es_nueva.png"]]></imageExpression>
			</image>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="-13" y="80" width="512" height="10" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{company_vat} + " " + $F{header2}]]></textFieldExpression>
			</textField>
			<componentElement>
				<reportElement x="0" y="565" width="483" height="13"/>
				<jr:list xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" printOrder="Vertical">
					<datasetRun subDataset="Facturas_pendientes">
						<datasetParameter name="polissa_id">
							<datasetParameterExpression><![CDATA[$F{polissa_id}]]></datasetParameterExpression>
						</datasetParameter>
						<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					</datasetRun>
					<jr:listContents height="13" width="483">
						<textField>
							<reportElement x="0" y="0" width="142" height="12"/>
							<textElement textAlignment="Center" lineSpacing="Single">
								<font size="8"/>
							</textElement>
							<textFieldExpression class="java.lang.String"><![CDATA[$F{numero_factura}]]></textFieldExpression>
						</textField>
						<textField pattern="dd/MM/yyyy">
							<reportElement x="142" y="1" width="155" height="12"/>
							<textElement textAlignment="Center" lineSpacing="Single">
								<font size="8"/>
							</textElement>
							<textFieldExpression class="java.util.Date"><![CDATA[$F{fecha_factura}]]></textFieldExpression>
						</textField>
						<textField pattern="#,##0.00">
							<reportElement x="297" y="0" width="113" height="12"/>
							<textElement textAlignment="Right" lineSpacing="Single">
								<font size="8"/>
							</textElement>
							<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{total}]]></textFieldExpression>
						</textField>
					</jr:listContents>
				</jr:list>
			</componentElement>
			<textField>
				<reportElement x="52" y="533" width="90" height="16"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement verticalAlignment="Middle" lineSpacing="Single">
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{abonado}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="0" y="533" width="52" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="3">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Contrato]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="297" y="549" width="186" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="0">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Importe]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="0" y="549" width="142" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="0">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Factura/s]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="523" width="483" height="10"/>
				<textElement verticalAlignment="Middle" lineSpacing="Single">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[IMPORTE PENDIENTE]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="142" y="549" width="155" height="16" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="0">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Fecha]]></text>
			</staticText>
			<image>
				<reportElement x="310" y="365" width="136" height="91"/>
				<imageExpression class="java.lang.String"><![CDATA[$P{REPORT_PARAMETERS_MAP}.SUBREPORT_DIR + "sello_es.png"]]></imageExpression>
			</image>
			<componentElement>
				<reportElement positionType="Float" x="10" y="578" width="424" height="13"/>
				<jr:list xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" printOrder="Vertical">
					<datasetRun subDataset="Total_facturas_pendientes">
						<datasetParameter name="polissa_id">
							<datasetParameterExpression><![CDATA[$F{polissa_id}]]></datasetParameterExpression>
						</datasetParameter>
						<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					</datasetRun>
					<jr:listContents height="13" width="424">
						<textField pattern="#,##0.00">
							<reportElement x="300" y="1" width="100" height="12"/>
							<textElement textAlignment="Right" lineSpacing="Single">
								<font isBold="false"/>
							</textElement>
							<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{total}]]></textFieldExpression>
						</textField>
						<staticText>
							<reportElement x="200" y="1" width="100" height="12"/>
							<textElement textAlignment="Right" lineSpacing="Single"/>
							<text><![CDATA[Importe total:]]></text>
						</staticText>
					</jr:listContents>
				</jr:list>
			</componentElement>
			<textField pattern="">
				<reportElement x="0" y="337" width="483" height="28">
					<printWhenExpression><![CDATA[$F{estado} == "tall"]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Justified" lineSpacing="Single" markup="styled">
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Además, dado que su suministro se encuentra cortado deberá abonar la cantidad de <style isBold=\"true\">" + $F{recon} + "</style> euros en concepto de derechos de conexión para restablecer su suministro."]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="117">
			<staticText>
				<reportElement x="-22" y="0" width="527" height="113"/>
				<textElement textAlignment="Justified" lineSpacing="Single">
					<font size="6"/>
				</textElement>
				<text><![CDATA[Conforme a lo previsto en la Ley Orgánica de Protección de Datos de Carácter Personal, Ley 15/1999 de 13 de diciembre (en adelante LOPD), Eléctrica Sollerense le informa que los datos obtenidos a través de este formulario o contrato están incluidos en varios fichero mixtos, y cuya Responsable de los Ficheros es Eléctrica Sollerense con CIF A57048332 con domicilio en C/Sa Mar 146, 07100 - Sóller, Islas Baleares. Le informamos que de acuerdo con la LOPD usted podrá ejercer gratuitamente sus derechos de acceso, rectificación, cancelación y oposición de conformidad con los art.15, 16 y 17 dirigiendo un escrito a Eléctrica Sollerense con CIF A57048332 con domicilio en C/Sa Mar 146, 07100 - Sóller, Islas Baleares. En este sentido informarle que de conformidad con lo dispuesto en la Ley 16/2009 de 13 de noviembre de servicios de pago le será de aplicación lo dispuesto en la misma, de conformidad con el art.49,2 de la Ley Orgánica de protección de datos, concretamente en lo dispuestos en su apartado segundo y tercero, reconociendo el cliente darse por enterado de todos estos extremos. En virtud de lo establecido en la LOPD y en el RLOPD, el Responsable de los Ficheros permitirá el acceso del Encargado del Tratamiento a los datos de carácter personal contenidos en los Ficheros, para que este realice el tratamiento de los mismos con el objeto de dar el cumplimiento prestación al contrato de prestación de servicios suscrito entre las partes. Siendo en este caso aplicable Articulo 12 de la LOPD en relación con los artículos 4 de la LOPD calidad de los datos, art. 5 de la LOPD derecho de información en la recogida de los datos, art. 6 de la LOPD consentimiento del afectado, art. 7 de la LOPD datos especialmente protegidos, art. 8 de la LOPD datos relativos a salud, art. 9 de la LOPD seguridad de los datos, art. 10 de la LOPD deber de secreto, art. 11 comunicación de los datos y art. 12 acceso de datos por cuenta de terceros. Todo ello de conformidad con la legislación  aplicable; en cualquier caso será de aplicación el art. 11.2 de la LOPD y art. 10.22 del reglamento de la LOPD, en relación a la ORDEN ITC 3860/2007 de 28 de DICIEMBRE y le será aplicable específicamente el art. 41, 44,45,47 de la Ley 54/1997 de 27 de noviembre del sector eléctrico y posterior modificaciones con la Ley 17/2007 en cuanto a las cesiones y accesos de datos de los clientes, de las empresas comercializadoras y distribuidoras, en el sistema de información de puntos de suministro.]]></text>
			</staticText>
		</band>
	</pageFooter>
</jasperReport>
