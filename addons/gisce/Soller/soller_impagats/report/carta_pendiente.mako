<%
from tools import config
from datetime import datetime
from bankbarcode.cuaderno57 import Recibo507

addons_path = config['addons_path']
cursor = objects[0]._cr
uid = user.id
pool = objects[0].pool
polissa_obj = pool.get('giscedata.polissa')
address_obj = pool.get('res.partner.address')
bank_obj = pool.get('res.partner.bank')
product_obj = pool.get('product.product')
factura_obj = pool.get('giscedata.facturacio.factura')

search_params = [('default_code', '=', 'RECON')]
product_id = product_obj.search(cursor, uid, search_params)[0]
product = product_obj.read(cursor, uid, product_id, ['price_extra'])
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <link rel="stylesheet" type="text/css" href="${addons_path}/soller_impagats/report/static/css/print.css"/>
</head>
<body>
% for factura in objects:
<%
setLang(factura.lang_partner)

#Company data
company = factura.company_id.partner_id
company_addresses = company.address_get(adr_pref=['contact'])
if 'contact' in company_addresses:
    company_address = address_obj.read(cursor, uid, company_addresses['contact'])
else:
    company_address = {}

search_params = [('partner_id', '=',  company.id)]
bank_ids = bank_obj.search(cursor, uid, search_params)
banks = bank_obj.browse(cursor, uid, bank_ids)

polissa = polissa_obj.browse(cursor, uid, factura.polissa_id.id)

total_amount = 0
search_params = [('polissa_id', '=', factura.polissa_id.id),
                 ('state', '=', 'open'),
                 ('estat_impagat', '<>', 'correcte')]
fact_ids = factura_obj.search(cursor, uid, search_params)
read_fields = ['number', 'date_invoice', 'amount_total']
invoice_vals = []
recibo507_list = []
entity = ''.join([c for c in company.vat if c.isdigit()])
suffix = '001' # code for electric invoice
notice = '000000'
if fact_ids:
    invoice_vals = factura_obj.read(cursor, uid, fact_ids, read_fields)
    for val in invoice_vals:
        total_amount += val['amount_total']
        # Bank barcode
        ref = '{}'.format(val['id']).zfill(11) # invoice id
        amount = '{0:.2f}'.format(val['amount_total']) # invoice amount
        recibo507_list.append(Recibo507(entity, suffix, ref, notice, amount))
%>
    <div class="container">
        <div class="row top-padding-fix">
            <div class="col-xs-5">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             <h4>Datos de empresa</h4>
                        </div>
                         <div class="panel-body invoice-data">
                            <p><b><dfn>${company.name.upper()}</dfn></b> (${company.vat.upper()})</p>
                            <p>${'{}, {} - {}'.format(company_address['street'], company_address['zip'], company_address['city'].upper())}</p>
                            <p>${'{} - {}'.format(company_address['state_id'][1].upper(), company_address['country_id'][1].upper())}</p>
                            <p>${'Teléfono {}, Fax {}'.format(company_address['phone'], company_address['fax'])}</p>
                            <p>${company.website}</p>
                            <p>${company_address['email']}</p>
                            <p>${factura.company_id.rml_footer1}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-7">
                <div class="col-xs-12" style="min-height: 148px;">
                    <img style="width: 558px; float:right;" src="${addons_path}/soller_impagats/report/cabecera-cartas-nueva.png"/>
                </div>
                <div class="col-xs-12">
                    <div class="invoice-address">
                        <p>${polissa.pagador.name}</p>
                        <p>${polissa.direccio_pagament.street}</p>
                        <p>${polissa.direccio_pagament.zip} - ${polissa.direccio_pagament.city}</p>
                        <p>
                        % if polissa.direccio_pagament.state_id:
                            ${polissa.direccio_pagament.state_id.name.upper()} -
                        % endif
                        % if polissa.direccio_pagament.country_id:
                            ${polissa.direccio_pagament.country_id.name.upper()}
                        % endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-xs-12">
            <div style="margin-top: 40px;">
            </div>
        </div>
        <div>
            <p align="right">${datetime.strftime(datetime.now(), 'Sóller, %d de %B de %Y')}</p>
        </div>
        <div id="letter_content">
        <div>
                <p>Muy Sr./a Nuestro/a:</p>
                <br>
                <p>Por la presente le comunicamos que a día de hoy existen recibos pendientes de pago por el importe abajo indicado:</p>
                <br>
                <p>Podrá realizar el pago a través de alguno de los siguientes métodos:</p>
                <br>
                <p>- Pago mediante tarjeta bancaria</p>
                <br>
                <p>- Pago en oficina o cajero utilizando el código de barras que aparece en su factura. (La Caixa / Bankia)</p>
                <br>
                <p>- Pago web a través de la oficina virtual de Eléctrica Sollerense</p>
                <br>
                <p>- Pago por transferencia bancaria indicando el número de contrato en las siguientes cuentas:</p>
                <br>
            </div>

            <div>
                <table width="100%" style="margin-left:5%">
                    %for bank in banks:
                        <tr>
                            <td>${'{} ({})'.format(bank.bank.name, bank.bank.bic)}</td>
                            <td>${bank.printable_iban}</td>
                        </tr>
                    %endfor
                </table>
            </div>

            <div>
                <br>
                <p>Le comunicamos que en caso de suspensión del suministo y reconexión del mismo, se le efectuará un cargo en su próxima factura por un importe de <b>${formatLang(product['price_extra'])}</b> euros.</p>
            </div>
        </div>

        <div class="row top-padding-fix">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>DATOS DEL SUMINISTRO</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <div class="col-xs-6">
                            <p><b>Titular: </b>${polissa.titular.name.upper()}</p>
                            <p><b>Direción: </b>${polissa.cups.direccio}</p>
                        </div>
                        <div class="col-xs-6">
                            <p><b>CUPS: </b>${polissa.cups.name}</p>
                            <p><b>Contrato: </b>${polissa.name}</p>
                        </div>
                    </div>
            </div>
        </div>

        </div>

       <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>IMPORTE PENDIENTE</h4>
                    </div>
                    <div>
                        <table width="100%" class="panel-body">
                             <tr>
                                <td style="text-align: center"><b>Factura/s</b></td>
                                <td style="text-align: center"><b>Emisora:</b> ${'{} {}'.format(entity, suffix)} <b>Identificación:</b> ${notice}</td>
                                <td style="text-align: center"><b>Referencia</b></td>
                                <td style="text-align: center"><b>Fecha</b></td>
                                <td style="text-align: right"><b>Importe</b></td>
                            </tr>
                            %for num, invoice in enumerate(invoice_vals):
                                <tr>
                                    <td style="text-align: center; padding-top: 40px;">${invoice['number']}</td>
                                    <td style="text-align: center; padding-top: 40px;">${ recibo507_list[num].svg(writer_options={'module_height': 8, 'font_size': 0, 'text_distance': 4, 'module_width': 0.4}) }</td>
                                    <td style="text-align: center; padding-top: 40px;">${'{}{}'.format(ref, recibo507_list[num].checksum())}</td>
                                    <td style="text-align: center; padding-top: 40px;">${formatLang(invoice['date_invoice'], date=True)}</td>
                                    <td style="text-align: right; padding-top: 40px;">${formatLang(invoice['amount_total'])}</td>
                                </tr>
                            %endfor
                        </table>
                    </div>
                    <div class="panel-footer">
                        <table style="width: 100%">
                            <tr>
                                <td><b>Importe total:</b></td>
                                <td class="amount currency"><b>${formatLang(total_amount)}</b></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align: justify; padding-top: 30px;">

            <p><small>Conforme a lo previsto en la Ley Orgánica de Protección de Datos de Carácter Personal, Ley 15/1999 de 13 de diciembre (en adelante LOPD), Eléctrica Sollerense le informa que los datos
    obtenidos a través de este formulario o contrato están incluidos en varios fichero mixtos, y cuya Responsable de los Ficheros es Eléctrica Sollerense con CIF A57048332 con domicilio en C/Sa Mar
    146, 07100 - Sóller, Islas Baleares. Le informamos que de acuerdo con la LOPD usted podrá ejercer gratuitamente sus derechos de acceso, rectificación, cancelación y oposición de conformidad
    con los art.15, 16 y 17 dirigiendo un escrito a Eléctrica Sollerense con CIF A57048332 con domicilio en C/Sa Mar 146, 07100 - Sóller, Islas Baleares. En este sentido informarle que de conformidad
    con lo dispuesto en la Ley 16/2009 de 13 de noviembre de servicios de pago le será de aplicación lo dispuesto en la misma, de conformidad con el art.49,2 de la Ley Orgánica de protección de
    datos, concretamente en lo dispuestos en su apartado segundo y tercero, reconociendo el cliente darse por enterado de todos estos extremos. En virtud de lo establecido en la LOPD y en el
    RLOPD, el Responsable de los Ficheros permitirá el acceso del Encargado del Tratamiento a los datos de carácter personal contenidos en los Ficheros, para que este realice el tratamiento de los
    mismos con el objeto de dar el cumplimiento prestación al contrato de prestación de servicios suscrito entre las partes. Siendo en este caso aplicable Articulo 12 de la LOPD en relación con los
    artículos 4 de la LOPD calidad de los datos, art. 5 de la LOPD derecho de información en la recogida de los datos, art. 6 de la LOPD consentimiento del afectado, art. 7 de la LOPD datos
    especialmente protegidos, art. 8 de la LOPD datos relativos a salud, art. 9 de la LOPD seguridad de los datos, art. 10 de la LOPD deber de secreto, art. 11 comunicación de los datos y art. 12
    acceso de datos por cuenta de terceros. Todo ello de conformidad con la legislación aplicable; en cualquier caso será de aplicación el art. 11.2 de la LOPD y art. 10.22 del reglamento de la LOPD,
    en relación a la ORDEN ITC 3860/2007 de 28 de DICIEMBRE y le será aplicable específicamente el art. 41, 44,45,47 de la Ley 54/1997 de 27 de noviembre del sector eléctrico y posterior
    modificaciones con la Ley 17/2007 en cuanto a las cesiones y accesos de datos de los clientes, de las empresas comercializadoras y distribuidoras, en el sistema de información de puntos de
    suministro.</small></p>
            </div>

    </div>
    % if objects.index(factura) + 1 != len(objects):
        <p class="page-break"></p>
    % endif
% endfor
</body>
</html>