# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
from soller_impagats.account_invoice import ESTAT_IMPAGAT


class GiscedataPolissaImpagats(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy(self, cursor, uid, id, default=None, context=None):
        """Do not copy pending fields"""

        default.update({'pendent': 0,
                       })
        
        res_id = super(GiscedataPolissaImpagats,
                       self).copy(cursor, uid, id, default, context)
        return res_id
    
    def _get_impagats(self, cursor, uid, ids, name, arg, context=None):

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        # li donem un pes als diferents estats
        estats_impagats = {'correcte': 0, 'avis1': 1, 'avis2':2, 'tall':3}

        res = {}

        for polissa_id in ids:
            #per defecte tornem aquestos valors
            res[polissa_id] = {'impagat': 'correcte',
                               'pendent': 0}
            # Search for pending invoices
            search_params = [('polissa_id', '=', polissa_id), 
                             ('state','=','open')]
            factura_ids = factura_obj.search(cursor, uid, search_params)
            if not factura_ids:
                continue
            factura_vals = factura_obj.read(cursor, uid, factura_ids,
                                            ['residual', 'estat_impagat'])
            
            for val in factura_vals:
                res[polissa_id]['pendent'] += val['residual']
                factura_impagat = estats_impagats[val['estat_impagat']]
                polissa_impagat = estats_impagats[res[polissa_id]['impagat']]
                if  factura_impagat > polissa_impagat:
                    res[polissa_id]['impagat'] = val['estat_impagat']

        return res        

    def _get_polissa_from_factura(self, cursor, uid, ids, context=None):
        """Return polissa ids from invoices in ids"""
        
        factura_obj = self.pool.get('giscedata.facturacio.factura')

        vals = factura_obj.read(cursor, uid, ids, ['polissa_id'])
        return [val['polissa_id'][0] for val in vals
                if val['polissa_id']]

    def _get_polissa_from_invoice(self, cursor, uid, ids, context=None):
        """Returns polissa ids from invoices in ids"""
    
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        #Search factura associated to invoices in ids
        query = '''
            SELECT f.polissa_id
            FROM giscedata_facturacio_factura f
            INNER JOIN account_invoice i
            ON i.id = f.invoice_id
            WHERE i.id in %s
            AND i.type in ('out_invoice', 'out_refund')
            '''
        cursor.execute(query, (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    _store_impagat = {
        'giscedata.facturacio.factura': (_get_polissa_from_factura,
                                         ['estat_impagat'], 20),
        'account.invoice': (_get_polissa_from_invoice,
                            ['state', 'residual'], 20),
        'giscedata.polissa': (lambda self, cr, uid, ids, c=None: ids,
                              ['potencies_periode'], 20),
    }

    _columns = {
        'impagat': fields.function(_get_impagats, method=True,
                                   type='selection', selection=ESTAT_IMPAGAT, 
                                   size=20, string='Impagat',
                                   store=_store_impagat,
                                   multi='impagats',                       
                                   ),
        'pendent': fields.function(_get_impagats, method=True,
                                   type='float', digits=(16, 2),
                                   string='Pendent',
                                   store=_store_impagat,
                                   multi='impagats', 
                                   ),        
    }

GiscedataPolissaImpagats()
