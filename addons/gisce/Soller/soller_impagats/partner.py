# -*- encoding: utf-8 -*-

from osv import osv, fields


class PartnerImpagat(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    def name_get(self, cursor, uid, ids, context=None):

        lang_obj = self.pool.get('res.lang')
        user_obj = self.pool.get('res.users')
        conf_obj = self.pool.get('res.config')

        # Get original result
        res = super(PartnerImpagat,
                    self).name_get(cursor, uid, ids, context=context)
        if not int(conf_obj.get(cursor, uid, 'show_pending', '0')):
            return res
        # Search for lang
        lang_code = context.get('lang', False)
        if not lang_code:
            lang_code = user_obj.browse(cursor, uid, uid).context_lang
        lang_id = lang_obj.search(cursor, uid, [('code', '=', lang_code)])

        # Read credit from all partners in ids
        vals = self.read(cursor, uid, ids, ['pendent'])
        pending_vals = dict([(x['id'], x['pendent']) for x in vals])

        mod_res = []
        for id, str in res:
            prefix = ''
            # Only modify original result if pending amount
            if pending_vals.get(id, 0):
                raw_debt = pending_vals.get(id, 0)
                if lang_id:
                    debt = lang_obj.format(cursor, uid, lang_id,
                                           '%.2f', raw_debt)
                else:
                    debt = '{0:.2f}'.format(raw_debt)
                prefix = '**{0}** '.format(debt)
            mod_res.append((id, prefix + str))

        return mod_res

    def get_pending(self, cursor, uid, ids, context=None):

        invoice_obj = self.pool.get('account.invoice')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'active_test': False})

        res = dict.fromkeys(ids, 0)
        # Search for all pending invoices associated to partner
        search_params = [('partner_id', 'in', ids),
                         ('state', 'in', ('open', 'paid')),
                         ('type', 'in', ('out_invoice', 'out_refund')),
                         ('residual', '<>', 0)]
        invoice_ids = invoice_obj.search(cursor, uid, search_params,
                                         context=ctx)
        if not invoice_ids:
            return res

        for invoice in invoice_obj.browse(cursor, uid, invoice_ids):
            multiplier = 1
            if invoice.type == 'out_refund':
                multiplier = -1
            res[invoice.partner_id.id] += invoice.residual * multiplier
        if context.get('return_list', False):
            return [(key, value) for key, value in res.iteritems()]
        return res

    def _get_pending_amount(self, cursor, uid, ids, name, arg, context=None):

        return self.get_pending(cursor, uid, ids, context=context)

    def _get_partner_from_invoice(self, cursor, uid, ids, context=None):
        """Returns ids of partner in invoice passed as ids"""

        invoice_obj = self.pool.get('account.invoice')

        invoice_vals = invoice_obj.read(cursor, uid, ids, ['partner_id'])
        return list(set([x['partner_id'][0] for x in invoice_vals]))

    def _get_partner_from_factura(self, cursor, uid, ids, context=None):
        """Return ids of cups associated to factura in ids"""

        factura_obj = self.pool.get('giscedata.facturacio.factura')

        factura_vals = factura_obj.read(cursor, uid, ids, ['partner_id'])
        return list(set([x['partner_id'][0] for x in factura_vals]))

    _store_pendent = {
        'giscedata.facturacio.factura': (_get_partner_from_factura,
                                         ['estat_impagat'], 20),
        'account.invoice': (_get_partner_from_invoice,
                            ['state', 'residual'], 20),
        'res.partner': (lambda self, cr, uid, ids, c=None: ids, [], 20),
    }

    _columns = {
        'pendent': fields.function(_get_pending_amount, method=True,
                                   type='float', digits=(16, 2),
                                   string='Pendent', store=_store_pendent,
                                   ),
    }

PartnerImpagat()
