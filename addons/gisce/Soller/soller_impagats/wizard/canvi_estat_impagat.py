# -*- coding: utf-8 -*-
from osv import osv, fields
from soller_impagats.account_invoice import ESTAT_IMPAGAT


class WizardCanviEstatImpagat(osv.osv_memory):
    """Wizard per canviar els estats d'impagats de les factures.
    """

    _name = 'wizard.canvi.estat.impagat'

    def action_set_new_state(self, cursor, uid, ids, context=None):
        """Assignem el nou estat a totes les factures que haguem filtrat (ids).
        """
        if context is None:
            context = {}
        model = context.get('model', False)
        wizard = self.browse(cursor, uid, ids[0])
        model_obj = self.pool.get(model)
        model_ids = context.get('active_ids', [])
        canviades = 0
        for invoice in model_obj.browse(cursor, uid, model_ids):
            if invoice.estat_impagat != wizard.new_state:
                invoice.write({'estat_impagat': wizard.new_state})
                canviades += 1

        wizard.write({'num_factures_canviades': canviades, 'state': 'end'})

    def action_cancel(self, cursor, uid, ids, context=None):
        """Cancel·lem.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    def default_new_state(self, cursor, uid, context=None):
        """L'estat per defecte.
        """
        return 'avis1'

    def default_num_factures_a_canviar(self, cursor, uid, context=None):
        """Inicialitzem el número de factures a canviar.
        """
        if not context:
            context = {}
        return len(context.get('active_ids', []))

    def default_num_factures_canviades(self, cursor, uid, context=None):
        """Inicialitzem el número de factures canviades.
        """
        return 0

    def default_state(self, cursor, uid, context=None):
        """Inicialitzem l'estat del wizard.
        """
        return 'init'

    _columns = {
        'new_state': fields.selection(ESTAT_IMPAGAT, 'Nou estat',
                                      required=True),
        'num_factures_a_canviar': fields.integer('Factures a canviar',
                                                 readonly=True),
        'num_factures_canviades': fields.integer('Factures canviades',
                                                 readonly=True),
        'state': fields.char('State', size=10),
    }

    _defaults = {
        'new_state': default_new_state,
        'num_factures_a_canviar': default_num_factures_a_canviar,
        'num_factures_canviades': default_num_factures_canviades,
        'state': default_state,
    }

WizardCanviEstatImpagat()
