# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.translate import _
from oorq.decorators import job
from tools import config
import csv
import base64
import StringIO
import netsvc


class WizardCartaImpagat(osv.osv_memory):

    """Wizard para sacar la carta de corte"""

    _name = "wizard.carta.impagat"

    def default_summary(self, cursor, uid, context=None):
        '''
            Init the summary text.
        '''

        if not context:
            context = {}

        invoice_ids = context.get('active_ids', [])

        emailList, postalList = self._splitInvoiceTypes(cursor, uid,
                                                        invoice_ids)
        msg = 'Postal: {}\n\nEmail: {}'.format(len(postalList), len(emailList))
        return msg

    def action_continue(self, cursor, uid, ids, context=None):
        '''
            Change state to end value
        '''

        wizard = self.browse(cursor, uid, ids[0])
        invoice_ids = context.get('active_ids', [])
        emailList, postalList = self._splitInvoiceTypes(cursor, uid,
                                                        invoice_ids)

        if wizard.selected_action == 'postal_info':
            vals_view = {
                'name': 'Facturas (Postal)',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.facturacio.factura',
                'limit': len(postalList),
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': "[('id', 'in', %s)]" % (postalList),
            }
            return vals_view

        elif wizard.selected_action == 'email_info':

            vals_view = {
                'name': 'Facturas (Email)',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.facturacio.factura',
                'limit': len(emailList),
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': "[('id', 'in', %s)]" % (emailList),
            }
            return vals_view

        elif wizard.selected_action == 'postal_print' and not postalList:
            return {}
        elif wizard.selected_action == 'email_send' and not emailList:
            return {}
        elif (wizard.selected_action == 'all_print' and
              not emailList and not postalList):
            return {}

        elif wizard.selected_action == 'postal_file':
            file = self.create_postal_file(cursor, uid, postalList)
            wizard.write({'state': 'postal_file',
                          'postal_file': file})
        else:
            if context['action'] == 'cutoff':
                wizard.write({'state': 'end_cutoff',})
            else:
                wizard.write({'state': 'end_takeoff',})

    def action_print_send(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        wizard = self.browse(cursor, uid, ids)

        if wizard.send_to == 'altre' and not wizard.address_id:
            raise osv.except_osv('Error',
                                 _(u"Ha de seleccionar una adreça"))

        invoice_ids = context.get('active_ids', [])

        if wizard.selected_action == 'postal_print':
            emailList, postalList = self._splitInvoiceTypes(cursor, uid,
                                                            invoice_ids)
            invoice_ids = postalList

        elif wizard.selected_action == 'email_send':
            emailList, postalList = self._splitInvoiceTypes(cursor, uid,
                                                            invoice_ids)
            if emailList:
                self._prepare_registered_email(cursor, uid, ids, emailList,
                                               context)
            return {}

        if not invoice_ids:
            return {}

        invoice_ids = self._order_invoices_by_policy(cursor, uid, invoice_ids)

        datas = {'form': {'data_carta': wizard.data_carta,
                          'data_accio': wizard.data_accio,
                          'send_to': wizard.send_to,
                          'address_id': (wizard.address_id
                                         and wizard.address_id.id
                                         or 0),
                          'ids': invoice_ids,
                          }
                 }

        if context.get('action', '') == 'cutoff':
            report_name = 'report_wizard_carta_corte_mako'
        else:
            report_name = 'report_factura_aviso_retirada_mako'

        return {
            'type': 'ir.actions.report.xml',
            'report_name': report_name,
            'datas': datas,
        }

    def create_postal_file(self, cursor, uid, postal_list):
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        polissa_obj = self.pool.get('giscedata.polissa')

        outfile = StringIO.StringIO()
        csvwriter = csv.writer(outfile, delimiter='\t')

        csvwriter.writerow(('NOMBRE', 'APELLIDOS', 'EMPRESA', 'DIRECCION', 'CP',
                           'POBLACION', 'PROVINCIA', 'PAIS', 'REFERENCIA',
                           'TRAMO DE PESO', 'VALOR ANADIDO'))

        read_fields = ['polissa_id']
        polissa_ids = [ x['polissa_id'][0] for x in invoice_obj.read(
                                        cursor, uid, postal_list, read_fields)]

        polissa_ids = list(set(polissa_ids))
        for polissa in polissa_obj.browse(cursor, uid, polissa_ids):

            namelist = polissa.pagador.name.split(',')
            name = ''
            surname = ''
            if len(namelist) > 1:
                surname = namelist[0]
                name = namelist[1]
            elif len(namelist) > 0:
                name = namelist[0]

            state_name = ''
            if polissa.direccio_pagament.state_id:
                state_name = polissa.direccio_pagament.state_id.name.upper()

            csvwriter.writerow((name, surname, '',
                                polissa.direccio_pagament.street,
                                polissa.direccio_pagament.zip,
                                polissa.direccio_pagament.city,
                                state_name,
                                'ES', polissa.name, '1', '1'))

        return base64.b64encode(unicode(outfile.getvalue()))

    def _prepare_registered_email(self, cursor, uid, ids,
                                  invoice_ids, context):
        '''
            Prepare and send registed mail
        '''
        acc_obj = self.pool.get('poweremail.core_accounts')

        wizard = self.browse(cursor, uid, ids)

        if context.get('action', '') == 'cutoff':
            subject = 'Aviso de corte'
            report = 'report.report_wizard_carta_corte_mako'
            body = (u'<p>Muy Sr./a Nuestro/a:</p>'
                    u'<p>Por la presente le informamos de la existencia de '
                    u'recibos pendientes. Adjuntamos documentación para su '
                    u'información y descarga.</p>'
                    u'<p>Atentamente,</p>')

        else:
            subject = 'Aviso baja suministro'
            report = 'report.report_factura_aviso_retirada_mako'
            body = (u'<p>Muy Sr./a Nuestro/a:</p>'
                    u'<p>Por la presente le informamos de la baja del '
                    u'suministro eléctrico debido a la existencia de recibos '
                    u'pendientes. Adjuntamos documentación para su información '
                    u'y descarga.</p>'
                    u'<p>Atentamente,</p>')

        email_from = config.get('email_from', '')
        search_params = [('email_id', '=', email_from)]
        acc_id = acc_obj.search(cursor, uid, search_params)[0]

        vals = {
            'pem_from': email_from,
            'pem_subject': subject,
            'pem_body_text': body,
            'pem_account_id': acc_id,
            'mc_type': 'mail',
        }

        datas = {'model': 'wizard.carta.impagat',
                 'form': {'data_carta': wizard.data_carta,
                          'data_accio': wizard.data_accio,
                          'send_to': wizard.send_to,
                          'address_id': (wizard.address_id
                                         and wizard.address_id.id
                                         or 0),
                          }
                 }

        for invoice_id in invoice_ids:
            self._send_registered_email(cursor, uid, invoice_id, vals, report,
                                        datas, context=context)

        return True

    @job(queue=config.get('poweremail_render_queue', 'poweremail'))
    def _send_registered_email(self, cursor, uid, invoice_id, vals, report,
                               datas, context):
        '''
            Create a job to create and send email
        '''
        mail_obj = self.pool.get('poweremail.mailbox')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        attachment_obj = self.pool.get('ir.attachment')

        factura = factura_obj.browse(cursor, uid, invoice_id)

        email_to = factura.polissa_id.pagador_email

        reference = 'giscedata.polissa,{}'.format(factura.polissa_id.id)

        vals.update(
            {
                'pem_to': email_to,
                'reference': reference,
            })
        # Create email
        mail_id = mail_obj.create(cursor, uid, vals, context)

        # Attachment
        datas['form'].update({'ids': [invoice_id]})

        obj = netsvc.LocalService(report)
        (attachment, format) = obj.create(cursor, uid, [invoice_id], datas,
                                          context)

        vals_att = {
            'name': 'carta.{}'.format(format),
            'datas': base64.b64encode(attachment),
            'datas_fname': 'carta.{}'.format(format),
            'res_model': 'poweremail.mailbox',
            'res_id': mail_id
        }
        attachment_id = attachment_obj.create(cursor, uid, vals_att)
        mail_obj.write(cursor, uid, mail_id,
                       {'pem_attachments_ids': [(6,0,[attachment_id])]})

        # Send email
        mail_obj.send_this_mail(cursor, uid, [mail_id], context)

        return True

    def _splitInvoiceTypes(self, cursor, uid, invoice_ids):
        '''
            Split invoices in email and postal list
        '''
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        emailList = []
        postalList = []
        for factura in factura_obj.browse(cursor, uid, invoice_ids):
            if ('email' in factura.polissa_id.enviament and
                    factura.polissa_id.pagador_email):
                emailList.append(factura.id)
            else:
                postalList.append(factura.id)

        return emailList, postalList

    def _order_invoices_by_policy(self, cursor, uid, invoice_ids):
        '''
           Order invoices by policy id
        '''
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        invoice_vals = factura_obj.read(cursor, uid, invoice_ids,
                                        ['polissa_id'])

        invoices_list = [(x['id'], x['polissa_id'][1].zfill(10))
                         for x in invoice_vals]

        invoices_list = sorted(invoices_list, key=lambda x: x[1])
        return [x[0] for x in invoices_list]

    def _select_action(self, cr, uid, context=None):
        if not context:
            context = {}

        res = [
            ('postal_info', 'Consultar postals'),
            ('email_info', 'Consultar emails'),
            ('postal_print', 'Imprimir postals'),
            ('email_send', 'Enviar emails'),
            ('all_print', 'Imprimir totes')]

        if context.get('action', '') == 'cutoff':
            res.append(('postal_file', 'Generar fitxer postals'))

        return res

    _columns = {
        'data_carta': fields.char('Data carta', size=64),
        'data_accio': fields.char('Data acció', size=64),
        'send_to': fields.selection([('pagador', 'Pagador'),
                                     ('titular', 'Titular'),
                                     ('altre', 'Altre')],
                                    'Enviar a', required=True),
        'address_id': fields.many2one('res.partner.address', 'Adreça'),
        'selected_action': fields.selection(_select_action, 'Acció',
                                            required=True),
        'summary_text': fields.text('Resum', readonly=True),
        'state': fields.char('Estat', size=10),
        'postal_file': fields.binary('Fitxer postals')
    }

    _defaults = {
        'send_to': lambda *a: 'pagador',
        'selected_action': lambda *a: 'postal_info',
        'state': lambda *a: 'init',
        'summary_text': default_summary,
    }

WizardCartaImpagat()
