# -*- coding: utf-8 -*-
{
    "name": "Modelo 347. Personalizacion ES",
    "description": """
    * Modelo de cartas para el envio individual de comprobaciones
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Localisation/Accounting",
    "depends":[
        "l10n_ES_aeat_mod347",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "mod347_report.xml"
    ],
    "active": False,
    "installable": True
}
