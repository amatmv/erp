# -*- encoding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
from contaexpress_export.invoice import _select_contaexpress


class WizardInvoiceSupplier(osv.osv_memory):

    _inherit= 'wizard.invoice.supplier'

    def create_invoice(self, cursor, uid, ids, context=None):

        invoice_obj = self.pool.get('account.invoice')

        invoice_id = super(WizardInvoiceSupplier,
                           self).create_invoice(cursor, uid, ids, context)

        wizard = self.browse(cursor, uid, ids[0])
        invoice_obj.write(cursor, uid, [invoice_id],
                          {'contaexpress_type': wizard.contaexpress_type})

        return invoice_id

    _columns = {
        'contaexpress_type': fields.selection(_select_contaexpress,
                                              'Contaexpress Tipo',
                                              required=True,),
    }

    _defaults = {
        'contaexpress_type': lambda *a: 'interior',
    }

WizardInvoiceSupplier()
