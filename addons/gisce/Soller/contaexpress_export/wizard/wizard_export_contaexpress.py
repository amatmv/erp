# -*- encoding: utf-8 -*-

from osv import osv, fields


class WizardExportContaexpress(osv.osv_memory):

    _name = 'wizard.export.contaexpress'

    def action_export(self, cursor, uid, ids, context=None):

        export_obj = self.pool.get('contaexpress.export')
        invoice_obj = self.pool.get('account.invoice')

        wizard = self.browse(cursor, uid, ids[0])

        # Search for invoices
        search_params = [
            ('date_invoice', '>=', wizard.date_from),
            ('date_invoice', '<=', wizard.date_to),
            ('state', 'in', ('open', 'paid')),
            ('journal_id', '=', wizard.journal_id.id),
        ]

        invoice_ids = invoice_obj.search(cursor, uid, search_params)

        if wizard.action == 'moves':
            file_content = export_obj.export_invoice_moves(cursor, uid,
                                                           invoice_ids)
            filename = 'asientos.txt'
        elif wizard.action == 'invoices':
            file_content = export_obj.export_supplier_invoices(cursor, uid,
                                                               invoice_ids)
            filename = 'facturas.csv'

        wizard.write({'file': file_content,
                      'name': filename,
                      'state': 'end'})

        return True

    _state_selection = [
        ('init', 'Init'),
        ('end', 'End'),
    ]

    _action_selection = [
        ('moves', 'Account moves'),
        ('invoices', 'Invoice data'),
    ]

    _columns = {
        'journal_id': fields.many2one('account.journal',
                                      'Journal', required=True),
        'date_from': fields.date('Date from', required=True),
        'date_to': fields.date('Date to', required=True),
        'file': fields.binary('File'),
        'name': fields.char('File name', size=50),
        'state': fields.selection(_state_selection,
                                  'State', readonly=True),
        'action': fields.selection(_action_selection,
                                   'Action', required=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardExportContaexpress()
