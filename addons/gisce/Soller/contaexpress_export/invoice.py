# -*- encoding: utf-8 -*-

from osv import osv, fields


_select_contaexpress = [
    ('interior', 'Interiores'),
    ('interior_inversion', 'Interiores de inversión'),
    ('intra', 'Intracomunitarias'),
    ('intra_inversion', 'Intracomunitarias de inversión'),
    ('sujeto_pasivo', 'Inversión del sujeto pasivo'),
    ('arrendamiento', 'Arrendamientos'),
    ('importacion', 'Importación'),
    ('exportación', 'Exportación'),
    ('abono', 'De abono'),
    ('rectificativas', 'Rectificativas'),
    ('exenta', 'Exentas de IVA'),
    ('interior_inmo', 'Interiores inmobiliarias'),
    ('import_inversion', 'Importación inversión'),
    ('servicios_intra_auto', 'Servicios emitidos Intracomunitarios (auto-factura)'),
    ('entregas_intra', 'Entregas intracomunitarias'),
    ('nosujeta', 'Operaciones no sujetas'),
    ('ejecucion_obra', 'ISP/Ejecución de obra o Entrega inmuebles'),
    ('servicios_intra', 'Servicios recibidos intracomunitarios'),
    ('regimen_agrario', 'Régimen especial Agrario')
]


class AccountInvoiceSupplier(osv.osv):

    _inherit = 'account.invoice'

    _no_readonly_states = {'draft': [('readonly', False)]}

    _columns = {
        'contaexpress_type': fields.selection(_select_contaexpress,
                                              'Contaexpress Tipo',
                                              readonly=True,
                                              states=_no_readonly_states),
    }

    _defaults = {
        'contaexpress_type': lambda *a: 'interior',
    }

AccountInvoiceSupplier()
