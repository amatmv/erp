# -*- encoding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
from tools.translate import _
from babel.numbers import format_decimal
import base64
import tempfile
import csv
from invoice import _select_contaexpress


def contaexpress_date(date):

    return datetime.strptime(date,
                             '%Y-%m-%d').strftime('%d/%m/%Y')


class ContaexpressExport(osv.osv):

    _name = 'contaexpress.export'
    _auto = False

    def get_tipo_factura(self, cursor, uid, invoice_id, context=None):

        invoice_obj = self.pool.get('account.invoice')

        invoice = invoice_obj.browse(cursor, uid, invoice_id)

        if invoice.type.startswith('in_'):
            res = 'R'
        else:
            res = 'E'

        for tax in invoice.tax_line:
            if ('intracomunitario' in tax.name.lower() or
               'pasivo' in tax.name.lower()):
                return 'I'

        return res

    def get_invoice_moves(self, cursor, uid, invoice_ids, context=None):
        '''get moves from invoice_ids'''

        invoice_obj = self.pool.get('account.invoice')

        if not isinstance(invoice_ids, (list, tuple)):
            invoice_ids = [invoice_ids]

        res = []

        for invoice_id in invoice_ids:
            move_number = invoice_ids.index(invoice_id)
            invoice = invoice_obj.browse(cursor, uid, invoice_id)

            if not invoice.move_id:
                msg = _(u"Invoice {} has no associated move")
                raise osv.except_osv('Error',
                                     msg.format(invoice.number))

            for line in invoice.move_id.line_id:
                vals = (
                    line.account_id.code,
                    line.account_id.name,
                    move_number,
                    contaexpress_date(line.date),
                    line.name,
                    invoice.reference,
                    format_decimal(line.debit, locale='es_ES'),
                    format_decimal(line.credit, locale='es_ES'),
                )
                res.append(vals)

        return res

    def export_invoice_moves(self, cursor, uid, invoice_ids, context=None):

        register_obj = self.pool.get('contaexpress.register')

        to_export_ids = register_obj.get_unregistered(cursor, uid, invoice_ids)

        store = tempfile.TemporaryFile()

        lines = self.get_invoice_moves(cursor, uid, to_export_ids,
                                       context=context)
        # Mark invoices as exported
        register_obj.register(cursor, uid, to_export_ids)

        writer = csv.writer(store, delimiter='\t')
        for line in lines:
            writer.writerow(line)

        store.seek(0)
        content = base64.b64encode(store.read())
        store.close()

        return content

    def get_supplier_invoice_data(self, cursor, uid, invoice_ids,
                                  context=None):

        invoice_obj = self.pool.get('account.invoice')

        res = []
        for invoice_id in invoice_ids:
            invoice = invoice_obj.browse(cursor, uid, invoice_id)

            multiplier = 1
            if invoice.type == 'in_refund':
                multiplier = -1

            for key, value in _select_contaexpress:
                if invoice.contaexpress_type == key:
                    contaexpress_type = value
                    break

            vals = [
                invoice.number,
                invoice.origin,
                invoice.origin_date_invoice,
                invoice.date_invoice,
                invoice.period_id.name,
                invoice.partner_id.name,
                invoice.partner_id.vat,
                invoice.journal_id.code,
                invoice.reference or '',
                (invoice.rectifying_id and
                 invoice.rectifying_id.original_invoice_number or
                 ''),
                contaexpress_type,
                invoice.amount_untaxed * multiplier,
                invoice.amount_tax * multiplier,
                invoice.amount_total * multiplier,
            ]

            for tax in invoice.tax_line:
                line_vals = list(vals)
                tax_vals = [
                    tax.name,
                    tax.base_amount * multiplier,
                    tax.tax_amount * multiplier,
                    tax.account_id.code,
                ]
                line_vals.extend(tax_vals)
                res.append(line_vals)

        return res

    def export_supplier_invoices(self, cursor, uid, invoice_ids, context=None):

        cols = [
            'Número interno',
            'Número original',
            'Fecha factura',
            'Fecha Contabilizado',
            'Periodo',
            'Proveedor',
            'NIF',
            'Diario',
            'Referencia',
            'Rectifica a',
            'Tipo',
            'Base',
            'Impuestos',
            'Total',
            'Impuesto',
            'Base impuesto',
            'Cuota impuesto',
            'Cuenta impuesto',
        ]
        invoice_data = self.get_supplier_invoice_data(cursor, uid,
                                                      invoice_ids, context)

        store = tempfile.TemporaryFile()

        writer = csv.writer(store, delimiter=';')
        writer.writerow(cols)
        for line in invoice_data:
            writer.writerow(line)

        store.seek(0)
        content = base64.b64encode(store.read())
        store.close()

        return content

ContaexpressExport()


class ContaexpressRegister(osv.osv):

    _name = 'contaexpress.register'

    _order = 'create_date desc'

    def register(self, cursor, uid, invoice_ids, context=None):

        if not isinstance(invoice_ids, (list, tuple)):
            invoice_ids = [invoice_ids]

        for invoice_id in invoice_ids:
            self.create(cursor, uid, {'invoice_id': invoice_id})

        return True

    def already_registered(self, cursor, uid, invoice_id, context=None):

        invoice_obj = self.pool.get('account.invoice')

        search_params = [('invoice_id', '=', invoice_id)]

        invoice_ids = invoice_obj.search(cursor, uid, search_params)

        if invoice_ids:
            return True
        else:
            return False

    def get_unregistered(self, cursor, uid, invoice_ids, context=None):
        '''return a list of not registered invoices'''

        search_params = [('invoice_id', 'in', invoice_ids)]
        register_ids = self.search(cursor, uid, search_params)
        register_vals = self.read(cursor, uid, register_ids, ['invoice_id'])
        registered_invoice_ids = [x['invoice_id'][0] for x in register_vals]

        return list(set(invoice_ids) - set(registered_invoice_ids))

    _columns = {
        'invoice_id': fields.many2one('account.invoice', 'Invoice',
                                      select=True, required=True,
                                      readonly=True),
        'invoice_reference': fields.related('invoice_id', 'reference',
                                            string='Invoice reference',
                                            type='char',
                                            readonly=True),
        'invoice_amount': fields.related('invoice_id', 'amount_total',
                                         string='Invoice amount',
                                         type='float', digits=(16, 2),
                                         readonly=True),
        'create_date': fields.datetime('Exported', readonly=True),
    }

    _sql_constrainst = [
        ('invoice_unique',
         'unique invoice_id',
         'The invoice is already exported'),
    ]

ContaexpressRegister()
