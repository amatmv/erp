# -*- coding: utf-8 -*-
{
    "name": "Contaexpress export",
    "description": """
Contaexpress export
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Accounting",
    "depends":[
        "account_invoice_supplier"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "invoice_view.xml",
        "contaexpress_view.xml",
        "wizard/wizard_invoice_supplier_view.xml",
        "wizard/wizard_export_contaexpress_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
