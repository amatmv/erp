# -*- coding: utf-8 -*-
{
    "name": "Giscedata facturacio payment date",
    "description": """
Giscedata facturacio last payment date:
    """,
    "version": "0-dev",
    "author": "Toni Llado",
    "category": "Soller",
    "depends":[
        "account_invoice_payment_date",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "facturacio_view.xml"
    ],
    "active": False,
    "installable": True
}
