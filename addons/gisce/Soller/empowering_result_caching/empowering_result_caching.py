# -*- coding: utf-8 -*-
from osv import osv

import time
import calendar
from datetime import datetime
from dateutil.relativedelta import relativedelta

import xmlrpclib

from oorq.decorators import job
from mongodb_backend.mongodb2 import mdbpool

from amoniak.caching import OT101Caching, OT103Caching
from amoniak.caching import OT201Caching, OT503Caching

from tools import config

# WARNING: if you have more than one worker one of them
# can take a job before another one has finished so
# to ensure the order of the jobs use only one worker


class EmpoweringOTCaching(osv.osv):
    """ Base class to for ot caching, childclass must override the
    " _name, _ot_obj and _periodicity if needed
    """

    _name = 'empowering.caching'

    _ot_obj = None

    _periodicity = 'month'

    @job(queue='empowering')
    def cache_contract(self, cursor, uid, contract, period=None, context=None):
        """
        " Cache the contract at the given period (or periods) and validate the
        " the results. Incorrect values won't be stored.
        """
        self.pull_contract(cursor, uid, contract, period, context)
        self.validate_contract(cursor, uid, contract, period, context)

    def pull_contract(self, cursor, uid, contract, period=None,
                      context=None):
        """ Will ask for results of online tool `ot_code` for the specidied
        " contract for ALL periods if is not specidied in period param.
        " Pulled results will be stored in the
        " database.
        """
        empowering_service = self.pool.get('empowering.api').service
        ot_obj = self._ot_obj(empowering_service, mdbpool.get_db())
        ot_obj.pull_contract(contract, period)

    def validate_contract(self, cursor, uid, contract, period=None,
                          context=None):
        """ Validate empowering results comparing to the actual DB measures
        """
        polissa_obj = self.pool.get('giscedata.polissa')
        tg_billing_obj = self.pool.get('tg.billing')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        polissa_search_params = [
            ('name', '=', contract)
        ]

        polissa_ids = polissa_obj.search(cursor, uid, polissa_search_params,
                                         context={'active_test': False})
        if not polissa_ids:
            raise osv.except_osv('Polissa contract not found')

        polissa = polissa_obj.browse(cursor, uid, polissa_ids[0])

        lectures_search_params = [
            ('type', '=', self._periodicity),
            ('value', '=', 'i'),
            ('valid', '=', 1),
            ('period', '=', 0),
        ]
        if period:
            # If period is specified only this period will be validated
            start_period_date = datetime.strptime(period[0:4] + '-' +
                                                  period[4:6] + '-01',
                                                  '%Y-%m-%d')
            end_period_date = start_period_date + relativedelta(months=1)

            start_period_str = datetime.strftime(start_period_date, '%Y-%m-%d')
            end_period_str = datetime.strftime(end_period_date, '%Y-%m-%d')

            # If more than one meter in same month this will fail,
            # but we asume that this is a corner case and will not
            # be validated.
            meter_id = polissa.comptadors_actius(start_period_str,
                                                 end_period_str,
                                                 order='data_alta desc')[0]
            meter = meter_obj.browse(cursor, uid, meter_id)
            lectures_extra_params = [
                ('name', '=', meter.build_name_tg()),
                ('date_end', '=', end_period_str)
            ]
            lectures_search_params.extend(lectures_extra_params)
            measures_ids = tg_billing_obj.search(cursor, uid,
                                                 lectures_search_params,
                                                 order="date_end asc")
        else:
            # Validate many periods
            measures_ids = []
            for meter in polissa.comptadors:
                # Take all measures of each meter while the meter
                # was/is associtated to the contract
                if not meter.tg:
                    continue

                meter_name = meter.build_name_tg()
                start_meter_str = meter.data_alta
                end_meter_str = meter.data_baixa or None

                search_params = list(lectures_search_params)
                search_params.extend([
                    ('name', '=', meter_name),
                    ('date_begin', '>=', start_meter_str),
                ])
                if end_meter_str:
                    search_params.extend([
                        ('date_begin', '<=', end_meter_str),
                    ])
                ids = tg_billing_obj.search(cursor, uid, search_params,
                                            order="date_begin asc")
                measures_ids.extend(ids)

        values = {}
        for measure_id in measures_ids:
            measure = tg_billing_obj.read(cursor, uid, measure_id,
                                          ['ai', 'date_begin'])
            date_str = datetime.strptime(measure['date_begin'],
                                         '%Y-%m-%d %H:%M:%S').strftime('%Y%m')
            values[date_str] = measure['ai']

        empowering_service = self.pool.get('empowering.api').service
        ot_obj = self._ot_obj(empowering_service, mdbpool.get_db())
        ot_obj.validate_contract(values, contract, period)

    @job(queue='empowering')
    def notify_result(self, cursor, uid, ot_code=None, contract=None,
                      period=None, validation_date='today', clear_errors=True,
                      context=None):
        """ Notify validating errors by email if no parameters given notify
        " the result of the validation done the same day
        "
        " WARNING if more than one worker the notification can be done
        " before the validate process fullily finish.
        """
        mail_obj = self.pool.get('poweremail.mailbox')

        empowering_service = self.pool.get('empowering.api').service
        ot_obj = self._ot_obj(empowering_service, mdbpool.get_db())

        email_from = config['email_from']
        email_to = 'informatica@el-gas.es'

        if not validation_date:
            validation_date = time.strftime('%Y-%m-%d')

        report = ot_obj.error_report(ot_code, contract, period, validation_date)
        subject = 'Empowering report: ot %s - contract %s - period %s - date %s'
        subject %= (
            ot_code and ot_code or 'all',
            contract and contract or 'all',
            period and period or 'all',
            validation_date and validation_date or 'all'
        )

        mail_obj.send_mail_generic(cursor, uid, email_from, subject, report,
                                   email_to=email_to, context=context)
        if clear_errors:
            ot_obj.error_clear(ot_code, contract, period, validation_date)

    def monthly_caching(self, cursor, uid, period=None, all_periods=False):
        """ Function executed monthly by cron to cache the results
        " If no period specified cache the previous month
        " If you want to cache all periods, especify period=None
        " and all_periods=True
        """

        if not period and not all_periods:
            prev_month_date = datetime.now() + relativedelta(months=-1)
            period = datetime.strftime(prev_month_date, '%Y%m')

        polissa_obj = self.pool.get('giscedata.polissa')

        if not all_periods:
            year = period[:4]
            month = period[-2:]
            last_day = calendar.monthrange(int(year), int(month))[1]
            first_day_of_month = '%s-%s-01' % (year, month)
            last_day_of_month = '%s-%s-%s' % (year, month, last_day)
            search_params = [
                ('data_alta', '<=', last_day_of_month),
                 '|',
                     ('data_baixa', '=', False),
                     ('data_baixa', '>=', first_day_of_month),
            ]
        else:
            search_params = []

        polissa_ids = polissa_obj.search(cursor, uid, search_params)
        polissa_names = [p['name'] for p in polissa_obj.read(cursor, uid,
                                                             polissa_ids,
                                                             ['name'])]

        ot_codes = ['ot101', 'ot103', 'ot201', 'ot503']
        for ot_code in ot_codes:
            ot_obj = self.pool.get('empowering.caching.%s' % ot_code)
            for contract in polissa_names:
                try:
                    ot_obj.cache_contract(cursor, uid, contract, period)
                except xmlrpclib.Fault:
                    pass

        # All ot_obj has this method
        #                  ot_code contract
        ot_obj.notify_result(cursor, uid, None, None, period)

    def get_cached(self, cursor, uid, contract, period):
        """
        " Return the cached value for the contract at the given period
        """
        polissa_obj = self.pool.get('giscedata.polissa')
        # If we are in comer we have to search the contract
        # by name and return abonat
        if 'abonat' in polissa_obj.fields_get_keys(cursor, uid):
            #Search the contract by name
            search_params = [('name', '=', contract)]
            polissa_ids = polissa_obj.search(cursor, uid, search_params)
            if polissa_ids:
                contract = polissa_obj.read(cursor, uid, polissa_ids,
                                            ['abonat'])[0]['abonat']

        empowering_service = self.pool.get('empowering.api').service
        online_tool = self._ot_obj(empowering_service, mdbpool.get_db())
        return online_tool.get_cached(contract, period)

EmpoweringOTCaching()


class EmpoweringOT101Caching(EmpoweringOTCaching):
    _name = 'empowering.caching.ot101'

    _ot_obj = OT101Caching

EmpoweringOT101Caching()


class EmpoweringOT103Caching(EmpoweringOTCaching):
    _name = 'empowering.caching.ot103'

    _ot_obj = OT103Caching

EmpoweringOT103Caching()


class EmpoweringOT201Caching(EmpoweringOTCaching):
    _name = 'empowering.caching.ot201'

    _ot_obj = OT201Caching

EmpoweringOT201Caching()


class EmpoweringOT503Caching(EmpoweringOTCaching):
    _name = 'empowering.caching.ot503'

    _ot_obj = OT503Caching

EmpoweringOT503Caching()
