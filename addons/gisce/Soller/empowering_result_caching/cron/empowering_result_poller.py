# -*- encoding: utf-8 -*-

import sys
from datetime import datetime
from dateutil.relativedelta import relativedelta
import calendar

from erppeek import Client
import xmlrpclib

import config

if __name__ == '__main__':
    erp_conn = Client(config.erp_server, db=config.erp_db,
                      user=config.erp_user, password=config.erp_pass)
    """
    " Donwload the results for the specified period i.e:
    "    python empowering_result_poller.py 201401 # January 2014
    "
    " If no period specified download results from last month
    " If --all specified download all results from all periods i.e.:
    "    python empowering_result_poller.py --all
    """

    ot_codes = ['ot101', 'ot103', 'ot201', 'ot503']

    if len(sys.argv) > 1:
        if sys.argv[1] == '--all':
            period = None
        else:
            period = sys.argv[1]
    else:
        prev_month_date = datetime.now() + relativedelta(months=-1)
        period = datetime.strftime(prev_month_date, '%Y%m')

    polissa_obj = erp_conn.model('giscedata.polissa')

    if period:
        year = period[:4]
        month = period[-2:]
        last_day = calendar.monthrange(int(year), int(month))[1]
        first_day_of_month = '%s-%s-01' % (year, month)
        last_day_of_month = '%s-%s-%s' % (year, month, last_day)
        search_params = [
            ('data_alta', '<=', last_day_of_month),
             '|',
                ('data_baixa', '=', False),
                ('data_baixa', '>=', first_day_of_month),
        ]
    else:
        search_params = []

    polissa_ids = polissa_obj.search(search_params)
    polissa_names = [p['name'] for p in polissa_obj.read(polissa_ids, ['name'])]

    for ot in ot_codes:
        ot_obj = erp_conn.model('empowering.caching.%s' % ot)
        for contract in polissa_names:
            try:
                ot_obj.cache_contract(contract, period)
            except xmlrpclib.Fault:
                pass

    # All ot_obj has this method
    #                  ot_code contract
    ot_obj.notify_result(None, None, period)
