# -*- coding: utf-8 -*-
{
    "name": "Empowering Result Caching",
    "description": """
    This module provide :
        * Integration between Empowering service to cache results
    """,
    "version": "0-dev",
    "author": "El Gas, S.A.",
    "category": "Misc",
    "depends":[
        "base",
        "empowering_api",
        "oorq",
        "poweremail"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "empowering_result_caching_scheduler.xml"
    ],
    "active": False,
    "installable": True
}
