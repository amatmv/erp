# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time
from datetime import date


class WizardSollerDividirMoviment(osv.osv_memory):

    _name = 'wizard.soller.dividir.moviment'

    def dividir_moviment(self, cr, uid, ids, context=None):
        '''funcion que divide un movimiento
        asignando un lote de produccion incremental para linea creada'''

        wizard = self.browse(cr, uid, ids[0])
        moviment_obj = self.pool.get("stock.move")
        lot_obj = self.pool.get("stock.production.lot")
        serial = wizard.serial
        moviment_ids = context.get('active_ids', [])

        for moviment in moviment_obj.browse(cr, uid, moviment_ids):
            moviment_id = moviment.id
            product_id = moviment.product_id.id
            lot_id = lot_obj.create(cr, uid, {'name': serial, 'product_id': product_id})
            quantitat = moviment.product_qty
            vals = {'product_qty': 1, 
                    'product_uos_qty': moviment.product_id.uos_coeff,
                    'prodlot_id': lot_id}
            moviment.write(vals)
            while quantitat > 1:
                serial = str(int(serial)+1)
                lot_id = lot_obj.create(cr, uid, {'name': serial, 'product_id': product_id})
                moviment_obj.copy(cr, uid, moviment_id, {'state': moviment.state, 'prodlot_id': lot_id})
                quantitat -=1

        return {}
    
    _columns = {
        'serial':fields.char('Nº de serie', size=64, required=True, readonly=False),
        
    }
WizardSollerDividirMoviment()
