# -*- coding: utf-8 -*-
{
    "name": "Extensió Stock Soller",
    "description": """
Extensió de les funcionalitats de Stock
    * Dividir una línea d'un albaran en números de serie consecutius
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "stock",
        "mrp"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_dividir_moviment_view.xml",
        "stock_view.xml"
    ],
    "active": False,
    "installable": True
}
