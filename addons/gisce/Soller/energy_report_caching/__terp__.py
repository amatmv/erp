# -*- coding: utf-8 -*-
{
    "name": "Energy Report Caching",
    "description": """
    This module provide :
        * Show results for energy reports
    """,
    "version": "0-dev",
    "author": "Electrica Sollerense SAU",
    "category": "Misc",
    "depends":[
        "base",
        "giscedata_polissa",
        "mongodb_backend"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
