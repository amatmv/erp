# -*- coding: utf-8 -*-
from osv import osv
from mongodb_backend.mongodb2 import mdbpool
from datetime import datetime
from dateutil.relativedelta import relativedelta


class ReportOTCaching(osv.osv):

    _name = 'empowering.caching'
    _auto = False

    _ot_code = None
    _period_key = None

    _hidden_keys = ['companyId', '_updated', '_etag',
                    '_id', '_created', '_links']

    def get_data(self, cursor, uid, contract, period, collection):

        query = {'contractId': contract,
                 self._period_key: int(period)}
        cached = [x for x in collection.find(query)]
        for elem in cached:
            for hidden_key in self._hidden_keys:
                if hidden_key in elem:
                    elem.pop(hidden_key)

        return {"_items": cached}

    def get_cached(self, cursor, uid, contract, period):
        """
        " Return the cached value for the contract at the given period
        """
        polissa_obj = self.pool.get('giscedata.polissa')
        # If we are in comer we have to search the contract
        # by name and return abonat
        if 'abonat' in polissa_obj.fields_get_keys(cursor, uid):
            #Search the contract by name
            search_params = [('name', '=', contract)]
            polissa_ids = polissa_obj.search(cursor, uid, search_params)
            if polissa_ids:
                contract = polissa_obj.read(cursor, uid, polissa_ids,
                                            ['abonat'])[0]['abonat']

        mongo_connection = mdbpool.get_db()
        collection = getattr(mongo_connection, self._ot_code)
        return self.get_data(cursor, uid, contract, period, collection)

ReportOTCaching()


class ReportOT101Caching(ReportOTCaching):
    _name = 'empowering.caching.ot101'

    _ot_code = 'ot101'
    _period_key = 'month'

ReportOT101Caching()


class ReportOT103Caching(ReportOTCaching):
    _name = 'empowering.caching.ot103'

    _ot_code = 'ot103'
    _period_key = 'month'

    def get_data(self, cursor, uid, contract, period, collection):
        month_range = 13
        end_period_date = datetime.strptime(period, '%Y%m')
        end_period_date = datetime(year=end_period_date.year,
                                   month=end_period_date.month,
                                   day=1)
        end_period = int(period)
        start_period_date = (end_period_date +
                             relativedelta(months=-(month_range)))
        start_period = int(start_period_date.strftime('%Y%m'))

        query = {'contractId': contract,
                 'month': {'$gt': start_period,
                           '$lte': end_period}}
        res = collection.find(query).sort('month', 1)
        cached = [x for x in res]

        for elem in cached:
            for hidden_key in self._hidden_keys:
                if hidden_key in elem:
                    elem.pop(hidden_key)

        return {"_items": cached}


ReportOT103Caching()


class ReportOT201Caching(ReportOTCaching):
    _name = 'empowering.caching.ot201'

    _ot_code = 'ot201'
    _period_key = 'month'

ReportOT201Caching()


class ReportOT503Caching(ReportOTCaching):
    _name = 'empowering.caching.ot503'

    _ot_code = 'ot503'
    _period_key = 'day'

    def get_data(self, cursor, uid, contract, period, collection):

        end_period_date = datetime.strptime(period, '%Y%m')

        end_period_date += relativedelta(months=1)
        end_period_date = datetime(year=end_period_date.year,
                                   month=end_period_date.month,
                                   day=1)
        end_period_date -= relativedelta(days=1)
        end_period = int(end_period_date.strftime('%Y%m%d'))

        # Set the start period at the begining of the month
        start_period_date = datetime(year=end_period_date.year,
                                     month=end_period_date.month,
                                     day=1)
        start_period = int(start_period_date.strftime('%Y%m%d'))

        query = {'contractId': contract,
                 'day': {'$gte': start_period,
                         '$lte': end_period}}
        res = collection.find(query).sort('day', 1)
        cached = [x for x in res]

        for elem in cached:
            for hidden_key in self._hidden_keys:
                if hidden_key in elem:
                    elem.pop(hidden_key)

        return {"_items": cached}

ReportOT503Caching()
