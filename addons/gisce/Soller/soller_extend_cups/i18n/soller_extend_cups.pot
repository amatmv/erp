# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* soller_extend_cups
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2012-03-28 16:07:30+0000\n"
"PO-Revision-Date: 2012-03-28 16:07:30+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: soller_extend_cups
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: soller_extend_cups
#: model:ir.module.module,description:soller_extend_cups.module_meta_information
msgid "\n"
"Funcionalitats extra pel cups\n"
"    * Afegir la descripció del CT al CUPS\n"
"    "
msgstr ""

#. module: soller_extend_cups
#: model:ir.module.module,shortdesc:soller_extend_cups.module_meta_information
msgid "Soller Extend CUPS"
msgstr ""

#. module: soller_extend_cups
#: field:giscedata.cups.ps,et_nom:0
msgid "Nom ET"
msgstr ""

