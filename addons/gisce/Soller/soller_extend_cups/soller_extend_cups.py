# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

class SollerExtendCUPS(osv.osv):

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def _get_cups_from_ct(self, cr, uid, ids, context=None):
        """Funció que retorna els ids dels cups associats al CT per nom"""
        
        ct_obj = self.pool.get('giscedata.cts')
        cups_obj = self.pool.get('giscedata.cups.ps')
        res=[]
        vals = ct_obj.read(cr,uid, ids, ['name'])
        for val in vals:
            #cerquem tots els cups amb aquest nom de ct
            search_params = [('et','=',val.get('name',False)),]
            cups_ids = cups_obj.search(cr, uid, search_params)
            res.extend(cups_ids)
        return res

    def _get_ct_nom(self, cr, uid, ids, name, arg, context={}):
        """Funció que ens dona el nom llarg del CT associat al cups"""
        
        res = {}

        ct_obj = self.pool.get('giscedata.cts')

        for cups in self.browse(cr, uid, ids, context):
            search_params = [('name','=',cups.et),]
            ct_ids = ct_obj.search(cr,uid, search_params)
            for ct in ct_obj.browse(cr, uid, ct_ids):
                res[cups.id] = ct.descripcio

        return res
    

    
    _columns = {
        'et_nom': fields.function(_get_ct_nom, method=True, readonly=True,
                        type='char', size=128, string='Nom ET', 
                        store={'giscedata.cts': (_get_cups_from_ct, ['name', 'descripcio'], 20),
                               'giscedata.cups.ps': (lambda self, cr, uid, ids, c={}: ids, ['et'], 20),
                              }),        
        
    }

SollerExtendCUPS()
