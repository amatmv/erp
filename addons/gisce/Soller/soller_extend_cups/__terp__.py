# -*- coding: utf-8 -*-
{
    "name": "Soller Extend CUPS",
    "description": """
Funcionalitats extra pel cups
    * Afegir la descripció del CT al CUPS
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_cups_distri",
        "giscedata_cts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_extend_cups_view.xml"
    ],
    "active": False,
    "installable": True
}
