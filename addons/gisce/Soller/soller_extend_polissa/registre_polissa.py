# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

GET_STATES_REGISTRE = [('add', 'Assignat'),
                       ('del', 'Cancel·lat')]

class RegistrePolissa(osv.osv):
    
    _name = 'registre.polissa'
    _description = 'Registre'

    _order = 'name desc'
    
    _columns = {
        'name':fields.char('Número', size=64, required=True, readonly=False),
        'polissa_id':fields.many2one('giscedata.polissa', 'Pòlissa', 
                                     readonly=True, required=False),
        'create_uid':fields.many2one('res.users', 'Creado por', 
                                     readonly=True, required=False),
        'create_date': fields.datetime('Data creació', readonly=True,
                                       required=False),
        'write_uid':fields.many2one('res.users', 'Modificado por', 
                                     readonly=True, required=False),
        'write_date': fields.datetime('Data modificació', readonly=True,
                                      required=False),
        'state': fields.selection(GET_STATES_REGISTRE, 'Estat', required=True),
        
    }

    _defaults = {
        'state': lambda *a: 'add',
        
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'Número de pòlissa duplicat!'),
        
    ]
RegistrePolissa()

class RegistrePolissaGiscedata(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def update_registre(self, cr, uid, ids, codi, mode='add', context={}):

        registre_obj = self.pool.get('registre.polissa')

        for polissa in self.browse(cr, uid, ids):
            vals = {}
            search_params = [('name','=', codi)]
            registre_id = registre_obj.search(cr, uid, search_params)
            if registre_id:
                #Si existeix, es que es cancela
                vals.update({'state': mode,})
                if mode == 'del':
                    vals.update({'polissa_id': None})
                registre_obj.write(cr, uid, registre_id, vals)
            else:
                #Si no, el creem
                vals.update({'name': codi,
                             'state': mode})
                if mode == 'add':
                    vals.update({'polissa_id': polissa.id})
                registre_id = registre_obj.create(cr, uid, vals)

        return True

RegistrePolissaGiscedata()
