# -*- coding: utf-8 -*-
import pooler
import netsvc


def migrate(cursor, installed_version):
    """
    Migration to update the is_signed field of operador contracts.
    @param cursor:
    @param installed_version:
    @return:
    """
    logger = netsvc.Logger()
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    contract_obj = pool.get('giscedata.polissa')
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u"Creating default is_signed value for giscedata.polissa contracts"
    )

    cursor.execute(
        "UPDATE giscedata_polissa "
        "SET reception_date = data_firma_contracte, is_signed = true "
        "WHERE data_firma_contracte IS NOT null AND is_signed = false "
        "and state not in ('esborrany', 'validar')"
    )
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')
