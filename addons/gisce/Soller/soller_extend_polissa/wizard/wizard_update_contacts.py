# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from giscedata_comerdist import OOOPPool, Sync
from giscedata_facturacio.giscedata_polissa import NOTIFICACIO_SELECTION
import time
import tools


class WizardUpdatePolissaContacts(osv.osv_memory):

    _name = 'wizard.update.polissa.contacts'

    _blacklist = ['titular_id', 'pagador_id', 'altre_p_id',
                  'direccio_titular_id', 'direccio_pagament_id',
                  'direccio_notificacio_id',
                  'bank_id', 'state', 'progress', 'notificacio_id']

    _pagador_selection = [('titular', 'Titular'),
                          ('altre_p', 'Altre'),
                          ('comercialitzadora', 'Comercialitzadora')]

    def default_get(self, cr, uid, fields, context=None):
        '''Por defecto rellenamos los campos con los valores de la poliza'''

        polissa_obj = self.pool.get('giscedata.polissa')
        address_obj = self.pool.get('res.partner.address')

        res = super(WizardUpdatePolissaContacts,
                    self).default_get(cr, uid, fields, context)

        polissa_id = context.get('active_id', [])

        field_list_original = self.fields_get_keys(cr, uid)

        field_list = [x for x in field_list_original
                      if x not in self._blacklist]
        polissa_fields = polissa_obj.read(cr, uid, [polissa_id],
                                          field_list, context)[0]
        for field in field_list:
            if isinstance(polissa_fields[field], list)\
               or isinstance(polissa_fields[field], tuple):
                res[field] = polissa_fields[field][0]
                res['%s_id' % field] = polissa_fields[field][0]
            else:
                res[field] = polissa_fields[field]
                res['%s_id' % field] = polissa_fields[field]

        if not res['pagador']:
            res['pagador'] = address_obj.browse(cr, uid,
                                    res['direccio_pagament']).partner_id.id
            res['pagador_id'] = address_obj.browse(cr, uid,
                                    res['direccio_pagament']).partner_id.id
        if not res['altre_p']:
            res['altre_p'] = address_obj.browse(cr, uid,
                                    res['direccio_notificacio']).partner_id.id
            res['altre_p_id'] = address_obj.browse(cr, uid,
                                    res['direccio_notificacio']).partner_id.id
        #HARD eliminamos los dos campos id que son campos lista
        del res['notificacio_id']
        del res['pagador_sel_id']
        return res

    def get_changes(self, cursor, uid, ids, polissa, fields, context=None):

        wizard = self.browse(cursor, uid, ids[0])
        polissa_vals = polissa.read(fields, context)[0]
        wizard_vals = wizard.read(fields, context)[0]
        del polissa_vals['id']
        del wizard_vals['id']
        res = {}
        for field in fields:
            if isinstance(polissa_vals.get(field), (list, tuple)):
                pol_val = polissa_vals.get(field)[0]
            else:
                pol_val = polissa_vals.get(field)
            if pol_val != wizard_vals.get(field):
                res.setdefault(field, {'old': pol_val,
                                       'new': wizard_vals.get(field)})
        return res

    def get_observacions(self, cursor, uid, ids, polissa_id,
                         fields, context=None):
        """Obtenim les observacions amb els canvis.
        """
        if not context:
            context = {}
        polissa_id = context.get('active_id', False)
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
        changes = self.get_changes(cursor, uid, ids, polissa, fields, context)
        changes_string = u'Canvis realitzats en actualitzar contactes:\n'
        polissa_fields = polissa_obj.fields_get(cursor, uid, fields)
        for field, change in changes.items():
            field_name = polissa_fields[field].get('string', field)
            if 'relation' in polissa_fields[field]:
                model_obj = self.pool.get(polissa_fields[field].\
                                          get('relation'))
                old_value = change['old'] and model_obj.name_get(cursor, uid,
                                                [change['old']],
                                                context)[0][1] or '()'
                new_value = change['new'] and model_obj.name_get(cursor, uid,
                                                [change['new']],
                                                context)[0][1] or '()'
            elif polissa_fields[field].get('type', False) == 'selection':
                for item in polissa_fields[field]['selection']:
                    if item[0] == change['old']:
                        old_value = item[1]
                    if item[0] == change['new']:
                        new_value = item[1]
            else:
                old_value = change['old']
                new_value = change['new']
            changes_string += u'• %s: %s → %s\n' % (field_name,
                                                    old_value,
                                                    new_value)
        return changes_string

    def onchange_field(self, cr, uid, ids, field, value, context=None):

        res = {'value': {}, 'domain': {}, 'warning': {}}

        res['value'].update({'%s_id' % field: value})

        return res

    def action_confirmar(self, cr, uid, ids, context=None):

        self.browse(cr, uid, ids[0]).write({'state': 'end'})

    def check_errors(self, cr, uid, ids, canvis, context={}):

        address_obj = self.pool.get('res.partner.address')

        #La direccion del titular tiene que ser del titular
        if (canvis.get('direccio_titular', False) and
            address_obj.browse(cr, uid,
                    canvis['direccio_titular']).partner_id.id
            != canvis['titular']):
            raise osv.except_osv('Error',
                            'L\'adreça del titular no pertany al titular!')

        #La direccion de pago tiene que ser del pagador
        if (canvis.get('direccio_pagament', False) and
            address_obj.browse(cr, uid,
                    canvis['direccio_pagament']).partner_id.id\
            != canvis['pagador']):
            raise osv.except_osv('Error',
                            'L\'adreça de pagament no pertany al pagador!')

        #La cuenta tiene que ser del pagador obligatoriamente
        if (canvis.get('bank', False) and
            self.pool.get('res.partner.bank').browse(cr,
                            uid, canvis['bank']).partner_id.id
            != canvis['pagador']):
            raise osv.except_osv('Error',
                            'El compte bancari no pertany al pagador!')

        #La direccion de notificacion tiene
        #que ser del contacto de notificacion
        if (canvis.get('direccio_notificacio', False) and
            address_obj.browse(cr, uid,
                    canvis['direccio_notificacio']).partner_id.id
            != canvis['altre_p']):
            raise osv.except_osv('Error',
                            u"L\'adreça de notificació no pertany "
                            u"al contacte de notificació!")

        return True

    def action_change(self, cr, uid, ids, context={}):

        polissa_obj = self.pool.get('giscedata.polissa')
        modcont_obj = self.pool.get('giscedata.polissa.modcontractual')
        address_obj = self.pool.get('res.partner.address')

        wizard = self.browse(cr, uid, ids[0])

        canvis = {}
        polissa_id = context.get('active_id', [])
        field_list_original = self.fields_get_keys(cr, uid)
        field_list = [x for x in field_list_original
                      if x not in self._blacklist]
        polissa_fields = polissa_obj.read(cr, uid, [polissa_id],
                                          field_list, context)[0]
        modcont_activa_id = polissa_obj.read(cr, uid, [polissa_id],
                                ['modcontractual_activa'],
                                context)[0]['modcontractual_activa'][0]
        wizard_fields = wizard.read(field_list, context)[0]

        for field in field_list:
            if isinstance(wizard_fields[field], list)\
                or isinstance(wizard_fields[field], tuple):
                canvis[field] = wizard_fields[field][0]
            else:
                canvis[field] = wizard_fields[field]
        if not canvis['pagador']:
            canvis['pagador'] = address_obj.browse(cr, uid,
                                canvis['direccio_pagament']).partner_id.id
        if not canvis['altre_p']:
            canvis['altre_p'] = address_obj.browse(cr, uid,
                                canvis['direccio_notificacio']).partner_id.id
        #Hacemos comprobaciones antes de escribir
        self.check_errors(cr, uid, ids, canvis, context)

        modcont = modcont_obj.browse(cr, uid, modcont_activa_id, context)
        #Generamos las observaciones con los cambios
        changes_str = self.get_observacions(cr, uid, ids, polissa_id,
                                            canvis.keys(), context)
        #Primero escribimos los cambios con sincronizacion automatica
        #para la poliza
        context.update({'sync': True})
        polissa_obj.write(cr, uid, [polissa_id], canvis, context)
        #Para la modificicacion contractual,
        #como no sincroniza casi ningun campo
        #y para no tocar la configuracion lo
        #tenemos que hacer de forma manual
        #Escribimos en local sin sincronizar
        context.update({'sync': False})
        #Primero quitamos los campos que no son del modelo modcont
        modcont_fields = modcont.fields_get_keys()
        for canvi in canvis.keys():
            if canvi not in modcont_fields:
                del canvis[canvi]
        modcont.write(canvis, context)
        #Actualizamos las observaciones con los cambios
        user = self.pool.get('res.users').browse(cr, uid, uid)
        observacions = ''
        log_line_long = 82  # 80 + 2 \n
        log_line_text = '\n----- %s - %s (%s) %%s\n' % (
            time.strftime('%d/%m/%Y %H:%M'), user.name, user.login
        )
        if modcont.observacions:
            observacions += modcont.observacions
        observacions += log_line_text % ('-' * (log_line_long
                                                - len(log_line_text)))
        observacions += changes_str
        modcont.write({'observacions': observacions})
        #Ahora vamos a escribir en remoto
        context.update({'sync': True})
        canvis_remots = {}
        if modcont.must_be_synched(context):
            config_id = modcont.get_config().id
            ooop = OOOPPool.get_ooop(cr, uid, config_id)
            sync = Sync(cr, uid, ooop, 'giscedata.polissa.modcontractual',
                        config=config_id)
            obj = sync.get_object(modcont.id)
            if obj:
                #Para saber que campos sincronizar utilizaremos
                #la configuracion de sincronizacion de la poliza
                sync_pol = Sync(cr, uid, ooop, 'giscedata.polissa',
                                config=config_id)
                polissa_remota = sync_pol.get_object(polissa_id)
                #Quitamos los campos blacklisted de la lista a sincronizar
                blacklisted = sync_pol.get_blacklisted_fields()
                sync_fields = [x for x in field_list if x not in blacklisted]
                local_fields = sync.get_local_fields()
                #Quitamos los campos que no son del modelo modcontractual
                sync_fields = [x for x in sync_fields
                               if x in local_fields.keys()]
                #mapeamos los campos para leerlos despues
                map_fields = sync_pol.get_mapping_fields()
                for map_field_name, map_field_value in map_fields.items():
                    if map_field_name in sync_fields:
                        sync_fields.remove(map_field_name)
                        sync_fields.append(map_field_value)
                #leemos los campos de la poliza remota
                #que ya hemos sincronizado anteriormente
                for field_name in sync_fields:
                    if local_fields[field_name].get('relation', False):
                        canvis_remots[field_name] =\
                         polissa_remota.read([field_name])[field_name].id
                    else:
                        canvis_remots[field_name] = canvis[field_name]

                obj.write(canvis_remots)

        return {}

    _columns = {
        'titular': fields.many2one('res.partner', 'Titular', required=True,),
        'pagador': fields.many2one('res.partner', 'Pagador', required=True),
        'altre_p': fields.many2one('res.partner', 'Contacte Alternatiu',
                                  required=True),
        'direccio_titular': fields.many2one('res.partner.address',
                                           'Adreça titular', required=False),
        'direccio_pagament': fields.many2one('res.partner.address',
                                            'Adreça pagament', required=True),
        'direccio_notificacio': fields.many2one('res.partner.address',
                                               'Adreça notificació',
                                               required=True),
        'bank': fields.many2one('res.partner.bank', 'Compte bancari',
                               required=False),
        #Campos id para facilitar la labor de saber
        #que id le estamos asignando realmente
        'titular_id': fields.integer('ID Titular', readonly=True),
        'pagador_id': fields.integer('ID Pagador', readonly=True),
        'altre_p_id': fields.integer('ID Contacte alternatiu', readonly=True),
        'direccio_titular_id': fields.integer('ID Adreça titular',
                                              readonly=True),
        'direccio_pagament_id': fields.integer('ID Adreça pagament',
                                               readonly=True),
        'direccio_notificacio_id': fields.integer('ID Adreça notificació',
                                                  readonly=True),
        'bank_id': fields.integer('ID Compte bancari', readonly=True),
        'state': fields.selection([('init', 'Init'),
                                  ('end', 'End'),
                                 ], 'State', select=True, readonly=True),
        'notificacio': fields.selection(NOTIFICACIO_SELECTION,
                                       'Persona notificació', required=True),
        'pagador_sel': fields.selection(_pagador_selection, 'Persona pagadora',
                                        required=True),
    }

    _defaults = {
        'state': lambda *a: 'init',

    }

WizardUpdatePolissaContacts()
