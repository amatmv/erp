# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields


class GiscedataPolissaSollerExtend(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy(self, cr, uid, id, default=None, context=None):
        """Canviem la funcionalitat del copy() per adaptar-ho
        a la pòlissa de Soller. Camps comuns de distri i comer"""

        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')
        attach_obj = self.pool.get('ir.attachment')
        periode_obj = self.pool.get('giscedata.polissa.potencia.'
                                    'contractada.periode')

        polissa = polissa_obj.browse(cr, uid, id)

        default.update(
            {
                'lot_facturacio': False,
                'data_ultima_lectura': False,
                'data_ultima_lectura_estimada': False,
                'state': 'esborrany',
                'versio_primera_factura': False,
                'data_alta': False,
                'data_firma_contracte': False,
                'proxima_facturacio': False,
                'comptadors': [],
                'name_auto': True,
                'active': True,
                'data_baixa': False,
                'renovacio_auto': True,
                'is_signed': False,
                'reception_date': False
            }
        )

        res_id = super(GiscedataPolissaSollerExtend,
                       self).copy(cr, uid, id, default, context)
        #copiamos manualmente solo el contador activo
        polissa = polissa_obj.browse(cr, uid, id)
        for comptador in polissa.comptadors:
            if comptador.active:
                comptador_nou_id = comptador_obj.copy(cr, uid, comptador.id,
                                                      {'polissa': res_id,
                                                       'lectures': [],
                                                       'lectures_pot': []})
        #Search for polissa attachments and copy
        search_params = [('res_model', '=', self._name),
                         ('res_id', '=', polissa.id)]
        attach_ids = attach_obj.search(cr, uid, search_params)
        for attach_id in attach_ids:
            attach_obj.copy(cr, uid, attach_id, {'res_id': res_id})

        for periode in polissa.potencies_periode:
            periode_obj.copy(cr, uid, periode.id, {
                'polissa_id': res_id
            })

        return res_id

    _columns = {
        'direccio_titular': fields.many2one('res.partner.address',
                            'Adreça titular', readonly=True,
                             states={'esborrany': [('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)],
                                     'modcontractual': [('readonly', False),
                                                        ('required', True)]},
                          ),
        'codi': fields.char('Num. Pòlissa', size=60, readonly=True,
                            states={'esborrany': [('readonly', False)],
                                    'validar': [('readonly', False)]}),
        'is_signed': fields.boolean('Firmat',required=True),
        'reception_date': fields.date('Data recepció firmat')

    }

    _defaults = {
        'is_signed': lambda *a: False,
    }

    def _check_pot_tarifa(self, cr, uid, ids):

        for polissa in self.browse(cr, uid, ids):
            if not polissa.potencia or not polissa.tarifa:
                continue

            if (not polissa.potencia > polissa.tarifa.pot_min
                or not polissa.potencia <= polissa.tarifa.pot_max):
                return False
        return True

    def _check_lot_facturacio(self, cursor, uid, ids):
        '''
            Insert lot_facturacio in draft contracts is not possible
        '''
        for polissa in self.browse(cursor, uid, ids):
            if (polissa.state == 'esborrany' and polissa.lot_facturacio):
                return False
        return True

    _constraints = [
        (_check_pot_tarifa,
         u"Error: La potència no està dins els "
         u"marges de la tarifa seleccionada",
         ['potencia', 'tarifa']),
        (_check_lot_facturacio,
         u"Error: No es pot insertar el lot de facturació, "
         u"la pòlissa està en esborrany.",
         ['lot_facturacio', 'state']),
    ]

GiscedataPolissaSollerExtend()


class GiscedataPolissaModContractualSollerExtend(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'direccio_titular': fields.many2one('res.partner.address',
                            'Adreça titular', readonly=True,
                            states={'esborrany': [('readonly', False)],
                                    'validar': [('readonly', False),
                                                ('required', True)],
                                    'modcontractual': [('readonly', False),
                                                       ('required', True)]},
                          ),

    }

GiscedataPolissaModContractualSollerExtend()
