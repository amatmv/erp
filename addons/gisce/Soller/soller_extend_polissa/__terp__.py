# -*- coding: utf-8 -*-
{
    "name": "Soller Extend Polissa",
    "description": """
Funcionalitats extra per polissa
    * Afegir el camp Adreça titular
    * Wizard per actualitzar els contactes de la polissa sense fer modificació contractual
    * Desactivar un partner fent comprovacions sobre pòlisses i factures
    * Personalitzar la funció copy per poder duplicar pòlisses
    * Accés directe a les pòlisses des dels partners
    * Informació per saber si el contracte està firmat i a quina data
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_comerdist_polissa",
        "giscedata_polissa",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/soller_extend_polissa_security.xml",
        "soller_extend_polissa_view.xml",
        "registre_polissa_view.xml",
        "wizard/wizard_update_contacts_view.xml",
        "security/ir.model.access.csv",
        "soller_extend_polissa_report.xml"
    ],
    "active": False,
    "installable": True
}
