# -*- coding: utf-8 -*-
{
    "name": "Soller Extend Partner",
    "description": """
Funcionalitats extra per partner
    * Afegit camp street a la vista tree de les adreces
    * Afegit camp vat a la vista tree dels partners
    * Afegida nacionalitat del partner
    * Wizard per copiar una adreça
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "base",
        "account"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_extend_partner_view.xml",
        "wizard/wizard_duplicate_address_view.xml"
    ],
    "active": False,
    "installable": True
}
