# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from tools.translate import _

class soller_extend_partner(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    def _get_default_country(self, cr, uid, context={}):
        '''Retornem espanya com a nacionalitat per defecte'''

        country_obj = self.pool.get('res.country')
        search_params = [('code','=','ES')]

        return country_obj.search(cr, uid, search_params)[0]

    
    _columns = {
        'country_id':fields.many2one('res.country', 'Nacionalidad', required=False,
                                     help='Indique el pais de nacionalidad'),
        
    }

    _defaults = {
        'country_id': _get_default_country,
        
    }

soller_extend_partner()
