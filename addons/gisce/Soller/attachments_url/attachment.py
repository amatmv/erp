# -*- coding: utf-8 -*-

from osv import osv, fields


class AttachmentURL(osv.osv):

    _inherit = 'ir.attachment'

    def _get_url(self, cursor, uid, ids, name, arg, context=None):

        config_obj = self.pool.get('res.config')

        base_url = config_obj.get(cursor, uid,
                                  'attachment_base_url',
                                  '')

        query = '''
            SELECT id, datas_mongo from ir_attachment where id in %s
        '''

        res = dict.fromkeys(ids, '')
        cursor.execute(query, (tuple(ids), ))
        for id, oid in cursor.fetchall():
            oid = oid or ''
            res[id] = base_url + oid
        return res

    _columns = {
        'url': fields.function(_get_url, 'URL', method=True,
                               type='char', size=255,
                               string="URL",
                               readonly=True),
    }

AttachmentURL()
