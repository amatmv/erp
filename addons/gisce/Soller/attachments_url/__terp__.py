# -*- coding: utf-8 -*-
{
    "name": "Attachments URL",
    "description": """Generate URL for downloading attachments""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Generic Modules",
    "depends":[
        "base_extended",
        "ir_attachment_mongodb"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "attachment_data.xml",
        "attachment_view.xml"
    ],
    "active": False,
    "installable": True
}
