# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class ResPartner(osv.osv):

    _inherit = 'res.partner'

    def deactivate_partner(self, cursor, uid, ids, context=None):

        operador_obj = self.pool.get('soller.operador.contrato')

        partner_vals = self.read(cursor, uid, ids, ['name'])

        for val in partner_vals:
            partner_id = val['id']
            partner_name = val['name']
            # Check if we have an associated operador contract
            search_params = ['|',
                             ('titular_id', '=', partner_id),
                             ('pagador_id', '=', partner_id),
                             ('state', 'not in', ('baixa', 'cancelat'))]
            operador_ids = operador_obj.search(cursor, uid, search_params)
            if operador_ids:
                msg = _(u"Customer {} has operator contracts")
                raise osv.except_osv('Error',
                                     msg.format(partner_name))

        return super(ResPartner, self).deactivate_partner(cursor, uid, ids,
                                                          context=context)

ResPartner()
