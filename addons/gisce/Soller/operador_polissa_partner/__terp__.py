# -*- coding: utf-8 -*-
{
    "name": "Operador+Policy",
    "description": """
Common functions to operador and policy
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "soller_operador"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
