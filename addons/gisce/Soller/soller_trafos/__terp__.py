# -*- coding: utf-8 -*-
{
    "name": "Soller Trafos",
    "description": """
Model pels trafos de tensió i d'intensitat
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_trafo_view.xml",
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv",
        "soller_trafo_data.xml"
    ],
    "active": False,
    "installable": True
}
