<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="mod347_letter.jrxml" language="groovy" pageWidth="595" pageHeight="842" columnWidth="511" leftMargin="42" rightMargin="42" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.4641000000000006"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<subDataset name="address">
		<parameter name="record_id" class="java.lang.Integer">
			<defaultValueExpression><![CDATA[1]]></defaultValueExpression>
		</parameter>
		<queryString>
			<![CDATA[SELECT
  partner.name as partner_name,
  coalesce(record.partner_vat, '') as partner_vat,
  address.street,
  address.city,
  coalesce(address.zip, '') as zip,
  coalesce(state.name, '') as state,
  coalesce(tr_country.value, '') as country
FROM l10n_es_aeat_mod347_partner_record record
LEFT JOIN l10n_es_aeat_mod347_invoice_record irecord
ON irecord.partner_record_id = record.id
LEFT JOIN account_invoice invoice
ON invoice.id = irecord.invoice_id
LEFT JOIN res_partner partner
ON record.partner_id = partner.id
INNER JOIN res_partner_address address
ON address.id = invoice.address_invoice_id
LEFT JOIN res_country_state state
ON state.id = address.state_id
LEFT JOIN res_country country
ON country.id = address.country_id
LEFT JOIN ir_translation tr_country
ON tr_country.name = 'res.country,name' and tr_country.res_id = country.id and tr_country.lang = 'es_ES'
WHERE record.id = $P{record_id}
AND
CASE
  WHEN partner.active = True THEN address.active = True
  ELSE address.active in (True, False)
END
order by invoice.date_invoice desc
LIMIT 1]]>
		</queryString>
		<field name="partner_name" class="java.lang.String"/>
		<field name="partner_vat" class="java.lang.String"/>
		<field name="street" class="java.lang.String"/>
		<field name="city" class="java.lang.String"/>
		<field name="zip" class="java.lang.String"/>
		<field name="state" class="java.lang.String"/>
		<field name="country" class="java.lang.String"/>
	</subDataset>
	<parameter name="IDS2" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{REPORT_PARAMETERS_MAP}.IDS.toString().replace("{","(").replace("}",")")]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
  record.id as record_id,
  coalesce(record.partner_vat, '') as partner_vat,
  record.first_quarter,
  record.second_quarter,
  record.third_quarter,
  record.fourth_quarter,
  record.amount as total,
  substring(company_partner.vat, 3) as company_vat,
  coalesce(company.rml_header1, '') as header1,
  coalesce(company.rml_footer1, '') as footer1


FROM l10n_es_aeat_mod347_partner_record record
INNER JOIN l10n_es_aeat_mod347_report report
ON report.id = record.report_id
LEFT JOIN res_company company ON company.id = report.company_id
LEFT JOIN res_partner company_partner ON company_partner.id = company.partner_id
WHERE record.id in $P!{IDS2}]]>
	</queryString>
	<field name="record_id" class="java.lang.Integer"/>
	<field name="partner_vat" class="java.lang.String"/>
	<field name="first_quarter" class="java.math.BigDecimal"/>
	<field name="second_quarter" class="java.math.BigDecimal"/>
	<field name="third_quarter" class="java.math.BigDecimal"/>
	<field name="fourth_quarter" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="company_vat" class="java.lang.String"/>
	<field name="header1" class="java.lang.String"/>
	<field name="footer1" class="java.lang.String"/>
	<pageHeader>
		<band height="92" splitType="Stretch">
			<image scaleImage="FillFrame">
				<reportElement x="-16" y="-11" width="553" height="80"/>
				<imageExpression class="java.lang.String"><![CDATA[$P{REPORT_PARAMETERS_MAP}.SUBREPORT_DIR + "vse_solo.jpg"]]></imageExpression>
			</image>
			<textField>
				<reportElement x="0" y="71" width="511" height="10"/>
				<textElement lineSpacing="Single">
					<font size="7" isItalic="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{header1}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="0" y="81" width="512" height="10" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="7" isBold="false" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{company_vat} + " " + $F{footer1}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="457" splitType="Stretch">
			<componentElement>
				<reportElement mode="Transparent" x="280" y="30" width="231" height="63"/>
				<jr:list xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" printOrder="Vertical">
					<datasetRun subDataset="address">
						<datasetParameter name="record_id">
							<datasetParameterExpression><![CDATA[$F{record_id}]]></datasetParameterExpression>
						</datasetParameter>
						<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					</datasetRun>
					<jr:listContents height="63" width="231">
						<textField isBlankWhenNull="true">
							<reportElement x="0" y="42" width="204" height="14"/>
							<textElement verticalAlignment="Middle" lineSpacing="Single">
								<font size="8"/>
							</textElement>
							<textFieldExpression class="java.lang.String"><![CDATA[($F{state}=="")?$F{country}.toUpperCase():$F{state}.toUpperCase() + " - " + $F{country}.toUpperCase()]]></textFieldExpression>
						</textField>
						<textField>
							<reportElement x="0" y="0" width="204" height="14"/>
							<textElement verticalAlignment="Middle" lineSpacing="Single">
								<font size="8"/>
							</textElement>
							<textFieldExpression class="java.lang.String"><![CDATA[$F{partner_name}]]></textFieldExpression>
						</textField>
						<textField>
							<reportElement x="0" y="28" width="204" height="14"/>
							<textElement verticalAlignment="Middle" lineSpacing="Single">
								<font size="8"/>
							</textElement>
							<textFieldExpression class="java.lang.String"><![CDATA[(($F{zip}=="")?$F{city}.toUpperCase():$F{zip}+ " " +$F{city}.toUpperCase())]]></textFieldExpression>
						</textField>
						<textField>
							<reportElement x="0" y="14" width="204" height="14"/>
							<textElement verticalAlignment="Middle" lineSpacing="Single">
								<font size="8"/>
							</textElement>
							<textFieldExpression class="java.lang.String"><![CDATA[$F{street}]]></textFieldExpression>
						</textField>
					</jr:listContents>
				</jr:list>
			</componentElement>
			<staticText>
				<reportElement x="0" y="167" width="511" height="57"/>
				<textElement textAlignment="Justified" lineSpacing="Single"/>
				<text><![CDATA[Muy Sr./s nuestros:

Con el fin de cumplimentar la DECLARACIÓN ANUAL DE OPERACIONES, pasamos a comunicarle que su volumen anual asciende a la cantidad de:]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="262" y="236" width="77" height="12"/>
				<textElement textAlignment="Right" lineSpacing="Single"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{first_quarter}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="145" y="236" width="117" height="12"/>
				<textElement lineSpacing="Single"/>
				<text><![CDATA[Primer trimestre]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="262" y="248" width="77" height="12"/>
				<textElement textAlignment="Right" lineSpacing="Single"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{second_quarter}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="145" y="248" width="117" height="12"/>
				<textElement lineSpacing="Single"/>
				<text><![CDATA[Segundo trimestre]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="262" y="260" width="77" height="12"/>
				<textElement textAlignment="Right" lineSpacing="Single"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{third_quarter}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="145" y="260" width="117" height="12"/>
				<textElement lineSpacing="Single"/>
				<text><![CDATA[Tercer trimestre]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="262" y="272" width="77" height="12"/>
				<textElement textAlignment="Right" lineSpacing="Single"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{fourth_quarter}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="145" y="272" width="117" height="12"/>
				<textElement lineSpacing="Single"/>
				<text><![CDATA[Cuarto trimestre]]></text>
			</staticText>
			<staticText>
				<reportElement x="145" y="294" width="117" height="12"/>
				<textElement lineSpacing="Single">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="262" y="294" width="77" height="12"/>
				<textElement textAlignment="Right" lineSpacing="Single">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="325" width="512" height="55"/>
				<textElement textAlignment="Justified" lineSpacing="Single"/>
				<text><![CDATA[Al mismo tiempo, y para evitar cualquier problema fiscal, le detallamos los datos de que dipsonemos. En caso de que no esté conforme le rogamos nos lo comunique, bien por teléfono o bien por escrito. En caso contrario, entenderemos que tanto la cifra indicada como los datos identificativos son correctos.]]></text>
			</staticText>
			<textField>
				<reportElement x="145" y="391" width="194" height="12"/>
				<textElement textAlignment="Center" lineSpacing="Single" markup="none">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["NIF: " + $F{partner_vat}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="415" width="511" height="35"/>
				<textElement textAlignment="Justified" lineSpacing="Single"/>
				<text><![CDATA[Sin otro particular quedamos a la espera de sus noticias y aprovechamos la ocasión para saludarle muy atentamente.]]></text>
			</staticText>
			<textField pattern="&apos;Sóller, &apos;d &apos;de&apos; MMMMM &apos;de&apos; yyyy">
				<reportElement x="287" y="125" width="224" height="15"/>
				<textElement textAlignment="Right" lineSpacing="Single"/>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
