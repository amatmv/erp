# -*- coding: utf-8 -*-
{
    "name": "Payment refund",
    "description": """
Bank refunds
    * Load bank refund file (Q34)
    * Undo invoice payment in file
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "account_invoice_extend"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "refund_view.xml",
        "refund_report.xml",
        "wizard/wizard_load_refund_view.xml",
        "wizard/wizard_show_invoices_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
