# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from tools.translate import _
import time

class PaymentRefundLine(osv.osv):

    _name = 'payment.refund.line'

    _columns = {
        'invoice_number':fields.char('Invoice number', size=64, 
                                      required=True, readonly=False),
        'partner':fields.char('Partner', size=128, required=True, readonly=False),
        'reference':fields.char('Reference', size=64, required=True, readonly=False),
        'bank_account':fields.char('Bank account', size=25, required=True, readonly=False),
        'total': fields.float('Total', digits=(16,2), required=True),
        'invoice_id': fields.integer('Invoice', required=False),                
                
    }
    
PaymentRefundLine()

class PaymentRefund(osv.osv):
    
    _name = 'payment.refund'

    _refund_states_selection = [
        ('draft', 'Draft'),
        ('validate', 'Validate'),
        ('confirm', 'Confirm')
    ]

    def action_draft(self, cr, uid, ids, context={}):

        self.write(cr, uid, ids, {'state': 'draft'}, context)
        return True
    
    def action_validate(self, cr, uid, ids, context={}):
        """check conditions to validate refund"""

        invoice_obj = self.pool.get('account.invoice')
        refline_obj = self.pool.get('payment.refund.line')
            
        notfound = 0
        notfound_extra = []  
        for refund in self.browse(cr, uid, ids):
            for refline in refund.line_ids:
                search_params = [('number','=', refline.invoice_number), 
                                 ('state','=','paid')]
                invoice_ids = invoice_obj.search(cr, uid, search_params)
                if invoice_ids:
                    #If found, save invoice id
                    invoice_id = invoice_ids[0]
                    refline.write({'invoice_id': invoice_id})
                else:
                    #If not found, ilike search
                    #Because of Q34 field length, 
                    #invoice number could be truncated
                    search_params = [('number','ilike', refline.invoice_number), 
                                     ('state','=','paid')]
                    invoice_like_ids = invoice_obj.search(cr, uid, search_params)
                    #If we find only one
                    if invoice_like_ids and len(invoice_like_ids) == 1:
                        invoice = invoice_obj.browse(cr, uid, 
                                                invoice_like_ids[0])
                        #Write the right number, id too.
                        refline.write({'invoice_number': invoice.number,
                                     'invoice_id': invoice.id,})
                    else:
                        notfound += 1
                        notfound_extra += ['N:%s'%(refline.invoice_number)]
        
        if notfound == 0:
            self.write(cr, uid, ids, {'state': 'validate'})
        else:
            raise osv.except_osv(_('Error'),
                        _('Cannot find %i invoices. %s')%(notfound, 
                                            ' '.join(notfound_extra)))
                    
        return True

    def action_confirm(self, cr, uid, ids, context={}):
        """confirm refund"""
        
        invoice_obj = self.pool.get('account.invoice')    

        for refund in self.browse(cr, uid, ids):
            invoice_ids = [x.invoice_id for x\
                           in refund.line_ids]
            invoice_obj.undo_payment(cr, uid, invoice_ids, context)
            
        self.write(cr, uid, ids,{'state':'confirm'})
        return True 

    def _get_total(self, cr, uid, ids, name, arg, context={}):
        """Total refund amount"""

        refline_obj = self.pool.get('payment.refund.line')

        res={}
        for refund in self.browse(cr, uid, ids, context):
            total = 0
            for refline in refund.line_ids:
                total += refline_obj.read(cr, uid, refline.id, ['total'])['total']
            res[refund.id] = total

        return res  

    _columns = {
        'name':fields.char('Reference', size=64, 
                    required=True, readonly=True, 
                    states={'draft':[('readonly', False)]}),
        'date': fields.date('Date', readonly=True, 
                    states={'draft':[('readonly', False)]}),
        'line_ids':fields.one2many('payment.refund.line', 'refund_id', 'Lines', 
                    required=False, readonly=True,
                    states={'draft':[('readonly', False)]}),
        'payment_order_id':fields.many2one('payment.order', 'Payment order', 
                    required=False, readonly=True,
                    states={'draft':[('readonly', False)]}),        
        'state': fields.selection(_refund_states_selection, 
                    'State', required=True, readonly=True),
        'total': fields.function(_get_total, method=True, type='float', 
                    digits=(16,2), readonly=True,
                    string='Total', store=False),        
        
    }
    
    _defaults = {
        'state': lambda *a: 'draft',
        
    }
PaymentRefund()

class PaymentRefundLine2(osv.osv):

    _name = 'payment.refund.line'
    _inherit = 'payment.refund.line'

    _columns = {
        'refund_id':fields.many2one('payment.refund', 'Refund', 
                                    ondelete='cascade',required=True),
    }

PaymentRefundLine2()
