# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import base64
import StringIO
import time
import datetime


class WizardLoadRefund(osv.osv_memory):
    
    _name = 'wizard.load.refund'

    def remove_zeros(self, cr, uid, string, context={}):
        
        res = ''
        for i in range(0,len(string)):
            if string[i] == '0' and not string[i+1] == '0':
                res = string[i+1:len(string)]
                break
        return res

    def import_file(self, cr, uid, ids, context={}):
        
        wizard = self.browse(cr, uid, ids[0])
        refline_obj = self.pool.get('payment.refund.line')
        invoice_obj = self.pool.get('account.invoice')
        
        self.pool.get('ir.attachment').create(cr, uid, {
            'name': 'refund_' + datetime.datetime.now().strftime('%d%m%Y'),
            'datas': wizard.file,
            'datas_fname': 'refund_' + datetime.datetime.now().\
                                        strftime('%d%m%Y') + '.txt',
            'res_model': 'payment.refund',
            'res_id': context.get('active_id', []),
            }, context=context)
                
        file = StringIO.StringIO(unicode(base64.b64decode(wizard.file),
                                                         'iso-8859-1'))
        
        for line in file:
            if not line.startswith('56'):
                continue
            invoice_number = line[104:114]
            reference = line[16:28]
            partner = line[28:68]
            bank_account = line[68:88]
            total = float(line[88:96] + '.' + line[96:98])
            vals = {'refund_id': context.get('active_id', []),
                    'invoice_number': self.remove_zeros(cr, uid,
                                                invoice_number, context),
                    'partner': partner,
                    'reference': self.remove_zeros(cr, uid, reference, context),
                    'bank_account': bank_account,
                    'total': total,
                   }
            refline_obj.create(cr, uid, vals, context)
        return {}
                        
        
    _columns = {
        'file':fields.binary('File', filters=None),
                              
    }
            
WizardLoadRefund()
