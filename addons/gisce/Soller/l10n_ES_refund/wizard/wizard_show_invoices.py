# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from tools.translate import _


class WizardRefundShowInvoices(osv.osv_memory):

    _name = 'wizard.refund.show.invoices'    

    def show_invoices(self, cr, uid, ids, context={}):
        """Returns a list with the invoices in the refund"""

        invoice_obj = self.pool.get('account.invoice')
        refund_obj = self.pool.get('payment.refund')
        refund_id = context.get('active_id', [])

        invoice_ids = [x.invoice_id for x in refund_obj.browse(cr, uid, 
                                                refund_id).line_ids]
        vals_view = {
                'name': _('Invoices'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.invoice',
                'limit': 500,
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': "[('id', 'in', %s)]" %(invoice_ids),
                }
        
        return vals_view
    
    

WizardRefundShowInvoices()
