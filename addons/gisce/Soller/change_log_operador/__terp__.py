# -*- coding: utf-8 -*-
{
    "name": "Change log for operador contrato",
    "description": """Audit changes for operador contrato""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules",
    "depends":[
        "base",
        "change_log",
        "soller_operador"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_operador_view.xml"
    ],
    "active": False,
    "installable": True
}
