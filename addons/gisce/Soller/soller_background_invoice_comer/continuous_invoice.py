# -*- coding: utf-8 -*-

from osv import osv
from tools import config
import netsvc


class SollerBackgroundContinuousInvoice(osv.osv):
    _inherit = 'continuous.invoicing.lot'

    def slice_factures(self, cursor, uid, factura_ids, slice_size):
        num_slices = ((len(factura_ids) % slice_size) and
                      (len(factura_ids) / slice_size + 1) or
                      (len(factura_ids) / slice_size))
        return [factura_ids[x:x + slice_size]
                for x in range(0, num_slices * slice_size,
                               slice_size)]

    def action_print_invoices(self, cursor, uid, ids,
                                     context=None):
        """ Print invocies of lot in pdf and attach to the lot.
        " Invoices are grouped by period (mensual bimensual), zip
        " and sliced in groups.
        """
        config_obj = self.pool.get('res.config')

        if not context:
            context = {}

        if isinstance(ids, (list, tuple)):
            lot_id = ids[0]
        else:
            lot_id = ids

        lot_name = self.read(cursor, uid, lot_id, ['name'])['name']
        lot_name = lot_name.replace('/', '-')
        slice_size = int(config_obj.get(cursor, uid, 'fact_slice_size', '100'))

        invoice_ids = self.get_invoices_from_lot(cursor, uid, lot_id,
                                                 context=context)
        facts_slices = self.slice_factures(cursor, uid, invoice_ids,
                                           slice_size)
        for facts_slice in facts_slices:
            start = facts_slices.index(facts_slice) * slice_size
            end = start + len(facts_slice)
            start = str(start).zfill(5)
            end = str(end).zfill(5)
            filename = ("facturas_{}_{}_{}.pdf".format(lot_name, start, end))

            self.generate_fact_slice(cursor, uid, lot_id, facts_slice, filename)
        return True

    def generate_fact_slice(self, cursor, uid, lot_id, fact_ids, filename,
                            context=None):

        if not context:
            context = {}

        source_model = 'giscedata.facturacio.factura'
        report_name = "report_phantom_factura_soller"

        ctx = context.copy()
        ctx.update({'control_pages': 2,
                    'webkit_unsigned_pdf': True})

        service = netsvc.LocalService("report.%s" % report_name)
        service.create_background(cursor, uid, fact_ids,
                                  filename, report_name,
                                  source_model, self._name,
                                  lot_id, compress=True, context=ctx)

SollerBackgroundContinuousInvoice()
