SELECT factura.id
FROM giscedata_facturacio_factura factura
INNER JOIN account_invoice invoice
  ON invoice.id = factura.invoice_id
INNER JOIN payment_mode mode
  ON mode.id = factura.payment_mode_id
INNER JOIN res_partner_address address
  ON invoice.address_invoice_id = address.id
WHERE
  factura.facturacio = %s
  AND factura.lot_facturacio = %s
  AND trim(coalesce(address.zip, '')) in %s
  AND mode.name in %s
  AND factura.per_enviar like '%%postal%%'
  AND invoice.type = 'out_invoice'
ORDER BY coalesce(address.zip, ''), factura.id
