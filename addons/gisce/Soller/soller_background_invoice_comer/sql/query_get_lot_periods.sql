SELECT DISTINCT factura.facturacio
FROM giscedata_facturacio_factura factura
WHERE
  factura.lot_facturacio = %s
ORDER BY factura.facturacio
