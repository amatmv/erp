SELECT DISTINCT trim(coalesce(address.zip, '')) as zip
FROM giscedata_facturacio_factura factura
INNER JOIN account_invoice invoice
  ON invoice.id = factura.invoice_id
INNER JOIN res_partner_address address
  ON invoice.address_invoice_id = address.id
WHERE
  factura.lot_facturacio = %s
ORDER BY zip
