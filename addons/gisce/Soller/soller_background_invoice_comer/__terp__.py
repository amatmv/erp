# -*- coding: utf-8 -*-
{
    "name": "Background Invoice Comer Reports",
    "description": """
Genera reports dels lots de facturació en background i els deixa com adjunt.

  Settings a posar al conf:
  fact_slice_size: Número de factures per cada PDF, per defecte si no
                   s'especifica son 100
  local_zips: Codis postals considerats locals, per defecte si no
              s'especifiquen són
              ['07100', '07108', '07109', '07101']
    """,
    "version": "0-dev",
    "author": "Bartomeu Miro Mateu",
    "category": "Soller",
    "depends":[
        "jasper_reports",
        "giscedata_facturacio_comer",
        "continuous_invoice"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "background_invoice_comer_view.xml",
        "continuous_invoice_view.xml"
    ],
    "active": False,
    "installable": True
}
