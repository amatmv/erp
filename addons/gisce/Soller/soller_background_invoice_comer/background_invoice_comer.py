# -*- coding: utf-8 -*-

from osv import osv
from tools import config
import netsvc


class SollerBackgroundReportsLot(osv.osv):
    _name = 'giscedata.facturacio.lot'
    _inherit = 'giscedata.facturacio.lot'

    def slice_factures(self, cursor, uid, factura_ids, slice_size):
        num_slices = ((len(factura_ids) % slice_size) and
                      (len(factura_ids) / slice_size + 1) or
                      (len(factura_ids) / slice_size))
        return [factura_ids[x:x + slice_size]
                for x in range(0, num_slices * slice_size,
                               slice_size)]

    def action_imprimir_factures_grup_cobratori(self, cursor, uid, ids,
                                                context=None):
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        config_obj = self.pool.get('res.config')
        mode_obj = self.pool.get('payment.mode')

        if not context:
            context = {}

        if isinstance(ids, (list, tuple)):
            lot_id = ids[0]
        else:
            lot_id = ids

        period_name = {
            1: 'men',
            2: 'bimen'
        }

        lot_name = self.read(cursor, uid, lot_id, ['name'])['name']
        lot_name = lot_name.replace('/', '-')

        slice_size = int(config_obj.get(cursor, uid,
                                        'fact_slice_size', '100'))

        query_file = (u"%s/soller_background_invoice_comer/sql"
                      u"/query_payment_mode.sql") % config['addons_path']
        query = open(query_file).read()
        cursor.execute(query, (lot_id, ))
        payment_mode_ids = [x[0] for x in cursor.fetchall()]

        for period in self.get_lot_periods(cursor, uid, lot_id):
            for payment_mode_id in payment_mode_ids:
                search_params = [
                    ('lot_facturacio', '=', lot_id),
                    ('per_enviar', 'like', 'postal%'),
                    ('state', 'in', ('open', 'paid')),
                    ('payment_mode_id', '=', payment_mode_id),
                    ('facturacio', '=', period),
                ]
                factura_ids = factura_obj.search(cursor, uid,
                                                 search_params,
                                                 order='id ASC',
                                                 context=context)
                if not factura_ids:
                    continue

                payment_mode_name = mode_obj.read(cursor, uid,
                                                  payment_mode_id,
                                                  ['name'])['name']

                facts_slices = self.slice_factures(cursor, uid, factura_ids,
                                                   slice_size)

                for facts_slice in facts_slices:
                    start = facts_slices.index(facts_slice) * slice_size
                    end = start + len(facts_slice)
                    start = str(start).zfill(5)
                    end = str(end).zfill(5)
                    filename = ("facturas_%s_%s_GC%s_%s-%s.pdf" %
                                (lot_name, period_name[period],
                                 payment_mode_name, start, end))

                    self.generate_fact_slice(cursor, uid, lot_id, facts_slice,
                                             filename, context=context)
        return True

    def get_all_lot_zips(self, cursor, uid, lot_id, context=None):
        """ Return a sorted list of all postal codes in the lot_id """
        query_file = (u"%s/soller_background_invoice_comer/sql"
                      u"/query_lot_zips.sql") % config['addons_path']
        query = open(query_file).read()
        cursor.execute(query, (lot_id, ))
        return [x[0] for x in cursor.fetchall()]

    def get_lot_periods(self, cursor, uid, lot_id, context=None):
        """ Return all the periods of the given lot_id """
        query_file = (u"%s/soller_background_invoice_comer/sql"
                      u"/query_get_lot_periods.sql") % config['addons_path']
        query = open(query_file).read()
        cursor.execute(query, (lot_id, ))
        return [x[0] for x in cursor.fetchall()]

    def get_lot_invocies_by_zip(self, cursor, uid, lot_id, period,
                                zips, payment_modes):
        """
        " Return a list of factura ids associated to the given lot_id and
        " period. Invoices are filtered with zips an sorted
        " by zip
        """
        query_file = (u"%s/soller_background_invoice_comer/sql"
                      u"/query_get_lot_invoices_by_zip.sql"
                      % config['addons_path'])
        query = open(query_file).read()
        cursor.execute(query, (period, lot_id, tuple(zips),
                               tuple(payment_modes)))
        return [x[0] for x in cursor.fetchall()]

    def action_imprimir_factures_codi_postal(self, cursor, uid, ids,
                                     context=None):
        """ Print invocies of lot in pdf and attach to the lot.
        " Invoices are grouped by period (mensual bimensual), zip
        " and sliced in groups.
        """
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        config_obj = self.pool.get('res.config')

        if not context:
            context = {}

        if isinstance(ids, (list, tuple)):
            lot_id = ids[0]
        else:
            lot_id = ids

        period_name = {
            1: 'men',
            2: 'bimen'
        }

        lot_name = self.read(cursor, uid, lot_id, ['name'])['name']
        lot_name = lot_name.replace('/', '-')
        slice_size = int(config_obj.get(cursor, uid,
                                        'fact_slice_size', '100'))

        all_zips = self.get_all_lot_zips(cursor, uid, lot_id, context)
        local_zips = set(eval(config_obj.get(cursor, uid,
                                      'local_zips',
                                      "['07100', '07108', '07109', '07101']")))
        other_zips = list(set(all_zips) - set(local_zips))

        zip_groups = {
            'local': local_zips,
            'other': other_zips,
        }
        payment_modes = list(set(eval(config_obj.get(cursor, uid,
                                      'fact_payment_mode_postal',
                                      "['0', '9020', '9022']"))))
        for period in self.get_lot_periods(cursor, uid, lot_id):
            for zip_name, zips in zip_groups.items():
                factura_ids = self.get_lot_invocies_by_zip(cursor, uid,
                                                           lot_id, period,
                                                           zips, payment_modes)
                facts_slices = self.slice_factures(cursor, uid, factura_ids,
                                                   slice_size)
                for facts_slice in facts_slices:
                    start = facts_slices.index(facts_slice) * slice_size
                    end = start + len(facts_slice)
                    start = str(start).zfill(5)
                    end = str(end).zfill(5)
                    filename = ("facturas_%s_%s_%s_%s-%s.pdf" %
                                (lot_name, period_name[period], zip_name,
                                 start, end))

                    self.generate_fact_slice(cursor, uid, lot_id,
                                             facts_slice, filename)
        return True

    def generate_fact_slice(self, cursor, uid, lot_id, fact_ids, filename,
                            context=None):

        if not context:
            context = {}

        source_model = 'giscedata.facturacio.factura'
        report_name = "report_phantom_factura_soller"

        ctx = context.copy()
        ctx.update({'control_pages': 2,
                    'webkit_unsigned_pdf': True})

        service = netsvc.LocalService("report.%s" % report_name)
        service.create_background(cursor, uid, fact_ids,
                                  filename, report_name,
                                  source_model, self._name,
                                  lot_id, compress=True, context=ctx)

SollerBackgroundReportsLot()
