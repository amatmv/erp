# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from osv import osv
from osv import fields
import math

class TelegestioExtendComptador(osv.osv):
    
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def _get_intensitat_tel(self, cr, uid, ids, name, arg, context={}):
        '''retorna la intensitat teorica segons la potencia
        programada al comptador'''
        
        res = {}
        
        for comptador in self.browse(cr, uid, ids, context):
            tensio = comptador.polissa.tensio
            nom_tensio = comptador.polissa.tensio_normalitzada.name
            pot = comptador.tel_icp_potencia*1000 # en watts
            #si es trifasic fem un càlcul diferent dels monofasics
            if nom_tensio.startswith('3x'):
                intensitat = round(pot/(tensio * math.sqrt(3)),2)
            else:
                intensitat = round(pot/tensio,2)
            res[comptador.id] = intensitat

        return res
        
    
    _columns = {
        'telegestio':fields.boolean('Telegestió'),
        'tel_icp_actiu':fields.boolean('ICP activat', required=False),
        'tel_icp_potencia': fields.float('Potència programada', digits=(16,3)),
        'tel_icp_intensitat': fields.function(_get_intensitat_tel, method=True, 
                                type='float', digits=(16,2), string='Intensitat', readonly=True,
                        store={'giscedata.lectures.comptador': 
                               (lambda self, cr, uid, ids, c={}: ids,['tel_icp_potencia'], 20),
                              },),
        
        
    }
    _defaults = {
        'telegestio': lambda *a: False,
        'tel_icp_actiu': lambda *a: False,
        'tel_icp_potencia': lambda *a: 0,
        
    }
TelegestioExtendComptador()
