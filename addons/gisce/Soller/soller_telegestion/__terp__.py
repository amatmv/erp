# -*- coding: utf-8 -*-
{
    "name": "Soller Telegestió",
    "description": """
Ampliació dels models de distribució per reflexar informació sobre telegestió:
    * Dades dels comptadors preparats per telegestió
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_polissa_distri",
        "giscedata_lectures_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_lectures_view.xml"
    ],
    "active": False,
    "installable": True
}
