# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields


class ConfigLinesDevolucio(osv.osv_memory):

    _name = "config.linies.devolucio"

    def action_set(self, cr, uid, ids, context={}):

        devline_obj = self.pool.get('soller.impagats.devolucio.linia')
        invoice_obj = self.pool.get('account.invoice')

        devline_ids = devline_obj.search(cr, uid, [])
        for linia in devline_obj.browse(cr, uid, devline_ids):
            search_params = [('number','=', linia.numfactura),
                            ]
            invoice_ids = invoice_obj.search(cr, uid, search_params)
            if invoice_ids:
                #Si hem trobat el numero, escribim a la linia el id i el model
                values = devline_obj.get_model_per_id(cr, uid, invoice_ids)
                linia.write(values)
            else:
                #Si no trobem la factura amb numero igual cerquem amb ilike
                #per si en el fitxer ens passen el numero tallat
                search_params = [('number','ilike', linia.numfactura), 
                                ]
                invoice_like_ids = invoice_obj.search(cr, uid, search_params)
                #Si trobem una, i no mes una
                if invoice_like_ids and len(invoice_like_ids) == 1:
                    invoice = invoice_obj.browse(cr, uid, invoice_like_ids[0])
                    values = devline_obj.get_model_per_id(cr, uid, invoice.id)
                    values['numfactura'] = invoice.number
                    #Com hem trobat el numero, escribim a la linia el numero real, id i model
                    linia.write(values)

        return {
                'view_type': 'form',
                "view_mode": 'form',
                'res_model': 'ir.actions.configuration.wizard',
                'type': 'ir.actions.act_window',
                'target':'new',
         }

ConfigLinesDevolucio()
