# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields


class WizardMostrarFactures(osv.osv_memory):

    _name = 'wizard.mostrar.factures.devolucio'    

    def mostrar_factures(self, cr, uid, ids, context={}):
        """Devuelve una vista con las facturas cargadas en la devolucion"""

        model = context.get('model',False)
        if not model:
            raise osv.except_osv('Error','No es poden mostrar les factures')    

        factura_obj = self.pool.get(model)
        devolucio_obj = self.pool.get('soller.impagats.devolucio')
        devolucio_id = context.get('active_id', [])

        factures_ids = []
        for linia in devolucio_obj.browse(cr, uid, devolucio_id).linies_ids:
            if linia.model != model:
                #Si no es del modelo, continuamos con la siguiente linea
                continue
            if linia.invoice_id:
                #si ya tenemos un id guardado lo cogemos
                factures_ids.append(linia.invoice_id)
            else:
                raise osv.except_osv('Error','No es poden mostrar totes les factures')

        vals_view = {
                'name': 'Facturas',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': model,
                'limit': 500,
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': "[('id', 'in', %s)]" %(factures_ids),
                }
        
        return vals_view

WizardMostrarFactures()
