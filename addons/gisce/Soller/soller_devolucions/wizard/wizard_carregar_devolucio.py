# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import base64
import StringIO
import time
import datetime

from soller_devolucions.xml_message import Message


def remove_zeros(string):
    res = ''
    for i in range(0, len(string)):
        if string[i] == '0' and not string[i + 1] == '0':
            res = string[i + 1:len(string)]
            break
    return res


class ClassName(osv.osv_memory):

    _name = 'wizard.carregar.devolucio'

    def is_sepa_file(self, cr, uid, wizard, context=None):
        file = StringIO.StringIO(unicode(
                                 base64.b64decode(wizard.arxiu), 'UTF-8'))

        header_th = 10
        i = 0
        for line in file.readlines():
            if '<?xml' in line:
                return True
            elif i > header_th:
                return False
            i += 1

        return False

    def import_file(self, cr, uid, ids, context=None):
        wizard = self.browse(cr, uid, ids[0])

        if self.is_sepa_file(cr, uid, wizard, context):
            file_extension = '.xml'
        else:
            file_extension = '.txt'

        self.pool.get('ir.attachment').create(cr, uid, {
            'name': 'devolucio_' + datetime.datetime.now().strftime('%d%m%Y'),
            'datas': wizard.arxiu,
            'datas_fname': ('devolucio_' +
                            datetime.datetime.now().strftime('%d%m%Y') +
                            file_extension),
            'res_model': 'soller.impagats.devolucio',
            'res_id': context.get('active_id', []),
            }, context=context)

        if self.is_sepa_file(cr, uid, wizard, context):
            return self.import_sepa_file(cr, uid, wizard, context)
        else:
            return self.import_text_file(cr, uid, wizard, context)

    def import_sepa_file(self, cr, uid, wizard, context=None):
        xml = base64.decodestring(wizard.arxiu)
        devolucio_linia_obj = self.pool.get('soller.impagats.devolucio.linia')

        devolution_message = Message(xml)
        for original_payment_info in (devolution_message.obj.CstmrPmtStsRpt.
                                      OrgnlPmtInfAndSts):
            # Normally this loop only iterate once
            for reason_status_info in original_payment_info.TxInfAndSts:
                numfactura = reason_status_info.OrgnlEndToEndId.text
                pagador = reason_status_info.OrgnlTxRef.Dbtr.Nm.text
                compte = reason_status_info.OrgnlTxRef.DbtrAcct.Id.IBAN.text
                import_factura = reason_status_info.OrgnlTxRef.Amt.InstdAmt.text
                reject_code = reason_status_info.StsRsnInf.Rsn.Cd.text
                referencia = (reason_status_info.OrgnlTxRef.
                              RmtInf.Ustrd.text.split(' ')[-1])
                vals = {
                    'devolucio_id': context.get('active_id', []),
                    'numfactura': numfactura,
                    'pagador': pagador,
                    'referencia': referencia.upper(),
                    'ccc_carrec': compte,
                    'import': import_factura,
                    'reject_code': reject_code
                }
                devolucio_linia_obj.create(cr, uid, vals, context)

        return {}

    def import_text_file(self, cr, uid, wizard, context=None):
        file = StringIO.StringIO(unicode(
                                 base64.b64decode(wizard.arxiu), 'iso-8859-1'))
        devolucio_linia_obj = self.pool.get('soller.impagats.devolucio.linia')

        for line in file:
            if not line.startswith('56'):
                continue
            numfactura = line[104:114]
            referencia = line[16:28]
            pagador = line[28:68]
            compte = line[68:88]
            import_factura = float(line[88:96] + '.' + line[96:98])
            vals = {'devolucio_id': context.get('active_id', []),
                    'numfactura': remove_zeros(numfactura),
                    'pagador': pagador,
                    'referencia': remove_zeros(referencia),
                    'ccc_carrec': compte,
                    'import': import_factura,
                    }
            devolucio_linia_obj.create(cr, uid, vals, context)
        return {}

    _columns = {
        'arxiu': fields.binary('Arxiu', filters=None),
    }

ClassName()
