# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from tools import config
from datetime import datetime, timedelta
from tools.translate import _
import netsvc

from defs import sepa_devolution_reason
from account.invoice import RECTIFICATIVE_TYPE_SELECTION

class SollerImpagatsDevolucio1(osv.osv):

    _name = 'soller.impagats.devolucio'

SollerImpagatsDevolucio1()


class SollerImpagatsDevolucioLinea(osv.osv):
    """ Linies d'una devolució """
    _name = 'soller.impagats.devolucio.linia'

    def _reason_msg_from_code(self, cursor, uid, ids, name, arg, context=None):
        res = {}
        for id in ids:
            line = self.browse(cursor, uid, id)
            code = line.reject_code

            if not code:
                res[id] = "Desconocido"
            elif code in sepa_devolution_reason:
                res[id] = "(%s) %s" % (code, sepa_devolution_reason[code])
            else:
                res[id] = "(%s) Desconocido" % code

        return res

    def get_model_per_id(self, cr, uid, invoice_id, context={}):
        '''Retorna quin model de factura es segons si el invoice_id
        esta relacionat amb giscedata o no'''

        factura_obj = self.pool.get('giscedata.facturacio.factura')

        if isinstance(invoice_id, tuple) or isinstance(invoice_id, list):
            invoice_id = invoice_id[0]

        search_params = [('invoice_id', '=', invoice_id), ]
        factura_ids = factura_obj.search(cr, uid, search_params)
        if factura_ids:
            #Si trobem una giscefactura associada al invoice_id que ens passen
            #vol dir que el model es el de gisce i guardem el id d'aquest model
            return {'invoice_id': factura_ids[0],
                    'model': 'giscedata.facturacio.factura'}
        else:
            return {'invoice_id': invoice_id,
                    'model': 'account.invoice'}

    _columns = {
        'devolucio_id': fields.many2one('soller.impagats.devolucio',
                                        'Devolució', required=True,
                                        ondelete='cascade'),
        'numfactura': fields.char('Factura', size=64, required=True,
                                  readonly=False),
        'pagador': fields.char('Pagador', size=128, required=True,
                               readonly=False),
        'referencia': fields.char('Referencia', size=64, required=True,
                                  readonly=False),
        'ccc_carrec': fields.char('Compte de càrrec', size=35, required=True,
                                  readonly=False),
        'import': fields.float('Import', digits=(16, 2), required=True),
        'invoice_id': fields.integer('Factura', required=False),
        'model': fields.char('Model', size=64, required=False),
        'reject_code': fields.char('Codi motiu', size=4, required=False),
        'reject_msg': fields.function(_reason_msg_from_code,
                                      method=True, type='char',
                                      size=128,
                                      string='Motiu',
                                      readonly=True,
                                      ),
    }

SollerImpagatsDevolucioLinea()


class SollerImpagatsDevolucio(osv.osv):
    '''
    Modelo de devoluciones de factura de energia
    '''
    _name = 'soller.impagats.devolucio'
    _inherit = 'soller.impagats.devolucio'
    _order = 'date desc'

    _soller_impagats_devolucio_states_selection = [
        ('esborrany', 'Esborrany'),
        ('validar', 'Validar'),
        ('confirmat', 'Confirmat')
    ]

    def action_validar(self, cr, uid, ids, context=None):
        """condicions per passar una devolució a validar"""

        res = {'value': {}, 'domain': {}, 'warning': {}}
        invoice_obj = self.pool.get('account.invoice')
        devline_obj = self.pool.get('soller.impagats.devolucio.linia')

        devolucio = self.browse(cr, uid, ids[0])

        notrobades = 0

        for devolucio in self.browse(cr, uid, ids):
            for linia in devolucio.linies_ids:
                search_params = [('number', '=', linia.numfactura),
                                 ('state', '=', 'paid')
                                 ]
                invoice_ids = invoice_obj.search(cr, uid, search_params)
                if invoice_ids:
                    #Si hem trobat el numero, escribim a la linia el id i
                    #el model
                    values = devline_obj.get_model_per_id(cr, uid, invoice_ids)
                    linia.write(values)
                else:
                    #Si no trobem la factura amb numero igual cerquem amb ilike
                    #per si en el fitxer ens passen el numero tallat
                    search_params = [('number', 'ilike', linia.numfactura),
                                     ('state', '=', 'paid')]
                    invoice_like_ids = invoice_obj.search(cr, uid,
                                                          search_params)
                    #Si trobem una, i no mes una
                    if invoice_like_ids and len(invoice_like_ids) == 1:
                        invoice = invoice_obj.browse(cr, uid,
                                                     invoice_like_ids[0])
                        values = devline_obj.get_model_per_id(cr, uid,
                                                              invoice.id)
                        values['numfactura'] = invoice.number
                        # Si hem trobat el numero, escribim a la linia el
                        # numero, id i model
                        linia.write(values)
                    else:
                        notrobades += 1

        if notrobades == 0:
            self.write(cr, uid, ids, {'state': 'validar'})
        else:
            raise osv.except_osv('Error',
                                 'No es poden trobar %s factures' % notrobades)

        return True

    def action_confirmar(self, cr, uid, ids, context=None):
        """passos per passar una devolució a confirmat"""

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        invoice_obj = self.pool.get('account.invoice')
        cost_obj = self.pool.get('soller.impagats.devolucio.despeses')

        undo_invoice_ids = []
        undo_factura_ids = []

        for devolucio in self.browse(cr, uid, ids):
            for linia in devolucio.linies_ids:
                if linia.model == 'giscedata.facturacio.factura':
                    undo_factura_ids.append(linia.invoice_id)
                else:
                    undo_invoice_ids.append(linia.invoice_id)
            #Deshacemos los pagos de todos los account.invoice encontrados
            invoice_obj.undo_payment(cr, uid, undo_invoice_ids, context)
            invoice_obj.write(cr, uid, undo_invoice_ids,
                              {'estat_impagat': 'avis1'})
            #Deshacemos los pagos de todas las giscefacturas encontradas
            factura_obj.undo_payment(cr, uid, undo_factura_ids, context)
            factura_obj.write(cr, uid, undo_factura_ids,
                              {'estat_impagat': 'avis1'})
            #Añadir coste extra devolución
            cost_obj.add_extra_cost(cr, uid, undo_invoice_ids,
                                    'account.invoice',
                                    context=context)

        self.write(cr, uid, ids, {'state': 'confirmat'})
        return True

    def action_generate_files(self, cursor, uid, ids, context={}):
        '''
            Generate warning files for invoice refunds
        '''
        linies_obj = self.pool.get('soller.impagats.devolucio.linia')

        if not context:
            context = {}

        if isinstance(ids, (list, tuple)):
            refund_id = ids[0]
        else:
            refund_id = ids
        read_fields = ['name', 'linies_ids']
        refund_vals = self.read(cursor, uid, refund_id, read_fields)

        slice_size = int(config.get('fact_slice_size', '100'))

        if not refund_vals['linies_ids']:
            return True

        read_fields = ['invoice_id', 'model']
        invoice_ids = linies_obj.read(cursor, uid, refund_vals['linies_ids'],
                                read_fields)
        invoice_ids = [x['invoice_id'] for x in invoice_ids
                       if x['model'] == 'giscedata.facturacio.factura']

        warn_slices = self.slice_warnings(cursor, uid, invoice_ids,
                                          slice_size)
        for warn_slice in warn_slices:
            start = warn_slices.index(warn_slice) * slice_size
            end = start + len(warn_slice)
            start = str(start).zfill(4)
            end = str(end).zfill(4)
            filename = "avisos_devolucion_{}_{}-{}.pdf".format(
                refund_vals['name'], start, end)

            self.generate_warn_slice(cursor, uid, refund_id,
                                     warn_slice, filename)

        return True

    def generate_warn_slice(self, cursor, uid, refund_id, fact_ids, filename,
                            context=None):
        phantom_obj = self.pool.get('phantom.report.background')

        if not context:
            context = {}

        source_model = 'soller.impagats.devolucio'
        report_name = "soller_report_factura_avis_impagat_mako"

        ctx = context.copy()
        ctx.update({'webkit_unsigned_pdf': True})

        phantom_obj.background_report(cursor, uid, fact_ids, filename,
                                      report_name, source_model, self._name,
                                      refund_id, compress=True, context=ctx)

    def slice_warnings(self, cursor, uid, lines_ids, slice_size):
        num_slices = ((len(lines_ids) % slice_size) and
                      (len(lines_ids) / slice_size + 1) or
                      (len(lines_ids) / slice_size))
        return [lines_ids[x:x + slice_size]
                for x in range(0, num_slices * slice_size, slice_size)]

    def _get_total(self, cr, uid, ids, name, arg, context=None):
        """ Funcion que devuelve la suma total de la devolucion """

        linia_obj = self.pool.get('soller.impagats.devolucio.linia')

        res = {}
        for devolucio in self.browse(cr, uid, ids):
            total = 0
            for linia in devolucio.linies_ids:
                total += linia_obj.read(cr, uid, linia.id,
                                        ['import'])['import']
            res[devolucio.id] = total

        return res

    _columns = {
        'name': fields.char('Referencia', size=64, required=True,
                            readonly=True,
                            states={'esborrany': [('readonly', False)]}),
        'date': fields.date('Data devolució', readonly=True,
                            states={'esborrany': [('readonly', False)]}),
        'linies_ids': fields.one2many('soller.impagats.devolucio.linia',
                                      'devolucio_id', 'Linies',
                                      required=False,
                                      readonly=True,
                                      states={'esborrany':
                                              [('readonly', False)]}),
        'remesa_id': fields.many2one('payment.order', 'Remesa', required=False,
                                     readonly=True,
                                     states={'esborrany':
                                             [('readonly', False)]}),
        'state': fields.selection(_soller_impagats_devolucio_states_selection,
                                  'Estado', required=True, readonly=True),
        'total': fields.function(_get_total, method=True, type='float',
                                 digits=(16, 2), readonly=True,
                                 string='Total', store=False),
    }

    _defaults = {
        'state': lambda *a: 'esborrany',

    }
SollerImpagatsDevolucio()


class SollerImpagatsDespesaDevolucio(osv.osv):

    _name = 'soller.impagats.devolucio.despesa'

    def add_extra_cost(self, cursor, uid, invoice_ids, model, context=None):
        '''
            Add extra cost in policy if necessary
        '''
        invoice_obj = self.pool.get(model)

        num_days = 365
        for invoice_id in invoice_ids:
            read_fields = ['journal_id', 'rectificative_type']
            invoice_vals = invoice_obj.read(cursor, uid, invoice_id,
                                            read_fields)

            search_params = [
                ('journal_id', '=', invoice_vals['journal_id'][0]),
                ('rectificative_type', '=', invoice_vals['rectificative_type']),
            ]
            return_cost_ids = self.search(cursor, uid, search_params)

            if len(return_cost_ids) > 1:
                msg = _('Hi ha més d\'una regla de despeses de devolució que '
                        'compleix les condicions de la factura {}'
                        ).format(invoice_id)
                raise osv.except_osv('Error', msg)

            if return_cost_ids:
                read_fields = ['max_returns', 'price']
                returns_vals = self.read(cursor, uid, return_cost_ids[0],
                                         read_fields)
                num_returns = self.get_num_returns(cursor, uid, invoice_id,
                                                   num_days, model, context)
                if num_returns > returns_vals['max_returns']:
                    contract_id = self.get_contract_id(cursor, uid, invoice_id,
                                                       model, context=context)
                    self.create_extra_line(cursor, uid, contract_id, model,
                                           return_cost_ids[0], context=context)

    def get_contract_id(self, cursor, uid, invoice_id, model, context=None):
        '''
            Get operator contract id or electric policy id from invoice
        '''
        contract_obj = self.pool.get('soller.operador.contrato')
        policy_obj = self.pool.get('giscedata.polissa')
        invoice_obj = self.pool.get(model)

        if 'account.invoice' == model:
            invoice_vals = invoice_obj.read(cursor, uid, invoice_id, ['origin'])
            search_params = [('name', '=', invoice_vals['origin'])]
            return contract_obj.search(cursor, uid, search_params)[0]
        else:
            invoice_vals = invoice_obj.read(cursor, uid, invoice_id, ['name'])
            search_params = ['name', '=', invoice_vals['name']]
            return policy_obj.search(cursor, uid, search_params)[0]

    def create_extra_line(self, cursor, uid, contract_id, model,
                          return_cost_id, context=None):
        '''
            Create extra line in contract
        '''
        product_obj = self.pool.get('product.product')
        extra_obj = self.pool.get('soller.operador.extra')
        period_obj = self.pool.get('account.period')

        return_cost = self.browse(cursor, uid, return_cost_id)

        if 'account.invoice' == model:
            search_params = [('default_code', '=', 'Costdevolucio')]
            product_ids = product_obj.search(cursor, uid, search_params)

            # Get the next period
            period_date = datetime.now() + timedelta(days=30)
            search_params = [('date_start', '<=', period_date),
                             ('date_stop', '>=', period_date)]
            period_ids = period_obj.search(cursor, uid, search_params)

            vals = {
                'name': 'Gastos devolución',
                'contracte_id': contract_id,
                'period_id': period_ids[0],
                'price_unit': return_cost.price,
                'quantity': 1,
                'product_id': product_ids[0],
            }

            extra_obj.create(cursor, uid, vals)

    def get_num_returns(self, cursor, uid, invoice_id, days, model,
                        context=None):
        '''
            Get number of returned invoices in the last X days for a contract.
        '''
        line_obj = self.pool.get('soller.impagats.devolucio.linia')
        invoice_obj = self.pool.get(model)

        if model == 'account.invoice':
            field_name = 'origin'
        else:
            field_name = 'name'

        read_fields = [field_name, 'journal_id', 'rectificative_type']
        invoice_vals = invoice_obj.read(cursor, uid, invoice_id, read_fields)
        contract_name = invoice_vals[field_name]

        # Obtiene los identificadores de factura asociados al contrato
        max_date = datetime.now() - timedelta(days=days)
        search_params = [
            (field_name, '=', contract_name),
            ('state', 'in', ('open', 'paid')),
            ('journal_id', '=', invoice_vals['journal_id'][0]),
            ('date_invoice', '>', max_date),
            ('rectificative_type', '=', invoice_vals['rectificative_type'])
        ]
        invoice_ids = invoice_obj.search(cursor, uid, search_params)

        # Obtener las devoluciones
        search_params = [('invoice_id', 'in', invoice_ids),
                         ('model', '=', model),
                         ('devolucio_id.state', '=', 'confirmat')]

        line_ids = line_obj.search(cursor, uid, search_params)
        return len(line_ids)

    _columns = {
        'name': fields.char('Nom', size=64, required=True),
        'journal_id': fields.many2one('account.journal', 'Diari de pagament',
                                      required=True),
        'rectificative_type': fields.selection(RECTIFICATIVE_TYPE_SELECTION,
                                             'Tipo rectificativa'),
        'max_returns': fields.integer('Màxim devolucions', required=True),
        'price': fields.float('Cost devolució', required=True)

    }

    _defaults = {
        'rectificative_type': lambda *a: 'N',

    }

SollerImpagatsDespesaDevolucio()