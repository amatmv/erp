# -*- coding: utf-8 -*-
{
    "name": "Soller devolucions",
    "description": """
Gestió de devolucions del banc
    * Wizard per carregar el fitxer de devolucions del banc (Q34)
    * Desfà els pagos de les factures implicades
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_facturacio",
        "soller_extend_invoice",
        "soller_impagats",
        "phantom_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "soller_devolucions_view.xml",
        "soller_devolucions_report.xml",
        "soller_devolucions_data.xml",
        "wizard/wizard_carregar_devolucio_view.xml",
        "wizard/wizard_mostrar_factures_view.xml",
        "wizard/config_linies_devolucio_view.xml"
    ],
    "active": False,
    "installable": True
}
