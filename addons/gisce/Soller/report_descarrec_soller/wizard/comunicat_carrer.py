# coding=utf-8

from osv import osv


class SollerDescarrecsComunicatCarrer(osv.osv_memory):
    _name = 'giscedata.descarrecs.comunicat.carrer'
    _inherit = 'giscedata.descarrecs.comunicat.carrer'
    _rec_name = 'descarrec_name'

    def _print_report(self, cursor, uid, ids, context=None):
        vals = super(SollerDescarrecsComunicatCarrer, self)._print_report(
            cursor, uid, ids, context=context)

        vals.update({
            'report_name': 'report_fulls_penjar_soller'
        })

        return vals


SollerDescarrecsComunicatCarrer()
