# -*- coding: utf-8 -*-
import wizard
import pooler


# Carreguem el text que ja està guardat a la base de dades
def _init(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec = descarrec_obj.browse(cr, uid, data['id'], context)
  causes = descarrec.causes

  if not causes:
    causes = descarrec.descripcio
  return {
    'nuclis_afectats': descarrec.nuclis_afectats,
    'causes': causes,
    'cts_afectats': descarrec.cts_afectats,
    'carrers_afectats': data.get('carrers_afectats', descarrec.carrers_afectats_diari)
  }

_init_form = """<?xml version="1.0"?>
<form string="Comunicat Indústria">
  <separator string="CTS afectats" colspan="4" />
  <field name="cts_afectats" nolabel="1" colspan="4" />
  <separator string="Nuclis afectats" colspan="4" />
  <field name="nuclis_afectats" nolabel="1" colspan="4" width="500" height="50"/>
  <separator string="Descripció dels treballs" colspan="4" />
  <field name="causes" nolabel="1" colspan="4" width="500" height="50"/>
  <separator string="Carrers afectats" colspan="4" />
  <field name="carrers_afectats" nolabel="1" colspan="4" width="500" height="200"/>
</form>"""

_init_fields = {
  'nuclis_afectats': {'string': 'Nuclis afectats', 'type': 'text'},
  'causes': {'string': 'Causes', 'type': 'text'},
  'cts_afectats': {'string': 'CTS afectats', 'type': 'text', 'readonly': True},
  'carrers_afectats': {'string': 'Carrers afectats', 'type': 'text'},
}

def _generar_carrers(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec = descarrec_obj.browse(cr, uid, data['id'], context)
  # Generem el text de carrers afectats si no hi ha res guardat
  carrers = {}
  for client in descarrec.clients:
    if not carrers.has_key(client.street):
      carrers[client.street] = []
    try:
      if int(client.number) and client.number not in carrers[client.street]:
        carrers[client.street].append(client.number)
    except:
      pass
  carrers_str = []
  for carrer in carrers.keys():
    if len(carrers[carrer]) > 2:
      carrers_str.append(carrer)

  data['carrers_afectats'] = ', '.join(carrers_str)
  return {}

# Guardem el text que ens han introduït amb el formulari
def _save_text(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec_obj.write(cr, uid, [data['id']], {'nuclis_afectats': data['form']['nuclis_afectats'], 'causes': data['form']['causes'], 'carrers_afectats_diari': data['form']['carrers_afectats']})
  return {}


def _print(self, cr, uid, data, context={}):
  return {'ids': [data['id']]}
  

class giscedata_descarrecs_comunicat_diari(wizard.interface):

  states = {
    'init': {
    	'actions': [_init],
      'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancelar', 'gtk-cancel'),('generar_carrers', 'Generar text carrers', 'gtk-refresh'), ('save_text', 'Imprimir', 'gtk-print')]}
    },
    'generar_carrers': {
      'actions': [_generar_carrers],
      'result': {'type': 'state', 'state': 'init'},
    },
    'save_text': {
    	'actions': [_save_text],
    	'result': {'type': 'state', 'state': 'print'}
    },
    'print': {
      'actions': [_print],
      'result': {'type': 'print', 'report':
      'giscedata.descarrecs.soller.comunicat_diari', 'get_id_from_action':True, \
      'state':'end'}
    },
  }
giscedata_descarrecs_comunicat_diari('giscedata.descarrecs.soller.comunicat.diari')
