<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  
  <xsl:template match="descarrec_list">
    <xsl:apply-templates select="descarrec" />
  </xsl:template>

  <xsl:template match="descarrec">
    <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.6cm" />
          </pageGraphics>
          <frame id="first" x1="1.5cm" y1="1cm" width="6.5cm" height="240mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
      
      <paraStyle name="text"
      		fontName="Helvetica"
      		alignment="CENTER"
      		fontSize="8"
      		leading="8" />
      
      <paraStyle name="textcarrer"
      		fontName="Times-Roman"
      		alignment="CENTER"
      		fontSize="6"
      		leading="6" />
      
      <paraStyle name="textbold"
      		fontName="Helvetica-Bold"
      		alignment="CENTER"
      		fontSize="8"
      		leading="8" />
      		
      <blockTableStyle id="taula">
      	<lineStyle kind="LINEABOVE" start="0,0" stop="0,0" thickness="1" colorName="black" />
      </blockTableStyle>
      		

      </stylesheet>
    
      <story>
      	<para style="textbold">AV�S DE TALLS DE CORRENT</para>
      	<spacer length="0.3cm" />
      	<para style="textcarrer">Per manteniment d'instal�lacions hem de treballar els sectors de</para>
      	<spacer length="0.3cm" />
      	<para style="textbold"><xsl:value-of select="data_comunicat_diari" /></para>
      	<para style="textbold">ENTRE LES <xsl:value-of select="substring(data_inici, 12, 5)" /> I LES <xsl:value-of select="substring(data_final, 12, 5)" /> H</para>
      	<para style="text"><xsl:value-of select="translate(cts_afectats, '&quot;', '')" /></para>
      	<para style="textcarrer">(<xsl:value-of select="carrers_afectats" />)</para>
      	<spacer length="0.3cm" />
      	<para style="textcarrer">Per la qual cosa ens veiem obligats a interrompre el subministrament</para>
      	<spacer length="0.2cm" />
      	<blockTable rowHeights="1mm" colWidths="3cm" style="taula">
      		<tr><td></td></tr>
      	</blockTable>
      	<spacer length="0.3cm" />
      	<para style="textcarrer">Treballam per millorar el servei. Us pregam disculpeu les mol�sties</para>
      	<para style="textcarrer">Tel�fon d'atenci� al client <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone" /></para>
      	<para style="textcarrer"><b>www.el-gas.es</b></para>
      </story>
    </document>

    </xsl:template>

</xsl:stylesheet>
