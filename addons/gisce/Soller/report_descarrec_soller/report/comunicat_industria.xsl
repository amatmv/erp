<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template name="doPara">
  	<xsl:param name="txt"/>
  	<para style="text" fontSize="8"><xsl:value-of select="$txt"/></para>
  </xsl:template>
  <xsl:template name="strSplit">
	<xsl:param name="string" />
	<xsl:param name="pattern" />

	<xsl:choose>
		<xsl:when test="contains($string, $pattern)">
			<xsl:call-template name="strSplit">
    		    <xsl:with-param name="string" select="substring-before($string, $pattern)" />
	    		<xsl:with-param name="pattern" select="$pattern" />
			</xsl:call-template>

			<xsl:call-template name="strSplit">
    		    <xsl:with-param name="string" select="substring-after($string, $pattern)" />
	    		<xsl:with-param name="pattern" select="$pattern" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
		<xsl:call-template name="doPara">
			<xsl:with-param name="txt" select="$string"/>
		</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>

  </xsl:template>

  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  
  <xsl:template match="descarrec_list">
    <xsl:apply-templates select="descarrec" />
  </xsl:template>

  <xsl:template match="descarrec">
    <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/report_descarrec_soller/report/vse.jpg" width="18.8cm"
            height="4.8cm" x="1cm" y="24.9cm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3.2cm" y="27.8cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3.2cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
          	<setFont name="Helvetica" size="8" />
          	<drawRightString x="19cm" y="28.7cm">CL Sa Mar, 146</drawRightString>
          	<drawRightString x="19cm" y="28.4cm">07100 S�ller (Illes Balears)</drawRightString>
          	<drawRightString x="19cm" y="28.1cm">Tel. 971 630 198 - Fax 971 631 108</drawRightString>
          	<!-- Firma -->
            <drawString x="11cm" y="5cm">Por VALL DE SOLLER ENERGIA SLU</drawString>
            <drawString x="11cm" y="2.5cm">Fdo. Pelayo Quir�s Maquinay</drawString>
            <drawString x="11cm" y="2.2cm">Ingeniero T�cnico</drawString>
            <!-- Final firma -->
          	<rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>
          </pageGraphics>
          <frame id="first" x1="1.5cm" y1="1cm" width="18cm" height="240mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
      
      <paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      <blockTableStyle id="taula">
      	<blockValign value="TOP" start="0,1" stop="0,1"/>
      	<blockValign value="MIDDLE" start="2,1" stop="2,1"/>
      	<lineStyle kind="GRID" colorName="black" />
      	<blockValign value="MIDDLE" start="0,0" stop="2,0"/>
      	<blockBottomPadding length="5" start="0,0" stop="-1,-1"/>
		<blockTopPadding length="5" start="0,0" stop="-1,-1"/>
      </blockTableStyle>

      </stylesheet>
    
      <story>
      	<para style="text" alignment="RIGHT">S�ller, a <xsl:value-of select="data" /></para>
      	
      	<spacer length="1cm" />
      	
      	<para style="text" alignment="LEFT">DIRECTOR GENERAL DE INDUSTRIA</para>
      	<para style="text" alignment="LEFT">CONSELLERIA DE COMER� INDUSTRIA I ENERGIA</para>
      	<para style="text" alignment="LEFT">Basti� d'en Sanoguera n�2 - 07003 - Palma</para>
      	
      	<spacer length="2cm" />
      	
      	<para style="text" alignment="JUSTIFY">ASUNTO: SOLICITUD AUTORIZACION CORTE DE SUMINISTRO POR MANTENIMIENTO DE INSTALACIONES EN <xsl:value-of select="atbt" /></para>
      	
      	<spacer length="0.5cm" />
      	<para style="text" alignment="JUSTIFY">Con motivo de tener que llevar a cabo trabajos de mantenimiento en nuestra red de <xsl:value-of select="atbt" />, el d�a <xsl:value-of select="data_comunicat_industria" /> y en aplicaci�n del Art. 101 del vigente reglamento de distribuci�n, por el presente fax solicitamos de esta Direcci�n General el correspondiente permiso al objeto de poder calificar de programa dicha interrupci�n La duraci�n del mismo est� programada seg�n queda especificado en cada centro de transformaci�n afectado.</para>
      	
      	<spacer length="0.5cm" />
      	<para style="text" alignment="JUSTIFY">De acuerdo con el citado Reglamento consideraremos otorgado el permiso si transcurrido el plazo de 48 horas no se establece objeci�n alguna.</para>
      	
      	<spacer length="0.5cm" />
      	<para style="text" alignment="JUSTIFY">Dicha interrupci�n ser� notificada a los afectados mediante carteles anunciadores en los dos medios de comunicaci�n escrita de mayor difusi�n en la comarca y los suministros mas relevantes ser�n comunicados por personal propio de esta compa��a.</para>
      	
      	<spacer length="0.5cm" />
      	<para style="text" alignment="JUSTIFY">Los centros de transformaci�n que quedan afectados son los siguientes:</para>
      	
      	<spacer length="0.5cm" />
      	<blockTable colWidths="6cm,9cm,3cm" style="taula">
      	  <tr>
      	  	<td><para style="text" alignment="CENTER">CENTRO DE TRANSFORMACI�N</para></td>
      	  	<td><para style="text" alignment="CENTER">DIRECCIONES AFECTADAS</para></td>
      	  	<td><para style="text" alignment="CENTER">DURACI�N</para></td>
      	  </tr>
      	  <tr>
      	  	<td><xpre style="text">
		<xsl:if test="xarxa='1'">
		<xsl:value-of select="cts_afectats" />
		</xsl:if>
		<xsl:if test="xarxa='2'">
		<xsl:value-of select="nuclis_afectats" />
		</xsl:if>
		</xpre></td>
      	  	<td><xsl:call-template name="strSplit">
		<xsl:with-param name="string" select="carrers_afectats" />
		<xsl:with-param name="pattern" select="'&#xA;'" />
	</xsl:call-template></td>
      	  	<td><xpre style="text"><xsl:value-of select="substring(data_inici, 12, 5)" /> a <xsl:value-of select="substring(data_final, 12, 5)" /> H</xpre></td>
      	  </tr>
      	</blockTable>
      	<!--
      	<spacer length="0.5cm" />
      	<para style="text" alignment="RIGHT">Por VALL DE SOLLER ENERGIA SLU</para>
      	<spacer length="1cm" />
      	<para style="text" alignment="RIGHT">Fdo. Pelayo Quir�s Maquinay</para>
      	-->
      </story>
    </document>

    </xsl:template>

</xsl:stylesheet>
