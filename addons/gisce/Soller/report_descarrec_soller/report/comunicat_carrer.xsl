<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  
  <xsl:template match="descarrec_list">
    <xsl:apply-templates select="descarrec" />
  </xsl:template>

  <xsl:template match="descarrec">
    <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
            <!-- Primera cabecera -->
          	<image file="addons/report_descarrec_soller/report/vse.jpg" width="18.8cm" height="4.8cm" x="1cm" y="24.9cm" />
          	<image file="addons/report_descarrec_soller/report/vse.jpg" width="18.8cm" height="4.8cm" x="1cm" y="12.9cm" />
          	<!--<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3.2cm" y="27.8cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3.2cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>-->
          	<setFont name="Helvetica" size="8" />
            <drawRightString x="19cm" y="28.7cm">CL Sa Mar, 146</drawRightString>
          	<drawRightString x="19cm" y="28.4cm">07100 S�ller (Illes Balears)</drawRightString>
            <drawRightString x="19cm" y="16.7cm">CL Sa Mar, 146</drawRightString>
          	<drawRightString x="19cm" y="16.4cm">07100 S�ller (Illes Balears)</drawRightString>

          	<rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <!--<drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>-->
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="230mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
      
      <paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="14" />
      
      <paraStyle name="text_gran"
      		fontName="Helvetica"
      		fontSize="16" />

      </stylesheet>
    
      <story>
        <para style="text">AV�S D'INTERRUPCI� DEL SUBMINISTRAMENT EL�CTRIC EN AQUESTA ZONA</para>
        <spacer length="0.5cm" />
        <para style="text_gran" alignment="center">PER AL DIA <xsl:value-of select="substring(data_inici, 9, 2)"/></para>
        <spacer length="0.5cm" />
	<para style="text_gran" alignment="center">
          <xsl:choose>
            <xsl:when test="substring(data_inici, 6, 2) = '01'">DE GENER</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '02'">DE FEBRER</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '03'">DE MAR�</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '04'">D'ABRIL</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '05'">DE MAIG</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '06'">DE JUNY</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '07'">DE JULIOL</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '08'">D'AGOST</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '09'">DE SETEMBRE</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '10'">D'OCTUBRE</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '11'">DE NOVEMBRE</xsl:when>
            <xsl:otherwise>DE DESEMBRE</xsl:otherwise>
          </xsl:choose> DE <xsl:value-of select="substring(data_inici, 1, 4)"/>
        </para>
        <spacer length="0.5cm" />
        <para style="text_gran" alignment="center">DE LES <xsl:value-of select="substring(data_inici, 12, 5)"/> A LES <xsl:value-of select="substring(data_final, 12, 5)"/></para>
        <spacer length="1cm" />
        <para style="text" alignment="center">TREBALLAM PER A UN MILLOR SERVEI - GR�CIES</para>
        <spacer length="1cm" />
        <para style="text" alignment="center">TEL�FON D'ATENCI� AL P�BLIC <xsl:value-of select="//corporate-header/corporation/address[type='contact']/phone"/> - www.el-gas.es</para>
        
        <spacer length="7cm" />
        <para style="text">AV�S D'INTERRUPCI� DEL SUBMINISTRAMENT EL�CTRIC EN AQUESTA ZONA</para>
        <spacer length="0.5cm" />
	<para style="text_gran" alignment="center">PER AL DIA <xsl:value-of select="substring(data_inici, 9, 2)"/></para>
        <spacer length="0.5cm" />
        <para style="text_gran" alignment="center">
          <xsl:choose>
            <xsl:when test="substring(data_inici, 6, 2) = '01'">DE GENER</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '02'">DE FEBRER</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '03'">DE MAR�</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '04'">D'ABRIL</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '05'">DE MAIG</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '06'">DE JUNY</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '07'">DE JULIOL</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '08'">D'AGOST</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '09'">DE SETEMBRE</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '10'">D'OCTUBRE</xsl:when>
            <xsl:when test="substring(data_inici, 6, 2) = '11'">DE NOVEMBRE</xsl:when>
            <xsl:otherwise>DE DESEMBRE</xsl:otherwise>
          </xsl:choose> DE <xsl:value-of select="substring(data_inici, 1, 4)"/>
        </para>
        <spacer length="0.5cm" />
        <para style="text_gran" alignment="center">DE LES <xsl:value-of select="substring(data_inici, 12, 5)"/> A LES <xsl:value-of select="substring(data_final, 12, 5)"/></para>
        <spacer length="1cm" />
        <para style="text" alignment="center">TREBALLAM PER A UN MILLOR SERVEI - GR�CIES</para>
        <spacer length="1cm" />
        <para style="text" alignment="center">TEL�FON D'ATENCI� AL P�BLIC <xsl:value-of select="//corporate-header/corporation/address[type='contact']/phone"/> - www.el-gas.es</para>
      	
      </story>
    </document>

    </xsl:template>

</xsl:stylesheet>
