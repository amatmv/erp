## Prints general release papers from context['active_ids']
## It does not receive the right values at objects

<%

from tools import config
from types import FunctionType
from datetime import datetime
from babel.dates import format_date, format_time

addons_path = config['addons_path']
cursor = objects[0]._cr
pool = objects[0].pool
uid = user.id
giscedata_descarrecs_descarrec_obj = pool.get('giscedata.descarrecs.descarrec')
locale = 'ca_ES'
active_ids = context['active_ids']
descarrecs_data = giscedata_descarrecs_descarrec_obj.read(cursor, uid, active_ids, ['data_inici', 'data_final'])

date_format = '%Y-%m-%d %H:%M:%S'
time_format = '%H:%M:%S'
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="${addons_path}/report_descarrec_soller/report/static/css/print.css"/>
    <link rel="stylesheet" type="text/css" href="${addons_path}/report_descarrec_soller/report/static/css/extra.css"/>
</head>
<body>
    <div class="box">
        %for descarrec in descarrecs_data:
            <%
            data_inici = descarrec['data_inici']
            data_final = descarrec['data_final']
            start_date = datetime.strptime(data_inici, date_format)
            finish_date = datetime.strptime(data_final, date_format)

            date = format_date(start_date, format='long', locale=locale)

            start_hour = format_time(start_date, format="short", locale=locale)
            finish_hour = format_time(finish_date, format="short", locale=locale)
            %>
            %for index in xrange(2):
                <div style="margin: 0 0; width: 100%">
                    <img style="width: 100%; margin: 0 auto; display: block;"
                         src="${addons_path}/report_descarrec_soller/report/vse.jpg"/>

                    <div class="text-box">
                        <p class="text-uppercase text-center large">
                            AVÍS D'INTERRUPCIÓ DEL SUBMINISTRAMENT ELÈCTRIC EN AQUESTA ZONA
                        </p>
                           <p class="text-uppercase text-center extra-large">
                               PER AL DIA
                           </p>
                           <p class="text-uppercase text-center extra-large">
                               ${date}
                           </p>
                           <p class="text-uppercase text-center extra-large">
                               DE LES ${start_hour} A LES ${finish_hour}
                          </p>
                        <p class="text-uppercase text-center large">
                            TREBALLAM PER A UN MILLOR SERVEI - GRÀCIES
                        </p>
                        <p class="text-uppercase text-center large">
                            TELÈFON D'ATENCIÓ AL PÚBLIC 971 638 145 - www.el-gas.es
                        </p>
                    </div>
                </div>

                %if index == 0:
                    <br/>
                %endif
            %endfor
         % if descarrecs_data.index(descarrec) + 1 != len(descarrecs_data):
              <p class="page-break"></p>
         % endif
        %endfor
    </div>
</body>
</html>