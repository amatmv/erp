<%

from tools import config
from types import FunctionType
from datetime import datetime
from babel.dates import format_date, format_time

addons_path = config['addons_path']
cursor = objects[0]._cr
pool = objects[0].pool
uid = user.id
giscedata_descarrecs_descarrec_obj = pool.get('giscedata.descarrecs.descarrec')
locale = 'ca_ES'

date_format = '%Y-%m-%d %H:%M:%S'
time_format = '%H:%M:%S'
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="${addons_path}/report_descarrec_soller/report/static/css/print.css"/>
    <link rel="stylesheet" type="text/css" href="${addons_path}/report_descarrec_soller/report/static/css/comunicat_diari.css"/>
</head>
<body>
    %for descarrec in objects:
        <%
            data_inici = datetime.strptime(descarrec.data_inici, date_format)
            data_final = datetime.strptime(descarrec.data_final, date_format)

            startdate = format_date(data_inici, format='full', locale=locale)
            starthour = format_time(data_inici, format='short', locale=locale)
            finishour = format_time(data_final, format='short', locale=locale)

            carrers = descarrec.carrers_afectats_diari
            cts_afectats = descarrec.cts_afectats
        %>
        <div class="box">
            <p class="text-bold increase-space text-uppercase xl">AVÍS DE TALLS DE CORRENT</p>
            <p class="small increase-space">Per manteniment d'instal·lacions hem de treballar els sectors de</p>
             <p class="text-bold text-uppercase">${startdate}</p>
            <p class="text-bold text-uppercase">ENTRE LES ${starthour} I LES ${finishour} H</p>
            <p>${cts_afectats}</p>
            <p class="increase-space small text-uppercase">(${carrers})</p>
            <p class="small">Per la qual cosa ens veiem obligats a interrompre el subministrament</p>
            <hr class="big-gap"/>
            <p class="small">Treballam per millorar el servei. Us pregam disculpeu les molèsties</p>
            <p class="small">Telèfon d'atenció al client 971968145</p>
            <p class="small">www.el-gas.es</p>
        </div>

        % if objects.index(descarrec) + 1 != len(objects):
              <p class="page-break"></p>
        % endif
    %endfor
</body>
</html>