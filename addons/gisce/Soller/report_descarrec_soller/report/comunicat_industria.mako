<%
    from tools import config
    from types import FunctionType
    from datetime import datetime
    from babel.dates import format_date, format_time

    def extract_carrers_afectats(cursor, uid, descarrec_id, context=None):
        pool = objects[0].pool
        client_obj = pool.get('giscedata.descarrecs.descarrec.clients')
        client_ids = client_obj.search(cursor, uid, [
            ('descarrec_id', '=', descarrec_id)
        ], context=context)
        client_data = client_obj.read(cursor, uid, client_ids, ['street', 'number'])
        carrers = {}

        for client in client_data:
            street = client['street']
            number = client['number']
            carrers.setdefault(street, set())
            if number:
                carrers[street].add(number)

        return carrers

    def format_carrers_afectats(carrers):
        carrers_str = ''
        for carrer, number_set in carrers.iteritems():
            if number_set:
                number_str = ', '.join(number_set)
                carrers_str += '{}: {} <br/>'.format(carrer, number_str)
            else:
                carrers_str += '{} <br/>'.format(carrer)
        return carrers_str

    addons_path = config['addons_path']
    module = 'report_descarrec_soller'
    cursor = objects[0]._cr
    pool = objects[0].pool
    uid = user.id
    descarrec_obj = pool.get('giscedata.descarrecs.descarrec')

    date_format = '%Y-%m-%d %H:%M:%S'

    today_date = datetime.now()
    today = format_date(today_date, format='full', locale='es_ES')

    descarrec_id = data['form']['descarrec_id']
    descarrec = descarrec_obj.browse(cursor, uid, descarrec_id)

    atbt_select = dict(descarrec_obj.fields_get(cursor, uid, ['xarxa'])['xarxa']['selection'])
    atbt = descarrec.xarxa
    atbt_str = atbt_select[str(atbt)]

    carrers_afectats = extract_carrers_afectats(cursor, uid, descarrec_id)
    cts_afectats = descarrec.cts_afectats or descarrec.sollicitud.descripcio

    cts_afectats_br_format = cts_afectats.replace('\n', '<br/>').replace(',', '<br/>')
    carrers_afectats_br_format = format_carrers_afectats(carrers_afectats)

    start_hour = datetime.strptime(descarrec.data_inici, date_format)
    finish_hour = datetime.strptime(descarrec.data_final, date_format)
    start_hour_str = format_time(start_hour, format='short', locale='es_ES')
    finish_hour_str = format_time(finish_hour, format='short', locale='es_ES')
    start_date_str = format_date(start_hour, format='long', locale='es_ES')
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/${module}/report/static/css/print.css"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/${module}/report/static/css/comunicat_industria.css"/>
    </head>
    <body>
        <div class="container" style="width: 95%;">
            <div style="width: 100%;">
                <img style="width: 100%; height: 175px;" src="${addons_path}/${module}/report/vse_no_border.png"/>
            </div>

            <br/>

            <div class="letter-date">
                <p class="text-right">Soller, a ${today}</p>
            </div>

            <div class="letter-header">
                <p class="text-left text-uppercase">DIRECTOR GENERAL D'ENERGIA I CANVI CLIMÀTIC</p>
                <p>C/ Gremi de Corredors, 10 (Polígon Son Rossinyol) - 07009 Palma</p>
            </div>

            <p class="text-left text-uppercase letter-subject">
                ASUNTO: SOLICITUD AUTORIZACIÓN CORTE DE SUMINISTRO
                POR MANTENIMIENTO DE INSTALACIONES EN
            </p>

            <div class="letter-body">
                 <p>
                Con motivo de tener que llevar a cabo trabajos de mantenimiento en nuestra red de
                ${atbt_str} el día ${start_date_str} y en aplicación del Art. 101 del vigente reglamento
                de distribución, por el presente fax solicitamos de esta Dirección General el
                correspondiente permiso al objeto de poder calificar de programa dicha interrupción.
                La duración de la misma está programada según queda especificado en cada centro de
                transformación afectado.
                </p>

                <p>
                    De acuerdo con el citado Reglamento consideraremos otorgado el permiso si transcurrido
                    el plazo de 48 horas no se establece objeción alguna.
                </p>

                <p>
                    Dicha interrupción será notificada a los afectados mediante carteles anunciadores en los
                    dos medios de comunicación escrita de mayor difusión en la comarca y los suministros más
                    relevamentes serán comunicados por personal propio de esta compañía.
                </p>

                <p>
                    Los centros de transformación que quedan afectados son los siguientes:
                </p>

                <table class="transformation-centers table-adjust-space">
                    <thead>
                        <tr>
                            <th class="text-uppercase table-cell-shrink">Centro de transformación</th>
                            <th class="text-uppercase table-cell-expand">Direcciones afectadas</th>
                            <th class="text-uppercase table-cell-shrink">Duración</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="cell-top-aligned table-cell-shrink">${cts_afectats_br_format}</td>
                            <td class="table-cell-expand cell-top-aligned">${carrers_afectats_br_format}</td>
                            <td class="table-cell-shrink cell-top-aligned">${start_hour_str} a ${finish_hour_str}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="letter-signature">
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
            </div>

        </div>
    </body>
</html>