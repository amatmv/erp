# -*- coding: utf-8 -*-
{
    "name": "Reports Descàrrecs Sóller",
    "description": """
Modificació dels reports de descarrecs:
  * Comunicat a Indústria
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "base",
        "giscedata_descarrecs"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_descarrec_soller_wizard.xml",
        "report_descarrec_soller_view.xml",
        "report_descarrec_soller_report.xml"
    ],
    "active": False,
    "installable": True
}
