# -*- coding: utf-8 -*-

from osv import fields, osv
import locale
import time

class giscedata_descarrecs_descarrec(osv.osv):
  _name = "giscedata.descarrecs.descarrec"
  _inherit = "giscedata.descarrecs.descarrec"


  def _data_comunicat_industria(self, cr, uid, ids, field_name, *args):
    res = {}
    for d in self.browse(cr, uid, ids):
      try:
        locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
        s = time.strftime('%d de %B de %Y', time.strptime(d.data_inici, '%Y-%m-%d %H:%M:%S'))
      except locale.Error:
        s = '%s/%s/%s %s' % (d.data_inici[8:10],d.data_inici[5:7],d.data_inici[0:4],d.data_inici[11:16])
        pass
      u = unicode( s, "utf-8" )
      res[d.id] = u
    return res

  def _data_comunicat_diari(self, cr, uid, ids, field_name, *args):
    res = {}
    for d in self.browse(cr, uid, ids):
      try:
        locale.setlocale(locale.LC_TIME, 'ca_ES.UTF-8')
        dt = time.strptime(d.data_inici, '%Y-%m-%d %H:%M:%S')
        dia = time.strftime('%A', dt).upper()
        s = '%s, %s' % (dia, time.strftime('%d de %B de %Y', dt).upper())
      except locale.Error:
        s = '%s/%s/%s %s' % (d.data_inici[8:10],d.data_inici[5:7],d.data_inici[0:4],d.data_inici[11:16])
        pass
      u = unicode( s, "utf-8" )
      res[d.id] = u
    return res

  _columns = {
    'carrers_afectats_industria': fields.text('Carrers afectats Indústria'),
    'carrers_afectats_diari': fields.text('Carrers afectats Diari'),
    'data_comunicat_industria': fields.function(_data_comunicat_industria, method=True, type='char', size=255, string="Fecha comunicado industria"),
    'data_comunicat_diari': fields.function(_data_comunicat_diari, method=True, type='char', size=255, string="Fecha comunicado industria"),
    'at_bt': fields.text('AT/BT'),
  }

  _defaults = {
    'carrers_afectats_industria': lambda *a: '',
    'carrers_afectats_diari': lambda *a: '',
    'at_bt': lambda *a: '',
  }

giscedata_descarrecs_descarrec()

