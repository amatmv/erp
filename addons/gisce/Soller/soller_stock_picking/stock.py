# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools import float_round
from tools.translate import _


class StockPicking(osv.osv):

    _inherit = 'stock.picking'
    _order = 'id desc'

    def onchange_partner_id(self, cursor, uid, ids, partner_id, context=None):

        partner_obj = self.pool.get('res.partner')
        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not partner_id:
            res['value'].update({'address_id': ''})
            res['domain'].update({'address_id': ''})
            return res

        address_domain = [('partner_id', '=', partner_id)]
        res['domain'].update({'address_id': address_domain})

        addresses = partner_obj.address_get(cursor, uid, [partner_id],
                                            ['contact'])
        address_id = addresses['contact']
        res['value'].update({'address_id': address_id})

        return res

    def create(self, cursor, uid, vals, context=None):

        address_obj = self.pool.get('res.partner.address')

        if vals and 'address_id' in vals and not 'partner_id' in vals:
            address = address_obj.browse(cursor, uid, vals['address_id'])
            vals.update({'partner_id': address.partner_id.id})

        return super(StockPicking, self).create(cursor, uid, vals,
                                                context=context)

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'origin_date': fields.date('Origin date'),
    }

    def _check_partner_and_origin_uniqness(self, cursor, uid, ids,
                                           context=None):
        for picking in self.browse(cursor, uid, ids, context):
            partner_id = picking.partner_id.id
            origin = picking.origin

            if partner_id and origin:
                search_params = [('partner_id', '=', partner_id),
                                 ('origin', 'ilike', origin),
                                 ('id', '<>', picking.id)]
                matching_stocks = self.search(cursor, uid, search_params)

                if len(matching_stocks) > 0:
                    return False

        return True

    _constraints = [(_check_partner_and_origin_uniqness,
                     _('A picking with same partner and origin already exists'),
                     ['partner_id', 'origin'])]


StockPicking()
