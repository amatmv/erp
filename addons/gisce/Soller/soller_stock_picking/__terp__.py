# -*- coding: utf-8 -*-
{
    "name": "Stock Picking",
    "description": """Stock picking mods""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "stock",
    "depends":[
        "stock"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "stock_view.xml"
    ],
    "active": False,
    "installable": True
}
