# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools import float_round


class ResPartnerAddress(osv.osv):

    _inherit = 'res.partner.address'

    def name_search(self, cursor, user, name, args=None,
                    operator='ilike', context=None, limit=80):
        '''Remove contact_display from context when searching'''

        if not context:
            context = {}

        if 'contact_display' in context:
            del context['contact_display']

        return super(ResPartnerAddress,
                     self).name_search(cursor, user, name, args=args,
                                       operator=operator, context=context,
                                       limit=limit)

ResPartnerAddress()
