# -*- coding: utf-8 -*-
{
    "name": "Soller Operador CRM",
    "description": """
Integracion del modulo de operador con el CRM:
  * Seguimiento de incidencias
  * Calculo de compensaciones por SLA
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "crm_generic",
        "soller_operador"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "operador_crm_data.xml",
        "operador_crm_view.xml",
        "operador_crm_report.xml"
    ],
    "active": False,
    "installable": True
}
