# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from datetime import datetime, timedelta

class SollerOperadorCRM(osv.osv):
    '''
    Calcular la compensacion de un contrato segun las incidencias
    '''
    _name = 'soller.operador.contrato'
    _inherit = 'soller.operador.contrato'
    
    def calcular_sla(self, cr, uid, contrato_id, 
                            data_inici, data_final,context={}):
        '''funcion para calcular si se tiene que abonar la cuota mensual
        segun el tiempo que haya estado sin servicio un contrato'''

        case_obj = self.pool.get('crm.case')
        equip_obj = self.pool.get('soller.operador.equip')

        #Buscamos todos los casos entre las fechas de facturacion
        #para la seccion operador, categoria incidencias empresa,
        #y relacionadas con un contrato
        search_params = [('section_id.code','=','OP'),
                         ('categ_id.name','=','INCIDENCIA EMPRESA'),
                         ('date','>=',data_inici),
                         ('date','<=',data_final),
                         ('ref','=','soller.operador.contrato,%d'%contrato_id),
                         ('state','not in',('draft','cancel')),
                        ]
        case_ids = case_obj.search(cr, uid, search_params)

        #Buscamos todos los casos entre las fechas de facturacion
        #para la seccion operador, categoria incidencia general
        search_params = [('section_id.code','=','OP'),
                         ('categ_id.name','=','INCIDENCIA GENERAL'),
                         ('date','>=',data_inici),
                         ('date','<=',data_final),
                         ('state','not in',('draft','cancel')),
                        ]
        case_ids.extend(case_obj.search(cr, uid, search_params))

        #Buscamos todos los casos entre las fechas de facturacion
        #para la seccion operador, categoria incidencia empresa
        #y relacionadas con un repetidor
        #primero buscamos el repetidor del contrato que nos pasan
        search_params = [('contracte_id','=',contrato_id)]
        equip_id = equip_obj.search(cr, uid, search_params)
        repetidor_id = equip_obj.read(cr, uid, equip_id, ['repetidor_id'])
        if len(repetidor_id):
            if repetidor_id[0].get('repetidor_id',False):
                repetidor_id = repetidor_id[0]['repetidor_id'][0]
                #Buscamos si ese repetidor ha tenido incidencias
                search_params = [('section_id.code','=','OP'),
                            ('categ_id.name','=','INCIDENCIA EMPRESA'),
                            ('date','>=',data_inici),
                            ('date','<=',data_final),
                            ('ref','=','soller.operador.repetidor,%d'%repetidor_id),
                            ('state','not in',('draft','cancel')),
                            ]
                case_ids.extend(case_obj.search(cr, uid, search_params))

        horas = 0
        for case in case_obj.browse(cr, uid, case_ids):
            tiempo = datetime.strptime(case.date_deadline, '%Y-%m-%d %H:%M:%S')\
                     - datetime.strptime(case.date, '%Y-%m-%d %H:%M:%S')
            horas += tiempo.days*24+tiempo.seconds/3600
        return horas       
            


SollerOperadorCRM()
