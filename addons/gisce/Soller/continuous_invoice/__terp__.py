# -*- coding: utf-8 -*-
{
    "name": "Soller Continuous Invoice",
    "description": """
    Continuous invoice
  """,
    "version": "0-dev",
    "author": "Toni Llado",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "giscedata_facturacio_comer",
        "soller_facturacio_validation",
        "giscedata_remeses"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "continuous_invoice_view.xml",
        "continuous_invoice_data.xml",
        "wizard/wizard_add_invoices_remittance_view.xml",
        "continuous_invoice_report.xml",
        "wizard/wizard_factures_per_email_view.xml",
        "wizard/wizard_multi_skip_continuous_validate_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
