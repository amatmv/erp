# -*- coding: utf-8 -*-
from osv import osv, fields


class MultiSkipValidate(osv.osv_memory):

    _name = 'wizard.multi.skip.continuous.validate'

    def action_multi_skip(self, cursor, uid, ids, context=None):
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')

        if not context:
            context = {}

        clot_ids = context.get('active_ids', [])
        clot_obj.write(cursor, uid, clot_ids,
                       {'pending_validation': False})

        return {}

MultiSkipValidate()