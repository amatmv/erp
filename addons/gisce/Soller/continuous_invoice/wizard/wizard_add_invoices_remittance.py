# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _


class WizardAfegirFacturesRemesaSoller(osv.osv_memory):

    _inherit = 'wizard.afegir.factures.remesa'

    def get_invoices(self, cursor, uid, context=None):
        """
        Gets selected invoices
        :return: list with invoices ids
        """
        lot_obj = self.pool.get('continuous.invoicing.lot')
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')
        if context is None:
            context = {}

        model = context.get('model')
        if model == 'continuous.invoicing.lot':
            lot_id = context.get('active_ids', False)
            if lot_id:
                lot_id = lot_id[0]

            contracte_lot_ids = lot_obj.read(
                cursor, uid, lot_id, ['contract_lot_ids'], context
            )['contract_lot_ids']
            lot_vals = clot_obj.read(
                cursor, uid, contracte_lot_ids, ['invoice_ids']
            )
            # list of lists
            invoices_in_lot = [x['invoice_ids'] for x in lot_vals]
            # only one invoice list
            invoice_ids = [i for s in invoices_in_lot for i in s]
            return invoice_ids

        else:
            return super(WizardAfegirFacturesRemesaSoller, self
                         ).get_invoices(cursor, uid, context=context)

    def action_afegir_factures(self, cursor, uid, ids, context):
        """
            Add invoices to remittance
        """
        if not context:
            context = {}

        model = context.get('model')

        if model == 'continuous.invoicing.lot':
            if isinstance(ids, list):
                ids = ids[0]
            wiz = self.browse(cursor, uid, ids, context)

            lot_id = context.get('active_ids', False)
            if lot_id:
                lot_id = lot_id[0]
            lot_obj = self.pool.get('continuous.invoicing.lot')
            lot = lot_obj.browse(cursor, uid, lot_id, context)
            ctx = context.copy()
            if wiz.invoices_limit:
                ctx['invoices_limit'] = wiz.invoices_limit
            if wiz.not_ordered:
                ctx['not_ordered'] = wiz.not_ordered
            if wiz.use_order_payment_type:
                ctx['use_order_payment_type'] = wiz.use_order_payment_type
            lot.afegir_factures_remesa(wiz.order.id, context=ctx)
            wiz.write({'state': 'end'})
        else:
            super(WizardAfegirFacturesRemesaSoller, self
                  ).action_afegir_factures(cursor, uid, ids, context=context)


WizardAfegirFacturesRemesaSoller()