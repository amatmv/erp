# -*- coding: utf-8 -*-

from osv import osv, fields
from addons import get_module_resource

class WizardFacturesPerEmail(osv.osv_memory):
    _inherit = 'wizard.factures.per.email'

    def action_mostrar_sense_email(self, cursor, uid, ids, context=None):


        active_model = context.get('active_model', False)
        if active_model == 'continuous.invoicing.lot':
            query_file = get_module_resource('continuous_invoice',
                                             'sql',
                                             'query_mostrar_sense_email.sql')
            query = open(query_file).read()
            lot_id = context.get('active_id', False)
            cursor.execute(query, (lot_id, ))

            fact_ids = [x[0] for x in cursor.fetchall()]

            return {
                'name': 'Factures',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.facturacio.factura',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', fact_ids)],
            }

        else:
            return super(WizardFacturesPerEmail,
                         self).action_mostrar_sense_email(cursor, uid, ids,
                                                          context=context)

    def _fact_per_lang(self, cursor, uid, context=None):

        if not context:
            context = {}

        active_model = context.get('active_model', False)
        if active_model == 'continuous.invoicing.lot':
            query_file = get_module_resource('continuous_invoice',
                                             'sql',
                                             'query_fact_per_lang.sql')
            query = open(query_file).read()
            lot_id = context.get('active_id', False)
            cursor.execute(query, (lot_id, ))

            return [x for x in cursor.fetchall()]

        else:
            return super(WizardFacturesPerEmail,
                         self)._fact_per_lang(cursor, uid, context=context)

    def action_enviar_lot_per_mail(self, cursor, uid, ids, context=None):
        """Enviar les factures la pòlissa de les quals està configurada
           amb l'enviament per correu electrònic
        """
        if not context:
            context = {}

        active_model = context.get('active_model', False)
        if active_model == 'continuous.invoicing.lot':
            query_file = get_module_resource('continuous_invoice',
                                             'sql',
                                             'query_fact_per_enviar.sql')

            query = open(query_file).read()
            lot_id = context.get('active_id', False)
            cursor.execute(query, (lot_id,))

            fact_ids = [x[0] for x in cursor.fetchall()]

            return self.send_invoices_by_email(cursor, uid, ids, fact_ids,
                                               context=context)
        else:
            return super(WizardFacturesPerEmail,
                         self).action_enviar_lot_per_mail(cursor, uid, ids,
                                                          context=context)

WizardFacturesPerEmail()