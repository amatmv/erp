# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
from tools.translate import _

class GiscedataFacturacioContinuous(osv.osv):

    _inherit = 'giscedata.facturacio.factura'

    def create_rectification_invoice(self, cursor, uid, ids, tipus='R',
                                     context=None):
        '''
            Create rectification invoice for continuous invoice
        '''

        if context is None:
            context = {}

        journal_obj = self.pool.get('account.journal')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')

        res_ids = []
        for factura in self.browse(cursor, uid, ids, context):
            polissa_id = factura.polissa_id.id

            journal_code = factura.journal_id.code.split('.')[0]
            search_p = [
                ('code', '=', '{0}.{1}'.format(journal_code, tipus[0]))
            ]
            journal_id = journal_obj.search(cursor, uid, search_p)
            if not journal_id:
                raise osv.except_osv(
                    'Error', _(
                        u"No s'ha trobat el diari per  rectificadores. "
                        u"Tipus %s.%s." % (factura.journal_id.code, tipus)
                    )
                )
            journal_id = journal_id[0]

            if self.is_continuous_invoice(cursor, uid, factura.id,
                                          context=context):
                # Continuous invoice
                method_name = 'fact_via_lectures'
                lot_id = False
                ulf = self.get_ultima_lectura_facturada(
                    cursor, uid, factura, context
                )

                ctx = context.copy()
                ctx.update({
                    'factura_manual': True,
                    'sync': False,
                    'data_inici_factura': factura.data_inici,
                    'data_final_factura': factura.data_final,
                    'fins_lectura_fact': False,
                    'ult_lectura_fact': ulf,
                    'date_boe': factura.date_boe,
                    'tipo_rectificadora': tipus,
                    'ref': factura.id,
                    'journal_id': journal_id,
                })

                res_ids += getattr(facturador_obj, method_name)(
                    cursor, uid, polissa_id, lot_id, ctx)

            else:
                res_ids += super(GiscedataFacturacioContinuous,
                                 self).create_rectification_invoice(cursor, uid,
                                                                   [factura.id],
                                                                    tipus,
                                                                    context)
        return res_ids

    def is_continuous_invoice(self, cursor, uid, factura_id, context=None):
        '''
            Check if invoice is continuous. If the invoice is rectificative
            search the original invoice
        '''
        read_fields = ['continuous_lot_id',
                       'rectificative_type',
                       'rectifying_id']
        invoice_vals = self.read(cursor, uid, factura_id, read_fields)
        if invoice_vals['rectificative_type'] != 'N':
            # Recursive call
            search_params = [('invoice_id', '=',
                              invoice_vals['rectifying_id'][0])]
            invoice_ids = self.search(cursor, uid, search_params)
            if invoice_ids:
                return self.is_continuous_invoice(cursor, uid, invoice_ids[0],
                                                  context=context)
        else:
            return invoice_vals['continuous_lot_id']

        return False

    _columns = {
        'continuous_lot_id': fields.many2one('continuous.invoicing.lot',
                                             'Continuous Lot',
                                             required=True, readonly=True),
    }


GiscedataFacturacioContinuous()