SELECT
  f.id
FROM giscedata_facturacio_factura f
INNER JOIN account_invoice i
  ON i.id = f.invoice_id
INNER JOIN res_partner_address address
  ON address.id = i.address_invoice_id
WHERE
  continuous_lot_id = %s
  AND per_enviar like '%%email%%'
  AND coalesce(address.email, '') = ''