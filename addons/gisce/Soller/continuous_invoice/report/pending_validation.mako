<%

from tools import config
from datetime import datetime
addons_path = config['addons_path']
cursor = objects[0]._cr
uid = user.id
pool = objects[0].pool

%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
         <link rel="stylesheet" type="text/css" href="${addons_path}/soller_operador_report/report/static/css/print.css"/>
</head>
<body>
%for lot in objects:
    <p>${'LOTE: {}'.format(lot.name)}</p>
    % for contract_lot in lot.contract_lot_ids:
        % if contract_lot.invalid:
            <div>
                <br/><p>${ 'Póliza: {}'.format(contract_lot.policy_id.name)}</p>
                % for warning_id in contract_lot.warning_ids:
                    <p>${'[{}] {}'.format(warning_id.code, warning_id.description) }</p>
                % endfor
            </div>
        %endif
    %endfor
%endfor
</body>
</html>