# -*- encoding: utf-8 -*-
from __future__ import absolute_import

from osv import osv
from osv import fields
from tools.translate import _
from datetime import datetime, timedelta
import netsvc
from oorq.decorators import job
from giscedata_remeses.wizard.afegir_factures_remesa import VALID_MODELS
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES
from giscedata_facturacio.giscedata_facturacio import (
    REFUND_RECTIFICATIVE_INVOICE_TYPES,
    RECTIFYING_RECTIFICATIVE_INVOICE_TYPES
)

VALID_MODELS.append('continuous.invoicing.lot')

class ContinuousFacturacioLot(osv.osv):

    _name = 'continuous.invoicing.lot'

    _states_selection = [
        ('esborrany', 'Esborrany'),
        ('obert', 'Obert'),
        ('tancat', 'Tancat'),
    ]

    def fill_lot(self, cursor, uid, lot_id, context=None):
        '''
        Select contracts for invoicing on date_end from lot
        '''
        invoicer_obj = self.pool.get('continuous.invoicing.invoicer')
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')
        contract_obj = self.pool.get('giscedata.polissa')
        config_obj = self.pool.get('res.config')
        warning_obj = self.pool.get('continuous.invoicing.warning')

        if isinstance(lot_id, (list, tuple)):
            lot_id = lot_id[0]

        lot = self.browse(cursor, uid, lot_id)

        contract_ids = invoicer_obj.get_contracts(cursor, uid, lot.date_end)

        contract_warn_dict, contract_ids = invoicer_obj.validate_contracts(
            cursor, uid, lot.date_end, contract_ids)

        # Search for already added contracts
        clot_ids = self.get_clots(cursor, uid, lot_id, context=context)
        clot_vals = clot_obj.read(cursor, uid, clot_ids, ['policy_id'])
        in_contract_ids = [x['policy_id'][0] for x in clot_vals]
        add_contract_ids = list(set(contract_ids) - set(in_contract_ids))
        if not context:
            context = {}

        clot_ids = []
        max_days_w = int(config_obj.get(cursor, uid, 'max_days_warning'))
        min_days = int(config_obj.get(cursor, uid, 'min_days_invoice'))
        for contract_id in add_contract_ids:
            values = {'lot_id': lot_id,
                      'policy_id': contract_id}

            clot_id = clot_obj.create(cursor, uid, values, context)
            clot_ids.append(clot_id)

            if contract_id in contract_warn_dict:
                num_months_invoicing = 1
                contract = contract_obj.read(cursor, uid, contract_id,
                                             ['facturacio'])
                if 'facturacio' in contract:
                    num_months_invoicing = int(contract['facturacio'])
                days_invoice = (min_days * num_months_invoicing) + max_days_w
                msg = _('Contract are more than {} days without invoicing: {}'
                        ).format(days_invoice, contract_warn_dict[contract_id])

                warning_obj.create_warning(cursor, uid, 'CV0001', msg, clot_id)

        return True

    def validate_invoices(self, cursor, uid, lot_id, context=None):
        """
            Validate action in background
        """
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')

        if isinstance(lot_id, (list, tuple)):
            lot_id = lot_id[0]

        clot_ids = self.get_clots(cursor, uid, lot_id, type='invoiced',
                                  context=context)

        for clot_id in clot_ids:
            clot_obj.validate_invoice_clot(cursor, uid, clot_id,
                                           context=context)

        return True

    def invoice_lot(self, cursor, uid, lot_id, context=None):
        '''
            Invoice lot action
        '''
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')
        logger = netsvc.Logger()

        if isinstance(lot_id, (list, tuple)):
            lot_id = lot_id[0]

        if context is None:
            context = {}

        lot = self.browse(cursor, uid, lot_id, context)

        # All policies for invoicing
        invoice_ids = self.get_clots(cursor, uid, lot_id, context=context)
        not_invoiced_ids = self.get_clots(cursor, uid, lot_id,
                                              type='not_invoiced',
                                              context=context)
        if not not_invoiced_ids:
            logger.notifyChannel("objects", netsvc.LOG_INFO,
                                 u"El lot {} està tot facturat.".format(
                                     lot.name))
            return True
        logger.notifyChannel("objects", netsvc.LOG_INFO,
                             u"Facturant el lot {} ({} de {} pòlisses)".format(
                                 lot.name,
                                 len(not_invoiced_ids),
                                 len(invoice_ids)))

        for clot_id in not_invoiced_ids:
            clot_obj.invoice_contract_lot(cursor, uid, clot_id, context=context)

        return True

    def open_invoices(self, cursor, uid, lot_id, context=None):
        '''
            Open invoices action
        '''
        invoice_obj = self.pool.get('giscedata.facturacio.factura')

        if isinstance(lot_id, (list, tuple)):
            lot_id = lot_id[0]

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'sync': False})

        invoice_ids = self.get_invoices_from_lot(cursor, uid, lot_id,
                                                 context=ctx)

        search_params = [('id', 'in', invoice_ids),
                         ('invoice_id.type', '=', 'out_invoice'),
                         ('state', '=', 'draft'),
                        ] + ctx.get('extra_filter', [])
        invoice_ids = invoice_obj.search(cursor, uid, search_params)

        for invoice_id in invoice_ids:
            self.open_invoice(cursor, uid, invoice_id, context=ctx)

        return True

    @job(queue='facturacio_continua')
    def open_invoice(self, cursor, uid, invoice_id, context=None):
        '''
            Open invoice in background
        '''
        invoice_obj = self.pool.get('giscedata.facturacio.factura')

        return invoice_obj.invoice_open(cursor, uid, [invoice_id],
                                        context=context)

    def get_clots(self, cursor, uid, lot_id, type='all', context=None):
        '''
            Get list of contract lots filtering by type
        '''
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')

        search_params = [('lot_id', '=', lot_id)]
        if type == 'invoiced':
            search_params.extend([('invoice_ids', '<>', False)])
        elif type == 'not_invoiced':
            search_params.extend([('invoice_ids', '=', False)])
        elif type == 'valid':
            search_params.extend([('invalid', '=', False)])
        elif type == 'invalid':
            search_params.extend([('invalid', '=', True)])
        elif type == 'for_invoicing':
            search_params.extend([('invoice_ids', '=', False),
                                  ('invalid', '=', False)])

        return clot_obj.search(cursor, uid, search_params)

    def afegir_factures_remesa(self, cursor, uid, ids, order_id, context=None):
        """
            Add invoices from lot to remittance.
        """
        order_obj = self.pool.get('payment.order')
        fac_obj = self.pool.get('giscedata.facturacio.factura')

        if not context:
            context = {}
        if isinstance(ids, list):
            ids = ids[0]

        order = order_obj.browse(cursor, uid, order_id)
        limit = context.get('invoices_limit', None)
        not_ordered = context.get('not_ordered', False)

        invoice_ids = self.get_invoices_from_lot(cursor, uid, ids,
                                                 context=context)

        search_params = [('id', 'in', invoice_ids),
                         ('type', '=', 'out_invoice'),
                         ('payment_type', '=', order.mode.type.id)]

        if 'payment_mode_id' in fac_obj.fields_get_keys(cursor, uid):
            search_params.append(('payment_mode_id', '=', order.mode.id))
        if not_ordered:
            search_params.append(('payment_order_id', '=', False))

        fids = fac_obj.search(
            cursor, uid, search_params, limit=limit, order='number, id'
        )
        fac_obj.afegeix_a_remesa(cursor, uid, fids, order_id, context)

    def get_invoices_from_lot(self, cursor, uid, lot_id, context=None):
        '''
            Get all open and paid invoices from continuous lot
        '''
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')

        clot_ids = self.get_clots(cursor, uid, lot_id, type='invoiced',
                                  context=context)
        clot_vals = clot_obj.read(cursor, uid, clot_ids, ['invoice_ids'])
        invoice_ids = []
        for clot_val in clot_vals:
            invoice_ids.extend(clot_val['invoice_ids'])

        return invoice_ids

    def _get_lot_from_clot(self, cursor, uid, ids, context=None):
        """Returns lot associated to clots in ids
        """

        clot_obj = self.pool.get('continuous.invoicing.contract_lot')

        lot_values = clot_obj.read(cursor, uid, ids, ['lot_id'])
        return list(set([x['lot_id'][0] for x in lot_values]))

    def _ff_n_clots(self, cursor, uid, ids, field_name, arg, context=None):

        res = {}
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')
        for lot_id in ids:
            search_params = [('lot_id', '=', lot_id)]
            res[lot_id] = clot_obj.search_count(cursor, uid, search_params)
        return res

    def _ff_n_invoices(self, cursor, uid, ids, field_name, arg, context=None):

        res = {}
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')
        for lot_id in ids:
            search_params = [('lot_id', '=', lot_id),
                             ('invoice_ids', '<>', False)]
            res[lot_id] = clot_obj.search_count(cursor, uid, search_params)
        return res

    _store_nclots = {'continuous.invoicing.lot':
                     (lambda self, cr, uid, ids, c=None: ids, [], 10),
                     'continuous.invoicing.contract_lot':
                     (_get_lot_from_clot, ['lot_id'], 10)}


    _columns = {
        'name': fields.char('Name', size=24, required=True),
        'date_end': fields.date('End date', required=True),
        'payment_term_id': fields.many2one('account.payment.term',
                                           'Payment term',
                                           required=True),
        'n_clots': fields.function(_ff_n_clots, method=True,
                                   string='Contracts number',
                                   type='integer',
                                   store=_store_nclots),
        'n_invoices': fields.function(_ff_n_invoices, method=True,
                                      string='Generated invoices',
                                      type='integer',
                                      store=False),
    }

ContinuousFacturacioLot()


class ContinuousFacturacioContractLot(osv.osv):
    _name = 'continuous.invoicing.contract_lot'

    @job(queue='facturacio_continua')
    def validate_invoice_clot(self, cursor, uid, clot_id, context=None):
        lot_obj = self.pool.get('continuous.invoicing.lot')
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        warn_obj = self.pool.get(
            'giscedata.facturacio.validation.warning.template')
        valid_obj = self.pool.get('giscedata.facturacio.validation.validator')
        continous_warn_obj = self.pool.get('continuous.invoicing.warning')

        if isinstance(clot_id, (list, tuple)):
            clot_id = clot_id[0]

        read_fields = ['lot_id', 'polissa_id', 'invoice_ids']
        clot_vals = self.read(cursor, uid, clot_id, read_fields)

        # If there are not invoices we not validate
        if not clot_vals['invoice_ids']:
            return False

        warn_list = []
        for invoice_id in clot_vals['invoice_ids']:
            invoice = invoice_obj.browse(cursor, uid, invoice_id)

            checks = valid_obj.get_checks(
                cursor, uid, validation_type='pre-facturacio', context=context
            )
            for template_vals in checks:
                vals = getattr(valid_obj, template_vals['method'])(
                    cursor, uid, invoice, template_vals['parameters']
                )
                if vals is not None:
                    warn = warn_obj.get_warning(
                            cursor, uid, template_vals['code'], context=context
                        )
                    if 'description' in warn:
                        warn_list.append((template_vals['code'],
                                          warn['description'].format(**vals))
                                         )
        if warn_list:
            for warn in warn_list:
                continous_warn_obj.create_warning(cursor, uid, warn[0],
                                                  warn[1], clot_id)
        else:
            continous_warn_obj.delete_warnings_from_clot(cursor, uid, clot_id)
        return True

    @job(queue='facturacio_continua')
    def invoice_contract_lot(self, cursor, uid, clot_id, context=None):
        invoicer_obj = self.pool.get('continuous.invoicing.invoicer')
        polissa_obj = self.pool.get('giscedata.polissa')
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        lot_obj = self.pool.get('continuous.invoicing.lot')
        warning_obj = self.pool.get('continuous.invoicing.warning')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        if isinstance(clot_id, (list, tuple)):
            clot_id = clot_id[0]
        if not context:
            context = {}
        read_fields = ['lot_id', 'policy_id', 'invoice_ids']
        clot_vals = self.read(cursor, uid, clot_id, read_fields)
        policy_id = clot_vals['policy_id'][0]

        # If lot is close we not invoice
        lot_vals = lot_obj.read(cursor, uid, clot_vals['lot_id'][0],
                                ['date_end'])

        if not clot_vals['invoice_ids']:
            # If there aren't readings, we don't invoice
            if not self.are_readings(cursor, uid, policy_id):
                msg = _('Contract not invoiced because there are not '
                        'readings.')
                warning_obj.create_warning(cursor, uid, 'CV0002', msg, clot_id)
                return False

            # If the last invoice is near, we don't invoice
            days_last_invoice = self.get_days_last_invoice(cursor, uid,
                                                           policy_id,
                                                           context=context)
            if not self.check_last_invoice(cursor, uid, policy_id,
                                           days_last_invoice, context=context):
                msg = _('Contract not invoiced because the last invoice '
                        'is invoiced {} days before.').format(days_last_invoice)
                warning_obj.create_warning(cursor, uid, 'CV0003', msg,
                                           clot_id)
                return False

            ctx = context.copy()
            ctx.update({
                'factura_manual': True,
                'fins_lectura_fact': lot_vals['date_end']
            })
            start_date, end_date = polissa_obj.get_inici_final_a_facturar(
                cursor, uid, policy_id, use_lot=False,
                context=ctx
            )
            ctx.update({
                'sync': False,
                'data_inici_factura': start_date,
                'data_final_factura': end_date,
                'fins_lectura_fact': False,
            })

            invoice_ids = invoicer_obj.invoice_by_reads(cursor, uid, policy_id,
                                                        context=ctx)
            self.write(cursor, uid, clot_id,
                       {'invoice_ids': [(6, 0, invoice_ids)]})

            # Write continuous lot and re-write date_invoice, due_date
            date_invoice = datetime.now()
            vals = {
                'continuous_lot_id': clot_vals['lot_id'][0],
                'date_invoice': date_invoice
            }
            # Calculate due date
            payment_term = factura_obj.read(
                cursor, uid, invoice_ids, ['partner_id', 'payment_term']
            )[0]['payment_term']
            if payment_term:
                payment_term_id = payment_term[0]
                date_due_res = factura_obj.onchange_payment_term_date_invoice(
                    cursor, uid, [], payment_term_id,
                    date_invoice.strftime('%Y-%m-%d')
                )
                date_due = date_due_res.get('value', {})

                if date_due:
                    vals.update({
                        'date_due': date_due['date_due']
                    })

            invoice_obj.write(cursor, uid, invoice_ids, vals)
        return True

    def check_last_invoice(self, cursor, uid, policy_id, days_last_invoice,
                           context=None):
        '''
            Check if last invoice is invoiced in minimum of days before.
        '''
        policy_obj = self.pool.get('giscedata.polissa')
        config_obj = self.pool.get('res.config')

        if not days_last_invoice:
            return True

        min_days = int(config_obj.get(cursor, uid, 'min_days_invoice'))

        contract = policy_obj.read(cursor, uid, policy_id, ['facturacio'])
        num_months_invoicing = 1
        if 'facturacio' in contract:
            num_months_invoicing = int(contract['facturacio'])

        min_days_last_invoice = (min_days * num_months_invoicing) - 10

        return days_last_invoice > min_days_last_invoice

    def get_days_last_invoice(self, cursor, uid, policy_id, context=None):
        '''
            Get number of days since last invoice
        '''
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        journal_obj = self.pool.get('account.journal')

        # Search all refund invoices in policy
        search_params = [
            ('type', '=', 'out_refund'),
            ('polissa_id', '=', policy_id),
            ('state', 'in', ('open', 'paid')),
            ('rectificative_type', 'in', REFUND_RECTIFICATIVE_INVOICE_TYPES)
        ]
        invoice_ids = invoice_obj.search(cursor, uid, search_params,
                                         context=context)
        refs_refund = [x['ref'][0]
                for x in invoice_obj.read(cursor, uid, invoice_ids, ['ref'])]

        # Search all rectifying invoices in policy
        search_params = [
            ('type', '=', 'out_invoice'),
            ('polissa_id', '=', policy_id),
            ('state', 'in', ('open', 'paid')),
            ('rectificative_type', 'in', RECTIFYING_RECTIFICATIVE_INVOICE_TYPES)
        ]
        invoice_ids = invoice_obj.search(cursor, uid, search_params,
                                         context=context)
        refs_rectify = [x['ref'][0]
                for x in invoice_obj.read(cursor, uid, invoice_ids, ['ref'])]

        # Remove refund invoices without rectifying. If there are a
        # rectifiying invoice get the original invoice.
        refs = list(set(refs_refund) - set(refs_rectify))

        search_params = [('code', '=', 'ENERGIA')]
        journal_ids = journal_obj.search(cursor, uid, search_params)

        search_params = [('type', '=', 'out_invoice'),
                         ('polissa_id', '=', policy_id),
                         ('journal_id', 'in', journal_ids),
                         ('tipo_factura', '=', '01'),
                         ('state', 'in', ('open', 'paid')),
                         ('rectificative_type', '=', 'N')]
        if refs:
            search_params += [('id', 'not in', refs)]

        invoice_ids = invoice_obj.search(cursor, uid, search_params, limit=1,
                                         order='data_inici desc')
        if not invoice_ids:
            return False

        date_invoice = invoice_obj.read(cursor, uid, invoice_ids[0],
                                        ['date_invoice'])['date_invoice']
        date_invoice = datetime.strptime(date_invoice, '%Y-%m-%d')

        return (datetime.today() - date_invoice).days

    def are_readings(self, cursor, uid, policy_id, context=None):
        '''
            check if there are readings from last invoice
        '''
        polissa_obj = self.pool.get('giscedata.polissa')
        last_read_date = polissa_obj.data_utlima_lectura_entrada(
            cursor, uid, policy_id, context=context)

        read_fields = ['data_ultima_lectura', 'data_alta']
        polissa_vals = polissa_obj.read(cursor, uid, policy_id, read_fields)
        start_date = polissa_vals['data_ultima_lectura']
        if not start_date:
            start_date = polissa_vals['data_alta']

        return last_read_date and start_date < last_read_date

    def _get_clot_from_invoice(self, cursor, uid, ids, context=None):
        """
            Returns clot associated to invoice in ids
        """
        contract_lot_obj = self.pool.get('continuous.invoicing.contract_lot')
        search_params = [('invoice_ids', 'in', ids)]
        return contract_lot_obj.search(cursor, uid, search_params)

    def _get_clot_from_warning(self, cursor, uid, ids, context=None):
        """
            Returns clot associated to invoice in ids
        """
        contract_lot_obj = self.pool.get('continuous.invoicing.contract_lot')
        search_params = [('warning_ids', 'in', ids)]
        return contract_lot_obj.search(cursor, uid, search_params)


    def _ff_num_invoices(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        for clot_id in ids:
            read_fiels = ['invoice_ids']
            clot_vals = self.read(cursor, uid, clot_id, read_fiels)
            res[clot_id] = len(clot_vals['invoice_ids'])
        return res

    def _ff_valid(self, cursor, uid, ids, field_name, arg, context=None):
        warning_obj = self.pool.get('continuous.invoicing.warning')
        res = {}
        for clot_id in ids:
            search_fields = [('contract_lot_id', '=', clot_id)]
            warn_ids = warning_obj.search(cursor, uid, search_fields)

            if warn_ids:
                res[clot_id] = True
            else:
                res[clot_id] = False
        return res

    _store_invalid = {'continuous.invoicing.contract_lot':
                      (lambda self, cr, uid, ids, c=None: ids, [], 10),
                      'continuous.invoicing.warning':
                       (_get_clot_from_warning, ['code'], 10)}

    _store_numinvoices = {'continuous.invoicing.contract_lot':
                     (lambda self, cr, uid, ids, c=None: ids, [], 10),
                     'giscedata.facturacio.factura':
                     (_get_clot_from_invoice, ['state'], 10)}

    _columns = {
        'policy_id': fields.many2one('giscedata.polissa', 'Policy',
                                       required=True, readonly=True),
        'lot_id': fields.many2one('continuous.invoicing.lot', 'Lot',
                                  required=True),
        'invoice_ids': fields.many2many('giscedata.facturacio.factura',
                                        'invoices_contract_lot_rel',
                                        'clot_id', 'invoice_id',
                                        'Invoices', readonly=True),
        'num_invoices': fields.function(_ff_num_invoices, method=True,
                                        string='Number of invoices',
                                        type='integer',
                                        store=_store_numinvoices),
        'invalid': fields.function(_ff_valid, method=True,
                                   string='Error',
                                   type='boolean',
                                   store=_store_invalid),
        'pending_validation': fields.boolean('Pending validations',
                                             readonly=True)
    }


ContinuousFacturacioContractLot()

class ContinuousFacturacioLot2(osv.osv):

    _inherit = 'continuous.invoicing.lot'

    _columns = {
        'contract_lot_ids': fields.one2many('continuous.invoicing.contract_lot',
                                            'lot_id', 'Contracts')
    }

ContinuousFacturacioLot2()


class ContinuousInvoicingInvoicer(osv.osv):
    _name = "continuous.invoicing.invoicer"
    _description = "Contracts invoicing"
    _auto = False

    def get_contracts(self, cursor, uid, date, context=None):
        '''select contracts for invoicing on date'''
        contract_obj = self.pool.get('giscedata.polissa')

        if not context:
            context = {}

        ctx = context.copy()
        ctx.update({'active_test': False})

        cancel_date = datetime.strptime(date, '%Y-%m-%d') - timedelta(days=180)  # 6 month window for cancellation contracts
        # Search for active contracts on date
        search_params = [('lot_facturacio', '=', None),
                         ('state', 'not in', CONTRACT_IGNORED_STATES),
                         ('data_alta', '<=', date),
                         '|',
                         ('data_baixa', '=', False),
                         ('data_baixa', '>=', cancel_date.strftime('%Y-%m-%d')),
                         ]

        return contract_obj.search(cursor, uid, search_params, context=ctx)


    def validate_contracts(self, cursor, uid, date, contract_ids):
        '''
            Validate contracts
        '''
        config_obj = self.pool.get('res.config')
        contract_obj = self.pool.get('giscedata.polissa')

        read_fields = ['data_ultima_lectura', 'data_ultima_lectura_estimada',
                       'data_alta', 'data_baixa', 'facturacio']

        contract_list = []
        contract_warn_dict = {}
        for contract_id in contract_ids:
            contract = contract_obj.read(cursor, uid, contract_id, read_fields)

            last_invoice_date = max(contract['data_ultima_lectura'],
                                    contract['data_ultima_lectura_estimada'],
                                    contract['data_alta'])

            if last_invoice_date == contract['data_baixa']:
                #Already invoiced
                continue

            num_months_invoicing = 1 # For bimensual invoices
            if 'facturacio' in contract:
                num_months_invoicing = int(contract['facturacio'])

            # Minimum days for invoicing
            last_invoice_date = datetime.strptime(last_invoice_date,
                                                  '%Y-%m-%d')
            end_date = datetime.strptime(date, '%Y-%m-%d')
            min_days_invoice = int(config_obj.get(cursor, uid,
                                                  'min_days_invoice'))
            min_days_invoice *= num_months_invoicing
            max_days_warning = int(config_obj.get(cursor, uid,
                                                  'max_days_warning'))
            last_invoice_days = (end_date - last_invoice_date).days
            if last_invoice_days < min_days_invoice:
                continue
            elif last_invoice_days > (min_days_invoice + max_days_warning):
                contract_warn_dict.update({contract_id: last_invoice_days})

            contract_list.append(contract_id)

        return contract_warn_dict, contract_list



    def invoice_by_reads(self, cursor, uid, policy_id, context=None):

        facturador = self.pool.get('giscedata.facturacio.facturador')

        return facturador.fact_via_lectures(cursor, uid, policy_id, False,
                                            context=context)

ContinuousInvoicingInvoicer()

class ContinuousInvoicingWarning(osv.osv):

    _name = 'continuous.invoicing.warning'

    def create_warning(self, cursor, uid, code, description, contract_lot_id,
                       context=None):
        '''
            create warning if not exist
        '''
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')
        search_params = [('code', '=', code),
                         ('contract_lot_id', '=', contract_lot_id)]

        warning_ids = self.search(cursor, uid, search_params)
        if not warning_ids:
            vals = {
                'code': code,
                'description': description,
                'contract_lot_id': contract_lot_id,
            }
            self.create(cursor, uid, vals)

        # Indicate error in clot
        clot_obj.write(cursor, uid, contract_lot_id,
                       {'pending_validation': True})

        return True

    def delete_warnings_from_clot(self, cursor, uid, clot_id, context=None):
        '''
            Delete all warnings from clot
        '''
        clot_obj = self.pool.get('continuous.invoicing.contract_lot')
        search_params = [('contract_lot_id', '=', clot_id)]
        warn_ids = self.search(cursor, uid, search_params)

        self.unlink(cursor, uid, warn_ids)

        # Delete error in clot
        clot_obj.write(cursor, uid, clot_id, {'pending_validation': False})

    _columns = {
        'code': fields.char('Code', size=10, required=True, readonly=True),
        'description': fields.text('Description', readonly=True),
        'contract_lot_id': fields.many2one('continuous.invoicing.contract_lot',
                                           'Contract lot', required=True,
                                           ondelete='cascade')
    }

ContinuousInvoicingWarning()

class ContinuousFacturacioContractLot2(osv.osv):
    _inherit = 'continuous.invoicing.contract_lot'

    _columns = {
        'warning_ids':  fields.one2many('continuous.invoicing.warning',
                                            'contract_lot_id', 'Warnings')
    }

ContinuousFacturacioContractLot2()
