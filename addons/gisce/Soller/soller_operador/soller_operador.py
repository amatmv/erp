# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any latevalidate_multiservice_changer version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
import netsvc

from osv import osv
from osv import fields
from datetime import datetime, timedelta
from tools.translate import _
from tools.misc import cache
from tools import config, float_round
from oorq.decorators import job, create_jobs_group
from oorq.oorq import setup_redis_connection
from rq import queue, get_failed_queue
from ast import literal_eval
from itertools import groupby
from autoworker import AutoWorker

import re
import calendar


CHANGE_CONTRACT_TYPE = [
    ('in', 'Entrant'),
    ('out', 'Sortint'),
]

class SollerOperadorContratoCondicions(osv.osv):

    _name = 'soller.operador.contrato.condicions'

    _columns = {
        'name': fields.char('Nom', size=64, required=True),
        'text': fields.text('Descripció', required=True)
    }

SollerOperadorContratoCondicions()


_soller_contrato_operador_states_selection = [
    ('esborrany', 'Esborrany'),
    ('validar', 'Validar'),
    ('modificar', 'Modificar'),
    ('actiu', 'Actiu'),
    ('cancelat', 'Cancel·lat'),
    ('impagat', 'Impagat'),
    ('tallat', 'Tallat'),
    ('baixa', 'Baixa')
]


class soller_operador_contrato(osv.osv):
    '''
    Modelo de contrato de operador
    '''
    _name = 'soller.operador.contrato'
    _description = 'Contrato de operador'

    def create(self, cursor, uid, vals, context=None):
        """Asignamos un numero de contrato automatico"""
        if not vals.get('name', False):
            vals['name'] = self.pool.get('ir.sequence').get(cursor,
                                        uid, 'soller.operador.contrato')

        return super(soller_operador_contrato,
                     self).create(cursor, uid, vals, context)

    def copy(self, cursor, uid, id, default=None, context=None):

        if default is None:
            default = {}

        default.update({
            'name': False,
            'active': True
        })

        res_id = super(soller_operador_contrato,
                       self).copy(cursor, uid, id,
                                  default=default,
                                  context=context)
        # Remove no active lines, equips and routers
        copy = self.browse(cursor, uid, res_id)
        all_fields = self.fields_get(cursor, uid)
        fields = ['lineas_ids', 'equip_ids', 'router_ids']
        for field in fields:
            to_remove = [x.id for x in getattr(copy, field)
                         if not x.active]
            if not to_remove:
                continue
            model = all_fields[field]['relation']
            self.pool.get(model).unlink(cursor, 1, to_remove)

        return res_id

    def on_change_titular_id(self, cursor, uid, ids, titular_id,
                             pagador_id, context=None):
        """Asignamos el mismo pagador que el
        titular en el caso de estar vacio"""
        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not titular_id:
            return res
        if not pagador_id:
            res['value'].update({'pagador_id': titular_id})
            pagador_id = titular_id
        res['domain'].update({'direccion_titular_id': [('partner_id', '=',
                                                        titular_id)],
                              'direccion_pagador_id': [('partner_id', '=',
                                                        pagador_id)],
                              'bank_id': [('partner_id', '=', pagador_id)]})
        return res

    def on_change_pagador_id(self, cursor, uid, ids, pagador_id, context=None):
        """Actualizamos el domain de direccion de pago y banco"""
        res = {'value': {}, 'domain': {}, 'warning': {}}

        if pagador_id:
            res['domain'].update({'direccion_pagador_id': [('partner_id', '=',
                                                            pagador_id)],
                                  'bank_id': [('partner_id', '=',
                                               pagador_id)]})
        return res

    def on_change_cups_id(self, cursor, uid, ids, cups_id, context=None):
        """Check if cups is already used"""

        if not context:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if cups_id:
            # Search for active contracts using this cups
            search_params = [('cups_id', '=', cups_id)]
            contract_ids = self.search(cursor, uid, search_params,
                                       context=context)
            contract_vals = self.read(cursor, uid, contract_ids, ['name'])
            contract_names = [x['name'] for x in contract_vals]
            if contract_ids:
                str = _(u"Aquest cups ja està en ús: {}")
                mes = str.format(','.join(contract_names))
                res['warning'].update({'title': 'Avís', 'message': mes})

        return res

    def onchange_condicions(self, cursor, uid, ids,
                            condicio_ids, text, context=None):

        res = {'value': {}, 'domain': {}, 'warning': {}}
        condicio_obj = self.pool.get('soller.operador.contrato.condicions')
        condicio_ids = condicio_ids and condicio_ids[0][2] or []

        text = text or ''
        for condicio in condicio_obj.browse(cursor, uid, condicio_ids):
            if text:
                text += '\n\n%s' % condicio.text
            else:
                text = condicio.text
        res['value'].update({'text_condicions': text})

        return res

    #Cambios de estado
    def b_soc_esborrany(self, cursor, uid, ids, context=None):

        self.write(cursor, uid, ids, {'state': 'esborrany', 'active': True})

    def b_soc_validar(self, cursor, uid, ids, context=None):

        facturador_obj = self.pool.get('soller.operador.facturador')

        #Para validar un contrato comprobamos
        for contrato in self.browse(cursor, uid, ids):
            #que tenga titular y contacto de titular
            if not contrato.titular_id or not contrato.direccion_titular_id:
                raise osv.except_osv('Error',
                                     _(u"Falta el titular o "
                                       u"l'adreça del titular!"))
                return False
            #que tenga pagador y contacto de pagador
            if not contrato.pagador_id or not contrato.direccion_factura_id:
                raise osv.except_osv('Error',
                                     _(u"Falta el pagador o "
                                       u"l'adreça del pagador!"))

            if contrato.tipus_instalacio == 'fibra':
                if (not contrato.tarifa_id
                        or not contrato.multitarifa_id
                        or not contrato.property_call_price):
                    raise osv.except_osv('Error', _(u"Falta la tarifa, "
                                                    u"la forma de pagament o"
                                                    u"la tarifa de criades!"))
            else:
                if not contrato.tarifa_id:
                    raise osv.except_osv('Error', _(u'¡Falta la tarifa!'))

            if not (contrato.pagador_id.vat and contrato.titular_id.vat):
                raise osv.except_osv('Error',
                                     _(u"Falta el NIF del titular o del "
                                       u"pagador!"))

            if (contrato.tipo_pago_id.code == 'RECIBO_CSB'):
                if not contrato.bank_id:
                    raise osv.except_osv('Error',
                                         _(u"Falta el compte bancari!"))

                mandato_exists = facturador_obj.search_mandate(cursor, uid,
                                                               contrato,
                                                               context=context)
                # Comprobamos que tengamos al menos un mandato si hay IBAN
                if not mandato_exists:
                    raise osv.except_osv('Error',
                                         _(u'No hi ha mandats associats a '
                                           u'aquest contracte!'))

            # comprobamos los productos, al menos uno
            if not contrato.lineas_ids:
                raise osv.except_osv('Error',
                                     _(u"Fa falta un producte per validar!"))

            if not contrato.data_firma_contracte:
                raise osv.except_osv('Error',
                                     _(u"Falta la data de "
                                       u"firma de contracte!"))

        self.write(cursor, uid, ids, {'state': 'validar'})

    def b_soc_activar(self, cursor, uid, ids, context=None):
        """La condicion para activar es que tenga fecha de firma"""
        for contrato in self.browse(cursor, uid, ids):
            # Comprobamos que hay al menos un producto activo y facturable
            active_and_facturable_products = [l.active and l.facturar
                                              for l in contrato.lineas_ids]
            if not any(active_and_facturable_products):
                raise osv.except_osv('Error',
                                     _(u'Deu haver-hi almenys un producte '
                                       u'actiu i facturable!'))

            contrato.write({'state': 'actiu'})
        return True

    def b_soc_modificar(self, cursor, uid, ids, context=None):

        self.write(cursor, uid, ids, {'state': 'modificar'})

    def b_soc_baixa(self, cursor, uid, ids, context=None):
        """La condicion para dar de baja es que tenga fecha de baja"""

        linia_obj = self.pool.get("soller.operador.contrato.linea")

        for contrato in self.browse(cursor, uid, ids):
            if contrato.data_baixa:
                baixa_vals = {'data_baixa': contrato.data_baixa,
                              'active': False}
                #Damos de baja todas las lineas activas con la misma fecha
                for linia in contrato.lineas_ids:
                    if linia.active:
                        linia.write(baixa_vals)
                # Baixa to all equips
                for equip in contrato.equip_ids:
                    if equip.active:
                        equip.write(baixa_vals)
                # Baixa to all routers
                for router in contrato.router_ids:
                    if router.active:
                        router.write(baixa_vals)
                contrato.write({'state': 'baixa', 'active': False})
            else:
                raise osv.except_osv('Error',
                                     _(u"Falta la data de baixa!"))
        return True

    def b_soc_cancelar(self, cursor, uid, ids, context=None):

        self.write(cursor, uid, ids, {'state': 'cancelat', 'active': False})

    def b_soc_impagat(self, cursor, uid, ids, context=None):

        self.write(cursor, uid, ids, {'state': 'impagat'})

    def b_soc_tallat(self, cursor, uid, ids, context=None):

        self.write(cursor, uid, ids, {'state': 'tallat'})

    def b_soc_reactivar(self, cursor, uid, ids, context=None):
        '''For each contract, reactivate lines, equips and routers
        with same data_baixa as the contract'''

        for contrato in self.browse(cursor, uid, ids):
            react_vals = {'data_baixa': False,
                          'active': True}
            # Reactivate all lines with same data_baixa as contract
            for linia in contrato.lineas_ids:
                if (not linia.active
                    and linia.data_baixa == contrato.data_baixa):
                    linia.write(react_vals)
            # Baixa to all equips
            for equip in contrato.equip_ids:
                if (not equip.active
                    and equip.data_baixa == contrato.data_baixa):
                    equip.write(react_vals)
            # Baixa to all routers
            for router in contrato.router_ids:
                if (not router.active
                    and router.data_baixa == contrato.data_baixa):
                    router.write(react_vals)
            react_vals.update({'state': 'actiu'})
            contrato.write(react_vals)

        return True

    def multiservicio(self, cursor, uid, ids, context=None):
        '''check if this contract has an associated polissa'''

        polissa_obj = self.pool.get('giscedata.polissa')

        if not context:
            context = {}
        now = datetime.now().strftime('%Y-%m-%d')
        date = context.get('date_multiservicio', now)
        date_month = date[:7]
        ctx = context.copy()
        ctx.update({'active_test': False})
        res = {}
        for id in ids:
            contract = self.browse(cursor, uid, id)
            multiservicio = False
            if (not contract.cups_id
                or not contract.cups_id.active
                or contract.tipus_instalacio != 'fibra'):
                res[id] = multiservicio
                continue
            #Search for polissa associated to cups
            search_params = [('cups', '=', contract.cups_id.id),
                             ('state', 'not in', ('esborrany', 'validar'))]
            polissa_ids = polissa_obj.search(cursor, uid, search_params,
                                             context=ctx)
            for polissa_id in polissa_ids:
                polissa = polissa_obj.browse(cursor, uid, polissa_id)
                if polissa.active:
                    multiservicio = True
                    break
                #If here, we have a baixa polissa
                #If the baixa has been in the month we have for computing
                #or after, the polissa has been active during the period
                #so we apply multiservicio
                data_mes_baixa = polissa.data_baixa[:7]
                if data_mes_baixa >= date_month:
                    multiservicio = True
                    break
            res[id] = multiservicio
        return res

    def update_mandate(self, cursor, uid, contrato_id, context=None):

        if not context:
            context = {}

        if isinstance(contrato_id, (list, tuple)):
            contrato_id = contrato_id[0]

        contrato = self.browse(cursor, uid, contrato_id)
        debtor_address = (contrato.direccion_factura_id.
                          name_get(context=context)[0][1]).upper()
        debtor_contact = (contrato.direccion_factura_id.name
                          and '%s, ' % contrato.direccion_factura_id.name
                          or '').upper()
        if debtor_contact:
            debtor_address = debtor_address.replace(debtor_contact, '')
        debtor_state = (contrato.direccion_factura_id.state_id
                        and contrato.direccion_factura_id.state_id.name
                        or '').upper()
        debtor_country = (contrato.direccion_factura_id.country_id
                          and contrato.direccion_factura_id.country_id.name
                          or '').upper()
        install_address = (contrato.direccion_titular_id.
                           name_get(context=context)[0][1]).upper()
        install_contact = (contrato.direccion_titular_id.name
                           and '%s, ' % contrato.direccion_titular_id.name
                           or '').upper()
        if install_contact:
            install_address = install_address.replace(install_contact, '')
        notes = u"Contrato: %s\n" % contrato.name
        notes += u"Dirección: %s" % install_address

        vals = {
            'debtor_name': contrato.pagador_id.name,
            'debtor_vat': contrato.pagador_id.vat,
            'debtor_address': debtor_address,
            'debtor_state': debtor_state,
            'debtor_country': debtor_country,
            'debtor_iban': contrato.bank_id.iban,
            'reference': '%s,%s' % ('soller.operador.contrato', contrato_id),
            'notes': notes,
        }

        return vals

    @cache()
    def get_contract_from_phone_date(self, cursor, uid, phone, date,
                                     context=None):
        linea_obj = self.pool.get('soller.operador.contrato.linea')

        if not context:
            context = {}

        if 'contract_states' in context:
            contract_states = context['contract_states']
        else:
            contract_states = ('esborrany', 'validar', 'cancelat')

        search_params = [('detall', '=', phone),
                         ('data_alta', '<=', date),
                         '|', ('data_baixa', '>=', date),
                              ('data_baixa', '=', None),
                         ]
        ctx = context.copy()
        ctx.update({'active_test': False})
        linea_ids = linea_obj.search(cursor, uid, search_params, context=ctx)
        linea_ids = [linea.id for linea in linea_obj.browse(cursor, uid,
                                                            linea_ids)
                     if (linea.product_id.categ_id.code in ('TLF', 'MOV') and
                         linea.contrato_id.state not in contract_states)]
        if not linea_ids:
            msg = _(u"No s'ha trobat el contracte per el "
                    u"telèfon %s") % (phone)
            raise osv.except_osv('Error', msg)
        if len(linea_ids) > 1:
            msg = _(u"S'han trobat %s línies de contracte per el "
                    u"telèfon %s") % (len(linea_ids), phone)
            raise osv.except_osv('Error', msg)

        if context.get('return_line_id', False):
            return linea_ids[0]
        else:
            return linea_obj.read(cursor, uid, linea_ids[0],
                                  ['contrato_id'])['contrato_id'][0]

    def _get_contracte_from_invoice(self, cursor, uid, ids, context=None):
        """retornem els ids dels contractes associats als
        ids que ens passen de invoice"""
        if not context:
            context = {}

        invoice_obj = self.pool.get('account.invoice')
        contrato_obj = self.pool.get('soller.operador.contrato')

        query = '''
            SELECT distinct contrato.id
            FROM account_invoice i
            INNER JOIN account_journal journal
            ON journal.id = i.journal_id
            INNER JOIN soller_operador_contrato contrato
            ON contrato.name = i.origin
            WHERE journal.code = 'OPERADOR'
            AND i.id in %s'''

        cursor.execute(query, (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _get_invoice_details(self, cursor, uid, ids, name, arg,
                                context=None):

        res = {}
        invoice_obj = self.pool.get('account.invoice')
        context.update({'active_test': False})
        #Buscamos todas las facturas de operador que no sean borrador
        #o proforma de este contrato para asignar la fecha mayor
        query = '''
            SELECT max(date_invoice)
            FROM account_invoice i
            INNER JOIN account_journal journal
            ON journal.id = i.journal_id
            WHERE journal.code = 'OPERADOR'
            and i.state in ('open', 'paid')
            and origin = %s'''

        for contract in self.browse(cursor, uid, ids, context):
            res[contract.id] = {'data_darrera_facturacio': False,
                                'pending': 0}
            # Search for last 5 invoices searching last invoice date
            search_params = [('origin', '=', contract.name),
                             ('journal_id.code', '=', 'OPERADOR'),
                             ('state', 'in', ('open', 'paid'))]
            invoice_ids = invoice_obj.search(cursor, uid, search_params,
                                             order='date_invoice desc',
                                             context=context)
            pending = 0
            date_list = []

            if not invoice_ids:
                continue
            counter = 0
            for invoice in invoice_obj.browse(cursor, uid, invoice_ids):
                multiplier = 1
                if invoice.type == 'out_refund':
                    multiplier = -1
                pending += invoice.residual * multiplier
                counter += 1
                # Only check dates for 5 last invoices
                if counter > 5:
                    continue
                invoice_dates = [literal_eval(x.origin)['date_to']
                                 for x in invoice.invoice_line
                                 if x.origin]
                # Old invoices, use date_invoice instead
                if not invoice_dates:
                    invoice_dates = [invoice.date_invoice]
                date_list.extend(invoice_dates)

            res[contract.id].update({'data_darrera_facturacio': max(date_list),
                                     'pending': pending})
        return res

    _states_attrs = {
        'validar': [('required', True), ('readonly', False)],
        'esborrany': [('readonly', False)],
        'modificar': [('required', True), ('readonly', False)],
    }

    _states_attrs_noreq = {
        'validar': [('readonly', False)],
        'esborrany': [('readonly', False)],
        'modificar': [('readonly', False)],
    }

    _store_df = {
        'account.invoice':
        (_get_contracte_from_invoice, ['state', 'residual'], 20),
        'soller.operador.contrato':
        (lambda self, cr, uid, ids, c=None: ids, ['state'], 20),
    }

    _columns = {
        'name': fields.char('Contracte', size=64, readonly=True,
                            states={'esborrany': [('readonly', False)],
                                    'validar': [('required', True)],
                                            }),
        'cups_id': fields.many2one('giscedata.cups.ps', 'CUPS',
                                   readonly=True, size=22,
                                   states=_states_attrs_noreq),
        'titular_id': fields.many2one('res.partner', 'Titular', readonly=True,
                                     states=_states_attrs,
                                     context={'active_test': True}),
        'nif_titular': fields.related('titular_id', 'vat', type='char',
                                      relation='res.partner',
                                      string='NIF Titular',
                                      store=False, readonly=True),
        'direccion_titular_id': fields.many2one('res.partner.address',
                                               'Direcció titular',
                                               readonly=True,
                                               states=_states_attrs,
                                               context={'active_test': True}),
        'pagador_id': fields.many2one('res.partner', 'Pagador', readonly=True,
                                     states=_states_attrs,
                                     context={'active_test': True}),
        'nif_pagador': fields.related('pagador_id', 'vat', type='char',
                                      relation='res.partner',
                                      string='NIF Pagador',
                                      store=False, readonly=True),
        'direccion_factura_id': fields.many2one('res.partner.address',
                                               'Direcció factura',
                                               readonly=True,
                                                states=_states_attrs,
                                                context={'active_test': True}),
        'tipo_pago_id': fields.many2one('payment.type', 'Tipus de pagament',
                                       readonly=True,
                                       states=_states_attrs),
        'bank_id': fields.many2one('res.partner.bank', 'CCC', readonly=True,
                                  states=_states_attrs),
        'tarifa_id': fields.many2one('product.pricelist', 'Tarifa',
                                    readonly=True,
                                    states=_states_attrs),
        'multitarifa_id': fields.many2one('product.pricelist',
                                    'Tarifa MultiServicio',
                                    readonly=True,
                                    states=_states_attrs_noreq),
        'property_call_price': fields.property('product.pricelist',
                                              type='many2one',
                                              relation='product.pricelist',
                                              string=u"Tarifa cridades",
                                              method=True,
                                              view_load=True,
                                              readonly=True,
                                              states=_states_attrs),
        'data_firma_contracte': fields.date('Data firma contracte'),
        'data_baixa': fields.date('Data baixa'),
        'data_darrera_facturacio': fields.function(_get_invoice_details,
                         method=True, type='date',
                         string='Data darrera facturació',
                         readonly=True, multi='invoicing',
                         store=_store_df),
        'pending': fields.function(_get_invoice_details,
                         method=True, type='float', digits=(16, 2),
                         string='Pendent',
                         readonly=True, multi='invoicing',
                         store=_store_df),
        'active': fields.boolean('Actiu', readonly=True),
        'state': fields.selection(_soller_contrato_operador_states_selection,
                                  'Estat', required=True, readonly=True),
        'tipus_instalacio': fields.selection([('plc', 'PLC'),
                                             ('radio', 'Radio'),
                                             ('fibra', 'Fibra')],
                                             'Tipus Instal·lació', select=True,
                                             readonly=True,
                                             states=_states_attrs),
        'observacions': fields.text('Observacions'),
        'sla': fields.integer('Nivell de servei', readonly=True,
                              states=_states_attrs,
                              help=u"Hores màximes per "
                                   u"solventar una incidència"),
        'condicions_especials': fields.boolean('Condicions particulars',
                    help=(u"Marcar aquesta opció per habilitar la "
                          u"possibilitat d'incloure condicions "
                          u"especials al contracte"),
                    readonly=True,
                    states=_states_attrs_noreq),
        'condicio_ids': fields.many2many('soller.operador.contrato.condicions',
                            'condicions_operador_rel',
                            'contrato_id', 'condicio_id',
                            string="Condicions",
                            readonly=True,
                            states=_states_attrs_noreq),
        'text_condicions': fields.text('Text condicions',
                                readonly=True,
                                states=_states_attrs_noreq),
    }

    _defaults = {
        'active': lambda *a: 1,
        'state': lambda *a: 'esborrany',
        'tipus_instalacio': lambda *a: 'radio',
        'sla': lambda *a: 72,
        'condicions_especials': lambda *a: False,
    }

    def _cnt_multiservicio(self, cursor, uid, ids, context=None):

        multiservicio = self.multiservicio(cursor, uid, ids)
        for id in ids:
            contract = self.browse(cursor, uid, id)
            if (contract.cups_id
                and multiservicio[id]
                and not contract.multitarifa_id):
                return False
        return True

    _constraints = [(_cnt_multiservicio,
                    'Error: Contracte en multiservei sense tarifa multiservei',
                    ['multitarifa_id'])]

soller_operador_contrato()


class soller_operador_contrato_linea(osv.osv):
    '''
    Productos que factura el operador
    '''
    _name = 'soller.operador.contrato.linea'
    _description = 'Líneas de contrato'
    _order = 'contrato_id, active, data_alta desc'
    _rec_name = 'product_id'

    _modalidad_selection = [('prepago', 'Prepago'),
                            ('postpago', 'Contrato')]

    def create(self, cursor, uid, vals, context=None):
        if vals.get('detall', False):
           #  Delete all white spaces character (\t \n \r \f \v)
           vals['detall'] = re.sub(r"\s+", "", vals['detall'])

        return super(soller_operador_contrato_linea,
                     self).create(cursor, uid, vals, context)


    def write(self, cursor, uid, ids, vals, context=None):
        if vals.get('detall', False):
           #  Delete all white spaces character (\t \n \r \f \v)
           vals['detall'] = re.sub(r"\s+", "", vals['detall'])

        return super(soller_operador_contrato_linea,
                     self).write(cursor, uid, ids, vals, context)

    def get_inici_factura(self, cursor, uid, linia_id, context=None):

        if not context:
            context = {}
        context.update({'active_test': False})
        custom_dates = context.get('custom_dates', False)

        if isinstance(linia_id, (list, tuple)):
            linia_id = linia_id[0]

        linia = self.browse(cursor, uid, linia_id, context=context)

        if custom_dates:
            date_df = context.get('date_from')
        elif linia.contrato_id.data_darrera_facturacio:
            df = linia.contrato_id.data_darrera_facturacio
            date_df = (datetime.strptime(df, '%Y-%m-%d')
                       + timedelta(days=1)).strftime('%Y-%m-%d')
        else:
            date_df = linia.data_alta

        return max(linia.data_alta, date_df)

    def get_final_factura(self, cursor, uid, linia_id, data_factura,
                          context=None):

        if not context:
            context = {}
        context.update({'active_test': False})
        custom_dates = context.get('custom_dates', False)

        if isinstance(linia_id, (list, tuple)):
            linia_id = linia_id[0]

        linia = self.browse(cursor, uid, linia_id, context=context)

        if custom_dates:
            data_final = context.get('date_to')
        else:
            data_final = data_factura

        if linia.data_baixa:
            data_final = min(linia.data_baixa, data_factura)

        return data_final

    def b_create_phone_case(self, cursor, uid, linia_id, context=None):
        """
        Create a phone case from contract line.
        """

        crm_obj = self.pool.get('crm.case')

        if isinstance(linia_id, (list, tuple)):
            linia_id = linia_id[0]

        linia = self.browse(cursor, uid, linia_id, context=context)

        # If the case not referenced to a phone product return false
        if linia.product_id.categ_id.code not in ('TLF', 'MOV'):
            raise osv.except_osv('Error al crear l\'ordre',
                'Només es poden crear ordres automàtiques pels telèfons')

        if not linia.detall:
            description = "NOU TELÈFON"
        else:
            description = "PORTABILITAT " + linia.detall

        vals = {
            'ref':  'soller.operador.contrato,{}'.format(linia.contrato_id.id),
            'ref2': 'soller.operador.contrato.linea,{}'.format(linia.id),
            'user_id': uid
        }

        case_id = crm_obj.create_case_generic(cursor, uid,
                                              [linia.contrato_id],
                                              context=context,
                                              description=description,
                                              section='OP',
                                              category='PORTF',
                                              extra_vals=vals)

        crm_obj.case_open(cursor, uid, case_id)
        return case_id

    def get_portability_orders(self, cursor, uid, line_id, context=None):
        crm_obj = self.pool.get('crm.case')
        search_params = [
            ('ref2', '=', 'soller.operador.contrato.linea,{}'.format(line_id)),
            ('categ_id.categ_code', '=', 'PORTF'),
        ]
        crm_ids = crm_obj.search(cursor, uid, search_params, context=context)

        return crm_ids

    def send_activation_mail(self, cursor, uid, order_id, context=None):
        """Send a successfull email to the customer"""

        if not context:
            context = {}

        config_obj = self.pool.get('res.config')
        tmpl_obj = self.pool.get('poweremail.templates')
        email_wizard_obj = self.pool.get('poweremail.send.wizard')

        template_id = int(
            config_obj.get(cursor, uid, 'operador_mobile_activation_template',
                           '0'))

        if template_id == 0:
            raise osv.except_osv('Error', _(u"No s'ha definit la "
                                            u"plantilla per l'enviament"))

        tmpl = tmpl_obj.browse(cursor, uid, template_id)

        ctx = context.copy()
        ctx.update({
            'state': 'single',
            'priority': '0',
            'from': tmpl.enforce_from_account.id,
            'template_id': template_id,
            'src_model': 'crm.case',
            'src_rec_ids': [order_id],
            'active_ids': [order_id],
            'active_id': order_id,
        })

        wiz_vals = {
            'state': ctx['state'],
            'priority': ctx['priority'],
            'from': ctx['from'],
        }

        pe_wizard = email_wizard_obj.create(cursor, uid, wiz_vals, context=ctx)
        email_wizard_obj.send_mail(cursor, uid, [pe_wizard], context=ctx)

    def name_get(self, cr, uid, ids, context=None):
        """
        Redefine the name_get method.
        """
        if not len(ids):
            return []
        res=[]
        for item in self.browse(cr,uid,ids):
            if item.detall:
                res.append((item.id, "{} {} ({}) - {}".format(
                    item.contrato_id.name, item.product_id.name, item.detall,
                    item.id)))
            else:
                res.append((item.id, "{} {} - {}".format(
                    item.contrato_id.name, item.product_id.name, item.id)))
        return res

    def _get_price(self, cursor, uid, ids, name, arg, context=None):

        pricelist_obj = self.pool.get('product.pricelist')
        tax_obj = self.pool.get('account.tax')
        res = {}
        for linea in self.browse(cursor, uid, ids, context=context):
            linea_res = {'product_price': 0,
                         'multiproduct_price': 0}
            taxes = linea.product_id.taxes_id
            if linea.contrato_id.tarifa_id:
                try:
                    tarifa = linea.contrato_id.tarifa_id
                    price = pricelist_obj.price_get(cursor, uid,
                        [tarifa.id],
                        linea.product_id.id,
                        linea.quantitat,
                        context={'date': linea.data_alta})[tarifa.id]
                    tax_amount = tax_obj.compute(cursor, uid, taxes, price, 1)
                    price_with_tax = price + tax_amount[0]['amount']
                    linea_res['product_price'] = float_round(price_with_tax, 2)
                except Exception, e:
                    pass
            if linea.contrato_id.multitarifa_id:
                try:
                    multitarifa = linea.contrato_id.multitarifa_id
                    price = pricelist_obj.price_get(cursor, uid,
                        [multitarifa.id],
                        linea.product_id.id,
                        linea.quantitat,
                        context={'date': linea.data_alta})[multitarifa.id]
                    tax_amount = tax_obj.compute(cursor, uid, taxes, price, 1)
                    price_with_tax = price + tax_amount[0]['amount']
                    linea_res['multiproduct_price'] = float_round(price_with_tax, 2)
                except Exception, e:
                    pass
            res[linea.id] = linea_res
        return res

    def _get_linea_from_contrato(self, cursor, uid, ids, context=None):
        '''returns product lines associated to contratos'''
        if not context:
            context = {}
        linea_obj = self.pool.get('soller.operador.contrato.linea')
        search_params = [('contrato_id', 'in', ids)]
        return linea_obj.search(cursor, uid, search_params)

    _product_price_store = {'soller.operador.contrato.linea':
                           (lambda self, cr, uid, ids, ctx=None: ids,
                            ['product_id'], 20),
                            'soller.operador.contrato':
                            (_get_linea_from_contrato,
                             ['tarifa_id', 'multitarifa_id',
                              'data_firma_contracte'], 20)}

    _columns = {
        'product_id': fields.many2one('product.product', 'Producte',
                                     required=False),
        'product_price': fields.function(_get_price, type='float',
                                         method=True, digits=(16, 2),
                                         store=_product_price_store,
                                         multi='prices'),
        'multiproduct_price': fields.function(_get_price, type='float',
                                         method=True, digits=(16, 2),
                                         store=_product_price_store,
                                         multi='prices'),
        'quantitat': fields.integer('Quantitat', required=True),
        'contrato_id': fields.many2one('soller.operador.contrato',
                                      'Contracte Operador',
                                      required=True,
                                      readonly=True,
                                      ondelete='cascade'),
        'facturar': fields.boolean('Facturar'),
        'contrato_state': fields.related('contrato_id', 'state',
                         type="selection",
                         selection=_soller_contrato_operador_states_selection,
                         string="Estat Contracte",
                         store=False,
                         readonly=True),
        'data_alta': fields.date('Data alta'),
        'data_baixa': fields.date('Data baixa'),
        'detall': fields.char('Detalls', size=128, required=False),
        'linea_partner_id': fields.many2one('res.partner', 'Operador',
                                            required=False),
        'active': fields.boolean('Actiu', required=False),
        'sim': fields.char('Sim operador', size=32),
        'sim_donant': fields.char('Sim operador donant', size=32),
        'operador_donant_id': fields.many2one('res.partner', 'Operador donant',
                                              required=False),
        'modalidad_donant': fields.selection(_modalidad_selection,
                                  'Modalidad donant', select=True,
                                  readonly=False, required=False),
        'client_donant_id': fields.many2one('res.partner', 'Client donant',
                                            required=False),
        'apoderat_id': fields.many2one('res.partner', 'Apoderat',
                                       required=False),
    }

    _defaults = {
        'quantitat': lambda *a: 1,
        'active': lambda *a: True,
        'facturar': lambda *a: False,
    }

    def _cnt_end_date(self, cursor, uid, ids, context=None):
        search_params = [('id', 'in', ids),
                         ('active', '=', False),
                         ('facturar', '=', True),
                         ('data_baixa', '=', False),
                         ('contrato_id.state', 'not in',
                          ('esborrany', 'validar', 'cancelat'))]

        line_ids = self.search(cursor, uid, search_params)

        if line_ids:
            return False

        return True

    _constraints = [(_cnt_end_date,
                     'Error: La data de baixa ha d\'estar informada a '
                     'línies de contracte no actives',
                     ['data_baixa'])]

soller_operador_contrato_linea()


class soller_operador_repetidor(osv.osv):

    _name = 'soller.operador.repetidor'
    _description = 'Repetidor'
    _order = 'name'

    _columns = {
        'name': fields.char('Nom', size=64, required=True, readonly=False),
        'direccio_ip': fields.char('Direcció IP', size=64, required=True,
                                   readonly=False),
        'active': fields.boolean('Actiu', required=True),
        'mac': fields.char('MAC', size=64, required=False, readonly=False),
        'pa_ids': fields.one2many('soller.operador.repetidor.pa',
                                  'repetidor_id', 'PA')
    }

    _defaults = {
        'active': lambda *a: True,
    }

soller_operador_repetidor()


class OperadorRepetidorPA(osv.osv):

    _name = 'soller.operador.repetidor.pa'
    _order = 'name'

    _columns = {
        'name': fields.char('Nom', required=True, size=20),
        'ssid': fields.char('SSID', required=True, size=40),
        'repetidor_id': fields.many2one('soller.operador.repetidor',
                                        'Repetidor', required=True)
    }

OperadorRepetidorPA()


class soller_operador_equip(osv.osv):

    _name = 'soller.operador.equip'
    _description = 'Equip'
    _order = 'data_alta desc'

    _equip_selection = [('antena', 'Antena'),
                        ('modem', 'Mòdem')]

    _antena_selection = [('petita', 'Petita'),
                         ('mitjana', 'Mitjana'),
                         ('gran', 'Gran')]

    _columns = {
        'name': fields.char('Nom', size=64, required=True, readonly=False),
        'direccio_ip': fields.char('Direcció IP', size=64, required=True,
                                   readonly=False),
        'firmware': fields.char('Firmware', size=64, required=False,
                                readonly=False),
        'tipus_equip': fields.selection(_equip_selection,
                                  'Equip', select=True,
                                  readonly=False,
                                  required=True),
        'tipus_antena': fields.selection(_antena_selection,
                                  'Tipus', select=True, readonly=False,
                                  required=False),
        'active': fields.boolean('Activa', required=True),
        'serial_id': fields.many2one('stock.production.lot', 'Nº Serie',
                                    required=False),
        'contracte_id': fields.many2one('soller.operador.contrato',
                                       'Contracte', required=False),
        'contracte_state': fields.related('contracte_id', 'state',
                         type="selection",
                         selection=_soller_contrato_operador_states_selection,
                         string="Estat Contracte",
                         store=False, readonly=True),
        'data_alta': fields.date('Data alta'),
        'data_baixa': fields.date('Data baixa'),
        'mac': fields.char('MAC', size=64, required=False, readonly=False),
        'repetidor_id': fields.many2one('soller.operador.repetidor',
                                       'Repetidor', required=False),
        'pa_id': fields.many2one('soller.operador.repetidor.pa',
                                 'PA', required=False),
    }

    _defaults = {
        'active': lambda *a: True,
    }

soller_operador_equip()


class soller_operador_repetidor2(osv.osv):

    _name = 'soller.operador.repetidor'
    _inherit = 'soller.operador.repetidor'

    _columns = {
        'equip_ids': fields.one2many('soller.operador.equip', 'repetidor_id',
                                    'Equips', required=False),
    }

soller_operador_repetidor2()


class soller_operador_router(osv.osv):

    _name = 'soller.operador.router'
    _description = 'Router'
    _rec_name = 'serial_id'
    _order = 'data_alta desc'

    _columns = {
        'firmware': fields.char('Firmware', size=64, required=False,
                                readonly=False),
        'tipus': fields.selection([('voip', 'VoIP'),
                                  ('novoip', 'Sense VoIP')],
                                  'Tipus', select=True, readonly=False,
                                  required=False),
        'active': fields.boolean('Actiu', required=True),
        'name': fields.char('Nº Serie', size=64, required=False),
        'serial_id': fields.many2one('stock.production.lot', 'Magatzem',
                                    required=False),
        'product_id': fields.related('serial_id', 'product_id',
                                     type='many2one',
                                     readonly=True, relation='product.product',
                                     string='Producte', store=False),
        'contracte_id': fields.many2one('soller.operador.contrato',
                                       'Contracte', required=False),
        'contracte_state': fields.related('contracte_id', 'state',
                         type="selection",
                         selection=_soller_contrato_operador_states_selection,
                         string="Estat Contracte",
                         store=False, readonly=True),
        'data_alta': fields.date('Data alta'),
        'data_baixa': fields.date('Data baixa'),
        'mac': fields.char('MAC', size=64, required=False, readonly=False),
        'usuari': fields.char('Usuari', size=64, readonly=False,),
        'password': fields.char('Contrasenya', size=64, readonly=False,),
        'caixa': fields.char('Caixa', size=32, readonly=False,),
        'port': fields.char('Port', size=32, readonly=False,),
    }

    _defaults = {
        'active': lambda *a: True,
    }

soller_operador_router()


class soller_operador_contrato2(osv.osv):
    '''
    Modelo de contrato de operador
    '''
    _name = 'soller.operador.contrato'
    _inherit = 'soller.operador.contrato'

    def _get_router(self, cursor, uid, ids, name, arg, context=None):
        '''retorna el name del router actiu'''

        router_obj = self.pool.get('soller.operador.router')

        res = {}
        for contrato in self.browse(cursor, uid, ids):
            search_params = [('contracte_id', '=', contrato.id)]
            router_ids = router_obj.search(cursor, uid, search_params)
            if not len(router_ids):
                res[contrato.id] = ''
                continue
            if name == 'router':
                res[contrato.id] = router_obj.read(cursor, uid, router_ids,
                                                   ['name'])[0]['name']
            elif name == 'usuari':
                res[contrato.id] = router_obj.read(cursor, uid, router_ids,
                                                   ['usuari'])[0]['usuari']
            elif name == 'caixa':
                res[contrato.id] = router_obj.read(cursor, uid, router_ids,
                                                   ['caixa'])[0]['caixa']
        return res

    def _get_router_search(self, cursor, uid, obj, name, args, context):
        '''cerquem el name del router que ens pasen
        en args dins els contractes'''

        router_obj = self.pool.get('soller.operador.router')

        if not len(args):
            return []
        if name == 'router':
            #recerca per nom del router
            search_params = [('name', args[0][1], args[0][2])]
        elif name == 'usuari':
            #recerca per nom d'usuari
            search_params = [('usuari', args[0][1], args[0][2])]
        elif name == 'caixa':
            #recerca per caixa
            search_params = [('caixa', args[0][1], args[0][2])]
        router_ids = router_obj.search(cursor, uid, search_params)
        contratos_ids = [router.contracte_id.id for
                         router in router_obj.browse(cursor, uid, router_ids)]
        return [('id', 'in', contratos_ids)]

    def _get_equip(self, cursor, uid, ids, name, arg, context=None):
        '''retorna el name de l'equip actiu'''

        equip_obj = self.pool.get('soller.operador.equip')

        res = {}
        for contrato in self.browse(cursor, uid, ids):
            search_params = [('contracte_id', '=', contrato.id)]
            equip_ids = equip_obj.search(cursor, uid, search_params)
            if not len(equip_ids):
                res[contrato.id] = ''
                continue
            res[contrato.id] = equip_obj.read(cursor, uid, equip_ids,
                                              ['name'])[0]['name']
        return res

    def _get_equip_search(self, cursor, uid, obj, name, args, context=None):
        '''cerquem l'equip que ens pasen en args dins els contractes'''

        equip_obj = self.pool.get('soller.operador.equip')

        if not len(args):
            return []
        search_params = [('name', args[0][1], args[0][2])]
        equip_ids = equip_obj.search(cursor, uid, search_params)
        contratos_ids = [equip.contracte_id.id for equip in
                         equip_obj.browse(cursor, uid, equip_ids)]
        return [('id', 'in', contratos_ids)]

    def _get_contracte_from_linia(self, cursor, uid, ids, context=None):
        '''retorna els contractes associats a
        les linies que ens pasen en ids'''

        linea_obj = self.pool.get('soller.operador.contrato.linea')

        contratos_ids = linea_obj.read(cursor, uid, ids, ['contrato_id'])

        return list(set([x['contrato_id'][0] for x in contratos_ids]))

    def _get_data_alta(self, cursor, uid, ids, name, arg, context=None):
        '''funcio que retorna la data_alta mes petita
        de totes les linies de contracte actives, per tal de
        tindre una data de alta generica de contracte'''

        res = {}

        for contracte in self.browse(cursor, uid, ids, context):
            if not contracte.lineas_ids:
                # no tiene lineas de contrato
                res[contracte.id] = False
                continue

            dates = [line.data_alta
                     for line in contracte.lineas_ids
                     if line.facturar]

            if len(dates) == 0:
                dates = [line.data_alta
                         for line in contracte.lineas_ids]

            res[contracte.id] = min(dates)

        return res

    def _get_repetidor(self, cursor, uid, ids, name, arg, context=None):
        '''retorna el repetidor de l'equip actiu'''

        res = {}
        for contrato in self.browse(cursor, uid, ids):
            res[contrato.id] = ''
            for equip in contrato.equip_ids:
                if equip.active:
                    res[contrato.id] = equip.repetidor_id.name
        return res

    def _get_repetidor_search(self, cursor, uid, obj, name, args, context):
        '''cerquem el repetidor que ens pasen en args dins els contractes'''

        repetidor_obj = self.pool.get('soller.operador.repetidor')
        equip_obj = self.pool.get('soller.operador.equip')

        if not len(args):
            return []
        search_params = [('name', args[0][1], args[0][2])]
        repetidor_ids = repetidor_obj.search(cursor, uid, search_params)
        search_params = [('repetidor_id', 'in', repetidor_ids)]
        equip_ids = equip_obj.search(cursor, uid, search_params)
        contratos_ids = []
        for equip in equip_obj.browse(cursor, uid, equip_ids):
            contratos_ids.append(equip.contracte_id.id)
        return [('id', 'in', contratos_ids)]

    def _get_ct(self, cursor, uid, ids, name, arg, context=None):
        if not context:
            context = {}

        result = {}

        contracts = self.browse(cursor, uid, ids, context)
        for contract in contracts:
            result[contract.id] = {
                'ct_fiber': contract.cups_id.ct_fiber,
                'cups_address': contract.cups_id.act_direccio
            }

        return result

    def _get_facturable(self, cursor, uid, ids, name, arg, context=None):

        res = dict.fromkeys(ids, True)
        for contract in self.browse(cursor, uid, ids, context=context):
            facturar = False
            for line in contract.lineas_ids:
                facturar = line.facturar or facturar
            res[contract.id] = facturar
        return res

    def _get_contract_from_cups(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        contracts_obj = self.pool.get('soller.operador.contrato')
        search_params = [('cups_id', 'in', ids)]
        return contracts_obj.search(cursor, uid, search_params,
                                    context=context)

    _CUPS_ADDRESS_FIELDS = [
        'act_tv', 'act_nv', 'act_pnp', 'act_es', 'act_pt', 'act_pu',
        'act_cpo', 'act_cpa', 'dp', 'id_municipi', 'id_poblacio',
        'act_aclarador'
    ]

    STORE_CT_FIBER = {
        'giscedata.cups.ps': (
            _get_contract_from_cups, ['ct_fiber'] + _CUPS_ADDRESS_FIELDS, 20
        ),
        'soller.operador.contrato':
        (lambda self, cr, uid, ids, c=None: ids, ['state', 'cups_id'], 20),

    }

    _store_facturar = {
        'soller.operador.contrato':
        (lambda self, cr, uid, ids, c=None: ids, ['state'], 20),
        'soller.operador.contrato.linea':
        (_get_contracte_from_linia, ['facturar'], 20),
    }

    _columns = {
        'lineas_ids': fields.one2many('soller.operador.contrato.linea',
                                      'contrato_id', 'Líneas',
                                      context={'active_test': False},
                                      required=False),
        'equip_ids': fields.one2many('soller.operador.equip', 'contracte_id',
                                     'Equips', required=False,
                                     context={'active_test': False}),
        'router_ids': fields.one2many('soller.operador.router', 'contracte_id',
                                      'Routers', required=False,
                                      context={'active_test': False}),
        'router': fields.function(_get_router, method=True, type='char',
                                  string='Router', store=False,
                                  fnct_search=_get_router_search),
        'usuari': fields.function(_get_router, method=True, type='char',
                                  string='Usuari', store=False,
                                  fnct_search=_get_router_search),
        'caixa': fields.function(_get_router, method=True, type='char',
                                  string='Caixa', store=False,
                                  fnct_search=_get_router_search),
        'equip': fields.function(_get_equip, method=True, type='char',
                                 string='Equip', store=False,
                                 fnct_search=_get_equip_search),
        'data_alta': fields.function(_get_data_alta, method=True,
                                     type='date', string='Data alta',
                                     readonly=True,
                                     store={'soller.operador.contrato.linea':
                                            (_get_contracte_from_linia,
                                             ['data_alta'], 20),
                                            },
                                     help=u"Calculada automàticament segons "
                                          u"les dates d'alta de les línies",
                                     ),
        'repetidor': fields.function(_get_repetidor, method=True, type='char',
                                     string='Repetidor', store=False,
                                     fnct_search=_get_repetidor_search),
        'ct_fiber': fields.function(_get_ct, method=True, type='char',
                                    string='CT Fibra', readonly=True,
                                    store=STORE_CT_FIBER, multi='ct', size=10),
        'cups_address': fields.function(_get_ct, method=True, type='char',
                                        string='Adreça CUPS', readonly=True,
                                        store=STORE_CT_FIBER, multi='ct',
                                        size=256),
        'facturar': fields.function(_get_facturable, method=True,
                                    type='boolean', string='Facturar',
                                    store=_store_facturar),
    }

soller_operador_contrato2()


class soller_operador_extra(osv.osv):

    _name = 'soller.operador.extra'

    def invoice_extra(self, cursor, uid, contracte_id,
                      invoice_id, context=None):
        '''afegeix els conceptes extra a la factura'''
        period_obj = self.pool.get('account.period')
        contracte_obj = self.pool.get('soller.operador.contrato')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')

        #Search for period than corresponds to the invoice
        invoice = invoice_obj.browse(cursor, uid, invoice_id)
        contracte = contracte_obj.browse(cursor, uid, contracte_id)
        period_id = period_obj.find(cursor, uid, dt=invoice.date_invoice)[0]
        search_params = [('contracte_id', '=', contracte_id),
                         ('period_id', '=', period_id)]
        extra_ids = self.search(cursor, uid, search_params)
        for extra_id in extra_ids:
            extra = self.browse(cursor, uid, extra_id)
            lvals = {
                    'invoice_id': invoice_id,
                    'product_id': extra.product_id.id,
                    'quantity': extra.quantity,
                    }
            fposition_id = (contracte.pagador_id.
                            property_account_position.id)
            lvals.update(
                        invoice_line_obj.product_id_change(cursor, uid, [],
                        extra.product_id.id,
                        extra.product_id.uom_id.id,
                        extra.quantity,
                        partner_id=contracte.pagador_id.id,
                        fposition_id=fposition_id)['value'])
            lvals['invoice_line_tax_id'] = [(6, 0,
                                    lvals.get('invoice_line_tax_id', []))]
            lvals['price_unit'] = extra.price_unit
            lvals['name'] = extra.name or extra.product_id.name
            line_id = invoice_line_obj.create(cursor, uid, lvals, context)
            extra.write({'invoice_id': invoice_id})

        return True

    def _get_price(self, cursor, uid, ids, name, args, context=None):

        res = {}
        for extra_id in ids:
            extra = self.browse(cursor, uid, extra_id)
            price_subtotal = round(extra.price_unit * extra.quantity, 2)
            res[extra.id] = price_subtotal or 0
        return res

    def _ff_has_invoice(self, cursor, uid, ids, field_name, arg, context=None):

        if not context:
            context = {}

        res = {}
        extras = self.read(cursor, uid, ids, ['invoice_id'], context=context)
        for extra in extras:
            has_invoice = extra['invoice_id'] != False
            res[extra['id']] = has_invoice
        return res

    _has_info_store = {
        'soller.operador.extra':
        (lambda self, cr, uid, ids, c=None: ids, [], 10)
    }

    _columns = {
        'contracte_id': fields.many2one('soller.operador.contrato',
                                        'Contracte', required=True),
        'name': fields.char('Descripció', size=100,
                            help=u"En cas de deixar-ho en blanc s'utilitzarà "
                                 u"el nom del producte associat"),
        'product_id': fields.many2one('product.product', 'Producte',
                                      required=True),
        'quantity': fields.float('Quantitat', digits=(16, 2), required=True),
        'price_unit': fields.float('Preu', digits=(16, 2), required=True),
        'price_subtotal': fields.function(_get_price, method=True,
                            readonly=True, string='Subtotal',
                            type="float", digits=(16, 2),
                            store={'soller.operador.extra':
                           (lambda self, cr, uid, ids, c=None: ids,
                            ['quantity', 'price_unit'], 10)}),
        'period_id': fields.many2one('account.period', 'Període',
                                     help=(u"Indica el mes en el que "
                                           u"es facturarà aquest concepte"),
                                     required=True),
        'invoice_id': fields.many2one('account.invoice', 'Factura associada',
                                      readonly=True),
        'has_invoice': fields.function(_ff_has_invoice, method=True,
                                       string='Té factura associada',
                                       type='boolean', store=_has_info_store)
    }

soller_operador_extra()


class SollerOperadorFacturador(osv.osv):
    _name = "soller.operador.facturador"
    _description = "Facturar contractes"
    _auto = False

    def get_product_taxes(self, cursor, uid, product_id, context=None):
        '''Retorna un diccionari amb els impostos associats a un producte'''

        product_obj = self.pool.get('product.product')
        if isinstance(product_id, (list, tuple)):
            product_id = product_id[0]
        product = product_obj.browse(cursor, uid, product_id)
        return [tax.id for tax in product.taxes_id]

    def get_quantitat(self, cursor, uid, data_inici, data_final,
                      quantitat, context=None):
        """ Calculamos la cantidad a facturar
        teniendo en cuenta periodos no completos """

        date_data_inici = datetime.strptime(data_inici, '%Y-%m-%d')
        date_data_final = datetime.strptime(data_final, '%Y-%m-%d')

        #calculamos los meses entre las dos fechas
        total = 0
        date_calcul = datetime(date_data_inici.year,
                               date_data_inici.month,
                               1)
        while date_calcul <= date_data_final:
            dies_del_mes = calendar.monthrange(date_calcul.year,
                                               date_calcul.month)[1]
            date_inici_mes = datetime(date_calcul.year,
                                      date_calcul.month, 1)
            date_final_mes = datetime(date_calcul.year,
                                      date_calcul.month, dies_del_mes)
            if date_data_inici >= date_inici_mes:
                date_inici_mes = date_data_inici
            if date_data_final <= date_final_mes:
                date_final_mes = date_data_final

            dies_factura = date_final_mes - date_inici_mes + timedelta(days=1)
            total += float(dies_factura.days) / dies_del_mes
            date_calcul += timedelta(days=dies_del_mes)
        return quantitat * total

    def search_mandate(self, cursor, uid, contrato, context=None):

        mandate_obj = self.pool.get('payment.mandate')

        #Search for mandate
        reference = 'soller.operador.contrato,%s' % contrato.id
        search_params = [('reference', '=', reference),
                         ('debtor_iban', '=', contrato.bank_id.iban)]
        mandate_ids = mandate_obj.search(cursor, uid, search_params, limit=1)
        if mandate_ids:
            return mandate_ids[0]
        return False

    def get_journal(self, cursor, uid, context=None):
        '''return journal associated to operador invoices'''

        journal_obj = self.pool.get('account.journal')

        search_params = [('code', '=', 'OPERADOR')]
        journal_id = journal_obj.search(cursor, uid, search_params)[0]
        return journal_id

    def get_contracts(self, cursor, uid, date, context=None):
        '''select contracts for invoicing on date'''

        if not context:
            context = {}

        ctx = context.copy()
        ctx.update({'active_test': False})

        line_obj = self.pool.get('soller.operador.contrato.linea')
        contract_obj = self.pool.get('soller.operador.contrato')

        contract_ids = []
        # Search for lines active on date
        search_params = [('facturar', '=', True),
                         ('data_alta', '<=', date),
                         '|',
                         ('data_baixa', '=', False),
                         ('data_baixa', '>=', date),
                         ]
        line_ids = line_obj.search(cursor, uid, search_params,
                                   context=ctx)
        if line_ids:
            line_vals = line_obj.read(cursor, uid, line_ids, ['contrato_id'])
            contract_ids = list(set([x['contrato_id'][0] for x in line_vals]))
        # Search for not active lines on date
        search_params = [('facturar', '=', True), ('data_baixa', '<=', date)]
        line_ids = line_obj.search(cursor, uid, search_params,
                                   context=ctx)
        for line_id in line_ids:
            line = line_obj.browse(cursor, uid, line_id)
            if line.contrato_id.id in contract_ids:
                continue
            darrera_factura = line.contrato_id.data_darrera_facturacio
            darrera_factura = (darrera_factura and
                               darrera_factura >= line.data_alta and
                               darrera_factura or
                               line.data_alta)
            if (line.data_baixa > darrera_factura):
                contract_ids.append(line.contrato_id.id)
        # Remove contracts in not desiderable states
        search_params = [('id', 'in', contract_ids),
                         ('facturar', '=', True),
                         ('state', 'not in',
                          ('esborrany', 'validar', 'cancelat'))]
        return contract_obj.search(cursor, uid, search_params, context=ctx)

    def action_refund(self, cursor, uid, invoice_ids, date_invoice,
                      type='A', context=None):

        invoice_obj = self.pool.get('account.invoice')

        if context is None:
            context = {}

        refund_ids = []
        for invoice_id in invoice_ids:
            refund_id = invoice_obj.refund(cursor, uid, [invoice_id],
                                           date=date_invoice,
                                           rectificative_type=type)[0]
            read_fields = ['payment_type', 'partner_bank',
                           'mandate_id', 'origin']
            invoice_vals = invoice_obj.read(cursor, uid, invoice_id,
                                            read_fields)

            refund_vals = {
                'rectifying_id': invoice_id
            }
            for key, val in invoice_vals.iteritems():
                if key == 'id':
                    continue
                if isinstance(val, (list, tuple)):
                    refund_vals.update({key: val[0]})
                else:
                    refund_vals.update({key: val})
            invoice_obj.write(cursor, uid, [refund_id], refund_vals)

            refund_ids.append(refund_id)
        invoice_obj.button_reset_taxes(cursor, uid, refund_ids)
        return refund_ids

    def action_facturar(self, cursor, uid, date_invoice, term_id,
                        context=None):

        contrato_obj = self.pool.get('soller.operador.contrato')
        journal_obj = self.pool.get('account.journal')

        if context is None:
            context = {}
        context.update({'active_test': False})

        if context.get('individual', False):
            contract_ids = context.get('active_ids', False)
        else:
            #Buscamos todos los contratos activos, que
            #tengan marcado el campo facturar
            #y cuya fecha de alta sea menor o igual a la fecha de facturacion
            search_params = [
                            ('state', 'not in',
                                ['esborrany', 'validar', 'cancelat']),
                            ('facturar', '=', True),
                            ('data_alta', '<=', date_invoice)
                            ]
            contract_ids = contrato_obj.search(cursor, uid, search_params,
                                               context=context)

        journal_id = self.get_journal(cursor, uid, context)

        for contract_id in contract_ids:
            self.action_facturar_contrato(cursor, uid, contract_id, journal_id,
                                          date_invoice, term_id, None, None,
                                          context=context)

        self.notify_result(cursor, uid, date_invoice, journal_id,
                           context=context)
        return True

    @job(queue='operador_facturacio')
    def notify_result(self, cursor, uid, date_invoice, journal_id,
                      context=None):

        acc_obj = self.pool.get('poweremail.core_accounts')
        mail_obj = self.pool.get('poweremail.mailbox')
        src_model_obj = self.pool.get('soller.operador.contrato')
        invoice_obj = self.pool.get('account.invoice')
        user_obj = self.pool.get('res.users')
        config_obj = self.pool.get('res.config')

        queue_name = 'operador_facturacio'
        action_name = 'action_facturar_contrato'

        redis_conn = setup_redis_connection()
        q = queue.Queue(queue_name, connection=redis_conn)

        while not q.is_empty():
            time.sleep(1)

        msg = u''
        failed_count = 0
        failed_queue = get_failed_queue(connection=redis_conn)
        for job in failed_queue.jobs:
            if job.origin == queue_name and job.args[4] == action_name:
                obj_name = src_model_obj.name_get(cursor, uid,
                                                  job.args[5])[0][1]
                msg += _(u'\n* Facturació fallida %s: \n') % obj_name
                msg += u'%s\n' % job.exc_info
                failed_count += 1
                job.cancel()
                failed_queue.remove(job)

        date_str = datetime.strptime(date_invoice,
                                     '%Y-%m-%d').strftime('%d/%m/%Y')
        subject = _(u'Facturació Operador %s acabada') % date_str

        search_params = [
                         ('journal_id', '=', journal_id),
                         ('date_invoice', '=', date_invoice),
                         ('state', '=', 'draft'),
                        ]
        invoice_count = invoice_obj.search(cursor, uid, search_params,
                                                 count=True)

        body = _(u'* Factures generades %s \n') % invoice_count
        if failed_count:
            body += _(u'* Contractes no facturats: %s \n %s') % (failed_count,
                                                                 msg)

        email_from = config_obj.get(cursor, uid, 'operador_notify_fact_from',
                                    'oficines@el-gas.es')
        email_to = config_obj.get(cursor, uid, 'operador_notify_fact_to',
                                  'oficines@el-gas.es')
        search_params = [('email_id', '=', email_from)]
        acc_id = acc_obj.search(cursor, uid, search_params)[0]

        user = user_obj.browse(cursor, uid, uid)
        email_to = user.address_id.email
        if not email_to:
            email_to = email_to

        vals = {
            'pem_from': email_from,
            'pem_to': email_to,
            'pem_subject': subject,
            'pem_body_text': body,
            'pem_account_id': acc_id,
        }
        mail_id = mail_obj.create(cursor, uid, vals, context)
        mail_obj.send_this_mail(cursor, uid, [mail_id], context)

        return True

    @job(queue='operador_facturacio')
    def action_facturar_contrato(self, cursor, uid, contract_id, journal_id,
                                 date_end, term_id, date_invoice,
                                 clot_id, context=None):
        """ Metodo que factura un contrato """

        contrato_obj = self.pool.get('soller.operador.contrato')
        lcontrato_obj = self.pool.get('soller.operador.contrato.linea')
        extra_obj = self.pool.get('soller.operador.extra')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        pricelist_obj = self.pool.get('product.pricelist')
        cdr_obj = self.pool.get('soller.operador.cdr')
        clot_obj = self.pool.get('operador.facturacio.contracte_lot')
        data_obj = self.pool.get('soller.operador.cdr.data')

        notes = ''
        if not context:
            context = {}

        custom_dates = context.get('custom_dates', False)
        if custom_dates:
            custom_date_from = context.get('date_from')
            custom_date_to = context.get('date_to')
            date_end = custom_date_to
            date_invoice = datetime.now().strftime('%Y-%m-%d')

        ctx = context.copy()
        ctx.update({'date_multiservicio': date_end})
        multiservicio = contrato_obj.multiservicio(cursor, uid,
                                                   [contract_id],
                                                   context=ctx)[contract_id]

        contrato = contrato_obj.browse(cursor, uid, contract_id,
                                       context=context)
        #No contratos de baja que ya hayamos facturado
        #en ocasiones anteriores
        if contrato.state == 'baixa':
            if custom_dates:
                dfacturacio = custom_date_from
            else:
                dfacturacio = contrato.data_darrera_facturacio or False
            if (dfacturacio and contrato.data_baixa <= dfacturacio):
                #contrato de baja, que ya se facturo por ultima vez
                return True
        if custom_dates:
            data_darrera_facturacio = custom_date_from
        else:
            data_darrera_facturacio = contrato.data_darrera_facturacio
        if data_darrera_facturacio >= date_end:
            #contratos de los que ya hemos facturado
            #hasta la fecha de factura
            return True

        mandate_id = self.search_mandate(cursor, uid, contrato, context)
        vals = {
            'state': 'draft',
            'date_invoice': (date_invoice is not None
                             and date_invoice or date_end),
            'type': 'out_invoice',
            'journal_id': journal_id,
            'partner_id': contrato.pagador_id.id,
            'address_invoice_id': contrato.direccion_factura_id.id,
            'address_contact_id': contrato.direccion_titular_id.id,
            'origin': contrato.name,
            'name': contrato.name,
            'account_id': (contrato.titular_id.
                           property_account_receivable.id),
            'partner_bank': contrato.bank_id.id,
            'mandate_id': mandate_id,
            'payment_type': contrato.tipo_pago_id.id,
            'payment_term': term_id,
        }

        invoice_id = invoice_obj.create(cursor, uid, vals, context)

        for linea in contrato.lineas_ids:
            if not linea.facturar:
                continue
            data_inici = lcontrato_obj.get_inici_factura(cursor,
                                                         uid,
                                                         linea.id,
                                                         context=context)
            data_final = lcontrato_obj.get_final_factura(cursor,
                                                         uid,
                                                         linea.id,
                                                         date_end,
                                                         context=context)
            origin_vals = {'date_from': data_inici,
                           'date_to': data_final}
            if data_inici > data_final:
                continue

            lvals = {
                'invoice_id': invoice_id,
                'product_id': linea.product_id.id,
                'quantity': self.get_quantitat(cursor, uid,
                                               data_inici, data_final,
                                               linea.quantitat),
                'origin': origin_vals
            }
            fposition_id = (contrato.pagador_id.
                            property_account_position.id)
            product_vals = invoice_line_obj.product_id_change(cursor, uid, [],
                                            linea.product_id.id,
                                            linea.product_id.uom_id.id,
                                            linea.quantitat,
                                            partner_id=contrato.pagador_id.id,
                                            fposition_id=fposition_id)['value']
            lvals.update(product_vals)
            if lvals.get('invoice_line_tax_id', []):
                lvals['invoice_line_tax_id'] = [(6, 0,
                                                lvals['invoice_line_tax_id'])]
            lvals['price_unit'] = (pricelist_obj.price_get(cursor, uid,
                                   [contrato.tarifa_id.id],
                                   linea.product_id.id,
                                   linea.quantitat,
                                   context={'date': date_end})
                                   [contrato.tarifa_id.id])
            dinici = datetime.strptime(data_inici,
                                       '%Y-%m-%d').strftime('%d-%m-%Y')
            dfinal = datetime.strptime(data_final,
                                       '%Y-%m-%d').strftime('%d-%m-%Y')
            if linea.product_id.categ_id.code in ('TLF', 'MOV'):
                lvals['name'] = ('%s - %s ' %
                                (linea.product_id.name, linea.detall))
                lvals['origin'].update({'phone': linea.detall})
            else:
                lvals['name'] = linea.product_id.name
            period_str = ' (%s a %s)' % (dinici, dfinal)
            lvals['name'] += period_str

            linea_creada_id = invoice_line_obj.create(cursor, uid,
                                                      lvals,
                                                      context)
            sla = contrato_obj.calcular_sla(cursor, uid, contrato.id,
                                            data_inici, data_final)
            if sla > contrato.sla:
                sla_vals = {'price_unit': (lvals['price_unit']
                                           * (-1)),
                            'name': (u"Abono de la cuota por "
                                     u"interrupciones en el servicio")}
                invoice_line_obj.copy(cursor, uid, linea_creada_id,
                                      sla_vals)
            elif (sla <= contrato.sla
                  and multiservicio):
                # If sla does not arrive contract sla hours
                # and we have multiservicio, we have to add a line
                # with the corresponding discount
                multi_price = (pricelist_obj.price_get(cursor, uid,
                               [contrato.multitarifa_id.id],
                               linea.product_id.id,
                               linea.quantitat,
                               context={'date': date_end})
                               [contrato.multitarifa_id.id])
                multidiscount_price = multi_price - lvals['price_unit']
                if multidiscount_price != 0:
                    multi_vals = {'price_unit': multidiscount_price,
                                  'name': (u"Descuento acceso internet")}
                    invoice_line_obj.copy(cursor, uid, linea_creada_id,
                                          multi_vals)

            if linea.product_id.categ_id.code in ('TLF', 'MOV'):
                call_line_vals = cdr_obj.compute_calls(cursor, uid,
                                                contrato.id,
                                                linea.product_id.id,
                                                linea.detall,
                                                data_inici, data_final,
                                                linea.product_id.categ_id.code,
                                                factor=lvals['quantity'],
                                                context=context)

                if call_line_vals['price'] > 0:
                    call_name = ('Llamadas fuera de tarifa plana - %s'
                                 % linea.detall)
                    call_name += period_str
                    call_vals = {'price_unit': call_line_vals['price'],
                                 'quantity': 1,
                                 'product_id': call_line_vals['product_id'],
                                 'name': call_name,
                                 }
                    invoice_line_obj.copy(cursor, uid, linea_creada_id,
                                          call_vals)
                # Data consumption
                data_line_vals = data_obj.compute_data(cursor, uid,
                                                       contrato.id,
                                                       linea.detall,
                                                       data_inici, data_final,
                                                       context=context)

                if data_line_vals['price'] > 0:
                    data_name = ('Datos fuera de tarifa plana - %s'
                                 % linea.detall)
                    data_name += period_str
                    data_vals = {'price_unit': data_line_vals['price'],
                                 'quantity': 1,
                                 'product_id': data_line_vals['product_id'],
                                 'name': data_name,
                                 }
                    invoice_line_obj.copy(cursor, uid, linea_creada_id,
                                          data_vals)

        #Afegim els conceptes extra
        extra_obj.invoice_extra(cursor, uid, contract_id, invoice_id)
        #Cridem la funcio per calcular els impostos
        if not isinstance(invoice_id, list):
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])
        else:
            invoice_obj.button_reset_taxes(cursor, uid, invoice_id)

        if clot_id is not None:
            clot_obj.write(cursor, uid, [clot_id],
                           {'invoice_id': invoice_id})

        #Finally if the invoice has no lines, delete it
        created_invoice = invoice_obj.browse(cursor, uid, invoice_id)
        if not created_invoice.invoice_line:
            invoice_obj.unlink(cursor, 1, [invoice_id])
            return True

        if 'rectify' in context:
            vals = {
                'rectificative_type': 'R',
                'rectifying_id': context['rectify'],
            }
            invoice_obj.write(cursor, uid, [invoice_id], vals)

        return True

SollerOperadorFacturador()


class SollerOperadorFacturacioLot(osv.osv):

    _name = 'operador.facturacio.lot'

    _order = 'date_end desc'

    _max_backround_procs = 4
    _default_timeout_background = 24 * 3600

    def fill_lot(self, cursor, uid, lot_id, context=None):
        '''Select contracts for invoicing on date_end from lot'''

        facturador_obj = self.pool.get('soller.operador.facturador')
        clot_obj = self.pool.get('operador.facturacio.contracte_lot')

        if isinstance(lot_id, (list, tuple)):
            lot_id = lot_id[0]

        lot = self.browse(cursor, uid, lot_id)

        contract_ids = facturador_obj.get_contracts(cursor, uid,
                                                    lot.date_end)
        # Search for already added contracts
        clot_ids = self.get_clots(cursor, uid, lot_id, context=context)
        clot_vals = clot_obj.read(cursor, uid, clot_ids, ['contrato_id'])
        in_contract_ids = [x['contrato_id'][0] for x in clot_vals]
        add_contract_ids = list(set(contract_ids) - set(in_contract_ids))
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'fill_lot': True})
        for idx, contract_id in enumerate(add_contract_ids):
            # If not, add it to the lot
            values = {'lot_id': lot_id,
                      'contrato_id': contract_id}

            if idx >= (len(add_contract_ids) - 1):
                ctx.update({'fill_lot': False})
            clot_obj.create(cursor, uid, values, ctx)

        return True

    def get_clots(self, cursor, uid, lot_id, type='all', context=None):

        clot_obj = self.pool.get('operador.facturacio.contracte_lot')

        search_params = [('lot_id', '=', lot_id)]
        if type == 'invoiced':
            search_params.extend([('invoice_id', '<>', False)])
        elif type == 'not_invoiced':
            search_params.extend([('invoice_id', '=', False)])
        elif type == 'valid':
            search_params.extend([('invalid', '=', False)])
        elif type == 'invalid':
            search_params.extend([('invalid', '=', True)])
        elif type == 'for_invoicing':
            search_params.extend([('invoice_id', '=', False),
                                  ('invalid', '=', False)])

        return clot_obj.search(cursor, uid, search_params)

    @job(queue='operador_facturacio', timeout=28800)
    def notify(self, cursor, uid, subject, queue_name, task, context=None):

        acc_obj = self.pool.get('poweremail.core_accounts')
        mail_obj = self.pool.get('poweremail.mailbox')
        user_obj = self.pool.get('res.users')

        redis_conn = setup_redis_connection()
        q = queue.Queue(queue_name, connection=redis_conn)

        while not q.is_empty():
            time.sleep(1)
        try:
            msg = u''
            failed_count = 0
            failed_queue = get_failed_queue(connection=redis_conn)
            for job in failed_queue.jobs:
                if job.origin == queue_name and job.args[4] == task:
                    model = job.args[3]
                    model_obj = self.pool.get(model)
                    obj_name = model_obj.name_get(cursor, uid,
                                                  job.args[5])[0][1]
                    msg += _(u'\n* Error processant %s: \n') % task
                    msg += 'Model: %s\n' % model
                    msg += 'Id: %s (%s)\n' % (job.args[5], obj_name)
                    msg += u'%s\n' % job.exc_info
                    failed_count += 1
                    job.cancel()
                    failed_queue.remove(job)

            body = _(u'* Tasca %s finalitzada\n') % subject
            if msg:
                body += _(u'* S\'han produït els següents errors\n %s') % msg

            email_from = config.get('email_from', '')
            email_to = config.get('email_from', '')

            search_params = [('email_id', '=', email_from)]
            acc_id = acc_obj.search(cursor, uid, search_params)[0]

            user = user_obj.browse(cursor, uid, uid)
            user_email = user.address_id.email
            if user_email:
                email_to = user_email

            vals = {
                'pem_from': email_from,
                'pem_to': email_to,
                'pem_subject': subject,
                'pem_body_text': body,
                'pem_account_id': acc_id,
            }
            mail_id = mail_obj.create(cursor, uid, vals, context)
            mail_obj.send_this_mail(cursor, uid, [mail_id], context)
        except Exception, e:
            raise osv.except_osv('Error',
                                 _(u"S'ha produït un error "
                                   u"generant la notificació\n%s\n") % e)

        return True

    def facturar_lot(self, cursor, uid, lot_id, context=None):
        '''create invoice for all the contracts in lot'''

        facturador_obj = self.pool.get('soller.operador.facturador')
        clot_obj = self.pool.get('operador.facturacio.contracte_lot')

        if isinstance(lot_id, (list, tuple)):
            lot_id = lot_id[0]

        lot = self.browse(cursor, uid, lot_id)
        date_end = lot.date_end
        date_invoice = lot.date_invoice

        journal_id = facturador_obj.get_journal(cursor, uid, context)

        clot_ids = self.get_clots(cursor, uid, lot_id,
                                  type='for_invoicing',
                                  context=context)

        job_ids = []
        for clot in clot_obj.read(cursor, uid, clot_ids, ['contrato_id']):
            clot_id = clot['id']
            contract_id = clot['contrato_id'][0]
            job = facturador_obj.action_facturar_contrato(cursor, uid,
                                    contract_id, journal_id,
                                    date_end, lot.payment_term_id.id,
                                    date_invoice,
                                    clot_id,
                                    context=context)
            job_ids.append(job.id)

        self.notify(cursor, uid, _('Facturar lot'),
                    'operador_facturacio',
                    'action_facturar_contrato', context=context)

        # Create a jobs_group to see the status of the operation
        create_jobs_group(cursor.dbname, uid,
                          _('Operador'
                            ' billing {}'
                            ' ({} invoices)').format(lot.name, len(job_ids)),
                          'operador.facturacio.lot', job_ids)

        # Create workers to process the task
        aw = AutoWorker(queue='operador_facturacio',
                        default_result_ttl=self._default_timeout_background,
                        max_procs=self._max_backround_procs)
        aw.work()

        return True

    def validate_lot(self, cursor, uid, lot_id, context=None):
        '''validate all clots in lot'''

        clot_obj = self.pool.get('operador.facturacio.contracte_lot')

        if isinstance(lot_id, (list, tuple)):
            lot_id = lot_id[0]

        clot_ids = self.get_clots(cursor, uid, lot_id,
                                  type='not_invoiced',
                                  context=context)

        job_ids = []
        for clot_id in clot_ids:
            job = clot_obj.validate_clot(cursor, uid, clot_id, context=context)
            job_ids.append(job.id)

        self.notify(cursor, uid, _('Validar lot'),
                    'operador_facturacio',
                    'validate_clot', context=context)

        # Create a jobs_group to see the status of the operation
        create_jobs_group(cursor.dbname, uid,
                          _('Operador validation '
                            '({} invoices)').format(len(job_ids)),
                          'operador.facturacio.lot', job_ids)

        # Create workers to process the task
        aw = AutoWorker(queue='operador_facturacio',
                        default_result_ttl=self._default_timeout_background,
                        max_procs=self._max_backround_procs)
        aw.work()
        return True

    def open_invoices(self, cursor, uid, ids, context=None):
        """Open all invoices associated with lots in ids"""

        clot_obj = self.pool.get('operador.facturacio.contracte_lot')

        for lot_id in ids:
            # Search for all clots in lot
            clot_ids = self.get_clots(cursor, uid, lot_id, context=context)
            for clot_id in clot_ids:
                clot_obj.open_clot_invoice(cursor, uid, clot_id,
                                           context=context)
        self.notify(cursor, uid, _('Obrir factures operador'),
                    'operador_facturacio',
                    'open_clot_invoice', context=context)
        return True

    def send_invoices(self, cursor, uid, ids, context=None):
        """Send all invoices by mail"""

        if not context:
            context = {}

        clot_obj = self.pool.get('operador.facturacio.contracte_lot')
        config_obj = self.pool.get('res.config')
        tmpl_obj = self.pool.get('poweremail.templates')

        template_id = int(config_obj.get(cursor, uid,
                                         'operador_invoice_email_template',
                                         '0'))
        if template_id == 0:
            raise osv.except_osv('Error',
                                 _(u"No s'ha definit la "
                                   u"plantilla per l'enviament"))

        tmpl = tmpl_obj.browse(cursor, uid, template_id)

        ctx = context.copy()
        ctx.update({
                    'state': 'single',
                    'priority': '0',
                    'from': tmpl.enforce_from_account.id,
                    'template_id': template_id,
                    'src_model': 'account.invoice',
                    'type': 'out_invoice',
        })

        for lot_id in ids:
            clot_ids = self.get_clots(cursor, uid, lot_id, context=context)
            for clot_id in clot_ids:
                clot_obj.send_clot_invoice(cursor, uid, clot_id, ctx)

        self.notify(cursor, uid, _('Enviar factures operador'),
                    config.get('poweremail_render_queue', 'poweremail'),
                    'send_clot_invoice', context=context)
        return True

    def changes_in_lot(self, cursor, uid, lot_id, context=None):

        # Get previous lot_id
        lot = self.browse(cursor, uid, lot_id)
        search_params = [('date_end', '<', lot.date_end)]
        prev_lot_ids = self.search(cursor, uid, search_params,
                                     order='date_end desc', limit=1)

        in_list = out_list = []
        if prev_lot_ids:
            prev_lot_id = prev_lot_ids[0]
            query = '''
                select contrato_id
                from operador_facturacio_contracte_lot clot
                where lot_id = %s
            '''
            cursor.execute(query, (prev_lot_id, ))

            prev_contract_list = [x[0] for x in cursor.fetchall()]

            cursor.execute(query, (lot_id,))
            contract_list = [x[0] for x in cursor.fetchall()]

            # In contracts
            in_list = list(set(contract_list) - set(prev_contract_list))
            # Out contracts
            out_list = list(set(prev_contract_list) - set(contract_list))

        return in_list, out_list


    def add_invoices_remittance(self, cursor, uid, ids, order_id, context=None):
        """
            Add invoices from lot to remittance.
            Filter invoices considering destination remittance
        """
        clot_obj = self.pool.get('operador.facturacio.contracte_lot')
        order_obj = self.pool.get('payment.order')
        invoice_obj = self.pool.get('account.invoice')

        if not context:
            context = {}
        if isinstance(ids, list):
            ids = ids[0]

        order = order_obj.browse(cursor, uid, order_id)
        limit = context.get('invoices_limit', None)
        not_ordered = context.get('not_ordered', False)

        clot_ids = self.get_clots(cursor, uid, ids, 'invoiced')
        clot_vals = clot_obj.read(cursor, uid, clot_ids, ['invoice_id'])
        invoice_ids = [x['invoice_id'][0] for x in clot_vals]

        search_params = [('id', 'in', invoice_ids),
                         ('type', '=', 'out_invoice'),
                         ('payment_type', '=', order.mode.type.id)]

        if not_ordered:
            search_params.append(('payment_order_id', '=', False))

        fids = invoice_obj.search(
            cursor, uid, search_params, limit=limit, order='number, id'
        )

        for invoice_id in fids:
            self.add_invoice_remittance(cursor, uid, invoice_id, order_id,
                                        context=context)

    @job(queue='operador_facturacio')
    def add_invoice_remittance(self, cursor, uid, id, order_id, context=None):
        """
           Add an invoice from lot to remittance.
        """
        invoice_obj = self.pool.get('account.invoice')
        invoice_obj.afegeix_a_remesa(cursor, uid, [id], order_id, context)

    def _get_lot_from_clot(self, cursor, uid, ids, context=None):
        """Returns lot associated to clots in ids
        """

        clot_obj = self.pool.get('operador.facturacio.contracte_lot')

        lot_values = clot_obj.read(cursor, uid, ids, ['lot_id'])
        return list(set([x['lot_id'][0] for x in lot_values]))

    def _ff_n_clots(self, cursor, uid, ids, field_name, arg, context=None):

        res = {}
        clot_obj = self.pool.get('operador.facturacio.contracte_lot')
        for lot_id in ids:
            search_params = [('lot_id', '=', lot_id)]
            res[lot_id] = clot_obj.search_count(cursor, uid, search_params)
        return res

    def _ff_n_invoices(self, cursor, uid, ids, field_name, arg, context=None):

        res = {}
        clot_obj = self.pool.get('operador.facturacio.contracte_lot')
        for lot_id in ids:
            search_params = [('lot_id', '=', lot_id),
                             ('invoice_id', '<>', False)]
            res[lot_id] = clot_obj.search_count(cursor, uid, search_params)
        return res

    def _ff_n_contracts(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        if not context:
            context = {}

        # If we are filling lot don't update values.
        if 'fill_lot' in context and context['fill_lot']:
            return res

        for lot_id in ids:
            in_list, out_list = self.changes_in_lot(cursor, uid, lot_id)
            res[lot_id] = {'n_in_contracts': len(in_list),
                           'n_out_contracts': len(out_list)}
        return res

    _store_nclots = {'operador.facturacio.lot':
                     (lambda self, cr, uid, ids, c=None: ids, [], 10),
                     'operador.facturacio.contracte_lot':
                     (_get_lot_from_clot, ['lot_id'], 10)}

    _store_ninvoices = {'operador.facturacio.lot':
                        (lambda self, cr, uid, ids, c=None: ids, [], 10),
                        'operador.facturacio.contracte_lot':
                        (_get_lot_from_clot, ['invoice_id'], 10)}

    _store_contracts = {'operador.facturacio.lot':
                            (lambda self, cr, uid, ids, c=None: ids, [], 10),
                        'operador.facturacio.contracte_lot':
                            (_get_lot_from_clot, ['lot_id'], 10)
                        }

    _columns = {
        'name': fields.char('Nom', size=12, required=True),
        'date_end': fields.date('Data final', required=True,
                                help='Data fins la qual es facturarà'),
        'date_invoice': fields.date('Data de factura', required=True),
        'payment_term_id': fields.many2one('account.payment.term',
                                           'Termini pagament',
                                           required=True),
        'n_clots': fields.function(_ff_n_clots, method=True,
                                   string='Número de contractes',
                                   type='integer',
                                   store=_store_nclots),
        'n_invoices': fields.function(_ff_n_invoices, method=True,
                                      string='Factures generades',
                                      type='integer',
                                      store=False),

        'n_in_contracts': fields.function(_ff_n_contracts, method=True,
                                          string='Número entrades',
                                          type='integer',
                                          multi='changes',
                                          store=_store_contracts),
        'n_out_contracts': fields.function(_ff_n_contracts, method=True,
                                           string='Número sortides',
                                           type='integer',
                                           multi='changes',
                                           store=_store_contracts),

    }

SollerOperadorFacturacioLot()


class SollerOperadorFacturacioLotLinia(osv.osv):

    _name = 'operador.facturacio.contracte_lot'

    def facturar_clot(self, cursor, uid, ids, context=None):

        facturador_obj = self.pool.get('soller.operador.facturador')

        for id in ids:
            clot = self.browse(cursor, uid, id)
            # If already invoiced, do nothing
            if clot.invoice_id:
                continue
            journal_id = facturador_obj.get_journal(cursor, uid, context)
            facturador_obj.action_facturar_contrato(cursor, uid,
                                    clot.contrato_id.id,
                                    journal_id,
                                    clot.lot_id.date_end,
                                    clot.lot_id.payment_term_id.id,
                                    clot.lot_id.date_invoice,
                                    id,
                                    context=context)
        return True

    def validate_mandate(self, cursor, uid, clot, context=None):

        facturador_obj = self.pool.get('soller.operador.facturador')

        msg = ''
        if clot.contrato_id.tipo_pago_id.code != 'RECIBO_CSB':
            return msg

        mandate_id = facturador_obj.search_mandate(cursor, uid,
                                                   clot.contrato_id,
                                                   context=context)
        if not mandate_id:
            msg = _(u"* Aquest contracte no té mandato associat")
        return msg

    def validate_multiservice_change(self, cursor, uid, clot, context=None):
        contrato_obj = self.pool.get('soller.operador.contrato')

        start_date = (clot.contrato_id.data_darrera_facturacio or
                      clot.contrato_id.data_alta)
        end_date = clot.lot_id.date_end
        contract_id = clot.contrato_id.id

        ctx = context.copy()
        ctx.update({'date_multiservicio': start_date})
        startMS = contrato_obj.multiservicio(cursor, uid,
                                                   [contract_id],
                                                   context=ctx)[contract_id]

        ctx.update({'date_multiservicio': end_date})
        endMS = contrato_obj.multiservicio(cursor, uid,
                                               [contract_id],
                                               context=ctx)[contract_id]
        msg = ''
        if startMS != endMS:
             state = ['D', 'A']

             msg = _(u"* Ha canviat l'estat del multiservei: {} -> {}".format(
                 state[startMS], state[endMS]))

        return msg


    _validate_functions = ['validate_mandate', 'validate_multiservice_change']

    @job(queue='operador_facturacio')
    def validate_clot(self, cursor, uid, ids, context=None):

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        final_msg = []
        for id in ids:
            clot = self.browse(cursor, uid, id)
            for function_name in self._validate_functions:
                try:
                    function = getattr(self, function_name)
                    msg = function(cursor, uid, clot, context=context)
                except:
                    msg = "* Error executant: {}".format(function_name)
                finally:
                    if msg:
                        final_msg.append(msg)

            if final_msg:
                clot.write({'status': '\n'.join(final_msg)})
            else:
                clot.write({'status': ''})
        return True

    @job(queue='operador_facturacio')
    def open_clot_invoice(self, cursor, uid, clot_id, context=None):

        wkf_service = netsvc.LocalService("workflow")

        clot_vals = self.read(cursor, uid, clot_id,
                              ['invoice_id'])
        if clot_vals['invoice_id']:
            invoice_id = clot_vals['invoice_id'][0]
            wkf_service.trg_validate(uid, 'account.invoice',
                                     invoice_id, 'invoice_open',
                                     cursor)
        return True

    @job(queue=config.get('poweremail_render_queue', 'poweremail'))
    def send_clot_invoice(self, cursor, uid, clot_id, context=None):
        """Send associated invoice by email"""

        email_wizard_obj = self.pool.get('poweremail.send.wizard')
        mail_obj = self.pool.get('poweremail.mailbox')

        clot = self.browse(cursor, uid, clot_id)
        if (not clot.invoice_id or
            clot.invoice_id.state not in ('open', 'paid')):
            return True
        invoice_id = clot.invoice_id.id

        wiz_vals = {
                    'state': context['state'],
                    'priority': context['priority'],
                    'from': context['from'],
        }
        context.update({'src_rec_ids': [invoice_id],
                        'active_ids': [invoice_id],
                       })
        pe_wizard = email_wizard_obj.create(cursor, uid, wiz_vals,
                                            context=context)

        mail_ids = email_wizard_obj.save_to_mailbox(cursor, uid, [pe_wizard],
                                                    context=context)

        return mail_obj.send_this_mail(cursor, uid, mail_ids, context)

    def _ff_valid(self, cursor, uid, ids, field_name, arg, context=None):

        res = {}
        clot_vals = self.read(cursor, uid, ids, ['status'], context=context)
        for clot_val in clot_vals:
            clot_id = clot_val['id']
            if clot_val['status']:
                res[clot_id] = True
            else:
                res[clot_id] = False
        return res

    _store_invalid = {'operador.facturacio.contracte_lot':
                      (lambda self, cr, uid, ids, c=None: ids, [], 10)}

    _columns = {
        'contrato_id': fields.many2one('soller.operador.contrato', 'Contracte',
                                       required=True),
        'lot_id': fields.many2one('operador.facturacio.lot', 'Lot',
                                  required=True),
        'invoice_id': fields.many2one('account.invoice', 'Factura associada'),
        'status': fields.text('Validacions'),
        'invalid': fields.function(_ff_valid, method=True,
                                 string='Validacions pendents',
                                 type='boolean',
                                 store=_store_invalid)
    }

SollerOperadorFacturacioLotLinia()


class SollerOperadorFacturacioLot2(osv.osv):

    _name = 'operador.facturacio.lot'
    _inherit = 'operador.facturacio.lot'

    _columns = {
        'lot_ids': fields.one2many('operador.facturacio.contracte_lot',
                                   'lot_id', 'Contractes')
    }

SollerOperadorFacturacioLot2()
