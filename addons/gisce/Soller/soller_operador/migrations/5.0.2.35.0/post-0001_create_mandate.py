# -*- coding: utf-8 -*-
import pooler
import netsvc

def migrate(cursor, installed_version):
    """Migration to 2.35.0
    """
    logger = netsvc.Logger()
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    contrato_obj = pool.get('soller.operador.contrato')
    mandate_obj = pool.get('payment.mandate')
    #Search for all contrato with an associated bank account
    search_params = [('bank_id', '<>', False),
                     ('state', 'not in', ('esborrany', 'validar', 'baixa'))]
    contrato_ids = contrato_obj.search(cursor, uid, search_params)
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u"Creating mandates for "
                         u"%s active contracts" % len(contrato_ids))
    for contrato_id in contrato_ids:
        vals = {
            'reference': 'soller.operador.contrato,%s' % contrato_id,
            #SEPA says all mandates signed before
            #migration must have this date
            'date': '2009-10-31' 
        }
        mandate_obj.create(cursor, uid, vals)
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')    
