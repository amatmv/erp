# coding=utf-8
import netsvc

def up(cursor, installed_version):
    logger = netsvc.Logger()
    if not installed_version:
        return

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        'Fill column cdr_number to soller_operador_cdr'
    )

    sql = "UPDATE soller_operador_cdr SET cdr_number =origin WHERE coalesce(cdr_number, '') = ''"
    cursor.execute(sql)

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Succesfully added!')


def down(cursor, installed_version):
    pass

migrate = up
