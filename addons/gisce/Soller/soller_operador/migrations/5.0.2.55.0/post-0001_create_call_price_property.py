# -*- coding: utf-8 -*-
import pooler
import netsvc

def migrate(cursor, installed_version):
    """Migration to 2.55.0
    """
    logger = netsvc.Logger()
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    property_obj = pool.get('ir.property')
    pricelist_obj = pool.get('product.pricelist')
    field_obj = pool.get('ir.model.fields')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u"Creating default call price property")

    #Search for default call pricelist
    search_params = [('name', '=', 'Llamadas')]
    pricelist_id = pricelist_obj.search(cursor, uid, search_params)[0]

    #Search for property_call_price field in contract
    search_params = [('model', '=', 'soller.operador.contrato'),
                     ('name', '=', 'property_call_price')]
    field_id = field_obj.search(cursor, uid, search_params)[0]

    vals = {
        'name': 'property_call_price',
        'fields_id': field_id,
        'value': 'product.pricelist,%s' % pricelist_id
    }
    property_obj.create(cursor, uid, vals)

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')    
