# -*- encoding: utf-8 -*-

from datetime import datetime

from erppeek import Client
from MySQLdb import connect

import config


def get_contract_phone(erp_conn):
    """ Returns a dict with contract id as key and a list of phone numbers
    " as value:
    " {
        '21344: ['971634000', '971634444'],
        '45653: ['971634330'],
      }'
    """
    line_obj = erp_conn.model('soller.operador.contrato.linea')
    search_params = [('product_id', '=', 'TLFPL'),
                     ('detall', '!=', ''),
                    ]
    context = {'active_test': False}
    phone_lines_ids = line_obj.search(search_params, context=context)

    contract_phone = {}
    for phone_line_id in phone_lines_ids:
        line = line_obj.get(phone_line_id)
        if line.contrato_id.id in contract_phone:
            contract_phone[line.contrato_id.id].append(line.detall)
        else:
            contract_phone[line.contrato_id.id] = [line.detall]

    return contract_phone


def last_inserted_call_date(erp_conn, contract_id, phone):
    """ Return the date of the last inserted call, None if no call found """
    call_obj = erp_conn.model('soller.operador.call')
    search_params = [('contract_id', '=', contract_id),
                     ('origin', '=', phone)]
    call_ids = call_obj.search(search_params, order='timestamp desc')
    if not call_ids:
        return None

    last_call = call_obj.get(call_ids[0])
    return datetime.strptime(last_call.timestamp, '%Y-%m-%d %H:%M:%S')


def get_mysql_calls(mysql_cursor, phone, date_from=None, date_to=None):
    """ Returns a list of call of the number phone betwen date_from and date to
    " Retun looks like dicts like:
    " [{'origin': '971634000', 'destination': '902000000',
    "   'duration': 69, 'timestamp': (Datetime)}]
    """
    if not date_from:
        date_from = datetime.fromtimestamp(0)

    if not date_to:
        date_to = datetime.now()

    calls = []
    query = u"""
             SELECT callingstationid, calledstationid,
                    sipcallduration,
                    str_to_date(connecttime, '%%b %%e %%Y %%H:%%i:%%s')
               FROM radacct
              WHERE callingstationid = %s
                AND str_to_date(connecttime, '%%b %%e %%Y %%H:%%i:%%s') >= %s
                AND str_to_date(connecttime, '%%b %%e %%Y %%H:%%i:%%s') <= %s
             ORDER BY str_to_date(connecttime, '%%b %%e %%Y %%H:%%i:%%s') ASC
            """
    date_from_str = date_from.strftime('%Y-%m-%d %H:%M:%S')
    date_to_str = date_to.strftime('%Y-%m-%d %H:%M:%S')
    mysql_cursor.execute(query, (phone, date_from_str, date_to_str, ))

    rows = mysql_cursor.fetchall()
    for origin, destination, duration, timestamp in rows:
        for prefix in config.ignore_prefixes:
            if prefix in origin:
                origin = origin.replace(prefix, '')
        for prefix in config.ignore_prefixes:
            if prefix in destination:
                destination = destination.replace(prefix, '')
        calls.append({'origin': origin,
                      'destination': destination,
                      'duration': duration,
                      'timestamp': timestamp.strftime("%Y-%m-%d %H:%M:%S"),
                     })
    return calls


def load_calls(erp_conn, mysql_cursor, contract_id, phone):
    """ Load calls of phone number to contract
    " Return the number of inserted calls
    """
    call_obj = erp_conn.model('soller.operador.call')

    inserts = 0

    last_insert_date = last_inserted_call_date(erp_conn, contract_id, phone)
    calls = get_mysql_calls(mysql_cursor, phone, date_from=last_insert_date)
    for call in calls:
        call['contract_id'] = contract_id

        # There can NOT be two calls from same origin at the same second
        search_params = [('origin', '=', call['origin']),
                         ('timestamp', '=', call['timestamp']),
                        ]
        already_inserted = call_obj.search(search_params)
        if not already_inserted:
            inserts += 1
            call_obj.create(call)
    return inserts

if __name__ == '__main__':
    erp_conn = Client(config.erp_server, db=config.erp_db,
                      user=config.erp_user, password=config.erp_pass)

    mysql_conn = connect(host=config.mysql_server, user=config.mysql_user,
                         passwd=config.mysql_pass, db=config.mysql_db,
                         port=config.mysql_port)
    mysql_cursor = mysql_conn.cursor()

    inserts = 0
    contract_phone = get_contract_phone(erp_conn)
    for contract_id, phones in contract_phone.iteritems():
        for phone in phones:
            inserts += load_calls(erp_conn, mysql_cursor, contract_id, phone)

    print(u'Inserted %d new calls to the ERP' % inserts)

    mysql_conn.close()
