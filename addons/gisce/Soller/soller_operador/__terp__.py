# -*- coding: utf-8 -*-
{
    "name": "Soller Operador de internet",
    "description": """
  Módulo base para el control de un operador de internet:
    * Contratos
    * Facturas
      """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "base",
        "account",
        "account_payment",
        "product",
        "l10n_chart_ES",
        "oorq",
        "poweremail",
        "phantom_reports",
        "fibra_cups",
        "product_category_code",
        "stock",
        "l10n_ES_remesas",
        "c2c_webkit_report",
        "giscedata_remeses"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/soller_operador_security.xml",
        "wizard/soller_operador_ver_password_view.xml",
        "soller_operador_sequence.xml",
        "soller_operador_data.xml",
        "soller_operador_view.xml",
        "partner_view.xml",
        "wizard/wizard_operador_facturador_view.xml",
        "wizard/wizard_operador_informes_facturacio_view.xml",
        "wizard/wizard_operador_importar_cdr_view.xml",
        "wizard/wizard_operador_refund_view.xml",
        "wizard/wizard_operador_facturacio_lot_canvis_view.xml",
        "wizard/wizard_operador_multiservice_view.xml",
        "wizard/wizard_add_invoices_remittance_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
