# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _
from giscedata_remeses.wizard.afegir_factures_remesa import VALID_MODELS

VALID_MODELS.append('operador.facturacio.lot')

class WizardAfegirFacturesRemesaOperador(osv.osv_memory):

    _inherit = 'wizard.afegir.factures.remesa'

    def get_invoices(self, cursor, uid, context=None):
        """
        Gets selected invoices
        :return: list with invoices ids
        """
        lot_obj = self.pool.get('operador.facturacio.lot')
        clot_obj = self.pool.get('operador.facturacio.contracte_lot')
        if context is None:
            context = {}

        model = context.get('model')
        if model == 'operador.facturacio.lot':
            lot_id = context.get('active_ids', False)
            if lot_id:
                lot_id = lot_id[0]

            contracte_lot_ids = lot_obj.read(
                cursor, uid, lot_id, ['lot_ids'], context
            )['lot_ids']
            lot_vals = clot_obj.read(
                cursor, uid, contracte_lot_ids, ['invoice_id']
            )
            # list of lists
            invoices_in_lot = [x['invoice_id'] for x in lot_vals]
            return invoices_in_lot
            # only one invoice list
            # invoice_ids = [i for s in invoices_in_lot for i in s]
            # return invoice_ids

        else:
            return super(WizardAfegirFacturesRemesaOperador, self
                         ).get_invoices(cursor, uid, context=context)

    def get_invoices_info(self, cursor, uid, invoice_ids, context=None):
        if context is None:
            context = {}

        model = context.get('model')
        if model == 'operador.facturacio.lot':
            return {}
        else:
            return super(WizardAfegirFacturesRemesaOperador, self
                         ).get_invoices_info(cursor, uid, invoice_ids,
                                             context=context)


    def action_afegir_factures(self, cursor, uid, ids, context):
        """
            Add invoices to remittance
        """
        if not context:
            context = {}

        model = context.get('model')

        if model == 'operador.facturacio.lot':
            if isinstance(ids, list):
                ids = ids[0]
            wiz = self.browse(cursor, uid, ids, context)

            lot_id = context.get('active_ids', False)
            if lot_id:
                lot_id = lot_id[0]
            lot_obj = self.pool.get('operador.facturacio.lot')
            lot = lot_obj.browse(cursor, uid, lot_id, context)
            ctx = context.copy()
            if wiz.invoices_limit:
                ctx['invoices_limit'] = wiz.invoices_limit
            if wiz.not_ordered:
                ctx['not_ordered'] = wiz.not_ordered
            lot.add_invoices_remittance(wiz.order.id, context=ctx)
            wiz.write({'state': 'end'})
        else:
            super(WizardAfegirFacturesRemesaOperador, self
                  ).action_afegir_factures(cursor, uid, ids, context=context)


WizardAfegirFacturesRemesaOperador()