# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields


class soller_operador_ver_password(osv.osv_memory):
    """
    Wizard para ver el password
    """

    _name = "soller.operador.password"
    _description = "Veure password"

    def get_password(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])
        router_obj = self.pool.get('soller.operador.router')
        router_id = context.get('active_id', 0)
        router = router_obj.browse(cursor, uid, router_id)
        wizard.write({'password': router.password, 'state': 'end'})
        return True

    _columns = {
        'password': fields.char('Contrasenya', size=64, required=False,
                               readonly=True),
        'state': fields.selection([('init', 'Init'),
                                  ('end', 'End')], 'Estat',
                                 select=True, readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

soller_operador_ver_password()
