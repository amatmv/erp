# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.translate import _

from itertools import count, ifilter
from soller_operador.soller_operador import CHANGE_CONTRACT_TYPE


class WizardOperadorFacturacioLotCanvis(osv.osv_memory):
    """
    Wizard where calculate the in and out contracts in an invoice lot
    """

    _name = "wizard.operador.facturacio.lot.canvis"

    def default_lot(self, cursor, uid, context=None):
        '''
            Init the lot.
        '''

        if not context:
            context = {}

        lot_id = context.get('active_ids', [])

        return lot_id[0]

    def action_accept(self, cursor, uid, ids, context=None):
        '''
            Create list of in/out contracts
        '''

        wizard = self.browse(cursor, uid, ids[0])
        lot_id = context.get('active_ids', [])

        if isinstance(lot_id, (list, tuple)):
            lot_id = lot_id[0]

        lot_obj = self.pool.get('operador.facturacio.lot')
        in_list, out_list = lot_obj.changes_in_lot(cursor, uid, lot_id)
        if wizard.change_type == 'in':
            contract_list = in_list
        else:
            contract_list = out_list

        type_text = {'in': 'Entrades', 'out': 'Sortides'}
        vals_view = {
            'name': 'Llistat de contractes ({})'.format(
                type_text[wizard.change_type]),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'soller.operador.contrato',
            'limit': len(contract_list),
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', {})]".format(contract_list),
            'context': {'active_test': False}
        }
        return vals_view


    _columns = {
        'change_type': fields.selection(CHANGE_CONTRACT_TYPE, 'Tipus canvi',
                                        required=True),
    }

    _defaults = {
        'change_type': lambda *a: 'in',

    }

WizardOperadorFacturacioLotCanvis()