# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config
from tools.translate import _


class WizardOperadorImportarCdr(osv.osv_memory):

    _name = 'wizard.operador.importar.cdr'

    _state_selection = [('init', 'Init'), ('done', 'Done')]

    def importar_cdr(self, cursor, uid, ids, context=None):
        """Importar un CDR
        """

        cdr_obj = self.pool.get('soller.operador.cdr')

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        cdr_obj.import_cdr(cursor, uid, wizard.name, wizard.cdr,
                           wizard.partner_id.id, context=context)

        wizard.write({'state': 'done',
                      'info': 'Important CDR en segon pla.'})

    _columns = {
        'cdr': fields.binary('Fitxer CDR', required=True),
        'partner_id': fields.many2one('res.partner', 'Proveïdor',
                                      required=True),
        'info': fields.text('Informació', readonly=True),
        'state': fields.selection(_state_selection, 'Estat'),
        'name': fields.char('File name', size=256)
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardOperadorImportarCdr()
