# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.translate import _


class WizardOperadorFacturador(osv.osv_memory):
    """
    Wizard para lanzar la facturacion mensual de los contratos de operador
    """

    _name = "wizard.operador.facturador"
    _description = "Facturar contractes"

    def create(self, cursor, uid, ids, context=None):
        '''comprobamos que el usuario tiene permisos para facturar'''

        model_access_obj = self.pool.get('ir.model.access')

        if not model_access_obj.check(cursor, uid,
                                      'soller.operador.facturador',
                                      'read', False):
            raise osv.except_osv('Error',
                                 _(u"¡No te permisos per "
                                   u"realitzar aquesta acció!"))

        return super(WizardOperadorFacturador,
                     self).create(cursor, uid, ids, context)

    def action_facturar(self, cursor, uid, ids, context=None):
        """ Metodo que factura todos los contratos """

        wizard = self.browse(cursor, uid, ids[0])

        facturador_obj = self.pool.get('soller.operador.facturador')

        facturador_obj.action_facturar(cursor, uid, wizard.data_factura,
                                       wizard.termini_pagament_id.id,
                                       context=context)

        wizard.write({'state': 'end',
                      'notes': _(u'El procés de facturació ha començat '
                                 u'en segon pla')})

    _columns = {
        'data_factura': fields.date('Data factura', required=True),
        'termini_pagament_id': fields.many2one('account.payment.term',
                                               'Termini pagament',
                                               required=True),
        'notes': fields.text('Notes'),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')], 'Estat'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardOperadorFacturador()
