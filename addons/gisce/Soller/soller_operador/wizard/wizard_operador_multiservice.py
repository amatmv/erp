# -*- encoding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
from tools.translate import _

class WizardOperadorMultiservice(osv.osv_memory):

    _name = 'wizard.operador.multiservice'

    def action_multiservice(self, cursor, uid, ids, context=None):
        contract_obj = self.pool.get('soller.operador.contrato')

        if context is None:
            context = {}

        contract_ids = context.get('active_ids', [])

        wizard = self.browse(cursor, uid, ids[0])


        ctx = context.copy()
        ctx.update({'date_multiservicio': wizard.multiservice_date})
        formated_date = datetime.strptime(
            wizard.multiservice_date, '%Y-%m-%d').strftime('%d/%m/%Y')

        if contract_obj.multiservicio(cursor, uid, contract_ids
                                      )[contract_ids[0]]:
            text = _('El contracte té multiservei per la data {}'
                     ).format(formated_date)
        else:
            text = _('El contracte NO té multiservei per la data {}'
                     ).format(formated_date)

        wizard.write({'multiservice_text': text,'state': 'end'})

        return True

    _columns = {
        'multiservice_date': fields.date('Data', required=True),
        'multiservice_text': fields.text('Multiservei', readonly=True),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')], 'Estat',
                                  select=True, readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'multiservice_date': lambda *a: datetime.now().strftime('%Y-%m-%d')
    }

WizardOperadorMultiservice()
