# -*- encoding: utf-8 -*-

from osv import osv, fields


class WizardOperadorRefund(osv.osv_memory):

    _name = 'wizard.operador.refund'

    def action_refund(self, cursor, uid, ids, context=None):

        if context is None:
            context = {}

        facturador_obj = self.pool.get('soller.operador.facturador')
        invoice_obj = self.pool.get('account.invoice')
        contrato_obj = self.pool.get('soller.operador.contrato')

        invoice_ids = context.get('active_ids', [])

        wizard = self.browse(cursor, uid, ids[0])

        if wizard.rectify:
            rectificative_type = 'B'
        else:
            rectificative_type = 'A'

        created_ids = facturador_obj.action_refund(cursor, uid,
                                                   invoice_ids,
                                                   wizard.date_invoice,
                                                   type=rectificative_type,
                                                   context=context)

        if wizard.rectify:
            for invoice in invoice_obj.browse(cursor, uid, invoice_ids):
                # Search for associated contracts
                contract_name = invoice.origin
                search_params = [('name', '=', contract_name)]
                ctx = context.copy()
                ctx.update({'active_test': False})
                contract_ids = contrato_obj.search(cursor, uid, search_params,
                                                   context=ctx)
                ctx = context.copy()
                ctx.update({'custom_dates': True,
                            'date_from': wizard.refund_date_from,
                            'date_to': wizard.refund_date_to,
                            'individual': True,
                            'active_ids': contract_ids,
                            'rectify': invoice.id})

                facturador_obj.action_facturar(cursor, uid,
                                               wizard.date_invoice,
                                               wizard.payment_term_id.id,
                                               context=ctx)

        return {}

    _columns = {
        'date_invoice': fields.date('Data factura', required=True),
        'rectify': fields.boolean('Generar Rectificadora'),
        'refund_date_from': fields.date('Data inici'),
        'refund_date_to': fields.date('Data final'),
        'payment_term_id': fields.many2one('account.payment.term',
                                           'Termini pagament'),
    }

    _defaults = {
        'rectify': lambda *a: False,
    }

WizardOperadorRefund()
