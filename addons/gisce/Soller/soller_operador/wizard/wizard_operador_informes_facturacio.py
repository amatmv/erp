# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import time
from tools import config
from tools.translate import _

INFORMES_DESC = {
    'report_facturacio_operador_producte':
    _(u"Resum de facturació de l'operador per productes agrupant per  "
      u"tipus de instal·lació (radio o fibra) i professional o domèstic, "
      u"mostrant import facturat (sense impostos) i quantitat."),
}


class WizardOperadorInformesFacturacio(osv.osv_memory):

    _name = 'wizard.operador.informes.facturacio'

    def _get_informe(self, cursor, uid, context=None):
        """ Com a funció perquè si no no tradueix"""

        _opcions = [('report_facturacio_operador_producte',
                    _('Facturació operador per productes')),
                    ('report_facturacio_operador_categoria_producte',
                    _('Facturació operador per categories de productes')),
                   ]

        return _opcions

    def get_info_desc(self, informe):
        """ Retorna la informació de l'informe """
        return INFORMES_DESC.get(informe)

    def imprimir(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])
        datas = {
            'date_from': wizard.date_from,
            'date_to': wizard.date_to,
        }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': wizard.informe,
            'datas': datas,
        }

    def onchange_informe(self, cursor, uid, ids, informe):
        return {'value': {'desc': self.get_info_desc(informe)}}

    _columns = {
        'date_from': fields.date('Des de', required=True),
        'date_to': fields.date('Fins', required=True),
        'informe': fields.selection(_get_informe, 'Informe', required=True),
        'desc': fields.text('Descripció', readonly=True)

    }


WizardOperadorInformesFacturacio()
