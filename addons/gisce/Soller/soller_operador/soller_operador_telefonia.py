# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
import pooler
from tools.translate import _
from tools.misc import cache
from tools import config
from oorq.decorators import job

from datetime import datetime
from re import match
import base64
import netsvc

from xlrd import open_workbook

import codecs


class SollerOperadorCallKind(osv.osv):

    _name = 'soller.operador.call.kind'
    _order = 'seq'  # Order by order field ASC

    _soller_operador_call_kinds = [
        ('national', 'Nacional'),
        ('international', 'Internacional'),
        ('mobile', 'Mòbil'),
        ('other', 'Altra'),
    ]

    def call_kind(self, cursor, uid, destination, context=None):
        kind_ids = self.search(cursor, uid, [])

        for kind_id in kind_ids:
            kind = self.browse(cursor, uid, kind_id)
            if match(kind.regex, destination):
                return kind_id

        # In case that we do not find any one return the last one (catch all)
        final_kind = self.search(cursor, uid, [], order='seq desc', limit=1)
        return final_kind[0]

    @cache()
    def get_uos(self, cursor, uid, kind_id, context=None):
        if isinstance(kind_id, (list, tuple)):
            kind_id = kind_id[0]
        kind = self.browse(cursor, uid, kind_id)
        return kind.product_id.uom_po_id.id

    _columns = {
        'name': fields.char('Nom', size=32, required=True),
        'type': fields.selection(_soller_operador_call_kinds,
                                 string='Tipus'),
        'product_id': fields.many2one('product.product', 'Producte'),
        'regex': fields.char('Regex', required=True, size=32),
        'seq': fields.integer('Ordre regex', required=True),
    }

SollerOperadorCallKind()


class SollerOperadorCall(osv.osv):

    _name = 'soller.operador.call'

    def _get_call_kind(self, cursor, uid, ids, name, arg, context=None):

        kind_obj = self.pool.get('soller.operador.call.kind')
        res = {}
        for id in ids:
            destination = self.read(cursor, uid, id,
                                    ['destination'])['destination']
            res[id] = kind_obj.call_kind(cursor, uid, destination,
                                         context=context)
        return res

    _columns = {
        'contract_id': fields.many2one('soller.operador.contrato',
                                       'Contracte', required=True),
        'origin': fields.char('Origen', size=20, required=True,
                              help=u"Origen de la trucada (qui truca)"),
        'destination': fields.char('Destí', size=20,
                                   required=True,
                                   help=u"Destí de la trucada (a qui es truca)"),
        'timestamp': fields.datetime('Data', required=True),
        'duration': fields.integer('Durada', required=True),
        'kind': fields.function(_get_call_kind, type='many2one',
                                obj='soller.operador.call.kind',
                                method=True, string='Tipus',
                                readonly=True,
                                store=True),
    }

SollerOperadorCall()


class SollerOperadorCdr(osv.osv):

    _name = 'soller.operador.cdr'

    _soller_operador_call_type = [
        ('call', 'Cridada'),
        ('sms', 'sms'),
    ]

    def sanitize_number_xtratelecom(self, cursor, uid, number):
        if number.startswith('3400') or number.startswith('3407'):
            number = number[6:]
        return number

    def sanitize_number_airenetworks(self, cursor, uid, number):
        if number.startswith('->'):
            number = number[2:]
        return number

    def import_cdr_from_xtratelecom(self, cursor, uid, filename, file,
                                    partner_id, context=None):
        contract_obj = self.pool.get('soller.operador.contrato')
        db = pooler.get_db_only(cursor.dbname)

        msg = ''
        created = 0
        not_created = 0
        already_created = 0
        data = base64.decodestring(file)
        wb = open_workbook(file_contents=data, encoding_override='latin1')
        sheet = wb.sheet_by_index(0)
        for row_index in range(sheet.nrows):
            origin = ''
            destination = ''
            timestamp_str = ''
            try:
                tmp_cr = db.cursor()
                if not unicode(sheet.cell(row_index, 0).value).isnumeric():
                    continue
                origin = sheet.cell(row_index, 0).value
                origin = self.sanitize_number_xtratelecom(tmp_cr, uid, origin)

                destination = sheet.cell(row_index, 1).value
                destination = self.sanitize_number_xtratelecom(tmp_cr, uid,
                                                               destination)

                description = sheet.cell(row_index, 2).value

                timestamp_str = sheet.cell(row_index, 3).value
                timestamp_str += sheet.cell(row_index, 4).value
                timestamp = datetime.strptime(timestamp_str,
                                              '%d-%m-%Y%H:%M:%S')
                timestamp = timestamp.strftime('%Y-%m-%d %H:%M:%S')

                d_str = sheet.cell(row_index, 5).value.split(':')
                duration = (int(d_str[0]) * 3600
                            + int(d_str[1]) * 60
                            + int(d_str[2]))

                cost_price = float(
                    sheet.cell(row_index, 7).value.replace(',', '.'))

                contract_id = contract_obj.get_contract_from_phone_date(tmp_cr,
                                           uid, origin, timestamp)
                search_params = [
                                 ('contract_id', '=', contract_id),
                                 ('partner_id', '=', partner_id),
                                 ('origin', '=', origin),
                                 ('destination', '=', destination),
                                 ('timestamp', '=', timestamp),
                                 ('duration', '=', duration),
                                 ]
                cdr_ids = self.search(tmp_cr, uid, search_params,
                                      context=context)
                if cdr_ids:
                    # Skip already inserted calls
                    already_created += 1
                    continue

                vals = {
                    'contract_id': contract_id,
                    'partner_id': partner_id,
                    'description': description,
                    'origin': origin,
                    'destination': destination,
                    'cost_price': cost_price,
                    'timestamp': timestamp,
                    'duration': duration,
                }
                self.create(tmp_cr, uid, vals)
                created += 1
                tmp_cr.commit()
            except Exception, e:
                tmp_cr.rollback()
                msg += _('\n* Error important trucada de %s a %s a les %s \n')
                msg %= (origin, destination, timestamp_str)
                if isinstance(e, osv.except_osv):
                    msg += e.value + '\n'
                else:
                    msg += str(e) + '\n'
                not_created += 1
            finally:
                tmp_cr.close()
        total = created + not_created + already_created
        res = _("El fitxer conté un total de %s trucades.\n") % total
        res += _(" - S'han importat %s trucades correctament. \n") % created
        res += _(" - Ja hi havia %s trucades importades. \n") % already_created
        res += _(" - Ha fallat la importació de %s trucades: \n") % not_created
        res += msg
        return res

    def import_cdr_from_airenetworks(self, cursor, uid, filename, file,
                                     partner_id, context=None):
        '''
        Import cdr data from aire file
        '''
        data_obj = self.pool.get('soller.operador.cdr.data')

        data = base64.decodestring(file).decode('iso8859-15')
        lines = data.split('\n')

        if 'datos' in filename:
            return data_obj.import_data_consumption(cursor, uid, lines,
                                                    partner_id, context)
        else:
            if 'voz-entrantes' in filename:
                type = 'incoming'
            elif '-sms-' in filename:
                type = 'sms'
            else:
                type = 'default'
            return self.import_call(cursor, uid, type, lines, partner_id,
                                    context)

    def import_call(self, cursor, uid, type, lines, partner_id,
                    context=None):
        contract_obj = self.pool.get('soller.operador.contrato')
        db = pooler.get_db_only(cursor.dbname)

        msg = ''
        created = 0
        not_created = 0
        already_created = 0

        key_list = []
        for line in lines:
            line = line.split(';')
            origin = ''
            destination = ''
            timestamp_str = ''

            if len(line) < 2:
                continue

            try:
                tmp_cr = db.cursor()
                timestamp_str = line[1]
                origin = line[2]
                destination = line[3]
                description = line[4]
                duration = int(line[5])
                cost_price = float(line[6])

                timestamp = datetime.strptime(timestamp_str,
                                              '%d/%m/%y %H:%M:%S')
                timestamp = timestamp.strftime('%Y-%m-%d %H:%M:%S')

                origin = self.sanitize_number_airenetworks(tmp_cr, uid, origin)
                destination = self.sanitize_number_airenetworks(tmp_cr, uid,
                                                                destination)

                if type == 'incoming':
                    phone_number = destination
                    if cost_price == 0.0:
                        continue
                else:
                    phone_number = origin

                if type == 'sms':
                    call_type = 'sms'
                else:
                    call_type = 'call'

                contract_id = contract_obj.get_contract_from_phone_date(tmp_cr,
                                           uid, phone_number, timestamp)

                search_params = [
                    ('contract_id', '=', contract_id),
                    ('partner_id', '=', partner_id),
                    ('origin', '=', origin),
                    ('destination', '=', destination),
                    ('timestamp', '=', timestamp),
                    ('duration', '=', duration),
                    ('cdr_number', '=', phone_number)
                ]
                cdr_ids = self.search(tmp_cr, uid, search_params,
                                      context=context)

                # Use a key because it's possible has differents lines with the same information. Ex: long sms sent in X parts
                key = "{}-{}-{}-{}-{}-{}-{}".format(contract_id, partner_id,
                                                    origin, destination,
                                                    timestamp, duration,
                                                    phone_number)

                if cdr_ids and not key in key_list:
                    # Skip already inserted calls
                    already_created += 1
                    continue

                vals = {
                    'contract_id': contract_id,
                    'partner_id': partner_id,
                    'description': description,
                    'origin': origin,
                    'destination': destination,
                    'cost_price': cost_price,
                    'timestamp': timestamp,
                    'duration': duration,
                    'cdr_number': phone_number,
                    'call_type': call_type
                }

                self.create(tmp_cr, uid, vals)
                created += 1
                tmp_cr.commit()
                key_list.append(key)

            except Exception, e:
                tmp_cr.rollback()
                msg += _('\n* Error important trucada de %s a %s a les %s \n')
                msg %= (origin, destination, timestamp_str)
                if isinstance(e, osv.except_osv):
                    msg += e.value + '\n'
                else:
                    msg += str(e) + '\n'
                not_created += 1

            finally:
                tmp_cr.close()


        total = created + not_created + already_created
        res = _("El fitxer conté un total de {} trucades.\n").format(total)
        res += _(" - S'han importat {} trucades correctament. \n"
                 ).format(created)
        res += _(" - Ja hi havia {} trucades importades. \n"
                 ).format(already_created)
        res += _(" - Ha fallat la importació de {} trucades: \n"
                 ).format(not_created)
        res += msg
        return res

    @job(queue='operador_facturacio')
    def import_cdr(self, cursor, uid, filename, file, partner_id, context=None):
        partner_obj = self.pool.get('res.partner')
        mail_obj = self.pool.get('poweremail.mailbox')

        partner = partner_obj.browse(cursor, uid, partner_id)

        logger = netsvc.Logger()
        msg = _(u"Inici importació del fitxer {} de CDRs de {}").format(
            filename,partner.name)
        logger.notifyChannel("objects", netsvc.LOG_INFO, msg)

        report = getattr(self, 'import_cdr_from_' + partner.ref)(cursor, uid,
                                                                 filename, file,
                                                                 partner_id)

        subject = _('Acabada importació del fitxer {} de CDRs de {}').format(
            filename, partner.name)
        email_from = config['email_from']

        mail_obj.send_mail_generic(cursor, uid, email_from, subject, report,
                                   context=context)

        logger.notifyChannel("objects", netsvc.LOG_INFO, subject)

    @cache()
    def call_price_get(self, cursor, uid, contract_id, product_id,
                       date, uom_id, context=None):

        contract_obj = self.pool.get('soller.operador.contrato')
        pricelist_obj = self.pool.get('product.pricelist')

        if not context:
            context = {}

        ctx = context.copy()
        ctx.update({'date': date,
                    'uom': uom_id,
                    })

        pricelist_id = contract_obj.read(cursor, uid, contract_id,
                            ['property_call_price'])['property_call_price'][0]

        return pricelist_obj.price_get(cursor, uid, [pricelist_id], product_id,
                                       1, context=ctx)[pricelist_id]

    @cache()
    def get_call_product(self, cursor, uid, categ_code, context=None):

        product_obj = self.pool.get('product.product')
        default_code = 'EXTLLAM{}'.format(categ_code)
        search_params = [('default_code', '=', default_code)]

        return product_obj.search(cursor, uid, search_params)[0]

    @cache()
    def get_nacional_product(self, cursor, uid, context=None):
        '''return nacional product'''
        product_obj = self.pool.get('product.product')
        search_params = [('default_code', '=', 'NAC')]

        product_id = product_obj.search(cursor, uid, search_params)[0]
        return product_obj.browse(cursor, uid, product_id)

    def compute_calls(self, cursor, uid, contract_id, base_product_id,
                      phone_number, date_from, date_to, categ_code, factor=1.0,
                      context=None):
        """ Fill the invoice_price field of calls. Factor means the
        " percentage of the limits avaliable to the user
        " (used for incomplete monthes)
        """
        product_obj = self.pool.get('product.product')
        config_obj = self.pool.get('res.config')

        price_margin = int(config_obj.get(cursor, uid,
                                          'operador_call_margin', '21'))
        # Price margin is a percentage
        price_margin = round(1 + (price_margin / 100.0), 4)

        if not context:
            context = {}

        text_limits = product_obj.read(cursor, uid, base_product_id,
                                       ['description'])['description']
        limits = eval(text_limits)
        # Limits come in minutes and we want seconds
        for key, value in limits.iteritems():
            limits[key] = int(value * 60 * factor)

        total_invoice_price = 0

        # From 00:00:00 of date_from to 23:59:59 of date_to
        datetime_from = date_from + ' 00:00:00'
        datetime_to = date_to + ' 23:59:59'
        search_params = [
                         ('contract_id', '=', contract_id),
                         ('cdr_number', '=', phone_number),
                         ('timestamp', '>=', datetime_from),
                         ('timestamp', '<=', datetime_to),
                         ]
        call_ids = self.search(cursor, uid, search_params, context=context)
        for call_id in call_ids:
            call = self.browse(cursor, uid, call_id)
            if categ_code == 'MOV':
                invoice_price = call.cost_price
            else:
                if call.kind.type in ('international', 'other'):
                    invoice_price = call.cost_price
                    if call.kind.type == 'international':
                        nac_product = self.get_nacional_product(cursor, uid,
                                                                context)
                        nac_price = self.call_price_get(cursor, uid,
                                                    contract_id,
                                                    nac_product.id,
                                                    date_to,
                                                    nac_product.uom_po_id.id,
                                                    context=context)
                        nac_total = round(nac_price * call.duration, 4)
                        int_total = round(call.cost_price * price_margin, 4)
                        if int_total < nac_total:
                            invoice_price = nac_total
                        else:
                            invoice_price = int_total
                else:
                    duration = call.duration
                    kind_type = call.kind.type
                    if kind_type in limits:
                        duration = 0
                        limits[kind_type] -= call.duration
                        if limits[kind_type] < 0:
                            duration = -limits[kind_type]
                            limits[kind_type] = 0

                    prod_kind_id = call.kind.product_id.id

                    price = self.call_price_get(cursor, uid, contract_id,
                                                prod_kind_id, date_to,
                                                call.kind.get_uos(), context)

                    invoice_price = round(price * duration, 4)
            total_invoice_price += invoice_price
            vals = {
                    'invoice_price': invoice_price,
                    }
            self.write(cursor, uid, call_id, vals)

        result = {
            'product_id': self.get_call_product(cursor,
                                                uid,
                                                categ_code=categ_code),
            'price': round(total_invoice_price, 2),
        }
        return result

    def _get_call_kind(self, cursor, uid, ids, name, arg, context=None):

        kind_obj = self.pool.get('soller.operador.call.kind')
        res = {}
        for id in ids:
            destination = self.read(cursor, uid, id,
                                    ['destination'])['destination']
            res[id] = kind_obj.call_kind(cursor, uid, destination,
                                         context=context)
        return res

    _columns = {
        'contract_id': fields.many2one('soller.operador.contrato',
                                       'Contracte', required=True,
                                       select=True),
        'partner_id': fields.many2one('res.partner', 'Proveïdor'),
        'description': fields.char('Descripció', size=128),
        'origin': fields.char('Origen', size=20, required=True, select=True,
                              help=u"Origen de la trucada (qui truca)"),
        'destination': fields.char('Destí', size=20,
                                   required=True,
                                help=u"Destí de la trucada (a qui es truca)"),
        'cost_price': fields.float('Preu de cost', digits=(16,4)),
        'invoice_price': fields.float('Preu de factura', digits=(16,4)),
        'timestamp': fields.datetime('Data', required=True),
        'duration': fields.integer('Durada', required=True),
        'kind': fields.function(_get_call_kind, type='many2one',
                                obj='soller.operador.call.kind',
                                method=True, string='Tipus',
                                readonly=True,
                                store=True),
        'cdr_number': fields.char('Número telèfon', size=20, required=True),
        'call_type': fields.selection(_soller_operador_call_type,
                                      string='Tipus cridada')
    }

    _defaults = {
        'call_type': lambda *a: 'call'
    }

SollerOperadorCdr()

class SollerOperadorCdrData(osv.osv):

    _name = 'soller.operador.cdr.data'

    def compute_data(self, cursor, uid, contract_id, phone_number,
                     date_from, date_to, context=None):
        """ Fill the invoice_price field of data.
        """
        cdr_obj = self.pool.get('soller.operador.cdr')

        total_invoice_price = 0

        # From 00:00:00 of date_from to 23:59:59 of date_to
        datetime_from = date_from + ' 00:00:00'
        datetime_to = date_to + ' 23:59:59'

        # Data consumption
        search_params = [
            ('contract_id', '=', contract_id),
            ('cdr_number', '=', phone_number),
            ('timestamp', '>=', datetime_from),
            ('timestamp', '<=', datetime_to),
        ]
        data_ids = self.search(cursor, uid, search_params, context=context)
        for data_id in data_ids:
            data = self.browse(cursor, uid, data_id)
            invoice_price = data.cost_price
            total_invoice_price += invoice_price
            vals = {
                'invoice_price': invoice_price,
            }
            self.write(cursor, uid, data_id, vals)

        result = {'product_id': self.get_data_product(cursor, uid),
                  'price': round(total_invoice_price, 2),
                  }
        return result

    def import_data_consumption(self, cursor, uid, lines, partner_id,
                                context=None):

        contract_obj = self.pool.get('soller.operador.contrato')
        linea_obj = self.pool.get('soller.operador.contrato.linea')
        cdr_obj = self.pool.get('soller.operador.cdr')
        db = pooler.get_db_only(cursor.dbname)

        msg = ''
        created = 0
        error = 0
        already_created = 0

        for line in lines:
            line = line.split(';')
            origin = ''
            timestamp_str = ''

            if len(line) < 2:
                continue

            try:
                tmp_cr = db.cursor()
                timestamp_str = line[0]
                origin = line[1]
                consumption = int(line[2]) / (1024 * 1024)  # Byte to MB
                cost_price = float(line[3])
                description = line[4]

                timestamp = datetime.strptime(timestamp_str,
                                              '%d/%m/%y %H:%M:%S')
                timestamp_2 = timestamp.strftime('%Y-%m-%d %H:%M:%S')

                origin = cdr_obj.sanitize_number_airenetworks(cursor, uid,
                                                              origin)

                contract_id = contract_obj.get_contract_from_phone_date(
                    cursor, uid, origin, timestamp.strftime('%Y-%m-%d'))

                # Check if it is already imported
                search_params = [
                    ('contract_id', '=', contract_id),
                    ('partner_id', '=', partner_id),
                    ('cdr_number', '=', origin),
                    ('timestamp', '=', timestamp_2),
                ]
                data_ids = self.search(tmp_cr, uid, search_params,
                                           context=context)

                if data_ids:
                    # Skip already inserted
                    already_created += 1
                    continue

                vals = {
                    'contract_id': contract_id,
                    'partner_id': partner_id,
                    'description': description,
                    'cdr_number': origin,
                    'cost_price': cost_price,
                    'timestamp': timestamp,
                    'consumption': consumption,
                }
                self.create(tmp_cr, uid, vals)
                created += 1
                tmp_cr.commit()

            except Exception, e:
                tmp_cr.rollback()
                msg += _('\n* Error important dades de %s a les %s \n')
                msg %= (origin, timestamp_str)
                if isinstance(e, osv.except_osv):
                    msg += e.value + '\n'
                else:
                    msg += str(e) + '\n'
                error += 1
            finally:
                tmp_cr.close()

        res = _(" - S'han importat {} consums de dades correctament. \n"
                ).format(created)
        res += _(" - Ja hi havia {} consums de dades importades. \n"
                 ).format(already_created)
        res += _(" - Ha fallat la importació de {} consums de dades: \n"
                 ).format(error)
        res += msg
        return res

    @cache()
    def get_data_product(self, cursor, uid, context=None):
        product_obj = self.pool.get('product.product')
        search_params = [('default_code', '=', 'DATA')]

        return product_obj.search(cursor, uid, search_params)[0]

    _columns = {
        'contract_id': fields.many2one('soller.operador.contrato',
                                       'Contracte', required=True),
        'partner_id': fields.many2one('res.partner', 'Proveïdor'),
        'cdr_number': fields.char('Número telèfon', size=20, required=True),
        'timestamp': fields.datetime('Data', required=True),
        'consumption': fields.integer('Consum (MB)', required=True),
        'cost_price': fields.float('Preu de cost', digits=(16,4)),
        'invoice_price': fields.float('Preu de factura', digits=(16, 4)),
        'description': fields.char('Descripció', size=128)
    }

SollerOperadorCdrData()
