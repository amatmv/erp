# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields


class WizardAssignUserCase(osv.osv_memory):

    _name = 'wizard.assign.user.case'

    def action_assign_user(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        case_obj = self.pool.get('crm.case')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        wizard = self.browse(cursor, uid, ids[0])
        user_id = wizard.user_id.id
        case_ids = context.get('active_ids', [])
        for case in case_obj.browse(cursor, uid, case_ids, context=context):
            #Search for active meter
            search_params = [('polissa', '=', case.polissa_id.id)]
            meter_ids = meter_obj.search(cursor, uid, search_params)
            if meter_ids:
                meter = meter_obj.browse(cursor, uid, meter_ids[0])
                #If it is tg connected, assign user
                if meter.tg_cnc_conn:
                    case.write({'user_id': user_id})
        return {}
                            
    _columns = {
        'user_id': fields.many2one('res.users', 'Responsable',
                                   required=True),
    }

WizardAssignUserCase()
