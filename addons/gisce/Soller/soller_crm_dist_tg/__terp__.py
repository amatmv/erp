# -*- coding: utf-8 -*-
{
    "name": "Extensió CRM Soller distribució",
    "description": """
Assignar ordres de tall a una persona segons si el comptador
esta telegestionat o no
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "crm_generic",
        "giscedata_polissa_crm",
        "jasper_reports",
        "giscedata_lectures_telegestio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_assign_user_view.xml"
    ],
    "active": False,
    "installable": True
}
