# -*- coding: utf-8 -*-
{
    "name": "",
    "description": """
    Mòdul per introduïr productes de manera massiva amb un lector de còdis
    """,
    "version": "0-dev",
    "author": "Bartomeu Miro Mateu",
    "category": "Stock",
    "depends":[
        "stock"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/stock_product_scan_view.xml"
    ],
    "active": False,
    "installable": True
}
