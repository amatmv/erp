# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class StockProductScanLine(osv.osv_memory):

    _name = 'stock.product.scan.line'

    _columns = {
        'barcode': fields.char('Codi de barres', size=32),
        'wizard_id': fields.many2one('stock.product.scan.wizard',
                                     required=True,
                                     )
        }

StockProductScanLine()


class StockProductScan(osv.osv_memory):

    _name = 'stock.product.scan.wizard'

    def action_create_moves(self, cursor, uid, ids, context=None):

        move_obj = self.pool.get('stock.move')
        lot_obj = self.pool.get('stock.production.lot')

        if not context:
            context = {}

        picking_id = context.get('active_ids', 0)[0]
        if not picking_id:
            raise osv.except_osv(_('Error!'), _('No picking defined '))

        wizard = self.browse(cursor, uid, ids[0])

        location_id = wizard.location_id.id
        location_dest_id = wizard.location_dest_id.id
        product_id = wizard.product_id.id

        for stock_line in wizard.product_lines:
            serial_number = stock_line.barcode

            search_params = [('name', '=', serial_number),
                             ('product_id', '=', product_id),
                             ]
            prodlot_ids = lot_obj.search(cursor, uid, search_params)
            if prodlot_ids:
                prodlot_id = prodlot_ids[0]
            else:
                # Create Production Lot
                vals = {'name': serial_number, 'product_id': product_id}
                prodlot_id = lot_obj.create(cursor, uid, vals)

            # Create Stock Move
            move_vals = {'location_id': location_id,
                         'location_dest_id': location_dest_id,
                         'product_id': product_id,
                         'prodlot_id': prodlot_id,
                         'picking_id': picking_id,
                         }

            move_prod_vals = move_obj.onchange_product_id(cursor, uid, [],
                                                          product_id)
            move_vals.update(move_prod_vals['value'])
            move_obj.create(cursor, uid, move_vals)

        return {}

    def onchange_ean13(self, cursor, uid, ids, ean13, context=None):

        product_obj = self.pool.get('product.product')
        res = {'value': {}, 'warning': {}, 'domain': {}}
 
        if not ean13:
            return res

        search_params = [('ean13', '=', ean13),
                         ]

        product_ids = product_obj.search(cursor, uid, search_params,
                                         context=context)

        if len(product_ids) == 1:
            res['value'].update({'product_id': product_ids[0]})
        elif len(product_ids) > 1:
            res['warning'].update({'title': _(u'Avís codi duplicat'),
                       'message': _(u"El codi ean13 introduït té "
                                    u"%s productes associats.") %
                                    len(product_ids)
                                  })

        return res

    _columns = {
        'product_id': fields.many2one('product.product', u'Producte',
                                      required=True),
        'location_id': fields.many2one('stock.location', u'Ubicació d\'origen',
                                       required=True),
        'location_dest_id': fields.many2one('stock.location', u'Ubicació destí',
                                            required=True),
        'product_lines': fields.one2many('stock.product.scan.line',
                                         'wizard_id',
                                         u'Codis de barres',
                                         ),
        'ean13': fields.char('Codi EAN13', size=32),
    }

StockProductScan()
