# -*- encoding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
from tools.translate import _

class WizardSollerProformaInvoice(osv.osv_memory):

    _name = 'wizard.proforma.invoice'

    def action_proforma_invoice(self, cursor, uid, ids, context=None):
        '''

        '''
        polissa_obj = self.pool.get('giscedata.polissa')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        line_obj = self.pool.get('wizard.proforma.invoice.line')
        l_pot_obj = self.pool.get('giscedata.lectures.potencia')

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])

        search_params = [('polissa', '=', wizard.policy_id.id),
                         ('data_alta', '<=', wizard.date_end),
                         '|', ('data_baixa', '=', None),
                         ('data_baixa', '>=', wizard.date_start)]

        meter_ids = meter_obj.search(cursor, uid, search_params)

        if len(meter_ids) > 1:
            error_msg = _(u'There are more than one meter in policy.')
            raise osv.except_osv(_(u'Error'), error_msg)

        ulf = polissa_obj.get_ultima_lectura_facturada(cursor, uid,
                                                       wizard.policy_id.id,
                                                       wizard.date_start,
                                                       wizard.journal_id.code,
                                                       context=context)

        read_fields = ['tarifa', 'facturacio_potencia']
        policy_vals = polissa_obj.read(cursor, uid, wizard.policy_id.id,
                                       read_fields)

        # Active and reactive reads
        read_date = False

        # If there are reads not create a new ones
        if self.is_created_reads(cursor, uid,  meter_ids[0],
                                 policy_vals['tarifa'][0],
                                 wizard.date_end, context=context):

            end_date = datetime.strptime(wizard.date_end, "%Y-%m-%d")
            wizard.write({
                'state': 'end',
                'remark': _(u"Reads for date {} already created").format(
                    end_date.strftime('%Y-%m-%d'))
            })
            return True

        for type in ['A', 'R']:

            read_dict_ulf = meter_obj.get_lectures(cursor, uid, meter_ids[0],
                                                   policy_vals['tarifa'][0],
                                                   ulf, type)

            for period in sorted(read_dict_ulf.keys()):
                read = read_dict_ulf[period]['actual']
                if 'name' in read:
                    read_date = read['name']
                    vals = {
                        'wizard_id': wizard.id,
                        'date': wizard.date_end,
                        'period_id': read['periode'][0],
                        'type': read['tipus'],
                        'meter_id': read['comptador'][0],
                        'actual_date': read_date,
                        'actual_read': read['lectura']
                    }
                    line_obj.create(cursor, uid, vals)

        # Maximeter
        if policy_vals['facturacio_potencia'] == 'max' and read_date:
            context.update({'active_test': False})
            search_params = [
                ('comptador', '=', meter_ids[0]),
                ('name', '=', read_date)
            ]
            max_ids = l_pot_obj.search(cursor, uid, search_params,
                                       context=context)

            for maxim in l_pot_obj.browse(cursor, uid, max_ids):
                vals = {
                    'wizard_id': wizard.id,
                    'date': wizard.date_end,
                    'period_id': maxim.periode.id,
                    'type': 'M',
                    'meter_id': maxim.comptador.id,
                    'actual_read': maxim['lectura']
                }
                line_obj.create(cursor, uid, vals)

        wizard.write({'state': 'end'})
        return True

    def is_created_reads(self, cursor, uid, meter_id, rate, read_date,
                         context=None):
        '''
            Check if there are active reads
        '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        read_dict = meter_obj.get_lectures(cursor, uid,meter_id,
                                           rate, read_date, 'A')

        return 'name' in read_dict['P1']['actual']

    def action_create_invoice(self, cursor, uid, ids, context=None):

        read_obj = self.pool.get('giscedata.lectures.lectura')
        pot_obj = self.pool.get('giscedata.lectures.potencia')
        origen_obj = self.pool.get('giscedata.lectures.origen')
        wizard_obj = self.pool.get('wizard.manual.invoice')
        invoice_obj = self.pool.get('giscedata.facturacio.factura')

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])

        search_params = [('codi', '=', '60')]
        origin_ids = origen_obj.search(cursor, uid, search_params)

        created_read_ids = []
        created_power_ids = []
        # Create reads
        for line in wizard.line_ids:
            if line.type in ['A', 'R']:
                vals = {
                    'name': line.date,
                    'comptador': line.meter_id.id,
                    'lectura': line.read_value,
                    'tipus': line.type,
                    'periode': line.period_id.id,
                    'origen_id': origin_ids[0]
                }
                created_read_ids.append(read_obj.create(cursor, uid, vals))
            else:
                vals = {
                    'name': line.date,
                    'comptador': line.meter_id.id,
                    'lectura': line.read_value,
                    'periode': line.period_id.id,
                    'origen_id': origin_ids[0]
                }
                created_power_ids.append(pot_obj.create(cursor, uid, vals))

        # Create invoice
        wizard_vals = {
            'date_start': wizard.date_start,
            'date_end': wizard.date_end,
            'date_invoice': wizard.date_invoice,
            'journal_id': wizard.journal_id.id,
            'polissa_id': wizard.policy_id.id,
        }
        wizard_id = wizard_obj.create(cursor, uid, wizard_vals, context=context)
        wizard_obj.action_manual_invoice(cursor, uid, [wizard_id],
                                         context=context)

        wizard_vals = wizard_obj.read(cursor, uid, wizard_id, ['invoice_ids'])

        invoice_obj.invoice_proforma2(cursor, uid,
                                      eval(wizard_vals[0]['invoice_ids']),
                                      context=context)
        # Delete created reads
        if created_read_ids:
            read_obj.unlink(cursor, uid, created_read_ids, context=context)
        if created_power_ids:
            pot_obj.unlink(cursor, uid, created_power_ids, context=context)

        # return {}
        invoice_ids = eval(wizard_vals[0]['invoice_ids'])
        vals_view = {
            'name': 'Facturas (Email)',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'limit': len(invoice_ids),
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % (invoice_ids),
        }
        return vals_view

    _columns = {
        'policy_id': fields.many2one('giscedata.polissa', 'Policy',
                                     required=True),
        'date_start': fields.date('Start date', required=True),
        'date_end': fields.date('End date', required=True),
        'date_invoice': fields.date('Invoice date'),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')],
                                  'State'),
        'journal_id': fields.many2one('account.journal', 'Journal',
                                      required=True),
        'remark': fields.text('Remarks', readonly=True)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'date_invoice': lambda *a: datetime.today().strftime('%Y-%m-%d'),
    }

WizardSollerProformaInvoice()

class WizardSollerProformaInvoiceLine(osv.osv_memory):

    _name = 'wizard.proforma.invoice.line'

    _columns = {
        'wizard_id': fields.many2one('wizard.proforma.invoice',
                                     required=True),
        'date': fields.date('Date', required=True),
        'actual_date': fields.date('Invoiced date', required=True,
                                   readonly=True),
        'period_id': fields.many2one('giscedata.polissa.tarifa.periodes',
                                     'Period', size=5, required=True,
                                     readonly=True),
        'type': fields.selection(
            [('A', 'Activa'), ('R', 'Reactiva'), ('M', 'Max')],
            'Type', required=True, readonly=True
        ),
        'meter_id':fields.many2one('giscedata.lectures.comptador',
                                   'Meter', required=True),
        'actual_read': fields.integer('Invoiced read', readonly=True),
        'read_value': fields.integer('Read', required=True),
    }

WizardSollerProformaInvoiceLine()

class WizardSollerProformaInvoice2(osv.osv_memory):
    _inherit = 'wizard.proforma.invoice'

    _columns = {
        'line_ids': fields.one2many('wizard.proforma.invoice.line', 'wizard_id',
                                    String='Lines'),
    }

WizardSollerProformaInvoice2()