# -*- encoding: utf-8 -*-

from osv import osv, fields

class WizardSollerManualInvoice(osv.osv_memory):

    _inherit = 'wizard.manual.invoice'

    def action_manual_invoice(self, cursor, uid, ids, context=None):
        '''
            Rewrite manual_invoice function adding ult_lectura_fact parameter
        '''
        polissa_obj = self.pool.get('giscedata.polissa')

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])

        ulf = polissa_obj.get_ultima_lectura_facturada(cursor, uid,
                                                       wizard.polissa_id.id,
                                                       wizard.date_start,
                                                       wizard.journal_id.code)
        context.update({'ult_lectura_fact': ulf})

        return super(WizardSollerManualInvoice, self
                     ).action_manual_invoice(cursor, uid, ids, context=context)

WizardSollerManualInvoice()
