# -*- coding: utf-8 -*-
{
  "name": "Soller invoicing",
  "description": """
    Extra functions for invoicing process
    """,
  "version": "0-dev",
  "author": "Toni Llado",
  "category": "Soller",
  "depends": ['giscedata_facturacio'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['wizard/wizard_proforma_invoice_view.xml', 'security/ir.model.access.csv'],
  "active": False,
  "installable": True
}
