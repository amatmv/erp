# -*- coding: utf-8 -*-

from osv import osv, fields, orm
from tools.translate import _
from datetime import datetime
import pooler
from oorq.decorators import job
import netsvc


class GiscedataPolissaModcontractualAsync(osv.osv):

    _name = 'polissa.modcontractual.async'
    _order = 'date_start desc, id desc'

    def clean_fields(self, cursor, uid, values, model, context=None):
        '''Clean values depending on origin model'''

        model_obj = self.pool.get(model)
        fields = model_obj.fields_get(cursor, uid)

        new_values = {}
        for field, value in values.iteritems():
            field_type = fields[field]['type']
            if field_type == 'many2one':
                new_values[field] = value and value[0] or False
            elif field_type == 'many2many':
                new_values[field] = [(6, 0, value)]
            elif field_type == 'one2many':
                continue
            else:
                new_values[field] = value

        return new_values

    def fill_fields(self, cursor, uid, values, model, context=None):
        '''Fill values depending on origin model'''

        model_obj = self.pool.get(model)
        fields = model_obj.fields_get(cursor, uid)

        new_values = {}
        for field, value in values.iteritems():
            field_type = fields[field]['type']
            if field_type == 'many2one':
                res_id = value
                if not res_id:
                    new_values[field] = '()'
                else:
                    rel_obj = self.pool.get(fields[field]['relation'])
                    rel_name = rel_obj.name_get(cursor, uid, [res_id],
                                                context=context)[0][1]
                    new_values[field] = rel_name
            elif field_type == 'many2many':
                if not value:
                    new_values[field] = value
                else:
                    rel_obj = self.pool.get(fields[field]['relation'])
                    rel_name = rel_obj.name_get(cursor, uid, value,
                                                context=context)
                    new_values[field] = rel_name
            elif field_type == 'one2many':
                continue
            elif field_type == 'selection':
                selection = fields[field]['selection']
                new_value = [val for key, val in selection if key == value][0]
                new_values[field] = new_value
            else:
                new_values[field] = value

        return new_values

    def get_own_fields(self, cursor, uid, context=None):
        '''Return list of fields that do not sync to polissa'''

        fields = self.fields_get_keys(cursor, uid)

        return [field for field in fields
                if self._columns[field].pol_rel == 'no']

    def get_write_fields(self, cursor, uid, context=None):
        """Return writable list of fields"""

        all_fields = self.fields_get(cursor, uid)

        write_fields = []
        map_fields = {}
        for field, values in all_fields.iteritems():
            if self._columns[field].pol_rel == 'yes':
                write_fields.append((field, values))

        return dict(write_fields)

    def get_write_values(self, cursor, uid, async_id, context=None):
        """Return a dict for writing to polissa"""

        values = self.get_vals_from_async(cursor, uid, async_id)
        changed_fields = self.get_changes(cursor, uid, async_id, context)
        changed_values = dict([(field, value)
                              for field, value in values.iteritems()
                              if field in changed_fields])
        return changed_values

    def get_vals_from_async(self, cursor, uid, async_id, context=None):
        """Return a async values in a clean dict"""

        write_fields = self.get_write_fields(cursor, uid)

        values = self.read(cursor, uid, [async_id], write_fields)[0]
        del values['id']

        clean_values = self.clean_fields(cursor, uid, values,
                                         self._name,
                                         context=context)
        return clean_values

    def get_vals_from_polissa(self, cursor, uid, polissa_id, context=None):
        """Return a dict for writing to async from associated polissa"""

        polissa_obj = self.pool.get('giscedata.polissa')

        write_fields = self.get_write_fields(cursor, uid)

        values = polissa_obj.read(cursor, uid, [polissa_id],
                                  write_fields)[0]
        del values['id']

        clean_values = self.clean_fields(cursor, uid, values,
                                         polissa_obj._name,
                                         context=context)
        return clean_values

    def get_changes(self, cursor, uid, async_id, context=None):
        """Get differences between async and polissa"""

        modasync = self.browse(cursor, uid, async_id)
        async_values = self.get_vals_from_async(cursor, uid, async_id,
                                                context=context)
        polissa = modasync.polissa_id
        polissa_values = self.get_vals_from_polissa(cursor, uid, polissa.id,
                                                    context=context)
        res = []
        for field, value in async_values.iteritems():
            if value != polissa_values[field]:
                res.append(field)
        return res

    def get_changes_str(self, cursor, uid, async_id, context=None):
        '''Get differences between async and polissa in str format'''

        if not context:
            context = {}

        write_fields = self.get_write_fields(cursor, uid, context)

        # Read fields from async
        async_values = self.get_write_values(cursor, uid, async_id,
                                             context=context)

        async_changes = self.fill_fields(cursor, uid, async_values,
                                         self._name,
                                         context=context)
        # Read fields from polissa
        # and filter those in change_fields
        modasync = self.browse(cursor, uid, async_id)
        polissa = modasync.polissa_id
        pol_values = self.get_vals_from_polissa(cursor, uid, polissa.id,
                                                context=context)
        pol_change_values = dict([(field, value)
                                  for field, value in pol_values.iteritems()
                                  if field in async_values.keys()])
        pol_changes = self.fill_fields(cursor, uid, pol_change_values,
                                       'giscedata.polissa', context)

        msg = ''
        for field, value in async_changes.iteritems():
            field_name = write_fields[field].get('string', field)
            tmp_msg = _(u'• {}:\n   old: {}\n   new: {}\n')
            msg += tmp_msg.format(field_name, pol_changes[field],
                                  async_changes[field])
        return msg

    def check_apply(self, cursor, uid, async_id, context=None):
        """Check if an async modcon can be applied"""

        modasync = self.browse(cursor, uid, async_id)
        polissa = modasync.polissa_id
        active_mod = polissa.modcontractual_activa

        # The contract to modify must be active
        if polissa.state != 'activa':
            msg = _('The contract {} is not in active state')
            raise osv.except_osv('Error', msg.format(polissa.name))

        if modasync.date_start < active_mod.data_inici:
            msg = _('You cannot apply this mod on date {}')
            date_start = datetime.strptime(modasync.date_start,
                                           '%Y-%m-%d').strftime('%d/%m/%Y')
            raise osv.except_osv('Error', msg.format(date_start))

        if not self.get_write_values(cursor, uid, async_id, context=context):
            msg = _('There are no changes to apply to {}')
            raise osv.except_osv('Error', msg.format(polissa.name))

        return True

    def polissa_browse(self, cursor, uid, async_id, context=None):
        """Return a polissa browse with changes from async_id
        """
        if not context:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')

        async = self.browse(cursor, uid, async_id, context=context)
        polissa = polissa_obj.browse(cursor, uid,
                                     async.polissa_id.id,
                                     context=context)

        fields = self.get_write_values(cursor, uid, async_id)
        if not fields:
            return polissa
        for field in fields.keys():
            value = getattr(async, field)
            setattr(polissa, field, value)

        return polissa

    def pending(self, cursor, uid, ids, context=None):

        # Only the ones in aproved or cancel state
        search_params = [('id', 'in', ids),
                         ('state', 'in', ('cancel', 'aproved'))]
        pending_ids = self.search(cursor, uid, search_params)

        write_vals = {
            'state': 'pending',
            'date_cancel': False,
            'cancel_user_id': False,
            'date_aproved': False,
            'aproved_user_id': False,
            'date_applied': False,
            'applied_user_id': False,
            'changes': False,
            'has_errors': False,
            'errors': False,
            'mod_id': False,
        }

        return self.write(cursor, uid, pending_ids, write_vals)

    def cancel(self, cursor, uid, ids, context=None):
        """Mark a mod as canceled"""

        # Check if we can cancel
        cancel_ids = []
        for modasync in self.browse(cursor, uid, ids):
            if modasync.state in ('pending', 'aproved'):
                cancel_ids.append(modasync.id)
            elif modasync.state == 'applied' and not modasync.mod_id:
                cancel_ids.append(modasync.id)

        write_vals = {
            'state': 'cancel',
            'date_cancel': datetime.now(),
            'cancel_user_id': uid,
        }

        return self.write(cursor, uid, cancel_ids, write_vals)

    def aprove(self, cursor, uid, ids, context=None):
        """Mark a mod as aproved"""

        common_vals = {
            'state': 'aproved',
            'aproved_user_id': uid,
            'date_aproved': datetime.now(),
        }

        for id in ids:
            changes = self.get_changes_str(cursor, uid, id, context)
            vals = common_vals.copy()
            vals.update({'changes': changes})
            self.write(cursor, uid, [id], vals)

        return True

    def apply(self, cursor, uid, async_id, context=None):
        """Apply changes to polissa and creates a new modcon"""

        polissa_obj = self.pool.get('giscedata.polissa')
        wizard_obj = self.pool.get('giscedata.polissa.crear.contracte')

        if isinstance(async_id, (list, tuple)):
            async_id = async_id[0]

        modasync = self.browse(cursor, uid, async_id,
                            context=context)

        db = pooler.get_db_only(cursor.dbname)
        error_vals = {}
        try:
            # Write changes
            tmp_cr = db.cursor()
            # Check if we can apply mod
            self.check_apply(tmp_cr, uid, async_id, context)
            polissa_obj.send_signal(tmp_cr, uid, [modasync.polissa_id.id],
                                    ['modcontractual'])
            write_values = self.get_write_values(tmp_cr, uid, async_id,
                                                 context=context)
            polissa_obj.write(tmp_cr, uid, [modasync.polissa_id.id], write_values)
            polissa = polissa_obj.browse(tmp_cr, uid, modasync.polissa_id.id)

            # Create new modcon using wizard
            action = 'nou'
            if polissa.modcontractual_activa.data_inici == modasync.date_start:
                action = 'modificar'
            wizard_vals = {
                'polissa_id': polissa.id,
                'data_inici': modasync.date_start,
                'data_final': polissa.modcontractual_activa.data_final,
                'accio': action
            }
            wizard_id = wizard_obj.create(tmp_cr, uid, wizard_vals,
                                          context={'active_id': polissa.id})
            wizard_obj.action_crear_contracte(tmp_cr, uid, [wizard_id])

            # Refresh polissa browse
            polissa = polissa_obj.browse(tmp_cr, uid, modasync.polissa_id.id)
            active_mod = polissa.modcontractual_activa

            # Update async status if everything has gone fine
            # also write mod where changes where applied
            async_vals = {
                'date_applied': datetime.now(),
                'applied_user_id': uid,
                'mod_id': polissa.modcontractual_activa.id,
                'state': 'applied',
                'has_errors': False,
                'errors': False
            }
            self.write(tmp_cr, uid, [async_id], async_vals)
            tmp_cr.commit()
            service_name = 'sync.{}'.format(id(tmp_cr))
            if netsvc.service_exist(service_name):
                netsvc.SERVICES[service_name].commit()
        except Exception, e:
            if isinstance(e, (orm.except_orm, osv.except_osv)):
                error_vals = {'has_errors': True,
                              'errors': e.value}
            else:
                error_vals = {'has_errors': True,
                              'errors': e}
            tmp_cr.rollback()
            service_name = 'sync.{}'.format(id(tmp_cr))
            if netsvc.service_exist(service_name):
                netsvc.SERVICES[service_name].rollback()
        finally:
            tmp_cr.close()
            service_name = 'sync.{}'.format(id(tmp_cr))
            if netsvc.service_exist(service_name):
                netsvc.SERVICES[service_name].close()
                del netsvc.SERVICES[service_name]

        if error_vals:
            self.write(cursor, uid, [async_id], error_vals)

        return True

    @job(queue='low')
    def apply_job(self, cursor, uid, async_id, context=None):
        '''Apply async inside a job'''

        return self.apply(cursor, uid, async_id, context)

    def apply_multi(self, cursor, uid, date=False, context=None):
        '''Apply all the aproved asyncs starting before date'''

        if not date:
            date = datetime.now().strftime('%Y-%m-%d')

        # Search for all aproved asyncs without errors
        search_params = [('state', '=', 'aproved'),
                         ('has_errors', '=', False),
                         ('date_start', '<=', date)]

        order = 'date_start, date_aproved'
        async_ids = self.search(cursor, uid, search_params,
                                order=order)

        for async_id in async_ids:
            self.apply_job(cursor, uid, async_id, context)

        return True

    def onchange_polissa(self, cursor, uid, ids, polissa_id, context=None):
        """Fill async with values from polissa when changing"""

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if polissa_id:
            values = self.get_vals_from_polissa(cursor, uid,
                                                polissa_id, context)
            res['value'].update(values)
        return res

    _states_selection = [
        ('pending', 'Pending'),
        ('aproved', 'Aproved'),
        ('applied', 'Applied'),
        ('cancel', 'Canceled')
    ]

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Contract',
                                      required=True, ondelete='restrict',
                                      select=True, pol_rel='no'),
        'cups': fields.related('polissa_id', 'cups', type='many2one',
                               relation='giscedata.cups.ps',
                               string='CUPS', readonly=True,
                               pol_rel='no'),
        'tarifa': fields.many2one('giscedata.polissa.tarifa','Tariff',
                                  pol_rel='yes'),
        'mod_id': fields.many2one('giscedata.polissa.modcontractual',
                                  'Associated mod', pol_rel='no',
                                  ondelete='set null',
                                  readonly=True),
        'state': fields.selection(_states_selection, 'Estat',
                                  required=True, readonly=True,
                                  pol_rel='no'),
        'date_start': fields.date('Start Date', required=True,
                                  pol_rel='no'),
        'date_aproved': fields.datetime('Date aproved', pol_rel='no',
                                        readonly=True),
        'aproved_user_id': fields.many2one('res.users', 'Aproved user',
                                           readonly=True, pol_rel='no'),
        'date_applied': fields.datetime('Date applied', readonly=True,
                                        pol_rel='no'),
        'applied_user_id': fields.many2one('res.users', 'Apply user',
                                           readonly=True, pol_rel='no'),
        'date_cancel': fields.datetime('Date cancel', readonly=True,
                                       pol_rel='no'),
        'cancel_user_id': fields.many2one('res.users', 'Cancel user',
                                          readonly=True, pol_rel='no'),
        'changes': fields.text('Changes', readonly=True, pol_rel='no'),
        'has_errors': fields.boolean('Errors detected', readonly=True,
                                     pol_rel='no'),
        'errors': fields.text('Errors', readonly=True, pol_rel='no'),
    }

    _defaults = {
        'state': lambda *a: 'pending',
        'has_errors': lambda *a: False,
    }

GiscedataPolissaModcontractualAsync()
