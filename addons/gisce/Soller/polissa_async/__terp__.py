# -*- coding: utf-8 -*-
{
    "name": "Polissa Modcontractual Async",
    "description": """Async modcontractual""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "polissa",
    "depends":[
        "report_polissa_comer_soller"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "async_view.xml",
        "schedule.xml",
        "polissa_async_report.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
