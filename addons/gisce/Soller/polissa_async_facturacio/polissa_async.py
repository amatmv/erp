# -*- coding: utf-8 -*-

from osv import osv, fields
from giscedata_facturacio.giscedata_polissa import NOTIFICACIO_SELECTION
from giscedata_facturacio_comer.giscedata_polissa import TIPUS_ENVIAMENT


class PolissaModcontractualAsync(osv.osv):

    _name = 'polissa.modcontractual.async'
    _inherit = 'polissa.modcontractual.async'

    def create_mandate(self, cursor, uid, ids, context=None):
        '''Create mandates for async associated polissa'''

        mandate_obj = self.pool.get('payment.mandate')

        for modasync in self.browse(cursor, uid, ids):
            polissa = modasync.polissa_id
            reference = 'giscedata.polissa,{}'.format(modasync.polissa_id.id)
            mandate_vals = {
                'reference': reference
            }
            mandate_id = mandate_obj.create(cursor, uid, mandate_vals)
            # Replace mandate values with the ones in async
            debtor_address = (modasync.direccio_pagament.
                              name_get(context=context)[0][1]).upper()
            debtor_contact = (modasync.direccio_pagament.name
                              and '%s, ' % modasync.direccio_pagament.name
                              or '').upper()
            if debtor_contact:
                debtor_address = debtor_address.replace(debtor_contact, '')
            debtor_state = (modasync.direccio_pagament.state_id
                            and modasync.direccio_pagament.state_id.name
                            or '').upper()
            debtor_country = (modasync.direccio_pagament.country_id
                              and modasync.direccio_pagament.country_id.name
                              or '').upper()
            notes = u"Contrato: %s\n" % polissa.name
            notes += u"CUPS: %s\n" % polissa.cups.name
            notes += u"Dirección de suministro: %s\n" % polissa.cups.direccio
            code_cnae = polissa.cnae and polissa.cnae.name or ''
            desc_cnae = polissa.cnae and polissa.cnae.descripcio or ''
            if code_cnae:
                notes += u"CNAE: ({0}) {1}\n".format(code_cnae, desc_cnae)

            vals = {
                'debtor_name': modasync.pagador.name,
                'debtor_vat': modasync.pagador.vat,
                'debtor_address': debtor_address,
                'debtor_state': debtor_state,
                'debtor_country': debtor_country,
                'debtor_iban': modasync.bank.iban,
                'notes': notes,
            }
            mandate_obj.write(cursor, uid, mandate_id, vals)

        return True

    def onchange_pagador(self, cursor, uid, ids, partner_id, context=None):
        """Change domain fields when changing pagador"""

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not partner_id:
            partner_id = -1
        domain = [('partner_id', '=', partner_id)]
        res['domain'].update({'direccio_pagament': domain,
                              'bank': domain})
        res['value'].update({'direccio_pagament': False,
                             'bank': False})
        return res

    def onchange_notificacio(self, cursor, uid, ids, partner_id, context=None):
        """Change domain fields when changing altre_p"""

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not partner_id:
            partner_id = -1
        domain = [('partner_id', '=', partner_id)]
        res['domain'].update({'direccio_notificacio': domain})
        res['value'].update({'direccio_notificacio': False})
        return res

    def get_vals_from_polissa(self, cursor, uid, polissa_id, context=None):

        res = super(PolissaModcontractualAsync,
                    self).get_vals_from_polissa(cursor, uid, polissa_id,
                                                context=context)
        if ('notificacio' in res and
            res['notificacio'] == 'pagador' and
            'pagador' in res and
            'altre_p' in res and
            not res['altre_p']):
            res['altre_p'] = res['pagador']

        return res

    def _pagador_selection(self, cursor, uid, context=None):

        polissa_obj = self.pool.get('giscedata.polissa')

        return polissa_obj._pagador_selection

    _columns = {
        'pagador_sel': fields.selection(_pagador_selection, 'Payer',
                                        size=64, pol_rel='yes'),
        'pagador': fields.many2one('res.partner', 'Invoice partner',
                                   required=True,
                                   pol_rel='yes'),
        'pagador_nif': fields.related('pagador', 'vat', type='char',
                                      string='Invoice VAT', readonly=True,
                                      pol_rel='no'),
        'direccio_pagament': fields.many2one('res.partner.address',
                                             u'Invoice address',
                                             required=True,
                                             pol_rel='yes',
                                             ),
        'pagador_email': fields.related('direccio_pagament', 'email',
                                        type='char', size=240,
                                        string='E-mail', readonly=True,
                                        pol_rel='no'),
        'notificacio': fields.selection(NOTIFICACIO_SELECTION,
                                        u'Notify',
                                        pol_rel='yes'),
        'altre_p': fields.many2one('res.partner', 'Notify partner',
                                   required=True,
                                   pol_rel='yes'),
        'direccio_notificacio': fields.many2one('res.partner.address',
                                                u'Notify address',
                                                required=True,
                                                pol_rel='yes',
                                                ),
        'tipo_pago': fields.many2one('payment.type', 'Payment type',
                                     required=True, pol_rel='yes'),
        'bank': fields.many2one('res.partner.bank', 'Bank',
                                pol_rel='yes'),
        'payment_mode_id': fields.many2one('payment.mode', 'Payment mode',
                                           pol_rel='yes'),
        'llista_preu': fields.many2one('product.pricelist', 'Pricelist',
                                       domain=[('type', '=', 'sale')],
                                       pol_rel='yes', required=True),
        'enviament': fields.selection(TIPUS_ENVIAMENT, 'Send invoice',
                                      pol_rel='yes'),
    }

PolissaModcontractualAsync()
