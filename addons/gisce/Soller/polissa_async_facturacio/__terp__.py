# -*- coding: utf-8 -*-
{
    "name": "Polissa Modcontractual Async Invoicing",
    "description": """Async modcontractual""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "polissa",
    "depends":[
        "polissa_async",
        "giscedata_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "async_view.xml"
    ],
    "active": False,
    "installable": True
}
