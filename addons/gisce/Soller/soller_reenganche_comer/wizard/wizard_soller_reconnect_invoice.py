# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
from datetime import datetime


class WizardSollerReconnectInvoice(osv.osv_memory):

    _name = 'wizard.soller.reconnect.invoice'

    def get_product_taxes(self, cursor, uid, product_id, context=None):
        '''Retorna un diccionari amb els impostos associats a un producte'''

        product_obj = self.pool.get('product.product')

        if isinstance(product_id, (list, tuple)):
            product_id = product_id[0]

        product = product_obj.browse(cursor, uid, product_id)
        return [tax.id for tax in product.taxes_id]

    def action_generate(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])

        polissa_obj = self.pool.get('giscedata.polissa')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura_linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        line_obj = self.pool.get('account.invoice.line')
        journal_obj = self.pool.get('account.journal')
        product_obj = self.pool.get('product.product')
        payment_obj = self.pool.get('payment.type')

        # This invoices are energy journal
        search_params = [('code', '=', 'ENERGIA')]
        journal_id = journal_obj.search(cursor, uid, search_params)[0]

        # Payment form CAJA
        search_params = [('code', '=', 'CAJA')]
        payment_type_id = payment_obj.search(cursor, uid, search_params)[0]

        polissa_id = wizard.policy_id.id
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context=context)
        # generate invoice
        vals = factura_obj.onchange_polissa(cursor, uid, [],
                                            polissa_id, 'out_invoice',
                                            context={'date': wizard.date}
                                            )['value']

        vals.update({
            'date_invoice': wizard.date,
            'date_due': wizard.date,
            'polissa_id': polissa_id,
            'type': 'out_invoice',
            'journal_id': journal_id,
            'date_boe': facturador_obj.get_data_boe(cursor, uid, wizard.date),
            'data_inici': wizard.date,
            'data_final': wizard.date,
            'tipo_factura': '04', # Derechos de contratación
            'payment_type': payment_type_id,
            'name': 'Reenganche',
        })

        factura_id = factura_obj.create(cursor, uid, vals, context)

        # Search for product
        search_params = [('default_code', '=', 'RECON')]
        product_id = product_obj.search(cursor, uid, search_params)[0]
        product = product_obj.browse(cursor, uid, product_id)

        # Afegim la linia de la factura amb el reenganche
        pagador = polissa.pagador
        fpos = pagador.property_account_position

        linia_vals = line_obj.product_id_change(cursor, uid, [],
                                                product.id,
                                                product.uom_id.id,
                                                1,
                                                partner_id=pagador.id,
                                                fposition_id=fpos.id)['value']
        linia_vals.update({
            'factura_id': factura_id,
            'product_id': product_id,
            'data_desde': wizard.date,
            'data_fins': wizard.date,
            'quantity': 1,
            'tipus': 'altres'
        })
        linia_vals['price_unit_multi'] = product.list_price
        tax_ids = [(6, 0, self.get_product_taxes(cursor, uid, product.id))]
        linia_vals['invoice_line_tax_id'] = tax_ids
        linia_vals['name'] = product.product_tmpl_id.name
        factura_linia_obj.create(cursor, uid, linia_vals, context)
        # Despues de crear les linies hem de cridar
        # la funcio per calcular els impostos
        factura_obj.button_reset_taxes(cursor, uid, [factura_id])

        return {
            'name': 'Facturas reenganche',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', '=', {})]".format(factura_id),
        }

    _columns = {
        'policy_id': fields.many2one('giscedata.polissa', 'Policy',
                                     required=True),
        'date': fields.date('Date', required=True),
    }

    _defaults = {
        'date': lambda *a: datetime.now().strftime('%Y-%m-%d'),
    }

WizardSollerReconnectInvoice()
