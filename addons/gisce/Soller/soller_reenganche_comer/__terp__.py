# -*- coding: utf-8 -*-
{
  "name": "Reconnections invoicing comer",
  "description": """
Quick invoices:
    * Reconnect invoicing after a cut off
    """,
  "version": "0-dev",
  "author": "Toni Lladó",
  "category": "Soller",
  "depends": ['giscedata_facturacio', 'giscedata_polissa'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
    'wizard/wizard_soller_reconnect_invoice_view.xml',
    'soller_reconnect_comer_data.xml'
  ],
  "active": False,
  "installable": True
}
