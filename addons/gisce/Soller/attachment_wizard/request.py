# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class RequestLinkAction(osv.osv):

    _inherit = 'res.request.link'

    def create_access_reference(self, cursor, uid, ids, context=None):
        '''create action and value to access attachment wizard
        directly from referenced objects'''

        action_obj = self.pool.get('ir.actions.act_window')
        value_obj = self.pool.get('ir.values')

        for id in ids:
            link = self.browse(cursor, uid, id)
            src_model = link.object
            # Action vals. Create if not present
            if link.ref_ir_act_window_access:
                action_id = link.ref_ir_act_window_access.id
            else:
                action_context_model = "'model_name': '{}', ".format(src_model)
                action_context = '{' + action_context_model + "'model_id': active_id}"
                action_vals = {
                    'name': _("Add attachment"),
                    'type': 'ir.actions.act_window',
                    'res_model': 'wizard.model.add.attachment',
                    'src_model': src_model,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'new',
                    'context': action_context
                }
                action_id = action_obj.create(cursor, uid, action_vals)
            if link.ref_ir_value_access:
                value_id = link.ref_ir_value_access.id
            else:
                value_vals = {
                    'name': _("Add attachment"),
                    'model': src_model,
                    'key': 'action',
                    'key2': 'client_action_relate',
                    'value': "ir.actions.act_window,%s" % action_id,
                    'object': True,
                }
                value_id = value_obj.create(cursor, uid, value_vals)

            link.write({'ref_ir_act_window_access': action_id,
                        'ref_ir_value_access': value_id})

        return True

    def delete_access_reference(self, cursor, uid, ids, context=None):
        '''delete action and value to access attachment wizard
        directly from referenced objects'''

        action_obj = self.pool.get('ir.actions.act_window')
        value_obj = self.pool.get('ir.values')

        for id in ids:
            link = self.browse(cursor, uid, id)
            if link.ref_ir_act_window_access:
                action_id = link.ref_ir_act_window_access.id
                action_obj.unlink(cursor, uid, action_id)

            if link.ref_ir_value_access:
                value_id = link.ref_ir_value_access.id
                value_obj.unlink(cursor, uid, value_id)

        return True

    def write(self, cursor, uid, ids, vals, context=None):

        res = super(RequestLinkAction,
                    self).write(cursor, uid, ids, vals, context=context)

        if 'attachment_action' not in vals:
            return res

        if vals['attachment_action']:
            self.create_access_reference(cursor, uid, ids, context)
        else:
            self.delete_access_reference(cursor, uid, ids, context)

        return True

    _columns = {
        'attachment_action': fields.boolean('Attachment action'),
        'ref_ir_act_window_access': fields.many2one('ir.actions.act_window',
                                                    'Window Action',
                                                    readonly=True),
        'ref_ir_value_access': fields.many2one('ir.values',
                                               'Model link',
                                               readonly=True),
    }

    _defaults = {
        'attachment_action': lambda *a: False,
    }

RequestLinkAction()
