# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardModelAddAttachment(osv.osv_memory):

    _name = 'wizard.model.add.attachment'

    def add_attachment(self, cursor, uid, ids, context=None):

        attach_obj = self.pool.get('ir.attachment')

        if not context:
            context = {}
        model_name = context.get('model_name', '') 
        model_id = context.get('model_id', 0) 

        if not model_name or not model_id:
            raise osv.except_osv('Error',
                                 _(u"No related model found"))

        wizard = self.browse(cursor, uid, ids[0])

        vals = {
            'name': wizard.attachment_name,
            'datas_fname': wizard.filename,
            'res_id': model_id,
            'res_model': model_name,
            'category_id': wizard.category_id.id,
            'datas': wizard.file,
        }

        attach_obj.create(cursor, uid, vals)

        return {}

    def _links_get(self, cursor, uid, context=None):

        link_obj = self.pool.get('res.request.link')
        search_params = [('attachment_action', '=', True)]
        link_vals = link_obj.search_reader(cursor, uid, search_params,
                                           ['object', 'name'],
                                           context=context)
        return [(r['object'], r['name']) for r in link_vals]

    def _get_default_reference(self, cursor, uid, context=None):

        if not context:
            context = {}

        model_name = context.get('model_name', '') 
        model_id = context.get('model_id', 0) 
        if model_name and model_id:
            return '{},{}'.format(model_name, model_id)
        else:
            return ''

    _columns = {
        'file': fields.binary('File', required=True),
        'filename': fields.char('Filename', size=100),
        'attachment_name': fields.char('Attachment name', size=100,
                                       required=True),
        'category_id': fields.many2one('ir.attachment.category',
                                       'Category'),
        'reference': fields.reference('Reference', selection=_links_get,
                                      readonly=True,
                                      size=128),        
    }

    _defaults = {
        'reference': _get_default_reference,
    }

WizardModelAddAttachment()
