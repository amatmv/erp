# -*- coding: utf-8 -*-
{
    "name": "Attachment wizard",
    "description": """Add attachments to any model""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "base",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "request_view.xml",
        "wizard/wizard_add_attachment_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
