# -*- coding: utf-8 -*-
{
    "name": "Integració WWW",
    "description": """
Mòdul per la integració de l'oficina virtual
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "www",
    "depends":[
        "base",
        "giscedata_polissa",
        "account",
        "soller_operador"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "www_partner_view.xml",
        "www_facturacio_view.xml",
        "www_scheduler.xml",
        "www_partner_data.xml"
    ],
    "active": False,
    "installable": True
}
