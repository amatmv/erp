# -*- coding: utf-8 -*-

from osv import osv, fields


class WWWAccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'
    
    _columns = {
        'www_publish': fields.boolean('WWW Publish'),
    }
    
    _defaults = {
        'www_publish':  lambda *a: True,
    }


WWWAccountInvoice()