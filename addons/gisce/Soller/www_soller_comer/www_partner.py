# -*- coding: utf-8 -*-

from osv import osv, fields
import netsvc
from datetime import datetime, timedelta
import pooler
from tools.translate import _
from addons import get_module_resource

class WWWPartner(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    def process_facturae(self, cursor, uid, email_from, email_to,
                         context=None):
        '''Change all polisses related to partner in ids
        where partner pays the invoice to facturae'''

        wf_service = netsvc.LocalService('workflow')
        polissa_obj = self.pool.get('giscedata.polissa')
        wizard_obj = self.pool.get('giscedata.polissa.crear.contracte')
        mail_obj = self.pool.get('poweremail.mailbox')
        acc_obj = self.pool.get('poweremail.core_accounts')
        user_obj = self.pool.get('res.users')
        conf_obj = self.pool.get('res.config')

        if not context:
            context = {}

        if 'lang' not in context:
            user = user_obj.browse(cursor, uid, uid)
            lang = user.context_lang
            company = user.company_id.name
            context.update({'lang': lang})

        db = pooler.get_db_only(cursor.dbname)

        # Activate check facturae
        if int(conf_obj.get(cursor, uid, 'check_facturae', '0')):
            self.activate_check_facturae(cursor, uid, context=context)

        # Search all partners with facturae. This way we will
        # check all the associated contracts for possible changes
        search_params = [('www_facturae', '=', True)]
        partner_ids = self.search(cursor, uid, search_params)
        
        # Search for deactivations
        days_back = int(context.get('days_back', 1))
        from_data = datetime.strftime((datetime.now() -
                                       timedelta(days=days_back)),
                                  '%Y-%m-%d 00:00:00')

        search_params = [('www_data_facturae', '>=', from_data),
                         ('www_facturae', '=', False)]
        deactivated_partner_ids = self.search(cursor, uid, search_params)
        partner_ids = list(set(partner_ids) | set(deactivated_partner_ids))

        mes = ''
        for partner_id in partner_ids:
            partner = self.browse(cursor, uid, partner_id)
            if partner.www_facturae:
                action = _(u"Activació")
            else:
                action = _(u"Desactivació")
            #Search for active polissa where partner pays the invoice
            search_params = [('pagador', '=', partner_id),
                             ('state', 'not in', ('esborrany', 'validar',
                                                  'baixa', 'modcontractual'))]
            polissa_ids = polissa_obj.search(cursor, uid, search_params)

            for polissa_id in polissa_ids:
                try:
                    tmp_cr = db.cursor()
                    polissa = polissa_obj.browse(tmp_cr, uid, polissa_id)
                    vals = {}
                    if partner.www_facturae:
                        vals.update({'enviament': 'email'})
                    else:
                        vals.update({'enviament': 'postal+email'})
                    if not polissa.direccio_notificacio.email:
                        dir_pago = polissa.direccio_pagament
                        dir_pago.write({'email': partner.www_email})
                    if polissa.enviament != vals['enviament']:
                        wf_service.trg_validate(uid, 'giscedata.polissa',
                                polissa_id, 'modcontractual', tmp_cr)
                        polissa.write(vals)
                        #Call wizard for modcontractual
                        wizard_vals = {
                            'polissa_id': polissa_id,
                            'accio': 'modificar',
                        }
                        wizard = wizard_obj.create(tmp_cr, uid, wizard_vals,
                                            context={'active_id': polissa_id})
                        wizard_obj.action_crear_contracte(tmp_cr,
                                                          uid, [wizard])
                        mes += (_(u"%s de factura electrònica pel "
                                  u"client %s (ID: %s). Contracte %s\n")
                                  % (action, partner.name,
                                     partner_id, polissa.name))
                    tmp_cr.commit()
                except Exception, e:
                    mes += (_(u"Error processant el contracte ID: %s "
                              u"del partner %s (ID: %s)\n")
                            % (polissa_id, partner.name, partner_id))
                    tmp_cr.rollback()
                finally:
                    tmp_cr.close()

        #Send email with result
        # Search for the account = email_from
        search_params = [('email_id', '=', email_from)]
        acc_id = acc_obj.search(cursor, uid, search_params)[0]
        subject = _(u"[Facturae] Notificacions") + " " + company
        if mes:
            vals = {
                'pem_from': email_from,
                'pem_to': email_to,
                'pem_subject': subject,
                'pem_body_text': mes,
                'pem_account_id': acc_id,
            }
            mail_id = mail_obj.create(cursor, uid, vals, context)
            mail_obj.send_this_mail(cursor, uid, [mail_id], context)

        return True

    def activate_check_facturae(self, cursor, uid, context=None):
        '''
            Activate check facturae in partners with an active operator contract
        '''
        contract_obj = self.pool.get('soller.operador.contrato')

        query_file = get_module_resource('www_soller_comer',
                                         'sql',
                                         'query_partner_facturae.sql')
        query = open(query_file).read()
        cursor.execute(query)

        payer_ids = [x[0] for x in cursor.fetchall()]

        for payer_id in payer_ids:
            # Get older contract
            search_params = [('pagador_id', '=', payer_id),
                             ('state', 'not in', ('esborrany', 'validar',
                                                  'cancelat'))]
            contract_ids = contract_obj.search(cursor, uid, search_params,
                                               order='data_alta', limit=1)

            older_contract = contract_obj.browse(cursor, uid,
                                                 contract_ids[0])
            vals = {
                'www_facturae': True,
                'www_data_facturae': older_contract.data_alta,
            }

            partner_vals = self.read(cursor, uid, payer_id, ['www_email'])
            if not partner_vals['www_email']:
                vals.update(
                    {'www_email': older_contract.direccion_factura_id.email}
                )

            self.write(cursor, uid, payer_id, vals)

    def b_activate_facturae(self, cursor, uid, ids, context=None):
        '''
            Activate or deactivate facturae in partner
        '''
        for partner_id in ids:
            partner_vals = self.read(cursor, uid, partner_id, ['www_facturae'])
            vals = {
                'www_facturae': not(partner_vals.get('www_facturae',
                                                     False)),
                'www_data_facturae': datetime.now(),
            }

            self.write(cursor, uid, partner_id, vals)
            return True

    _columns = {
        'www_facturae': fields.boolean('Facturae', readonly=True),
        'www_data_facturae': fields.datetime('Data Facturae',
                        readonly=True,
                        help="Data de modificació de factura electrònica"),
        'www_email': fields.char('Email', size=100, readonly=True),
    }

WWWPartner()
