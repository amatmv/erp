SELECT
  pagador_id
FROM
  soller_operador_contrato c
INNER JOIN res_partner p
  ON c.pagador_id = p.id
WHERE
  c.state NOT IN ('esborrany', 'validar', 'cancelat')
  AND not p.www_facturae
  AND (coalesce(p.www_data_facturae, '1900-01-01') = '1900-01-01' OR p.www_data_facturae < c.data_alta)
  AND p.active
  AND c.active