# -*- coding: utf-8 -*-
{
    "name": "Integració facturació WWW",
    "description": """
Mòdul per la integració de les factures a l'oficina virtual
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "www",
    "depends":[
        "base",
        "giscedata_facturacio_comer",
        "continuous_invoice"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "www_facturacio_view.xml"
    ],
    "active": False,
    "installable": True
}
