# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* www_soller_facturacio_comer
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2014-03-11 08:17:08+0000\n"
"PO-Revision-Date: 2014-03-11 08:17:08+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: www_soller_facturacio_comer
#: view:giscedata.facturacio.lot:0
msgid "Accions WWW"
msgstr "Accions WWW"

#. module: www_soller_facturacio_comer
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "XML invàlid per a l'estructura de la vista!"

#. module: www_soller_facturacio_comer
#: model:ir.module.module,description:www_soller_facturacio_comer.module_meta_information
msgid "\n"
"Mòdul per la integració de les factures a l'oficina virtual\n"
"    "
msgstr "\n"
"Mòdul per la integració de les factures a l'oficina virtual\n"
"    "

#. module: www_soller_facturacio_comer
#: view:giscedata.facturacio.lot:0
msgid "Publicar"
msgstr "Publicar"

#. module: www_soller_facturacio_comer
#: model:ir.module.module,shortdesc:www_soller_facturacio_comer.module_meta_information
msgid "Integració facturació WWW"
msgstr "Integració facturació WWW"

