# -*- coding: utf-8 -*-

from osv import osv, fields
from oorq.decorators import job


class WWWGiscedataFacturacioLot(osv.osv):
    _name = 'giscedata.facturacio.lot'
    _inherit = 'giscedata.facturacio.lot'
    
    def action_publicar_factures_web(self, cursor, uid, ids, context=None):
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        
        if not context:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        
        search_params = [('lot_facturacio', 'in', ids),
                         ('www_publish', '=', False),
                        ]
        
        factura_ids = factura_obj.search(cursor, uid,
                                         search_params,
                                         context=context)
        for fact_id in factura_ids:
            factura_obj.action_publicar_factures_web_background(cursor, uid,
                                                                [fact_id])
        return True

WWWGiscedataFacturacioLot()

class WWWGiscedataFacturacioFactura(osv.osv):
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'
    
    @job(queue='www')
    def action_publicar_factures_web_background(self, cursor, uid, ids,
                                                context=None):
        return self.write(cursor, uid, ids, {'www_publish': True})

    def create(self, cursor, uid, vals, context=None):
        '''Set www_publish to False when creating'''

        if not 'www_publish' in vals:
            vals['www_publish'] = False
        return super(WWWGiscedataFacturacioFactura,
                     self).create(cursor, uid, vals, context=context) 

WWWGiscedataFacturacioFactura()


class WWWContinuousInvoicingLot(osv.osv):
    _name = 'continuous.invoicing.lot'
    _inherit = 'continuous.invoicing.lot'

    def action_publicar_factures_continues_web(self, cursor, uid, ids, context=None):
        factura_obj = self.pool.get('giscedata.facturacio.factura')

        if not context:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        search_params = [('continuous_lot_id', 'in', ids),
                         ('www_publish', '=', False),
                         ]

        factura_ids = factura_obj.search(cursor, uid,
                                         search_params,
                                         context=context)
        for fact_id in factura_ids:
            factura_obj.action_publicar_factures_web_background(cursor, uid,
                                                                [fact_id])
        return True


WWWContinuousInvoicingLot()
        
        