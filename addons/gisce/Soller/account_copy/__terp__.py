# -*- coding: utf-8 -*-
{
    "name": "Account copy",
    "description": """Add helper to copy an account""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "account",
    "depends":[
        "account"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_account_copy_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
