# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardAccountCopy(osv.osv_memory):

    _name = 'wizard.account.copy'

    def action_copy_account(self, cursor, uid, ids, context=None):

        account_obj = self.pool.get('account.account')

        if context is None:
            context = {}

        account_ids = context.get('active_ids', [])
        wizard = self.browse(cursor, uid, ids[0])

        for account in account_obj.browse(cursor, uid, account_ids):
            if len(wizard.code) != len(account.code):
                msg = _(u"Incorrect account length. "
                        u"It should be {}")
                raise osv.except_osv('Error',
                                     msg.format(len(account.code)))
            elif not wizard.code.startswith(account.parent_id.code):
                msg = _("New account does not match group {}")
                raise osv.except_osv('Error',
                                     msg.format(account.parent_id.code))

            vals = {
                'name': wizard.name,
                'code': wizard.code
            }
            new_account_id = account_obj.copy(cursor, uid, account.id)
            account_obj.write(cursor, uid, [new_account_id], vals)

        return {}

    _columns = {
        'name': fields.char('Name', size=128, required=True),
        'code': fields.char('Code', size=64, required=True),
    }

WizardAccountCopy()
