SELECT
  count(l.id),
  l.enviat,
  coalesce(lang.name, '*'),
  CASE
    WHEN coalesce(l.email, '') = '' THEN false
    ELSE true
  END
FROM soller_descarrec_comer_linia l
INNER JOIN giscedata_polissa pol
  ON pol.id = l.polissa_id
INNER JOIN res_partner p
  ON p.id = pol.pagador
LEFT JOIN res_lang lang
  ON lang.code = p.lang
WHERE
  descarrec_id = %s
GROUP BY
  coalesce(lang.name, '*'),
  l.enviat,
  CASE
    WHEN coalesce(l.email, '') = '' THEN false
    ELSE true
  END
ORDER BY
  coalesce(lang.name, '*'),
  l.enviat