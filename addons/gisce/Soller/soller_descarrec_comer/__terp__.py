# -*- coding: utf-8 -*-
{
    "name": "Soller descàrrecs comer",
    "description": """
Cut offs administration in comer
    """,
    "version": "0-dev",
    "author": "Toni Lladó",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "poweremail"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/soller_descarrec_security.xml",
        "soller_descarrec_view.xml",
        "wizard/wizard_carregar_descarrec_view.xml",
        "wizard/wizard_enviar_descarrec_view.xml",
        "soller_descarrec_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
