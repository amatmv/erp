# -*- coding: utf-8 -*-


from osv import osv
from osv import fields
from tools.translate import _
from tools import config
import tools
from oorq.decorators import job


class WizardEnviarDescarrec(osv.osv_memory):
    _name = 'wizard.enviar.descarrec'

    def action_without_mail(self, cursor, uid, ids, context=None):
        desc_line_obj = self.pool.get('soller.descarrec.comer.linia')
        desc_id = context.get('active_id', False)
        search_params = [('descarrec_id', '=',  desc_id), ('email', '=', None)]
        desc_lin_ids = desc_line_obj.search(cursor, uid, search_params)

        return {
            'name': 'Cut offs lines',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'soller.descarrec.comer.linia',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', desc_lin_ids)],
        }

    def action_send_mail(self, cursor, uid, ids, context=None):
        desc_line_obj = self.pool.get('soller.descarrec.comer.linia')
        ir_mod_dat = self.pool.get('ir.model.data')

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])


        desc_id = context.get('active_id', False)
        search_params = [('descarrec_id', '=', desc_id), ('email', '<>', None)]
        desc_lin_ids = desc_line_obj.search(cursor, uid, search_params)

        if not desc_lin_ids:
            msg = _("Can't found anything cut off to send by email")
            wizard.write({'state': 'err', 'info': msg})
            return True

        tmpl = ir_mod_dat._get_obj(cursor, uid,
                                   'soller_descarrec_comer',
                                   'send_cut_off_by_email')

        ctx = context.copy()

        ctx.update({
            'state': 'single',
            'priority': '0',
            'from': tmpl.enforce_from_account.id,
            'template_id': tmpl.id,
            'src_model': 'soller.descarrec.comer.linia',
        })

        for desc_lin_id in desc_lin_ids:
            ctx.update({
                'src_rec_ids': [desc_lin_id],
                'active_ids': [desc_lin_id],
            })
            self.action_send_cut_off_by_mail_background(cursor, uid, ids, ctx)

        return {'type': 'ir.actions.act_window_close'}

    @job(queue=config.get('poweremail_render_queue', 'poweremail'))
    def action_send_cut_off_by_mail_background(self, cursor, uid, ids,
                                               context=None):
        email_wizard_obj = self.pool.get('poweremail.send.wizard')

        wiz_vals = {
            'state': context['state'],
            'priority': context['priority'],
            'from': context['from'],
        }
        pe_wiz = email_wizard_obj.create(cursor, uid, wiz_vals,
                                         context=context)
        return email_wizard_obj.send_mail(cursor, uid, [pe_wiz],
                                          context=context)

    def _cut_off_by_lang(self, cursor, uid, context=None):
        if not context:
            context = {}
        descarrec_id = context.get('active_id', False)
        query_file = (u"%s/soller_descarrec_comer/sql"
                      u"/query_desc_per_lang.sql") %  config['addons_path']
        query = open(query_file).read()
        cursor.execute(query, (descarrec_id, ))

        return [x for x in cursor.fetchall()]

    def _default_info(self, cursor, uid, context=None):
        """
            Summary with languages and sending status of mails
        """
        msg = '\n\n'

        msg_send = _('Sent')
        msg_no_mail = _('Without email')
        msg_no_send = _('Not sent')
        msg_no_lang = _('Without language')
        msg += _("State of Cut offs for send:\n")
        for vals in self._cut_off_by_lang(cursor, uid, context):
            num, send, lang, email_ok = vals
            lang_msg = lang == '*' and msg_no_lang or lang
            status_msg = send and msg_send or msg_no_send
            status_msg = email_ok and status_msg or msg_no_mail
            msg += _(" * %s (%s): %s \n") % (lang_msg, status_msg, num)

        msg += _(u"\nIf click send button you will send by email all cut offs"
                 u" not sent.")
        return msg

    _columns = {
        'state': fields.char('State', size=16, required=True),
        'info': fields.text('info', readonly=True),
    }

    _defaults = {
        'info': _default_info,
        'state': lambda *a: 'init',
    }

WizardEnviarDescarrec()
