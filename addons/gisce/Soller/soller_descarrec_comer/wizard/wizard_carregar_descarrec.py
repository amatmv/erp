# -*- coding: utf-8 -*-


from osv import osv
from osv import fields
import base64


class WizardCarregarDescarrec(osv.osv_memory):
    _name = 'wizard.carregar.descarrec'

    def action_load_cut_off(self, cursor, uid, ids, context=None):
        descarrec_obj = self.pool.get('soller.descarrec.comer')
        desline_obj = self.pool.get('soller.descarrec.comer.linia')
        polissa_obj = self.pool.get('giscedata.polissa')
        cups_obj = self.pool.get('giscedata.cups.ps')

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        data = base64.decodestring(wizard.cups_file)
        lines = data.split('\n')
        if lines:
            vals = {
                'data_inici': wizard.start_date,
                'data_fi': wizard.end_date,
                'name': wizard.name,
            }
            create_id = descarrec_obj.create(cursor, uid, vals)

            for line in lines:
                if not line:
                    continue

                search_params = [('name', '=', line)]
                cups_ids = cups_obj.search(cursor, uid, search_params)

                if cups_ids:
                    cups_id = cups_ids[0]
                else:
                    continue

                search_params = [('cups', '=', cups_id),
                                 ('state', 'not in', (
                                     'esborrany', 'validar', 'baixa'))]

                polissa_id = polissa_obj.search(cursor, uid, search_params)


                if polissa_id:
                    polissa_id = polissa_id[0]
                else:
                    polissa_id = False
                polissa = polissa_obj.browse(cursor, uid, polissa_id)

                vals = {
                    'descarrec_id': create_id,
                    'cups_id': cups_id,
                    'polissa_id': polissa_id,
                    'email': polissa.direccio_pagament.email,
                }
                desline_obj.create(cursor, uid, vals)

        return {'type': 'ir.actions.act_window_close'}

    _columns = {
        'name': fields.char('Name', size=250, required=True),
        'cups_file': fields.binary('Cups file', required=True),
        'start_date': fields.datetime('Start date', required=True),
        'end_date': fields.datetime('End date', required=True),
    }

WizardCarregarDescarrec()
