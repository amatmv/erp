# -*- encoding: utf-8 -*-
from osv import osv, fields

class SollerDescarrecComer(osv.osv):
    _name = 'soller.descarrec.comer'

    _order = 'data_inici desc'

    _columns = {
        'name': fields.char('Name', size=250, required=True),
        'data_inici': fields.datetime('Cut off start date', required=True),
        'data_fi': fields.datetime('Cut off end date', required=True),
    }

SollerDescarrecComer()

class SollerDescarrecComerLinia(osv.osv):
    _name = 'soller.descarrec.comer.linia'

    def name_get(self, cursor, uid, ids, context=None):
        res = []
        read_fields = ['descarrec_id', 'cups_id']
        vals = self.read(cursor, uid, ids, read_fields)
        for val in vals:
            name = '{} - {}'.format(val['descarrec_id'][1], val['cups_id'][1])
            res.append((val['id'], name))
        return res

    # Poweremails hooks
    def poweremail_create_callback(self, cursor, uid, ids, vals,
                                   context=None):
        """Hook from poweremail when a mail is create from cut off.
        """
        self.write(cursor, uid, ids, {'enviat': 1})
        return True

    def poweremail_unlink_callback(self, cursor, uid, ids, context=None):
        """Hook from poweremail when a mail is unlink
        """
        self.write(cursor, uid, ids, {'enviat': 0})
        return True

    _columns = {
        'descarrec_id': fields.many2one('soller.descarrec.comer',
                                        'Cut off',
                                        required=True,
                                        readonly=True,
                                        ondelete='cascade'),
        'cups_id': fields.many2one('giscedata.cups.ps', 'Cups',
                                   required=True,
                                   readonly=True),
        'polissa_id': fields.many2one('giscedata.polissa',
                                      'Policy',
                                      required=True,
                                      readonly=True),
        'email': fields.char('Email', size=100),
        'enviat': fields.boolean('Sent')
    }
SollerDescarrecComerLinia()

class SollerDescarrecComer2(osv.osv):
    _inherit = 'soller.descarrec.comer'


    _columns = {
        'client_ids': fields.one2many('soller.descarrec.comer.linia',
                                     'descarrec_id', 'Customers'),
    }

SollerDescarrecComer2()


