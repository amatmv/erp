# -*- coding: utf-8 -*-
{
    "name": "Facturar reconnexions",
    "description": """
Factures ràpides:
    * Facturar les reconnexions despres d'un tall
    * Facturar les verificacions dels comptadors
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_facturacio",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_reenganche_data.xml",
        "wizard/wizard_soller_factura_reenganche_view.xml"
    ],
    "active": False,
    "installable": True
}
