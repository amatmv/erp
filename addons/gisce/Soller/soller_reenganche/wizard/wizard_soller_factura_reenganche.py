# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
from datetime import datetime


class WizardSollerFacturaReenganche(osv.osv_memory):

    _name = 'wizard.soller.factura.reenganche'

    def get_product_taxes(self, cursor, uid, product_id, context=None):
        '''Retorna un diccionari amb els impostos associats a un producte'''

        product_obj = self.pool.get('product.product')

        if isinstance(product_id, (list, tuple)):
            product_id = product_id[0]

        product = product_obj.browse(cursor, uid, product_id)
        return [tax.id for tax in product.taxes_id]

    def get_product(self, cursor, uid, ids, polissa_id, context=None):

        polissa_obj = self.pool.get('giscedata.polissa')
        product_obj = self.pool.get('product.product')

        wizard = self.browse(cursor, uid, ids[0])

        _map_drets = {
            'extensio': ['DEBT', 'DEAT0', 'DEAT1', 'DEAT2'],
            'acces': ['DABT', 'DAAT0', 'DAAT1', 'DAAT2'],
            'enganche': ['DENBT', 'DENAT0', 'DENAT1', 'DENAT2'],
            'verificacio': ['DVBT', 'DVAT0', 'DVAT1', 'DVAT2'],
            'actuacio': ['DAMICBT', 'DAMICAT0',
                         'DAMICAT1', 'DAMICAT2'],
            'reconnexio': 'RECON',
        }

        polissa = polissa_obj.browse(cursor, uid, polissa_id)

        # Search for correct product
        if wizard.tipo_factura == 'reenganche':
            dret = 'reconnexio'
            default_code = _map_drets[dret]
        elif wizard.tipo_factura == 'verificacio':
            dret = 'actuacio'
            if (polissa.tarifa.tipus == 'BT'):
                default_code = _map_drets[dret][0]
            else:
                tensio = polissa.tensio
                if (tensio >= 0) and (tensio <= 36000):
                    default_code = _map_drets[dret][1]
                elif (tensio > 36000) and (tensio <= 72500):
                    default_code = _map_drets[dret][2]
                else:
                    default_code = _map_drets[dret][3]

        search_params = [('default_code', '=', default_code)]
        return product_obj.search(cursor, uid, search_params)[0]

    def action_generar_extra(self, cursor, uid, ids, context=None):
        '''genera linies extra de factures per incloure dins
        la factura electrica i no fer factures'''

        extra_obj = self.pool.get('giscedata.facturacio.extra')
        journal_obj = self.pool.get('account.journal')
        product_obj = self.pool.get('product.product')
        polissa_obj = self.pool.get('giscedata.polissa')

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])

        #Search for default journal. The one with code ENERGIA
        search_params = [('code', '=', 'ENERGIA')]
        journal_id = journal_obj.search(cursor, uid, search_params)

        polissa_id = context.get('polissa_id', 0)
        polissa = polissa_obj.browse(cursor, uid, polissa_id)

        date = datetime.strftime(datetime.now(), '%Y-%m-%d')
        product_id = self.get_product(cursor, uid, ids, polissa_id, context)
        product = product_obj.browse(cursor, uid, product_id)

        account_id = (product.property_account_income and
                      product.property_account_income.id or
                      product.categ_id.property_account_income_categ.id)

        tax_ids = self.get_product_taxes(cursor, uid, product_id,
                                         context=context)

        vals = {'name': product.name,
                'polissa_id': polissa_id,
                'product_id': product_id,
                'uos_id': product.uom_id.id,
                'date_from': date,
                'date_to': date,
                'date_line_from': date,
                'date_line_to': date,
                'quantity': 1,
                'price_unit': product.list_price,
                'term': 1,
                'account_id': account_id,
                'tax_ids': [(6, 0, tax_ids)],
                'journal_ids': [(6, 0, journal_id)]
                }

        extra_id = extra_obj.create(cursor, uid, vals)

        return {
            'name': 'Extra %s' % (wizard.tipo_factura),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.extra',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', '=', %s)]" % (extra_id),
        }

    def action_generar_factura(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])

        polissa_obj = self.pool.get('giscedata.polissa')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura_linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        line_obj = self.pool.get('account.invoice.line')
        journal_obj = self.pool.get('account.journal')
        product_obj = self.pool.get('product.product')
        payment_obj = self.pool.get('payment.type')

        #Aquestes factures son de la serie ENERGIA
        search_params = [('code', '=', 'ENERGIA')]
        journal_id = journal_obj.search(cursor, uid, search_params)[0]

        #Forma de pagament CAJA
        search_params = [('code', '=', 'CAJA')]
        payment_type_id = payment_obj.search(cursor, uid, search_params)[0]

        polissa_id = context.get('polissa_id', 0)
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        #generem la factura
        vals = {'date_invoice': wizard.data,
                'date_due': wizard.data,
                'polissa_id': polissa_id,
                'type': 'out_invoice',
                'journal_id': journal_id,
                'partner_id': wizard.pagador_id.id,
                'address_invoice_id': wizard.direccio_pagador_id.id,
                'account_id': wizard.pagador_id.property_account_receivable.id,
                'tarifa_acces_id': polissa.tarifa.id,
                'llista_preu': polissa.llista_preu.id,
                'date_boe': facturador_obj.get_data_boe(cursor, uid,
                                                        wizard.data),
                'data_inici': wizard.data,
                'data_final': wizard.data,
                'facturacio': 1,
                'cups_id': polissa.cups.id,
                'potencia': polissa.potencia,
                'payment_type': payment_type_id,
                }
        if wizard.tipo_factura == 'reenganche':
            vals['name'] = 'Reenganche'
            vals['tipo_factura'] = '04'  # Derechos de contratación
        elif wizard.tipo_factura == 'verificacio':
            vals['name'] = 'Verificació'
            vals['tipo_factura'] = '07' # Atenciones (Verificaciones,)

        factura_id = factura_obj.create(cursor, uid, vals, context)

        #Afegim la linia de la factura amb el reenganche
        product_id = self.get_product(cursor, uid, ids,
                                      polissa_id, context)
        product = product_obj.browse(cursor, uid, product_id)

        fpos = wizard.pagador_id.property_account_position
        pagador = wizard.pagador_id
        linia_vals = line_obj.product_id_change(cursor, uid, [],
                                                product.id,
                                                product.uom_id.id,
                                                1,
                                                partner_id=pagador.id,
                                                fposition_id=fpos.id)['value']
        linia_vals.update({
            'factura_id': factura_id,
            'product_id': product_id,
            'data_desde': wizard.data,
            'data_fins': wizard.data,
            'quantity': 1,
            'tipus': 'altres'
        })
        linia_vals['price_unit_multi'] = product.list_price
        tax_ids = [(6, 0, self.get_product_taxes(cursor, uid, product.id))]
        linia_vals['invoice_line_tax_id'] = tax_ids
        linia_vals['name'] = product.product_tmpl_id.name
        factura_linia_obj.create(cursor, uid, linia_vals, context)
        # Despues de crear les linies hem de cridar
        # la funcio per calcular els impostos
        factura_obj.button_reset_taxes(cursor, uid, [factura_id])

        return {
            'name': 'Factures {}'.format(wizard.tipo_factura),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', '=', {})]".format(factura_id),
        }

    def action_generar(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])

        if wizard.generar == 'factura':
            res = self.action_generar_factura(cursor, uid, ids, context)
        elif wizard.generar == 'extra':
            res = self.action_generar_extra(cursor, uid, ids, context)

        return res

    def _get_default_pagador(self, cursor, uid, context=None):
        polissa_obj = self.pool.get('giscedata.polissa')

        if not context:
            context = {}

        polissa_id = context.get('polissa_id', 0)
        vals = polissa_obj.read(cursor, uid, polissa_id, ['comercialitzadora'])
        return vals['comercialitzadora'][0]

    def _get_default_direccio(self, cursor, uid, context=None):
        partner_obj = self.pool.get('res.partner')
        partner_id = self._get_default_pagador(cursor, uid, context)
        return partner_obj.address_get(cursor, uid, [partner_id],
                                       adr_pref=['invoice'])['invoice']

    _tipo_factura_select = [
        ('reenganche', 'Reenganche'),
        ('verificacio', 'Verificació')
    ]

    _generar_select = [
        ('factura', 'Factura'),
        ('extra', 'Extra')
    ]

    _columns = {
        'pagador_id': fields.many2one('res.partner', 'Pagador',
                                      required=True),
        'direccio_pagador_id': fields.many2one('res.partner.address',
                                               'Direcció Pagador',
                                               required=True),
        'data': fields.date('Data', required=True),
        'tipo_factura': fields.selection(_tipo_factura_select, 'Factura',
                                         select=True, required=True),
        'generar': fields.selection(_generar_select, 'Generar',
                                    select=True, required=True),
    }

    _defaults = {
        'data': lambda *a: datetime.now().strftime('%Y-%m-%d'),
        'pagador_id': _get_default_pagador,
        'direccio_pagador_id': _get_default_direccio,
    }

WizardSollerFacturaReenganche()
