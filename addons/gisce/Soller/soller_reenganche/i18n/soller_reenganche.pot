# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* soller_reenganche
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2017-08-23 12:49\n"
"PO-Revision-Date: 2017-08-23 12:49\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: soller_reenganche
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: soller_reenganche
#: selection:wizard.soller.factura.reenganche,tipo_factura:0
msgid "Verificació"
msgstr "Verificació"

#. module: soller_reenganche
#: constraint:product.template:0
msgid "Error: The default UOM and the purchase UOM must be in the same category."
msgstr "Error: La UdM per defecte i la UdM de compra han d'estar en la mateixa categoria."

#. module: soller_reenganche
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Nom de model invàlid en la definició de l'acció"

#. module: soller_reenganche
#: model:ir.module.module,description:soller_reenganche.module_meta_information
msgid "\n"
"Factures ràpides:\n"
"    * Facturar les reconnexions despres d'un tall\n"
"    * Facturar les verificacions dels comptadors\n"
"    "
msgstr "\n"
"Factures ràpides:\n"
"    * Facturar les reconnexions despres d'un tall\n"
"    * Facturar les verificacions dels comptadors\n"
"    "

#. module: soller_reenganche
#: view:wizard.soller.factura.reenganche:0
msgid "Endavant"
msgstr "Endavant"

#. module: soller_reenganche
#: view:wizard.soller.factura.reenganche:0
msgid "Facturar Reenganche"
msgstr "Facturar Reenganche"

#. module: soller_reenganche
#: model:product.category,name:soller_reenganche.categ_reenganche
#: model:product.template,name:soller_reenganche.soller_reenganche_product_template
#: selection:wizard.soller.factura.reenganche,tipo_factura:0
msgid "Reenganche"
msgstr "Reenganche"

#. module: soller_reenganche
#: view:wizard.soller.factura.reenganche:0
msgid "Cancel·lar"
msgstr "Cancel·lar"

#. module: soller_reenganche
#: model:ir.actions.act_window,name:soller_reenganche.action_wizard_soller_factura_reenganche
msgid "Reen/Verif"
msgstr "Reen/Verif"

#. module: soller_reenganche
#: field:wizard.soller.factura.reenganche,data:0
msgid "Data"
msgstr "Data"

#. module: soller_reenganche
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: soller_reenganche
#: constraint:product.template:0
msgid "Error: UOS must be in a different category than the UOM"
msgstr "Error: La UdV ha d'estar en una categoria diferent que la UdM"

#. module: soller_reenganche
#: field:wizard.soller.factura.reenganche,direccio_pagador_id:0
msgid "Direcció Pagador"
msgstr "Direcció Pagador"

#. module: soller_reenganche
#: field:wizard.soller.factura.reenganche,pagador_id:0
msgid "Pagador"
msgstr "Pagador"

#. module: soller_reenganche
#: selection:wizard.soller.factura.reenganche,generar:0
msgid "Extra"
msgstr "Extra"

#. module: soller_reenganche
#: constraint:product.category:0
msgid "Error ! You can not create recursive categories."
msgstr "Error! No podeu crear categories recursives."

#. module: soller_reenganche
#: selection:wizard.soller.factura.reenganche,generar:0
#: field:wizard.soller.factura.reenganche,tipo_factura:0
msgid "Factura"
msgstr "Factura"

#. module: soller_reenganche
#: field:wizard.soller.factura.reenganche,generar:0
msgid "Generar"
msgstr "Generar"

#. module: soller_reenganche
#: constraint:product.product:0
msgid "Error: Invalid ean code"
msgstr "Error: Codi EAN erroni"

#. module: soller_reenganche
#: model:ir.model,name:soller_reenganche.model_wizard_soller_factura_reenganche
msgid "wizard.soller.factura.reenganche"
msgstr "wizard.soller.factura.reenganche"

#. module: soller_reenganche
#: model:ir.module.module,shortdesc:soller_reenganche.module_meta_information
msgid "Facturar reconnexions"
msgstr "Facturar reconnexions"

