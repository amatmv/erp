# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.misc import cache
from tools import config
from datetime import datetime
from soller_bi.polissa import gen_token

class SollerBILectures(osv.osv):
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'
    
    def _get_url_consum(self, cursor, uid, ids, name, arg, context=None):
        config_obj = self.pool.get('res.config')
        user_obj = self.pool.get('res.users')

        bi_domain = config_obj.get(cursor, uid, 'bi_domain',
                                   'intranet.grupelgas.es')
        if not bi_domain:
            return dict([(id, '') for id in ids])
        base_url = "https://%s/customer-consumption/%s?token=%s"
        
        today = datetime.now().strftime('%Y-%m-%d')
        user = user_obj.browse(cursor, uid, uid)
        token = gen_token(user.login, user.password, today,
                          config.options['token_key'])
        res = {}
        for id in ids:
            meter = self.browse(cursor, uid, id)
            res[id] = base_url % (bi_domain, meter.name, token)

        return res

    _columns = {
        'url_consum': fields.function(_get_url_consum,
                             method=True, type='char',
                             size=250,
                             string='Gràfic de consum',
                             readonly=True,
                     ),
    }
    

SollerBILectures()
