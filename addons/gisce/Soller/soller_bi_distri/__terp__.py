# -*- coding: utf-8 -*-
{
    "name": "Soller BI",
    "description": """
Accés a urls de gràfics i informes:
    * Fitxa de contracte (Polissa)
    * Gràfic de consum (Comptadors / Telegestió)
    
    Requereix la llibreria pycrypto
    pip install pycrypto
    """,
    "version": "0-dev",
    "author": "Bartomeu Miro Mateu",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures_telegestio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "lectures_view.xml"
    ],
    "active": False,
    "installable": True
}
