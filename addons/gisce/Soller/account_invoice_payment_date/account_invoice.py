# -*- encoding: utf-8 -*-

from osv import osv, fields


class AccountInvoicePaymentDate(osv.osv):
    '''
    Extend account invoice for add payment date
    '''
    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def get_last_payment_vals(self, cursor, uid, invoice_id, context=None):
        move_obj = self.pool.get('account.move.line')

        move_ids = self.move_line_id_payment_get(cursor, uid, [invoice_id])
        if not move_ids:
            return False

        line_ids = []  # Added temp list to avoid duplicate records
        for move in move_obj.browse(cursor, uid, move_ids, context):
            if move.reconcile_id:
                line_ids.extend([x.id for x in move.reconcile_id.line_id])
            elif move.reconcile_partial_id:
                line_ids.extend([x.id for x in
                                 move.reconcile_partial_id.line_partial_ids])

        payment_ids = list(set(line_ids) - set(move_ids))
        if not payment_ids:
            return False
        payments = move_obj.browse(cursor, uid, payment_ids)
        date = '1900-01-01'
        res = {}
        for payment in payments:
            if payment.date > date:
                res = {'date': payment.date,
                       'journal_id': payment.journal_id.id,
                       'journal_code': payment.journal_id.code}

        return res

    def _lastpayment(self, cursor, uid, ids, name, args, context):
        res = {}
        for id in ids:
            payment_vals = self.get_last_payment_vals(cursor, uid, id)
            if not payment_vals:
                res[id] = {
                    'last_payment_date': False,
                    'last_payment_journal_code': False,
                    'last_payment_journal_id': False,
                }
            else:
                res[id] = {
                    'last_payment_date': payment_vals['date'],
                    'last_payment_journal_code': payment_vals['journal_code'],
                    'last_payment_journal_id': payment_vals['journal_id'],
                }
        return res

    def _get_invoice_from_line(self, cursor, uid, ids, context=None):
        line_obj = self.pool.get('account.move.line')
        invoice_obj = self.pool.get('account.invoice')

        read_fields = ['reconcile_id', 'reconcile_partial_id']
        line_vals = line_obj.read(cursor, uid, ids, read_fields)
        reconcile_ids = [x['reconcile_id'][0]
                         for x in line_vals if x['reconcile_id']]
        partial_ids = [x['reconcile_partial_id'][0]
                       for x in line_vals if x['reconcile_partial_id']]
        search_params = ['|',
                         ('reconcile_id', 'in', reconcile_ids),
                         ('reconcile_partial_id', 'in', partial_ids)]
        line_ids = line_obj.search(cursor, uid, search_params)
        if not line_ids:
            return []
        line_vals = line_obj.read(cursor, uid, line_ids, ['move_id'])
        move_ids = [x['move_id'][0] for x in line_vals]
        if move_ids:
            search_params = [('move_id', 'in', move_ids)]
            return invoice_obj.search(cursor, uid, search_params)

        return []

    def _get_invoice_from_reconcile(self, cursor, uid, ids, context=None):
        line_obj = self.pool.get('account.move.line')
        invoice_obj = self.pool.get('account.invoice')

        search_params = ['|',
                         ('reconcile_id', 'in', ids),
                         ('reconcile_partial_id', 'in', ids)]
        line_ids = line_obj.search(cursor, uid, search_params)
        if not line_ids:
            return []
        line_vals = line_obj.read(cursor, uid, line_ids, ['move_id'])
        move_ids = [x['move_id'][0] for x in line_vals]
        if move_ids:
            search_params = [('move_id', 'in', move_ids)]
            return invoice_obj.search(cursor, uid, search_params)

        return []

    def _get_journals(self, cursor, uid, context=None):
        '''return all cash journals'''

        journal_obj = self.pool.get('account.journal')

        search_params = [('type', '=', 'cash')]
        journal_ids = journal_obj.search(cursor, uid, search_params,
                                         context=context)
        res = []
        read_fields = ['code', 'name']
        journal_vals = journal_obj.read(cursor, uid, journal_ids, read_fields)
        for journal_vals in journal_vals:
            journal_name = '({}) {}'.format(journal_vals['code'],
                                            journal_vals['name'])
            res.append((journal_vals['code'], journal_name))

        return res

    _store_last_payment_date = {
        'account.invoice': (lambda self, cursor, uid, ids, c=None: ids, None,
                            50),
        'account.move.line': (_get_invoice_from_line, None, 50),
        'account.move.reconcile': (_get_invoice_from_reconcile, None, 50)
    }

    _columns = {
        'last_payment_date': fields.function(_lastpayment, method=True,
                                             string='Last payment date',
                                             type='date',
                                             store=_store_last_payment_date,
                                             multi='last_payment',
                                             select=True,
                                             ),
        'last_payment_journal_code': fields.function(
                                                _lastpayment,
                                                method=True,
                                                string='Payment Journal',
                                                type='selection',
                                                selection=_get_journals,
                                                store=_store_last_payment_date,
                                                multi='last_payment',
                                                select=True),
        'last_payment_journal_id': fields.function(
                                            _lastpayment,
                                            method=True,
                                            string='Payment Journal',
                                            type='many2one',
                                            relation='account.journal',
                                            store=_store_last_payment_date,
                                            multi='last_payment',
                                            select=True),
    }

AccountInvoicePaymentDate()
