# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* account_invoice_payment_date
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2017-05-05 10:18:06+0000\n"
"PO-Revision-Date: 2017-05-05 10:18:06+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: account_invoice_payment_date
#: field:account.invoice,last_payment_journal_code:0
#: field:account.invoice,last_payment_journal_id:0
msgid "Payment Journal"
msgstr "Payment Journal"

#. module: account_invoice_payment_date
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: account_invoice_payment_date
#: field:account.invoice,last_payment_date:0
msgid "Last payment date"
msgstr "Last payment date"

#. module: account_invoice_payment_date
#: model:ir.module.module,description:account_invoice_payment_date.module_meta_information
msgid "\n"
"Account invoice last payment date:\n"
"    "
msgstr "\n"
"Account invoice last payment date:\n"
"    "

#. module: account_invoice_payment_date
#: model:ir.module.module,shortdesc:account_invoice_payment_date.module_meta_information
msgid "Account Invoice payment date"
msgstr "Account Invoice payment date"

