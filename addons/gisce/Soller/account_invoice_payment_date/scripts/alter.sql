ALTER TABLE "account_invoice" ADD COLUMN "last_payment_journal_code" text;
ALTER TABLE "account_invoice" ADD COLUMN "last_payment_journal_id" int4;
ALTER TABLE "account_invoice" ADD COLUMN "last_payment_date" date;