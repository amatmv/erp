# -*- encoding: utf-8 -*-

from erppeek import Client
from datetime import datetime, timedelta
import csv
import re
from progressbar import ProgressBar, ETA, Percentage, Bar

# host = '192.168.2.186'
host = 'localhost'

client = Client(
    'http://%s:18069' % host,
    db='oerp5_comer',
    user='admin',
    password='admin'
)

def main():
    invoice_obj = client.model('account.invoice')
    search_params = [('state', '=', 'paid')]
    invoice_ids = invoice_obj.search(search_params)
    pb = ProgressBar(maxval=len(invoice_ids),
                     widgets=[Percentage(), ' ', Bar(), ' ', ETA()]).start()
    done = 0
    for invoice_id in invoice_ids:
        done += 1
        pb.update(done)
        payment_vals = invoice_obj.get_last_payment_vals(invoice_id)		
        invoice_obj.write(invoice_id, {'last_payment_date': payment_vals['date'],
                                       'last_payment_journal_id': payment_vals['journal_id'],
                                       'last_payment_journal_code': payment_vals['journal_code']})

    pb.finish()

if __name__ == "__main__":
    start = datetime.now()
    try:
        main()
    except KeyboardInterrupt:
        print
        print 'Shutting Down...'
    finally:
        end = datetime.now()
        duration = "Time: %.3fs." % (
            (end - start).seconds +
            float((end - start).microseconds) / 10**6
        )
        print duration
