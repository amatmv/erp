# -*- coding: utf-8 -*-
{
    "name": "Account Invoice payment date",
    "description": """
Account invoice last payment date:
    """,
    "version": "0-dev",
    "author": "Toni Llado",
    "category": "Soller",
    "depends":[
        "account"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "account_invoice_view.xml"
    ],
    "active": False,
    "installable": True
}
