# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* soller_vat_control
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2017-10-03 14:59\n"
"PO-Revision-Date: 2017-10-03 14:59\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: soller_vat_control
#: model:ir.module.module,shortdesc:soller_vat_control.module_meta_information
msgid "VAT Control"
msgstr "VAT Control"

#. module: soller_vat_control
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: soller_vat_control
#: field:res.partner,previous_vat:0
msgid "Previous VAT"
msgstr "Previous VAT"

#. module: soller_vat_control
#: model:ir.module.module,description:soller_vat_control.module_meta_information
msgid "\n"
"Helpers to control vat assignment to partners\n"
"    "
msgstr "\n"
"Helpers to control vat assignment to partners\n"
"    "

#. module: soller_vat_control
#: field:res.partner,duplicated_vat:0
msgid "Duplicated Vat"
msgstr "Duplicated Vat"

#. module: soller_vat_control
#: view:res.partner:0
msgid "<b>Warning!</b> Duplicated VAT"
msgstr "<b>Warning!</b> Duplicated VAT"

