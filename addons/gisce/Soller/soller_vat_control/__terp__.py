# -*- coding: utf-8 -*-
{
    "name": "VAT Control",
    "description": """
Helpers to control vat assignment to partners
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "base_vat"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_view.xml"
    ],
    "active": False,
    "installable": True
}
