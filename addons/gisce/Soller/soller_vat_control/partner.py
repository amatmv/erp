# -*- encoding: utf-8 -*-

from osv import osv, fields


class PartnerVat(osv.osv):

    _inherit = 'res.partner'

    def check_duplicated_vat(self, cursor, uid, ids, context=None):
        '''checks if the partner has a duplicated vat'''

        vals = self.read(cursor, uid, ids, ['vat'])

        res = dict.fromkeys(ids, False)
        for val in vals:
            # Search for partners with same vat
            partner_id = val['id']
            vat = val['vat']
            if not vat:
                continue
            search_params = [('vat', '=', vat)]
            partner_ids = self.search(cursor, uid, search_params)
            if len(partner_ids) > 1:
                res[partner_id] = True
        return res

    def _get_duplicated_vat(self, cursor, uid, ids, name, arg,
                            context=None):

        return self.check_duplicated_vat(cursor, uid, ids, context)

    def _get_partners_duplicated_vat(self, cursor, uid, ids, context=None):
        '''return all partners with vat in ids
        to recompute duplicated_vat field'''

        vals = self.read(cursor, uid, ids, ['vat', 'previous_vat'])

        res = []
        for val in vals:
            vat = val['vat']
            previous_vat = val['previous_vat']
            partner_id = val['id']
            res.append(partner_id)
            vat_list = []
            if previous_vat:
                vat_list.append(previous_vat)
            if vat:
                vat_list.append(vat)
            if not vat_list:
                continue
            search_params = [('vat', 'in', vat_list)]
            partner_ids = self.search(cursor, uid, search_params)
            if partner_ids:
                res.extend(partner_ids)
        return list(set(res))

    def write(self, cursor, uid, ids, vals, context=None):
        '''Save vat in previous_vat when vat changes'''

        if 'vat' in vals:
            for id in ids:
                new_vals = vals.copy()
                pre_vals = self.read(cursor, uid, id, ['vat'])
                previous_vat = pre_vals['vat'] or ''
                if previous_vat == new_vals['vat']:
                    continue
                query = '''
                    UPDATE res_partner
                    SET previous_vat = %s
                    WHERE id = %s
                '''
                cursor.execute(query, (previous_vat, id, ))

        return super(PartnerVat, self).write(cursor, uid, ids,
                                             vals, context=context)

    _store_duplicated_vat = {
        'res.partner': (_get_partners_duplicated_vat,
                        ['vat', 'active'], 20),
    }

    _columns = {
        'previous_vat': fields.char('Previous VAT', size=32, readonly=True),
        'duplicated_vat': fields.function(_get_duplicated_vat, method=True,
                                          string='Duplicated Vat',
                                          type='boolean',
                                          readonly=True,
                                          store=_store_duplicated_vat)
    }

PartnerVat()
