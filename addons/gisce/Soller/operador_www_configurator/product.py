# -*- coding: utf-8 -*-

from osv import osv, fields


class ProductConfiguratorCategory(osv.osv):

    _name = 'operador.configurator.category'

    _order = 'category_order'

    _columns = {
        'name': fields.char('Category', size=40,
                            translate=True,
                            required=True),
        'code': fields.char('Code', size=20, required=True),
        'category_order': fields.integer('Order', required=True),
    }

ProductConfiguratorCategory()


class ProductProductConfigurator(osv.osv):

    _inherit = 'product.product'

    _columns = {
        'config_show': fields.boolean('Show'),
        'config_categ_id': fields.many2one('operador.configurator.category',
                                           'Category'),
        'config_order': fields.integer('Order'),
    }

ProductProductConfigurator()
