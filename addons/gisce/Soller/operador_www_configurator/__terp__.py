# -*- coding: utf-8 -*-
{
    "name": "Operator WWW Configurator",
    "description": """
  Add fields for web configurator
      """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "soller_operador"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "product_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
