# -*- coding: utf-8 -*-
{
    "name": "Informes de Pòlisses",
    "description": """
Diferents informes de pòlisses
    * Carta de normalització de tensions
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_informes_polissa_report.xml"
    ],
    "active": False,
    "installable": True
}
