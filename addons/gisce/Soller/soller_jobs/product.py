# -*- encoding: utf-8 -*-

from osv import osv, fields
from jobs import _type_selection
from tools.translate import _


class ProductProduct(osv.osv):

    _inherit = 'product.product'

    def get_job_categories(self, cursor, uid, context=None):

        categ_obj = self.pool.get('product.category')

        # The root category will be the one with code MATER
        search_params = [('parent_id', '=', False),
                         ('code', '=', 'MATER')]
        categ_id = categ_obj.search(cursor, uid, search_params)
        # Search for childs
        search_params = [('parent_id', 'child_of', categ_id)]
        return categ_obj.search(cursor, uid, search_params)

    def create(self, cursor, uid, vals, context=None):
        '''Add default code if not present'''

        seq_obj = self.pool.get('ir.sequence')

        categ_ids = self.get_job_categories(cursor, uid, context)

        if (('default_code' not in vals or
             not vals['default_code']) and
            vals['categ_id'] in categ_ids):
            # Check if the product belongs to categ_id
            code = seq_obj.get(cursor, uid, 'soller.jobs.product.code')
            vals['default_code'] = code

        return super(ProductProduct,
                     self).create(cursor, uid, vals, context=context)

    _columns = {
        'job_type': fields.selection(_type_selection, 'Job Type'),
    }

    def _check_duplicated_code(self, cursor, uid, ids, context=None):

        categ_ids = self.get_job_categories(cursor, uid, context)
        vals = self.read(cursor, uid, ids, ['default_code', 'categ_id'])

        for val in vals:
            code = val['default_code']
            product_id = val['id']
            categ_id = val['categ_id'][0]
            if not code or not categ_id in categ_ids:
                continue
            search_params = [('default_code', '=', code),
                             ('categ_id', 'in', categ_ids),
                             ('id', '<>', product_id)]
            product_ids = self.search(cursor, uid, search_params)
            if product_ids:
                return False

        return True

    _constraints = [
        (_check_duplicated_code,
         _('The product code already exists!'),
         ['default_code']),
    ]

ProductProduct()
