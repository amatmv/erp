# -*- encoding: utf-8 -*-

from datetime import datetime

from osv import osv, fields
from tools import float_round
from tools.translate import _

_type_selection = [
    ('engineering', 'Engineering'),
    ('materials', 'Materials'),
    ('construction', 'Obra civil'),
    ('works', 'Works')
]

_state_selection = [
    ('draft', 'Draft'),
    ('review', 'To Review'),
    ('done', 'Done')
]


class JobProject(osv.osv):

    _name = 'job.project'

    def name_get(self, cursor, uid, ids, context=None):

        if not len(ids):
            return []
        res = []

        read_fields = ['name', 'code']
        read_vals = self.read(cursor, uid, ids, read_fields)

        for val in read_vals:
            project_id = val['id']
            res.append((project_id, '({}) {}'.format(val['code'],
                                                     val['name'])))
        return res

    def create(self, cursor, uid, vals, context=None):

        seq_obj = self.pool.get('ir.sequence')

        if 'code' not in vals:
            code = seq_obj.get(cursor, uid, 'job.project')
            vals.update({'code': code})

        return super(JobProject, self).create(cursor, uid, vals,
                                              context=context)

    def get_childs(self, cursor, uid, project_id, depth=1, context=None):

        search_params = [('parent_id', '=', project_id)]
        res = [project_id]
        child_ids = self.search(cursor, uid, search_params)
        res.extend(child_ids)
        # if no direct childs do not continue
        if not child_ids:
            return res

        if depth == 0:
            return [project_id]
        elif depth > 0:
            decrement = 1
        elif depth == -1:
            # Recurse until no childs found
            decrement = 0
        for child_id in child_ids:
            recursive_child_ids = self.get_childs(cursor, uid, child_id,
                                                  depth=depth-decrement,
                                                  context=context)
            res.extend(recursive_child_ids)

        return list(set(res))

    def get_tasks(self, cursor, uid, project_id, depth=1, order=None,
                  from_date=None, to_date=None, context=None):
        '''get related tasks including the ones associated
        to child projects depending on depth'''

        task_obj = self.pool.get('job.task')

        project_ids = self.get_childs(cursor, uid, project_id,
                                      depth=depth, context=context)

        search_params = [('project_id', 'in', project_ids)]

        if from_date is not None:
            search_params.append(('date', '>=', from_date))

        if to_date is not None:
            search_params.append(('date', '<=', to_date))

        return task_obj.search(cursor, uid, search_params,
                               context=context, order=order)

    def _get_default_code(self, cursor, uid, context=None):

        seq_obj = self.pool.get('ir.sequence')

        return seq_obj.get(cursor, uid, 'job.project')

    _type_select = [
        ('investment', 'Investment'),
        ('maintenance', 'Maintenance'),
    ]

    _columns = {
        'name': fields.char('Name', size=200, required=True),
        'code': fields.char('Code', size=20,
                            readonly=True),
        'parent_id': fields.many2one('job.project', 'Parent project'),
        'description': fields.text('Description'),
        'type': fields.selection(_type_select, 'Type', required=True),
        'active': fields.boolean('Active'),
        'task_ids': fields.one2many('job.task', 'project_id', 'Tasks')
    }

    _defaults = {
        'active': lambda *a: True,
    }


JobProject()


class JobCeco(osv.osv):

    _name = 'job.ceco'

    def name_get(self, cursor, uid, ids, context=None):

        if not len(ids):
            return []
        res = []

        read_fields = ['name', 'code']
        read_vals = self.read(cursor, uid, ids, read_fields)

        for val in read_vals:
            ceco_id = val['id']
            res.append((ceco_id, '({}) {}'.format(val['code'], val['name'])))
        return res

    def name_search(self, cursor, uid, name='', args=[], operator='ilike',
                    context=None, limit=80):

        field_names = [x[0] for x in args]
        ceco_ids = []

        if 'code' not in field_names:
            code_args = [('code', operator, name)]
            ceco_ids += self.search(cursor, uid, code_args + args,
                                    context=context,
                                    limit=limit)

        if 'name' not in field_names:
            name_args = [('name', operator, name)]
            ceco_ids += self.search(cursor, uid, name_args + args,
                                    context=context, limit=limit)

        ceco_ids = list(set(ceco_ids))

        return self.name_get(cursor, uid, ceco_ids)

    _columns = {
        'name': fields.char('Name', size=200, required=True),
        'code': fields.char('Code', size=20, required=True),
    }


JobCeco()


class JobTask(osv.osv):

    _name = 'job.task'
    _order = 'date desc'

    def create(self, cursor, uid, vals, context=None):

        seq_obj = self.pool.get('ir.sequence')

        if not vals.get('name', False):
            vals['name'] = seq_obj.get(cursor, uid, 'job.task')

        res = super(JobTask, self).create(cursor, uid, vals, context)

        return res

    def copy(self, cursor, uid, id, default=None, context=None):

        if default is None:
            default = {}

        default.update({
            'name': False,
        })

        res_id = super(JobTask,
                       self).copy(cursor, uid, id,
                                  default=default,
                                  context=context)
        return res_id

    def update_cecos(self, cursor, uid, task_id, context=None):
        '''update all cecos from task_id'''

        task_obj = self.pool.get('job.task')

        task = task_obj.browse(cursor, uid, task_id)
        total = task.total_cost
        ceco_amount = 0
        cecos = {}
        for ceco in task.ceco_ids:
            amount = float_round((total * ceco.percent) / 100, 2)
            ceco_amount += amount
            cecos[ceco.id] = amount

        diff = float_round(total - ceco_amount, 2)
        if diff != 0 and task.ceco_ids:
            ceco = task.ceco_ids[-1]
            ceco_amount = float_round(cecos[ceco.id] + diff, 2)
            cecos[ceco.id] = ceco_amount

        query = '''
            UPDATE job_task_ceco SET ceco_amount = %s
            WHERE id = %s
        '''
        for ceco_id, ceco_cost in cecos.iteritems():
            cursor.execute(query, (ceco_cost, ceco_id, ))

        return True

    def create_stock_moves(self, cursor, uid, ids, context=None):
        '''create stock moves from task lines'''

        move_obj = self.pool.get('stock.move')
        line_obj = self.pool.get('job.task.line')

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        for id in ids:
            task = self.browse(cursor, uid, id)
            # Delete already created moves
            for move in task.move_ids:
                move.unlink()

            move_ids = []
            for line in task.line_ids:
                move_id = line_obj.create_move_from_line(cursor, uid, line.id)
                if move_id:
                    move_ids.append(move_id)

            vals = {
                'move_ids': [(6, 0, move_ids)]
            }
            self.write(cursor, uid, [id], vals)

        return True

    def delete_stock_moves(self, cursor, uid, ids, context=None):

        move_obj = self.pool.get('stock.move')

        if not context:
            context = {}

        move_ids = self.get_stock_moves(cursor, uid, ids, context)
        if move_ids:
            # Force deletion even if not in draft state
            ctx = context.copy()
            ctx.update({'call_unlink': True})
            move_obj.unlink(cursor, 1, move_ids, context=ctx)

        return True

    def get_stock_moves(self, cursor, uid, ids, context=None):

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        vals = self.read(cursor, uid, ids, ['move_ids'])
        all_move_ids = []
        for val in vals:
            all_move_ids.extend(val['move_ids'])

        return all_move_ids

    def confirm_stock_moves(self, cursor, uid, ids, context=None):

        move_obj = self.pool.get('stock.move')

        all_move_ids = self.get_stock_moves(cursor, uid, ids, context)
        move_obj.action_confirm(cursor, uid, all_move_ids,
                                context=context)
        return True

    def do_stock_moves(self, cursor, uid, ids, context=None):

        move_obj = self.pool.get('stock.move')

        all_move_ids = self.get_stock_moves(cursor, uid, ids, context)
        move_obj.action_done(cursor, uid, all_move_ids,
                             context=context)
        return True

    def action_draft(self, cursor, uid, ids, context=None):

        self.delete_stock_moves(cursor, uid, ids, context)

        return self.write(cursor, uid, ids, {'state': 'draft'})

    def action_review(self, cursor, uid, ids, context=None):

        return self.write(cursor, uid, ids, {'state': 'review'})

    def action_done(self, cursor, uid, ids, context=None):

        # Create and confirm and mark as done stock moves
        self.create_stock_moves(cursor, uid, ids, context)
        self.confirm_stock_moves(cursor, uid, ids, context)
        self.do_stock_moves(cursor, uid, ids, context)
        return self.write(cursor, uid, ids, {'state': 'done'})

    def action_compute_cost(self, cursor, uid, ids, line_ids=None, context=None):

        product_obj = self.pool.get('product.product')
        config_obj = self.pool.get('res.config')
        line_obj = self.pool.get('job.task.line')

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        if line_ids is None:
            line_ids = {}

        # Create extra cost for materials
        # Product CONS
        search_params = [('default_code', '=', 'CONS')]
        product_id = product_obj.search(cursor, uid, search_params)[0]

        cons_percent = int(config_obj.get(cursor, uid,
                                          'job_task_consumable_percent',
                                          '3'))
        total_material = 0
        for task_id in ids:
            to_delete = []
            task = self.browse(cursor, uid, task_id)
            compute_line_ids = line_ids.get(task_id, [])
            for line in task.line_ids:
                if not line.product_id:
                    continue
                if line.product_id.default_code == 'CONS':
                    to_delete.append(line.id)
                    continue
                if line.type != 'materials':
                    if line.manual_price:
                        cost = line.cost_price
                    else:
                        cost = line.product_id.computed_cost
                else:
                    costs = product_obj.compute_cost(cursor, uid,
                                                     [line.product_id.id],
                                                     date=task.date,
                                                     context=context)
                    cost = costs[line.product_id.id]

                # Refresh all or only the ones in compute_line_ids
                if (not compute_line_ids or
                   (compute_line_ids and line.id in compute_line_ids)):
                    line.write({'cost_price': cost})

                if line.type == 'materials':
                    # Refresh browse to get correct computed value after write
                    refresh_line = line_obj.browse(cursor, uid, line.id)
                    total_material += refresh_line.price_subtotal
            if to_delete:
                line_obj.unlink(cursor, uid, to_delete)
            if total_material == 0:
                continue
            material_cost = total_material * cons_percent / 100
            material_cost = float_round(material_cost, 2)

            vals = line_obj.onchange_product_id(cursor, uid, [], product_id,
                                                context=context)['value']
            vals.update({
                'cost_price': material_cost,
                'task_id': task_id,
                'product_id': product_id,
                'quantity': 1
            })

            line_obj.create(cursor, uid, vals)

        return True

    def onchange_policy_id(self, cursor, uid, ids, policy_id, context=None):

        if policy_id:
            policy_obj = self.pool.get('giscedata.polissa')
            cups = policy_obj.read(cursor, uid, policy_id, ['cups'],
                                   context=context)

            return {
                'value': {
                    'cups_id': cups['cups'][0],
                }
            }

    _columns = {
        'name': fields.char('Name', size=20, readonly=True),
        'date': fields.date('Task date', required=True),
        'description': fields.text('Description'),
        'project_id': fields.many2one('job.project', 'Project',
                                      required=True),
        'policy_id': fields.many2one('giscedata.polissa', 'Policy'),
        'move_ids': fields.many2many('stock.move', 'job_task_move_rel',
                                     'task_id', 'move_id',
                                     string='Moves'),
        'state': fields.selection(_state_selection, 'State',
                                  readonly=True),
        'notes': fields.text('Notes'),
        'cups_id': fields.many2one('giscedata.cups.ps', size=22, string="CUPS")
    }

    _defaults = {
        'date': lambda *a: datetime.now().strftime('%Y-%m-%d'),
        'state': lambda *a: 'draft',
    }

    def _check_total_ceco_percent(self, cursor, uid, ids, context=None):

        for task in self.browse(cursor, uid, ids):
            if not task.ceco_ids:
                continue
            percent = sum([ceco.percent for ceco in task.ceco_ids])
            if percent != 100:
                return False
        return True

    _constraints = [
        (_check_total_ceco_percent,
         'Error: CECO percent sum is not 100',
         ['ceco_ids'])
    ]


JobTask()


class JobTaskLine(osv.osv):

    _name = 'job.task.line'

    _rec_name = 'description'

    def create(self, cursor, uid, vals, context=None):

        res = super(JobTaskLine, self).create(cursor, uid, vals, context)
        self.update_cecos_from_line(cursor, uid, [res])
        return res

    def write(self, cursor, uid, ids, vals, context=None):

        res = super(JobTaskLine, self).write(cursor, uid, ids, vals,
                                             context=context)
        self.update_cecos_from_line(cursor, uid, ids, context)

        return res

    def update_cecos_from_line(self, cursor, uid, ids, context=None):

        task_obj = self.pool.get('job.task')

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        # Get all tasks related to lines in ids
        vals = self.read(cursor, uid, ids, ['task_id'])
        task_ids = list(set([val['task_id'][0] for val in vals]))
        for task_id in task_ids:
            task_obj.update_cecos(cursor, uid, task_id, context=context)
        return True

    def create_move_from_line(self, cursor, uid, line_id, context=None):
        '''Create a stock move using vals in line'''

        move_obj = self.pool.get('stock.move')
        config_obj = self.pool.get('res.config')

        line = self.browse(cursor, uid, line_id)
        if not line.product_id:
            raise osv.except_osv('Error',
                                 _('Cannot create move without product'))

        product = line.product_id
        # Do not create moves for non stockable objects
        if product.type != 'product':
            return 0

        # Get default move locations from config
        loc_id = int(config_obj.get(cursor, uid,
                                    'job_task_default_location_id',
                                    '0'))
        locdest_id = int(config_obj.get(cursor, uid,
                                        'job_task_default_location_dest_id',
                                        '0'))
        if not loc_id or not locdest_id:
            raise osv.except_osv('Error',
                                 _('No default locations defined for moves'))

        vals = move_obj.onchange_product_id(cursor, uid, [],
                                            prod_id=product.id,
                                            loc_id=loc_id,
                                            loc_dest_id=locdest_id)
        move_vals = vals['value']
        move_vals.update({
            'product_qty': line.quantity,
            'price_discount': 0,
            'price_unit': line.cost_price,
            'price_net': line.cost_price,
            'price_subtotal': line.price_subtotal,
            'product_id': product.id,
            'prodlot_id': line.lot_id and line.lot_id.id or False,
        })
        # We do not use uos fields
        if 'product_uos_qty' in move_vals:
            del move_vals['product_uos_qty']
        if 'product_uos' in move_vals:
            del move_vals['product_uos']

        return move_obj.create(cursor, uid, move_vals)

    def onchange_product_id(self, cursor, uid, ids,
                            product_id, context=None):

        res = {'value': {}, 'domain': {}, 'warning': {}}
        product_obj = self.pool.get('product.product')

        if not product_id:
            return res

        product = product_obj.browse(cursor, uid, product_id)
        vals = {
            'cost_price': product.standard_price,
            'description': product_obj.name_get(cursor, uid,
                                                [product_id])[0][1],
        }
        if product.job_type:
            vals.update({'type': product.job_type})

        res['value'].update(vals)

        domain_vals = {
            'lot_id': [('product_id', '=', product.id)],
        }
        res['domain'].update(domain_vals)

        return res

    def _amount_subtotal(self, cursor, uid, ids, name, args, context=None):

        res = dict.fromkeys(ids, 0)
        read_fields = ['quantity', 'cost_price']
        line_vals = self.read(cursor, uid, ids, read_fields)

        for line in line_vals:
            line_id = line['id']
            subtotal = line['quantity'] * line['cost_price']
            res[line_id] = float_round(subtotal, 2)

        return res

    def _get_default_task(self, cursor, uid, context=None):
        if context is None:
            context = {}

        return context.get('task_id', None)

    _store_subtotal = {
        'job.task.line':
        (lambda self, cr, uid, ids, c=None: ids,
         ['quantity', 'cost_price'], 20),
    }

    _columns = {
        'task_id': fields.many2one('job.task', 'Task', required=True,
                                   readonly=True,
                                   ondelete='cascade'),
        'task_state': fields.related('task_id', 'state',
                                     type="selection",
                                     selection=_state_selection,
                                     string="Task state",
                                     store=False,
                                     readonly=True),
        'task_date': fields.related('task_id', 'date',
                                    type="date",
                                    string="Task date", store=False,
                                    readonly=True),
        'product_id': fields.many2one('product.product', 'Product'),
        'lot_id': fields.many2one('stock.production.lot', 'Serial'),
        'description': fields.char('Description', size=200,
                                   required=True),
        'quantity': fields.float('Quantity', digits=(16, 2), required=True),
        'cost_price': fields.float('Cost', digits=(16, 2), required=True),
        'price_subtotal': fields.function(_amount_subtotal, method=True,
                                          string='Subtotal', readonly=True,
                                          store=_store_subtotal),
        'type': fields.selection(_type_selection, 'Type', required=True),
        'manual_price': fields.boolean(string='Manual price'),
    }

    _defaults = {
        'quantity': lambda *a: 1,
        'task_id': _get_default_task,
        'manual_price': lambda *a: False,
    }


JobTaskLine()


class JobTaskCeco(osv.osv):

    _name = 'job.task.ceco'

    def create(self, cursor, uid, vals, context=None):

        res = super(JobTaskCeco, self).create(cursor, uid, vals, context)
        self.update_cecos_from_ceco(cursor, uid, [res])

        return res

    def write(self, cursor, uid, ids, vals, context=None):

        if not context:
            context = {}

        res = super(JobTaskCeco, self).write(cursor, uid, ids, vals,
                                             context=context)
        self.update_cecos_from_ceco(cursor, uid, ids, context)

        return res

    def update_cecos_from_ceco(self, cursor, uid, ids, context=None):

        task_obj = self.pool.get('job.task')

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        # Get all tasks related to lines in ids
        vals = self.read(cursor, uid, ids, ['task_id'])
        task_ids = list(set([val['task_id'][0] for val in vals]))
        for task_id in task_ids:
            task_obj.update_cecos(cursor, uid, task_id, context=context)
        return True

    def _get_default_task(self, cursor, uid, context=None):
        if context is None:
            context = {}

        return context.get('task_id', None)

    _columns = {
        'task_id': fields.many2one('job.task', 'Task', required=True,
                                   ondelete='cascade',
                                   readonly=True),
        'task_state': fields.related('task_id', 'state', type="selection",
                                     selection=_state_selection,
                                     string="Task state", store=False,
                                     readonly=True),
        'ceco_id': fields.many2one('job.ceco', 'CECO', required=True),
        'percent': fields.float('Assign (%)', digits=(16, 2)),
        'ceco_amount': fields.float('CECO amount', digits=(16, 2),
                                    readonly=True)
    }

    _sql_constraints = [
        ('percent', 'CHECK (percent > 0 AND percent <= 100)',
         _("Percent must be greater than 0 and lower or equal to 100")
         ),
    ]

    _defaults = {
        'percent': lambda *a: 100,
        'task_id': _get_default_task,
    }


JobTaskCeco()


class JobTaskFacility(osv.osv):

    _name = 'job.task.facility'

    def _get_name(self, cursor, uid, ids, name, args, context=None):

        res = dict.fromkeys(ids, '')
        links = dict(self._get_facility_links)
        for facility in self.browse(cursor, uid, ids):
            if not facility.reference:
                continue
            model_name, model_id = facility.reference.split(',')
            model_id = int(model_id)
            model_obj = self.pool.get(model_name)
            name_get = model_obj.name_get(cursor, uid,
                                          model_id)[0][1]
            name = '{} -> {}'.format(_(links[model_name]),
                                     name_get)
            res[facility.id] = name
        return res

    _get_facility_links = [
        ('giscedata.cts', 'CT'),
        ('giscedata.bt.element', 'BT'),
        ('giscedata.at.tram', 'AT'),
        ('giscedata.celles.cella', 'Fiability'),
    ]

    _select_cost_type = [
        ('0', 'Complete Cost'),
        ('1', 'No Complete Cost'),
        ('2', 'Investment with no new physical units'),
    ]

    _columns = {
        'task_id': fields.many2one('job.task', 'Task', required=True,
                                   ondelete='cascade'),
        'name': fields.function(_get_name, method=True,
                                type='char', size=200,
                                string="Reference",
                                readonly=True),
        'reference': fields.reference('Facility', _get_facility_links,
                                      size=128),
        'cost_type': fields.selection(_select_cost_type, 'Cost Type'),
    }


JobTaskFacility()


class JobType(osv.osv):

    _name = 'job.task.type'

    _columns = {
        'name': fields.char('Name', size=50, required=True),
    }


JobType()


class JobTask2(osv.osv):

    _inherit = 'job.task'

    def _get_total_cost(self, cursor, uid, ids, name, arg, context=None):

        res = {}

        for task in self.browse(cursor, uid, ids):
            total = 0
            for line in task.line_ids:
                subtotal = line['quantity'] * line['cost_price']
                total += float_round(subtotal, 2)
            res[task.id] = total

        return res

    def _get_task_from_line(self, cursor, uid, ids, context=None):

        line_obj = self.pool.get('job.task.line')

        line_vals = line_obj.read(cursor, uid, ids, ['task_id'])

        return list(set([line['task_id'][0] for line in line_vals]))

    _store_total = {
        'job.task': (lambda self, cr, uid, ids, c=None: ids, [], 20),
        'job.task.line': (_get_task_from_line,
                          ['quantity', 'cost_price'], 20),
    }

    _columns = {
        'line_ids': fields.one2many('job.task.line', 'task_id', 'Lines'),
        'ceco_ids': fields.one2many('job.task.ceco', 'task_id', 'CECOs'),
        'facility_ids': fields.one2many('job.task.facility', 'task_id',
                                        'Facilities'),
        'type_id': fields.many2one('job.task.type', 'Job Type',
                                   required=True),
        'total_cost': fields.function(_get_total_cost, method=True,
                                      string='Total cost', readonly=True,
                                      store=_store_total)
    }


JobTask2()
