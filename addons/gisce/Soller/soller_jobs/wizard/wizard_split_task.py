# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools import float_round


class WizardSplitTask(osv.osv_memory):

    _name = 'wizard.split.task'

    def action_split(self, cursor, uid, ids, context=None):

        task_obj = self.pool.get('job.task')
        line_obj = self.pool.get('job.task.line')

        wizard = self.browse(cursor, uid, ids[0])
        task = wizard.task_id

        line_percent = {}
        for line in wizard.line_ids:
            # Do not split lines with 0 percent
            if line.percent == 0:
                continue
            line_percent[line.task_line_id.id] = line.percent

        # If nothing to split, terminate
        if not line_percent:
            return True

        new_vals = {
            'line_ids': [],
            'move_ids': [],
        }
        new_task_id = task_obj.copy(cursor, uid, task.id, new_vals)

        for line in task.line_ids:
            if line.id in line_percent.keys():
                # Write value for existing line
                percent = line_percent[line.id]
                cost = line.cost_price
                new_cost = cost - float_round(((cost * percent) / 100), 2)
                line_obj.write(cursor, uid, [line.id],
                               {'cost_price': new_cost})
                # Create line for new task
                vals = {
                    'task_id': new_task_id,
                    'product_id': (line.product_id and
                                   line.product_id.id or
                                   False),
                    'lot_id': (line.lot_id and
                               line.lot_id.id or
                               False),
                    'description': line.description,
                    'quantity': line.quantity,
                    'cost_price': float_round(((cost * percent) / 100), 2),
                    'type': line.type,
                }
                line_obj.create(cursor, uid, vals)

        vals_view = {
            'name': 'Splitted tasks',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'job.task',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', {})]".format([task.id, new_task_id]),
        }
        return vals_view

    def _get_default_task(self, cursor, uid, context=None):

        if not context:
            context = {}

        return context.get('task_id', 0)

    def _get_default_lines(self, cursor, uid, context=None):

        access_obj = self.pool.get('ir.model.access')
        access_obj.check(cursor, uid, self._name, 'create', context=context)

        task_obj = self.pool.get('job.task')
        wline_obj = self.pool.get('wizard.split.task.line')

        if not context:
            context = {}

        task_id = context.get('task_id', 0)
        if not task_id:
            return []

        task = task_obj.browse(cursor, uid, task_id)

        res = []
        for line in task.line_ids:
            vals = {
                'task_line_id': line.id,
                'line_subtotal': line.price_subtotal,
                'percent': 0,
            }
            res.append(vals)

        return res

    _columns = {
        'task_id': fields.many2one('job.task', 'Task'),
        'line_ids': fields.one2many('wizard.split.task.line',
                                    'wizard_id', 'Lines'),
    }

    _defaults = {
        'task_id': _get_default_task,
        'line_ids': _get_default_lines,
    }

WizardSplitTask()


class WizardSplitLine(osv.osv_memory):

    _name = 'wizard.split.task.line'

    _columns = {
        'wizard_id': fields.many2one('wizard.split.task', 'Wizard',
                                     required=True),
        'task_line_id': fields.many2one('job.task.line', 'Task line',
                                        required=True),
        'line_subtotal': fields.float('Line subtotal', digits=(16, 2),
                                      readonly=True),
        'percent': fields.float('Percent', digits=(16, 2),
                                required=True),
    }

WizardSplitLine()
