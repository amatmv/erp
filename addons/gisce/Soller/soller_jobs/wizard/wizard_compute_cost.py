# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools import float_round


class WizardComputeTaskCost(osv.osv_memory):

    _name = 'wizard.compute.task.cost'

    def action_compute(self, cursor, uid, ids, context=None):

        task_obj = self.pool.get('job.task')

        wizard = self.browse(cursor, uid, ids[0])
        task = wizard.task_id

        to_compute = [line.task_line_id.id for line in wizard.line_ids
                      if line.compute_cost]
        
        task_obj.action_compute_cost(cursor, uid, [task.id],
                                     line_ids = {task.id: to_compute},
                                     context=context)
        return {}

    def _get_default_task(self, cursor, uid, context=None):

        if not context:
            context = {}

        return context.get('task_id', 0)

    def _get_default_lines(self, cursor, uid, context=None):

        access_obj = self.pool.get('ir.model.access')
        access_obj.check(cursor, uid, self._name, 'create', context=context)

        task_obj = self.pool.get('job.task')

        if not context:
            context = {}

        task_id = context.get('task_id', 0)
        if not task_id:
            return []

        task = task_obj.browse(cursor, uid, task_id)

        res = []
        for line in task.line_ids:
            vals = {
                'task_line_id': line.id,
                'compute_cost': False,
            }
            res.append(vals)

        return res

    _columns = {
        'task_id': fields.many2one('job.task', 'Task'),
        'line_ids': fields.one2many('wizard.compute.task.cost.line',
                                    'wizard_id', 'Lines'),
    }

    _defaults = {
        'task_id': _get_default_task,
        'line_ids': _get_default_lines,
    }

WizardComputeTaskCost()


class WizardComputeTaskCostLine(osv.osv_memory):

    _name = 'wizard.compute.task.cost.line'

    _columns = {
        'wizard_id': fields.many2one('wizard.compute.task.cost', 'Wizard',
                                     required=True),
        'task_line_id': fields.many2one('job.task.line', 'Task line',
                                        required=True),
        'compute_cost': fields.boolean('Compute', required=True),
    }

WizardComputeTaskCostLine()
