# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardShowTask(osv.osv_memory):
    _name = 'wizard.show.task'

    def action_show(self, cursor, uid, ids, context=None):

        task_obj = self.pool.get('job.task')
        project_obj = self.pool.get('job.project')

        wizard = self.browse(cursor, uid, ids[0])
        group_project = wizard.group_project

        if group_project == 'all-projects':
            depth = -1
        elif group_project == 'without-children':
            depth = 0
        elif group_project == 'with-children' and wizard.depth < 1:
            raise osv.except_osv('Error', _('Depth must be one or greater'))
        else:
            depth = wizard.depth

        task_ids = project_obj.get_tasks(cursor, uid, wizard.project_id.id,
                                         depth, context=context)

        vals_view = {
            'name': 'Project tasks',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'job.task',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', {})]".format(task_ids),
        }

        return vals_view

    def _get_default_task(self, cursor, uid, context=None):

        if not context:
            context = {}

        return context.get('project_id', 0)

    _group_projects = [
        ('all-projects', 'All projects'),
        ('without-children', 'Without children'),
        ('with-children', 'With children')
    ]

    _columns = {
        'project_id': fields.many2one('job.task', 'Task'),
        'depth': fields.integer('Depth'),
        'group_project': fields.selection(_group_projects, string="Depth",
                                          required=True)
    }

    _defaults = {
        'project_id': _get_default_task,
        'depth': lambda *a: 1,
        'group_project': lambda *a: 'all-projects'
    }


WizardShowTask()
