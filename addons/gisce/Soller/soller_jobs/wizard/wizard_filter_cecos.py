# -*- encoding: utf-8 -*-

from osv import osv, fields


class WizardFilterCecos(osv.osv_memory):
    _name = 'wizard.filter.cecos'

    def action_filter(self, cursor, uid, ids, context=None):

        taskceco_obj = self.pool.get('job.task.ceco')

        # job.task.ceco < ceco_ids
        wizard = self.browse(cursor, uid, ids[0])
        ceco_ids = [x.id for x in wizard.ceco_ids]
        search_params = [('ceco_id', 'in', ceco_ids)]
        task_ceco_ids = taskceco_obj.search(cursor, uid, search_params)

        vals_view = {
            'name': 'Task CECOs',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'job.task.ceco',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', {})]".format(task_ceco_ids),
        }

        return vals_view

    _columns = {
        'ceco_ids': fields.many2many('job.ceco', 'job_task_ceco_filter_rel',
                                     'wizard_id', 'ceco_id',
                                     string='CECOs'),
    }

WizardFilterCecos()