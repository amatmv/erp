# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools import float_round


class WizardAssignHours(osv.osv_memory):

    _name = 'wizard.task.assign.hours'

    def action_create_wizard_lines(self, cursor, uid, ids, context=None):

        product_obj = self.pool.get('product.product')
        line_obj = self.pool.get('wizard.task.assign.hours.line')

        wizard = self.browse(cursor, uid, ids[0])

        # Search for products on selected category
        search_params = [('categ_id', '=', wizard.category_id.id)]
        product_ids = product_obj.search(cursor, uid, search_params)

        for product_id in product_ids:
            vals = {
                'wizard_id': wizard.id,
                'product_id': product_id,
            }
            line_obj.create(cursor, uid, vals)

        wizard.write({'state': 'end'})

        return True 

    def action_create_task_lines(self, cursor, uid, ids, context=None):

        task_line_obj = self.pool.get('job.task.line')
        product_obj = self.pool.get('product.product')

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])

        task_id = context['task_id']

        for line in wizard.line_ids:
            if not line.assign:
                continue
            desc = product_obj.name_get(cursor, uid,
                                        [line.product_id.id])[0][1]
            vals = {
                'task_id': task_id,
                'product_id': line.product_id.id,
                'quantity': wizard.quantity,
                'description': desc,
                'type': line.product_id.job_type,
                'cost_price': 0,
            }
            task_line_obj.create(cursor, uid, vals,
                                 context=context)

        return {}

    _states_selection = [
        ('init', 'Init'),
        ('end', 'End'),
    ]

    _columns = {
        'category_id': fields.many2one('product.category', 'Category',
                                       required=True),
        'quantity': fields.float('Quantity', digits=(16, 2),
                                 required=True),
        'state': fields.selection(_states_selection, string='State',
                                  readonly=True)
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardAssignHours()


class WizardAssignHoursLine(osv.osv_memory):

    _name = 'wizard.task.assign.hours.line'

    _columns = {
        'wizard_id': fields.many2one('wizard.task.assign.hours',
                                     required=True),
        'product_id': fields.many2one('product.product', 'Product',
                                      required=True),
        'assign': fields.boolean('Assign'),
    }

    _defaults = {
        'assign': lambda *a: False,
    }

WizardAssignHoursLine()


class WizardAssignHours2(osv.osv_memory):

    _inherit = 'wizard.task.assign.hours'

    _columns = {
        'line_ids': fields.one2many('wizard.task.assign.hours.line',
                                    'wizard_id', string='Lines'),
    }

WizardAssignHours2()
