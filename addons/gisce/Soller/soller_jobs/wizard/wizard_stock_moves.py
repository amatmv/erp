# coding=utf-8

from osv import osv, fields


class WizardStockMoves(osv.osv_memory):
    _name = 'wizard.stock.moves'

    def action_show_stocks(self, cursor, uid, ids, context=None):

        if not isinstance(context['active_ids'], list):
            context['active_ids'] = [context['active_ids']]

        stock_move_obj = self.pool.get('stock.move')
        config_obj = self.pool.get('res.config')
        wizard = self.browse(cursor, uid, ids[0], context=context)

        stock_input_id = int(config_obj.get(cursor,
                                            uid,
                                            'job_task_default_location_id',
                                            '0'))
        stock_output_id = int(config_obj.get(cursor,
                                             uid,
                                             'job_task_default_location'
                                             '_dest_id',
                                             '0'))

        stock_kind = wizard.stock_kind
        is_stock_input = stock_kind == 'stock_move_in'

        if not stock_input_id or not stock_output_id:
            raise osv.except_osv('Error',
                                 _('No default locations defined for moves'))

        module = 'soller_jobs'
        name = 'In' if is_stock_input else 'Out'
        dest_id = stock_input_id if is_stock_input else stock_output_id
        view_tree_ref = '{}.view_soller_jobs_tree_{}'.format(module, stock_kind)
        product_id = context['active_ids'][0]

        stock_move_ids = stock_move_obj.search(cursor, uid, [
            ('product_id', '=', product_id),
            ('location_dest_id', '=', dest_id)
        ], context=context)

        vals_view = {
            'name': 'All Stock Move {}'.format(name),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.move',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', {})]".format(stock_move_ids),
            'context': {
                'tree_view_ref': view_tree_ref,
            }
        }

        return vals_view

    _stock_kinds = [
        ('stock_move_in', 'Stock In'),
        ('stock_move_out', 'Stock Out')
    ]

    _columns = {
        'stock_kind': fields.selection(_stock_kinds, 'Stock', required=True)
    }


WizardStockMoves()
