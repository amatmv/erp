# coding=utf-8

from osv import osv, fields


class WizardPrintProject(osv.osv_memory):
    _name = 'wizard.print.project'

    def action_print_project(self, cursor, uid, ids, context=None):

        if context is None:
            context = {}

        form_values = self.read(cursor, uid, ids, ['start_date', 'end_date'],
                                context=context)

        vals_view = {
            'type': 'ir.actions.report.xml',
            'report_name': 'report_soller_jobs_project_webkit',
            'datas': {
                'ids': context.get('active_ids', []),
                'parameters': form_values and form_values[0] or {}
            }
        }

        return vals_view

    _columns = {
        'start_date': fields.date('Start date'),
        'end_date': fields.date('End date'),
    }


WizardPrintProject()
