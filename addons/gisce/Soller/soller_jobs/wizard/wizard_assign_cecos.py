# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardAssignCecos(osv.osv_memory):
    _name = 'wizard.assign.cecos'

    def action_perform_massive_assigment(self, cursor, uid, ids, context=None):

        if context is None:
            context = {}

        job_task_ceco_obj = self.pool.get('job.task.ceco')
        job_task_obj = self.pool.get('job.task')
        wizard = self.browse(cursor, uid, ids, context=context)[0]
        task_ids = context.get('active_ids', [])
        ceco_vals = []
        total_percent = 0
        skipped_tasks = []
        assigned_tasks = []

        # Create initial CECO objects
        for line in wizard.line_ids:
            ceco_vals.append({
                'ceco_id': line.ceco_id.id,
                'percent': line.percent
            })
            total_percent += line.percent

        if total_percent != 100:
            raise osv.except_osv('Error', _('Total percent must be 100%'))

        # Create a CECO val for each task ID
        # Skip update if task has a CECO created
        for task in job_task_obj.browse(cursor, uid, task_ids):
            if task.ceco_ids:
                skipped_tasks.append(task.id)
                continue
            for ceco_val in ceco_vals:
                vals = ceco_val.copy()
                vals.update({'task_id': task.id})
                job_task_ceco_obj.create(cursor, uid, vals, context=context)
                assigned_tasks.append(task.id)

        wizard.write({
            'state': 'finished',
            'skipped_tasks': [(6, 0, skipped_tasks)],
            'assigned_tasks': [(6, 0, assigned_tasks)]
        })

    _states = [('select-cecos', 'Select CECOs'), ('finished', 'Finished')]

    _columns = {
        'state': fields.selection(_states, string="State"),
        'line_ids': fields.one2many('wizard.assign.ceco_lines', 'wizard_id',
                                    string='CECO Lines'),
        'skipped_tasks': fields.many2many('job.task',
                                          'wizard_assign_cecos_skipped_rel',
                                          'wizard_id',
                                          'task_id',
                                          string='Skipped tasks'),
        'assigned_tasks': fields.many2many('job.task',
                                           'wizard_assign_cecos_assigned_rel',
                                           'wizard_id',
                                           'task_id',
                                           string='Assigned tasks')
    }

    _defaults = {
        'state': lambda *a: 'select-cecos',
    }


WizardAssignCecos()


class WizardAssignCecosLines(osv.osv_memory):
    _name = 'wizard.assign.ceco_lines'

    _columns = {
        'wizard_id': fields.many2one('wizard.assign.cecos', 'wizard'),
        'ceco_id': fields.many2one('job.ceco', string="CECO"),
        'percent': fields.float('Assign (%)', digits=(16, 2)),
    }

    _defaults = {
        'percent': lambda *a: 100,
    }


WizardAssignCecosLines()
