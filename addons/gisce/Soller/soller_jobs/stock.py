# coding=utf-8

from osv import osv, fields


class StockMove(osv.osv):
    _inherit = 'stock.move'

    def _get_task_data(self, cursor, uid, ids, field_name, arg, context=None):

        values = {}

        for move in self.browse(cursor, uid, ids):
            task_date = move.task_ids[0].date
            task_name = move.task_ids[0].name
            project_id = move.task_ids[0].project_id.id

            values[move.id] = {
                'task_date': task_date,
                'task_name': task_name,
                'project_id': project_id,
            }

        return values

    _columns = {
        'task_ids': fields.many2many('job.task', 'job_task_move_rel',
                                     'move_id', 'task_id'),
        'picking_origin_date': fields.related('picking_id', 'origin_date',
                                              type='date',
                                              string='Purchase date'),
        'picking_name': fields.related('picking_id', 'name',
                                       type='char',
                                       string='Picking number'),
        'picking_origin': fields.related('picking_id', 'origin',
                                         type='char',
                                         string='Internal number'),
        'partner_id': fields.related('picking_id', 'partner_id',
                                     type='many2one',
                                     relation='res.partner',
                                     string='Partner'),
        'task_date': fields.function(_get_task_data,
                                     string='Output date',
                                     type='date',
                                     method='true',
                                     multi='stock_move_task_data'),
        'task_name': fields.function(_get_task_data,
                                     string='Task number',
                                     type='char',
                                     method='true',
                                     multi='stock_move_task_data'),
        'project_id': fields.function(_get_task_data,
                                      string='Project',
                                      type='many2one',
                                      relation='job.project',
                                      method='true',
                                      multi='stock_move_task_data'),
    }


StockMove()
