<%

from tools import config
from itertools import groupby
from functools import reduce

depth = 0
addons_path = config['addons_path']
cursor = objects[0]._cr
pool = objects[0].pool
uid = user.id
user_obj = pool.get('res.users')
address_obj = pool.get('res.partner.address')
project_obj = pool.get('job.project')
task_line_obj = pool.get('job.task.line')
task_obj = pool.get('job.task')
data_obj = pool.get('ir.model.data')
line_types = dict(task_line_obj.fields_get(cursor, uid, context={'lang': user.context_lang})['type']['selection'])
user_group_ids = [group.id for group in user.groups_id]
financial_group_id = data_obj.get_object_reference(cursor, uid, 'soller_jobs', 'group_jobs_financial')[1]
has_financial = financial_group_id in user_group_ids
company_obj = user_obj.browse(cursor, uid, uid).company_id
company_addresses = company.partner_id.address_get(adr_pref=['contact'])
parameters = data.get('parameters', {})

if 'contact' in company_addresses:
    company_address = address_obj.read(cursor, uid, company_addresses['contact'])
else:
    company_address = {}

fax_str = ''

if company_address['fax']:
    fax_str = ', {} {}'.format(_('Fax'), company_address['fax'])

project_types = dict(project_obj.fields_get(cursor, uid, context={'lang': user.context_lang})['type']['selection'])

## task_lines_t = pool.search(cursor, uid, ['project_id'])
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="${addons_path}/soller_jobs/report/static/css/print.css"/>
    <link rel="stylesheet" type="text/css" href="${addons_path}/soller_jobs/report/static/css/extra.css"/>
</head>
<body>
    <div class="container">
        %for project in objects:
        <%
            line_ids = []
            total_price = 0
            from_date = parameters.get('start_date', None)
            to_date = parameters.get('end_date', None)

            task_ids = project_obj.get_tasks(cursor, uid, project.id, depth, order='date,name', from_date=from_date,
                                             to_date=to_date)
            tasks = task_obj.browse(cursor, uid, task_ids)
        %>
        <!-- Row: Sender Data + VSE Logo -->
        <div class="row top-padding-fix">
            <!-- Col: Sender Data -->
            <div class="col-xs-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>${_('Sender data')}</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <p><b><dfn>${company.partner_id.name.upper()}</dfn></b> (${company.partner_id.vat.upper()})</p>
                            <p>${'{}, {} - {}'.format(company_address['street'], company_address['zip'], company_address['city'].upper())}</p>
                            <p>${'{} - {}'.format(company_address['state_id'][1].upper(), company_address['country_id'][1].upper())}</p>
                            <p>${_('Phone')} ${company_address['phone']}${fax_str}</p>
                            %if company.partner_id.website:
                                <p>${company.partner_id.website}</p>
                            %endif
                            %if company_address['email']:
                                <p>${company_address['email']}</p>
                            %endif
                            <p style="font-size: 8px;">${company.rml_footer1}</p>
                        </div>
                    </div>
                </div>
                <!-- Col: VSE Logo -->
                <div class="col-xs-5 col-xs-offset-2">
                    <img style="width: 80%; margin: 0 auto" src="${addons_path}/soller_jobs/report/static/img/vse_solo.jpg"/>
                </div>
        </div>

        <!-- Row: Project data-->
        <div class="row">
            <!-- Col: Project data -->
            <div class="col-xs-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Project data')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <p><strong>${_('Number')}</strong>: ${project.code}</p>
                        <p><strong>${_('Name')}</strong>: ${project.name}</p>
                        <p><strong>${_('Type')}</strong>: ${project_types[project.type]}</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Row: Task data -->
        <div class="row">
            <!-- Col: Task data -->
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Task data')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <table class="table-adjust-space">
                            <thead>
                                <tr>
                                    <th class="table-cell-shrink">${_('Task date')}</th>
                                    <th class="table-cell-shrink">${_('Task number')}</th>
                                    <th class="table-cell-expand">${_('Task notes')}</th>
                                    %if has_financial:
                                        <th class="text-right table-cell-shrink">${_('Task cost')}</th>
                                    %endif
                                </tr>
                            </thead>
                            <tbody>
                                %for task in tasks:
                                <%
                                    line_ids += task.line_ids
                                    total_price += task.total_cost
                                %>
                                <tr>
                                    <td class="table-cell-shrink">${task.date}</td>
                                    <td class="text-center table-cell-shrink">${task.name}</td>
                                    <td class="table-cell-expand">
                                        ${task.notes and task.notes.replace("\n", "<br/>") or ''}
                                    </td>
                                    %if has_financial:
                                        <td class="text-right amount table-cell-shrink">${formatLang(task.total_cost, digits=2)} €</td>
                                    %endif
                                </tr>
                                %endfor
                                %if has_financial:
                                    <tr>
                                        <td class="table-cell-shrink"></td> <!-- Date column -->
                                        <td class="table-cell-shrink"></td> <!-- Name column -->
                                        <th class="text-right table-cell-expand">${_('Total')}</th> <!-- Notes column -->
                                        <td class="text-right amount table-cell-shrink">${formatLang(total_price, digits=2)} €</td> <!-- Price column-->
                                    </tr>
                                %endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        %if has_financial:
            <div class="row">
                <div class="col-xs-5">
                    <div class="panel panel-default avoid-break">
                        <div class="panel-heading">
                            <h4>${_('Subtotals')}</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <%
                                sort_func = lambda task: task.type
                                sorted_lines = sorted(line_ids, key=sort_func)
                            %>
                            <table class="table-adjust-space">
                                <tbody>
                                    %for type, lines in groupby(sorted_lines, key=lambda task: task.type):
                                        <tr>
                                            <%
                                                sum_subtotal = reduce(lambda x, y: x + y.price_subtotal, lines, 0)
                                            %>
                                            <th class="table-cell-expand text-left">${line_types[type]}</th>
                                            <td class="table-cell-shrink text-right">${formatLang(sum_subtotal, digits=2)} €</td>
                                        </tr>
                                    %endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        %endif
        % if objects.index(project) + 1 != len(objects):
            <p class="page-break"></p>
        % endif
        %endfor
    </div>
</body>
</html>