<%

from tools import config
from itertools import groupby
from functools import reduce
from datetime import datetime

addons_path = config['addons_path']
cursor = objects[0]._cr
pool = objects[0].pool
uid = user.id
user_obj = pool.get('res.users')
address_obj = pool.get('res.partner.address')
task_line_obj = pool.get('job.task.line')
data_obj = pool.get('ir.model.data')
company_obj = user_obj.browse(cursor, uid, uid).company_id
company_addresses = company.partner_id.address_get(adr_pref=['contact'])

user_group_ids = [group.id for group in user.groups_id]
# group_jobs_financial
financial_group_id = data_obj.get_object_reference(cursor, uid, 'soller_jobs', 'group_jobs_financial')[1]

has_financial = financial_group_id in user_group_ids

line_types = dict(task_line_obj.fields_get(cursor, uid, context={'lang': user.context_lang})['type']['selection'])

if 'contact' in company_addresses:
    company_address = address_obj.read(cursor, uid, company_addresses['contact'])
else:
    company_address = {}

fax_str = ''

if company_address['fax']:
    fax_str = ', {} {}'.format(_('Fax'), company_address['fax'])
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" type="text/css" href="${addons_path}/soller_jobs/report/static/css/print.css"/>
        <link rel="stylesheet" type="text/css" href="${addons_path}/soller_jobs/report/static/css/extra.css"/>
    </head>
    <body>
    %for task in objects:
        <% subtotal_sum = 0 %>
        <div class="container">
            <div class="row top-padding-fix">
                <div class="col-xs-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>${_('Sender data')}</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <p><b><dfn>${company.partner_id.name.upper()}</dfn></b> (${company.partner_id.vat.upper()})</p>
                            <p>${'{}, {} - {}'.format(company_address['street'], company_address['zip'], company_address['city'].upper())}</p>
                            <p>${'{} - {}'.format(company_address['state_id'][1].upper(), company_address['country_id'][1].upper())}</p>
                            %if company.partner_id.website:
                                <p>${company.partner_id.website}</p>
                            %endif
                            %if company_address['email']:
                                <p>${company_address['email']}</p>
                            %endif
                            <p style="font-size: 8px;">${company.rml_footer1}</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-5 col-xs-offset-2">
                    <!-- Logo -->
                    <img style="width: 80%; margin: 0 auto;" src="${addons_path}/soller_jobs/report/static/img/vse_solo.jpg"/>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>${_('Task data')}</h4>
                        </div>
                        <div class="panel panel-body invoice-data">
                            <p><strong>${_('Project')}</strong>: (${task.project_id.code}) ${task.project_id.name}</p>
                            <p><strong>${_('Task number')}</strong>: ${task.name}</p>
                            <p><strong>${_('Task date')}</strong>: ${task.date}</p>
                            <p><strong>${_('Task type')}</strong>: ${task.type_id.name}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>${_('Task line details')}</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <table class="table-adjust-space">
                                <thead>
                                    <tr>
                                        <th class="table-cell-shrink">${_('Code')}</th>
                                        <th class="table-cell-expand">${_('Name')}</th>
                                        <th class="table-cell-shrink">${_('Type')}</th>
                                        <th class="table-cell-shrink text-right">${_('Quantity')}</th>
                                        %if has_financial:
                                            <th class="table-cell-shrink text-right">${_('Cost')}</th>
                                            <th class="table-cell-shrink text-right">${_('Subtotal')}</th>
                                        %endif
                                    </tr>
                                </thead>
                                <tfoot></tfoot>
                                <tbody>
                                    %for task_line in task.line_ids:
                                        <%
                                            subtotal_sum += task_line.price_subtotal
                                        %>
                                        <tr>
                                            <td class="table-cell-shrink">${task_line.product_id.code}</td>
                                            <td class="table-cell-expand">${task_line.product_id.name}</td>
                                            <td class="table-cell-shrink">${line_types[task_line.type]}</td>
                                            <td class="table-cell-shrink text-right amount">${formatLang(task_line.quantity, digits=2)}</td>
                                            %if has_financial:
                                                <td class="table-cell-shrink text-right amount">${formatLang(task_line.cost_price, digits=2)}</td>
                                                <td class="table-cell-shrink text-right amount">${formatLang(task_line.price_subtotal, digits=2)}</td>
                                            %endif
                                        </tr>
                                    %endfor
                                    <tr>
                                        <td class="table-cell-shrink"></td> <!-- Code -->
                                        <td class="table-cell-expand"></td> <!-- Name -->
                                        <td class="table-cell-shrink"></td> <!-- Type -->
                                        <td class="table-cell-shrink"></td> <!-- Quantity -->
                                        %if has_financial:
                                            <th class="table-cell-shrink">${_('Total')}</th> <!-- Cost -->
                                            <th class="table-cell-shrink text-right amount">${formatLang(subtotal_sum, digits=2)} €</th> <!-- Subtotal -->
                                        %endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            %if task.notes:
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default avoid-break">
                        <div class="panel-heading">
                            <h4>${_('Notes')}</h4>
                        </div>
                        <div class="panel-body invoice-data">
                            <p>${task.notes.replace('\n', '<br/>')}</p>
                        </div>
                    </div>
                </div>
            </div>
            %endif
            %if has_financial:
                <div class="row">
                    <div class="col-xs-4">
                        <div class="panel panel-default avoid-break">
                            <div class="panel-heading">
                                <h4>${_('Subtotal')}</h4>
                            </div>
                            <div class="panel-body">
                                <table class="table-adjust-space">
                                    <tbody>
                                        <%
                                            sort_func = lambda task: task.type
                                            sorted_lines = sorted(task.line_ids, key=sort_func)
                                        %>
                                        %for type, lines in groupby(sorted_lines, key=sort_func):
                                            <tr>
                                                <%
                                                    sum_subtotal = reduce(lambda x, y: x + y.price_subtotal, lines, 0)
                                                %>
                                                <th class="table-cell-expand text-left">${line_types[type]}</th>
                                                <td class="table-cell-shrink text-right">${formatLang(sum_subtotal, digits=2)} €</td>
                                            </tr>
                                        %endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            %endif
        </div>
    % if objects.index(task) + 1 != len(objects):
        <p class="page-break"></p>
    % endif
    %endfor
    </body>
</html>