# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
import zipfile
import base64
import tempfile

class SollerDescarrecs(osv.osv):

    _inherit = 'giscedata.descarrecs.descarrec'

    def action_get_cups(self, cursor, uid, ids, context=None):
        client_obj = self.pool.get('giscedata.descarrecs.descarrec.clients')
        polissa_obj = self.pool.get('giscedata.polissa')
        attachment_obj = self.pool.get('ir.attachment')

        for id in ids:
            search_params = [('descarrec_id', '=', id)]
            client_ids = client_obj.search(cursor, uid, search_params)
            read_fields = ['cups', 'policy']
            client_vals = client_obj.read(cursor, uid, client_ids, read_fields)

            comer_dict = {}
            zip_file_descriptor = tempfile.NamedTemporaryFile()

            zip_file = zipfile.ZipFile(zip_file_descriptor, 'w',
                                      compression=zipfile.ZIP_DEFLATED)
            for client in client_vals:
                if not client['policy']:
                    continue

                polissa = polissa_obj.browse(cursor, uid, client['policy'][0])
                comer_name = polissa.comercialitzadora.ref
                comer_dict.setdefault(comer_name, [])
                comer_dict[comer_name].append(client['cups'])

            for key, values in comer_dict.iteritems():
                try:
                    filename = "{}.txt".format(key)
                    file = tempfile.NamedTemporaryFile()

                    strvalues = '\n'.join(values)
                    file.write(strvalues)
                    file.flush()
                    zip_file.write(file.name, filename)

                except Exception, e:
                    raise osv.except_osv('error',
                        'Error in file generation of retailer {}'.format(key))

                finally:
                    file.close()

            zip_file.close()
            zip_file_descriptor.seek(0)
            vals = {
                'name': 'cups_list.zip',
                'datas': base64.b64encode(zip_file_descriptor.read()),
                'datas_fname': 'cups_list.zip',
                'res_model': 'giscedata.descarrecs.descarrec',
                'res_id': id
            }
            attachment_obj.create(cursor, uid, vals)
            print zip_file_descriptor.name
            zip_file_descriptor.close()


        return True

SollerDescarrecs()