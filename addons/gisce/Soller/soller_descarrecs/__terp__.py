# -*- coding: utf-8 -*-
{
    "name": "Soller descarrecs",
    "description": """Additional functionality for cut offs""",
    "version": "0-dev",
    "author": "Toni Llado",
    "category": "Soller",
    "depends":[
        "giscedata_descarrecs"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_descarrecs_view.xml"
    ],
    "active": False,
    "installable": True
}
