# -*- coding: utf-8 -*-

from osv import osv

from gestionatr.input.messages import message
from tools.translate import _


class SollerSwitching(osv.osv):
    _inherit = "giscedata.switching"

    def importar_xml(self, cursor, uid, data, fname, context=None):
        if not context:
            context = {}

        # Get the file type
        general_xml = message.Message(data)
        general_xml.parse_xml(validate=False)
        emisor = general_xml.get_codi_emisor
        sender = context.get('sender_code', '')

        if sender != emisor:
            raise Exception(_(u"The file xml creator is {} "
                              u"and you are {}").format(sender, emisor))

        return super(SollerSwitching, self).importar_xml(cursor, uid, data,
                                                         fname, context)


SollerSwitching()

