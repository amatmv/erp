# -*- coding: utf-8 -*-
{
    "name": "Soller Switching",
    "description": "Switching. Marketer change",
    "version": "0-dev",
    "author": "Pedro José Piquero Plaza",
    "category": "Soller",
    "depends": [
        "giscedata_switching",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}