# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import base64
import StringIO
import time
import datetime
import csv


class SollerWizardImportarLecturas(osv.osv_memory):

    _name = 'wizard.soller.importar.lecturas'

    def comprovar(self, cr, uid, ids, context=None):

        if not context:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')
        wizard = self.browse(cr, uid, ids[0])

        file = StringIO.StringIO(unicode(base64.b64decode(wizard.arxiu),
                                         'iso-8859-1'))
        reader = csv.reader(file, delimiter=';')
        total_abonados = []
        for row in reader:
            #$codabonado es la primera part de row[3]
            #$periodo es row[5];
            #$activa es row[6];
            #$reactiva es row[7];
            #$maximetro es row[8];
            if row[0] != 'initdate':
                #Buscamos el abonado de esta lectura
                abonat = row[3].split(' ')[0]
                if str(int(abonat)) not in total_abonados:
                    total_abonados.append(str(int(abonat)))
        #Cerquem els abonats per si no existeixen
        context.update({'active_test': False})
        notes = ''
        for abonat in total_abonados:
            search_params = [('name', '=', abonat)]
            polissa_ids = polissa_obj.search(cr, uid, search_params,
                                             context=context)
            if len(polissa_ids) != 1:
                notes += 'No s\'ha trobat l\'abonat %s\n' % abonat
            else:
                polissa = polissa_obj.browse(cr, uid, polissa_ids[0])
                if polissa.state in ('esborrany', 'validar',
                                     'modcontractual'):
                    notes += 'L\'abonat %s esta en estat %s\n' % (polissa.name,
                                                                polissa.state)

        if not notes:
            notes = 'Totes les comprovacions han estat correctes\n'
        wizard.write({'abonats': len(total_abonados),
                      'notes': notes,
                      'state': 'import'})

    def import_file(self, cr, uid, ids, context=None):

        if not context:
            context = {}
        context.update({'active_test': False,
                        'sync': False})

        wizard = self.browse(cr, uid, ids[0])
        polissa_obj = self.pool.get("giscedata.polissa")
        comptador_obj = self.pool.get("giscedata.lectures.comptador")
        periode_obj = self.pool.get("giscedata.polissa.tarifa.periodes")
        lectura_obj = self.pool.get("giscedata.lectures.lectura")
        lect_pot_obj = self.pool.get("giscedata.lectures.potencia")

        file = StringIO.StringIO(base64.b64decode(wizard.arxiu))
        reader = csv.reader(file, delimiter=';')
        lecturas = {}

        for row in reader:
            #$codabonado es la primera part de row[3]
            #$periodo es row[5];
            #$activa es row[6];
            #$reactiva es row[7];
            #$maximetro es row[8];
            if row[0] != 'initdate':
                #Buscamos el abonado de esta lectura
                abonat = row[3].split(' ')[0]
                search_params = [('name', '=', str(int(abonat)))]
                polissa_ids = polissa_obj.search(cr, uid, search_params,
                                                context=context)
                if not polissa_ids:
                    continue
                polissa = polissa_obj.browse(cr, uid, polissa_ids[0])
                if polissa.state in ('esborrany', 'validar', 'modcontractual'):
                    continue
                polissa_id = polissa.id
                #Buscamos el contador activo del abonado
                #o en el caso de baja el ultimo activo
                #que es el contador con la fecha de alta mayor
                search_params = [('polissa', '=', polissa_id)]
                comptador_id = comptador_obj.search(cr, uid, search_params,
                                                    offset=0,
                                                    limit=1,
                                                    order='data_alta desc',
                                                    context=context)[0]
                #guardamos la lectura
                if not comptador_id in lecturas:
                    lecturas[comptador_id] = {}
                    lecturas[comptador_id]['act'] = {}
                    lecturas[comptador_id]['react'] = {}
                    lecturas[comptador_id]['max'] = {}
                lecturas[comptador_id]['act']['P%s' % (row[5])] = row[6]
                lecturas[comptador_id]['react']['P%s' % (row[5])] = row[7]
                lecturas[comptador_id]['max']['P%s' % (row[5])] = (row[8],
                                                                   row[9])

        #Todas estas lecturas tienen origen telelectura (codi = 10)
        search_params = [('codi', '=', '10')]
        origen_id = self.pool.get("giscedata.lectures.origen").search(cr,
                                                        uid, search_params)[0]

        #Parseamos el diccionario de lecturas que hemos generado
        for comptador_id in lecturas.keys():
            comptador = comptador_obj.browse(cr, uid, comptador_id)
            #Para cada periodo de energia de la
            #tarifa vamos introduciendo las lecturas
            search_params = [('tipus', '=', 'te'),
                             ('tarifa', '=', comptador.polissa.tarifa.id)]
            periodes_ids = periode_obj.search(cr, uid, search_params)
            for periode in periode_obj.browse(cr, uid, periodes_ids):
                if periode.name in lecturas[comptador_id]['act']:
                    vals = {'name': wizard.data_lectura,
                        'periode': periode.id,
                        'tipus': 'A',
                        'comptador': comptador_id,
                        'observacions': 'Importacion automatica',
                        'origen_id': origen_id,
                        'lectura': lecturas[comptador_id]['act'][periode.name],
                        }
                else:
                    vals = {'name': wizard.data_lectura,
                            'periode': periode.id,
                            'tipus': 'A',
                            'comptador': comptador_id,
                            'observacions': 'Importacion automatica',
                            'origen_id': origen_id,
                            'lectura': 0,
                           }
                search_params = [
                        ('comptador.id', '=', comptador_id),
                        ('periode.id', '=', periode.id),
                        ('tipus', '=', 'A'),
                        ('name', '=', wizard.data_lectura),
                    ]
                lecturas_ids = lectura_obj.search(cr, uid, search_params)
                if not len(lecturas_ids):
                    lectura_creada_id = lectura_obj.create(cr, uid, vals,
                                                    context=context)

                if periode.name in lecturas[comptador_id]['react']:
                    vals = {
                    'name': wizard.data_lectura,
                    'periode': periode.id,
                    'tipus': 'R',
                    'comptador': comptador_id,
                    'observacions': 'Importacion automatica',
                    'origen_id': origen_id,
                    'lectura': lecturas[comptador_id]['react'][periode.name],
                    }
                else:
                    vals = {'name': wizard.data_lectura,
                            'periode': periode.id,
                            'tipus': 'R',
                            'comptador': comptador_id,
                            'observacions': 'Importacion automatica',
                            'origen_id': origen_id,
                            'lectura': 0,
                           }
                search_params = [
                        ('comptador.id', '=', comptador_id),
                        ('periode.id', '=', periode.id),
                        ('tipus', '=', 'R'),
                        ('name', '=', wizard.data_lectura),
                    ]
                lecturas_ids = lectura_obj.search(cr, uid, search_params)
                if not len(lecturas_ids):
                    lectura_creada_id = lectura_obj.create(cr, uid, vals,
                                                    context=context)

            #Para cada periodo de potencia de la tarifa
            #vamos introduciendo las lecturas
            search_params = [('tipus', '=', 'tp'),
                             ('tarifa', '=', comptador.polissa.tarifa.id)]
            periodes_ids = periode_obj.search(cr, uid, search_params)
            for periode in periode_obj.browse(cr, uid, periodes_ids):
                if periode.name in lecturas[comptador_id]['max']:
                    vals = {
                    'name': wizard.data_lectura,
                    'periode': periode.id,
                    'comptador': comptador_id,
                    'observacions': 'Importacion automatica',
                    'origen_id': origen_id,
                    'lectura': lecturas[comptador_id]['max'][periode.name][0],
                    'exces': lecturas[comptador_id]['max'][periode.name][1],
                    }
                else:
                    vals = {'name': wizard.data_lectura,
                            'periode': periode.id,
                            'comptador': comptador_id,
                            'observacions': 'Importacion automatica',
                            'origen_id': origen_id,
                            'lectura': 0,
                            'exces': 0,
                           }
                search_params = [
                        ('comptador.id', '=', comptador_id),
                        ('periode.id', '=', periode.id),
                        ('name', '=', wizard.data_lectura),
                    ]
                lecturas_pot_ids = lect_pot_obj.search(cr, uid, search_params)
                if not len(lecturas_pot_ids):
                    lectura_creada_id = lect_pot_obj.create(cr, uid, vals,
                                                    context=context)

        wizard.write({'state': 'end'})

    _columns = {
        'data_lectura': fields.date('Data lectura', required=True),
        'arxiu': fields.binary('Arxiu', required=True, filters=None),
        'abonats': fields.integer('Abonats totals', readonly=True),
        'notes': fields.text('Notes', readonly=True),
        'state': fields.char('Estat', size=64, required=False, readonly=True),

    }

    _defaults = {
        'state': lambda *a: 'init',

    }

SollerWizardImportarLecturas()
