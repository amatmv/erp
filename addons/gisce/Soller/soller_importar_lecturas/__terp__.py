# -*- coding: utf-8 -*-
{
    "name": "Soller importar lectures",
    "description": """
Importació de lectures de clients de més de 50kW
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_importar_lecturas_view.xml"
    ],
    "active": False,
    "installable": True
}
