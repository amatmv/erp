# -*- coding: utf-8 -*-
{
    "name": "Soller BI",
    "description": """
Accés a urls informes:
    * Fitxa de contracte (Pòlissa)
    
    Requereix la llibreria pycrypto
    pip install pycrypto
    """,
    "version": "0-dev",
    "author": "Bartomeu Miro Mateu",
    "category": "Soller",
    "depends":[
        "giscedata_polissa",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
