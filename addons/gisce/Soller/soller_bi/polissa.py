# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.misc import cache
from base64 import b64encode, b64decode, encodestring
from Crypto import Random
from Crypto.Cipher import AES
from urllib import quote
from datetime import datetime
from tools import config
import json


class AESCipher(object):
    # Thanks to
    # http://stackoverflow.com/questions/12524994/\
    # encrypt-decrypt-using-pycrypto-aes-256
    def __init__(self, key, BS=32):
        self.key = key
        self.bs = BS

    def encrypt(self, raw):
        BS = self.bs
        pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
        raw = pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        unpad = lambda s: s[0:-ord(s[-1])]
        return unpad(cipher.decrypt(enc[16:]))


def gen_token(username, password, date, cipher_password):
    """ Date is in format YYYY-mm-dd and password must be 32 char long """
    aes = AESCipher(cipher_password)
    data = [username, password, date]
    token = quote(aes.encrypt(json.dumps(data)), '')
    return token


class SollerBIPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'
    
    def _get_url_fitxa(self, cursor, uid, ids, name, arg, context=None):
        config_obj = self.pool.get('res.config')
        cups_obj = self.pool.get('giscedata.cups.ps')
        user_obj = self.pool.get('res.users')
        
        bi_domain = config_obj.get(cursor, uid, 'bi_domain',
                                   'intranet.grupelgas.es')
        base_url = "https://%s/customer-report/%s/%s?token=%s"
        
        if not bi_domain:
            return dict([(id, '') for id in ids])

        if ids:
            polissa = self.browse(cursor, uid, ids[0])
            if polissa.cups:
                whereiam = cups_obj.whereiam(cursor, uid, polissa.cups.id)
            else:
                whereiam = ''
        
        user = user_obj.browse(cursor, uid, uid)
        today = datetime.now().strftime('%Y-%m-%d')
        token = gen_token(user.login, user.password, today,
                          config.options['token_key'])
        
        res = {}
        for id in ids:
            polissa = self.browse(cursor, uid, id)
            if whereiam:
                res[id] = base_url % (bi_domain, whereiam, polissa.name, token)
            else:
                res[id] = 'N/D'

        return res

    _columns = {
        'url_fitxa': fields.function(_get_url_fitxa,
                             method=True, type='char',
                             size=250,
                             string='Fitxa',
                             readonly=True
                     ),
    }

SollerBIPolissa()
