# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
# Joan Grande <jgrande@el-gas.es>, 2017, 2018.
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2018-03-02 12:06\n"
"PO-Revision-Date: 2018-02-27 09:35+0000\n"
"Last-Translator: Joan Grande <jgrande@el-gas.es>\n"
"Language-Team: Catalan (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ca_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: switching_f1
#: field:switching.f1.header,power_uom_factor:0
msgid "Power factor"
msgstr "Factor potència"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1563
#: code:addons/switching_f1/switching_f1.py:1628
#, python-format
msgid "There are a read for date {} in period {}"
msgstr "Hi ha una lectura per a la data {} en el període {}"

#. module: switching_f1
#: field:switching.f1.measure,consumption:0
msgid "Consumption"
msgstr "Consum"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:421
#, python-format
msgid "There are more than one rental amounts"
msgstr "Hi ha més d'un preu de lloguer"

#. module: switching_f1
#: field:switching.f1.measure,meter_name:0
msgid "Meter name"
msgstr "Nom comptador"

#. module: switching_f1
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Nom de model invàlid en la definició de l'acció."

#. module: switching_f1
#: selection:switching.f1.header,rectifier_type:0
msgid "Regularizing"
msgstr "Regularitzadora"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1144
#, python-format
msgid "The policy {} have active two or more meters"
msgstr "La pòlissa {} té actius dos o més comptadors"

#. module: switching_f1
#: view:switching.f1.header:0 field:switching.f1.header,measure_ids:0
msgid "Measures"
msgstr "Mesures"

#. module: switching_f1
#: model:ir.actions.act_window,name:switching_f1.action_switching_f1_error
msgid "Switching f1 errors"
msgstr "Errors switching f1"

#. module: switching_f1
#: field:switching.f1.header,rental_meter_price:0
msgid "Rental meter price"
msgstr "Preu lloguer del comptador"

#. module: switching_f1
#: field:switching.f1.measure,adjustment_origin:0
msgid "Adjustment origin"
msgstr "Origen de l'ajust"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1885
#, python-format
msgid "Excess for date {} in period {} not found"
msgstr "No trobat l'excés de potència per a la data {} en el periode {}"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:589
#, python-format
msgid ""
"Contracted power {} and invoiced power {} not equals and power invoicing is "
"not for maximeter"
msgstr "La potència contractada {} i la potència facturada {} no són iguales i la facturació de la potència no és per maxímetre"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:722
#, python-format
msgid "Rectified invoice {} not found"
msgstr "Factura rectificada {} no trobada"

#. module: switching_f1
#: field:switching.f1.concept,end_date:0 field:switching.f1.header,end_date:0
#: field:switching.f1.measure,end_date:0
msgid "End date"
msgstr "Data fi"

#. module: switching_f1
#: field:switching.f1.measure,turn:0
msgid "Turn"
msgstr "Volta"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:745
#: field:switching.f1.concept,amount:0
#, python-format
msgid "Amount"
msgstr "Import"

#. module: switching_f1
#: model:ir.model,name:switching_f1.model_switching_f1_measure
msgid "switching.f1.measure"
msgstr "switching.f1.measure"

#. module: switching_f1
#: view:switching.f1.concept:0
msgid "F1 concept"
msgstr "Concepte F1"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1356
#, python-format
msgid "Rectifier type {} not implemented"
msgstr "Tipo rectificadora {} no implementat"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:641
#, python-format
msgid ""
"The energy price {} is not equals to erp price {} for rate {} in period {}"
msgstr "El preu de l'energia {} no és igual al preu de l'erp {} per a la tarifa {} en el període {}"

#. module: switching_f1
#: field:switching.f1.header,tax_amount:0
msgid "Tax amount"
msgstr "Import tases"

#. module: switching_f1
#: model:ir.model,name:switching_f1.model_wizard_f1_reimport
msgid "wizard.f1.reimport"
msgstr "wizard.f1.reimport"

#. module: switching_f1
#: selection:switching.f1.measure,type:0
msgid "Active output"
msgstr "Sortida activa"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Reads"
msgstr "Lectures"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1594
#, python-format
msgid "There are not a read for date {} in period {}"
msgstr "No hi ha una lectura a la data {} en el periode {}"

#. module: switching_f1
#: field:switching.f1.measure,end_read:0
msgid "End read"
msgstr "Lectura fi"

#. module: switching_f1
#: selection:switching.f1.header,rectifier_type:0
#: field:switching.f1.measure,adjustment:0
msgid "Adjustment"
msgstr "Ajust"

#. module: switching_f1
#: selection:switching.f1.measure,type:0
msgid "Reactive R2"
msgstr "Reactiva R2"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1890
#, python-format
msgid "Excess for date {} in period {} not equals"
msgstr "L'excés de potència per a la data {} en el periode {} no és igual"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1285
#, python-format
msgid "There are more than one meter associated for policy {}"
msgstr "Hi ha més d'un comptador associat a la pòlissa {}"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:520
#, python-format
msgid "The power price {} is not equals to erp power price  {} for period {}"
msgstr "El preu de la potència {} no és igual al preu de la potència de l'erp {} per al període {}"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:565
#, python-format
msgid ""
"Contracted power {} not equals to erp contracted power {} for period {}"
msgstr "La potència contractada {} no és igual a la potència contractada de l'erp {} per al periode {} "

#. module: switching_f1
#: model:ir.actions.act_window,name:switching_f1.action_open_contract_f1_invoice
msgid "F1 invoices"
msgstr "Factures F1"

#. module: switching_f1
#: view:wizard.switching.import:0
msgid "Select import file to load"
msgstr "Seleccionar el fitxer d'importació a carregar"

#. module: switching_f1
#: field:switching.f1.measure,start_read:0
msgid "Start read"
msgstr "Lectura inici"

#. module: switching_f1
#: field:switching.f1.header,rental_amount:0
msgid "Rental amount"
msgstr "Import lloguer"

#. module: switching_f1
#: model:ir.module.module,description:switching_f1.module_meta_information
msgid ""
"\n"
"    Import switching invoices from F1 files\n"
"    "
msgstr "\n    Importació de factures de switching des de fitxers F1\n    "

#. module: switching_f1
#: field:wizard.switching.import,state:0
msgid "State"
msgstr "Estat"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:234
#, python-format
msgid "The invoice number {} is duplicated"
msgstr "El número de factura {} està duplicat"

#. module: switching_f1
#: field:switching.f1.concept,start_date:0
#: field:switching.f1.header,start_date:0
#: field:switching.f1.measure,start_date:0
msgid "Start date"
msgstr "Date inici"

#. module: switching_f1
#: field:switching.f1.header,cups_id:0
msgid "CUPS"
msgstr "CUPS"

#. module: switching_f1
#: field:switching.f1.header,invoice_type:0 field:switching.f1.measure,type:0
msgid "Type"
msgstr "Tipus"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1166
#, python-format
msgid ""
"The last read date {} of the old meter {} must equals to start measure date "
"{}"
msgstr "La darrera data de lectura {} de l'antic comptador {} ha de ser igual a la data de la mesura inicial {}"

#. module: switching_f1
#: view:switching.f1.measure:0
msgid "F1 measure"
msgstr "Mesura F1"

#. module: switching_f1
#: field:switching.f1.header,base_amount:0
msgid "Base amount"
msgstr "Import base"

#. module: switching_f1
#: model:ir.actions.act_window,name:switching_f1.action_reimport_f1
msgid "F1 reimport"
msgstr "Reimportar f1"

#. module: switching_f1
#: field:switching.f1.header,check_consumption_reads:0
msgid "Consumption reads"
msgstr "Lectures per consum"

#. module: switching_f1
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: switching_f1
#: field:switching.f1.header,energy_amount:0
msgid "Energy amount"
msgstr "Import energia"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:85
#, python-format
msgid "Correct validation"
msgstr "Validació correcte"

#. module: switching_f1
#: selection:switching.f1.measure,type:0
msgid "Excess power"
msgstr "Excés potència"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Create invoice"
msgstr "Crear factura"

#. module: switching_f1
#: field:switching.f1.header,total_amount:0
msgid "Total amount"
msgstr "Import total"

#. module: switching_f1
#: field:wizard.f1.reimport,info:0 field:wizard.switching.import,info:0
msgid "Info"
msgstr "Informació"

#. module: switching_f1
#: selection:switching.f1.measure,type:0
msgid "Reactive R3"
msgstr "Reactiva R3"

#. module: switching_f1
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: switching_f1
#: selection:switching.f1.measure,type:0
msgid "Reactive R1"
msgstr "Reactiva R1"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Reload readings and concepts"
msgstr "Recarregar lectures i conceptes"

#. module: switching_f1
#: selection:switching.f1.measure,type:0
msgid "Reactive R4"
msgstr "Reactiva R4"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Power reads"
msgstr "Lectures de potència"

#. module: switching_f1
#: selection:switching.f1.header,rectifier_type:0
msgid "Adjustment with substituent"
msgstr "Ajust amb sustituent"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1836
#, python-format
msgid ""
"Consumption is not correct for measure in date {} period {} and type {}"
msgstr "El consum no és correcte per a la mesura en la data {} periode {} i tipus {}"

#. module: switching_f1
#: model:ir.model,name:switching_f1.model_switching_f1_concept
msgid "switching.f1.concept"
msgstr "switching.f1.concept"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:754
#, python-format
msgid "Amount without taxes"
msgstr "Import sense tases"

#. module: switching_f1
#: model:ir.model,name:switching_f1.model_switching_f1_header
msgid "switching.f1.header"
msgstr "switching.f1.header"

#. module: switching_f1
#: selection:switching.f1.header,rectifier_type:0
msgid "Rectifier"
msgstr "Rectificadora"

#. module: switching_f1
#: field:switching.f1.header,rectifier_invoice_id:0
msgid "Rectified invoice"
msgstr "Factura rectificada"

#. module: switching_f1
#: selection:switching.f1.measure,type:0
msgid "Active input"
msgstr "Entrada activa"

#. module: switching_f1
#: field:switching.f1.error,message:0
msgid "Message"
msgstr "Missatge"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1657
#, python-format
msgid "Power read for date {} in period {} is duplicated"
msgstr "La lectura de potència per a la data {} en el periode {} està duplicada"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:249
#, python-format
msgid "The cups {} is not valid"
msgstr "La cups {} no és vàlida"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1829
#, python-format
msgid "Measure for date {} in period {} not equals"
msgstr "Les mesures per a la data {} en el periode {} no són iguals"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Totals"
msgstr "Totals"

#. module: switching_f1
#: selection:switching.f1.header,invoice_type:0
msgid "Other invoice"
msgstr "Altre factura"

#. module: switching_f1
#: model:ir.ui.menu,name:switching_f1.view_switching_f1_error
msgid "Switching f1 error"
msgstr "Error switching f1"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Consumptions"
msgstr "Consums"

#. module: switching_f1
#: view:wizard.f1.reimport:0 view:wizard.switching.import:0
msgid "Import"
msgstr "Importació"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:789
#, python-format
msgid "F1 invoice {} is not open"
msgstr "La factura F1 {} no està oberta"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1327
#: code:addons/switching_f1/switching_f1.py:1338
#: code:addons/switching_f1/switching_f1.py:1349
#, python-format
msgid "Rectifier invoice not found"
msgstr "Factura rectificativa no trobada"

#. module: switching_f1
#: view:wizard.f1.reimport:0 view:wizard.switching.import:0
msgid "F1 import"
msgstr "Importació F1"

#. module: switching_f1
#: field:switching.f1.header,energy_consumption:0
msgid "Energy kWh"
msgstr "Energia KWh"

#. module: switching_f1
#: model:ir.model,name:switching_f1.model_switching_f1_error
msgid "switching.f1.error"
msgstr "switching.f1.error"

#. module: switching_f1
#: field:switching.f1.header,extra_line_ids:0
#: field:switching.f1.header,meter_ids:0
#: field:switching.f1.header,power_read_ids:0
#: field:switching.f1.header,read_ids:0
#: field:switching.f1.header,xml_attachment_id:0
msgid "unknown"
msgstr "desconegut"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Readings for consumption"
msgstr "Lectures per consum"

#. module: switching_f1
#: model:ir.actions.act_window,name:switching_f1.action_open_f1_invoice
msgid "F1 invoice"
msgstr "Factura F1"

#. module: switching_f1
#: field:switching.f1.measure,end_origin_id:0
msgid "End origin"
msgstr "Origen fi"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:806
#: code:addons/switching_f1/switching_f1.py:823
#, python-format
msgid "Not found Invoice line with amount {}"
msgstr "No s'ha trobat la línia de factura amb import {}"

#. module: switching_f1
#: field:switching.f1.header,power_amount:0
msgid "Power amount"
msgstr "Import potència"

#. module: switching_f1
#: field:switching.f1.header,other_amount:0
msgid "Other amount"
msgstr "Altre import"

#. module: switching_f1
#: field:switching.f1.header,error:0
msgid "Error"
msgstr "Error"

#. module: switching_f1
#: model:account.journal,name:switching_f1.journal_f1
msgid "Factures Proveïdor F1"
msgstr "Factures Proveïdor F1"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Validate"
msgstr "Validar"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "F1"
msgstr "F1"

#. module: switching_f1
#: field:switching.f1.concept,code:0 field:switching.f1.error,code:0
msgid "Code"
msgstr "Codi"

#. module: switching_f1
#: field:switching.f1.measure,period:0
msgid "Period"
msgstr "Periode"

#. module: switching_f1
#: code:addons/switching_f1/wizard/wizard_switching_import.py:68
#, python-format
msgid "File imported correctly"
msgstr "Fitxer importat correctament"

#. module: switching_f1
#: field:switching.f1.header,version:0
msgid "Version"
msgstr "Versió"

#. module: switching_f1
#: view:wizard.f1.reimport:0 view:wizard.switching.import:0
msgid "Cancel"
msgstr "Cancel·lar"

#. module: switching_f1
#: view:wizard.switching.import:0
msgid "Close"
msgstr "Tancar"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1855
#, python-format
msgid "Maximeter for date {} in period {} not found"
msgstr "No s'ha trobat el maxímetre per a la data {} en el periode {}"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Created meters"
msgstr "Comptadors creats"

#. module: switching_f1
#: field:switching.f1.header,destination_company_id:0
msgid "Destination company"
msgstr "Companyia destí"

#. module: switching_f1
#: field:switching.f1.header,reactive_amount:0
msgid "Reactive amount"
msgstr "Import reactiva"

#. module: switching_f1
#: field:switching.f1.header,origin_company_id:0
msgid "Origin company"
msgstr "Companyia origen"

#. module: switching_f1
#: code:addons/switching_f1/wizard/wizard_f1_reimport.py:63
#, python-format
msgid "Are you sure f1 reimport?"
msgstr "Està segur de reimportar l'f1?"

#. module: switching_f1
#: selection:switching.f1.header,rectifier_type:0
msgid "Rectifier without Adjustment"
msgstr "Rectificadora amb ajust"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Amounts"
msgstr "Imports"

#. module: switching_f1
#: selection:switching.f1.header,invoice_type:0
msgid "ATR invoice"
msgstr "Factura ATR"

#. module: switching_f1
#: field:switching.f1.error,create_date:0
msgid "Create date"
msgstr "Data creació"

#. module: switching_f1
#: field:switching.f1.header,polissa_id:0
msgid "Policy"
msgstr "Pòlissa"

#. module: switching_f1
#: code:addons/switching_f1/wizard/wizard_f1_reimport.py:28
#, python-format
msgid "Cannot reimport a xml with cups or policy informed"
msgstr "No es pot reimportar un xml amb un CUPS o una pòlissa informats"

#. module: switching_f1
#: model:ir.module.module,shortdesc:switching_f1.module_meta_information
msgid "F1 Switching"
msgstr "Switching F1"

#. module: switching_f1
#: field:switching.f1.concept,description:0
msgid "Description"
msgstr "Descripció"

#. module: switching_f1
#: selection:switching.f1.header,rectifier_type:0
msgid "Complementary"
msgstr "Complementària"

#. module: switching_f1
#: selection:switching.f1.measure,type:0
msgid "Max"
msgstr "Max"

#. module: switching_f1
#: model:ir.actions.act_window,name:switching_f1.action_switching_f1
#: model:ir.ui.menu,name:switching_f1.view_switching_f1
msgid "Switching f1"
msgstr "Switching F1"

#. module: switching_f1
#: code:addons/switching_f1/wizard/wizard_switching_import.py:22
#, python-format
msgid ""
"It begins import process in background. When finishes you will receive a "
"request."
msgstr "Comença el procés d'importació en segon pla."

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1122
#, python-format
msgid "Adjustment field not implemented"
msgstr "Camp ajust no implementat"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:841
#, python-format
msgid "There are not meter associated"
msgstr "No hi ha comptador associat"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:472
#, python-format
msgid "The energy total amount {} is not equals to energy breakdown amount {}"
msgstr "El preu total de l'energia {} no és igual al preu del desglosament {}"

#. module: switching_f1
#: model:ir.actions.act_window,name:switching_f1.action_switching_import
#: model:ir.ui.menu,name:switching_f1.menu_wizard_switching_import
msgid "Switching import"
msgstr "Importació switching"

#. module: switching_f1
#: field:switching.f1.error,name:0
msgid "Name"
msgstr "Nom"

#. module: switching_f1
#: view:switching.f1.error:0
msgid "F1 error"
msgstr "Error F1"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1815
#, python-format
msgid "Measure for date {} in period {} not found"
msgstr "No s'ha trobat la mesura per la data {} en el periode {}"

#. module: switching_f1
#: field:switching.f1.header,invoice_date:0
msgid "Invoice date"
msgstr "Data factura"

#. module: switching_f1
#: field:switching.f1.header,reactive_consumption:0
msgid "Reactive kVarh"
msgstr "Reactiva KVarh"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1867
#, python-format
msgid "Maximeter for date {} in period {} not equals"
msgstr "el maxímetre per a la data {} en el periode {} no és igual"

#. module: switching_f1
#: field:switching.f1.measure,start_origin_id:0
msgid "Start origin"
msgstr "Origen inici"

#. module: switching_f1
#: view:switching.f1.header:0 field:switching.f1.header,concept_ids:0
msgid "Concepts"
msgstr "Conceptes"

#. module: switching_f1
#: view:switching.f1.header:0
msgid "Extra lines"
msgstr "Línees extra"

#. module: switching_f1
#: field:switching.f1.header,power_excess_amount:0
msgid "Power excess amount"
msgstr "Import excés de potència"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1494
#, python-format
msgid ""
"Incorrect maximeter for date {} in period {}, there are not energy "
"consumption"
msgstr "Maximetre incorrecte per a la data {} en el periode {}, no hi ha consum d'energia"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:779
#, python-format
msgid "There are more than one F1 invoices associated"
msgstr "Hi ha més de una factura F1 associada"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:1381
#, python-format
msgid "Measure for start date {} in period {} not found"
msgstr "No s'ha trobat la mesura per la data inicial {} en el periode {}"

#. module: switching_f1
#: model:ir.model,name:switching_f1.model_wizard_switching_import
msgid "wizard.switching.import"
msgstr "wizard.switching.import"

#. module: switching_f1
#: field:switching.f1.concept,switching_f1_id:0
#: field:switching.f1.error,switching_f1_id:0
#: field:switching.f1.header,invoice_number:0
#: field:switching.f1.measure,switching_f1_id:0
msgid "Invoice number"
msgstr "Número de factura"

#. module: switching_f1
#: field:switching.f1.measure,meter_id:0
msgid "Meter"
msgstr "Comptador"

#. module: switching_f1
#: field:switching.f1.header,rectifier_type:0
msgid "Rectifier type"
msgstr "Tipo rectificadora"

#. module: switching_f1
#: field:switching.f1.header,rate_id:0
msgid "Rate"
msgstr "Tarifa"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:261
#, python-format
msgid "The policy with cups id {} is not valid"
msgstr "La pòlissa amb el cups id {} no és vàlida"

#. module: switching_f1
#: code:addons/switching_f1/switching_f1.py:383
#, python-format
msgid "There are more than one policies associates to cups {}"
msgstr "Hi ha més d'una pòlissa associada a la cups {}"

#. module: switching_f1
#: field:wizard.switching.import,file:0
msgid "File"
msgstr "Fitxer"

#. module: switching_f1
#: model:ir.actions.act_window,name:switching_f1.action_open_f1_errors
msgid "F1 errors"
msgstr "Errors F1"
