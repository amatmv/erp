# -*- coding: utf-8 -*-

CODE_WITHOUT_TAX = '06'

# Returns period code
LINK_T42 = { '01': 'P1',  # Punta + Llano
             '03': 'P2',  # Valle
             '10': 'P1',  # Totalizador
             '21': 'P1',  # P1 Tarifes: 004, 006
             '22': 'P2',  # P2 Tarifes: 004, 006
             '31': 'P1',  # P1 Tarifa 003
             '32': 'P2',  # P2 Tarifa 003
             '33': 'P3',  # P3 Tarifa 003
             '41': 'P1',  # P1 Tarifa 011
             '42': 'P2',  # P2 Tarifa 011
             '43': 'P3',  # P3 Tarifa 011
             '61': 'P1',  # Periodo 1 Tarifes: 003, 011, 012 - 016
             '62': 'P2',  # Periodo 2 Tarifes: 003, 011, 012 - 016
             '63': 'P3',  # Periodo 3 Tarifes: 003, 011, 012 - 016
             '64': 'P4',  # Periodo 4 Tarifes: 003, 011, 012 - 016
             '65': 'P5',  # Periodo 5 Tarifes: 003, 011, 012 - 016
             '66': 'P6',  # Periodo 6 Tarifes: 003, 011, 012 - 016
             '81': 'P1',  # P1 Tarifa 007
             '82': 'P2',  # P2 Tarifa 007
             '83': 'P3'}  # P3 Tarifa 007

# TABLE_44 = {
#     '10': 'Telemedida',
#     '11': 'Telemedida corregida',
#     '20': 'TPL',
#     '21': 'TPL corregida',
#     '30': 'Visual',
#     '31': 'Visual corregida',
#     '40': 'Estimada',
#     '50': 'Autolectura',
#     '99': 'Sin lectura',
# }

TABLE_101 = { '01': 'Normal',
              '02': 'Modificación de Contrato',
              '03': 'Baja de Contrato',
              '04': 'Derechos de Contratacion',
              '05': 'Deposito de garantía',
              '06': 'Inspección',
              '07': 'Atenciones (verificaciones, .)',
              '08': 'Indemnizacion',
              '09': 'Intereses de demora',
              '10': 'Servicios'}

TABLE_103 = {
    '01': 'Indemnización',
    '02': 'Derechos de extensión',
    '03': 'Derechos de acceso',
    '04': 'Derechos de enganche',
    '05': 'Derechos de verificación',
    '06': 'Depósito de garantía',
    '07': 'Gastos de anulación de contratos',
    '08': 'Actuaciones en la medida',
    '09': 'Reparametrización de la medida',
    '10': '2º Cambio de Comercializador en el periodo de un año',
    '11': 'Intereses de demora',
    '12': 'Verificación de Equipos de Medida',
    '13': 'Derechos de Reconexión',
    '14': 'Varios',
    '15': 'Gastos de acometida',
    '16': 'Abonos',
    '17': 'Abono por calidad de suministro',
    '18': 'Abono por calidad individual',
    '19': 'Coste de reposición',
    '20': 'Supervisión instalaciones cedidas',
    '32': 'Coste de corte a petición de comercializador',
    '33': 'Coste de reconexión a petición de comercializador',
    '40': 'Recargo fraccionado por refacturación (Orden IET/843/2012) del cuarto trimestre de 2011',
    '41': 'Recargo fraccionado por refacturación (Orden IET/843/2012) del primer trimestre de 2012',
    '42': 'Recargo fraccionado por refacturación (Orden IET/843/2012) de abril de 2012',
    '43': 'Cargo Fijo Autoconsumo',
    '44': 'Cargo Variable Autoconsumo',
    '45': 'Exención Cargo Fijo Autoconsumo',
    '46': 'Exención Cargo Variable Autoconsumo',
    '47': 'Derechos de acometida de generación',
    '48': 'Suplemento territorial',
}

TABLE_106 = {
    '01': u'Verificación equipo de medida',
    '02': u'Avería en contador',
    '03': u'Avería en Trafo de Tensión',
    '04': u'Avería en Trafo de intensidad',
    '05': u'Desbordamiento del Registrador',
    '06': u'Problemas en la sincronización del registrador',
    '07': u'Pérdida de alimentación del registrador',
    '08': u'Manipulación de equipos',
    '09': u'Servicio Directo (sin EM)',
    '10': u'Punto de medida inaccesible',
    '11': u'Punto de medida ilocalizable',
    '99': u'Otros',
}