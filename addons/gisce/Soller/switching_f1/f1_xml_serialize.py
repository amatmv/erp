# -*- coding: utf-8 -*-

from datetime import datetime

from xml2py import dict_loads

from defs import *
from tools import config


class F1Xml(object):

    def __init__(self, xml):
        f1_dict = dict_loads(xml)

        if 'Version' in f1_dict['MensajeFacturacion']['Cabecera']:
            self.version = f1_dict['MensajeFacturacion']['Cabecera']['Version']
        else:
            self.version = config.get('f1_version', '1.0')

        self.f1_dict = f1_dict
        self.f1_header = f1_dict['MensajeFacturacion']['Cabecera']

    def serialize_xml(self):
        res = []
        invoices = self.f1_dict['MensajeFacturacion']['Facturas']
        for key, value in invoices.iteritems():

            if key == 'FacturaATR':
                for invoice in self.get_list(value):
                    if self.version == '1.0':
                        serializer = AtrInvoiceSerializerV1(self.f1_header,
                                                            invoice,
                                                            self.version)
                    else:
                        serializer = AtrInvoiceSerializer(self.f1_header,
                                                          invoice,
                                                          self.version)
                    res.append(serializer.serialize_invoice())

            elif key == 'OtrasFacturas':
                for invoice in self.get_list(value):
                    if self.version == '1.0':
                        serializer = OtherInvoiceSerializerV1(self.f1_header,
                                                              invoice,
                                                              self.version)
                    else:
                        serializer = OtherInvoiceSerializer(self.f1_header,
                                                            invoice,
                                                            self.version)
                    res.append(serializer.serialize_invoice())

        return res

    def get_list(self, obj):
        if not isinstance(obj, list):
            return [obj]

        return obj


class AtrInvoiceSerializer(object):

    def __init__(self, header, body, version):
        self.header = header
        self.body = body
        self.version = version

    def get_list(self, obj):
        if not isinstance(obj, list):
            return [obj]

        return obj

    def serialize_invoice(self):
        '''
            Serialize atr invoices node
        '''
        result = {
            'type': 'ATR',
        }

        # Header data
        self.serialize_header_data(
            self.body['DatosGeneralesFacturaATR'], result)

        # Total amount data
        self.serialize_amount_data(self.body, result)

        result.update({'lines': {}})
        # Power
        self.serialize_power(self.body['Potencia'], result)

        # Energy
        if 'EnergiaActiva' in self.body:
            self.serialize_energy(self.body['EnergiaActiva'], result)

        # Reactive energy
        if 'EnergiaReactiva' in self.body:
            self.serialize_reactive_energy(self.body['EnergiaReactiva'], result)

        # Excess power
        if 'ExcesoPotencia' in self.body:
            self.serialize_excess_power(self.body['ExcesoPotencia'], result)

        # Others
        if 'ConceptoIVA' in self.body:
            self.serialize_other_lines(self.body['ConceptoIVA'], result)

        # Reinvoicing
        if 'Refacturaciones' in self.body:
            self.serialize_reinvoicing(self.body['Refacturaciones'], result)
        # Measures
        if 'Medidas' in self.body:
            result.update({
                'measures': [],
            })
            self.serialize_measures(self.body['Medidas'], result)

        # CCH
        if 'PeriodoCCH' in self.body:
            self.serialize_cch(self.body['PeriodoCCH'], result)

        return result

    def serialize_header_data(self, gen_data, result):
        '''
              Serialize header data node
        '''

        header = {
            'type': 'ATR',
            'company_origin_code': (
                self.header['CodigoREEEmpresaEmisora'].strip()),
            'company_destination_code': (
                self.header['CodigoREEEmpresaDestino'].strip())
        }

        header.update({
            'version': self.version,
            'invoice_number': (gen_data['DatosGeneralesFactura']
                               ['NumeroFactura'].strip()),
            'cups': gen_data['DireccionSuministro']['CUPS'].strip(),
            'invoice_type': TABLE_101[
                gen_data['DatosGeneralesFactura']['TipoFactura']],
            'invoice_date': gen_data['DatosGeneralesFactura']['FechaFactura'],
            'rate_code': gen_data['DatosFacturaATR']['CodigoTarifa'].strip(),
            'start_date': (gen_data['DatosFacturaATR']['Periodo']
                           ['FechaDesdeFactura']),
            'end_date': (gen_data['DatosFacturaATR']['Periodo']
                         ['FechaHastaFactura']),
            'invoicing_type': (
                gen_data['DatosFacturaATR']['TipoFacturacion'].strip()),
            'rectifier_type': (gen_data['DatosGeneralesFactura']
                               ['IndicativoFacturaRectificadora']),
        })

        if 'NumeroFacturaRectificada' in gen_data['DatosGeneralesFactura']:
            header.update({
                'rectifier_invoice': (gen_data['DatosGeneralesFactura']
                                      ['NumeroFacturaRectificada'].strip()),
            })

        if 'Observaciones' in gen_data['DatosGeneralesFactura']:
            header.update({
                'remarks': gen_data['DatosGeneralesFactura']['Observaciones'],
            })

        result.update({'header': header})

    def serialize_amount_data(self, invoice, result):
        gen_data = invoice['DatosGeneralesFacturaATR']

        total_base = 0.0
        total_tax = 0.0
        for iva in self.get_list(invoice['IVA']):
            base = float(iva['BaseImponible'])
            if base != 0:
                total_base += base
                total_tax += float(iva['Importe'])

        totals = {
            'total_amount': float(gen_data['DatosGeneralesFactura']
                            ['ImporteTotalFactura']),

            'base_amount': total_base,
            'tax_amount': total_tax,
            'rental_amount': 0,
        }

        # Rent data
        if 'Alquileres' in invoice:
            totals.update({
                'rental_amount': float(invoice['Alquileres']
                                       ['ImporteFacturacionAlquileres']),
            })

        result.update({'totals': totals})



    def serialize_power(self, power, result):
        '''
            Serialize power node
        '''
        result['totals'].update({
            'total_power': float(power['ImporteTotalTerminoPotencia']),
        })

        power_lines = []
        for power_term in self.get_list(power['TerminoPotencia']):
            for idx, period in enumerate(self.get_list(power_term['Periodo'])):
                invoiced_power = float(period['PotenciaAFacturar'])
                if invoiced_power != 0:
                    power_lines.append({
                        'period': 'P{}'.format(idx + 1),
                        'start_date': power_term['FechaDesde'],
                        'end_date': power_term['FechaHasta'],
                        'contracted_power': int(period['PotenciaContratada']),
                        'maximeter': int(period['PotenciaMaxDemandada']),
                        'price': float(period['PrecioPotencia']),
                        'invoiced_power': invoiced_power,
                    })
        result['lines'].update({
            'power': power_lines,
        })

    def serialize_energy(self, energy, result):
        '''
            Serialize active energy node
        '''
        energy_lines = []
        total_consumption = 0.0
        ocsum_code_rate = result['header']['rate_code']
        for energy_term in self.get_list(energy['TerminoEnergiaActiva']):
            idx = 1
            for period in self.get_list(energy_term['Periodo']):
                energy_price = float(period['PrecioEnergia'])
                if energy_price != 0:
                    energy_value = float(period['ValorEnergiaActiva'])
                    total_consumption += energy_value

                    energy_lines.append({
                        'period': 'P{}'.format(idx),
                        'start_date': energy_term['FechaDesde'],
                        'end_date': energy_term['FechaHasta'],
                        'price': energy_price,
                        'consumption': energy_value,
                    })

                if self.get_all_periods(ocsum_code_rate) or energy_price != 0:
                    idx += 1

        result['lines'].update({
            'energy': energy_lines,
        })

        total_energy = {
            'amount': float(energy['ImporteTotalEnergiaActiva']),
            'consumption': total_consumption
        }

        result['totals'].update({
            'total_energy': total_energy,
        })

    def get_all_periods(self, ocsum_code_rate):
        '''
           There are companies that not include energy price in all periods
           for 3.x and 6.x rates.Return if it's necessary all periods treatment
        '''
        periods_codes = ['003', '011', '012', '013', '014', '015', '016', '017']
        return ocsum_code_rate in periods_codes

    def serialize_reactive_energy(self, energy, result):
        '''
            Serialize reactive energy node
        '''
        energy_lines = []
        total_consumption = 0.0
        for energy_term in self.get_list(energy['TerminoEnergiaReactiva']):
            for idx, period in enumerate(self.get_list(energy_term['Periodo'])):
                energy_price =float(period['PrecioEnergiaReactiva'])
                if energy_price != 0:
                    energy_value = float(period['ValorEnergiaReactiva'])
                    total_consumption += energy_value

                    energy_lines.append({
                        'period': 'P{}'.format(idx + 1),
                        'start_date': energy_term['FechaDesde'],
                        'end_date': energy_term['FechaHasta'],
                        'price': energy_price,
                        'consumption': energy_value,
                    })

        result['lines'].update({
            'reactive_energy': energy_lines,
        })

        total_energy = {
            'amount': float(energy['ImporteTotalEnergiaReactiva']),
            'consumption': total_consumption
        }

        result['totals'].update({
            'total_reactive_energy': total_energy,
        })

    def serialize_excess_power(self, excess_power, result):
        '''
            Serialize excess power node
        '''
        result['totals'].update({
            'total_excess_power': float(excess_power['ImporteTotalExcesos']),
        })

        excess_lines = []
        if 'Periodo' in excess_power:
            for idx, period in enumerate(
                    self.get_list(excess_power['Periodo'])):
                excess_lines.append({
                    'period': 'P{}'.format(idx + 1),
                    'excess_power_value': period['ValorExcesoPotencia'],
                })

        result['lines'].update({
            'excess_power': excess_lines,
        })

    def serialize_other_lines(self, others, result):
        '''
            Serialize other concepts
        '''
        other_lines = []
        total_amount = 0.0
        for other in self.get_list(others):
            line = {}
            if 'Concepto' in other:
                line.update({
                    'code': other['Concepto'].strip(),
                    'detail': TABLE_103[other['Concepto']]
                })
            if 'Observaciones' in other:
                line.update({'remarks': other['Observaciones']})
            if 'ImporteConceptoIVA' in other:
                amount = float(other['ImporteConceptoIVA'])
                line.update({'amount': amount})
                total_amount += amount
                if amount != 0:
                    other_lines.append(line)

        if other_lines:
            result['lines'].update({
                'others': other_lines,
            })

            result['totals'].update({
                'other': total_amount,
            })

    def serialize_reinvoicing(self, reinvoicings, result):
        '''
            Serialize reinvoicing node
        '''
        result.update({
            'reinvoicings': [],
        })
        for reinvoicing in self.get_list(reinvoicings['Refacturacion']):
            data = {
                'type': reinvoicing['Tipo'],
                'consumption': reinvoicing['RConsumo'],
            }
            if 'RFechaDesde' in reinvoicing:
                data.update({
                    'start_date': reinvoicing['RFechaDesde'],
                })

            if 'ImporteTotal' in reinvoicing:
                data.update({
                    'total_amount': float(reinvoicing['ImporteTotal']),
                })

            if 'Observaciones' in reinvoicing:
                data.update({
                    'remarks': reinvoicing['Observaciones'],
                })

            result['reinvoicings'].append(data)

    def serialize_measures(self, measures_array, result):
        '''
            Serialize measures node
        '''
        measure_list = []
        for measures in self.get_list(measures_array):
            for meter in self.get_list(measures['Aparato']):
                for integrator in self.get_list(meter['Integrador']):
                    start_date = self.format_date(
                        integrator['LecturaDesde']['FechaHora'])
                    end_date = self.format_date(
                        integrator['LecturaHasta']['FechaHora'])

                    measure = {
                        'meter': meter['NumeroSerie'].strip(),
                        'turn': 10 ** int(integrator['NumeroRuedasEnteras']),
                        'type': integrator['Magnitud'].strip(),
                        'period': LINK_T42.get(integrator['CodigoPeriodo'],
                                               None),
                        'consumption': integrator['ConsumoCalculado'],
                        'start_date': str(start_date),
                        'start_origin': (integrator['LecturaDesde']
                                         ['Procedencia']),
                        'start_read': integrator['LecturaDesde']['Lectura'],
                        'end_date': str(end_date),
                        'end_origin': integrator['LecturaHasta']['Procedencia'],
                        'end_read': integrator['LecturaHasta']['Lectura'],
                    }
                    if 'Ajuste' in integrator:
                        measure.update({
                            'adjustment': (integrator['Ajuste']
                                           ['AjustePorIntegrador']),
                            'adjustment_origin':(
                                integrator['Ajuste']['CodigoMotivoAjuste']
                                    .strip(),
                                TABLE_106[integrator['Ajuste']
                                    ['CodigoMotivoAjuste'].strip()])
                        })

                    measure_list.append(measure)

        result['measures'] = sorted(measure_list, key=lambda x: x['start_read'])

    def format_date(self, date):
        '''
            Format date
        '''
        date_type = '%Y-%m-%d'
        return datetime.strptime(date[:10], date_type).date()

    def serialize_cch(self, cchPeriod, result):
        '''
            Serialize cch node
        '''
        cch = {
            'start_date': cchPeriod['FechaDesdeCCH'],
            'end_date': cchPeriod['FechaHastaCCH'],
        }
        result.update({'cch': cch})


class OtherInvoiceSerializer(object):

    def __init__(self, header, body, version):
        self.header = header
        self.body = body
        self.version = version

    def get_list(self, obj):
        if not isinstance(obj, list):
            return [obj]

        return obj

    def serialize_invoice(self):
        '''
            Serialize other invoices node
        '''
        result = {}
        # Header data
        self.serialize_header_data(self.body['DatosGeneralesOtrasFacturas'],
                                   result)

        # Total amount data
        self.serialize_amount_data(self.body, result)

        # Others lines
        result.update({'lines': {}})
        if 'Concepto' in self.body:
            self.serialize_concepts(self.body['Concepto'], result)

        return result

    def serialize_header_data(self, gen_data, result):
        '''
              Serialize header data node
        '''

        header = {
            'type': 'OTHER',
            'company_origin_code': (
                self.header['CodigoREEEmpresaEmisora'].strip()),
            'company_destination_code': (
                self.header['CodigoREEEmpresaDestino'].strip()),
        }

        header.update({
            'version': self.version,
            'invoice_number': (gen_data['DatosGeneralesFactura']
                               ['NumeroFactura'].strip()),
            'cups': gen_data['DireccionSuministro']['CUPS'].strip(),
            'invoice_type': TABLE_101[
                gen_data['DatosGeneralesFactura']['TipoFactura'].strip()],
            'invoice_date': gen_data['DatosGeneralesFactura']['FechaFactura'],
        })

        if 'NumeroFacturaRectificada' in gen_data['DatosGeneralesFactura']:
            header.update({
                'rectifier_invoice': (gen_data['DatosGeneralesFactura']
                                      ['NumeroFacturaRectificada'].strip()),
            })

        if 'Observaciones' in gen_data['DatosGeneralesFactura']:
            header.update({
                'remarks': gen_data['DatosGeneralesFactura']['Observaciones'],
            })

        result.update({'header': header})

    def serialize_amount_data(self, invoice, result):
        '''
               Serialize amount data node
         '''
        gen_data = invoice['DatosGeneralesOtrasFacturas']
        totals = {
            'total_amount': float(gen_data['DatosGeneralesFactura']
                                  ['ImporteTotalFactura']),

            'base_amount': float(invoice['IVA']['BaseImponible']),
            'tax_amount': float(invoice['IVA']['Importe']),
        }

        result.update({'totals': totals})

    def serialize_concepts(self, others, result):
        '''
            Serialize other concepts
        '''
        other_lines = []
        total_amount = 0.0
        for other in self.get_list(others):
            line = {}
            if 'TipoConcepto' in other:
                line.update({
                    'code': other['TipoConcepto'].strip(),
                    'detail': TABLE_103[other['TipoConcepto'].strip()]
                })
            if 'ImporteTotalConcepto' in other:
                amount = float(other['ImporteTotalConcepto'])
                line.update({'amount': amount})
                total_amount += amount
                if float(amount) != 0:
                    other_lines.append(line)

        if other_lines:
            result['lines'].update({
                'others': other_lines,
            })

            result['totals'].update({
                'other': total_amount,
            })


class AtrInvoiceSerializerV1(AtrInvoiceSerializer):

    def serialize_invoice(self):
        '''
            Serialize atr invoices node
        '''
        result = {
            'type': 'ATR',
        }

        # Header data
        self.serialize_header_data(
            self.body['DatosGeneralesFacturaATR'], result)

        result.update({'lines': {}})
        # Total amount data
        self.serialize_amount_data(self.body, result)

        # Power
        self.serialize_power(self.body['Potencia'], result)

        # Energy
        if 'EnergiaActiva' in self.body:
            self.serialize_energy(self.body['EnergiaActiva'], result)

        # Reactive energy
        if 'EnergiaReactiva' in self.body:
            self.serialize_reactive_energy(self.body['EnergiaReactiva'], result)

        # Excess power
        if 'ExcesoPotencia' in self.body:
            self.serialize_excess_power(self.body['ExcesoPotencia'], result)

        # Others
        if 'ConceptoRepercutible' in self.body:
            self.serialize_other_lines(
                self.body['ConceptoRepercutible'], result)

        # Measures
        if 'Medidas' in self.body:
            result.update({
                'measures': [],
            })
            self.serialize_measures(self.body['Medidas'], result)
        # CCH
        if 'PeriodoCCH' in self.body:
            self.serialize_cch(self.body['PeriodoCCH'], result)

        return result

    def serialize_header_data(self, gen_data, result):
        '''
              Serialize header data node
        '''

        header = {
            'type': 'ATR',
            'company_origin_code': (
                self.header['CodigoREEEmpresaEmisora'].strip()),
            'company_destination_code': (
                self.header['CodigoREEEmpresaDestino'].strip())
        }

        header.update({
            'version': self.version,
            'invoice_number': (gen_data['DatosGeneralesFactura']
                               ['CodigoFiscalFactura'].strip()),
            'cups': self.header['CUPS'].strip(),
            'invoice_type': TABLE_101[
                gen_data['DatosGeneralesFactura']['MotivoFacturacion']],
            'invoice_date': gen_data['DatosGeneralesFactura']['FechaFactura'],
            'rate_code': gen_data['DatosFacturaATR']['TarifaATRFact'].strip(),
            'start_date': (gen_data['DatosFacturaATR']['Periodo']
                           ['FechaDesdeFactura']),
            'end_date': (gen_data['DatosFacturaATR']['Periodo']
                         ['FechaHastaFactura']),
            # 'invoicing_type': (
            #     gen_data['DatosFacturaATR']['MotivoFacturacion'].strip()),
            'rectifier_type': self.get_rectifier_type(
                gen_data['DatosGeneralesFactura']['TipoFactura']),
        })

        if ('CodigoFacturaRectificadaAnulada' in
                gen_data['DatosGeneralesFactura']):
            header.update({
                'rectifier_invoice': (gen_data['DatosGeneralesFactura']
                                  ['CodigoFacturaRectificadaAnulada'].strip()),
            })

        if 'Comentarios' in gen_data['DatosGeneralesFactura']:
            header.update({
                'remarks': gen_data['DatosGeneralesFactura']['Comentarios'],
            })

        # CUPS address
        address = {
            'street': gen_data['DireccionSuministro']['Calle'],
            'number': gen_data['DireccionSuministro']['NumeroFinca'],
        }

        if 'TipoVia' in gen_data['DireccionSuministro']:
            address.update({
                'type': gen_data['DireccionSuministro']['TipoVia']
            })

        if 'Escalera' in gen_data['DireccionSuministro']:
            address.update({
                'staircase': gen_data['DireccionSuministro']['Escalera']
            })

        if 'Piso' in gen_data['DireccionSuministro']:
            address.update({
                'floor': gen_data['DireccionSuministro']['Piso']
            })

        if 'Puerta' in gen_data['DireccionSuministro']:
            address.update({
                'door': gen_data['DireccionSuministro']['Puerta']
            })

        if 'AclaradorFinca' in gen_data['DireccionSuministro']:
            address.update({
                'description': gen_data['DireccionSuministro']['AclaradorFinca']
            })

        header.update({'address': address})

        result.update({'header': header})

    def get_rectifier_type(self, code):
        if code == 'R':
            return 'RA'

        return code

    def serialize_amount_data(self, invoice, result):
        gen_data = invoice['DatosGeneralesFacturaATR']

        total_base = 0.0
        total_tax = 0.0
        for iva in self.get_list(invoice['IVA']):
            base = float(iva['BaseImponible'])
            if base != 0:
                total_base += base
                total_tax += float(iva['Importe'])

        totals = {
            'total_amount': float(gen_data['DatosGeneralesFactura']
                                  ['ImporteTotalFactura']),

            'base_amount': total_base,
            'tax_amount': total_tax,
            'rental_amount': 0,
        }

        # Rent data
        if 'Alquileres' in invoice:
            totals.update({
                'rental_amount': float(invoice['Alquileres']
                                       ['ImporteFacturacionAlquileres']),
            })
            rental_lines = []
            for rent in self.get_list(
                    invoice['Alquileres']['PrecioDiarioAlquiler']):

                if float(rent['PrecioDia']) != 0:
                    rental_lines.append({
                        'price_day': float(rent['PrecioDia']),
                        'num_days': rent['NumeroDias']
                        })

            result['lines'].update({
                'rental': rental_lines,
            })

        result.update({'totals': totals})

    def serialize_other_lines(self, others, result):
        '''
            Serialize other concepts
        '''
        other_lines = []
        total_amount = 0.0
        for other in self.get_list(others):
            line = {}
            if 'ConceptoRepercutible' in other:
                line.update({
                    'code': other['ConceptoRepercutible'].strip(),
                    'detail': TABLE_103[other['ConceptoRepercutible']]
                })
            if 'Comentarios' in other:
                line.update({'remarks': other['Comentarios']})
            if 'FechaHasta' in other:
                line.update({
                    'start_date': other['FechaDesde'],
                    'end_date': other['FechaHasta']
                })
            elif 'FechaOperacion' in other:
                line.update({
                    'start_date': other['FechaOperacion'],
                    'end_date': other['FechaOperacion']
                })

            if 'ImporteTotalConceptoRepercutible' in other:
                amount = float(other['ImporteTotalConceptoRepercutible'])
                line.update({'amount': amount})
                total_amount += amount
                if amount != 0:
                    other_lines.append(line)

        if total_amount != 0:
            result['lines'].update({
                'others': other_lines,
            })

            result['totals'].update({
                'other': total_amount,
            })

    def serialize_measures(self, measures_array, result):
        '''
            Serialize measures node
        '''
        measure_list = []
        for measures in self.get_list(measures_array):
            for meter in self.get_list(measures['ModeloAparato']):
                for integrator in self.get_list(meter['Integrador']):
                    start_date = self.format_date(
                        integrator['LecturaDesde']['Fecha'])
                    end_date = self.format_date(
                        integrator['LecturaHasta']['Fecha'])

                    measure = {
                        'meter': meter['NumeroSerie'].strip(),
                        'turn': 10 ** int(integrator['NumeroRuedasEnteras']),
                        'type': integrator['Magnitud'].strip(),
                        'period': LINK_T42.get(integrator['CodigoPeriodo'],
                                               None),
                        'consumption': integrator['ConsumoCalculado'],
                        'start_date': str(start_date),
                        'start_origin': (integrator['LecturaDesde']
                                         ['Procedencia']),
                        'start_read': integrator['LecturaDesde']['Lectura'],
                        'end_date': str(end_date),
                        'end_origin': integrator['LecturaHasta']['Procedencia'],
                        'end_read': integrator['LecturaHasta']['Lectura'],
                    }
                    if 'Ajuste' in integrator:
                        measure.update({
                            'adjustment': (integrator['Ajuste']
                                           ['AjustePorIntegrador']),
                            'adjustment_origin': (
                                integrator['Ajuste']['CodigoMotivoAjuste']
                                    .strip(),
                                TABLE_106[integrator['Ajuste']
                                    ['CodigoMotivoAjuste'].strip()])

                        })

                    measure_list.append(measure)

        result['measures'] = sorted(measure_list, key=lambda x: x['start_read'])

class OtherInvoiceSerializerV1(OtherInvoiceSerializer):

    def serialize_invoice(self):
        '''
            Serialize other invoices node
        '''
        result = {}
        # Header data
        self.serialize_header_data(self.body['DatosGeneralesOtrasFacturas'],
                                   result)

        # Total amount data
        self.serialize_amount_data(self.body, result)

        # Others lines
        result.update({'lines': {}})
        if 'ConceptoRepercutible' in self.body:
            self.serialize_concepts(self.body['ConceptoRepercutible'], result)

        return result

    def serialize_header_data(self, gen_data, result):
        '''
              Serialize header data node
        '''

        header = {
            'type': 'OTHER',
            'company_origin_code': (
                self.header['CodigoREEEmpresaEmisora'].strip()),
            'company_destination_code': (
                self.header['CodigoREEEmpresaDestino'].strip()),
        }

        header.update({
            'version': self.version,
            'invoice_number': (gen_data['DatosGeneralesFactura']
                               ['CodigoFiscalFactura'].strip()),
            'cups': self.header['CUPS'].strip(),
            'invoice_type': TABLE_101[
                gen_data['DatosGeneralesFactura']['MotivoFacturacion'].strip()],
            'invoice_date': gen_data['DatosGeneralesFactura']['FechaFactura'],
        })

        if 'NumeroFacturaRectificada' in gen_data['DatosGeneralesFactura']:
            header.update({
                'rectifier_invoice': (gen_data['DatosGeneralesFactura']
                                      ['NumeroFacturaRectificada'].strip()),
            })

        if 'Comentarios' in gen_data['DatosGeneralesFactura']:
            header.update({
                'remarks': gen_data['DatosGeneralesFactura']['Comentarios'],
            })

        # CUPS address
        address = {
            'street': gen_data['DireccionSuministro']['Calle'],
            'number': gen_data['DireccionSuministro']['NumeroFinca'],
        }

        if 'TipoVia' in gen_data['DireccionSuministro']:
            address.update({
                'type': gen_data['DireccionSuministro']['TipoVia']
            })

        if 'Escalera' in gen_data['DireccionSuministro']:
            address.update({
                'staircase': gen_data['DireccionSuministro']['Escalera']
            })

        if 'Piso' in gen_data['DireccionSuministro']:
            address.update({
                'floor': gen_data['DireccionSuministro']['Piso']
            })

        if 'Puerta' in gen_data['DireccionSuministro']:
            address.update({
                'door': gen_data['DireccionSuministro']['Puerta']
            })

        if 'AclaradorFinca' in gen_data['DireccionSuministro']:
            address.update({
                'description': gen_data['DireccionSuministro']['AclaradorFinca']
            })

        header.update({'address': address})

        result.update({'header': header})

    def serialize_concepts(self, others, result):
        '''
            Serialize other concepts
        '''
        other_lines = []
        total_amount = 0.0
        for other in self.get_list(others):
            line = {}
            if 'ConceptoRepercutible' in other:
                line.update({
                    'code': other['ConceptoRepercutible'].strip(),
                    'detail': TABLE_103[other['ConceptoRepercutible'].strip()]
                })
                if 'FechaHasta' in other:
                    line.update({
                        'start_date': other['FechaDesde'],
                        'end_date': other['FechaHasta']
                    })
                elif 'FechaOperacion' in other:
                    line.update({
                        'start_date': other['FechaOperacion'],
                        'end_date': other['FechaOperacion']
                    })
            if 'ImporteTotalConceptoRepercutible' in other:
                amount = float(other['ImporteTotalConceptoRepercutible'])
                line.update({'amount': amount})
                total_amount += amount
                if float(amount) != 0:
                    other_lines.append(line)

        if total_amount != 0:
            result['lines'].update({
                'others': other_lines,
            })

            result['totals'].update({
                'other': total_amount,
            })