# -*- coding: utf-8 -*-
{
    "name": "F1 Switching",
    "description": """
    Import switching invoices from F1 files
    """,
    "version": "0-dev",
    "author": "Toni Llado",
    "category": "Soller",
    "depends":[
        "giscedata_facturacio",
        "giscedata_polissa",
        "giscedata_cups",
        "mongodb_backend",
        "account_invoice_supplier"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "switching_f1_data.xml",
        "switching_f1_view.xml",
        "wizard/wizard_switching_import_view.xml",
        "wizard/wizard_f1_reimport_view.xml",
        "security/switching_f1_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
