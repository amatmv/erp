# -*- coding: utf-8 -*-

from osv import osv, fields
import base64
from mongodb_backend.mongodb2 import mdbpool
from datetime import datetime, timedelta, date
from tools.translate import _
import traceback
import netsvc
from tools.float_utils import float_round
from defs import *
from oorq.decorators import job

class SwitchingF1Header(osv.osv):

    _name = "switching.f1.header"

    _rec_name = 'invoice_number'

    _order = 'create_date desc'

    _invoice_type_select = [
        ('ATR','ATR invoice'),
        ('OTHER','Other invoice'),
    ]

    _rectifier_type_select = [
        ('A', 'Adjustment'),
        ('B', 'Adjustment with substituent'),
        ('R', 'Rectifier'),
        ('RA', 'Rectifier without Adjustment'),
        ('C', 'Complementary'),
        ('G', 'Regularizing')
    ]

    def unlink(self, cursor, uid, ids, context=None):
        for id in ids:

            self.delete_reads(cursor, uid, id, context)
            self.delete_meters(cursor, uid, id, context)
            self.delete_extra_lines(cursor, uid, id, context)

        return super(SwitchingF1Header, self).unlink(cursor, uid, ids,
                                                     context=context)

    def b_reload_reads(self, cursor, uid, ids, context=None):
        '''
            Reload reads and create invoice
        '''
        measure_obj = self.pool.get('switching.f1.measure')
        concept_obj = self.pool.get('switching.f1.concept')

        if not context:
            context = {}

        for id in ids:
            date_adjust = measure_obj.need_adjust_date(cursor, uid, id,
                                                       context=context)

            measure_obj.check_meter(cursor, uid, id, date_adjust, context)
            self.delete_reads(cursor, uid, id, context)
            self.delete_extra_lines(cursor, uid, id, context)

            ctx = context.copy()
            ctx.update({'reload': True})
            measure_obj.save_reads(cursor, uid, id, date_adjust, False,
                                   context=ctx)
            concept_obj.create_extra_lines(cursor, uid, id, context=context)
            self.create_invoice(cursor, uid, id, context)

            vals = {'error': False}
            self.write(cursor, uid, id, vals)
        return True

    def b_validate_f1(self, cursor, uid, ids, context=None):
        '''
            validate f1 and create invoice
        '''
        for id in ids:
            self.check_reads(cursor, uid, id, context)

            self.create_invoice(cursor, uid, id, context)

            vals = {'error': False}
            self.write(cursor, uid, id, vals)
        return _('Correct validation')

    def b_create_invoice_f1(self, cursor, uid, ids, context=None):
        '''
            Create invoice from f1
        '''
        for id in ids:
            self.create_invoice(cursor, uid, id, context)

        return True

    def b_readings_from_consumption(self, cursor, uid, ids, context=None):
        '''
            Create readings from consumption only
        '''
        measure_obj = self.pool.get('switching.f1.measure')
        if not context:
            context = {}

        for id in ids:
            date_adjust = measure_obj.need_adjust_date(cursor, uid, id,
                                                       context=context)

            measure_obj.check_meter(cursor, uid, id, date_adjust, context)
            ctx = context.copy()
            ctx.update({'reload': True})
            measure_obj.save_reads(cursor, uid, id, False, True,
                                   context=ctx)

            self.create_invoice(cursor, uid, id, context)

            vals = {
                'check_consumption_reads': True,
                'error': False
            }
            self.write(cursor, uid, id, vals)
        return True

    @job(queue='import_f1')
    def create_from_dict(self, cursor, uid, f1_dict, xml, context=None):
        attachment_obj = self.pool.get('ir.attachment')
        measure_obj = self.pool.get('switching.f1.measure')
        concept_obj = self.pool.get('switching.f1.concept')
        error_obj = self.pool.get('switching.f1.error')

        header = f1_dict['header']
        # Create header
        f1_header_id = self.create_header_from_dict(cursor, uid, f1_dict,
                                                    context)
        try:
            # Xml attachment
            vals = {
                'name': '{}.xml'.format(header['invoice_number']),
                'datas': base64.b64encode(xml),
                'datas_fname': '{}.xml'.format(header['invoice_number']),
                'res_model': 'switching.f1.header',
                'res_id': f1_header_id
            }
            attachment_id = attachment_obj.create(cursor, uid, vals)
            vals = {
                'xml_attachment_id': attachment_id,
            }
            self.write(cursor, uid, f1_header_id, vals)

            # Dictionary attachment
            switching_mdb = mdbpool.get_collection('switching_f1')
            switching_mdb.insert(
                {
                    'switching_f1_id': f1_header_id,
                    'data': f1_dict,
                })

            # Update header
            self.update_header_from_dict(cursor, uid, f1_dict, f1_header_id,
                                         context)

            # Other concepts
            if 'lines' in f1_dict and 'others' in f1_dict['lines']:
                concept_obj.create_concepts_from_dict(cursor, uid,
                                                     f1_dict['lines']['others'],
                                                      f1_header_id, context)
            # Measures
            if 'measures' in f1_dict:
                measure_obj.create_measures_from_dict(cursor, uid,
                                                      f1_dict['measures'],
                                                      f1_header_id, context)
            # Rental price for new meter
            if 'lines' in f1_dict and 'rental' in f1_dict['lines']:
                self.get_rental_price_from_dict(cursor, uid,
                                                f1_dict['lines']['rental'],
                                                f1_header_id, context)

            # Validate
            if 'lines' in f1_dict:
                self.price_validate(cursor, uid, f1_dict, f1_header_id, context)

            date_adjust = measure_obj.need_adjust_date(cursor, uid,
                                                       f1_header_id,
                                                       context=context)

            measure_obj.check_meter(cursor, uid, f1_header_id, date_adjust,
                                    context)

            measure_obj.save_reads(cursor, uid, f1_header_id, date_adjust,
                                   False, context)

            concept_obj.create_extra_lines(cursor, uid, f1_header_id, context)

            self.create_invoice(cursor, uid, f1_header_id, context)


        except osv.except_osv, e:
            error_obj.create_error(cursor, uid, e.name, e.value, f1_header_id,
                                   context)
        except Exception:
            error_obj.create_error(cursor, uid, '0001', traceback.format_exc(),
                                   f1_header_id, context)

        return f1_header_id

    def create_header_from_dict(self, cursor, uid, f1_dict, context=None):
        '''
            Create header from f1 parsed dictionary. The parser check invoice
            number.
        '''
        header = f1_dict['header']
        invoice_number = header['invoice_number']
        vals = {
            'invoice_number': invoice_number
        }
        return self.create(cursor, uid, vals, context)

    def update_header_from_dict(self, cursor, uid, f1_dict, f1_header_id,
                                context=None):
        '''
            Update f1 header from f1 parsed dictionary
        '''
        partner_obj = self.pool.get('res.partner')
        cups_obj = self.pool.get('giscedata.cups.ps')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        header = f1_dict['header']
        totals = f1_dict['totals']
        invoice_number = header['invoice_number']

        search_params = [('invoice_number', '=', invoice_number),
                         ('id', '<>', f1_header_id)]
        header_ids = self.search(cursor, uid, search_params)
        if header_ids:
            msg = _(u"The invoice number {} is duplicated").format(
                invoice_number)
            raise osv.except_osv('0012', msg)

        search_params = [('ref', '=', header['company_origin_code'])]
        origin_company_id = partner_obj.search(cursor, uid, search_params)[0]

        search_params = [('ref', '=', header['company_destination_code'])]
        destination_company_id = partner_obj.search(cursor, uid,
                                                    search_params)[0]

        search_params = [('name', 'ilike', header['cups'])]
        cups_id = cups_obj.search(cursor, uid, search_params)

        if not cups_id:
            msg = _(u"The cups {} is not valid").format(header['cups'])
            raise osv.except_osv('0014', msg)
        else:
            cups_id = cups_id[0]

        # Save cups information
        if 'address' in header:
            self.fill_cups(cursor, uid, cups_id, header['address'],
                           context=context)

        start_date = (header.get('start_date', False) or
                      header.get('invoice_date', False))
        end_date = (header.get('end_date', False) or
                    header.get('invoice_date', False))

        polissa_id = self.get_policy_from_cups(cursor, uid, cups_id, start_date,
                                               end_date, context=context)

        if not polissa_id:
            cups = cups_obj.read(cursor, uid, cups_id, ['name'])['name']
            msg = _(u"The policy with cups id {} is not valid").format(cups)
            raise osv.except_osv('0002', msg)

        vals = {
            'version': header['version'],
            'invoice_type': header['type'],
            'invoice_date': header['invoice_date'],
            'origin_company_id': str(origin_company_id),
            'destination_company_id': str(destination_company_id),
            'cups_id': cups_id,
            'polissa_id': polissa_id,
            'base_amount': totals['base_amount'],
            'tax_amount': totals['tax_amount'],
            'total_amount': totals['total_amount'],
        }

        if 'rate_code' in header:
            rate_id = tarifa_obj.get_tarifa_from_ocsum(cursor, uid,
                                                       header['rate_code'])
            vals.update({
                'rate_id': rate_id,
            })

        if 'start_date' in header:
            vals.update({
                'start_date': header['start_date'],
            })

        if 'end_date' in header:
            vals.update({
                'end_date': header['end_date'],
            })

        if 'rectifier_invoice' in header:
            search_params = [
                ('invoice_number', '=', header['rectifier_invoice'])]
            rectifier_id = self.search(cursor, uid, search_params)
            if rectifier_id:
                vals.update({
                    'rectifier_invoice_id': rectifier_id[0],
                })

        if 'rectifier_type' in header and header['rectifier_type'] != 'N':
            vals.update({
                'rectifier_type': header['rectifier_type']
            })

        if 'rental_amount' in totals:
            vals.update({
                'rental_amount': totals['rental_amount'],
            })

        if 'total_power' in totals:
            vals.update({
                'power_amount': totals['total_power'],
            })

        if 'total_excess_power' in totals:
            vals.update({
                'power_excess_amount': totals['total_excess_power'],
            })

        if 'total_energy' in totals:
            vals.update({
                'energy_amount': totals['total_energy']['amount'],
                'energy_consumption': totals['total_energy']['consumption'],
            })

        if 'total_reactive_energy' in totals:
            vals.update({
                'reactive_amount': (totals['total_reactive_energy']
                                    ['amount']),
                'reactive_consumption': (totals['total_reactive_energy']
                                         ['consumption']),
            })
        if 'other' in totals:
            vals.update({
                'other_amount': totals['other'],
            })

        self.write(cursor, uid, f1_header_id, vals, context)

    def fill_cups(self, cursor, uid, cups_id, address, context=None):
        '''
            Fill cups data from F1
        '''
        cups_obj = self.pool.get('giscedata.cups.ps')
        tipovia_obj = self.pool.get('res.tipovia')

        cups_vals = cups_obj.read(cursor, uid, cups_id, ['nv', 'pnp'])

        if not cups_vals['nv'] and not cups_vals['pnp']:
            vals = {
                'nv': address['street'],
                'pnp': address['number']
            }

            if 'type' in address:
                search_params = [('codi', '=', address['type'])]
                type_id = tipovia_obj.search(cursor, uid, search_params)
                if type_id:
                    type_id = type_id[0]
                else:
                    # If not found, create type
                    vals_create = {
                        'abr': address['type'],
                        'name': address['type'],
                        'codi': address['type'],
                    }
                    type_id = tipovia_obj.create(cursor, uid, vals_create)

                vals.update({'tv': type_id})

            if 'staircase' in address:
                vals.update({'es': address['staircase']})

            if 'floor' in address:
                vals.update({'pt': address['floor']})

            if 'door' in address:
                vals.update({'pu': address['door']})

            if 'description' in address:
                vals.update({'aclarador': address['description']})

            cups_obj.write(cursor, uid, cups_id, vals)

        return True

    def get_policy_from_cups(self, cursor, uid, cups_id, start_date,
                             end_date, context=None):
        '''
            Return the policy associate to cups
        '''
        cups_obj = self.pool.get('giscedata.cups.ps')
        if isinstance(cups_id, (list, tuple)):
            cups_id = cups_id[0]
        if not context:
            context = {}

        if start_date and end_date:
            # from dates range, using contractual modifications
            context.update({'active_test': False})
            modcontractual_obj = self.pool.get(
                                            'giscedata.polissa.modcontractual')
            search_params = [
                ('cups', '=', cups_id),
                '|',
                '|',
                '&',
                ('data_inici', '<=', start_date),
                ('data_final', '>=', end_date),
                '&',
                ('data_inici', '>=', start_date),
                ('data_inici', '<=', end_date),
                '&',
                ('data_final', '>', start_date),
                ('data_final', '<=', end_date),
            ]
            modc_ids = modcontractual_obj.search(cursor, uid, search_params,
                                                 context=context,
                                                 order="data_inici asc")

            read_fields = ['polissa_id']
            modc_vals = modcontractual_obj.read(cursor, uid, modc_ids,
                                                read_fields)
            polissa_ids = list(set([x['polissa_id'] for x in modc_vals]))
            if len(polissa_ids) > 1:
                cups = cups_obj.read(cursor, uid, cups_id, ['name'])['name']
                msg = _(u"There are more than one policies associates to "
                        u"cups {}").format(cups)
                raise osv.except_osv('0003', msg)

            if polissa_ids:
                return polissa_ids[0][0]

        else:
            # Without dates first search policy in state 'esborrany' and 'validar'
            policy_obj = self.pool.get('giscedata.polissa')
            search_params = [('cups', '=', cups_id),
                             ('state', 'in', ('esborrany', 'validar'))]
            polissa_id = policy_obj.search(cursor, uid, search_params,
                                            context=context)
            #If not found, search for active policy
            if not polissa_id:
                search_params = [
                    ('cups', '=', cups_id),
                    ('state', 'not in', ('esborrany', 'validar', 'baixa'))
                ]
                polissa_id = policy_obj.search(cursor, uid, search_params,
                                                context=context)
            if polissa_id:
                return polissa_id[0]

        return None

    def get_rental_price_from_dict(self, cursor, uid, rental_dict, f1_header_id,
                                   context=None):
        '''
            Get rental meter price from f1 parsed dictionary
        '''
        if len(rental_dict) > 1:
            rental_price = False
            for rental in rental_dict:
                if not rental_price:
                    rental_price = rental['price_day']
                elif rental_price != rental['price_day']:
                    msg = _(u"There are more than one rental amounts")
                    raise osv.except_osv('0034', msg)

        vals = {
            'rental_meter_price': rental_dict[0]['price_day']
        }
        self.write(cursor, uid, f1_header_id, vals, context)

    def price_validate(self, cursor, uid, f1_dict, f1_header_id, context=None):
        '''
            Validate price fields
        '''
        rate_obj = self.pool.get('giscedata.polissa.tarifa')
        categ_obj = self.pool.get('product.uom.categ')
        read_fields = ['rate_id', 'polissa_id', 'version']
        header_vals = self.read(cursor, uid, f1_header_id, read_fields)
        lines = f1_dict['lines']
        # Power
        if 'power' in lines:
            rate_id = header_vals['rate_id'][0]
            product_periods = rate_obj.get_periodes_producte(cursor, uid,
                                                             rate_id,
                                                             'tp', context)

            search_params = [('name', '=', 'POT ELEC')]
            categ_id = categ_obj.search(cursor, uid, search_params)
            for power in lines['power']:
                self.check_power_price(cursor, uid, product_periods, power,
                                       categ_id, context)

                self.check_power(cursor, uid, power, rate_id,
                                 header_vals['polissa_id'][0], f1_header_id,
                                 header_vals['version'], context)

        # Energy
        if 'energy' in lines:
            rate_id = header_vals['rate_id'][0]
            product_periods = rate_obj.get_periodes_producte(cursor, uid,
                                                             rate_id,
                                                             'te', context)
            total_breakdown = 0.0
            for energy in lines['energy']:
                self.check_energy_price(cursor, uid, product_periods, energy,
                                        rate_id, context)

                total_breakdown += float_round(
                    float(energy['consumption']) * float(energy['price']), 2)

            total_amount = float_round(
                float(f1_dict['totals']['total_energy']['amount']), 2)
            if total_amount != float_round(total_breakdown, 2):
                msg = _(u"The energy total amount {} is not equals to energy "
                        u"breakdown amount {}").format(total_amount,
                                                       total_breakdown)
                raise osv.except_osv('0017', msg)

        # TODO validar reactiva y exces de potencia

        return True

    def check_power_price(self, cursor, uid, product_periods, power,
                          categ_id, context=None):
        '''
            Check if power price is correct
        '''
        uom_obj = self.pool.get('product.uom')
        pricelist_obj = self.pool.get('product.pricelist')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        model_obj = self.pool.get('ir.model.data')

        if not context:
            ctxt = {}
        else:
            ctxt = context.copy()

        res_id = model_obj.get_object_reference(cursor, uid,
                                                'giscedata_facturacio',
                                                'pricelist_tarifas_electricidad'
                                                )[1]

        search_params = [('category_id', '=', categ_id), ('factor', '!=', 366)]
        uom_ids = uom_obj.search(
            cursor, uid, search_params, order="factor desc")
        for uom_id in uom_ids:
            uom_id = facturador_obj.get_uoms_leaps(cursor, uid, uom_id,
                                                   power['end_date'])
            ctxt.update({'date': power['end_date'],
                         'uom': uom_id})

            atrprice = pricelist_obj.price_get(cursor, uid, [res_id],
                                               product_periods[power['period']],
                                               1, context=ctxt
                                               )[res_id]

            atrprice = float_round(atrprice, 6)
            power_price = float_round(power['price'], 6)
            if atrprice == power_price:
                return True

        msg = _(u"The power price {} is not equals to erp power price "
                u" {} for period {}").format(power_price, atrprice,
                                             power['period'])
        raise osv.except_osv('0004', msg)

    def check_power(self, cursor, uid, power, rate_id, policy_id, f1_header_id,
                    version, context=None):
        '''
            Check if contracted power is correct
        '''
        rate_obj = self.pool.get('giscedata.polissa.tarifa')

        periods = rate_obj.get_periodes(cursor, uid, rate_id, 'tp')

        grouped_periods = rate_obj.get_grouped_periods(cursor, uid,
                                                       rate_id, context)
        period = power['period']
        if period in grouped_periods:
            period = grouped_periods[period]

        contr_power = self.get_contracted_power_by_policy(
            cursor, uid, policy_id, periods[period])

        # Check power for 2.x rates
        self.check_power_2x_rate(cursor, uid, power, rate_id, policy_id,
                                 context=context)

        if version == '02':
            # Adjust power if necessary
            if contr_power == power['contracted_power']:
                vals= {'power_uom_factor': 1}
                self.write(cursor, uid, f1_header_id, vals)
                return True
            elif contr_power == (float(power['contracted_power']) / 1000):
                vals = {'power_uom_factor': 1000}
                self.write(cursor, uid, f1_header_id, vals)
                return True
            else:
                if (float(power['contracted_power']) / contr_power) > 500:
                    f1_contr_power = (float(power['contracted_power']) / 1000)
                    vals = {'power_uom_factor': 1000}
                else:
                    f1_contr_power = power['contracted_power']
                    vals = {'power_uom_factor': 1}
                self.write(cursor, uid, f1_header_id, vals)
                msg = _(u"Contracted power {} not equals to erp contracted"
                        u" power {} for period {}").format(
                    f1_contr_power, contr_power, power['period'])
                raise osv.except_osv('0016', msg)

    def check_power_2x_rate(self, cursor, uid, power, rate_id, policy_id,
                            context=None):
        '''
            Check 2.x rate power. If rate is 2.x, icp invoicing and contracted
            power is different invoiced power return a warning
        '''
        rate_obj = self.pool.get('giscedata.polissa.tarifa')
        policy_obj = self.pool.get('giscedata.polissa')

        if power['contracted_power'] != power['invoiced_power']:
            ocsum_codes = ['001', '005', '004', '006', '007', '008']
            search_params = [('codi_ocsum', 'in', ocsum_codes)]
            rate_ids = rate_obj.search(cursor, uid, search_params)

            read_fields = ['facturacio_potencia']
            policy_vals = policy_obj.read(cursor, uid, policy_id, read_fields)

            if (rate_id in rate_ids and
                    policy_vals['facturacio_potencia'] == 'icp'):
                msg = _(u"Contracted power {} and invoiced power {} not equals"
                        u" and power invoicing is not for maximeter").format(
                    power['contracted_power'], power['invoiced_power'])
                raise osv.except_osv('0035', msg)

    def get_contracted_power_by_policy(self, cursor, uid, policy_id, period_id):
        '''
            Get contracted power by policy and period
        '''
        contr_power_obj = self.pool.get(
            'giscedata.polissa.potencia.contractada.periode')

        search_params = [('polissa_id', '=', policy_id),
                         ('periode_id', '=', period_id)]
        contr_power_id = contr_power_obj.search(cursor, uid, search_params)

        read_fields = ['potencia']
        contr_power_vals = contr_power_obj.read(cursor, uid, contr_power_id,
                                                read_fields)[0]
        return contr_power_vals['potencia']

    def check_energy_price(self, cursor, uid, product_periods, energy, rate_id,
                           context=None):
        '''
            Check if energy price is correct
        '''
        pricelist_obj = self.pool.get('product.pricelist')
        product_obj = self.pool.get('product.product')
        model_obj = self.pool.get('ir.model.data')
        rate_obj = self.pool.get('giscedata.polissa.tarifa')

        if not context:
            ctxt = {}
        else:
            ctxt = context.copy()

        product = product_obj.browse(cursor, uid,
                                     product_periods[energy['period']])
        ctxt.update({'date': energy['end_date'],
                     'uom': product.uom_id.id})

        res_id = model_obj.get_object_reference(cursor, uid,
                                                'giscedata_facturacio',
                                                'pricelist_tarifas_electricidad'
                                                 )[1]

        atrprice = pricelist_obj.price_get(cursor, uid, [res_id],
                                           product_periods[energy['period']],
                                           1, context=ctxt
                                           )[res_id]
        if float_round(atrprice, 6) != float_round(energy['price'], 6):
            rate = rate_obj.read(cursor, uid, rate_id, ['name'])['name']
            msg = _(u"The energy price {} is not equals to erp price {} for "
                    u"rate {} in period {}").format(energy['price'], atrprice,
                                                    rate, energy['period'])
            raise osv.except_osv('0005', msg)

        return True

    def delete_reads(self, cursor, uid, switching_header_id, context=None):
        '''
            Delete reads and power_reads from a switching f1 header
        '''
        read_obj = self.pool.get('giscedata.lectures.lectura')
        power_obj = self.pool.get('giscedata.lectures.potencia')
        read_fields = ['read_ids', 'power_read_ids']
        header_vals = self.read(cursor, uid, switching_header_id, read_fields)
        if header_vals['read_ids']:
            read_obj.unlink(cursor, uid, header_vals['read_ids'], context)
        if header_vals['power_read_ids']:
            power_obj.unlink(cursor, uid, header_vals['power_read_ids'],
                             context)
        return True

    def delete_meters(self, cursor, uid, switching_header_id, context):
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        read_fields = ['meter_ids']
        header_vals = self.read(cursor, uid, switching_header_id,
                                read_fields)
        if header_vals['meter_ids']:
            meter_obj.unlink(cursor, uid, header_vals['meter_ids'], context)
        return True

    def create_invoice(self, cursor, uid, f1_header_id, context=None):
        '''
            Create F1 invoice for SII
        '''
        header_obj = self.pool.get('switching.f1.header')
        wizard_obj = self.pool.get('wizard.invoice.supplier')
        wizard_line_obj = self.pool.get('wizard.invoice.supplier.line')
        journal_obj = self.pool.get('account.journal')
        account_obj = self.pool.get('account.account')
        invoice_obj = self.pool.get('account.invoice')

        if not context:
            ctx = {}
        else:
            ctx = context.copy()

        header = header_obj.browse(cursor, uid, f1_header_id)

        search_params = [('origin', '=', header.invoice_number),
                         ('partner_id', '=', header.origin_company_id.id)]
        invoice_ids = invoice_obj.search(cursor, uid, search_params)

        # If there are an invoice not create a new one
        if self.validate_invoice(cursor, uid, invoice_ids, header, ctx):
            return False

        search_params = [('code', '=', 'F1')]
        journal_ids = journal_obj.search(cursor, uid, search_params)

        rect_type = self.get_rectifier_type(cursor, uid, f1_header_id)

        wizard_vals = {
            'partner_id': header.origin_company_id.id,
            'account_id': header.origin_company_id.property_account_payable.id,
            'amount_total': abs(header.total_amount),
            'accounting_date': datetime.now(),
            'invoice_date': header.invoice_date,
            'journal_id': journal_ids[0],
            'invoice_number': header.invoice_number,
            'r_type': rect_type
        }

        if header.rectifier_invoice_id:
            invoice_number = header.rectifier_invoice_id.invoice_number
            search_params = [('origin', '=', invoice_number)]
            invoice_ids = invoice_obj.search(cursor, uid, search_params)

            if not invoice_ids:
                msg = _(u"Rectified invoice {} not found").format(
                    invoice_number)
                raise osv.except_osv('0031', msg)

            wizard_vals.update({
                'r_id': invoice_ids[0]
            })

        ctx.update({
            'partner_id': header.origin_company_id.id
        })
        wizard_id = wizard_obj.create(cursor, uid, wizard_vals, context=ctx)

        # Create lines
        search_params = [('code', '=', '60000000')]
        account_ids = account_obj.search(cursor, uid, search_params)

        without_tax = abs(self.get_without_tax_amount(cursor, uid, header))

        vals = {
            'wizard_id': wizard_id,
            'account_id': account_ids[0],
            'amount': abs(header.base_amount),
            'description': _(u"Amount"),
        }
        wizard_line_obj.create(cursor, uid, vals, context=ctx)

        if without_tax > 0:
            vals = {
                'wizard_id': wizard_id,
                'account_id': account_ids[0],
                'amount': without_tax,
                'description': _(u"Amount without taxes"),
                'tax_ids': [(6,0,[])]
            }
            wizard_line_obj.create(cursor, uid, vals, context=ctx)

        invoice_id = wizard_obj.create_invoice(cursor, uid, [wizard_id])

        #Open invoice
        wkf_service = netsvc.LocalService("workflow")
        wkf_service.trg_validate(uid, 'account.invoice', invoice_id,
                                 'invoice_open', cursor)

        return True

    def get_rectifier_type(self, cursor, uid, f1_header_id):
        '''
            Get rectifier type. For negative invoices(tiepis) return adjustment
            type.
        '''
        header_obj = self.pool.get('switching.f1.header')

        header = header_obj.browse(cursor, uid, f1_header_id)

        rect_type = 'N'
        if header.rectifier_type:
            rect_type = header.rectifier_type

        # Check if is a tiepis invoice
        if 'N' in rect_type and header.total_amount < 0:
            rect_type = 'A'

        return rect_type


    def validate_invoice(self, cursor, uid, invoice_ids, header, context=None):
        '''
            Validate F1 invoice
        '''
        invoice_obj = self.pool.get('account.invoice')
        line_obj = self.pool.get('account.invoice.line')

        if not invoice_ids:
            return False

        if len(invoice_ids) > 1:
            msg = _(u"There are more than one F1 invoices associated")
            raise osv.except_osv('0021', msg)

        if isinstance(invoice_ids, (list, tuple)):
            invoice_ids = invoice_ids[0]

        read_fields = ['state', 'number']
        invoice_vals = invoice_obj.read(cursor, uid, invoice_ids, read_fields)

        if invoice_vals['state'] not in ('open', 'paid'):
            msg = _(u"F1 invoice {} is not open").format(invoice_vals['number'])
            raise osv.except_osv('0022', msg)

        # Validate lines with taxes
        line_ids = self.search_invoice_line(cursor, uid, invoice_ids,
                                            header.base_amount)
        if line_ids:
            valid = False
            tax_header_list = self.get_taxes(cursor, uid,
                                             header.origin_company_id.id)
            for line_id in line_ids:
                line = line_obj.browse(cursor, uid, line_id)
                tax_list = [tax.id for tax in line.invoice_line_tax_id]
                if sorted(tax_list) == sorted(tax_header_list):
                    valid = True

            if not valid:
                msg = _(u"Not found Invoice line with amount {}").format(
                    header.base_amount
                )
                raise osv.except_osv('0023', msg)

        # Validate lines without taxes
        without_tax = abs(self.get_without_tax_amount(cursor, uid, header))
        if without_tax > 0:
            valid = False
            line_ids = self.search_invoice_line(cursor, uid, invoice_ids,
                                                without_tax)
            for line_id in line_ids:
                line = line_obj.browse(cursor, uid, line_id)
                if not line.invoice_line_tax_id:
                    valid = True

            if not valid:
                msg = _(u"Not found Invoice line with amount {}").format(
                    without_tax
                )
                raise osv.except_osv('0024', msg)

        return True

    def check_reads(self, cursor, uid, f1_header_id, context):
        measure_obj = self.pool.get('switching.f1.measure')

        read_fields = ['measure_ids']
        head_vals = self.read(cursor, uid, f1_header_id, read_fields)
        for measure_id in head_vals['measure_ids']:
            measure = measure_obj.browse(cursor, uid, measure_id)
            type_measure = measure_obj.get_measure_type(measure.type)
            rate_id = measure.switching_f1_id.rate_id.id

            if not measure.meter_id:
                msg = _(u"There are not meter associated")
                raise osv.except_osv('0029', msg)

            if type_measure in ('A', 'R'):
                if (type_measure == 'R' and
                        measure_obj.is_rate_without_reactive(cursor, uid,
                                                             rate_id)):
                    return True

                measure_obj.check_read(cursor, uid, measure_id,
                                       measure.end_date, 'END', context)

            elif type_measure == 'PM':
                measure_obj.check_power_read(cursor, uid, measure,
                                             context=context)
            elif type_measure == 'EP':
                measure_obj.check_excess_read(cursor, uid, measure,
                                              context=context)

        return True

    def search_invoice_line(self, cursor, uid, invoice_id, amount,
                            context=None):
        line_obj = self.pool.get('account.invoice.line')
        search_params = [('invoice_id', '=', invoice_id),
                         ('price_unit', '=', amount)]
        return line_obj.search(cursor, uid, search_params)

    def get_taxes(self, cursor, uid, partner_id, context=None):
        '''
            Get taxes from a partner
        '''
        partner_obj = self.pool.get('res.partner')
        partner = partner_obj.browse(cursor, uid, partner_id)
        fiscal_position = partner.supplier_fiscal_position_id
        return [tax.tax_src_id.id
                for tax in fiscal_position.tax_ids if not tax.tax_dest_id]

    def get_without_tax_amount(self, cursor, uid, header):
        '''
            Get without tax amount
        '''
        concept_obj = self.pool.get('switching.f1.concept')
        amount = 0
        for concept in header.concept_ids:
            if concept.code == CODE_WITHOUT_TAX:
                amount += concept.amount
        return amount

    def delete_extra_lines(self, cursor, uid, switching_header_id,
                           context=None):
        '''
            Delete extra lines from a switching f1 header
        '''
        extra_obj = self.pool.get('giscedata.facturacio.extra')
        read_fields = ['extra_line_ids']
        header_vals = self.read(cursor, uid, switching_header_id, read_fields)
        if header_vals['extra_line_ids']:
            extra_obj.unlink(cursor, uid, header_vals['extra_line_ids'],
                             context)

        return True

    def _ff_error(self, cursor, uid, ids, field_name, arg, context=None):
        error_obj = self.pool.get('switching.f1.error')
        res = {}
        if not context:
            context = {}

        for header_id in ids:
            search_params = [('switching_f1_id', '=', header_id)]
            error_ids = error_obj.search(cursor, uid, search_params)
            if error_ids:
                res[header_id] = True
            else:
                res[header_id] = False
        return res

    def _get_error_from_header(self, cursor, uid, ids, context=None):
        """
            Returns error associated to header in ids
        """

        error_obj = self.pool.get('switching.f1.error')

        lot_values = error_obj.read(cursor, uid, ids, ['switching_f1_id'])
        return list(set([x['switching_f1_id'][0] for x in lot_values]))

    _store_error = {'switching.f1.header':
                     (lambda self, cr, uid, ids, c=None: ids, [], 10),
                     'switching.f1.error':
                     (_get_error_from_header, ['switching_f1_id'], 10)}

    _columns = {
        'version': fields.char('Version', size=10, readonly=True),
        'invoice_number': fields.char('Invoice number', size=64, readonly=True),
        'invoice_type': fields.selection(_invoice_type_select, 'Type',
                                         readonly=True),
        'invoice_date': fields.date('Invoice date', readonly=True),
        'start_date': fields.date('Start date', readonly=True),
        'end_date': fields.date('End date', readonly=True),
        'origin_company_id': fields.many2one('res.partner', 'Origin company',
                                             readonly=True),
        'destination_company_id': fields.many2one('res.partner',
                                                  'Destination company',
                                                  readonly=True),
        'cups_id': fields.many2one('giscedata.cups.ps', 'CUPS', readonly=True),
        'rate_id': fields.many2one('giscedata.polissa.tarifa','Rate',
                                   readonly=True),
        'rectifier_invoice_id': fields.many2one('switching.f1.header',
                                                'Rectified invoice',
                                                readonly=True),
        'rectifier_type': fields.selection(_rectifier_type_select,
                                           'Rectifier type', readonly=True),
        'polissa_id': fields.many2one('giscedata.polissa', 'Policy',
                                      readonly=True),
        'xml_attachment_id': fields.many2one('ir.attachment', readonly=True),
        # Amounts
        'rental_amount': fields.float('Rental amount', digits=(16, 2),
                                      readonly=True),
        'rental_meter_price': fields.float('Rental meter price', digits=(16, 6),
                                      readonly=True),
        'power_amount': fields.float('Power amount', digits=(16, 2),
                                     readonly=True),
        'power_uom_factor': fields.integer('Power factor', readonly=True),
        'power_excess_amount': fields.float('Power excess amount',
                                            digits=(16, 2),
                                            readonly=True),
        'energy_amount': fields.float('Energy amount', digits=(16, 2),
                                      readonly=True),
        'reactive_amount': fields.float('Reactive amount', digits=(16, 2),
                                       readonly=True),
        'other_amount': fields.float('Other amount', digits=(16, 2),
                                     readonly=True),
        'energy_consumption': fields.integer('Energy kWh', readonly=True),
        'reactive_consumption': fields.float('Reactive kVarh', readonly=True),
        'base_amount': fields.float('Base amount', digits=(16, 2),
                                     readonly=True),
        'tax_amount': fields.float('Tax amount', digits=(16, 2), readonly=True),
        'total_amount': fields.float('Total amount', digits=(16, 2),
                                     readonly=True),
        'read_ids': fields.many2many('giscedata.lectures.lectura',
                                     'switching_read_rel',
                                     'header_id', 'read_id',
                                     String='Reads', readonly=True),
        'power_read_ids': fields.many2many('giscedata.lectures.potencia',
                                           'switching_power_read_rel',
                                           'header_id', 'power_read_id',
                                           String='Power reads', readonly=True),
        'meter_ids': fields.many2many('giscedata.lectures.comptador',
                                      'switching_meter_rel',
                                      'header_id', 'meter_id',
                                      String='Meters', readonly=True),
        'error': fields.boolean(string='Error', readonly=True),
        'extra_line_ids': fields.many2many('giscedata.facturacio.extra',
                                           'switching_extra_rel',
                                           'header_id', 'extra_line_id',
                                           String='Extra lines', readonly=True),
        'check_consumption_reads': fields.boolean(string='Consumption reads',
                                                  readonly=True)
    }

SwitchingF1Header()


class SwitchingF1Measure(osv.osv):

    _name = "switching.f1.measure"

    _order = 'meter_id, type, start_date, period'

    _START_TYPE = 'START'
    _END_TYPE = 'END'

    _measure_type_selection = [
        ('AE','Active input'),
        ('AS','Active output'),
        ('EP','Excess power'),
        ('PM','Max'),
        ('R1','Reactive R1'),
        ('R2','Reactive R2'),
        ('R3','Reactive R3'),
        ('R4','Reactive R4'),
    ]

    def create_measures_from_dict(self, cursor, uid, measure_dict,
                                  f1_header_id, context=None):
        '''
            Add reads from a dictionary.

            Search the meter relates a policy. If there are not meter, create it
            and if there are more than one meter return an error. The meter can
            be inactive when arrive f1 file with consumption.
        '''
        orig_obj = self.pool.get('giscedata.lectures.origen')
        rate_obj = self.pool.get('giscedata.polissa.tarifa')
        header_obj = self.pool.get('switching.f1.header')

        if not context:
            context = {}

        measure_ids = []
        measure_period_dict = {}
        adjustment = False
        for measure in measure_dict:

            if not measure['period']:
                continue

            search_params = [('codi', '=', measure['start_origin']),
                             ('subcodi', '=', '00')]
            start_origin_ids = orig_obj.search(cursor, uid, search_params)

            search_params = [('codi', '=', measure['end_origin']),
                             ('subcodi', '=', '00')]
            end_origin_ids = orig_obj.search(cursor, uid, search_params)

            vals = {
                'meter_name': measure['meter'],
                'type': measure['type'],
                'period': measure['period'],
                'consumption': int(float(measure['consumption'])),
                'start_date': measure['start_date'],
                'start_origin_id': start_origin_ids[0],
                'start_read': int(float(measure['start_read'])),
                'end_date': measure['end_date'],
                'end_origin_id': end_origin_ids[0],
                'end_read': int(float(measure['end_read'])),
                'turn': measure['turn'],
                'switching_f1_id': f1_header_id,
            }

            if 'adjustment' in measure:
                adjustment = True
                vals.update({
                    'adjustment': int(float(measure['adjustment'])),
                })

            if 'adjustment_origin' in measure:
                vals.update({
                    'adjustment_origin': measure['adjustment_origin'],
                })

            measure_ids.append(self.create(cursor, uid, vals, context))

            measure_period_dict.setdefault(measure['type'], [])
            measure_period_dict[measure['type']].append(measure['period'])

        # Fill measures for all periods
        read_fields = ['rate_id']
        header_vals = header_obj.read(cursor, uid, f1_header_id, read_fields)
        for key in measure_period_dict:
            type = self.get_type_for_period(key)
            periods = rate_obj.get_periodes(cursor, uid,
                                            header_vals['rate_id'][0],
                                            type, context)
            for period in periods:
                if period not in measure_period_dict[key]:
                    search_params = [('type', '=', key),
                                     ('switching_f1_id', '=', f1_header_id)]
                    dflt_measure_ids = self.search(cursor, uid, search_params)
                    if len(dflt_measure_ids) == 0:
                        continue
                    dflt_measure = self.browse(cursor, uid, dflt_measure_ids[0])
                    vals = {
                        'meter_name': dflt_measure['meter_name'],
                        'type': dflt_measure['type'],
                        'period': period,
                        'consumption': 0,
                        'start_date': dflt_measure['start_date'],
                        'start_origin_id': dflt_measure['start_origin_id'].id,
                        'start_read': 0,
                        'end_date': dflt_measure['end_date'],
                        'end_origin_id': dflt_measure['end_origin_id'].id,
                        'end_read': 0,
                        'turn': dflt_measure['turn'],
                        'switching_f1_id': f1_header_id,
                    }
                    measure_ids.append(self.create(cursor, uid, vals, context))

        if adjustment and not context.get('reload', False):
            msg = _(u"Adjustment field not implemented")
            raise osv.except_osv('0018', msg)

        return measure_ids

    def create_meter(self, cursor, uid, measure, policy_id, rate_id,
                     rental_amount, date_adjust, context=None):
        '''
            create a new meter. If there are an old meter, deactivate it
        '''
        date_type = '%Y-%m-%d'
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        product_obj = self.pool.get('product.product')
        policy_obj = self.pool.get('giscedata.polissa')
        uom_categ_obj = self.pool.get('product.uom.categ')
        product_uom_obj = self.pool.get('product.uom')

        search_params = [('polissa', '=', policy_id)]

        old_meter_ids = meter_obj.search(cursor, uid, search_params)
        if len(old_meter_ids) > 1:
            policy = policy_obj.read(cursor, uid, policy_id, ['name'])['name']
            msg = _(u"The policy {} have active two or more meters").format(
                policy)
            raise osv.except_osv('0007', msg)

        start_date = datetime.strptime(measure.start_date, date_type)
        if date_adjust:
            start_date = start_date + timedelta(days=1)

        if old_meter_ids:
            # Delete old meter
            old_meter_id =  old_meter_ids[0]

            end_read_date = meter_obj.data_ultima_lectura(cursor, uid,
                                                          old_meter_id,
                                                          rate_id,
                                                          context)
            end_read_date = datetime.strptime(end_read_date, date_type)

            read_fields = ['data_alta']
            old_meter = meter_obj.read(cursor, uid, old_meter_id, read_fields)
            start_read = float(measure['start_read'])
            if start_read > 0.0 and end_read_date != start_date:
                msg = _(u"The last read date {} of the old meter {} must equals"
                        u" to start measure date {}").format(end_read_date,
                                                             old_meter['id'],
                                                             start_date)
                raise osv.except_osv('0008', msg)

            register_date = datetime.strptime(old_meter['data_alta'], date_type)

            if register_date <= start_date:
                # Deactive old meter. Deactivation date is equals to first read
                # of new meter
                vals = {'active': False,
                        'data_baixa': str(start_date)}
                meter_obj.write(cursor, uid, old_meter_id, vals)

        uom_categ_id = uom_categ_obj.search(cursor, uid,
                                            [('name', '=', 'ALQ ELEC')])
        search_params = [('category_id', '=', uom_categ_id),
                         ('name', '=', 'ALQ/dia'),
                         ('factor', '=', 365.0)]
        uom_id = product_uom_obj.search(cursor, uid, search_params)[0]

        # Add new meter
        reg_date = start_date
        if old_meter_ids:
            reg_date += timedelta(days=1) # Meter date + 1 for not overlap with old meter

        vals = {'name': measure['meter_name'],
                'polissa': policy_id,
                'giro': measure['turn'],
                'data_alta': str(reg_date),
                'preu_lloguer': rental_amount,
                'lloguer': rental_amount > 0,
                'uom_id': uom_id}

        return meter_obj.create(cursor, uid, vals, context=context)

    def need_adjust_date(self, cursor, uid, f1_header_id, context=None):
        '''
            The start date is included or excluded depending the company
        '''
        header_obj = self.pool.get('switching.f1.header')
        policy_obj =  self.pool.get('giscedata.polissa')

        read_fields = ['measure_ids', 'polissa_id']
        header_vals = header_obj.read(cursor, uid, f1_header_id, read_fields)
        measure_ids = header_vals.get('measure_ids', [])
        policy_id = header_vals.get('polissa_id', False)
        if not policy_id:
            return False
        read_fields = ['data_alta']
        policy = policy_obj.read(cursor, uid, policy_id[0], read_fields)

        for measure_id in measure_ids:
            # Check if all mesures are with included dates
            read_fields = ['start_date']
            measure_vals = self.read(cursor, uid, measure_id, read_fields)
            if measure_vals['start_date'] < policy['data_alta']:
                return True

        return False

    def check_meter(self, cursor, uid, f1_header_id, date_adjust, context=None):
        '''
            Insert meter in measures, create a new meter if necessary
        '''
        header_obj = self.pool.get('switching.f1.header')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        policy_obj = self.pool.get('giscedata.polissa')

        read_fields = ['measure_ids', 'polissa_id', 'rate_id',
                       'rental_meter_price', 'start_date', 'end_date']
        header_vals = header_obj.read(cursor, uid, f1_header_id, read_fields)
        measure_ids = header_vals.get('measure_ids', [])
        policy_id = header_vals['polissa_id'][0]

        for measure in self.browse(cursor, uid, measure_ids):
            if measure.meter_id:
                continue

            if (self.get_measure_type(measure.type) == 'R' and
                    self.is_rate_without_reactive(cursor, uid,
                                                  header_vals['rate_id'][0])):
                continue

            search_params = [('polissa', '=', policy_id),
                             ('data_alta', '<=', header_vals['end_date']),
                             '|',('data_baixa', '=', None),
                             ('data_baixa', '>=', header_vals['start_date'])]

            if not context:
                ctx = {}
            else:
                ctx = context.copy()
            ctx.update({'active_test': False})
            meter_ids = meter_obj.search(cursor, uid, search_params,
                                         context=ctx)

            meter_vals = meter_obj.read(cursor, uid, meter_ids, ['name'])
            meter_id = [x['id'] for x in meter_vals
                        if x['name'].replace('_', '') ==
                            measure.meter_name.replace('_', '')]

            if not meter_id:
                meter_id = self.create_meter(cursor, uid, measure,
                                             policy_id,
                                             header_vals['rate_id'][0],
                                             header_vals['rental_meter_price'],
                                             date_adjust,
                                             context)
                header_obj.write(cursor, uid, [f1_header_id], {
                    'meter_ids': [(4, meter_id)]})
            elif len(meter_id) > 1:
                policy = policy_obj.read(
                    cursor, uid, policy_id, ['name'])['name']
                msg = _(u"There are more than one meter associated for policy "
                        u"{}").format(policy)
                raise osv.except_osv('0006', msg)

            if isinstance(meter_id, list):
                meter_id = meter_id[0]

            vals = {'meter_id': meter_id}
            self.write(cursor, uid, measure['id'], vals, context)

        # TODO Update meter rental

    def save_reads(self, cursor, uid, switching_header_id, date_adjust,
                   force_reads, context=None):
        '''
            Save measures. There are different cases:
            - Normal invoice:
                Create reads
            - Adjustment
                delete reads
            - Adjustment with substituent + Rectifier:
                check and delete reads + create new reads when rectifier
        '''
        header_obj = self.pool.get('switching.f1.header')

        read_fields = ['rectifier_type', 'rectifier_invoice_id', 'measure_ids']
        header_vals = header_obj.read(cursor, uid, switching_header_id,
                                      read_fields)
        rectifier_type = header_vals.get('rectifier_type', False)
        rectifier_header_id = header_vals.get('rectifier_invoice_id', False)
        measure_ids = header_vals.get('measure_ids', [])

        if not rectifier_type or rectifier_type == 'N' or rectifier_type == 'R':
            # Normal invoice / Rectifier invoice
            self.create_reads(cursor, uid, measure_ids, switching_header_id,
                              date_adjust, force_reads, context=context)
        elif rectifier_type == 'A':
            # Adjustment invoice
            if rectifier_header_id:
                header_obj.delete_reads(cursor, uid, rectifier_header_id[0],
                                        context)
            else:
                msg = _(u"Rectifier invoice not found")
                raise osv.except_osv('0019', msg)
        elif rectifier_type == 'B':
            # Adjustment with substituent invoice
            if rectifier_header_id:
                self.check_measures(cursor, uid, measure_ids,
                                    rectifier_header_id[0], context=context)

                header_obj.delete_reads(cursor, uid, rectifier_header_id[0],
                                        context)
            else:
                msg = _(u"Rectifier invoice not found")
                raise osv.except_osv('0019', msg)
        elif rectifier_type == 'RA':
            # Rectifier invoice without adjustment
            if rectifier_header_id:
                header_obj.delete_reads(cursor, uid, rectifier_header_id[0],
                                        context)

                self.create_reads(cursor, uid, measure_ids, switching_header_id,
                                  date_adjust, force_reads, context=context)
            else:
                msg = _(u"Rectifier invoice not found")
                raise osv.except_osv('0019', msg)
        elif rectifier_type == 'G' and context.get('reload', False):
            # Regularazing invoice
            self.create_reads(cursor, uid, measure_ids, switching_header_id,
                              date_adjust, force_reads, context=context)
        elif rectifier_type == 'C' and context.get('reload', False):
            # Complementary invoice
            self.create_extra_complementary(cursor, uid, switching_header_id,
                                            context=context)
        else:
            msg = _(u"Rectifier type {} not implemented").format(rectifier_type)
            raise osv.except_osv('0030', msg)

        return True

    def check_measures(self, cursor, uid, measure_ids, switching_header_id,
                       context=None):
        header_obj = self.pool.get('switching.f1.header')
        read_fields = ['measure_ids']
        header_vals = header_obj.read(cursor, uid, switching_header_id,
                                      read_fields)

        for measure in self.browse(cursor, uid, measure_ids):
            found = False
            for header_measure in self.browse(cursor, uid,
                                              header_vals['measure_ids']):
                if (header_measure.start_read == measure.start_read and
                            header_measure.end_read == measure.end_read and
                            header_measure.period == measure.period and
                            header_measure.type == measure.type):
                    found = True
                    break
            if not found:
                msg = _(u"Measure for start date {} in period {} not found"
                        ).format(measure.start_date, measure.period)
                raise osv.except_osv('0020', msg)

        return True

    def create_reads(self, cursor, uid, measure_ids, switching_header_id,
                     date_adjust, force_reads, context=None):
        header_obj = self.pool.get('switching.f1.header')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        header = header_obj.browse(cursor, uid, switching_header_id)

        for measure in self.browse(cursor, uid, measure_ids):
            type_measure = self.get_measure_type(measure.type)
            if type_measure in ('A', 'R'):
                if (type_measure == 'R' and
                        self.is_rate_without_reactive(cursor, uid,
                                                    header.rate_id.id)):
                    continue
                meter_ids = [x['id'] for x in header.meter_ids]
                is_new_meter = measure.meter_id.id in meter_ids
                if is_new_meter:
                    add_days = self.add_days_new_meter(cursor, uid,
                                                       switching_header_id)
                    # Create the start read for the new meter
                    self.create_read(cursor, uid, measure,
                                     type=self._START_TYPE,
                                     add_days=add_days, date_adjust=date_adjust,
                                     context=context)

                measure_copy_id = self.copy(cursor, 1, measure.id)
                measure_copy = self.browse(cursor, uid, measure_copy_id)
                try:
                    if (self.need_initial_read(cursor, uid, measure_copy,
                                               header.rectifier_type, context)
                            or self.is_change_rate(cursor, uid, measure_copy,
                                                   context)):
                        self.new_contract_treatment(cursor, uid,
                                                    header.polissa_id.id,
                                                    measure_copy,
                                                    context=context)
                        # Create initial read
                        self.create_read(cursor, uid, measure_copy,
                                         type=self._START_TYPE,
                                         date_adjust=date_adjust,
                                         context=context)

                    if force_reads:
                        # Create read
                        self.create_read_from_consumption(cursor, uid,
                                                          measure_copy,
                                                          context=context)
                    else:
                        # Search and check start read
                        self.validate_read(cursor, uid, switching_header_id,
                                           measure_copy, is_new_meter,
                                           date_adjust, context)
                        # Create read
                        self.create_read(cursor, uid, measure_copy,
                                         date_adjust=date_adjust,
                                         context=context)
                except Exception:
                    raise
                finally:
                    self.unlink(cursor, 1, measure_copy_id) # Delete copy

            elif type_measure in ('EP', 'PM'):
                self.validate_power_read(cursor, uid, measure, context=context)
                self.create_power_read(cursor, uid, measure,
                                       header.power_uom_factor,
                                       context=context)

    def add_days_new_meter(self, cursor, uid, switching_header_id):
        '''
            If there are an old meter add a day for not
            overlap with old meter
        '''
        header_obj = self.pool.get('switching.f1.header')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        header = header_obj.browse(cursor, uid, switching_header_id)
        meter_ids = [x['id'] for x in header.meter_ids]
        search_params = [('polissa', '=', header.polissa_id.id),
                         ('id', 'not in', meter_ids)]
        old_meter_ids = meter_obj.search(
            cursor, uid, search_params,
            context={'active_test': False}
        )
        if old_meter_ids:
            return 1  # + 1 for not overlap with old meter

        return 0

    def create_extra_complementary(self, cursor, uid, f1_header_id,
                                   context=None):
        '''
            Create extra line for complementary invoice. For this invoices we
            create a extra energy line with consumption
        '''
        extra_obj = self.pool.get('giscedata.facturacio.extra')
        header_obj = self.pool.get('switching.f1.header')
        product_obj = self.pool.get('product.product')
        journal_obj = self.pool.get('account.journal')
        rate_obj = self.pool.get('giscedata.polissa.tarifa')

        if not context:
            context = {}

        header = header_obj.browse(cursor, uid, f1_header_id)

        # Get product by rate and period P1 (P1 exist in all rates)
        product_id = rate_obj.get_periodes_producte(cursor, uid,
                                                    header.polissa_id.tarifa.id,
                                                    'te',context)['P1']

        read_fields = ['taxes_id']
        product_vals = product_obj.read(cursor, uid, product_id, read_fields)
        tax_ids = product_vals['taxes_id']

        search_params = [('code', '=', 'ENERGIA')]
        journal_ids = journal_obj.search(cursor, uid, search_params)

        start_date = datetime.strptime(header.start_date, '%Y-%m-%d'
                                       ).strftime('%d/%m/%Y')
        end_date = datetime.strptime(header.end_date, '%Y-%m-%d'
                                     ).strftime('%d/%m/%Y')
        vals = {
            'name': 'Consumo estimado ({} - {})'.format(start_date, end_date),
            'polissa_id': header.polissa_id.id,
            'product_id': product_id,
            'date_from': header.start_date,
            'date_to': header.end_date,
            'quantity': header.energy_consumption,
            'price_unit': (header.energy_amount / header.energy_consumption),
            'term': 1,
            'journal_ids': [(6, 0, journal_ids)],
            'tipus': 'energia',
            'tax_ids': [(6, 0, tax_ids)]
        }

        extra_id = extra_obj.create(cursor, uid, vals, context)
        # Save in f1 header model
        header_obj.write(cursor, uid, [f1_header_id],
                         {'extra_line_ids': [(4, extra_id)]})


    def validate_power_read(self, cursor, uid, measure, context=None):
        '''
            Validate maximeters and excess powers
        '''
        measure_obj = self.pool.get('switching.f1.measure')

        search_params = [('meter_id', '=', measure.meter_id.id),
                         ('period', '=', measure.period),
                         ('end_date', '=', measure.end_date),
                         ('type', '=', 'AE')]

        energy_measure_ids = measure_obj.search(cursor, uid, search_params)
        energy_measure = False
        if energy_measure_ids:
            energy_measure = measure_obj.browse(cursor, uid,
                                                energy_measure_ids[0])

        if (not context.get('reload', False) and measure.consumption > 0 and
                (not energy_measure or energy_measure.consumption == 0)):
            msg = _(u"Incorrect maximeter for date {} in period {}, there are "
                    u"not energy consumption").format(measure.end_date,
                                                        measure.period)
            raise osv.except_osv('0033', msg)


    def create_read(self, cursor, uid, measure, type=_END_TYPE, add_days=0,
                    date_adjust=False, context=None):
        '''
            Create read
        '''
        date_type = '%Y-%m-%d'
        read_obj = self.pool.get('giscedata.lectures.lectura')
        ori_comer_obj = self.pool.get('giscedata.lectures.origen_comer')
        rate_obj = self.pool.get('giscedata.polissa.tarifa')
        switching_obj = self.pool.get('switching.f1.header')

        type_measure = self.get_measure_type(measure.type)

        search_params = [('codi', '=', 'F1')]
        ori_comer_ids = ori_comer_obj.search(cursor, uid, search_params)

        rate_id = measure.switching_f1_id.rate_id.id

        if type == self._START_TYPE:
            if date_adjust:
                start_date = datetime.strptime(measure.start_date, date_type)
                start_date = str(start_date + timedelta(days=1))
            else:
                start_date = measure.start_date

            measure_date = start_date
            measure_read = measure.start_read
            measure_origin = measure.start_origin_id.id
        else:
            measure_date = measure.end_date
            measure_read = measure.end_read
            measure_origin = measure.end_origin_id.id

        if add_days > 0:
            measure_date = datetime.strptime(measure_date, date_type)
            measure_date = str(measure_date + timedelta(days=add_days))  # + 1 for not overlap with old meter

        periods = rate_obj.get_periodes(cursor, uid, rate_id, 'te')
        vals = {
            'name': measure_date,
            'periode': periods[measure.period],
            'comptador': measure.meter_id.id,
            'origen_comer_id': ori_comer_ids[0],
            'lectura': measure_read,
            'consum': measure.consumption,
            'tipus': type_measure,
            'origen_id': measure_origin,
        }

        if 'adjustment' in measure:
            vals.update({
                'ajust': measure.adjustment
            })

        meter_read_id = self.get_read_id(cursor, uid, measure, measure_date)

        if not meter_read_id:
            # Read not exist
            read_id = read_obj.create(cursor, uid, vals, context)
            # Save in f1 header model
            switching_obj.write(cursor, uid, [measure.switching_f1_id.id],
                                {'read_ids': [(4, read_id)]})
        else:
            msg = _(u"There are a read for date {} in period {}").format(
                measure_date, measure.period)
            raise osv.except_osv('0009', msg)

        return True

    def create_read_from_consumption(self, cursor, uid, measure, add_days=0,
                                     context=None):
        '''
            Create read
        '''
        date_type = '%Y-%m-%d'
        read_obj = self.pool.get('giscedata.lectures.lectura')
        ori_comer_obj = self.pool.get('giscedata.lectures.origen_comer')
        rate_obj = self.pool.get('giscedata.polissa.tarifa')
        switching_obj = self.pool.get('switching.f1.header')

        type_measure = self.get_measure_type(measure.type)

        search_params = [('codi', '=', 'F1')]
        ori_comer_ids = ori_comer_obj.search(cursor, uid, search_params)

        rate_id = measure.switching_f1_id.rate_id.id

        measure_date = measure.end_date
        measure_origin = measure.end_origin_id.id

        # Get start read to add the consumption
        start_read_id = self.get_read_id(cursor, uid, measure,
                                         measure.start_date)
        if not start_read_id:
            msg = _(u"There are not a read for date {} in period {}").format(
                measure_date, measure.period)
            raise osv.except_osv('0032', msg)

        read_fields = ['consum', 'lectura']
        start_read = read_obj.read(cursor, uid, start_read_id, read_fields)

        measure_read = start_read['lectura'] + measure.consumption

        if add_days > 0:
            measure_date = datetime.strptime(measure_date, date_type)
            measure_date = str(measure_date + timedelta(days=add_days))  # + 1 for not overlap with old meter

        periods = rate_obj.get_periodes(cursor, uid, rate_id, 'te')
        vals = {
            'name': measure_date,
            'periode': periods[measure.period],
            'comptador': measure.meter_id.id,
            'origen_comer_id': ori_comer_ids[0],
            'lectura': measure_read,
            'consum': measure.consumption,
            'tipus': type_measure,
            'origen_id': measure_origin,
        }

        meter_read_id = self.get_read_id(cursor, uid, measure, measure_date)

        if not meter_read_id:
            # Read not exist
            read_id = read_obj.create(cursor, uid, vals, context)
            # Save in f1 header model
            switching_obj.write(cursor, uid, [measure.switching_f1_id.id],
                                {'read_ids': [(4, read_id)]})
        else:
            msg = _(u"There are a read for date {} in period {}").format(
                measure_date, measure.period)
            raise osv.except_osv('0009', msg)

        return True

    def create_power_read(self, cursor, uid, measure, power_uom_factor,
                          context=None):
        '''
            Create a power read.
        '''
        power_obj = self.pool.get('giscedata.lectures.potencia')
        ori_comer_obj = self.pool.get('giscedata.lectures.origen_comer')
        rate_obj = self.pool.get('giscedata.polissa.tarifa')
        switching_obj = self.pool.get('switching.f1.header')

        type_measure = self.get_measure_type(measure.type)

        search_params = [('codi', '=', 'F1')]
        ori_comer_ids = ori_comer_obj.search(cursor, uid, search_params)

        rate_id = measure.switching_f1_id.rate_id.id

        periods = rate_obj.get_periodes(cursor, uid, rate_id, 'tp')
        search_params = [('name', '=', measure.end_date),
                         ('periode', '=', periods[measure.period]),
                         ('comptador', '=', measure.meter_id.id)]
        old_power_ids = power_obj.search(cursor, uid, search_params)
        if len(old_power_ids) > 1:
            msg = _(u"Power read for date {} in period {} is "
                    u"duplicated").format(measure.end_date,
                                         measure.period)
            raise osv.except_osv('0010', msg)

        if type_measure == 'EP':
            consumption = measure.consumption / 1000 #Convert watts to Kw
            vals = {
                'exces': consumption,
            }
        else:
            # Convert value in watts to kw if necessari.
            if power_uom_factor:
                consumption = float(measure.consumption) / power_uom_factor
            else:
                consumption = measure.consumption / 1000  #Convert watts to Kw
            vals = {
                'lectura': consumption,
            }

        if not old_power_ids:
            vals.update({
                'name': measure.end_date,
                'periode': periods[measure.period],
                'comptador': measure.meter_id.id,
                'origen_comer_id': ori_comer_ids[0],
                'consum': measure.consumption,
            })
            read_power_id = power_obj.create(cursor, uid, vals, context)
            # Save in f1 header model
            switching_obj.write(cursor, uid, [measure.switching_f1_id.id],
                                {'power_read_ids': [(4, read_power_id)]})
        else:
            power_obj.write(cursor, uid, old_power_ids[0], vals,
                            context)
        return True

    def validate_read(self, cursor, uid, switching_header_id, measure,
                      is_new_meter, date_adjust, context=None):
        '''
            Check if there are the start read in meter.
        '''
        date_type = '%Y-%m-%d'

        start_date = measure.start_date
        if is_new_meter:
            add_days = self.add_days_new_meter(cursor, uid,
                                               switching_header_id)
            start_date = datetime.strptime(measure.start_date, date_type)
            start_date = str(start_date + timedelta(days=add_days))

        if date_adjust:
            start_date = datetime.strptime(measure.start_date, date_type)
            start_date = str(start_date + timedelta(days=1))

        self.check_read(cursor, uid, measure.id, start_date, 'START', context)

    def need_initial_read(self, cursor, uid, measure, rectifier_type,
                          context=None):
        '''
             Return true if there are not reads in the meter
        '''
        lectures_obj = self.pool.get('giscedata.lectures.lectura')
        rate_obj = self.pool.get('giscedata.polissa.tarifa')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        meter_id = measure.meter_id.id

        if rectifier_type:
            # If rectificative F1, it's possible that the meter is not empty
            meter_vals = meter_obj.read(cursor, uid,
                                        measure.meter_id.id,
                                        ['data_alta'])
            if meter_vals['data_alta'] >= measure.start_date:
                return True

        else:
            # Check if meter is empty.
            rate_id = measure.switching_f1_id.rate_id.id
            periods = rate_obj.get_periodes(cursor, uid, rate_id, 'te')
            type_measure = self.get_measure_type(measure.type)
            search_params = [('comptador', '=', meter_id),
                             ('periode', '=', periods[measure.period]),
                             ('tipus', '=', type_measure)]

            read_ids = lectures_obj.search(cursor, uid, search_params,
                                           context={'active_test': False})

            if not read_ids:
               return True

        return False

    def is_change_rate(self, cursor, uid, measure, context=None):
        '''
            Check if there are a read in meter but the rate is changed
        '''
        lectures_obj = self.pool.get('giscedata.lectures.lectura')

        if context.get('reload', False):
            meter_read_id = self.get_read_id(cursor, uid, measure,
                                             measure.start_date)
            if not meter_read_id:
                type_measure = self.get_measure_type(measure.type)
                search_params = [('comptador', '=', measure.meter_id.id),
                                 ('tipus', '=', type_measure),
                                 ('name', '=', measure.start_date)]

                read_ids = lectures_obj.search(cursor, uid, search_params,
                                               context={'active_test': False})

                if read_ids:
                    return True

        return False

    def new_contract_treatment(self, cursor, uid, contract_id, measure,
                               context=None):
        '''
            If is new contract and start_date is previous of new contract date,
            add a day.
        '''
        contract_obj = self.pool.get('giscedata.polissa')
        date_type = '%Y-%m-%d'
        # Add 1 day
        start_date = datetime.strptime(measure.start_date, date_type)
        start_date = (start_date + timedelta(days=1)).strftime(date_type)
        read_fields = ['data_alta']
        contract_vals = contract_obj.read(cursor, uid, contract_id,
                                          read_fields)
        if start_date == contract_vals['data_alta']:
            measure.start_date = start_date

        return True

    def get_read_id(self, cursor, uid, measure, measure_date, meter_id=None):
        '''
            Get read id from measure
        '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        rate_obj = self.pool.get('giscedata.polissa.tarifa')

        type_measure = self.get_measure_type(measure.type)
        rate_id = measure.switching_f1_id.rate_id.id
        periods = rate_obj.get_periodes(cursor, uid, rate_id, 'te')
        if not meter_id:
            meter_id = measure.meter_id.id

        meter_read_vals = meter_obj.get_lectures(
            cursor, uid, meter_id, rate_id, measure_date,
            tipus=type_measure, periodes=[periods[measure.period]]
        )[measure.period]['actual']

        if 'id' in meter_read_vals:
            return meter_read_vals['id']
        else:
            return False

    def check_read(self, cursor, uid, measure_id, measure_date, type,
                   context=None):
        '''
            Check if there are a created read with measure data
        '''
        read_obj = self.pool.get('giscedata.lectures.lectura')
        measure_obj = self.pool.get('switching.f1.measure')

        measure = measure_obj.browse(cursor, uid, measure_id)
        meter_read_id = self.get_read_id(cursor, uid, measure, measure_date)

        if not meter_read_id:
            msg = _(u"Measure for date {} in period {} not found").format(
                measure_date, measure.period)
            raise osv.except_osv('0011', msg)

        read_fields = ['lectura']
        read_vals = read_obj.read(cursor, uid, meter_read_id,
                                  read_fields)

        if type == 'START':
            read_measure = measure.start_read
        else:
            read_measure = measure.end_read

        if read_vals['lectura'] != read_measure:
            msg = _(u"Measure for date {} in period {} not equals").format(
                measure_date, measure.period)
            raise osv.except_osv('0013', msg)

        consumption = float(measure.end_read) - float(measure.start_read)
        adjustment = float(measure.adjustment or 0)
        if consumption + adjustment != float(measure.consumption):
            msg = _(u"Consumption is not correct for measure in date {} period"
                    u" {} and type {}").format(measure.end_date, measure.period,
                                               measure.type)
            raise osv.except_osv('0015', msg)
        return True

    def check_power_read(self, cursor, uid, measure, context=None):
        '''
            Check if there are a created power read with measure data
        '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        rate_id = measure.switching_f1_id.rate_id.id

        maximeter = meter_obj.get_maximetres_per_facturar(
            cursor, uid, measure.meter_id.id, rate_id, measure.start_date,
            measure.end_date)[measure.period]['maximetre']

        if not maximeter and maximeter > 0:
            msg = _(u"Maximeter for date {} in period {} not found").format(
                measure.end_date, measure.period)
            raise osv.except_osv('0025', msg)

        f1_maximeter = measure.consumption
        # Convert value in watts to kw if necessari.
        if measure.switching_f1_id.power_uom_factor:
            f1_maximeter /= measure.switching_f1_id.power_uom_factor
        else:
            f1_maximeter /= 1000

        if maximeter != f1_maximeter:
            msg = _(u"Maximeter for date {} in period {} not equals"
                    ).format(measure.end_date, measure.period)
            raise osv.except_osv('0026', msg)

        return True

    def check_excess_read(self, cursor, uid, measure, context=None):
        '''
            Check if there are a created excess read with measure data
        '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        rate_id = measure.switching_f1_id.rate_id.id

        exces = meter_obj.get_excesos_per_facturar(
            cursor, uid, measure.meter_id.id, rate_id, measure.start_date,
            measure.end_date)[measure.period]

        if not exces:
            msg = _(u"Excess for date {} in period {} not found").format(
                measure.end_date, measure.period)
            raise osv.except_osv('0027', msg)

        if exces != measure.consumption:
            msg = _(u"Excess for date {} in period {} not equals").format(
                measure.end_date, measure.period)
            raise osv.except_osv('0028', msg)

        return True

    def get_measure_type(self, type):
        if type in ('R1', 'R2', 'R3', 'R4'):
            return 'R'
        elif type == 'AE':
            return 'A'
        return type

    def get_type_for_period(self, type):
        if type == 'PM':
            return 'tp'
        elif type == 'EP':
            return 'ep'
        return 'te'

    def is_rate_without_reactive(self, cursor, uid, rate_id):
        '''
            Indicate if the rate id is a rate without reactive reads
        '''
        WITHOUT_REACTIVE = ['001', '004', '005', '006', '007', '008']
        rate_obj = self.pool.get('giscedata.polissa.tarifa')
        read_fields = ['codi_ocsum']
        rate_vals = rate_obj.read(cursor, uid, rate_id, read_fields)
        return rate_vals['codi_ocsum'] in WITHOUT_REACTIVE

    _columns = {
        'meter_id': fields.many2one('giscedata.lectures.comptador','Meter',
                                 readonly=True),
        'meter_name': fields.char('Meter name', size=64, readonly=True),
        'type': fields.selection(_measure_type_selection, 'Type',
                                 readonly=True),
        'period': fields.char('Period', size=64, readonly=True),
        'consumption': fields.integer('Consumption', readonly=True),
        'start_date': fields.date('Start date', readonly=True),
        'start_origin_id': fields.many2one('giscedata.lectures.origen',
                                           'Start origin', readonly=True),
        'start_read': fields.integer('Start read', readonly=True),
        'end_date': fields.date('End date', readonly=True),
        'end_origin_id': fields.many2one('giscedata.lectures.origen',
                                         'End origin', readonly=True),
        'end_read': fields.integer('End read', readonly=True),
        'turn': fields.integer('Turn'),
        'adjustment': fields.integer('Adjustment', readonly=True),
        'adjustment_origin': fields.char('Adjustment origin', size=64,
                                         readonly=True),
        'switching_f1_id': fields.many2one('switching.f1.header',
                                           'Invoice number', ondelete='cascade',
                                           readonly=True)
    }


SwitchingF1Measure()


class SwitchingF1Concept(osv.osv):
    _name = "switching.f1.concept"

    def create_concepts_from_dict(self, cursor, uid, concept_dict, f1_header_id,
                                  context=None):
        '''
            Create concepts
        '''
        concept_obj = self.pool.get('switching.f1.concept')

        for concept in concept_dict:
            vals = {
                'code': concept['code'],
                'description': concept['detail'],
                'amount': concept['amount'],
                'start_date': concept['start_date'],
                'end_date': concept['end_date'],
                'switching_f1_id': f1_header_id
            }
            concept_obj.create(cursor, uid, vals)

        return True

    def create_extra_lines(self, cursor, uid, f1_header_id, context=None):
        '''
            Create extra lines from concepts
        '''
        extra_obj = self.pool.get('giscedata.facturacio.extra')
        header_obj = self.pool.get('switching.f1.header')
        product_obj = self.pool.get('product.product')
        journal_obj = self.pool.get('account.journal')

        if not context:
            context = {}

        read_fields = ['polissa_id', 'concept_ids']
        header_vals = header_obj.read(cursor, uid, f1_header_id, read_fields)
        concept_ids = header_vals.get('concept_ids', [])

        search_params = [('default_code', '=', 'EXTRA')]
        product_id = product_obj.search(cursor, uid, search_params)[0]
        read_fields = ['taxes_id']
        product_vals = product_obj.read(cursor, uid, product_id, read_fields)

        search_params = [('code', '=', 'ENERGIA')]
        journal_ids = journal_obj.search(cursor, uid, search_params)

        for concept in self.browse(cursor, uid, concept_ids):
            if concept.code == CODE_WITHOUT_TAX:
                tax_ids = []
            else:
                tax_ids = product_vals['taxes_id']

            vals = {
                'name': concept.description,
                'polissa_id': header_vals['polissa_id'][0],
                'product_id': product_id,
                'date_from': concept.start_date,
                'date_to': concept.end_date,
                'quantity': 1,
                'price_unit': concept.amount,
                'term': 1,
                'journal_ids': [(6, 0, journal_ids)],
                'tax_ids': [(6, 0, tax_ids)]
            }

            extra_id = extra_obj.create(cursor, uid, vals, context)
            # Save in f1 header model
            header_obj.write(cursor, uid, [f1_header_id],
                             {'extra_line_ids': [(4, extra_id)]})

    _columns = {
        'code': fields.char('Code', size=5, readonly=True),
        'description': fields.char('Description', size=64, readonly=True),
        'amount': fields.float('Amount', digits=(16, 2), readonly=True),
        'start_date': fields.date('Start date', readonly=True),
        'end_date': fields.date('End date', readonly=True),
        'switching_f1_id': fields.many2one('switching.f1.header',
                                           'Invoice number', ondelete='cascade',
                                           readonly=True)
    }

SwitchingF1Concept()


class SwitchingF1Header2(osv.osv):

    _name = "switching.f1.header"
    _inherit = "switching.f1.header"

    _columns = {
        'measure_ids': fields.one2many('switching.f1.measure',
                                       'switching_f1_id',
                                       'Measures',
                                       readonly=True),
        'concept_ids': fields.one2many('switching.f1.concept',
                                       'switching_f1_id',
                                       'Concepts',
                                       readonly=True),
    }

SwitchingF1Header2()


class F1Error(osv.osv):

    _name = "switching.f1.error"

    _order = 'id desc'

    def create_error(self, cursor, uid, code, message, f1_header_id,
                     context):
        header_obj = self.pool.get('switching.f1.header')
        vals = {
            'code': code,
            'message': message
        }
        if f1_header_id:
           vals.update({'switching_f1_id': f1_header_id})
           vals_header = {'error': True}
           header_obj.write(cursor, uid, f1_header_id, vals_header)

        return self.create(cursor, uid, vals, context)

    _columns = {
        'code': fields.char('Code', size=24, required=True, readonly=True),
        'name': fields.char('Name', size=64, readonly=True),
        'message': fields.text('Message', readonly=True),
        'create_date': fields.datetime('Create date'),
        'switching_f1_id': fields.many2one('switching.f1.header',
                                           'Invoice number', ondelete='cascade',
                                           readonly=True)
    }
F1Error()
