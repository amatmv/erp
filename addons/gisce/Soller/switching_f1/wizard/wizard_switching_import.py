# -*- coding: utf-8 -*-
import StringIO
import base64
import traceback
import zipfile
from os.path import basename

from switching_f1.f1_xml_serialize import F1Xml
from osv import osv, fields
from tools.translate import _


class WizardSwitchingImport(osv.osv_memory):
    """Wizard F1 file import"""
    _name = 'wizard.switching.import'

    def action_f1_import(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        if wizard.file:
            wiz_msg = _(u"It begins import process in background."
                        u" When finishes you will receive a request.")
            wizard.write({'state': 'done', 'info': wiz_msg})
            try:
                self.import_f1(cursor, uid, wizard.file, context)
            except osv.except_osv, e:
                # pass
                return e.value

    def import_f1(self, cursor, uid, file, context=None):
        """File import"""
        f1_header_obj = self.pool.get('switching.f1.header')
        error_obj = self.pool.get('switching.f1.error')

        if not context:
            context = {}

        i_id = context.get('active_id', False)

        try:
            data = self.get_data_from_file(file)
        except Exception, e:
            error_obj.create_error(cursor, uid, 'LCT0001',
                                   traceback.format_exc(), None, context)
            return e.message

        for fitxer in data.keys():
            try:
                F1 = F1Xml(data[fitxer]['xml'])
                f1_serialize = F1.serialize_xml()
                for f1_dict in f1_serialize:
                    f1_header_obj.create_from_dict(cursor, uid, f1_dict,
                                                   data[fitxer]['xml'],
                                                   context=context)
            except osv.except_osv, e:
                error_id = error_obj.create_error(cursor, uid, e.name, e.value,
                                                  None, context)
                # Xml attachment
                self.attach_xml(cursor, uid, data[fitxer]['xml'], error_id)
            except Exception, e:
                error_id = error_obj.create_error(cursor, uid, 'XML0001',
                                                  traceback.format_exc(),
                                                  None, context)
                # Xml attachment
                self.attach_xml(cursor, uid, data[fitxer]['xml'], error_id)

        return _(u"File imported correctly")

    def attach_xml(self, cursor, uid, xml, error_id):
        attachment_obj = self.pool.get('ir.attachment')

        vals = {
            'name': 'f1_xml.xml',
            'datas': base64.b64encode(xml),
            'datas_fname': 'f1_xml.xml',
            'res_model': 'switching.f1.error',
            'res_id': error_id
        }
        attachment_obj.create(cursor, uid, vals)

    def get_data_from_file(self, data):
        """Process a zip with xml files or a xml file

            Returns a dictionary where the keys are files names and contains
            the xml content
        """
        _data = base64.decodestring(data)
        fileHandle = StringIO.StringIO(_data)
        is_xml = False
        try:
            zfile = zipfile.ZipFile(fileHandle, "r")
        except zipfile.BadZipfile, e:
            is_xml = True

        val = {}
        if is_xml:
            retorn = {'xml': _data}
            val.update({'file1': retorn})
        else:
            for i, info in enumerate(zfile.infolist()):
                fname = info.filename
                xml = zfile.read(fname)

                retorn = {'xml': xml}
                val.update({basename(fname): retorn})

        return val

    def action_cancel(self, cursor, uid, ids, context=None):
        """Cancel.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    def _default_state(self, cursor, uid, context=None):
        if not context:
            context = {}
        return 'init'

    _columns = {
        'file': fields.binary('File'),
        'state': fields.char('State', size=16),
        'info': fields.text('Info'),
    }

    _defaults = {
        'state': _default_state,
    }

WizardSwitchingImport()
