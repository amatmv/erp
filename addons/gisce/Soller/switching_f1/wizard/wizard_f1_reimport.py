# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from mongodb_backend.mongodb2 import mdbpool
import base64


class WizardF1Reimport(osv.osv_memory):
    """Wizard F1 file import"""
    _name = 'wizard.f1.reimport'

    def action_f1_reimport(self, cursor, uid, ids, context=None):
        '''
            Reimport f1. Create a new one and delete old
        '''
        attach_obj = self.pool.get('ir.attachment')
        header_obj = self.pool.get('switching.f1.header')

        if not context:
            context = {}

        header_id = context.get('active_id', False)

        if isinstance(header_id, (list, tuple)):
            header_id = header_id[0]
        read_fields = ['xml_attachment_id', 'cups_id', 'polissa_id']
        head_vals = header_obj.read(cursor, uid, header_id, read_fields)
        if head_vals['cups_id'] and head_vals['polissa_id']:
            msg = _(u"Cannot reimport a xml with cups or policy informed")
            raise osv.except_osv('0031', msg)
        read_fields = ['datas']
        attach_vals = attach_obj.read(cursor, uid,
                                      head_vals['xml_attachment_id'][0],
                                      read_fields)
        attachment = base64.b64decode(attach_vals['datas'])
        # Get f1 in mongo
        switching_mdb = mdbpool.get_collection('switching_f1')
        f1_mongo = switching_mdb.find({'switching_f1_id': header_id})

        # Delete header f1
        header_obj.unlink(cursor, uid, [header_id], context=context)

        new_header_id = header_obj.create_from_dict(cursor, uid,
                                                    f1_mongo[0]['data'],
                                                    attachment,
                                                    context=context)
        return {
            'name': 'New f1 header',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'switching.f1.header',
            'type': 'ir.actions.act_window',
            'view_id': False,
            'domain': "[('id', '=', {})]".format(new_header_id)
        }

    def action_cancel(self, cursor, uid, ids, context=None):
        """Cancel.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    def _default_info(self, cursor, uid, context=None):
        return _('Are you sure f1 reimport?')


    _columns = {
        'info': fields.text('Info'),
    }

    _defaults = {
        'info': _default_info,
    }

WizardF1Reimport()