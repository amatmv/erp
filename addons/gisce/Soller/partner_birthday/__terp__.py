# -*- coding: utf-8 -*-
{
    "name": "Partner birthday",
    "description": """Add Birthday field in partner""",
    "version": "0-dev",
    "author": "Toni Lladó",
    "category": "partner",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_view.xml"
    ],
    "active": False,
    "installable": True
}
