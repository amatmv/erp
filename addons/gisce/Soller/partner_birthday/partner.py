# -*- encoding: utf-8 -*-

from osv import fields,osv
import tools
import ir
import pooler
from tools.translate import _

class PartnerBirthday(osv.osv):
    _inherit = 'res.partner'

    _columns = {
        'birthday' : fields.date("Birthday"),
    }

PartnerBirthday()