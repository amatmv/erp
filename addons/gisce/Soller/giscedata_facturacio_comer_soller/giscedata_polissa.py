# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields


class SollerExtendGiscedataPolissaComer(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def onchange_tipo_pago(self, cr, uid, ids, tipo_pago, pagador, context=None):
        '''sobreescrivim la funcio general per afegir mes comprovacions'''

        payment_mode_obj = self.pool.get('payment.mode')
        payment_type_obj = self.pool.get('payment.type')

        res = super(SollerExtendGiscedataPolissaComer, self).\
                    onchange_tipo_pago(cr, uid, ids, tipo_pago, pagador, context)
        if not tipo_pago:
            return res

        tipus_pagament = payment_type_obj.browse(cr, uid, tipo_pago)
        if tipus_pagament.code == 'CAJA':
            #esborrem el grup cobratori existent
            res['value'].update({'payment_mode_id': False})
            #quant es caixa, el payment_mode no pot ser el 0
            res['domain'].update({'payment_mode_id': [('name', '!=', '0')]})
        elif tipus_pagament.code == 'RECIBO_CSB':
            #quan canviem a rebut, hem de tornar a posar el compte bancari
            if res['value']['payment_mode_id']:
                res2 = self.onchange_payment_mode(cr, uid, ids,
                                res['value']['payment_mode_id'],
                                False, context)
                for key in res2.keys():
                    res[key].update(res2[key])
                res['value'].update({'bank': False})
            else:
                res['value'].update({'bank': False, 'payment_mode_id': False})
            res['domain'].update({'payment_mode_id': [('name', '=', '0')]})
        else:
            res['warning'].update({'title': 'Error',
                    'message': u"Aquest tipus de pagament no està permés"})
            res['value'].update({'tipo_pago': False})
        return res

    def onchange_payment_mode(self, cr, uid, ids, payment_mode_id,
                              bank, context=None):

        payment_mode_obj = self.pool.get('payment.mode')

        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not payment_mode_id:
            return res
        payment_mode = payment_mode_obj.browse(cr, uid, payment_mode_id)

        if payment_mode.require_bank_account:
            if not bank:
                res['warning'].update({'title': 'Avís',
                    'message': u"Aquest grup de pagament "
                               u"necessita un compte bancari"})
        else:
            #Si te compte assignat, i no es necessari, ho esborrem
            if bank:
                res['warning'].update({'title': 'Avís',
                    'message': u"Aquest grup de pagament no "
                               u"permet un compte bancari"})
            res['value'].update({'bank': False})

        return res

    def onchange_bank(self, cr, uid, ids, tipo_pago, payment_mode,
                      bank, context=None):
        if not context:
            context = {}
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if payment_mode:
            res.update(self.onchange_payment_mode(cr, uid, ids, payment_mode,
                                                  bank, context))
        elif tipo_pago:
            payment_type_obj = self.pool.get('payment.type')
            tipus_pagament = payment_type_obj.browse(cr, uid, tipo_pago)
            if tipus_pagament.code == 'CAJA':
                res['value'].update({'bank': False})
                res['warning'].update({'title': 'Avís',
                    'message': u"Aquest tipus de pagament no "
                               u"permet un compte bancari"})
        return res

    def _check_tipo_pago(self, cr, uid, ids, context=None):
        '''funcio que retorna un error quan hi ha una combinació de
        tipo_pago, payment_mode i bank incorrecta'''

        for polissa in self.browse(cr, uid, ids):
            #En esborrany no fem la comprobacio
            if polissa.state == 'esborrany':
                continue
            #no es permeten pagaments fora de rebut o caixa
            if polissa.tipo_pago.code not in ('RECIBO_CSB', 'CAJA'):
                return False
            #si es rebut ha de tindre compte bancari i payment_mode 0
            elif polissa.tipo_pago.code == 'RECIBO_CSB':
                if not polissa.payment_mode_id:
                    return False
                if polissa.payment_mode_id.name != '0':
                    return False
                if not polissa.bank:
                    return False
            #si es per caixa, payment_mode <> 0 i no ha de tindre compte
            elif polissa.tipo_pago.code == 'CAJA':
                if not polissa.payment_mode_id:
                    return False
                if polissa.payment_mode_id.name == '0':
                    return False
                if polissa.bank:
                    return False

        return True

    _constraints = [
        (_check_tipo_pago,
         u"Error: No es pot validar la combinació de forma "
         u"de pago, grup cobratori i compte bancari",
         ['tipo_pago', 'payment_mode_id', 'bank']),
    ]
SollerExtendGiscedataPolissaComer()
