SELECT
  polissa.id,
  m2.cie,
  polissa.name,
  m2.potencia,
  m2.ref_dist,
  m2.facturacio_potencia,
  tarifa.name AS tarifa_name,
  distribuidora.id AS distribuidora_id,
  distribuidora.name AS distribuidora_name,
  titular.name AS titular_name,
  titular.vat AS titular_nif,
  pricelist.name AS pricelist_name,
  m2.data_final AS modcontractual_data_final
FROM giscedata_polissa_modcontractual m2
INNER JOIN giscedata_polissa_tarifa tarifa ON m2.tarifa = tarifa.id
INNER JOIN res_partner distribuidora on m2.distribuidora = distribuidora.id
INNER JOIN res_partner titular on m2.titular = titular.id
INNER JOIN product_pricelist pricelist on m2.llista_preu = pricelist.id
INNER JOIN giscedata_polissa polissa on m2.polissa_id = polissa.id
WHERE polissa.id = %s -- polissa_id
AND m2.data_inici <= %s -- data
AND m2.data_final >= %s -- data
