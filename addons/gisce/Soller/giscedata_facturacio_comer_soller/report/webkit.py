# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        if 'report_factura_webkit_duplicado' in name:
            logo = True
            duplicate = True
            personal_data = True
        elif 'report_factura_webkit_noheader' in name:
            logo = False
            duplicate = False
            personal_data = True
        elif 'report_factura_webkit_nopersonaldata' in name:
            logo = True
            duplicate = False
            personal_data = False
        else:
            logo = True
            duplicate = False
            personal_data = True
        self.localcontext.update({
            'logo': logo,
            'duplicate': duplicate,
            'personal_data': personal_data
        })

webkit_report.WebKitParser(
    'report.report_factura_webkit',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_duplicado',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_noheader',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_20150301',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20150301.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_duplicado_20150301',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20150301.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_noheader_20150301',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20150301.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_20160101',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20160101.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_duplicado_20160101',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20160101.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_noheader_20160101',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20160101.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_20161101',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20161101.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_duplicado_20161101',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20161101.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_noheader_20161101',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20161101.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_20170301',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20170301.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_duplicado_20170301',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20170301.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_noheader_20170301',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20170301.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_nopersonaldata_20170301',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20170301.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_20180201',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20180201.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_duplicado_20180201',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20180201.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_noheader_20180201',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20180201.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.report_factura_webkit_nopersonaldata_20180201',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_soller/report/factura-20180201.mako',
    parser=report_webkit_html
)
