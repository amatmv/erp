<%

from tools import config
from itertools import groupby

addons_path = config['addons_path']
cursor = objects[0]._cr
uid = user.id
pool = objects[0].pool
polissa_obj = pool.get('giscedata.polissa')
address_obj = pool.get('res.partner.address')

historic_sql_path = '%s/giscedata_facturacio_comer_soller/report/historic.sql' % addons_path

historic_sql = open(historic_sql_path).read()

pagos_capacidad = {
    '2.0A': 0.009812,
    '2.1A': 0.009812,
    '2.0DHA': 0.0052,
    '2.1DHA': 0.0052,
    }
barras_central = {
    '2.0A': 1.14,
    '2.1A': 1.14,
    '2.0DHA': 1.12,
    '2.1DHA': 1.12,
    }

retribucion = 0.00024810 + 0.0010865

%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>

       <script language="javascript" type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/jquery.jqplot.min.js"></script>

         <script type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/plugins/jqplot.dateAxisRenderer.min.js"></script>
         <script type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
         <script type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
         <script type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
         <script type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
         <script type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/plugins/jqplot.barRenderer.min.js"></script>
         <script type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/plugins/jqplot.pieRenderer.min.js"></script>
         <script type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/plugins/jqplot.highlighter.min.js"></script>
         <script type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/plugins/jqplot.cursor.min.js"></script>
         <script type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/plugins/jqplot.pointLabels.min.js"></script>

         <script language="javascript" type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/js/ot-translation.js"></script>
         <script language="javascript" type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/js/ot-common.js"></script>
         <script language="javascript" type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/js/historic.js"></script>
         <script language="javascript" type="text/javascript" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/js/destino1.js"></script>

         <link rel="stylesheet" type="text/css" href="${addons_path}/giscedata_facturacio_comer_soller/report/static/jqplot/jquery.jqplot.min.css"/>
         <link rel="stylesheet" type="text/css" href="${addons_path}/giscedata_facturacio_comer_soller/report/static/css/print.css"/>

</head>
<body>
%for factura in objects:
<%
setLang(factura.lang_partner)

generacion = [(_('Renovable'), '20,3%'),
              (_('Cogeneració d\'alta eficiència'), '1,6%'),
              (_('Cogeneració'), '12,2%'),
              (_('CC Gas Natural'), '13,7%'),
              (_('Carbó'), '19,0%'),
              (_('Fuel/Gas'), '4,7%'),
              (_('Nuclear'), '26,7%'),
              (_('Altres'), '1,8%')
             ]

impacto_medi = [(_('Emissions de diòxid de carboni'),
                 '0,27',
                 _('Expressat en kilograms de diòxid de carboni per kWh')),
                (_('Residus radiactius alta activitat'),
                 '0,56',
                 _('Expressat en miligrams per kWh')),
               ]

# Look for invoice number depending on factura state
invoice_number = '<b>%s:</b> %s' % (_('Número'), factura.number)
if factura.state == 'proforma2':
    invoice_number = '<b>%s</b>' % _('FACTURA PROFORMA')
elif factura.state == 'draft':
    invoice_number = '<b>%s</b>' % _('FACTURA BORRADOR')

polissa = polissa_obj.browse(cursor, uid, factura.polissa_id.id,
                             context={'date': factura.data_final.val})

# Check por CUPS city
if factura.cups_id.id_poblacio:
    cups_poblacio = factura.cups_id.id_poblacio.name.upper()
elif factura.cups_id.id_municipi:
    cups_poblacio = factura.cups_id.id_municipi.name.upper()
else:
    cups_poblacio = ''

lines_order = {'potencia': 1,
               'energia': 2,
               'reactiva': 3,
               'exces_potencia': 4,
               'lloguer': 5,
               'altres': 6}    

lines_desc = {'potencia': _('Potència'),
              'energia': _('Energia'),
              'reactiva': _('Reactiva'),
              'exces_potencia': _('Excés de potència'),
              'lloguer': _('Lloguer d\'equips de mesura'),
              'altres': _('Altres conceptes'),
              'tax': _('Impostos')}

# Loop invoice lines
total_tipus = {}
totals = {}
lines = {}
total_energia = 0
for line in factura.linia_ids:
    line_vals = line.read()[0]
    tipus = line_vals['tipus']
    weight = 10
    for tax in line.invoice_line_tax_id:
        tax_name = tax.name.lower()
        if 'especial' in tax_name:
            weight += 1000
        elif 'iva' in tax_name:
            weight += 100
    weight -= lines_order[tipus]
    totals.setdefault(weight, {})
    totals_detail = {'desc': lines_desc[tipus],
                     'amount': 0,
                     'amount_atr': 0}
    totals[weight].setdefault(tipus, totals_detail )
    totals[weight][tipus]['amount'] += line_vals['price_subtotal']
    totals[weight][tipus]['amount_atr'] += line_vals['atrprice_subtotal']
    total_tipus.setdefault(tipus, {'amount': 0, 'amount_atr': 0})
    total_tipus[tipus]['amount'] += line_vals['price_subtotal']
    total_tipus[tipus]['amount_atr'] += line_vals['atrprice_subtotal']
    if not line_vals['isdiscount'] and tipus == 'energia':
        total_energia += line_vals['quantity']
    #Build lines dict
    lines.setdefault(weight, {})
    lines[weight].setdefault(tipus, [])
    if tipus == 'potencia':
        period = ''
        uom_name = ''
        uom_vals = (line_vals['uos_id']
                    and line_vals['uos_id'][1].split('/')
                    or '')
        if uom_vals:
            uom_name = uom_vals[0]
            if line_vals['multi'] <> 1:
                if 'mes' in uom_vals:
                    period = 'meses'
                elif 'dia' in uom_vals:
                    period = 'dias'
            else:
                period = uom_vals[1]

        desc = '%s: %s %s x %s %s x %s € %s' % (
                        line_vals['name'],
                        formatLang(line_vals['quantity'], digits=3),
                        uom_name,
                        formatLang(line_vals['multi'], digits=2),
                        period,
                        formatLang(line_vals['price_unit_multi'], digits=6),
                        line_vals['uos_id'] and line_vals['uos_id'][1] or '')
        key = line_vals['name']
    elif tipus in ('energia', 'reactiva', 'exces_potencia'):
        if tipus == 'exces_potencia':
            quantity = line_vals['multi'] * line_vals['quantity']
        else:
            quantity = line_vals['quantity']
        desc = '%s: %s %s x %s €/%s' % (
                        line_vals['name'],
                        formatLang(quantity, digits=2),
                        line_vals['uos_id'] and line_vals['uos_id'][1] or '',
                        formatLang(line_vals['price_unit_multi'], digits=6),
                        line_vals['uos_id'] and line_vals['uos_id'][1] or '')
        key = line_vals['name']
    elif tipus == 'lloguer':
        if line_vals['quantity'] <> 1:
            period = _('mesos')
        else:
            period = _('mes')
        quantity = line_vals['quantity'] * line_vals['multi']
        desc = '%s %s %s x %s €/%s' % (
                        line_vals['name'],
                        formatLang(quantity, digits=2),
                        period,
                        formatLang(line_vals['price_unit_multi'], digits=2),
                        _('mes'))
        key = 'P0'
    elif tipus == 'altres':
        quantity = line_vals['quantity'] * line_vals['multi']
        if line_vals['uos_id'][1] == 'kWh':
            desc = '%s %s %s x %s €' % (
                        line_vals['name'],
                        formatLang(quantity, digits=2),
                        line_vals['uos_id'][1],
                        formatLang(line_vals['price_unit_multi'], digits=6))
        else:
            desc = '%s %s x %s €' % (
                        line_vals['name'],
                        formatLang(quantity, digits=2),
                        formatLang(line_vals['price_unit_multi'], digits=2))
        key = 'P0'
    if line_vals['isdiscount']:
        key += '1'
    else:
        key += '0'
    line_short = {'key': key,
                  'desc': desc,
                  'amount': line_vals['price_subtotal'],
                  'notes': line_vals['note'],
                  'isdiscount': line_vals['isdiscount']}
    lines[weight][tipus].append(line_short)
# Loop tax lines
total_tax = 0
for tax in factura.tax_line:
    tipus = 'tax'
    base_amount = abs(tax.base_amount)
    tax_amount = abs(tax.tax_amount)
    if 'especial' in tax.name.lower():
        tax_name = _('Impost especial sobre l\'electricitat')
        weight = 1000
        desc = '%s 4,864 %% %s %s x 1,05113' % (
                        tax_name,
                        _('damunt'),
                        formatLang(base_amount, digits=2))
    else:
        tax_name = tax.name
        weight = 100
        desc = '%s %s %s' % (tax.name, _('damunt'),
                                formatLang(abs(base_amount), digits=2))
    totals.setdefault(weight, {})
    totals_detail = {'desc': tax_name,
                     'amount': tax_amount}
    totals[weight].setdefault(tipus, totals_detail)
    # Insert taxes in lines for printing
    lines.setdefault(weight, {})
    lines[weight].setdefault(tipus, [])
    key = 'P0'
    tax_short = {'key': key,
                 'desc': desc,
                 'amount': tax_amount}
    lines[weight][tipus].append(tax_short)
    total_tax += tax_amount

totals = sorted(totals.items(), reverse=True)       
lines = sorted(lines.items(), reverse=True)

# Loop measures
keyfunc1 = lambda a: (a['comptador'], a['data_actual'], a['data_anterior'])
keyfunc2 = lambda a: (a['tipus'])
keysort = lambda a: (a['comptador'], a['data_actual'], a['data_anterior'], a['tipus'], a['name'])
keysort_pot = lambda a: a['name']
measures = []
measures_pot = {}
for measure in factura.lectures_energia_ids:
    vals = measure.read()[0]
    measures.append(vals)
for measure in factura.lectures_potencia_ids:
    vals = measure.read()[0]
    comptador = vals['comptador']
    measures_pot.setdefault(comptador, [])
    measures_pot[comptador].append(vals)
measures = sorted(measures, key=keysort)


if polissa.tarifa.name.startswith('2'):
    short_measures = True
else:
    short_measures = False

#Company data
company = factura.company_id.partner_id
company_addresses = company.address_get(adr_pref=['contact'])
if 'contact' in company_addresses:
    company_address = address_obj.read(cursor, uid, company_addresses['contact'])
else:
    company_address = {}

#Distri data
distri = polissa.distribuidora
distri_addresses = distri.address_get(adr_pref=['contact'])
if 'contact' in distri_addresses:
    distri_address = address_obj.read(cursor, uid, distri_addresses['contact'], ['phone', 'mobile'])
else:
    distri_address = {'phone': '', 'mobile': ''}
history_limit = 7
#History
factura._cr.execute(historic_sql, (polissa.id, factura.data_final.val, history_limit))
history_data = {'dates': [], 'values': []}
for value in factura._cr.fetchall():
    history_data['dates'].append(formatLang(value[2], date=True))
    history_data['values'].append(int(value[3]))
history_data['dates'] = history_data['dates'][::-1]
history_data['values'] = history_data['values'][::-1]

#Energy dest
tarifa_name = factura.tarifa_acces_id.name
if factura.potencia < 15:
    pot_peajes = 'potencia' in total_tipus and total_tipus['potencia']['amount_atr'] or 0
    react_peajes = 'reactiva' in total_tipus and total_tipus['reactiva']['amount_atr'] or 0
    exces_peajes = 'exces_potencia' in total_tipus and total_tipus['exces_potencia']['amount_atr'] or 0
    ene_peajes = 'energia' in total_tipus and total_tipus['energia']['amount_atr'] or 0
    regulado = pot_peajes + react_peajes + exces_peajes
    regulado += ene_peajes
    regulado += (total_energia * pagos_capacidad[tarifa_name] +
                 total_energia * retribucion) * barras_central[tarifa_name]
    regulado = round(regulado, 2)
    total_tax = round(total_tax, 2)
    produccion = round(factura.amount_total - 
                  regulado -
                  total_tax -
                  ('altres' in total_tipus and total_tipus['altres']['amount'] or 0) -
                  ('lloguer' in total_tipus and total_tipus['lloguer']['amount'] or 0)
                  , 2)
    data_destino1 = {'values': [produccion, total_tax, regulado],
                     'names': ['P', 'I', 'R'],
                     'colors': ['#e5f5f9', '#fec44f', '#a6bddb']
                    }
    renovables = round(regulado * 0.3628, 2)
    redes = round(regulado * 0.3212, 2)
    otros = regulado - renovables - redes
    data_destino2 = {'values': [renovables, redes, otros],
                     'names': ['IR', 'RD', 'O'],
                     'colors': ['#e5f5f9', '#fec44f', '#a6bddb']
                    }

# Text for duplicates, proformas and refunds
extra_text = []
if factura.tipo_rectificadora in ('A', 'B'):
    extra_text.append(_('ABONAMENT'))
if factura.state == 'proforma2':
    extra_text.append(_('PROFORMA'))
if duplicate:
    extra_text.append(_('DUPLICAT'))
extra_text_invoice = ' - '.join(extra_text)
%>
<div class="container">
    <div class="row top-padding-fix">
        <div class="col-xs-5">    
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Dades de l\'emisor')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <p><b><dfn>${company.name.upper()}</dfn></b> (${company.vat.upper()})</p>
                        <p>${'%s, %s - %s' % (company_address['street'], company_address['zip'], company_address['city'].upper())}</p>
                        <p>${'%s - %s' %(company_address['state_id'][1].upper(), company_address['country_id'][1].upper())}</p>
                        <p>${_('Telèfon')} ${company_address['phone']}, ${_('Fax')} ${company_address['fax']}</p>
                        <p>${company.website}</p>
                        <p>${company_address['email']}</p>
                        <p style="font-size: 8px;">${factura.company_id.rml_footer1}</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Dades de factura')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <p>${invoice_number}</p>
                        % if factura.tipo_rectificadora in ('A', 'B'):
                        <p class="refund">${_('Abonament de la factura')} ${factura.ref.number}</p>
                        <p class="refund">${_('Data d\'exigibilitat')} ${formatLang(factura.ref.date_due, date=True)}
                        % endif
                        % if factura.tipo_rectificadora  == 'R':
                        <p class="refund">${_('Rectificativa de la seva factura')} ${factura.ref.number}</p>
                        <p class="refund">${_('Data d\'exigibilitat')} ${formatLang(factura.ref.date_due, date=True)}
                        % endif

                        <p><b>${_('Data')}:</b> ${factura.date_invoice}</p>
                        <p><b>${_('NIF/CIF')}:</b> ${factura.partner_id.vat}</p>
                        <p><b>${_('Període')}:</b> ${factura.data_inici} ${_('a')} ${factura.data_final}</p>
                        % if factura.payment_type.name == 'CAJA':
                        <p><b>${_('Forma de pagament')}:</b> ${'%s (%s)' % (factura.payment_type.name, factura.payment_mode_id.name)}</p>
                        % else:
                        <p><b>${_('Forma de pagament')}:</b> ${factura.payment_type.name}</p>
                        % endif
                        % if factura.partner_bank:
                        <p><b>IBAN:</b> ${factura.partner_bank.printable_iban[:-4] + '****'}</p>
                        % endif 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-7">
            <div class="col-xs-12" style="min-height: 148px;">
                % if logo:
                <img style="width: 558px; float:right;" src="${addons_path}/giscedata_facturacio_comer_soller/report/cabecera-facturas-nueva.png"/>
                % endif
            </div>
            <div class="col-xs-12">
                <div class="invoice-address">
                        <p>${factura.partner_id.name.upper()}</p>
                        <p>${factura.address_invoice_id.street.upper()}</p>
                        % if factura.address_invoice_id.zip:
                        <p>${'%s - %s' % (factura.address_invoice_id.zip, factura.address_invoice_id.city and factura.address_invoice_id.city.upper() or '')}</p>
                        % else:
                        <p>${factura.address_invoice_id.city.upper()}</p>
                        % endif
                        %if factura.address_invoice_id.state_id and factura.address_invoice_id.country_id:
                        <p>${'%s - %s' % (factura.address_invoice_id.state_id.name.upper(), factura.address_invoice_id.country_id.name.upper())}</p>
                        % elif factura.address_invoice_id.state_id:
                        <p>${factura.address_invoice_id.state_id.name.upper()}</p>
                        % elif factura.address_invoice_id.country_id:
                        <p>${factura.address_invoice_id.country_id.name.upper()}</p>
                        % endif
                        
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            % if extra_text:
                <p class="duplicado">${extra_text_invoice}</p>
            % else:
                <div style="margin-top: 50px;">
            </div>
            % endif
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Resum de factura')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <table >
                        % for item in totals:
                        % for key, value in item[1].iteritems():
                        <tr>
                            <td>${value['desc']}</td>
                            <td class="amount currency">${formatLang(value['amount'], digits=2)}</td>
                        </tr>
                        % endfor    
                        % endfor    
                    </table>
                </div>
                <div class="panel-footer">
                    <table style="width: 100%">
                        <tr>
                            <td><h4>${_('Total')}</h4></td>
                            % if factura.type == 'out_refund':
                                <td class="amount currency"><b>${formatLang(factura.amount_total * -1, digits=2)}</b></td>
                            % else:
                                <td class="amount currency"><b>${formatLang(factura.amount_total, digits=2)}</b></td>
                            % endif    
                        <tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Historial de consum')}</h4>
                </div>
                <div class="panel-body">
                    <div id="historic_${factura.id}" style="height: 200px;">
                    </div>
                    <script>
                        data = ${history_data};
                        historic('historic_${factura.id}', data, 'es');
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Informació')}</h4>
                </div>
                <div class="panel-body">
                    <img class="spam_image" src="${addons_path}/giscedata_facturacio_comer_soller/report/static/img/factura_112014.png"/>
                </div>
            </div>
        </div>
    </div>
    <p class="page-break"></p>

    <div class="row top-padding-fix">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>${_('Dades del contracte')}</h4>
                </div>
                <div class="panel-body invoice-data">
                    <div class="col-xs-6">
                        <p><b>${_('Titular')}:</b> ${polissa.titular.name}</p>
                        <p><b>${_('NIF/CIF Titular')}:</b> ${polissa.titular_nif}</p>
                        <p><b>${_('Adreça')}:</b> ${factura.cups_id.direccio}</p>
                        <p><b>${_('Població')}:</b> ${cups_poblacio}</p>
                        <p><b>${_('Tarifa')}:</b> ${polissa.llista_preu.name}</p>
                        <p><b>${_('Tarifa d\'accés')}:</b> ${polissa.tarifa.name}</p>
                        <p><b>${_('Telèfon d\'atenció al client')}:</b> 900 373 417</p>
                        <p><b>${_('Avaries')} (${polissa.distribuidora.name}):</b> ${distri_address['phone']} ${distri_address['mobile'] and distri_address['mobile'] or ''}</p>
                    </div>
                    <div class="col-xs-6">
                        <p><b>CUPS:</b> ${factura.cups_id.name}</p>
                        <p><b>${_('Potència')}:</b> ${formatLang(polissa.potencia, digits=3)} kW</p>
                        <p><b>${_('Contracte')}:</b> ${polissa.name}</p>
                        <p><b>${_('Contracte d\'accés')}:</b> ${polissa.ref_dist}</p>
                        <p><b>${_('Distribuïdora')}:</b> ${polissa.distribuidora.name}</p>
                        <p><b>${_('Vigència fins')}:</b> ${formatLang(polissa.modcontractual_activa.data_final, date=True)}</p>
                        <p><b>${_('Email d\'atenció al client')}:</b> ${company_address['email']}</p>
                    </div>
                    <div class="col-xs-12">
                        <p class="long_text">${_('Per reclamacions sobre el contracte o facturacions podrà dirigir-se, en cas de tractar-se d\'una persona física, a la JUNTA ARBITRAL NACIONAL DE CONSUM en el telèfon 918224418 o mitjançant l\'adreça de correu: junta-nacional@consumo-inc.es. Així mateix, podrà acudir alternativament a la Conselleria en matèria d\'Energia de la comunitat autònoma on es situi el punt de subministrament.')}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-8">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Detall de conceptes')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <table>
                            % for line in lines:
                            % for key, value in line[1].items():
                            <tr>
                                <td><b>${lines_desc[key]}</b></td>
                            </tr>
                            % for item in sorted(value, key=lambda x: x['key']):
                            % if 'isdiscount' in item and item['isdiscount']:
                            <tr>
                                <td class="discount">${item['notes']}</td>
                            </tr>
                            <tr>
                                <td class="discount">${item['desc']}</td>
                                <td class="amount currency">${formatLang(item['amount'], digits=2)}</td>
                            </tr>
                            % else:
                            <tr>
                                <td class="line_desc">${item['desc']}</td>
                                <td class="amount currency">${formatLang(item['amount'], digits=2)}</td>
                            </tr>
                            % endif
                            % endfor
                            % if key in total_tipus and total_tipus[key]['amount_atr'] <> 0:
                            % if total_tipus[key]['amount_atr'] > total_tipus[key]['amount']:
                            <tr>
                                <td class="atr_line"> ${_('L\'import total dels peatges és de')} ${formatLang(total_tipus[key]['amount_atr'], digits=2)} €</td>
                            </tr>
                            % else:
                            <tr>
                                <td class="atr_line"> ${_('En total')} ${formatLang(total_tipus[key]['amount'], digits=2)} € ${_('dels quals')} ${formatLang(total_tipus[key]['amount_atr'], digits=2)} € ${_('corresponen a peatges')}</td>
                            </tr>
                            % endif
                            % endif
                            % endfor
                            % endfor
                        </table>
                    </div>
                    <div class="panel-footer">
                        <table style="width: 100%">
                            <tr>
                                <td><h4>${_('Import total')}</h4></td>
                                % if factura.type == 'out_refund':
                                    <td class="amount currency"><b>${formatLang(factura.amount_total*-1, digits=2)}</b></td>
                                % else:
                                    <td class="amount currency"><b>${formatLang(factura.amount_total, digits=2)}</b></td>
                                % endif    
                            <tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Lectures')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        % if len(measures) == 0:
                        <p>${_('Sense lectures en el període de facturació')}</p>
                        % else:
                        % if short_measures: 
                        <table style="width: 60%;">
                        % else:
                        <table>
                        % endif
                            % for keys, group in groupby(measures, key=keyfunc1):
                            <tr>
                                % if short_measures: 
                                <td colspan="3"><b>${_('Lectures del comptador')} ${keys[0]}</b></td>
                                % else:
                                <td colspan="2"><b>${_('Lectures del comptador %s entre el %s y el %s') % (keys[0], formatLang(keys[2], date=True), formatLang(keys[1], date=True))}</b></td>
                                % endif
                            </tr>
                            % for tipus, measures_list in groupby(group, key=keyfunc2):
                            <% measures_list = list(measures_list) %>
                            % if not short_measures:
                            <tr>
                                <td colspan="2">${tipus.upper()}</td>
                            </tr>
                            % for measure in zip(measures_list[:3], measures_list[3:]):
                            <tr>
                                <td>${'%s: %s - %s = %s' % (measure[0]['name'][-3:-1], measure[0]['lect_actual'], measure[0]['lect_anterior'], measure[0]['consum'])}</td>
                                <td>${'%s: %s - %s = %s' % (measure[1]['name'][-3:-1], measure[1]['lect_actual'], measure[1]['lect_anterior'], measure[1]['consum'])}</td>
                            </tr>
                            % endfor
                            % else:
                            % for measure in measures_list:
                            % if measure['tipus'] == 'reactiva':
                                <% continue %>
                            % endif
                            <tr>
                                <td><dfn>${_('Període')} ${measure['name'][-3:-1]}</dfn></td>
                                <td>${_('Lectura final')} (${formatLang(measure['data_actual'], date=True)})</td>
                                <td class="amount">${formatLang(measure['lect_actual'], digits=0)} kWh</td>
                            </tr>
                            <tr>
                                <td> </td>
                                <td>${_('Lectura inicial')} (${formatLang(measure['data_anterior'], date=True)})</td>
                                <td class="amount">${formatLang(measure['lect_anterior'], digits=0)} kWh</td>
                            </tr>
                            <tr>
                                <td> </td>
                                <td>${_('Consum')}</td>
                                <td class="amount">${formatLang(measure['consum'], digits=0)} kWh</td>
                            </tr>
                            % endfor
                            % endif
                            % endfor
                            % if keys[0] in measures_pot and polissa.facturacio_potencia == 'max':
                            <% meter_pot_measures = sorted(measures_pot[keys[0]], key=keysort_pot) %>
                            <tr>
                                <td colspan="2">${_('POTÈNCIA')}</td>
                            </tr>
                            % if not short_measures:
                            % for measure_pot in zip(meter_pot_measures[:3], meter_pot_measures[3:]):
                            <tr>
                                <td>${_('%s: Pot: %s, Max: %.0f, Excés: %.0f') % (measure_pot[0]['name'], formatLang(measure_pot[0]['pot_contract'], digits=3), measure_pot[0]['pot_maximetre'], measure_pot[0]['exces'])}</td>
                                <td>${_('%s: Pot: %s, Max: %.0f, Excés: %.0f') % (measure_pot[1]['name'], formatLang(measure_pot[1]['pot_contract'], digits=3), measure_pot[1]['pot_maximetre'], measure_pot[1]['exces'])}</td>
                            </tr>
                            % endfor
                            % else:
                            % for measure_pot in meter_pot_measures:
                            <tr>
                                <td colspan="3"><dfn>${_('Període')} ${measure_pot['name']}</dfn>, ${_('Potència')}: ${formatLang(measure_pot['pot_contract'], digits=3)}, ${_('Maxímetre')}: ${'%.0f' % measure_pot['pot_maximetre']}, ${_('Excés')}: ${'%.0f' % measure_pot['exces']}</td>
                            </tr>
                            % endfor 
                            % endif 
                            % endif
                            % endfor
                        </table>
                        % endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Mescla de producció')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <table>
                            % for key, value in generacion:
                            <tr>
                                <td>${key}</td>
                                <td style="text-align: right;">${value}</td>
                            </tr>
                            % endfor
                        </table>
                    </div>
            </div>
        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Impacte mediambiental')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <table>
                            % for desc, quantity, unit in impacto_medi:
                            <tr>
                                <td>${desc}</td>
                                <td style="text-align: right;">${quantity}</td>
                            </tr>
                            <tr>
                                <td class="impacto_unit" colspan="2">${unit}</td>
                            </tr>
                            % endfor
                        </table>
                    </div>
            </div>
            % if factura.potencia < 15:
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>${_('Destí de l\'import')}</h4>
                    </div>
                    <div class="panel-body invoice-data">
                        <div class="col-xs-12">
                            <div id="destino1_${factura.id}" style="height: 200px;">
                            </div>
                            <script>
                                data = ${data_destino1};
                                destino1('destino1_${factura.id}', data, 'es');
                            </script> 
                        </div>
                        <div class="col-xs-12">
                            <table style="width: 100%;">
                                <tr>
                                    <td>${_('Total de factura')}</td>
                                    <td class="amount currency">${formatLang(factura.amount_total, digits=2)}</td>
                                </tr>
                                <tr>
                                    <td><b>I</b> ${_('Impostos')}</td>
                                    <td class="amount currency">${formatLang(total_tax, digits=2)}</td>
                                </tr>
                                <tr>
                                    <td><b>P</b> ${_('Producció')}</td>
                                    <td class="amount currency">${formatLang(produccion, digits=2)}</td>
                                </tr>
                                <tr>
                                    <td><b>R</b> ${_('Regulats')}</td>
                                    <td class="amount currency">${formatLang(regulado, digits=2)}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="long_text">${_('Als imports desglosats en el gràfic s\'ha d\'afegir el lloguer dels equips de mesura i altres conceptes')}</td>
                                <tr>
                            </table>
                        </div>
                        <div class="col-xs-12">
                            <p class="long_text">${_('Els costs regulats del gràfic anterior es subdivideixen en')}:</p>
                            <div id="destino2_${factura.id}" style="height: 200px;">
                            </div>
                            <script>
                                data = ${data_destino2};
                                destino1('destino2_${factura.id}', data, 'es');
                            </script>
                        </div>
                        <div class="col-xs-12">
                            <table style="width: 100%;">
                                <tr>
                                    <td><b>IR</b> ${_('Incentius a les energies renovables, cogeneració i residus')}</td>
                                    <td class="amount currency">${formatLang(renovables, digits=2)}</td>
                                </tr>
                                <tr>
                                    <td><b>RD</b> ${_('Costs de xarxes de distribució i transport')}</td>
                                    <td class="amount currency">${formatLang(redes, digits=2)}</td>
                                </tr>
                                <tr>
                                    <td><b>O</b> ${_('Altres costs regulats')}</td>
                                    <td class="amount currency">${formatLang(otros, digits=2)}</td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            % endif
        </div>
    </div>
</div>
% if objects.index(factura) + 1 != len(objects):
    <p class="page-break"></p>
% endif
%endfor
</body>
</html>
