SELECT * FROM (
SELECT
     giscedata_facturacio_factura.polissa_id AS giscedata_facturacio_factura_polissa_id,
     giscedata_facturacio_factura.id AS giscedata_facturacio_factura_id,
     giscedata_facturacio_factura.data_final AS giscedata_facturacio_data_final,
     round(sum(invoice_line.quantity),0) as total_energia_kw,
     row_number() over (partition by giscedata_facturacio_factura.data_final order by giscedata_facturacio_factura.id desc) as pos
FROM
     giscedata_facturacio_factura
     INNER JOIN account_invoice ON
     giscedata_facturacio_factura.invoice_id = account_invoice.id
     INNER JOIN account_journal journal on journal.id = account_invoice.journal_id
     INNER JOIN giscedata_facturacio_factura_linia factura_linia
     on factura_linia.factura_id = giscedata_facturacio_factura.id
     INNER JOIN account_invoice_line invoice_line
     on invoice_line.id = factura_linia.invoice_line_id
WHERE
    journal.code like 'ENERGIA%%' AND
    account_invoice.type = 'out_invoice'AND
    factura_linia.tipus = 'energia' AND
    factura_linia.isdiscount = False AND
    account_invoice.state not in ('draft', 'proforma2') AND
    giscedata_facturacio_factura.polissa_id = %s AND
    giscedata_facturacio_factura.data_final BETWEEN '2011-01-01' AND %s
GROUP BY
    giscedata_facturacio_factura_polissa_id, giscedata_facturacio_data_final,
    giscedata_facturacio_factura.id) AS historico
WHERE historico.pos = 1
ORDER BY giscedata_facturacio_data_final DESC LIMIT %s
