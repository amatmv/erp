# -*- coding: utf-8 -*-

import jasper_reports
from datetime import datetime
import pooler
from phantom_reports import phantom_report


def _get_report_factura(cr, uid, ids, context):

    vals = {}
    if not context:
        context = {}
    report_name = context.get('report_name', '')
    if not report_name:
        return vals
    report_name = report_name[7:]
    if ids:
        pool = pooler.get_pool(cr.dbname)
        factura_obj = pool.get('giscedata.facturacio.factura')
        history_obj = pool.get('report.jasper.history')
        factura_id = sorted(ids)[0]
        factura_date = factura_obj.read(cr, uid, factura_id,
                                        ['date_invoice'])['date_invoice']

        # search for historified reports based on date_invoice
        search_params = [('original_report_id.report_name', '=', report_name),
                         ('date_from', '<=', factura_date)]
        history_ids = history_obj.search(cr, uid, search_params, limit=1)
        if history_ids:
            history = history_obj.browse(cr, uid, history_ids[0])
            report_name = history.new_report_id.report_name
            vals.update({'name': 'report.%s' % report_name})
    return vals


def _factura_print_factura(cr, uid, ids, data, context):

    vals = {
        'ids': ids,
        'parameters': {'cabecera': False,
                       'duplicado': False},
    }
    vals.update(_get_report_factura(cr, uid, ids, context))
    return vals


def _factura_print_duplicado(cr, uid, ids, data, context):
    vals = {
        'ids': ids,
        'parameters': {'cabecera': False,
                       'duplicado': True},
    }
    vals.update(_get_report_factura(cr, uid, ids, context))
    return vals


def _factura_print_cabecera(cr, uid, ids, data, context):
    vals = {
        'ids': ids,
        'parameters': {'cabecera': True,
                       'duplicado': False},
    }
    vals.update(_get_report_factura(cr, uid, ids, context))
    return vals


def _factura_print_cabecera_duplicado(cr, uid, ids, data, context):
    vals = {
        'ids': ids,
        'parameters': {'cabecera': True,
                       'duplicado': True},
    }
    vals.update(_get_report_factura(cr, uid, ids, context))
    return vals


def _get_phantom_report_name(cr, uid, ids, data, context):

    pool = pooler.get_pool(cr.dbname)
    factura_obj = pool.get('giscedata.facturacio.factura')
    factura_vals = factura_obj.read(cr, uid, ids, ['date_invoice'])
    res = {}
    for factura_val in factura_vals:
        date = factura_val['date_invoice']
        if date < '2014-10-01':
            report_name = 'report_factura_cabecera'
        elif date < '2015-03-01':
            report_name = 'report_factura_webkit'
        elif date < '2016-01-01':
            report_name = 'report_factura_webkit_20150301'
        elif date < '2016-11-01':
            report_name = 'report_factura_webkit_20160101'
        elif date < '2017-03-01':
            report_name = 'report_factura_webkit_20161101'
        elif date < '2018-02-01':
            report_name = 'report_factura_webkit_20170301'
        else:
            report_name = 'report_factura_webkit_20180201'
        res.setdefault(report_name, [])
        res[report_name].append(factura_val['id'])
    return res


def _get_phantom_report_name_duplicado(cr, uid, ids, data, context):

    pool = pooler.get_pool(cr.dbname)
    factura_obj = pool.get('giscedata.facturacio.factura')
    factura_vals = factura_obj.read(cr, uid, ids, ['date_invoice'])
    res = {}
    for factura_val in factura_vals:
        date = factura_val['date_invoice']
        if date < '2014-10-01':
            report_name = 'report_factura_cabecera_duplicado'
        elif date < '2015-03-01':
            report_name = 'report_factura_webkit_duplicado'
        elif date < '2016-01-01':
            report_name = 'report_factura_webkit_duplicado_20150301'
        elif date < '2016-11-01':
            report_name = 'report_factura_webkit_duplicado_20160101'
        elif date < '2017-03-01':
            report_name = 'report_factura_webkit_duplicado_20161101'
        elif date < '2018-02-01':
            report_name = 'report_factura_webkit_duplicado_20170301'
        else:
            report_name = 'report_factura_webkit_duplicado_20180201'
        res.setdefault(report_name, [])
        res[report_name].append(factura_val['id'])
    return res


def _get_phantom_report_name_noheader(cr, uid, ids, data, context):

    pool = pooler.get_pool(cr.dbname)
    factura_obj = pool.get('giscedata.facturacio.factura')
    factura_vals = factura_obj.read(cr, uid, ids, ['date_invoice'])
    res = {}
    for factura_val in factura_vals:
        date = factura_val['date_invoice']
        if date < '2014-10-01':
            report_name = 'giscedata.facturacio.factura'
        elif date < '2015-03-01':
            report_name = 'report_factura_webkit_noheader'
        elif date < '2016-01-01':
            report_name = 'report_factura_webkit_noheader_20150301'
        elif date < '2016-11-01':
            report_name = 'report_factura_webkit_noheader_20160101'
        elif date < '2017-03-01':
            report_name = 'report_factura_webkit_noheader_20161101'
        elif date < '2018-02-01':
            report_name = 'report_factura_webkit_noheader_20170301'
        else:
            report_name = 'report_factura_webkit_noheader_20180201'
        res.setdefault(report_name, [])
        res[report_name].append(factura_val['id'])
    return res


def _get_phantom_report_name_nopersonaldata(cr, uid, ids, data, context):

    pool = pooler.get_pool(cr.dbname)
    factura_obj = pool.get('giscedata.facturacio.factura')
    factura_vals = factura_obj.read(cr, uid, ids, ['date_invoice'])
    res = {}
    for factura_val in factura_vals:
        date = factura_val['date_invoice']
        if date < '2014-10-01':
            report_name = 'report_factura_cabecera'
        elif date < '2015-03-01':
            report_name = 'report_factura_webkit'
        elif date < '2016-01-01':
            report_name = 'report_factura_webkit_20150301'
        elif date < '2016-11-01':
            report_name = 'report_factura_webkit_20160101'
        elif date < '2017-03-01':
            report_name = 'report_factura_webkit_20161101'
        elif date < '2018-02-01':
            report_name = 'report_factura_webkit_nopersonaldata_20170301'
        else:
            report_name = 'report_factura_webkit_nopersonaldata_20180201'
        res.setdefault(report_name, [])
        res[report_name].append(factura_val['id'])
    return res


jasper_reports.report_jasper(
   'report.giscedata.facturacio.factura',
   'giscedata.facturacio.factura',
   parser=_factura_print_factura
)

jasper_reports.report_jasper(
   'report.report_factura_duplicado',
   'giscedata.facturacio.factura',
   parser=_factura_print_duplicado
)

jasper_reports.report_jasper(
   'report.report_factura_cabecera',
   'giscedata.facturacio.factura',
   parser=_factura_print_cabecera
)

jasper_reports.report_jasper(
   'report.report_factura_cabecera_duplicado',
   'giscedata.facturacio.factura',
   parser=_factura_print_cabecera_duplicado
)

phantom_report.phantom_report(
   'report.report_phantom_factura_soller',
   parser=_get_phantom_report_name
)

phantom_report.phantom_report(
   'report.report_phantom_factura_soller_duplicado',
   parser=_get_phantom_report_name_duplicado
)

phantom_report.phantom_report(
   'report.report_phantom_factura_soller_noheader',
   parser=_get_phantom_report_name_noheader
)

phantom_report.phantom_report(
   'report.report_phantom_factura_soller_nopersonaldata',
   parser=_get_phantom_report_name_nopersonaldata
)
