var _chartError = $('#chart_error');
var _chartLoading = $('#chart_loading');
var _chartPlot = $('#chart_plot');

var showError = function(errorText) {
    _chartPlot.hide();
    _chartLoading.hide();
    _chartError.find('span').text(errorText);
    _chartError.show();
}

var showLoading = function() {
    _chartPlot.hide();
    _chartError.hide();
    _chartLoading.show();
}

var showPlot= function() {
    _chartError.hide();
    _chartLoading.hide();
    _chartPlot.empty().show();
}

var loadChart = function(url, data, lang, callback, billing) {
    showLoading();
    if (billing) {
        callback(billing);
    } else {
        $.get(
            url, data
        ).done(function (jsonData) {
            callback(jsonData);
        }).fail(function (xhr, textStatus, errorThrown) {
            var errorText = _t("There was an error loading this chart.", lang);
            showError(errorText);
        });
    }
};

var cssOverride = {
    'jqplot-yaxis-label': {
        'font-size': '10pt',
    },
    'jqplot-xaxis-tick': {
        'font-size': '9pt',
    },
}


var loadCSSProperties = function() {
    /* JQPlot does not load some properties correctly, create elements and assing
     * css properties to get them as variables and use them in the code
     */

    /* jqplot-yaxis-label */
    var element = document.createElement("div");
    element.className = "jqplot-yaxis-label";
    style = window.getComputedStyle(element),
    jqplot_yaxis_label_font_size = style.getPropertyValue('font-size');


    /* jqplot-xaxis-tick */
    var element = document.createElement("div");
    element.className = "jqplot-xaxis-tick";
    style = window.getComputedStyle(element),
    jqplot_xaxis_tick_font_size = style.getPropertyValue('font-size');

};
