/*
 * online-tools.translation
 * ~~~~~~~~~~~~~~
 *
 * :copyrigth: (c) 2014 by El Gas S.A. See AUTHORS for more details.
 * :licese: EUPL, see LICENSE for more details.
 */


/* Translations */
var translations = {
    'There was an error loading this chart.': {
        'ca': 'Ha ocorregut un error carregnat aquesta gràfica.',
        'es': 'Ha ocurrido un error cargando esta gráfica.',
    },

    'This chart is currently not avaliable.': {
        'es': 'En estos momentos el gráfico no está disponible.',
        'ca': 'En aquests moments el gràfic no està disponible.',
    },

    'Yearly consumption comparison': {
        'es': 'Comparación de consumo anual',
        'ca': 'Comparació de consum anual',
    },

    'Monthly consumption comparison': {
        'es': 'Comparación de consumo mensual',
        'ca': 'Comparació de consum mensual',
    },

    'Comparison with previous year': {
        'es': 'Comparación con el año anterior',
        'ca': 'Comparació amb l\'any anterior',
    },

    'Power consumption (kWh)': {
        'es': 'Consumo eléctrico (kWh)',
        'ca': 'Consum elèctric (kWh)',
        'fr': 'La consommation électrique (kWh)',
    },

    'Power consumption from ': {
        'es': 'Consumo eléctrico de las ',
        'ca': 'Consum elèctric de les ',
        'fr': 'La consommation électrique et ',
    },

    ' hours': {
        'es': ' horas',
        'ca': ' hores',
    },

    ' to ': {
        'es': ' hasta las ',
        'ca': ' fins les ',
    },

    'Your home': {
        'es': 'Tu casa',
        'ca': 'Casa teva',
    },

    'Similar homes': {
        'es': 'Casas similares',
        'ca': 'Cases similars',
    },

    'Efficient homes': {
        'es': 'Casas eficientes',
        'ca': 'Cases eficients',
    },

    'Daily power consumption profile': {
        'es': 'Curva diaria de consumo eléctrico',
        'ca': 'Corba diària de consum eléctric',
        'fr': 'courbe de la consommation d\'électricité par jour',
    },

    'Monthly power consumption profile': {
        'es': 'Curva mensual de consumo eléctrico',
        'ca': 'Corba mensual de consum eléctric',
        'fr': 'Courbe mensuelle de la consommation d\'électricité',
    },

   'Power consumption of ': {
        'es': 'Consumo electrico de día ',
        'ca': 'Consum elèctric de dia ',
    },

   'Average Monthly Temperature and Energy Consumption': {
        'es': 'Media de temperatura y consumo eléctrico',
        'ca': 'Mitjana de temperatura i consum elèctric',
    },

    'Average Monthly Energy Consumption': {
        'es': 'Media de consumo eléctrico',
        'ca': 'Mitjana de consum elèctric',
    },

   'Temperature (ºC)':{
        'es': 'Temperatura (ºC)',
        'ca': 'Temperatura (ºC)',
    },
    'Energy': {
        'es': 'Energía',
        'ca': 'Energia',
    },
    'Temperature': {
        'es': 'Temperatura',
        'ca': 'Temperatura',
    },

    'monthes': {
        'en': [
            '', 'January', 'February', 'March', 'April', 'May',
            'June', 'July', 'August', 'September', 'October', 'November',
            'December'
        ],
        'ca': [
            '', 'Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny',
            'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre'
        ],
        'es': [
               '', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
           'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'
       ]
    },
    'short-weekdays': {
        'en': ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        'es': ['do', 'lu', 'ma', 'mi', 'ju', 'vi', 'sa'],
        'ca': ['dg', 'dl', 'dm', 'dc', 'dj', 'dv', 'ds'],
    }
}

var _t = function(string, lang) {
    if (string in translations) {
        if (lang in translations[string]) {
            return translations[string][lang];
        }
    }
    return string
}
