/*
 * online-tools.ot101
 * ~~~~~~~~~~~~~~
 *
 * :copyrigth: (c) 2014 by El Gas S.A. See AUTHORS for more details.
 * :licese: EUPL, see LICENSE for more details.
 *
 * :contributor: apsl.net.
 */

var ot503 = function(dstDivId, url, data, lang) {
    ot503Real(dstDivId, url, data, lang, false);
}

var bt503 = function(dstDivId, url, data, lang, billing) {
    ot503Real(dstDivId, url, data, lang, billing);
}

var ot503Real = function(dstDivId, url, data, lang, billing) {

    // callback for ajax call with plot data in json.
    var ot503callback = function(jsonData) {
        if (!('_items' in jsonData) || jsonData['_items'].length == 0) {
            var errorText = _t("This chart is currently not avaliable.", lang);
            showError(errorText);
            return false;
        }

        var data = jsonData['_items'];

        /* Text translations & overrides */
        if ('text' in jsonData) {
            var text = jsonData['text'];
        } else {
            date = data[0]['day'].toString();
            date = date.slice(0, 4) + '/' + date.slice(4, 6);
            var title = _t('Average Monthly Temperature and Energy Consumption', lang);
            var subtitle = '<br><span class="ot-subtitle">' + date + '</span>';
            var title_text = title + subtitle;
            var yaxis_label = _t('Power consumption (kWh)', lang);
            var y2axis_label = _t('Temperature (ºC)', lang);
            var tooltip_energy = _t('Energy', lang);
            var tooltip_temperature = _t ('Temperature', lang);
        }

        /* Data transfromation for jqplot */
        var ticks = [];
        // fullTicks used for the toolTip
        var fullTicks = [];
        var energies = [];
        var temperatures = [];

        for (var i=0; i < data.length; i++) {
            if (data[i]['consumption'] !== null) {
                energies.push(parseInt(Math.ceil(data[i]['consumption'])));
            } else {
                energies.push(null);
            }
            if (data[i]['temperature'] !== null) {
                temperatures.push(parseInt(Math.ceil(data[i]['temperature'])));
            } else {
                temperatures.push(null);
            }
            if ('tick' in data[i]) {
                ticks.push(data[i][tick]);
                fullTicks.push(data[i][tick]);
            } else {
                date = data[i]['day'].toString();
                var monthNum = parseInt(date.slice(-2));
                ticks.push(monthNum);
                fullTicks.push(date.slice(-2) + '/' + date.slice(4, 6)  + '/' + date.slice(0, 4));
            }
        }

        if (billing) {
            var animation = false;
            title_text = '(A) ' + title_text;
        } else {
            var animation = !$.jqplot.use_excanvas;
        }

        showPlot();
        var plotOt503 = $.jqplot(dstDivId, [energies, temperatures], {
            series: [
                { renderer: $.jqplot.BarRenderer,
                    rendererOptions: {
                        barWidth: 8
                    }
                },
                { yaxis: 'y2axis', color: '#2151a8' },
            ],
            title: {
                text: title_text,
                textAlign: 'center',
            },
            animate: animation,
            seriesDefaults:{
                renderer:$.jqplot.LineRenderer,
                rendererOptions: {
                    shadow: false,
                    color: '#9cc2ea',
                    highlightMouseDown: true,
                }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        angle: -30,
                        fontSize: cssOverride['jqplot-xaxis-tick']['font-size'],
                        showGridline: false
                    },
                    ticks: ticks
                },
                yaxis: {
                    label: yaxis_label,
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    autoscale: true,
                    labelOptions: {
                        fontSize: cssOverride['jqplot-yaxis-label']['font-size'],
                    },
                },
                y2axis: {
                    label: y2axis_label,
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontSize: cssOverride['jqplot-yaxis-label']['font-size'],
                    },
                    autoscale: true,
                    min: 0,
                    tickOptions: {
                        showGridline: false
                    }
                }
            },
            highlighter: {
                show: true,
                showMarker: true,
                tooltipLocation: 'n',
                tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
                  var xLabel = fullTicks[pointIndex];
                  var yValue = plot.series[0].data[pointIndex][1];
                  var y2Value = plot.series[1].data[pointIndex][1];
                  var toolTip = '<b>' + xLabel + '</b><br>';
                  toolTip += tooltip_energy + ': ' + yValue + ' kWh <br>' ;
                  toolTip += tooltip_temperature + ': ' + y2Value + ' ºC';
                  return toolTip;
                },
            },
            legend: {
                show: true,
                showLabels: true,
                labels: [yaxis_label, y2axis_label],
                renderer: $.jqplot.EnhancedLegendRenderer,
                location: 's',
                placement : "outside",
                marginTop : "30px",
                rendererOptions: {
                    numberRows: 1
                }
            },
            cursor: {
                show: false,
            },
            grid: {
                background: '#ffffff',
                borderWidth: 0.0,
                shadow: false,
            }
        });
        $(window).resize(function() {
            plotOt503.replot( { resetAxes: true } );
        });
    }

    loadChart(url, data, lang, ot503callback, billing);
};
