/*
 * online-tools.ot101
 * ~~~~~~~~~~~~~~
 *
 * :copyrigth: (c) 2014 by El Gas S.A. See AUTHORS for more details.
 * :licese: EUPL, see LICENSE for more details.
 *
 * :contributor: apsl.net.
 */

var ot201 = function(dstDivId, url, data, lang) {
    ot201Real(dstDivId, url, data, lang, false);
}

var bt201 = function(dstDivId, url, data, lang, billing) {
    ot201Real(dstDivId, url, data, lang, billing);
}

var ot201Real = function(dstDivId, url, data, lang, billing) {

    // callback for ajax call with plot data in json.
    var ot201callback = function(jsonData) {

        if (!('_items' in jsonData) || jsonData['_items'].length == 0) {
            var errorText = _t("This chart is currently not avaliable.", lang);
            showError(errorText);
            return false;
        }

        var data = jsonData['_items'][0];

        date = data['month'].toString();
        month = translations['monthes'][lang][parseInt(date.slice(4, 6))]
        year = parseInt(date.slice(0, 4));
        startYear = (year - 1).toString()
        endYear = year.toString()

        dateStart = month + ' ' + startYear
        dateEnd = month + ' ' + date.slice(0, 4);
        var title = _t('Comparison with previous year', lang);
        var subtitle = '<br><span class="ot-subtitle">' + dateStart + ' - ' + dateEnd + '</span>';
        var title_text = title + subtitle;
        var yaxis_label = _t('Power consumption (kWh)', lang);

        /* Data transfromation for jqplot */
        var ticks = [];
        var yValues = [];

        date = data['month'].toString();
        dateStart = translations['monthes'][lang][parseInt(date.slice(4, 6))]
        dateStart = dateStart + ' ' + date.slice(0, 4);

        date = (data['month'] - 100).toString();
        dateEnd = translations['monthes'][lang][parseInt(date.slice(4, 6))]
        dateEnd = dateEnd + ' ' + date.slice(0, 4);

        yValues.push(parseInt(Math.ceil(data['actualConsumption'])));
        ticks.push(dateStart);

        yValues.push(parseInt(Math.ceil(data['previousConsumption'])));
        ticks.push(dateEnd);

        if (billing) {
            var animation = false;
            title_text = '(B) ' + title_text;
        } else {
            var animation = !$.jqplot.use_excanvas;
        }

        showPlot();
        var plotOt201 = $.jqplot(dstDivId, [yValues], {
            title: {
                text: title_text,
                textAlign: 'center',
            },
            animate: animation,
            seriesColors: ['#2151a8', '#9cc2ea'],
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                rendererOptions: {
                    varyBarColor: true,
                    barWidth: 50,
                    barDirection: 'vertical',
                    shadow: false,
                },
                pointLabels: {show: true, edgeTolerance: -20},
            },
            axes: {
                xaxis: {
                    ticks: ticks,
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        angle: -30,
                        fontSize: cssOverride['jqplot-xaxis-tick']['font-size'],
                        showGridline: false,
                    },
                },
                yaxis: {
                    label: yaxis_label,
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontSize: cssOverride['jqplot-yaxis-label']['font-size'],
                    },
                    pad: 1.2,
                    min: 0
                },
            },
            highlighter: {
                show: false,
                showMarker: false,
                tooltipLocation: 'n',
                tooltipAxes: 'y',
                sizeAdjust: 7.5,
                tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
                  var xLabel = plot.axes.xaxis.ticks[pointIndex];
                  return '<b>' + xLabel + ':</b> ' + str + ' kWh';
                },
            },
            legend: {
                show: false,
            },
            cursor: {
                show: false
            },
            grid: {
                background: '#ffffff',
                borderWidth: 0.0,
                shadow: false,
            }
        });
        $(window).resize(function() {
            plotOt201.replot( { resetAxes: true } );
        });
    }

    loadChart(url, data, lang, ot201callback, billing);
};
