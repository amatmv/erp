/*
 * online-tools.ot101
 * ~~~~~~~~~~~~~~
 *
 * :copyrigth: (c) 2014 by El Gas S.A. See AUTHORS for more details.
 * :licese: EUPL, see LICENSE for more details.
 *
 * :contributor: apsl.net.
 */

var historic = function(dstDivId, data, lang) {

    // callback for ajax call with plot data in json.
    var historic_callback = function(jsonData) {

        /* Text translations & overrides */
        if ('text' in jsonData) {
            var text = jsonData['text'];
        } else {
            var title = '<b>' + _t('Historial de consumo', lang) + '</b>';
            var text = {
                'title-text': title,
                'yaxis-label': _t('Consumo (kWh)', lang),
            }
        }

        /* Data transfromation for jqplot */
        var ticks = jsonData['dates'];
        var yValues = jsonData['values'];

        showPlot();
        var plothistoric = $.jqplot(dstDivId, [yValues], {
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                rendererOptions: {
                    varyBarColor: false,
                    color: '#2151a8',
                    barWidth: 30,
                    barDirection: 'vertical',
                    shadow: false,
                },
                pointLabels: {
                    show: true,
                    edgeTolerance: -20,
                }
            },
            axes: {
                xaxis: {
                    ticks: ticks,
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        angle: -30,
                        fontSize: '10pt',
                        showGridline: false
                    },
                },
                yaxis: {
                    label: text['yaxis-label'],
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                },
            },
            highlighter: {
                show: true,
                showMarker: false,
                tooltipLocation: 'n',
                tooltipAxes: 'y',
                sizeAdjust: 7.5,
                tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
                  var xLabel = plot.axes.xaxis.ticks[pointIndex];
                  return '<b>' + xLabel + ':</b> ' + str + ' kWh';
                },
            },
            legend: {
                show: false,
            },
            cursor: {
                show: false
            },
            grid: {
                background: '#ffffff',
                borderWidth: 0.0,
                shadow: false,
            }
        });
        $(window).resize(function() {
            plothistoric.replot( { resetAxes: true } );
        });
    }

    historic_callback(data);
};
