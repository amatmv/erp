/*
 * online-tools.ot101
 * ~~~~~~~~~~~~~~
 *
 * :copyrigth: (c) 2014 by El Gas S.A. See AUTHORS for more details.
 * :licese: EUPL, see LICENSE for more details.
 *
 * :contributor: apsl.net.
 */

var ot101 = function(dstDivId, url, data, lang) {
    ot101Real(dstDivId, url, data, lang, false);
}

var bt101 = function(dstDivId, url, data, lang, billing) {
    ot101Real(dstDivId, url, data, lang, billing);
}

var ot101Real = function(dstDivId, url, data, lang, billing) {

    // callback for ajax call with plot data in json.
    var ot101callback = function(jsonData) {
        if (!('_items' in jsonData) || jsonData['_items'].length == 0) {
            var errorText = _t("This chart is currently not avaliable.", lang);
            showError(errorText);
            return false;
        }

        var data = jsonData['_items'][0];

        date = data['month'].toString();
        date = date.slice(0, 4) + '/' + date.slice(4, 6);
        var title = _t('Monthly consumption comparison', lang);
        var subtitle = '<br><span class="ot-subtitle">' + date + '</span>';
        title_text = title + subtitle;
        yaxis_label = _t('Power consumption (kWh)', lang);

        /* Data transfromation for jqplot */
        var ticks = [];
        var yValues = [];

        yValues.push(parseInt(Math.ceil(data['consumption'])));
        ticks.push(_t('Your home', lang));

        yValues.push(parseInt(Math.ceil(data['averageConsumption'])));
        ticks.push(_t('Similar homes', lang));

        yValues.push(parseInt(Math.ceil(data['averageEffConsumption'])));
        ticks.push(_t('Efficient homes', lang));

        if (billing) {
            var animation = false;
            title_text = '(D) ' + title_text;
        } else {
            var animation = !$.jqplot.use_excanvas;
        }

        showPlot();
        var plotOt101 = $.jqplot(dstDivId, [yValues], {
            title: {
                text: title_text,
            },
            animate: animation,
            seriesColors: ['#2151a8', '#9cc2ea', '#8bbc21'],
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                rendererOptions: {
                    varyBarColor: true,
                    barWidth: 50,
                    barDirection: 'vertical',
                    shadow: false,
                },
                pointLabels: {show: true, edgeTolerance: -20}
            },
            axes: {
                xaxis: {
                    ticks: ticks,
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        angle: -30,
                        fontSize: cssOverride['jqplot-xaxis-tick']['font-size'],
                        showGridline: false
                    },
                },
                yaxis: {
                    label: yaxis_label,
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    labelOptions: {
                        fontSize: cssOverride['jqplot-yaxis-label']['font-size'],
                    },
                    pad: 1.2,
                    min: 0,
                },
            },
            highlighter: {
                show: false,
                showMarker: false,
                tooltipLocation: 'n',
                tooltipAxes: 'y',
                sizeAdjust: 7.5,
                tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
                  var xLabel = plot.axes.xaxis.ticks[pointIndex];
                  return '<b>' + xLabel + ':</b> ' + str + ' kWh';
                },
            },
            legend: {
                show: false,
            },
            cursor: {
                show: false
            },
            grid: {
                background: '#ffffff',
                borderWidth: 0.0,
                shadow: false,
            }
        });
        $(window).resize(function() {
            plotOt101.replot( { resetAxes: true } );
        });
    }
    loadChart(url, data, lang, ot101callback, billing);
};
