/*
 * online-tools.ot103
 * ~~~~~~~~~~~~~~
 *
 * :copyrigth: (c) 2014 by El Gas S.A. See AUTHORS for more details.
 * :licese: EUPL, see LICENSE for more details.
 *
 * :contributor: apsl.net.
 */

var ot103 = function(dstDivId, url, data, lang) {
    ot103Real(dstDivId, url, data, lang, false);
}

var bt103 = function(dstDivId, url, data, lang, billing) {
    ot103Real(dstDivId, url, data, lang, billing);
}

var ot103Real = function(dstDivId, url, data, lang, billing) {

    // callback for ajax call with plot data in json.
    var ot103callback = function(jsonData) {
        if (!('_items' in jsonData) || jsonData['_items'].length == 0) {
            var errorText = _t("This chart is currently not avaliable.", lang);
            showError(errorText);
            return false;
        }

        var data = jsonData['_items'];

        date = data[0]['month'].toString();
        dateStart = translations['monthes'][lang][parseInt(date.slice(4, 6))] + ' ' + date.slice(0, 4);
        date = data[data.length-1]['month'].toString();
        dateEnd = translations['monthes'][lang][parseInt(date.slice(4, 6))] + ' ' + date.slice(0, 4);
        var title = _t("Yearly consumption comparison", lang);
        var subtitle = '<br><span class="ot-subtitle">' + dateStart + ' - ' + dateEnd + '</span>';
        var title_text = title + subtitle;
        var yaxis_label = _t('Power consumption (kWh)', lang);
        var tooltip_you = _t('Your home', lang);
        var tooltip_high = _t('Similar homes', lang);
        var tooltip_low = _t('Efficient homes', lang);

        /* Data transfromation for jqplot */
        var ticks = [];
        var energies = [];
        var high = [];
        var low = [];

        for (var i=0; i < data.length; i++) {
            energies.push(parseInt(Math.ceil(data[i]['consumption'])));
            high.push(parseInt(Math.ceil(data[i]['averageConsumption'])));
            low.push(parseInt(Math.ceil(data[i]['averageEffConsumption'])));
            if ('tick' in data[i]) {
                ticks.push(data[i][tick]);
            } else {
                date = data[i]['month'].toString();
                var monthNum = date.slice(-2);
                ticks.push(translations['monthes'][lang][parseInt(monthNum)] + ' ' + date.slice(0, 4));
            }
        }

        if (billing) {
            var animation = false;
            title_text = '(C) ' + title_text;
        } else {
            var animation = !$.jqplot.use_excanvas;
        }

        showPlot();
        var plotOt103 = $.jqplot(dstDivId, [energies, high, low], {
            seriesColors: [
                '#2151a8', '#9cc2ea', '#8bbc21'
            ],
            series: [
                { showMarker: true },
                { showMarker: false },
                { showMarker: false },
            ],
            title: {
                text: title_text,
                textAlign: 'center',
            },
            animate: animation,
            seriesDefaults:{
                renderer:$.jqplot.LineRenderer,
                rendererOptions: {
                    shadow: false,
                    color: '#2151a8',
                    highlightMouseDown: true
                },
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        angle: -30,
                        fontSize: cssOverride['jqplot-xaxis-tick']['font-size'],
                        showGridline: false
                    },
                    ticks: ticks,
                },
                yaxis: {
                    label: yaxis_label,
                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                    autoscale: true,
                    labelOptions: {
                        fontSize: cssOverride['jqplot-yaxis-label']['font-size'],
                    },

                },
            },
            fillBetween: {
                series1: 1,
                series2: 2,
                color: "rgba(156, 194, 234, 0.4)",
                baseSeries: 0,
                fill: true,
            },
            highlighter: {
                show: true,
                tooltipLocation: 'nw',
                tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
                    var xLabel = plot.axes.xaxis.ticks[pointIndex];
                    var yValue = plot.series[0].data[pointIndex][1];
                    var yHighValue = plot.series[1].data[pointIndex][1];
                    var yLowValue = plot.series[2].data[pointIndex][1];
                    var toolTip = '<b>' + xLabel + '</b><br>';
                    toolTip += tooltip_you + ': ' + yValue + ' kWh<br>' ;
                    toolTip += tooltip_high + ': ' + yHighValue + ' kWh<br>';
                    toolTip += tooltip_low + ': ' + yLowValue + ' kWh';
                    return toolTip
                },
            },
            legend: {
                show: true,
                showLabels: true,
                labels: [tooltip_you, tooltip_high, tooltip_low],
                renderer: $.jqplot.EnhancedLegendRenderer,
                location: 's' ,
                placement : "outside",
                marginTop : "65px",
                rendererOptions: {
                    numberRows: 1
                }
            },
            cursor: {
                show: false,
            },
            grid: {
                background: '#ffffff',
                borderWidth: 0.0,
                shadow: false,
            }
        });
        $(window).resize(function() {
            plotOt103.replot( { resetAxes: true } );
        });
    }

    loadChart(url, data, lang, ot103callback, billing);
};
