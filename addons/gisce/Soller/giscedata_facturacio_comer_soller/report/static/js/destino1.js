/*
 * online-tools.ot101
 * ~~~~~~~~~~~~~~
 *
 * :copyrigth: (c) 2014 by El Gas S.A. See AUTHORS for more details.
 * :licese: EUPL, see LICENSE for more details.
 *
 * :contributor: apsl.net.
 */

var destino1 = function(dstDivId, data, lang) {
    
    /* Data transfromation for jqplot */
    var yValues = data['values'];
    var seriesNames = data['names'];
    if ('colors' in data) {
        var colors = data['colors'];
    }
    
    values = [];
    pieLabels = [];
    for (var i = 0; i < yValues.length; i++) {
        values.push([seriesNames[i], yValues[i]]);
        label = '<center>' + seriesNames[i];
        label =  label + '<br>' + yValues[i] + '</center>';
        pieLabels.push(label);
    }

    showPlot();
    var plotPie = $.jqplot(dstDivId, [values], {
        seriesDefaults:{
            renderer: $.jqplot.PieRenderer,
            rendererOptions: {
                shadow: false,
                showDataLabels: true,
                dataLabels: 'label',
                sliceMargin: 2,
                padding: 0
            }
        },
        seriesColors: data['colors'],
        legend: {
            show: false
        },
        grid: {
            background: '#ffffff',
            borderWidth: 0.0,
            shadow: false,
        }
    });
    $(window).resize(function() {
        plotPie.replot( );
    });

};
