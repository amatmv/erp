# -*- coding: utf-8 -*-
{
    "name": "GISCE Facturació Comercialitzadora (Sóller)",
    "description": """Mòdul de personalització de la facturació de
    comercialitzadora per Sóller.
    * Validar la combinació de forma de pago, grup cobratori y compte bancari
    * Afegir el camp estat_impagat al model de factura
    * Afegir el camp grup cobratoria al model de factura
    * Report personalitzat de factura i rebut
    * Report de duplicat de factura
    * Reports dels avisos d'impagats
    * Reports dels avisos de retirada de comptador
    * Llistat de factures personalitzat
    * Llistat agrupat per forma de pago""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "Facturació Comercialitzadora",
    "depends":[
        "base",
        "giscedata_facturacio",
        "giscedata_facturacio_comer",
        "giscedata_facturacio_facturae",
        "c2c_webkit_report",
        "phantom_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_comer_report.xml",
        "giscedata_facturacio_comer_view.xml",
        "giscedata_polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
