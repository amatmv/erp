# -*- coding: utf-8 -*-
{
    "name": "Phantom reports",
    "description": """Allow generate different kind of reports using the same api call or the same report name.
    * Historize reports through time
    * Allow changing report technology through time
    * Parser instance must return selected report name to render
    """,
    "version": "0-dev",
    "author": "Electrica Sollerense SAU",
    "category": "General",
    "depends":[
        "base",
        "oorq"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
