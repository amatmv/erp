# -*- coding: utf-8 -*-

from osv import osv, fields
import report
import netsvc
import pooler

from PyPDF2 import PdfFileMerger, PdfFileReader, PdfFileWriter
import StringIO
import base64
import zipfile

from oorq.decorators import job


class PhantomReportBackground(osv.osv):
    _name = 'phantom.report.background'
    _auto = False

    @job(queue='reports', timeout=3600)
    def background_report(self, cr, uid, ids, filename, report_name,
                          source_model, attach_model, attach_id,
                          compress=False, context=None):
        user_obj = self.pool.get('res.users')
        attachment_obj = self.pool.get('ir.attachment')

        if not context:
            context = {}

        search_params = [('res_model', '=', attach_model),
                         ('res_id', '=', attach_id),
                         ('name', '=', filename)]
        # Skipe attachments that already exist
        if attachment_obj.search(cr, uid, search_params, context=context):
            return True

        data = {'model': source_model}
        ctx = context.copy()
        if 'lang' not in ctx or ctx['lang'] == 'False':
            ctx['lang'] = user_obj.read(cr, uid, uid, ['context_lang'],
                                        context)['context_lang']
        service = netsvc.LocalService("report.%s" % report_name)
        if 'control_pages' in ctx and ctx['control_pages'] > 0:
            control_pages = ctx['control_pages']
        else:
            control_pages = 0
        special = False
        if control_pages > 0:
            # Create reports one by one to control the number of pages
            merger = PdfFileMerger()
            merge_file = StringIO.StringIO()
            special_merger = PdfFileMerger()
            special_merge_file = StringIO.StringIO()
            for id in ids:
                result, format = service.create(cr, uid, [id], data, ctx)
                temp_file = StringIO.StringIO(result)
                pdf_reader = PdfFileReader(temp_file)
                if pdf_reader.numPages != control_pages:
                    pdf_writer = PdfFileWriter()
                    for page in pdf_reader.pages:
                        pdf_writer.addPage(page)
                    blank_pages = pdf_reader.numPages % control_pages
                    for i in range(blank_pages):
                        pdf_writer.addBlankPage()
                    pdf_writer_file = StringIO.StringIO()
                    pdf_writer.write(pdf_writer_file)
                    special_merger.append(pdf_writer_file)
                    special = True
                else:
                    merger.append(temp_file)
            # Write all documents where control_pages match
            merger.write(merge_file)
            merge_file.seek(0)
            result = merge_file.read()

            # Write all documents where control_pages do not match
            if special:
                special_merger.write(special_merge_file)
                special_merge_file.seek(0)
                special_result = special_merge_file.read()
                name_split = filename.split('.')
                special_filename = '%s_special.%s' % (name_split[0],
                                                      name_split[1])
            # Close mergers
            merger.close()
            merge_file.close()
            special_merger.close()
            special_merge_file.close()
        else:
            result, format = service.create(cr, uid, ids, data, ctx)

        if compress:
            pdfname = filename
            filename += '.zip'
            fileHandle = StringIO.StringIO()
            comp = zipfile.ZIP_DEFLATED
            zf = zipfile.ZipFile(fileHandle, mode='w', compression=comp)
            zf.writestr(pdfname, result)
            zf.close()
            result = fileHandle.getvalue()
            if special:
                special_pdfname = special_filename
                special_filename += '.zip'
                fileHandle = StringIO.StringIO()
                comp = zipfile.ZIP_DEFLATED
                zf = zipfile.ZipFile(fileHandle, mode='w', compression=comp)
                zf.writestr(special_pdfname, special_result)
                zf.close()
                special_result = fileHandle.getvalue()

        vals = {
            'name': filename,
            'datas': base64.b64encode(result),
            'datas_fname': filename,
            'res_model': attach_model,
            'res_id': attach_id
        }
        attachment_obj.create(cr, uid, vals)
        if special:
            vals = {
                'name': special_filename,
                'datas': base64.b64encode(special_result),
                'datas_fname': special_filename,
                'res_model': attach_model,
                'res_id': attach_id
            }
            attachment_obj.create(cr, uid, vals)
        return True

PhantomReportBackground()


class PhantomReportXML(osv.osv):

    _name = 'ir.actions.report.xml'
    _inherit = 'ir.actions.report.xml'

    def __init__(self, pool, cr):
        """Nasty hack for OpenOffice report compatibility due
        to the inexistance of selection fields inheritance mechanism"""
        super(PhantomReportXML, self).__init__(pool, cr)
        if (('phantom', 'Phantom') not in
            self._columns['report_type'].selection):
            (self._columns['report_type'].selection.
             append(('phantom', 'Phantom')))

PhantomReportXML()


class phantom_report(report.interface.report_int):

    def __init__(self, name, parser=None):
        if name in netsvc.SERVICES:
            del netsvc.SERVICES[name]
        super(phantom_report, self).__init__(name)
        self.parser = parser
        self.exportMethod(self.create_background)

    def create(self, cr, uid, ids, data, context=None):
        '''Create return pdf from different kind of reports'''

        report_names = self.parser(cr, uid, ids, data, context=context)

        if len(report_names.keys()) == 1:
            report_name = report_names.keys()[0]
            service = netsvc.LocalService('report.%s' % report_name)
            return service.create(cr, uid, ids, data,
                                  context=context)
        merger = PdfFileMerger()
        merge_file = StringIO.StringIO()
        for report_name, model_ids in report_names.iteritems():
            service = netsvc.LocalService('report.%s' % report_name)
            result, format = service.create(cr, uid, model_ids, data,
                                            context=context)
            #Write result to a temporary file
            temp_file = StringIO.StringIO(result)
            merger.append(temp_file)
        merger.write(merge_file)
        merge_file.seek(0)
        result_pdf = merge_file.read()
        merger.close()
        merge_file.close()

        return (result_pdf, 'pdf')

    def create_background(self, cr, uid, ids, filename, report_name,
                          source_model, attach_model, attach_id,
                          compress=False, context=None):
        """ Create the report in background
        and attach it to the attac_id object """

        pool = pooler.get_pool(cr.dbname)
        phantom_obj = pool.get('phantom.report.background')
        phantom_obj.background_report(cr, uid, ids, filename, report_name,
                                      source_model, attach_model,
                                      attach_id, compress, context)
        return True


def register_report(name):
    name = 'report.%s' % name
    if netsvc.service_exist(name):
        if isinstance(netsvc.SERVICES[name], phantom_report):
            return
        del netsvc.SERVICES[name]
    phantom_report(name)

old_register_all = report.interface.register_all


def new_register_all(db):
    value = old_register_all(db)
    cursor = db.cursor()
    cursor.execute("SELECT * FROM ir_act_report_xml")
    records = cursor.dictfetchall()
    cursor.close()
    for record in records:
        if record['report_type'] in ('phantom',):
            register_report(record['report_name'])
    return value

report.interface.register_all = new_register_all
