# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

class RegistrePolissaGiscedataComer(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def create(self, cr, uid, vals, context={}):
        
        res = super(RegistrePolissaGiscedataComer, self).create(cr, 
                                                    uid, vals, context=context)
        #Actualitzem el registre
        polissa = self.browse(cr, uid, res)
        polissa.update_registre(polissa.name, mode='add')
        return res

    def unlink(self, cr, uid, ids, context={}):
        '''Ampliem la funcionalitat del unlink 
        per actualitzar el registre de numeros de contracte'''

        for polissa in self.browse(cr, uid, ids, context=context):
            polissa.update_registre(polissa.name, mode='del')
        
        return super(RegistrePolissaGiscedataComer, self).unlink(cr, 
                                                    uid, ids, context=context)

RegistrePolissaGiscedataComer()

class RegistrePolissaComer(osv.osv):

    _name = 'registre.polissa'
    _inherit = 'registre.polissa'

    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context={}, toolbar=False):
        '''sobreescrivim el metode per canviar el label del camp polissa_id'''

        res = super(RegistrePolissaComer, self).fields_view_get(cr, uid, view_id, 
                                           view_type, context=context, toolbar=toolbar)

        if 'fields' in res.keys():
            fields = res.get('fields',{}).keys()
            if 'polissa_id' in fields:
                res['fields']['polissa_id']['string'] = 'Contracte'
            
        return res

RegistrePolissaComer()
