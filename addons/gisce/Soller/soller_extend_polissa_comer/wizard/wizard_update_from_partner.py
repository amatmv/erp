# -*- coding: utf-8 -*-

from osv import osv, fields
import pooler


class WizardUpdatePolissaFromPartner(osv.osv):

    _name = 'wizard.update.polissa.from.partner'

    def search_address(self, cursor, uid, partner, address, context=None):
        '''Search for address in partner_id'''

        address_obj = self.pool.get('res.partner.address')

        search_params = [('partner_id', '=', partner.id),
                         ('street', '=', address.street)]

        address_ids = address_obj.search(cursor, uid, search_params)
        if address_ids:
            res = address_ids[0]
        else:
            address_vals = {'partner_id': partner.id,
                            'name': partner.name}
            res = address_obj.copy(cursor, uid, address.id, address_vals)
        return res

    def action_move_partner(self, cursor, uid, ids, context=None):

        partner_obj = self.pool.get('res.partner')
        address_obj = self.pool.get('res.partner.address')
        bank_obj = self.pool.get('res.partner.bank')
        polissa_obj = self.pool.get('giscedata.polissa')
        wizard_upd_obj = self.pool.get('wizard.update.polissa.contacts')

        wizard = self.browse(cursor, uid, ids[0])
        db = pooler.get_db_only(cursor.dbname)

        new_partner = wizard.new_partner_id        

        if not context:
            context = {}
        old_partner_id = wizard.partner_id.id

        #Search for all associated polissa to partner
        search_params = ['|', ('titular', '=', old_partner_id),
                         '|', ('pagador', '=', old_partner_id),
                              ('altre_p', '=', old_partner_id)]

        polissa_ids = polissa_obj.search(cursor, uid, search_params)
        notes = ''
        # Update each polissa
        for polissa_id in polissa_ids:
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            try:
                ctx = {'active_id': polissa_id}
                wizard_upd_id = wizard_upd_obj.create(cursor, uid, {}, context=ctx)
                wizard_upd = wizard_upd_obj.browse(cursor, uid, wizard_upd_id)
                vals = {}
                new_address_id = False
                if wizard_upd.titular.id == old_partner_id:
                    #Search for the address in the new partner
                    new_address_tit_id = self.search_address(cursor, uid, new_partner,
                                                             wizard_upd.direccio_titular)
                    vals.update({'titular': new_partner.id,
                                 'direccio_titular': new_address_tit_id})
                if wizard_upd.pagador.id == old_partner_id:
                    #Copy the address if necessary
                    new_address_pag_id = False
                    if wizard_upd.direccio_pagament.id == wizard_upd.direccio_titular.id:
                        new_address_pag_id = new_address_tit_id
                    else:
                        new_address_pag_id = self.search_address(cursor, uid, new_partner,
                                                             wizard_upd.direccio_pagament)
                    #Search for the bank in the new partner
                    #If not found we have to create it
                    if wizard_upd.bank:
                        search_params = [('partner_id', '=', new_partner.id)]
                        if wizard_upd.bank.iban:
                            search_params.extend([('iban', '=', wizard_upd.bank.iban)])
                            bank_state = 'iban'
                        else:
                            search_params.extend([('acc_number', '=', wizard_upd.bank.acc_number)])
                            bank_state = 'bank'
                        bank_ids = bank_obj.search(cursor, uid, search_params)
                        if bank_ids:
                            new_bank_id = bank_ids[0]
                        else:
                            bank_vals = {'partner_id': new_partner.id,
                                         'bank': wizard_upd.bank.bank.id,
                                         'acc_number': wizard_upd.bank.acc_number,
                                         'iban': wizard_upd.bank.iban,
                                         'acc_country_id': wizard_upd.bank.acc_country_id.id,
                                         'state': bank_state}
                            new_bank_id = bank_obj.create(cursor, uid, bank_vals)
                    else:
                        new_bank_id = False
                    vals.update({'pagador': new_partner.id,
                                 'direccio_pagament': new_address_pag_id,
                                 'bank': new_bank_id})
                if wizard_upd.altre_p.id == old_partner_id:
                    new_address_not_id = False
                    #Copy the address if necessary
                    if wizard_upd.direccio_notificacio.id == wizard_upd.direccio_titular.id:
                        new_address_not_id = new_address_tit_id
                    elif wizard_upd.direccio_notificacio.id == wizard_upd.direccio_pagament.id:
                        new_address_not_id = new_address_pag_id
                    else:
                        new_address_not_id = self.search_address(cursor, uid, new_partner,
                                                             wizard_upd.direccio_notificacio)              
                    vals.update({'altre_p': new_partner.id,
                                 'direccio_notificacio': new_address_not_id})
                elif wizard_upd.altre_p.id != wizard_upd.direccio_notificacio.partner_id.id:
                        vals.update({'altre_p': wizard_upd.direccio_notificacio.partner_id.id})
                wizard_upd.write(vals)
                wizard_upd.action_change(context=ctx)
                notes += u"Póliza %s actualizada correctamente\n" % polissa.name
            except Exception, e:
                notes += u'Error actualizando la póliza %s' % polissa.name                

        wizard.write({'notes': notes,
                      'state': 'end'})
        return True
            

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Client', required=True),
        'new_partner_id': fields.many2one('res.partner', 'Client nou',
                                          required=True),
        'state': fields.selection([('init', 'Init'),
                                  ('end', 'End'),
                                 ], 'State', select=True, readonly=True),
        'notes': fields.text('Notes', readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardUpdatePolissaFromPartner()
