# -*- coding: utf-8 -*-
{
    "name": "Soller Extend Polissa",
    "description": """
Funcionalitats extra per polissa de comercialitzadora
    * Redefinir la funcio copy per pòlissa de comercialitzadora
    * Afegit el camp abonat
    * Canviar el nom dels camps name i ref_dist
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_polissa_comer",
        "soller_extend_polissa",
        "giscedata_comerdist_polissa_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_extend_polissa_comer_view.xml",
        "soller_extend_polissa_comer_report.xml",
        "wizard/wizard_update_from_partner_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
