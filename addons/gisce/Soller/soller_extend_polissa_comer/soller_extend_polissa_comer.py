# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

class GiscedataPolissaComerSollerExtend(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy(self, cr, uid, id, default=None, context=None):
        """Canviem la funcionalitat del copy() per adaptar-ho a la pòlissa de Soller.
           Camps especifics de comercialitzadora"""

        default.update({'distribuidora': False,
                        'ref_dist': False,
                       })
        
        res_id = super(GiscedataPolissaComerSollerExtend, self).copy(cr, uid, id, default,
                                                                context)
        return res_id

    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context={}, toolbar=False):
        '''sobreescrivim el metode per canviar el label del camps name i codi'''

        res = super(GiscedataPolissaComerSollerExtend, self).fields_view_get(cr, uid, view_id, 
                                                    view_type, context=context, toolbar=toolbar)

        if 'fields' in res.keys():
            fields = res.get('fields',{}).keys()
            if 'name' in fields:
                res['fields']['name']['string'] = 'Contracte'
            if 'ref_dist' in fields:
                res['fields']['ref_dist']['string'] = 'Pòlissa Dist.'                                

        return res

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
        '''sobreescrivim el metode per cercar per codi amb el mateix criteri que per name
        en el que no fem ilike, sino ='''

        args2 = []
        for arg in args:
            if len(arg) != 3:
                args2.append(arg)
                continue
            field, operator, filter = arg
            if field == 'abonat':
                operator = '='
            args2.append([field, operator, filter])
        
        res = super(GiscedataPolissaComerSollerExtend, self).search(cr, uid, args2, offset=offset,
                                                     limit=limit, order=order, context=context, count=count)

        return res

    _columns = {
        'abonat': fields.char('Abonat', size=60, readonly=True,
                            states={'esborrany':[('readonly', False)],
                                    'validar': [('readonly', False)]}),
    }

    

GiscedataPolissaComerSollerExtend()

