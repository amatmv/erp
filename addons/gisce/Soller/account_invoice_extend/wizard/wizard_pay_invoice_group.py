# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from tools.translate import _
import time

class WizardPayInvoiceGroup(osv.osv_memory):
    
    _name = 'wizard.pay.invoice.group'

    def default_get(self, cr, uid, fields, context={}):

        res = super(WizardPayInvoiceGroup, self).default_get(cr, uid, fields, context)

        if 'period_id' in fields and 'period_id' not in res:
            res['period_id'] = self._get_period(cr, uid, res.get('date', None), context)

        return res

    def _get_name(self, cr, uid, context={}):
        
        return _('Cash invoice payment')

    def _get_period(self, cr, uid, date=None, context={}):

        period_obj = self.pool.get('account.period')

        return period_obj.find(cr, uid, date, context)[0]

    def _get_amount(self, cr, uid, context={}):

        invoice_obj = self.pool.get('account.invoice')
        invoice_ids = context.get('active_ids',[])

        total_amount = 0
        for invoice in invoice_obj.browse(cr, uid, invoice_ids):
            total_amount += invoice.residual

        return total_amount

    def _get_quantity(self, cr, uid, context={}):

        return len(context.get('active_ids',[]))

    def onchange_date(self, cr, uid, ids, date, context={}):

        res = {'value': {}, 'domain': {}, 'warning': {}}

        period = self._get_period(cr, uid, date, context)
        res['value'].update({'period_id': period})

        return res

    def action_pay_invoice_group(self, cr, uid, ids, context={}):

        wizard = self.browse(cr, uid, ids[0])
        invoice_obj = self.pool.get('account.invoice')

        invoice_ids = context.get('active_ids',[])
        context['date_p'] = wizard.date

        for invoice in invoice_obj.browse(cr, uid, invoice_ids):
            invoice.pay_and_reconcile(invoice.residual, 
                                      wizard.journal_id.default_credit_account_id.id,
                                      wizard.period_id.id, 
                                      wizard.journal_id.id,
                                      False,
                                      False,
                                      False, 
                                      context, invoice.number)

        wizard.write({'state': 'end'})
        return True
    
    _columns = {
        'name':fields.char('Name', size=128, 
                            required=True, readonly=False),
        'date': fields.date('Payment date', required=True),
        'journal_id':fields.many2one('account.journal', 'Journal', required=True,
                                     domain=[('type','=','cash')],),
        'period_id':fields.many2one('account.period', 'Period', required=True),
        'amount': fields.float('Quantity', digits=(16, 2), readonly=True),
        'quantity': fields.integer('Invoices number', readonly=True),
        'state':fields.selection([('init','Init'),
                                  ('end','End'),],
                                  'State', select=True, readonly=True),   
        
    }

    _defaults = {
        'date': lambda *a: time.strftime('%Y-%m-%d'),
        'name': _get_name,
        'amount': _get_amount,
        'quantity': _get_quantity,
        'state': lambda *a: 'init',
        
    }
WizardPayInvoiceGroup()
