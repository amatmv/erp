# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
import netsvc


class AccountInvoiceUndoPayment(osv.osv):
    '''
    Extend account invoice for undoing payments
    '''
    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def undo_payment(self, cr, uid, ids, context=None):
        """undo payments"""
        if not context:
            context = {}
        wf_service = netsvc.LocalService('workflow')
        move_obj = self.pool.get('account.move')
        reconcile_obj = self.pool.get('account.move.reconcile')

        for invoice in self.browse(cr, uid, ids):
            #Get payment moves
            move_ids = map(lambda x: x.move_id.id, invoice.payment_ids)
            reconcile_ids = []
            for payment in invoice.payment_ids:
                if payment.reconcile_id:
                    reconcile_ids.append(payment.reconcile_id.id)
                if payment.reconcile_partial_id:
                    reconcile_ids.append(payment.reconcile_partial_id.id)
            #Undo reconcile
            reconcile_obj.unlink(cr, uid, reconcile_ids, context)
            #Unlink payment moves
            move_obj.unlink(cr, uid, move_ids, context)
            #Re-Open the invoice
            wf_service.trg_validate(uid, 'account.invoice', invoice.id,
                                    'open_test', cr)
        return True

AccountInvoiceUndoPayment()
