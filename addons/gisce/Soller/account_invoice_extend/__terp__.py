# -*- coding: utf-8 -*-
{
    "name": "Account Invoice Extension",
    "description": """
Account invoice funcionalities extended:
    * Wizard for undoing payments
    * Wizard for paying invoices in groups
    * Invoice list report
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "account",
        "account_payment"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/account_invoice_security.xml",
        "wizard/wizard_undo_payment_view.xml",
        "wizard/wizard_pay_invoice_group_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
