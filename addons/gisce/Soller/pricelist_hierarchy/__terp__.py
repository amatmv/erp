# -*- coding: utf-8 -*-
{
    "name": "Pricelist hierarchy",
    "description": """
* Define a tree structure for pricelist""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "product"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "pricelist_hierarchy_view.xml"
    ],
    "active": False,
    "installable": True
}
