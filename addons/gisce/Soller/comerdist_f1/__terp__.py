# -*- coding: utf-8 -*-
{
    "name": "F1 export from distri",
    "description": """ Export F1 xml from distri to comer """,
    "version": "0-dev",
    "author": "Toni Llado",
    "category": "Facturació Distribuidora",
    "depends":[
        "giscedata_facturacio_distri",
        "giscedata_comerdist_facturacio_dist"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_export_f1_view.xml",
        "giscedata_facturacio_view.xml",
        "comerdist_f1_data.xml",
        "wizard/wizard_not_exported_f1_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
