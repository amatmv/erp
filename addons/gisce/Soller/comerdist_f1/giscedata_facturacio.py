# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
from addons import get_module_resource

class GiscedataFacturacioF1(osv.osv):
    _inherit = 'giscedata.facturacio.lot'

    def get_export_f1(self, cursor, uid, ids, context=None):
        '''
            Action export f1 to comer
        '''
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        config_obj = self.pool.get('giscedata.comerdist.config')

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        config_ids = config_obj.search(cursor, uid, [])
        partner_ids = [x['partner_id'][0] for x in
                       config_obj.read(cursor, uid, config_ids)]
        search_params = [('lot_facturacio', '=', ids),
                         ('partner_id', 'in', partner_ids),
                         ('state', 'in', ('open', 'paid')),
                         ('rectificative_type', '=', 'N')]

        return invoice_obj.search(cursor, uid, search_params)

    def _get_lot_from_clot(self, cursor, uid, ids, context=None):
        """Returns lot associated to clots in ids
        """
        clot_obj = self.pool.get('giscedata.facturacio.contracte_lot')

        lot_values = clot_obj.read(cursor, uid, ids, ['lot_id'])
        return list(set([x['lot_id'][0] for x in lot_values]))

    def _get_lot_from_register(self, cursor, uid, ids, context=None):
        """Returns lot associated to clots in ids
        """
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        register_obj = self.pool.get('comerdist.f1.register')


        register_vals = register_obj.read(cursor, uid, ids, ['invoice_id'])
        invoice_ids = [x['invoice_id'][0] for x in register_vals]
        invoice_vals = invoice_obj.read(cursor, uid, invoice_ids,
                                        ['lot_facturacio'])
        return list(set([x['lot_facturacio'][0] for x in invoice_vals
                         if x['lot_facturacio']]))


    def _ff_n_f1_invoice(self, cursor, uid, ids, field_name, arg,
                                  context=None):
        config_obj = self.pool.get('giscedata.comerdist.config')
        res = {}

        config_ids = config_obj.search(cursor, uid, [])
        partner_ids = [x['partner_id'][0] for x in
                       config_obj.read(cursor, uid, config_ids)]

        sql_file = get_module_resource(
            'comerdist_f1', 'sql', 'invoices_to_export.sql'
        )
        sql = open(sql_file, 'r').read()

        cursor.execute(sql, (tuple(partner_ids),))

        count_list = {}
        for row in cursor.fetchall():
            count_list.update({row[0]: row[1]})

        for lot_id in ids:
            if lot_id in count_list:
                res[lot_id] = count_list[lot_id]

        return res

    def _ff_n_f1_invoice_exported(self, cursor, uid, ids, field_name, arg,
                                  context=None):
        res = {}

        sql_file = get_module_resource(
            'comerdist_f1', 'sql', 'exported_invoices.sql'
        )
        sql = open(sql_file, 'r').read()

        cursor.execute(sql, ())

        count_list = {}
        for row in cursor.fetchall():
            count_list.update({row[0]: row[1]})

        for lot_id in ids:
            if lot_id in count_list:
                res[lot_id] = count_list[lot_id]

        return res

    _store_invoices = {'giscedata.facturacio.lot':
                           (lambda self, cr, uid, ids, c=None: ids, [], 10),
                       'giscedata.facturacio.contracte_lot':
                           (_get_lot_from_clot, ['state'], 10),
                       'comerdist.f1.register':
                           (_get_lot_from_register, ['invoice_id'], 10)
                       }

    _columns = {
        'n_f1_invoice': fields.function(_ff_n_f1_invoice, method=True,
                                        string='F1 invoice number',
                                        type='integer',
                                        store=False),
        'n_f1_invoice_exported': fields.function(_ff_n_f1_invoice_exported,
                                                 method=True,
                                            string='F1 exported invoice number',
                                                 type='integer',
                                                 store=False),
    }
GiscedataFacturacioF1()