# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
from giscedata_comerdist import OOOPPool
import base64
from oorq.decorators import job
from tools import config
import traceback

class ComerdistF1(osv.osv):

    _name = 'comerdist.f1'

    _auto = False

    @job(queue='comerdist_f1')
    def export_f1_to_comer(self, cursor, uid, id, force_export, context=None):
        '''
            Export f1 invoice in xml format to comer
        '''
        register_obj = self.pool.get('comerdist.f1.register')
        mail_obj = self.pool.get('poweremail.mailbox')
        acc_obj = self.pool.get('poweremail.core_accounts')
        config_obj = self.pool.get('res.config')

        if not context:
            context = {}

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura = factura_obj.browse(cursor, uid, id)

        if factura.must_be_synched(context):
            try:
                # Get f1 from distri
                fname, xml = factura_obj.export_new_f1(cursor, uid, [id], context)

                # Import to comer
                config_id = factura.get_config().id
                proxy = OOOPPool.get_ooop(cursor, uid, config_id)

                # Check if invoice exists
                search_params = [('invoice_id', '=', factura.id)]
                register_ids = register_obj.search(cursor, uid, search_params)

                if register_ids and not force_export:
                    return False

                wizard_proxy = proxy.WizardSwitchingImport
                wizard_proxy.import_f1(base64.encodestring(xml))

                if not register_ids:
                    vals = {'invoice_id': factura.id}
                    register_obj.create(cursor, uid, vals)

            except Exception, e:
                # Send email if there are error
                email_from = config.get('email_from', '')
                search_params = [('email_id', '=', email_from)]
                acc_id = acc_obj.search(cursor, uid, search_params)[0]
                subject = 'Error al exportar factura f1'
                body = ('Error al exportar factura f1 {} desde distribuidora '
                        '\n{}').format(factura.number, traceback.format_exc())
                vals = {
                    'pem_from': email_from,
                    'pem_to': config_obj.get(cursor, uid, 'comerdist_email_to'),
                    'pem_subject': subject,
                    'pem_body_text': body,
                    'pem_account_id': acc_id,
                }
                mail_id = mail_obj.create(cursor, uid, vals, context)
                mail_obj.send_this_mail(cursor, uid, [mail_id], context)

        return True

ComerdistF1()


class ComerdistF1Register(osv.osv):

    _name = 'comerdist.f1.register'

    _rec_name = 'invoice_id'

    _columns = {
        'invoice_id': fields.many2one('giscedata.facturacio.factura', 'Invoice',
                                      readonly=True),
        'create_date': fields.datetime('Create date'),
    }

ComerdistF1Register()