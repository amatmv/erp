select f.lot_facturacio, count(f.id) from giscedata_facturacio_factura f
inner join account_invoice i on i.id = f.invoice_id
inner join comerdist_f1_register r on f.id = r.invoice_id
where i.rectificative_type = 'N'
and i.state in ('open', 'paid')
group by f.lot_facturacio