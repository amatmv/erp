select f.id from giscedata_facturacio_lot l
inner join giscedata_facturacio_factura f on f.lot_facturacio = l.id
inner join account_invoice i on i.id = f.invoice_id
where l.id = %s
and i.partner_id in %s
and i.rectificative_type = 'N'
and i.state in ('open', 'paid')
and f.id not in (select invoice_id from comerdist_f1_register)