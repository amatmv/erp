select f.lot_facturacio, count(f.id) from giscedata_facturacio_factura f
inner join account_invoice i on i.id = f.invoice_id
where i.partner_id in %s
and i.rectificative_type = 'N'
and i.state in ('open', 'paid') group by f.lot_facturacio;