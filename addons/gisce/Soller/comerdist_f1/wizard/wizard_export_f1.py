# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardExportF1(osv.osv_memory):
    _name = 'wizard.export.f1'

    def default_summary(self, cursor, uid, context=None):
        '''
            Init the summary text.
        '''
        lot_obj = self.pool.get('giscedata.facturacio.lot')
        if not context:
            context = {}

        if 'from_lot' in context and context['from_lot']:
            invoice_ids = lot_obj.get_export_f1(cursor, uid,
                                                context['from_lot'])
        else:
            invoice_ids = context.get('active_ids', [])

        msg = 'Invoices to export: {}'.format(len(invoice_ids))
        return msg

    def action_export_f1(self, cursor, uid, ids, context=None):
        comerdist_obj = self.pool.get('comerdist.f1')
        lot_obj = self.pool.get('giscedata.facturacio.lot')

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])

        if 'from_lot' in context and context['from_lot']:
            invoice_ids = lot_obj.get_export_f1(cursor, uid,
                                                context['from_lot'])
        else:
            invoice_ids = context.get('active_ids', [])

        for invoice_id in invoice_ids:
            comerdist_obj.export_f1_to_comer(cursor, uid, invoice_id,
                                             wizard.force_export, context)

        return {}

    _columns = {
        'summary_text': fields.text('Summary', readonly=True),
        'force_export': fields.boolean('Force export')
    }

    _defaults = {
        'summary_text': default_summary,
        'force_export': lambda *a: False
    }

WizardExportF1()
