# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from addons import get_module_resource


class WizardNewNumberAire(osv.osv_memory):
    """
    Wizard for create a new number request in Aire
    """

    _name = "wizard.not.exported.f1"

    def action_not_exported_f1(self, cursor, uid, ids, context=None):
        '''
            Return list of not exported F1 invoices
        '''
        config_obj = self.pool.get('giscedata.comerdist.config')

        if not context:
            context = {}

        lot_ids = context.get('active_ids', [])

        config_ids = config_obj.search(cursor, uid, [])
        partner_ids = [x['partner_id'][0] for x in
                       config_obj.read(cursor, uid, config_ids)]

        sql_file = get_module_resource(
            'comerdist_f1', 'sql', 'not_exported_invoices_list.sql'
        )
        sql = open(sql_file, 'r').read()
        cursor.execute(sql, (lot_ids[0], tuple(partner_ids),))

        invoice_ids = [x[0] for x in cursor.fetchall()]

        vals_view = {
            'name': _('Not exported F1 invoices'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'limit': len(invoice_ids),
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', {})]".format(invoice_ids),
        }
        return vals_view

    _columns = {
        'info': fields.text('Information', readonly=True),
    }

    _defaults = {
        'info': lambda *a: _('We are going to show not exported F1 invoices'),
    }

WizardNewNumberAire()