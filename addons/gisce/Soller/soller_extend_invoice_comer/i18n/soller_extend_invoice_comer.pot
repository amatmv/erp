# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* soller_extend_invoice_comer
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2012-03-28 17:05:03+0000\n"
"PO-Revision-Date: 2012-03-28 17:05:03+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Referencia"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Fecha de emisión"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Fecha_vencimiento-date_due"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Direccion_factura-address_invoice_id/Calle-street"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Eléctrica Sollerense, S.A.U."
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Fecha_factura-date_invoice"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Direccion_factura-address_invoice_id/Nombre-name"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Factura"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Nombre-name}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Recibo"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Cuenta_bancaria-partner_bank/Banco-bank/Nombre-name"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Fecha_creacion-date_created}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Numero_factura-number"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Calle-street} + ' ' + $F{Calle2-street2}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Total-amount_total"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Nombre y domicilio del pagador"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Direccion_factura-address_invoice_id/C.P.-zip"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Origen-origin"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Nombre-name2}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Localidad de expedición"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Numero_de_cuenta-acc_number} != \"\""
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "P.P."
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "SOLLER"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Cuenta_bancaria-partner_bank/Numero_de_cuenta-acc_number"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Numero_factura-number}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Fecha de vencimiento"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Fecha de factura"
msgstr ""

#. module: soller_extend_invoice_comer
#: model:ir.actions.report.xml,name:soller_extend_invoice_comer.report_rebut_generic_comercial
msgid "Rebut"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Origen-origin}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Número de cuenta"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Fecha_vencimiento-date_due}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "\"/home/erp5/comer/versio5/GISCE-ERP/addons/gisce/Soller/soller_extend_invoice_comer/report/firma_jaf.jpg\""
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Pagos-payment_ids/Fecha_creacion-date_created"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Direccion_factura-address_invoice_id/Ciudad-city"
msgstr ""

#. module: soller_extend_invoice_comer
#: model:ir.module.module,description:soller_extend_invoice_comer.module_meta_information
msgid "\n"
"Extensió de les funcionalitats de factura\n"
"    *Report genèric de factura de comercialitzadora\n"
"    *Report genèric de rebut de comercialitzadora\n"
"    "
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Numero_de_cuenta-acc_number}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Ciudad-city}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "\"/home/erp5/comer/versio5/GISCE-ERP/addons/gisce/Soller/soller_extend_invoice_comer/report/logo_recibo_ES.png\""
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{C.P.-zip}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Total-amount_total}"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "$F{Fecha_factura-date_invoice}"
msgstr ""

#. module: soller_extend_invoice_comer
#: model:ir.module.module,shortdesc:soller_extend_invoice_comer.module_meta_information
msgid "Extensió Invoice Soller Comer"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Direccion_factura-address_invoice_id/Calle2-street2"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Banco"
msgstr ""

#. module: soller_extend_invoice_comer
#: rml:report_rebut_generic_comercial:0
msgid "Importe"
msgstr ""

