# -*- coding: utf-8 -*-
{
    "name": "Extensió Invoice Soller Comer",
    "description": """
Extensió de les funcionalitats de factura
    *Report genèric de factura de comercialitzadora
    *Report genèric de rebut de comercialitzadora
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "soller_extend_invoice"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_extend_invoice_comer_report.xml"
    ],
    "active": False,
    "installable": True
}
