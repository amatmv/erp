# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time

class SollerClausula(osv.osv):

    _name = "soller.clausula"
    _description = 'Clausula'
    

    _columns = {
        'name':fields.char('Nom', size=64, required=False, readonly=False),
        'text': fields.text('Texte'),      
        
    }

SollerClausula()

class SollerExtendCedula(osv.osv):
        
    
    _name = 'giscedata.cedula'
    _inherit = 'giscedata.cedula'
    
    def fields_view_get(self, cursor, uid, view_id=None, 
                        view_type='form', context=None, toolbar=False):
        '''sobreescrivim el metode per canviar 
        el label del camp name'''

        res = super(SollerExtendCedula, self).fields_view_get(cursor, uid, view_id, 
                                view_type, context=context, toolbar=toolbar)

        if 'fields' in res:
            fields = res.get('fields',{}).keys()
            if 'polissa_id' in fields:
                res['fields']['polissa_id']['string'] = 'Abonat'
        return res

    
    _columns = {
        'especial':fields.boolean('Condicions especials', required=False),
        'data_final_clausula': fields.date('Data final Clausula'),
        'clausula_id':fields.many2one('soller.clausula', 'Clausula', required=False),
        'text_clausula': fields.related('clausula_id','text', 
                            type='text', store=False, readonly=True,
                            relation='soller.clausula', string='Texte'),
            
        
    }
     

SollerExtendCedula()
