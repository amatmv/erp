# -*- coding: utf-8 -*-
{
    "name": "Soller Extend Cedula",
    "description": """
Funcionalitats extra per cedula
    *Condicions especials per la contractacio
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Soller",
    "depends":[
        "giscedata_polissa_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "soller_extend_cedula_view.xml",
        "soller_extend_cedula_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
