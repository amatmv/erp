# -*- coding: iso-8859-1 -*-
import wizard
import pooler
import asterisk.manager
import time

def _trucar(self, cr, uid, data, context={}):
  print "Trucant a %s..." % data['form']['telefons']
  manager = asterisk.manager.Manager()
  manager.connect('uma.gisce.local')
  manager.login('erp', 'erp')
  o = manager.originate('Zap/1', data['form']['telefons'], 'internal', 1)
  return {}

def _multiples_telefons(self, cr, uid, data, context={}):
  print data
  cr.execute("select name,phone from res_partner_address where partner_id = %i and phone is not null" % data['id'])
  phones = cr.dictfetchall()
  cr.execute("select name,mobile from res_partner_address where partner_id = %i and mobile is not null" % data['id'])
  mobiles = cr.dictfetchall()

  nums = [(r['mobile'], "%s - %s" % (r['name'], r['mobile'])) for r in mobiles]
  
  for phone in phones:
    nums.append((phone['phone'], "%s - %s" % (phone['name'], phone['phone'])))

  data['telefons'] = nums


  if len(nums) > 1:
    return 'escollir_telefon'
  else:
    return 'end'

def _escollir_telefon(self, cr, uid, data, context={}):

  self.states['escollir_telefon']['result']['fields']['telefons'].update({'selection': data['telefons']})
  return {}


_escollir_telefon_form = """<?xml version="1.0"?>
<form string="Telèfons disponibles">
  <field name="telefons" />
</form>"""

_escollir_telefon_fields = {
  'telefons' : {'string': "Telèfon", 'type': 'selection', 'selection': [] },
}



class wizard_trucar(wizard.interface):
	states = {
	  	'init': {
	    	'actions': [],
	    	'result': {'type':'choice', 'next_state':_multiples_telefons}
	    },
      'escollir_telefon': {
    	  'actions': [_escollir_telefon],
        'result': {'type': 'form', 'arch': _escollir_telefon_form,'fields': _escollir_telefon_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('trucar', 'Trucar', 'gtk-go-forward')]}
      },
      'trucar': {
        'actions': [_trucar],
        'result': {'type':'state', 'state': 'end'}
      },
      'end': {
        'actions': [],
        'result': {'type':'state', 'state': 'end'}
      }
	}

wizard_trucar('giscevoip.partner.trucar')

