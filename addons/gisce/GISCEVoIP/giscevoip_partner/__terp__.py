# -*- coding: utf-8 -*-
{
    "name": "GISCE VoIP partner",
    "description": """Trucar a un partner des de l'ERP""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Comunications",
    "depends":[
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscevoip_partner_wizard.xml"
    ],
    "active": False,
    "installable": True
}
