#!/bin/bash
set -e
if [ "$1" == "openerp-server" ]; then
    . /home/erp/bin/activate

    # Creating conf
    mkdir -p /home/erp/conf
    cd /home/erp/conf
    sh /tmp/server.conf.tmpl
    echo "Exporting src libs... "
    for EGG in `ls -1d /home/erp/src/*`; do
        export PYTHONPATH=$PYTHONPATH:$EGG
    done

    export OPENERP_ROOT_PATH=/home/erp/src/erp/server/bin
    export OPENERP_ADDONS_PATH=/home/erp/src/erp/server/bin/addons
    export PYTHONPATH=$OPENERP_ROOT_PATH:$OPENERP_ADDONS_PATH:/home/erp/src/erp/server/sitecustomize:$PYTHONPATH
    export PATH=/home/erp/bin:$PATH

    # Make the addons visible
    cd /home/erp/src/erp && bash tools/link_addons.sh

    # Test if DB exist //avoiding fatal errors
    while ! python -c "from psycopg2 import connect; connect(\"dbname='${OPENERP_DB_NAME}' host='${OPENERP_DB_HOST}' user='${OPENERP_DB_USER}' password='${OPENERP_DB_PASSWORD}'\")"; do
        echo "Preparing DB from scratch based on specs and demo data"
        # Prepare the test db for passed module
        if [ -n "$MODULE_TO_TEST" ]; then
            echo "Destraling ${MODULE_TO_TEST}"
            python /home/erp/bin/destral -m $MODULE_TO_TEST --no-dropdb
        fi
    done

    # Start ERP server
    cd /home/erp/src/erp/server/bin
    exec ./openerp-server.py --config=/home/erp/conf/server.conf --log-level=debug --price_accuracy=6 
else
  exec "$@"
fi
