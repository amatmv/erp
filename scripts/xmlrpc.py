#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Script plantilla per fer consultes XMLRPC
"""

from optparse import OptionParser
import xmlrpclib

def run():
    """Mètode principal"""
    parser = OptionParser(usage="%prog [OPTIONS]", version="%prog 0.1")
    parser.add_option('-u', '--user', dest='user', metavar='USER',
                      default='admin',
                      help='User to use to connect to the database')
    parser.add_option('-w', '--password', dest='password',
                      default='admin',
                      help='Password to use for user USER')
    parser.add_option('-d', '--database', dest='database',
                      metavar='DATABASE', help='Database to connect to')
    parser.add_option('-H', '--host', dest='host', metavar='HOST',
                      default='localhost',
                      help='The host where the ERP is listening on')
    parser.add_option('-p', '--port', dest='port', metavar='PORT',
                      default=8069,
                      help='The port where the ERP is listening on')

    (options, _) = parser.parse_args()
    if not options.database:
        parser.error(u"The database parameter is required")

    user = options.user
    pwd = options.password
    dbname = options.database
    port = options.port
    host = options.host

    # Comencem...
    sock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/common' % (host, port))
    uid = sock.login(dbname, user, pwd)
    sock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/object' % (host, port))
    
    # Editar aquí al gust... :)
    # res = sock.execute(dbname, uid, pwd, 'model.name', 'method.name', args)

if __name__ == '__main__':
    run()

