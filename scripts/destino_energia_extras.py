#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ooop import OOOP
import csv
import os
import xmlrpclib
from datetime import datetime

O5 = OOOP(dbname=os.getenv('DBNAME', 'oerp5_distri'), user='root', pwd='root',
          port=int(os.getenv('OOOP_PORT', '8050')),
         debug=int(os.getenv('DEBUG', 0)))
CSV_FILE = os.getenv('CSV_FILE', '/tmp/aspirapol5.csv')
ERR_FILE = '%s.err' % CSV_FILE
READER = csv.reader(open(CSV_FILE), delimiter=';')
if int(os.getenv('START_LINE', 0)):
    ERROR = open(ERR_FILE, 'a+')
else:
    ERROR = open(ERR_FILE, 'w')
PROCESSADES = 0
ROW_FIELDS = 6


def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))

def _get_proxy(model):
    """Retorna un objecte OOOP pel model.
    """
    return getattr(O5, model2cc(model))

def create_or_update(model, vals, **kwargs):
    """Creem o actualitzem segons faci o no faci match.
    """
    proxy = _get_proxy(model)
    results = proxy.filter(**kwargs)
    msg = ""
    start = datetime.now()
    if len(results):
        msg += "Updating"
        obj = results[0]
    else:
        msg += "Creating"
        obj = proxy.new()
    for key, value in vals.items():
        setattr(obj, key, value)
    obj.save()
    end = datetime.now()
    msg += " %s (id: %i)" % (model, obj.id)
    print msg
    print "Time: %.3fs." % ((end - start).seconds +
                             float((end - start).microseconds) / 10**6)
    return obj.id

def get_id(model, **kwargs):
    """Retorna l'id segons el match.
    """
    proxy = _get_proxy(model)
    results = proxy.filter(**kwargs)
    if not len(results):
        raise Exception("No s'ha trobat l'id del model: %s amb match: %s"
                        % (model, kwargs))
    return results[0].id

def parse_row(row):
    """Processa la fila.
    """
    # Stripping all values
    row = map(str.strip, row)
    
    id_vals = {}
    
    # Butlletí
    #0    Polissa
    #3    Fecha final obras
    #4    Potència màxima (al butlletí!)
    #5    Fecha alta contrato
    #6    Data del butlletí
    #7    data de vigència
    
    if row[6] == '1900-01-01':
        row[6] = False
    if row[7] == '1900-01-01':
        row[7] = False
    
    id_vals['polissa'] = get_id('giscedata.polissa', name=row[0])
    polissa_proxy = _get_proxy('giscedata.polissa')
    butlleti_proxy = _get_proxy('giscedata.butlleti')
    polissa = polissa_proxy.read(id_vals['polissa'], ['butlletins'])
    if len(polissa.get('butlletins', [])):
        bids = polissa['butlletins']
        butlleti_proxy.write(bids, {'data_provisional_obra': row[3] or False,
                                    'pot_max_admisible': float(row[4].replace(',', '.')),
                                    'data': row[6],
                                    'data_vigencia': row[7]})
        print u"Up butlletí"
    polissa_proxy.write([id_vals['polissa']],
                        {'data_firma_contracte': row[5] or False})
    print u"Up polissa"
    

def print_row(row):
    """Print row.
    """
    i = 0
    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    for value in row:
        print '%i: %s' % (i, value)
        i += 1
    print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    print "Line: %i" % READER.line_num

def write_error(row, comment=None):
    """Write error to a file.
    """
    ERROR.write('# Line %i - %s\n' % (READER.line_num, comment or ''))
    ERROR.write(';'.join(row)+'\n')
    ERROR.flush()

def halt():
    """Halt waiting for user input.
    """
    print "Prem intro per continuar ..."
    raw_input()

def main():
    """Funció principal de l'script.
    """
    global PROCESSADES
    for row in READER:
        if READER.line_num < int(os.getenv('START_LINE', 0)):
            continue
        if row[0].startswith('#'):
            continue
        if len(row) != ROW_FIELDS:
            print "!!! %i != %i" % (len(row), ROW_FIELDS)
            write_error(row, comment="Longitud: %i diferent a %i;" % 
                        (ROW_FIELDS, len(row)))
            continue
        try:
            time_start = datetime.now()
            print_row(row)
            parse_row(row)
            PROCESSADES += 1
            time_end = datetime.now()
            print "Time elapsed: %.3fs." % ((time_end - time_start).seconds +
                            float((time_end - time_start).microseconds) / 10**6)
        except xmlrpclib.Fault, fault:
            print "Exception! %s" % fault.faultString
            write_error(row)
            if int(os.getenv('HALT_ON_ERROR', 0)):
                halt()
        except Exception, ecx:
            print "Exception! %s" % ecx
            write_error(row)
            if int(os.getenv('HALT_ON_ERROR', 0)):
                halt()
        finally:
            if int(os.getenv('STEP_BY_STEP', 0)):
                halt()

if __name__ == "__main__":
    try:
        START_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        main()
    except KeyboardInterrupt:
        print
        print 'Shutting Down...'
    finally:
        END_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print "Start at %s" % START_DATE
        print "End at %s" % END_DATE
        print "Processed: %i" % PROCESSADES
        ERROR.close()
