#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
pj = os.path.sep.join
import sys
import base64
import ConfigParser
import subprocess

from ooop import OOOP


def usage():
    print "***ERROR: Executar des de l'arrel del projecte:"
    print "    %s dir http://localhost 8069 dbname user pwd [module]" % \
        sys.argv[0]
    print "O pots utilitzar el format url"
    print "    %s dir http://user:pwd@localhost:8069/dbname[?module]" % \
        sys.argv[0]
    sys.exit(-1)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "***ERROR: Falten més paràmetres (<2)***"
        usage()
    elif 7 <= len(sys.argv) <= 8:
        directory = sys.argv[1]
        uri = sys.argv[2]
        port = sys.argv[3]
        dbname = sys.argv[4]
        user = sys.argv[5]
        pwd = sys.argv[6]
        module_dst = None
        if len(sys.argv) == 8:
            module_dst = sys.argv[7]
    elif len(sys.argv) == 3:

        from urlparse import urlparse
        try:
            url = urlparse(sys.argv[2])
        except:
            print "***ERROR: la URL '%s' no és correcte" % sys.argv[2]
            usage()

        #valors per defecte
        directory = sys.argv[1]
        uri = "%s://%s" % (url.scheme or 'http', url.hostname or 'localhost')

        port = url.port or 8069
        dbname = url.path[1:]
        user = url.username or 'root'
        pwd = url.password or 'root'
        module_dst = None
        if url.query:
            module_dst = url.query

        print url
    else:
        print "***ERROR: Falten alguns paràmetres ***"
        usage()

    O = OOOP(uri=uri, port=int(port), dbname=dbname, user=user, pwd=pwd)
    config = ConfigParser.RawConfigParser()
    config.read(os.path.join('.tx', 'config'))
    sections = [s for s in config.sections() if s != 'main']
    resources = []
    for section in sections:
        source_file = config.get(section, 'source_file')
        if not os.path.exists(source_file):
            print "Removing section %s" % section
            sections.remove(section)
            config.remove_section(section)
    for root, dirs, files in os.walk(directory):
        if '.svn' in dirs:
            dirs.remove('.svn')
        if '.git' in dirs:
            dirs.remove('.git')
        if '__terp__.py' in files:
            module = root.split(os.path.sep)[-1]
            if module_dst and module != module_dst:
                continue
            terp_file_path = os.path.join(root, '__terp__.py')
            f = open(terp_file_path, 'r')
            terp = eval(f.read())
            f.close()
            mod_ids = O.IrModuleModule.search([('name', '=', module),
                                               ('state', '=', 'installed')])
            if mod_ids:
                # Està instal·lat, passem l'assistent per generarl el .po
                section = 'erp.%s' % module
                if section not in sections:
                    config.add_section(section)
                    status = 'ADD    '
                else:
                    status = 'UPDATED'
                print "**** [%s] MODULE: %s ****" % (status, module)
                wiz_id = O.WizardModuleLangExport.create({
                    'format': 'po',
                    'modules': [(6, 0, mod_ids)],
                })
                wiz = O.WizardModuleLangExport.get(wiz_id)
                wiz.act_getfile()
                wiz = O.WizardModuleLangExport.get(wiz_id)
                if 'i18n' not in dirs:
                    os.mkdir(os.path.join(root, 'i18n'))
                with open(os.path.join(root, 'i18n', wiz.name), 'w') as pot:
                    pot.write(base64.b64decode(wiz.data))
                i18n_path = pj([root, 'i18n'])
                config_options = (
                    ('file_filter', pj([i18n_path, '<lang>.po'])),
                    ('source_file', pj([i18n_path, '%s.pot' % module])),
                    ('source_lang', 'ca_ES'),
                    ('type', 'PO')
                )
                for option, value in config_options:
                    config.set(section, option, value)
                resources.append(section)
    with open(os.path.join('.tx', 'config'), 'wb') as configfile:
        config.write(configfile)
    for resource in resources:
        print "Pushing %s..." % resource
        p = subprocess.Popen("tx push -s -r %s" % resource, shell=True,
                             stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                             close_fds=True)
        p.wait()
