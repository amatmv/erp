#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

"""
This script generates a dependency graph for OpenERP modules.

You should provide to the script the path where addons are located and then
the script will generate a PNG image with the dependency graph.
"""

import os
import pydot

def find_terps(path):
    """Locates the __terp__.py files.
    """
    if not os.access(path, os.R_OK):
        raise Exception("Could not access %s" % path)
    file_list = []
    for dirs, subdirs, files in os.walk(path):
        file_list.extend('%s%s%s' % (dirs, os.sep, f) for f in files 
                         if f == '__terp__.py')
    return [(p.split('/')[-2], p) for p in file_list]

def run():
    modules = find_terps('/home/marcos/Projects/VirtualEnvs/erp-5/src/erp/addons/gisce')
    graphs = {}
    for module, terp in modules:
        mdict = eval(open(terp, 'r').read())
        graphs[module] = pydot.Dot(graph_type='digraph')
        for dep in mdict['depends']:
            graphs[module].add_edge(pydot.Edge(pydot.Node(module), pydot.Node(dep)))
        graphs[module].write_png('/home/marcos/Projects/gdeps/%s_deps.png' % (module,))



if __name__ == '__main__':
    run()

