# -*- coding: utf-8 -*-

# Actualitzar una pòlissa amb la modificació contractual
# més recent

from datetime import datetime
import csv
import sys

from ooop import OOOP


t0 = datetime.now()

uri, dbname, port, user, pwd, mute = sys.argv[1:]
mute = eval(mute)
O = OOOP(dbname=dbname, user=user, pwd=pwd, port=int(port), uri=uri)

poli_id = int(raw_input("Introdueix l'id de la pòlissa actualitzar "
                        "amb la modificació contractual més recent: "))

mod_id = O.GiscedataPolissaModcontractual.search(
                          [('polissa_id', '=', poli_id)],
                          0, 0, 'data_inici desc', {'active_test': False})[0]
O.GiscedataPolissaModcontractual.aplicar_modificacio(mod_id, poli_id)

print u"Temps d'execució: %s" % (datetime.now() - t0)
