﻿UPDATE giscedata_cups_ps c 
SET cne_anual_activa = energia.activa , cne_anual_reactiva = energia.reactiva
FROM (
select c.id as id , c.name as cups ,
COALESCE(SUM(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as activa,
COALESCE(SUM(il.quantity*(fl.tipus='reactiva')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as reactiva
from giscedata_cups_ps c
LEFT JOIN giscedata_facturacio_factura f ON (f.cups_id=c.id and to_char(f.data_final, 'YYYY') = '2011')
--LEFT JOIN giscedata_facturacio_factura f ON (f.cups_id=c.id and to_char(f.data_final, 'YYYY') = '%')
LEFT JOIN account_invoice i ON (i.id=f.invoice_id)

LEFT JOIN giscedata_facturacio_factura_linia fl ON (fl.factura_id=f.id and fl.tipus in ('energia','reactiva'))
LEFT JOIN account_invoice_line il on (fl.invoice_line_id=il.id)

GROUP BY c.id,c.name
) as energia
WHERE energia.id=c.id

