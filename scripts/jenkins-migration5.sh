#!/bin/bash
# Activem el virtualenv
source /home/eduard/migracio/bin/activate
echo $(which python)
tools/link_addons.sh
cd server/bin/addons
unlink spawn_oop
ln -s /home/eduard/migracio/src/spawn_oop/spawn_oop .
cd ../../..
if [ "$(psql -l | grep $JOB_NAME)" != "" ]; then
  echo "Drop db $JOB_NAME"
  dropdb $JOB_NAME
fi
if ([ $COMPLETE == false ] && [ -e "/home/eduard/migracio/src/sqls/${JOB_NAME}_pre.sql.bz2" ]); then
  RESTORE="/home/eduard/migracio/src/sqls/${JOB_NAME}_pre.sql.bz2"
  RESTORED=1
elif [ -e "/home/eduard/migracio/src/sqls/${JOB_NAME}.sql.bz2" ]; then
  RESTORE="/home/eduard/migracio/src/sqls/${JOB_NAME}.sql.bz2"
  RESTORED=0
else
  echo "No sql.bz2 found for $JOB_NAME"
  exit 1
fi
createdb $JOB_NAME
bzcat $RESTORE | psql --quiet $JOB_NAME > /dev/null 2>&1
psql -e $JOB_NAME < /home/eduard/migracio/src/sql_scripts/pre2_0.sql
if ([ $RESTORED -eq 0 ] && [ $COMPLETE == false ]); then
  psql -e $JOB_NAME < /home/eduard/migracio/src/sql_scripts/reduce.sql
  pg_dump $JOB_NAME | bzip2 > "/home/eduard/migracio/src/sqls/${JOB_NAME}_pre.sql.bz2"
fi

for PATCH in `find /home/eduard/migracio/src/patches/ -name '*.patch'`; do 
  git apply $PATCH
done

python scripts/update_terp_version.py addons/gisce 2.17.0

cd server/bin
python openerp-server.py --database=$JOB_NAME --price_accuracy=6 --no-netrpc --port=0 --update=all --stop-after-init
