#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Genera el contingut dels fitxers ir.model.access.csv.

Com a entrada rep un fitxer amb cada un dels models pels què s'ha de generar
el contingut del fitxer ir.model.access.csv. Un nom de model per línia.

Imprimeix el seu contingut per pantalla per defecte o a un fitxer si se li'n
proporciona.

Input d'exemple
---------------

giscedata.at.aillador
giscedata.at.armat
giscedata.at.cables
giscedata.at.conductors
giscedata.at.familiacables

Output d'exemple
----------------

"access_giscedata_at_aillador","giscedata.at.aillador","model_giscedata_at_aillador","base.group_user",1,1,1,0
"access_giscedata_at_armat","giscedata.at.armat","model_giscedata_at_armat","base.group_user",1,1,1,0
"access_giscedata_at_cables","giscedata.at.cables","model_giscedata_at_cables","base.group_user",1,1,1,0
"access_giscedata_at_conductors","giscedata.at.conductors","model_giscedata_at_conductors","base.group_user",1,1,1,0
"access_giscedata_at_familiacables","giscedata.at.familiacables","model_giscedata_at_familiacables","base.group_user",1,1,1,0

"""

import csv
import sys
from optparse import OptionParser

def run():
    """Mètode principal"""
    parser = OptionParser(usage="%prog [OPTIONS]", version="%prog 0.1")
    parser.add_option('-i', '--input', dest='input', metavar='INPUT',
                      default=sys.stdin,
                      help='The file to read the lines from, default to STDIN')
    parser.add_option('-o', '--output', dest='output', metavar='OUTPUT',
                      default=sys.stdout,
                      help='The file to write the output to, default to STDOUT')

    (options, _) = parser.parse_args()
    if isinstance(options.input, str):
        options.input = open(options.input, 'r')
    if isinstance(options.output, str):
        options.output = open(options.output, 'w')
    input = options.input
    output = options.output
    
    fout = csv.writer(output, delimiter=',', quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
    for model in input.readlines():
        model = model.replace('\n','')
        row = []
        name = model.replace('.', '_')
        row.append("access_%s" % (name,))
        row.append(model)
        row.append("model_%s" % (name,))
        row.append("base.group_user")
        row.append(1)
        row.append(1)
        row.append(1)
        row.append(0)
        fout.writerow(row)
    input.close()
    output.close()

if __name__ == '__main__':
    run()
