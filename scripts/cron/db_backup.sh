#!/bin/sh
# Backup db

PATHBK='/media/backup'
DBNAME='03.HIDROELECTRICA_VIRGEN_DE_CHILLA'
COMPANY=Chilla

HISTFOLDER=$PATHBK/ERP_backup_anual
DATE=`date +%Y%m%d%H%M%S`
BACKUPNAME=$COMPANY-$DATE.sql

CURRFOLDER=$PATHBK/ERP_backup_`date +%Y%m`
if [ ! -d  $CURRFOLDER ]; then
  mkdir $CURRFOLDER
fi

pg_dump $DBNAME | bzip2 > $CURRFOLDER/$BACKUPNAME.bz2

NUMFOLDERS=`ls -p $PATHBK | grep backup_.*/ | wc -l`

if [ $NUMFOLDERS -eq 3 ]; then
  RMFOLDER=`ls -p $PATHBK | grep backup_.*/ | head -n 1`
  if [ ! -d  $HISTFOLDER ]; then 
      mkdir $HISTFOLDER
  fi
  SAVEBK=`ls $PATHBK/$RMFOLDER/ | tail -n 1`
  cp $PATHBK/$RMFOLDER/$SAVEBK $HISTFOLDER
  rm -r $PATHBK/$RMFOLDER
fi

if [ -d $HISTFOLDER ]; then
  NUMSAVES=`ls $HISTFOLDER/*bz2 | wc -l`;
  if [ $NUMSAVES -eq 13 ]; then
    RMSAVE=`ls $HISTFOLDER/*bz2 | head -n 1`;
    rm $RMSAVE;
  fi
fi
