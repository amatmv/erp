#!/usr/bin/env python
import sys
import re

from ooop import OOOP


PATTERN_SUB = re.compile('[A-Z]').sub


def camel2dot(model):
    return PATTERN_SUB(lambda m: '.' + m.group(0).lower(), model).lstrip('.')


def main():
    action, dbname, port, user, pwd = sys.argv[1:6]
    uri = len(sys.argv) > 6 and sys.argv[6] or 'http://localhost'
    O = OOOP(dbname=dbname, port=int(port), user=user, pwd=pwd, uri=uri)
    # Busquem l'action al sistema de crons
    search_params = []
    if '.' in action:
        model, action = action.split('.')
        model = camel2dot(model)
        search_params += [('model', '=', model)]
    search_params += [('function', '=', action)]
    cron_ids = O.IrCron.search(search_params, 0, 0, False,
                           {'active_test': False})
    if not cron_ids:
        sys.stderr.write("No s'ha trobat l'action: %s en els cronjobs."
                         % action)
        sys.stderr.flush()
        sys.exit(1)
    cron = O.IrCron.get(cron_ids[0])
    obj = getattr(O, O.normalize_model_name(cron.model))
    getattr(obj, cron.function)(*eval(cron.args))
    sys.exit(0)


if __name__ == '__main__':
    main()
