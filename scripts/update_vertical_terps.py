#!/usr/bin/env python
# -*- coding: utf-8 -*-


def parsed_list_elem(element, list_value):
    indent = '    '
    parsed_str = '\n{indent}"{element}": [\n'.format(**locals())
    for value in list_value:
        parsed_str += '{indent}{indent}"{value}",\n'.format(**locals())
    parsed_str += '{indent}],'.format(**locals())
    return parsed_str


if __name__ == "__main__":
    import os
    import sys

    directory = sys.argv[1]

    default_values = {'name': '',
                      'description': '',
                      'version': '0-dev',
                      'author': 'GISCE',
                      'category': '',
                      'depends': ['base'],
                      'extra_depends': '',
                      'init_xml': [],
                      'demo_xml': [],
                      'update_xml': [],
                      'active': False,
                      'installable': True}

    template = """# -*- coding: utf-8 -*-
{
    "name": "%(name)s",
    "description": \"""%(description)s\""",
    "version": "%(version)s",
    "author": "%(author)s",
    "category": "%(category)s",
    "depends": %(depends)s,
    "init_xml": %(init_xml)s,
    "demo_xml": %(demo_xml)s,
    "update_xml": %(update_xml)s,
    "active": %(active)s,
    "installable": %(installable)s
}
"""

    for root, dirs, files in os.walk(directory):
        if '.svn' in dirs:
            dirs.remove('.svn')
        if '.git' in dirs:
            dirs.remove('.git')
        if '__terp__.py' in files:
            module = root.split(os.path.sep)[-1]
            print "**** MODULE: %s ****" % module
            terp_file_path = os.path.join(root, '__terp__.py')
            print "  => Editing file %s" % terp_file_path
            f = open(terp_file_path, 'r')
            terp = eval(f.read())
            f.close()
            # Ensure that terp description has all keys
            for k, v in default_values.items():
                if not terp.has_key(k):
                    terp[k] = v
            module_template = """# -*- coding: utf-8 -*-
{"""
            for template_line in template.split('\n')[2:-2]:
                template_elem = template_line.strip().split(':')[0][1:-1]
                if template_elem in terp.keys():
                    if (isinstance(terp[template_elem], list) and
                            terp[template_elem]):
                        module_template += parsed_list_elem(
                            template_elem, terp[template_elem])
                        if (template_elem == 'depends' and
                                terp.get('extra_depends', False)):
                            module_template += parsed_list_elem(
                                'extra_depends', terp['extra_depends'])
                    else:
                        module_template += ('\n' + (
                            template_line % {template_elem: terp[template_elem]}
                        ))

            module_template += '\n}\n'

            f = open(terp_file_path, 'w')
            f.write(module_template)
            f.flush()
            f.close()
