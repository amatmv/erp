#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Crea un CLMAG a partir d'un fitxer CSV amb el dump de la taula de perfils.

Els camps del CSV han de venir en el següent ordre:

 'comercialitzadora',
 'agree_tensio',
 'month',
 'year',
 'agree_dh',
 'estacio',
 'agree_tipo',
 'distribuidora',
 'aprox',
 'day',
 'hour',
 'agree_tarifa',
 'magnitud',
 'provincia'

La consulta la farem de la següent manera:

psql PROHIDA_PERFILS -tA  -c "select comercialitzadora,agree_tensio,month,year,agree_dh,estacio,agree_tipo,distribuidora,aprox,day,hour,agree_tarifa,magnitud,provincia
from giscedata_lectures_perfils_perfil where name > '2010030100' and name <= '20100331240'" > ~/perfils_201003.dump

amb les dates i base de dades corresponents :)
"""

import calendar
import csv
from optparse import OptionParser
import time

def run(file, month, year):
    """Funció principal.
    """
    csvp = csv.reader(open(file, 'rb'), delimiter='|')
    days = [day for day in range(1, calendar.monthrange(year, month)[1]+1)]
    energia = {}
    energia_reg = {}
    quant = {}
    quant_reg = {}

    done_days = []
    
    agres = {}
    
    first = True
    extra = 0
    for reg in csvp:
        if first:
            estacio = reg[5]
        key = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;" % (
            reg[9], reg[2], reg[3], reg[7], reg[0], reg[13], reg[1], reg[11], 
            reg[4], reg[6], reg[12])
        if not agres.has_key(key):
            agres[key] = {}
            for i in range(1, 26):
                agres[key][i] = [0, 0]
        if estacio == 1 and reg[5] == 0:
            extra = 1
        agres[key][int(reg[10])+extra][0] += int(reg[8])
        agres[key][int(reg[10])+extra][1] += 1
        first = False
    
    output = open('/tmp/CLMAG_%s_%s%02i_%s.V' % (reg[7], year, month, 
                                                 time.strftime("%Y%m%d")), 'w')
    wrote = 0
    for line, consums_dict in agres.items():
        consums = []
        for i in xrange(1, 26):
            consums.extend(consums_dict[i]*2)
        output.write("%s%s;\n" % (line, ";".join(map(str, consums))))
        wrote += 1
        if not wrote % 100:
            output.flush() 

    output.flush()
    output.close()
    
    
if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="filename", help="the input FILE", 
                      metavar="FILE")
    parser.add_option("-m", "--month", dest="month", help="the data MONTH",
                      type="int", metavar="MONTH")
    parser.add_option("-y", "--year", dest="year", help="the data YEAR",
                      type="int", metavar="YEAR")
    

    (options, args) = parser.parse_args()
    if not options.filename or not options.month or not options.year:
        parser.error(u"Tots els paràmetres són obligatoris.")
        
    run(options.filename, options.month, options.year)
    
    
