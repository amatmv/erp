#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ooop import OOOP
ODISTRI = OOOP(dbname='oerp5_distri', user='root', pwd='root', port=8051)
OCOMER = OOOP(dbname='oerp5_comer', user='admin', pwd='admin', port=8050)

for polissa in ODISTRI.GiscedataPolissa.all():
    num_polissa = polissa.codi
    res = OCOMER.GiscedataPolissa.filter(ref_dist=num_polissa)
    if len(res):
        polissa_comer = res[0]
        polissa.ref_comer = polissa_comer.name
        print(u"Actualitzant pòlissa i modcontractual: "
              u"%s ref_dist => %s (%s)" % (polissa.name, polissa.ref_comer,
                                           polissa.comercialitzadora.name))
        polissa.save()
        if polissa.modcontractual_activa:
            polissa.modcontractual_activa.ref_comer = polissa_comer.name
            polissa.modcontractual_activa.save()
