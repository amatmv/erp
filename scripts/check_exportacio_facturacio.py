#!/usr/bin/env python
#
# --- check_exportacio_facturacio.py ---
# ./check_exportacio_facturacio.py --iteration=<val>
# on val és el número de fila a retornar del resultat de d'executar la consulta
# ---

# -*- coding: UTF-8 -*-

# ----
# configuració connexió BD
# ----

strUser='gis'
strPass='atrallum.gis'
strDSN='Test'

# ----


keywords = ['iteration=']

import getopt
import pyodbc
import sys

opts, extraparam = getopt.getopt(sys.argv[1:], '',keywords)

eval=True
for o, a in opts:
	if o == '--iteration':
		try:
			niter=int(a)
			break
		except ValueError:
			print "Valor incorrecte"
			eval=False

if eval:

	query="SELECT dbo.Abonados.CUPS,dbo.Ciudades.CodigoINE AS MUNICIPI, dbo.Ciudades.[texto-ciudad] AS POBLACIO, dbo.DOMICILIOS.nombreCalle AS CARRER, dbo.Abonados.NumDomicilio AS NUMERO, dbo.Abonados.extnumerodomici1 AS ESCALA, '' AS PLANTA, '' AS PIS, dbo.Abonados.CodAbonado AS [CODI ABONAT], dbo.Abonados.NombreAbonado AS [NOM CLIENT], dbo.Abonados.Tel1 AS TELEFON, dbo.Abonados.Fax, dbo.Abonados.[E-mail] AS EMAIL, dbo.Contadores.NUMERO AS [N COMPTADOR], '' AS [N COMPTADOR_2], '' AS [N COMPTADOR_3], '' AS [N COMPTADOR REACTIVA], CAST(CAST(dbo.Abonados.Poliza AS int) AS nvarchar) AS POLISSA, dbo.Abonados.Tarifa AS [NOM TARIFA], CONVERT(char(10), dbo.Abonados.FechaAlta, 103) AS [DATA ALTA], CASE WHEN dbo.Abonados.FechaBaja IS NULL THEN 0 ELSE 1 END AS BAIXA, dbo.Abonados.FechaBaja AS DBAIXA, '' AS ZONA, 0 AS ORDRE, CASE WHEN POT1 = 20 OR POT2 = 20 OR POT3 = 20 THEN CAST(POTENCIAREALICP AS decimal(6, 2)) WHEN POT2 = 0 THEN CAST(POT1 AS decimal(6, 2)) ELSE CAST(POT2 AS decimal(6, 2)) END AS POT, dbo.TENSIONSUMINISTRO.DESCRIPCION AS TENSIO, '' [CONSUM ANY], 'CT-' + RIGHT('000' + CONVERT(varchar(10), dbo.Abonados.CentroTransformacion), 3) AS ET, dbo.Abonados.Circuito AS LINIA, '' AS AVIS_DESCARREGA, '' AS TIPUS_AVIS, '' AS ACTIVITAT, '' AS COMERCIALITZADORA, '' AS OBSERVACIONS, dbo.Abonados.Nombre2Abonado AS TITULAR, dbo.Abonados.Discriminador AS DISCRIMINACIO, '' AS ESTAT, '' AS SERVEI, '' AS ICP, '' AS FASE, 'R1-091' AS DISTRIBUIDORA, '' AS COORDENADES, '' AS PROVINCIA, '' AS POT_MAX_INSTALADOR, '' AS TIPUS_PUNT_MIG, '' AS DISP_INTERR_CTRL_POT, '' AS TIPUS_PERFIL_CONSUMO, '' AS DERECHOS_EXTENSION, '' AS DERECHOS_ACCESOS, '' AS PROP_EQUIPO_MEDIDA, '' AS PROP_INTERR_CTRL_POT, '' AS POT_CONTRAC_PERIODO, '' AS DATA_ULT_MOV_TARIFA, '' AS DATA_ULT_MOV_COMER, '' AS DATA_LIMIT_DERECHOS_EXTENSION, '' AS CONSUM_ULTIMS_2_ANYS, '' AS DATA_ULTIMA_LECTURA, dbo.TENSIONSUMINISTRO.Fases AS NUM_FASES, dbo.Abonados.NIF FROM dbo.Abonados INNER JOIN dbo.TENSIONSUMINISTRO ON dbo.Abonados.empresa = dbo.TENSIONSUMINISTRO.Empresa AND dbo.Abonados.Tension = dbo.TENSIONSUMINISTRO.CODIGO LEFT OUTER JOIN dbo.Contadores ON dbo.Abonados.CodAbonado = dbo.Contadores.ABONADO LEFT OUTER JOIN dbo.Ciudades ON dbo.Abonados.Ciudad = dbo.Ciudades.codigo AND dbo.Abonados.empresa = dbo.Ciudades.empresa LEFT OUTER JOIN dbo.DOMICILIOS ON dbo.Abonados.empresa = dbo.DOMICILIOS.Empresa AND dbo.Abonados.Ciudad = dbo.DOMICILIOS.Ciudad AND dbo.Abonados.Domicilio = dbo.DOMICILIOS.Codigo WHERE     (dbo.Abonados.CodAbonado <> 0) AND (dbo.Abonados.empresa = 1) AND (dbo.Abonados.FechaBaja IS NULL) AND (dbo.Contadores.TIPOAPARATO = 1) AND (dbo.Contadores.CLASEAPARATO = 1) AND (dbo.Contadores.EMPRESA = 1)"

	
	conn=pyodbc.connect('DSN='+ strDSN + ';UID=' + strUser + ';PWD=' + strPass)
	cursor = conn.cursor()
	cursor.execute(query)

	for j in range(niter):
		if j==niter-1:
			for i in range(len(qdata)):
				sys.stdout.write(' ')
				sys.stdout.write(str(i+1))
				sys.stdout.write(': ')
				sys.stdout.write(str(qdata[i]))
				sys.stdout.write(';')
			print(" ")
		else:
			qdata=cursor.fetchone()


	cursor.close()
	del cursor
	conn.close()

