#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import os
i = 0
j = 0
NUM_LINES = os.getenv('NUM_LINES', 5000)
CSV_FILE = os.getenv('CSV_FILE', '/tmp/aspirapol5.csv')
READER = csv.reader(open(CSV_FILE), delimiter=';')

CSV_SPLIT = '%s_%i' % (CSV_FILE, i)
WRITER = csv.writer(open(CSV_SPLIT, 'w'), delimiter=';')
for row in READER:
    j += 1
    WRITER.writerow(row)
    if j > 5000:
        j = 0
        i += 1
        CSV_SPLIT = '%s_%i' % (CSV_FILE, i)
        WRITER = csv.writer(open(CSV_SPLIT, 'w'), delimiter=';')