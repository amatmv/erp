#!/usr/bin/env python
# -*- coding: utf-8 -*-
from loop import OOOP

o = OOOP(dbname='oerp5_distri_fact', user='root', pwd='root', port=8051)
for factura in o.GiscedataFacturacioFactura.all():
    taxes_calc = round(factura.amount_untaxed * 0.18, 2)
    if taxes_calc != factura.amount_tax:
        print(u"- Factura de la pòlissa %s incorrecte: %s - %s" %
              (factura.polissa.name, taxes_calc, factura.amount_tax))