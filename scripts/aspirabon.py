#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Script per crear els abonats al sistema de la comercialitzadora
"""

import optparse
import xmlrpclib
import sys
import time
import os

parser = optparse.OptionParser(version="[GISCE ERP] Migrations")

group = optparse.OptionGroup(parser, "Servidor distribuidora")
group.add_option("--host-dist", dest="host_dist", help="specify the database host") 
group.add_option("--port-dist", dest="port_dist", help="specify the database port") 
group.add_option("--database-dist", dest="dbname_dist", help="specify the database name")
group.add_option("--user-dist", dest="user_dist", help="specify the username")
group.add_option("--password-dist", dest="password_dist", help="specify the password")
parser.add_option_group(group)

group2 = optparse.OptionGroup(parser, "Servidor comercialitzadora")
group2.add_option("--host-comer", dest="host_comer", help="specify the database host") 
group2.add_option("--port-comer", dest="port_comer", help="specify the database port") 
group2.add_option("--database-comer", dest="dbname_comer", help="specify the database name")
group2.add_option("--user-comer", dest="user_comer", help="specify the username")
group2.add_option("--password-comer", dest="password_comer", help="specify the password")
parser.add_option_group(group2)


options = optparse.Values()
options.host_dist = 'localhost'
options.port_dist = 18069
options.user_dist = 'admin'
options.host_comer = 'localhost'
options.port_comer = 8069
options.user_comer = 'admin'
parser.parse_args(values=options)

host_dist = options.host_dist
port_dist = options.port_dist
dbname_dist = options.dbname_dist
user_dist = options.user_dist
pwd_dist = options.password_dist

host_comer = options.host_comer
port_comer = options.port_comer
dbname_comer = options.dbname_comer
user_comer = options.user_comer
pwd_comer = options.password_comer


def pout(msg, flush=True):
    sys.stdout.write(msg)
    if flush:
        sys.stdout.flush()
    
def perr(msg, flush=True):
    try:
        sys.stderr.write(msg)
    except:
        pass
    if flush:
        sys.stderr.flush()
        
def crc(txt):
    factor=(1, 2, 4, 8, 5, 10, 9, 7, 3, 6)
    nValor= 11 - reduce(int.__add__, [int(x) * y for x, y in zip(txt, factor)]) % 11    
    if nValor == 10: nValor = 1
    elif nValor == 11: nValor = 0
    return nValor

def calc_cc(co ,cc):
    return "%1d%1d" % (crc(co), crc(cc))

# Connexió amb la distribuidora
sock_dist = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/common' % (host_dist, int(port_dist)))
uid_dist = sock_dist.login(dbname_dist, user_dist, pwd_dist)
sock_dist = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/object' % (host_dist, int(port_dist)))

# Connexió amb la comercialitzadora
sock_comer = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/common' % (host_comer, int(port_comer)))
uid_comer = sock_comer.login(dbname_comer, user_comer, pwd_comer)
sock_comer = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/object' % (host_comer, int(port_comer)))

# Connexió amb la comercialitzadora (wizard)
wsock_comer = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/wizard' % (host_comer, int(port_comer)))

try:
    ts_inici = time.strftime('%d/%m/%Y %H:%M:%S')
    # Ens connectem a la distribuidora
    # 1. Busquem les dades de la nostra distribuidora per crear-la a la distribuidora
    c_ids = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.company', 'search', [])
    if len(c_ids) > 1:
      # More than one company ask the user select one
      for company in sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.company', 'read', c_ids, ['name', 'partner_id']):
          print "  %s - %s" % (company['id'], company['name'])
      pout('Escollir empresa - ID: ')
      c_id = raw_input()
      c_id = int(c_id)
    else:
        c_id = c_ids[0] 
    
    distri = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.company', 'read', [c_id], ['name', 'partner_id'])[0]
    print '[D] Utilitzant "%s" com a distribuidora' % distri['name']
    
    # 2. Busquem la comercialitzadora
    c_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.company', 'search', [])
    if len(c_ids) > 1:
      # More than one company ask the user select one
      for company in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.company', 'read', c_ids, ['name', 'partner_id']):
          print "  %i - %s" % (company['id'], company['name'])
      pout("Escollir empresa - ID: ")
      c_id = raw_input()
      c_id = int(c_id)
    else:
        c_id = c_ids[0]
        
    comer = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.company', 'read', [c_id], ['name', 'partner_id'])[0]
    print '[C] Utilitzant "%s" com a comercialitzadora' % comer['name']
    
    # 3. Busquem si existex la comercialitzadora a la bbdd de la distribuidora
    print '[D] Buscant comercialitzadora "%s" en la bbdd de la distribuidora' % (comer['name'])
    c_ids = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.partner', 'search', [('name', '=', comer['name'])])
    if not c_ids or len(c_ids) > 1:
        # No hi ha concidencia exacte, fem escollir l'usuari
        print '[D] No hi ha concidencia exacte, obtenint llistat de comercialitzadores ...'
        ids = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.polissa', 'search', [])
        comers = {}
        for p in sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.polissa', 'read', ids, ['comercialitzadora']):
            if not p['comercialitzadora']:
                # TODO: Enregitrar l'error
                continue
            if not comers.has_key(p['comercialitzadora'][0]):
                comers[p['comercialitzadora'][0]] = p['comercialitzadora'][1]
        for k,v in comers.items():
            print '  %i - %s' % (k, v)
        pout("Escollir comercialitzadora - ID: ")
        c_id = raw_input()
        c_id = int(c_id)
    else:
        c_id = c_ids[0]
    
    comer = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.partner', 'read', [c_id], ['name'])[0]
    
    # 4. Comprovar que existeix la distribuidora a la comercialitzadora
    print '[C] Buscant la distribuidora "%s" en la bbdd de la comercialitzadora' % (distri['name'])
    d_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner', 'search', [('name', '=', distri['name'])])
    if not d_ids or len(d_ids) > 1:
        # No hi ha concidencia exacte, fem escollir l'usuari
        print '[C] No hi ha concidencia exacte, obtenint llistat de distribuidores ...'
        ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.cups.ps', 'search', [])
        distris = {}
        for c in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.cups.ps', 'read', ids, ['distribuidora']):
            if not c['distribuidora']:
                # TODO: Enregitrar l'error
                continue
            if not distris.has_key(c['distribuidora'][0]):
                distris[c['distribuidora'][0]] = c['distribuidora'][1]
            print '  0 - Crear la distribuidora'
            for k,v in distris.items():
                print '  %i - %s' % (k, v)
        pout("Escollir distribuidora - ID: ")
        d_id = raw_input()
        d_id = int(d_id)
        if d_id == 0:
            # Hem de crear la distribuidora
            vals = distri.copy()
            vals['supplier'] = 1
            d_id = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner', 'create', vals)
            print '[C] Distribuidora "%s" creada' % (vals['name'])
    else:
        d_id = d_ids[0]
    # Pre-carrega de municipis
    pout('[D] Recuperant municipis ... ')
    municipis_dist = {}
    ids = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.municipi', 'search', [])
    for m in sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.municipi', 'read', ids, ['ine']):
        municipis_dist[m['id']] = m['ine']
    pout('OK\n')
    pout('[C] Recuperant municipis ... ')
    municipis_comer = {}
    ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.municipi', 'search', [])
    for m in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.municipi', 'read', ids, ['ine']):
        municipis_comer[m['ine']] = m['id']
    pout('OK\n')
    # Pre-carrega de provincies
    pout('[D] Recuperant provincies ... ')
    provincies_dist = {}
    ids = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.country.state', 'search', [])
    for p in sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.country.state', 'read', ids, ['code']):
        provincies_dist[p['id']] = p['code']
    pout('OK\n')
    pout('[C] Recuperant provincies ... ')
    provincies_comer = {}
    ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.country.state', 'search', [])
    for p in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.country.state', 'read', ids, ['code']):
        provincies_comer[p['code']] = p['id']
    pout('OK\n')
    # Pre-carrega de paisos
    pout('[D] Recuperant paisos ... ')
    paisos_dist = {}
    ids = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.country', 'search', [])
    for p in sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.country', 'read', ids, ['code']):
        paisos_dist[p['id']] = p['code']
    pout('OK\n')
    pout('[C] Recuperant paisos ... ')
    paisos_comer = {}
    ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.country', 'search', [])
    for p in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.country', 'read', ids, ['code']):
        paisos_comer[p['code']] = p['id']
    pout('OK\n')
    # Pre-carrega de tarifes
    pout('[D] Recuperant tarifes ... ')
    tarifes_dist = {}
    ids = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.polissa.tarifa', 'search', [])
    for p in sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.polissa.tarifa', 'read', ids, ['name']):
        tarifes_dist[p['id']] = p['name']
    pout('OK\n')
    pout('[C] Recuperant tarifes ... ')
    tarifes_comer = {}
    ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.polissa.tarifa', 'search', [])
    for p in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.polissa.tarifa', 'read', ids, ['name']):
        tarifes_comer[p['name']] = p['id']
    pout('OK\n')
    pout('[C] Recuperant tipus pagament ... ')
    tipo_pago_comer = {}
    ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'payment.type', 'search', [])
    for p in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'payment.type', 'read', ids, ['code']):
        tipo_pago_comer[p['code']] = p['id']
    pout('OK\n')
    pout('[C] Recuperant llistes de preus ... ')
    llista_preus_comer = {}
    ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'product.pricelist', 'search', [('type', '=', 'sale')])
    for p in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'product.pricelist', 'read', ids, ['name']):
        llista_preus_comer[p['name']] = p['id']
    pout('OK\n')
    pout('[C] Recuperant bancs ... ')
    banks_comer = {}
    ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.bank', 'search', [('country.code', '=', 'ES')])
    for p in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.bank', 'read', ids, ['code']):
        banks_comer[p['id']] = p['code']
    pout('OK\n')
    pout('[C] Recuperant tensions normalitzades ... ')
    tensions_comer = {}
    ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.tensions.tensio', 'search', [])
    for p in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.tensions.tensio', 'read', ids, ['name']):
        tensions_comer[p['name']] = p['id']
    pout('OK\n')
    pout('[C] Recuperant CNAEs ... ')
    cnaes_comer = {}
    ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscemisc.cnae', 'search', [])
    for p in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscemisc.cnae', 'read', ids, ['name']):
        cnaes_comer[p['name']] = p['id']
    pout('OK\n')
    pout("[C] Recuperant productes de tarifa...")
    periodes_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.polissa.tarifa.periodes', 'search', [])
    productes = {}
    for periode in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.polissa.tarifa.periodes', 'read', periodes_ids):
        productes.setdefault(periode['tarifa'][1], {})
        productes[periode['tarifa'][1]].setdefault(periode['tipus'], {})
        productes[periode['tarifa'][1]][periode['tipus']][periode['name']] = periode['product_id'][0]
    pout("OK\n")
    pout("[C] Recuperant productes de lectures...")
    periodes_tarifes = {}
    for periode in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.polissa.tarifa.periodes', 'read', periodes_ids):
        periodes_tarifes.setdefault(periode['tarifa'][1], {})
        periodes_tarifes[periode['tarifa'][1]].setdefault(periode['tipus'], {})
        periodes_tarifes[periode['tarifa'][1]][periode['tipus']][periode['name']] = periode['id']
    pout("OK\n")
    pout("[C] Recuperant incidències de lectures...")
    incidencies_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.incidencies.incidencia', 'search', [])
    incidencies = {}
    for incidencia in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.incidencies.incidencia', 'read', incidencies_ids):
        incidencies[incidencia['name']] = incidencia['id']
    pout("OK\n")
    pout("[C] Recuperant orígens de lectures...")
    origens_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.origen', 'search', [])
    origens = {}
    for origen in sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.origen', 'read', origens_ids):
        origens[origen['name']] = origen['id']
    pout("OK\n")
    # 5. Obtenir polisses de la comercialitzadora
    print '[D] Obtenint llistat de polisses per la comercialitzadora "%s"' % (comer['name'])
    search_params = [
        ('comercialitzadora', '=', comer['id']),
    ]
    offset = int(os.getenv('OFFSET', 0))
    limit = int(os.getenv('LIMIT', 0))
    ids = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.polissa', 'search', search_params, offset, limit)
    print '[D] %i polisses en total' % len(ids)
    i = 0 # index
    t = len(ids) # total
    # percentatge
    per = 0
    # 6. Procés de creació
    cu = 0
    cc = 0
    ce = 0
    pu = 0
    pc = 0
    pe = 0
    pou = 0
    poc = 0
    poe = 0
    pout('[D] Recuperant dades de les polisses ... ')
    polisses = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.polissa', 'read', ids, ['name', 'cups', 'partner_id', 'comptadors'])
    pout('OK\n')
    print '[C] Actualitzant dades ... '
    for p in polisses:
        i += 1
        per = int(round(((i * 1.0) / t) * 100))
        msg = "CUPS => [U: %s | C: %s | E: %s] PARTNERS => [U: %s | C: %s | E: %s] POLISSES => [U: %s | C: %s | E: %s] (%%s%%%%)" % tuple(map(lambda x: str.rjust(str(x), len(str(t))), (cu, cc, ce, pu, pc, pe, pou, poc, poe)))
        msg = msg % str.rjust(str(per), 3)
        pout(msg)
        del_msg = '\b'*len(msg)
        pout(del_msg)
        ##################################################################
        #  CUPS
        ##################################################################
        if not p['cups']:
            perr('CUPS: polissa %s no te cups assignat\n' % p['name'])
            ce += 1
            continue
        cups = p['cups'][1]
        cups_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.cups.ps', 'search', [('name', '=', cups)], 0, False, False, {'active_test': False})
        # Llegim els valors de la distribuidora
        mapping_cups = {
          'name': 'name',
          'id_municipi': 'id_municipi',
          'carrer': 'nv',
          'numero': 'pnp', 
          'escala': 'es',
          'pis': 'pt', 
          'porta': 'pu', 
          'ref_catastral': 'ref_catastral', 
          'poligon': 'cpo', 
          'parcela': 'cpa',
        }
        cups = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.cups.ps', 'read', [p['cups'][0]], mapping_cups.keys())[0]
        vals = {}
        for k,v in mapping_cups.items():
            if isinstance(cups[k], str):
                cups[k] = unicode(cups[k], "utf-8")
            vals[v] = cups[k]
        # Substituim el municipi per l'id de la comercialitzadora fent
        # Agafem l'ine de la dist
        ine = municipis_dist[cups['id_municipi'][0]]
        # Agafem l'id de la comercialitzadora per aquest ine
        vals['id_municipi'] = municipis_comer[ine]
        # Afegim la distribuidora
        vals['distribuidora_id'] = d_id
        # Sempre l'activem
        vals['active'] = 1
        if len(cups_ids):
            # Actualitzem
            sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.cups.ps', 'write', cups_ids, vals)
            cups_id = cups_ids[0]
            cu += 1
        else:
            # Creem
            try:
                cups_id = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.cups.ps', 'create', vals)
                cc += 1
            except xmlrpclib.Fault, err:
                perr(err.faultString)
                ce += 1
                continue
                
        # Si el cups té referencia catastral provem d'obtenir l'adreça
        if cups['ref_catastral'] and cups['id_municipi']:
            sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.cups.ps', 'update_dir_from_ref', [cups_id])
        
        ##################################################################
        # PARTNERS
        ##################################################################
        # TODO: Crear la compte del bank
        if not p['partner_id']:
            perr('PARTNER: polissa %s no te partner associat\n' % p['name'])
            pe += 1
            continue
        mapping_partner = {
            'name': 'name',
            'lang': 'lang',
            'vat': 'vat',
            'address': 'address'
        }
        partner = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.partner', 'read', [p['partner_id'][0]], mapping_partner.keys())[0]
        search_params = [('name', '=', partner['name'])]
        if partner.get('vat', False) and len(partner['vat']) > 1:
            partner['vat'] = 'ES%s' % partner['vat'].replace('-', '')
            search_params.append(('vat', '=', partner['vat']))
        else:
            partner['vat'] = False
        partner_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner', 'search', search_params, 0, False, False, {'active_test': False})
        vals = {}
        for k,v in mapping_partner.items():
            vals[v] = partner[k]
        # No posem address
        del vals['address']
        # El marquem com a client
        vals['customer'] =  1
        # tipus de pagament
        vals['payment_type_customer'] = tipo_pago_comer['RECIBO_CSB']
        # Posem el pais
        if len(partner_ids):
            # Actualitzem
            try:
                sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner', 'write', partner_ids, vals)
            except xmlrpclib.Fault, e:
                perr('%s: %s\n' % (e.faultCode, vals))
                pe += 1
                continue
            partner_id = partner_ids[0]
            pu += 1
        else:
            # Creem
            try:
                partner_id = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner', 'create', vals)
            except xmlrpclib.Fault, e:
                vals['vat'] = False
                partner_id = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner', 'create', vals)
            pc += 1
        
        # Creem les contes contables (Millorar, guardar en un array i cada N executar l'assistent)
        wiz_id = wsock_comer.create(dbname_comer, uid_comer, pwd_comer, 'l10n_ES_partner_seq.create_accounts')
        params = {
            'ids': [partner_id]
        }
        res = wsock_comer.execute(dbname_comer, uid_comer, pwd_comer, wiz_id, params, 'init')
        params['form'] = {
            'parent_receivable': '430',
            'parent_payable': '410',
        }
        params['form'].update(res.get('datas', {}))
        wsock_comer.execute(dbname_comer, uid_comer, pwd_comer, wiz_id, params, 'create')
        ##################################################################
        # ADDRESS
        ##################################################################
        if not partner['address']:
            vals = {'name': partner['name'],
                    'type': 'invoice',
                    'partner_id': partner_id}
            partner_address_id = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner.address', 'create', vals)
        else:
            mapping_partner_address = {
                'name': 'name',
                'partner_id': 'partner_id',
                'street': 'street',
                'city': 'city',
                'zip': 'zip',
                'country_id': 'country_id',
                'state_id': 'state_id',
            }
            # Eliminem totes les direccions del partner i les re creem totes
            partner_address_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner.address', 'search', [('partner_id', '=', partner_id)], 0, False, False, {'active_test': False})
            if id in partner_address_ids:
                try:
                    sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner.address', 'unlink', [id])
                except xmlrpclib.Fault:
                    pass
            for partner_address in sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'res.partner.address', 'read', partner['address'], mapping_partner_address.keys()):
                vals = {}
                for k,v in mapping_partner_address.items():
                    vals[v] = partner_address[k]
                # Posem per defecte type = 'invoice'
                vals['type'] = 'invoice'
                # Posem correctament la provincia i el pais
                if vals['state_id']:
                    codi_provincia = provincies_dist[vals['state_id'][0]]
                    vals['state_id'] = provincies_comer[codi_provincia]
                if vals['country_id']:
                    codi_pais = paisos_dist[vals['country_id'][0]]
                    vals['country_id'] = paisos_comer[codi_pais]
                # Posem el partner_id de la comercialitzadora
                vals['partner_id'] = partner_id
                # L'última partner_address_id és la que ficarem a les pòlisses
                partner_address_id = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner.address', 'create', vals)
        
        # Obtenim les dades del pagador
        address = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner.address', 'read', [partner_address_id])[0]
        # Amb l'última partner_address creem la compte bancària
        # Fem un random dels bancs que tinguem
        index = i%len(banks_comer.keys())
        bank_id = banks_comer.keys()[index]
        bank_code = banks_comer[bank_id]
        bank_sucursal = '0001'
        bank_cc = str(partner_id).zfill(10)
        bank_dc = calc_cc('00%s%s' % (bank_code, bank_sucursal), bank_cc)
        acc_number = '%s %s %s %s' % (bank_code, bank_sucursal, bank_dc, bank_cc)
        vals = {
            'bank': bank_id,
            'acc_number': acc_number,
            'owner_name':  address['name'],
            'street': address['street'],
            'zip': address['zip'],
            'city': address['city'],
            'acc_country_id': address.get('acc_country_id', False) and address['country_id'][0] or False,
            'country_id': address.get('country_id', False) and address['country_id'][0],
            'state_id': address.get('state_id', False) and address['state_id'][0],
            'partner_id': address['partner_id'][0],
            'state': 'bank',
            'sequence': 1,
        }
        # Comprovem si existeix ja
        search_params = [
            ('acc_number', '=', acc_number)
        ]
        bank_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner.bank', 'search', search_params)
        if bank_ids:
            sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner.bank', 'write', bank_ids, vals)
            bank_id = bank_ids[0]
        else:
            bank_id = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'res.partner.bank', 'create', vals)
        ##################################################################
        # POLISSES
        ##################################################################
        if not p['name']:
            perr('POLISSA: id:%i no te codi\n' % p['id'])
            continue
        mapping_polissa = {
            'name': 'ref_dist',
            'potencia': 'potencia',
            'tensio': 'tensio_normalitzada',
            'data_alta': 'data_alta',
            'facturacio': 'facturacio',
            'tarifa': 'tarifa',
            'proxima_facturacio': 'lot_facturacio',
            'cnae': 'cnae'
        }
        polissa = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.polissa', 'read', [p['id']], mapping_polissa.keys())[0]
        vals = {}
        for k,v in mapping_polissa.items():
            vals[v] = polissa[k]
        # Posem el cups
        vals['cups'] = cups_id
        # Sempre active
        vals['active'] = 1
        # Posem la tarifa correcte
        tarifa_codi = tarifes_dist[polissa['tarifa'][0]]
        vals['tarifa'] = tarifes_comer[tarifa_codi]
        vals['distribuidora'] = d_id
        vals['cnae'] = polissa.get('cnae', False) and cnaes_comer.get(polissa['cnae'][1], False)
        if not vals['data_alta']:
            vals['data_alta'] = time.strftime('%Y-%m-%d')
        polissa['tensio'] = str(polissa['tensio']).lower()
        try:
            vals['tensio_normalitzada'] = tensions_comer[polissa['tensio']]
        except:
            if polissa['tensio'].find('x') > -1:
                vals['tensio_normalitzada'] = tensions_comer.get(polissa['tensio'].split('x')[1], tensions_comer['230'])
            else:
                vals['tensio_normalitzada'] = tensions_comer['230']
                #perr('POLISSA: %s tensio no normalitzada %s\n' % (p['name'], polissa['tensio']))
                #poe += 1
        vals['bank'] = bank_id
        vals['titular'] = partner_id
        vals['pagador'] = partner_id
        vals['direccio_pagament'] = partner_address_id
        vals['notificacio'] = 'titular'
        vals['direccio_notificacio'] = partner_address_id
        # Posem a totes tipo_pago domicialitzat code:RECIBO_CSB
        vals['tipo_pago'] = tipo_pago_comer['RECIBO_CSB']
        # Posem la llista de preus
        # si existeix una amb el mateix nom que la tarifa d'acces hi posem aquesta
        if llista_preus_comer.get(tarifa_codi, False):
            vals['llista_preu'] = llista_preus_comer[tarifa_codi]
        # si no posem la per defecte: Public Pricelist
        else:
            vals['llista_preu'] = llista_preus_comer['Public Pricelist']
        # Posem la versio per la primera factura
        if not polissa['proxima_facturacio']:
            perr('POLISSA: La polissa %s no te el camp proxima facturacio\n' % polissa['name'])
            poe += 1
            continue
        # Busquem el lot de facturació
        search_params = [
            ('data_inici', '<=', polissa['proxima_facturacio']),
            ('data_final', '>=', polissa['proxima_facturacio'])
        ]
        v = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.facturacio.lot', 'search', search_params)
        vals['lot_facturacio'] = v[0]
        vals['proxima_facturacio'] = polissa['proxima_facturacio']
        # Posem la primera versio que trobem
        search_params = [
            ('pricelist_id', '=', vals['llista_preu']),
        ]
        if vals['llista_preu'] != llista_preus_comer['Public Pricelist']:
            search_params.extend([
                ('date_start', '<=', vals['proxima_facturacio']),
                ('date_end', '>=', vals['proxima_facturacio'])
            ])
        v = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'product.pricelist.version', 'search', search_params)
        if not v:
            perr('POLISSA: Polissa %s versio per la tarifa %s i proxima facturacio %s no trobada\n' % (polissa['name'], vals['llista_preu'], vals['proxima_facturacio']))
            poe += 1
            continue
        vals['versio_primera_factura'] = v[0] 
        # Busquem si ja existeix la polissa
        search_params = [
            ('ref_dist', '=', p['name'])
        ]
        polissa_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.polissa', 'search', search_params, 0, False, False, {'active_test': False})
        if polissa_ids:
            sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.polissa', 'write', polissa_ids, vals)
            # Actualitzem
            polissa_id = polissa_ids[0]
            pou += 1
        else:
            # Creem
            polissa_id = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.polissa', 'create', vals)
            poc += 1
        # Actualitzem les potencies
        sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.polissa', 'generar_periodes_potencia', [polissa_id])
        
        ##################################################################
        # COMPTADOR
        ##################################################################
        if not p['comptadors']:
            perr('COMPTADOR: La polissa %s no te cap comptador\n' % p['name'])
            poe = +1
            continue
        
        mapping_comptador = {
          'name': 'name',
          'lloguer': 'lloguer',
          'data_alta': 'data_alta',
          'data_baixa': 'data_baixa',
          'active': 'active',
          'giro': 'giro',
        }
        for comptador in sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.lectures.comptador', 'read', p['comptadors'], mapping_comptador.keys()):
            vals = {}
            for k,v in mapping_comptador.items():
                vals[v] = comptador[k]
            # Posem el número de sèrie
            vals['name'] = comptador['name'][1]
            # Assignem la pòlissa
            vals['polissa'] = polissa_id
            
            # Comprovem si ja existeix
            search_params = [
                ('name', '=', vals['name']),
                ('polissa', '=', vals['polissa'])
            ]
            comptadors_ids = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.comptador', 'search', search_params, 0, False, False, {'active_test': False})
            if comptadors_ids:
                sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.comptador', 'write', comptadors_ids, vals)
            else:
                sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.comptador', 'create', vals)
        
            ####
            # LECTURES
            ####
            # lectures_fields = ['name', 'periode', 'lectura', 'tipus', 'comptador', 'incidencia_id', 'observacions', 'origen_id']
            comptador_comer_id = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.comptador', 'search', [('name', '=', vals['name'])])
            origen_comer_id = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.origen', 'search', [('codi', '=', '30')])[0]
            if len(comptador_comer_id):
                comptador_comer_id = comptador_comer_id[0]
                ## ENERGIA    
                lectures_dist = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.lectures.lectura', 'search', [('comptador', '=', comptador['id'])], 0, False, "name asc")
                lectures_mapping = {
                    'name': 'name',
                    'observacions': 'observacions',
                    'lectura': 'lectura',
                    'tipus': 'tipus',
                }
                search_params = [
                    ('comptador.id', '=', comptador_comer_id)
                ]
                lectures_to_del = sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.lectura', 'search', search_params)
                sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.lectura', 'unlink', lectures_to_del)
                for lectura_dist in sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.lectures.lectura', 'read', lectures_dist):
                    # {'hora': '12:00:00', 'periode': [1, '2.0A (P1)'], 'name': '2009-12-31', 
                    # 'observacions': 'Datos importados', 'lectura_incremental': 0.0, 
                    # 'lectura': 0, 'incidencia_id': False, 
                    # 'comptador': [2259, '208013'], 'id': 37862, 'tipus': 'R'}
                    cvals = {}
                    cvals['comptador'] = comptador_comer_id
                    for k,v in lectures_mapping.items():
                        cvals[v] = lectura_dist[k]
                    nom_periode = lectura_dist['periode'][1].split()[1].split('(')[1].split(')')[0] # ugly!!
                    cvals['periode'] = periodes_tarifes[tarifa_codi]['te'][nom_periode]
                    cvals['origen_id'] = origen_comer_id
                    sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.lectura', 'create', cvals)
                ## POTENCIA
                lects_potencia = sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.lectures.potencia', 'search', [('comptador', '=', comptador['id'])])
                lectures_potencia_mapping = {
                    'name': 'name',
                    'observacions': 'observacions',
                    'lectura': 'lectura',
                    'exces': 'exces',
                }
                for lect_potencia in sock_dist.execute(dbname_dist, uid_dist, pwd_dist, 'giscedata.lectures.potencia', 'read', lects_potencia):
                    cvals = {}
                    cvals['comptador'] = comptador_comer_id
                    for k,v in lectures_potencia_mapping.items():
                        cvals[v] = lect_potencia[k]
                    nom_periode = lect_potencia['periode'][1].split()[1].split('(')[1].split(')')[0] # ugly!!
                    cvals['periode'] = periodes_tarifes[tarifa_codi]['tp'][nom_periode]
                    sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.lectures.potencia', 'create', cvals)
            else:
                perr('El comptador %s no existeix a la comercialitzadora' % (vals['name']))
            
                
        # TODO: Activar finalment la pòlissa, això ho hem de fer amb workflow, sino crear una funcio
        # al model que faci la transició.
        sock_comer.execute(dbname_comer, uid_comer, pwd_comer, 'giscedata.polissa', 'activar_xmlrpc', [polissa_id])
    ts_final = time.strftime('%d/%m/%Y %H:%M:%S')
    print
    print
    print "*********** RESUM ***********"
    print "Proc. iniciat: %s" % ts_inici
    print "Proc. finalitzat: %s" % ts_final
    print "-----------------------------"
    print "CUPS creats: %i" % cc
    print "CUPS actualitzats: %i" % cu
    print "CUPS Total: %i (%i errors)" % (cc+cu, ce)
    print "-----------------------------"
    print "PARTNERS creats: %i" % pc
    print "PARTNERS actualitzats: %i" % pu
    print "PARTNERS Total: %i (%i errors)" % (pc+pu, pe)
    print "-----------------------------"
    print "POLISSES creades: %i" % poc
    print "POLISSES actualitzades: %i" % pou
    print "POLISSES Total: %i (%i errors)" % (poc+pou, poe)
    print
    print "********* END RESUM *********"
    print
except KeyboardInterrupt:
    print
    print 'Shutting Down...'
