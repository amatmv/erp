#!/usr/bin/env python
# -*- coding: utf-8 -*-
from loop import OOOP

o = OOOP(dbname='oerp5_distri_fact', user='root', pwd='root', port=8051)
FACTURES = 0

for polissa in o.GiscedataPolissa.filter(facturacio=1,
                                    proxima_facturacio='2011-01-31',
                                    active='True',
                                    state__not_in=('esborrany', 'validar')):
    facturador = o.GiscedataFacturacioFacturador
    lot_id = polissa.lot_facturacio.id
    print u'- Facturant pòlissa: %s - %s' % (polissa.name,
                                             polissa.titular.name)
    facturador.fact_via_lectures(polissa.id, lot_id)
    FACTURES += 1

print "**** Total: %i factures creades" % FACTURES
