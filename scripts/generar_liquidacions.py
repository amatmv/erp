#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import ConfigParser
import optparse
import os
import sys
from loop import OOOP

__version__ = 0.1

def run(argv):
    """Mètode principal
    """
    parser = optparse.OptionParser(usage="%prog [OPTIONS]", version=__version__)
    parser.add_option('--file', '-f', dest='file', default='liquidacions.csv',
                      help='The file to write the output to')
    parser.add_option('--database', '-d', dest='database',
                      help='Database')
    parser.add_option('--user', '-u', dest='user', default="root", help="User")
    parser.add_option('--password', '-w', dest='password', default="root", 
                      help="Password")
    parser.add_option('--host', '-H', dest='host', default="localhost",
                      help="Host")
    parser.add_option('--port', '-p', dest='port', default=8059, help="Port")
    parser.add_option('--period', '-e', dest='period', help="Period ID")

    (options, args) = parser.parse_args()

    if not options.database:
        parser.error("The --database/-d option is mandatory")
    if not options.period:
        parser.error("The --period/-e option is mandatory")

    sign = {'N': 1, 'R': 1, 'A': -1, 'B': -1}
    liquidacio = {}
    clients = {}
    default_dict = {
        'n_clients': 0,
        'pc_ph1': 0,
        'pc_ph2': 0,
        'pc_ph3': 0,
        'pc_ph4': 0,
        'pc_ph5': 0,
        'pc_ph6': 0,
        'pf_ph1': 0,
        'pf_ph2': 0,
        'pf_ph3': 0,
        'e_ph1': 0,
        'e_ph2': 0,
        'e_ph3': 0,
        'e_ph4': 0,
        'e_ph5': 0,
        'e_ph6': 0,
        'energia_facturada': 0,
        'facturacio_potencia': 0.0,
        'facturacio_energia': 0.0,
        'facturacio_energia_r': 0.0,
        'facturacio_excessos_pot': 0.0,
        'total_facturacio': 0.0,
    }
    O = OOOP(dbname=options.database, user=options.user, pwd=options.password,
             port=int(options.port))

    search_params = [('periode_liquidacio.id', '=', options.period)]
    factures = O.GiscedataFacturacioFactura.search(search_params)
    print(">> %s Factures" % len(factures))
    for factura_id in factures:
        factura = O.GiscedataFacturacioFactura.get(factura_id)
        tarifa = factura.tarifa_acces_id
        liquidacio.setdefault(tarifa.name, default_dict.copy())
        clients.setdefault(tarifa.name, [])
        sign_ = sign[factura.tipo_rectificadora]
        if factura.polissa_id.id not in clients[tarifa.name] and \
            factura.tipo_rectificadora in ('N'):
            clients[tarifa.name].append(factura.polissa_id.id)
            liquidacio[tarifa.name]['n_clients'] += 1
        for p_cont in factura.lectures_potencia_ids:
            ph = int(p_cont.name[-1])
            if ph > 3 and tarifa.name in ('3.0A', '3.1A'):
                continue
            liquidacio[tarifa.name]['pc_ph%s' % ph] += \
                p_cont.pot_contract * sign_
        if not factura.lectures_potencia_ids:
            if tarifa.name in ('2.0A', '2.1A'):
                liquidacio[tarifa.name]['pc_ph1'] += factura.polissa_id.potencia
            elif tarifa.name in ('2.0DHA', '2.1DHA'):
                liquidacio[tarifa.name]['pc_ph1'] += factura.polissa_id.potencia
                liquidacio[tarifa.name]['pc_ph2'] += factura.polissa_id.potencia

        for linia in factura.linia_ids:
            if linia.tipus == 'potencia':
                if tarifa.name in ('3.0A', '3.1A'):
                    ph = int(linia.name[-1])
                    if ph > 3:
                        continue
                    liquidacio[tarifa.name]['pf_ph%s' % ph] += \
                        linia.quantity * sign_
                liquidacio[tarifa.name]['facturacio_potencia'] += \
                    linia.price_subtotal * sign_
            elif linia.tipus == 'energia':
                ph = int(linia.name[-1])
                liquidacio[tarifa.name]['e_ph%s' % ph] += linia.quantity * sign_
                liquidacio[tarifa.name]['energia_facturada'] += \
                    linia.quantity * sign_
                liquidacio[tarifa.name]['facturacio_energia'] += \
                    linia.price_subtotal * sign_
                if tarifa.name in ('2.0A', '2.1A'):
                    liquidacio[tarifa.name]['e_ph%s' % ph] = 0
            elif linia.tipus == 'reactiva':
                liquidacio[tarifa.name]['facturacio_energia_r'] += \
                    linia.price_subtotal * sign_
            elif linia.tipus == 'exces_potencia': 
                liquidacio[tarifa.name]['facturacio_excessos_pot'] += \
                    linia.price_subtotal * sign_
        liquidacio[tarifa.name]['total_facturacio'] = \
            liquidacio[tarifa.name]['facturacio_potencia'] + \
            liquidacio[tarifa.name]['facturacio_energia'] + \
            liquidacio[tarifa.name]['facturacio_excessos_pot'] + \
            liquidacio[tarifa.name]['facturacio_energia_r']
    output = open(options.file, 'w')
    writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE)
    header = [
    'TARIFA',
    'NUMERO CLIENTES',
    'POT CONTRACT PH1',
    'POT CONTRACT PH2',
    'POT CONTRACT PH3',
    'POT CONTRACT PH4',
    'POT CONTRACT PH5',
    'POT CONTRACT PH6',
    'POT FACTURADA PH1',
    'POT FACTURADA PH2',
    'POT FACTURADA PH3',
    'ENERGIA PH1',
    'ENERGIA PH2',
    'ENERGIA PH3',
    'ENERGIA PH4',
    'ENERGIA PH5',
    'ENERGIA PH6',
    'ENERGIA FACTURADA',
    'FACTURACION POTENCIA',
    'FACTURACION ENERGIA',
    'FACTURACION REACTIVA',
    'FACTURACION EXC POT',
    'TOTAL FACTURACION',
    ]
    writer.writerow(header)
    for tarifa in liquidacio.keys():
        line = []
        line.append(tarifa)
        line.append(liquidacio[tarifa]['n_clients'])
        line.append(int(round(liquidacio[tarifa]['pc_ph1'])))
        line.append(int(round(liquidacio[tarifa]['pc_ph2'])))
        line.append(int(round(liquidacio[tarifa]['pc_ph3'])))
        line.append(int(round(liquidacio[tarifa]['pc_ph4'])))
        line.append(int(round(liquidacio[tarifa]['pc_ph5'])))
        line.append(int(round(liquidacio[tarifa]['pc_ph6'])))
        line.append(int(round(liquidacio[tarifa]['pf_ph1'])))
        line.append(int(round(liquidacio[tarifa]['pf_ph2'])))
        line.append(int(round(liquidacio[tarifa]['pf_ph3'])))
        line.append(int(round(liquidacio[tarifa]['e_ph1'])))
        line.append(int(round(liquidacio[tarifa]['e_ph2'])))
        line.append(int(round(liquidacio[tarifa]['e_ph3'])))
        line.append(int(round(liquidacio[tarifa]['e_ph4'])))
        line.append(int(round(liquidacio[tarifa]['e_ph5'])))
        line.append(int(round(liquidacio[tarifa]['e_ph6'])))
        line.append(int(round(liquidacio[tarifa]['energia_facturada'])))
        line.append(round(liquidacio[tarifa]['facturacio_potencia'], 2))
        line.append(round(liquidacio[tarifa]['facturacio_energia'], 2))
        line.append(round(liquidacio[tarifa]['facturacio_energia_r'], 2))
        line.append(round(liquidacio[tarifa]['facturacio_excessos_pot'], 2))
        line.append(round(liquidacio[tarifa]['total_facturacio'], 2))
        writer.writerow(line)
    output.close()
 
if __name__ == '__main__':
    run(sys.argv[1:])
