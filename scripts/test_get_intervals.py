# -*- coding: utf-8 -*-
from ooop import OOOP
from libfacturacioatr import tarifes

o = OOOP(dbname='aspirabon5', port=8050)

TARIFES = {
    '2.0A': tarifes.Tarifa20A,
    '2.1A': tarifes.Tarifa21A,
    '2.0DHA': tarifes.Tarifa20DHA,
    '2.1DHA': tarifes.Tarifa21DHA,
    '3.0A': tarifes.Tarifa30A,
    '3.1A': tarifes.Tarifa31A,
    '3.1A LB': tarifes.Tarifa31ALB,
    '6.1': tarifes.Tarifa61,
    '6.2': tarifes.Tarifa62,
    '6.3': tarifes.Tarifa63,
    '6.4': tarifes.Tarifa64,
    '6.5': tarifes.Tarifa65,
}

polissa = o.GiscedataPolissa.get(7712)
data_inici, data_final = polissa.get_inici_final_a_facturar()
print "Interval a facturar: %s - %s" % (data_inici, data_final) 
intervals = polissa.get_modcontractual_intervals(data_inici, data_final)
for mod_data in sorted(intervals.keys()):
    mod_id = intervals[mod_data]
    modcontractual = o.GiscedataPolissaModcontractual.get(mod_id)
    print "  * Modcontractual: %s (%i): Data inici: %s" % (modcontractual.name,
                                                            mod_id, mod_data)
    tarifa_class = TARIFES[modcontractual.tarifa.name]
    data_inici_periode = max(data_inici, modcontractual.data_inici)
    data_final_periode = min(data_final, modcontractual.data_final)
    print '    -> Facturarem des de %s a %s' % (data_inici_periode,
                                                data_final_periode)
    comptadors_actius = polissa.comptadors_actius(data_inici_periode,
                                                  data_final_periode)
    for comptador_id in comptadors_actius:
        consums = {}
        comptador = o.GiscedataLecturesComptador.get(comptador_id)
        print "    -> Comptador: %s (%s - %s)" % (comptador.name,
                                                comptador.data_alta,
                                                comptador.data_baixa)
        tarifa_id = modcontractual.tarifa.id
        consums['activa'] = comptador.get_consum_per_facturar(tarifa_id, 'A')
        consums['reactiva'] = comptador.get_consum_per_facturar(tarifa_id, 'R')
        pot_contract = modcontractual.get_potencies_dict()
        fact_potencia = modcontractual.facturacio_potencia
        facturador = tarifa_class(consums, {}, data_inici,
                                   data_final,
                                   facturacio=modcontractual.facturacio,
                                   facturacio_potencia=fact_potencia,
                                   potencies_contractades=pot_contract,
                                   data_inici_periode=data_inici_periode,
                                   data_final_periode=data_final_periode)
        facturador.factura_energia()
        facturador.factura_reactiva()
        facturador.factura_potencia()
        facturador.factura_lloguer()
        print facturador.termes
