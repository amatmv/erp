#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from optparse import OptionParser
from subprocess import Popen, PIPE
import sys

import psycopg

# CONFIG VARIABLES
PSQL = '/usr/bin/psql'
PGDUMP = '/usr/bin/pg_dump'
DBNAME4 = "SOLLER_1.12"
DBNAME5 = "lolcatz"
# these columns will not be copied over to the destination tables
DROP_COLUMNS = ('perm_id', 'perm_default')

MOD_TABLES_ORDER = ['base', 'expedients', 'at',
                    'cts', 'bt', 'cne', 'nodes', 'gis_caixes',  
                    'gis_ctat', 'gis_elementsat', 'gis_elementsbt', 
                    'gis_escomeses', 'gis_netsource', 'gis_simulacio',
                    'descarrecs', 'qualitat', 'revisions', 'trafos',
                    'gis_fusibles', 'gis_interruptors', 'gis_maniobrables',
                    'gis_blocs', 'gis_blocs2', 'gis_gis', 'other']
MOD_TABLES = {
    'base': ['res_comarca', 'res_country_state', 'res_municipi',
             'res_autonomia', 'res_tipovia', 'res_config'],
    'expedients': ['giscedata_expedients_expedient', 
                   'giscedata_expedients_projecte',
                   'giscedata_expedients_projecte_tipus',
                   'giscedata_expedients_projecte_planol_tipus',
                   'giscedata_expedients_projecte_planol'],
    'at': ['giscedata_at_plat', 'giscedata_at_aillador', 'giscedata_at_armat',
           'giscedata_at_conductors', 'giscedata_at_familiacables', 
           'giscedata_at_material', 'giscedata_at_tipuscable', 
           'giscedata_at_cables',
           'giscedata_at_installacio', 'giscedata_at_linia',
           'giscedata_at_poste', 'giscedata_at_suport',
           'giscedata_at_historicmesures', 'giscedata_at_suport_aillador',
           'giscedata_at_tipuslinia', 'giscedata_at_tram',
           'giscedata_at_tram_expedient'],
    'cts': ['giscedata_cts_installacio', 'giscedata_cts_tipus', 
            'giscedata_cne_ct_tipus', 'giscedata_cts_subtipus', 
            'giscedata_cts_ventilacio', 'giscedata_cts_zona', 'giscedata_cts',
            'giscedata_cts_sortidesbt', 'giscedata_cts_zona_qualitat',
            'giscedata_cts_mesures', 'giscedata_cts_expedients_rel'],
    'bt': ['giscedata_bt_conductors', 'giscedata_bt_familiacables',
           'giscedata_bt_material', 'giscedata_bt_tipuscable',
           'giscedata_bt_cables', 'giscedata_bt_tipuslinia', 
           'giscedata_bt_element'],
    'cne': ['giscedata_cne_estadistica', 'giscedata_cne_estadistica_crs',
            'giscedata_cne_estadistica_cts', 
            'giscedata_cne_estadistica_cts_cedits', 
            'giscedata_cne_estadistica_lat',
            'giscedata_cne_estadistica_lat_cedits',
            'giscedata_cne_estadistica_crs_trafo',
            'giscedata_cne_facturacio', 'giscedata_cne_facturacio_linia'],
    'nodes': ['giscegis_vertex', 'giscegis_nodes'],
    'gis_caixes': ['giscegis_blocs_caixesbt_blockname', 
                   'giscegis_blocs_caixesbt'],
    'gis_ctat': ['giscegis_blocs_ctat_blockname', 'giscegis_blocs_ctat',],
    'gis_elementsat': ['giscegis_blocs_elementsat_blockname'],
    'gis_elementsbt': ['giscegis_blocs_elementsbt_blockname'],
    'gis_escomeses': ['giscedata_cups_blockname', 'giscedata_cups_escomesa', 
                      'giscegis_blocs_escomeses'],
    'gis_netsource': ['giscegis_blocs_netsource'],
    'gis_simulacio': ['giscegis_configsimulacio_at'],
    'descarrecs': ['giscedata_descarrecs_sollicitud', 
                   'giscedata_descarrecs_descarrec',
                   'giscedata_descarrecs_descarrec_clients',
                   'giscedata_descarrecs_descarrec_installacions',
                   'giscedata_descarrecs_avis_mail',
                   'giscedata_descarrecs_operacions_obertura',
                   'giscedata_descarrecs_operacions_tancament'],
    'qualitat': ['giscedata_qualitat_type', 'giscedata_qualitat_cause',
                 'giscedata_qualitat_origin', 'giscedata_qualitat_incidence',
                 'giscedata_qualitat_span_type', 'giscedata_qualitat_span',
                 'giscedata_qualitat_affected_customer',
                 'giscedata_qualitat_affected_installation',
                 'giscedata_qualitat_install_power_data',
                 'giscedata_qualitat_install_power',
                 'giscedata_qualitat_interrupciobt_anual',
                 'giscedata_qualitat_interrupciobt_anual_municipi',
                 'giscedata_qualitat_interrupciobt_mensual',
                 'giscedata_qualitat_interrupciobt_mensual_municipi',
                 'giscedata_qualitat_mes', 'giscedata_qualitat_niepi_anual',
                 'giscedata_qualitat_niepi_anual_municipi',
                 'giscedata_qualitat_niepi_mensual',
                 'giscedata_qualitat_niepi_mensual_municipi',
                 'giscedata_qualitat_percentil80',
                 'giscedata_qualitat_percentil80_municipi',
                 'giscedata_qualitat_report_niepi_zonal_any',
                 'giscedata_qualitat_report_niepi_zonal_mes',
                 'giscedata_qualitat_report_tiepi_percentil80',
                 'giscedata_qualitat_report_tiepi_zonal_any',
                 'giscedata_qualitat_tiepi_anual',
                 'giscedata_qualitat_tiepi_mensual',
                 'giscedata_qualitat_tiepi_mensual_municipi',
                 'giscedata_qualitat_type',
                 'giscedata_qualitat_report_tiepi_zonal_mes',
                 'giscedata_qualitat_span_cause',
                 'giscedata_qualitat_span_switch',
                 'giscedata_qualitat_tiepi_anual_municipi'],
    'revisions': ['giscedata_revisions_at_defectes',
                  'giscedata_revisions_at_normativa',
                  'giscedata_revisions_at_revisio',
                  'giscedata_revisions_at_tipusdefectes',
                  'giscedata_revisions_at_tipusvaloracio',
                  'giscedata_revisions_bt_defectes',
                  'giscedata_revisions_bt_normativa',
                  'giscedata_revisions_bt_revisio',
                  'giscedata_revisions_bt_tipusdefectes',
                  'giscedata_revisions_bt_tipusvaloracio',
                  'giscedata_revisions_ct_defectes',
                  'giscedata_revisions_ct_normativa',
                  'giscedata_revisions_ct_revisio',
                  'giscedata_revisions_ct_revisio_proves',
                  'giscedata_revisions_ct_revisio_proves_situacio',
                  'giscedata_revisions_cts_equips',
                  'giscedata_revisions_cts_equips_pic',
                  'giscedata_revisions_ct_tipusdefectes',
                  'giscedata_revisions_ct_tipusvaloracio',
                  'giscedata_revisions_equips',
                  'giscedata_revisions_tecnic'],
    'trafos': ['giscedata_transformador_connexio',
               'giscedata_transformador_connexio_situacio',
               'giscedata_transformador_connexioune',
               'giscedata_transformador_estat',
               'giscedata_transformador_historic',
               'giscedata_transformador_incidencia',
               'giscedata_transformador_localitzacio',
               'giscedata_transformador_magatzem',
               'giscedata_transformador_marca',
               'giscedata_transformador_mesures',
               'giscedata_transformador_refrigerant',
               'giscedata_transformador_trafo',
               'giscedata_trieni'],
    'gis_fusibles': ['giscegis_blocs_fusiblesat',
                     'giscegis_blocs_fusiblesat_blockname',
                     'giscegis_blocs_fusiblesbt',
                     'giscegis_blocs_fusiblesbt_blockname',
                     'giscegis_blocs_fusibles_caixes',
                     'giscegis_blocs_fusiblescaixes_blockname'],
    'gis_interruptors': [ 'giscegis_blocs_interruptorat',
                          'giscegis_blocs_interruptorat_blockname',
                          'giscegis_blocs_interruptorbt',
                          'giscegis_blocs_interruptorbt_blockname',],
    'gis_maniobrables': [ 'giscegis_blocs_maniobrables_at',
                          'giscegis_blocs_maniobrables_bt'],
    'gis_blocs': ['giscegis_blocs_netsource_blockname',
                 'giscegis_blocs_seccionadorat',
                 'giscegis_blocs_seccionadorat_blockname',
                 'giscegis_blocs_seccionadorunifilar',
                 'giscegis_blocs_seccionador_unifilar_blockname',
                 'giscegis_blocs_stop_nodes_at',
                 'giscegis_blocs_stop_nodes_bt',
                 'giscegis_blocs_stop_trace',
                 'giscegis_blocs_stoptrace_blockname',
                 'giscegis_blocs_suports_at',
                 'giscegis_blocs_suports_at_blockname',
                 'giscegis_blocs_suports_bt',
                 'giscegis_blocs_suports_bt_blockname',
                 'giscegis_blocs_tensio',
                 'giscegis_blocs_tensio_blockname',
                 'giscegis_blocs_transformador_blockname',
                 'giscegis_blocs_transformador_reductor_blockname',
                 'giscegis_blocs_transformadors',
                 'giscegis_blocs_transformadors_reductors',
                 'giscegis_color_acadmapguide',
                 'giscegis_configdefault_at',
                 'giscegis_configdefault_bt'],
    'gis_blocs2': ['giscegis_configsimulacio_at_oberts',
                     'giscegis_configsimulacio_bt',
                     'giscegis_configsimulacio_bt_oberts',
                     'giscegis_edge',
                     'giscegis_edgetype',
                     'giscegis_elementsat',
                     'giscegis_elementsbt',
                     'giscegis_escomeses_traceability',
                     'giscegis_fibraoptica',
                     'giscegis_fibraoptica_categoria',
                     'giscegis_lbt_traceability',
                     'giscegis_linies_ct',
                     'giscegis_linies_ct_stop',
                     'giscegis_node_bloc_model'],
    'gis_gis': ['giscegis_polyline',
                 'giscegis_polyline_vertex',
                 'giscegis_polyline_vertex_rel',
                 'giscegis_simulacio_at_resultats',
                 'giscegis_simulacio_at_resultats_escomeses',
                 'giscegis_simulacio_at_resultats_transformadors',
                 'giscegis_simulacio_bt_resultats',
                 'giscegis_simulacio_bt_resultats_escomeses',
                 'giscegis_textcelda',
                 'giscegis_txt_cartografia'],
    'other': ['giscedata_industria_estadistica',
             'giscedata_industria_estadistica_year',
             'giscedata_liquidacio_curs',
             'giscedata_liquidacio_dh',
             'giscedata_liquidacio_fpa',
             'giscedata_liquidacio_fpa_mesos',
             'giscedata_liquidacio_fpd',
             'giscedata_liquidacio_fpd_tarifa',
             'giscedata_liquidacio_ftd',
             'giscedata_liquidacio_ftd_tarifa',
             'giscedata_liquidacio_rfur',
             'giscedata_liquidacio_rfur_cur',
             'giscedata_liquidacio_tarifes'],
}

FILE4 = '/home/marcos/sql/export.sql'
FILE5 = '/home/marcos/sql/import.sql'
FP4 = open(FILE4, 'w')
FP5 = open(FILE5, 'w')

# END of CONFIG variables

def pout(msg, flush=True):
    sys.stdout.write(msg)
    if flush:
        sys.stdout.flush()

def perr(msg, flush=True):
    try:
        sys.stderr.write(msg)
        if flush:
            sys.stderr.flush()
    except:
        pass

def prepare():
    """Writes the SQL statements needed before inserting data.
    """
    FP5.write('BEGIN;\n')
    FP5.write('SET CONSTRAINTS ALL DEFERRED;\n')
    FP5.write('SET SESSION session_replication_role = replica;\n')
    FP5.write('ALTER TABLE giscedata_bt_element ALTER COLUMN municipi '
              'DROP NOT NULL;\n')
    FP5.write('ALTER TABLE giscedata_descarrecs_sollicitud ALTER COLUMN '
              'descripcio DROP NOT NULL;\n')
    FP5.flush()


def finalize():
    """Executes commands after dumping SQL instructions.
    """
    # Update ir_sequence
    cr4.execute("SELECT code,number_next FROM ir_sequence "
                "WHERE code ilike 'gisce%'")
    for seq in cr4.fetchall():
        FP5.write("UPDATE ir_sequence SET number_next=%s WHERE code='%s';\n" % (
                                                                seq[1], seq[0]))

    FP5.write('COMMIT;\n')

def _migrate_table(table, fields, condition='', tuples_only=True):
    """Method for dumping TABLES from DBNAME4 to DBNAME5
    
    :param condition: string with the SQL condition (MUST start with 
                      WHERE clause)
    :param tuples_only: boolean value to indicate if only data has to be
                        migrated. If False, CREATE TABLE statement will be
                        added to the migration query.
    """
    update_sequence = True
    fields = ','.join(fields)
    pout("Writing %s COPY...\n" % (table,))
    sql4 = "COPY %s (%s) TO '/tmp/%s.sql' DELIMITERS '|' CSV;\n" % (table, 
                                                                fields, table)
    sql5 = "COPY %s (%s) FROM '/tmp/%s.sql' DELIMITERS '|' CSV;\n" % (table, 
                                                                fields, table)
    
    FP4.write(sql4)
    FP5.write("\echo Migrating %s...\n" % (table,))
    FP5.write("DELETE FROM %s;\n" % (table,))

    if table == 'giscedata_descarrecs_sollicitud':
        FP5.write("ALTER TABLE giscedata_descarrecs_sollicitud ALTER COLUMN descripcio SET DEFAULT '';\n")
        FP5.flush()
    FP5.write(sql5)
    # check if there is an _id_seq for this table
    seq_name = '%s_id_seq' % (table,)
    cr4.execute("SELECT count(sequence_name) FROM information_schema.sequences "
                "WHERE sequence_name = %s", (seq_name,))
    if cr4.fetchall()[0][0]:
        cr4.execute("SELECT nextval(%s)", (seq_name,))
        seq_value = cr4.fetchone()[0]
        cr4.execute("SELECT setval(%s, %s)", (seq_name, seq_value))
        FP5.write("SELECT setval('%s', %s);\n" % (seq_name, seq_value))     
    FP4.flush()
    FP5.flush()
    
def migrate_partners():
    """Migrate the partners in ids
    """

    fields = 'id,lang,website,credit_limit,user_id,name,title,comment,active,\
parent_id,ean13,date,ref,vat,debit_limit,create_uid,write_uid'.split(',')
    
    # select the ids of the partners we want to migrate (partners which have
    # addresses assigned to a user)
    cr4.execute("SELECT partner_id FROM res_partner_address "
                "WHERE id IN (SELECT address_id FROM res_users)")
    ids = [str(a[0]) for a in cr4.fetchall()]
    
    _migrate_table('res_partner', fields, 'WHERE id IN (%s)' % (','.join(ids),))

def migrate_addresses():
    """Migrate the partner addresses in ids
    """
    
    cr4.execute("SELECT distinct address_id FROM res_users "
                "WHERE address_id IS NOT NULL")
    ids = [str(a[0]) for a in cr4.fetchall()]
    
    fields = 'id,function,city,fax,name,zip,title,mobile,partner_id,\
street2,birthdate,phone,street,active,type,email,municipi,\
create_uid,write_uid'.split(',')

    _migrate_table('res_partner_address', fields, "WHERE id in (%s)" % (
                                                                ','.join(ids),))

def migrate_companies():
    """Migrate the companies' table
    
    """

    cr4.execute("SELECT distinct company_id FROM res_users")
    company_ids = [str(a[0]) for a in cr4.fetchall()]
    fields = "id,name,parent_id,rml_footer1,rml_footer2,rml_header1,\
partner_id,cups_code,codi_r1,logo,currency_id".split(',')
    
    _migrate_table('res_company', fields)

def migrate_currencies():
    """Migrate the currencies' table
    """

    fields = "id,create_uid,write_uid,code,name,rounding,active,accuracy".split(',')
    _migrate_table('res_currency', fields)
    
def migrate_users():
    """Migrate the users' table
    """

    fields = "id,name,active,login,password,signature,action_id,menu_id,\
address_id,company_id"

    cr5.execute("BEGIN")
    cr5.execute("SET CONSTRAINTS ALL DEFERRED")
    cr5.execute("SET SESSION session_replication_role = replica")
    cr4.execute("SELECT id,login FROM res_users")
    users4 = cr4.fetchall()
    ids4 = [a[0] for a in users4]
    names4 = [a[1] for a in users4]
    cr5.execute("SELECT DISTINCT id,login FROM res_users")
    users5 = cr5.fetchall()
    ids5 = [a[0] for a in users5]
    names5 = [a[1] for a in users5]
    for user in users4:
        if user[1] in names5:
            # user exists
            print("User %s exists" % (user[1],))
            if user in users5:
                # same id, do nothing
                print("Same id, nothing to do")
            else:
                # different id, update
                print("Diffrent id, updating...")
                cr5.execute("UPDATE res_users SET id=%s WHERE login=%s" , user)
                cr5.execute("UPDATE res_groups_users_rel SET uid=%s \
WHERE uid=%s" % (user[0], user[0]))
        else:
            # check for clashing id's
            cr5.execute("SELECT id FROM res_users WHERE id=%s", (user[0],))
            id = cr5.fetchone()
            if id:
                print('hoho')
                cr5.execute("UPDATE res_users SET id=999 WHERE id=%s", (id[0],))
            # create user from user4
            print("No user found, creating...")
            cr4.execute("SELECT %s FROM res_users WHERE id=%s" % (fields,
                                                                  user[0]))
            values = [a or False for a in list(cr4.fetchone())]
            if values[2] == 1:
                values[2] = True
            if not values[6]:
                values[6] = 1
            if not values[7]:
                values[7] = 1
            if not values[8]:
                values[8] = 1
            if not values[9]:
                values[9] = 1
            values = tuple(values)
            cr5.execute("INSERT INTO res_users (id,name,active,login,password,\
signature,action_id,menu_id,address_id,company_id) VALUES %s" , (values,))
    cr5.execute("COMMIT")

def migrate_countries():
    """Migrates the countries table
    """

    fields = "id,name,create_uid,create_date,write_uid,write_date,code".split(',')
    _migrate_table('res_country', fields)


# Connection setup

if __name__ == '__main__':
    
    parser = OptionParser(usage="%prog [OPTIONS]", version="%prog 0.1")
    parser.add_option("-4", "--db-4", dest="DBNAME4", default="SOLLER_1.12",
                      help="Base de dades origen (4.2)")
    parser.add_option("-5", "--db-5", dest="DBNAME5", default="lolcatz",
                      help=u"Base de dades destí (5.x)")
    (options, args) = parser.parse_args()
    if options.DBNAME4:
        DBNAME4 = options.DBNAME4
    if options.DBNAME5:
        DBNAME5 = options.DBNAME5

    db4 = psycopg.connect('dbname=%s' % (DBNAME4,), serialize=1)
    db5 = psycopg.connect('dbname=%s' % (DBNAME5,), serialize=1)
    cr4 = db4.cursor()
    cr5 = db5.cursor()
    
    # Let's start doing real stuff...
    pout("Starting migration...\n")
    prepare()
    
    migrate_users()
    
    migrate_currencies()
    migrate_companies()
    migrate_countries()
    migrate_partners()
    migrate_addresses()
    
    for module in MOD_TABLES_ORDER:
        for table in MOD_TABLES[module]:
            # check for DROP_COLUMNS
            cr4.execute("SELECT column_name FROM information_schema.columns \
                        WHERE table_name=%s", (table,))
            cols4 = [a[0] for a in cr4.fetchall()]
            for col in DROP_COLUMNS:
                if col in cols4:
                    cols4.remove(col)
            fields = cols4[:]
            
            # select the common columns
            cr5.execute("SELECT column_name FROM information_schema.columns "
                        "WHERE table_name=%s", (table,))
            cols5 = [a[0] for a in cr5.fetchall()]
            for col in cols4:
                if col not in cols5:
                    perr("Table %s in %s has missing columns in %s... fixing\n" 
                            % (table, DBNAME4, DBNAME5))
                    # drop the origin column from the fields
                    fields.remove(col)
            _migrate_table(table, fields)
    
    # All stuff (hopefully) migrated, let's tidy up things...
    finalize()
    cr4.close()
    cr5.close()
    pout("Migration finished!\n")
