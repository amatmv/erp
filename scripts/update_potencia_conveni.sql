-- Actualitza la potència de conveni de tots els CUPS amb la 
-- potència de la modificació contractual més recent vinculada 
-- al CUPS (a través de la pòlissa).
update giscedata_cups_ps cups 
set potencia_conveni = query.potencia 
from (
    select cups.id as cups_id,
           mc.potencia as potencia
    from giscedata_cups_ps as cups
    inner join giscedata_polissa as poli on poli.cups = cups.id
    inner join giscedata_polissa_modcontractual as mc on mc.polissa_id = poli.id
    order by mc.data_inici desc
) as query where cups.id = query.cups_id
