#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ooop import OOOP
import csv
import os
import xmlrpclib
from datetime import datetime

"""
Esborra partners duplicats, quedant-se amb el que té NIF ES%
"""
O5 = OOOP(dbname=os.getenv('DBNAME', 'oerp5_distri'), user='admin', pwd='admin',
          port=int(os.getenv('OOOP_PORT', '8069')),
         debug=int(os.getenv('DEBUG', 0)))

def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))

def _get_proxy(model, versio='5'):
    """Retorna un objecte OOOP pel model.
    """
    return getattr(O5, model2cc(model))


def halt():
    """Halt waiting for user input.
    """
    print "Prem intro per continuar ..."
    raw_input()

def main():
    """Funció principal de l'script.
    """
    partner_proxy = _get_proxy('res.partner')
    paddress_proxy = _get_proxy('res.partner.address')
    partners = partner_proxy.filter(vat__ilike='ES%')
    print("%s partners ES trobats" % (len(partners)))

    for partner_es in partners:
        partner = partner_proxy.filter(vat=partner_es.vat[2:])
        print("ES: %s (%s)" % (partner_es.name, partner_es.vat))
        if partner:
            print("NO-ES: %s (%s)" % (partner[0].name, partner[0].vat))
            paddr = paddress_proxy.filter(partner_id=partner[0].id)
            print("%s adreces trobades pel partner sense ES" % (len(paddr)))
            # guardem el name pel tema unicode
            partner_es.name = partner[0].name
            partner_es.save()
            for addr in paddr:
                addr.partner_id = partner_es
                addr.save()
            partner[0].unlink()
            print("Partner NO-ES esborrat")

if __name__ == "__main__":
    try:
        START_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        main()
    except KeyboardInterrupt:
        print
        print 'Shutting Down...'
    finally:
        END_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print "Start at %s" % START_DATE
        print "End at %s" % END_DATE
