#! /bin/sh
# /etc/init.d/virtualbox.sh
#

case "$1" in
  start)
    echo "Starting VirtualBox"
    su gis -c "VBoxManage startvm \"GIS Biar\" --type vrdp"
    ;;
  stop)
    echo "Stopping VirtualBox"
    su gis -c "VBoxManage controlvm \"GIS Biar\" acpipowerbutton"
    ;;
  *)
    echo "Usage: /etc/init.d/virtualbox {start|stop}"
    exit 1
    ;;
esac

exit 0
