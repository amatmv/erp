#! /bin/sh
# /etc/init.d/tomcat.sh
#

export JRE_HOME="/usr/lib/jvm/java-6-sun"

case "$1" in
  start)
    echo "Starting Tomcat"
    su gis -c "/home/gis/tomcat/bin/startup.sh"
    ;;
  stop)
    echo "Stopping Tomcat"
    su gis -c "/home/gis/tomcat/bin/shutdown.sh"
    ;;
  restart)
    /etc/init.d/tomcat.sh stop
    /etc/init.d/tomcat.sh start
    ;;
  *)
    echo "Usage: /etc/init.d/tomcat {start|stop|restart}"
    exit 1
    ;;
esac

exit 0
