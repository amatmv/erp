#!/bin/sh
# Firewall script for iptables i connexió VPN
# Habilitat el port forwarding del tun0:3389 (local) al 3389 de la màquina windows.

ETH_IF="eth0"
VPN_IF="tun0"
IP_WIN="192.168.1.34"
IP_LINUX="192.168.1.33"

echo "1" > /proc/sys/net/ipv4/ip_forward

case "$1" in
  start)
    echo "Creating iptables rules"
    iptables -A PREROUTING -t nat -i $VPN_IF -p tcp --dport 3389 -j DNAT --to $IP_WIN:3389
    iptables -A PREROUTING -t nat -i $VPN_IF -p tcp --dport 80 -j DNAT --to $IP_WIN:80
    iptables -t nat -A POSTROUTING -o $ETH_IF -j MASQUERADE
    ;;
  stop)
    echo "Deleting iptables rules"
    iptables -F 
    iptables -t nat -F
    ;;
  *)
    echo "Usage: /etc/init.d/firewall.sh {start|stop}"
    exit 1
    ;;
esac

exit 0
