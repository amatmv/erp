#! /bin/sh
# /etc/init.d/vboxnetwork.sh
#

case "$1" in
  start)
    echo "Starting eth2"
    ifconfig eth2 up
    ;;
  stop)
    echo "Stopping eth2"
    ifconfig eth2 down
    ;;
  *)
    echo "Usage: /etc/init.d/vboxnetwork.sh {start|stop}"
    exit 1
    ;;
esac

exit 0
