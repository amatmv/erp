#!/bin/sh

VPN_PATH="/etc/openvpn/porta/centelles@porta.gisce.net"
VPN_CONFIG="centelles@porta.gisce.net.ovpn"

case "$1" in 
  start)  
    echo "Connecting to GISCE VPN"
    openvpn --client --config $VPN_PATH/$VPN_CONFIG &>$VPN_PATH/logfile &
    ;;
  stop)
    echo "Disconnecting from GISCE VPN"
    VPN_PID=`ps aux|grep "openvpn --client "|awk '{print $2}'|head -n 1`
    kill -9 $VPN_PID
    ;;
  restart)
    /etc/init.d/vpn-GISCE.sh stop
    /etc/init.d/vpn-GISCE.sh start
    ;;
  *)
    echo "Usage: /etc/init.d/vpn-GISCE.sh {start|stop|restart}"
    exit 1
    ;;
esac

exit 0
