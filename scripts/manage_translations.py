from configparser import ConfigParser
from tqdm import tqdm
from os.path import realpath, dirname, join

import os
import subprocess
import click


def detect_module(path):
    """Detect if a path is part of a openerp module or not

    :param path: to examine
    :return: None if is not a module or the module name
    """
    stack = path.split(os.path.sep)
    if not stack[0]:
        stack[0] = os.path.sep
    stack = [x for x in stack if x]
    black_list = ['ABEnergia', 'Agrienergia','Albatera','Catral','EDM','Lucera','GISCEGas','Lumina','Panticosa', 'Soller', 'SomEnergia']
    
    for item in black_list:
       if item in stack:
          return None
    while stack:
        path = os.path.join(*stack)
        module = stack[-1]
        stack = stack[:-1]
        if not os.path.isdir(path):
            continue
        files = os.listdir(path)
        if '__terp__.py' in files:
            return module
    return None


def upload_translations(modules):
    base_command = 'tx push -s -r'.split(' ')
    for resource in tqdm(modules, desc='Uploading Translations'):
        result = subprocess.call(base_command+[resource])


def download_translations(modules, configs):
    base_command = 'tx pull -f -r'.split(' ')
    for resource in tqdm(modules, desc='Downloading Translations'):
        source_lang = configs.get(resource, 'source_lang')
        langs = []
        if source_lang != 'ca_ES':
            langs.append('ca_ES')
        if source_lang != 'es_ES':
            langs.append('es_ES')
        for lang in langs:
            result = subprocess.call(
                base_command+[resource, '-l', lang],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE
            )


def show_modules(modules):
    for modules in modules:
        print(modules.split('.')[-1])


@click.command()
@click.option('--upload/--no-upload', default=False,
              help='Upload translation files')
@click.option('--download/--no-download', default=False,
              help='Download translation files')
@click.option('--modules/--no-modules', default=False,
              help='Only show affected modules and exit')
@click.option('--from-version', default='', type=str,
              help='Version to compare to get affected modules')
def manage_translations(**kwargs):
    translate_confs = ConfigParser()
    translate_confs.read('.tx/config')
    modules = translate_confs.sections()
    modules.remove('main')
    if not kwargs.get('from_version', False):
        if not click.confirm("Uploading/Downloading ALL MODULES?"):
            return
    else:
        command = [
            "git", "diff", "--name-only", 
            "HEAD~1..{}".format(kwargs['from_version'])
        ]
        paths = subprocess.check_output(command)
        paths = [x for x in paths.split('\n') if x]
        modules_to_test = []
        for path in paths:
            if '__terp__' in path:
                continue
            module = detect_module(path)
            if module and module not in modules_to_test:
                modules_to_test.append(module)
        filtered_mods = []
        for module in modules:
            try:
                project, name = module.split('.')
            except:
                print module
                exit(-1)
            if name in modules_to_test:
                filtered_mods.append(module)
        modules = filtered_mods
    if kwargs.get('modules', False):
        return show_modules(modules)
    if kwargs.get('upload', False):
        upload_translations(modules)
    if kwargs.get('download', False):
        download_translations(modules, translate_confs)

if __name__ == '__main__':
    correct_path = dirname(dirname(realpath(__file__)))
    os.chdir(correct_path)
    manage_translations()
