from erppeek import Client
import base64
import time
import click


@click.command()
@click.option("-s", "--server", default="http://localhost:8069", help="Server URL")
@click.option("-d", "--database", help="Database")
@click.option("-u", "--user", help="User")
@click.option("-p", "--password", help="Password")
@click.option("-o", "--destination", help="Destination to store the file")
def export_descarrecs(server, database, user, password, destination):
    """
    Exports the descarrecs in Sqlite format

    :param server: erppeek server URL
    :type server: str
    :param database: erppeek connection
    :type database: str
    :param user: erppeek user
    :type user: str
    :param password: erppeek password
    :type password: str
    :param destination: Destination to store the sqlite file
    :type destination: str
    :return: None
    """

    actual_date = time.strftime("%Y-%m-%d")

    c = Client(server, database, user, password)
    mod_desc = c.model('giscedata.descarrecs.descarrec')
    data_sqlite = base64.decodestring(mod_desc.export_descarrecs(actual_date, "sqlite"))

    with open(destination, "w") as f:
        f.write(data_sqlite)

if __name__ == "__main__":
    export_descarrecs()