#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Script per arreglar els ID's que apunten a res_municipi.
"""

from optparse import OptionParser
import psycopg2

TABLES = [('giscedata_at_linia', 'municipi'),
          ('giscedata_bt_caixes', 'id_municipi'),
          ('giscedata_bt_element', 'municipi'),
          ('giscedata_cts', 'id_municipi'),
          ('giscedata_cts_sortidesbt', 'municipi'),
          ('giscedata_descarrecs_descarrec_clients', 'codeine'),
          ('giscedata_descarrecs_descarrec_installacions', 'codeine'),
          ('giscedata_fotovoltaiques_sollicitud', 'municipi'),
          ('giscedata_qualitat_affected_customer', 'codeine'),
          ('giscedata_qualitat_affected_installation', 'codeine'),
          ('giscedata_qualitat_install_power', 'codeine'),
          ('giscedata_qualitat_interrupciobt_anual_municipi', 'municipi'),
          ('giscedata_qualitat_interrupciobt_mensual_municipi', 'municipi'),
          ('giscedata_qualitat_niepi_anual_municipi', 'municipi'),
          ('giscedata_qualitat_niepi_mensual_municipi', 'municipi'),
          ('giscedata_qualitat_percentil80_municipi', 'municipi'),
          ('giscedata_qualitat_tiepi_anual_municipi', 'municipi'),
          ('giscedata_qualitat_tiepi_mensual_municipi', 'municipi'),
          ]

DB4 = 'SOLLER_1.12'
DB5 = 'distri_soller'

def run():
    parser = OptionParser(version='0.1')
    parser.add_option('-4', '--db-4', dest='db4', help='The origin database',
                      default=DB4)
    parser.add_option('-5', '--db-5', dest='db5', help='The destination '
                      'database', default=DB5)
    (options, _) = parser.parse_args()
    
    db4 = psycopg2.connect('dbname=%s' % (options.db4,))
    db5 = psycopg2.connect('dbname=%s' % (options.db5,))

    cr4 = db4.cursor()
    cr5 = db5.cursor()

    cr4.execute("SELECT id, name FROM res_municipi")
    municipis4 = {}
    for (id_, name) in cr4.fetchall():
        municipis4[name] = id_

    municipis = []
    for municipi in municipis4.keys():
        cr5.execute("SELECT id FROM res_municipi WHERE name=%s", (municipi,))
        res = cr5.fetchone()
        if res:
            municipis.append((municipis4[municipi], res[0]))

    cr5.execute("BEGIN")
    for (table, column) in TABLES:
        for mun in municipis:
            query = "UPDATE %s SET %s=%%s WHERE %s=%%s;" % (table, column,
                                                          column)
            print(query % (mun[1], mun[0]))
            cr5.execute(query, (mun[1], mun[0]))
    cr5.execute("COMMIT")
    cr4.close()
    cr5.close()

if __name__ == '__main__':
    run()
