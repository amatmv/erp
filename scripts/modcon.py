#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Detectar modificacions contractuals en un periode de facturació 
from loop import OOOP
O = OOOP(dbname='oerp5_distri', port=8051, user='root', pwd='root')
for polissa in O.GiscedataPolissa.all():
    #data_inici, data_final = polissa.get_inici_final_a_facturar(True)
    data_final = '2011-02-28'
    if polissa.facturacio == 1:
        data_inici = '2011-02-01'
    else:
        data_inici = '2011-01-01'
    intervals = polissa.get_modcontractual_intervals(data_inici, data_final)
    if len(intervals) > 1:
        print '---------------------------------------------'
        print 'Polissa: %s (%s - %s)' % (polissa.name, data_inici, data_final)
        print intervals
        for data_int in sorted(intervals.keys()):
            interval = intervals[data_int]
            interv = O.GiscedataPolissaModcontractual.get(interval['id'])
            for change in interval.get('changes', []):
                print 'Canvi de %s: %s' % (change, getattr(interv, change))

