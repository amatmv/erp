#!/usr/bin/python
# -*- coding: UTF-8 -*-

# ----
# Modificar les dades que conté el fitxer generat per audinfor tal que compleixin amb el que esperem
# --file=<fitxer d'entrada: iso-8859-1>
# ----

import csv
import sys
import getopt
import codecs
import os

CodiDistri={'' : '0187'}

keywords = ['file='] 

try:                                
    opts, args = getopt.getopt(sys.argv[1:], "", keywords) 
except getopt.GetoptError, err:
    print str(err)
    usage()   
    sys.exit(2)  

strFileOrig = ""
for opt, arg in opts:
    if opt == "--file":
        strFileOrig=os.path.abspath(arg)
    else:
        assert False,"unhandled option"
        sys.exit()

strFileDest = "%s.%s" % (strFileOrig.split('.')[0], 'soa')
strFileOrig_utf8='%s_%s' % (strFileOrig, 'utf8')
BLOCKSIZE = 1048576 
sourceFile = codecs.open(strFileOrig, "r", "iso-8859-1")
targetFile = codecs.open(strFileOrig_utf8, "w", "utf-8")
while True:
    contents = sourceFile.read(BLOCKSIZE)
    if not contents:
        break
    targetFile.write(contents)

fdr=open(strFileOrig_utf8,'r')
objReader=csv.reader(fdr, delimiter='\t')
fdw=open(strFileDest, 'w')
objWriter=csv.writer(fdw,delimiter='\t')
for pos, row in enumerate(objReader):
    try:
        if len(row[5]) > 10:
            trim10=0
            try:
                str_unic=unicode(row[5][9:11],"utf-8")
            except UnicodeDecodeError, e:
                trim10=1;
            if trim10:
                row[5]=row[5][:10]
            else:
                row[5]=row[5][:9]
        if len(row[6]) > 10:
            trim10=0
            try:
                str_unic=unicode(row[6][9:11],"utf-8")
            except UnicodeDecodeError, e:
                trim10=1;
            if trim10:
                row[6]=row[6][:10]
            else:
                row[6]=row[6][:9]
        if '/' in row[19]:
            row[19] = row[19].replace('/', '-')
        if '/' in row[21]:
            row[21] = row[21].replace('/', '-')
        if row[38].count(' ') > 0:
            row[38]=row[38][0:row[38].find(' ')]
        if row[38].count('/') > 0:
            row[38]=row[38][0:row[38].find('/')]
        if row[58]=="":
            row[58]='x'
        if row[13]=='':
            row[13]='1'
        if ',' in row[24]:
            row[24] = row[24].replace(',', '.')
        if row[25].find('x')!=-1:
            row[57]=row[25][0:row[25].find('x')]
            row[25]=row[25][row[25].find('/')+1:len(row[25])]
        else:
            row[57]=1
        row[38] = row[38].replace(',','.')
        try:
            row[38]=int(float(row[38]))
        except ValueError, e:
            pass
        row[43] = row[43].replace(',','.')
        row[32]='%04d' % int(row[32])
        row[40]='%04d' % int(CodiDistri.get(row[40], row[40]))
        objWriter.writerow(row)
    except:
        print "Fitxer %s -> No s'ha pogut preprocessar la línia %d" % (arg, pos)
        pass


fdr.close()
del fdr
fdw.close()
del fdw
os.remove(strFileOrig_utf8)
