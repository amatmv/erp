# -*- encoding:utf-8 -*-
import erppeek

import re
import urllib2


def extract_bic(html):
    """ Extract the BIC from the given URL """
    regex = ur'<td class="gdpc-property-label">Swift/BIC:</td>'
    regex += ur'<td class="gdpc-property-value">(\w+)</td>'
    search_res = re.search(regex, html)
    if search_res:
        return search_res.group(1)
    return False


def extract_new_code(html):
    """ Return false is it does NOT have a new CODE (bank fusion,... ),
    " new code otherwise
    """
    # Look for new code
    regex = ur'<td class="gdpc-property-label">Código entidad sucesora:</td>'
    regex += ur'<td class="gdpc-property-value">'
    regex += ur'<a href="http://bandir.infotelefonica.es/(\d+)">(\d+)</a></td>'
    search_res = re.search(regex, html)
    if search_res:
        return search_res.group(1)
    
    regex = ur'<td class="gdpc-property-label">Código Entidad:</td>\s*'
    regex += ur'<td class="gdpc-property-value">(\d+)</td>'
    search_res = re.search(regex, html)
    if search_res:
        return search_res.group(1)
    return False


def get_code_bic_from_the_web(code):
    """ Return the bank code and BIC from a bank """

    url = 'http://bandir.infotelefonica.es/%s' % code
    url_handler = urllib2.urlopen(url)

    html = url_handler.read()

    new_code = extract_new_code(html)
    if code != new_code:
        return get_code_bic_from_the_web(new_code)    

    bic = extract_bic(html)
    return new_code, bic


if __name__=='__main__':
    erp_conn = erppeek.Client('http://localhost:18069', 'oerp5_comer', 'sync', 'sync')

    bank_obj = erp_conn.model('res.bank')
    banks_ids = bank_obj.search()

    processed_codes = []
    for bank_id in banks_ids:
        bank = bank_obj.get(bank_id)
        bic = bank.bic
        code = bank.code
   
        if code in processed_codes:
            continue

        ok = True
        new_code, new_bic = get_code_bic_from_the_web(code)
        if new_code != code:
            ok = False
            if not new_code:
                print "[FAIL] Bank code %s not found in the web" % (code)
            else:
                print "[FAIL] Bank code %s now is %s with bic: %s" % (code, new_code, new_bic) 

        if new_bic != bic:
            ok = False
            if not new_bic:
                print "[FAIL] Bank code %s has no BIC on the web" % (code)
            else:
                print "[FAIL] Bank code %s has wrong BIC %s, should be %s" % (code, bic, new_bic)   
      
        if ok:
            print "Bank code %s has correct bic" % (code)

        processed_codes.append(code)
