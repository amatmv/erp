#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tqdm import tqdm
from datetime import datetime
from erppeek import Client
import click
import logging


@click.group(name='sync_cups')
def sync_cups(**kwargs):
    pass


def connect_client_local(args):
    local_server = '{}:{}'.format(args['local_server'], args['local_port'])
    logger.info('Connecting to local server[{}]\n'.format(local_server))
    return Client(
        server=local_server, db=args['local_database'],
        user=args['local_user'], password=args['local_password']
    )


def connect_client_remote(args):
    remote_server = '{}:{}'.format(args['server'], args['port'])
    logger.info('Connecting to remote server[{}]\n'.format(remote_server))
    return Client(
        server=remote_server, db=args['database'],
        user=args['user'], password=args['password']
    )


def connect_clients(args):
    return connect_client_local(args), connect_client_remote(args)


def get_cups(connection, vals, update=False):
    """
    :param connection:  ERPPEEK like connection
    :param vals:        VALS to create/update the CUPS object
    :param update:      Flag to update or no update the CUPS if found
    :return:            Returns <0 on errors, True if updated and ID if created
    """
    logger = logging.getLogger('sync')
    cups_obj = connection.model('giscedata.cups.ps')
    tv_obj = connection.model('res.tipovia')
    municipi_obj = connection.model('res.municipi')

    if 'id' in vals:
        vals.pop('id')
    tv_id = tv_obj.search([('codi', '=', vals['tv'])]) if vals['tv'] else False
    vals['tv'] = tv_id[0] if tv_id else False
    municipi_id = municipi_obj.search([('name', '=', vals['id_municipi'])])
    vals['id_municipi'] = municipi_id[0] if municipi_id else False
    cups_name = vals['name']
    cups_id = cups_obj.search([
        ('name', '=', cups_name),
    ], 0, 0, False, {'active_test': False})
    if not cups_id:
        by_addr = cups_obj.search([
            (keyname, '=', vals[keyname])
            for keyname in [
                'nv', 'pnp', 'es', 'pt', 'pu',
                'cpo', 'cpa', 'dp', 'aclarador'
            ]
        ], 0, 0, False, {'active_test': False})
        if by_addr:
            logger.error('Ommiting cups {} by address values'.format(cups_name))
            return -1, False
        # logger.info('Creating {}'.format(cups_name))
        try:
            return 1, [cups_obj.create(vals).id]
        except Exception as err:
            logger.error('Could not create {}; ERROR: {}'.format(cups_name, err))
            return -2, False
    if len(cups_id) > 1:
        cups_id = cups_obj.search([('name', '=', cups_name)])
        if len(cups_id) > 1:
            logger.error('DUPLICATED CUPS! {}: {}'.format(cups_id, cups_name))
            return -3, False
    if update:
        cups_obj.write(cups_id[0], vals)
        return True, cups_id

    return True, cups_id


def get_partner(connection, partner_name):
    partner_obj = connection.model('res.partner')
    partner_id = partner_obj.search([
        ('name', '=', partner_name)
    ])
    if not partner_id:
        partner_id = partner_obj.create({
            'name': partner_name,
            'vat': 'ES11111111H',
        }).id
    else:
        partner_id = partner_id[0]
    return partner_id


def get_partner_address(connection, vals, partner_id=False):
    address_obj = connection.model('res.partner.address')
    tv_obj = connection.model('res.tipovia')
    municipi_obj = connection.model('res.municipi')
    poblacio_obj = connection.model('res.poblacio')
    state_obj = connection.model('res.country.state')
    if 'id' in vals:
        vals.pop('id')
    if 'partner_id' in vals:
        vals.pop('partner_id')
    if partner_id:
        vals['partner_id'] = partner_id
    municipi_id = municipi_obj.search([('name', '=', vals['id_municipi'])])
    vals['id_municipi'] = municipi_id[0] if municipi_id else False

    tv_id = tv_obj.search([('codi', '=', vals['tv'])]) if vals['tv'] else False
    vals['tv'] = tv_id[0] if tv_id else False

    poblacio_id = poblacio_obj.search(
        [('name', '=', vals['id_poblacio'])]) if vals['id_poblacio'] else False
    vals['id_poblacio'] = poblacio_id[0] if poblacio_id else False

    state_id = state_obj.search(
        [('name', '=', vals['state_id'])]) if vals['state_id'] else False
    vals['state_id'] = state_id[0] if state_id else False

    address_id = address_obj.search([
        (key, '=', value)
        for key, value in vals.items()
    ])
    if address_id:
        address_id = address_id[0]
    else:
        address_id = address_obj.create(vals).id

    return address_id


def get_cups_butlleti(connection, cups_id=False, butlleti=False):
    """
    :param connection:  OpenERP Connection
    :param cups_id:     CUPS to get data from
    :param butlleti:    Butlletí data, if not present, return all butlletins
    :type butlleti:     dict
    :return:
    """
    butlleti_obj = connection.model('giscedata.butlleti')
    activitat_us_obj = connection.model('giscedata.butlleti.activitat')
    butlleti_ids = butlleti_obj.search([
        ('cups_id', '=', cups_id)
    ], 0, 0, False)
    if not butlleti:
        butlleti_data = []
        for butlleti_id in butlleti_ids:
            b_data = butlleti_obj.read([butlleti_id], [
                'name', 'partner_id', 'partner_address_id',
                'installador_id', 'dades_installacio', 'activitat_us',
                'giscedata.butlleti.activitat', 'pot_max_admisible',
                'pot_installada', 'tensio', 'resistencia_terra',
                'resistencia_aillament', 'interruptor_diferencial',
                'derivacio_individual', 'data_provisional_obra', 'data',
                'data_vigencia', 'numero_installacio',
            ])[0]
            if b_data['partner_address_id']:
                partner_address_obj = connection.model('res.partner.address')
                b_data['partner_address_id'] = partner_address_obj.read(
                    [b_data['partner_address_id'][0]], [
                        'street', 'tv', 'city', 'name', 'zip', 'phone', 'email',
                        'mobile', 'state_id', 'id_municipi', 'id_poblacio',
                        'es', 'pt', 'cpo', 'cpa', 'pu', 'pnp', 'type', 'fax',
                    ]
                )[0]
            butlleti_data.append(b_data)
        return butlleti_data
    if 'id' in butlleti:
        butlleti.pop('id')
    partner_id = butlleti.pop('partner_id')
    if partner_id:
        partner_id = get_partner(connection, partner_id[1])
    if partner_id:
        butlleti['partner_id'] = partner_id

    installador_id = butlleti.pop('installador_id')
    if installador_id:
        installador_id = get_partner(connection, installador_id[1])
    if installador_id:
        butlleti['installador_id'] = installador_id
    partner_address_id = butlleti.pop('partner_address_id')
    if partner_address_id:
        partner_address_id = get_partner_address(
            connection, partner_address_id,
            partner_id
        )
    if partner_address_id:
        butlleti['partner_address_id'] = partner_address_id
    activitat_us = butlleti.pop('activitat_us')
    if activitat_us:
        activitat_us = activitat_us_obj.search([
            ('name', '=', activitat_us[1]),
        ])
    if activitat_us:
        butlleti['activitat_us'] = activitat_us[0]

    butlleti_id = False
    if butlleti_ids:
        # Si ja en té, busquem un "igual" al local
        butlleti_id = butlleti_obj.search([('id', 'in', butlleti_ids)] + [
            (key, '=', value)
            for key, value in butlleti.items()
        ])
        butlleti_id = butlleti_id[0]

    if not butlleti_id:
        # Si no l'hem trobat o no existeix, el creem
        butlleti['cups_id'] = cups_id
        butlleti_id = butlleti_obj.create(butlleti).id
    return butlleti_id


def get_cups_attachment(connection, cups_id=False, att_data=False):
    """
    :param connection:  OpenERP Connection
    :param cups_id:     CUPS to get data from
    :param att_data:    Attachment data, if not present, return all attachments
    :return:
    """
    categ_obj = connection.model('ir.attachment.category')
    attachment_obj = connection.model('ir.attachment')
    attach_ids = attachment_obj.search([
        ('res_model', '=', 'giscedata.cups.ps'),
        ('res_id', '=', cups_id)
    ])
    if not att_data:
        att_data = []
        for att_id in attach_ids:
            att_data.append(attachment_obj.read(att_id, [
                'name', 'datas', 'description', 'res_model',
                'datas_fname', 'category_id'
            ]))
        return att_data
    att_id = attachment_obj.search([
        ('id', 'in', attach_ids),
        ('datas', '=', att_data['datas']),
        ('name', '=', att_data['name']),
        ('datas_fname', '=', att_data['datas_fname']),
    ])
    if att_id:
        return att_id
    att_vals = att_data.copy()
    att_vals.pop('id')
    att_vals.update({
        'res_id': cups_id,
    })

    categ = att_data.pop('category_id')
    if categ:
        categ_id = categ_obj.search([
            ('name', '=', categ[1])
        ])
        categ_id = categ_id[0] if categ_id else False
        att_vals.update({
            'category_id': categ_id,
        })
    return attachment_obj.create(att_vals).id


def get_cups_from_date(connection, date_from=False, date_to=False):
    """
    Search all CUPS from specific date (not using active test)
    :param connection:  ERPPEEK like connection
    :param date_from:   Date to start importing CUPS from
    :return:            Yields CUPS data for each CUPS found
    """
    domain = []
    if date_from:
        domain.append(
            ('create_date', '>=', date_from)
        )
    if date_to:
        domain.append(
            ('create_date', '<=', date_to)
        )
    cups_ids = connection.GiscedataCupsPs.search(
            domain, 0, 0, False, {'active_test': False})
    return cups_ids


def get_cups_vals(connection, cups_id):
    cups = connection.GiscedataCupsPs.read(cups_id, [
        'id', 'name', 'tv', 'nv', 'pnp', 'es', 'pt', 'pu', 'cpo', 'cpa', 'dp',
        'id_municipi', 'ref_catastral', 'aclarador'
    ])
    if cups.get('tv', False):
        cups['tv'] = connection.ResTipovia.read(cups['tv'][0], ['codi'])['codi']
    return {
        'id': cups['id'],
        'name': cups['name'],
        'tv': cups['tv'] or '',
        'nv': cups['nv'] or '',
        'pnp': cups['pnp'] or '',
        'es': cups['es'] or '',
        'pt': cups['pt'] or '',
        'pu': cups['pu'] or '',
        'cpo': cups['cpo'] or '',
        'cpa': cups['cpa'] or '',
        'dp': cups['dp'] or '',
        'id_municipi': cups['id_municipi'][1] or '',
        'ref_catastral': cups['ref_catastral'] or '',
        'aclarador': cups['aclarador'] or '',
    }


def sync_cups_between_dates(
        local_c, remote_c, distri, update, from_data_str, to_data_str):
    logger = logging.getLogger('sync')
    error = 0
    updated = 0
    created = 0
    for cups_id in tqdm(
        get_cups_from_date(local_c, from_data_str, to_data_str),
        desc='UPDATING REMOTE CUPS'
    ):
        cups_vals = get_cups_vals(connection=local_c, cups_id=cups_id)
        if distri:
            cups_vals.update({'distribuidora_id': 2})
        else:
            cups_vals.update({'distribuidora_id': 1})
        res, r_cups_id = get_cups(
            connection=remote_c,
            vals=cups_vals,
            update=update
        )

        if res is True:
            updated += 1
        elif res < 0:
            error += 1
        else:
            created += 1
        if not res:
            continue
        for butlleti in get_cups_butlleti(
                connection=local_c, cups_id=cups_id):
            get_cups_butlleti(connection=remote_c, cups_id=r_cups_id[0],
                              butlleti=butlleti)
        for attachment in get_cups_attachment(
                connection=local_c, cups_id=cups_id):
            get_cups_attachment(connection=remote_c, cups_id=r_cups_id[0],
                                att_data=attachment)

    logger.info('\tNEW:\t{}'.format(created))
    logger.info('\tUPDATE:\t{}'.format(updated))
    logger.info('\tERROR:\t{}'.format(error))
    logger.info('--------------------')
    logger.info('\tTOTAL:\t{}'.format(created+updated+error))


@sync_cups.command('date')
@click.option('-s', '--local_server', default='http://localhost',
              help=u'Adreça servidor Local ERP',
              show_default=True)
@click.option('-p', '--local_port', default=8069,
              help='Port servidor Local ERP', type=click.INT,
              show_default=True)
@click.option('-u', '--local_user', default='admin',
              help='Usuari servidor Local ERP', show_default=True)
@click.option('-w', '--local_password', default='',
              help='Contrasenya usuari servidor Local ERP')
@click.option('-d', '--local_database',
              help='Nom de la base de dades servidor Local', required=True)
@click.option('-S', '--server', default='http://localhost', show_default=True,
              help=u'Adreça servidor Remot ERP', required=True)
@click.option('-P', '--port', default=8069, help='Port servidor Remot ERP',
              type=click.INT, show_default=True)
@click.option('-U', '--user', default='admin', help='Usuari servidor Remot ERP',
              show_default=True)
@click.option('-W', '--password', default='',
              help='Contrasenya usuari servidor Remot ERP')
@click.option('-D', '--database', help='Nom de la base de dades servidor Remot',
              required=True, show_default=True)
@click.option('-f', '--from_date', default=False,
              help='Desde quina data importar (Format: %Y-%m-%d)')
@click.option('-t', '--from_time', default='00:00:00',
              help='Desde quina hora importar (Format: %H:%M:%S)')
@click.option('-F', '--to_date', default=False,
              help='Fins a quina data importar (Format: %Y-%m-%d)')
@click.option('-T', '--to_time', default='00:00:00',
              help='Fins a quina hora importar (Format: %H:%M:%S)')
@click.option('--update/--no-update', default=False, help='Actualitza els CUPS')
@click.option('--distri/--no-distri', default=True, help='From Distri or Comer')
def create_cups_date(**kwargs):
    local_c, remote_c = connect_clients(kwargs)
    distri = kwargs.get('distri', False)
    update = kwargs['update']
    if kwargs['from_date']:
        from_data_str = datetime.strptime(
            '{} {}'.format(kwargs['from_date'], kwargs['from_time']),
            '%Y-%m-%d %H:%M:%S'
        )
        from_data_str = from_data_str.strftime(
            '%Y-%m-%d %H:%M:%S'
        )
    else:
        from_data_str = False
    if kwargs['to_date']:
        to_data_str = datetime.strptime(
            '{} {}'.format(kwargs['to_date'], kwargs['to_time']),
            '%Y-%m-%d %H:%M:%S'
        )
        to_data_str = to_data_str.strftime(
            '%Y-%m-%d %H:%M:%S'
        )
    else:
        to_data_str = False
    return sync_cups_between_dates(
        local_c, remote_c, distri, update, from_data_str, to_data_str)


@sync_cups.command('name')
@click.option('-s', '--local_server', default='http://localhost',
              help=u'Adreça servidor Local ERP',
              show_default=True)
@click.option('-p', '--local_port', default=8069,
              help='Port servidor Local ERP', type=click.INT,
              show_default=True)
@click.option('-u', '--local_user', default='admin',
              help='Usuari servidor Local ERP', show_default=True)
@click.option('-w', '--local_password', default='',
              help='Contrasenya usuari servidor Local ERP')
@click.option('-d', '--local_database',
              help='Nom de la base de dades servidor Local', required=True)
@click.option('-S', '--server', default='http://localhost', show_default=True,
              help=u'Adreça servidor Remot ERP', required=True)
@click.option('-P', '--port', default=8069, help='Port servidor Remot ERP',
              type=click.INT, show_default=True)
@click.option('-U', '--user', default='admin', help='Usuari servidor Remot ERP',
              show_default=True)
@click.option('-W', '--password', default='',
              help='Contrasenya usuari servidor Remot ERP')
@click.option('-D', '--database', help='Nom de la base de dades servidor Remot',
              required=True, show_default=True)
@click.option('-f', '--from_cups', default=False,
              help='Desde quin cups importar (Format: ES0000000000000000000F)')
@click.option('-t', '--to_cups', default='00:00:00',
              help='Fins quin cups importar (Format: ES0000000000000000000F)')
@click.option('--update/--no-update', default=False, help='Actualitza els CUPS')
@click.option('--distri/--no-distri', default=True, help='From Distri or Comer')
def create_cups_date(**kwargs):
    local_c, remote_c = connect_clients(kwargs)
    distri = kwargs.get('distri', False)
    update = kwargs['update']
    cups_from = kwargs['from_cups']
    cups_to = kwargs['to_cups']
    # Get dates
    cups_obj = local_c.model('giscedata.cups.ps')
    cups_from_id = cups_obj.search([('name', '=ilike', cups_from)])
    if not cups_from_id:
        print("No s'ha trobat el primer CUPS: {}".format(cups_from))
        exit(-1)
    cups_to_id = cups_obj.search([('name', '=ilike', cups_to)])
    if not cups_to_id:
        print("No s'ha trobat el ultim CUPS: {}".format(cups_to))
        exit(-1)
    cups_from_id = cups_from_id[0]
    cups_to_id = cups_to_id[0]
    from_data_str = cups_obj.read(cups_from_id, ['create_date'])['create_date']
    to_data_str = cups_obj.read(cups_from_id, ['create_date'])['create_date']
    # Sync cups
    return sync_cups_between_dates(
        local_c, remote_c, distri, update, from_data_str, to_data_str)


if __name__ == '__main__':
    logging.basicConfig(
        format='[%(asctime)s] %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=logging.INFO
    )
    sync_cups()
