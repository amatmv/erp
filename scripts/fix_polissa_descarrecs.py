#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from optparse import OptionParser
from ooop import OOOP

def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))

def _get_proxy(ooop, model):
    """Retorna un objecte OOOP pel model.
    """
    return getattr(ooop, model2cc(model))

def run():
    parser = OptionParser(version='0.1')
    parser.add_option('-d', '--database', dest='database',
                      help='The database to fix')
    parser.add_option('-p', '--port', dest='port', help='The XMLRPC port',
                      default=int(os.getenv('OOOP_PORT', '8069')))
    parser.add_option('-u', '--user', dest='user', help='The ERP user',
                      default='admin')
    parser.add_option('-w', '--password', dest='password',
                      help='The password of the user', default='admin')
    parser.add_option('-H', '--host', dest='host', help='The ERP host',
                      default='localhost')
    (options, _) = parser.parse_args()
    
    if not options.database:
        parser.error('The database argument is mandatory.')

    options.host = 'http://%s' % (options.host,)
    
    o = OOOP(user=options.user, pwd=options.password, dbname=options.database,
             uri=options.host, port=options.port)
    
    print("Start...")
    desc_clients = _get_proxy(o, 'giscedata.descarrecs.descarrec.clients')
    polisses = _get_proxy(o, 'giscedata.polissa')
    for client in desc_clients.all():
        polissa = polisses.filter(name=client.codecustomer)
        if len(polissa):
            client.policy = polissa[0]
            client.save()
            sys.stdout.write('.')
        else:
            sys.stdout.write('x')
            sys.stderr.write('Polissa %s not found\n' % (client.codecustomer,))
            sys.stderr.flush()
        sys.stdout.flush()
    print('\nDone.')

if __name__ == '__main__':
    run()