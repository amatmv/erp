#!/usr/bin/env python
# -*- coding: utf-8 -*-
from loop import OOOP
import csv
import os
import xmlrpclib
from datetime import datetime

O5 = OOOP(dbname=os.getenv('DBNAME', 'LERSA_comer'), user='sync', pwd='sync',
          port=int(os.getenv('OOOP_PORT', '8050')),
         debug=int(os.getenv('DEBUG', 0)))
CSV_FILE = os.getenv('CSV_FILE', '/tmp/aspirapol5.csv')
ERR_FILE = '%s.err' % CSV_FILE
READER = csv.reader(open(CSV_FILE), delimiter=',')
if int(os.getenv('START_LINE', 0)):
    ERROR = open(ERR_FILE, 'a+')
else:
    ERROR = open(ERR_FILE, 'w')
PROCESSADES = 0
ROW_FIELDS = 13


def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))

def _get_proxy(model):
    """Retorna un objecte OOOP pel model.
    """
    return getattr(O5, model2cc(model))

def create_or_update(model, vals, **kwargs):
    """Creem o actualitzem segons faci o no faci match.
    """
    proxy = _get_proxy(model)
    results = proxy.filter(**kwargs)
    msg = ""
    start = datetime.now()
    if len(results):
        msg += "Updating"
        obj = results[0]
    else:
        msg += "Creating"
        obj = proxy.new()
    for key, value in vals.items():
        setattr(obj, key, value)
    obj.save()
    end = datetime.now()
    msg += " %s (id: %i)" % (model, obj.id)
    print msg
    print "Time: %.3fs." % ((end - start).seconds +
                             float((end - start).microseconds) / 10**6)
    return obj.id

def get_id(model, versio='5', **kwargs):
    """Retorna l'id segons el match.
    """
    proxy = _get_proxy(model)
    results = proxy.filter(**kwargs)
    if not len(results):
        raise Exception("No s'ha trobat l'id del model: %s amb match: %s"
                        % (model, kwargs))
    return results[0].id

def parse_row(row):
    """Processa la fila.
    """
    # Stripping all values
    row = map(str.strip, row)
    
    # 0: POLISSA
    # 1: TITULAR
    # 2: PAGADOR
    # 3: PAGADOR_DOMICILI
    # 4: PAGADOR_NUM
    # 5: PAGADOR_ESCALA
    # 6: PAGADOR_PIS
    # 7: PAGADOR_PORTA
    # 8: PAGADOR_CP
    # 9: PAGADOR_INE
    # 10: PAGADOR_POBLACIO
    # 11: PAGADOR_TELEFON
    # 12: PAGADOR_TELEFON2
    row[0] = str(int(row[0]))
    polissa = _get_proxy('giscedata.polissa').get(get_id('giscedata.polissa',
                                                name=row[0],
                                                active__in=('True', 'False')))
    
    try:
        municipi_obj = _get_proxy('res.municipi').get(get_id('res.municipi',
                                                             ine=row[9]))
    except Exception:
        municipi_obj = False
        pass
        
    if polissa.direccio_pagament:
        dp = polissa.direccio_pagament
    else:
        dp = _get_proxy('res.partner.address').new()
    if not dp.partner_id:
        dp.partner_id = polissa.pagador
    dp.name = row[2]
    dp.nv = row[3]
    dp.pnp = row[4]
    dp.es = row[5]
    dp.pt = row[6]
    dp.pu = row[7]
    dp.zip = row[8]
    dp.city = row[10]
    if municipi_obj:
        dp.id_municipi = municipi_obj
        dp.id_poblacio = _get_proxy('res.poblacio').get(
                             create_or_update('res.poblacio',
                                              {'name': row[10],
                                               'municipi_id': municipi_obj},
                                               name=row[10]))
    dp.phone = row[11]
    dp.mobile = row[12]
    dp.save()
    if not polissa.direccio_pagament:
        polissa.direccio_pagament = dp
        polissa.save()
    

def print_row(row):
    """Print row.
    """
    i = 0
    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    for value in row:
        print '%i: %s' % (i, value)
        i += 1
    print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    print "Line: %i" % READER.line_num

def write_error(row, comment=None):
    """Write error to a file.
    """
    ERROR.write('# Line %i - %s\n' % (READER.line_num, comment or ''))
    ERROR.write(';'.join(row)+'\n')
    ERROR.flush()

def halt():
    """Halt waiting for user input.
    """
    print "Prem intro per continuar ..."
    raw_input()

def main():
    """Funció principal de l'script.
    """
    global PROCESSADES
    for row in READER:
        if READER.line_num < int(os.getenv('START_LINE', 0)):
            continue
        if row[0].startswith('#'):
            continue
        if len(row) != ROW_FIELDS:
            print "!!! %i != %i" % (len(row), ROW_FIELDS)
            write_error(row, comment="Longitud: %i diferent a %i;" % 
                        (ROW_FIELDS, len(row)))
            continue
        try:
            time_start = datetime.now()
            print_row(row)
            parse_row(row)
            PROCESSADES += 1
            time_end = datetime.now()
            print "Time elapsed: %.3fs." % ((time_end - time_start).seconds +
                            float((time_end - time_start).microseconds) / 10**6)
        except xmlrpclib.Fault, fault:
            print "Exception! %s" % fault.faultString
            write_error(row)
            if int(os.getenv('HALT_ON_ERROR', 0)):
                halt()
        except Exception, ecx:
            print "Exception! %s" % ecx
            write_error(row)
            if int(os.getenv('HALT_ON_ERROR', 0)):
                halt()
        finally:
            if int(os.getenv('STEP_BY_STEP', 0)):
                halt()

if __name__ == "__main__":
    try:
        START_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        main()
    except KeyboardInterrupt:
        print
        print 'Shutting Down...'
    finally:
        END_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print "Start at %s" % START_DATE
        print "End at %s" % END_DATE
        print "Processed: %i" % PROCESSADES
        ERROR.close()
