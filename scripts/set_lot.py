# -*- coding: utf-8 -*-
from ooop import OOOP
import sys
o = OOOP(dbname='aspirabon5', port=8050)
i = 0
for polissa in o.GiscedataPolissa.all():
    i += 1
    polissa.lot_facturacio = o.GiscedataFacturacioLot.filter(name='02/2010')[0]
    polissa.save()
    sys.stdout.write('.')
    if not i % 5:
        sys.stdout.write(' ')
    if not i % 80:
        sys.stdout.write('%i\n' % i)
    sys.stdout.flush()
sys.stdout.write(' %i\n' % i)
sys.stdout.flush()
