#!/usr/bin/python
from loop import OOOP
import sys
import time
O = OOOP(dbname='oerp5_comer_fact201103', port=8050)
ORDER_ID = 5
amount = 0
selected_ids = []
invoice_ids = [a['invoice_id'][0] for a in O.GiscedataFacturacioFactura.read(O.GiscedataFacturacioFactura.search([('invoice_id.date_invoice', '=', '2011-03-31'), ('facturacio', '=', 1), ('invoice_id.payment_type.id', '=', 1), ('invoice_id.amount_total', '<>', 0)]), ['invoice_id'])]
print "%i factures trobades per remesar" % len(invoice_ids)
line_ids = O.AccountMoveLine.search([('invoice', 'in', invoice_ids), ('account_id.type', '=', u'receivable')])
order = O.PaymentOrder.get(ORDER_ID)
for line in O.AccountMoveLine.filter(id__in=line_ids):
    amount += line.amount_to_pay
    O.PaymentLine.create(
        {
            'move_line_id': line.id,
            'amount_currency': line.amount_to_pay,
            'bank_id': line.invoice.partner_bank.id,
            'order_id': ORDER_ID,
            'partner_id': line.partner_id and line.partner_id.id or False,
            'communication': (line.ref and line.name!='/' and line.ref+'. '+line.name) or line.ref or line.name or '/',
            'communication2': False,
            'date': order.date_planned or time.strftime('%Y-%m-%d'),
            'currency': line.invoice and line.invoice.currency_id.id or False,
            'account_id': line.account_id.id
         })
    sys.stdout.write('.')
    sys.stdout.flush()
print
print "Remesa total: %s" % amount
