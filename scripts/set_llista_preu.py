# -*- coding: utf-8 -*-
from ooop import OOOP
import sys
import csv
o = OOOP(dbname='comer_soller', port=8050)
i = 1
csv_file = '/home/eduard/Downloads/tarifas_comer_042010.csv'
reader = csv.reader(open(csv_file), delimiter=',')
for row in reader:
    ref_dist = row[0].strip()
    tarifa = row[1].strip()
    polissa = o.GiscedataPolissa.filter(ref_dist=ref_dist)
    if polissa:
        polissa = polissa[0]
    llista_preus = o.ProductPricelist.filter(name=tarifa)
    if llista_preus:
        llista_preus = llista_preus[0]
        polissa.llista_preu = llista_preus
        polissa.versio_primera_factura  = llista_preus.version_id[0]
        polissa.save()
        sys.stdout.write('.')
    else:
        sys.stdout.write('X %s' % tarifa)
    if not i % 5:
        sys.stdout.write(' ')
    if not i % 80:
        sys.stdout.write('%i\n' % i)
    i += 1
    sys.stdout.flush()
sys.stdout.write(' %i\n' % i)
sys.stdout.flush()
