# prints log with data log
function log {
  echo '['`date +'%c'`']: '$1
  if [ $LOG_FILE ];
  then
    echo '['`date +'%c'`']: '$1 >> $LOG_FILE
  fi
}

# prints log and exists
function error {
  log $1
  exit 1
}

# checks for command existence
# i.e command_exists pigz && ZIPPER='pigz'
function command_exists {
  command -v "$1" > /dev/null 2>&1
}

# Partition is mounted
function is_mounted {
  # echoes 0 if $1 is not mounted and 1 if it is
  if [ -z "`mount | grep $1`" ]
  then
    echo 0
  else
    echo 1
  fi
}

# Get dbs 
# you can select de propietary (erp)
function get_dbs {
  if [ -z $1 ];
  then
    user='erp'
  else
    user=$1
  fi

  psql -l | grep '| '$user | cut -d '|' -f 1
} 
