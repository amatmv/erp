#!/bin/sh
# Backup db SomEnergia

PATHBK='/var/backup'
DBNAME='somenergia'
REMOTEPATH='/home/sombackup/erpserver'
REMOTENAME=`date +"%Y-%m-%d"`'-erpbackup.sql.gz'
OVHPATH='/var/backup_nfs/archive/'

ZIPPER='gzip'

command_exists() {
    command -v "$1" > /dev/null 2>&1
}

log(){
  echo '['`date +'%c'`']: '$1
}

command_exists pigz && ZIPPER='pigz'

if [ ! -d $PATHBK ]; then
    mkdir $PATHBK
fi

BKNAME=$PATHBK/$DBNAME.sql
GZNAME=$PATHBK/$DBNAME.sql.gz
if [ -s $GZNAME ]; then
    log 'eliminem el fitxer antic'
    rm $GZNAME
fi
# Es comprimeix directament per falta d'espai
log 'Iniciant bolcat...'
pg_dump $DBNAME > $BKNAME
log "Comprimint amb $ZIPPER..."
$ZIPPER $BKNAME
log 'Calculant md5...'
MD5SUM=`md5sum $GZNAME`
echo $MD5SUM

log 'Copiant a OVH backup system ...'
OVHGZ=$OVHPATH/$REMOTENAME
cp $GZNAME $OVHGZ

log 'Copiant a sw2.somenergia.coop...'
REMOTEGZ=$REMOTEPATH/$REMOTENAME
scp $GZNAME sombackup@sw2.somenergia.coop:$REMOTEGZ

log 'Calculant md5 remot...'
MD5SUMREMOTE=`ssh sombackup@sw2.somenergia.coop "md5sum $REMOTEGZ"`
echo $MD5SUMREMOTE

log 'Fet'

