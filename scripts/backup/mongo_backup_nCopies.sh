#!/bin/sh
#Backup mongodb

TOTAL_COPIES_MONGO=2

MONGO_BACKUP_PATH='/home/erp/var/backups/mongo'
DBNAME='openerp'

if [ $# -gt 0 ] ;
then
	DBNAME=$1
fi

# Si REMOTESERVER buit, no es copia per ssh
#REMOTESERVER="erp@backupserver"
REMOTEPATH='backups/arpydecal'

SMB_SHARE="//server/share"
SMB_DIR="SMBDIR"
SMB_CREDENTIALS='/home/erp/conf/smbcredentials'

ZIPPER='gzip'

if [ -e ~/conf/backup.conf ]; then #Si tenim un fitxer de configuracio, substituim les variables per les d'aquest fitxer
 . ~/conf/backup.conf
fi

PATHBK=$MONGO_BACKUP_PATH
REMOTEPATH=$MONGOREMOTEPATH

command_exists() {
    command -v "$1" > /dev/null 2>&1
}

log() {
    echo '['`date +'%c'`']: '$1
}

command_exists pigz && ZIPPER='pigz'

if [ ! -d $PATHBK ]; then #Creem el directori on hem de guardar la copia
    mkdir $PATHBK
fi

ULTIM_FITXER=`ls -t1 ${PATHBK} | head -n 1` #Agafem el fitxer mes recent
ITER=0 #Per defecte la iteracio es 0
if [ $ULTIM_FITXER ] ; #Si hem trobat algun fitxer
then
   ULTIMA_ITER=`echo ${ULTIM_FITXER} | cut -d'_' -f2 | cut -d'.' -f1` #Agafem el numero d'iteracio del fitxer mes recent
   ITER=$((($ULTIMA_ITER+1) % $TOTAL_COPIES_MONGO)) #I busquem quina es la seguent
fi

BKNAME=${PATHBK}/${DBNAME}
GZNAME=${PATHBK}/${DBNAME}_${ITER}.mongodb.gz
REMOTENAME="${DBNAME}_mongo_${ITER}.tgz"

if [ -s $GZNAME ]; then
    log 'eliminem el fitxer antic'
    rm $GZNAME #Elimiem el fitxer amb el mateix nom, ja que sera el mes antic
fi

log 'iniciant bolcat '$DBNAME'...'
#No es pot comprimir directament :(
mongodump -d $DBNAME -o $PATHBK 2>&1 > /dev/null
log "compactant amb $ZIPPER..."
tar cvf - $BKNAME 2> /dev/null | $ZIPPER > $GZNAME #Creem l'arxiu comprimit
log 'Calculant md5...'
MD5SUM=`md5sum $GZNAME`
echo $MD5SUM
# SSH
if [ ${#REMOTESERVER} -gt 0 ] ; #Si hem donat un servidor ssh
then
    log "Copiant a ${REMOTESERVER}..."
    REMOTEGZ=$REMOTEPATH/$REMOTENAME
    scp $GZNAME ${REMOTESERVER}:$REMOTEGZ #Copiem la nova copia al servidor, sobreescrivint la vella
    log 'Calculant md5 remot...'
    MD5SUMREMOTE=`ssh ${REMOTESERVER} "md5sum $REMOTEGZ"`
    echo $MD5SUMREMOTE
fi
#SAMBA
if [ ${#SMB_SHARE} -gt 0 ] ; #Si hem donat un servidor SAMBA
then
    remote_file=`basename $GZNAME`
    log "smbclient $SMB_SHARE -A $SMB_CREDENTIALS -c \"cd $SMB_DIR_MONGO; put $GZNAME $remote_file\""
    smbclient $SMB_SHARE -A $SMB_CREDENTIALS -c "cd $SMB_DIR_MONGO; put $GZNAME $remote_file" #Copiem la nova copia al servidor, sobreescrivint la vella
fi

log "esborrem directori '$BKNAME'..."
rm -rf $BKNAME
log 'Fet'
