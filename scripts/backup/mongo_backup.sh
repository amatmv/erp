#!/bin/sh
#Backup mongodb

MONGO_BACKUP_PATH='/home/erp/var/backups/mongo'
DBNAME='openerp'

if [ $# -gt 0 ] ; 
then
	DBNAME=$1
fi

# Si REMOTESERVER buit, no es copia per ssh
#REMOTESERVER="erp@backupserver"
REMOTEPATH='backups/arpydecal'
REMOTENAME="${DBNAME}_mongo.tgz"

SMB_SHARE="//server/share"
SMB_DIR="SMBDIR"
SMB_CREDENTIALS='/home/erp/conf/smbcredentials'

ZIPPER='gzip'

if [ -e ~/conf/backup.conf ]; then
 . ~/conf/backup.conf
fi

PATHBK=$MONGO_BACKUP_PATH
REMOTEPATH=$MONGOREMOTEPATH

command_exists() {
    command -v "$1" > /dev/null 2>&1
}

log() {
    echo '['`date +'%c'`']: '$1
}

command_exists pigz && ZIPPER='pigz'

if [ ! -d $PATHBK ]; then
    mkdir $PATHBK
fi

BKNAME=${PATHBK}/${DBNAME}
GZNAME=${PATHBK}/${DBNAME}.mongodb.gz

if [ -s $GZNAME ]; then
    log 'eliminem el fitxer antic'
    rm $GZNAME
fi

log 'iniciant bolcat '$DBNAME'...'
#No es pot comprimir directament :(
mongodump -d $DBNAME -o $PATHBK 2>&1 > /dev/null
log "compactant amb $ZIPPER..."
tar cvf - $BKNAME 2> /dev/null | $ZIPPER > $GZNAME
log 'Calculant md5...'
MD5SUM=`md5sum $GZNAME`
echo $MD5SUM
# SSH
if [ ${#REMOTESERVER} -gt 0 ] ; 
then
    log "Copiant a ${REMOTESERVER}..."
    REMOTEGZ=$REMOTEPATH/$REMOTENAME
    scp $GZNAME ${REMOTESERVER}:$REMOTEGZ
    log 'Calculant md5 remot...'
    MD5SUMREMOTE=`ssh ${REMOTESERVER} "md5sum $REMOTEGZ"`
    echo $MD5SUMREMOTE
fi
#SAMBA
if [ ${#SMB_SHARE} -gt 0 ] ;
then
    remote_file=`basename $GZNAME`
    log "smbclient $SMB_SHARE -A $SMB_CREDENTIALS -c \"cd $SMB_DIR; put $GZNAME $GZNAME\""
    smbclient $SMB_SHARE -A $SMB_CREDENTIALS -c "cd $SMB_DIR; put $GZNAME $remote_file"
fi

log "esborrem directori '$BKNAME'..."
rm -rf $BKNAME
log 'Fet'
