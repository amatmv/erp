#!/bin/bash
# DOC at http://wiki.gisce.lan/pages/viewpage.action?pageId=1867856

function is_mounted {
	# echoes 0 if $1 is not mounted and 1 if it is
	if [ -z "`mount | grep $1`" ]
	then
		echo 0
	else
		echo 1
	fi
}

function msg {
	echo "[`date +%Y-%m-%d\ %H:%M:%S`] $1"
}

function error {
	msg $1
	exit 1
}

msg "Comencem el backup"

MP=/media/copies
DISK=`ls -1 /dev/disk/by-label/* | grep "GISCE"`
ERP=/home/erp
DBNAME=Caldense

if [ $(is_mounted ${DISK}) -eq 0 ]
then
	# no està muntat, cal muntar-ho i comprovar que s'ha muntat
	msg "Muntem el disk"
	/sbin/mount.ntfs-3g -o rw ${DISK} ${MP}
	if [ $(is_mounted ${MP}) -eq 1 ]
	then
		msg "S'ha muntat el disk ${DISK} a ${MP}."
		
	else
		error "No s'ha pogut muntar ${DISK}. Mount error: $?"
	fi
fi

# comprovem si existeix la carpeta per avui
DAY=`date +%A`
if [ ! -x "${MP}/${DAY}" ]
then
	msg "Creem la carpeta per ${DAY}"
	mkdir ${MP}/${DAY}
fi

msg "Creem tarball de l'ERP"
TAR="${MP}/${DAY}/erp_`date +%Y%m%d`.tgz"
tar pPczf ${TAR} ${ERP}
msg "Còpia de l'ERP creada"
msg "Creem dump de la base de dades"
su postgres -c "pg_dump ${DBNAME} > ${MP}/${DAY}/${DBNAME}_`date +%Y%m%d`.sql"
msg "Dump de la base de dades creat"
msg "Desmuntem el disc"
umount ${MP}
msg "Disc desmuntat, sortim"

