#!/bin/bash
SCRIPT_PATH=`readlink -f $0`
SCRIPT_DIR=`dirname $SCRIPT_PATH`
source ${SCRIPT_DIR}/backup_funcs.sh #Creem les funcions mes habituals

DB_USER='erp'
BACKUP_PATH=/home/erp/backup_remot/
LOG_FILE=/home/erp/var/log/backup.log
PATH=/usr/bin:$PATH
data(){
  date "+%Y-%m-%d %H:%M:%S"
}
SMB_SHARE="//nastro32/myshare"
SMB_DIR="ERP-GISCE"
TOTAL_COPIES=4 #Assignem els valors per defecte a les variables

if [ -f ~/conf/backup.conf ]; #Pero si existeix el fitxer de configuracio les sobreescriurem alla
then
   source ~/conf/backup.conf
fi

data_fitxer=`date "+%Y%m%d%H%M%S"` >> ${LOG_FILE}

FITXER_ACTUAL=`ls -1 -lt ${BACKUP_PATH}/*.bz2 2> /dev/null` #Agafem la llista de copies de la base de dades
SETMANA=`echo ${FITXER_ACTUAL} | cut -d'_' -f2 | cut -d'.' -f1` #I busquem el numero de setmana de la copia mes recent
SETMANA=$(($SETMANA+1))
SETMANA_ACTUAL=$(($SETMANA%$TOTAL_COPIES)) #La setmana actual sera la seguent, tenint en compte el limit de copies

log "esborrem totes les copies de la ultima iteracio"
log "rm ${BACKUP_PATH}*${SETMANA_ACTUAL}.sql.bz2"
rm ${BACKUP_PATH}*${SETMANA_ACTUAL}.sql.bz2 #Esborrem la copia local de la base de dades que tenia la mateixa setmana (que sera la mes antiga)

FILENAME=${DB}-${data_fitxer}_${SETMANA_ACTUAL} #I decidim quin sera el nom del fitxer

log "setmana actual > ${SETMANA_ACTUAL}"
log "Dump base de dades"
log "pg_dump $DB | bzip2 > ${BACKUP_PATH}${FILENAME}.sql.bz2"
pg_dump $DB | bzip2 > ${BACKUP_PATH}${FILENAME}.sql.bz2 #Creem un arxiu comprimit de la base de dades amb el nom que hem decidit

if [ $REMOTESERVER ]; #Si hem donat un servidor ssh remot
then
  log "ssh ${REMOTESERVER} \"rm $REMOTEPATH/${DB}*_${SETMANA_ACTUAL}.sql.bz2\""
  ssh ${REMOTESERVER} "rm $REMOTEPATH/${DB}*_${SETMANA_ACTUAL}.sql.bz2" #Eliminem la copia remota mes antiga
  log "scp ${BACKUP_PATH}${FILENAME}.sql.bz2 $REMOTESERVER:$REMOTEPATH/$FILENAME"
  scp ${BACKUP_PATH}${FILENAME}.sql.bz2 $REMOTESERVER:$REMOTEPATH/$FILENAME.sql.bz2 #I passem la nova
fi

if [ $SMB_SHARE ]; #Si hem donat un servidor SAMBA remot
then
  log "smbclient $SMB_SHARE -A ${SMB_CREDENTIALS} -c \"cd $SMB_DIR; rm ${DB}*${SETMANA_ACTUAL}.sql.bz2; put ${BACKUP_PATH}${FILENAME}.sql.bz2  ${FILENAME}.sql.bz2\""
  smbclient $SMB_SHARE -A ${SMB_CREDENTIALS} -c "cd $SMB_DIR; rm ${DB}*${SETMANA_ACTUAL}.sql.bz2; put ${BACKUP_PATH}${FILENAME}.sql.bz2  ${FILENAME}.sql.bz2" #Eliminem la copia remota que toca i passem la nova
fi

log "Tasca de backup finalitzada correctament."
