#!/bin/bash
SCRIPT_PATH=`readlink -f $0`
SCRIPT_DIR=`dirname $SCRIPT_PATH`
source ${SCRIPT_DIR}/backup_funcs.sh

BACKUP_PATH=/home/erp/var/backups
LOG_FILE=/home/erp/var/log/backup.log
PATH=/usr/bin:$PATH
DB_USER='erp'
#SMB_DIR="ERP-GISCE"

# Si REMOTESERVER buit, no es copia per ssh
#REMOTESERVER="erp@backupserver"
REMOTEPATH='backups/arpydecal'

TOTAL_COPIES=4
data_fitxer=`date "+%Y%m%d%H%M%S"` 

ZIPPER='gzip'

if [ -e ~/conf/backup.conf ]; then
  source ~/conf/backup.conf
fi

command_exists pigz && ZIPPER='pigz'

DBS=`get_dbs $DB_USER`

for DB in $DBS ;
do
  log " -- ${DB} --"
  if [ ! -d ${BACKUP_PATH}/${DB} ]; then 
    log "Creant carpeta inexistent '${BACKUP_PATH}/${DB}'"
    mkdir -p ${BACKUP_PATH}/${DB} ; 
  fi
  FITXER_ACTUAL=`ls -1 ${BACKUP_PATH}/${DB}/${DB}*_[0-9].sql.gz`
  SETMANA=`basename ${FITXER_ACTUAL} | cut -d'.' -f1 | awk -F_ '{print $NF}'`
  log "FITXER_ACTUAL: $FITXER_ACTUAL SETMANA (inicial): $SETMANA"
  if [ -z $SETMANA ]; then SETMANA=0 ; fi
  SETMANA=$(($SETMANA+1))
  SETMANA_ACTUAL=$(($SETMANA%$TOTAL_COPIES))
  FILENAME=${DB}-${data_fitxer}_${SETMANA_ACTUAL}

  log "SETMANA: $SETMANA SETMANA_ACTUAL: $SETMANA_ACTUAL"
  log "setmana actual > ${SETMANA_ACTUAL}"
  log "Dump base de dades"
  GZNAME=${BACKUP_PATH}/${DB}/${FILENAME}.sql.gz
  log "pg_dump $DB | $ZIPPER > ${GZNAME}"
  pg_dump $DB | $ZIPPER > $GZNAME
  log 'Calculant md5...'
  MD5SUM=`md5sum $GZNAME`
  log $MD5SUM
  if [ -n ${REMOTESERVER} ] ; 
  then
    log "Esborrant fitxer antic..."
    ssh ${REMOTESERVER} "rm $REMOTEPATH/${DB}*_${SETMANA_ACTUAL}.sql.gz"
    log "Copiant a ${REMOTESERVER}..."
    REMOTEGZ=$REMOTEPATH/${FILENAME}.sql.gz
    scp $GZNAME ${REMOTESERVER}:$REMOTEGZ
    log 'Calculant md5 remot...'
    MD5SUMREMOTE=`ssh ${REMOTESERVER} "md5sum $REMOTEGZ"`
    log $MD5SUMREMOTE
  fi
  log "Tasca de backup '$DB' finalitzada correctament."
  TOTAL=`ls -1 ${BACKUP_PATH}/${DB}/*.sql.gz | wc -l`
  if [[ $TOTAL -gt 1 ]]; then
    for x in `find ${BACKUP_PATH}/${DB} -name "${DB}-*.sql.gz" -type f -exec ls -1rt "{}" +`; do log "esborrant la copia ${x}"; rm $x; break; done;
  fi
done
log Fi
