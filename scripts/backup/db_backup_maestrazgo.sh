#!/bin/bash
BACKUP_PATH=/home/erp/backup_remot/
LOG_FILE=/home/erp/var/log/backup.log
DB=Maestrazgo
PATH=/usr/bin:$PATH
data(){
  date "+%Y-%m-%d %H:%M:%S"
}
SMB_SHARE="//nastro32/myshare"
SMB_DIR="ERP-GISCE"
TOTAL_COPIES=4
data_fitxer=`date "+%Y%m%d%H%M%S"` >> ${LOG_FILE}

FITXER_ACTUAL=`ls -1 ${BACKUP_PATH}*.bz2`
SETMANA=`echo ${FITXER_ACTUAL} | cut -d'_' -f3 | cut -d'.' -f1`
SETMANA=$(($SETMANA+1))
SETMANA_ACTUAL=$(($SETMANA%$TOTAL_COPIES))
FILENAME=${DB}-${data_fitxer}_${SETMANA_ACTUAL}

log(){
   echo "[$(data)] $1"
   echo "[$(data)] $1" >> ${LOG_FILE}
}

log "setmana actual > ${SETMANA_ACTUAL}"
log "Dump base de dades"
log "pg_dump $DB | bzip2 > ${BACKUP_PATH}${FILENAME}.sql.bz2"
pg_dump $DB | bzip2 > ${BACKUP_PATH}${FILENAME}.sql.bz2
log "smbclient $SMB_SHARE --user=admin%admin -c \"cd $SMB_DIR; rm *${SETMANA_ACTUAL}.sql.bz2; put ${BACKUP_PATH}${FILENAME}.sql.bz2  ${FILENAME}.sql.bz2\""
smbclient $SMB_SHARE --user=admin%admin -c "cd $SMB_DIR; rm *${SETMANA_ACTUAL}.sql.bz2; put ${BACKUP_PATH}${FILENAME}.sql.bz2  ${FILENAME}.sql.bz2"

log "Tasca de backup finalitzada correctament."
TOTAL=`ls -1 ${BACKUP_PATH}*.bz2 | wc -l`
if [[ $TOTAL -gt 1 ]]; then
 for x in `find ${BACKUP_PATH} -type f -exec ls -1rt "{}" +`; do log "esborrant la copia ${x}"; rm $x; break; done;
fi

