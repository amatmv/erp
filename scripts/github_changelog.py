#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Parse GitHub issues to generate a changelog

Hem de fer pipe de la comanda:
 http -b -a USER:PASSWORD GET \
 "https://api.github.com/repos/gisce/erp/issues?milestone=14&state=closed"

"""
import json
import sys
import codecs
from textwrap import wrap

sys.stdout = codecs.getwriter('utf8')(sys.stdout)

jdata = json.loads(sys.stdin.read())

def pout(msg):
    sys.stdout.write(msg)
    sys.stdout.flush()
    

milestone_closed_at = max([i['closed_at'] for i in jdata])
milestone = jdata[0]['milestone']
title = "%s - (%s)" % (milestone['title'], milestone_closed_at.split('T')[0])
pout("%s\n" % title)
pout("-" * len(title) + "\n")

for issue in jdata:
    if issue['labels']:
        labels = ' - (%s) ' % ', '.join([l['name'] for l in issue['labels']])
    else:
        labels = ''
    pout("* %s%s - [%s]\n" % (issue['title'], labels, issue['html_url']))
    if issue['body']:
        pout("%s\n" % "\n".join(wrap(issue['body'], 80, initial_indent="    ",
                                   subsequent_indent="    ")))
    if issue['pull_request']:
        pout("    Diff: %s\n" % issue['pull_request']['diff_url'])
