#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ooop import OOOP
import csv
import os
import xmlrpclib
from datetime import datetime
O = OOOP(dbname=os.getenv('OOOP_DBNAME', 'oerp5_comer'),
         port=int(os.getenv('OOOP_PORT', '8050')),
         user='sync',
         pwd='sync',
         debug=False)
CSV_FILE = os.getenv('CSV_FILE', '/tmp/aspirabon5.csv')
ERR_FILE = '%s.err' % CSV_FILE
READER = csv.reader(open(CSV_FILE), delimiter=';')
if int(os.getenv('START_LINE', 0)):
    ERROR = open(ERR_FILE, 'a+')
else:
    ERROR = open(ERR_FILE, 'w')
PROCESSADES = 0
ROW_FIELDS = 80

def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))

def _get_proxy(model):
    """Retorna un objecte OOOP pel model.
    """
    return getattr(O, model2cc(model))

def create_or_update(model, vals, **kwargs):
    """Creem o actualitzem segons faci o no faci match.
    """
    proxy = _get_proxy(model)
    results = proxy.filter(**kwargs)
    msg = ""
    start = datetime.now()
    if len(results):
        msg += "Updating"
        obj = results[0]
    else:
        msg += "Creating"
        obj = proxy.new()
    for key, value in vals.items():
        setattr(obj, key, value)
    obj.save()
    end = datetime.now()
    msg += " %s (id: %i)" % (model, obj.id)
    print msg
    print "Time: %.3fs." % ((end - start).seconds +
                             float((end - start).microseconds) / 10**6)
    return obj.id

def get_id(model, **kwargs):
    """Retorna l'id segons el match.
    """
    proxy = _get_proxy(model)
    results = proxy.filter(**kwargs)
    if not len(results):
        raise Exception("No s'ha trobat l'id del model: %s amb match: %s"
                        % (model, kwargs))
    return results[0].id

def parse_row(row):
    """Processa la fila.
    """
    # Stripping all values
    row = map(str.strip, row)
    id_vals = {}
    # Tipus de via
    model = 'res.tipovia'
    vals = {
        'codi': row[4],
        'abr': row[4]+'.',
        'name': row[5]
    }
    id_vals['tv'] = create_or_update(model, vals, codi=vals['codi'])
    # Creem provincia desconeguda
    vals = {
        'name': 'DESCON.',
        'code': '00',
        'country_id': _get_proxy('res.country').get(get_id('res.country',
                                                           code='ES')) 
    }
    create_or_update('res.country.state', vals, code=vals['code'])
    if not row[2].strip():
        # Creem un municipi desconegut
        vals = {
            'name': 'DESCON.',
            'ine': '00000',
            'state': _get_proxy('res.country.state').get(get_id('res.country.state', code='00'))
        }
        id_vals['id_municipi'] = create_or_update('res.municipi', vals,
                                                  ine=vals['ine'])
    else:
        id_vals['id_municipi'] = get_id('res.municipi', ine=row[2].zfill(5))
    # Distribuidora
    if not row[1].strip():
        row[1] = row[0][3:6]
    id_vals['distribuidora_id'] = get_id('res.partner', ref=row[1].zfill(4))
    # Cups
    #0     CODI
    #1     DISTRIBUIDORA
    #2     MUNICIPI
    #3     CODI TIPUS DE VIA
    #4     ABBR TIPUS DE VIA
    #5     TIPUS DE VIA
    #6     CARRER
    #7     NUMERO
    #8     ESCALA
    #9     PIS
    #10    PORTA
    #11    REF CATASTRAL
    #12    ACLARADOR
    #13    POLIGON
    #14    PARCELA
    #15    CODI POSTAL
    if row[6].startswith(row[4]):
        row[6] = row[6][len(row[4])+1:]
    vals = {
        'name': row[0],
        'distribuidora_id': _get_proxy('res.partner').get(
                                                   id_vals['distribuidora_id']),
        'id_municipi': _get_proxy('res.municipi').get(id_vals['id_municipi']),
        'tv': _get_proxy('res.tipovia').get(id_vals['tv']),
        'nv': row[6],
        'pnp': row[7],
        'es': row[8],
        'pt': row[9],
        'pu': row[10],
        'ref_catastral': row[12],
        'aclarador': row[11],
        'cpo': row[13],
        'cpa': row[14],
        'dp': row[15],
    }
    id_vals['cups'] = create_or_update('giscedata.cups.ps', vals,
                                       name=vals['name'])
    # Titular
    #16    NOM
    #17    IDIOMA
    #18    NIF
    #19    CODI
    model = 'res.partner'
    vals = {
        'name': row[16],
        'lang': row[17],
        'vat': 'ES'+row[18],
        'ref': row[19],
    }
    if vals['ref'] == 'auto':
        vals['ref'] = False
    try:
        id_vals['titular'] = create_or_update(model, vals, name=vals['name'],
                                              vat=vals['vat'])
    except:
        # Provem sense el NIF
        vals['vat'] = False
        vals['comment'] = row[18]
        id_vals['titular'] = create_or_update(model, vals, name=vals['name'])
    # Direcció titular
    #20    NOM
    #21    DIRECCIÓ
    #22    CODI POSTAL
    #23    CIUTAT
    #24    PAÍS
    #25    PROVÍNCIA
    #26    EMAIL
    #27    TELÈFON
    #28    FAX
    #29    MÒBIL
    if not row[24].strip():
        row[24] = 'ES'
    if not row[25].strip():
        row[25] = '00'
    model = 'res.partner.address'
    vals = {
        'partner_id': _get_proxy('res.partner').get(id_vals['titular']),
        'name': row[20],
        'street': row[21],
        'zip': row[22],
        'city': row[23],
        'country_id': _get_proxy('res.country').get(get_id('res.country',
                                                           code=row[24])),
        'state_id': _get_proxy('res.country.state').get(
                                                    get_id('res.country.state',
                                                           code=row[25].zfill(2))),
        'email': row[26],
        'phone': row[27],
        'fax': row[28],
        'mobile': row[29]
    }
    id_vals['direccio_pagament'] = create_or_update(model, vals,
                                             partner_id=vals['partner_id'].id,
                                             name=vals['name'],
                                             street=vals['street'])
    # Tipus de cobrament
    #model = 'payment.type'
    if row[59].strip():
        vals = {
            'name': row[59],
            'code': row[59].upper()
        }
        id_vals['cobro'] = create_or_update('payment.type', vals,
                                            code=vals['code'])
    else:
        id_vals['cobro'] = False
    # Pagador
    #30    NOM
    #31    IDIOMA
    #32    NIF
    #33    CODI
    #34    COBRO
    if not row[30].strip():
        id_vals['pagador'] = id_vals['titular']
    else:
        vals = {
            'name': row[30],
            'lang': row[31],
            'vat': 'ES'+row[32],
            'ref': row[33],
            'payment_type_customer': _get_proxy('payment.type').get(
                                                            id_vals['cobro'])
        }
        if vals['ref'] == 'auto':
            vals['ref'] = False
        try:
            id_vals['pagador'] = create_or_update('res.partner', vals,
                                                  name=vals['name'],
                                                  vat=vals['vat'])
        except:
            # Provem sense el NIF
            vals['vat'] = False
            vals['commnet'] = row[32]
            id_vals['pagador'] = create_or_update('res.partner', vals,
                                                  name=vals['name'])
    # Direcció pagament
    #35    NOM
    #36    DIRECCIÓ
    #37    CODI POSTAL
    #38    CIUTAT
    #39    PAÍS
    #40    CODI PROVÍNCIA
    #41    PROVINCIA
    #42    EMAIL
    #43    TELÈFON
    #44    FAX
    #45    MÒBIL
    model = 'res.partner.address'
    if not row[39].strip():
        row[39] = 'ES'
    if not row[40].strip():
        row[40] = '00'
    vals = {
        'partner_id': _get_proxy('res.partner').get(id_vals['pagador']),
        'name': row[35],
        'street': row[36],
        'zip': row[37],
        'city': row[38],
        'country_id': _get_proxy('res.country').get(get_id('res.country',
                                                        code=row[39].zfill(2))),
        'state_id': _get_proxy('res.country.state').get(
                                                    get_id('res.country.state',
                                                        code=row[40].zfill(2))),
        'email': row[42],
        'phone': row[43],
        'fax': row[44],
        'mobile': row[45]
    }
    id_vals['direccio_pagament'] = create_or_update(model, vals,
                                             partner_id=vals['partner_id'].id,
                                             name=vals['name'],
                                             street=vals['street'])
    # Direcció notificació
    #46    NOM
    #47    DIRECCIÓ
    #48    CODI POSTAL
    #49    CIUTAT
    #50    PAÍS
    #51    CODI PROVÍNCIA
    #52    PROVÍNCIA
    #53    EMAIL
    #54    TELÈFON
    #55    FAX
    #56    MOBILE
    model = 'res.partner.address'
    if not row[50].strip():
        row[50] = 'ES'
    if not row[51].strip():
        row[51] = '00'
    vals = {
        'name': row[46],
        'street': row[47],
        'zip': row[48],
        'city': row[49],
        'country_id': _get_proxy('res.country').get(get_id('res.country',
                                                           code=row[50])),
        'state_id': _get_proxy('res.country.state').get(
                                                    get_id('res.country.state',
                                                        code=row[51].zfill(2))),
        'email': row[53],
        'phone': row[54],
        'fax': row[55],
        'mobile': row[56]
    }
    # Posem a quin partner va la direcció de notificació
    if row[60] not in ('P', 'T', 'A'):
        raise Exception("El valor del la persona a notifcar no és vàlida."
                        "Només pot ser P, T o A")
    if row[60] == 'P':
        vals['partner_id'] = _get_proxy('res.partner').get(id_vals['pagador'])
    elif row[60] == 'T':
        vals['partner_id'] = _get_proxy('res.partner').get(id_vals['titular'])
    else:
        id_vals['altre_p'] = create_or_update('res.partner',
                                              {'name': vals['name']},
                                              name=vals['name'])
        vals['partner_id'] = _get_proxy('res.partner').get(id_vals['altre_p'])
    id_vals['notificacio_id'] = create_or_update(model, vals,
                                            partner_id=vals['partner_id'].id,
                                            name=vals['name'],
                                            street=vals['street'])
    # Bank
    #69    DESCRIPCIÓ
    #70    NÚMERO DE CC
    model = 'res.partner.bank'
    vals = {
        'name': row[69],
        'acc_number': row[70],
        'state': 'bank',
        'country_id': _get_proxy('res.country').get(get_id('res.country',
                                                           code=row[50])),
        'state_id': _get_proxy('res.country.state').get(
                                                    get_id('res.country.state',
                                                        code=row[51].zfill(2))),
        'partner_id': _get_proxy('res.partner').get(id_vals['pagador']),
        'acc_country_id': _get_proxy('res.country').get(get_id('res.country',
                                                               code=row[50]))
    }
    proxy = _get_proxy('res.partner.bank')
    n_vals = proxy.onchange_banco([], vals['acc_number'],
                                  vals['acc_country_id'].id, False)
    if n_vals.get('warning', False):
        print n_vals['warning']
    else:
        vals.update(n_vals['value'])
        if vals.has_key('bank'):
            vals['bank'] = _get_proxy('res.bank').get(vals['bank'])
        id_vals['bank'] = create_or_update(model, vals,
                                           acc_number=vals['acc_number'])
    vals = {
        'name': row[34],
        'bank_id': False,
        'journal': _get_proxy('account.journal').get(get_id('account.journal',
                                                            code='CAJA')),
        'type': _get_proxy('payment.type').get(get_id('payment.type',
                                                      code='CAJA')),
    }
    id_vals['payment_mode'] = create_or_update('payment.mode', vals,
                                               name=vals['name'])
    # Contracte
    #57    NÚM. CONTRACTE
    #58    REF DISTRIBUIDORA
    #59    TIPUS PAGAMENT
    #60    PERSONA NOTIFICACIÓ
    #61    TERMINIS DE PAGAMENT
    #62    TARIFA COMER.
    #63    TARIFA ACCES
    #64    POTENCIA
    #65    TENSIO NORM.
    #66    DATA ALTA
    #67    TIPUS FACTURACIO
    #68    PRÒXIMA FACTURACIÓ
    # TODO: Terminis de pagament
    model = 'giscedata.polissa'
    if row[68].strip():
        proxima_facturacio = datetime.strptime(row[68],
                                               '%d/%m/%Y').strftime('%Y-%m-%d')
    else:
        proxima_facturacio = False
    if row[66].strip():
        spc = '-' in row[66] and '-' or '/'
        year = row[66].split(spc)[2]
        if len(year) < 4:
            data_alta = datetime.strptime(row[66],
                                    '%d'+spc+'%m'+spc+'%y').strftime('%Y-%m-%d')
        else:
            data_alta = datetime.strptime(row[66],
                                    '%d'+spc+'%m'+spc+'%Y').strftime('%Y-%m-%d')
    else:
        data_alta = False
    
    data_baixa = False
    if row[79].strip():
        spc = '-' in row[79] and '-' or '/'
        year = row[79].split(spc)[2]
        if len(year) < 4:
            data_baixa = datetime.strptime(row[79],
                                '%d'+spc+'%m'+spc+'%y').strftime('%Y-%m-%d')
        else:
            data_baixa = datetime.strptime(row[79],
                                '%d'+spc+'%m'+spc+'%Y').strftime('%Y-%m-%d')
                                
    data_firma_contracte = False
    if row[78].strip():
        spc = '-' in row[78] and '-' or '/'
        year = row[78].split(spc)[2]
        if len(year) < 4:
            data_firma_contracte = datetime.strptime(row[78],
                                '%d'+spc+'%m'+spc+'%y').strftime('%Y-%m-%d')
        else:
            data_firma_contracte = datetime.strptime(row[78],
                                '%d'+spc+'%m'+spc+'%Y').strftime('%Y-%m-%d')
    
    notificacio = {
        'P': 'pagador',
        'T': 'titular',
        'A': 'altre_p',
    }
    vals = {
        'name_auto': 0,
        'name': row[57],
        'ref_dist': row[58],
        'tipo_pago': id_vals['cobro'] and _get_proxy('payment.type').get(
                                                            id_vals['cobro']),
        'llista_preu': row[62].strip() and _get_proxy('product.pricelist').get(
                                                    get_id('product.pricelist',
                                                name=row[62].strip())) or False,
        'tarifa': row[63].strip() and _get_proxy(
                                            'giscedata.polissa.tarifa').get(
                                            get_id('giscedata.polissa.tarifa',
                                                   name=row[63])),
        'potencia': float(row[64].strip() or 0.0),
        'tensio_normalitzada': row[65].strip() and _get_proxy(
                                            'giscedata.tensions.tensio').get(
                                            get_id('giscedata.tensions.tensio',
                                                   name=row[65])),
        'data_alta': data_alta,
        'facturacio': row[67] and int(row[67]) or 2,
        'proxima_facturacio': proxima_facturacio or False,
        'lot_facturacio': proxima_facturacio and \
        _get_proxy('giscedata.facturacio.lot').get(
                                get_id('giscedata.facturacio.lot',
                                       data_inici__lte=proxima_facturacio,
                                       data_final__gte=proxima_facturacio)),
        'pagador': _get_proxy('res.partner').get(id_vals['pagador']),
        'titular': _get_proxy('res.partner').get(id_vals['titular']),
        'notificacio': notificacio.get(row[60], False),
        'direccio_notificacio': _get_proxy('res.partner.address').get(
                                                    id_vals['notificacio_id']),
        'cups': _get_proxy('giscedata.cups.ps').get(id_vals['cups']),
        'distribuidora': _get_proxy('res.partner').get(
                                                id_vals['distribuidora_id']),
        'direccio_pagament': _get_proxy('res.partner.address').get(
                                                id_vals['direccio_pagament']),
        'bank': row[59] == 'RECIBO_CSB' and \
        _get_proxy('res.partner.bank').get(id_vals['bank']) or False,
        # TODO: Camp CNE
        'cnae': _get_proxy('giscemisc.cnae').get(get_id('giscemisc.cnae',
                                                        name='639')),
        'data_firma_contracte': data_firma_contracte,
        'data_baixa': data_baixa,
        'payment_mode_id': _get_proxy('payment.mode').get(id_vals['payment_mode'])
        
    }
    if row[62].strip():
        vals['versio_primera_factura'] = _get_proxy(
                                            'product.pricelist.version').get(
                                            get_id('product.pricelist.version',
                                        pricelist_id=vals['llista_preu'].id))
    if vals['notificacio'] == 'altre_p':
        vals['altre_p'] = id_vals.get('altre_p', False)
    if vals['potencia'] > 15:
        vals['facturacio_potencia'] = 'max'
    else:
        vals['facturacio_potencia'] = 'icp'
    if vals['pagador'].id != vals['titular'].id:
        vals['pagador_sel'] = 'altre_p'
    else:
        vals['pagador_sel'] = 'titular'
    id_vals['polissa'] = create_or_update(model, vals, name=vals['name'],
                                          active__in=('True', 'False'))
    #Activem la pòlissa si la data d'alta és diferent a 1910-01-01
    polissa = _get_proxy('giscedata.polissa').get(id_vals['polissa'])
    if vals['data_baixa']:
        polissa.send_signal('baixa')
    elif vals['data_alta'] != '1910-01-01':
        polissa.activar_xmlrpc()
    """
    # Actualització de modificacions contractuals fins que la data de baixa
    # de la modificació contractual activa sigui més gran que now()
    polissa = _get_proxy('giscedata.polissa').get(id_vals['polissa'])
    data_final = polissa.modcontractuals_ids[0].data_final
    print "Renovant contracte"
    start = datetime.now()
    while data_final <= datetime.now().strftime('%Y-%m-%d'):
        polissa.modcontractuals_ids[0].renovar()
        polissa = _get_proxy('giscedata.polissa').get(id_vals['polissa'])
        data_final = polissa.modcontractuals_ids[0].data_final
    print "Time: %is." % (start - datetime.now()).seconds
    """
    # Comptador
    #71    NÚMERO DE SÉRIE
    #72    PROPIETAT
    #73    PREU LLOGUER
    #74    DESCRIPCIO LLOGUER
    #75    DATA ALTA
    #76    DATA BAIXA
    #77    GIRO
    polissa = _get_proxy('giscedata.polissa').get(id_vals['polissa'])
    if polissa.state == 'activa':
        model = 'giscedata.lectures.comptador'
        data_alta = False
        if row[75].strip():
            spc = '-' in row[75] and '-' or '/'
            year = row[75].split(spc)[2]
            if len(year) < 4:
                data_alta = datetime.strptime(row[75],
                                    '%d'+spc+'%m'+spc+'%y').strftime('%Y-%m-%d')
            else:
                data_alta = datetime.strptime(row[75],
                                    '%d'+spc+'%m'+spc+'%Y').strftime('%Y-%m-%d')
        data_baixa = False
        if row[76].strip():
            spc = '-' in row[76] and '-' or '/'
            year = row[76].split(spc)[2]
            if len(year) < 4:
                data_baixa = datetime.strptime(row[76],
                                    '%d'+spc+'%m'+spc+'%y').strftime('%Y-%m-%d')
            else:
                data_baixa = datetime.strptime(row[76],
                                    '%d'+spc+'%m'+spc+'%Y').strftime('%Y-%m-%d')
        if row[72] == 'cliente':
            row[72] = 'client'
        vals = {
            'name': row[71] or '-'+row[57],
            'propietat': row[72],
            'lloguer': row[73] and row[72] != 'client' and 1 or 0,
            'descripcio_lloguer': row[74].strip(),
            'preu_lloguer': row[73] and float(row[73]) or 0.0,
            'data_alta': data_alta,
            'data_baixa': data_baixa,
            'giro': row[77] and 10**int(row[77]) or 0,
            'polissa': _get_proxy('giscedata.polissa').get(id_vals['polissa']),
        }
        id_vals['comptador'] = create_or_update('giscedata.lectures.comptador',
                                                vals, name=vals['name'],
                                                polissa=polissa.name)
    #78    data firma contracte
    #79    data de baixa
    
def print_row(row):
    """Print row.
    """
    i = 0
    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    for value in row:
        print '%i: %s' % (i, value)
        i += 1
    print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    print "Line: %i" % READER.line_num

def write_error(row, comment=None):
    """Write error to a file.
    """
    ERROR.write('# Line %i - %s\n' % (READER.line_num, comment or ''))
    ERROR.write(';'.join(row)+'\n')
    ERROR.flush()

def main():
    """Funció principal de l'script.
    """
    global PROCESSADES
    for row in READER:
        if READER.line_num < int(os.getenv('START_LINE', 0)):
            continue
        if row[0].startswith('#'):
            continue
        if len(row) != ROW_FIELDS:
            print "!!! %i != %i" % (len(row), ROW_FIELDS)
            write_error(row, comment="Longitud: %i diferent a %i;" %
                        (ROW_FIELDS, len(row)))
            continue
        try:
            time_start = datetime.now()
            print_row(row)
            parse_row(row)
            PROCESSADES += 1
            time_end = datetime.now()
            print "Time elapsed: %.3fs." % ((time_end - time_start).seconds +
                            float((time_end - time_start).microseconds) / 10**6)
        except xmlrpclib.Fault, fault:
            print "Exception! %s" % fault.faultString
            write_error(row)
        except Exception, ecx:
            print "Exception! %s" % ecx
            write_error(row)
        finally:
            if int(os.getenv('STEP_BY_STEP', 0)):
                print "Prem intro per continuar ..."
                raw_input()

if __name__ == "__main__":
    try:
        start_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        main()
    except KeyboardInterrupt:
        print
        print 'Shutting Down...'
    finally:
        end_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print "Start at %s" % start_date
        print "End at %s" % end_date
        print "Processed: %i" % PROCESSADES
        ERROR.close()
