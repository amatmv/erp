#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Aquí migrarem tots els camps de facturació que tenen les pòlisses.
"""
import sys
import netsvc
logger = netsvc.Logger()
import tools
tools.config['addons_path'] = sys.argv[1]
tools.config['db_name'] = sys.argv[2]
import os
from progressbar import ProgressBar, ETA, Percentage, Bar
import pooler
from tools import cache, config
import osv
import workflow
import report
import service



# Importem el model antic
ad = os.path.abspath('%s/giscedata_facturacio/migrations'
                     % config['addons_path'])
sys.path.insert(1, ad)
from old_objects import giscedata_factura
#sys.path.remove(ad)

MAGS = {'activa': 'AE', 'reactiva': 'R1'}

OLD_BOES = {
    '2008-12-31': '2007-12-29',
    '2009-06-30': '2008-12-31',
}


def perr(msg):
    sys.stderr.write(msg)
    sys.stderr.flush()


def pout(msg):
    sys.stdout.write(msg)
    sys.stdout.flush()


@cache()
def get_periode(fck, cursor, uid, date):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    p_obj = pool.get('account.period')
    return p_obj.find(cursor, uid, date)[0]


@cache()
def get_default_account(fck, cursor, uid):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    acc_obj = pool.get('account.account')
    return acc_obj.search(cursor, uid, [('code', '=', '700000')])[0]


@cache()
def get_productes(fck, cursor, uid, tarifa, tipus):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    reconv = {'energia': 'te',
              'reactiva': 'tr',
              'potencia': 'tp',
              'exces_potencia': 'ep'}
    if tipus not in reconv:
        return {}
    tar_obj = pool.get('giscedata.polissa.tarifa')
    return tar_obj.get_periodes_producte(cursor, uid, tarifa, reconv[tipus])


@cache()
def get_date_boe(fck, cursor, uid, date):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    ftor_obj = pool.get('giscedata.facturacio.facturador')
    try:
        return ftor_obj.get_data_boe(cursor, uid, date)
    except Exception, e:
        for date_end in sorted(OLD_BOES):
            if date_end >= date:
                return OLD_BOES[date_end]
        raise e


@cache()
def get_lot(fck, cursor, uid, data):
    if not data:
        return False
    lot_obj = pooler.get_pool(cursor.dbname).get('giscedata.facturacio.lot')
    search_params = [('data_final', '<=', data),
                     ('data_final', '>=', data)]
    lots_ids = lot_obj.search(cursor, uid, search_params)
    if lots_ids:
        return lots_ids[0]
    return False


@cache()
def get_periode_liquidacio(fck, cursor, uid, data):
    liq_obj = pooler.get_pool(cursor.dbname).get('giscedata.liquidacio.fpd')
    search_params = [('inici', '<=', data),
                     ('final', '>=', data)]
    lots_ids = liq_obj.search(cursor, uid, search_params)
    if lots_ids:
        return lots_ids[0]
    return False


@cache()
def get_journal(fck, cursor, uid, tipo_rect):
    jou_obj = pooler.get_pool(cursor.dbname).get('account.journal')
    code = 'ENERGIA'
    if tipo_rect != 'N':
        code += '.%s' % tipo_rect
    return jou_obj.search(cursor, uid, [('code', '=', code)])[0]


@cache()
def get_potencia_uom(fck, cursor, uid, tipus):
    uom_obj = pooler.get_pool(cursor.dbname).get('product.uom')
    UOMS = {
        'potencia': 'kW/mes',
        'energia': 'kWh',
        'reactiva': 'kVArh',
        'exces_potencia': 'kW',
        'lloguer': 'ALQ/mes',
        'altres': 'Unidad'
    }
    return uom_obj.search(cursor, uid, [('name', '=',  UOMS[tipus])])[0]


def migrate(cursor_, installed_version):
    # Actualitzem a totes la llista de preu a TARIFAS ELECTRICIDAD i a
    # la primera versió que trobem després després de la data d'alta
    uid = 1
    pool = pooler.get_pool(cursor_.dbname)
    imb_obj = pool.get('ir.model.data')
    imd_id = imb_obj._get_id(cursor_, uid, 'giscedata_facturacio',
                             'pricelist_tarifas_electricidad')
    llista_preu = imb_obj.read(cursor_, uid, imd_id, ['res_id'])['res_id']
    pol_obj = pool.get('giscedata.polissa')
    fac_obj = pool.get('giscedata.facturacio.factura')
    le_obj = pool.get('giscedata.facturacio.lectures.energia')
    lp_obj = pool.get('giscedata.facturacio.lectures.potencia')
    lf_obj = pool.get('giscedata.facturacio.factura.linia')
    pool.instanciate('giscedata_facturacio_old', cursor_)
    fold_obj = pool.get('giscedata.factura')
    cur_obj = pool.get('res.currency')
    config_obj = pool.get('res.config')
    invoice_obj = pool.get('account.invoice')

    # Factura
    cursor_.execute("""SELECT
    f.id as id,
    f.tax as tax,
    f.name as number,
    f.factura as invoice_id,
    False as llista_preu,
    f.polissa as polissa_id,
    f.tarifa as tarifa_acces_id,
    coalesce(max(lp.pot_contract), 0) as potencia,
    False as cups_id,
    greatest(c.data_alta, f.data_inici) as data_inici,
    least(c.data_baixa, f.data_final) as data_final,
    f.data_final as data_final_old,
    f.tipo_factura as tipo_factura,
    f.tipo_rectificadora as tipo_rectificadora,
    f.tipo_facturacion as tipo_facturacion,
    False as date_boe,
    f.facturacio as facturacio,
    False as lot_facturacio,
    f.ref as ref,
    f.comptador as comptador_id,
    c.giro as giro
    from giscedata_factura f,
    giscedata_lectures_comptador c,
    giscedata_factura_lectura_potencia lp
    where f.comptador = c.id
    and lp.factura = f.id
    and f.name not in (
        select number from account_invoice ai inner join account_journal j
        on (ai.journal_id = j.id and j.code ilike 'ENERGIA%')
    )
    group by f.id, f.tax, f.name, f.factura, f.polissa, f.tarifa, c.data_alta,
    f.data_inici, data_inici, c.data_baixa, f.data_final, data_final,
    f.data_final, f.tipo_factura, f.tipo_rectificadora, f.tipo_facturacion,
    f.facturacio, f.ref, f.comptador, c.giro
    order by f.id desc, f.polissa asc""")
    factures = cursor_.dictfetchall()
    pout("* Queden %s factures per migrar..\n" % len(factures))
    # Com que el procés anirà per lots hem de comprovar que la moneda amb
    # precisió 4 ja no estigui creada
    db = pooler.get_db_only(cursor_.dbname)
    ccursor = db.cursor()
    cur_id = cur_obj.search(ccursor, uid, [('code', '=', 'EUR')])[0]
    new_cur_id = cur_obj.search(ccursor, uid, [('name', '=', 'EUR4')])
    if new_cur_id:
        new_cur_id = new_cur_id[0]
        pout("* Utilizem EUR4 (id:%s)\n" % new_cur_id)
    else:
        vals = {
            'name': 'EUR4',
            'rounding': '0.0001'
        }
        new_cur_id = cur_obj.copy(ccursor, uid, cur_id, vals)
        pout("* Creem EUR4 (id:%s)\n" % new_cur_id)
    ccursor.commit()
    ccursor.close()
    _hrd = config_obj.get(cursor_, uid, 'giscedata.factura.round2.date')
    pout("* Data de tall: %s\n" % _hrd)
    if factures:
        total = len(factures)
        widgets = [Percentage(), ' ', Bar(), ' ', ETA()]
        pbar = ProgressBar(widgets=widgets, maxval=total).start()
        done = 0

        for factura in factures:
            done += 1
            cursor = db.cursor()
            # Eliminem l'id
            try:
                old_fid = factura['id']
                del factura['id']

                number = factura['number']
                del factura['number']

                comptador_id = factura['comptador_id']
                del factura['comptador_id']

                giro = factura['giro']
                del factura['giro']

                ref = factura['ref']
                del factura['ref']

                tax = factura['tax']
                del factura['tax']

                data_final_old = factura['data_final_old']
                del factura['data_final_old']

                if data_final_old >= _hrd:
                    cur_id = cur_id
                else:
                    cur_id = new_cur_id

                factura.update({
                    'currency_id': cur_id,
                    'llista_preu': llista_preu,
                    'cups_id': pol_obj.browse(cursor, uid,
                                              factura['polissa_id']).cups.id,
                    'date_boe': get_date_boe(False, cursor, uid, factura['data_final']),
                    'lot_facturacio': get_lot(False, cursor, uid, factura['data_final']),
                    'periode_liquidacio': get_periode_liquidacio(False, cursor, uid,
                                                                 factura['data_final'])
                })
                if int(factura['facturacio']) not in (1, 2):
                    factura['facturacio'] = 2
                if factura['tipo_rectificadora'] in ('B', 'A'):
                    factura['type'] = 'out_refund'
                else:
                    factura['type'] = 'out_invoice'
                fid = fac_obj.create(cursor, uid, factura.copy())

                update_vals = {
                    'number': number,
                    'journal_id': get_journal(False, cursor, uid,
                                              factura['tipo_rectificadora'])
                }
                fac_obj.write(cursor, uid, [fid], update_vals)

                # Lectures d'energia
                cursor.execute("""SELECT
                name,
                False as comptador_id,
                False as factura_id,
                tipus,
                False as magnitud,
                data_actual,
                lect_actual,
                data_anterior,
                lect_anterior,
                False as consum
                from giscedata_factura_lectura_energia
                where factura = %s""", (old_fid,))

                lectures = cursor.dictfetchall()
                for lectura in lectures:
                    if not lectura['data_actual']:
                        lectura['data_actual'] = factura['data_final']
                    if not lectura['data_anterior']:
                        lectura['data_anterior'] = factura['data_anterior']
                    consum = lectura['lect_actual'] - lectura['lect_anterior']
                    if consum < 0:
                        if not giro and lectura['tipus'] == 'reactiva':
                            consum = 0
                        else:
                            consum += giro
                    lectura.update({
                        'comptador_id': comptador_id,
                        'factura_id': fid,
                        'magnitud': MAGS.get(lectura['tipus']),
                        'consum': consum,
                    })
                    le_obj.create(cursor, uid, lectura)

                # Lectures potencia
                cursor.execute("""SELECT
                name,
                False as comptador_id,
                False as factura_id,
                pot_contract,
                pot_maximetre,
                exces
                from  giscedata_factura_lectura_potencia
                where factura = %s""", (old_fid,))

                lectures = cursor.dictfetchall()
                for lectura in lectures:
                    lectura.update({
                        'comptador_id': comptador_id,
                        'factura_id': fid,
                    })
                    lp_obj.create(cursor, uid, lectura)

                # Línies de factura! comença el festival!
                cursor.execute("""SELECT
                id,
                name,
                tipus,
                preu as price_unit_multi,
                extra as multi,
                quantitat as quantity,
                False as uom_multi_id,
                False as factura_id,
                False as invoice_line_id,
                False as cosfi,
                data_desde,
                data_hasta as data_fins
                from  giscedata_factura_linia
                where factura = %s""", (old_fid,))
                linies = cursor.dictfetchall()
                # Busquem les línies
                cursor.execute("""SELECT
                id,
                name,
                product_id,
                quantity
                from account_invoice_line
                where invoice_id = %s""", (factura['invoice_id'],))
                ilines = cursor.dictfetchall()
                pr_ilines = {}
                for iline in ilines:
                    pr_ilines[iline['name']] = iline['product_id']
                ilines = None
                # Les eliminem! ja no les necessitarem
                cursor.execute("""delete
                from account_invoice_line
                where invoice_id = %s""", (factura['invoice_id'],))

                for linia in linies:
                    if linia['tipus'] == 'iee':
                        continue
                    productes = get_productes(False, cursor, uid,
                                              factura['tarifa_acces_id'],
                                              linia['tipus'])
                    iproducte = pr_ilines.get(linia['name'], False)
                    producte = productes.get(linia['name'], iproducte)
                    old_lid = linia['id']
                    del linia['id']

                    linia.update({
                        'product_id': producte,
                        'data_desde': linia['data_desde'] or factura['data_inici'],
                        'data_fins': linia['data_fins'] or factura['data_final'],
                        'account_id': get_default_account(False, cursor, uid),
                        'factura_id': fid,
                        'invoice_line_tax_id': [(6, 0, [tax])],
                        'uos_id': get_potencia_uom(False, cursor, uid,
                                                   linia['tipus'])
                    })
                    if factura['type'] == 'out_refund' and linia['quantity'] < 0:
                        linia['quantity'] *= -1
                    if linia['tipus'] == 'potencia':
                        linia['price_unit_multi'] *= 1 / 12.0
                    lid = lf_obj.create(cursor, uid, linia)
                # Comprovar els totals, necessitem la lògica del model antic
                fold = fold_obj.browse(cursor, uid, old_fid)
                fnew = fac_obj.browse(cursor, uid, fid)
                fnew.button_reset_taxes()
                fnew.invoice_open()

                # Aquí les pagarem
                invoice = invoice_obj.browse(cursor, uid, factura['invoice_id'])
                invoice.pay_and_reconcile(invoice.residual,
                    invoice.journal_id.default_credit_account_id.id,
                    get_periode(False, cursor, uid, invoice.date_invoice),
                    invoice.journal_id.id, False, False, False,
                    {'date_p': invoice.date_invoice}, invoice.number)

                # Treballem amb valor absolut en v5
                fold.total = abs(fold.total)
                fold.subtotal = abs(fold.subtotal)
                fold.taxes = abs(fold.taxes)

                cursor.execute("update account_invoice set amount_total = %s where id = %s", (fold.total, factura['invoice_id']))
                cursor.execute("update account_invoice set residual = %s where id = %s", (fold.total, factura['invoice_id']))
                cursor.execute("update account_invoice set amount_untaxed = %s where id = %s", (fold.subtotal, factura['invoice_id']))
                cursor.execute("update account_invoice set amount_tax = %s where id = %s", (fold.taxes, factura['invoice_id']))
                cursor.execute("update account_invoice_tax set tax_amount = %s, base_amount = %s,  amount = %s, base = %s where invoice_id = %s",
                               (fold.taxes, fold.subtotal, fold.taxes, fold.subtotal, factura['invoice_id']))

                fnew = fac_obj.browse(cursor, uid, fid)

                # Total
                diff = fnew.amount_total - fold.total
                if diff:
                    perr("(id: %s) Total actual: %s - (id: %s) "
                         "Total antic: %s = Diff %s "
                         "Cur id: %s Data Final: %s HRD: %s\n"
                         % (fid, fnew.amount_total, fold.id, fold.total, diff,
                            cur_id, data_final_old, _hrd))

                # Subtotal
                diff = fnew.amount_untaxed - fold.subtotal
                if diff:
                    perr("(id: %s) Subtotal actual: %s - (id: %s) "
                         "Subtotal antic: %s = Diff %s "
                         "Cur id: %s Data Final: %s HRD: %s\n"
                         % (fid, fnew.amount_untaxed, fold.id, fold.subtotal,
                            diff, cur_id, data_final_old, _hrd))

                # Taxes
                diff = fnew.amount_tax - fold.taxes
                if diff:
                    perr("(id: %s) Taxes actual: %s - (id: %s) "
                         "Taxes antic: %s = Diff %s "
                         "Cur id: %s Data Final: %s HRD: %s\n"
                         % (fid, fnew.amount_tax, fold.id, fold.taxes, diff,
                            cur_id, data_final_old, _hrd))

                cursor.commit()
            except Exception:
                import traceback
                perr("ERRORRRRRRRRRRRRRRRRRRRR! Factura: %s\n" % old_fid)
                traceback.print_exc()
                cursor.rollback()
            finally:
                cursor.close()
            pbar.update(done)
        pbar.finish()
    else:
        pout("* No queden factures. Enllaçem Refs\n")
        cursor_.execute("SELECT f.name as number, ref.name as ref from "
                        "giscedata_factura f inner join giscedata_factura ref "
                       "on (f.ref = ref.id)")
        rects = cursor_.dictfetchall()
        for rect in rects:
            ref = fac_obj.search(cursor_, uid, [('number', '=', rect['ref'])],
                                 context={'active_test': False})[0]
            fid = fac_obj.search(cursor_, uid, [('number', '=', rect['number'])],
                                 context={'active_test': False})[0]
            pout("  %s (id:%s) => %s (id:%s)\n" % (rect['number'], fid,
                                           rect['ref'], ref))
            fac_obj.write(cursor_, uid, [fid], {'ref': ref})
        pout("* Enllaçament complet!\n")


logger.notifyChannel("script", netsvc.LOG_INFO, "Inicialitzant script!")
db, pool = pooler.get_db_and_pool(tools.config['db_name'], update_module='all')

if __name__ == "__main__":
    try:
        cursor_ = db.cursor()
        uid = 1
        migrate(cursor_, "4.2")
        cursor_.commit()
    except KeyboardInterrupt:
        sys.exit(1)
    finally:
        cursor_.close()
