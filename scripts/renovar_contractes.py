#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ooop import OOOP
from datetime import datetime
import os

o = OOOP(dbname=os.getenv('DBNAME', 'aspirabon5'),
         port=int(os.getenv('OOOP_PORT', 8050)))

polisses = o.GiscedataPolissa.all()
total = len(polisses)
START_INDEX = int(os.getenv('START_INDEX', 0))
i = START_INDEX
END_INDEX = int(os.getenv('END_INDEX', total))
while i < END_INDEX and i >= START_INDEX:
    polissa = polisses[i]
    i += 1
    if polissa.state == 'esborrany':
        continue
    print "(%i/%i) Renovant contracte pòlissa: %s" % (i, total, polissa.name)
    data_final = polissa.modcontractuals_ids[0].data_final
    start = datetime.now()
    while data_final <= datetime.now().strftime('%Y-%m-%d'):
        polissa.modcontractuals_ids[0].renovar()
        polissa = o.GiscedataPolissa.get(polissa.id)
        data_final = polissa.modcontractuals_ids[0].data_final
    end = datetime.now()
    print "Time elapsed: %is." % (end - start).seconds