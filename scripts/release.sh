#!/bin/bash

# Script per fer nova release de GISCE ERP

python=`command -v python`
git=`command -v git`

help=$(cat <<EOF
release.sh\n
    -r REVISION Especifica la versió de la release\n
    -t TAGNAME  Especifica quin tag crear per la release\n
    -h          Aquesta ajuda\n
EOF
)

while getopts ":r:t:h" opt; do
    case $opt in
        h)
            echo -e $help
            exit 0
            ;;
        r)
            revision=$OPTARG
            ;;
        t)
            tag=$OPTARG
            ;;
        *)
            echo "Error cridant al programa."
            echo -e $help
            exit 1
            ;;
    esac
done

if [ "x" == "x$revision" ]; then
    echo "L'opció -r REVISION és obligatòria"
    exit 1
fi

if [ "x" == "x$tag" ]; then
    echo "L'opció -t TAGNAME és obligatòria"
    exit 1
fi

echo "Anem a fer la versió $revision i el tag $tag"
read -n 1 -p "Vols continuar? (s/N) " confirm
if [ "xs" != "x$confirm" ]; then
    echo -e "\nAbortem."
    exit 1
fi

echo -e "\nOk. Som-hi! "

if [ ! -d ".git" ]; then
    echo "Vés primer a l'arrel del repositori, sisplau."
    exit 1
fi

# Let's release this shit!
echo "Marquem el commit actual a developer per futurs fixes..."
$git co developer
$git tag "pre_$tag"
$git push --tags origin developer
echo "Anem a la branca master..."
$git co master
echo "Mirem de fer merge de developer... Resoldre els possibles conflictes"
$git merge developer
echo "Actualitzem la versió dels __terp__'s a $revision..."
$python scripts/update_terp_version.py addons/gisce $revision
echo "Commiting..."
$git ci -a -m "REL $tag"
echo "Tagging..."
$git tag $tag
echo "Publicant..."
$git push --tags origin master

