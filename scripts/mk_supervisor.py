#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ConfigParser
import io
import optparse
import os
import sys

__version__ = 0.1

def run(argv):
    """Mètode principal
    """
    parser = optparse.OptionParser(usage="%prog [OPTIONS]", version=__version__)
    parser.add_option('--file', '-f', dest='file', default='supervisor.conf',
                      help='The file to write the output to')
    parser.add_option('--template', '-t', dest='template',
                      default='template.conf', help='The template file')
    parser.add_option('--basedir', '-b', dest='basedir',
                      default=os.getcwd(),
                      help='The ERP installation base dir')
    parser.add_option('--company', '-c', dest='company',
                      help='The company the file is for')
    (options, args) = parser.parse_args()

    if not options.company:
        parser.error("The --company/-c option is mandatory")

    substitutions = {
        'basedir': options.basedir,
        'empresa': options.company,
    }
    print("Using %s as basedir" % options.basedir)
    print("Warning: %s file will be overwritten, if any" % (options.file,))
    output = open(options.file, 'w')
    inputf = open(options.template, 'r')
    template = inputf.read()
    print("The read template is: ")
    print(template)
    template = template % substitutions

    config = ConfigParser.RawConfigParser()
    config.readfp(io.BytesIO(template))
    print("The read config has the following sections: ")
    print(config.sections())
    print("Writing config to file %s" % options.file)
    config.write(output)
    output.close()
    inputf.close()


if __name__ == '__main__':
    run(sys.argv[1:])
