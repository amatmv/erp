#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pylint: disable-msg=E1101
from loop import OOOP
from datetime import datetime
import multiprocessing
import sys

POOLSIZE = 4
ODIST = OOOP(dbname='LERSA_distri', user='root', pwd='root', port=8051)
OCOM = OOOP(dbname='LERSA_comer', user='admin', pwd='admin', port=8050)

def process_polissa(polissa_id):
    """Processem aquesta pòlissa.
    """
    polissa = ODIST.GiscedataPolissa.get(polissa_id)
    pname = polissa.name
    if not polissa.modcontractual_activa:
        sys.stdout.write("**** [D] Polissa %s no té modcontractual activa\n"
                         % pname)
        sys.stdout.flush()
        return
    data_inici = polissa.modcontractual_activa.data_inici
    data_final = polissa.modcontractual_activa.data_final
    results = OCOM.GiscedataPolissa.filter(name=pname)
    if len(results) != 1:
        sys.stdout.write("**** [C] Polissa %s trobada %i vegades"
                         " a comercialitzacio\n" % (pname, len(results)))
        sys.stdout.flush()
        return
    polissa_c = results[0]
    if not polissa_c.modcontractual_activa:
        sys.stdout.write("**** [C] Polissa %s no té modcontractual activa\n"
                         % pname)
        sys.stdout.flush()
        try:
            polissa_c.activar_xmlrpc()
        except Exception as exc:
            sys.stdout.write("$$$$ [C] Error en activar polissa %s.\n%s\n"
                             % (pname, exc))
            sys.stdout.flush()
            return
        polissa_c = OCOM.GiscedataPolissa.get(polissa_c.id)
        if polissa_c.state == 'activa':
            sys.stdout.write("^^^^ [C] Polissa %s activada!\n" % pname)
            sys.stdout.flush()
        else:
            return
    # Eliminem  altres modificacions contractuals si en té
    for mcc in polissa_c.modcontractuals_ids:
        if mcc.state != 'actiu':
            sys.stdout.write("!!!! Eliminem modcontractual de la "
                             "polissa %s (%s)\n" % (polissa_c.name, mcc.state))
            sys.stdout.flush()
            OCOM.GiscedataPolissaModcontractual.unlink([mcc.id])
    mcc_activa = polissa_c.modcontractual_activa
    mcc_activa.data_inici = data_inici
    mcc_activa.data_final = data_final
    try:
        mcc_activa.save()
    except Exception as exc:
        sys.stdout.write("$$$$ [C] Error en activar polissa %s.\n%s\n"
                             % (pname, exc))
        sys.stdout.flush()
        return
    sys.stdout.write("==> Up %s (%s - %s)\n" % (pname, data_inici, data_final))
    sys.stdout.flush()

if __name__ == '__main__':
    SEARCH_PARAMS = [('comercialitzadora.id', '=', 4953)]
    POLISSES_DIST = ODIST.GiscedataPolissa.search(SEARCH_PARAMS)
    START = datetime.now()
    sys.stdout.write("Processarem %s polisses\n" % len(POLISSES_DIST))
    sys.stdout.flush()
    POLISSA_POOL = multiprocessing.Pool(POOLSIZE)
    POLISSA_POOL.map(process_polissa, POLISSES_DIST, 100)
    POLISSA_POOL.close()
    END = datetime.now()
    DURATION = END - START
    HORES = DURATION.seconds / 3600
    MINUTS = (DURATION.seconds % 3600) / 60
    SEGONS = (DURATION.seconds % 3600) % 60
    sys.stdout.write("Duracio: %sh %sm %ss\n" % (HORES, MINUTS, SEGONS))
    sys.stdout.flush()
