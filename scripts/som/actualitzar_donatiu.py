#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Actualitzar el camp donatiu amb la informació existent en el camp
# observacions provinent del formulari

import sys
from ooop_wst import OOOP_WST


def read_donatiu(txt):
    ret = False
    if 'donatiu' in txt and \
         txt[txt.find('donatiu') + len('donatiu: ')] == '1':
            ret = True
    return ret

O_wst = OOOP_WST(dbname='', user='', pwd='', port=8069, 
                 uri='https://localhost')
O = O_wst.begin()

ids_poli = O.GiscedataPolissa.search([('donatiu', '=', False)],
                                            0, 0, False, {'active_test': True})
info_poli = O.GiscedataPolissa.read(ids_poli, ['observacions'])

poli_donatiu = []
poli_void = []
assign_ok = []
assign_err = []
for i in info_poli:
    if i['observacions']:
        if read_donatiu(i['observacions']):
            poli_donatiu.append(i['id'])
    else:
        poli_void.append(i['id'])
for poli in poli_donatiu:
    try:
        O.GiscedataPolissa.write(poli, {'donatiu': True})
        assign_ok.append(poli)
    except Exception, e:
        assign_err.append(poli)

O.commit()
O.close()

print u"S'han actualitzat %s pòlisses" % (len(poli_donatiu) - len(assign_err))
print u"Pòlisses actualitzades: %s" % assign_ok
print u"Pòlisses amb error en l'actualització: %s" % assign_err
print u"Pòlisses sense informació de formulari: %s" % poli_void
