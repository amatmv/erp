#! /bin/sh

log() { 
  echo "[`date`]: $*"
}

log "Sincronització de socis de Somenergia"
FILE='/tmp/eggs.csv'
export PYTHONPATH="/home/erp/src/erp/server/sitecustomize"
/home/erp/bin/python /home/erp/src/erp/scripts/som/sync_socis.py https://localhost somenergia 8069 admin laif8Eed False $FILE
scp $FILE somenergia@sw2.somenergia.coop:/home/somenergia/apps/export-socis-erp.csv 
log "Fi"
