#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Modificar el lot de les pòlisses que es troben en estat obert 
# en el contracte_lot, del lot_origen al lot_desti

import sys
import optparse

from ooop import OOOP

parser = optparse.OptionParser(version="[GISCE ERP] Moure pòlisses de lot")

parser.add_option("--lot_origen", dest="lot_origen", help="Lot en el que es troben les pòlisses actualment. Format MM/YYYY")
parser.add_option("--lot_desti", dest="lot_desti", help="Lot en el que es mouran les pòlisses. Format MM/YYYY")

group = optparse.OptionGroup(parser, "SERVER Related")
group.add_option("--host", dest="host", help="specify the database host")
group.add_option("--port", dest="port", help="specify the database port")
group.add_option("-d", "--database", dest="dbname", help="specify the database name")
group.add_option("-u", "--user", dest="user", help="specify the user name")
group.add_option("-p", "--password", dest="password", help="specify the password")

parser.add_option_group(group)

options = optparse.Values()
options.db_name = 'terp' # default value
options.host = 'localhost'
options.user = 'admin'
options.port = '8069'
parser.parse_args(values=options)

host = options.host
port = options.port
dbname = options.dbname
user = options.user
pwd = options.password
lot_origen = options.lot_origen
lot_desti = options.lot_desti

O = OOOP(dbname=dbname, user=user, pwd=pwd, port=int(port), 
                 uri=host)
id_lot_orig = O.GiscedataFacturacioLot.search([('name', '=', lot_origen)])
id_lot_dest = O.GiscedataFacturacioLot.search([('name', '=', lot_desti)])

id_clot = O.GiscedataFacturacioContracte_lot.search([('lot_id', '=', id_lot_orig[0]),
                                         ('state', '=', 'obert')])
ids_poli = O.GiscedataFacturacioContracte_lot.read(id_clot, ['polissa_id'])
ids_poli = [(i['polissa_id'][0], i['id']) for i in ids_poli]

for poli, clot in ids_poli:
    print 'Pòlissa %s' % poli
    O.GiscedataPolissa.write(poli, {'lot_facturacio': id_lot_dest[0]})
    O.GiscedataFacturacioContracte_lot.unlink([clot])

