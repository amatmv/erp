#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Crea comptes comptables per a tots els pagadors que no en
# tenen

from datetime import datetime

import sys
from ooop import OOOP

t0 = datetime.now()
print t0

uri, dbname, port, user, pwd, mute = sys.argv[1:]
mute = eval(mute)
O = OOOP(dbname=dbname, user=user, pwd=pwd, port=int(port), uri=uri)

poli = O.GiscedataPolissa.search([], 0, 0, 'name asc', {'active_test': True})
pagadors = O.GiscedataPolissa.read(poli, ['pagador'])
ids_pagadors = list(set([x['pagador'][0] for x in pagadors if x['pagador']]))

# l'ERP no permet buscar pels camps property...
fields = ['property_account_receivable', 'ref']
ids_pagadors_void_acc = []
ids_pagadors_void_ref = []
ids_pagadors_erronis = []
count = 0
for i in O.ResPartner.read(ids_pagadors, fields):
    count += 1
    try:
        codi = O.AccountAccount.read(i['property_account_receivable'][0], ['code'])
    except Exception, e:
        ids_pagadors_erronis.append(i['id'])
        print "{0} Error llegint compte comptable {1}".format(
            count, i['property_account_receivable'][0]
        )
    if codi['code'] == '430000000000':
        if i['ref']:
            ids_pagadors_void_acc.append(i['id'])
        else:
            ids_pagadors_void_ref.append(i['id'])

# contractació - 43
total = len(ids_pagadors_void_acc)
ids_pagadors_act = []
pos = 0
for pos, id_pagador in enumerate(ids_pagadors_void_acc):
    if (datetime.now() - t0).seconds >= 3600:
        break
    if not mute:
        print 'id: %s %s de %s' % (id_pagador, pos, total)
    O.ResPartner.button_assign_acc_43([id_pagador])
    ids_pagadors_act.append(id_pagador)

ids_pagadors_pendents = []
if pos + 1 < len(ids_pagadors_void_acc):
    ids_pagadors_pendents = ids_pagadors_void_acc[pos:]

# Socis Generation kWh
# Partim de la les noves inversions
new_investments = O.GenerationkwhInvestment.search([('draft','=','TRUE')])
for inv in new_investments:
    member_id = O.GenerationkwhInvestment.read(inv, ['member_id'])['member_id'][0]
    partner_id = O.SomenergiaSoci.read(member_id, ['partner_id'])['partner_id'][0]
    O.ResPartner.button_assign_acc_43([partner_id])
    O.ResPartner.button_assign_acc_410([partner_id])
    O.ResPartner.button_assign_acc_1635([partner_id])
    print "Inversio: ", member_id, ": ", partner_id

print u"S'han actualitzat %s partners\n" % len(ids_pagadors_act)
print u"S'han actualitzat %s Socis Generation kWh\n" % len(payment_line_ids)
print u"Falten %s partners\n" % len(ids_pagadors_pendents)
print u"Encara hi ha %s partners sense ref\n" % len(ids_pagadors_void_ref)
print u"Pagadors actualitzats: %s\n" % ids_pagadors_act
print u"Pagadors pendents d'actualitzar: %s\n" % ids_pagadors_pendents
print u"Pagadors no actualitzats - sense ref: %s\n" % ids_pagadors_void_ref
print u"Temps d'execució: %s" % (datetime.now() - t0)
