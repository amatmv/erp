#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Activar contractes de SomEnergia

from datetime import datetime
import csv
import sys

from ooop import OOOP

##################
# Conf DB
##################
len_num_polissa = 5
data_firma_ctr_default = '2012-01-01'

################
# Camps fitxer
################
num_ctr = 0
cups = 1
pot_ctr = 2
data_alta = 3
lot_fact = 4
col = 5
pot = 6
tarifa = 7
tarifa_comer = 8
versio_fact = 9
contracte_distri = 10
periodicitat_facturacio = 11
tarifa_distri = 12
potencies_distri = 13
################

t0 = datetime.now()
ctr_pref = '%%0%dd' % len_num_polissa

uri, dbname, port, user, pwd, mute, dfile = sys.argv[1:]
mute = eval(mute)
O = OOOP(dbname=dbname, user=user, pwd=pwd, port=int(port), uri=uri)
reader = csv.reader(open(dfile), delimiter=';')
per_obj = O.GiscedataPolissaPotenciaContractadaPeriode

for lin, row in enumerate(reader):
    try:
        #import pdb; pdb.set_trace()
        print 'processant la línia %d' % lin
        canvi_tarifa = False
        canvi_potencies = False

        potencies = eval('{%s}' % row[potencies_distri])

        lot_id = O.GiscedataFacturacioLot.search([('name', '=', row[lot_fact])])[0]
        prod_id = O.ProductPricelist.search(
                        [('name', '=', row[tarifa_comer].split(' ')[0])])[0]
        v_p_fid = O.ProductPricelistVersion.search(
                                          [('name', '=', row[versio_fact])])[0]
        data = datetime.strptime(row[data_alta], "%d/%m/%Y").strftime("%Y-%m-%d")
        ref_distri = row[contracte_distri] or ''
        periodicitat = int(row[periodicitat_facturacio]) or 2
        vals = {
            'data_alta': data,
            'lot_facturacio': lot_id,
            'llista_preu': prod_id,
            'versio_primera_factura': v_p_fid,
            'ref_dist': ref_distri,
            'facturacio': periodicitat,
        }
        str_ctr = ctr_pref % int(row[num_ctr])
        p_id = O.GiscedataPolissa.search([('name', '=', str_ctr)])
        if not p_id:
            sys.stderr.write(" -> no s'ha pogut identificar el "
                             " contracte %s\n" % (str_ctr))
            continue
        camps = ['data_firma_contracte', 'tensio_normalitzada', 'state',
                 'potencia', 'tarifa', 'potencies_periode']
        dades_ctr = O.GiscedataPolissa.read(p_id[0], camps)
        if dades_ctr['state'] == 'activa':
            continue
        if not dades_ctr['tensio_normalitzada']:
            vals.update({'tensio_normalitzada': 230})
        data_firma = dades_ctr['data_firma_contracte']
        if not data_firma or data_firma == data_firma_ctr_default:
            vals.update({'data_firma_contracte': data})

        #actualitzem tarifa (només quan canvia)
        tarifa_cas = int(row[tarifa_distri])
        if tarifa_cas != dades_ctr['tarifa'][0]:
            vals.update({'tarifa': tarifa_cas})
            canvi_tarifa = True

        #validem potències
        for per_id in dades_ctr['potencies_periode']:
            periode = per_obj.get(per_id)
            if potencies.get(periode.periode_id.name, False):
                if potencies[periode.periode_id.name] != periode.potencia:
                    canvi_potencies = True
                    break

        if float(row[pot]) != dades_ctr['potencia']:
            canvi_potencies = True

        if canvi_potencies or canvi_tarifa:
            vals.update({'potencia': float(row[pot])})

        try:
            O.GiscedataPolissa.write(p_id, vals)
        except:
            sys.stderr.write(" -> no s'ha pogut actualizar el contracte %s. id %s. "
                             "vals: %s\n" % (str_ctr, str(p_id), vals))
            continue

        if canvi_tarifa or canvi_potencies:
            O.GiscedataPolissa.generar_periodes_potencia(p_id,
                                                         {'force_genpot': True})
            polissa_id = p_id[0]
            pol = O.GiscedataPolissa.get(polissa_id)
            for periode in pol.potencies_periode:
                p_name = periode.periode_id.name
                periode.write({'potencia': potencies[p_name]})

        try:
            O.GiscedataPolissa.send_signal(p_id, 'validar')
        except:
            sys.stderr.write(" -> no s'ha pogut validar el contracte %s. "
                             "id %s\n" % (str_ctr, str(p_id)))
            continue
        try:
            O.GiscedataPolissa.send_signal(p_id, 'contracte')
            # Aquí aprofitem per crear el "Mandato"
            O.PaymentMandate.create({
                'reference': 'giscedata.polissa,%s' % p_id[0],
                'date': data_firma or data
            })
        except:
            sys.stderr.write(" -> no s'ha pogut activar el contracte %s. "
                             "id %s\n" % (str_ctr, str(p_id)))
            continue
    except:
        sys.stderr.write(" -> error processant la línia %d\n" % lin)


print u"Temps d'execució: %s" % (datetime.now() - t0)
