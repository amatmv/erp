#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Generar CSV per a sincronitzar la informació de l'ERP
# cas [86929]

import csv
import sys
from datetime import datetime


from ooop import OOOP

#############
# Paràmetres
#############

camps = (
 ('id', int),
 ('active', bool),
 ('name', str),
 ('ref', str),
 ('vat', str),
 ('address.street', str),
 ('address.zip', str),
 ('address.city', str),
 ('address.phone', str),
 ('address.mobile', str),
 ('address.email', str),
 ('lang', str),
 ('date', str),
)

vist = 0
partner_set = []
address_set = []
for elem in camps:
    i = elem[0]
    if 'address' in i:
        address_set.append(i.split('.')[1])
        if not 'address' in partner_set:
            partner_set.append('address')
    else:
        partner_set.append(i)

uri, dbname, port, user, pwd, mute, dfile = sys.argv[1:]
mute = eval(mute)
O = OOOP(dbname=dbname, user=user, pwd=pwd, port=int(port), uri=uri)
csvfile = open(dfile, 'wb')
spamwriter = csv.writer(csvfile, delimiter=',',
                        quoting=csv.QUOTE_ALL, escapechar='\\',
                        doublequote=False)

p_id = O.ResPartner.search([('category_id.name', 'ilike', 'soci')])
for partner in O.ResPartner.read(p_id, partner_set):
    row = []
    if not partner['address']:
        continue
    address = O.ResPartnerAddress.read(partner['address'][0], address_set)
    for elem in camps:
        field = elem[0]
        tipus = elem[1]
        if not 'address' in field:
            val = partner[field]
        else:
            fname = field.split('.')[1]
            val = address[fname]
        if not isinstance(val, tipus):
            if tipus == str and isinstance(val, bool):
                val = ''
        row.append(val) 
    spamwriter.writerow(row)
csvfile.close()
