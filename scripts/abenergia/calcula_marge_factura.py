# -*- coding: utf-8 -*-

import os
import sys
import csv
import click
import logging
import StringIO
from datetime import datetime

from erppeek import Client

'''
Script per a treure el marge de facturació per a cada factura,
calculant la diferència entre el preu de factura i el
preu de cost sense impostos per a cada producte d'energia i potencia.
'''

@click.command()
@click.option('-s',     '--server',   default='localhost',   help=   u'Adreça servidor ERP')
@click.option('-p',     '--port',     default=18069,         help=   'Port servidor ERP', type=click.INT)
@click.option('-u',     '--user',                            help=   'Usuari servidor ERP')
@click.option('-w',     '--password',                        help=   'Contrasenya usuari ERP')
@click.option('-d',     '--database', default='abenergia',   help=   'Nom de la base de dades')

@click.option('-del',   '--delegacion',   default='',               help='Ids de delegacio/ns a filtrar separades per comes')
@click.option('-ccial', '--comercial',    default='',               help='Ids de responsables a filtrar separats per comes')
@click.option('-ini',   '--inici',        default='2016-01-01',     help='Data de factura inicial inclosa')
@click.option('-fin',   '--final',        default='2016-12-31',     help='Data de factura final inclosa')
@click.option('-out',   '--out_file',     default='/tmp/outfile.txt')
@click.option('-resum', '--resum_file',   default='/tmp/resum.txt')


def calcula_margen(**kwargs):
    server = 'http://{0}:{1}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server,
               db=kwargs['database'],
               user=kwargs['user'],
               password=kwargs['password'])

    path, filename = os.path.split(kwargs['out_file'])
    log_file = os.path.join(path, '{0}_{1}'.format('LOG', filename))
    logger = logging.getLogger('calcula_marge_factura')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    logger.info('####### Start now {0}'.format(datetime.today()))

    infobj = c.model('marge.facturacio')
    delegs = []
    if kwargs['delegacion']:
        delegs = [int(d) for d in kwargs['delegacion'].split(',')]
    cciales = []
    if kwargs['comercial']:
        delegs = kwargs['comercial'].split(',')

    infobj.calcula_margen(delegs,
                          cciales,
                          kwargs['inici'],
                          kwargs['final'],
                          kwargs['out_file'],
                          kwargs['resum_file'])


if __name__ == '__main__':
    calcula_margen()
