# -*- coding: utf-8 -*-

'''
Script que, per cada cups que troba a la coleccio --coldesti, va a buscar
l'adreça i el nom del titular a la colecció --colorigen i actualitza aquests
camps a la colecció --coldesti
'''

import click
from pymongo import MongoClient

class ActualitzarAdrecaTitular(object):

    def __init__(self, dbname, colorigen, coldesti, dburi):

        self.DBNAME = dbname
        self.COLORIGEN = colorigen
        self.COLDESTI = coldesti
        self.DBURI = dburi

    def mongoconnection(self):

        if self.DBURI:
            client = MongoClient(self.DBURI)

            if self.DBNAME:
                self.mongodb = client[self.DBNAME]
                self.colorigen = self.mongodb[self.COLORIGEN]
                self.coldesti = self.mongodb[self.COLDESTI]

        return True

    def actualitzar(self):

        self.mongoconnection()

        for cups in self.coldesti.find():
            if not cups['direccio'] or not cups['nom'] or not cups['cognom']:

                # buscar l'adreça i nom als antics sips i actualitzar-lo
                origen = self.colorigen.find({'name': cups['name']})
                if origen.count():
                    self.coldesti.update({
                       'name': cups['name']
                    },{
                      '$set': {
                        'direccio': origen[0].get('direccio',''),
                        'nom': origen[0].get('nom',''),
                        'cognom': origen[0].get('cognom',''),
                      }
                    }, upsert=False, multi=False)

        return True

@click.command()
@click.option('--dbname', default='abenergia', help='Nom de la bd.')
@click.option('--colorigen', default='giscedata_sips_ps', help='Nom de la colecció.')
@click.option('--coldesti', default='giscedata_sips_ps_fmtjul16', help='Nom de la colecció destí.')
@click.option('--dburi', default='mongodb://192.168.0.24/abenergia')

def main(dbname, colorigen, coldesti, dburi):

    actualitzador = ActualitzarAdrecaTitular(dbname, colorigen, coldesti, dburi)
    actualitzador.actualitzar()

if __name__ == '__main__':
    main()
