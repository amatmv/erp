# -*- coding: utf-8 -*-

import os
import sys
import csv
import click
import logging
from datetime import datetime

from erppeek import Client

'''El fitxer de input ve de audinfor comer:
select poliza, agente from abonados where condabo = 'Alta' '''

agente_to_user_workoffice = {
    1: {'nombre': u"Mª Paz Gil", 'delegacion': "Binefar"},
    3: {'nombre': u"Miren Ruiz", 'delegacion': "Barcelona"},
    4: {'nombre': u"Lidia Fumanal", 'delegacion': "Barbastro"},
    5: {'nombre': u"Carlos Moya", 'delegacion': "Huesca"},
    6: {'nombre': u"Raquel Salas", 'delegacion': "Binefar"},
    7: {'nombre': u"Lidia Fumanal", 'delegacion': "Barbastro"},
    9: {'nombre': u"Miren Ruiz", 'delegacion': "Barcelona"},
    11: {'nombre': u"Lidia Fumanal", 'delegacion': "Barbastro"},
    12: {'nombre': u"Jara Santafé", 'delegacion': "Huesca"},
    13: {'nombre': u"Maria Salas", 'delegacion': "Barbastro"},
    14: {'nombre': u"Maria Salas", 'delegacion': "Barbastro"},
    15: {'nombre': u"Maria Salas", 'delegacion': "Colaborador ZGZ"},
    16: {'nombre': u"Maria Salas", 'delegacion': "Colaborador Castellon"},
    17: {'nombre': u"Maria Salas", 'delegacion': "Colaborador Norte"},
    19: {'nombre': u"Noemi Mascaró", 'delegacion': "Fraga"},
    20: {'nombre': u"Manel Sanchez", 'delegacion': "Fraga"},
    99: {'nombre': u"Lidia Fumanal", 'delegacion': "Barbastro"},
}

def update_agente_delegacion(c, polissa_name, agente_name, delegacion):
    logger = logging.getLogger('update_comercial_delegacion')
    pol_obj = c.model('giscedata.polissa')
    user_obj = c.model('res.users')
    delegacion_obj = c.model('hr.office')
    pol = pol_obj.read([('name', '=', polissa_name)], ['user_id','delegacion'])
    if pol and len(pol) == 1:

        values = {'user_id':0, 'delegacion':0}

        # buscar l'usuari
        user = user_obj.read([('name', '=', agente_name)], ['id'])
        if not user:
            logger.error('No user found with name {0}'.format(agente_name))
            return

        # buscar la delegació
        delegacio = delegacion_obj.read([('name', '=', delegacion)], ['id'])
        if not delegacio:
            logger.error('No delegacion found with name {0}'.format(delegacion))
            return

        # escriure els valors a la polissa
        values = {'user_id': user[0]['id'], 'delegacion': delegacio[0]['id']}
        try:
            pol_obj.write([pol[0]['id']], values)
        except Exception as e:
            logger.info('Error updating polissa number {0}: '.format(pol[0], str(e)))
            return True

        logger.info('Update polissa number {0}'.format(pol[0]))

    elif len(pol) > 1:
        logger.error('More than one policy with name {0}'.format(
            polissa_name
        ))
    else:
        logger.error('Policy not found with name {0}'.format(
            polissa_name
        ))
    return True


@click.command()
@click.option('-s', '--server', default='localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=18069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contrasenya usuari ERP')
@click.option('-d', '--database', help='Nom de la base de dades')
@click.option('-f', '--file-input', type=click.Path(exists=True))

def importar_agente_delegacion(**kwargs):
    server = '{0}:{1}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])
    path, filename = os.path.split(kwargs['file_input'])
    log_file = os.path.join(path, '{0}_{1}'.format('LOG', filename))
    logger = logging.getLogger('update_comercial_delegacion')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.ERROR)
    logger.info('#######Start now {0}'.format(datetime.today()))
    with open(kwargs['file_input'], 'rb') as polissa_agentcomercial:
        reader = csv.reader(polissa_agentcomercial, delimiter=',')
        for row in reader:

            # descartem headers
            if row[0] == 'poliza': continue

            polissa_name = row[0]
            agente_id = row[1]
            if agente_id == '': agente_id = 99 # casos en que es null

            try:
                agente_name = agente_to_user_workoffice[int(agente_id)]['nombre']
                delegacion = agente_to_user_workoffice[int(agente_id)]['delegacion']
            except Exception as e:
                logger.error('Agent {0} found in polissa {1} not found. Agent field type: {2}'.format(agente_id, polissa_name, type(agente_id)))
            update_agente_delegacion(c, polissa_name, agente_name, delegacion)

if __name__ == '__main__':
    importar_agente_delegacion()
