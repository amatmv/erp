# -*- coding: utf-8 -*-

import click

from erppeek import Client

'''Script que busca totes les lectures de tots els comptadors d'un contracte
i els hi modificarà la tarifa d'accés per la indicada'''

@click.command()
@click.option('-uc', '--user_comer', default='admin', help='Usuari servidor ERP COMER')
@click.option('-wc', '--password_comer', default='admin', help='Contrasenya usuari ERP COMER')
@click.option('-sc', '--server_comer', default='localhost', help=u'IP servidor ERP COMER')
@click.option('-pc', '--port_comer', help='Port servidor ERP COMER', type=click.INT)
@click.option('-dc', '--database_comer', help='Nom de la base de dades de COMER')

@click.option('-pname', '--polissa_name', help='Numero contracte')
@click.option('-t', '--tarifa', help='Nom de la tarifa destí')

def canviar_lectures_contracte(**kwargs):

    server_comer = 'http://{0}:{1}'.format(kwargs['server_comer'],
                                           kwargs['port_comer'])
    c = Client(server=server_comer,
               db=kwargs['database_comer'],
               user=kwargs['user_comer'],
               password=kwargs['password_comer'])

    compt = c.model('giscedata.lectures.comptador')
    lect_obj = c.model('giscedata.lectures.lectura')
    lect_pool_obj = c.model('giscedata.lectures.lectura.pool')
    lect_pot_obj = c.model('giscedata.lectures.potencia')
    lect_pot_pool_obj = c.model('giscedata.lectures.potencia.pool')
    pol = c.model('giscedata.polissa')
    per_obj = c.model('giscedata.polissa.tarifa.periodes')
    tar_obj = c.model('giscedata.polissa.tarifa')

    tarifa = tar_obj.search([('name', '=', kwargs['tarifa'])])
    p_id = pol.search([('name', '=', kwargs['polissa_name'])])
    cs_ids = compt.search([('polissa', '=', p_id)])
    cs = compt.read(cs_ids, ['lectures',
                             'lectures_pot',
                             'pool_lectures',
                             'pool_lectures_pot'])

    for compt in cs:

        lects = [lect_obj.browse(l) for l in compt['lectures']]
        for lectura in lects:
            tarifa_origen = lectura.periode.tarifa
            # busquem el periode amb igual nom i igual tipus pero de la
            # tarifa indicada
            per_id = per_obj.search([('tarifa', '=', tarifa),
                                     ('name', '=', lectura.periode.name),
                                     ('tipus','=', lectura.periode.tipus)])[0]
            per_id
            lect_obj.write(lectura.id,{'periode': per_id})

            print 'Actualitzada la lectura tipus -{0}- amb id {1} ' \
                  'de la tarifa {2} a la {3}'.format(
                'lectura',
                lectura.id,
                tarifa_origen,
                kwargs['tarifa']
            )

        lects_pool = [lect_pool_obj.browse(l) for l in compt['pool_lectures']]
        for lectura in lects_pool:

            tarifa_origen = lectura.periode.tarifa
            # busquem el periode amb igual nom i igual tipus pero de la
            # tarifa indicada
            per_id = per_obj.search([('tarifa', '=', tarifa),
                                     ('name', '=', lectura.periode.name),
                                     ('tipus','=', lectura.periode.tipus)])[0]
            lect_pool_obj.write(lectura.id,{'periode': per_id})

            print 'Actualitzada la lectura tipus -{0}- amb id {1} ' \
                  'de la tarifa {2} a la {3}'.format(
                'lectura_pool',
                lectura.id,
                tarifa_origen,
                kwargs['tarifa']
            )

        pot = [lect_pot_obj.browse(l) for l in compt['lectures_pot']]
        for lectura in pot:
            tarifa_origen = lectura.periode.tarifa
            # busquem el periode amb igual nom i igual tipus pero de la
            # tarifa indicada
            per_id = per_obj.search([('tarifa', '=', tarifa),
                                     ('name', '=', lectura.periode.name),
                                     ('tipus','=', lectura.periode.tipus)])[0]
            lect_pot_obj.write(lectura.id,{'periode': per_id})

            print 'Actualitzada la lectura tipus -{0}- amb id {1} ' \
                  'de la tarifa {2} a la {3}'.format(
                'lectura_potencia',
                lectura.id,
                tarifa_origen,
                kwargs['tarifa']
            )

        pot_pool = [lect_pot_pool_obj.browse(l) for l in compt['pool_lectures_pot']]
        for lectura in pot_pool:
            tarifa_origen = lectura.periode.tarifa
            # busquem el periode amb igual nom i igual tipus pero de la
            # tarifa indicada
            per_id = per_obj.search([('tarifa', '=', tarifa),
                                     ('name', '=', lectura.periode.name),
                                     ('tipus','=', lectura.periode.tipus)])[0]
            lect_pot_pool_obj.write(lectura.id,{'periode': per_id})

            print 'Actualitzada la lectura tipus -{0}- amb id {1} ' \
                  'de la tarifa {2} a la {3}'.format(
                'potencia_pool',
                lectura.id,
                tarifa_origen,
                kwargs['tarifa']
            )

    return True


def main():
    canviar_lectures_contracte()

if __name__ == '__main__':
    main()