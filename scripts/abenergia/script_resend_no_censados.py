# -*- coding: utf-8 -*-

import os
import sys
import csv
import click
import logging
from datetime import datetime

from erppeek import Client


@click.command()
@click.option('-s', '--server', default='192.168.0.23',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=18069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', help='Contrasenya usuari ERP')
@click.option('-d', '--database', default='abenergia', help='Nom de la base de dades')


def resend(**kwargs):
    server = 'http://{0}:{1}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])

    siiregobj = c.model('account.sii.register')
    invobj = c.model('account.invoice')
    res_pt = c.model('res.partner')

    # buscar els registres amb error de no censat
    registres_1117 = siiregobj.search([('current_state', '=', True),
                                       ('result', 'like', '%[1117]%')])

    for reg in registres_1117:
        dades = siiregobj.read(reg,['invoice_partner_id','invoice_id'])
        pt_id = dades['invoice_partner_id']
        inv_id = dades['invoice_id']

        res_pt.check_is_aeat_registered([pt_id[0]])

        invobj.send_sii_sync(inv_id[0])

if __name__ == '__main__':
    resend()
