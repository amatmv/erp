# -*- coding: utf-8 -*-

import click
import base64
import smtplib
import shutil
import StringIO
import zipfile

from erppeek import Client

'''Script que exporta els cups de distri per a importar-los a comer a través de SIPS'''

@click.command()
@click.option('-ud', '--user_distri', default='admin', help='Usuari servidor ERP DISTRI')
@click.option('-uc', '--user_comer', default='admin', help='Usuari servidor ERP COMER')
@click.option('-wd', '--password_distri', default='admin', help='Contrasenya usuari ERP DISTRI')
@click.option('-wc', '--password_comer', default='admin', help='Contrasenya usuari ERP COMER')

@click.option('-sd', '--server_distri', default='localhost', help=u'IP servidor ERP DISTRI')
@click.option('-sc', '--server_comer', default='localhost', help=u'IP servidor ERP COMER')
@click.option('-pd', '--port_distri', help='Port servidor ERP DISTRI', type=click.INT)
@click.option('-pc', '--port_comer', help='Port servidor ERP COMER', type=click.INT)
@click.option('-dd', '--database_distri', help='Nom de la base de dades de DISTRI')
@click.option('-dc', '--database_comer', help='Nom de la base de dades de COMER')
@click.option('-i', '--filepath', default='/home/erp/sync/sips/propios', help="Directori on s'exportaran/importaran els fitxers de sips")

def export_import_cnmc_sips(**kwargs):

    try:
        server_distri = 'http://{0}:{1}'.format(kwargs['server_distri'], kwargs['port_distri'])
        client_distri = Client(server=server_distri, db=kwargs['database_distri'],
                               user=kwargs['user_distri'], password=kwargs['password_distri'])

        server_comer = 'http://{0}:{1}'.format(kwargs['server_comer'], kwargs['port_comer'])
        client_comer = Client(server=server_comer, db=kwargs['database_comer'],
                               user=kwargs['user_comer'], password=kwargs['password_comer'])

        path = kwargs['filepath']

        export_sips(client_distri, path)
        import_sips(client_comer, path)

        return True

    except Exception as e:

        # és lleig però enviem el mail usant les credencials de temperatures
        import imp
        credentials = imp.load_source('credentials', '/home/erp/src/erp/addons/gisce/ABEnergia/sensacio_termica/script/credentials.py')

        server = smtplib.SMTP(credentials.mail_host)
        server.login(credentials.mail_username, credentials.mail_pass)
        fromaddr = credentials.from_addr
        toaddrs = credentials.to_addr
        subject = 'Error en exportacio/importacio SIPS propis'
        content = 'Error en exportacio/importacio SIPS propis'
        msg = 'From: {0}\nSubject: {1}\n\n{2}'.format(fromaddr,subject,content)
        server.sendmail(fromaddr, toaddrs, msg)
        server.quit()

        return False

def import_sips(client, path):

    importer = client.model('giscedata.sips.importer')

    importer.process_dir(path)

    return True

def export_sips(client, path):
    # guardem el zip
    cnmc_export_obj = client.model('giscedata.cnmc.sips.export')
    wiz = cnmc_export_obj.create({})
    wiz.export_file()
    file_encoded = wiz.read(['file'])['file']
    file = base64.b64decode(file_encoded)
    zip_io = StringIO.StringIO(file)
    with open ('/tmp/sips_consums.zip', 'w') as fd:
        zip_io.seek(0)
        shutil.copyfileobj(zip_io, fd)

    # treiem els sips i els consums a la carpeta on
    with zipfile.ZipFile('/tmp/sips_consums.zip') as zf:
        zf.extractall(path)

    return True

if __name__ == '__main__':
    export_import_cnmc_sips()