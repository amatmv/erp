# -*- coding: utf-8 -*-

import csv
import click

from erppeek import Client

#script per importar les dades dels fitxers csv donats

def construir_adreca(row, tipo):

    if tipo == 'suministro':
        tipo_via = row['s.Tipo via']
        via = row['s.Via']
        num = row['s.Núm. Finca'].strip('0')
        portal = row['s.Portal']
        escalera = row['s.Escalera']
        piso = row['s.Piso']
        puerta = row['s.Puerta']
    elif tipo == 'cliente':
        tipo_via = row['Tipo Via']
        via = row['Via']
        num = row['Número Finca'].strip('0')
        portal = row['Portal']
        escalera = row['Escalera']
        piso = row['Piso']
        puerta = row['Puerta']

    direccion = ""
    if tipo_via:
        direccion += '{0} '.format(tipo_via)
    if via:
        direccion += '{0} '.format(via)
    if num:
        direccion += 'num {0} '.format(num)
    if portal:
        direccion += 'portal {0} '.format(portal)
    if escalera:
        direccion += 'escalera {0} '.format(escalera)
    if piso:
        direccion += 'piso {0} '.format(piso)
    if puerta:
        direccion += 'puerta {0}'.format(puerta)

    return direccion

@click.command()
@click.option('-s', '--server', default='localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=18069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', help='Contrasenya usuari ERP')
@click.option('-d', '--database', default='abenergia', help='Nom de la base de dades')

@click.option('-f', '--file-input', default='/tmp/huesca2.csv', type=click.Path(exists=True))

def main(**kwargs):
    server = 'http://{0}:{1}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])
    gas_offer_obj = c.model('crm.gas')

    with open(kwargs['file_input']) as ofertes_gas_file:
        reader = csv.DictReader(ofertes_gas_file, delimiter=';')
        for row in reader:
            try:
                # precios
                if row['Tarifa Peaje'] == '31':
                    precio_fijo = row['pf31'].replace(',','.')
                    precio_energia = row['pe31'].replace(',','.')
                elif row['Tarifa Peaje'] == '32':
                    precio_fijo = row['pf32'].replace(',','.')
                    precio_energia = row['pe32'].replace(',','.')

                # tipo persona
                if row['Tipo Persona'] == 'F':
                    tipo_persona = u'fisica'
                elif row['Tipo Persona'] == 'J':
                    tipo_persona = u'juridica'

                values = {
                    'suministro_cups_name': row['CUPS'],
                    'distribuidor_codigo': row['Distribuidor'],
                    'distribuidor_nombre': row['Razon Social'],
                    'suministro_cp': row['s.Código Postal'],
                    'suministro_municipio': row['s.Municipio'],
                    'suministro_provincia': row['s.Provincia'],
                    'suministro_direccion': construir_adreca(row, 'suministro'),
                    'qh': row['Qh'].replace(',','.'),
                    'qmax': row['QMax'].replace(',','.'),
                    'derecho_tur': row['Derecho Tur'],
                    'tarifa': row['Tarifa Peaje'],
                    'fecha_ultima_inspeccion': row['Fecha de última Inspección'] if row['Fecha de última Inspección'] != "" else False,
                    'resultado_ultima_inspeccion': row['Resultado de última Inspección'],
                    'fecha_ultimo_cambio_tarifa': row['Fecha último Cambio Tarifa'] if row['Fecha último Cambio Tarifa'] != "" else False,
                    'fecha_ultimo_cambio_comer': row['Fecha último Cambio Comercializador '] if row['Fecha último Cambio Comercializador '] != "" else False,
                    'cliente_nombre': row['Nombre'],
                    'cliente_apellidos': row['Apellido 1'] + ' ' + row['Apellido 2'],
                    'cliente_direccion': construir_adreca(row, 'cliente'),
                    'cliente_cp': row['Código Postal'],
                    'cliente_municipio': row['Municipio'],
                    'cliente_provincia': row['Provincia'],
                    'clte_tipo_persona': tipo_persona,
                    'vivienda_habitual': row['Vivienda Habitual'],
                    'impago': row['Impagos'],
                    'impago_fecha': row['Fecha Impagos'] if row['Fecha Impagos'] != "" else False,
                    'consumo_anual_estimado': row['CONSUMO_ANUAL'].replace(',','').replace('.',''),
                    'precio_termino_energia': precio_energia,
                    'precio_termino_fijo': precio_fijo,
                    'user_id': row['CCIAL'],
                }
                gas_offer_obj.create(values)
            except:
                print row



if __name__ == '__main__':
    main()
