# -*- coding: utf-8 -*-
from ooop import OOOP
import sys
import os
dbname = os.getenv('DBNAME', 'comer_soller')
print "Reset taxes to %s..." % dbname
o = OOOP(dbname=dbname, port=8050)
i = 1
for factura in o.GiscedataFacturacioFactura.all():
    factura.button_reset_taxes()
    sys.stdout.write('.')
    if not i % 5:
        sys.stdout.write(' ')
    if not i % 80:
        sys.stdout.write('%i\n' % i)
    i += 1
    sys.stdout.flush()
sys.stdout.write(' %i\n' % i)
sys.stdout.flush()
print "End"