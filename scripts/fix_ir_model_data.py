#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from ooop import OOOP


def pout(msg, flush=True):
    sys.stdout.write(msg)
    if flush:
        sys.stdout.flush()

def perr(msg, flush=True):
    try:
        sys.stderr.write(msg)
        if flush:
            sys.stderr.flush()
    except:
        pass

def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))

S = OOOP(dbname='dist_plantilla', port=8068)
D = OOOP(dbname='dist_soller', port=8069)

def migrate_liquidacio():
    print("BEGIN;")
    print("SET CONSTRAINTS ALL DEFERRED;")
    print("SET SESSION session_replication_role = replica;")
    for irm in S.IrModelData.filter(module='giscedata_liquidacio', model__like='gisce%'):
        Srow = getattr(S, model2cc(irm.model)).get(irm.res_id)
        rows = getattr(D, model2cc(irm.model)).filter(name__ilike='%s%%'%Srow.name)
        for Drow in rows:
            table = irm.model.replace('.', '_')
            print("UPDATE %s SET id = %s WHERE id = %s;" % (
                        table, Srow.id, Drow.id))
            if table == 'giscedata_liquidacio_tarifes':
                print("UPDATE giscedata_liquidacio_fpd_tarifa SET name = %s WHERE name = %s;" % (
                        Srow.id, Drow.id))
    print("COMMIT;")

migrate_liquidacio()
