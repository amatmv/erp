#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import optparse
import xmlrpclib
import time
import sys

parser = optparse.OptionParser(version="[GISCE ERP] Importació de lectures")

parser.add_option("-f", "--csv", dest="csv_file", help="specify the cvs file")
parser.add_option("--delimiter", dest="delimiter", help="specify the delimiter for cvs file")
parser.add_option("-m", "--multiplicador", help="indica si el CSV te el camp multiplicador", action="store_true", dest="multiplicador", default=False)
parser.add_option("--date-format", dest="date_format", help="indica el fomat de data, per defecte %%d/%%m/%%y")
parser.add_option("--silent", dest="silent", action="store_true", help="Silent mode (readonly)")

group = optparse.OptionGroup(parser, "SERVER Related")
group.add_option("--host", dest="host", help="specify the database host")
group.add_option("--port", dest="port", help="specify the database port")
group.add_option("-d", "--database", dest="dbname", help="specify the database name")
group.add_option("-u", "--user", dest="user", help="specify the user name")
group.add_option("-p", "--password", dest="password", help="specify the password")

parser.add_option_group(group)

options = optparse.Values()
options.db_name = 'terp' # default value
options.host = 'localhost'
options.user = 'admin'
options.port = '8069'
options.date_format = '%Y-%m-%d'
options.multiplicador = False
options.silent = False
options.delimiter = ';'
parser.parse_args(values=options)

host = options.host
port = options.port
dbname = options.dbname
user = options.user
pwd = options.password
csv_file = options.csv_file
multi = bool(options.multiplicador)
date_format = options.date_format
silent = options.silent
delimiter = options.delimiter

sock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/common' % (host,int(port)))
uid = sock.login(dbname ,user ,pwd)
sock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/object' % (host,int(port)))
reader = csv.reader(open(csv_file, "rb"), delimiter=delimiter)

conta_lect_no_valida = []
conta_no_trobats = []
conta_sense_lectura = []
conta_amb_lectura = []

HEADERS = {
  0: 'data',
  1: 'polissa',
  2: 'comptador',
  3: 'P1A',
  4: 'P2A',
  5: 'P3A',
  6: 'P4A',
  7: 'P5A',
  8: 'P6A',
  9: 'P1R',
  10: 'P2R',
  11: 'P3R',
  12: 'P4R',
  13: 'P5R',
  14: 'P6R',
  15: 'MAX1',
  16: 'MAX2',
  17: 'MAX3',
  18: 'MAX4',
  19: 'MAX5',
  20: 'MAX6',
  21: 'EXC1',
  22: 'EXC2',
  23: 'EXC3',
  24: 'EXC4',
  25: 'EXC5',
  26: 'EXC6',
  27: 'MULT',
}

REV_HEADERS = {}

for index, name in HEADERS.items():
    REV_HEADERS[name] = index

if silent:
    print '***********************************************'
    print ' Running in silent mode (readonly)'
    print '***********************************************'
    
ORIGEN_ID = sock.execute(dbname, uid, pwd, 'giscedata.lectures.origen',
                         'search', [('codi', '=', '30')])[0]

for row in reader:
    if row[0].startswith('#'):
        continue
    # Busquem el contador
    data_lectura = row[0]
    polissa = row[1]
    comptador = row[2] 
    id = sock.execute(dbname, uid, pwd, 'giscedata.lectures.comptador',
                      'search', [('name', '=', comptador),
                                 ('polissa.name', '=', polissa)])
    print(">> Important lectures comptador %s" % (comptador,))
    contador = sock.execute(dbname, uid, pwd, 'giscedata.lectures.comptador',
                            'read', id, ['id', 'polissa'])
    if not len(contador):
        conta_no_trobats.append(comptador)
        continue

    data = time.strftime('%Y-%m-%d', time.strptime(data_lectura, date_format))
    # mirem si el comptador ja té lectura per la data que importem
    lectures = sock.execute(dbname, uid, pwd, 'giscedata.lectures.lectura',
                            'search', [('comptador','=', id[0])])
    amb_lectura = False
    for l in lectures:
        ll = sock.execute(dbname, uid, pwd, 'giscedata.lectures.lectura',
                          'read', l, ['id', 'name'])
        if ll['name'] == data:
            print(">> tenim lectura ja entrada pel comptador %s i la data %s"
                  % (comptador, data))
            amb_lectura = True
    if amb_lectura:
        conta_amb_lectura.append(comptador)
        continue
    amb_lectura = False

    polissa = sock.execute(dbname, uid, pwd, 'giscedata.polissa', 'read',
                           [contador[0]['polissa'][0]], ['name', 'tarifa'])[0]

    periode = sock.execute(dbname, uid, pwd,
                           'giscedata.polissa.tarifa.periodes', 'search',
                           [('tipus', '=', 'te'),
                            ('tarifa.id', '=', polissa['tarifa'][0])])
    periodes = sock.execute(dbname, uid, pwd,
                            'giscedata.polissa.tarifa.periodes', 'read',
                            periode, ['name', 'tipus'])
    i = REV_HEADERS['P1A']
    # FORMAT CSV
    # data - polissa - comptador - activa [ p1, p2, p3, p4, p5, p6 ] 
    #  0         1        2                  3   4   5   6   7   8
    # reactiva [ p1, p2, p3, p4, p5, p6 ] - max [ p1, p2, p3, p4, p5, p6 ]
    #            9   10  11  12  13  14           15  16  17  18  19  20
    # excesos [ p1, p2, p3, p4, p5, p6 ] - multiplicador
    #           21  22  23  24  25  26         27

    for p in periodes:
        vals = {}
        if row[i]:
            try:
                # fem dos castings, algú hi carda decimals...
                lectura = int(float(row[i]))  
                if multi:
                    lectura = lectura * float(REV_HEADERS['MULT'])
                vals['name'] = data
                vals['periode'] = p['id']
                vals['lectura'] = lectura
                vals['tipus'] = 'A'
                vals['comptador'] = id[0]
                vals['observacions'] = 'Datos importados des de energía'
                vals['origen_id'] = ORIGEN_ID
                try:
                    if not silent:
                        sock.execute(dbname, uid, pwd,
                                     'giscedata.lectures.lectura', 'create',
                                     vals)
                except xmlrpclib.Fault, e:
                    print(e)
                    sys.stderr.write(e.message)
                    sys.stderr.flush()
            except ValueError, e:
                print(e)
                sys.stderr.write(e.message)
                sys.stderr.flush()
                if conta_lect_no_valida.count(comptador) == 0:
                    conta_lect_no_valida.append(comptador)
        else:
            if conta_sense_lectura.count(comptador) == 0:
                conta_sense_lectura.append("%s a %s [%s] (%s)"
                                           % (comptador, HEADERS[i],i, row[i]))

        if row[i+6]:
            try:
                lectura = int(float(row[i+6]))
                if multi:
                    lectura = lectura * float(REV_HEADERS['MULT'])
                vals['name'] = data
                vals['periode'] = p['id']
                vals['lectura'] = lectura
                vals['tipus'] = 'R'
                vals['comptador'] = id[0]
                vals['observacions'] = 'Datos importados des de Energía'
                vals['origen'] = ORIGEN_ID
                try:
                    if not silent:
                        sock.execute(dbname, uid, pwd,
                                     'giscedata.lectures.lectura', 'create',
                                     vals)
                except xmlrpclib.Fault, e:
                    print(e)
            except ValueError, e:
                print(e)
                if conta_lect_no_valida.count(row[1]) == 0:
                    conta_lect_no_valida.append(row[1])
        else:
            pass
        i += 1

    # importació de lectures de potència (_p)
    periode_p = sock.execute(dbname, uid, pwd,
                             'giscedata.polissa.tarifa.periodes', 'search',
                             [('tipus', '=', 'tp'),
                              ('tarifa.id', '=', polissa['tarifa'][0])])
    periodes_p = sock.execute(dbname, uid, pwd,
                              'giscedata.polissa.tarifa.periodes', 'read',
                              periode_p, ['name', 'tipus'])

    i = REV_HEADERS['MAX1']
    for pp in periodes_p:
        vals = {}
        if row[i]:
            try:
                lectura = float(row[i])
                if row[i+6]:
                    exces = float(row[i+6])
                else:
                    exces = 0
                if multi:
                    lectura = lectura * float(REV_HEADERS['MULT'])
                vals['name'] = data
                vals['periode'] = pp['id']
                vals['lectura'] = lectura
                vals['comptador'] = id[0]
                vals['exces'] = exces
                vals['observacions'] = 'Datos importados des de Energía'
                try:
                    if not silent:
                        sock.execute(dbname, uid, pwd,
                                     'giscedata.lectures.potencia', 'create',
                                     vals)
                except xmlrpclib.Fault, e:
                    print(e)
                    sys.stderr.write(e.message)
                    sys.stderr.flush()
            except ValueError, e :
                print(e)
                sys.stderr.write(e.message)
                sys.stderr.flush()
                if conta_lect_no_valida.count(row[1]) == 0:
                    conta_lect_no_valida.append(row[1])
        else:
            pass
        i += 1

print(">> Comptadors amb lectures no vàlides al CSV: %s" %
      (conta_lect_no_valida,))
print(">> Comptadors no trobats a la BD: %s" % (conta_no_trobats,))
print(">> Comptadors sense lectura al CSV: %s" % (conta_sense_lectura,))
print(">> Comptadors que ja tenien lectura a la BD: %s" % (conta_amb_lectura,))
