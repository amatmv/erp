from loop import OOOP
O = OOOP(dbname='oerp5_distri_test', port=8051, user='sync', pwd='sync')
for pid in O.GiscedataPolissa.search([('lot_facturacio', '=', 2),
                                      ('facturacio', '=', 2)], 0, False, False,
                                      {'active_test':False}):
    polissa = O.GiscedataPolissa.read([pid], ['facturacio', 'name'])[0]
    # Busquem la data de la primera lectura importada
    lects = []
    for compt in O.GiscedataPolissa.comptadors_actius(pid, '2011-01-01',
                                                      '2011-02-28'):
        search_params = [('comptador', '=', compt)]
        lects += O.GiscedataLecturesLectura.search(search_params)
    data_ultima = '3000-01-01'
    for lectura in  O.GiscedataLecturesLectura.read(lects, ['name']):
        data_ultima = min(data_ultima, lectura['name'])
    print '---------------------------------------------'
    print '--Polissa: %s data ultima lectura facturada %s' % (polissa['name'],
                                                            data_ultima)
    print "BEGIN;"
    #O.GiscedataPolissa.write([pid], {'data_ultima_lectura': data_ultima})
    print "UPDATE giscedata_polissa set data_utlima_lectura = '%s' where id = %i;" % (data_ultima, polissa['id'])
    print "COMMIT;"