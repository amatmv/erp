# -*- encoding: utf-8 -*-
#!/usr/bin/python

import sys
import time

import csv
import calendar
from datetime import datetime, timedelta
from pymongo import Connection, ASCENDING, DESCENDING
from progressbar import ProgressBar, ETA, Percentage, Bar

mdb_con = Connection('192.168.2.196', 27017)

def main():

    mdb = mdb_con['mongodb_distri']
    collection = mdb['tg_profile']

    #Create index
    print 'Creating index...'
    index_fields = [('name', ASCENDING),
                    ('timestamp', ASCENDING)]
    collection.ensure_index(index_fields,
                            name='tgprofile_multi_index')
    
    #meter_names = [x['_id'] for x in collection.aggregate(
    #              [{ '$group': { '_id': "$name" }}])['result']
    #            ]
    print 'Obtaining meter names...'
    meter_names = collection.distinct('name')
    #meter_names = ['ZIV0034646945']
    print len(meter_names)
    pb = ProgressBar(maxval=len(meter_names),
                         widgets=[Percentage(), ' ', Bar(), ' ', ETA()]).start()
    done = 0
   
    for meter_name in meter_names:
        for profile in collection.find({'name': meter_name}):
            vals = {}
            if isinstance(profile['ai'], (str, unicode)):
                vals.update({'ai': float(profile['ai'])})

            if isinstance(profile['ae'], (str, unicode)):
                vals.update({'ae': float(profile['ae'])})

            if isinstance(profile['r1'], (str, unicode)):
                vals.update({'r1': float(profile['r1'])})

            if isinstance(profile['r2'], (str, unicode)):
                vals.update({'r2': float(profile['r2'])})

            if isinstance(profile['r3'], (str, unicode)):
                vals.update({'r3': float(profile['r3'])})

            if isinstance(profile['r4'], (str, unicode)):
                vals.update({'r4': float(profile['r4'])})

            if isinstance(profile['magn'], (str, unicode)):
                vals.update({'magn': int(profile['magn'])})

            if 'bc' not in profile:
                vals.update({'bc': '00'})

            if 'valid' not in profile:
                vals.update({'valid': False,
                             'valid_date': False})

            if vals:
                collection.update({'_id' : profile['_id']},
                                {"$set": vals}
                                )
        
        done += 1
        pb.update(done)
    pb.finish()
        
if __name__ == "__main__":
    try:
        start = datetime.now()
        main()
    except KeyboardInterrupt:
        print
        print 'Shutting Down...'
    finally:
        end = datetime.now()
        print "Total Time: %.3fs." % ((end - start).seconds +
                             float((end - start).microseconds) / 10**6)
