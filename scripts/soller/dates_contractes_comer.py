#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Versió final!
from loop import OOOP
import os
DBNAME = os.getenv('OOOP_DBNAME', 'oerp5_distri_test')
PORT = os.getenv('OOOP_PORT', 8051)
USER = os.getenv('OOOP_USER', 'sync')
PSW = os.getenv('OOOP_PSW', 'sync')
O = OOOP(dbname=DBNAME, port=int(PORT), user=USER, pwd=PSW, debug=False)
for contracte in O.GiscedataPolissa.filter(state__not_in=('esborrany', 'validar'), comercialitzadora=10317):
    print "-- %s --" % (contracte.name)
    print "BEGIN;"
    print "UPDATE giscedata_polissa set data_firma_contracte = '%s' where name = '%s';" % (contracte.data_firma_contracte, contracte.name)
    print "UPDATE giscedata_polissa_modcontractual set data_final = '%s' from giscedata_polissa where giscedata_polissa_modcontractual.id = giscedata_polissa.modcontractual_activa and giscedata_polissa.name = '%s';" % (contracte.modcontractual_activa.data_final, contracte.name)
    print "COMMIT;"