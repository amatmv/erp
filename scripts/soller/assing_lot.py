from loop import OOOP
O = OOOP(dbname='oerp5_distri_test', user='sync', pwd='sync', port=8051)
lot = O.GiscedataFacturacioLot.filter(name='02/2011')[0]
search_params = [
    ('state', 'not in', ('esborrany', 'validar')),
    ('facturacio', '=', 2)
]
polisses_ids = O.GiscedataPolissa.search(search_params)
print "Assignant %i polisses al lot 02/2011" % len(polisses_ids)
c = 0
for pid in polisses_ids:
    O.GiscedataPolissa.write(pid, {'lot_facturacio': lot.id})
    c += 1
    print c
print "Fet!"

search_params = [
    ('state', '=', 'baixa'),
    ('data_baixa', '>=', '2011-01-01'),
    ('data_baixa', '<=', '2011-02-28')
]
polisses_ids = O.GiscedataPolissa.search(search_params, 0, False, False,
                                         {'active_test': False})
print "Assignant %i polisses al lot 02/2011" % len(polisses_ids)
O.GiscedataPolissa.write(polisses_ids, {'lot_facturacio': lot.id})
print "Fet!"