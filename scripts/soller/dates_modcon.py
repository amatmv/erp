from loop import OOOP
from datetime import datetime, timedelta
O = OOOP(dbname='oerp5_distri_test', port=8051, user='sync', pwd='sync')
names = ['491801','4601','506335','521650','70201','74601','108301','630902',
         '631386','117002','638943','134120','674451','699155','699451',
         '700425','700431','701150','710301','714970','731813','733070',
         '733074','733076','738015','297201','297802','301970','302010',
         '302030', '303975','864005','344203','346530','376002','389703',
         '392702','395100','396201','398755','409805','425701','443456',
         '474000','475500', '482450','1000745','1000807','1000912','1000922']
print "BEGIN;"
for name in names:
    to_save = []
    for pid in O.GiscedataPolissa.search([('name', '=', name)], 0, False,
                                             False, {'active_test': False}):
        polissa = O.GiscedataPolissa.get(pid)
        print "------------------ %s ---------------" % polissa.name
        modcontractual_activa = polissa.modcontractual_activa
        modcontractual_activa.data_inici = '2011-01-01'
        print "-- Modcontractual activa a %s - %s" % (modcontractual_activa.data_inici,
                                                   modcontractual_activa.data_final)
        modtmp = modcontractual_activa
        print "UPDATE giscedata_polissa_modcontractual set data_inici = '%s' where id = %i;" % (modtmp.data_inici, modtmp.id) 
        while modtmp.modcontractual_ant:
            data_final = datetime.strptime(modtmp.data_inici, '%Y-%m-%d')
            data_final -= timedelta(days=1)
            modtmp = modtmp.modcontractual_ant
            modtmp.data_final = data_final.strftime('%Y-%m-%d')
            if modtmp.data_inici >= modtmp.data_final:
                data_inici = datetime.strptime(modtmp.data_final, '%Y-%m-%d')
                data_inici -= timedelta(days=1)
                modtmp.data_inici = data_inici.strftime('%Y-%m-%d')
            print "-- Modcontractual a %s - %s" % (modtmp.data_inici,
                                                modtmp.data_final)
            print "UPDATE giscedata_polissa_modcontractual set data_inici = '%s', data_final = '%s' where id = %i;" % (modtmp.data_inici,
                                                                                                                      modtmp.data_final,
                                                                                                                      modtmp.id)
print "COMMIT;"