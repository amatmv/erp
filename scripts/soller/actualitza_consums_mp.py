#!/usr/bin/python
# -*- coding: utf-8 -*-

from loop import OOOP
import multiprocessing

from datetime import datetime
from progressbar import ProgressBar, ETA, Percentage, Bar

O = OOOP(dbname='oerp5_distri', user='sync', pwd='sync', port=28069)

def consumer(input_q, progress_q):
    while True:
        item = input_q.get()
        progress_q.put(item)
        lid = O.GiscedataLecturesLectura.search([('comptador.id', '=', item)],
                                                0, 1, 'name asc',
                                                {'active_test': False})
        if lid:
            name = O.GiscedataLecturesLectura.read(lid, ['name'])[0]['name']
            for lid in O.GiscedataLecturesLectura.search(
                [('comptador.id', '=', item), ('name', '=', name)], 0, 0, False,
                {'active_test': False}):
                l = O.GiscedataLecturesLectura.read(lid, ['lectura'])
                O.GiscedataLecturesLectura.write(lid, {'lectura': l['lectura']})
        input_q.task_done()

def producer(sequence, output_q):
    for item in sequence:
        output_q.put(item)

def progress(total, input_q):
    """Render progressbar
    """

    widgets = [Percentage(), ' ', Bar(), ' ', ETA()]
    pbar = ProgressBar(widgets=widgets, maxval=total).start()
    done = 0
    while True:
        input_q.get()
        done += 1
        pbar.update(done)
        if done >= total:
            pbar.finish()

def main():
    start = datetime.now()

    sequence = []
    search_params = []
    sequence += O.GiscedataLecturesComptador.search(search_params, 0, 0, False,
                                                    {'active_test': False})

    q = multiprocessing.JoinableQueue()
    q2 = multiprocessing.Queue()
    processes = [multiprocessing.Process(target=consumer, args=(q,q2))
                 for x in range(0, multiprocessing.cpu_count())]
    processes += [multiprocessing.Process(target=progress,
                                          args=(len(sequence), q2))]

    for proc in processes:
        proc.daemon = True
        proc.start()
        print "^Starting process PID: %s" % proc.pid
    producer(sequence, q)
    q.join()
    print "Time Elapsed: %s" % (datetime.now() - start)

if __name__ == '__main__':
    main()

