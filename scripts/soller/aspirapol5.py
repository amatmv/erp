#!/usr/bin/env python
# -*- coding: utf-8 -*-
from lRoop import OOOP
import csv
import os
import xmlrpclib
from datetime import datetime

O5 = OOOP(dbname='oerp5_distri_test', user='sync', pwd='sync',
          port=int(os.getenv('OOOP_PORT', '8051')),
         debug=int(os.getenv('DEBUG', 0)))
O4 = False
CSV_FILE = os.getenv('CSV_FILE', '/tmp/aspirapol5.csv')
ERR_FILE = '%s.err' % CSV_FILE
READER = csv.reader(open(CSV_FILE), delimiter=';')
if int(os.getenv('START_LINE', 0)):
    ERROR = open(ERR_FILE, 'a+')
else:
    ERROR = open(ERR_FILE, 'w')
PROCESSADES = 0
ROW_FIELDS = 170


def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))

def _get_proxy(model, versio='5'):
    """Retorna un objecte OOOP pel model.
    """
    proxies = {
        '4': O4,
        '5': O5,
    } 
    return getattr(proxies[versio], model2cc(model))

def create_or_update(model, vals, versio='5', **kwargs):
    """Creem o actualitzem segons faci o no faci match.
    """
    proxy = _get_proxy(model, versio)
    results = proxy.filter(**kwargs)
    msg = ""
    start = datetime.now()
    if len(results):
        msg += "Updating"
        obj = results[0]
    else:
        msg += "Creating"
        obj = proxy.new()
    for key, value in vals.items():
        setattr(obj, key, value)
    obj.save()
    end = datetime.now()
    msg += " %s (id: %i)" % (model, obj.id)
    print msg
    print "Time: %.3fs." % ((end - start).seconds +
                             float((end - start).microseconds) / 10**6)
    return obj.id

def get_id(model, versio='5', **kwargs):
    """Retorna l'id segons el match.
    """
    proxy = _get_proxy(model, versio)
    results = proxy.filter(**kwargs)
    if not len(results):
        raise Exception("No s'ha trobat l'id del model: %s amb match: %s"
                        % (model, kwargs))
    return results[0].id

def parse_row(row):
    """Processa la fila.
    """
    # Stripping all values
    row = map(str.strip, row)
    
    id_vals = {}
    
    # Creem provincia desconeguda
    vals = {
        'name': 'DESCON.',
        'code': '00',
        'country_id': _get_proxy('res.country').get(get_id('res.country',
                                                           code='ES')) 
    }
    create_or_update('res.country.state', vals, code=vals['code'])
    # Creem municipi desconegut
    vals = {
        'name': 'DESCON.',
        'ine': '00000',
        'state': _get_proxy('res.country.state').get(get_id('res.country.state',
                                                            code='00'))
    }
    create_or_update('res.municipi', vals, ine=vals['ine'])

    # Tipo de via
    vals = {
        'codi': row[2],
        'abr': row[2],
        'name': row[2]
    }
    id_vals['tv'] = create_or_update('res.tipovia', vals, codi=vals['codi'])
    
    # Municipi
    id_vals['id_municipi'] = get_id('res.municipi', ine=row[13].zfill(5))
    
    # Població
    vals = {
        'name': row[12],
        'municipi_id': _get_proxy('res.municipi').get(id_vals['id_municipi'])
    }
    id_vals['id_poblacio'] = create_or_update('res.poblacio', vals,
                                              name=vals['name'])
    
    # Distribuidora
    # Cups
    #0     name
    #1     distribuidora_id
    #2     tv
    #3     nv
    #4     pnp
    #5     es
    #6     pt
    #7     pu
    #8     cpo
    #9     cpa
    #10    direccio
    #11    dp
    #12    id_poblacio
    #13    id_municipi
    #14    ref_catastral
    #15    aclarador
    #16    zona
    #17    ordre
    #18    linia
    #19    et
    #20    id_escomesa
    #21    active
    vals = {
        'name': row[0],
        'distribuidora_id': _get_proxy('res.partner',
                                       '5').get(get_id('res.partner',
                                                       ref=row[1].zfill(4))),
        'tv': _get_proxy('res.tipovia', '5').get(id_vals['tv']),
        'nv': row[3],
        'pnp': row[4],
        'es': row[5],
        'pt': row[6],
        'pu': row[7],
        'cpo': row[8],
        'cpa': row[9],
        'direccio': row[10],
        'dp': row[11],
        'id_poblacio': _get_proxy('res.poblacio').get(id_vals['id_poblacio']),
        'id_municipi': _get_proxy('res.municipi').get(id_vals['id_municipi']),
        'ref_catastral': row[14],
        'aclarador': row[15],
        'zona': row[16],
        'ordre': row[17],
    }
    # Posem valors de l'ERP 4.2
#    cups42 = _get_proxy('giscedata.cups.ps', '4').filter(name=vals['name'])
#    if len(cups42):
#        cups42 = cups42[0]
#        vals['et'] =  cups42.et
#        vals['linia'] = cups42.linia
#        if cups42.id_escomesa:
#            res = _get_proxy('giscedata.cups.escomesa').filter(
#                                                name=cups42.id_escomesa.name)
#            vals['id_escomesa'] = len(res) and res[0] or False
            
    id_vals['cups'] = create_or_update('giscedata.cups.ps', vals,
                                       name=vals['name'])
    # Titular
    #22     ref                       Referència
    #23     name                      Nom del partner
    #24     vat                       NIF
    #25     customer                  Client
    #26     supplier                  Proveïdor
    #27     comment                   Comentari
    #28     lang                      idiome
    vals = {
        'ref': row[22],
        'name': row[23],
        'vat': 'ES'+row[24],
        'customer': row[25] and int(row[25]) or 0,
        'supplier': row[26] and int(row[26]) or 0,
        'comment': row[27],
        'lang': row[28],
    }
    try:
        id_vals['titular'] = create_or_update('res.partner', vals,
                                              vat=vals['vat'],
                                              name=vals['name'])
    except:
        # Provem sense NIF:
        vals['vat'] = False
        vals['commnet'] = row[24]
        id_vals['titular'] = create_or_update('res.partner', vals,
                                              name=vals['name'])
    # Direcció del titular
    #29    nom
    #30    tv
    #31    nv
    #32    pnp
    #33    es
    #34    pt
    #35    pu
    #36    cpo
    #37    cpa
    #38    direccio
    #39    dp
    #40    id_poblacio
    #41    id_municipi
    #42    ref_catastral
    #43    aclarador
    #44    email
    #45    telefon
    #46    fax
    #47    mobil
    vals = {
        'partner_id': _get_proxy('res.partner').get(id_vals['titular']),
        'name': row[29],
        'type': 'contact',
        'tv': _get_proxy('res.tipovia').get(create_or_update('res.tipovia',
                                                             {'name': row[30],
                                                              'codi': row[30],
                                                              'abr': row[30]},
                                                              codi=row[30])),
        'nv': row[31],
        'pnp': row[32],
        'es': row[33],
        'pt': row[34],
        'pu': row[35],
        'cpo': row[36],
        'cpa': row[37],
        'zip': row[39],
        'id_poblacio': _get_proxy('res.poblacio').get(
                        create_or_update('res.poblacio',
                                         {'name': row[40],
                                          'municipi_id': 
                                            _get_proxy('res.municipi').get(
                                                        get_id('res.municipi',
                                                        ine=row[41].zfill(5)))
                                          }, name=row[40])),
        'id_municipi': _get_proxy('res.municipi').get(
                                get_id('res.municipi', ine=row[41].zfill(5))),
        'ref_catastral': row[42],
        'aclarador': row[43],
        'email': row[44],
        'phone': row[45],
        'fax': row[46],
        'mobile': row[47],
    }
    vals['state_id'] = _get_proxy('res.country.state').get(
                                                vals['id_municipi'].state.id)
    vals['country_id'] = _get_proxy('res.country').get(
                                                vals['state_id'].country_id.id)
    id_vals['direccio_titular'] = create_or_update('res.partner.address', vals,
                                             partner_id=vals['partner_id'].id,
                                             name=vals['name'],
                                             nv=vals['nv'],
                                             pnp=vals['pnp'])
    # Pagador
    #48     ref                   
    #49     name                  
    #50     vat                   
    #51     customer              
    #52     supplier              
    #53     comment
    #54     lang
    if row[114] == 'comercialitzadora':
        id_vals['pagador'] = get_id('res.partner', ref=row[111].zfill(4))
    elif row[114] == 'altre_p':
        vals = {
            'ref': row[48],
            'name': row[49],
            'vat': 'ES'+row[50],
            'customer': row[51] and int(row[51]) or 0,
            'supplier': row[52] and int(row[52]) or 0,
            'comment': row[53],
            'lang': row[54] or 'es_ES',
        }
        try:
            id_vals['pagador'] = create_or_update('res.partner', vals,
                                                  vat=vals['vat'],
                                                  name=vals['name'])
        except:
            # Provem sense NIF:
            vals['commnet'] = vals['vat']
            vals['vat'] = False
            id_vals['pagador'] = create_or_update('res.partner', vals,
                                                  name=vals['name'])
    elif row[114] == 'titular':
        id_vals['pagador'] = id_vals['titular']
    else:
        raise Exception("Pagador no identificat.")
    
    # Direcció de pagament
    #55    tv
    #56    nv
    #57    pnp
    #58    es
    #59    pt
    #60    pu
    #61    cpo
    #62    cpa
    #63    direccio
    #64    dp
    #65    id_poblacio
    #66    id_municipi
    #67    ref_catastral
    #68    aclarador
    if row[114] != 'comercialitzadora':
        vals = {
            'partner_id': _get_proxy('res.partner').get(id_vals['pagador']),
            'name': _get_proxy('res.partner').get(id_vals['pagador']).name,
            'type': 'invoice',
            'tv': _get_proxy('res.tipovia').get(create_or_update('res.tipovia',
                                                            {'name': row[55],
                                                             'codi': row[55],
                                                             'abr': row[55]},
                                                            codi=row[55])),
            'nv': row[56],
            'pnp': row[57],
            'es': row[58],
            'pt': row[59],
            'pu': row[60],
            'cpo': row[61],
            'cpa': row[62],
            'zip': row[64],
            'id_poblacio': _get_proxy('res.poblacio').get(
                            create_or_update('res.poblacio',
                                             {'name': row[65],
                                              'municipi_id': 
                                                _get_proxy('res.municipi').get(
                                                        get_id('res.municipi',
                                                        ine=row[66].zfill(5)))
                                              }, name=row[65])),
            'id_municipi': _get_proxy('res.municipi').get(
                                get_id('res.municipi', ine=row[66].zfill(5))),
            'ref_catastral': row[67],
            'aclarador': row[68],
        }
        vals['state_id'] = _get_proxy('res.country.state').get(
                                                vals['id_municipi'].state.id)
        vals['country_id'] = _get_proxy('res.country').get(
                                                vals['state_id'].country_id.id)
        id_vals['direccio_pagament'] = create_or_update('res.partner.address',
                                                        vals,
                                                partner_id=vals['partner_id'],
                                                    name=vals['name'],
                                                    nv=vals['nv'],
                                                    pnp=vals['pnp'])
    # Notificació titular, pagador, altre_p
    #69     ref                   
    #70     name                  
    #71     vat                   
    #72     customer              
    #73     supplier              
    #74     comment               
    #75     lang
    if row[115] == 'titular':
        id_vals['partner_notificacio'] = id_vals['titular']
    elif row[115] == 'pagador':
        id_vals['partner_notificacio'] = id_vals['pagador']          
    elif row[115] == 'altre_p':
        vals = {
            'ref': row[69],
            'name': row[70],
            'vat': 'ES'+row[71],
            'customer': row[72] and int(row[72]) or 0,
            'supplier': row[73] and int(row[73]) or 0,
            'comment': row[74],
            'lang': row[75] or 'es_ES',
        }
        try:
            id_vals['partner_notificacio'] = create_or_update('res.partner',
                                                              vals,
                                                              vat=vals['vat'],
                                                              name=vals['name'])
        except:
            # Provem sense NIF:
            vals['commnet'] = vals['vat']
            vals['vat'] = False
            id_vals['partner_notificacio'] = create_or_update('res.partner',
                                                              vals,
                                                              name=vals['name'])
    else:
        raise Exception("Partner notificacio no identificat.")
    # Direcció notificacio
    #76    nom
    #77    tv
    #78    nv
    #79    pnp
    #80    es
    #81    pt
    #82    pu
    #83    cpo
    #84    cpa
    #85    direccio
    #86    dp
    #87    id_poblacio
    #88    id_municipi
    #89    ref_catastral
    #90    aclarador
    #91    country_id
    #92    state_id
    #93    provincia
    #94    email
    #95    telefon
    #96    fax
    #97    mòbil
    vals = {
        'partner_id': _get_proxy('res.partner').get(
                                                id_vals['partner_notificacio']),
        'name': row[76],
        'type': 'contact',
        'tv': _get_proxy('res.tipovia').get(create_or_update('res.tipovia',
                                                             {'name': row[77],
                                                              'codi': row[77],
                                                              'abr': row[77]},
                                                              codi=row[77])),
        'nv': row[78],
        'pnp': row[79],
        'es': row[80],
        'pt': row[81],
        'pu': row[82],
        'cpo': row[83],
        'cpa': row[84],
        'zip': row[86],
        'id_poblacio': _get_proxy('res.poblacio').get(
                        create_or_update('res.poblacio',
                                         {'name': row[87],
                                          'municipi_id': 
                                            _get_proxy('res.municipi').get(
                                                        get_id('res.municipi',
                                                        ine=row[88].zfill(5)))
                                          }, name=row[87])),
        'id_municipi': _get_proxy('res.municipi').get(
                                get_id('res.municipi', ine=row[88].zfill(5))),
        'ref_catastral': row[89],
        'aclarador': row[90],
        'email': row[94],
        'phone': row[95],
        'fax': row[96],
        'mobile': row[97],
    }
    vals['state_id'] = _get_proxy('res.country.state').get(
                                                vals['id_municipi'].state.id)
    vals['country_id'] = _get_proxy('res.country').get(
                                                vals['state_id'].country_id.id)
    id_vals['direccio_notificacio'] = create_or_update('res.partner.address',
                                                       vals,
                                             partner_id=vals['partner_id'].id,
                                             name=vals['name'],
                                             nv=vals['nv'],
                                             pnp=vals['pnp'])
    # Pòlissa
    #98     name                   
    #99     tarifa                 
    #100     potencia               
    #101    potencia_periode
    #102     facturacio             
    #103     facturacio_potencia    
    #104     data_alta
    #105     data_baixa             
    #106     tensio_normalitzada    
    #107     tensio                 
    #108     codi                   
    #109     renovacio_auto         
    #110     cnae                   
    #111     comercialitzadora      
    #112     proxima_facturacio     
    #113     tipo_pago              
    #114     pagador                
    #115     notificacio            
    #116     trafo                  
    #117     avis_sms               
    #118     avis_email             
    #119     avis_escrit            
    #120     avis_telefon           
    #121     observacions
    
    if row[112]:
        proxima_facturacio = datetime.now().strptime(row[112],
                                                '%Y-%m-%d').strftime('%Y-%m-%d')
    else:
        proxima_facturacio = False
                                                
    if row[103].lower() not in ('icp', 'max'):
        row[103] = 'recarrec'
    vals = {
        'titular': _get_proxy('res.partner').get(id_vals['titular']),
        'cups': _get_proxy('giscedata.cups.ps').get(id_vals['cups']),
        'direccio_notificacio': _get_proxy('res.partner.address').get(
                                            id_vals['direccio_notificacio']),
        'pagador': _get_proxy('res.partner').get(id_vals['pagador']),
        'name': row[98],
        'tarifa': _get_proxy('giscedata.polissa.tarifa').get(
                                            get_id('giscedata.polissa.tarifa',
                                                   name=row[99])),
        'potencia': row[100] and float(row[100]) or 0.0,
        #TODO: 101 potencia_periode
        'facturacio': row[102] and int(row[102]) or 2,
        'facturacio_potencia': row[103].lower(),
        'data_alta': row[104] and datetime.strptime(row[104],
                                             '%Y-%m-%d').strftime('%Y-%m-%d') \
                                             or False,
        'data_baixa': row[105] and datetime.strptime(row[105],
                                              '%Y-%m-%d').strftime('%Y-%m-%d') \
                                              or False,
        'data_firma_contracte': row[169] and datetime.strptime(row[169],
                                              '%Y-%m-%d').strftime('%Y-%m-%d') \
                                              or False,
        'tensio_normalitzada': _get_proxy('giscedata.tensions.tensio').get(
                                            get_id('giscedata.tensions.tensio',
                                                   name=row[106])),
        # tensio row[80] s'emplena sol
        'codi': row[108],
        'comercialitzadora': _get_proxy('res.partner').get(get_id('res.partner',
                                                        ref=row[111].zfill(4))),
        'proxima_facturacio': row[112] and proxima_facturacio or False,
        'tipo_pago': _get_proxy('payment.type').get(
                                                create_or_update('payment.type', 
                                                    {'name': row[113], 
                                                    'code': row[113].upper()},
                                                    code=row[113].upper())),
        'pagador_sel': row[114],
        'notificacio': row[115],
        'trafo': row[116] and float(row[116]) or 0.0,
        # TODO: avisos descàrrec
        'observacions': row[121],
        'lot_facturacio': proxima_facturacio and _get_proxy(
                                            'giscedata.facturacio.lot').get(
                                        get_id('giscedata.facturacio.lot',
                                            data_inici__lte=proxima_facturacio,
                                data_final__gte=proxima_facturacio)) or False,
    }
    vals['direccio_pagament'] = _get_proxy('res.partner.address').get(
                                                id_vals.get('direccio_pagament',
                                                vals['pagador'].address[0].id))
    
    vals['llista_preu'] = _get_proxy('product.pricelist').filter(
                                                name='TARIFAS ELECTRICIDAD')[0]
    vals['versio_primera_factura'] = _get_proxy(
                                    'product.pricelist.version').filter(
                                        name='BOE núm. 158 - 30/06/2010')[0]
    id_vals['polissa'] = create_or_update('giscedata.polissa', vals,
                                          name=vals['name'],
                                          active__in=('True', 'False'))
    polissa = _get_proxy('giscedata.polissa').get(id_vals['polissa'])
    # Activem la pòlissa
    if polissa.data_baixa:
        polissa.send_signal('baixa')
    elif polissa.data_alta:
        polissa.activar_xmlrpc()
    # Comptador
    #122    name
    #123    data_alta
    #124    data_baixa
    #125    propietat
    #126    product_lloguer_id
    #127    preu_lloguer
    #128    giro
    #129    data_verificacio
    #130    descripció verificació
    #131    marca
    #132    product_id
    #133    tensio
    # De moment ho fem tot amb l'ERP vell (4.2)
    if row[125] == 'cliente':
        row[125] = 'client'
    vals = {
        'name': row[122],
        'data_alta': row[123] and datetime.strptime(row[123],
                                             '%Y-%m-%d').strftime('%Y-%m-%d') \
                                             or False,
        'data_baixa': row[124] and datetime.strptime(row[124],
                                              '%Y-%m-%d').strftime('%Y-%m-%d') \
                                              or False,
        'propietat': row[125],
        'giro': row[128] and 10**int(row[128]),
        'polissa': _get_proxy('giscedata.polissa').get(id_vals['polissa']),
        # TODO: Preu de lloguer
        'lloguer': row[126] and 1 or 0,
    }
    # Posem el lloguer si el trobem
    #res = _get_proxy('product.product').filter(default_code=row[126])
    #vals['product_lloguer_id'] = len(res) and res[0] or False

    #res = _get_proxy('giscedata.lectures.comptador',
    #                 '4').filter(polissa=polissa.name)
    res = []
    if len(res):
        comptador42 = res[0]
        vals2 = {
            'polissa': _get_proxy('giscedata.polissa').get(id_vals['polissa']),
            'name': comptador42.name.name,
            'giro': row[128] and 10**int(row[128]) or comptador42.giro,
            'lloguer': row[127] and float(row[127]) and 1 or \
            comptador42.lloguer,
            'data_alta': row[123] and datetime.strptime(row[123],
                                        '%Y-%m-%d').strftime('%Y-%m-%d') or \
                                        comptador42.data_alta,
            'data_baixa': row[124] and datetime.strptime(row[124],
                                        '%Y-%m-%d').strftime('%Y-%m-%d') or \
                                        comptador42.data_baixa, 
        }
        if comptador42.product_id:
            res = _get_proxy('product.product').filter(
                            default_code=comptador42.product_id.default_code)
            vals2['product_lloguer_id'] = len(res) and res[0] or False
    
        # Per cada valor que haguem trobat a l'ERP vell ho actualitzem si no
        # existeix en les dades del CSV
        for item, value in vals2.items():
            if item not in vals:
                vals[item] = value
    id_vals['comptador'] = create_or_update('giscedata.lectures.comptador',
                                                vals, name=vals['name'],
                                                polissa=polissa.name)
    # ICP
    #134    name
    #135    aportat_client
    #136    intensitat
    #137    data_alta
    #138    data_verificació
    #139    producte
    
    # TODO: Model ICP
    
    # Butlletí
    #140    name
    #141    partner_id
    #142    partner_id_ref
    #143    installador_id
    #144    street
    #145    city
    #146    telèfon
    #147    email
    #148    fax
    #149    llicència
    #150    numregistre
    #151    installador_vat
    #152    dades_installacio
    #153    activitat_us
    #154    pot_max_admisible
    #155    pot_installada
    #156    tensio
    #157    resistencia_terra
    #158    resistencia_aillament
    #159    interruptor_diferencial
    #160    derivacio_individual
    #161    eic
    #162    eic_data
    #163    doc_tecnica
    
    vals = {
        'name': row[153],
    }
    id_vals['activitat_us'] = create_or_update('giscedata.butlleti.activitat',
                                               vals, name=vals['name'])
    
    vals = {
        'polissa_id': _get_proxy('giscedata.polissa').get(id_vals['polissa']),
        'name': row[140],
        'dades_installacio': row[152] or False,
        'activitat_us': _get_proxy('giscedata.butlleti.activitat').get(
                                                    id_vals['activitat_us']),
        'tensio': row[156] and int(row[156]),
    }
    create_or_update('giscedata.butlleti', vals, name=vals['name'],
                     polissa_id=vals['polissa_id'].name)
    
    # Cèdula
    #164    name
    #165    data_inici
    #166    data_final
    vals = {
            'polissa_id': _get_proxy('giscedata.polissa').get(
                                                        id_vals['polissa']),
            'name': row[166],
            'data_inici': row[164] and datetime.strptime(row[164],
                                        '%Y-%m-%d').strftime('%Y-%m-%d') or \
                                        False,
            'data_final': row[165] and datetime.strptime(row[165],
                                        '%Y-%m-%d').strftime('%Y-%m-%d') or \
                                        False,
    }
    create_or_update('giscedata.cedula', vals, name=vals['name'],
                     polissa_id=vals['polissa_id'].name)
    # 167    data firma contracte

def print_row(row):
    """Print row.
    """
    i = 0
    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    for value in row:
        print '%i: %s' % (i, value)
        i += 1
    print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    print "Line: %i" % READER.line_num

def write_error(row, comment=None):
    """Write error to a file.
    """
    ERROR.write('# Line %i - %s\n' % (READER.line_num, comment or ''))
    ERROR.write(';'.join(row)+'\n')
    ERROR.flush()

def halt():
    """Halt waiting for user input.
    """
    print "Prem intro per continuar ..."
    raw_input()

def main():
    """Funció principal de l'script.
    """
    global PROCESSADES
    for row in READER:
        if READER.line_num < int(os.getenv('START_LINE', 0)):
            continue
        if row[0].startswith('#'):
            continue
        if len(row) != ROW_FIELDS:
            print "!!! %i != %i" % (len(row), ROW_FIELDS)
            write_error(row, comment="Longitud: %i diferent a %i;" % 
                        (ROW_FIELDS, len(row)))
            continue
        try:
            time_start = datetime.now()
            print_row(row)
            parse_row(row)
            PROCESSADES += 1
            time_end = datetime.now()
            print "Time elapsed: %.3fs." % ((time_end - time_start).seconds +
                            float((time_end - time_start).microseconds) / 10**6)
        except xmlrpclib.Fault, fault:
            print "Exception! %s" % fault.faultString
            write_error(row)
            if int(os.getenv('HALT_ON_ERROR', 0)):
                halt()
        except Exception, ecx:
            print "Exception! %s" % ecx
            write_error(row)
            if int(os.getenv('HALT_ON_ERROR', 0)):
                halt()
        finally:
            if int(os.getenv('STEP_BY_STEP', 0)):
                halt()

if __name__ == "__main__":
    try:
        START_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        main()
    except KeyboardInterrupt:
        print
        print 'Shutting Down...'
    finally:
        END_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print "Start at %s" % START_DATE
        print "End at %s" % END_DATE
        print "Processed: %i" % PROCESSADES
        ERROR.close()
