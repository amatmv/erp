#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Detectar modificacions contractuals en un periode de facturació 
from loop import OOOP
O = OOOP(dbname='oerp5_distri_test', port=8051, user='sync', pwd='sync')
for pid in O.GiscedataPolissa.search([('facturacio', '=', 2), ('state', 'not in', ('esborrany', 'validar'))]):
    polissa = O.GiscedataPolissa.read([pid], ['facturacio', 'name'])[0]
    data_final = '2011-02-28'
    if polissa['facturacio'] == 1:
        data_inici = '2011-02-01'
    else:
        data_inici = '2011-01-01'
    intervals = O.GiscedataPolissa.get_modcontractual_intervals(pid, data_inici, data_final)
    if len(intervals) > 1:
        print '---------------------------------------------'
        print 'Polissa: %s (%s - %s)' % (polissa['name'], data_inici, data_final)
        print intervals
        for data_int in sorted(intervals.keys()):
            interval = intervals[data_int]
            interv = O.GiscedataPolissaModcontractual.get(interval['id'])
            for change in interval.get('changes', []):
                print 'Canvi de %s: %s' % (change, getattr(interv, change))
