#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Versió final!
from datetime import datetime, timedelta
from loop import OOOP
import calendar
import os
DBNAME = os.getenv('OOOP_DBNAME', 'oerp5_distri_test')
PORT = os.getenv('OOOP_PORT', 8051)
USER = os.getenv('OOOP_USER', 'sync')
PSW = os.getenv('OOOP_PSW', 'sync')
O = OOOP(dbname=DBNAME, port=int(PORT), user=USER, pwd=PSW, debug=False)
for contracte in O.GiscedataPolissa.filter(data_firma_contracte__ne='2009-07-01',
                                           state__not_in=('esborrany', 'validar')):
    data_final = datetime.strptime(contracte.data_firma_contracte, '%Y-%m-%d')
    year = data_final.year
    while data_final and data_final < datetime.now()+timedelta(days=1):
#        dies_durada_contracte = 365
#        data_final += timedelta(days=dies_durada_contracte)
#        if calendar.isleap(data_final.year) and data_final > datetime(data_final.year, 2, 28):
#            dies_durada_contracte += 1
#        data_final += timedelta(days=dies_durada_contracte)
#    data_final -= timedelta(days=1)
        year += 1
        data_final = datetime.strptime('%s-%s-%s' % (year, data_final.month, data_final.day), '%Y-%m-%d')
    data_final -= timedelta(days=1)
    contracte.modcontractual_activa.data_final = data_final.strftime('%Y-%m-%d')
    try:
        #contracte.modcontractual_activa.save()
        print contracte.data_firma_contracte, data_final.strftime('%Y-%m-%d')
    except:
        pass