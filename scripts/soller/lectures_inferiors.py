# -*- coding: utf-8 -*-

from loop import OOOP

o = OOOP(dbname=os.getenv('DBNAME', 'oerp5_distri'), 
         user=os.getenv('OOOP_USER', 'sync'), 
         pwd=os.getenv('OOOP_PASS', 'sync',
         port=int(os.getenv('OOOP_PORT', '8050')),
         debug=int(os.getenv('DEBUG', 0)))


lot = '02/2011'


lot_id = o.GiscedataFacturacioLot.filter(name=lot)
if lot_id:
    lot_id = lot_id[0]

for polissa_id in o.GiscedataFacturacioContracteLot.filter(lot_id=lot_id):
    
