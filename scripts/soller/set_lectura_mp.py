#!/usr/bin/python
# -*- coding: utf-8 -*-
from loop import OOOP
import multiprocessing
from datetime import datetime

O = OOOP(dbname='oerp5_distri', user='sync', pwd='sync', port=28069)

def consumer(input_q):
    while True:
        item = input_q.get()
        cl = O.GiscedataFacturacioContracte_lot.get(item)
        c_str = cl.status.split(' ')[2]
        for comptador in cl.polissa_id.comptadors:
            if comptador.name == c_str:
                search_params = [('comptador.id', '=', comptador.id)]
                lectura_id = O.GiscedataLecturesLectura.search(search_params,
                                    0, 1, "name desc")          
                if lectura_id:
                    lectura = O.GiscedataLecturesLectura.get(lectura_id[0])
                    data_lectura = lectura.name
                    search_params.append(('name', '=', data_lectura))
                    lectures = O.GiscedataLecturesLectura.search(search_params)
                    for lectura_id in lectures:
                        lectura = O.GiscedataLecturesLectura.get(lectura_id)
                        vals = lectura.read()[0]
                        del vals['id']
                        vals['name'] = '2011-04-30'
                        for k, v in vals.items():
                            if isinstance(v, tuple) or isinstance(v, list):
                                vals[k] =  v[0]
                        vals['origen_id'] = 9
                        del vals['observacions']
                        O.GiscedataLecturesLectura.create(vals)
        cl.validar()
        input_q.task_done()

def producer(sequence, output_q):
    for item in sequence:
        output_q.put(item)

def main():
    start = datetime.now()
    q = multiprocessing.JoinableQueue()
    q2 = multiprocessing.Queue()
    processes = [multiprocessing.Process(target=consumer, args=(q,))
                 for x in range(0, multiprocessing.cpu_count())]
    for proc in processes:
        proc.daemon = True
        proc.start()
        print "^Starting process PID: %s" % proc.pid
    sequence = []
    search_params = [('lot_id.name', '=', '04/2011'),
                     ('status', 'ilike', '%lectura anterior')]
    sequence += O.GiscedataFacturacioContracte_lot.search(search_params)
    search_params = [('lot_id.name', '=', '04/2011'),
                     ('status', 'ilike', '%lectura actual')]
    sequence += O.GiscedataFacturacioContracte_lot.search(search_params)
    print "Assignant %s lectures pòlisses" % len(sequence)
    producer(sequence, q)
    q.join()
    print "Time Elapsed: %s" % (datetime.now() - start)

if __name__ == '__main__':
    main()
