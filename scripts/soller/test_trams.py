# -*- coding: utf-8 -*-
from ooop import OOOP
o = OOOP(dbname='oerp5_comer_test', port=8050, user='admin', pwd='admin')
prod_id = 2
qty = 700
context = {'date': '2011-02-28'}
llista_preu = o.ProductPricelist.filter(name='Social - 3')[0]
qtys = llista_preu.qty_get(prod_id, qty, context)
print qtys
total = 0
for qty, qty_f in qtys:
    total += qty
    preu = llista_preu.price_get_xmlrpc(prod_id, qty_f, False, context)
    print 'Facturem %s a preu %s' % (qty, preu)
print "Total: %s" % total
