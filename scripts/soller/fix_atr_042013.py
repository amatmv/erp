# -*- encoding: utf-8 -*-
#!/usr/bin/python
from erppeek import Client
import sys
import time
import csv
from datetime import datetime

distri = Client('http://192.168.2.196:28069', db='oerp5_distri', user='sync', password='sync')
comer = Client('http://192.168.2.196:18069', db='oerp5_comer', user='sync', password='sync')

LOG_FILE = 'fix_atr_comer_042013.log'
LOG = open(LOG_FILE, 'w')

def pout(msg, flush=True):
    sys.stdout.write(msg)
    if flush:
        sys.stdout.flush()

def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))

def _get_proxy(conn, model):
    """Retorna un objecte OOOP pel model.
    """
    return getattr(conn, model2cc(model))

def main():

    factdistri_obj = distri.model('giscedata.facturacio.factura')
    factcomer_obj = comer.model('giscedata.facturacio.factura')

    #Search for invoices in this lot with remote_id
    search_params = [('lot_facturacio.name', '=', '04/2013'),
                     ('remote_id', '!=', False)]
    fact_ids = factdistri_obj.search(search_params)
    pout("%s facturas encontradas... Correcto?: (s/n): " % len(fact_ids))
    correcto = raw_input().strip()
    if correcto != 's':
        sys.exit()
    for fact_id in fact_ids:
        try:
            fact_dist = factdistri_obj.get(fact_id)
            fact_comer = factcomer_obj.get(fact_dist.remote_id)
            #Get atr totals in comer and distri
            atr_comer = sum([x.atrprice_subtotal
                             for x in fact_comer.linies_energia])
            atr_distri = sum([x.price_subtotal
                              for x in fact_dist.linies_energia])
            #If different, recalc atr in comer
            if round(atr_comer,2) != round(atr_distri,2):
                for linia_dist in fact_dist.linies_energia:
                    if linia_dist.price_subtotal == 0:
                        continue
                    linies_comer_energia = [x for x
                                            in fact_comer.linies_energia
                                            if (x.product_id.name
                                                == linia_dist.product_id.name)]
                    atrtotal = linia_dist.price_subtotal
                    counter = 0
                    total_linies = len(linies_comer_energia)
                    for linia_comer in linies_comer_energia:
                        counter += 1
                        if counter == total_linies:
                            atrprice = atrtotal
                        else:
                            atrprice = round((linia_comer.quantity
                                    * linia_dist.price_unit), 2)
                            atrtotal -= atrprice
                        #Write quantity for trigger factura total_atr
                        linia_comer.write({'atrprice_subtotal': atrprice,
                                           'quantity': linia_comer.quantity})

        except Exception, e:
            LOG.write('NOOK %s\n' % fact_id)
            LOG.flush()
            continue
        LOG.write('SIOK %s (%s), %.2f -> %.2f\n' % (fact_id,
                                                    fact_comer.number,
                                                    atr_comer, atr_distri))
        LOG.flush()

if __name__ == "__main__":
    try:
        start = datetime.now()
        main()
    except KeyboardInterrupt:
        print
        print 'Shutting Down...'
    finally:
        end = datetime.now()
        duration = "Time: %.3fs." % ((end - start).seconds +
                                     float((end - start).microseconds) / 10**6)
        LOG.write(duration) 
        LOG.close()
