# -*- coding: utf-8 -*-
from loop import OOOP
o = OOOP(dbname='oerp5_comer_remesa', port=8050)
REMESA_ID = 4
lines = o.PaymentLine.search([('order_id', '=', REMESA_ID)])
print "Pagant %i linies de remesa.." % len(lines)
for lid in lines:
    linia = o.PaymentLine.get(lid)
    if linia.ml_inv_ref.reconciled:
        continue
    print linia.amount_currency, linia.date, linia.ml_inv_ref.number
    invoice_id = linia.ml_inv_ref.id
    pay_amount = abs(linia.amount_currency)
    pay_account_id = 881  # Bancs 572
    period_id = 2  # 02/2011
    pay_journal_id = 7  # BANC
    context = {'date_p': linia.date, 'type': 'out_invoice'}
    writeoff_acc_id = False
    writeoff_period_id = False
    writeoff_journal_id = False
    o.AccountInvoice.pay_and_reconcile([invoice_id],
                                       pay_amount,
                                       pay_account_id,
                                       period_id,
                                       pay_journal_id,
                                       writeoff_acc_id,
                                       period_id,
                                       writeoff_journal_id,
                                       context,
                                       linia.ml_inv_ref.number)
