# -*- encoding: utf-8 -*-
#!/usr/bin/python
from erppeek import Client
import sys
import time
from datetime import datetime

host = 'localhost'

client = Client('http://%s:18069' % host, db='oerp5_comer', user='admin', password='admin')

def pout(msg, flush=True):
    sys.stdout.write(msg)
    if flush:
        sys.stdout.flush()

def main():

    fact_obj = client.model('giscedata.facturacio.factura')
    invtax_obj = client.model('account.invoice.tax')

    lot_id = 48
    search_params = [('lot_facturacio', '=', lot_id),
                     ('state', 'in', ('open', 'paid'))]

    context = {'active_test': False}
    fact_ids = fact_obj.search(search_params, context=context)
    pout("%s polizas encontradas... Correcto?: (s/n): " % len(fact_ids))
    correcto = raw_input().strip()
    if correcto != 's':
        sys.exit()
    fact_values = fact_obj.read(fact_ids, ['invoice_id', 'tax_line'])
    for fact_value in fact_values:
        try:
            if (fact_values.index(fact_value) + 1) % 500 == 0:
                print 'Comprobando factura %s de %s' % (fact_values.index(fact_value) + 1, len(fact_values))
            start = datetime.now()
            invoice_id = fact_value['invoice_id'][0]
            new_taxes = invtax_obj.compute(invoice_id, context={'string_key': True})
            old_taxes = invtax_obj.read(fact_value['tax_line'])
            
            for tax in new_taxes.itervalues():
                for old_tax in old_taxes:
                    if old_tax['sequence'] == tax['sequence']:
                        '''print 'Base %.2f, Base Amount %.2f, Amount %.2f, Tax amount %.2f' % (tax['base'],
                                                                                 tax['base_amount'],
                                                                                 tax['amount'],
                                                                                 tax['tax_amount'])
                        print 'Base %.2f, Base Amount %.2f, Amount %.2f, Tax amount %.2f' % (old_tax['base'],
                                                                                 old_tax['base_amount'],
                                                                                 old_tax['amount'],
                                                                                 old_tax['tax_amount'])'''
                        if ('%.2f' % tax['base'] != '%.2f' % old_tax['base'] or
                            '%.2f' % tax['base_amount'] != '%.2f' % old_tax['base_amount'] or
                            '%.2f' % tax['amount'] != '%.2f' % old_tax['amount'] or
                            '%.2f' % tax['tax_amount'] != '%.2f' % old_tax['tax_amount']):
                            print 'Diferencias para la factura con id %s' % fact_value['id']
                        if ('%.2f' % tax['amount'] != '%.2f' % old_tax['amount'] and
                            '%.2f' % tax['base'] == '%.2f' % old_tax['base']):
                            print (u"Cuota de %s incorrecta Base: %.2f"
                                   u" -> Correcto: %.2f - En factura: %.2f\n\n") % (tax['name'],
                                                                                tax['base'],
                                                                                tax['amount'],
                                                                                old_tax['amount'])
            '''end = datetime.now()
            print "Time: %.3fs." % ((end - start).seconds +
                                 float((end - start).microseconds) / 10**6)'''
        except Exception, e:
            print e
            print 'Error comprovant factura amb id: %s' % fact_value['id']
    
if __name__ == "__main__":
    try:
        start = datetime.now()
        main()
    except KeyboardInterrupt:
        print 'Shutting Down...'
    finally:
        end = datetime.now()
        print "Time: %.3fs." % ((end - start).seconds +
                             float((end - start).microseconds) / 10**6)
        

