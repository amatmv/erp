# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from loop import OOOP
O = OOOP(dbname='oerp5_comer_test', port=8050, user='admin', pwd='admin')
for pid in O.GiscedataPolissa.search([('facturacio', '=', 2), ('state', 'not in', ['esborrany', 'validar'])], 0, False, False,
                                      {'active_test':False}):
    polissa = O.GiscedataPolissa.read([pid], ['facturacio', 'name'])[0]
    data_final = '2011-02-28'
    if polissa['facturacio'] == 1:
        data_inici = '2011-02-01'
    else:
        data_inici = '2011-01-01'    
    # Comprovem els comptadors i les dates
    comptadors_actius = O.GiscedataPolissa.comptadors_actius(pid, data_inici, data_final,
                                                  'data_alta asc')
    if len(comptadors_actius) > 1:
        print '---------------------------------------------'
        print 'Polissa: %s (%i comptadors actius)' % (polissa['name'],
                                                      len(comptadors_actius))
        comptadors = []
        for c_id in comptadors_actius:
            comptadors.append(O.GiscedataLecturesComptador.get(c_id))
        idx = 0
        while idx+1 < len(comptadors):
            comptador_0 = comptadors[idx]
            comptador_1 = comptadors[idx+1]
            print('Comptador %s data_baixa %s i Comptador %s data_alta %s' %
                  (comptador_0.name, comptador_0.data_baixa, comptador_1.name,
                   comptador_1.data_alta))
            if comptador_0.data_baixa == comptador_1.data_alta:
                new_data = datetime.strptime(comptador_0.data_baixa, '%Y-%m-%d')
                new_data += timedelta(days=1)
                # Busquem si té lectures amb la data d'alta vella
                search_params = [
                    ('comptador.id', '=', comptador_1.id),
                    ('name', '=', comptador_1.data_alta)
                ]
                for lectura in O.GiscedataLecturesLectura.search(search_params):
                    lectura = O.GiscedataLecturesLectura.get(lectura)
                    print 'Lectura %s %s amb data %s' % (lectura.periode.name,
                                                         lectura.tipus,
                                                         lectura.name)
                    lectura.write({'name': new_data.strftime('%Y-%m-%d')})
                # Assignem la data d'alta al comptador
                print 'Comptador %s. Data alta a %s' % (comptador_1.name,
                                                        comptador_1.data_alta)
                comptador_1.write({'data_alta': new_data.strftime('%Y-%m-%d')})
            idx += 1