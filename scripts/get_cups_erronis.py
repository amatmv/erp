# -*- coding: utf-8 -*-
from ooop import OOOP
import sys

dbname, port, user, password = sys.argv[1:]
PROXY = OOOP(dbname=dbname, user=user, pwd=password, port=int(port))

_cups_checksum_table = 'TRWAGMYFPDXBNJZSQVHLCKE'

cups_ids = PROXY.GiscedataCupsPs.search([], 0, 0, False,
                                        {'active_test': False})
for cups in PROXY.GiscedataCupsPs.read(cups_ids, ['name']):
    codi = cups['name'][2:18]
    try:
            rest0 = int(codi) % 529
    except ValueError:
        print cups['id'], cups['name']
    coficient = int(rest0 / 23)
    rest1 = rest0 % 23
    checksum = '%s%s' % (_cups_checksum_table[coficient],
                         _cups_checksum_table[rest1])
    if cups['name'][:20] != 'ES%s%s' % (codi, checksum):
        print "%s: %s => %s" % (cups['id'], cups['name'], checksum)
