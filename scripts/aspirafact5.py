#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Script per crear els abonats al sistema de la comercialitzadora
"""

import optparse
import xmlrpclib
import sys
import time
import os

parser = optparse.OptionParser(version="[GISCE ERP] Migrations")

group = optparse.OptionGroup(parser, "Servidor distribuidora")
group.add_option("--host-dist", dest="host_dist", help="specify the database host")
group.add_option("--port-dist", dest="port_dist", help="specify the database port")
group.add_option("--database-dist", dest="dbname_dist", help="specify the database name")
group.add_option("--user-dist", dest="user_dist", help="specify the username")
group.add_option("--password-dist", dest="password_dist", help="specify the password")
parser.add_option_group(group)

group2 = optparse.OptionGroup(parser, "Servidor comercialitzadora")
group2.add_option("--host-comer", dest="host_comer", help="specify the database host")
group2.add_option("--port-comer", dest="port_comer", help="specify the database port")
group2.add_option("--database-comer", dest="dbname_comer", help="specify the database name")
group2.add_option("--user-comer", dest="user_comer", help="specify the username")
group2.add_option("--password-comer", dest="password_comer", help="specify the password")
parser.add_option_group(group2)


options = optparse.Values()
options.host_dist = 'localhost'
options.port_dist = 8051
options.dbname_dist = 'oerp5_distri_fact'
options.user_dist = 'root'
options.password_dist = 'root'
options.host_comer = 'localhost'
options.port_comer = 8050
options.dbname_comer = 'oerp5_comer_fact'
options.user_comer = 'admin'
options.password_comer = 'admin'
parser.parse_args(values=options)

host_dist = options.host_dist
port_dist = options.port_dist
dbname_dist = options.dbname_dist
user_dist = options.user_dist
pwd_dist = options.password_dist

host_comer = options.host_comer
port_comer = options.port_comer
dbname_comer = options.dbname_comer
user_comer = options.user_comer
pwd_comer = options.password_comer


class RPCSock:
    def __init__(self, url, db, uid, passwd, obj='/object'):
        print url
        self._url = url
        self._db = db
        self._uid = uid
        self._obj = obj
        self._passwd = passwd
        self._sock = xmlrpclib.ServerProxy('http://%s/xmlrpc%s' % (url, obj))

    def execute(self, *args):
        return self._sock.execute(self._db, self._uid, self._passwd, *args)


def pout(msg, flush=True):
    sys.stdout.write(msg)
    if flush:
        sys.stdout.flush()

def perr(msg, flush=True):
    try:
        sys.stderr.write(msg)
    except:
        pass
    if flush:
        sys.stderr.flush()

# Connexió amb la distribuidora
sock_dist = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/common' % (host_dist, int(port_dist)))
uid_dist = sock_dist.login(dbname_dist, user_dist, pwd_dist)
sock_dist = RPCSock('%s:%d' % (host_dist, int(port_dist)), dbname_dist, uid_dist, pwd_dist)

# Connexió amb la comercialitzadora
sock_comer = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/common' % (host_comer, int(port_comer)))
uid_comer = sock_comer.login(dbname_comer, user_comer, pwd_comer)
sock_comer = RPCSock('%s:%d' % (host_comer, int(port_comer)), dbname_comer, uid_comer, pwd_comer)

# Connexió amb la comercialitzadora (wizard)
wsock_comer = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/wizard' % (host_comer, int(port_comer)))


try:
    ts_inici = time.strftime('%d/%m/%Y %H:%M:%S')
    # Ens connectem a la distribuidora
    # 1. Busquem les dades de la nostra distribuidora per crear-la a la distribuidora
    c_ids = sock_dist.execute('res.company', 'search', [])
    if len(c_ids) > 1:
        # More than one company ask the user select one
        for company in sock_dist.execute('res.company', 'read', c_ids, ['name', 'partner_id']):
            print "  %s - %s" % (company['id'], company['name'])
        pout('Escollir empresa - ID: ')
        c_id = raw_input()
        c_id = int(c_id)
    else:
        c_id = c_ids[0]

    distri = sock_dist.execute('res.company', 'read', [c_id], ['name', 'partner_id'])[0]
    print '[D] Utilitzant "%s" com a distribuidora' % distri['partner_id'][1]

    # 2. Busquem la comercialitzadora
    c_ids = sock_comer.execute('res.company', 'search', [])
    if len(c_ids) > 1:
        # More than one company ask the user select one
        for company in sock_comer.execute('res.company', 'read', c_ids, ['name', 'partner_id']):
            print "  %i - %s" % (company['id'], company['name'])
        pout("Escollir empresa - ID: ")
        c_id = raw_input()
        c_id = int(c_id)
    else:
        c_id = c_ids[0]

    comer = sock_comer.execute('res.company', 'read', [c_id], ['name', 'partner_id'])[0]
    print '[C] Utilitzant "%s" com a comercialitzadora' % comer['partner_id'][1]

    # 3. Busquem si existex la comercialitzadora a la bbdd de la distribuidora
    print '[D] Buscant comercialitzadora "%s" en la bbdd de la distribuidora' % (comer['partner_id'][1])
    c_ids = sock_dist.execute('res.partner', 'search', [('name', '=', comer['partner_id'][1])])
    print c_ids
    if not c_ids or len(c_ids) > 1:
        # No hi ha concidencia exacte, fem escollir l'usuari
        print '[D] No hi ha concidencia exacte, obtenint llistat de comercialitzadores ...'
        ids = sock_dist.execute('giscedata.polissa', 'search', [])
        comers = {}
        for p in sock_dist.execute('giscedata.polissa', 'read', ids, ['comercialitzadora']):
            if not p['comercialitzadora']:
                # TODO: Enregitrar l'error
                continue
            if not comers.has_key(p['comercialitzadora'][0]):
                comers[p['comercialitzadora'][0]] = p['comercialitzadora'][1]
        for k,v in comers.items():
            print '  %i - %s' % (k, v)
        pout("Escollir comercialitzadora - ID: ")
        c_id = raw_input()
        c_id = int(c_id)
    else:
        c_id = c_ids[0]

    comer = sock_dist.execute('res.partner', 'read', [c_id], ['name'])[0]

    # 4. Comprovar que existeix la distribuidora a la comercialitzadora
    print '[C] Buscant la distribuidora "%s" en la bbdd de la comercialitzadora' % (distri['partner_id'][1])
    d_ids = sock_comer.execute('res.partner', 'search', [('name', '=', distri['partner_id'][1])])
    if not d_ids or len(d_ids) > 1:
        # No hi ha concidencia exacte, fem escollir l'usuari
        print '[C] No hi ha concidencia exacte, obtenint llistat de distribuidores ...'
        ids = sock_comer.execute('giscedata.cups.ps', 'search', [])
        distris = {}
        for c in sock_comer.execute('giscedata.cups.ps', 'read', ids, ['distribuidora_id']):
            if not c['distribuidora_id']:
                # TODO: Enregitrar l'error
                continue
            if not distris.has_key(c['distribuidora_id'][0]):
                distris[c['distribuidora_id'][0]] = c['distribuidora_id'][1]
            print '  0 - Crear la distribuidora'
            for k,v in distris.items():
                print '  %i - %s' % (k, v)
        pout("Escollir distribuidora - ID: ")
        d_id = raw_input()
        d_id = int(d_id)
        if d_id == 0:
            # Hem de crear la distribuidora
            vals = distri.copy()
            vals['supplier'] = 1
            d_id = sock_comer.execute('res.partner', 'create', vals)
            print '[C] Distribuidora "%s" creada' % (vals['name'])
    else:
        d_id = d_ids[0]

    pout("Escollir data inici facturacio (YYYY-MM-DD): ")
    factures_data_inici = raw_input()
    pout("Escollir data final facturacio (YYYY-MM-DD): ")
    factures_data_final = raw_input()

    pout("Escollir facturacio 1-Mensual, 2-Bimestral: ")
    facturacio = int(raw_input())

    offset = int(os.getenv('OFFSET', 0))
    limit = int(os.getenv('LIMIT', 0))

    # Busquem totes les factures que volem importar:
    search_params = [('data_inici', '>=', factures_data_inici),
                     ('data_final', '<=', factures_data_final),
                     ('invoice_id.partner_id.id', '=', c_id),
                     ('facturacio', '=', facturacio)]
    dist_factures_ids = sock_dist.execute('giscedata.facturacio.factura', 'search',
                                          search_params)
    pout("%s factures a importar...\n" % len(dist_factures_ids))
    pout("[C] Carregant productes de tarifa...")
    productes = {}
    periodes_ids = sock_comer.execute('giscedata.polissa.tarifa.periodes', 'search', [])
    productes = {}
    for periode in sock_comer.execute('giscedata.polissa.tarifa.periodes', 'read', periodes_ids):
        productes.setdefault(periode['tarifa'][1], {})
        productes[periode['tarifa'][1]].setdefault(periode['tipus'], {})
        productes[periode['tarifa'][1]][periode['tipus']][periode['name']] = periode['product_id'][0]
        if periode['tipus'] == 'te' and periode['product_reactiva_id']:
            productes[periode['tarifa'][1]].setdefault('tr', {})
            productes[periode['tarifa'][1]]['tr'][periode['name']] = periode['product_reactiva_id'][0]
    pout("OK\n")
    factures_importades = 0
    fields_to_read = ['date_boe', 
                      'facturacio', 
                      'linia_ids', 
                      'polissa', 
                      'tarifa', 
                      'lectures_energia_ids', 
                      'lectures_potencia_ids', 
                      'factura',
                      'date_invoice',
                      'lot_facturacio']
    for factura in sock_dist.execute('giscedata.facturacio.factura', 'read', dist_factures_ids, ):
        try:
            ref_dist = factura['polissa_id'][1]
            polissa_ids = sock_comer.execute('giscedata.polissa', 'search', [('name', '=', ref_dist)])
            if not polissa_ids:
                continue
            else:
                polissa_id = polissa_ids[0]
                polissa = sock_comer.execute('giscedata.polissa', 'read', polissa_id, ['payment_mode_id'])
            vals = sock_comer.execute('giscedata.facturacio.factura', 'onchange_polissa', [], polissa_id, 'out_invoice')['value']
            vals['date_boe'] = factura['date_boe']
            vals['facturacio'] = factura['facturacio']
            vals['payment_mode_id'] = polissa['payment_mode_id'][0]
            vals['date_invoice'] = '2011-02-03'
            vals['date_due'] = '2011-02-20'
            vals['data_inici'] = factura['data_inici']
            vals['data_final'] = factura['data_final']
            vals['name'] = ref_dist
            # TODO: Busquem el lot de facturacio
            vals['polissa_id'] = polissa_id
            id_factura = sock_comer.execute('giscedata.facturacio.factura', 'create', vals)
            # Importem les linies de factura
            for l in sock_dist.execute('giscedata.facturacio.factura.linia', 'read', factura['linia_ids']):
                if l['tipus'] == 'energia':
                    product = productes[factura['tarifa_acces_id'][1]]['te'][l['name']]
                elif l['tipus'] == 'reactiva':
                    product = productes[factura['tarifa_acces_id'][1]]['tr'][l['name']]
                elif l['tipus'] in ('potencia', 'exces_potencia'):
                    product = productes[factura['tarifa_acces_id'][1]]['tp'][l['name']]
                else:
                    # Els lloguers de moment no ho vincularem a cap producte
                    product = False
                if product:
                    uom_id = sock_comer.execute('product.product', 'read', [product], ['uom_id'])[0]['uom_id'][0]
                    vals_l = sock_comer.execute('giscedata.facturacio.factura.linia', 'product_id_change', [], vals['llista_preu'], vals['date_invoice'], product, uom_id, vals['polissa_id'], l['quantity'], l['name'], 'out_invoice', vals['partner_id'])['value']
                else:
                    vals_l = {}
                vals_l['invoice_line_tax_id'] = [(6,0, vals_l.get('invoice_line_tax_id', []))]
                vals_l['factura_id'] = id_factura
                vals_l['name'] = l['name']
                vals_l['quantity'] = l['quantity']
                vals_l['tipus'] = l['tipus']
                vals_l['multi'] = l['multi']
                vals_l['product_id'] = product
                if not product:
                    vals_l['name'] = l['name']
                    vals_l['invoice_line_tax_id'] = [(6,0, [37])]
                    vals_l['price_unit_multi'] = l['price_unit_multi']
                if l['tipus'] == 'potencia':
                    vals_l['multi'] = l['multi']
                vals_l['account_id'] = 1264
                id_factura_linia = sock_comer.execute('giscedata.facturacio.factura.linia', 'create', vals_l)

            sock_comer.execute('giscedata.facturacio.factura', 'button_reset_taxes', [id_factura])
            # Importem lectures
            comptadors = {}
            for l in sock_dist.execute('giscedata.facturacio.lectures.energia', 'read', factura['lectures_energia_ids']):
                c_id = comptadors.get(l['comptador_id'][1], False)
                if not c_id:
                    search_params = [
                        ('polissa.name', '=', ref_dist),
                        ('name', '=', l['comptador_id'][1])
                    ]
                    c_ids = sock_comer.execute('giscedata.lectures.comptador',
                                               'search', search_params)
                    if c_ids:
                        compt = sock_comer.execute('giscedata.lectures.comptador',
                                                   'read', c_ids, ['name'])[0]
                        comptadors[compt['name']] = compt['id']
                        c_id = compt['id']
                vals = l.copy()
                del vals['id']
                vals['factura_id'] = id_factura
                # TODO: Buscar comptador per la pòlissa
                vals['comptador_id'] = c_id
                vals['comptador'] = l['comptador_id'][1]
                sock_comer.execute('giscedata.facturacio.lectures.energia', 'create', vals)
            for l in sock_dist.execute('giscedata.facturacio.lectures.potencia', 'read', factura['lectures_potencia_ids']):
                c_id = comptadors.get(l['comptador_id'][1], False)
                if not c_id:
                    search_params = [
                        ('polissa.name', '=', ref_dist),
                        ('name', '=', l['comptador_id'][1])
                    ]
                    c_ids = sock_comer.execute('giscedata.lectures.comptador',
                                               'search', search_params)
                    if c_ids:
                        compt = sock_comer.execute('giscedata.lectures.comptador',
                                                   'read', c_ids, ['name'])[0]
                        comptadors[compt['name']] = compt['id']
                        c_id = compt['id']
                vals = l.copy()
                del vals['id']
                vals['factura_id'] = id_factura
                # TODO: Buscar comptador per la pòlissa
                vals['comptador_id'] = c_id
                vals['comptador'] = l['comptador_id'][1]
                sock_comer.execute('giscedata.facturacio.lectures.potencia', 'create', vals)
            pout(".")
            factures_importades += 1
        except Exception, e:
            print e
            pass

    print
    print "*** Total: %i factures imporades ***" % factures_importades

except KeyboardInterrupt:
    print
    print 'Shutting Down...'
