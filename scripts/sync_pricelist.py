# -*- coding: utf-8 -*-
#pylint: disable-msg=E1101,W0223
from ooop import OOOP
OSRC = OOOP(dbname='comer_soller', port=8050)
ODST = OOOP(dbname='aspirabon5', port=8050,)
EXCLUDE = ['Public Pricelist', 'Default Purchase Pricelist',
           'TARIFAS ELECTRICIDAD']
def create_tarifa(tarifa_id):
    tarifa = OSRC.ProductPricelist.get(tarifa_id)
    if tarifa.name in EXCLUDE:
        return ODST.ProductPricelist.filter(name=tarifa.name)[0].id
    print "####################################################################"
    print "Creating pricelist %s ..." % tarifa.name
    res = ODST.ProductPricelist.filter(name=tarifa.name)
    if len(res):
        pricelist = res[0]
    else:
        pricelist = ODST.ProductPricelist.new()
    pricelist.name = tarifa.name
    pricelist.type = tarifa.type
    pricelist.save()
    for version in tarifa.version_id:
        print "Creating version %s (%s - %s) ..." % (version.name,
                                                    version.date_start,
                                                    version.date_end)
        res = ODST.ProductPricelistVersion.filter(name=version.name)
        if len(res):
            ver = res[0]
        else:
            ver = ODST.ProductPricelistVersion.new()
        ver.name = version.name
        ver.pricelist_id = pricelist
        ver.date_end = version.date_end
        ver.date_start = version.date_start
        ver.save()
        for item in version.items_id:
            print "Creating item %s ..." % item.name
            res = ODST.ProductPricelistItem.filter(name=item.name)
            if len(res):
                nitem = res[0]
            else:
                nitem = ODST.ProductPricelistItem.new()
            # Comprovem si existeix la tarifa base
            if item.base_pricelist_id:
                if not len(ODST.ProductPricelist.filter(
                                            name=item.base_pricelist_id.name)):
                    base_id = create_tarifa(item.base_pricelist_id.id)
                    nitem.base_pricelist_id = ODST.ProductPricelist.get(base_id)
            nitem.name = item.name
            nitem.sequence = item.sequence
            nitem.price_version_id = ver
            nitem.base = item.base
            nitem.min_quantity = item.min_quantity
            nitem.price_discount = item.price_discount
            nitem.price_surcharge = item.price_surcharge
            nitem.price_round = item.price_round
            nitem.price_min_margin = item.price_min_margin
            nitem.price_max_margin = item.price_max_margin
            if item.product_tmpl_id:
                xmlid = get_xmlid('product.template', item.product_tmpl_id.id)
                resid = get_resid('product.template', xmlid)
                nitem.product_tmpl_id = ODST.ProductTemplate.get(resid)
            if item.product_id:
                xmlid = get_xmlid('product.product', item.product_id.id)
                resid = get_resid('product.product', xmlid)
                nitem.product_id = ODST.ProductProduct.get(resid)
            if item.categ_id:
                xmlid = get_xmlid('product.category', item.categ_id.id)
                resid = get_resid('product.category', xmlid)
                nitem.categ_id = ODST.ProductCategory.get(resid)
            nitem.save()
    return pricelist.id

def get_xmlid(model, res_id):
    res = OSRC.IrModelData.filter(model=model, res_id=res_id)
    if not len(res):
        raise Exception("SRC: No XML id for this product")
    return res[0].name

def get_resid(model, xmlid):
    res = ODST.IrModelData.filter(model=model, name=xmlid)
    if not len(res):
        raise Exception("DST: No XML id for this product")
    return res[0].res_id

def main():
    for tarifa in OSRC.ProductPricelist.all():
        create_tarifa(tarifa.id)

if __name__ == "__main__":
    main()
