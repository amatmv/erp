#!/usr/bin/env python
# -*- coding: utf-8 -*-
from loop import OOOP
import csv
import os
import xmlrpclib
from datetime import datetime

O5 = OOOP(dbname='oerp5_comer_fact', port=int(os.getenv('OOOP_PORT', '8050')),
         debug=int(os.getenv('DEBUG', 0)))
CSV_FILE = os.getenv('CSV_FILE', '/home/eduard/Desktop/rebuts_pendents_comer.csv')
ERR_FILE = '%s.err' % CSV_FILE
READER = csv.reader(open(CSV_FILE), delimiter=';')
if int(os.getenv('START_LINE', 0)):
    ERROR = open(ERR_FILE, 'a+')
else:
    ERROR = open(ERR_FILE, 'w')
PROCESSADES = 0
ROW_FIELDS = 12


def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))

def _get_proxy(model, versio='5'):
    """Retorna un objecte OOOP pel model.
    """
    return getattr(O5, model2cc(model))

def create_or_update(model, vals, **kwargs):
    """Creem o actualitzem segons faci o no faci match.
    """
    proxy = _get_proxy(model)
    results = proxy.filter(**kwargs)
    msg = ""
    start = datetime.now()
    if len(results):
        msg += "Updating"
        obj = results[0]
    else:
        msg += "Creating"
        obj = proxy.new()
    for key, value in vals.items():
        setattr(obj, key, value)
    obj.save()
    end = datetime.now()
    msg += " %s (id: %i)" % (model, obj.id)
    print msg
    print "Time: %.3fs." % ((end - start).seconds +
                             float((end - start).microseconds) / 10**6)
    return obj.id

def get_id(model, **kwargs):
    """Retorna l'id segons el match.
    """
    proxy = _get_proxy(model)
    results = proxy.filter(**kwargs)
    if not len(results):
        raise Exception("No s'ha trobat l'id del model: %s amb match: %s"
                        % (model, kwargs))
    return results[0].id

def parse_row(row):
    """Processa la fila.
    """
    # Stripping all values
    row = map(str.strip, row)
    
    id_vals = {}
    

    #0     contracte (polissa.name)
    #1     nom titular (res.partner.name)
    #5     acc_account (comprovar si grup pago != 0)
    #6     vat
    #7     import total
    #8     data de factura
    #9     facturacio (1, 2)
    #10    num_factura number
    #11    data_boe
    
    # Pagador
    #1 Nom
    #6 Vat
    row[6] = filter(lambda x: x.isalnum(), row[6])
    vals = {
        'name': row[1],
        'lang': 'es_ES',
        'vat': 'ES'+row[6],
    }
    try:
        id_vals['pagador'] = create_or_update('res.partner', vals,
                                              name=vals['name'],
                                              vat=vals['vat'])
    except:
        # Provem sense el NIF
        vals['vat'] = False
        vals['commnet'] = row[6]
        id_vals['pagador'] = create_or_update('res.partner', vals,
                                              name=vals['name'])
        
    # Partner address
    #2     adreça facturació (res.partner.address.street)
    #3     zip 
    #4     city
    model = 'res.partner.address'
    vals = {
        'partner_id': _get_proxy('res.partner').get(id_vals['pagador']),
        'name': _get_proxy('res.partner').get(id_vals['pagador']).name,
        'street': row[2],
        'zip': row[3],
        'city': row[4],
    }
    id_vals['direccio_pagament'] = create_or_update(model, vals,
                                             partner_id=vals['partner_id'].name,
                                             name=vals['name'],
                                             street=vals['street'],
                                             zip=vals['zip'])
    # Obtenim els valors onchange_polissa
    polissa_id = get_id('giscedata.polissa', name=row[0],
                        active__in=('True', 'False'))
    factura_proxy = _get_proxy('giscedata.facturacio.factura')
    vals = factura_proxy.onchange_polissa([], polissa_id,
                                          'out_invoice').get('value', {})
    # Hem de crear-lo
    bank_vals = {
        'acc_number': row[5],
        'state': 'bank',
        'country_id': get_id('res.country', code='ES'),
        'partner_id': id_vals['pagador'],
        'acc_country_id': get_id('res.country', code='ES')
    }
    proxy = _get_proxy('res.partner.bank')
    n_vals = proxy.onchange_banco([], bank_vals['acc_number'],
                                  bank_vals['acc_country_id'], False)
    if n_vals.get('warning', False):
        print n_vals['warning']
    else:
        bank_vals.update(n_vals['value'])
        if bank_vals.has_key('bank'):
            bank_vals['bank'] = bank_vals['bank']
            res = proxy.filter(acc_number=bank_vals['acc_number'])
            if len(res):
                id_vals['bank'] = res[0].id
                proxy.write(id_vals['bank'], bank_vals)
            else:
                id_vals['bank'] = proxy.create(bank_vals)
            vals['partner_bank'] = id_vals['bank']
        else:
            if 'partner_bank' in vals:
                del vals['partner_bank']
    # Actualitzem la data límit de pagament
    payment_term_id = get_id('account.payment.term', name='20 días fecha factura')
    vals.update(factura_proxy.onchange_payment_term_date_invoice([],
                                                    payment_term_id,
                                                    row[8]).get('value', {}))
    factura_name = '%s - IMPAGATS -' % row[10]
    polissa = _get_proxy('giscedata.polissa').get(polissa_id)
    vals2 = {
        'name': factura_name,
        'journal_id': get_id('account.journal', code='IMPAGATS'),
        'date_boe': row[11],
        'facturacio': int(row[9]),
        'partner_id': id_vals['pagador'],
        'address_invoice_id': id_vals['direccio_pagament'],
        'number': row[10],
        'date_invoice': row[8],
        'polissa_id': polissa_id,
        'payment_term': payment_term_id,
        'payment_mode_id': polissa.payment_mode_id.id,
    }
    vals.update(vals2)
    if 'cnae' in vals:
        del vals['cnae']
    res = factura_proxy.filter(number=vals['number'])
    if not len(res):
        id_vals['factura'] = factura_proxy.create(vals)
    else:
        id_vals['factura'] = res[0].id
        factura_proxy.write(res[0].id, vals)
         
    vals = {
        'name': 'Total (impuestos incluidos)',
        'price_unit_multi': float(row[7].replace(',', '.')),
        'account_id': _get_proxy('account.account').get(
                                                get_id('account.account',
                                                       code='700000')),
        'factura_id': _get_proxy('giscedata.facturacio.factura').get(id_vals['factura']),
        'tipus': 'altres',
    }
    create_or_update('giscedata.facturacio.factura.linia', vals,
                     name=vals['name'], factura_id=factura_name)
    factura_proxy.get(id_vals['factura']).invoice_open()
                
    
    

def print_row(row):
    """Print row.
    """
    i = 0
    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    for value in row:
        print '%i: %s' % (i, value)
        i += 1
    print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    print "Line: %i" % READER.line_num

def write_error(row, comment=None):
    """Write error to a file.
    """
    ERROR.write('# Line %i - %s\n' % (READER.line_num, comment or ''))
    ERROR.write(';'.join(row)+'\n')
    ERROR.flush()

def halt():
    """Halt waiting for user input.
    """
    print "Prem intro per continuar ..."
    raw_input()

def main():
    """Funció principal de l'script.
    """
    global PROCESSADES
    for row in READER:
        if READER.line_num < int(os.getenv('START_LINE', 0)):
            continue
        if row[0].startswith('#'):
            continue
        if len(row) != ROW_FIELDS:
            print "!!! %i != %i" % (len(row), ROW_FIELDS)
            write_error(row, comment="Longitud: %i diferent a %i;" % 
                        (ROW_FIELDS, len(row)))
            continue
        try:
            time_start = datetime.now()
            print_row(row)
            parse_row(row)
            PROCESSADES += 1
            time_end = datetime.now()
            print "Time elapsed: %.3fs." % ((time_end - time_start).seconds +
                            float((time_end - time_start).microseconds) / 10**6)
        except xmlrpclib.Fault, fault:
            print "Exception! %s" % fault.faultString
            write_error(row)
            if int(os.getenv('HALT_ON_ERROR', 0)):
                halt()
        except Exception, ecx:
            print "Exception! %s" % ecx
            write_error(row)
            if int(os.getenv('HALT_ON_ERROR', 0)):
                halt()
        finally:
            if int(os.getenv('STEP_BY_STEP', 0)):
                halt()

if __name__ == "__main__":
    try:
        START_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        main()
    except KeyboardInterrupt:
        print
        print 'Shutting Down...'
    finally:
        END_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print "Start at %s" % START_DATE
        print "End at %s" % END_DATE
        print "Processed: %i" % PROCESSADES
        ERROR.close()
