#!/usr/bin/env python
import re
import sys

xml = open(sys.argv[1], 'r').read()
for price_coma in re.findall("([0-9]+,[0-9]{6})", xml):
    price_dot = price_coma.replace(',', '.')
    print "%s => %s" % (price_coma, price_dot)
    xml = xml.replace(price_coma, price_dot)
xml_file = open(sys.argv[1], 'w')
xml_file.write(xml)
xml_file.close()
