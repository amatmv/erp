#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ooop import OOOP
import csv
import os
import xmlrpclib
from datetime import datetime

O5 = OOOP(dbname=os.getenv('DBNAME', 'oerp5_distri'), user='root', pwd='root',
          port=int(os.getenv('OOOP_PORT', '8050')),
         debug=int(os.getenv('DEBUG', 0)))
CSV_FILE = os.getenv('CSV_FILE', '/tmp/aspirapol5.csv')
ERR_FILE = '%s.err' % CSV_FILE
READER = csv.reader(open(CSV_FILE), delimiter=';')
if int(os.getenv('START_LINE', 0)):
    ERROR = open(ERR_FILE, 'a+')
else:
    ERROR = open(ERR_FILE, 'w')
PROCESSADES = 0
ROW_FIELDS = 169


def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))

def _get_proxy(model):
    """Retorna un objecte OOOP pel model.
    """
    return getattr(O5, model2cc(model))

def create_or_update(model, vals, **kwargs):
    """Creem o actualitzem segons faci o no faci match.
    """
    proxy = _get_proxy(model)
    results = proxy.filter(**kwargs)
    msg = ""
    start = datetime.now()
    if len(results):
        msg += "Updating"
        obj = results[0]
    else:
        msg += "Creating"
        obj = proxy.new()
    for key, value in vals.items():
        setattr(obj, key, value)
    obj.save()
    end = datetime.now()
    msg += " %s (id: %i)" % (model, obj.id)
    print msg
    print "Time: %.3fs." % ((end - start).seconds +
                             float((end - start).microseconds) / 10**6)
    return obj.id

def get_id(model, **kwargs):
    """Retorna l'id segons el match.
    """
    proxy = _get_proxy(model)
    results = proxy.filter(**kwargs)
    if not len(results):
        raise Exception("No s'ha trobat l'id del model: %s amb match: %s"
                        % (model, kwargs))
    return results[0].id

def parse_row(row):
    """Processa la fila.
    """
    # Stripping all values
    row = map(str.strip, row)
    
    id_vals = {}
    
    
    # Pòlissa
    #98     name
    
    id_vals['polissa'] = get_id('giscedata.polissa', name=row[98])
    
    # ICP
    #134    name
    #135    aportat_client
    #136    intensitat
    #137    data_alta
    #138    data_verificació
    #139    producte
    
    if row[135] and int(row[135]) > 0:
        row[135] = 'client'
    else:
        row[135] = 'empresa'
    try:
        datetime.strptime(row[137], '%Y-%m-%d')
    except:
        row[137] = False
    try:
        datetime.strptime(row[138], '%Y-%m-%d')
    except:
        row[138] = False
    if row[137] == '1900-01-01':
        row[137] = False 
    if row[138] == '1900-01-01':
        row[138] = False
    vals = {
        'polissa_id': _get_proxy('giscedata.polissa').get(id_vals['polissa']),
        'name': row[134],
        'propietat': row[135],
        'intensitat': row[136] and float(row[136].replace(',', '.')),
        'data_alta': row[137],
        'data_verificacio': row[138],
        'observacions': row[139],
    }
    create_or_update('giscedata.polissa.icp', vals, name=vals['name'],
                     polissa_id=vals['polissa_id'].name)
    
    # Butlletí
    #140    name
    #141    partner_id
    #142    partner_id_ref (NO)
    #143    installador_id
    #144    street
    #145    city
    #146    telèfon
    #147    email
    #148    fax
    #149    llicència (No)
    #150    numregistre
    #151    installador_vat
    #152    dades_installacio
    #153    activitat_us
    #154    pot_max_admisible
    #155    pot_installada
    #156    tensio
    #157    resistencia_terra
    #158    resistencia_aillament
    #159    interruptor_diferencial
    #160    derivacio_individual
    #161    eic
    #162    eic_data
    #163    doc_tecnica
    
    vals = {
        'name': row[141],
        'category_id': [_get_proxy('res.partner.category').get(8)],
    }
    if len(_get_proxy('res.partner').filter(name=vals['name'],
                                            category_id='Instalador')):
        id_vals['partner_id'] = get_id('res.partner', name=vals['name'],
                                       category_id='Instalador')
    else:
        id_vals['partner_id'] = create_or_update('res.partner', vals,
                                                 name=vals['name'],
                                                 category_id='Instalador')

    if row[143]:
        vals = {
            'name': row[143]
        }
        id_vals['installador_id'] = create_or_update('res.partner', vals,
                                                     name=vals['name'])
        
    vals = {
        'partner_id': _get_proxy('res.partner').get(id_vals['partner_id']),
        'name': row[141],
        'nv': row[144],
        'city': row[145],
        'phone': row[146],
        'email': row[147],
        'fax': row[148]
    }
    if row[141]:
        id_vals['partner_address_id'] = create_or_update('res.partner.address',
                                                     vals, name=vals['name'],
                                                     nv=vals['nv'],
                                                     city=vals['city'],
                                                     partner_id=vals['partner_id'].name)
    vals = {
        'name': row[153],
    }
    id_vals['activitat_us'] = create_or_update('giscedata.butlleti.activitat',
                                               vals, name=vals['name'])
    #156    tensio
    #157    resistencia_terra
    #158    resistencia_aillament
    #159    interruptor_diferencial
    #160    derivacio_individual
    vals = {
        'polissa_id': _get_proxy('giscedata.polissa').get(id_vals['polissa']),
        'name': row[140],
        'dades_installacio': row[152] or False,
        'activitat_us': _get_proxy('giscedata.butlleti.activitat').get(
                                                    id_vals['activitat_us']),
        'tensio': row[156] and int(row[156]),
        'partner_id': _get_proxy('res.partner').get(id_vals['partner_id']),
        'partner_address_id': 'partner_address_id' in id_vals and _get_proxy('res.partner.address').get(
                                                id_vals['partner_address_id']) or False,
        'pot_installada': row[155] and float(row[155]),
        'tensio': row[156] and int(row[156]),
        'resistencia_terra': row[157] and float(row[157]),
        'resistencia_aillament': row[158] and float(row[158]),
        'interruptor_diferencial': row[159] and float(row[159]),
        'derivacio_individual': row[160],
        
    }
    create_or_update('giscedata.butlleti', vals, name=vals['name'],
                     polissa_id=vals['polissa_id'].name)
    

def print_row(row):
    """Print row.
    """
    i = 0
    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    for value in row:
        print '%i: %s' % (i, value)
        i += 1
    print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    print "Line: %i" % READER.line_num

def write_error(row, comment=None):
    """Write error to a file.
    """
    ERROR.write('# Line %i - %s\n' % (READER.line_num, comment or ''))
    ERROR.write(';'.join(row)+'\n')
    ERROR.flush()

def halt():
    """Halt waiting for user input.
    """
    print "Prem intro per continuar ..."
    raw_input()

def main():
    """Funció principal de l'script.
    """
    global PROCESSADES
    for row in READER:
        if READER.line_num < int(os.getenv('START_LINE', 0)):
            continue
        if row[0].startswith('#'):
            continue
        if len(row) != ROW_FIELDS:
            print "!!! %i != %i" % (len(row), ROW_FIELDS)
            write_error(row, comment="Longitud: %i diferent a %i;" % 
                        (ROW_FIELDS, len(row)))
            continue
        try:
            time_start = datetime.now()
            print_row(row)
            parse_row(row)
            PROCESSADES += 1
            time_end = datetime.now()
            print "Time elapsed: %.3fs." % ((time_end - time_start).seconds +
                            float((time_end - time_start).microseconds) / 10**6)
        except xmlrpclib.Fault, fault:
            print "Exception! %s" % fault.faultString
            write_error(row)
            if int(os.getenv('HALT_ON_ERROR', 0)):
                halt()
        except Exception, ecx:
            print "Exception! %s" % ecx
            write_error(row)
            if int(os.getenv('HALT_ON_ERROR', 0)):
                halt()
        finally:
            if int(os.getenv('STEP_BY_STEP', 0)):
                halt()

if __name__ == "__main__":
    try:
        START_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        main()
    except KeyboardInterrupt:
        print
        print 'Shutting Down...'
    finally:
        END_DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print "Start at %s" % START_DATE
        print "End at %s" % END_DATE
        print "Processed: %i" % PROCESSADES
        ERROR.close()
