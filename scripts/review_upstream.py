#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import sys

CODEREVIEW_SCRIPT = '/home/eduard/Projectes/TinyERP/repository/scripts/updates/update_codereview.py'
CODEREVIEW_OPTS = '-y -s cr.gisce.net --account_type=HOSTED -rmdevera@gisce.net,pcosta@gisce.net --cc=devel@gisce.net --send_mail'


UPSTREAM_MAPPINGS = {
  'addons/extra': '/home/eduard/Projectes/ERPCom/addons/extra',
  'addons/official': '/home/eduard/Projectes/ERPCom/addons/official',
  'addons/spain': '/home/eduard/Projectes/ERPCom/addons/spain',
  'addons/koo': '/home/eduard/Projectes/OpenERP/openobject-client-kde/server-modules',
  'server': '/home/eduard/Projectes/ERPCom/server'
}

FILES_TO_REVIEW = []
MODULES_TO_REVIEW = []

BRANCH_NAME = os.getenv('UPSTREAM_BRANCH_NAME', 'UPSTREAM_%s' % time.strftime('%Y%m%d%H%M%S'))

def branch_for_upstream():
    print ' * Branching for upstream merging to %s' % BRANCH_NAME
    git_checkout_check = os.popen('git branch --no-color')
    branch_exists = False
    for line in git_checkout_check.readlines():
        if line[2:-1] == BRANCH_NAME:
            branch_exists = True
            if line.startswith('*'):
                return
    if not branch_exists:
        os.system('git checkout -b %s' % BRANCH_NAME)
    else:
        os.system('git checkout %s' % BRANCH_NAME)

def bazzar_update(path):
    print ' * Updating upstream %s' % upstream
    actual_dir = os.getcwd()
    os.chdir(path)
    os.system('bzr pull -q')
    os.chdir(actual_dir)
    
def local_update(local, upstream):
    print ' * Updating local %s' % local
    if not local.endswith('/'):
        local += '/'
    if not upstream.endswith('/'):
        upstream += '/'
    os.system('rsync -qrcC %s %s' % (upstream, local))
    
def tracking_files(local):
    print ' * Searching changed files'
    git_status_command = os.popen('git status -s %s' % local)
    for line in git_status_command.readlines():
        tracked_type,tracked_file = line.strip().split(' ')
        if tracked_type == '??':
            print '[%s] Adding %s' % (tracked_type, tracked_file)
            os.system('git add -N %s' % tracked_file)
        else:
            print '[%s] %s' % (tracked_type, tracked_file)
        FILES_TO_REVIEW.append(tracked_file)
    git_status_command.close()
    
def upload_review_and_commit(module):
    print ' * Uploading review'
    message = 'Upstream changes for %s' % module
    description = 'This is automatic codereview from upstream for the module: %s\nGit branch: %s' % (module, BRANCH_NAME)
    codereview_command = os.popen('%s %s -m "%s" -d "%s" %s' % (CODEREVIEW_SCRIPT, CODEREVIEW_OPTS, message, description, module))
    commit_message = "Autocommit merging from upstream. \n"
    ok = False
    for line in codereview_command.readlines():
        if line.startswith('Issue created. URL:'):
            ok = True
            url_review = line.strip()
    codereview_command.close()
    if not ok:
        print '*** ERROR: no review found.'
        sys.exit(0)
    print ' * Local commit: %s' % url_review
    commit_message += url_review
    os.system('git add %s' % module)
    os.system('git commit -qm "%s" %s' % (commit_message, module))
    
for local, upstream in UPSTREAM_MAPPINGS.items():
    branch_for_upstream()
    bazzar_update(upstream)
    local_update(local, upstream)
    tracking_files(local)
    module = ''
    for root, dirs, files in os.walk(local):
        if local == 'server':
            module = 'server'
        elif '__terp__.py' in files and module != 'server':
            module = root
        checks = filter(lambda f: root+'/'+f in FILES_TO_REVIEW, files)
        if checks and module not in MODULES_TO_REVIEW:
            print 'Adding %s module to review' % module
            MODULES_TO_REVIEW.append(module)
            upload_review_and_commit(module)
