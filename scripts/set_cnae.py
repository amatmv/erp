# -*- coding: utf-8 -*-
from ooop import OOOP
import csv
import os
from datetime import datetime
o = OOOP(dbname='aspirabon5', port=8050)
count = 0
csv_file = '/tmp/cnaes.csv'
reader = csv.reader(open(os.getenv('CSV_FILE', csv_file)), delimiter=',')
for row in reader:
    contracte = row[0]
    cnae = row[2]
    if not int(cnae):
        continue
    start = datetime.now()
    polissa = o.GiscedataPolissa.filter(name=contracte)
    if len(polissa):
        polissa = polissa[0]
        res = o.GiscemiscCnae.filter(name=cnae)
        if len(res):
            polissa.cnae = res[0]
            polissa.save()
            end = datetime.now()
            count += 1
            print "[%i] Time elapsed: %is." % (reader.line_num,
                                               (end - start).seconds)
print "Total: %i" % count
