-- factures Normals
UPDATE account_invoice SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA') WHERE id IN (
    SELECT invoice_id FROM giscedata_facturacio_factura WHERE
        tipo_rectificadora = 'N');
UPDATE account_move SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA') WHERE id IN (
    SELECT move_id FROM account_invoice WHERE id IN (
        SELECT invoice_id FROM giscedata_facturacio_factura WHERE
            tipo_rectificadora = 'N'));
UPDATE account_move_line SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA') WHERE id IN (
    SELECT id FROM account_move_line WHERE move_id IN (
        SELECT move_id FROM account_invoice WHERE id IN (
            SELECT invoice_id FROM giscedata_facturacio_factura WHERE
                tipo_rectificadora = 'N')));

-- factures Rectificadores
UPDATE account_invoice SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA.R') WHERE id IN (
    SELECT invoice_id FROM giscedata_facturacio_factura WHERE
        tipo_rectificadora = 'R');
UPDATE account_move SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA.R') WHERE id IN (
    SELECT move_id FROM account_invoice WHERE id IN (
        SELECT invoice_id FROM giscedata_facturacio_factura WHERE
            tipo_rectificadora = 'R'));
UPDATE account_move_line SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA.R') WHERE id IN (
    SELECT id FROM account_move_line WHERE move_id IN (
        SELECT move_id FROM account_invoice WHERE id IN (
            SELECT invoice_id FROM giscedata_facturacio_factura WHERE
                tipo_rectificadora = 'R')));

-- factures Anuladores
UPDATE account_invoice SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA.A') WHERE id IN (
    SELECT invoice_id FROM giscedata_facturacio_factura WHERE
        tipo_rectificadora = 'A');
UPDATE account_move SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA.A') WHERE id IN (
    SELECT move_id FROM account_invoice WHERE id IN (
        SELECT invoice_id FROM giscedata_facturacio_factura WHERE
            tipo_rectificadora = 'A'));
UPDATE account_move_line SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA.A') WHERE id IN (
    SELECT id FROM account_move_line WHERE move_id IN (
        SELECT move_id FROM account_invoice WHERE id IN (
            SELECT invoice_id FROM giscedata_facturacio_factura WHERE
                tipo_rectificadora = 'A')));

-- factures Anuladores amb sustituyente
UPDATE account_invoice SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA.B') WHERE id IN (
    SELECT invoice_id FROM giscedata_facturacio_factura WHERE
        tipo_rectificadora = 'B');
UPDATE account_move SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA.B') WHERE id IN (
    SELECT move_id FROM account_invoice WHERE id IN (
        SELECT invoice_id FROM giscedata_facturacio_factura WHERE
            tipo_rectificadora = 'B'));
UPDATE account_move_line SET journal_id = (SELECT id FROM account_journal WHERE
    code = 'ENERGIA.B') WHERE id IN (
    SELECT id FROM account_move_line WHERE move_id IN (
        SELECT move_id FROM account_invoice WHERE id IN (
            SELECT invoice_id FROM giscedata_facturacio_factura WHERE
                tipo_rectificadora = 'B')));
