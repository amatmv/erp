#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Generador de CSV resums de facturació i lectures
================================================

Genera fitxers CSV amb informació de facturació o lectures segons
s'especifiqui.

"""
import csv
import re
import sys
from optparse import OptionGroup, OptionParser

from loop import OOOP

__version__ = '0.1'

def run(argv):
    parser = OptionParser(usage="%prog [OPTIONS]", version=__version__)
    parser.add_option("-o", "--output", dest="fout", help="Fitxer de sortida")
    parser.add_option("-f", "--factures", dest="resum", action="store_const",
                      const=1, help=u"Fer el resum de facturació")
    parser.add_option("-m", "--mesures", dest="resum", action="store_const",
                      const=2, help="Fer el resum de mesures")
    parser.add_option("-t", "--potencies", dest="resum", action="store_const",
                      const=3, help="Fer el resum de potencies")
    parser.add_option("-e", "--periode", dest="periode",
                      help=u"El periode de liquidació/comptable (id) pel qual es farà el resum")
    parser.add_option("-D", "--distribuidora", dest="distri", default=True,
                      action="store_true", help=u"Fer el resum de distribuidora")
    parser.add_option("-C", "--comercialitzadora", dest="comer", default=False,
                      action="store_false", help=u"Fer el resum de comercialitzadora")
    group = OptionGroup(parser, "Server options")

    group.add_option("-s", "--server", dest="server", default="localhost",
                     help=u"Adreça servidor ERP")
    group.add_option("-p", "--port", dest="port", default=8069,
                     help="Port servidor ERP")
    group.add_option("-u", "--user", dest="user", default="admin",
                     help="Usuari servidor ERP")
    group.add_option("-w", "--password", dest="password", default="admin",
                     help="Contrasenya usuari ERP")
    group.add_option("-d", "--database", dest="database",
                     help="Nom de la base de dades")

    parser.add_option_group(group)
    (options, args) = parser.parse_args()

    if not options.resum:
        parser.error("S'ha de dir si volem el resum en base a factures, \
                     mesures o potencies")

    if options.distri and options.comer:
        parser.error(u"Les opcions -D i -C son exclusives mutuament")

    if not options.fout:
        parser.error("Es necessita indicar un nom de fitxer")
    if not options.periode:
        parser.error("Es necessita indicar un ID de periode")

    fout = open(options.fout, 'wb')
    fitxer = csv.writer(fout, delimiter=';')
  
    o = OOOP(dbname=options.database, user=options.user, pwd=options.password,
             port=int(options.port))
    pat = re.compile('^.*\((?P<nom>.*)\)$')
    tipus_rectificadora = {'B': 'ANULCSUST', 'A': 'ANULADORA', 'N': 'NORMAL', 'R': 'RECTIFICADORA'}
    sign = {'B': -1, 'A': -1, 'N': 1, 'R': 1}
    search_params = [('invoice_id.journal_id.code', 'not ilike', 'CONCEPTES%')]
    if options.comer or (options.distri and options.resum != 2):
        search_params += [('period_id', '=', int(options.periode))]
        factures = o.GiscedataFacturacioFactura.search(search_params)
        print(">> Resum de %s factures del periode %s" % (len(factures), options.periode))
        for factura_id in factures:
            factura = o.GiscedataFacturacioFactura.get(factura_id)
            row = []
            linies = {}
            if options.resum == 1:
                _iter = factura.linies_energia
                _tipus = 'energia'
            elif options.resum == 3:
                _iter = factura.linies_potencia
                _tipus = 'potencia'
            for e in _iter:
                if e.tipus == _tipus:
                    linies.setdefault(e.name, 0)
                    linies[e.name] += e.quantity
            taxes = {}
            for t in factura.tax_line:
                taxes[t.name] = t.amount
            row.append(tipus_rectificadora[factura.tipo_rectificadora])
            row.append(factura.number)
            row.append(factura.tarifa_acces_id.name)
            # afegim la tarifa de comer, si estem a comer
            if factura.llista_preu.name != 'TARIFAS ELECTRICIDAD':
                row.append(unicode(factura.llista_preu.name).encode('utf-8'))
            if options.distri:
                row.append(unicode(factura.partner_id.name).encode('utf-8'))
            i = 1
            for p in ('P1', 'P2', 'P3', 'P4', 'P5', 'P6'):
                row.append(linies.get(p, 0))
                i += 1
            lloguers = 0
            for ll in o.GiscedataFacturacioFacturaLinia.filter(
                factura_id=factura.id, tipus='lloguer'):
                lloguers += ll.price_subtotal
            row.append(lloguers)
            row.append(factura.amount_untaxed)
            if 'Impuesto especial sobre la electricidad' in taxes:
                row.append(taxes['Impuesto especial sobre la electricidad'])
            row.append(taxes['IVA 18%'])
            row.append(factura.amount_total)
            fitxer.writerow(row)
    elif options.distri and options.resum == 2:
        search_params += [('periode_liquidacio', '=', int(options.periode))]
        factures = o.GiscedataFacturacioFactura.search(search_params)
        print(">> Resum de %s factures del periode %s" % (len(factures), options.periode))
        for factura_id in factures:
            factura = o.GiscedataFacturacioFactura.get(factura_id)
            sign_ = sign[factura.tipo_rectificadora]
            row = []
            row.append(factura.cups_id.name)  # CUPS
            row.append(factura.tarifa_acces_id.name)  # TARIFA
            row.append(factura.data_inici)  # DATA INICI
            row.append(factura.data_final)  # DATA FINAL
            lectures = {
                'activa': {},
                'reactiva': {},
            }
            for lectura in factura.lectures_energia_ids:
                giro = lectura.comptador_id.giro
                consum = lectura.lect_actual - lectura.lect_anterior
                if consum < 0:
                    consum += giro
                _match = re.match(pat, lectura.name)
                if _match:
                    _name = _match.group('nom')
                else:
                    _name = lectura.name # per tarifes 2.X
                lectures[lectura.tipus][_name] = consum * sign_
            for tipus in ('activa', 'reactiva'):
                if tipus in lectures:
                    for periode in ('P1', 'P2', 'P3', 'P4', 'P5', 'P6'):
                        print(lectures[tipus].get(periode))
                        row.append(lectures[tipus].get(periode, 0))
                else:
                    row.extend([0]*6)
            row.append(factura.partner_id.ref)  # COMERCIALITZADORA
            row.append(factura.tarifa_acces_id.name)  # TARIFA
            row.append(factura.polissa_id.potencia)  # POTENCIA
            row.append(factura.polissa_id.name)  # N. CLIENT
            fitxer.writerow(row)
    fout.close()

if __name__ == '__main__':
    run(sys.argv[1:])

