#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from loop import OOOP

o = OOOP(dbname='LERSA_distribuidora_facturacio', user='root', pwd='root', port=8069)
FACTURES = 0

lot_id = o.GiscedataFacturacioLot.filter(name='01/2011')
if lot_id:
    lot_id = lot_id[0].id
else:
    print("ouch!")
    sys.exit(1)

for contracte_lot in o.GiscedataFacturacioContracte_lot.filter(lot_id=lot_id):
    facturador = o.GiscedataFacturacioFacturador
    polissa = contracte_lot.polissa_id
    print u'- Facturant pòlissa: %s - %s' % (polissa.name,
                                             polissa.titular.name)
    facturador.fact_via_lectures(polissa.id, lot_id)
    FACTURES += 1

print "**** Total: %i factures creades" % FACTURES

