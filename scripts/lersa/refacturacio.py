#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from optparse import OptionParser, OptionGroup
from loop import OOOP

__version__ = '0.1'

def run(argv):
    parser = OptionParser(usage="%prog OPTIONS", version=__version__)
#    parser.add_option("-f", "--factures", dest="factures", 
#                      help="Fitxer amb els IDS de factures a rectificar.")
    group = OptionGroup(parser, "Server options")
    group.add_option("-d", "--database", dest="database", default="terp",
                     help=u"Base de dades a connectar-se")
    group.add_option("-s", "--server", dest="server", default="localhost",
                     help=u"Adreça del servidor ERP")
    group.add_option("-p", "--port", dest="port", default=8069,
                     help=u"Port on escolta el servidor ERP")
    group.add_option("-u", "--user", dest="user", default="admin",
                     help=u"Nom de l'usuari per connectar")
    group.add_option("-w", "--password", dest="password", default="admin",
                     help=u"Contrasenya de l'usuari")
    parser.add_option_group(group)
    (options, args) = parser.parse_args()

#    if not options.factures:
#        parser.error(u"Es necessita saber quines factures s'ha de rectificar!")
    
    O = OOOP(dbname=options.database, user=options.user, pwd=options.password,
             uri='http://%s' % options.server, port=int(options.port))

    # Busquem les factures
    search_params = [
        ('date_invoice', '=', '2011-02-28'),
        ('tarifa_acces_id', '=', 3),
    ]
    factures = O.GiscedataFacturacioFactura.search(search_params)
    print("Es rectificaran %s factures." % len(factures))
    # creem el wizard
    wiz = O.WizardRanas.new()
    wiz_id = wiz.save()

    # llegim les factures que s'han de rectificar
    for fid in factures:
        fact = O.GiscedataFacturacioFactura.get(fid)
        # anul·lem d'una en una per fer un print i tenir feedback
        f_rects = wiz._action('rectificar', {'active_ids': [fid]})
        # esborrem les factures de distri, que no les volem per res, estan OK.
        O.GiscedataFacturacioFactura.unlink(f_rects)
        sys.stdout.write('.')
        sys.stdout.flush()
    sys.stdout.write('\n')

if __name__ == '__main__':
    run(sys.argv[1:])

