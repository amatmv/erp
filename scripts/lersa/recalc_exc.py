#!/usr/bin/env python
# -*- coding: utf-8 -*-

from loop import OOOP

TARIFES = ('3.0A', '3.1A', '3.1A LB')
P_AGRUPATS = [('P1', 'P4'), ('P2', 'P5'), ('P3', 'P6')]

o = OOOP(dbname='LERSA_distri_fcomer2', port=8059)

"""
interval = int((potencia_maximetre/potencia_contractada) * 100)
if interval > 105:
    return potencia_maximetre + (2 * (potencia_maximetre
                              - (1.05 * potencia_contractada)))
elif interval >= 85 and interval <= 105:
    return potencia_maximetre
else:
    return potencia_contractada * 0.85

"""
# busquem les factures
factures = []
for l in o.GiscedataFacturacioLecturesPotencia.all():
    if (l.pot_maximetre > l.pot_contract and 
        l.factura_id.id not in factures and
        l.factura_id.tarifa_acces_id.name in TARIFES):
        factures.append(l.factura_id.id)
        print("S'ha de modificar la factura %s" % (l.factura_id.number,))

nfactures = 0
for f in o.GiscedataFacturacioFactura.filter(id__in=factures):
    maximetres = {}
    lectures = {}
    intervals = {}
    contractades = {}
    if f.tarifa_acces_id.name in TARIFES:
        # emplenem dades de lectures i línies de factura
        for lp in f.lectures_potencia_ids:
            lectures[lp.name] = lp.pot_maximetre
            contractades[lp.name] = lp.pot_contract
            intervals[lp.name] = int((lp.pot_maximetre/lp.pot_contract) *
                                     100)

        for p in P_AGRUPATS:
            maximetres[p[0]] = max(lectures[p[0]],
                                   lectures[p[1]])
            intervals[p[0]] = max(intervals[p[0]],
                                  intervals[p[1]])

        for lf in f.linies_potencia:
            if intervals[lf.name] > 105:
                lf.quantity = (maximetres[lf.name] + 
(2 * (maximetres[lf.name] - (1.05 * contractades[lf.name]))))
            elif intervals[lf.name] >= 85 and intervals[lf.name] <= 105:
                lf.quantity = maximetres[lf.name]
            else:
                lf.quantity = 0.85 * contractades[lf.name]
            lf.save()
        f.button_reset_taxes()
        nfactures += 1

print("Corregides %s factures de %s" % (nfactures, len(factures)))
