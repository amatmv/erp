#!/usr/bin/env python
# -*- coding: utf-8 -*-
from loop import OOOP
o = OOOP(dbname='LERSA_comercialitzadora', port=8049)
REMESA_ID = 3
lines = o.PaymentLine.search([('order_id', '=', REMESA_ID)])
print "Pagant %i linies de remesa.." % len(lines)
for lid in lines:
    linia = o.PaymentLine.get(lid)
    if not linia.ml_inv_ref:
        continue
    if linia.ml_inv_ref.reconciled:
        # la factura està "conciliada", la retornem
        o.AccountInvoice.undo_payment([linia.ml_inv_ref.id])
    print linia.amount_currency, linia.date, linia.ml_inv_ref.number
    invoice_id = linia.ml_inv_ref.id
    pay_amount = abs(linia.amount_currency)
    pay_account_id = 881  # Bancs 572
    period_id = o.AccountInvoice.get(invoice_id).period_id.id
    pay_journal_id = 6  # BANC
    context = {'date_p': linia.date, 'type': 'out_invoice'}
    writeoff_acc_id = False
    writeoff_period_id = False
    writeoff_journal_id = False
    o.AccountInvoice.pay_and_reconcile([invoice_id],
                                       pay_amount,
                                       pay_account_id,
                                       period_id,
                                       pay_journal_id,
                                       writeoff_acc_id,
                                       period_id,
                                       writeoff_journal_id,
                                       context,
                                       linia.ml_inv_ref.number)
