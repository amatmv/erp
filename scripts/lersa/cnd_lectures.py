#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from loop import OOOP

"""
Comprova les lectures amb cnd_lectures()
"""
o = OOOP(dbname='LERSA_distribuidora_facturacio', user='root', pwd='root',
         port=8069)

lot_id = o.GiscedataFacturacioLot.filter(name='01/2011')
if lot_id:
    lot_id = lot_id[0].id
else:
    print("ouch!")
    sys.exit(1)

for clot in o.GiscedataFacturacioContracte_lot.filter(lot_id=lot_id):
    if not clot.cnd_lectures():
        print(" * La pòlissa %s NO té les lectures OK." %
                                                        (clot.polissa_id.name))

