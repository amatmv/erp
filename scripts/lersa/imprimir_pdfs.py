#!/usr/bin/python
# -*- coding: utf-8 -*-

import optparse
import ConfigParser
import xmlrpclib
import base64
import calendar
import time
import sys

parser = optparse.OptionParser(version="[GISCE ERP] Migrations")

group = optparse.OptionGroup(parser, "SERVER Related")
group.add_option("--host", dest="host", help="specify the database host") 
group.add_option("--port", dest="port", help="specify the database port") 
group.add_option("-d", "--database", dest="dbname", help="specify the database name")
group.add_option("-u", "--user", dest="user", help="specify the username")
group.add_option("-p", "--password", dest="password", help="specify the password")
group.add_option("--lot", dest="lot", help="specify the lot")
group.add_option("--limit", dest="period", help="specify the limit")
parser.add_option_group(group)

options = optparse.Values()
options.db_name = 'terp' # default value
options.host = 'localhost'
options.user = 'admin'
options.port = '8069'
options.limit = '1000'

parser.parse_args(values=options)

host = options.host
port = options.port
dbname = options.dbname
user = options.user
pwd = options.password
period = options.period
limit = int(options.limit)

pb = ['|', '/', '-', '\\', '|', '/', '-']
i = 0
j = 0

def up():
	global i,j
	i += 1
	if i%1 == 0:
		j += 1

sock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/common' % (host,int(port)))
uid = sock.login(dbname ,user ,pwd)
sock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/object' % (host,int(port)))
printsock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/report' % (host,int(port)))

try:
	month,year = period.split('/')
	data_final = '%s-%s-%s' % (year, month, calendar.monthrange(int(year), int(month))[1])
	# Busquem el periode
	pid = sock.execute(dbname, uid, pwd, 'giscedata.facturacio.lot', 'search', [('name','=',lot)])
	if len(pid):
		pid = pid[0]
	else:
		sys.stderr.write('**** NO SE HA ENCONTRADO UN PERIODO PARA %s' % period)
		sys.stderr.flush()
	# Busquem les diferents comercialitzadores de moment seran tots els partners
	# que ref no sigui null
	comers_ids = sock.execute(dbname, uid, pwd, 'res.partner', 'search', [('ref', '!=', False)])
	comers = {}
	for c in sock.execute(dbname, uid, pwd, 'res.partner', 'read', comers_ids, ['name', 'ref']):
		comers[c['id']] = {'ref': c['ref'], 'name': c['name']}
		
	for cid in comers_ids:
		sys.stdout.write('[*] Buscando facturas para %s...' % (comers[cid]['ref']))
		sys.stdout.flush()
		
		# Obtenim totes les factures d'aquest periode per la comercialitzacora
		count = sock.execute(dbname, uid, pwd, 'giscedata.facturacio.factura',
        'search_count', [('date_invoice','=',lot), ('comercialitzadora', '=', cid)])
		sys.stdout.write('%i\n' % count)
		sys.stdout.flush()
		
		offset = 0

		while offset < count:
			ids = sock.execute(dbname, uid, pwd,
            'giscedata.facturacio.factura', 'search', [('date_invoice', '=',
            lot), ('comercialitzadora', '=', cid)], offset, limit)
			id_report = printsock.report(dbname, uid, pwd,
                'giscedata.facturacio.factura', ids, {'model':
                'giscedata.facturacio.factura', 'id': ids[0], 
                'report_type':'pdf'})
			sys.stdout.write('  [+] Generant fitxer de %i a %i...' % (offset+1,offset+len(ids)))
			sys.stdout.flush()
			time.sleep(5)
			state = False
			sys.stdout.write('[|]')
			sys.stdout.flush()
			while not state:
				sys.stdout.write('\b\b%s]' % (pb[j%len(pb)]))
				sys.stdout.flush()
				report = printsock.report_get(dbname, uid, pwd, id_report)
				state = report['state']
				if not state:
					time.sleep(0.25)
				up()
			
			filename = 'FACTURACION_%s_%s%s_%s_%s_%s.pdf' % (dbname, year, month, comers[cid]['ref'], offset+1, offset+len(ids))

			sys.stdout.write('\n    => %s\n' % filename)
			sys.stdout.flush()
			f = open(filename, 'wb')
			f.write(base64.decodestring(report['result']))
			f.flush()
			f.close()
			
			offset += limit

except KeyboardInterrupt:
	print '\nShutting Down...'

