#!/usr/bin/env python
# -*- coding: utf-8 -*-

from loop import OOOP

d = OOOP(dbname='LERSA_D_XX', user='root', pwd='root', port=8000)
c = OOOP(dbname='LERSA_febrer_comer_per_facturar', user='admin', pwd='admin',
         port=8001)

# carreguem ids...
lot_id = d.GiscedataFacturacioLot.filter(name='02/2011')
if lot_id:
    lot_id = lot_id[0].id

for factura in d.GiscedataFacturacioFactura.search([('lot_facturacio','=',lot_id)]):
    linies = d.GiscedataFacturacioFacturaLinia.filter(factura_id=factura,
                                                      tipus='reactiva')
    if len(linies):
        # aquestes són les línies de reactiva facturades
        # buscar-la a comer i actualitzar-li el preu
        linies_d = {}
        for lin in linies:
            linies_d[lin.name] = lin
        fac = d.GiscedataFacturacioFactura.get(factura)
        ref_comer = c.GiscedataPolissa.filter(ref_dist=fac.polissa_id.name)
        if ref_comer:
            ref_comer = ref_comer[0]
        else:
            continue
        print(u">> Modificant factura de la pòlissa %s (%s)..." %
                                                        (fac.polissa_id.name,
                                                        ref_comer.name))

        f_comer = c.GiscedataFacturacioFactura.filter(polissa_id=ref_comer.id,
                                                      date_invoice='2011-02-28')
        if f_comer:
            f_comer = f_comer[0]
        else:
            print f_comer
            continue
        for linia_c in c.GiscedataFacturacioFacturaLinia.filter(
                         factura_id=f_comer.id, tipus='reactiva'):
            print("%s,%s,%s" % (fac.polissa_id.name,linia_c.id,linia_c.name) )
            """
            print("> Actualitzant preu de %s a %s" % (linia_c.price_unit,
                                    linies_d[linia_c.name].price_unit))
            linia_c.price_unit_multi = linies_d[linia_c.name].price_unit_multi
            linia_c.price_unit = linies_d[linia_c.name].price_unit
            linia_c.save()
            """
print("The End")
