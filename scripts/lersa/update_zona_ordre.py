#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
from optparse import OptionGroup, OptionParser

from ooop import OOOP

__version__ = '0.1.0'

"""Actualitza els camps zona i ordre de giscedata_polissa segons 
   fitxer d'entrada."""

# Definició de la posició del camp en el CSV
NAME = 0
ZONA_CARTA = 6
ORDRE_CARTA = 7

def main(fitxer):
    """Funció principal
    """
    reader = csv.reader(open(fitxer, 'rb'), delimiter=';')
    print 'FILE: %s' % fitxer
    for row in reader:
        if row[ZONA_CARTA]:
            search_params = [('name', '=', row[NAME])]
            polissa_id = O.GiscedataPolissa.search(search_params)
            if polissa_id:
                O.GiscedataPolissa.write(polissa_id, {'zona_carta': 
                                                       row[ZONA_CARTA]})
                if row[ORDRE_CARTA]:
                    O.GiscedataPolissa.write(polissa_id, {'ordre_carta': 
                                                           row[ORDRE_CARTA]})
            else:
                print "[ERR] polissa.name %s inexistent" % row[0]


if __name__ == '__main__':

    try:
        parser = OptionParser(usage="%prog [OPTIONS]", version=__version__)
        parser.add_option("-i", "--input", dest="datafile",
                help="Fitxer d'entrada")
    
        group = OptionGroup(parser, "Server options")
        group.add_option("-s", "--server", dest="server", default="localhost",
                help=u"Adreça servidor ERP")
        group.add_option("-p", "--port", dest="port", default=8069,
                help="Port servidor ERP")
        group.add_option("-u", "--user", dest="user", default="admin",
                help="Usuari servidor ERP")
        group.add_option("-w", "--password", dest="password", default="admin",
                help="Contrasenya usuari ERP")
        group.add_option("-d", "--database", dest="database",
                help="Nom de la base de dades")
    
        parser.add_option_group(group)
        (options, args) = parser.parse_args()
        O = OOOP(dbname=options.database, user=options.user, pwd=options.password,
                     port=int(options.port))
    
        main(options.datafile)
    
    except KeyboardInterrupt:
        pass


