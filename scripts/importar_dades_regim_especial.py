#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv

from loop import OOOP
from fuzzywuzzy import process

# ----------------------
# Camps fitxer CSV
# ----------------------
extCups = 0
extName = 1
extCarrer = 2
extMunicipi = 3
extProvincia = 4
extNif = 5
extTipus = 6

# ----------------------
# Camps fitxer Municipis
# ----------------------
datINE = 0
datMunicipi = 1
datComarca = 2

# ----------------------
# Fitxers CSV
# ----------------------
strFileRE = "/tmp/12_03_13_Clients RE.csv"
# strFileMunicipis es pot obtenir de:
# http://municat.gencat.cat/upload/descarregues/tots_ens.xls
strFileMunicipis = "/home/cpey/Documents/codis/municipis_i_comarques_Cat"

# ----------------------
# Codis provincies REE
# ----------------------
codis_provincies = {
    'Girona': 'GI',
    'Barcelona': 'B',
    'Lleida': 'L',
    'Tarragona': 'T'
}

O = OOOP(dbname='', port=8069, user='', pwd='', uri='')

def get_llista_municipis():
    fdMun = open(strFileMunicipis,'r')
    objMun = csv.reader(fdMun, delimiter=',')
    municipis = {}
    for row in objMun:
        municipis[row[datMunicipi]] = row[datINE][:6]
    fdMun.close()
    return municipis

municipis = get_llista_municipis()

def get_poblacio_id(mun_id):
    pob_id = O.ResPoblacio.search([('municipi_id', '=', mun_id)])
    if not pob_id:
        vals = {
            'name': O.ResMunicipi.read(mun_id, ['name'])['name'],
            'municipi_id': mun_id
        }
        pob_id = O.ResPoblacio.create(vals)
    else:
        pob = O.ResPoblacio.read(pob_id, ['name'])
        dict_pob = dict(((i['name'], i['id']) for i in pob))
        res = process.extractOne(strMun, dict_pob.keys())
        pob_id = dict_pob[res[0]]
    return pob_id

def get_cups_id(strCups, strCarr, strMun, strProv):
    cups = O.GiscedataCupsPs.search([('name', '=', strCups)],
                                      0, 0, 'name asc', {'active_test': False})
    if not cups:
        mun_id = get_municipi_id(strMun, strProv, municipis)
        vals = {
            'name': strCups,
            'carrer': strCarr,
            'id_municipi': mun_id,
            #'poblacio': get_poblacio_id(mun_id)
            'poblacio': get_municipi_name(mun_id)
        }
        cups_id = O.GiscedataCupsPs.create(vals)
    else:
        cups_id = cups[0]
    return cups_id

def get_titular_id(name, nif):
    p_id = O.ResPartner.search([('vat', '=', nif)], 0, 0, 'name asc')
    if p_id:
        p_id = p_id[0]
    else:
        vals = {
            'name': name,
            'vat': nif
        }
        p_id = O.ResPartner.create(vals)
    return p_id

def get_provincia_name(prov_id):
    name = ''
    mun =  O.ResCountryState.read(prov_id, ['name'])
    if mun:
        name = mun['name']
    return name

def get_provincia_id(prov):
    prov_id = O.ResCountryState.search([('name', 'ilike', prov)])
    if not prov_id:
        st_id = O.ResCountry.search([('code', '=', 'ES')])
        rc_id = O.ResCountryState.search([('country_id', '=', st_id[0])])
        rc = O.ResCountryState.read(rc_id, ['name'])
        provincies = dict(((i['name'], i['id']) for i in rc))
        res = process.extractOne(prov, provincies.keys())
        if not res[1] >= 90:
            msg = u"No s'ha pogut localitzar la província %s en l'ERP" % prov
            raise Exception('Error', msg)
        prov_id = provincies[res[0]]
    else:
        noms_prov = O.ResCountryState.read(prov_id)
        dict_prov = dict(((i['name'], i['id']) for i in noms_prov))
        res = process.extractOne(prov, dict_prov.keys())
        prov_id = dict_prov[res[0]]
    return prov_id

def get_municipi_name(mun_id):
    name = ''
    mun =  O.ResMunicipi.read(mun_id, ['name'])
    if mun:
        name = mun['name']
    return name

def get_municipi_id(mun, prov, municipis):
    mun = mun.lower()
    mun = 'sta ' in mun and mun.replace('sta ', 'santa ') or mun
    mun = 'st ' in mun and mun.replace('st ', 'sant ') or mun
    mun_id = O.ResMunicipi.search([('name', 'ilike', mun)])
    if not mun_id:
        prov_id = get_provincia_id(prov)
        res = process.extractOne(mun, municipis.keys())
        if not res[1] >= 90:
            msg = u"No s'ha pogut localitzar el municipi %s en el fitxer "\
                  u"de municipis" % mun
            raise Exception('Error', msg)
        mun_id = O.ResMunicipi.search([('name', 'ilike', res[0])])
        if not mun_id:
            vals = {
                'ine': municipis[res[0]],
                'state': prov_id,
                'name': res[0]
            }
            mun_id = O.ResMunicipi.create(vals)
        else:
            mun_id = mun_id[0]
    else:
        noms_mun = O.ResMunicipi.read(mun_id)
        dict_mun = dict(((i['name'], i['id']) for i in noms_mun))
        res = process.extractOne(mun, dict_mun.keys())
        mun_id = dict_mun[res[0]]
    return mun_id


fdRE=open(strFileRE,'r')
objRE=csv.reader(fdRE, delimiter=';')

for row in objRE:
    strCups = row[extCups]
    strName = row[extName]
    strCarr = row[extCarrer]
    strMun = row[extMunicipi]
    strProv = row[extProvincia]
    strNif = row[extNif]
    strTipus = row[extTipus]
    try:
        vals = {
            'cups': get_cups_id(strCups, strCarr, strMun, strProv),
            'name': strName,
            'titular': get_titular_id(strName, strNif),
            'provincia': codis_provincies[
                            get_provincia_name(get_provincia_id(strProv))],
            'tipo': strTipus
        }
    except Exception, e:
        print e
    O.GiscedataRe.create(vals)

fdRE.close()

print("Fet!")

