#!/bin/bash

# Detect if bash is executed in windows
windows() { [[ -n "$WINDIR" ]]; }

# Makes a symbolic link in current folder
link() {    
    target="$(basename $1)"
    if ! [ -e $target ]; then
        if windows; then             
            cmd <<< "mklink /J \"./${target}\" \"$1\"" > /dev/null
        else
            ln -s $1 .
        fi
    fi
}

# Remove symbolic link
rmlink() {
    if [ -e $1 ]; then
        if windows; then
            rm -f "$1"
        else
            unlink $1
        fi
    fi
}

cd server/bin/addons

echo '* Removing links...'
find . -type l -exec rm -f {} \;

echo '* Finding addons...'
ADDONS=`find ../../../addons -name '__terp__.py' -exec dirname {} \;`

echo '* Finding extra addons...'
EXTRA_ADDONS+=" ../../../../spawn_oop/spawn_oop"
EXTRA_ADDONS+=" ../../../../ws_transactions"
EXTRA_ADDONS+=" ../../../../openerp-sentry/sentry"
EXTRA_ADDONS+=" ../../../../poweremail2"
EXTRA_ADDONS+=" ../../../../aeroo/report_aeroo"
EXTRA_ADDONS+=" ../../../../mongodb_backend"
EXTRA_ADDONS+=" ../../../../oorq/oorq"
EXTRA_ADDONS+=" ../../../../poweremail_oorq/poweremail_oorq"
EXTRA_ADDONS+=" ../../../../ir_attachment_mongodb/ir_attachment_mongodb"
EXTRA_ADDONS+=" ../../../../erp-empowering/empowering_customize"
EXTRA_ADDONS+=" ../../../../erp-empowering/empowering_api"
EXTRA_ADDONS+=" ../../../../erp-empowering/empowering_api_online"
EXTRA_ADDONS+=" ../../../../cchloader/cchloader"
EXTRA_ADDONS+=" ../../../../somenergia-generationkwh/som_generationkwh"
EXTRA_ADDONS+=" ../../../../plantmeter/som_plantmeter"
EXTRA_ADDONS+=" ../../../../crm_poweremail"

for E_ADDON in $EXTRA_ADDONS; do
    if [ -e $E_ADDON ]; then
        ADDONS+=" $E_ADDON"
    fi;
done;

echo '* Finding custom indexed invoicing modules...'
IDX_ADDONS=`find ../../../../giscedata_facturacio_indexada_* -name '__terp__.py' -exec dirname {} \;`

for I_ADDON in $IDX_ADDONS; do
    if [ -e $I_ADDON ]; then
        ADDONS+=" $I_ADDON"
    fi;
done;

echo '* Finding ecasa custom modules...'
ECASA_ADDONS=`find ../../../../ecasa-modules/ -name '__terp__.py' -exec dirname {} \;`

for ECASA_ADDON in $ECASA_ADDONS; do
    if [ -e $ECASA_ADDON ]; then
        ADDONS+=" $ECASA_ADDON"
    fi;
done;

echo '* Finding agri custom modules...'
AGRI_ADDONS=`find ../../../../agri-modules/ -name '__terp__.py' -exec dirname {} \;`

for AGRI_ADDON in $AGRI_ADDONS; do
    if [ -e $AGRI_ADDON ]; then
        ADDONS+=" $AGRI_ADDON"
    fi;
done;

echo '* Finding lumina custom modules...'
LUMINA_ADDONS=`find ../../../../lumina-modules/ -name '__terp__.py' -exec dirname {} \;`

for LUMINA_ADDON in $LUMINA_ADDONS; do
    if [ -e $LUMINA_ADDON ]; then
        ADDONS+=" $LUMINA_ADDON"
    fi;
done;

echo '* Linking modules'
for ADDON in $ADDONS; do	
    link $ADDON	
    echo "  => `basename ${ADDON}`"
done;

echo '* Removing modules'
rmlink giscedata_as400link
rmlink partner_export
rmlink giscevoip_partner

if [ -e poweremail2 ]; then    
    echo "* Renaming poweremail2 to poweremail"
    rmlink poweremail
    mv poweremail2 poweremail
fi
