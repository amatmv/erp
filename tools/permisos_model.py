#!/usr/bin/python

import os
import sys, getopt
import tempfile

query_local_perm = "SELECT" \
        "    '\"access_'|| replace(m.model,'.','_')||'_r\",\"' ||m.model||'\",\"model_'|| replace(m.model,'.','_')||'\",\"' || CASE WHEN length('{base}') > 0 THEN '{base}'||'.' ELSE '' END ||'group_'|| '{base}' ||'_r\",1,0,0,0' as permis" \
        " FROM ir_model_data i left join ir_model m on (i.res_id = m.id)" \
        " WHERE " \
        "    i.module like '{modul}' and i.model = 'ir.model' " \
        " UNION ALL " \
        " SELECT" \
        "    '\"access_' || replace(m.model,'.','_')||'_w\",\"' ||m.model||'\",\"model_' || replace(m.model,'.','_')||'\",\"' || CASE WHEN length('{base}') > 0 THEN '{base}'||'.' ELSE '' END ||'group_'|| '{base}' ||'_w\",1,1,1,0' as permis" \
        " FROM ir_model_data i left join ir_model m on (i.res_id = m.id)" \
        " WHERE" \
        "    i.module like '{modul}' and i.model = 'ir.model'" \
        " UNION ALL" \
        " SELECT" \
        "   '\"access_' || replace(m.model,'.','_')||'_u\",\"'||m.model||'\",\"model_'|| replace(m.model,'.','_')||'\",\"' || CASE WHEN length('{base}') > 0 THEN '{base}'||'.' ELSE '' END ||'group_'|| '{base}'||'_u\",1,1,1,1' as permis" \
        " FROM ir_model_data i left join ir_model m on (i.res_id = m.id)" \
        " WHERE" \
        "    i.module like '{modul}' and i.model = 'ir.model'"

query_foreign_perm = "SELECT " \
                     " distinct '\"access_foreign_'|| replace(imf.relation, '.','_') ||'_{permis}\",\"' ||imf.relation||'\",\"'||imd2.module || '.model_' || replace(imf.relation, '.','_')||'\",\"' || CASE WHEN length('{base}') > 0 THEN '{base}'||'.' ELSE '' END ||'group_'|| '{base}' ||'_{permis}\",1,0,0,0' as permis " \
                     " FROM ir_model_data imd INNER JOIN ir_model_fields imf  " \
                     " ON (imd.res_id = imf.id AND imf.ttype IN ('many2one', " \
                     "              'one2many','many2many') AND imf.relation IS NOT null)" \
                     "  INNER JOIN ir_model imm    ON (imf.relation = imm.model) " \
                     " INNER JOIN ir_model_data imd2  ON (imm.id = imd2.res_id AND imd2.model = 'ir.model' AND imd2.module != '{modul}' and imd2.module!='{base}')" \
                     " WHERE imd.model = 'ir.model.fields' AND imd.module = '{modul}'"

ir_model_access_csv = 'ir.model.access.csv'
create_directory = 'mkdir -p %s/security'
create_file = 'echo \'"id","name","model_id:id","group_id:id","perm_read","perm_write","perm_create","perm_unlink"\' > %s/security/'+ir_model_access_csv
add_permissions = 'psql -tA {database} -f {query} >> {folder}/security/'+ir_model_access_csv

def generate_foreign_permisions(inputfolder, database, base, modul):
    try:

        fp, path = tempfile.mkstemp()
        modul_query = query_foreign_perm.format(base=base, modul=modul,
                                                permis='r')
        os.write(fp, modul_query)
        os.write(fp, ' UNION ')
        modul_query = query_foreign_perm.format(base=base, modul=modul,
                                                permis='w')
        os.write(fp, modul_query)
        os.write(fp, ' UNION ')
        modul_query = query_foreign_perm.format(base=base, modul=modul,
                                                permis='u')
        os.write(fp, modul_query)

        if base != modul:
            base_query = ' EXCEPT '
            base_query += query_foreign_perm.format(base=base, modul=base,
                                                    permis='r')
            os.write(fp, base_query)
            base_query = ' EXCEPT '
            base_query += query_foreign_perm.format(base=base, modul=base,
                                                    permis='w')
            os.write(fp, base_query)
            base_query = ' EXCEPT '

            base_query += query_foreign_perm.format(base=base, modul=base,
                                                    permis='u')
            os.write(fp, base_query)

        os.write(fp, ' order by permis ')
        os.close(fp)
        os.system(add_permissions.format(database=database, query=path,
                                         folder=inputfolder))
        os.remove(path)
        return True
    except Exception:
        return False

def generate_permisions(inputfolder, database, base, modul):
    try:
        if os.path.exists(inputfolder + '/security'):

            if os.path.exists(inputfolder + '/security/' + ir_model_access_csv):
                pass
            else:
                os.system(create_file % inputfolder)
        else:
            os.system(create_directory % inputfolder)
            os.system(create_file % inputfolder)

        fp, path = tempfile.mkstemp()
        current_query = query_local_perm.format(base=base, modul=modul)
        os.write(fp, current_query)
        os.write(fp, ' order by permis ')
        os.close(fp)
        os.system(add_permissions.format(database=database, query=path,
                                     folder=inputfolder))
        os.remove(path)
        if generate_foreign_permisions(inputfolder, database, base, modul):
            print u'S\'ha generat correctament'
        else:
            print u'Hi ha hagut problemes en la generacio de permisos'

    except Exception:
        print 'no s\'ha pogut generar el fitxer'



def print_usage(filename, status):
    print '%s [--no-interactive][--foreign] -i <inputfile> -d <database>' \
          ' -b <base> -m <modul>' % filename.split('/')[-1]
    sys.exit(status)


def main(filename, argv):
    inputfolder = ''
    database  = ''
    base = ''
    modul = ''
    foreign = False
    interactive = True

    try:
        opts, args = getopt.getopt(argv, "hi:d:b:m:nf", ["ifile=",
                                                           "database=",
                                                           "base=",
                                                           "modul=",
                                                           "no-interactive=",
                                                           "foreign=",
                                                           ])
    except getopt.GetoptError:
        print_usage(filename, 2)
    for opt, arg in opts:
        if opt == '-h':
            print_usage(filename, None )
        elif opt in ("-i", "--ifile"):
            inputfolder = arg
        elif opt in ("-d", "--database"):
            database = arg
        elif opt in ("-b", "--base"):
            base = arg
        elif opt in ("-m", "--modul"):
            modul = arg
        elif opt in ("-f", "--foreign"):
            foreign = True
        elif opt in ("-n", "--no-interactive"):
            interactive = False

    if interactive:
        inputfolder = raw_input('A quina carpeta vols crear els permisos?')
        database = raw_input('Quina BD voleu utilitzar?')
        base = raw_input('Quina base tenen els permisos '
                         '(si el modul es base deixa\'l buit)?')
        modul = raw_input('De quin modul es fan els permissos?')

    if not database and not inputfolder and not modul:
        print_usage(filename, 2)
    else:
        if foreign:
            generate_foreign_permisions(inputfolder, database, base, modul)
        else:
            generate_permisions(inputfolder, database, base, modul)

if __name__ == "__main__":
    main(sys.argv[0], sys.argv[1:])
