# coding=utf-8
from os.path import islink, dirname, realpath, join, basename
from os import symlink, remove, listdir, walk
from tqdm import tqdm
import click


BASE_PATH = dirname(dirname(realpath(__file__)))
SERVER_ADDONS_PATH = join(BASE_PATH, "server/bin/addons")
ADDONS_PATH = join(BASE_PATH, "addons")

SKIP_MODULES = [
    'giscedata_as400link',
    'partner_export',
    'giscevoip_partner',
    'poweremail',
]


def find_modules(path=ADDONS_PATH):
    for root, dirs, files in walk(path):
        if '__terp__.py' in files:
            yield root


def find_indexada_modules(path):
    for _dir in listdir(path):
        if 'giscedata_facturacio_indexada_' not in _dir:
            continue
        addon_path = join(path, _dir)
        if '__terp__.py' in listdir(addon_path):
            yield addon_path


def link_modules(path=ADDONS_PATH, verbose=False, quiet=False):
    total_linked = 0
    total_overriden = 0
    for module in find_modules(path):
        total_linked += 1
        mod_name = basename(module)
        if mod_name in SKIP_MODULES:
            pass
        if mod_name == 'poweremail2':
            mod_name = 'poweremail'
        new_path = join(SERVER_ADDONS_PATH, mod_name)
        try:
            symlink(module, new_path)
            if verbose:
                print('  => {}'.format(mod_name))
        except OSError:
            remove(new_path)
            symlink(module, new_path)
            total_overriden += 1
            if verbose:
                print ('/!\\  => {}'.format(mod_name))
    if not quiet:
        print('  => {} Linked ({} Overriden)'.format(
            total_linked, total_overriden))
    return total_linked, total_overriden


@click.command()
@click.option(
    '-v', '--verbose', default=False, is_flag=True,
    help='Show all links removed and added'
)
@click.option(
    '-q', '--quiet', default=False, is_flag=True,
    help="Don't even show how many modules linked"
)
def link_addons(**kwargs):
    print('* Removing links...')
    for module in tqdm([
        d for d in listdir(SERVER_ADDONS_PATH)
        if islink(join(SERVER_ADDONS_PATH, d))
    ]):
        mod_path = join(SERVER_ADDONS_PATH, module)
        remove(mod_path)

    # FIXED MODULES
    addons_list = [
        # BASE
        ADDONS_PATH,
        # SOM
        join(BASE_PATH, '../erp-empowering'),
        join(BASE_PATH, '../somenergia-generationkwh'),
        # ECASA
        join(BASE_PATH, '../ecasa-modules'),
        # AGRI
        join(BASE_PATH, '../agri-modules'),
        # LUMINA
        join(BASE_PATH, '../lumina-modules'),
        # UTILS
        join(BASE_PATH, '../plantmeter'),
        join(BASE_PATH, '../aeroo'),
        join(BASE_PATH, '../mongodb_backend'),
        join(BASE_PATH, '../ir_attachment_mongodb'),
        join(BASE_PATH, '../cchloader'),
        join(BASE_PATH, '../oorq'),
        join(BASE_PATH, '../spawn_oop'),
        join(BASE_PATH, '../ws_transactions'),
        join(BASE_PATH, '../openerp-sentry'),
        # POWEREMAIL
        join(BASE_PATH, '../poweremail2'),
        join(BASE_PATH, '../poweremail_oorq'),
        join(BASE_PATH, '../crm_poweremail'),
    ]
    # INDEXADA
    addons_list += find_indexada_modules(dirname(BASE_PATH))

    print('* Linking modules')
    total_links = 0
    total_overrides = 0
    for add_path in addons_list:
        if not kwargs.get('quiet', False):
            print(' - {}'.format(add_path))
        linked, overriden = link_modules(
            add_path,
            verbose=kwargs.get('verbose', False),
            quiet=kwargs.get('quiet', False),
        )
        total_links += linked
        total_overrides += overriden
    print('Linked {}/{} ({} Overriden)'.format(
        total_links-total_overrides,
        total_links,
        total_overrides
    ))
    print('Total modules: {}'.format(len(listdir(SERVER_ADDONS_PATH))))


if __name__ == '__main__':
    link_addons()
