FROM python:2.7
MAINTAINER GISCE-TI, S.L <devel@gisce.net>

ARG GITHUB_TOKEN

RUN virtualenv /home/erp

ENV LIB /home/erp/src
RUN mkdir -p ${LIB}

COPY requirements* ./
RUN /home/erp/bin/pip install cython lxml psycopg2 egenix-mx-base babel \
    vatnumber reportlab==3.0 osconf -r requirements.txt -r requirements-dev.txt

RUN /home/erp/bin/pip install --upgrade pip
RUN /home/erp/bin/pip install scipy pandas
RUN curl -H "Authorization: token ${GITHUB_TOKEN}" -L https://api.github.com/repos/gisce/pandapower_validator/zipball > /tmp/pandapower_validator.zip
RUN curl -H "Authorization: token ${GITHUB_TOKEN}" -L https://github.com/gisce/pandapower_erp/archive/master.zip > /tmp/pandapower_erp.zip
RUN /home/erp/bin/pip install /tmp/pandapower_erp.zip /tmp/pandapower_validator.zip

RUN git clone --depth 1 https://github.com/gisce/oorq.git -b api_v5 $LIB/oorq
RUN git clone --depth 1 https://github.com/gisce/spawn_oop.git $LIB/spawn_oop
RUN git clone --depth 1 https://github.com/gisce/poweremail.git $LIB/poweremail2
RUN git clone --depth 1 https://github.com/gisce/openerp-sentry.git -b v5_legacy $LIB/openerp-sentry
RUN git clone --depth 1 https://github.com/gisce/ws_transactions.git $LIB/ws_transactions
RUN git clone --depth 1 https://github.com/gisce/ir_attachment_mongodb.git $LIB/ir_attachment_mongodb
RUN git clone --depth 1 https://github.com/gisce/mongodb_backend.git -b gisce $LIB/mongodb_backend
RUN git clone --depth 1 https://github.com/gisce/poweremail_oorq.git $LIB/poweremail_oorq

COPY docker/entrypoint.sh /
COPY docker/server.conf.tmpl /tmp/server.conf.tmpl

ENTRYPOINT ["/entrypoint.sh"]
CMD ["openerp-server"]
